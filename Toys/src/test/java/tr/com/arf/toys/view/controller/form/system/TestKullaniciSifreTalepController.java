package tr.com.arf.toys.view.controller.form.system;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.postgresql.ds.PGPoolingDataSource;

import javax.faces.application.FacesMessage;
import javax.naming.Context;
import javax.naming.InitialContext;

public class TestKullaniciSifreTalepController {

    @BeforeClass
    public static void setUpClass() throws Exception {
        // rcarver - setup the jndi context and the datasource
        // Create initial context
        System.setProperty(Context.INITIAL_CONTEXT_FACTORY,
                "org.apache.naming.java.javaURLContextFactory");
        System.setProperty(Context.URL_PKG_PREFIXES,
                "org.apache.naming");
        InitialContext ic = new InitialContext();

        ic.createSubcontext("java:");
        ic.createSubcontext("java:comp");
        ic.createSubcontext("java:comp/env");
        ic.createSubcontext("java:comp/env/jdbc");

        PGPoolingDataSource ds = new PGPoolingDataSource();
        ds.setUser("toys");
        ds.setPassword("123qwe");
        ds.setDatabaseName("TOYS");
        ds.setServerName("localhost");
        ds.setPortNumber(5432);


        ic.bind("java:comp/env/jdbc/DsWebAppDB", ds);
    }

    @Test
    public void testAdres() throws Exception {
        KullaniciSifreTalepControllerImpl controller = new KullaniciSifreTalepControllerImpl();

        controller.validateTCKimlikNumarasi("123");
        Assert.assertEquals("TC Kimlik Numarası Geçersiz!", controller.getMessage());

        controller.setMessage(null);
        controller.validateTCKimlikNumarasi("11111111110");
        Assert.assertNull(controller.getMessage());

        controller.validateTCKimlikNumarasi("12345678901");
        Assert.assertEquals("TC Kimlik Numarası Geçersiz!", controller.getMessage());

        controller.setMessage(null);
        controller.validateTCKimlikNumarasi("01111111110");
        Assert.assertEquals("TC Kimlik Numarası Geçersiz!", controller.getMessage());

        controller.setMessage(null);
        controller.validateTCKimlikNumarasi("1111111111a");
        Assert.assertEquals("TC Kimlik Numarası Geçersiz!", controller.getMessage());

        controller.setMessage(null);
        controller.validateTCKimlikNumarasi("12345678950");
        Assert.assertNull(controller.getMessage());
    }

    private class KullaniciSifreTalepControllerImpl extends KullaniciSifreTalepController {
        private String message;

        @Override
        public void createGenericMessage(String messageValue, FacesMessage.Severity messageSeverity) {
            message = messageValue;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

    }

}
