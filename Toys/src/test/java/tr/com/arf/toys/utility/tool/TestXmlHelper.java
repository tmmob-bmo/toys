package tr.com.arf.toys.utility.tool;

import org.junit.BeforeClass;
import org.junit.Test;
import org.postgresql.ds.PGConnectionPoolDataSource;
import org.postgresql.ds.PGPoolingDataSource;
import tr.com.arf.framework.db.enumerated._common.EvetHayir;
import tr.com.arf.toys.db.enumerated.iletisim.AdresTuru;
import tr.com.arf.toys.db.model.iletisim.Adres;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import static org.junit.Assert.assertNotNull;

public class TestXmlHelper {

    private String ADRES = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n" +
            "<KimlikNoileKisiAdresBilgileri xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\"\n" +
            "                               xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">\n" +
            "    <ExtensionData/>\n" +
            "    <DigerAdresBilgileri>\n" +
            "        <KimlikNoileKisiAdresBilgisi>\n" +
            "            <ExtensionData/>\n" +
            "            <AdresNo xsi:nil=\"true\"/>\n" +
            "            <BeyanTarihi>\n" +
            "                <ExtensionData/>\n" +
            "                <Ay xsi:nil=\"true\"/>\n" +
            "                <Gun xsi:nil=\"true\"/>\n" +
            "                <Yil xsi:nil=\"true\"/>\n" +
            "            </BeyanTarihi>\n" +
            "            <BeyandaBulunanKimlikNo xsi:nil=\"true\"/>\n" +
            "            <HataBilgisi>\n" +
            "                <ExtensionData/>\n" +
            "                <Aciklama>KayitBulunamadi</Aciklama>\n" +
            "                <Kod>2</Kod>\n" +
            "            </HataBilgisi>\n" +
            "            <TasinmaTarihi>\n" +
            "                <ExtensionData/>\n" +
            "                <Ay xsi:nil=\"true\"/>\n" +
            "                <Gun xsi:nil=\"true\"/>\n" +
            "                <Yil xsi:nil=\"true\"/>\n" +
            "            </TasinmaTarihi>\n" +
            "            <TescilTarihi>\n" +
            "                <ExtensionData/>\n" +
            "                <Ay xsi:nil=\"true\"/>\n" +
            "                <Gun xsi:nil=\"true\"/>\n" +
            "                <Yil xsi:nil=\"true\"/>\n" +
            "            </TescilTarihi>\n" +
            "        </KimlikNoileKisiAdresBilgisi>\n" +
            "    </DigerAdresBilgileri>\n" +
            "    <KimlikNo>49768594956</KimlikNo>\n" +
            "    <YerlesimYeriAdresi>\n" +
            "        <ExtensionData/>\n" +
            "        <AcikAdres>Eymir manzaralı bir ev No: 66/6 GÖLBAŞI / ANKARA</AcikAdres>\n" +
            "        <AdresNo>123321</AdresNo>\n" +
            "        <AdresTip>\n" +
            "            <ExtensionData/>\n" +
            "            <Aciklama>İl/İlçe Merkezi</Aciklama>\n" +
            "            <Kod>1</Kod>\n" +
            "        </AdresTip>\n" +
            "        <BeyanTarihi>\n" +
            "            <ExtensionData/>\n" +
            "            <Ay>4</Ay>\n" +
            "            <Gun>15</Gun>\n" +
            "            <Yil>2010</Yil>\n" +
            "        </BeyanTarihi>\n" +
            "        <BeyandaBulunanKimlikNo>12345678950</BeyandaBulunanKimlikNo>\n" +
            "        <IlIlceMerkezAdresi>\n" +
            "            <ExtensionData/>\n" +
            "            <BagimsizBolumDurum>\n" +
            "                <ExtensionData/>\n" +
            "                <Aciklama>Bilinmeyen</Aciklama>\n" +
            "                <Kod>1</Kod>\n" +
            "            </BagimsizBolumDurum>\n" +
            "            <BagimsizBolumTipi>\n" +
            "                <ExtensionData/>\n" +
            "                <Aciklama>Özel</Aciklama>\n" +
            "                <Kod>2</Kod>\n" +
            "            </BagimsizBolumTipi>\n" +
            "            <BinaAda>2211</BinaAda>\n" +
            "            <BinaBlokAdi>Bizim BLOK</BinaBlokAdi>\n" +
            "            <BinaDurum>\n" +
            "                <ExtensionData/>\n" +
            "                <Aciklama>Bilinmeyen</Aciklama>\n" +
            "                <Kod>1</Kod>\n" +
            "            </BinaDurum>\n" +
            "            <BinaKodu>1111</BinaKodu>\n" +
            "            <BinaNo>2222</BinaNo>\n" +
            "            <BinaNumaratajTipi>\n" +
            "                <ExtensionData/>\n" +
            "                <Aciklama>Bina Ana Giriş</Aciklama>\n" +
            "                <Kod>1</Kod>\n" +
            "            </BinaNumaratajTipi>\n" +
            "            <BinaParsel>1</BinaParsel>\n" +
            "            <BinaYapiTipi>\n" +
            "                <ExtensionData/>\n" +
            "                <Aciklama>Mesken</Aciklama>\n" +
            "                <Kod>1</Kod>\n" +
            "            </BinaYapiTipi>\n" +
            "            <Csbm>Ana CAD.</Csbm>\n" +
            "            <CsbmKodu>4444</CsbmKodu>\n" +
            "            <DisKapiNo>66</DisKapiNo>\n" +
            "            <IcKapiNo>6</IcKapiNo>\n" +
            "            <Il>ANKARA</Il>\n" +
            "            <IlKodu>6</IlKodu>\n" +
            "            <Ilce>GÖLBAŞI</Ilce>\n" +
            "            <IlceKodu>1744</IlceKodu>\n" +
            "            <Mahalle>EYMİR MAH.</Mahalle>\n" +
            "            <MahalleKodu>5555</MahalleKodu>\n" +
            "            <YapiKullanimAmac>\n" +
            "                <ExtensionData/>\n" +
            "                <Aciklama>Mesken</Aciklama>\n" +
            "                <Kod>1110</Kod>\n" +
            "            </YapiKullanimAmac>\n" +
            "            <YolAltiKatSayisi xsi:nil=\"true\"/>\n" +
            "            <YolUstuKatSayisi xsi:nil=\"true\"/>\n" +
            "        </IlIlceMerkezAdresi>\n" +
            "        <TasinmaTarihi>\n" +
            "            <ExtensionData/>\n" +
            "            <Ay>3</Ay>\n" +
            "            <Gun>26</Gun>\n" +
            "            <Yil>2010</Yil>\n" +
            "        </TasinmaTarihi>\n" +
            "        <TescilTarihi>\n" +
            "            <ExtensionData/>\n" +
            "            <Ay>4</Ay>\n" +
            "            <Gun>15</Gun>\n" +
            "            <Yil>2010</Yil>\n" +
            "        </TescilTarihi>\n" +
            "    </YerlesimYeriAdresi>\n" +
            "</KimlikNoileKisiAdresBilgileri>";


    @BeforeClass
    public static void setUpClass() throws Exception {
        // rcarver - setup the jndi context and the datasource
        // Create initial context
        System.setProperty(Context.INITIAL_CONTEXT_FACTORY,
                "org.apache.naming.java.javaURLContextFactory");
        System.setProperty(Context.URL_PKG_PREFIXES,
                "org.apache.naming");
        InitialContext ic = new InitialContext();

        ic.createSubcontext("java:");
        ic.createSubcontext("java:comp");
        ic.createSubcontext("java:comp/env");
        ic.createSubcontext("java:comp/env/jdbc");

        PGPoolingDataSource ds = new PGPoolingDataSource();
        ds.setUser("toys");
        ds.setPassword("123qwe");
        ds.setDatabaseName("TOYS");
        ds.setServerName("localhost");
        ds.setPortNumber(5432);


        ic.bind("java:comp/env/jdbc/DsWebAppDB", ds);
    }

    @Test
    public void testAdres() throws Exception {
        Adres kpsAdres = XmlHelper.kisiAdresAktar(ADRES, AdresTuru._KPSADRESI, EvetHayir._EVET);
        assertNotNull(kpsAdres);
        Adres digerAdres = XmlHelper.kisiAdresAktar(ADRES, AdresTuru._KPSDIGERADRESI, EvetHayir._EVET);
        assertNotNull(digerAdres);
    }

}
