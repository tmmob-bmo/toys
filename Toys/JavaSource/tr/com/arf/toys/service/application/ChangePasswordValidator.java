package tr.com.arf.toys.service.application;


import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ChangePasswordValidator {
    private String pattern = null;

    /**
     * No one can make a direct instance
     * */
    private ChangePasswordValidator() {
	// do nothing
    }

    public static ChangePasswordValidator buildValidator() {
        return buildValidator(false, true, true, 6, 20);
    }

    /**
     * Force the user to build a validator using this way only
     * */
    public static ChangePasswordValidator buildValidator(boolean forceSpecialChar, boolean forceCapitalLetter, boolean forceNumber, int minLength, int maxLength) {
		StringBuilder patternBuilder = new StringBuilder("((?=.*[a-z])");
	
		if (forceSpecialChar) {
		    patternBuilder.append("(?=.*[&@#$%])");
		}
	
		if (forceCapitalLetter) {
		    patternBuilder.append("(?=.*[A-Z])");
		}
	
		if (forceNumber) {
		    patternBuilder.append("(?=.*\\d)");
		}
	
		patternBuilder.append(".{")
                .append(minLength)
                .append(",")
                .append(maxLength)
                .append("})");

        ChangePasswordValidator validator = new ChangePasswordValidator();
        validator.pattern = patternBuilder.toString();

		return validator;
    }

    /**
     * Here we will validate the password
     */
    public boolean validatePassword(final String password) {
		Pattern p = Pattern.compile(pattern);
		Matcher m = p.matcher(password);
		return m.matches();
    }

}
