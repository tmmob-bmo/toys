package tr.com.arf.toys.service.application;

import java.util.ArrayList;
import java.util.Locale;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.component.UIViewRoot;
import javax.faces.context.FacesContext;

import tr.com.arf.framework.utility.tool.DateUtil;

@ManagedBean
@ApplicationScoped
public class ConfigurationController {
	 	
	/* Data Table */
	private final String edt_selectionMode = "single";
	private final String edt_sortMode = "single";
	private final String edt_height = "250px";
	private final String edt_height_largest = "400px";
	private final String edt_height_larger = "360px";
	private final String edt_height_large = "320px";
	private String edt_height_medium = "280px";
	private String edt_height_tabPanel = "280px";
	private String edt_height_small = "196px";
	private String edt_height_smaller = "150px";
	private final boolean exportRendered = false;
	private String edt_height_550px = "550px";
	
	private final String calendarYearRangeForBirthdays = "1920:2010";
	
	/* Date */
	private final String datePattern = DateUtil.getDatePattern();
	private final String dateTimePattern = DateUtil.getDateTimePattern();
	//private final String timePattern = DateUtil.getHourMinutePattern();
	private final String timeZone = java.util.TimeZone.getDefault().getID();
	
	/* Chart */
	private final ArrayList<String> columnChartColors = new ArrayList<String>();
		
	public ConfigurationController() {
		columnChartColors.add("#5DB2C2");
		columnChartColors.add("#B0343C");
		columnChartColors.add("#DD9F2C");
		
		UIViewRoot viewRoot = FacesContext.getCurrentInstance().getViewRoot();
		viewRoot.setLocale(Locale.getDefault());		
	} 
	
	/* Data Table */
	public String getEdt_selectionMode() {
		return edt_selectionMode;
	}
	
	public String getEdt_sortMode() {
		return edt_sortMode;
	}
	
	public String getEdt_height() {
		return edt_height;
	}
	
	public String getEdt_height_medium() {
		return edt_height_medium;
	}
	
	public void setEdt_height_medium(String edt_height_medium) {
		this.edt_height_medium = edt_height_medium;
	}
	
	public String getEdt_height_small() {
		return edt_height_small;
	}
	
	public void setEdt_height_small(String edt_height_small) {
		this.edt_height_small = edt_height_small;
	}
	
	public String getEdt_height_tabPanel() {
		return edt_height_tabPanel;
	}

	public void setEdt_height_tabPanel(String edt_height_tabPanel) {
		this.edt_height_tabPanel = edt_height_tabPanel;
	}

	public boolean isExportRendered() {
		return exportRendered;
	}

	/* Date */
	public String getDatePattern() {
		return datePattern;
	}
		
//	public String getTimePattern() {
//		return timePattern;
//	}

	public String getDateTimePattern() {
		return dateTimePattern;
	}
	
	public String getTimeZone() {
		return timeZone;
	}
	
	/* Chart */
	public ArrayList<String> getColumnChartColors() {
		return columnChartColors;
	}

	public String getEdt_height_smaller() {
		return edt_height_smaller;
	}

	public void setEdt_height_smaller(String edt_height_smaller) {
		this.edt_height_smaller = edt_height_smaller;
	}

	public String getEdt_height_largest() {
		return edt_height_largest;
	}

	public String getEdt_height_larger() {
		return edt_height_larger;
	}

	public String getEdt_height_large() {
		return edt_height_large;
	}

	public String getEdt_height_550px() {
		return edt_height_550px;
	}

	public void setEdt_height_550px(String edt_height_550px) {
		this.edt_height_550px = edt_height_550px;
	}

	public String getCalendarYearRangeForBirthdays() {
		return calendarYearRangeForBirthdays;
	}  
	
}
