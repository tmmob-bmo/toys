package tr.com.arf.toys.service.application;

public class ApplicationDescriptor {

	/* Common */
	public static final String _LOGIN_OUTCOME = "login.do";
	public static final String _UYELOGIN_OUTCOME = "uyelogin.do";
	public static final String _USERROLE_OUTCOME = "userRole.do";
	public static final String _REGISTER_OUTCOME = "register.do";
	public static final String _REGISTER_SUMMARY_OUTCOME = "register_Summary.do";
	public static final String _DASHBOARD_OUTCOME = "dashboard.do";
	public static final String _UYEDASHBOARD_OUTCOME = "uyedashboard.do";
	public static final String _ACCESSDENIED_OUTCOME = "accessDenied.do";
	public static final String _NOFUNCTIONALITY_OUTCOME = "noFunctionality.do";
	public static final String _EXCEPTION_OUTCOME = "exception.do";
	public static final String _REPORT_OUTCOME = "report.do";
	public static final String _PASSWORDCHANGE_OUTCOME = "passwordChange.do";
	public static final String _PASSWORDREQUEST_OUTCOME = "passwordRequest.do";
	public static final String _PAROLE_EXPIRED_OUTCOME = "parole_Expired.do";
	public static final String _ACCOUNT_LOCKED_OUTCOME = "account_Locked.do";

	public static final String _SEHIR_MODEL = "Sehir";
	public static final String _SEHIR_CLASS =
 		"tr.com.arf.toys.db.model.basvuru.Il";
 	public static final String _SEHIR_OUTCOME = "sehir.do";
 	public static final String _SEHIR_EDIT_OUTCOME = "sehir_Edit.do";

 	public static final String _ILCE_MODEL = "Ilce";
	public static final String _ILCE_CLASS =
 		"tr.com.arf.toys.db.model.basvuru.Ilce";
 	public static final String _ILCE_OUTCOME = "ilce.do";
 	public static final String _ILCE_EDIT_OUTCOME = "ilce_Edit.do";

 	public static final String _KOY_MODEL = "Koy";
	public static final String _KOY_CLASS =
 		"tr.com.arf.toys.db.model.basvuru.Koy";
 	public static final String _KOY_OUTCOME = "koy.do";
 	public static final String _KOY_EDIT_OUTCOME = "koy_Edit.do";

	public static final String _BIRIM_MODEL = "Birim";
	public static final String _BIRIM_CLASS =
 		"tr.com.arf.toys.db.model.system.Birim";
 	public static final String _BIRIM_OUTCOME = "birim.do";
 	public static final String _BIRIM_EDIT_OUTCOME = "birim_Edit.do";
 	public static final String _BIRIM_TREE_OUTCOME = "birim_Tree.do";

 	public static final String _PERSONEL_MODEL = "Personel";
	public static final String _PERSONEL_CLASS =
 		"tr.com.arf.toys.db.model.system.Personel";
 	public static final String _PERSONEL_OUTCOME = "personel.do";
 	public static final String _PERSONEL_EDIT_OUTCOME = "personel_Edit.do";

	public static final String _KULLANICI_MODEL = "Kullanici";
	public static final String _KULLANICI_CLASS =
 		"tr.com.arf.toys.db.model.system.Kullanici";
 	public static final String _KULLANICI_OUTCOME = "kullanici.do";
 	public static final String _KULLANICI_EDIT_OUTCOME = "kullanici_Edit.do";

	public static final String _KULLANICIROLE_MODEL = "KullaniciRole";
	public static final String _KULLANICIROLE_CLASS =
 		"tr.com.arf.toys.db.model.system.KullaniciRole";
 	public static final String _KULLANICIROLE_OUTCOME = "kullanicirole.do";
 	public static final String _KULLANICIROLE_EDIT_OUTCOME = "kullanicirole_Edit.do";

 	public static final String _ISLEM_MODEL = "Islem";
	public static final String _ISLEM_CLASS =
 		"tr.com.arf.toys.db.model.system.Islem";
 	public static final String _ISLEM_OUTCOME = "islem.do";
 	public static final String _ISLEM_EDIT_OUTCOME = "islem_Edit.do";

	public static final String _ROLE_OUTCOME = "role.do";
	public static final String _ROLE_MODEL = "Role";
	public static final String _ISLEMROLE_MODEL = "IslemRole";
	public static final String _ISLEMROLE_CLASS =
 		"tr.com.arf.toys.db.model.system.IslemRole";
 	public static final String _ISLEMROLE_OUTCOME = "islemrole.do";
 	public static final String _ISLEMROLE_EDIT_OUTCOME = "islemrole_Edit.do";

 	public static final String _ISLEMKULLANICI_MODEL = "IslemKullanici";
	public static final String _ISLEMKULLANICI_CLASS =
 		"tr.com.arf.toys.db.model.system.IslemKullanici";
 	public static final String _ISLEMKULLANICI_OUTCOME = "islemkullanici.do";
 	public static final String _ISLEMKULLANICI_EDIT_OUTCOME = "islemkullanici_Edit.do";

	public static final String _MENUITEM_MODEL = "Menuitem";
	public static final String _MENUITEM_CLASS =
 		"tr.com.arf.toys.db.model.system.Menuitem";
 	public static final String _MENUITEM_OUTCOME = "menuitem.do";
 	public static final String _MENUITEM_EDIT_OUTCOME = "menuitem_Edit.do";
 	public static final String _MENUITEM_TREE_OUTCOME = "menuitem_Tree.do";


	/* FILTER ITEM CONSTANTS */
	public static final String _ULKE_MODEL = "Ulke";
	public static final String _ULKE_CLASS =
 		"tr.com.arf.toys.db.model.system.Ulke";
 	public static final String _ULKE_OUTCOME = "ulke.do";
 	public static final String _ULKE_EDIT_OUTCOME = "ulke_Edit.do";

	public static final String _TELEFON_MODEL = "Telefon";
	public static final String _TELEFON_CLASS =
 		"tr.com.arf.toys.db.model.iletisim.Telefon";
 	public static final String _TELEFON_OUTCOME = "telefon.do";
 	public static final String _TELEFON_EDIT_OUTCOME = "telefon_Edit.do";

 	public static final String _INTERNETADRES_MODEL = "Internetadres";
	public static final String _INTERNETADRES_CLASS =
 		"tr.com.arf.toys.db.model.iletisim.Internetadres";
 	public static final String _INTERNETADRES_OUTCOME = "internetadres.do";
 	public static final String _INTERNETADRES_EDIT_OUTCOME = "internetadres_Edit.do";

 	public static final String _KISI_MODEL = "Kisi";
	public static final String _KISI_CLASS =
 		"tr.com.arf.toys.db.model.kisi.Kisi";
 	public static final String _KISI_OUTCOME = "kisi.do";
 	public static final String _KISI_EDIT_OUTCOME = "kisi_Edit.do";

	public static final String _KURUM_MODEL = "Kurum";
	public static final String _KURUM_CLASS =
 		"tr.com.arf.toys.db.model.kisi.Kurum";
 	public static final String _KURUM_OUTCOME = "kurum.do";
 	public static final String _KURUM_EDIT_OUTCOME = "kurum_Edit.do";

 	public static final String _KURUMHESAP_MODEL = "KurumHesap";
	public static final String _KURUMHESAP_CLASS =
 		"tr.com.arf.toys.db.model.system.KurumHesap";
 	public static final String _KURUMHESAP_OUTCOME = "kurumhesap.do";
 	public static final String _KURUMHESAP_EDIT_OUTCOME = "kurumhesap_Edit.do";

 	public static final String _BIRIMHESAP_MODEL = "BirimHesap";
	public static final String _BIRIMHESAP_CLASS =
 		"tr.com.arf.toys.db.model.system.BirimHesap";
 	public static final String _BIRIMHESAP_OUTCOME = "birimhesap.do";
 	public static final String _BIRIMHESAP_EDIT_OUTCOME = "birimhesap_Edit.do";

 	public static final String _KISIIZIN_MODEL = "Kisiizin";
	public static final String _KISIIZIN_CLASS =
 		"tr.com.arf.toys.db.model.kisi.Kisiizin";
 	public static final String _KISIIZIN_OUTCOME = "kisiizin.do";
 	public static final String _KISIIZIN_EDIT_OUTCOME = "kisiizin_Edit.do";

 	public static final String _KISIFOTOGRAF_MODEL = "Kisifotograf";
	public static final String _KISIFOTOGRAF_CLASS =
 		"tr.com.arf.toys.db.model.kisi.Kisifotograf";
 	public static final String _KISIFOTOGRAF_OUTCOME = "kisifotograf.do";
 	public static final String _KISIFOTOGRAF_EDIT_OUTCOME = "kisifotograf_Edit.do";

 	public static final String _EVRAK_MODEL = "Evrak";
	public static final String _EVRAK_CLASS =
 		"tr.com.arf.toys.db.model.evrak.Evrak";
 	public static final String _EVRAK_OUTCOME = "evrak.do";
 	public static final String _EVRAK_EDIT_OUTCOME = "evrak_Edit.do";

 	public static final String _KISIKIMLIK_MODEL = "Kisikimlik";
	public static final String _KISIKIMLIK_CLASS =
 		"tr.com.arf.toys.db.model.kisi.Kisikimlik";
 	public static final String _KISIKIMLIK_OUTCOME = "kisikimlik.do";
 	public static final String _KISIKIMLIK_EDIT_OUTCOME = "kisikimlik_Edit.do";

 	public static final String _KISIUYRUK_MODEL = "Kisiuyruk";
	public static final String _KISIUYRUK_CLASS =
 		"tr.com.arf.toys.db.model.kisi.Kisiuyruk";
 	public static final String _KISIUYRUK_OUTCOME = "kisiuyruk.do";
 	public static final String _KISIUYRUK_EDIT_OUTCOME = "kisiuyruk_Edit.do";

	public static final String _ADRES_MODEL = "Adres";
	public static final String _ADRES_CLASS =
 		"tr.com.arf.toys.db.model.iletisim.Adres";
 	public static final String _ADRES_OUTCOME = "adres.do";
 	public static final String _ADRES_EDIT_OUTCOME = "adres_Edit.do";


	public static final String _GOREV_MODEL = "Gorev";
	public static final String _GOREV_CLASS =
 		"tr.com.arf.toys.db.model.gorev.Gorev";
 	public static final String _GOREV_OUTCOME = "gorev.do";
 	public static final String _GOREV_EDIT_OUTCOME = "gorev_Edit.do";
 	public static final String _GOREV_TREE_OUTCOME = "gorev_Tree.do";

	public static final String _UYE_MODEL = "Uye";
	public static final String _UYE_CLASS =
 		"tr.com.arf.toys.db.model.uye.Uye";
 	public static final String _UYE_OUTCOME = "uye.do";
 	public static final String _UYE_EDIT_OUTCOME = "uye_Edit.do";

	public static final String _AIDAT_MODEL = "Aidat";
	public static final String _AIDAT_CLASS =
 		"tr.com.arf.toys.db.model.system.Aidat";
 	public static final String _AIDAT_OUTCOME = "aidat.do";
 	public static final String _AIDAT_EDIT_OUTCOME = "aidat_Edit.do";

 	public static final String _UYEAIDAT_MODEL = "Uyeaidat";
	public static final String _UYEAIDAT_CLASS =
 		"tr.com.arf.toys.db.model.uye.Uyeaidat";
 	public static final String _UYEAIDAT_OUTCOME = "uyeaidat.do";
 	public static final String _UYEAIDAT_EDIT_OUTCOME = "uyeaidat_Edit.do";

	public static final String _UYEMUAFIYET_MODEL = "Uyemuafiyet";
	public static final String _UYEMUAFIYET_CLASS =
 		"tr.com.arf.toys.db.model.uye.Uyemuafiyet";
 	public static final String _UYEMUAFIYET_OUTCOME = "uyemuafiyet.do";
 	public static final String _UYEMUAFIYET_EDIT_OUTCOME = "uyemuafiyet_Edit.do";

 	public static final String _UYEODEME_MODEL = "Uyeodeme";
	public static final String _UYEODEME_CLASS =
 		"tr.com.arf.toys.db.model.uye.Uyeodeme";
 	public static final String _UYEODEME_OUTCOME = "uyeodeme.do";
 	public static final String _UYEODEME_EDIT_OUTCOME = "uyeodeme_Edit.do";

// 	public static final String _UYEKURUM_MODEL = "Uyekurum";
//	public static final String _UYEKURUM_CLASS =
// 		"tr.com.arf.toys.db.model.uye.Uyekurum";
// 	public static final String _UYEKURUM_OUTCOME = "uyekurum.do";
// 	public static final String _UYEKURUM_EDIT_OUTCOME = "uyekurum_Edit.do";

	public static final String _BELGETIP_MODEL = "Belgetip";
	public static final String _BELGETIP_CLASS =
 		"tr.com.arf.toys.db.model.system.Belgetip";
 	public static final String _BELGETIP_OUTCOME = "belgetip.do";
 	public static final String _BELGETIP_EDIT_OUTCOME = "belgetip_Edit.do";

 	public static final String _UYEBELGE_MODEL = "Uyebelge";
	public static final String _UYEBELGE_CLASS =
 		"tr.com.arf.toys.db.model.uye.Uyebelge";
 	public static final String _UYEBELGE_OUTCOME = "uyebelge.do";
 	public static final String _UYEBELGE_EDIT_OUTCOME = "uyebelge_Edit.do";

 	public static final String _UZMANLIKTIP_MODEL = "Uzmanliktip";
	public static final String _UZMANLIKTIP_CLASS =
 		"tr.com.arf.toys.db.model.system.Uzmanliktip";
 	public static final String _UZMANLIKTIP_OUTCOME = "uzmanliktip.do";
 	public static final String _UZMANLIKTIP_EDIT_OUTCOME = "uzmanliktip_Edit.do";

 	public static final String _UYEUZMANLIK_MODEL = "Uyeuzmanlik";
	public static final String _UYEUZMANLIK_CLASS =
 		"tr.com.arf.toys.db.model.uye.Uyeuzmanlik";
 	public static final String _UYEUZMANLIK_OUTCOME = "uyeuzmanlik.do";
 	public static final String _UYEUZMANLIK_EDIT_OUTCOME = "uyeuzmanlik_Edit.do";

 	public static final String _UYEBILIRKISI_MODEL = "Uyebilirkisi";
	public static final String _UYEBILIRKISI_CLASS =
 		"tr.com.arf.toys.db.model.uye.Uyebilirkisi";
 	public static final String _UYEBILIRKISI_OUTCOME = "uyebilirkisi.do";
 	public static final String _UYEBILIRKISI_EDIT_OUTCOME = "uyebilirkisi_Edit.do";

 	public static final String _OGRENIMKURUM_MODEL = "OgrenimKurum";
	public static final String _OGRENIMKURUM_CLASS =
 		"tr.com.arf.toys.db.model.system.OgrenimKurum";
 	public static final String _OGRENIMKURUM_OUTCOME = "ogrenimKurum.do";
 	public static final String _OGRENIMKURUM_EDIT_OUTCOME = "ogrenimKurum_Edit.do";

 	public static final String _KISIOGRENIM_MODEL = "Kisiogrenim";
	public static final String _KISIOGRENIM_CLASS =
 		"tr.com.arf.toys.db.model.kisi.Kisiogrenim";
 	public static final String _KISIOGRENIM_OUTCOME = "kisiogrenim.do";
 	public static final String _KISIOGRENIM_EDIT_OUTCOME = "kisiogrenim_Edit.do";

 	public static final String _UYESIGORTA_MODEL = "Uyesigorta";
	public static final String _UYESIGORTA_CLASS =
 		"tr.com.arf.toys.db.model.uye.Uyesigorta";
 	public static final String _UYESIGORTA_OUTCOME = "uyesigorta.do";
 	public static final String _UYESIGORTA_EDIT_OUTCOME = "uyesigorta_Edit.do";

 	public static final String _CEZATIP_MODEL = "Cezatip";
	public static final String _CEZATIP_CLASS =
 		"tr.com.arf.toys.db.model.system.Cezatip";
 	public static final String _CEZATIP_OUTCOME = "cezatip.do";
 	public static final String _CEZATIP_EDIT_OUTCOME = "cezatip_Edit.do";

 	public static final String _UYECEZA_MODEL = "Uyeceza";
	public static final String _UYECEZA_CLASS =
 		"tr.com.arf.toys.db.model.uye.Uyeceza";
 	public static final String _UYECEZA_OUTCOME = "uyeceza.do";
 	public static final String _UYECEZA_EDIT_OUTCOME = "uyeceza_Edit.do";

 	public static final String _ODULTIP_MODEL = "Odultip";
	public static final String _ODULTIP_CLASS =
 		"tr.com.arf.toys.db.model.system.Odultip";
 	public static final String _ODULTIP_OUTCOME = "odultip.do";
 	public static final String _ODULTIP_EDIT_OUTCOME = "odultip_Edit.do";

 	public static final String _UYEODUL_MODEL = "Uyeodul";
	public static final String _UYEODUL_CLASS =
 		"tr.com.arf.toys.db.model.uye.Uyeodul";
 	public static final String _UYEODUL_OUTCOME = "uyeodul.do";
 	public static final String _UYEODUL_EDIT_OUTCOME = "uyeodul_Edit.do";

 	public static final String _UYARINOT_MODEL = "Uyarinot";
	public static final String _UYARINOT_CLASS =
 		"tr.com.arf.toys.db.model.system.Uyarinot";
 	public static final String _UYARINOT_OUTCOME = "uyarinot.do";
 	public static final String _UYARINOT_EDIT_OUTCOME = "uyarinot_Edit.do";

 	public static final String _UYEABONE_MODEL = "Uyeabone";
	public static final String _UYEABONE_CLASS =
 		"tr.com.arf.toys.db.model.uye.Uyeabone";
 	public static final String _UYEABONE_OUTCOME = "uyeabone.do";
 	public static final String _UYEABONE_EDIT_OUTCOME = "uyeabone_Edit.do";

 	public static final String _ODEMETIP_MODEL = "Odemetip";
	public static final String _ODEMETIP_CLASS =
 		"tr.com.arf.toys.db.model.system.Odemetip";
 	public static final String _ODEMETIP_OUTCOME = "odemetip.do";
 	public static final String _ODEMETIP_EDIT_OUTCOME = "odemetip_Edit.do";

 	public static final String _BORC_MODEL = "Borc";
	public static final String _BORC_CLASS =
 		"tr.com.arf.toys.db.model.finans.Borc";
 	public static final String _BORC_OUTCOME = "borc.do";
 	public static final String _BORC_EDIT_OUTCOME = "borc_Edit.do";

	public static final String _BORCODEME_MODEL = "Borcodeme";
	public static final String _BORCODEME_CLASS =
 		"tr.com.arf.toys.db.model.finans.Borcodeme";
 	public static final String _BORCODEME_OUTCOME = "borcodeme.do";
 	public static final String _BORCODEME_EDIT_OUTCOME = "borcodeme_Edit.do";

	public static final String _YAYIN_MODEL = "Yayin";
	public static final String _YAYIN_CLASS =
 		"tr.com.arf.toys.db.model.system.Yayin";
 	public static final String _YAYIN_OUTCOME = "yayin.do";
 	public static final String _YAYIN_EDIT_OUTCOME = "yayin_Edit.do";

	public static final String _PERSONELBIRIMGOREV_MODEL = "PersonelBirimGorev";
	public static final String _PERSONELBIRIMGOREV_CLASS =
 		"tr.com.arf.toys.db.model.system.PersonelBirimGorev";
 	public static final String _PERSONELBIRIMGOREV_OUTCOME = "personelBirimGorev.do";
 	public static final String _PERSONELBIRIMGOREV_EDIT_OUTCOME = "personelBirimGorev_Edit.do";

 	public static final String _KURUMTURU_MODEL = "KurumTuru";
	public static final String _KURUMTURU_CLASS =
 		"tr.com.arf.toys.db.model.system.KurumTuru";
 	public static final String _KURUMTURU_OUTCOME = "kurumTuru.do";
 	public static final String _KURUMTURU_EDIT_OUTCOME = "kurumTuru_Edit.do";

	public static final String _ODEME_MODEL = "Odeme";
	public static final String _ODEME_CLASS =
 		"tr.com.arf.toys.db.model.finans.Odeme";
 	public static final String _ODEME_OUTCOME = "odeme.do";
 	public static final String _ODEME_EDIT_OUTCOME = "odeme_Edit.do";


	public static final String _KOMISYON_MODEL = "Komisyon";
	public static final String _KOMISYON_CLASS =
 		"tr.com.arf.toys.db.model.system.Komisyon";
 	public static final String _KOMISYON_OUTCOME = "komisyon.do";
 	public static final String _KOMISYON_EDIT_OUTCOME = "komisyon_Edit.do";
 	public static final String _KOMISYON_TREE_OUTCOME = "komisyon_Tree.do";


	public static final String _ISLEMLOG_MODEL = "Islemlog";
	public static final String _ISLEMLOG_CLASS =
 		"tr.com.arf.toys.db.model.system.Islemlog";
 	public static final String _ISLEMLOG_OUTCOME = "islemlog.do";
 	public static final String _ISLEMLOG_EDIT_OUTCOME = "islemlog_Edit.do";

 	public static final String _SISTEMPARAMETRE_MODEL = "SistemParametre";
	public static final String _SISTEMPARAMETRE_CLASS =
 		"tr.com.arf.toys.db.model.system.SistemParametre";
 	public static final String _SISTEMPARAMETRE_OUTCOME = "sistemParametre.do";
 	public static final String _SISTEMPARAMETRE_EDIT_OUTCOME = "sistemParametre_Edit.do";

 	public static final String _DONEM_MODEL = "Donem";
	public static final String _DONEM_CLASS =
 		"tr.com.arf.toys.db.model.kisi.Donem";
 	public static final String _DONEM_OUTCOME = "donem.do";
 	public static final String _DONEM_EDIT_OUTCOME = "donem_Edit.do";

	public static final String _KURUL_MODEL = "Kurul";
	public static final String _KURUL_CLASS =
 		"tr.com.arf.toys.db.model.kisi.Kurul";
 	public static final String _KURUL_OUTCOME = "kurul.do";
 	public static final String _KURUL_EDIT_OUTCOME = "kurul_Edit.do";

 	public static final String _KURULDONEM_MODEL = "KurulDonem";
	public static final String _KURULDONEM_CLASS =
 		"tr.com.arf.toys.db.model.kisi.KurulDonem";
 	public static final String _KURULDONEM_OUTCOME = "kurulDonem.do";
 	public static final String _KURULDONEM_EDIT_OUTCOME = "kurulDonem_Edit.do";

 	public static final String _KURULGOREV_MODEL = "KurulGorev";
	public static final String _KURULGOREV_CLASS =
 		"tr.com.arf.toys.db.model.kisi.KurulGorev";
 	public static final String _KURULGOREV_OUTCOME = "kurulGorev.do";
 	public static final String _KURULGOREV_EDIT_OUTCOME = "kurulGorev_Edit.do";

 	public static final String _KURULDONEMUYE_MODEL = "KurulDonemUye";
	public static final String _KURULDONEMUYE_CLASS =
 		"tr.com.arf.toys.db.model.kisi.KurulDonemUye";
 	public static final String _KURULDONEMUYE_OUTCOME = "kurulDonemUye.do";
 	public static final String _KURULDONEMUYE_EDIT_OUTCOME = "kurulDonemUye_Edit.do";

 	public static final String _KOMISYONDONEM_MODEL = "KomisyonDonem";
	public static final String _KOMISYONDONEM_CLASS =
 		"tr.com.arf.toys.db.model.kisi.KomisyonDonem";
 	public static final String _KOMISYONDONEM_OUTCOME = "komisyonDonem.do";
 	public static final String _KOMISYONDONEM_EDIT_OUTCOME = "komisyonDonem_Edit.do";

 	public static final String _KOMISYONDONEMUYE_MODEL = "KomisyonDonemUye";
	public static final String _KOMISYONDONEMUYE_CLASS =
 		"tr.com.arf.toys.db.model.kisi.KomisyonDonemUye";
 	public static final String _KOMISYONDONEMUYE_OUTCOME = "komisyonDonemUye.do";
 	public static final String _KOMISYONDONEMUYE_EDIT_OUTCOME = "komisyonDonemUye_Edit.do";

 	public static final String _ISYERITEMSILCILERI_MODEL = "IsYeriTemsilcileri";
	public static final String _ISYERITEMSILCILERI_CLASS =
 		"tr.com.arf.toys.db.model.kisi.IsYeriTemsilcileri";
 	public static final String _ISYERITEMSILCILERI_OUTCOME = "isYeriTemsilcileri.do";
 	public static final String _ISYERITEMSILCILERI_EDIT_OUTCOME = "isYeriTemsilcileri_Edit.do";

 	public static final String _UNIVERSITE_MODEL = "Universite";
	public static final String _UNIVERSITE_CLASS =
 		"tr.com.arf.toys.db.model.system.Universite";
 	public static final String _UNIVERSITE_OUTCOME = "universite.do";
 	public static final String _UNIVERSITE_EDIT_OUTCOME = "universite_Edit.do";

 	public static final String _FAKULTE_MODEL = "Fakulte";
	public static final String _FAKULTE_CLASS =
 		"tr.com.arf.toys.db.model.system.Fakulte";
 	public static final String _FAKULTE_OUTCOME = "fakulte.do";
 	public static final String _FAKULTE_EDIT_OUTCOME = "fakulte_Edit.do";

	public static final String _BOLUM_MODEL = "Bolum";
	public static final String _BOLUM_CLASS =
 		"tr.com.arf.toys.db.model.system.Bolum";
 	public static final String _BOLUM_OUTCOME = "bolum.do";
 	public static final String _BOLUM_EDIT_OUTCOME = "bolum_Edit.do";


	public static final String _DOSYAKODU_MODEL = "Dosyakodu";
	public static final String _DOSYAKODU_CLASS =
 		"tr.com.arf.toys.db.model.evrak.Dosyakodu";
 	public static final String _DOSYAKODU_OUTCOME = "dosyakodu.do";
 	public static final String _DOSYAKODU_EDIT_OUTCOME = "dosyakodu_Edit.do";
 	public static final String _DOSYAKODU_TREE_OUTCOME = "dosyakodu_Tree.do";


	public static final String _EVRAKEK_MODEL = "Evrakek";
	public static final String _EVRAKEK_CLASS =
 		"tr.com.arf.toys.db.model.evrak.Evrakek";
 	public static final String _EVRAKEK_OUTCOME = "evrakek.do";
 	public static final String _EVRAKEK_EDIT_OUTCOME = "evrakek_Edit.do";

	public static final String _UYEDURUMDEGISTIRME_MODEL = "Uyedurumdegistirme";
	public static final String _UYEDURUMDEGISTIRME_CLASS =
 		"tr.com.arf.toys.db.model.uye.Uyedurumdegistirme";
 	public static final String _UYEDURUMDEGISTIRME_OUTCOME = "uyedurumdegistirme.do";
 	public static final String _UYEDURUMDEGISTIRME_EDIT_OUTCOME = "uyedurumdegistirme_Edit.do";

 	public static final String _UYEBASVURU_MODEL = "UyeBasvuru";
	public static final String _UYEBASVURU_CLASS =
 		"tr.com.arf.toys.db.model.evrak.UyeBasvuru";
 	public static final String _UYEBASVURU_OUTCOME = "uyeBasvuru.do";
 	public static final String _UYEBASVURU_EDIT_OUTCOME = "uyeBasvuru_Edit.do";

	public static final String _ANADONEM_MODEL = "Anadonem";
	public static final String _ANADONEM_CLASS =
 		"tr.com.arf.toys.db.model.donem.Anadonem";
 	public static final String _ANADONEM_OUTCOME = "anadonem.do";
 	public static final String _ANADONEM_EDIT_OUTCOME = "anadonem_Edit.do";

	public static final String _KURULDONEMTOPLANTI_MODEL = "KurulDonemToplanti";
	public static final String _KURULDONEMTOPLANTI_CLASS =
 		"tr.com.arf.toys.db.model.kisi.KurulDonemToplanti";
 	public static final String _KURULDONEMTOPLANTI_OUTCOME = "kurulDonemToplanti.do";
 	public static final String _KURULDONEMTOPLANTI_EDIT_OUTCOME = "kurulDonemToplanti_Edit.do";

	public static final String _KURULDONEMTOPLANTITUTANAK_MODEL = "KurulDonemToplantiTutanak";
	public static final String _KURULDONEMTOPLANTITUTANAK_CLASS =
 		"tr.com.arf.toys.db.model.kisi.KurulDonemToplantiTutanak";
 	public static final String _KURULDONEMTOPLANTITUTANAK_OUTCOME = "kurulDonemToplantiTutanak.do";
 	public static final String _KURULDONEMTOPLANTITUTANAK_EDIT_OUTCOME = "kurulDonemToplantiTutanak_Edit.do";

 	public static final String _KOMISYONDONEMTOPLANTI_MODEL = "KomisyonDonemToplanti";
	public static final String _KOMISYONDONEMTOPLANTI_CLASS =
 		"tr.com.arf.toys.db.model.kisi.KomisyonDonemToplanti";
 	public static final String _KOMISYONDONEMTOPLANTI_OUTCOME = "komisyonDonemToplanti.do";
 	public static final String _KOMISYONDONEMTOPLANTI_EDIT_OUTCOME = "komisyonDonemToplanti_Edit.do";

	public static final String _KOMISYONDONEMTOPLANTITUTANAK_MODEL = "KomisyonDonemToplantiTutanak";
	public static final String _KOMISYONDONEMTOPLANTITUTANAK_CLASS =
 		"tr.com.arf.toys.db.model.kisi.KomisyonDonemToplantiTutanak";
 	public static final String _KOMISYONDONEMTOPLANTITUTANAK_OUTCOME = "komisyonDonemToplantiTutanak.do";
 	public static final String _KOMISYONDONEMTOPLANTITUTANAK_EDIT_OUTCOME = "komisyonDonemToplantiTutanak_Edit.do";

	public static final String _KURULDONEMDELEGE_MODEL = "KurulDonemDelege";
	public static final String _KURULDONEMDELEGE_CLASS =
 		"tr.com.arf.toys.db.model.kisi.KurulDonemDelege";
 	public static final String _KURULDONEMDELEGE_OUTCOME = "kurulDonemDelege.do";
 	public static final String _KURULDONEMDELEGE_EDIT_OUTCOME = "kurulDonemDelege_Edit.do";

	public static final String _ILETISIMLISTE_MODEL = "Iletisimliste";
	public static final String _ILETISIMLISTE_CLASS =
 		"tr.com.arf.toys.db.model.iletisim.Iletisimliste";
 	public static final String _ILETISIMLISTE_OUTCOME = "iletisimliste.do";
 	public static final String _ILETISIMLISTE_EDIT_OUTCOME = "iletisimliste_Edit.do";
 	public static final String _ILETISIMLISTE_TREE_OUTCOME = "iletisimliste_Tree.do";

	public static final String _ILETISIMLISTEDETAY_MODEL = "Iletisimlistedetay";
	public static final String _ILETISIMLISTEDETAY_CLASS =
 		"tr.com.arf.toys.db.model.iletisim.Iletisimlistedetay";
 	public static final String _ILETISIMLISTEDETAY_OUTCOME = "iletisimlistedetay.do";
 	public static final String _ILETISIMLISTEDETAY_EDIT_OUTCOME = "iletisimlistedetay_Edit.do";

 	public static final String _GONDERIM_MODEL = "Gonderim";
	public static final String _GONDERIM_CLASS =
 		"tr.com.arf.toys.db.model.iletisim.Gonderim";
 	public static final String _GONDERIM_OUTCOME = "gonderim.do";
 	public static final String _GONDERIM_EDIT_OUTCOME = "gonderim_Edit.do";

	public static final String _ICERIK_MODEL = "Icerik";
	public static final String _ICERIK_CLASS =
 		"tr.com.arf.toys.db.model.iletisim.Icerik";
 	public static final String _ICERIK_OUTCOME = "icerik.do";
 	public static final String _ICERIK_EDIT_OUTCOME = "icerik_Edit.do";

 	public static final String _EPOSTAGONDERIMLOG_MODEL = "Epostagonderimlog";
	public static final String _EPOSTAGONDERIMLOG_CLASS =
 		"tr.com.arf.toys.db.model.iletisim.Epostagonderimlog";
 	public static final String _EPOSTAGONDERIMLOG_OUTCOME = "epostagonderimlog.do";
 	public static final String _EPOSTAGONDERIMLOG_EDIT_OUTCOME = "epostagonderimlog_Edit.do";

 	public static final String _SMSGONDERIMLOG_MODEL = "Smsgonderimlog";
	public static final String _SMSGONDERIMLOG_CLASS =
 		"tr.com.arf.toys.db.model.iletisim.Smsgonderimlog";
 	public static final String _SMSGONDERIMLOG_OUTCOME = "smsgonderimlog.do";
 	public static final String _SMSGONDERIMLOG_EDIT_OUTCOME = "smsgonderimlog_Edit.do";

 	public static final String _EPOSTAANLIKGONDERIMLOG_MODEL = "Epostaanlikgonderimlog";
	public static final String _EPOSTAANLIKGONDERIMLOG_CLASS =
 		"tr.com.arf.toys.db.model.iletisim.Epostaanlikgonderimlog";
 	public static final String _EPOSTAANLIKGONDERIMLOG_OUTCOME = "epostaanlikgonderimlog.do";
 	public static final String _EPOSTAANLIKGONDERIMLOG_EDIT_OUTCOME = "epostaanlikgonderimlog_Edit.do";

 	public static final String _SMSANLIKGONDERIMLOG_MODEL = "Smsanlikgonderimlog";
	public static final String _SMSANLIKGONDERIMLOG_CLASS =
 		"tr.com.arf.toys.db.model.iletisim.Smsanlikgonderimlog";
 	public static final String _SMSANLIKGONDERIMLOG_OUTCOME = "smsanlikgonderimlog.do";
 	public static final String _SMSANLIKGONDERIMLOG_EDIT_OUTCOME = "smsanlikgonderimlog_Edit.do";


	public static final String _ETKINLIK_MODEL = "Etkinlik";
	public static final String _ETKINLIK_CLASS =
 		"tr.com.arf.toys.db.model.etkinlik.Etkinlik";
 	public static final String _ETKINLIK_OUTCOME = "etkinlik.do";
 	public static final String _ETKINLIK_EDIT_OUTCOME = "etkinlik_Edit.do";

	public static final String _ETKINLIKTANIM_MODEL = "Etkinliktanim";
	public static final String _ETKINLIKTANIM_CLASS =
 		"tr.com.arf.toys.db.model.etkinlik.Etkinliktanim";
 	public static final String _ETKINLIKTANIM_OUTCOME = "etkinliktanim.do";
 	public static final String _ETKINLIKTANIM_EDIT_OUTCOME = "etkinliktanim_Edit.do";

 	public static final String _ETKINLIKKATILIMCI_MODEL = "Etkinlikkatilimci";
	public static final String _ETKINLIKKATILIMCI_CLASS =
 		"tr.com.arf.toys.db.model.etkinlik.Etkinlikkatilimci";
 	public static final String _ETKINLIKKATILIMCI_OUTCOME = "etkinlikkatilimci.do";
 	public static final String _ETKINLIKKATILIMCI_EDIT_OUTCOME = "etkinlikkatilimci_Edit.do";

 	public static final String _ETKINLIKKURUL_MODEL = "Etkinlikkurul";
	public static final String _ETKINLIKKURUL_CLASS =
 		"tr.com.arf.toys.db.model.etkinlik.Etkinlikkurul";
 	public static final String _ETKINLIKKURUL_OUTCOME = "etkinlikkurul.do";
 	public static final String _ETKINLIKKURUL_EDIT_OUTCOME = "etkinlikkurul_Edit.do";

	public static final String _ETKINLIKKURULUYE_MODEL = "Etkinlikkuruluye";
	public static final String _ETKINLIKKURULUYE_CLASS =
 		"tr.com.arf.toys.db.model.etkinlik.Etkinlikkuruluye";
 	public static final String _ETKINLIKKURULUYE_OUTCOME = "etkinlikkuruluye.do";
 	public static final String _ETKINLIKKURULUYE_EDIT_OUTCOME = "etkinlikkuruluye_Edit.do";

 	public static final String _ETKINLIKKURUM_MODEL = "EtkinlikKurum";
	public static final String _ETKINLIKKURUM_CLASS =
 		"tr.com.arf.toys.db.model.etkinlik.EtkinlikKurum";
 	public static final String _ETKINLIKKURUM_OUTCOME = "etkinlikKurum.do";
 	public static final String _ETKINLIKKURUM_EDIT_OUTCOME = "etkinlikKurum_Edit.do";

 	public static final String _ETKINLIKDOSYA_MODEL = "Etkinlikdosya";
	public static final String _ETKINLIKDOSYA_CLASS =
 		"tr.com.arf.toys.db.model.etkinlik.Etkinlikdosya";
 	public static final String _ETKINLIKDOSYA_OUTCOME = "etkinlikdosya.do";
 	public static final String _ETKINLIKDOSYA_EDIT_OUTCOME = "etkinlikdosya_Edit.do";


	public static final String _EGITIMTANIM_MODEL = "Egitimtanim";
	public static final String _EGITIMTANIM_CLASS =
 		"tr.com.arf.toys.db.model.egitim.Egitimtanim";
 	public static final String _EGITIMTANIM_OUTCOME = "egitimtanim.do";
 	public static final String _EGITIMTANIM_EDIT_OUTCOME = "egitimtanim_Edit.do";

	public static final String _EGITIM_MODEL = "Egitim";
	public static final String _EGITIM_CLASS =
 		"tr.com.arf.toys.db.model.egitim.Egitim";
 	public static final String _EGITIM_OUTCOME = "egitim.do";
 	public static final String _EGITIM_EDIT_OUTCOME = "egitim_Edit.do";

	public static final String _EGITIMKATILIMCI_MODEL = "Egitimkatilimci";
	public static final String _EGITIMKATILIMCI_CLASS =
 		"tr.com.arf.toys.db.model.egitim.Egitimkatilimci";
 	public static final String _EGITIMKATILIMCI_OUTCOME = "egitimkatilimci.do";
 	public static final String _EGITIMKATILIMCI_EDIT_OUTCOME = "egitimkatilimci_Edit.do";

	public static final String _EGITIMDERS_MODEL = "Egitimders";
	public static final String _EGITIMDERS_CLASS =
 		"tr.com.arf.toys.db.model.egitim.Egitimders";
 	public static final String _EGITIMDERS_OUTCOME = "egitimders.do";
 	public static final String _EGITIMDERS_EDIT_OUTCOME = "egitimders_Edit.do";

 	public static final String _EGITIMOGRETMEN_MODEL = "Egitimogretmen";
	public static final String _EGITIMOGRETMEN_CLASS =
 		"tr.com.arf.toys.db.model.egitim.Egitimogretmen";
 	public static final String _EGITIMOGRETMEN_OUTCOME = "egitimogretmen.do";
 	public static final String _EGITIMOGRETMEN_EDIT_OUTCOME = "egitimogretmen_Edit.do";

 	public static final String _EGITIMDOSYA_MODEL = "Egitimdosya";
	public static final String _EGITIMDOSYA_CLASS =
 		"tr.com.arf.toys.db.model.egitim.Egitimdosya";
 	public static final String _EGITIMDOSYA_OUTCOME = "egitimdosya.do";
 	public static final String _EGITIMDOSYA_EDIT_OUTCOME = "egitimdosya_Edit.do";

 	public static final String _EGITIMDEGERLENDIRME_MODEL = "Egitimdegerlendirme";
	public static final String _EGITIMDEGERLENDIRME_CLASS =
 		"tr.com.arf.toys.db.model.egitim.Egitimdegerlendirme";
 	public static final String _EGITIMDEGERLENDIRME_OUTCOME = "egitimdegerlendirme.do";
 	public static final String _EGITIMDEGERLENDIRME_EDIT_OUTCOME = "egitimdegerlendirme_Edit.do";

 	public static final String _KURULDONEMTOPLANTIKATILIMCI_MODEL = "KurulDonemToplantiKatilimci";
	public static final String _KURULDONEMTOPLANTIKATILIMCI_CLASS =
 		"tr.com.arf.toys.db.model.kisi.KurulDonemToplantiKatilimci";
 	public static final String _KURULDONEMTOPLANTIKATILIMCI_OUTCOME = "kurulDonemToplantiKatilimci.do";
 	public static final String _KURULDONEMTOPLANTIKATILIMCI_EDIT_OUTCOME = "kurulDonemToplantiKatilimci_Edit.do";

 	public static final String _KOMISYONDONEMTOPLANTIKATILIMCI_MODEL = "KomisyonDonemToplantiKatilimci";
	public static final String _KOMISYONDONEMTOPLANTIKATILIMCI_CLASS =
 		"tr.com.arf.toys.db.model.kisi.KomisyonDonemToplantiKatilimci";
 	public static final String _KOMISYONDONEMTOPLANTIKATILIMCI_OUTCOME = "komisyonDonemToplantiKatilimci.do";
 	public static final String _KOMISYONDONEMTOPLANTIKATILIMCI_EDIT_OUTCOME = "komisyonDonemToplantiKatilimci_Edit.do";

	public static final String _DOLASIMSABLON_MODEL = "Dolasimsablon";
	public static final String _DOLASIMSABLON_CLASS =
 		"tr.com.arf.toys.db.model.evrak.Dolasimsablon";
 	public static final String _DOLASIMSABLON_OUTCOME = "dolasimsablon.do";
 	public static final String _DOLASIMSABLON_EDIT_OUTCOME = "dolasimsablon_Edit.do";

	public static final String _DOLASIMSABLONDETAY_MODEL = "Dolasimsablondetay";
	public static final String _DOLASIMSABLONDETAY_CLASS =
 		"tr.com.arf.toys.db.model.evrak.Dolasimsablondetay";
 	public static final String _DOLASIMSABLONDETAY_OUTCOME = "dolasimsablondetay.do";
 	public static final String _DOLASIMSABLONDETAY_EDIT_OUTCOME = "dolasimsablondetay_Edit.do";

	public static final String _EVRAKDOLASIM_MODEL = "Evrakdolasim";
	public static final String _EVRAKDOLASIM_CLASS =
 		"tr.com.arf.toys.db.model.evrak.Evrakdolasim";
 	public static final String _EVRAKDOLASIM_OUTCOME = "evrakdolasim.do";
 	public static final String _EVRAKDOLASIM_EDIT_OUTCOME = "evrakdolasim_Edit.do";


	public static final String _EVRAKDOLASIMBILDIRIM_MODEL = "Evrakdolasimbildirim";
	public static final String _EVRAKDOLASIMBILDIRIM_CLASS =
 		"tr.com.arf.toys.db.model.evrak.Evrakdolasimbildirim";
 	public static final String _EVRAKDOLASIMBILDIRIM_OUTCOME = "evrakdolasimbildirim.do";
 	public static final String _EVRAKDOLASIMBILDIRIM_EDIT_OUTCOME = "evrakdolasimbildirim_Edit.do";


 	public static final String _UYESIPARIS_MODEL = "UyeSiparis";
	public static final String _UYESIPARIS_CLASS =
 		"tr.com.arf.toys.db.model.uye.UyeSiparis";
 	public static final String _UYESIPARIS_OUTCOME = "uyeSiparis.do";
 	public static final String _UYESIPARIS_EDIT_OUTCOME = "uyeSiparis_Edit.do";

 	public static final String _UYEHESAP_OUTCOME = "uyeHesap.do";
 	public static final String _UYEHESAP_PARAYUKLEME_OUTCOME = "uyeHesap_Edit.do";

	public static final String _RAPORLOG_MODEL = "Raporlog";
	public static final String _RAPORLOG_CLASS =
 		"tr.com.arf.toys.db.model.system.Raporlog";
 	public static final String _RAPORLOG_OUTCOME = "raporlog.do";
 	public static final String _RAPORLOG_EDIT_OUTCOME = "raporlog_Edit.do";


	public static final String _KULLANICIRAPOR_MODEL = "Kullanicirapor";
	public static final String _KULLANICIRAPOR_CLASS =
 		"tr.com.arf.toys.db.model.system.Kullanicirapor";
 	public static final String _KULLANICIRAPOR_OUTCOME = "kullanicirapor.do";
 	public static final String _KULLANICIRAPOR_EDIT_OUTCOME = "kullanicirapor_Edit.do";

 	public static final String _UNIVERSITEFAKULTE_MODEL = "UniversiteFakulte";
	public static final String _UNIVERSITEFAKULTE_CLASS =
 		"tr.com.arf.toys.db.model.system.UniversiteFakulte";
 	public static final String _UNIVERSITEFAKULTE_OUTCOME = "universiteFakulte.do";
 	public static final String _UNIVERSITEFAKULTE_EDIT_OUTCOME = "universiteFakulte_Edit.do";

 	public static final String _KULLANICISIFRETALEP_MODEL = "KullaniciSifreTalep";
	public static final String _KULLANICISIFRETALEP_CLASS =
 		"tr.com.arf.toys.db.model.system.KullaniciSifreTalep";
 	public static final String _KULLANICISIFRETALEP_OUTCOME = "kullaniciSifreTalep.do";
 	public static final String _KULLANICISIFRETALEP_OUTCOMEUYE = "kullaniciSifreTalepUye.do";
 	public static final String _KULLANICISIFRETALEP_EDIT_OUTCOME = "kullaniciSifreTalep_Edit.do";

 	public static final String _ISBANKASI_ODEME_OUTCOME = "isbankOdeme.do";
 	public static final String _GARANTI_ODEME_OUTCOME = "garantiOdeme.do";

 	public static final String _OGRENCIUYELISTESI_OUTCOME = "ogrenciUyeListesi.do";

 	public static final String _DEMIRBAS_MODEL = "Demirbas";
	public static final String _DEMIRBAS_CLASS =
 		"tr.com.arf.toys.db.model.system.Demirbas";
 	public static final String _DEMIRBAS_OUTCOME = "demirbas.do";
 	public static final String _DEMIRBAS_EDIT_OUTCOME = "demirbas_Edit.do";

 	public static final String _EGITIMLISANS_MODEL = "EgitimLisans";
	public static final String _EGITIMLISANS_CLASS =
 		"tr.com.arf.toys.db.model.egitim.EgitimLisans";
 	public static final String _EGITIMLISANS_OUTCOME = "egitimLisans.do";
 	public static final String _EGITIMLISANS_EDIT_OUTCOME = "egitimLisans_Edit.do";

 	public static final String _EGITMENEGITIMTANIM_MODEL = "EgitmenEgitimtanim";
	public static final String _EGITMENEGITIMTANIM_CLASS =
 		"tr.com.arf.toys.db.model.egitim.EgitmenEgitimtanim";
 	public static final String _EGITMENEGITIMTANIM_OUTCOME = "egitmenEgitimtanim.do";
 	public static final String _EGITMENEGITIMTANIM_EDIT_OUTCOME = "egitmenEgitimtanim_Edit.do";

 	public static final String _EGITIMGUNLERI_MODEL = "EgitimGunleri";
	public static final String _EGITIMGUNLERI_CLASS =
 		"tr.com.arf.toys.db.model.egitim.EgitimGunleri";
 	public static final String _EGITIMGUNLERI_OUTCOME = "egitimGunleri.do";
 	public static final String _EGITIMGUNLERI_EDIT_OUTCOME = "egitimGunleri_Edit.do";

 	public static final String _DEGERLENDIRICI_MODEL = "Degerlendirici";
	public static final String _DEGERLENDIRICI_CLASS =
 		"tr.com.arf.toys.db.model.egitim.Degerlendirici";
 	public static final String _DEGERLENDIRICI_OUTCOME = "degerlendirici.do";
 	public static final String _DEGERLENDIRICI_EDIT_OUTCOME = "degerlendirici_Edit.do";

 	public static final String _EGITIMDEGERLENDIRICI_MODEL = "EgitimDegerlendirici";
	public static final String _EGITIMDEGERLENDIRICI_CLASS =
 		"tr.com.arf.toys.db.model.egitim.EgitimDegerlendirici";
 	public static final String _EGITIMDEGERLENDIRICI_OUTCOME = "egitimDegerlendirici.do";
 	public static final String _EGITIMDEGERLENDIRICI_EDIT_OUTCOME = "egitimDegerlendirici_Edit.do";

 	public static final String _SINAV_MODEL = "Sinav";
	public static final String _SINAV_CLASS =
 		"tr.com.arf.toys.db.model.egitim.Sinav";
 	public static final String _SINAV_OUTCOME = "sinav.do";
 	public static final String _SINAV_EDIT_OUTCOME = "sinav_Edit.do";

 	public static final String _SINAVDEGERLENDIRICI_MODEL = "SinavDegerlendirici";
	public static final String _SINAVDEGERLENDIRICI_CLASS =
 		"tr.com.arf.toys.db.model.egitim.SinavDegerlendirici";
 	public static final String _SINAVDEGERLENDIRICI_OUTCOME = "sinavDegerlendirici.do";
 	public static final String _SINAVDEGERLENDIRICI_EDIT_OUTCOME = "sinavDegerlendirici_Edit.do";

 	public static final String _GOZETMEN_MODEL = "Gozetmen";
	public static final String _GOZETMEN_CLASS =
 		"tr.com.arf.toys.db.model.egitim.Gozetmen";
 	public static final String _GOZETMEN_OUTCOME = "gozetmen.do";
 	public static final String _GOZETMEN_EDIT_OUTCOME = "gozetmen_Edit.do";

	public static final String _SINAVGOZETMEN_MODEL = "SinavGozetmen";
	public static final String _SINAVGOZETMEN_CLASS =
 		"tr.com.arf.toys.db.model.egitim.SinavGozetmen";
 	public static final String _SINAVGOZETMEN_OUTCOME = "sinavGozetmen.do";
 	public static final String _SINAVGOZETMEN_EDIT_OUTCOME = "sinavGozetmen_Edit.do";

 	public static final String _SINAVKATILIMCI_MODEL = "SinavKatilimci";
	public static final String _SINAVKATILIMCI_CLASS =
 		"tr.com.arf.toys.db.model.egitim.SinavKatilimci";
 	public static final String _SINAVKATILIMCI_OUTCOME = "sinavKatilimci.do";
 	public static final String _SINAVKATILIMCI_EDIT_OUTCOME = "sinavKatilimci_Edit.do";

 	public static final String _EGITIMUCRETI_MODEL = "EgitimUcreti";
	public static final String _EGITIMUCRETI_CLASS =
 		"tr.com.arf.toys.db.model.egitim.EgitimUcreti";
 	public static final String _EGITIMUCRETI_OUTCOME = "egitimUcreti.do";
 	public static final String _EGITIMUCRETI_EDIT_OUTCOME = "egitimUcreti_Edit.do";


	public static final String _UYEFORM_MODEL = "Uyeform";
	public static final String _UYEFORM_CLASS =
 		"tr.com.arf.toys.db.model.uye.Uyeform";
 	public static final String _UYEFORM_OUTCOME = "uyeform.do";
 	public static final String _UYEFORM_EDIT_OUTCOME = "uyeform_Edit.do";

}

