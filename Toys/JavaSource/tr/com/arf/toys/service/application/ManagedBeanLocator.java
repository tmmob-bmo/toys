package tr.com.arf.toys.service.application;

import java.util.Map;

import javax.faces.context.FacesContext;

import tr.com.arf.framework.utility.application.ApplicationVariables;
import tr.com.arf.toys.view.controller._common.ApplicationController;
import tr.com.arf.toys.view.controller._common.EnumeratedController;
import tr.com.arf.toys.view.controller._common.SessionController;
import tr.com.arf.toys.view.controller._common.SessionUser;
import tr.com.arf.toys.view.controller.form.system.MenuController;

public class ManagedBeanLocator {

	public static SessionUser locateSessionUser() {
		if (FacesContext.getCurrentInstance().getExternalContext().getSessionMap() != null) {
			Map<String, Object> sessionMap = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
			return (SessionUser) sessionMap.get("sessionUser");
		} else {
			return null;
		}
	}

	public static MenuController locateMenuController() {
		if (FacesContext.getCurrentInstance().getExternalContext().getSessionMap() != null) {
			Map<String, Object> sessionMap = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
			return (MenuController) sessionMap.get("menuController");
		} else {
			return null;
		}
	}

	public static ApplicationController locateApplicationController() {
		if (FacesContext.getCurrentInstance().getExternalContext().getSessionMap() != null) {
			Map<String, Object> sessionMap = FacesContext.getCurrentInstance().getExternalContext().getApplicationMap();
			return (ApplicationController) sessionMap.get("applicationController");
		} else {
			return null;
		}
	}

	public static SessionController locateSessionController() {
		if (FacesContext.getCurrentInstance().getExternalContext().getSessionMap() != null) {
			Map<String, Object> sessionMap = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
			return (SessionController) sessionMap.get("sessionController");
		} else {
			return null;
		}
	}

	public static ApplicationVariables locateApplicationVariables() {
		if (FacesContext.getCurrentInstance().getExternalContext().getSessionMap() != null) {
			Map<String, Object> sessionMap = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
			return (ApplicationVariables) sessionMap.get("applicationVariables");
		} else {
			return null;
		}
	}
	
	public static EnumeratedController locateEnumeratedController() {
		if (FacesContext.getCurrentInstance().getExternalContext().getSessionMap() != null) {
			Map<String, Object> sessionMap = FacesContext.getCurrentInstance().getExternalContext().getApplicationMap();
			return (EnumeratedController) sessionMap.get("enumeratedController");
		} else {
			return null;
		}
	}

}