package tr.com.arf.toys.service.application;

import java.io.IOException;
import java.rmi.RemoteException;
import java.util.Map;

import javax.faces.context.FacesContext;

import org.apache.log4j.Logger;
import org.tempuri.Tmmob_x0020_KPS_x0020_ServiceV20SoapProxy;

import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import tr.com.arf.framework.view.controller._common.SessionUserBase;
import tr.com.arf.toys.utility.application.ApplicationConstant;

public class KPSService {
	protected static Logger logger = Logger.getLogger(KPSService.class);

	private static KPSService instance = new KPSService();

	private Tmmob_x0020_KPS_x0020_ServiceV20SoapProxy kpsServis = new Tmmob_x0020_KPS_x0020_ServiceV20SoapProxy();

	private String kpsKullaniciAdi;
	private String kpsSifre;

	private KPSService() {
		kpsKullaniciAdi = ManagedBeanLocator.locateSessionController().getKpsKullaniciAdi();
		kpsSifre = ManagedBeanLocator.locateSessionController().getKpsSifre();
	}

	public static KPSService getInstance() {
		return instance;
	}

	public String ilBilgisiGetir() throws RemoteException {
		String ilBilgisi = kpsServis.ilBilgisiGetir(kpsKullaniciAdi, kpsSifre);
		logYaz("kps il = " + ilBilgisi);
		return ilBilgisi;
	}

//	public String kisiAdresBilgisiGetir(long tcKimlikNo) throws RemoteException {
//		String kisiAdresBilgisi = kpsServis.kisiAdresBilgisiGetir(tcKimlikNo, kpsKullaniciAdi, kpsSifre);
//		logYaz("kps adres = " + kisiAdresBilgisi);
//		return kisiAdresBilgisi;
//	}
//
//	public String kisiKimlikBilgisiGetir(long tcKimlikNo) throws RemoteException {
//		String kisiKimlikBilgisi = kpsServis.TCKimlikNodanKisiBilgisiGetir(tcKimlikNo, kpsKullaniciAdi, kpsSifre);
//		logYaz("kps kisi = " + kisiKimlikBilgisi);
//		return kisiKimlikBilgisi;
//	}

	public String kisiAdresBilgisiGetirYeni(long tcKimlikNo) throws IOException {
		OkHttpClient client = new OkHttpClient();
		MediaType mediaType = MediaType.parse("text/xml");
		RequestBody body = RequestBody.create(mediaType,
				"<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">\r\n  <soap:Body>\r\n    <KisiAdresBilgisiGetir xmlns=\"http://tempuri.org/\">\r\n      <TCKimlikNo>"
						+ tcKimlikNo + "</TCKimlikNo>\r\n      <odaKodu>" + ApplicationConstant._kpsKullaniciAdi + "</odaKodu>\r\n  <sifre>" + ApplicationConstant._kpsSifre
						+ "</sifre>\r\n    </KisiAdresBilgisiGetir>\r\n  </soap:Body>\r\n</soap:Envelope>");
		Request request = new Request.Builder().url("https://kps.emo.org.tr/TmmobKpsService.asmx").post(body).addHeader("content-type", "text/xml").addHeader("cache-control", "no-cache")
				.addHeader("postman-token", "52950a9a-7a2a-acaa-2a94-4ad0bd69f20f").build();

		Response response = client.newCall(request).execute();
		if (response.isSuccessful()) {
			return response.body().string();
		} else {
			return null;
		}
	}

	public String kisiKimlikBilgisiGetirYeni(long tcKimlikNo) throws IOException {
		OkHttpClient client = new OkHttpClient();
		MediaType mediaType = MediaType.parse("text/xml");
		RequestBody body = RequestBody.create(mediaType,
				"<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">\r\n  <soap:Body>\r\n    <KimlikNodanKisiBilgisiGetir xmlns=\"http://tempuri.org/\">\r\n      <kimlikNo>"
						+ tcKimlikNo + "</kimlikNo>\r\n      <odaKodu>" + ApplicationConstant._kpsKullaniciAdi + "</odaKodu>\r\n<sifre>" + ApplicationConstant._kpsSifre
						+ "</sifre>\r\n    </KimlikNodanKisiBilgisiGetir>\r\n  </soap:Body>\r\n</soap:Envelope>");
		Request request = new Request.Builder().url("https://kps.emo.org.tr/TmmobKpsService.asmx").post(body).addHeader("content-type", "text/xml").addHeader("cache-control", "no-cache").build();

		Response response = client.newCall(request).execute();
		if (response.isSuccessful()) {
			return response.body().string();
		} else {
			return null;
		}
	}

	private SessionUserBase getSessionUser() {
		Map<String, Object> sessionMap = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
		return (SessionUserBase) sessionMap.get("sessionUser");
	}

	protected void logYaz(String logMessage) {
		System.out.println(logMessage);
		logger.error(logMessage);
		logger.error("Kullanıcı Adı : " + getSessionUser().getKullaniciText());
		logger.error("Kullanıcı RID : " + getSessionUser().getKullaniciRid());
		logger.error("Kullanıcı IP Adress : " + getSessionUser().getIpAddress());
	}

}
