package tr.com.arf.toys.service.timer;

import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import tr.com.arf.framework.db._management.DBOperator;
import tr.com.arf.framework.utility.tool.DateUtil;
import tr.com.arf.toys.db.enumerated.iletisim.AdresTuru;
import tr.com.arf.toys.db.model.iletisim.Adres;
import tr.com.arf.toys.db.model.system.SistemParametre;
import tr.com.arf.toys.utility.tool.SendEmail;

public class TimedJobService implements Job {
	protected static Logger logger = Logger.getLogger(TimedJobService.class);

	DBOperator dbOperator = new DBOperator();

	public DBOperator getDBOperator() {
		return dbOperator;
	}

	public void setDbOperator(DBOperator dbOperator) {
		this.dbOperator = dbOperator;
	}

	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		logger.debug("Job A is runing :" + DateUtil.dateToDMYHMS(new Date()));
		SendEmail sm = new SendEmail();
		if (getDBOperator().recordCount(SistemParametre.class.getSimpleName()) > 0) {
			SistemParametre param = (SistemParametre) getDBOperator().load(SistemParametre.class.getSimpleName()).get(0);
			String sistemmail = param.getSistemmail();
			logger.debug("system mail = " + sistemmail);
			if (sistemmail != null) {
				sm.sendEmailToRecipent(sistemmail, "TOYS", "Toys sorunsuz sekilde calismaktadir. " + DateUtil.dateToDMYHMS(new Date()));
			}
		}
		duplicateKPSAdresleriniTemizle();
	}

	@SuppressWarnings("unchecked")
	private void duplicateKPSAdresleriniTemizle() {
		String queryForDuplicateKPSAdres = "select kisiRef from oltp.Adres where adresturu = " + AdresTuru._KPSADRESI.getCode() + " group by kisiRef, adresturu HAVING count(*) > 1";
		List<Object> objectList = getDBOperator().loadByNativeQuery(queryForDuplicateKPSAdres);

		if (objectList != null && objectList.size() > 0) {
			for (Object o : objectList) {
				// Duplike KPS adresi olan uyelerin, son adresleri haric tumu siliniyor.
				List<Adres> kpsAdresleriList = getDBOperator().load(Adres.class.getSimpleName(), "o.kisiRef = " + o + " AND o.adresturu = " + AdresTuru._KPSADRESI.getCode(), "o.rID DESC");
				if (kpsAdresleriList != null && kpsAdresleriList.size() > 0) {
					int index = 0;
					for (Adres ad : kpsAdresleriList) {
						if (index > 0) {
							try {
								getDBOperator().delete(ad);
							} catch (Exception e) {
								System.out.println("ERROR @duplicateKPSAdresleriniTemizle: " + e.getMessage());
								e.printStackTrace();
							}
						}
						index++;
					}
				}
			}
		}
	}

}