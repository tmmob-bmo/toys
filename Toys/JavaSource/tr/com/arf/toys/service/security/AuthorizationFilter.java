package tr.com.arf.toys.service.security;

import java.io.IOException;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.log4j.MDC;
import tr.com.arf.toys.db.model.kisi.Kisi;
import tr.com.arf.toys.view.controller._common.SessionUser;

public class AuthorizationFilter extends HttpServlet implements Filter {
    private static final Logger LOGGER = Logger.getLogger(AuthorizationFilter.class);

	private static final long serialVersionUID = 1L;

	private ServletContext context;

	private String NOT_AUTHORIZED_URI = "/faces/_page/authentication/login.xhtml";
	private final Lock mutex = new ReentrantLock();

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		context = filterConfig.getServletContext();
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain) throws IOException, ServletException {
		HttpServletRequest httpRequest;
		HttpServletResponse httpResponse;
		String uri;
		String path;
		RequestDispatcher requestDispatcher;
		mutex.lock();
		try {
			httpRequest = (HttpServletRequest) request;
			httpResponse = (HttpServletResponse) response;
			uri = httpRequest.getRequestURI();
			path = context.getContextPath();
			if (uri.startsWith(path)) {
				uri = uri.substring(path.length());
			}
			// LOGGER.debug("Uri :" + uri);
			if(!uri.contains(".xhtml") && !uri.contains(".jsp")) {
				/* Eger bir xhtml ve jsp sayfasi cagrilmiyorsa, login sayfasina yonlendirilir.*/
				httpResponse.sendRedirect(httpRequest.getContextPath() + NOT_AUTHORIZED_URI);
				return;
			}
			if (uri.contains("warning") || uri.contains("login") || uri.contains("uyelogin") || uri.contains("userRole") || uri.contains("exception") || uri.contains("accessDenied") || uri.contains("kullaniciSifreTalep")) {
				requestDispatcher = context.getRequestDispatcher(uri);
			} else {
				if (httpRequest.getSession().getAttribute("xcdbxuamsh372nnrensdw24ew.ed3454dd.ssdxc.23dsqqm45") == null) {
					LOGGER.debug("Session null !!!!...");
					httpResponse.sendRedirect(httpRequest.getContextPath() + NOT_AUTHORIZED_URI);
					return;
				}
				try {
					SessionUser sessionUser = (SessionUser) httpRequest.getSession().getAttribute("xcdbxuamsh372nnrensdw24ew.ed3454dd.ssdxc.23dsqqm45");
					if (sessionUser == null) {
                        LOGGER.debug("sessionUser null !!!!...");
						httpResponse.sendRedirect(httpRequest.getContextPath() + NOT_AUTHORIZED_URI);
						return;
					} else {
						if (sessionUser.getKullanici().getUIString().trim().equalsIgnoreCase("")) {
                            LOGGER.debug("sessionUser kullanici NULL !!!!...");
							httpResponse.sendRedirect(httpRequest.getContextPath() + NOT_AUTHORIZED_URI);
							return;
						}
						if (!sessionUser.isKullaniciUyeMi()) {
							if (sessionUser.getSelectedRole() == null) {
                                LOGGER.debug("sessionUser getSelectedRole NULL !!!!...");
								httpResponse.sendRedirect(httpRequest.getContextPath() + NOT_AUTHORIZED_URI);
								return;
							}
						}
					}
				} catch (Exception exc) {
                    LOGGER.error(exc.toString(), exc);
					httpResponse.sendRedirect(httpRequest.getContextPath() + NOT_AUTHORIZED_URI);
					return;
				}
				requestDispatcher = context.getRequestDispatcher(uri);
			}
            log4jConfig(httpRequest);
			requestDispatcher.forward(request, response);
		} finally {
			mutex.unlock();
		}
	}

    private void log4jConfig(HttpServletRequest request) {
        HttpSession session = request.getSession();
        SessionUser sessionUser = (SessionUser) session.getAttribute("xcdbxuamsh372nnrensdw24ew.ed3454dd.ssdxc.23dsqqm45");
        if (sessionUser != null
                && sessionUser.getKullanici() != null
                && sessionUser.getKullanici().getKisiRef() != null) {
            Kisi kisi = sessionUser.getKullanici().getKisiRef();
            String ad = kisi.getAd();
            String soyad = kisi.getSoyad();
            Long kimlikno = kisi.getKimlikno();
            MDC.put("kullanici", kimlikno + " | " + ad + " " + soyad + " ");
        } else {
            MDC.put("kullanici", "---");
        }
        String remoteAddr = request.getRemoteAddr();
        MDC.put("ip", remoteAddr);
    }

}