package tr.com.arf.toys.service.export;

import java.io.IOException;

import tr.com.arf.toys.service.application.ManagedBeanLocator;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.FontProvider;
import com.itextpdf.text.pdf.BaseFont;

public class Fonts {
	public static final String BOSLUK = "\u00a0";
	
	static ClassLoader loader = Thread.currentThread().getContextClassLoader();
	static String timesTTF =  "/tr/com/arf/toys/service/export/ttfs/TIMES.TTF";
	static String corsvaTTF = "/tr/com/arf/toys/service/export/ttfs/MTCORSVA.TTF";;
	static String arialTTF =  "/tr/com/arf/toys/service/export/ttfs/ARIALUNI_TR.TTF";
	 
	public static class MyFontFactory implements FontProvider {
		@Override
		public Font getFont(String fontname, String encoding, boolean embedded,
				float size, int style, BaseColor color) {
			// return Fonts.NORMAL;
			BaseFont bf;
			try { 
				bf = BaseFont.createFont(ManagedBeanLocator.locateSessionUser().getFontPath(), "Identity-H", BaseFont.EMBEDDED);  
			} catch (DocumentException e) {
				e.printStackTrace();
				return new Font();
			} catch (IOException e) {
				e.printStackTrace();
				return new Font();
			} 
			Font ff = new Font(bf, size);
			ff.setColor(color);
			ff.setStyle(style);
			return ff;
		} 
		
        @Override
		public boolean isRegistered(String fontname) {
            return false;
        }
    }
	
	public static BaseFont baseFont = null;
	static {
		try {
			//TODO Gerçek projeye taşındığında fontlar webcontent altında ilgili yere taşınarak path düzenlenmeli
			baseFont = BaseFont.createFont(ManagedBeanLocator.locateSessionUser().getFontPath(), "Identity-H", BaseFont.EMBEDDED);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static BaseFont baseFontCorsva = null;
	static {
		try {
			//TODO Gerçek projeye taşındığında fontlar webcontent altında ilgili yere taşınarak path düzenlenmeli
			baseFontCorsva = BaseFont.createFont(corsvaTTF, BaseFont.IDENTITY_H, true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	public static Font BUYUK_BASLIK;	
		static {
		BUYUK_BASLIK = new MyFontFactory().getFont(arialTTF, BaseFont.CP1252, false, 12, Font.BOLD, BaseColor.BLACK);  
	}
	
	public static Font BASLIK;	
		static {
			BASLIK = new MyFontFactory().getFont(arialTTF, BaseFont.CP1252, false, 11, Font.BOLD, BaseColor.BLACK);  
	}
		
	public static Font BASLIK_12PX;	
	static {
		BASLIK_12PX = new MyFontFactory().getFont(arialTTF, BaseFont.CP1252, false, 12, Font.BOLD, BaseColor.BLACK);  
	}
	
	public static Font BASLIK_10PX;	
		static {
			BASLIK_10PX = new MyFontFactory().getFont(arialTTF, BaseFont.CP1252, false, 10, Font.BOLD, BaseColor.BLACK);  
	}
	
	public static Font BASLIK_CORSVA;	
		static {
			BASLIK_CORSVA = new Font(baseFontCorsva);
			BASLIK_CORSVA.setStyle(Font.BOLD ); 
			BASLIK_CORSVA.setSize(20);
			BASLIK_CORSVA.setColor(BaseColor.BLACK);
	}
	
	public static Font BASLIK_ALT_CIZGI;		
		static {
			BASLIK_ALT_CIZGI = new Font(baseFont);
			BASLIK_ALT_CIZGI.setSize(12); 
			BASLIK_ALT_CIZGI.setStyle("weight:bold; decoration:underline"); 
			BASLIK_ALT_CIZGI.setColor(BaseColor.BLACK);
	}	

	 
	public static Font NORMAL;	
		static {
			NORMAL = new MyFontFactory().getFont(arialTTF, BaseFont.CP1252, false, 11, Font.NORMAL, BaseColor.BLACK); 
	}
	
	public static Font NORMAL_12PX;		
		static {
			NORMAL_12PX = new MyFontFactory().getFont(arialTTF, BaseFont.CP1252, false, 12, Font.NORMAL, BaseColor.BLACK); 
	}
		
	public static Font NORMAL_ALT_CIZGI;	
		static {
			NORMAL_ALT_CIZGI = new MyFontFactory().getFont(arialTTF, BaseFont.CP1252, false, 12, Font.UNDERLINE, BaseColor.BLACK);  
	} 
	
	public static Font DIP_NOT;	
		static {
			DIP_NOT = new MyFontFactory().getFont(arialTTF, BaseFont.CP1252, false, 9, Font.NORMAL, BaseColor.BLACK);  
	}	
	
	public static Font DIP_NOT_ALTCIZGI;		
		static {
			DIP_NOT_ALTCIZGI =new MyFontFactory().getFont(arialTTF, BaseFont.CP1252, false, 9, Font.UNDERLINE, BaseColor.BLACK);
	}	
	
	public static Font DIP_NOT_BOLD;		
		static {
			DIP_NOT_BOLD = new MyFontFactory().getFont(arialTTF, BaseFont.CP1252, false, 9, Font.BOLD, BaseColor.BLACK);  
	}	
	 	
	public static Font SEMBOL;		
		static {
			SEMBOL = new Font(baseFont);
			SEMBOL.setSize(10);
			SEMBOL.setFamily(FontFactory.SYMBOL);
			SEMBOL.setColor(BaseColor.BLACK);
	}	 	
	
}
