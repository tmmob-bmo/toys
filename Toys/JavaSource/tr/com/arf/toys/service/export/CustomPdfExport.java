package tr.com.arf.toys.service.export;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.StringReader;
import java.math.BigDecimal;
import java.net.MalformedURLException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontProvider;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.html.simpleparser.HTMLWorker;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import tr.com.arf.framework.db._management.DBOperator;
import tr.com.arf.framework.utility.tool.BigDecimalUtil;
import tr.com.arf.framework.utility.tool.DateUtil;
import tr.com.arf.framework.utility.tool.KeyUtil;
import tr.com.arf.framework.utility.tool.StringUtil;
import tr.com.arf.toys.db.enumerated.evrak.EvrakDurum;
import tr.com.arf.toys.db.enumerated.evrak.EvrakHareketTuru;
import tr.com.arf.toys.db.enumerated.evrak.HazirEvrakTuru;
import tr.com.arf.toys.db.enumerated.finans.OdemeTuru;
import tr.com.arf.toys.db.enumerated.iletisim.AdresTuru;
import tr.com.arf.toys.db.enumerated.iletisim.NetAdresTuru;
import tr.com.arf.toys.db.enumerated.iletisim.TelefonTuru;
import tr.com.arf.toys.db.model.evrak.Evrak;
import tr.com.arf.toys.db.model.evrak.Evrakek;
import tr.com.arf.toys.db.model.finans.Odeme;
import tr.com.arf.toys.db.model.iletisim.Adres;
import tr.com.arf.toys.db.model.iletisim.Internetadres;
import tr.com.arf.toys.db.model.iletisim.Telefon;
import tr.com.arf.toys.db.model.kisi.Kisikimlik;
import tr.com.arf.toys.db.model.kisi.Kisiogrenim;
import tr.com.arf.toys.db.model.system.Kullanici;
import tr.com.arf.toys.db.model.system.KurumKisi;
import tr.com.arf.toys.db.model.system.Personel;
import tr.com.arf.toys.db.model.uye.Uye;
import tr.com.arf.toys.service.application.ApplicationDescriptor;
import tr.com.arf.toys.service.application.ManagedBeanLocator;

@SuppressWarnings("deprecation")
public class CustomPdfExport {

	// private static final char[] ilgiIlkHarfleri = new char[]
	// {'a','b','c','d','e','f','g','h','i','j','k','l','m','n'};

	private static final float leftMargin = 60f;
	private static final float rightMargin = 60f;
	private static final float topMargin = 140f;
	private static final float bottomMargin = 20f;

	private static DBOperator getDBOperator() {
		return ManagedBeanLocator.locateSessionController().getDBOperator();
	}

	@SuppressWarnings("unchecked")
	public static byte[] gidenEvrak(String filename, Long evrakId, Kullanici systemUser) throws DocumentException, Exception {
		Document document = new Document();
		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		PdfWriter writer = PdfWriter.getInstance(document, stream);
		document.open();
		@SuppressWarnings("unused")
		PdfContentByte cb = writer.getDirectContent();
		Evrak dokuman = (Evrak) getDBOperator().load(Evrak.class.getSimpleName(), "o.rID=" + evrakId, "o.rID").get(0);
		String documentWhereCon = "o.rID=" + dokuman.getRID();

		Rectangle rec = PageSize.A4; // Width :595.0 --- Height :842.0
		// float pageWidth = rec.getWidth();
		@SuppressWarnings("unused")
		float pageHeight = rec.getHeight();

		// rec = rec.rotate(); // LANDSCAPE
		document.setMargins(leftMargin, rightMargin, topMargin, bottomMargin); // left,right, top, bottom
		document.open();
		Paragraph pa = new Paragraph();
		pa.setAlignment(Element.ALIGN_CENTER);
		try {
			HashMap<String, Object> providers = new HashMap<String, Object>();
			// providers.put(HTMLWorker.FONT_PROVIDER, new MyFontFactory());

			/* LOGO YERLESTIRILIYOR */
			@SuppressWarnings("unused")
			HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
			// SistemParametre parametre = (SistemParametre)
			// getDBOperator().load(SistemParametre.class.getSimpleName()).get(0);
			// String URL_IMAGES = parametre.getLogodegeri();
			PdfPTable table = new PdfPTable(2);
			table.setWidthPercentage(100f);
			table.setWidths(new float[] { 15, 85 });
			table.getDefaultCell().setBorder(Rectangle.NO_BORDER);
			PdfPCell cell = new PdfPCell();
			cell.setBorder(0);
			cell.setHorizontalAlignment(Element.ALIGN_LEFT);
			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			try {
				// Image logo1 = Image.getInstance(URL_IMAGES);
				// logo1.setAlignment(Element.ALIGN_LEFT);
				// logo1.scaleAbsolute(50f, 50f);
				// cell.addElement(logo1);
				table.addCell(cell);
			} catch (Exception e) {
				// System.out.println("Logo bulunamadi :" + URL_IMAGES);
			}

			/* LOGO YERLESTIRILDI */

			/* BASLIK YAZILIYOR */
			pa.setAlignment(Element.ALIGN_LEFT);
			// pa.add(new
			// Phrase(dokuman.getSistemeTanimlayan().getBirimRef().getAd() +
			// "\n", Fonts.BUYUK_BASLIK));
			// pa.add(new
			// Phrase(dokuman.getSistemeTanimlayan().getBirimRef().getEvrakPdfText(),
			// Fonts.DIP_NOT));
			cell = new PdfPCell(pa);
			cell.setLeading(4f, 1f);// Satir araliklari genisletiliyor
			cell.setBorder(0);
			cell.setVerticalAlignment(Element.ALIGN_CENTER);
			cell.setHorizontalAlignment(Element.ALIGN_LEFT);
			table.addCell(cell);
			table.setSpacingAfter(5f); // Tablodan sonra 15 pk bosluk
			document.add(table); // TABLO EKLENDI
			/* BASLIK YAZILDI */

			/* ODA ADRES VE TELEFON BILGILERI YAZILIYOR */
			table = new PdfPTable(1);
			table.setWidthPercentage(100f);
			table.setWidths(new float[] { 100 });
			table.getDefaultCell().setBorder(Rectangle.NO_BORDER);
			pa = new Paragraph();
			pa.setAlignment(Element.ALIGN_LEFT);

			pa.setLeading(1.0f, 1.0f); // Satir aralikları azaltiliyor
			cell = new PdfPCell(new Phrase(pa));
			cell.setBorder(0); // Bottom border set edildi
			cell.setHorizontalAlignment(Element.ALIGN_CENTER);
			cell.addElement(pa);
			table.addCell(cell);
			table.setSpacingAfter(10f); // Tablodan sonra 10 pk bosluk
			document.add(table); // TABLO EKLENDI
			/* ODA ADRES VE TELEFON BILGILERI YAZILDI */

			/* TARIH VE SAYI BILGILERI YAZILIYOR */
			table = new PdfPTable(2);
			table.setWidthPercentage(100f);
			table.setWidths(new float[] { 70, 30 });
			table.getDefaultCell().setBorder(Rectangle.NO_BORDER);
			pa = new Paragraph();
			pa.setAlignment(Element.ALIGN_LEFT);
			pa.setLeading(4.0f, 1.0f);
			pa.add(new Phrase(dokuman.getGittigiText() + "\n", Fonts.BASLIK));
			pa.add(new Phrase("Sayı :", Fonts.BASLIK));
			pa.add(new Phrase(dokuman.getKayitno(), Fonts.NORMAL));
			if (dokuman.getIlgi() != null && dokuman.getIlgi().trim().length() > 0) {
				List<Element> ilgiObjects = HTMLWorker.parseToList(new StringReader("<b>İlgi</b> : " + dokuman.getIlgi()), null, providers);
				for (Element element : ilgiObjects) {
					pa.setAlignment(Element.ALIGN_LEFT);
					pa.setFont(Fonts.NORMAL);
					pa.add(element);
				}
			} else {
				pa.add(new Phrase("\n"));
			}
			pa.add(new Phrase("Konu :", Fonts.BASLIK));
			pa.add(new Phrase(dokuman.getKonusu(), Fonts.NORMAL));

			cell = new PdfPCell(new Phrase(pa));
			cell.setBorder(0);
			cell.setHorizontalAlignment(Element.ALIGN_CENTER);
			cell.addElement(pa);
			table.addCell(cell);

			/* TARIH VE SAYI BILGILERI YAZILIYOR */
			pa = new Paragraph();
			pa.setAlignment(Element.ALIGN_LEFT);
			pa.add(new Phrase(DateUtil.dateToDMY(dokuman.getKayittarihi()) + "\n", Fonts.NORMAL));
			cell = new PdfPCell(new Phrase(pa));
			cell.setHorizontalAlignment(Element.ALIGN_LEFT);
			cell.setBorder(0);
			cell.addElement(pa);
			table.addCell(cell);

			document.add(table); // TABLO EKLENDI
			/* TARIH VE SAYI BILGILERI YAZILDI */

			/* METIN - ICERIK YAZILIYOR */
			pa = new Paragraph();
			pa.setAlignment(Element.ALIGN_JUSTIFIED);
			if (dokuman.getIcerik() == null) {
				dokuman.setIcerik("...");
			}
			dokuman.setIcerik(dokuman.getIcerik().replace("İ", "$I$"));
			dokuman.setIcerik(dokuman.getIcerik().replace("/p>", "/p>$NEWLINE$"));
			List<Element> objects = HTMLWorker.parseToList(new StringReader(dokuman.getIcerik()), null, providers);
			for (Element element : objects) {
				pa.setAlignment(Element.ALIGN_LEFT);
				pa.setFont(Fonts.NORMAL);
				pa.add(element);
			}
			Paragraph para = new Paragraph();
			para.setAlignment(Element.ALIGN_JUSTIFIED);
			for (Chunk ch : pa.getChunks()) {
				String text = ch.getContent().replace("$I$", "İ");
				Chunk ch2 = new Chunk(text.replace("$NEWLINE$", "\n"));
				ch2.setFont(new MyFontFactory().getFont(ch.getFont().getFamilyname(), BaseFont.CP1252, false, ch.getFont().getSize(), ch.getFont().getStyle(), ch.getFont().getColor()));
				ch2.getFont().setStyle(ch.getFont().getStyle());
				ch2.setLineHeight(20f);
				para.add(ch2);
			}

			document.add(para);
			/* METIN - ICERIK YAZILDI */

			if (dokuman.getHazirEvrakTuru() == null || dokuman.getHazirEvrakTuru() == HazirEvrakTuru._GENELEVRAK) {
				/* BIRIM VE HAZIRLAYAN BILGILERI YAZILIYOR */
				pa = new Paragraph();
				table = new PdfPTable(1);
				table.setSpacingBefore(15f);
				table.setWidthPercentage(100f);
				table.getDefaultCell().setBorder(Rectangle.NO_BORDER);
				pa.setIndentationLeft(320f);
				pa.setAlignment(Element.ALIGN_CENTER);

				String imzaMetni = "";
				if (dokuman.getSistemeTanimlayan() != null) {
					imzaMetni = dokuman.getSistemeTanimlayan().getBirimRef().getUIString() + "\n";
				}
				if (dokuman.getHazirlayanPersonelRef() != null) {
					imzaMetni += dokuman.getHazirlayanPersonelRef().getUIString();
				}

				pa.add(new Phrase(imzaMetni, Fonts.NORMAL));
				cell = new PdfPCell(new Phrase(pa));
				cell.setBorder(0);
				cell.setLeading(2f, 1f);
				cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
				cell.addElement(pa);
				table.setSpacingAfter(10f);
				table.addCell(cell);
				document.add(table);
				/* BIRIM VE HAZIRLAYAN BILGILERI YAZILDI */
			}

			/* EKLERI VARSA YAZILIYOR */
			List<Evrakek> attachmentList = getDBOperator().load(ApplicationDescriptor._EVRAKEK_MODEL, documentWhereCon, "o.rID");
			if (attachmentList != null) {
				if (attachmentList.size() > 0) {
					table = new PdfPTable(1);
					table.setWidthPercentage(100f);
					table.getDefaultCell().setBorder(Rectangle.NO_BORDER);
					pa = new Paragraph();
					pa.setAlignment(Element.ALIGN_LEFT);
					pa.add(new Phrase("EK :", Fonts.BASLIK_ALT_CIZGI));
					cell = new PdfPCell(new Phrase(pa));
					cell.setBorder(0);
					cell.setLeading(2f, 1f);
					cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
					cell.addElement(pa);
					table.setSpacingAfter(30f);
					table.addCell(cell);

					pa = new Paragraph();
					pa.setAlignment(Element.ALIGN_LEFT);
					for (int x = 0; x < attachmentList.size(); x++) {
						if (x < attachmentList.size() - 1) {
							pa.add(new Phrase(attachmentList.get(x).getTanim() + ", \n", Fonts.NORMAL));
						} else {
							pa.add(new Phrase(attachmentList.get(x).getTanim() + ". ", Fonts.NORMAL));
						}
					}
					cell = new PdfPCell(new Phrase(pa));
					cell.setBorder(0);
					cell.setLeading(2f, 1f);
					cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
					cell.addElement(pa);
					table.setSpacingAfter(30f);
					table.addCell(cell);
					document.add(table);
				}
			}
			/* EKLERI YAZILDI */

			/*
			 * // PARAFLAR String paraf = "Paraf";
			 * 
			 * pa = new Paragraph(); pa.setAlignment(Element.ALIGN_LEFT); pa.add(new Phrase(paraf + " \n", Fonts.NORMAL)); pa.setSpacingAfter(16f); document.add(pa);
			 */
			// FOOTER
			// draw a line above text...
			// drawLine(cb, 1.0f, pageWidth, 60f, 44f, pageWidth - 60, 44f);
			// String text = "TMMOB " + parametre.getOdaadi().toUpperCase() +
			// ", Anayasa'nın 135. maddesinde tanımlanan 66 ve 85 sayılı KHK ve 7303 sayılı yasa ile"
			// +
			// "değişik 6235 sayılı \nyasaya göre kurulmuş kamu kurumu niteliğinde bir meslek kuruluşudur";
			// writeToAbsolutePosition(cb, text, 60f, 0f, 590f, 40f, 11,
			// Element.ALIGN_LEFT, Fonts.DIP_NOT);
			//
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		document.close();
		return stream.toByteArray();
	}

	@SuppressWarnings("unused")
	public static byte[] uyeForm(String filename, Long uyeRID, Kullanici systemUser) throws DocumentException, Exception {
		Document document = new Document();
		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		PdfWriter writer = PdfWriter.getInstance(document, stream);
		document.open();
		PdfContentByte cb = writer.getDirectContent();
		Uye uye = (Uye) getDBOperator().load(Uye.class.getSimpleName(), "o.rID=" + uyeRID, "o.rID").get(0);
		Personel personel = (Personel) getDBOperator().load(Personel.class.getSimpleName(), "o.kisiRef.rID=" + systemUser.getKisiRef().getRID(), "o.durum ASC").get(0);
		Rectangle rec = PageSize.A4; // Width :595.0 --- Height :842.0
		document.setMargins(leftMargin - 20, rightMargin - 20, topMargin - 30, bottomMargin); // left,
																								// right,
																								// top,
																								// bottom
		document.open();
		Paragraph pa = new Paragraph();
		pa.setAlignment(Element.ALIGN_CENTER);
		try {
			/* LOGO YERLESTIRILIYOR */
			HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
			PdfPTable table = new PdfPTable(2);
			table.setWidthPercentage(100f);
			table.setWidths(new float[] { 15, 85 });
			table.getDefaultCell().setBorder(Rectangle.NO_BORDER);
			PdfPCell cell = new PdfPCell();
			/* LOGO YERLESTIRILDI */

			/* BASLIK YAZILIYOR */
			pa.setAlignment(Element.ALIGN_LEFT);
			cell = new PdfPCell(pa);
			cell.setLeading(4f, 1f);// Satir araliklari genisletiliyor
			cell.setBorder(0);
			cell.setVerticalAlignment(Element.ALIGN_CENTER);
			cell.setHorizontalAlignment(Element.ALIGN_LEFT);
			table.addCell(cell);
			table.setSpacingAfter(5f); // Tablodan sonra 15 pk bosluk
			document.add(table); // TABLO EKLENDI
			/* BASLIK YAZILDI */

			/* ODA ADRES VE TELEFON BILGILERI YAZILIYOR */
			table = new PdfPTable(1);
			table.setWidthPercentage(100f);
			table.setWidths(new float[] { 100 });
			table.getDefaultCell().setBorder(Rectangle.NO_BORDER);
			pa = new Paragraph();
			pa.setAlignment(Element.ALIGN_LEFT);

			pa.setLeading(1.0f, 1.0f); // Satir aralikları azaltiliyor
			cell = new PdfPCell(new Phrase(pa));
			cell.setBorder(0); // Bottom border set edildi
			cell.setHorizontalAlignment(Element.ALIGN_CENTER);
			cell.addElement(pa);
			table.addCell(cell);
			table.setSpacingAfter(30f); // Tablodan sonra .. pk bosluk
			document.add(table); // TABLO EKLENDI
			/* ODA ADRES VE TELEFON BILGILERI YAZILDI */

			// form BASLIGI
			table = new PdfPTable(1);
			table.setWidthPercentage(100f);
			table.setWidths(new float[] { 100 });
			table.getDefaultCell().setBorder(Rectangle.NO_BORDER);
			pa = new Paragraph();
			pa.setAlignment(Element.ALIGN_CENTER);
			pa = new Paragraph();
			pa.setAlignment(Element.ALIGN_CENTER);
			pa.add(new Phrase("ÜYE KAYIT FORMU", Fonts.BUYUK_BASLIK));
			cell = new PdfPCell(new Phrase(pa));
			cell.setBorder(0);
			cell.setMinimumHeight(35f);
			cell.setHorizontalAlignment(Element.ALIGN_CENTER);
			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell.addElement(pa);
			table.addCell(cell);
			table.setSpacingAfter(20f); // Tablodan sonra 10 pk bosluk
			document.add(table); // TABLO EKLENDI

			/* FORM ICERIGI YAZILIYOR */
			table = new PdfPTable(2);
			table.setWidthPercentage(100f);
			table.setWidths(new float[] { 35, 65 });
			table.getDefaultCell().setBorder(Rectangle.NO_BORDER);

			table = new PdfPTable(2);
			table.setWidthPercentage(100f);
			table.setWidths(new float[] { 35, 65 });
			table.getDefaultCell().setBorder(Rectangle.NO_BORDER);

			// TC KIMLIK NO
			pa = new Paragraph();
			pa.setAlignment(Element.ALIGN_LEFT);
			pa.add(new Phrase("TC Kimlik No :", Fonts.BASLIK));
			cell = new PdfPCell(new Phrase(pa));
			cell.setBorder(2);
			cell.setMinimumHeight(35f);
			cell.setHorizontalAlignment(Element.ALIGN_LEFT);
			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell.addElement(pa);
			table.addCell(cell);

			pa = new Paragraph();
			pa.setAlignment(Element.ALIGN_LEFT);
			pa.add(new Phrase(" : " + uye.getKisiRef().getKimlikno() + "", Fonts.NORMAL));
			cell = new PdfPCell(new Phrase(pa));
			cell.setBorder(2);
			cell.setMinimumHeight(35f);
			cell.setHorizontalAlignment(Element.ALIGN_LEFT);
			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell.addElement(pa);
			table.addCell(cell);

			// AD - SOYAD
			pa = new Paragraph();
			pa.setAlignment(Element.ALIGN_LEFT);
			pa.add(new Phrase("Adı Soyadı", Fonts.BASLIK));
			cell = new PdfPCell(new Phrase(pa));
			cell.setBorder(2);
			cell.setMinimumHeight(35f);
			cell.setHorizontalAlignment(Element.ALIGN_LEFT);
			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell.addElement(pa);
			table.addCell(cell);

			pa = new Paragraph();
			pa.setAlignment(Element.ALIGN_LEFT);
			pa.add(new Phrase(" : " + uye.getKisiRef().getUIStringShort().toUpperCase(), Fonts.NORMAL));
			cell = new PdfPCell(new Phrase(pa));
			cell.setBorder(2);
			cell.setMinimumHeight(35f);
			cell.setHorizontalAlignment(Element.ALIGN_LEFT);
			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			table.addCell(cell);

			// SICIL NO - SUBE
			pa = new Paragraph();
			pa.setAlignment(Element.ALIGN_LEFT);
			pa.add(new Phrase("Oda Sicil No, Birimi", Fonts.BASLIK));
			cell = new PdfPCell(new Phrase(pa));
			cell.setBorder(2);
			cell.setMinimumHeight(35f);
			cell.setHorizontalAlignment(Element.ALIGN_LEFT);
			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell.addElement(pa);
			table.addCell(cell);

			pa = new Paragraph();
			pa.setAlignment(Element.ALIGN_LEFT);
			pa.add(new Phrase(" : " + uye.getSicilno().toUpperCase() + ", " + personel.getBirimRef().getAd().toUpperCase(), Fonts.NORMAL));
			cell = new PdfPCell(new Phrase(pa));
			cell.setBorder(2);
			cell.setMinimumHeight(35f);
			cell.setHorizontalAlignment(Element.ALIGN_LEFT);
			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell.addElement(pa);
			table.addCell(cell);

			// DOGUM YERI - TARIHI
			pa = new Paragraph();
			pa.setAlignment(Element.ALIGN_LEFT);
			pa.add(new Phrase("Doğum Yeri - Doğum Tarihi", Fonts.BASLIK));
			cell = new PdfPCell(new Phrase(pa));
			cell.setBorder(2);
			cell.setMinimumHeight(35f);
			cell.setHorizontalAlignment(Element.ALIGN_LEFT);
			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell.addElement(pa);
			table.addCell(cell);

			pa = new Paragraph();
			pa.setAlignment(Element.ALIGN_LEFT);
			if (getDBOperator().recordCount(Kisikimlik.class.getSimpleName(), "o.kisiRef.rID=" + uye.getKisiRef().getRID()) > 0) {
				Kisikimlik kimlik = (Kisikimlik) getDBOperator().load(Kisikimlik.class.getSimpleName(), "o.kisiRef.rID=" + uye.getKisiRef().getRID(), "o.rID").get(0);
				pa.add(new Phrase(" : " + kimlik.getDogumyer() + " - " + DateUtil.dateToDMY(kimlik.getDogumtarih()), Fonts.NORMAL));
			} else {
				pa.add(new Phrase(" : " + ".... - ....", Fonts.NORMAL));
			}
			cell = new PdfPCell(new Phrase(pa));
			cell.setBorder(2);
			cell.setMinimumHeight(35f);
			cell.setHorizontalAlignment(Element.ALIGN_LEFT);
			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell.addElement(pa);
			table.addCell(cell);

			// MEZUN OLDUGU OKUL - YILI
			pa = new Paragraph();
			pa.setAlignment(Element.ALIGN_LEFT);
			pa.add(new Phrase("Mezun Olduğu Okul - Mezuniyet Yılı", Fonts.BASLIK));
			cell = new PdfPCell(new Phrase(pa));
			cell.setBorder(2);
			cell.setMinimumHeight(35f);
			cell.setHorizontalAlignment(Element.ALIGN_LEFT);
			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell.addElement(pa);
			table.addCell(cell);

			pa = new Paragraph();
			pa.setAlignment(Element.ALIGN_LEFT);
			if (getDBOperator().recordCount(Kisiogrenim.class.getSimpleName(), "o.kisiRef.rID=" + uye.getKisiRef().getRID() + " AND o.universiteRef != NULL") > 0) {
				Kisiogrenim ogrenim = (Kisiogrenim) getDBOperator().load(Kisiogrenim.class.getSimpleName(), "o.kisiRef.rID=" + uye.getKisiRef().getRID() + " AND o.universiteRef != NULL", "o.rID").get(0);
				pa.add(new Phrase(" : " + ogrenim.getUniversiteRef().getAd() + " - " + ogrenim.getMezuniyettarihi(), Fonts.NORMAL));
			} else {
				pa.add(new Phrase(" : " + ".... - ....", Fonts.NORMAL));
			}
			cell = new PdfPCell(new Phrase(pa));
			cell.setBorder(2);
			cell.setMinimumHeight(35f);
			cell.setHorizontalAlignment(Element.ALIGN_LEFT);
			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell.addElement(pa);
			table.addCell(cell);

			// CALISTIGI KURUM
			pa = new Paragraph();
			pa.setAlignment(Element.ALIGN_LEFT);
			pa.add(new Phrase("Çalıştığı Kurum", Fonts.BASLIK));
			cell = new PdfPCell(new Phrase(pa));
			cell.setBorder(2);
			cell.setMinimumHeight(35f);
			cell.setHorizontalAlignment(Element.ALIGN_LEFT);
			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell.addElement(pa);
			table.addCell(cell);

			pa = new Paragraph();
			pa.setAlignment(Element.ALIGN_LEFT);
			if (getDBOperator().recordCount(KurumKisi.class.getSimpleName(), "o.kisiRef.rID=" + uye.getKisiRef().getRID()) > 0) {
				KurumKisi kurum = (KurumKisi) getDBOperator().load(KurumKisi.class.getSimpleName(), "o.kisiRef.rID=" + uye.getKisiRef().getRID(), "o.varsayilan ASC").get(0);
				pa.add(new Phrase(" : " + kurum.getKurumRef().getAd(), Fonts.NORMAL));
			} else {
				pa.add(new Phrase(" : " + "....", Fonts.NORMAL));
			}
			cell = new PdfPCell(new Phrase(pa));
			cell.setBorder(2);
			cell.setMinimumHeight(35f);
			cell.setHorizontalAlignment(Element.ALIGN_LEFT);
			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell.addElement(pa);
			table.addCell(cell);

			// IS ADRESI
			pa = new Paragraph();
			pa.setAlignment(Element.ALIGN_LEFT);
			pa.add(new Phrase("İş Adresi", Fonts.BASLIK));
			cell = new PdfPCell(new Phrase(pa));
			cell.setBorder(2);
			cell.setMinimumHeight(35f);
			cell.setHorizontalAlignment(Element.ALIGN_LEFT);
			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell.addElement(pa);
			table.addCell(cell);

			pa = new Paragraph();
			pa.setAlignment(Element.ALIGN_LEFT);
			if (getDBOperator().recordCount(Adres.class.getSimpleName(), "o.kisiRef.rID=" + uye.getKisiRef().getRID() + " AND o.adresturu=" + AdresTuru._ISADRESI.getCode()) > 0) {
				Adres adres = (Adres) getDBOperator().load(Adres.class.getSimpleName(), "o.kisiRef.rID=" + uye.getKisiRef().getRID() + " AND o.adresturu=" + AdresTuru._ISADRESI.getCode(), "o.varsayilan ASC").get(0);
				pa.add(new Phrase(" : " + adres.getAcikAdres() + " ", Fonts.NORMAL));
			} else {
				pa.add(new Phrase(" : " + "....", Fonts.NORMAL));
			}
			cell = new PdfPCell(new Phrase(pa));
			cell.setBorder(2);
			cell.setMinimumHeight(35f);
			cell.setHorizontalAlignment(Element.ALIGN_LEFT);
			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell.addElement(pa);
			table.addCell(cell);

			// EV ADRESI
			pa = new Paragraph();
			pa.setAlignment(Element.ALIGN_LEFT);
			pa.add(new Phrase("Ev Adresi", Fonts.BASLIK));
			cell = new PdfPCell(new Phrase(pa));
			cell.setBorder(2);
			cell.setMinimumHeight(35f);
			cell.setHorizontalAlignment(Element.ALIGN_LEFT);
			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell.addElement(pa);
			table.addCell(cell);

			pa = new Paragraph();
			pa.setAlignment(Element.ALIGN_LEFT);
			if (getDBOperator().recordCount(Adres.class.getSimpleName(), "o.kisiRef.rID=" + uye.getKisiRef().getRID() + " AND o.adresturu=" + AdresTuru._EVADRESI.getCode()) > 0) {
				Adres adres = (Adres) getDBOperator().load(Adres.class.getSimpleName(), "o.kisiRef.rID=" + uye.getKisiRef().getRID() + " AND o.adresturu=" + AdresTuru._EVADRESI.getCode(), "o.varsayilan").get(0);
				pa.add(new Phrase(" : " + adres.getAcikAdres() + " ", Fonts.NORMAL));
			} else {
				pa.add(new Phrase(" : " + "....", Fonts.NORMAL));
			}
			cell = new PdfPCell(new Phrase(pa));
			cell.setBorder(2);
			cell.setMinimumHeight(35f);
			cell.setHorizontalAlignment(Element.ALIGN_LEFT);
			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell.addElement(pa);
			table.addCell(cell);

			// IS TELEFONU
			pa = new Paragraph();
			pa.setAlignment(Element.ALIGN_LEFT);
			pa.add(new Phrase("Telefon (İş)", Fonts.BASLIK));
			cell = new PdfPCell(new Phrase(pa));
			cell.setBorder(2);
			cell.setMinimumHeight(35f);
			cell.setHorizontalAlignment(Element.ALIGN_LEFT);
			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell.addElement(pa);
			table.addCell(cell);

			pa = new Paragraph();
			pa.setAlignment(Element.ALIGN_LEFT);
			if (getDBOperator().recordCount(Telefon.class.getSimpleName(), "o.kisiRef.rID=" + uye.getKisiRef().getRID() + " AND o.telefonturu=" + TelefonTuru._ISTELEFONU.getCode()) > 0) {
				Telefon telefon = (Telefon) getDBOperator().load(Telefon.class.getSimpleName(), "o.kisiRef.rID=" + uye.getKisiRef().getRID() + " AND o.telefonturu=" + TelefonTuru._ISTELEFONU.getCode(), "o.varsayilan").get(0);
				pa.add(new Phrase(" : " + telefon.getTelefonno(), Fonts.NORMAL));
			} else {
				pa.add(new Phrase(" : " + "....", Fonts.NORMAL));
			}
			cell = new PdfPCell(new Phrase(pa));
			cell.setBorder(2);
			cell.setMinimumHeight(35f);
			cell.setHorizontalAlignment(Element.ALIGN_LEFT);
			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell.addElement(pa);
			table.addCell(cell);

			// EV TELEFONU
			pa = new Paragraph();
			pa.setAlignment(Element.ALIGN_LEFT);
			pa.add(new Phrase("Telefon (Ev)", Fonts.BASLIK));
			cell = new PdfPCell(new Phrase(pa));
			cell.setBorder(2);
			cell.setMinimumHeight(35f);
			cell.setHorizontalAlignment(Element.ALIGN_LEFT);
			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell.addElement(pa);
			table.addCell(cell);

			pa = new Paragraph();
			pa.setAlignment(Element.ALIGN_LEFT);
			if (getDBOperator().recordCount(Telefon.class.getSimpleName(), "o.kisiRef.rID=" + uye.getKisiRef().getRID() + " AND o.telefonturu=" + TelefonTuru._EVTELEFONU.getCode()) > 0) {
				Telefon telefon = (Telefon) getDBOperator().load(Telefon.class.getSimpleName(), "o.kisiRef.rID=" + uye.getKisiRef().getRID() + " AND o.telefonturu=" + TelefonTuru._EVTELEFONU.getCode(), "o.varsayilan").get(0);
				pa.add(new Phrase(" : " + telefon.getTelefonno(), Fonts.NORMAL));
			} else {
				pa.add(new Phrase(" : " + "....", Fonts.NORMAL));
			}
			cell = new PdfPCell(new Phrase(pa));
			cell.setBorder(2);
			cell.setMinimumHeight(35f);
			cell.setHorizontalAlignment(Element.ALIGN_LEFT);
			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell.addElement(pa);
			table.addCell(cell);

			// FAKS
			pa = new Paragraph();
			pa.setAlignment(Element.ALIGN_LEFT);
			pa.add(new Phrase("Faks", Fonts.BASLIK));
			cell = new PdfPCell(new Phrase(pa));
			cell.setBorder(2);
			cell.setMinimumHeight(35f);
			cell.setHorizontalAlignment(Element.ALIGN_LEFT);
			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell.addElement(pa);
			table.addCell(cell);

			pa = new Paragraph();
			pa.setAlignment(Element.ALIGN_LEFT);
			if (getDBOperator().recordCount(Telefon.class.getSimpleName(), "o.kisiRef.rID=" + uye.getKisiRef().getRID() + " AND o.telefonturu=" + TelefonTuru._FAKS.getCode()) > 0) {
				Telefon telefon = (Telefon) getDBOperator().load(Telefon.class.getSimpleName(), "o.kisiRef.rID=" + uye.getKisiRef().getRID() + " AND o.telefonturu=" + TelefonTuru._FAKS.getCode(), "o.varsayilan").get(0);
				pa.add(new Phrase(" : " + telefon.getTelefonno(), Fonts.NORMAL));
			} else {
				pa.add(new Phrase(" : " + "....", Fonts.NORMAL));
			}
			cell = new PdfPCell(new Phrase(pa));
			cell.setBorder(2);
			cell.setMinimumHeight(35f);
			cell.setHorizontalAlignment(Element.ALIGN_LEFT);
			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell.addElement(pa);
			table.addCell(cell);

			// GSM
			pa = new Paragraph();
			pa.setAlignment(Element.ALIGN_LEFT);
			pa.add(new Phrase("GSM", Fonts.BASLIK));
			cell = new PdfPCell(new Phrase(pa));
			cell.setBorder(2);
			cell.setMinimumHeight(35f);
			cell.setHorizontalAlignment(Element.ALIGN_LEFT);
			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell.addElement(pa);
			table.addCell(cell);

			pa = new Paragraph();
			pa.setAlignment(Element.ALIGN_LEFT);
			if (getDBOperator().recordCount(Telefon.class.getSimpleName(), "o.kisiRef.rID=" + uye.getKisiRef().getRID() + " AND o.telefonturu=" + TelefonTuru._GSM.getCode()) > 0) {
				Telefon telefon = (Telefon) getDBOperator().load(Telefon.class.getSimpleName(), "o.kisiRef.rID=" + uye.getKisiRef().getRID() + " AND o.telefonturu=" + TelefonTuru._GSM.getCode(), "o.varsayilan").get(0);
				pa.add(new Phrase(" : " + telefon.getTelefonno(), Fonts.NORMAL));
			} else {
				pa.add(new Phrase(" : " + "....", Fonts.NORMAL));
			}
			cell = new PdfPCell(new Phrase(pa));
			cell.setBorder(2);
			cell.setMinimumHeight(35f);
			cell.setHorizontalAlignment(Element.ALIGN_LEFT);
			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell.addElement(pa);
			table.addCell(cell);

			// Eposta Adresi
			pa = new Paragraph();
			pa.setAlignment(Element.ALIGN_LEFT);
			pa.add(new Phrase("e-Posta", Fonts.BASLIK));
			cell = new PdfPCell(new Phrase(pa));
			cell.setBorder(2);
			cell.setMinimumHeight(35f);
			cell.setHorizontalAlignment(Element.ALIGN_LEFT);
			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell.addElement(pa);
			table.addCell(cell);

			pa = new Paragraph();
			pa.setAlignment(Element.ALIGN_LEFT);
			if (getDBOperator().recordCount(Internetadres.class.getSimpleName(), "o.kisiRef.rID=" + uye.getKisiRef().getRID() + " AND o.netadresturu=" + NetAdresTuru._EPOSTA.getCode()) > 0) {
				Internetadres webAdres = (Internetadres) getDBOperator().load(Internetadres.class.getSimpleName(), "o.kisiRef.rID=" + uye.getKisiRef().getRID() + " AND o.netadresturu=" + NetAdresTuru._EPOSTA.getCode(), "o.varsayilan")
						.get(0);
				pa.add(new Phrase(" : " + webAdres.getNetadresmetni(), Fonts.NORMAL));
			} else {
				pa.add(new Phrase(" : " + "....", Fonts.NORMAL));
			}
			cell = new PdfPCell(new Phrase(pa));
			cell.setBorder(2);
			cell.setMinimumHeight(35f);
			cell.setHorizontalAlignment(Element.ALIGN_LEFT);
			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell.addElement(pa);
			table.addCell(cell);

			// Yayin icin Adresi
			pa = new Paragraph();
			pa.setAlignment(Element.ALIGN_LEFT);
			pa.add(new Phrase("Yayın İçin Adres", Fonts.BASLIK));
			cell = new PdfPCell(new Phrase(pa));
			cell.setBorder(2);
			cell.setMinimumHeight(35f);
			cell.setHorizontalAlignment(Element.ALIGN_LEFT);
			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell.addElement(pa);
			table.addCell(cell);

			pa = new Paragraph();
			pa.setAlignment(Element.ALIGN_LEFT);
			pa.add(new Phrase(" : " + "Ev ( )   İş ( )", Fonts.NORMAL));
			cell = new PdfPCell(new Phrase(pa));
			cell.setBorder(2);
			cell.setMinimumHeight(35f);
			cell.setHorizontalAlignment(Element.ALIGN_LEFT);
			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell.addElement(pa);
			table.addCell(cell);

			// Duzenleme Tarihi
			pa = new Paragraph();
			pa.setAlignment(Element.ALIGN_LEFT);
			pa.add(new Phrase("Düzenleme Tarihi", Fonts.BASLIK));
			cell = new PdfPCell(new Phrase(pa));
			cell.setBorder(2);
			cell.setMinimumHeight(35f);
			cell.setHorizontalAlignment(Element.ALIGN_LEFT);
			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell.addElement(pa);
			table.addCell(cell);

			pa = new Paragraph();
			pa.setAlignment(Element.ALIGN_LEFT);
			pa.add(new Phrase(" : " + DateUtil.dateToDMY(new Date()), Fonts.NORMAL));
			cell = new PdfPCell(new Phrase(pa));
			cell.setBorder(2);
			cell.setMinimumHeight(35f);
			cell.setHorizontalAlignment(Element.ALIGN_LEFT);
			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell.addElement(pa);
			table.addCell(cell);

			// Imza
			pa = new Paragraph();
			pa.setAlignment(Element.ALIGN_LEFT);
			pa.add(new Phrase("İmza", Fonts.BASLIK));
			cell = new PdfPCell(new Phrase(pa));
			cell.setBorder(2);
			cell.setMinimumHeight(35f);
			cell.setHorizontalAlignment(Element.ALIGN_LEFT);
			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell.addElement(pa);
			table.addCell(cell);

			pa = new Paragraph();
			pa.setAlignment(Element.ALIGN_LEFT);
			pa.add(new Phrase(" : ", Fonts.NORMAL));
			cell = new PdfPCell(new Phrase(pa));
			cell.setBorder(2);
			cell.setMinimumHeight(35f);
			cell.setHorizontalAlignment(Element.ALIGN_LEFT);
			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell.addElement(pa);
			table.addCell(cell);
			document.add(table); // TABLO EKLENDI
			/* FORM ICERIGI YAZILDI */
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		document.close();
		return stream.toByteArray();
	}

	private static void drawLine(PdfContentByte cb, float lineThickness, float pageWidth, float x, float y, float toX, float toY) {
		cb.setLineWidth(lineThickness); // Make a bit thicker than 1.0 default
		cb.moveTo(x, y);
		cb.lineTo(toX, toY);
		cb.stroke();
	}

	private static void writeToAbsolutePosition(PdfContentByte cb, String text, float xMargin, float yMargin, float width, float height, int leading, int alignment, Font fontStyle) {
		ColumnText ct = new ColumnText(cb);
		Phrase myText = new Phrase(text, fontStyle);
		// Width :595.0 --- Height :842.0
		ct.setSimpleColumn(myText, xMargin, yMargin, width, height, leading, alignment);
		try {
			ct.go();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@SuppressWarnings("unchecked")
	public static byte[] documentReport(String fileName) throws DocumentException, Exception {
		Document document = new Document();
		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		PdfWriter writer = PdfWriter.getInstance(document, stream);
		document.open();
		PdfContentByte cb = writer.getDirectContent();

		Rectangle rec = PageSize.A4; // Width :595.0 --- Height :842.0
		float pageWidth = rec.getWidth();
		@SuppressWarnings("unused")
		float pageHeight = rec.getHeight();

		// rec = rec.rotate(); // LANDSCAPE
		document.setMargins(leftMargin, rightMargin, topMargin, bottomMargin); // left, right, top, bottom
		document.open();
		Paragraph pa = new Paragraph();
		pa.setAlignment(Element.ALIGN_CENTER);
		try {
			HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
			String URL_IMAGES = request.getRealPath("/_image/logo_60_33.png");
			PdfPTable table = new PdfPTable(2);
			table.setWidthPercentage(100f);
			table.setWidths(new float[] { 15, 85 });
			table.getDefaultCell().setBorder(Rectangle.NO_BORDER);
			Image logo1 = Image.getInstance(URL_IMAGES);
			logo1.setAlignment(Element.ALIGN_LEFT);
			logo1.scaleAbsolute(60f, 33f);

			PdfPCell cell = new PdfPCell();
			cell.setBorder(0);
			cell.setHorizontalAlignment(Element.ALIGN_LEFT);
			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell.addElement(logo1);
			table.addCell(cell);

			pa.setAlignment(Element.ALIGN_CENTER);
			pa.add(new Phrase("TÜRK MÜHENDİS \n", Fonts.BASLIK));
			pa.add(new Phrase("VE MİMAR ODALARI BİRLİĞİ \n", Fonts.BASLIK));
			pa.add(new Phrase(DateUtil.getYear(new Date()) + " YILI EVRAK SAYILARI" + " \n", Fonts.BASLIK));
			cell = new PdfPCell(pa);
			cell.setLeading(4f, 1f);// Satir araliklari genisletiliyor
			cell.setBorder(0);
			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell.setHorizontalAlignment(Element.ALIGN_CENTER);
			table.addCell(cell);
			table.setSpacingAfter(70f); // Tablodan sonra 15 pk bosluk
			document.add(table); // TABLO EKLENDI

			List<Object[]> objectList = null;
			Integer[] movTypes = new Integer[] { EvrakHareketTuru._GELEN.getCode(), EvrakHareketTuru._GIDEN.getCode(), EvrakHareketTuru._ICYAZISMA.getCode() };

			PdfPTable tableMain = new PdfPTable(2);
			tableMain.setWidthPercentage(100f);
			tableMain.setWidths(new float[] { 50, 50 });
			tableMain.getDefaultCell().setBorder(Rectangle.NO_BORDER);

			for (Integer b : movTypes) {
				PdfPCell cellMain = new PdfPCell();
				cellMain.setBorder(Rectangle.NO_BORDER);
				Long toplam = new Long(0);
				// BASLIK
				table = new PdfPTable(2);
				table.setWidthPercentage(96f);
				table.setWidths(new float[] { 60, 40 });
				table.getDefaultCell().setBorder(Rectangle.BOX);
				pa = new Paragraph();
				pa.setAlignment(Element.ALIGN_CENTER);
				pa.add(new Phrase(EvrakHareketTuru.getWithCode(b) + " SAYILARI ", Fonts.BASLIK));
				cell = new PdfPCell(new Phrase(pa));
				cell.setBorder(Rectangle.BOX);
				cell.setColspan(2);
				cell.addElement(pa);
				table.addCell(cell);

				String query = "SELECT d.status, COUNT(d.rID) FROM " + ApplicationDescriptor._EVRAK_MODEL + " AS d " + " WHERE d.year = '" + DateUtil.getYear(new Date()) + "' " + " AND d.movementType = " + b + " GROUP BY d.status";
				objectList = getDBOperator().loadByQuery(query);
				for (Object[] objectArray : objectList) {
					pa = new Paragraph();
					pa.setAlignment(Element.ALIGN_CENTER);
					pa.add(new Phrase(StringUtil.makeFirstCharUpperRestLowerCase(EvrakDurum.getWithCode((Integer) objectArray[0]).getLabel()), Fonts.BASLIK));
					cell = new PdfPCell(new Phrase(pa));
					cell.setBorder(Rectangle.BOX);
					table.addCell(cell);

					pa = new Paragraph();
					pa.setAlignment(Element.ALIGN_RIGHT);
					toplam += (Long) objectArray[1];
					pa.add(new Phrase(objectArray[1] + "", Fonts.NORMAL));
					cell = new PdfPCell(new Phrase(pa));
					cell.setBorder(Rectangle.BOX);
					cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
					table.addCell(cell);
				}

				// TOPLAM SATIRI EKLENIYOR...
				pa = new Paragraph();
				pa.setAlignment(Element.ALIGN_CENTER);
				pa.add(new Phrase("TOPLAM ", Fonts.BASLIK));
				cell = new PdfPCell(new Phrase(pa));
				cell.setBorder(Rectangle.BOX);
				table.addCell(cell);

				pa = new Paragraph();
				pa.setAlignment(Element.ALIGN_RIGHT);
				pa.add(new Phrase(toplam + "", Fonts.BASLIK));
				cell = new PdfPCell(new Phrase(pa));
				cell.setBorder(Rectangle.BOX);
				cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
				table.addCell(cell);

				table.setSpacingAfter(20f);
				cellMain.addElement(table);
				tableMain.addCell(cellMain);
			}
			document.add(tableMain);

			// FOOTER
			// draw a line above text...
			drawLine(cb, 1.0f, pageWidth, 60f, 44f, pageWidth - 60, 44f);
			String text = "TOYS Evrak Yönetim Sistemi v0.01 ";
			writeToAbsolutePosition(cb, text, 0f, 0f, 595f, 40f, 11, Element.ALIGN_CENTER, Fonts.DIP_NOT);

		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		document.close();
		return stream.toByteArray();

	}

	public static class MyFontFactory implements FontProvider {
		@Override
		public Font getFont(String fontname, String encoding, boolean embedded, float size, int style, BaseColor color) {
			// return Fonts.NORMAL;
			BaseFont bf;
			try {
				String filePath = ManagedBeanLocator.locateSessionUser().getSystemPath();
				bf = BaseFont.createFont(filePath + "fonts/arialuni.ttf", "Identity-H", BaseFont.EMBEDDED);
			} catch (DocumentException e) {
				e.printStackTrace();
				return new Font();
			} catch (IOException e) {
				e.printStackTrace();
				return new Font();
			}
			return new Font(bf, size);
		}

		@Override
		public boolean isRegistered(String fontname) {
			return false;
		}
	}

	public static byte[] makbuz(String filename, Uye uyeForMakbuz, Kullanici systemUser, Odeme odeme) throws DocumentException, Exception {
		Document document = new Document();
		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		PdfWriter writer = PdfWriter.getInstance(document, stream);
		// writer.setPageEvent(new PDFBackground()); // Set background
		document.open();
		PdfContentByte cb = writer.getDirectContent();
		document.setMargins(leftMargin, rightMargin, topMargin, bottomMargin); // left, right, top, bottom
		document.open();
		Paragraph pa = new Paragraph();
		pa.setAlignment(Element.ALIGN_CENTER);
		@SuppressWarnings("unused")
		HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
		PdfPTable table = new PdfPTable(2);
		table.setWidthPercentage(100f);
		table.setWidths(new float[] { 15, 85 });
		table.getDefaultCell().setBorder(Rectangle.BOX);

		// PdfContentByte cb, String text, float xMargin, float yMargin, float
		// width, float height, int leading, int alignment, Font fontStyle
		// DIKKAT : Y MARGIN'LER ASAGIDAN HESAPLANIYOR...
		// UYE ADI SOYADI VE SICIL NUMARASI
		String text = uyeForMakbuz.getKisiRef().getUIStringShort() + " (" + uyeForMakbuz.getSicilno() + ")";
		writeToAbsolutePosition(cb, text, 85f, 681f, 500f, 40f, 11, Element.ALIGN_LEFT, Fonts.NORMAL);
		writeToAbsolutePosition(cb, text, 85f, 256f, 500f, 40f, 11, Element.ALIGN_LEFT, Fonts.NORMAL);

		// TARIH - GUN VE AY
		Date tarih = new Date();
		text = DateUtil.getDay(tarih) + "      " + DateUtil.getMonth(tarih); 
		System.out.println("TARIH TEXT : '" + text + "'");
		writeToAbsolutePosition(cb, text, 512f, 710f, 70f, 40f, 11, Element.ALIGN_RIGHT, Fonts.NORMAL);
		writeToAbsolutePosition(cb, text, 512f, 287f, 70f, 40f, 11, Element.ALIGN_RIGHT, Fonts.NORMAL);
		// YIL
		text = (DateUtil.getYear(tarih) + "").substring(3, 4);
		System.out.println("YIL TEXT : '" + text + "'");
		writeToAbsolutePosition(cb, text, 558f, 710f, 70f, 40f, 11, Element.ALIGN_RIGHT, Fonts.NORMAL);
		writeToAbsolutePosition(cb, text, 558f, 287f, 70f, 40f, 11, Element.ALIGN_RIGHT, Fonts.NORMAL);

		float a = 616f;
		float b = 190f;
		float xMargin = 160f;
		 
		// 1. ÜCRET KALEMİ
		if (odeme.getOdemeTuru().getCode() == OdemeTuru._BORC.getCode()) {
			text = "Kayıt Ücreti";
			// System.out.println("TEXT : '" + text + "'");
			writeToAbsolutePosition(cb, text, xMargin, a, 500f, 40f, 11, Element.ALIGN_LEFT, Fonts.NORMAL); 																											// KOPYA
			writeToAbsolutePosition(cb, text, xMargin, b, 500f, 40f, 11, Element.ALIGN_LEFT, Fonts.NORMAL); 

			text = odeme.getMiktar().toString() + "-";
			writeToAbsolutePosition(cb, text, xMargin, a, 540f, 40f, 11, Element.ALIGN_RIGHT, Fonts.NORMAL); 
			writeToAbsolutePosition(cb, text, xMargin, b, 540f, 40f, 11, Element.ALIGN_RIGHT, Fonts.NORMAL); 
		} else if (odeme.getOdemeTuru().getCode() == OdemeTuru._AIDAT.getCode()) {
			// 2. ÜCRET KALEMİ
			text = "Üye Aidat Bedeli";
			writeToAbsolutePosition(cb, text, xMargin, a, 500f, 40f, 11, Element.ALIGN_LEFT, Fonts.NORMAL); 
			writeToAbsolutePosition(cb, text, xMargin, b, 500f, 40f, 11, Element.ALIGN_LEFT, Fonts.NORMAL); 

			text = odeme.getMiktar().toString() + " TL";
			// text = "40.00-";
			writeToAbsolutePosition(cb, text, xMargin, a, 540f, 40f, 11, Element.ALIGN_RIGHT, Fonts.NORMAL); 
			writeToAbsolutePosition(cb, text, xMargin, b, 540f, 40f, 11, Element.ALIGN_RIGHT, Fonts.NORMAL); 
		}

		// TOPLAMLAR 
		text = "#" + TurkishNumber.sayi2YaziForParaFormatli(Double.parseDouble(odeme.getMiktar().toString())) + "#";
		writeToAbsolutePosition(cb, text, 90f, 480f, 500f, 40f, 11, Element.ALIGN_LEFT, Fonts.NORMAL); 
		writeToAbsolutePosition(cb, text, 90f, 59f, 500f, 40f, 11, Element.ALIGN_LEFT, Fonts.NORMAL); 

		// text = "110.00-";
		text = odeme.getMiktar().toString() + " TL";
		writeToAbsolutePosition(cb, text, 140f, 490f, 540f, 40f, 11, Element.ALIGN_RIGHT, Fonts.NORMAL); 
		writeToAbsolutePosition(cb, text, 140f, 65f, 540f, 40f, 11, Element.ALIGN_RIGHT, Fonts.NORMAL); 
		document.close();
		return stream.toByteArray();
	}

	public static byte[] gunlukAidatTahsilatRaporu(List<Odeme> odemeListesi) throws DocumentException, Exception {
		Document document = new Document();
		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		PdfWriter writer = PdfWriter.getInstance(document, stream);
		document.open();
		PdfContentByte cb = writer.getDirectContent();
		Rectangle rec = PageSize.A4; // Width :595.0 --- Height :842.0
		float pageWidth = rec.getWidth();
		// rec = rec.rotate(); // LANDSCAPE
		document.setMargins(leftMargin, rightMargin, topMargin, bottomMargin); // left, right, top, bottom
		document.open();

		Paragraph pa = new Paragraph();
		pa.setAlignment(Element.ALIGN_CENTER);
		PdfPTable table = new PdfPTable(1);
		table.setWidthPercentage(100f);
		table.getDefaultCell().setBorder(Rectangle.BOX);

		try {
			// HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
			// SistemParametre parametre = (SistemParametre) getDBOperator().load(SistemParametre.class.getSimpleName()).get(0);
			// String URL_IMAGES = parametre.getLogodegeri();
			table.setWidthPercentage(100f);
			// Image logo1 = Image.getInstance(URL_IMAGES);
			// logo1.setAlignment(Element.ALIGN_LEFT);
			// logo1.scaleAbsolute(60f, 33f);

			PdfPCell cell = new PdfPCell();
			cell.setBorder(0);
			cell.setHorizontalAlignment(Element.ALIGN_LEFT);
			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			// cell.addElement(logo1);
			table.addCell(cell);

			pa.setAlignment(Element.ALIGN_CENTER);
			pa.add(new Phrase("TÜRK MÜHENDİS \n", Fonts.BASLIK));
			pa.add(new Phrase("VE MİMAR ODALARI BİRLİĞİ \n", Fonts.BASLIK));
			pa.add(new Phrase(DateUtil.dateToDMY(odemeListesi.get(0).getTarih()) + " TARİHLİ AİDAT TAHSİLATLARI" + " \n", Fonts.BASLIK));
			cell = new PdfPCell(pa);
			cell.setLeading(2f, 1f);// Satir araliklari genisletiliyor
			cell.setBorder(0);
			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell.setHorizontalAlignment(Element.ALIGN_CENTER);
			cell.addElement(pa);
			table.addCell(cell);
			table.setSpacingAfter(40f); // Tablodan sonra 15 pk bosluk
			document.add(table); // TABLO EKLENDI

			PdfPTable tableMain = new PdfPTable(3);
			tableMain.setWidthPercentage(100f);
			tableMain.setWidths(new float[] { 60, 20, 20 });
			tableMain.getDefaultCell().setBorder(Rectangle.BOX);
			BigDecimal toplamOdeme = BigDecimal.ZERO;
			PdfPCell cellMain = new PdfPCell();
			// Basliklar set ediliyor.
			pa = new Paragraph();
			pa.setAlignment(Element.ALIGN_CENTER);
			pa.add(new Phrase(KeyUtil.getLabelValue("odeme.kisiRef"), Fonts.BASLIK));
			cellMain = new PdfPCell(new Phrase(pa));
			cellMain.setBorder(Rectangle.BOX);
			cellMain.addElement(pa);
			tableMain.addCell(cellMain);

			pa = new Paragraph();
			pa.setAlignment(Element.ALIGN_CENTER);
			pa.add(new Phrase(KeyUtil.getLabelValue("odeme.tarih"), Fonts.BASLIK));
			cellMain = new PdfPCell(new Phrase(pa));
			cellMain.setBorder(Rectangle.BOX);
			cellMain.addElement(pa);
			tableMain.addCell(cellMain);

			pa = new Paragraph();
			pa.setAlignment(Element.ALIGN_CENTER);
			pa.add(new Phrase(KeyUtil.getLabelValue("odeme.miktar"), Fonts.BASLIK));
			cellMain = new PdfPCell(new Phrase(pa));
			cellMain.setBorder(Rectangle.BOX);
			cellMain.addElement(pa);
			tableMain.addCell(cellMain);

			for (Odeme odeme : odemeListesi) {
				pa = new Paragraph();
				pa.setAlignment(Element.ALIGN_CENTER);
				pa.add(new Phrase(odeme.getKisiRef().getUIString(), Fonts.NORMAL));
				cellMain = new PdfPCell(new Phrase(pa));
				cellMain.setBorder(Rectangle.BOX);
				cellMain.addElement(pa);
				tableMain.addCell(cellMain);

				pa = new Paragraph();
				pa.setAlignment(Element.ALIGN_CENTER);
				pa.add(new Phrase(DateUtil.dateToDMY(odeme.getTarih()), Fonts.NORMAL));
				cellMain = new PdfPCell(new Phrase(pa));
				cellMain.setBorder(Rectangle.BOX);
				cellMain.addElement(pa);
				tableMain.addCell(cellMain);

				// Toplam odemelere ekleniyor.
				toplamOdeme = toplamOdeme.add(odeme.getMiktar());

				pa = new Paragraph();
				pa.setAlignment(Element.ALIGN_CENTER);
				pa.add(new Phrase(BigDecimalUtil.changeToCurrency(odeme.getMiktar()) + "", Fonts.NORMAL));
				cellMain = new PdfPCell(new Phrase(pa));
				cellMain.setBorder(Rectangle.BOX);
				cellMain.addElement(pa);
				tableMain.addCell(cellMain);
			}

			pa = new Paragraph();
			pa.setAlignment(Element.ALIGN_CENTER);
			pa.add(new Phrase("TOPLAM", Fonts.BASLIK));
			cellMain = new PdfPCell(new Phrase(pa));
			cellMain.setBorder(Rectangle.BOX);
			cellMain.addElement(pa);
			tableMain.addCell(cellMain);

			pa = new Paragraph();
			pa.setAlignment(Element.ALIGN_CENTER);
			pa.add(new Phrase("", Fonts.BASLIK));
			cellMain = new PdfPCell(new Phrase(pa));
			cellMain.setBorder(Rectangle.BOX);
			cellMain.addElement(pa);
			tableMain.addCell(cellMain);
			document.add(tableMain);

			pa = new Paragraph();
			pa.setAlignment(Element.ALIGN_CENTER);
			pa.add(new Phrase(BigDecimalUtil.changeToCurrency(toplamOdeme) + "", Fonts.BASLIK));
			cellMain = new PdfPCell(new Phrase(pa));
			cellMain.setBorder(Rectangle.BOX);
			cellMain.addElement(pa);
			tableMain.addCell(cellMain);

			// FOOTER draw a line above text...
			drawLine(cb, 1.0f, pageWidth, 60f, 44f, pageWidth - 60, 44f);
			String text = "TOYS Oda Yönetim Sistemi v0.01 ";
			writeToAbsolutePosition(cb, text, 0f, 0f, 595f, 40f, 11, Element.ALIGN_CENTER, Fonts.DIP_NOT);
			document.close();
		} catch (Exception e) {
			System.out.println("ERROR @gunlukAidatTahsilatRaporu of CustomPdfExport :" + e.getMessage());
		}
		return stream.toByteArray();
	}

	@SuppressWarnings("unused")
	private String convertTurkishCharToSpecialCharacters(String text) {
		text = text.replace("ü", "u");
		text = text.replace("ı", "i");
		text = text.replace("ö", "o");
		text = text.replace("ü", "u");
		text = text.replace("ş", "s");
		text = text.replace("ğ", "g");
		text = text.replace("ç", "c");
		text = text.replace("Ü", "U");
		text = text.replace("İ", "I");
		text = text.replace("Ö", "O");
		text = text.replace("Ü", "U");
		text = text.replace("Ş", "S");
		text = text.replace("Ğ", "G");
		text = text.replace("Ç", "C");
		return text;
	}

}
