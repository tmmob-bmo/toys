package tr.com.arf.toys.service.export;

import java.math.BigDecimal;

public class TurkishNumber {

	private static final String[] birler = { " Bir", " İki", " Üç", " Dört", " Beş", " Altı", " Yedi", " Sekiz", " Dokuz" };
	private static final String[] onlar = { " On", " Yirmi", " Otuz", " Kırk", " Elli", " Altmış", " Yetmiş", " Seksen", " Doksan" };
	private static final String[] katlar = { "", " Bin", " Milyon", " Milyar", " Trilyon" };

	public static String sayi2Yazi(long sayi) {
		StringBuffer sonuc = new StringBuffer();
		// Go through the number one group at a time.
		for (int i = katlar.length - 1; i >= 0; i--) {
			// Is the number as big as this group?
			long cutoff = (long) Math.pow(10, i * 3);
			if (sayi >= cutoff) {
				int thisPart = (int) (sayi / cutoff);
				if (thisPart >= 100) {
					if (thisPart / 100 > 1) {
						sonuc.append(birler[thisPart / 100 - 1]);
					}
					sonuc.append(" Yüz");
					thisPart = thisPart % 100;
				}

				if (thisPart >= 10) {
					sonuc.append(onlar[thisPart / 10 - 1]);
					thisPart = thisPart % 10;
				}

				if (thisPart >= 1) {
					if (!(cutoff == 1000 && thisPart == 1)) {
						sonuc.append(birler[thisPart - 1]);
					}
				}
				sonuc.append(katlar[i]);
				sayi = sayi % cutoff;
			}
		}

		if (sonuc.length() != 0) {
			sonuc.deleteCharAt(0);
		}
		return sonuc.toString();
	}

	public static String[] sayi2YaziForPara(double para) {
		String[] sonuc = new String[] { "", "" };
		if (para == 0) {
			return sonuc;
		}
		String strPara = new BigDecimal(para).setScale(2, 4).toString();
		int dotIndex = strPara.indexOf(".");
		long kusurat = Long.valueOf(String.valueOf(strPara).substring(dotIndex + 1));
		sonuc[0] = sayi2Yazi((long) para);
		if (kusurat > 0) {
			sonuc[1] = sayi2Yazi(kusurat);
		}
		return sonuc;
	}

	public static String sayi2YaziForParaFormatli(double para) {
		StringBuffer sonuc = new StringBuffer();
		String[] sonucListesi = sayi2YaziForPara(para);
		if (!sonucListesi[0].isEmpty()) {
			sonuc.append(sonucListesi[0]);
			sonuc.append(" Türk Lirası");
			if (!sonucListesi[1].isEmpty()) {
				sonuc.append(" ");
				sonuc.append(sonucListesi[1]);
				sonuc.append(" Kuruş");
			}
		}
		return sonuc.toString();
	}
}
