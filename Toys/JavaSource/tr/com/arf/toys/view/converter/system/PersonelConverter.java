package tr.com.arf.toys.view.converter.system; 
 
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.FacesConverter;

import tr.com.arf.framework.view.converter._base.BaseConverter;
import tr.com.arf.toys.db.model.system.Personel;
import tr.com.arf.toys.service.application.ManagedBeanLocator;
 
 
@FacesConverter(forClass=Personel.class, value="personelConverter")
public class PersonelConverter extends BaseConverter{
 	
 	public PersonelConverter() {
 		super();
 	}

 	@Override
 	public Object getAsObject(FacesContext facesContext,
 			UIComponent uIComponent, String value) {
 		if (value == null || value.trim().equalsIgnoreCase("")) {
 			return null;
 		}
 		// DBOperator dbOperator = new DBOperator();
 		try {
 			Long rid = Long.parseLong(value);
 			return ManagedBeanLocator.locateSessionController().getDBOperator().find("Personel", "rID", rid + "").get(0);
 		} catch(NumberFormatException e){ 
 			return null;
 		}
 	}

 	@Override
 	public String getAsString(FacesContext facesContext,
 			  UIComponent uIComponent, Object value) {
 		if (value == null) {
 			return null;
 		}

 		if (value instanceof Personel) {
 			Personel personel = (Personel) value;
 			return "" + personel.getRID();
 		} else if (value instanceof String) {
 			return "";
 		}
 		else { 
 			throw new IllegalArgumentException("object:" + value + " of type:" +
 					value.getClass().getName() + "; expected type: Personel Model");
 		}
 	}

 } // class
