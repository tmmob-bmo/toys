package tr.com.arf.toys.view.converter.kisi; 
 
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.FacesConverter;

import tr.com.arf.framework.view.converter._base.BaseConverter;
import tr.com.arf.toys.db.model.kisi.KomisyonDonemToplantiKatilimci;
import tr.com.arf.toys.service.application.ManagedBeanLocator;
 
 
@FacesConverter(forClass=KomisyonDonemToplantiKatilimci.class, value="komisyonDonemToplantiKatilimciConverter")
public class KomisyonDonemToplantiKatilimciConverter extends BaseConverter{
 	
 	public KomisyonDonemToplantiKatilimciConverter() {
 		super();
 	}

 	@Override
 	public Object getAsObject(FacesContext facesContext,
 			UIComponent uIComponent, String value) {
 		if (value == null || value.trim().equalsIgnoreCase("")) {
 			return null;
 		}
 		// DBOperator dbOperator = new DBOperator();
 		return ManagedBeanLocator.locateSessionController().getDBOperator().find("KomisyonDonemUyeKatilimci", "rID", value).get(0);
 	}

 	@Override
 	public String getAsString(FacesContext facesContext,
 			  UIComponent uIComponent, Object value) {
 		if (value == null) {
 			return null;
 		}

 		if (value instanceof KomisyonDonemToplantiKatilimci) {
 			KomisyonDonemToplantiKatilimci komisyonDonemToplantiKatilimci = (KomisyonDonemToplantiKatilimci) value;
 			return "" + komisyonDonemToplantiKatilimci.getRID();
 		}
 		else {
 			throw new IllegalArgumentException("object:" + value + " of type:" +
 					value.getClass().getName() + "; expected type: KomisyonDonemToplantiKatilimci Model");
 		}
 	}

 } // class
