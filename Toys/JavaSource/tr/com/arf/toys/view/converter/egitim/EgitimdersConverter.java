package tr.com.arf.toys.view.converter.egitim; 
 
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.FacesConverter;

import tr.com.arf.framework.view.converter._base.BaseConverter;
import tr.com.arf.toys.db.model.egitim.Egitimders;
import tr.com.arf.toys.service.application.ManagedBeanLocator;
 
 
@FacesConverter(forClass=Egitimders.class, value="egitimdersConverter")
public class EgitimdersConverter extends BaseConverter{
 	
 	public EgitimdersConverter() {
 		super();
 	}

 	@Override
 	public Object getAsObject(FacesContext facesContext,
 			UIComponent uIComponent, String value) {
 		if (value == null || value.trim().equalsIgnoreCase("")) {
 			return null;
 		}
 		// DBOperator dbOperator = new DBOperator();
 		return ManagedBeanLocator.locateSessionController().getDBOperator().find("Egitimders", "rID", value).get(0);
 	}

 	@Override
 	public String getAsString(FacesContext facesContext,
 			  UIComponent uIComponent, Object value) {
 		if (value == null  || value.getClass() == java.lang.String.class) {
 			return null;
 		}

 		if (value instanceof Egitimders) {
 			Egitimders egitimders = (Egitimders) value;
 			return "" + egitimders.getRID();
 		}
 		else {
 			throw new IllegalArgumentException("object:" + value + " of type:" +
 					value.getClass().getName() + "; expected type: Egitimders Model");
 		}
 	}

 } // class
