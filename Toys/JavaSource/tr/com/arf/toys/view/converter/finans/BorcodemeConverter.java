package tr.com.arf.toys.view.converter.finans; 
 
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.FacesConverter;

import tr.com.arf.framework.view.converter._base.BaseConverter;
import tr.com.arf.toys.db.model.finans.Borcodeme;
import tr.com.arf.toys.service.application.ManagedBeanLocator;
 
 
@FacesConverter(forClass=Borcodeme.class, value="borcodemeConverter")
public class BorcodemeConverter extends BaseConverter{
 	
 	public BorcodemeConverter() {
 		super();
 	}

 	@Override
 	public Object getAsObject(FacesContext facesContext,
 			UIComponent uIComponent, String value) {
 		if (value == null || value.trim().equalsIgnoreCase("")) {
 			return null;
 		}
 		// DBOperator dbOperator = new DBOperator();
 		return ManagedBeanLocator.locateSessionController().getDBOperator().find("Borcodeme", "rID", value).get(0);
 	}

 	@Override
 	public String getAsString(FacesContext facesContext,
 			  UIComponent uIComponent, Object value) {
 		if (value == null) {
 			return null;
 		}

 		if (value instanceof Borcodeme) {
 			Borcodeme borcodeme = (Borcodeme) value;
 			return "" + borcodeme.getRID();
 		}
 		else {
 			throw new IllegalArgumentException("object:" + value + " of type:" +
 					value.getClass().getName() + "; expected type: Borcodeme Model");
 		}
 	}

 } // class
