package tr.com.arf.toys.view.converter.egitim; 
 
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.FacesConverter;

import tr.com.arf.framework.view.converter._base.BaseConverter;
import tr.com.arf.toys.db.model.egitim.EgitimGunleri;
import tr.com.arf.toys.service.application.ManagedBeanLocator;
 
 
@FacesConverter(forClass=EgitimGunleri.class, value="egitimGunleriConverter")
public class EgitimGunleriConverter extends BaseConverter{
 	
 	public EgitimGunleriConverter() {
 		super();
 	}

 	@Override
 	public Object getAsObject(FacesContext facesContext,
 			UIComponent uIComponent, String value) {
 		if (value == null || value.trim().equalsIgnoreCase("")) {
 			return null;
 		}
 		// DBOperator dbOperator = new DBOperator();
 		return ManagedBeanLocator.locateSessionController().getDBOperator().find("EgitimGunleri", "rID", value).get(0);
 	}

 	@Override
 	public String getAsString(FacesContext facesContext,
 			  UIComponent uIComponent, Object value) {
 		if (value == null  || value.getClass() == java.lang.String.class) {
 			return null;
 		}

 		if (value instanceof EgitimGunleri) {
 			EgitimGunleri egitimGunleri = (EgitimGunleri) value;
 			return "" + egitimGunleri.getRID();
 		}
 		else {
 			throw new IllegalArgumentException("object:" + value + " of type:" +
 					value.getClass().getName() + "; expected type: EgitimGunleri Model");
 		}
 	}

 } // class
