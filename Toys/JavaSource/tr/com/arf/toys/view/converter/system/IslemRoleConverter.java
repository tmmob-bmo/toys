package tr.com.arf.toys.view.converter.system; 
 
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.FacesConverter;

import tr.com.arf.framework.view.converter._base.BaseConverter;
import tr.com.arf.toys.db.model.system.IslemRole;
import tr.com.arf.toys.service.application.ManagedBeanLocator;
 
@FacesConverter(forClass=IslemRole.class, value="islemRoleConverter")
public class IslemRoleConverter extends BaseConverter{
 	
 	public IslemRoleConverter() {
 		super();
 	}

 	@Override
 	public Object getAsObject(FacesContext facesContext,
 			UIComponent uIComponent, String value) {
 		if (value == null || value.trim().equalsIgnoreCase("")) {
 			return null;
 		}
 		// DBOperator dbOperator = new DBOperator();
 		return ManagedBeanLocator.locateSessionController().getDBOperator().find("IslemRole", "rID", value).get(0);
 	}

 	@Override
 	public String getAsString(FacesContext facesContext,
 			  UIComponent uIComponent, Object value) {
 		if (value == null) {
 			return null;
 		}

 		if (value instanceof IslemRole) {
 			IslemRole islemRole = (IslemRole) value;
 			return "" + islemRole.getRID();
 		}
 		else {
 			throw new IllegalArgumentException("object:" + value + " of type:" +
 					value.getClass().getName() + "; expected type: IslemRole Model");
 		}
 	}

 } // class
