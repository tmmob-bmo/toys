package tr.com.arf.toys.view.converter.etkinlik; 
 
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.FacesConverter;

import tr.com.arf.framework.view.converter._base.BaseConverter;
import tr.com.arf.toys.db.model.etkinlik.Etkinlikkuruluye;
import tr.com.arf.toys.service.application.ManagedBeanLocator;
 
 
@FacesConverter(forClass=Etkinlikkuruluye.class, value="etkinlikkuruluyeConverter")
public class EtkinlikkuruluyeConverter extends BaseConverter{
 	
 	public EtkinlikkuruluyeConverter() {
 		super();
 	}

 	@Override
 	public Object getAsObject(FacesContext facesContext,
 			UIComponent uIComponent, String value) {
 		if (value == null || value.trim().equalsIgnoreCase("")) {
 			return null;
 		}
 		// DBOperator dbOperator = new DBOperator();
 		return ManagedBeanLocator.locateSessionController().getDBOperator().find("Etkinlikkuruluye", "rID", value).get(0);
 	}

 	@Override
 	public String getAsString(FacesContext facesContext,
 			  UIComponent uIComponent, Object value) {
 		if (value == null  || value.getClass() == java.lang.String.class) {
 			return null;
 		}

 		if (value instanceof Etkinlikkuruluye) {
 			Etkinlikkuruluye etkinlikkuruluye = (Etkinlikkuruluye) value;
 			return "" + etkinlikkuruluye.getRID();
 		}
 		else {
 			throw new IllegalArgumentException("object:" + value + " of type:" +
 					value.getClass().getName() + "; expected type: Etkinlikkuruluye Model");
 		}
 	}

 } // class
