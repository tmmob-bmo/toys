package tr.com.arf.toys.view.converter.iletisim; 
 
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.FacesConverter;

import tr.com.arf.framework.view.converter._base.BaseConverter;
import tr.com.arf.toys.db.model.iletisim.Iletisimliste;
import tr.com.arf.toys.service.application.ManagedBeanLocator;
 
 
@FacesConverter(forClass=Iletisimliste.class, value="iletisimlisteConverter")
public class IletisimlisteConverter extends BaseConverter{
 	
 	public IletisimlisteConverter() {
 		super();
 	}

 	@Override
 	public Object getAsObject(FacesContext facesContext,
 			UIComponent uIComponent, String value) {
 		if (value == null || value.trim().equalsIgnoreCase("")) {
 			return null;
 		}
 		// DBOperator dbOperator = new DBOperator();
 		return ManagedBeanLocator.locateSessionController().getDBOperator().find("Iletisimliste", "rID", value).get(0);
 	}

 	@Override
 	public String getAsString(FacesContext facesContext,
 			  UIComponent uIComponent, Object value) {
 		if (value == null  || value.getClass() == java.lang.String.class) {
 			return null;
 		}

 		if (value instanceof Iletisimliste) {
 			Iletisimliste iletisimliste = (Iletisimliste) value;
 			return "" + iletisimliste.getRID();
 		}
 		else {
 			throw new IllegalArgumentException("object:" + value + " of type:" +
 					value.getClass().getName() + "; expected type: Iletisimliste Model");
 		}
 	}

 } // class
