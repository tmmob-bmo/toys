package tr.com.arf.toys.view.converter.kisi; 
 
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.FacesConverter;

import tr.com.arf.framework.view.converter._base.BaseConverter;
import tr.com.arf.toys.db.model.kisi.Kurum;
import tr.com.arf.toys.service.application.ManagedBeanLocator;
 
 
@FacesConverter(forClass=Kurum.class, value="kurumConverter")
public class KurumConverter extends BaseConverter{
 	
 	public KurumConverter() {
 		super();
 	}

 	@Override
 	public Object getAsObject(FacesContext facesContext,
 			UIComponent uIComponent, String value) {
 		if (value == null || value.trim().equalsIgnoreCase("")) {
 			return null;
 		}
 		// DBOperator dbOperator = new DBOperator();
 		try {
 			Long rid = Long.parseLong(value);
 			return ManagedBeanLocator.locateSessionController().getDBOperator().find("Kurum", "rID", rid + "").get(0);
 		} catch(NumberFormatException e){ 
 			return null;
 		}
 		
 	}

 	@Override
 	public String getAsString(FacesContext facesContext,
 			  UIComponent uIComponent, Object value) {
 		if (value == null || value.getClass() == java.lang.String.class) {
 			return null;
 		}

 		if (value instanceof Kurum) {
 			Kurum kurum = (Kurum) value;
 			return "" + kurum.getRID();
 		} else if (value instanceof String) {
 			return "";
 		} else {
 			throw new IllegalArgumentException("object:" + value + " of type:" +
 					value.getClass().getName() + "; expected type: Kurum Model");
 		}
 	}

 } // class
