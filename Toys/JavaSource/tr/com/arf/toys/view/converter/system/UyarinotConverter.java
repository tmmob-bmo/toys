package tr.com.arf.toys.view.converter.system; 
 
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.FacesConverter;

import tr.com.arf.framework.view.converter._base.BaseConverter;
import tr.com.arf.toys.db.model.system.Uyarinot;
import tr.com.arf.toys.service.application.ManagedBeanLocator;
 
 
@FacesConverter(forClass=Uyarinot.class, value="uyarinotConverter")
public class UyarinotConverter extends BaseConverter{
 	
 	public UyarinotConverter() {
 		super();
 	}

 	@Override
 	public Object getAsObject(FacesContext facesContext,
 			UIComponent uIComponent, String value) {
 		if (value == null || value.trim().equalsIgnoreCase("")) {
 			return null;
 		}
 		// DBOperator dbOperator = new DBOperator();
 		return ManagedBeanLocator.locateSessionController().getDBOperator().find("Uyarinot", "rID", value).get(0);
 	}

 	@Override
 	public String getAsString(FacesContext facesContext,
 			  UIComponent uIComponent, Object value) {
 		if (value == null) {
 			return null;
 		}

 		if (value instanceof Uyarinot) {
 			Uyarinot uyarinot = (Uyarinot) value;
 			return "" + uyarinot.getRID();
 		}
 		else {
 			throw new IllegalArgumentException("object:" + value + " of type:" +
 					value.getClass().getName() + "; expected type: Uyarinot Model");
 		}
 	}

 } // class
