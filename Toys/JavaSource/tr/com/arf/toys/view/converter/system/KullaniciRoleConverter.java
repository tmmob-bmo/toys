package tr.com.arf.toys.view.converter.system; 
 
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.FacesConverter;

import tr.com.arf.framework.view.converter._base.BaseConverter;
import tr.com.arf.toys.db.model.system.KullaniciRole;
import tr.com.arf.toys.service.application.ManagedBeanLocator;
 
@FacesConverter(forClass=KullaniciRole.class, value="kullaniciRoleConverter")   
public class KullaniciRoleConverter extends BaseConverter{
 	
 	public KullaniciRoleConverter() {
 		super();
 	}

 	@Override
 	public Object getAsObject(FacesContext facesContext,
 			UIComponent uIComponent, String value) { 
 		if (value == null || value.trim().equalsIgnoreCase("")) {
 			return null;
 		}
 		// DBOperator dbOperator = new DBOperator();
 		return ManagedBeanLocator.locateSessionController().getDBOperator().find("KullaniciRole", "rID", value).get(0); 		
 	}

 	@Override
 	public String getAsString(FacesContext facesContext,
 			  UIComponent uIComponent, Object value) { 
 		if (value == null) {
 			return null;
 		} 

 		if (value instanceof KullaniciRole) { 
 			KullaniciRole kullaniciRole = (KullaniciRole) value;
 			return "" + kullaniciRole.getRID();
 		}
 		else {
 			throw new IllegalArgumentException("object:" + value + " of type:" +
 					value.getClass().getName() + "; expected type: KullaniciRole Model");
 		}
 	}

 } // class
