package tr.com.arf.toys.view.converter.uye;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.FacesConverter;

import tr.com.arf.framework.db._management.DBOperator;
import tr.com.arf.framework.view.converter._base.BaseConverter;
import tr.com.arf.toys.db.model.uye.Uyeform;

@FacesConverter(forClass = Uyeform.class, value = "uyeformConverter")
public class UyeformConverter extends BaseConverter {

	public UyeformConverter() {
		super();
	}

	@Override
	public Object getAsObject(FacesContext facesContext, UIComponent uIComponent, String value) {
		if (value == null || value.trim().equalsIgnoreCase("")) {
			return null;
		}
		DBOperator dbOperator = new DBOperator();
		return dbOperator.find("Uyeform", "rID", value).get(0);
	}

	@Override
	public String getAsString(FacesContext facesContext, UIComponent uIComponent, Object value) {
		if (value == null || value.getClass() == java.lang.String.class) {
			return null;
		}

		if (value instanceof Uyeform) {
			Uyeform uyeform = (Uyeform) value;
			return "" + uyeform.getRID();
		} else {
			throw new IllegalArgumentException("object:" + value + " of type:" + value.getClass().getName() + "; expected type: Uyeform Model");
		}
	}

} // class
