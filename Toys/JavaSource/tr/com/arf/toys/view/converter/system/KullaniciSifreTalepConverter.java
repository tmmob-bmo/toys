package tr.com.arf.toys.view.converter.system; 
 
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.FacesConverter;

import tr.com.arf.framework.view.converter._base.BaseConverter;
import tr.com.arf.toys.db.model.system.KullaniciSifreTalep;
import tr.com.arf.toys.service.application.ManagedBeanLocator;
 
@FacesConverter(forClass=KullaniciSifreTalep.class, value="kullaniciSifreTalepConverter")  
public class KullaniciSifreTalepConverter extends BaseConverter{
 	
 	public KullaniciSifreTalepConverter() {
 		super();
 	}

 	@Override
 	public Object getAsObject(FacesContext facesContext,
 			UIComponent uIComponent, String value) {
 		if (value == null || value.trim().equalsIgnoreCase("")) {
 			return null;
 		}
 		// DBOperator dbOperator = new DBOperator();
 		return ManagedBeanLocator.locateSessionController().getDBOperator().find("KullaniciSifreTalep", "rID", value).get(0);
 	}

 	@Override
 	public String getAsString(FacesContext facesContext,
 			  UIComponent uIComponent, Object value) {
 		if (value == null) {
 			return null;
 		}

 		if (value instanceof KullaniciSifreTalep) {
 			KullaniciSifreTalep kullaniciSifreTalep = (KullaniciSifreTalep) value;
 			return "" + kullaniciSifreTalep.getRID();
 		}
 		else {
 			throw new IllegalArgumentException("object:" + value + " of type:" +
 					value.getClass().getName() + "; expected type: KullaniciSifreTalep Model");
 		}
 	}

 } // class
