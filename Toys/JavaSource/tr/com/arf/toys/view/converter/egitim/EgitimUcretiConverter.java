package tr.com.arf.toys.view.converter.egitim; 
 
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.FacesConverter;

import tr.com.arf.framework.view.converter._base.BaseConverter;
import tr.com.arf.toys.db.model.egitim.EgitimUcreti;
import tr.com.arf.toys.service.application.ManagedBeanLocator;
 
 
@FacesConverter(forClass=EgitimUcreti.class, value="egitimUcretiConverter")
public class EgitimUcretiConverter extends BaseConverter{
 	
 	public EgitimUcretiConverter() {
 		super();
 	}

 	@Override
 	public Object getAsObject(FacesContext facesContext,
 			UIComponent uIComponent, String value) {
 		if (value == null || value.trim().equalsIgnoreCase("")) {
 			return null;
 		}
 		// DBOperator dbOperator = new DBOperator();
 		return ManagedBeanLocator.locateSessionController().getDBOperator().find("EgitimUcreti", "rID", value).get(0);
 	}

 	@Override
 	public String getAsString(FacesContext facesContext,
 			  UIComponent uIComponent, Object value) {
 		if (value == null  || value.getClass() == java.lang.String.class) {
 			return null;
 		}

 		if (value instanceof EgitimUcreti) {
 			EgitimUcreti egitimUcreti = (EgitimUcreti) value;
 			return "" + egitimUcreti.getRID();
 		}
 		else {
 			throw new IllegalArgumentException("object:" + value + " of type:" +
 					value.getClass().getName() + "; expected type: EgitimUcreti Model");
 		}
 	}

 } // class
