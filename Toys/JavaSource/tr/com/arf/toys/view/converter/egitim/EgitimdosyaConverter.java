package tr.com.arf.toys.view.converter.egitim; 
 
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.FacesConverter;

import tr.com.arf.framework.view.converter._base.BaseConverter;
import tr.com.arf.toys.db.model.egitim.Egitimdosya;
import tr.com.arf.toys.service.application.ManagedBeanLocator;
 
 
@FacesConverter(forClass=Egitimdosya.class, value="egitimdosyaConverter")
public class EgitimdosyaConverter extends BaseConverter{
 	
 	public EgitimdosyaConverter() {
 		super();
 	}

 	@Override
 	public Object getAsObject(FacesContext facesContext,
 			UIComponent uIComponent, String value) {
 		if (value == null || value.trim().equalsIgnoreCase("")) {
 			return null;
 		}
 		// DBOperator dbOperator = new DBOperator();
 		return ManagedBeanLocator.locateSessionController().getDBOperator().find("Egitimdosya", "rID", value).get(0);
 	}

 	@Override
 	public String getAsString(FacesContext facesContext,
 			  UIComponent uIComponent, Object value) {
 		if (value == null  || value.getClass() == java.lang.String.class) {
 			return null;
 		}

 		if (value instanceof Egitimdosya) {
 			Egitimdosya egitimdosya = (Egitimdosya) value;
 			return "" + egitimdosya.getRID();
 		}
 		else {
 			throw new IllegalArgumentException("object:" + value + " of type:" +
 					value.getClass().getName() + "; expected type: Egitimdosya Model");
 		}
 	}

 } // class
