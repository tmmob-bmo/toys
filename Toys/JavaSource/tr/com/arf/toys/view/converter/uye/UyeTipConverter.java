package tr.com.arf.toys.view.converter.uye;

import javax.faces.convert.EnumConverter;
import javax.faces.convert.FacesConverter;

import tr.com.arf.toys.db.enumerated.uye.UyeTip;

@FacesConverter(value = "uyeTipConverter", forClass=UyeTip.class)
public class UyeTipConverter extends EnumConverter {

	public UyeTipConverter() {
		super(UyeTip.class);
	}

}