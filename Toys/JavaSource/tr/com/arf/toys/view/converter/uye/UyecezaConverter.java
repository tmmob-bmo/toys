package tr.com.arf.toys.view.converter.uye; 
 
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.FacesConverter;

import tr.com.arf.framework.view.converter._base.BaseConverter;
import tr.com.arf.toys.db.model.uye.Uyeceza;
import tr.com.arf.toys.service.application.ManagedBeanLocator;
 
 
@FacesConverter(forClass=Uyeceza.class, value="uyecezaConverter")
public class UyecezaConverter extends BaseConverter{
 	
 	public UyecezaConverter() {
 		super();
 	}

 	@Override
 	public Object getAsObject(FacesContext facesContext,
 			UIComponent uIComponent, String value) {
 		if (value == null || value.trim().equalsIgnoreCase("")) {
 			return null;
 		}
 		// DBOperator dbOperator = new DBOperator();
 		return ManagedBeanLocator.locateSessionController().getDBOperator().find("Uyeceza", "rID", value).get(0);
 	}

 	@Override
 	public String getAsString(FacesContext facesContext,
 			  UIComponent uIComponent, Object value) {
 		if (value == null) {
 			return null;
 		}

 		if (value instanceof Uyeceza) {
 			Uyeceza uyeceza = (Uyeceza) value;
 			return "" + uyeceza.getRID();
 		}
 		else {
 			throw new IllegalArgumentException("object:" + value + " of type:" +
 					value.getClass().getName() + "; expected type: Uyeceza Model");
 		}
 	}

 } // class
