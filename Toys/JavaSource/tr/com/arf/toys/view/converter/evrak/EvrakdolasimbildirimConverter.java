package tr.com.arf.toys.view.converter.evrak; 
 
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.FacesConverter;

import tr.com.arf.framework.view.converter._base.BaseConverter;
import tr.com.arf.toys.db.model.evrak.Evrakdolasimbildirim;
import tr.com.arf.toys.service.application.ManagedBeanLocator;
 
 
@FacesConverter(forClass=Evrakdolasimbildirim.class, value="evrakdolasimbildirimConverter")
public class EvrakdolasimbildirimConverter extends BaseConverter{
 	
 	public EvrakdolasimbildirimConverter() {
 		super();
 	}

 	@Override
 	public Object getAsObject(FacesContext facesContext,
 			UIComponent uIComponent, String value) {
 		if (value == null || value.trim().equalsIgnoreCase("")) {
 			return null;
 		}
 		// DBOperator dbOperator = new DBOperator();
 		return ManagedBeanLocator.locateSessionController().getDBOperator().find("Evrakdolasimbildirim", "rID", value).get(0);
 	}

 	@Override
 	public String getAsString(FacesContext facesContext,
 			  UIComponent uIComponent, Object value) {
 		if (value == null  || value.getClass() == java.lang.String.class) {
 			return null;
 		}

 		if (value instanceof Evrakdolasimbildirim) {
 			Evrakdolasimbildirim evrakdolasimbildirim = (Evrakdolasimbildirim) value;
 			return "" + evrakdolasimbildirim.getRID();
 		}
 		else {
 			throw new IllegalArgumentException("object:" + value + " of type:" +
 					value.getClass().getName() + "; expected type: Evrakdolasimbildirim Model");
 		}
 	}

 } // class
