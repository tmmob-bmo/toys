package tr.com.arf.toys.view.converter.system; 
 
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.FacesConverter;

import tr.com.arf.framework.view.converter._base.BaseConverter;
import tr.com.arf.toys.db.model.system.Duyuru;
import tr.com.arf.toys.service.application.ManagedBeanLocator;
 
 
@FacesConverter(forClass=Duyuru.class, value="duyuruConverter")
public class DuyuruConverter extends BaseConverter{
 	
 	public DuyuruConverter() {
 		super();
 	}

 	@Override
 	public Object getAsObject(FacesContext facesContext,
 			UIComponent uIComponent, String value) {
 		if (value == null || value.trim().equalsIgnoreCase("")) {
 			return null;
 		}
 		// DBOperator dbOperator = new DBOperator();
 		return ManagedBeanLocator.locateSessionController().getDBOperator().find("Duyuru", "rID", value).get(0);
 	}

 	@Override
 	public String getAsString(FacesContext facesContext,
 			  UIComponent uIComponent, Object value) {
 		if (value == null) {
 			return null;
 		}

 		if (value instanceof Duyuru) {
 			Duyuru duyuru = (Duyuru) value;
 			return "" + duyuru.getRID();
 		}
 		else {
 			throw new IllegalArgumentException("object:" + value + " of type:" +
 					value.getClass().getName() + "; expected type: Duyuru Model");
 		}
 	}

 } // class
