package tr.com.arf.toys.view.controller.form.egitim; 
 
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.toys.db.filter.egitim.EgitimUcretiFilter;
import tr.com.arf.toys.db.model.egitim.EgitimUcreti;
import tr.com.arf.toys.db.model.egitim.Egitimtanim;
import tr.com.arf.toys.service.application.ApplicationDescriptor;
import tr.com.arf.toys.view.controller.form._base.ToysBaseFilteredController;
  
  
@ManagedBean 
@ViewScoped 
public class EgitimUcretiController extends ToysBaseFilteredController {  
  
  private static final long serialVersionUID = 6531152165884980748L; 
  
	private EgitimUcreti egitimUcreti;  
	private EgitimUcretiFilter egitimUcretiFilter = new EgitimUcretiFilter();  
  
	public EgitimUcretiController() { 
		super(EgitimUcreti.class);  
		setDefaultValues(false);  
		setLoggable(false);  
		setTable(null); 
		if (getMasterEntity() != null) {
			setMasterObjectName(ApplicationDescriptor._EGITIMTANIM_MODEL);
			setMasterOutcome(ApplicationDescriptor._EGITIMTANIM_MODEL);
			getEgitimUcretiFilter().setEgitimtanimRef(getMasterEntity());
			getEgitimUcretiFilter().createQueryCriterias();
		}
		queryAction();
	}
	public Egitimtanim getMasterEntity() {
		if (getEgitimUcretiFilter().getEgitimtanimRef() != null) {
			return getEgitimUcretiFilter().getEgitimtanimRef();
		} else {
			return (Egitimtanim) getObjectFromSessionFilter(ApplicationDescriptor._EGITIMTANIM_MODEL);
		}
	}

	public EgitimUcreti getEgitimUcreti() {
		egitimUcreti=(EgitimUcreti) getEntity();
		return egitimUcreti;
	}

	public void setEgitimUcreti(EgitimUcreti egitimUcreti) {
		this.egitimUcreti = egitimUcreti;
	}

	public EgitimUcretiFilter getEgitimUcretiFilter() {
		return egitimUcretiFilter;
	}

	public void setEgitimUcretiFilter(EgitimUcretiFilter egitimUcretiFilter) {
		this.egitimUcretiFilter = egitimUcretiFilter;
	}

	@Override  
	public BaseFilter getFilter() {  
		return getEgitimUcretiFilter(); 
	}  
	
	@Override
		public void save() {
			if (getMasterEntity()!=null){
				egitimUcreti.setEgitimtanimRef(getMasterEntity());
			}else {
				createGenericMessage("Eğitim Ücreti Bilgileri Boş Geldiğinden Dolayı Kaydetme İşlemini"
						+ " Gerçekleştiremiyoruz!", FacesMessage.SEVERITY_INFO);
				return ;
			}
			super.save();
			queryAction();
		}
	
} // class 
