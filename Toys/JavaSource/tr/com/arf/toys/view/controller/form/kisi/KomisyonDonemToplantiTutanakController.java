package tr.com.arf.toys.view.controller.form.kisi; 
  
 
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.toys.db.filter.kisi.KomisyonDonemToplantiTutanakFilter;
import tr.com.arf.toys.db.model.kisi.KomisyonDonemToplanti;
import tr.com.arf.toys.db.model.kisi.KomisyonDonemToplantiTutanak;
import tr.com.arf.toys.service.application.ApplicationDescriptor;
import tr.com.arf.toys.view.controller.form._base.ToysBaseFilteredController;
  
  
@ManagedBean 
@ViewScoped 
public class KomisyonDonemToplantiTutanakController extends ToysBaseFilteredController {  
  
  private static final long serialVersionUID = 6531152165884980748L; 
  
	private KomisyonDonemToplantiTutanak komisyonDonemToplantiTutanak;  
	private KomisyonDonemToplantiTutanakFilter komisyonDonemToplantiTutanakFilter = new KomisyonDonemToplantiTutanakFilter();  
  
	public KomisyonDonemToplantiTutanakController() { 
		super(KomisyonDonemToplantiTutanak.class);  
		setDefaultValues(false);  
		setOrderField("path");  
		setLoggable(false);  
		setAutoCompleteSearchColumns(new String[]{"path"}); 
		setTable(null); 
		if(getMasterEntity() != null){ 
			setMasterObjectName(ApplicationDescriptor._KOMISYONDONEMTOPLANTI_MODEL);
			setMasterOutcome(ApplicationDescriptor._KOMISYONDONEMTOPLANTI_MODEL);
			getKomisyonDonemToplantiTutanakFilter().setKomisyonDonemToplantiRef(getMasterEntity());
			getKomisyonDonemToplantiTutanakFilter().createQueryCriterias();   
		} 
		queryAction(); 
	} 
 
	public KomisyonDonemToplanti getMasterEntity(){
		if(getKomisyonDonemToplantiTutanakFilter().getKomisyonDonemToplantiRef() != null){
			return getKomisyonDonemToplantiTutanakFilter().getKomisyonDonemToplantiRef();
		} else {
			return (KomisyonDonemToplanti) getObjectFromSessionFilter(ApplicationDescriptor._KOMISYONDONEMTOPLANTI_MODEL);
		}			
	} 	
	
	public KomisyonDonemToplantiTutanak getKomisyonDonemToplantiTutanak() { 
		komisyonDonemToplantiTutanak = (KomisyonDonemToplantiTutanak) getEntity(); 
		return komisyonDonemToplantiTutanak; 
	} 
	
	public void setKomisyonDonemToplantiTutanak(KomisyonDonemToplantiTutanak komisyonDonemToplantiTutanak) { 
		this.komisyonDonemToplantiTutanak = komisyonDonemToplantiTutanak; 
	} 
	
	public KomisyonDonemToplantiTutanakFilter getKomisyonDonemToplantiTutanakFilter() { 
		return komisyonDonemToplantiTutanakFilter; 
	} 
	 
	public void setKomisyonDonemToplantiTutanakFilter(KomisyonDonemToplantiTutanakFilter komisyonDonemToplantiTutanakFilter) {  
		this.komisyonDonemToplantiTutanakFilter = komisyonDonemToplantiTutanakFilter;  
	}  
	 
	@Override  
	public BaseFilter getFilter() {  
		return getKomisyonDonemToplantiTutanakFilter();  
	}  
 
	@Override	
	public void insert() {	
		super.insert();	
		getKomisyonDonemToplantiTutanak().setKomisyonDonemToplantiRef(getKomisyonDonemToplantiTutanakFilter().getKomisyonDonemToplantiRef());	
		getKomisyonDonemToplantiTutanak().setYukleyenRef(getSessionUser().getPersonelRef());
	}	
	
} // class 
