package tr.com.arf.toys.view.controller.form.system; 
 
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.framework.db.model._base.BaseEntity;
import tr.com.arf.toys.db.filter.system.BirimHesapFilter;
import tr.com.arf.toys.db.model.system.Birim;
import tr.com.arf.toys.db.model.system.BirimHesap;
import tr.com.arf.toys.service.application.ApplicationDescriptor;
import tr.com.arf.toys.view.controller.form._base.ToysBaseFilteredController;
  
  
@ManagedBean 
@ViewScoped 
public class BirimHesapController extends ToysBaseFilteredController {  
  
  private static final long serialVersionUID = 6531152165884980748L; 
  
	private BirimHesap birimHesap;  
	private BirimHesapFilter birimHesapFilter = new BirimHesapFilter();  
  
	public BirimHesapController() { 
		super(BirimHesap.class);  
		setDefaultValues(false);  
		setOrderField("bankaadi");  
		setAutoCompleteSearchColumns(new String[]{"bankaadi"}); 
		setTable(null); 
		setStaticWhereCondition(getSessionUser().createStaticWhereCondition("o."));
		if(getMasterEntity() != null){ 
		  if(getMasterEntity().getClass() == Birim.class){
			setMasterObjectName(ApplicationDescriptor._BIRIM_MODEL);
			setMasterOutcome(ApplicationDescriptor._BIRIM_MODEL);	
			getBirimHesapFilter().setBirimRef((Birim) getMasterEntity());
			} 
		  else {
				setMasterOutcome(null);
			}
		}
		getBirimHesapFilter().createQueryCriterias();  
		queryAction(); 
	} 
	
	public BaseEntity getMasterEntity(){
		if(getObjectFromSessionFilter(ApplicationDescriptor._BIRIM_MODEL) != null){
			return (Birim) getObjectFromSessionFilter(ApplicationDescriptor._BIRIM_MODEL);
		}  
		else {
			return null;
		}			
	} 
	
	public BirimHesap getBirimHesap() { 
		birimHesap = (BirimHesap) getEntity(); 
		return birimHesap; 
	} 
	
	public void setBirimHesap(BirimHesap birimHesap) { 
		this.birimHesap = birimHesap; 
	} 
	
	public BirimHesapFilter getBirimHesapFilter() { 
		return birimHesapFilter; 
	} 
	 
	public void setBirimHesapFilter(BirimHesapFilter birimHesapFilter) {  
		this.birimHesapFilter = birimHesapFilter;  
	}  
	 
	@Override  
	public BaseFilter getFilter() {  
		return getBirimHesapFilter();  
	}   
	
} // class 
