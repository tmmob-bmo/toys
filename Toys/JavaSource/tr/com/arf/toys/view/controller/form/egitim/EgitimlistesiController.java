package tr.com.arf.toys.view.controller.form.egitim;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.model.SelectItem;

import tr.com.arf.framework.db.enumerated._common.EvetHayir;
import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.framework.db.model._base.BaseEntity;
import tr.com.arf.framework.utility.tool.KeyUtil;
import tr.com.arf.toys.db.enumerated.egitim.EgitimDurum;
import tr.com.arf.toys.db.filter.egitim.EgitimFilter;
import tr.com.arf.toys.db.filter.egitim.EgitimkatilimciFilter;
import tr.com.arf.toys.db.filter.egitim.EgitimogretmenFilter;
import tr.com.arf.toys.db.model.egitim.Egitim;
import tr.com.arf.toys.db.model.egitim.Egitimkatilimci;
import tr.com.arf.toys.db.model.egitim.Egitimogretmen;
import tr.com.arf.toys.db.model.egitim.Egitimtanim;
import tr.com.arf.toys.service.application.ApplicationDescriptor;
import tr.com.arf.toys.service.application.ManagedBeanLocator;
import tr.com.arf.toys.view.controller.form._base.ToysBaseFilteredController;

@ManagedBean
@ViewScoped
public class EgitimlistesiController extends ToysBaseFilteredController {

	private static final long serialVersionUID = 6531152165884980748L;

	private Egitim egitim;
	private EgitimFilter egitimFilter = new EgitimFilter();
	public Egitimkatilimci egitimkatilimci;
	public final EgitimkatilimciFilter egitimkatilimciFilter = new EgitimkatilimciFilter();
	public Egitimogretmen egitimogretmen;
	public final EgitimogretmenFilter egitimogretmenFilter = new EgitimogretmenFilter();
	public EgitimkatilimciController egitimkatilimciController = new EgitimkatilimciController();
	protected boolean update = true;
	private EgitimtanimController egitimtanimController = new EgitimtanimController();
	private Egitimtanim egitimtanim = new Egitimtanim();
	
	public EgitimlistesiController() {
		super(Egitim.class);
		setDefaultValues(false);
		setOrderField("ad");
		setLoggable(false);
		setAutoCompleteSearchColumns(new String[] { "ad" });
		setTable(null);
		if (getMasterEntity() != null) {
			setMasterObjectName(ApplicationDescriptor._EGITIMTANIM_MODEL);
			setMasterOutcome(ApplicationDescriptor._EGITIMTANIM_MODEL);
			getEgitimFilter().setEgitimtanimRef(getMasterEntity());
			getEgitimFilter().createQueryCriterias();
		}
		queryAction();
	}
	
	public Egitimtanim getMasterEntity() {
		if (getEgitimFilter().getEgitimtanimRef() != null) {
			return getEgitimFilter().getEgitimtanimRef();
		} else {
			return (Egitimtanim) getObjectFromSessionFilter(ApplicationDescriptor._EGITIMTANIM_MODEL);
		}
	}

	public Egitim getEgitim() {
		egitim = (Egitim) getEntity();
		return egitim;
	}

	public void setEgitim(Egitim egitim) {
		this.egitim = egitim;
	}

	public EgitimFilter getEgitimFilter() {
		return egitimFilter;
	}

	public void setEgitimFilter(EgitimFilter egitimFilter) {
		this.egitimFilter = egitimFilter;
	}

	public EgitimkatilimciController getEgitimkatilimciController() {
		return egitimkatilimciController;
	}

	public void setEgitimkatilimciController(
			EgitimkatilimciController egitimkatilimciController) {
		this.egitimkatilimciController = egitimkatilimciController;
	}

	public EgitimkatilimciFilter getEgitimkatilimciFilter() {
		return egitimkatilimciFilter;
	}

	public boolean getDurumKontrol() {
		if (this.egitim.getDurum() == EgitimDurum._Gerceklesen) {
			return true;
		}
		return false;
	}
	public EgitimtanimController getEgitimtanimController() {
		return egitimtanimController;
	}

	public void setEgitimtanimController(EgitimtanimController egitimtanimController) {
		this.egitimtanimController = egitimtanimController;
	}
	
	public Egitimtanim getEgitimtanim() {
		return egitimtanim;
	}

	public void setEgitimtanim(Egitimtanim egitimtanim) {
		this.egitimtanim = egitimtanim;
	}
	public boolean getDurumKontrolForGerceklesen() {
		if (this.egitim.getDurum() == EgitimDurum._Gerceklesen) {
			return true;
		}
		return false;
	}

	public boolean getDurumKontrolForIptalEdilen() {
		if (this.egitim.getDurum() == EgitimDurum._Iptal) {
			return true;
		}
		return false;
	}

	@Override
	public BaseFilter getFilter() {
		return getEgitimFilter();
	}

	@Override
	public void resetFilter() {
		super.resetFilterOnly();
		if (getMasterEntity() != null) {
			getEgitimFilter().setEgitimtanimRef(getMasterEntity());
		}
		queryAction();
	}

	@Override
	public void insert() {
		super.insert();
		getEgitim().setEgitimtanimRef(getEgitimFilter().getEgitimtanimRef());
	}

	@Override
	public void delete() {
		if (this.egitim.getDurum() == EgitimDurum._Gerceklesen) {
			createGenericMessage(
					KeyUtil.getMessageValue("egitim.gerceklesen"),
					FacesMessage.SEVERITY_ERROR);
		} else {
			super.delete();
		}
	}

	@Override
	public void update() {

		update = false;
		if (this.egitim.getDurum() == EgitimDurum._Gerceklesen) {
			createGenericMessage(
					KeyUtil.getMessageValue("egitim.gerceklesenDurum"),
					FacesMessage.SEVERITY_ERROR);
		} else {
			super.update();
		}
	}
	

	@Override
	public void save() {

		BaseEntity ch = this.getEntity();

		if (((Egitim) ch).getDurum() == EgitimDurum._Gerceklesen) {
			int check = -1;
			String criter = new String("EGITIMREF = "
					+ this.egitim.getRID().toString());
			egitimkatilimciFilter.setEgitimRef(getEgitim());
			egitimkatilimciFilter.createQueryCriterias();

			try {
				check = getDBOperator().recordCount("Egitimogretmen", criter);
			} catch (Exception e) {
				logYaz("Exception @" + getModelName() + "Controller :", e);
			}
			if (check < 1) {
				createGenericMessage(
						KeyUtil.getMessageValue("egitim.birOgretmen"),
						FacesMessage.SEVERITY_ERROR);
			} else {
				try {
					check = getDBOperator().recordCount("Egitimkatilimci",
							criter);
				} catch (Exception e) {
					logYaz("Exception @" + getModelName() + "Controller :", e);
				}
				if (check < 1) {
					createGenericMessage(
							KeyUtil.getMessageValue("egitim.birKatilimci"),
							FacesMessage.SEVERITY_ERROR);
				} else {
					createGenericMessage(KeyUtil.getMessageValue("egitim.basarili"),
							FacesMessage.SEVERITY_INFO);
					update = true;
				}
			}

		} else {
			update = true;
		}

		if (update) {
			if (this.getEgitim().getBaslangictarih()
					.before(this.getEgitim().getBitistarih())) {
				// createGenericMessage(
				// KeyUtil.getMessageValue("egitim.planlananEklendi"),
				// FacesMessage.SEVERITY_INFO);
//				if (this.egitim.getSinavli() == EvetHayir._HAYIR) {
//					Date date = new Date();
//					this.egitim.setSinavtarihi(date);
//					this.egitim.setSinavyeri("");
//					this.egitim.setTarih(date);
//					this.egitim.setDurum(EgitimDurum._Planlanan);
//					super.save();
//				}else{
//					Date date = new Date();
//					this.egitim.setTarih(date);
					super.save();
//				}
				
			} else {
				createGenericMessage(
						KeyUtil.getMessageValue("egitim.tarihAralikKontrol"),
						FacesMessage.SEVERITY_ERROR);
			}
		}
		update = true;
	}
	
	@SuppressWarnings("unchecked")
	public SelectItem[] selectItemListForEgitim(String modelNameForSelectItems) {
		List<BaseEntity> entityList = getDBOperator().loadAll(modelNameForSelectItems, null, "rID");
		if (entityList != null) {
			SelectItem[] selectItemList = new SelectItem[entityList.size() + 1];
			selectItemList[0] = new SelectItem(null, "");
			int i = 1;
			for (BaseEntity entity : entityList) {
				selectItemList[i++] = new SelectItem(entity, entity.getUIString());
			}
			return selectItemList;
		}
		return new SelectItem[0]; 
	}
	
	public boolean handleSinavliChange(AjaxBehaviorEvent event) {
		try {
			HtmlSelectOneMenu menu = (HtmlSelectOneMenu) event.getComponent();
			if (menu.getValue() instanceof EvetHayir) {
//				if (this.egitim.getSinavli() == EvetHayir._EVET) { 
					return true;
//				}
			} else {
				return false;
			}
		} catch (Exception e) {
			logYaz("Exception @" + getModelName() + "Controller :", e);
		}
		return true;
	}
	public int getKalanKontenjan() throws Exception {
		String criter = new String("EGITIMREF = "
				+ this.getMasterEntity().getRID().toString());
		egitimkatilimciFilter.createQueryCriterias(); 
		int kalanKontenjan= getDBOperator().recordCount("Egitimkatilimci", criter);
		return this.egitim.getKontenjan() - kalanKontenjan ;
		
	}
	
	public String egitimkatilimci() {
		if (!setSelected()) {
			return "";
		}
		getSessionUser().setLegalAccess(1);
		putObjectToSessionFilter(ApplicationDescriptor._EGITIM_MODEL, getEgitim());
		return ManagedBeanLocator.locateMenuController().gotoPage("menu_Egitimkatilimci");
	}

	public String egitimders() {
		if (!setSelected()) {
			return "";
		}
		getSessionUser().setLegalAccess(1);
		putObjectToSessionFilter(ApplicationDescriptor._EGITIM_MODEL, getEgitim());
		return ManagedBeanLocator.locateMenuController().gotoPage("menu_Egitimders");
	}

	public String egitimogretmen() {
		if (!setSelected()) {
			return "";
		}
		getSessionUser().setLegalAccess(1);
		putObjectToSessionFilter(ApplicationDescriptor._EGITIM_MODEL, getEgitim());
		return ManagedBeanLocator.locateMenuController().gotoPage("menu_Egitimogretmen");
	}

	public String egitimdosya() {
		if (!setSelected()) {
			return "";
		}
		getSessionUser().setLegalAccess(1);
		putObjectToSessionFilter(ApplicationDescriptor._EGITIM_MODEL, getEgitim());
		return ManagedBeanLocator.locateMenuController().gotoPage("menu_Egitimdosya");
	}

} // class 
