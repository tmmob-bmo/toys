package tr.com.arf.toys.view.controller.form.kisi; 
  
 
import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.component.html.HtmlSelectBooleanCheckbox;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.model.SelectItem;

import tr.com.arf.framework.db.enumerated._common.EvetHayir;
import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.framework.db.model._base.BaseEntity;
import tr.com.arf.framework.utility.exception.data.DBException;
import tr.com.arf.toys.db.enumerated.kisi.KomisyonGorev;
import tr.com.arf.toys.db.enumerated.kisi.KomisyonUyelikDurumu;
import tr.com.arf.toys.db.enumerated.kisi.UyeTuru;
import tr.com.arf.toys.db.enumerated.uye.UyeDurum;
import tr.com.arf.toys.db.enumerated.uye.UyeTip;
import tr.com.arf.toys.db.filter.kisi.KomisyonDonemUyeFilter;
import tr.com.arf.toys.db.model.kisi.Kisi;
import tr.com.arf.toys.db.model.kisi.KomisyonDonem;
import tr.com.arf.toys.db.model.kisi.KomisyonDonemUye;
import tr.com.arf.toys.db.model.kisi.KomisyonDonemUyeDurum;
import tr.com.arf.toys.db.model.uye.Uye;
import tr.com.arf.toys.service.application.ApplicationDescriptor;
import tr.com.arf.toys.view.controller.form._base.ToysBaseFilteredController;
  
  
@ManagedBean 
@ViewScoped 
public class KomisyonDonemUyeController extends ToysBaseFilteredController {  
  
  private static final long serialVersionUID = 6531152165884980748L; 
  
	private KomisyonDonemUye komisyonDonemUye;  
	private KomisyonDonemUyeFilter komisyonDonemUyeFilter = new KomisyonDonemUyeFilter();  
  
	public KomisyonDonemUyeController() { 
		super(KomisyonDonemUye.class);  
		setDefaultValues(false);  
		setTable(null); 
		if(getMasterEntity() != null){ 
			  if(getMasterEntity().getClass() == KomisyonDonem.class){
				setMasterObjectName(ApplicationDescriptor._KOMISYONDONEM_MODEL);
				setMasterOutcome(ApplicationDescriptor._KOMISYONDONEM_MODEL);	
				getKomisyonDonemUyeFilter().setKomisyonDonemRef((KomisyonDonem) getMasterEntity());
				} 
			  else {
					setMasterOutcome(null);
				}
			}
		getKomisyonDonemUyeFilter().createQueryCriterias();  
		queryAction(); 
	} 
	
	public KomisyonDonemUyeController(Object object) { 
		super(KomisyonDonemUye.class);    
	}
	
	public BaseEntity getMasterEntity(){
		if(getObjectFromSessionFilter(ApplicationDescriptor._KOMISYONDONEM_MODEL) != null){
			return (KomisyonDonem) getObjectFromSessionFilter(ApplicationDescriptor._KOMISYONDONEM_MODEL);
		}  
		else {
			return null;
		}			
	} 
	
	public KomisyonDonemUye getKomisyonDonemUye() {
		komisyonDonemUye=(KomisyonDonemUye) getEntity();
		return komisyonDonemUye;
	}

	public void setKomisyonDonemUye(KomisyonDonemUye komisyonDonemUye) {
		this.komisyonDonemUye = komisyonDonemUye;
	}

	public KomisyonDonemUyeFilter getKomisyonDonemUyeFilter() {
		return komisyonDonemUyeFilter;
	}

	public void setKomisyonDonemUyeFilter(KomisyonDonemUyeFilter komisyonDonemUyeFilter) {
		this.komisyonDonemUyeFilter = komisyonDonemUyeFilter;
	}

	@Override
	public BaseFilter getFilter() {
		return getKomisyonDonemUyeFilter();
	}

	@Override
	public void insert(){
		super.insert();
		getKomisyonDonemUye().setKomisyonDonemRef((KomisyonDonem) getMasterEntity());
		getKomisyonDonemUye().setKomisyonUyelikDurumu(KomisyonUyelikDurumu._NULLDEGERI);
		getKomisyonDonemUye().setKomisyonGorev(KomisyonGorev._UYE);
		UyeTuruItems = new SelectItem[UyeTuru.values().length - 1];
		for (int x = 0; x < UyeTuru.values().length - 1; x++) {
			UyeTuruItems[x] = new SelectItem(
				UyeTuru.values()[x + 1],
				UyeTuru.values()[x + 1].getLabel());
		}
		setUyeTuruItems(UyeTuruItems);
//		super.fillDualListFor(Birim.class.getSimpleName(), "o.durum=" + BirimDurum._ACIK.getCode() + " AND o.erisimKodu LIKE '" 
//				+ ((KomisyonDonem) getMasterEntity()).getKomisyonRef().getBirimRef().getErisimKodu() +"%'", "ad");
	}
	

	@SuppressWarnings("unchecked")
	public SelectItem[] getSelectItemListForTurkUye(){
		List<Uye> entityList = getDBOperator().load(Uye.class.getSimpleName(), "o.uyetip <>" + UyeTip._YABANCILAR.getCode(), "rID");
		if (entityList != null) {
			SelectItem[] selectItemList = new SelectItem[entityList.size() + 1];
			selectItemList[0] = new SelectItem(null, "");
			int i = 1;
			for (Uye entity : entityList) {
				selectItemList[i++] = new SelectItem(entity, entity.getUIString());
			}
			return selectItemList;
		}
		return new SelectItem[0]; 
	}
	
	@SuppressWarnings("unchecked")
	public SelectItem[] getSelectItemListForUyeOlmayanKisi(){
		List<Kisi> entityList = getDBOperator().load(Kisi.class.getSimpleName(), "o.aktif = " + EvetHayir._EVET.getCode() + 
				" AND o.rID NOT IN (Select m.kisiRef.rID From Uye m WHERE " +
				" m.uyedurum = " + UyeDurum._AKTIFUYE.getCode()  + " OR   " +
						"m.uyedurum = " + UyeDurum._ASKER.getCode()  + " OR " +
								"m.uyedurum = " + UyeDurum._YURTDISI.getCode()  + " OR " +
										"m.uyedurum = " + UyeDurum._DOGUMIZNI.getCode()  + ")", "rID");
		if (entityList != null) {
			SelectItem[] selectItemList = new SelectItem[entityList.size() + 1];
			selectItemList[0] = new SelectItem(null, "");
			int i = 1;
			for (Kisi entity : entityList) {
				selectItemList[i++] = new SelectItem(entity, entity.getUIString());
			}
			return selectItemList;
		}
		return new SelectItem[0]; 
	}
	
	
	@SuppressWarnings("unchecked")
	public Kisi getUyeOlmayanKisi(){
		ArrayList<Kisi> kisiListesi=new ArrayList<Kisi>();
		List<Kisi> entityList = getDBOperator().load(Kisi.class.getSimpleName(), "o.aktif = " + EvetHayir._EVET.getCode() + 
				" AND o.rID NOT IN (Select m.kisiRef.rID From Uye m WHERE " +
				" m.uyedurum = " + UyeDurum._AKTIFUYE.getCode()  + " OR   " +
						"m.uyedurum = " + UyeDurum._ASKER.getCode()  + " OR " +
								"m.uyedurum = " + UyeDurum._YURTDISI.getCode()  + " OR " +
										"m.uyedurum = " + UyeDurum._DOGUMIZNI.getCode()  + ")", "rID");
		if (entityList != null) {
			for (Kisi entity : entityList) {
				kisiListesi.add(entity);
			}
			return kisiListesi.get(0);
		}
		return kisiListesi.get(0); 
	}
	
	@SuppressWarnings("unchecked")
	public List<Kisi> getUyeOlmayanKisiListesi(){
		ArrayList<Kisi> kisiListesi=new ArrayList<Kisi>();
		List<Kisi> entityList = getDBOperator().load(Kisi.class.getSimpleName(), "o.aktif = " + EvetHayir._EVET.getCode() + 
				" AND o.rID NOT IN (Select m.kisiRef.rID From Uye m WHERE " +
				" m.uyedurum = " + UyeDurum._AKTIFUYE.getCode()  + " OR   " +
						"m.uyedurum = " + UyeDurum._ASKER.getCode()  + " OR " +
								"m.uyedurum = " + UyeDurum._YURTDISI.getCode()  + " OR " +
										"m.uyedurum = " + UyeDurum._DOGUMIZNI.getCode()  + ")", "rID");
		if (entityList != null) {
			for (Kisi entity : entityList) {
				kisiListesi.add(entity);
			}
			return kisiListesi;
		}
		return kisiListesi; 
	}
	 
	public void handleUyeKisiChange(AjaxBehaviorEvent event) {
		try { 
			HtmlSelectOneMenu menu = (HtmlSelectOneMenu) event.getComponent(); 	
			if(menu.getValue() instanceof Uye){ 
				this.komisyonDonemUye.setKisiRef(null);
			} else if(menu.getValue() instanceof Kisi){ 
				this.komisyonDonemUye.setUyeRef(null);
			}  			
		} catch(Exception e){
			logYaz("Exception @" + getModelName() + "Controller :", e);
		}
	}  
	
	@Override
	public void save() {
		if (komisyonDonemUye.getRID()!=null){
			if (komisyonDonemUye.getKomisyonGorev()== KomisyonGorev._BASKAN  ) {
				@SuppressWarnings("unchecked")
				List <KomisyonDonemUye> komisyonDonemUyeListesi= getDBOperator().load(KomisyonDonemUye.class.getSimpleName(), "o.rID <>" + komisyonDonemUye.getRID() + " AND o.komisyonDonemRef.rID=" +
					komisyonDonemUye.getKomisyonDonemRef().getRID() + " AND  o.komisyonGorev=" +KomisyonGorev._BASKAN.getCode() +
					" AND o.durum="+EvetHayir._EVET.getCode() ,"");
					if (!isEmpty(komisyonDonemUyeListesi)){
							createGenericMessage("Birden Fazla Başkanı Üye Yapamazsınız ! ", FacesMessage.SEVERITY_ERROR);
							return ;
					}
				
			} else if ( komisyonDonemUye.getKomisyonGorev()== KomisyonGorev._BASKANYARDIMCISI ) {
				@SuppressWarnings("unchecked")
				List <KomisyonDonemUye> komisyonDonemUyeListesi= getDBOperator().load(KomisyonDonemUye.class.getSimpleName(), "o.rID <>" + komisyonDonemUye.getRID() + " AND o.komisyonDonemRef.rID=" +
					komisyonDonemUye.getKomisyonDonemRef().getRID() + " AND  o.komisyonGorev=" +KomisyonGorev._BASKANYARDIMCISI.getCode() +
					" AND o.durum="+EvetHayir._EVET.getCode(),"");
				if (!isEmpty(komisyonDonemUyeListesi)){
							createGenericMessage("Birden Fazla Başkan Yardımcısı Üye Yapamazsınız ! ", FacesMessage.SEVERITY_ERROR);
							return ;
				}
				
			} else if (	komisyonDonemUye.getKomisyonGorev()== KomisyonGorev._RAPORTOR ) {
				@SuppressWarnings("unchecked")
				List <KomisyonDonemUye> komisyonDonemUyeListesi= getDBOperator().load(KomisyonDonemUye.class.getSimpleName(),"o.rID <>" + komisyonDonemUye.getRID() + " AND o.komisyonDonemRef.rID=" +
					komisyonDonemUye.getKomisyonDonemRef().getRID() + " AND o.komisyonGorev=" +KomisyonGorev._RAPORTOR.getCode()
					+" AND o.durum="+EvetHayir._EVET.getCode() ,"");
				if (!isEmpty(komisyonDonemUyeListesi)){
						if (komisyonDonemUye.getKomisyonGorev()== KomisyonGorev._RAPORTOR){
							createGenericMessage("Birden Fazla Raportör Üye Yapamazsınız ! ", FacesMessage.SEVERITY_ERROR);
							return ;
						}
				}
				
			}
			
		}else{
			if (komisyonDonemUye.getKomisyonGorev()== KomisyonGorev._BASKAN  ) {
				@SuppressWarnings("unchecked")
				List <KomisyonDonemUye> komisyonDonemUyeListesi= getDBOperator().load(KomisyonDonemUye.class.getSimpleName(), "o.komisyonDonemRef.rID=" +
					komisyonDonemUye.getKomisyonDonemRef().getRID() + " AND  o.komisyonGorev=" +KomisyonGorev._BASKAN.getCode()+
					" AND o.durum="+EvetHayir._EVET.getCode(),"");
					if (!isEmpty(komisyonDonemUyeListesi)){
							createGenericMessage("Birden Fazla Başkanı Üye Yapamazsınız ! ", FacesMessage.SEVERITY_ERROR);
							return ;
					}
				
			} else if ( komisyonDonemUye.getKomisyonGorev()== KomisyonGorev._BASKANYARDIMCISI ) {
				@SuppressWarnings("unchecked")
				List <KomisyonDonemUye> komisyonDonemUyeListesi= getDBOperator().load(KomisyonDonemUye.class.getSimpleName(), "o.komisyonDonemRef.rID=" +
					komisyonDonemUye.getKomisyonDonemRef().getRID() + " AND  o.komisyonGorev=" +KomisyonGorev._BASKANYARDIMCISI.getCode() +
					" AND o.durum="+EvetHayir._EVET.getCode(),"");
				if (!isEmpty(komisyonDonemUyeListesi)){
							createGenericMessage("Birden Fazla Başkan Yardımcısı Üye Yapamazsınız ! ", FacesMessage.SEVERITY_ERROR);
							return ;
				}
				
			} else if (	komisyonDonemUye.getKomisyonGorev()== KomisyonGorev._RAPORTOR ) {
				@SuppressWarnings("unchecked")
				List <KomisyonDonemUye> komisyonDonemUyeListesi= getDBOperator().load(KomisyonDonemUye.class.getSimpleName(), "o.komisyonDonemRef.rID=" +
					komisyonDonemUye.getKomisyonDonemRef().getRID() + " AND o.komisyonGorev=" +KomisyonGorev._RAPORTOR.getCode() +
					" AND o.durum="+EvetHayir._EVET.getCode(),"");
				if (!isEmpty(komisyonDonemUyeListesi)){
						if (komisyonDonemUye.getKomisyonGorev()== KomisyonGorev._RAPORTOR){
							createGenericMessage("Birden Fazla Raportör Üye Yapamazsınız ! ", FacesMessage.SEVERITY_ERROR);
							return ;
						}
				}
				
			}
			
		}
		
		super.justSave();
		if (komisyonDonemUye.getKomisyonUyelikDurumu()==KomisyonUyelikDurumu._UYELIGINDUSMESI || komisyonDonemUye.getKomisyonUyelikDurumu()==KomisyonUyelikDurumu._UYELIKTENISTIFA){
			komisyonDonemUye.setDurum(EvetHayir._HAYIR);
		} else {
			komisyonDonemUye.setDurum(EvetHayir._EVET);
		}
		
		
		try {
			getDBOperator().update(komisyonDonemUye);
			if (getKomisyonDonemUye()!=null){
				KomisyonDonemUyeDurum komisyonUyeDurum=new KomisyonDonemUyeDurum ();
				
				if (getKomisyonDonemUye().getUyeRef()!=null){
					komisyonUyeDurum.setUyeRef(getKomisyonDonemUye().getUyeRef());
				}
				if (getKomisyonDonemUye().getKisiRef()!=null){
					komisyonUyeDurum.setKisiRef(getKomisyonDonemUye().getKisiRef());
				}
				if (getKomisyonDonemUye().getDurumtarihi()!=null){
					komisyonUyeDurum.setDurumtarihi(getKomisyonDonemUye().getDurumtarihi());
				}
				if (getKomisyonDonemUye().getKomisyonUyelikDurumu()!=null){
					komisyonUyeDurum.setKomisyonUyelikDurumu(getKomisyonDonemUye().getKomisyonUyelikDurumu());
				}
				komisyonUyeDurum.setKomisyonDonemRef(getKomisyonDonemUye().getKomisyonDonemRef());
				komisyonUyeDurum.setDurum(getKomisyonDonemUye().getDurum());
				komisyonUyeDurum.setKomisyonGorev(getKomisyonDonemUye().getKomisyonGorev());
//				komisyonUyeDurum.setAktif(getKomisyonDonemUye().getDurum());
				komisyonUyeDurum.setUyeTuru(getKomisyonDonemUye().getUyeTuru());
				getDBOperator().insert(komisyonUyeDurum);
			}
		} catch (DBException e) {
			// TODO Auto-generated catch block
			logYaz("Exception @" + getModelName() + "Controller :", e);
		}
		queryAction();
	}
	

	private UyeTuru uyeTuru;

	public UyeTuru getUyeTuru() {
		return uyeTuru;
	}

	public void setUyeTuru(UyeTuru uyeTuru) {
		this.uyeTuru = uyeTuru;
	}
	

	private SelectItem[] UyeTuruItems;

	public SelectItem[] getUyeTuruItems() {
		return UyeTuruItems;
	}

	public void setUyeTuruItems(SelectItem[] uyeTuruItems) {
		UyeTuruItems = uyeTuruItems;
	}

	public void handleChangeForUyeTuru(AjaxBehaviorEvent event) {
		try { 
			HtmlSelectOneMenu menu = (HtmlSelectOneMenu) event.getComponent(); 			 
			KomisyonGorev secilenKomisyonGorev = (KomisyonGorev) menu.getValue();
			if (secilenKomisyonGorev!=KomisyonGorev._UYE){
				UyeTuruItems = new SelectItem[UyeTuru.values().length - 2];
				for (int x = 0; x < UyeTuru.values().length - 2; x++) {
					UyeTuruItems[x] = new SelectItem(
						UyeTuru.values()[x + 1],
						UyeTuru.values()[x + 1].getLabel());
				}
				
			}
			else {
				UyeTuruItems = new SelectItem[UyeTuru.values().length - 1];
				for (int x = 0; x < UyeTuru.values().length - 1; x++) {
					UyeTuruItems[x] = new SelectItem(
						UyeTuru.values()[x + 1],
						UyeTuru.values()[x + 1].getLabel());
				}
			}
			setUyeTuruItems(UyeTuruItems);
		} catch(Exception e){
			logYaz("Exception @" + getModelName() + "Controller :", e);
		}
		
	}
	
	
	@Override
		public void update() {
		try {
			
			if (getKomisyonDonemUye().getKomisyonGorev()!=KomisyonGorev._UYE){
				UyeTuruItems = new SelectItem[UyeTuru.values().length - 2];
				for (int x = 0; x < UyeTuru.values().length - 2; x++) {
					UyeTuruItems[x] = new SelectItem(
						UyeTuru.values()[x + 1],
						UyeTuru.values()[x + 1].getLabel());
				}
				
			}
			else {
				UyeTuruItems = new SelectItem[UyeTuru.values().length - 1];
				for (int x = 0; x < UyeTuru.values().length - 1; x++) {
					UyeTuruItems[x] = new SelectItem(
						UyeTuru.values()[x + 1],
						UyeTuru.values()[x + 1].getLabel());
				}
			}
			setUyeTuruItems(UyeTuruItems);
		} catch(Exception e){
			logYaz("Exception @" + getModelName() + "Controller :", e);
		}
		
			super.update();
		}
	
	private Boolean sadeceuye=false;
	
	public Boolean getSadeceuye() {
		return sadeceuye;
	}

	public void setSadeceuye(Boolean sadeceuye) {
		this.sadeceuye = sadeceuye;
	}

	public void handleChangeSadeceUyeler(AjaxBehaviorEvent event) {
		try { 
			HtmlSelectBooleanCheckbox menu = (HtmlSelectBooleanCheckbox) event.getComponent(); 
			if(menu.getClientId().equalsIgnoreCase("sadeceUyeGetir")){
				if((boolean) menu.getValue() == true){
					this.sadeceuye=true;
				}
			}   
		} catch(Exception e){
			logYaz("Error @handleUyeler :" + e.getMessage());
		}
	} 
	
	@SuppressWarnings("rawtypes")
	public List completeMethodKisiNotUye(String value) {  
		String[] values = new String[getSessionUser().getAutoCompleteSearchColumns(Kisi.class.getSimpleName()).length];
		for(int x = 0; x < getSessionUser().getAutoCompleteSearchColumns(Kisi.class.getSimpleName()).length; x++){
			values[x] = value;
		}  
		List entityList = getDBOperator().load(Kisi.class.getSimpleName(),  "o.rID NOT IN ( SELECT m.kisiRef.rID FROM  " + Uye.class.getSimpleName() +" AS m ) " +
				"AND o.aktif=" + EvetHayir._EVET.getCode() + " AND (UPPER(o.ad) LIKE UPPER('%" + value + "%') OR UPPER(o.soyad) LIKE UPPER ('%" + value + "%'))  )", "o.ad");	
		return entityList;
	}
	
	@SuppressWarnings("rawtypes")
	public List completeMethodKisiUyeBirim(String value) {  
		String[] values = new String[getSessionUser().getAutoCompleteSearchColumns(Kisi.class.getSimpleName()).length];
		for(int x = 0; x < getSessionUser().getAutoCompleteSearchColumns(Kisi.class.getSimpleName()).length; x++){
			values[x] = value;
		}
		List entityList=new ArrayList<>();
		try {
			if(getDBOperator().recordCount(Kisi.class.getSimpleName(),  "o.rID IN ( SELECT m.kisiRef.rID FROM  " + Uye.class.getSimpleName() +" AS m WHERE m.birimRef.rID=" + 
					((KomisyonDonem)getMasterEntity()).getBirimRef().getRID()  +" ) " +
					"AND o.aktif=" + EvetHayir._EVET.getCode() + " AND (UPPER(o.ad) LIKE UPPER('%" + value + "%') OR UPPER(o.soyad) LIKE UPPER ('%" + value + "%'))")>0){
				logYaz("Bu kisi uyede var");
				entityList = getDBOperator().load(Kisi.class.getSimpleName(), "o.rID IN ( SELECT m.kisiRef.rID FROM  " + Uye.class.getSimpleName() +" AS m WHERE m.birimRef.rID=" + 
						((KomisyonDonem)getMasterEntity()).getBirimRef().getRID()  +" ) " +
						"AND o.aktif=" + EvetHayir._EVET.getCode() + " AND (UPPER(o.ad) LIKE UPPER('%" + value + "%') OR UPPER(o.soyad) LIKE UPPER ('%" + value + "%'))", "o.ad");	
				
			}else if (getDBOperator().recordCount(Kisi.class.getSimpleName(),  "o.rID NOT IN ( SELECT m.kisiRef.rID FROM  " + Uye.class.getSimpleName() +" AS m ) " +
					"AND o.aktif=" + EvetHayir._EVET.getCode() + " AND (UPPER(o.ad) LIKE UPPER('%" + value + "%') OR UPPER(o.soyad) LIKE UPPER ('%" + value + "%'))  )")>0){
				logYaz("Uyede olmayan bir kisi");
				entityList = getDBOperator().load(Kisi.class.getSimpleName(),  "o.rID NOT IN ( SELECT m.kisiRef.rID FROM  " + Uye.class.getSimpleName() +" AS m ) " +
						"AND o.aktif=" + EvetHayir._EVET.getCode() + " AND (UPPER(o.ad) LIKE UPPER('%" + value + "%') OR UPPER(o.soyad) LIKE UPPER ('%" + value + "%'))  )", "o.ad");
			}
		} catch (DBException e) {
			// TODO Auto-generated catch block
			logYaz("Exception @" + getModelName() + "Controller :", e);
		}
		return entityList;
	}
	


	
} // class 
