package tr.com.arf.toys.view.controller.form.egitim; 
 
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.toys.db.filter.egitim.SinavGozetmenFilter;
import tr.com.arf.toys.db.model.egitim.Sinav;
import tr.com.arf.toys.db.model.egitim.SinavGozetmen;
import tr.com.arf.toys.service.application.ApplicationDescriptor;
import tr.com.arf.toys.view.controller.form._base.ToysBaseFilteredController;
  
  
@ManagedBean 
@ViewScoped 
public class SinavGozetmenController extends ToysBaseFilteredController {  
  
  private static final long serialVersionUID = 6531152165884980748L; 
  
	private SinavGozetmen sinavGozetmen;  
	private SinavGozetmenFilter sinavGozetmenFilter = new SinavGozetmenFilter();  
  
	public SinavGozetmenController() { 
		super(SinavGozetmen.class);  
		setDefaultValues(false);  
		setLoggable(false);  
		setTable(null); 
		if (getMasterEntity() != null) {
			setMasterObjectName(ApplicationDescriptor._SINAV_MODEL);
			setMasterOutcome(ApplicationDescriptor._SINAV_MODEL);
			getSinavGozetmenFilter().setSinavRef(getMasterEntity());
			getSinavGozetmenFilter().createQueryCriterias();
		}
		queryAction();
	}
	
	public Sinav getMasterEntity() {
		if (getSinavGozetmenFilter().getSinavRef() != null) {
			return getSinavGozetmenFilter().getSinavRef();
		} else {
			return (Sinav) getObjectFromSessionFilter(ApplicationDescriptor._SINAV_MODEL);
		}
	}
	

	public SinavGozetmen getSinavGozetmen() {
		sinavGozetmen=(SinavGozetmen) getEntity();
		return sinavGozetmen;
	}

	public void setSinavGozetmen(SinavGozetmen sinavGozetmen) {
		this.sinavGozetmen = sinavGozetmen;
	}

	public SinavGozetmenFilter getSinavGozetmenFilter() {
		return sinavGozetmenFilter;
	}

	public void setSinavGozetmenFilter(SinavGozetmenFilter sinavGozetmenFilter) {
		this.sinavGozetmenFilter = sinavGozetmenFilter;
	}

	@Override  
	public BaseFilter getFilter() {  
		return getSinavGozetmenFilter(); 
	}  
	
	@Override
		public void save() {
			
			if (getMasterEntity()!=null){
				sinavGozetmen.setSinavRef(getMasterEntity());
			}else {
				createGenericMessage("Gözetmen Bilgileri Boş Geldiğinden Dolayı Kaydetme İşlemini"
						+ " Gerçekleştiremiyoruz!", FacesMessage.SEVERITY_INFO);
				return ;
			}
			super.save();
			queryAction();
		}
	
} // class 
