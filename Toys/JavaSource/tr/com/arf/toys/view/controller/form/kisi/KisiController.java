package tr.com.arf.toys.view.controller.form.kisi;

import java.io.File;
import java.rmi.RemoteException;
import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.component.html.HtmlSelectBooleanCheckbox;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.model.SelectItem;
import javax.persistence.Transient;

import org.apache.log4j.Logger;
import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;

import tr.com.arf.framework.db.enumerated._common.EvetHayir;
import tr.com.arf.framework.db.enumerated._common.IslemTuru;
import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.framework.db.model.sistem.Islemlog;
import tr.com.arf.framework.utility.exception.data.DBException;
import tr.com.arf.framework.utility.tool.DateUtil;
import tr.com.arf.framework.utility.tool.KeyUtil;
import tr.com.arf.toys.db.enumerated.iletisim.AdresTipi;
import tr.com.arf.toys.db.enumerated.iletisim.AdresTuru;
import tr.com.arf.toys.db.enumerated.iletisim.NetAdresTuru;
import tr.com.arf.toys.db.enumerated.iletisim.TelefonTuru;
import tr.com.arf.toys.db.enumerated.kisi.Cinsiyet;
import tr.com.arf.toys.db.enumerated.kisi.KimlikTip;
import tr.com.arf.toys.db.enumerated.kisi.KisiDurum;
import tr.com.arf.toys.db.enumerated.kisi.MedeniHal;
import tr.com.arf.toys.db.enumerated.system.KpsTuru;
import tr.com.arf.toys.db.enumerated.uye.OgrenimTip;
import tr.com.arf.toys.db.enumerated.uye.UyeDurum;
import tr.com.arf.toys.db.enumerated.uye.UyeKayitDurum;
import tr.com.arf.toys.db.enumerated.uye.UyeTip;
import tr.com.arf.toys.db.enumerated.uye.UyekurumDurum;
import tr.com.arf.toys.db.filter.kisi.KisiFilter;
import tr.com.arf.toys.db.manager.CustomDBOperator;
import tr.com.arf.toys.db.model.iletisim.Adres;
import tr.com.arf.toys.db.model.iletisim.Internetadres;
import tr.com.arf.toys.db.model.iletisim.Telefon;
import tr.com.arf.toys.db.model.kisi.Kisi;
import tr.com.arf.toys.db.model.kisi.Kisifotograf;
import tr.com.arf.toys.db.model.kisi.Kisikimlik;
import tr.com.arf.toys.db.model.kisi.Kisiogrenim;
import tr.com.arf.toys.db.model.kisi.Kurum;
import tr.com.arf.toys.db.model.system.Ilce;
import tr.com.arf.toys.db.model.system.Kpslog;
import tr.com.arf.toys.db.model.system.KurumKisi;
import tr.com.arf.toys.db.model.system.Personel;
import tr.com.arf.toys.db.model.system.Sehir;
import tr.com.arf.toys.db.model.system.SistemParametre;
import tr.com.arf.toys.db.model.system.Ulke;
import tr.com.arf.toys.db.model.uye.Uye;
import tr.com.arf.toys.db.nonEntityModel.PojoAdres;
import tr.com.arf.toys.db.nonEntityModel.PojoKisi;
import tr.com.arf.toys.service.application.ApplicationDescriptor;
import tr.com.arf.toys.service.application.KPSService;
import tr.com.arf.toys.service.application.ManagedBeanLocator;
import tr.com.arf.toys.utility.application.ApplicationConstant;
import tr.com.arf.toys.utility.tool.ToysXMLReader;
import tr.com.arf.toys.view.controller.form._base.ToysBaseFilteredController;
import tr.com.arf.toys.view.controller.form.iletisim.AdresController;
import tr.com.arf.toys.view.controller.form.iletisim.InternetadresController;
import tr.com.arf.toys.view.controller.form.iletisim.TelefonController;
import tr.com.arf.toys.view.controller.form.system.KurumKisiController;
import tr.com.arf.toys.view.controller.form.system.PersonelController;
import tr.com.arf.toys.view.controller.form.uye.UyeController;

@ManagedBean
@ViewScoped
public class KisiController extends ToysBaseFilteredController {

	private static final long serialVersionUID = 6531152165884980748L;

	protected static Logger logger = Logger.getLogger(KisiController.class);
	protected static Logger epostaLogger = Logger.getLogger("eposta");

	private Kisi kisi;
	private Kisi kisiDetay;
	private KisiFilter kisiFilter = new KisiFilter();
	private String selectedTab;
	private int wizardStep = 1;

	private Adres adres = new Adres();
	private Telefon telefon = new Telefon();
	private Internetadres internetadres = new Internetadres();
	private KurumKisi kurumKisi;
	private Kurum kurum;

	private String detailEditType;
	private Kisifotograf kisifotograf;
	private Kisikimlik kisikimlik;
	private Personel personel;
	private Uye uye;
	private Kisiogrenim kisiogrenim;

	/* LOG ICIN */
	private String oldValueForAdres;
	private String oldValueForInternetadres;
	private String oldValueForInternetadresMetni;
	private String oldValueForTelefon;
	private String oldValueForPersonel;
	private String oldValueForUye;
	private String oldValueForKisiogrenim;
	private String oldValueForKisi;
	private String oldValueForKisikimlik;
	private String oldValueForKisiFotograf;
	private String oldValueForKurumKisi;

	private Adres kpsAdres;
	private Adres kpsDigerAdres;
	private Adres adres1;
	private Adres isyeriadresi;

	/* MANAGED PROPERTIES USED */
	private InternetadresController internetadresController = new InternetadresController(null);
	private AdresController adresController = new AdresController(null);
	private TelefonController telefonController = new TelefonController(null);
	private KisifotografController kisifotografController = new KisifotografController(null);
	private KisikimlikController kisikimlikController = new KisikimlikController(null);
	private PersonelController personelController = new PersonelController(null);
	private UyeController uyeController = new UyeController(null);
	private KisiogrenimController kisiogrenimController = new KisiogrenimController(null);
	private KurumKisiController kurumKisiController = new KurumKisiController(new Kisi());

	private boolean kpsSorguSonucDondu = false;
	private boolean kpsKaydiBulundu = false;
	private Boolean isyeriadresvarsayilan = false;
	private Boolean beyanadresvarsayilan = false;
	private Kurum previousKurumForKurumKisi;

	private boolean uyeYap = false;

	private Uye kisiUye;

	public KisiController() {
		super(Kisi.class);
		setDefaultValues(false);
		setOrderField("kimlikno");
		setStaticWhereCondition(ApplicationConstant._kisiStaticWhereCondition);
		setAutoCompleteSearchColumns(new String[] { "kimlikno" });
		setTable(null);
		setSelectedTab("kisi");
		setLoggable(true);
		if (getMasterEntity() != null && getMasterEntity().getRID() != null) {
			this.kisiDetay = getMasterEntity();
			/* Detay sayfasinda sadece secilen kaydin gorunmesi icin */
			this.kisiFilter.setKimliknoEquals(this.kisiDetay.getKimlikno());
			/* Listeye donuldugunda,kimliknumarasina göre arama yapilmasi icin */
			this.kisiFilter.setKimlikno(this.kisiDetay.getKimlikno());
			createKisiDetay("kisi");
		}
		queryAction();
		prepareForWizard();
		setEditPanelRendered(false);
	}

	public KisiController(Object object) {
		super(Kisi.class);
		setLoggable(true);
	}

	public Kisi getMasterEntity() {
		if (getKisi() != null && getKisi().getRID() != null) {
			return getKisi();
		} else if (getObjectFromSessionFilter(getModelName()) != null) {
			return (Kisi) getObjectFromSessionFilter(getModelName());
		} else {
			return null;
		}
	}

	public Kisi getKisi() {
		kisi = (Kisi) getEntity();
		return kisi;
	}

	public void setKisi(Kisi kisi) {
		this.kisi = kisi;
	}

	public Kisi getKisiDetay() {
		return kisiDetay;
	}

	public void setKisiDetay(Kisi kisiDetay) {
		this.kisiDetay = kisiDetay;
	}

	public KisiFilter getKisiFilter() {
		return kisiFilter;
	}

	public void setKisiFilter(KisiFilter kisiFilter) {
		this.kisiFilter = kisiFilter;
	}

	public Adres getAdres1() {
		return adres1;
	}

	public void setAdres1(Adres adres1) {
		this.adres1 = adres1;
	}

	public Kisiogrenim getKisiogrenim() {
		return kisiogrenim;
	}

	public void setKisiogrenim(Kisiogrenim kisiogrenim) {
		this.kisiogrenim = kisiogrenim;
	}

	public String getOldValueForKisiogrenim() {
		return oldValueForKisiogrenim;
	}

	public void setOldValueForKisiogrenim(String oldValueForKisiogrenim) {
		this.oldValueForKisiogrenim = oldValueForKisiogrenim;
	}

	public KisiogrenimController getKisiogrenimController() {
		return kisiogrenimController;
	}

	public void setKisiogrenimController(KisiogrenimController kisiogrenimController) {
		this.kisiogrenimController = kisiogrenimController;
	}

	public Kurum getPreviousKurumForKurumKisi() {
		return previousKurumForKurumKisi;
	}

	public void setPreviousKurumForKurumKisi(Kurum previousKurumForKurumKisi) {
		this.previousKurumForKurumKisi = previousKurumForKurumKisi;
	}

	public String getOldValueForKurumKisi() {
		return oldValueForKurumKisi;
	}

	public void setOldValueForKurumKisi(String oldValueForKurumKisi) {
		this.oldValueForKurumKisi = oldValueForKurumKisi;
	}

	public KurumKisiController getKurumKisiController() {
		return kurumKisiController;
	}

	public void setKurumKisiController(KurumKisiController kurumKisiController) {
		this.kurumKisiController = kurumKisiController;
	}

	public boolean isUyeYap() {
		return uyeYap;
	}

	public void setUyeYap(boolean uyeYap) {
		this.uyeYap = uyeYap;
	}

	public Uye getKisiUye() {
		return kisiUye;
	}

	public void setKisiUye(Uye kisiUye) {
		this.kisiUye = kisiUye;
	}

	private void kurumKisiGuncelle(Kisi refKisi) {
		this.kurumKisiController.setTable(null);
		this.kurumKisiController.getKurumKisiFilter().setKisiRef(refKisi);
		this.kurumKisiController.getKurumKisiFilter().createQueryCriterias();
		this.kurumKisiController.queryAction();
	}

	@Override
	public BaseFilter getFilter() {
		return getKisiFilter();
	}

	public void kisi() {
		setSelectedTab("kisi");
	}

	public void createKisiDetay(String tabSelected) {
		setSelectedTab(tabSelected);
		getSessionUser().setLegalAccess(1);
		if (tabSelected.equalsIgnoreCase("adres") || tabSelected.equalsIgnoreCase("telefon") || tabSelected.equalsIgnoreCase("internetadres")) {
			// logYaz("adres");
			adresListesiGuncelle(getKisiDetay());
			telefonListesiGuncelle(getKisiDetay());
			internetadresListesiGuncelle(getKisiDetay());
		} else if (tabSelected.equalsIgnoreCase("kisifotograf")) {
			this.kisifotografController.getKisifotografFilter().setKisiRef(getKisiDetay());
			this.kisifotografController.getKisifotografFilter().createQueryCriterias();
			this.kisifotografController.queryAction();
		} else if (tabSelected.equalsIgnoreCase("kisikimlik")) {
			this.kisikimlikController.getKisikimlikFilter().setKisiRef(getKisiDetay());
			this.kisikimlikController.getKisikimlikFilter().createQueryCriterias();
			this.kpsKaydiBulundu = false;
			this.kpsSorguSonucDondu = false;
			this.kisikimlikController.queryAction();
			if (!isEmpty(this.kisikimlikController.getList())) {
				this.kisikimlik = (Kisikimlik) this.kisikimlikController.getList().get(0);
			} else {
				this.kisikimlik = new Kisikimlik();
			}
		} else if (tabSelected.equalsIgnoreCase("personel")) {
			this.personelController.getPersonelFilter().setKisiRef(getKisiDetay());
			this.personelController.getPersonelFilter().createQueryCriterias();
			this.personelController.queryAction();
		} else if (tabSelected.equalsIgnoreCase("uye")) {
			this.uyeController.getUyeFilter().setKisiRef(getKisiDetay());
			this.uyeController.getUyeFilter().createQueryCriterias();
			this.uyeController.queryAction();
		} else if (tabSelected.equalsIgnoreCase("kurumkisi")) {
			kurumKisiGuncelle(getKisiDetay());
		} else if (tabSelected.equalsIgnoreCase("kisiogrenim")) {
			this.kisiogrenimController.getKisiogrenimFilter().setKisiRef(getKisiDetay());
			this.kisiogrenimController.getKisiogrenimFilter().createQueryCriterias();
			this.kisiogrenimController.queryAction();
		}
	}

	@SuppressWarnings("unchecked")
	public void newDetail(String detailType) {
		detailType = detailType.toLowerCase(Locale.ENGLISH);
		this.detailEditType = detailType;
		RequestContext context = RequestContext.getCurrentInstance();
		context.update("dialogUpdateBox");
		context.update("dialogUpdateBox1");
		if (detailType.equalsIgnoreCase("adres")) {
			this.adres = new Adres();
			this.adres.setVarsayilan(EvetHayir._EVET);
			this.adres.setAdresturu(AdresTuru._EVADRESI);
			this.oldValueForAdres = "";
		} else if (detailType.equalsIgnoreCase("telefon")) {
			this.telefon = new Telefon();
			this.telefon.setVarsayilan(EvetHayir._HAYIR);
			this.oldValueForTelefon = "";
		} else if (detailType.equalsIgnoreCase("internetadres")) {
			this.internetadres = new Internetadres();
			this.internetadres.setVarsayilan(EvetHayir._HAYIR);
			this.oldValueForInternetadres = "";
		} else if (detailType.equalsIgnoreCase("kisifotograf")) {
			this.kisifotograf = new Kisifotograf();
			this.oldValueForKisiFotograf = "";
		} else if (detailType.equalsIgnoreCase("kisikimlik")) {
			this.kisikimlik = new Kisikimlik();
			this.oldValueForKisikimlik = "";
		} else if (detailType.equalsIgnoreCase("personel")) {
			this.personel = new Personel();
			this.oldValueForPersonel = "";
		} else if (detailType.equalsIgnoreCase("uye")) {
			if (getSessionUser().getPersonelRef() == null || getSessionUser().getPersonelRef().getBirimRef() == null) {
				return;
			}
			this.uye = new Uye();
			this.uye.setKayitdurum(UyeKayitDurum._INCELEMEDE);
			this.uye.setUyedurum(UyeDurum._AKTIFUYE);
			this.uye.setIletisimEmail(EvetHayir._EVET);
			this.uye.setIletisimPosta(EvetHayir._EVET);
			this.uye.setIletisimSms(EvetHayir._EVET);
			// this.uye.setUyeliktarih(new Date());
			this.uye.setBirimRef(getSessionUser().getPersonelRef().getBirimRef());

			this.oldValueForUye = "";
			List<Kisiogrenim> kisiOgrenimListesi = getDBOperator().load(Kisiogrenim.class.getSimpleName(), "o.kisiRef.rID = " + getKisiDetay().getRID() + " AND o.ogrenimtip =" + OgrenimTip._LISANS.getCode(), "o.rID");
			if (!isEmpty(kisiOgrenimListesi)) {
				this.kisiogrenim = kisiOgrenimListesi.get(0);
			} else {
				this.kisiogrenim = new Kisiogrenim();
			}
			if (getKisiDetay().getKimliktip() == KimlikTip._PASAPORT) {
				this.uye.setUyetip(UyeTip._YABANCILAR);
			}
		} else if (detailType.equalsIgnoreCase("kurumkisi")) {
			this.kurumKisi = new KurumKisi();
			try {
				if (getDBOperator().recordCount(KurumKisi.class.getSimpleName(), "o.kisiRef.rID=" + getKisiDetay().getRID()) > 0) {
					KurumKisi entity = (KurumKisi) getDBOperator().load(KurumKisi.class.getSimpleName(), "o.kisiRef.rID=" + getKisiDetay().getRID(), "o.rID DESC").get(0);
					setPreviousKurumForKurumKisi(entity.getKurumRef());
				}
			} catch (DBException e) {
				logYaz("Exception @" + getModelName() + "Controller :", e);
			}
			this.oldValueForKurumKisi = "";
		} else if (detailType.equalsIgnoreCase("kisiogrenim")) {
			this.kisiogrenim = new Kisiogrenim();
			Calendar cal = Calendar.getInstance();
			int year = cal.get(Calendar.YEAR);
			this.kisiogrenim.setMezuniyettarihi(year);
			this.oldValueForKisiogrenim = "";
		}
	}

	public void newKurum(String detailType) {
		this.detailEditType = detailType;
		this.kurum = new Kurum();
		RequestContext context = RequestContext.getCurrentInstance();
		context.update("dialogUpdateBox");
	}

	public boolean uyeyse(String detailType) {
		detailType = detailType.toLowerCase(Locale.ENGLISH);
		if (detailType.equals("uye")) {
			try {
				if (getKisiDetay() != null) {
					if (getDBOperator().recordCount(Uye.class.getSimpleName(), "o.kisiRef.rID=" + getKisiDetay().getRID()) > 0) {
						return true;
					} else {
						return false;
					}
				}
			} catch (DBException e) {
				logYaz("Exception @" + getModelName() + "Controller :", e);
			}
		}
		return true;
	}

	public boolean uyemi(Kisi k) throws DBException {
		if (k != null) {
			if (getDBOperator().recordCount(Uye.class.getSimpleName(), "o.kisiRef.rID=" + k.getRID()) == 0) {
				return false;
			} else {
				return true;
			}
		}
		return false;
	}

	public boolean personelmi(Kisi k) throws DBException {
		if (k != null) {
			if (getDBOperator().recordCount(Personel.class.getSimpleName(), "o.kisiRef.rID=" + k.getRID()) == 0) {
				return false;
			} else {
				return true;
			}
		}
		return false;
	}

	public void updateDetail(String detailType, Object entity) {
		detailType = detailType.toLowerCase(Locale.ENGLISH);
		this.detailEditType = detailType;
		RequestContext context = RequestContext.getCurrentInstance();
		context.update("dialogUpdateBox");

		if (detailType.equalsIgnoreCase("adres")) {
			this.adres = (Adres) entity;
			if (this.adres.getSehirRef() != null) {
				changeIlce(this.adres.getSehirRef().getRID());
			}
			this.oldValueForAdres = this.adres.getValue();
		} else if (detailType.equalsIgnoreCase("telefon")) {
			this.telefon = (Telefon) entity;
			this.oldValueForTelefon = this.telefon.getValue();
		} else if (detailType.equalsIgnoreCase("internetadres")) {
			this.internetadres = (Internetadres) entity;
			this.oldValueForInternetadresMetni = this.internetadres.getNetadresmetni();
			this.oldValueForInternetadres = this.internetadres.getValue();
		} else if (detailType.equalsIgnoreCase("kisifotograf")) {
			this.kisifotograf = (Kisifotograf) entity;
		} else if (detailType.equalsIgnoreCase("personel")) {
			this.personel = (Personel) entity;
			this.oldValueForPersonel = this.personel.getValue();
		} else if (detailType.equalsIgnoreCase("kisikimlik")) {
			this.kisikimlik = (Kisikimlik) entity;
			this.oldValueForKisikimlik = this.kisikimlik.getValue();
		} else if (detailType.equalsIgnoreCase("uye")) {
			this.uye = (Uye) entity;
			this.oldValueForUye = this.uye.getValue();
			this.kisiogrenim = (Kisiogrenim) entity;
			this.oldValueForKisiogrenim = this.kisiogrenim.getValue();
		} else if (detailType.equalsIgnoreCase("kurumkisi")) {
			this.kurumKisi = (KurumKisi) entity;
			setPreviousKurumForKurumKisi(this.kurumKisi.getKurumRef());
			this.oldValueForKurumKisi = this.kurumKisi.getValue();
		} else if (detailType.equalsIgnoreCase("kisiogrenim")) {
			this.kisiogrenim = (Kisiogrenim) entity;
			this.oldValueForKisiogrenim = this.kisiogrenim.getValue();
		}
	}

	public void deleteDetail(String detailType, Object entity) throws Exception {
		detailType = detailType.toLowerCase(Locale.ENGLISH);
		this.detailEditType = null;
		if (detailType.equalsIgnoreCase("adres")) {
			this.adres = (Adres) entity;
			if (this.adres.getVarsayilan() == EvetHayir._EVET) {
				if (getDBOperator().recordCount(Adres.class.getSimpleName(), "o.rID <> " + this.adres.getRID() + " AND o.kisiRef.rID = " + this.adres.getKisiRef().getRID()) > 0) {
					Adres varsayilanYapilacakAdres = (Adres) getDBOperator().load(Adres.class.getSimpleName(), "o.rID <> " + this.adres.getRID() + " AND o.kisiRef.rID = " + this.adres.getKisiRef().getRID(), "o.rID DESC").get(0);
					varsayilanYapilacakAdres.setVarsayilan(EvetHayir._EVET);
					getDBOperator().update(varsayilanYapilacakAdres);
				}
			}
			super.justDelete(this.adres, this.adresController.isLoggable());
			this.adresController.queryAction();
		} else if (detailType.equalsIgnoreCase("telefon")) {
			this.telefon = (Telefon) entity;
			if (this.telefon.getVarsayilan() == EvetHayir._EVET) {
				if (getDBOperator().recordCount(Telefon.class.getSimpleName(), "o.rID <> " + this.telefon.getRID() + " AND o.kisiRef.rID = " + this.telefon.getKisiRef().getRID()) > 0) {
					Telefon varsayilanYapilacakTelefon = (Telefon) getDBOperator().load(Telefon.class.getSimpleName(), "o.rID <> " + this.telefon.getRID() + " AND o.kisiRef.rID = " + this.telefon.getKisiRef().getRID(), "o.rID DESC").get(0);
					varsayilanYapilacakTelefon.setVarsayilan(EvetHayir._EVET);
					getDBOperator().update(varsayilanYapilacakTelefon);
				} else if (getDBOperator().recordCount(Telefon.class.getSimpleName(), "o.kisiRef.rID = " + this.telefon.getKisiRef().getRID()) == 1) {
					createGenericMessage("Varsayılan telefon numarasını silemezsiniz!", FacesMessage.SEVERITY_INFO);
					return;
				}
			}
			super.justDelete(this.telefon, this.telefonController.isLoggable());
			this.telefonController.queryAction();
		} else if (detailType.equalsIgnoreCase("internetadres")) {
			this.internetadres = (Internetadres) entity;
			if (this.internetadres.getVarsayilan() == EvetHayir._EVET) {
				if (getDBOperator().recordCount(Internetadres.class.getSimpleName(), "o.kisiRef.rID = " + this.internetadres.getKisiRef().getRID()) > 0) {
					Internetadres varsayilanYapilacakInternetadres = (Internetadres) getDBOperator()
							.load(Internetadres.class.getSimpleName(), "o.rID <> " + this.internetadres.getRID() + " AND o.kisiRef.rID = " + this.internetadres.getKisiRef().getRID(), "o.rID DESC").get(0);
					varsayilanYapilacakInternetadres.setVarsayilan(EvetHayir._EVET);
					getDBOperator().update(varsayilanYapilacakInternetadres);
				}
			}
			// todo internet adresinin silindigi kisim
			String silinenMail = this.internetadres.getNetadresmetni();
			logYaz("SILINDI | " + silinenMail);
			epostaLogger.info("SILINDI | " + epostaKisiLog() + silinenMail);
			super.justDelete(this.internetadres, this.internetadresController.isLoggable());
			this.internetadresController.queryAction();
		} else if (detailType.equalsIgnoreCase("kisifotograf")) {
			this.kisifotograf = (Kisifotograf) entity;
			super.justDelete(this.kisifotograf, this.kisifotografController.isLoggable());
			this.kisifotografController.queryAction();
		} else if (detailType.equalsIgnoreCase("kisikimlik")) {
			this.kisikimlik = (Kisikimlik) entity;
			super.justDelete(this.kisikimlik, this.kisikimlikController.isLoggable());
			this.kisikimlikController.queryAction();
		} else if (detailType.equalsIgnoreCase("personel")) {
			this.personel = (Personel) entity;
			super.justDelete(this.personel, this.personelController.isLoggable());
			this.personelController.queryAction();
		} else if (detailType.equalsIgnoreCase("uye")) {
			this.uye = (Uye) entity;
			super.justDelete(this.uye, this.uyeController.isLoggable());
			getUyeController().queryAction();
		} else if (detailType.equalsIgnoreCase("kurumkisi")) {
			this.kurumKisi = (KurumKisi) entity;
			Long kisiRef = 0L;
			if (getKurumKisi().getKisiRef() != null) {
				kisiRef = this.kurumKisi.getKisiRef().getRID();
			}
			try {
				if (getDBOperator().recordCount(KurumKisi.class.getSimpleName(), "o.kurumRef.rID=" + getKurumKisi().getKurumRef().getRID() + " AND o.kisiRef.rID=" + getKurumKisi().getKisiRef().getRID()) > 0) {
					if (getDBOperator().recordCount(Adres.class.getSimpleName(),
							"o.kisiRef.rID=" + getKurumKisi().getKisiRef().getRID() + " AND o.kurumRef.rID=" + getKurumKisi().getKurumRef().getRID() + " AND o.varsayilan= " + EvetHayir._EVET.getCode()) > 0) {
						Adres eskiAdres = (Adres) getDBOperator().load(Adres.class.getSimpleName(),
								"o.kisiRef.rID=" + getKurumKisi().getKisiRef().getRID() + " AND o.kurumRef.rID=" + getKurumKisi().getKurumRef().getRID() + " AND o.varsayilan= " + EvetHayir._EVET.getCode(), "").get(0);
						getDBOperator().delete(eskiAdres);
					}
					super.justDelete(this.kurumKisi, this.kurumKisiController.isLoggable());
				} else {
					createGenericMessage("Varsayılan İşyeri Adresini Silemezsiniz!", FacesMessage.SEVERITY_INFO);
					return;

				}

				setSelection(null);
				setTable(null);
				createGenericMessage(KeyUtil.getMessageValue("kayit.silindi"), FacesMessage.SEVERITY_INFO);
			} catch (Exception e) {
				createGenericMessage(KeyUtil.getMessageValue("islem.hata") + " Mes : " + e.getMessage(), FacesMessage.SEVERITY_ERROR);
				return;
			}
			String whereCondition = "o.varsayilan =" + EvetHayir._EVET.getCode();
			try {
				if (getDBOperator().recordCount(KurumKisi.class.getSimpleName(), whereCondition) == 0) {
					whereCondition = "o.rID <> " + this.kurumKisi.getRID() + " AND o.varsayilan =" + EvetHayir._HAYIR.getCode();
					if (kisiRef != 0) {
						whereCondition += " AND o.kisiRef.rID = " + kisiRef;
					}
					if (getDBOperator().recordCount(KurumKisi.class.getSimpleName(), whereCondition) > 0) {
						KurumKisi kurumkisi1 = (KurumKisi) getDBOperator().load(KurumKisi.class.getSimpleName(), whereCondition, "o.rID DESC").get(0);
						if (kurumkisi1 != null) {
							kurumkisi1.setVarsayilan(EvetHayir._EVET);
							getDBOperator().update(kurumkisi1);
						}
					}
				}
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			}
			this.kurumKisiController.queryAction();
		} else if (detailType.equalsIgnoreCase("kisiogrenim")) {
			this.kisiogrenim = (Kisiogrenim) entity;
			Long kisiRef = 0L;
			if (getKisiogrenim().getKisiRef() != null) {
				kisiRef = this.kisiogrenim.getKisiRef().getRID();
				if (getKisiogrenim().getOgrenimtip() == OgrenimTip._LISANS && getKisiogrenim().getVarsayilan() == EvetHayir._EVET) {
					createGenericMessage("Lisans öğrenim bilgilerini silemezsiniz ! ", FacesMessage.SEVERITY_INFO);
					return;
				}
			}
			try {
				super.justDelete(this.kisiogrenim, this.kisiogrenimController.isLoggable());
				setSelection(null);
				setTable(null);
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
				createGenericMessage(KeyUtil.getMessageValue("islem.hata") + " Mes : " + e.getMessage(), FacesMessage.SEVERITY_ERROR);
				return;
			}

			String whereCondition = "o.varsayilan =" + EvetHayir._EVET.getCode();
			if (kisiRef != 0) {
				whereCondition += " AND o.kisiRef.rID = " + kisiRef;
			}
			try {
				if (getDBOperator().recordCount(Kisiogrenim.class.getSimpleName(), whereCondition) == 0) {
					whereCondition = "o.rID <> " + this.kisiogrenim.getRID() + " AND o.varsayilan =" + EvetHayir._HAYIR.getCode();
					if (kisiRef != 0) {
						whereCondition += " AND o.kisiRef.rID = " + kisiRef;
					}
					if (getDBOperator().recordCount(Kisiogrenim.class.getSimpleName(), whereCondition) > 0) {
						Kisiogrenim kisiogrenim1 = (Kisiogrenim) getDBOperator().load(Kisiogrenim.class.getSimpleName(), whereCondition, "o.rID DESC").get(0);
						if (kisiogrenim1 != null) {
							kisiogrenim1.setVarsayilan(EvetHayir._EVET);
							getDBOperator().update(kisiogrenim1);
						}
					}
				}
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			}
			this.kisiogrenimController.queryAction();
		}
	}

	public String getHeightOfEditDialog() {
		if (this.detailEditType == null) {
			return "400";
		} else if (this.detailEditType.equalsIgnoreCase("adres")) {
			return "600";
		} else if (this.detailEditType.equalsIgnoreCase("telefon")) {
			return "300";
		} else if (this.detailEditType.equalsIgnoreCase("internetadres")) {
			return "300";
		} else if (this.detailEditType.equalsIgnoreCase("kisifotograf")) {
			return "300";
		} else if (this.detailEditType.equalsIgnoreCase("kisikimlik")) {
			return "600";
		} else if (this.detailEditType.equalsIgnoreCase("kisiuyruk")) {
			return "300";
		} else if (this.detailEditType.equalsIgnoreCase("personel")) {
			return "300";
		} else if (this.detailEditType.equalsIgnoreCase("uye")) {
			return "700";
		} else if (this.detailEditType.equalsIgnoreCase("kurumkisi")) {
			return "600";
		} else if (this.detailEditType.equalsIgnoreCase("kisiogrenim")) {
			return "600";
		} else {
			return "400";
		}
	}

	private void updateDialogAndForm(RequestContext context) {
		context.update("dialogUpdateBox");
		context.update("kisiForm");
	}

	public void saveDetails() throws Exception {
		RequestContext context = RequestContext.getCurrentInstance();
		if (this.detailEditType.equalsIgnoreCase("adres")) {
			if (this.adres.getAdrestipi() == AdresTipi._YURTDISIADRESI) {
				if (this.adres.getUlkeRef() == null || isEmpty(this.adres.getAcikAdres()) || this.adres.getSehirRef() == null) {
					createGenericMessage("Yurdışı adresleri için ülke, il ve açık adres alanları zorunludur!", FacesMessage.SEVERITY_ERROR);
					return;
				}
			} else if (this.adres.getAdrestipi() != AdresTipi._YURTDISIADRESI) {
				if (isEmpty(this.adres.getAcikAdres()) || this.adres.getSehirRef() == null || this.adres.getIlceRef() == null) {
					createGenericMessage("Adres için il, ilçe ve açık adres alanları zorunludur!", FacesMessage.SEVERITY_ERROR);
					return;
				} else if (this.adres.getUlkeRef() == null) {
					this.adres.setUlkeRef((Ulke) getDBOperator().find(Ulke.class.getSimpleName(), "o.rID=9980"));
				}
			}
			if (this.adres.getAdresturu() != AdresTuru._NULL) {
				this.adres.setKisiRef(getKisiDetay());
				if (this.adres.getAdresturu() == AdresTuru._EVADRESI) {
					if (getDBOperator().recordCount(Adres.class.getSimpleName(), "o.kisiRef.rID=" + getKisiDetay().getRID() + " AND o.adresturu=" + AdresTuru._EVADRESI.getCode()) > 0) {
						createGenericMessage("Birden fazla ev adresi ekleyemezsiniz!", FacesMessage.SEVERITY_INFO);
						this.detailEditType = null;
						updateDialogAndForm(context);
						return;
					}
				} else if (this.adres.getAdresturu() == AdresTuru._ISADRESI) {
					if (getDBOperator().recordCount(Adres.class.getSimpleName(), "o.kisiRef.rID=" + getKisiDetay().getRID() + " AND o.adresturu=" + AdresTuru._ISADRESI.getCode()) > 0) {
						createGenericMessage("Birden fazla işyeri adresi ekleyemezsiniz!", FacesMessage.SEVERITY_INFO);
						this.detailEditType = null;
						updateDialogAndForm(context);
						return;
					}
				}
				if (this.adres.getVarsayilan() == EvetHayir._HAYIR) {
					if (getDBOperator().recordCount(Adres.class.getSimpleName(), "o.kisiRef.rID = " + getKisiDetay().getRID() + " AND o.varsayilan = " + EvetHayir._EVET.getCode()) == 0) {
						this.adres.setVarsayilan(EvetHayir._EVET);
					}
				}
				super.justSave(this.adres, this.adresController.isLoggable(), this.oldValueForAdres);
				if (this.adres.getVarsayilan() == EvetHayir._EVET) {
					getDBOperator().executeQuery(
							"UPDATE " + Adres.class.getSimpleName() + " o set o.varsayilan = " + EvetHayir._HAYIR.getCode() + " " + "	WHERE o.kisiRef.rID = " + this.adres.getKisiRef().getRID() + " AND o.rID <> " + this.adres.getRID());
				}
				this.adres = new Adres();
				this.adresController.queryAction();
				updateDialogAndForm(context);
			}
		} else if (this.detailEditType.equalsIgnoreCase("telefon")) {
			if (!isEmpty(this.telefon.getTelefonno()) && this.telefon.getTelefonturu() != TelefonTuru._NULL) {
				this.telefon.setKisiRef(getKisiDetay());
				if (this.telefon.getTelefonturu() == TelefonTuru._FAKS) {
					this.telefon.setVarsayilan(EvetHayir._HAYIR);
				}
				if (this.telefon.getVarsayilan() == EvetHayir._HAYIR) {
					if (getDBOperator().recordCount(Telefon.class.getSimpleName(), "o.kisiRef.rID = " + getKisiDetay().getRID() + " AND o.varsayilan = " + EvetHayir._EVET.getCode()) == 0) {
						this.telefon.setVarsayilan(EvetHayir._EVET);
					}
				}
				super.justSave(this.telefon, this.telefonController.isLoggable(), this.oldValueForTelefon);
				if (this.telefon.getVarsayilan() == EvetHayir._EVET) {
					getDBOperator().executeQuery("UPDATE " + Telefon.class.getSimpleName() + " o set o.varsayilan = " + EvetHayir._HAYIR.getCode() + " " + "	WHERE o.kisiRef.rID = " + this.telefon.getKisiRef().getRID() + " AND o.rID <> "
							+ this.telefon.getRID());
				}
				telefonListesiGuncelle(getKisiDetay());
				this.telefon = new Telefon();
				updateDialogAndForm(context);
			}
		} else if (this.detailEditType.equalsIgnoreCase("internetadres")) {
			if (!isEmpty(this.internetadres.getNetadresmetni()) && this.internetadres.getNetadresturu() != NetAdresTuru._NULL) {
				this.internetadres.setKisiRef(getKisiDetay());
				if (this.internetadres.getNetadresturu() != NetAdresTuru._EPOSTA) {
					this.internetadres.setVarsayilan(EvetHayir._HAYIR);
				}
				String mail = null;
				if (this.internetadres.getNetadresturu() == NetAdresTuru._WEBSAYFASI) {
					if (internetadres.getNetadresmetni() != null) {
						mail = internetadres.getNetadresmetni();
						if (mail.contains("www.") == false) {
							createCustomMessage("Geçersiz Web Sayfası !", FacesMessage.SEVERITY_ERROR, "detailEditForm:netadresmetniInputText:netadresmetni");
							return;
						}
					}
				} else {
					if (internetadres.getNetadresmetni() != null) {
						mail = internetadres.getNetadresmetni();
						if (mail.contains("@") == false) {
							createCustomMessage("Geçersiz Eposta Adresi !", FacesMessage.SEVERITY_ERROR, "detailEditForm:netadresmetniInputText:netadresmetni");
							return;
						}
					}
				}
				if (this.internetadres.getVarsayilan() == EvetHayir._HAYIR) {
					if (getDBOperator().recordCount(Internetadres.class.getSimpleName(), "o.kisiRef.rID = " + getKisiDetay().getRID() + " AND o.varsayilan = " + EvetHayir._EVET.getCode()) == 0) {
						this.internetadres.setVarsayilan(EvetHayir._EVET);
					}
				}
				// todo mail eklendigi ve guncellendigi kisim - oldValue atanmamissa eklemedir yoksa guncelleme
				String yeniMail = this.internetadres.getNetadresmetni();
				String eskiMail;
				if (this.oldValueForInternetadresMetni != null) {
					eskiMail = this.oldValueForInternetadresMetni;
					logYaz("GUNCELLENDI | " + epostaKisiLog() + eskiMail + " | " + yeniMail);
					epostaLogger.info("GUNCELLENDI | " + epostaKisiLog() + eskiMail + " | " + yeniMail);
				} else {
					logYaz("EKLENDI | " + epostaKisiLog() + yeniMail);
					epostaLogger.info("EKLENDI | " + epostaKisiLog() + yeniMail);
				}
				super.justSave(this.internetadres, this.internetadresController.isLoggable(), this.oldValueForInternetadres);
				if (this.internetadres.getVarsayilan() == EvetHayir._EVET) {
					getDBOperator().executeQuery("UPDATE " + Internetadres.class.getSimpleName() + " o set o.varsayilan = " + EvetHayir._HAYIR.getCode() + " " + "	WHERE o.kisiRef.rID = " + this.internetadres.getKisiRef().getRID()
							+ " AND o.rID <> " + this.internetadres.getRID());
				}
				this.internetadres = new Internetadres();
				this.internetadresController.queryAction();
				updateDialogAndForm(context);
			}
		} else if (this.detailEditType.equalsIgnoreCase("kisifotograf")) {
			// if(!isEmpty(this.kisifotograf.getPath())){
			if (!isEmpty(this.kisifotograf.getPath())) {
				this.kisifotograf.setVarsayilan(EvetHayir._EVET);
				getDBOperator().executeQuery("UPDATE " + Kisifotograf.class.getSimpleName() + " o set o.varsayilan = " + EvetHayir._HAYIR.getCode() + " " + "	WHERE o.kisiRef.rID = " + getKisiDetay().getRID());
				this.kisifotograf.setKisiRef(getKisiDetay());
				super.justSave(this.kisifotograf, this.kisifotografController.isLoggable(), "");
				this.kisifotograf = new Kisifotograf();
				this.kisifotografController.queryAction();
				updateDialogAndForm(context);
			}
		} else if (this.detailEditType.equalsIgnoreCase("kisikimlik")) {
			this.kisikimlik.setKimlikno(getKisiDetay().getKimlikno());
			// char[] kimlikno = null;
			// if (getKisiDetay().getKimlikno() != null) {
			// for (int x = 0; x <
			// getKisiDetay().getKimlikno().toString().length(); x++) {
			// kimlikno[x] = getKisiDetay().getKimlikno().toString().charAt(x);
			// if (kimlikno[x] == '0' || kimlikno[x] == '1' || kimlikno[x] ==
			// '2' || kimlikno[x] == '3'
			// || kimlikno[x] == '4' || kimlikno[x] == '5' || kimlikno[x] == '6'
			// || kimlikno[x] == '7'
			// || kimlikno[x] == '8' || kimlikno[x] == '9') {
			// createGenericMessage("Geçersiz kimlik numarası !",FacesMessage.SEVERITY_ERROR);
			// }
			// }
			// }
			if (this.kisikimlik.getKimlikno() != null && !isEmpty(this.kisikimlik.getBabaad()) && !isEmpty(this.kisikimlik.getAnaad()) && !isEmpty(this.kisikimlik.getDogumyer()) && this.kisikimlik.getDogumtarih() != null) {
				this.kisikimlik.setKisiRef(getKisiDetay());
				super.justSave(this.kisikimlik, this.kisikimlikController.isLoggable(), "");
				this.kisikimlik = new Kisikimlik();
				this.kisikimlikController.queryAction();
				if (!this.kisikimlikController.getList().isEmpty()) {
					this.kisikimlik = (Kisikimlik) this.kisikimlikController.getList().get(0);
				} else {
					this.kisikimlik = new Kisikimlik();
				}

				updateDialogAndForm(context);

			}
		} else if (this.detailEditType.equalsIgnoreCase("personel")) {
			if (this.personel.getBirimRef() != null && this.personel.getBaslamatarih() != null) {
				this.personel.setKisiRef(getKisiDetay());
				this.personel.setTumUyeSorgulama(EvetHayir._HAYIR);
				super.justSave(this.personel, this.personelController.isLoggable(), "");
				this.personel = new Personel();
				this.personelController.queryAction();
				updateDialogAndForm(context);
			}
		} else if (this.detailEditType.equalsIgnoreCase("uye")) {
			if (this.uye.getUyedurum() != null) {
				// this.uye.setSicilno(null);
				this.uye.setKisiRef(getKisiDetay());
				if (!isEmpty(this.uye.getSicilno())) {
					this.uye.setSicilno(this.uye.getSicilno().trim());
					if (getDBOperator().recordCount(Uye.class.getSimpleName(), "o.sicilno = '" + this.uye.getSicilno().trim() + "'") > 0) {
						createGenericMessage(KeyUtil.getMessageValue("uye.duplikesicil"), FacesMessage.SEVERITY_ERROR);
						// context.execute("alert('Duplike sicil no!');");
						return;
					}
				}
				super.justSave(this.uye, this.uyeController.isLoggable(), "");
				this.uye = new Uye();
				this.uyeController.queryAction();
				updateDialogAndForm(context);
			}
		} else if (this.detailEditType.equalsIgnoreCase("kurumkisi")) {
			this.kurumKisi.setKisiRef(getKisiDetay());
			if (this.kurumKisi.getVarsayilan() == EvetHayir._EVET) {
				String whereCondition = "o.rID <> " + this.kurumKisi.getRID() + " AND o.varsayilan =" + EvetHayir._EVET.getCode();
				if (this.kurumKisi.getKisiRef() != null) {
					whereCondition += " AND o.kisiRef.rID = " + this.kurumKisi.getKisiRef().getRID();
				}
				try {
					if (getDBOperator().recordCount(KurumKisi.class.getSimpleName(), whereCondition) > 0) {
						for (Object obj : getDBOperator().load(KurumKisi.class.getSimpleName(), whereCondition, "o.rID")) {
							KurumKisi uyeKurum = (KurumKisi) obj;
							uyeKurum.setVarsayilan(EvetHayir._HAYIR);
							getDBOperator().update(uyeKurum);
						}
					}
				} catch (Exception e) {
					logYaz("Exception @" + getModelName() + "Controller :", e);
				}
			} else if (this.kurumKisi.getVarsayilan() == EvetHayir._HAYIR) {
				String whereCondition = "o.rID <> " + this.kurumKisi.getRID() + " AND o.varsayilan =" + EvetHayir._HAYIR.getCode();
				if (this.kurumKisi.getKisiRef() != null) {
					whereCondition += " AND o.kisiRef.rID = " + this.kurumKisi.getKisiRef().getRID();
				}
				String whereCondition1 = "o.rID <> " + this.kurumKisi.getRID() + " AND o.varsayilan =" + EvetHayir._EVET.getCode();
				if (this.kurumKisi.getKisiRef() != null) {
					whereCondition += " AND o.kisiRef.rID = " + this.kurumKisi.getKisiRef().getRID();
				}
				try {
					if (getDBOperator().recordCount(KurumKisi.class.getSimpleName(), whereCondition) == 0 && getDBOperator().recordCount(KurumKisi.class.getSimpleName(), whereCondition1) == 0) {
						kurumKisi.setVarsayilan(EvetHayir._EVET);
						getDBOperator().update(kurumKisi);
					}
					whereCondition = "o.rID <> " + this.kurumKisi.getRID() + " AND o.varsayilan =" + EvetHayir._EVET.getCode();
					if (this.kurumKisi.getKisiRef() != null) {
						whereCondition += " AND o.kisiRef.rID = " + this.kurumKisi.getKisiRef().getRID();
					}
					if (getDBOperator().recordCount(KurumKisi.class.getSimpleName(), whereCondition) == 0) {
						createGenericMessage(KeyUtil.getMessageValue("varsayilan.hata"), FacesMessage.SEVERITY_INFO);
						kurumKisi.setVarsayilan(EvetHayir._EVET);
						getDBOperator().update(kurumKisi);
					}

				} catch (Exception e) {
					logYaz("Exception @" + getModelName() + "Controller :", e);
				}
			}
			this.kurumKisi.setKisiRef(getKisiDetay());
			if (this.kurumKisi.getKurumRef() == null) {
				this.kurumKisi.setKurumRef(getPreviousKurumForKurumKisi());
			}
			if (this.kurumKisi.getUyekurumDurum().getCode() == UyekurumDurum._CALISIYOR.getCode()) {
				this.kurumKisi.setBitistarih(null);
			}
			super.justSave(this.kurumKisi, this.kurumKisiController.isLoggable(), this.oldValueForKurumKisi);
			KurumKisi kurumKisi = new KurumKisi();
			if (this.kurumKisi.getKisiRef() != null) {
				if (getDBOperator().recordCount(KurumKisi.class.getSimpleName(), "o.kurumRef.rID=" + getKurumKisi().getKurumRef().getRID() + " AND o.kisiRef.rID=" + this.kurumKisi.getKisiRef().getRID()) <= 0
						&& this.kurumKisi.getUyekurumDurum().getCode() == UyekurumDurum._CALISIYOR.getCode()) {
					kurumKisi.setKisiRef(this.kurumKisi.getKisiRef());
					kurumKisi.setKurumRef(getKurumKisi().getKurumRef());
					kurumKisi.setGecerli(EvetHayir._EVET);
					getDBOperator().insert(kurumKisi);
				} else if (getDBOperator().recordCount(KurumKisi.class.getSimpleName(), "o.kurumRef.rID=" + getKurumKisi().getKurumRef().getRID() + " AND o.kisiRef.rID=" + this.kurumKisi.getKisiRef().getRID()) > 0) {
					kurumKisi = (KurumKisi) getDBOperator().load(KurumKisi.class.getSimpleName(), "o.kurumRef.rID=" + getKurumKisi().getKurumRef().getRID() + " AND o.kisiRef.rID=" + this.kurumKisi.getKisiRef().getRID(), "").get(0);
					if (this.kurumKisi.getUyekurumDurum().getCode() == UyekurumDurum._AYRILMIS.getCode()) {
						getDBOperator().update(kurumKisi);
					}
				}
			}
			if (getPreviousKurumForKurumKisi() != null && getKurumKisi() != null) {
				if (getPreviousKurumForKurumKisi().getRID() != getKurumKisi().getKurumRef().getRID()) {
					if (getDBOperator().recordCount(Adres.class.getSimpleName(), "o.kisiRef.rID =" + getKurumKisi().getKisiRef().getRID() + " AND o.adresturu=" + AdresTuru._ISADRESI.getCode()) > 0) {
						Adres eskiadres = (Adres) getDBOperator().load(Adres.class.getSimpleName(), "o.kisiRef.rID =" + getKurumKisi().getKisiRef().getRID() + " AND o.adresturu=" + AdresTuru._ISADRESI.getCode(), "").get(0);
						getDBOperator().delete(eskiadres);

					}
					if (getDBOperator().recordCount(Adres.class.getSimpleName(), "o.kurumRef.rID=" + getKurumKisi().getKurumRef().getRID() + " AND o.varsayilan=" + EvetHayir._EVET.getCode()) > 0) {
						Adres kurumAdres = (Adres) getDBOperator().load(Adres.class.getSimpleName(), "o.kurumRef.rID=" + getKurumKisi().getKurumRef().getRID() + " AND o.varsayilan=" + EvetHayir._EVET.getCode(), "").get(0);
						adres = new Adres();
						adres = (Adres) kurumAdres.cloneObject();
						adres.setRID(null);
						adres.setKisiRef(getKurumKisi().getKisiRef());
						getDBOperator().insert(adres);
						if (getDBOperator().recordCount(Adres.class.getSimpleName(),
								"o.kisiRef.rID=" + getKurumKisi().getKisiRef().getRID() + " AND ( o.adresturu=" + AdresTuru._EVADRESI.getCode() + " OR o.adresturu=" + AdresTuru._KPSADRESI.getCode() + " ) ") > 0) {
							@SuppressWarnings("unchecked")
							List<Adres> tumadresleri = getDBOperator().load(Adres.class.getSimpleName(),
									"o.kisiRef.rID=" + getKurumKisi().getKisiRef().getRID() + " AND ( o.adresturu=" + AdresTuru._EVADRESI.getCode() + " OR o.adresturu=" + AdresTuru._KPSADRESI.getCode() + " ) ", "");
							for (Adres entity : tumadresleri) {
								entity.setVarsayilan(EvetHayir._HAYIR);
								getDBOperator().update(entity);
							}
						}
					}
				}
			} else if (getKurumKisi() != null) {
				if (getDBOperator().recordCount(Adres.class.getSimpleName(), "o.kurumRef.rID=" + getKurumKisi().getKurumRef().getRID() + " AND o.varsayilan=" + EvetHayir._EVET.getCode()) > 0) {
					Adres kurumAdres = (Adres) getDBOperator().load(Adres.class.getSimpleName(), "o.kurumRef.rID=" + getKurumKisi().getKurumRef().getRID() + " AND o.varsayilan=" + EvetHayir._EVET.getCode(), "").get(0);
					adres = new Adres();
					adres = (Adres) kurumAdres.cloneObject();
					adres.setRID(null);
					adres.setVarsayilan(EvetHayir._EVET);
					adres.setKisiRef(getKurumKisi().getKisiRef());
					getDBOperator().insert(adres);
					if (getDBOperator().recordCount(Adres.class.getSimpleName(),
							"o.kisiRef.rID=" + getKurumKisi().getKisiRef().getRID() + " AND ( o.adresturu=" + AdresTuru._EVADRESI.getCode() + " OR o.adresturu=" + AdresTuru._KPSADRESI.getCode() + " ) ") > 0) {
						@SuppressWarnings("unchecked")
						List<Adres> tumadresleri = getDBOperator().load(Adres.class.getSimpleName(),
								"o.kisiRef.rID=" + getKurumKisi().getKisiRef().getRID() + " AND ( o.adresturu=" + AdresTuru._EVADRESI.getCode() + " OR o.adresturu=" + AdresTuru._KPSADRESI.getCode() + " ) ", "");
						for (Adres entity : tumadresleri) {
							entity.setVarsayilan(EvetHayir._HAYIR);
							getDBOperator().update(entity);
						}
					}
				}
			}
			this.kurumKisi = new KurumKisi();
			this.kurumKisiController.queryAction();
			updateDialogAndForm(context);
		} else if (this.detailEditType.equalsIgnoreCase("kisiogrenim")) {
			this.kisiogrenim.setKisiRef(getKisiDetay());
			super.justSave(this.kisiogrenim, this.kisiogrenimController.isLoggable(), this.oldValueForKisiogrenim);
			if (this.kisiogrenim.getVarsayilan() == EvetHayir._EVET) {
				String whereCondition = "o.rID <> " + this.kisiogrenim.getRID() + " AND o.varsayilan =" + EvetHayir._EVET.getCode();
				if (this.kisiogrenim.getKisiRef() != null) {
					whereCondition += " AND o.kisiRef.rID = " + this.kisiogrenim.getKisiRef().getRID();
				}
				try {
					if (getDBOperator().recordCount(Kisiogrenim.class.getSimpleName(), whereCondition) > 0) {
						for (Object obj : getDBOperator().load(Kisiogrenim.class.getSimpleName(), whereCondition, "o.rID")) {
							Kisiogrenim kisiogrenim = (Kisiogrenim) obj;
							kisiogrenim.setVarsayilan(EvetHayir._HAYIR);
							getDBOperator().update(kisiogrenim);
						}
					}
				} catch (Exception e) {
					logYaz("Exception @" + getModelName() + "Controller :", e);
				}
			} else if (this.kisiogrenim.getVarsayilan() == EvetHayir._HAYIR) {
				String whereCondition = "o.rID <> " + this.kisiogrenim.getRID() + " AND o.varsayilan =" + EvetHayir._HAYIR.getCode();
				if (this.kisiogrenim.getKisiRef() != null) {
					whereCondition += " AND o.kisiRef.rID = " + this.kisiogrenim.getKisiRef().getRID();
				}
				String whereCondition1 = "o.rID <> " + this.kisiogrenim.getRID() + " AND o.varsayilan =" + EvetHayir._EVET.getCode();
				if (this.kisiogrenim.getKisiRef() != null) {
					whereCondition += " AND o.kisiRef.rID = " + this.kisiogrenim.getKisiRef().getRID();
				}
				try {
					if (getDBOperator().recordCount(Kisiogrenim.class.getSimpleName(), whereCondition) == 0 && getDBOperator().recordCount(Kisiogrenim.class.getSimpleName(), whereCondition1) == 0) {
						kisiogrenim.setVarsayilan(EvetHayir._EVET);
						getDBOperator().update(kisiogrenim);
					}
					whereCondition = "o.rID <> " + this.kisiogrenim.getRID() + " AND o.varsayilan =" + EvetHayir._EVET.getCode();
					if (this.kisiogrenim.getKisiRef() != null) {
						whereCondition += " AND o.kisiRef.rID = " + this.kisiogrenim.getKisiRef().getRID();
					}
					if (getDBOperator().recordCount(Kisiogrenim.class.getSimpleName(), whereCondition) == 0) {
						createGenericMessage(KeyUtil.getMessageValue("varsayilan.hata"), FacesMessage.SEVERITY_INFO);
						kisiogrenim.setVarsayilan(EvetHayir._EVET);
						getDBOperator().update(kisiogrenim);
					}

				} catch (Exception e) {
					logYaz("Exception @" + getModelName() + "Controller :", e);
				}
			}
			this.kisiogrenimController.queryAction();
			updateDialogAndForm(context);
		}
		context.update("kisiPanel");
		this.detailEditType = null;
	}

	private String epostaKisiLog() {
		StringBuilder builder = new StringBuilder();
		builder.append(kisiDetay.getKimlikno());
		builder.append(" | ");
		builder.append(kisiDetay.getAd());
		builder.append(" ");
		builder.append(kisiDetay.getSoyad());
		builder.append(" | ");
		return builder.toString();
	}

	@Override
	public void fileUploadListener(FileUploadEvent event) throws Exception {
		File creationFile = super.createFile(event.getFile(), Kisifotograf.class.getSimpleName());
		this.kisifotograf.setPath(creationFile.getName());
		saveDetails();
	}

	public String wizardPage() {
		getSessionUser().setLegalAccess(1);
		return "kisi_Wizard.do";
	}

	public void saveKurum() throws DBException {
		getDBOperator().insert(this.kurum);
		this.kurumKisi.setKurumRef(this.kurum);
		RequestContext context = RequestContext.getCurrentInstance();
		this.detailEditType = null;
		context.update("dialogUpdateBox");
		queryAction();
		context.update("kurumEditPanel");
		context.update("kisiForm");
	}

	private void prepareForWizard() {
		this.wizardStep = 1;
		super.insert();
		getKisi().setKimliknotip(KimlikTip._TCKIMLIKNO);
		this.kisi.setAktif(EvetHayir._EVET);
		this.kisikimlik = new Kisikimlik();
		this.kisifotograf = new Kisifotograf();
		this.kurumKisi = new KurumKisi();
		this.isyeriadresi = new Adres();
		this.kisiogrenim = new Kisiogrenim();
		this.kisiogrenim.setOgrenimtip(OgrenimTip._LISANS);
		this.kisiogrenim.setDenklikdurum(EvetHayir._HAYIR);
		Calendar now = Calendar.getInstance();
		int year = now.get(Calendar.YEAR);
		this.kisiogrenim.setMezuniyettarihi(year);
		this.kurum = new Kurum();
		this.adres = new Adres();
		this.internetadres = new Internetadres();
		this.telefon = new Telefon();
		this.kpsKaydiBulundu = false;
		this.kpsSorguSonucDondu = false;
		this.kpsAdres = new Adres();
		clearOldValues();
	}

	private void clearOldValues() {
		this.oldValueForAdres = "";
		this.oldValueForInternetadres = "";
		this.oldValueForKisi = "";
		this.oldValueForKisikimlik = "";
		this.oldValueForPersonel = "";
		this.oldValueForTelefon = "";
		this.oldValueForUye = "";
		this.oldValueForKisiFotograf = "";
		this.oldValueForKisiogrenim = "";
	}

	public void handleChangeForKisi(AjaxBehaviorEvent event) {
		try {
			if (event.getComponent() instanceof HtmlSelectOneMenu) {
				HtmlSelectOneMenu menu = (HtmlSelectOneMenu) event.getComponent();
				if (menu.getValue() instanceof Sehir) {
					changeIlceForUyeIsyeri(((Sehir) menu.getValue()).getRID());
				}
			} else if (event.getComponent() instanceof HtmlSelectBooleanCheckbox) {
				HtmlSelectBooleanCheckbox menu = (HtmlSelectBooleanCheckbox) event.getComponent();
				if (menu.getClientId().contains("eski")) {
					if ((Boolean) menu.getValue() == true) {
						CustomDBOperator customDBOperator = new CustomDBOperator();
						this.zorunlu = true;
						getKisi().setYabanciMi(false);
						getKisi().setKimlikno(Long.parseLong(customDBOperator.getMaxSicilNo()));
					} else {
						this.zorunlu = false;
						getKisi().setKimlikno(null);
					}
				} else if (menu.getClientId().contains("yabanci")) {
					if ((Boolean) menu.getValue() == true) {
						this.zorunlu = true;
						getKisi().setEskiKayitMi(false);
						getKisi().setKimliknotip(KimlikTip._PASAPORT);
						getKisi().setKimlikno(null);
					} else {
						this.zorunlu = false;
						getKisi().setKimlikno(null);
					}
				}
			}
		} catch (Exception e) {
			logYaz("Exception @handleChangeForKisi of " + getModelName() + "Controller :", e);
		}
	}

	private SelectItem[] ilItemListForIsyeriAdres;
	private SelectItem[] ilceItemListForIsyeriAdres;

	public SelectItem[] getIlItemListForIsyeriAdres() {
		return ilItemListForIsyeriAdres;
	}

	public void setIlItemListForIsyeriAdres(SelectItem[] ilItemListForIsyeriAdres) {
		this.ilItemListForIsyeriAdres = ilItemListForIsyeriAdres;
	}

	public SelectItem[] getIlceItemListForIsyeriAdres() {
		return ilceItemListForIsyeriAdres;
	}

	public void setIlceItemListForIsyeriAdres(SelectItem[] ilceItemListForIsyeriAdres) {
		this.ilceItemListForIsyeriAdres = ilceItemListForIsyeriAdres;
	}

	protected void changeIlForUyeIsyeri(Long selectedRecId) {
		if (selectedRecId.longValue() == 0) {
			setIlItemListForIsyeriAdres(new SelectItem[0]);
		} else {
			@SuppressWarnings("unchecked")
			List<Sehir> entityList = getDBOperator().load(Sehir.class.getSimpleName(), "", "o.ad");
			if (entityList != null) {
				ilItemListForIsyeriAdres = new SelectItem[entityList.size() + 1];
				ilItemListForIsyeriAdres[0] = new SelectItem(null, "");
				int i = 1;
				for (Sehir entity : entityList) {
					ilItemListForIsyeriAdres[i++] = new SelectItem(entity, entity.getUIString());
				}
			} else {
				setIlItemListForIsyeriAdres(new SelectItem[0]);
			}
		}
	}

	protected void changeIlceForUyeIsyeri(Long selectedRecId) {
		if (selectedRecId.longValue() == 0) {
			setIlceItemListForIsyeriAdres(new SelectItem[0]);
		} else {
			@SuppressWarnings("unchecked")
			List<Ilce> entityList = getDBOperator().load(Ilce.class.getSimpleName(), "o.sehirRef.rID=" + selectedRecId, "o.ad");
			if (entityList != null) {
				ilceItemListForIsyeriAdres = new SelectItem[entityList.size() + 1];
				ilceItemListForIsyeriAdres[0] = new SelectItem(null, "");
				int i = 1;
				for (Ilce entity : entityList) {
					ilceItemListForIsyeriAdres[i++] = new SelectItem(entity, entity.getUIString());
				}
			} else {
				setIlceItemListForIsyeriAdres(new SelectItem[0]);
			}
		}
	}

	public void saveAndForward() throws DBException {
		try {
			if (this.wizardStep == 1) {
				if (isUyeYap()) {
					this.kisiUye = new Uye();
					this.kisiUye.setKisiRef(this.kisi);
					if (this.kisi.isYabanciMi()) {
						this.kisiUye.setUyetip(UyeTip._YABANCILAR);
					}
				}
				if (this.kisi.getRID() == null) {
					if (getDBOperator().recordCount(Kisi.class.getSimpleName(), "o.kimlikno='" + this.kisi.getKimlikno() + "'") > 0) {
						createGenericMessage(KeyUtil.getMessageValue("kisi.duplikeSicil"), FacesMessage.SEVERITY_INFO);
						return;
					}
					if (!this.kisi.isYabanciMi()) {
						if (this.kisikimlik.getIlceRef() == null) {
							createCustomMessage(KeyUtil.getMessageValue("javax.faces.component.UIInput.REQUIRED"), FacesMessage.SEVERITY_ERROR, "ilceRef1DropDown:ilceRef");
							return;
						}
						if (isEmpty(this.kisikimlik.getCiltno())) {
							createCustomMessage(KeyUtil.getMessageValue("javax.faces.component.UIInput.REQUIRED"), FacesMessage.SEVERITY_ERROR, "ciltnoInputText:ciltno");
							return;
						}
						if (isEmpty(this.kisikimlik.getAileno())) {
							createCustomMessage(KeyUtil.getMessageValue("javax.faces.component.UIInput.REQUIRED"), FacesMessage.SEVERITY_ERROR, "ailenoInputText:aileno");
							return;
						}
						if (isEmpty(this.kisikimlik.getSirano())) {
							createCustomMessage(KeyUtil.getMessageValue("javax.faces.component.UIInput.REQUIRED"), FacesMessage.SEVERITY_ERROR, "siranoInputText:sirano");
							return;
						}
					}
					getDBOperator().insert(this.kisi);
					this.oldValueForKisi = this.kisi.getValue();
					if (isLoggable()) {
						logKaydet(this.kisi, IslemTuru._EKLEME, "");
					}
					this.kisikimlik.setKisiRef(this.kisi);
					getDBOperator().insert(kisikimlik);
					if (this.kisikimlikController.isLoggable()) {
						logKaydet(this.kisikimlik, IslemTuru._EKLEME, "");
						this.oldValueForKisikimlik = this.kisikimlik.getValue();
					}
					createGenericMessage(KeyUtil.getMessageValue("kayit.eklendi"), FacesMessage.SEVERITY_INFO);
				} else {
					if (this.oldValueForKisi.hashCode() != this.kisi.getValue().hashCode()) {
						getDBOperator().update(this.kisi);
						if (isLoggable()) {
							Islemlog islemLog = (Islemlog) getDBOperator().load(Islemlog.class.getSimpleName(), "o.tabloadi='" + Kisi.class.getSimpleName() + "' AND o.referansRID=" + this.kisi.getRID(), "o.rID").get(0);
							islemLog.setYenideger(this.kisi.getValue());
							getDBOperator().update(islemLog);
							this.oldValueForKisi = this.kisi.getValue();
						}
					} else {
						createGenericMessage(KeyUtil.getMessageValue("kisi.degisiklikYok"), FacesMessage.SEVERITY_INFO);
					}
					if (this.oldValueForKisikimlik.hashCode() != this.kisikimlik.getValue().hashCode()) {
						getDBOperator().update(kisikimlik);
						if (this.kisikimlikController.isLoggable()) {
							Islemlog islemLog = (Islemlog) getDBOperator().load(Islemlog.class.getSimpleName(), "o.tabloadi='" + Kisikimlik.class.getSimpleName() + "' AND o.referansRID=" + this.kisikimlik.getRID(), "o.rID").get(0);
							islemLog.setYenideger(this.kisikimlik.getValue());
							getDBOperator().update(islemLog);
							this.oldValueForKisikimlik = this.kisikimlik.getValue();
						}
						createGenericMessage(KeyUtil.getMessageValue("kayit.guncellendi"), FacesMessage.SEVERITY_INFO);
					} else {
						createGenericMessage(KeyUtil.getMessageValue("kisikimlik.degisiklikYok"), FacesMessage.SEVERITY_INFO);
					}
				}
				if (kpsAdres.getKisiRef() != null) {
					this.kpsAdres.setKisiRef(getKisi());
					getDBOperator().insert(this.kpsAdres);
					if (this.kpsDigerAdres.getSehirRef() != null) {
						this.kpsDigerAdres.setKisiRef(getKisi());
						getDBOperator().insert(this.kpsDigerAdres);
					}
				}

				if (getKpsAdres() != null && !isEmpty(getKpsAdres().getAcikAdres())) {
					setAdres1((Adres) kpsAdres.cloneObject());
					getAdres1().setRID(null);
					getAdres1().setAdresturu(AdresTuru._KPSADRESI);
					getAdres1().setUlkeRef((Ulke) getDBOperator().find(Ulke.class.getSimpleName(), "o.ulkekod = '9980'"));
					if (getAdres1().getUlkeRef() != null) {
						changeIl(getAdres1().getUlkeRef().getRID());
						if (this.kpsAdres.getSehirRef() != null) {
							getAdres1().setSehirRef(this.kpsAdres.getSehirRef());
							if (getAdres1().getSehirRef() != null) {
								changeIlce(getAdres1().getSehirRef().getRID());
								getAdres1().setIlceRef(this.kpsAdres.getIlceRef());
							}
						}
					}
				}
				try {
					if (this.adres1 != null) {
						if (this.adres1.getAdrestipi() == AdresTipi._YURTDISIADRESI) {

							if (this.adres1.getUlkeRef() != null && !isEmpty(this.adres1.getAcikAdres()) && this.adres1.getSehirRef() != null) {
								this.adres1.setKisiRef(getKisi());
								this.adres1.setVarsayilan(EvetHayir._EVET);
								getDBOperator().insert(this.adres1);
							}
						} else if (this.adres1.getAdrestipi() != AdresTipi._YURTDISIADRESI) {
							if (getDBOperator().recordCount(Ulke.class.getSimpleName(), "o.ulkekod='9980'") > 0) {
								Ulke ulke = (Ulke) getDBOperator().load(Ulke.class.getSimpleName(), "o.ulkekod='9980'", "o.rID").get(0);
								this.adres1.setUlkeRef(ulke);
							}
							if (this.adres1.getUlkeRef() != null && !isEmpty(this.adres1.getAcikAdres()) && this.adres1.getSehirRef() != null && this.adres1.getIlceRef() != null && !isEmpty(this.adres1.getMahalle())) {
								this.adres1.setKisiRef(getKisi());
								this.adres1.setVarsayilan(EvetHayir._EVET);
								getDBOperator().insert(this.adres1);
							}
						}
					}
					if (getAdres1() != null) {
						setAdres((Adres) getAdres1().cloneObject());
						getAdres().setRID(null);
						getAdres().setAdresturu(AdresTuru._EVADRESI);
						getAdres().setUlkeRef((Ulke) getDBOperator().find(Ulke.class.getSimpleName(), "o.ulkekod = '9980'"));
						if (getAdres1().getUlkeRef() != null) {
							changeIl(getAdres1().getUlkeRef().getRID());
							if (this.kpsAdres.getSehirRef() != null) {
								getAdres1().setSehirRef(this.kpsAdres.getSehirRef());
								if (getAdres1().getSehirRef() != null) {
									changeIlce(getAdres1().getSehirRef().getRID());
									getAdres1().setIlceRef(this.kpsAdres.getIlceRef());
								}
							}
						}
					}
				} catch (DBException e) {
					logYaz("Exception @" + getModelName() + "Controller :", e);
				}
				adresListesiGuncelle(this.kisi);
				telefonListesiGuncelle(this.kisi);
				internetadresListesiGuncelle(this.kisi);
				wizardStep++;
			}

		} catch (Exception e) {
			logYaz("Exception @" + getModelName() + "Controller :", e);
			createGenericMessage(KeyUtil.getMessageValue("islem.hata"), FacesMessage.SEVERITY_ERROR);
		}
	}

	private void adresListesiGuncelle(Kisi refKisi) {
		this.adresController.setTable(null);
		this.adresController.getAdresFilter().setKisiRef(refKisi);
		this.adresController.getAdresFilter().createQueryCriterias();
		this.adresController.queryAction();
	}

	private void telefonListesiGuncelle(Kisi refKisi) {
		this.telefonController.setTable(null);
		this.telefonController.getTelefonFilter().setKisiRef(refKisi);
		this.telefonController.getTelefonFilter().createQueryCriterias();
		this.telefonController.queryAction();
	}

	private void internetadresListesiGuncelle(Kisi refKisi) {
		this.internetadresController.setTable(null);
		this.internetadresController.getInternetadresFilter().setKisiRef(refKisi);
		this.internetadresController.getInternetadresFilter().createQueryCriterias();
		this.internetadresController.queryAction();
	}

	private void kisifotografListesiGuncelle(Kisi refKisi) {
		this.kisifotografController.setTable(null);
		this.kisifotografController.getKisifotografFilter().setKisiRef(refKisi);
		this.kisifotografController.getKisifotografFilter().createQueryCriterias();
		this.kisifotografController.queryAction();
	}

	private Boolean zorunlu = false;

	public Boolean getZorunlu() {
		return zorunlu;
	}

	public void setZorunlu(Boolean zorunlu) {
		this.zorunlu = zorunlu;
	}

	private Boolean kpsZorla = false;

	public Boolean getKpsZorla() {
		return kpsZorla;
	}

	public void setKpsZorla(Boolean kpsZorla) {
		this.kpsZorla = kpsZorla;
	}

	public void kimlikBilgisiSorgula() throws Exception {
		// logYaz("kimlikBilgisiSorgula calisti...");
		this.kpsKaydiBulundu = false;
		this.kpsSorguSonucDondu = false;
		String kpsKisi = null;
		if (getDBOperator().recordCount(Kisi.class.getSimpleName(), "o.kimlikno = '" + this.kisi.getKimlikno() + "'") > 0) {
			// logYaz("Kisi sistemde var...");
			setSelectedRID(((Kisi) getDBOperator().load(Kisi.class.getSimpleName(), "o.kimlikno = '" + this.kisi.getKimlikno() + "'", "o.rID").get(0)).getRID() + "");
			createCustomMessage("Bu kisi sistemde tanımlı!", FacesMessage.SEVERITY_ERROR, "kimliknoInputTextKisi:kimliknoKisi");
			return;
		} else if (getKisi().isEskiKayitMi() || getKisi().isYabanciMi()) {
			// logYaz("Eski kullanici...");
			if (isEmpty(getKisi().getAd()) || isEmpty(getKisi().getSoyad())) {
				createCustomMessage("Ad-soyad girilmelidir!", FacesMessage.SEVERITY_ERROR, "adInputText:ad");
				createCustomMessage("Ad-soyad girilmelidir!", FacesMessage.SEVERITY_ERROR, "soyadInputText:soyad");
				return;
			}
			this.kpsSorguSonucDondu = true;
			this.kpsKaydiBulundu = false;
			this.kpsZorla = true;
			this.kisikimlik.setKimlikno(this.kisi.getKimlikno());
			this.kisi.setAktif(EvetHayir._EVET);
			this.kisi.setIletisimtercih(EvetHayir._EVET);
			this.kisi.setKimliktip(KimlikTip._TCKIMLIKNO);
			getAdres().setAdresturu(AdresTuru._EVADRESI);
		} else {
			// logYaz("kps klciAdi " + ManagedBeanLocator.locateSessionController().getKpsKullaniciAdi());
			// logYaz("kps sifre " + ManagedBeanLocator.locateSessionController().getKpsSifre());
			// logYaz("this.kisi.getKimlikno()" + this.kisi.getKimlikno());
			boolean hataOlustu = false;
			try {
				KPSService kpsService = KPSService.getInstance();
				kpsKisi = kpsService.kisiKimlikBilgisiGetirYeni(this.kisi.getKimlikno());
			} catch (Exception e) {
				hataOlustu = true;
				logYaz("ERROR @kimlikBilgisiSorgula of KisiController : " + e.getMessage(), e);
			}
			if (hataOlustu || kpsKisi == null) {
				createGenericMessage("KPS sorgulamasında bir hata meydana geldi!\nLütfen sistem yöneticilerine konu hakkında bilgi veriniz!", FacesMessage.SEVERITY_ERROR);
				return;
			}

			// logYaz("Kisi kps'de var...");
			this.selectedRID = null;
			this.zorunlu = true;
			PojoKisi kisikimlikPojo = ToysXMLReader.readXmlStringForKimlik(kpsKisi.trim());
			this.kpsSorguSonucDondu = true;
			SistemParametre sistemParametre = new SistemParametre();
			if (getDBOperator().recordCount(SistemParametre.class.getSimpleName(), "") > 0) {
				sistemParametre = (SistemParametre) getDBOperator().load(SistemParametre.class.getSimpleName(), "", "o.rID").get(0);
			}

			if (!isEmpty(kisikimlikPojo.getHataBilgisiKodu()) && sistemParametre != null) {
				if (sistemParametre.getKpszorla() == EvetHayir._HAYIR) {
					createCustomMessage("Girdiğiniz kimlik numarası KPS sisteminde bulunamadı!", FacesMessage.SEVERITY_ERROR, "kimliknoInputTextKisi:kimliknoKisi");
					this.kisikimlik.setKimlikno(this.kisi.getKimlikno());
					setKpsZorla(true);
					return;
				} else {
					setKpsZorla(false);
					createGenericMessage("Girdiğiniz kimlik numarası KPS sisteminde bulunamadı! \nSistem Yönetimi KPS'de bulunmayan kimlik numarasıyla giriş yapılmasını engelledi.Bu yüzden kişiyi ekleyemezsiniz!",
							FacesMessage.SEVERITY_ERROR);
					return;
				}
			}
			this.kpsKaydiBulundu = true;
			this.kisi.setAd(kisikimlikPojo.getAd());
			this.kisi.setSoyad(kisikimlikPojo.getSoyad());
			if (isNumber(kisikimlikPojo.getCinsiyetKod())) {
				this.kisikimlik.setCinsiyet(Cinsiyet.getWithCode(Integer.parseInt(kisikimlikPojo.getCinsiyetKod())));
			}
			this.kisikimlik.setKimlikno(kisikimlikPojo.getTcKimlikNo());
			this.kisikimlik.setBabaad(kisikimlikPojo.getBabaAdi());
			this.kisikimlik.setAnaad(kisikimlikPojo.getAnaAdi());
			if (isNumber(kisikimlikPojo.getMedeniHalKodu())) {
				this.kisikimlik.setMedenihal(MedeniHal.getWithCode(Integer.parseInt(kisikimlikPojo.getMedeniHalKodu())));
			}
			this.kisikimlik.setDogumyer(kisikimlikPojo.getDogumYeri());
			if (kisikimlikPojo.getDogumTarihi() != null && !kisikimlikPojo.getDogumTarihi().toString().equalsIgnoreCase("null/null/null")) {
				this.kisikimlik.setDogumtarih(DateUtil.createDateObject(kisikimlikPojo.getDogumTarihi()));
			}
			if (kisikimlikPojo.getOlumTarihi() != null && !kisikimlikPojo.getOlumTarihi().toString().equalsIgnoreCase("null/null/null")) {
				this.kisikimlik.setOlumtarih(DateUtil.createDateObject(kisikimlikPojo.getOlumTarihi()));
			}
			if (isNumber(kisikimlikPojo.getDurumKod())) {
				this.kisikimlik.setKisidurum(KisiDurum.getWithCode(Integer.parseInt(kisikimlikPojo.getDurumKod())));
			}
			String plakakodu = kisikimlikPojo.getNufusaKayitliOlduguIlKodu();
			if (getDBOperator().recordCount(Sehir.class.getSimpleName(), "o.ilKodu='" + plakakodu + "'") > 0) {
				this.kisikimlik.setSehirRef((Sehir) getDBOperator().find(Sehir.class.getSimpleName(), "o.ilKodu='" + plakakodu + "'"));
				changeIlce(this.kisikimlik.getSehirRef().getRID());
				this.kisikimlik.setIlceRef((Ilce) getDBOperator().find(Ilce.class.getSimpleName(), "o.ilcekodu='" + kisikimlikPojo.getNufusaKayitliOlduguIlceKodu() + "'"));
				this.kisikimlik.setMahalle(kisikimlikPojo.getMahalle());
			}
			this.kisikimlik.setCiltno(kisikimlikPojo.getCiltNo());
			// this.kisikimlik.setCiltAciklama(kisikimlikPojo.getCiltAciklama());
			this.kisikimlik.setMahalle(kisikimlikPojo.getMahalle());
			this.kisikimlik.setAileno(kisikimlikPojo.getAileSiraNo());
			this.kisikimlik.setSirano(kisikimlikPojo.getBireySiraNo());
			this.kisikimlik.setKpsdogrulamatarih(new Date());

			KPSService kpsService = KPSService.getInstance();
			String kpsAdres = kpsService.kisiAdresBilgisiGetirYeni(this.kisi.getKimlikno());
			try {
				// PojoAdres kpsDigerPojo = XmlHelper.readXmlStringForAdres(kpsAdres, AdresTuru._KPSDIGERADRESI);
				// PojoAdres kpsPojo = XmlHelper.readXmlStringForAdres(kpsAdres, AdresTuru._KPSADRESI);
				PojoAdres kpsPojo = ToysXMLReader.readXmlStringForAdres(kpsAdres, "YerlesimYeriAdresi");
				PojoAdres kpsDigerPojo = ToysXMLReader.readXmlStringForAdres(kpsAdres, "DigerAdresBilgileri");
				if (kpsPojo != null && (kpsPojo.getHataBilgisiKodu() == null || kpsPojo.getHataBilgisiKodu().trim().equals(""))) {
					setKpsAdres(kisiAdresAktar(kpsPojo, AdresTuru._KPSADRESI));
				}
				if (kpsDigerPojo != null && (kpsDigerPojo.getHataBilgisiKodu() == null || kpsDigerPojo.getHataBilgisiKodu().trim().equals(""))) {
					setKpsDigerAdres(kisiAdresAktar(kpsDigerPojo, AdresTuru._KPSDIGERADRESI));
				}
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			}
		}
	}

	private Adres kisiAdresAktar(PojoAdres kpsPojo, AdresTuru adresTuru) throws Exception {
		Adres yeniAdres = new Adres();
		yeniAdres.setAdresturu(adresTuru);
		yeniAdres.setAdrestipi(kpsPojo.getAdrestipi());
		yeniAdres.setAcikAdres(kpsPojo.getAcikAdres());
		yeniAdres.setAdresNo(kpsPojo.getAdresNo());
		if (!isEmpty(kpsPojo.getBeyanTarihi()) && !kpsPojo.getBeyanTarihi().toString().equalsIgnoreCase("null/null/null")) {
			yeniAdres.setBeyanTarihi(DateUtil.createDateObject(kpsPojo.getBeyanTarihi()));
		}
		yeniAdres.setBinaAda(kpsPojo.getBinaAda());
		yeniAdres.setBinaBlokAdi(kpsPojo.getBinaBlokAdi());
		yeniAdres.setBinaKodu(kpsPojo.getBinaKodu());
		yeniAdres.setBinaPafta(kpsPojo.getBinaPafta());
		yeniAdres.setBinaParsel(kpsPojo.getBinaParsel());
		yeniAdres.setBinaPafta(kpsPojo.getBinaPafta());
		yeniAdres.setBinaSiteAdi(kpsPojo.getBinaSiteAdi());
		yeniAdres.setCsbm(kpsPojo.getCsbm());
		yeniAdres.setCsbmKodu(kpsPojo.getCsbmKodu());
		yeniAdres.setDisKapiNo(kpsPojo.getDisKapiNo());
		yeniAdres.setIcKapiNo(kpsPojo.getIcKapiNo());
		if (!isEmpty(kpsPojo.getIlKodu()) && isNumber(kpsPojo.getIlKodu().trim())) {
			String ilKodu = kpsPojo.getIlKodu();
			if (getDBOperator().recordCount(Sehir.class.getSimpleName(), "o.ilKodu='" + ilKodu + "'") > 0) {
				yeniAdres.setSehirRef((Sehir) getDBOperator().find(Sehir.class.getSimpleName(), "o.ilKodu='" + ilKodu + "'"));
			} else {
				if (ilKodu.toString().substring(0, 1).equals("0")) {
					ilKodu = ilKodu.substring(1);
					if (getDBOperator().recordCount(Sehir.class.getSimpleName(), "o.ilKodu='" + ilKodu + "'") > 0) {
						yeniAdres.setSehirRef((Sehir) getDBOperator().find(Sehir.class.getSimpleName(), "o.ilKodu='" + ilKodu + "'"));
					}
				}
			}
		}
		if (!isEmpty(kpsPojo.getIlceKodu()) && isNumber(kpsPojo.getIlceKodu().trim())) {
			String ilceKodu = kpsPojo.getIlceKodu();
			if (getDBOperator().recordCount(Ilce.class.getSimpleName(), "o.ilcekodu='" + kpsPojo.getIlceKodu() + "'") > 0) {
				yeniAdres.setIlceRef((Ilce) getDBOperator().find(Ilce.class.getSimpleName(), "o.ilcekodu='" + kpsPojo.getIlceKodu() + "'"));
			} else {
				if (ilceKodu.toString().substring(0, 1).equals("0")) {
					ilceKodu = ilceKodu.substring(1);
					if (getDBOperator().recordCount(Ilce.class.getSimpleName(), "o.ilcekodu='" + kpsPojo.getIlceKodu() + "'") > 0) {
						yeniAdres.setIlceRef((Ilce) getDBOperator().find(Ilce.class.getSimpleName(), "o.ilcekodu='" + kpsPojo.getIlceKodu() + "'"));
					}
				}
			}
		}
		yeniAdres.setKpsGuncellemeTarihi(new Date());
		yeniAdres.setMahalle(kpsPojo.getMahalle());
		yeniAdres.setMahalleKodu(kpsPojo.getMahalleKodu());
		if (kpsPojo.getTasinmaTarihi() != null && !kpsPojo.getTasinmaTarihi().toString().equalsIgnoreCase("null/null/null")) {
			yeniAdres.setTasinmaTarihi(DateUtil.createDateObject(kpsPojo.getTasinmaTarihi()));
		}
		if (kpsPojo.getTescilTarihi() != null && !kpsPojo.getTescilTarihi().toString().equalsIgnoreCase("null/null/null")) {
			yeniAdres.setTescilTarihi(DateUtil.createDateObject(kpsPojo.getTescilTarihi()));
		}
		yeniAdres.setVarsayilan(EvetHayir._EVET);
		return yeniAdres;
	}

	@Override
	public void wizardForward() {
		try {
			if (this.wizardStep == 2) {
				if (this.kurumKisi.getKurumRef() != null) {
					this.kurumKisi.setKisiRef(this.kisi);
					this.kurumKisi.setVarsayilan(EvetHayir._EVET);
					this.kurumKisi.setUyekurumDurum(UyekurumDurum._CALISIYOR);
					this.kurumKisi.setGecerli(EvetHayir._EVET);
					// dbOperator.insert(this.kurumKisi);
					this.isyeriadresi.setAdresturu(AdresTuru._ISADRESI);
					if (getKurumKisi().getKurumRef() != null && getKurumKisi().getKurumRef().getKurumAdres() != null) {
						setIsyeriadresi(getKurumKisi().getKurumRef().getKurumAdres());
					}

					if (getKurumKisi().getKurumRef() != null && getKurumKisi().getKurumRef().getKurumAdres() != null) {
						setIsyeriadresi((Adres) getKurumKisi().getKurumRef().getKurumAdres().cloneObject());
						this.isyeriadresi.setVarsayilan(EvetHayir._HAYIR);
						this.isyeriadresi.setAcikAdres(getKurumKisi().getKurumRef().getKurumAdres().getAcikAdres());
						this.isyeriadresi.setAdresturu(AdresTuru._ISADRESI);
						this.isyeriadresi.setAdrestipi(AdresTipi._ILILCEMERKEZI);
						this.isyeriadresi.setRID(null);
						this.isyeriadresi.setKisiRef(this.kisi);
						if (getDBOperator().recordCount(Ulke.class.getSimpleName(), "o.ulkekod = '9980'") > 0) {
							this.isyeriadresi.setUlkeRef((Ulke) getDBOperator().find(Ulke.class.getSimpleName(), "o.ulkekod = '9980'"));
						}
						if (this.isyeriadresi.getUlkeRef() != null) {
							this.isyeriadresi.setSehirRef(getKurumKisi().getKurumRef().getKurumAdres().getSehirRef());
							if (this.isyeriadresi.getSehirRef() != null) {
								changeIlceForUyeIsyeri(this.isyeriadresi.getSehirRef().getRID());
								this.isyeriadresi.setIlceRef(getKurumKisi().getKurumRef().getKurumAdres().getIlceRef());
							}
						}
					}
					this.adres.setAdrestipi(AdresTipi._ILILCEMERKEZI);
					this.isyeriadresi.setAdrestipi(AdresTipi._ILILCEMERKEZI);
				}

				wizardStep++;
			} else if (this.wizardStep == 4) {
				this.adres.setKisiRef(this.kisi);
				if (this.isyeriadresvarsayilan) {
					this.adres.setVarsayilan(EvetHayir._EVET);
				} else {
					this.adres.setVarsayilan(EvetHayir._HAYIR);
				}
				if (this.beyanadresvarsayilan) {
					this.isyeriadresi.setVarsayilan(EvetHayir._EVET);
				} else {
					this.isyeriadresi.setVarsayilan(EvetHayir._HAYIR);
				}
				if (this.adres.getVarsayilan() == EvetHayir._EVET) {
					String whereCondition = "o.rID <> " + this.adres.getRID() + " AND o.varsayilan =" + EvetHayir._EVET.getCode();
					if (this.adres.getKisiRef() != null) {
						whereCondition += " AND o.kisiRef.rID = " + this.adres.getKisiRef().getRID();
					}
					try {
						if (getDBOperator().recordCount(Adres.class.getSimpleName(), whereCondition) > 0) {
							for (Object obj : getDBOperator().load(Adres.class.getSimpleName(), whereCondition, "o.rID")) {
								Adres adres = (Adres) obj;
								adres.setVarsayilan(EvetHayir._HAYIR);
								getDBOperator().update(adres);
							}
						}
					} catch (Exception e) {
						logYaz("Exception @" + getModelName() + "Controller :", e);
					}
				} else if (this.adres.getVarsayilan() == EvetHayir._HAYIR) {
					String whereCondition = "o.rID <> " + this.adres.getRID() + " AND o.varsayilan =" + EvetHayir._HAYIR.getCode();
					if (this.adres.getKisiRef() != null) {
						whereCondition += " AND o.kisiRef.rID = " + this.adres.getKisiRef().getRID();
					}
					String whereCondition1 = "o.rID <> " + this.adres.getRID() + " AND o.varsayilan =" + EvetHayir._EVET.getCode();

					try {
						if (getDBOperator().recordCount(Adres.class.getSimpleName(), whereCondition) == 0 && getDBOperator().recordCount(Adres.class.getSimpleName(), whereCondition1) == 0) {
							adres.setVarsayilan(EvetHayir._EVET);
							// getDBOperator().update(adres);
						}
						whereCondition = "o.rID <> " + this.adres.getRID() + " AND o.varsayilan =" + EvetHayir._EVET.getCode();
						if (this.adres.getKisiRef() != null) {
							whereCondition += " AND o.kisiRef.rID = " + this.adres.getKisiRef().getRID();
						}
						if (getDBOperator().recordCount(Adres.class.getSimpleName(), whereCondition) == 0) {
							createGenericMessage(KeyUtil.getMessageValue("varsayilan.hata"), FacesMessage.SEVERITY_INFO);
							adres.setVarsayilan(EvetHayir._EVET);
							// getDBOperator().update(adres);
						}

					} catch (Exception e) {
						logYaz("Exception @" + getModelName() + "Controller :", e);
					}
				}

				if (getDBOperator().recordCount(Ulke.class.getSimpleName(), "o.ulkekod='9980'") > 0) {
					this.adres.setUlkeRef((Ulke) getDBOperator().load(Ulke.class.getSimpleName(), "o.ulkekod='9980'", "o.rID").get(0));
				}
				if (this.adres.getRID() == null) {
					if (this.adres.getAdrestipi() == AdresTipi._YURTDISIADRESI) {
						if (this.adres.getYabanciulke() == null || isEmpty(this.adres.getYabancisehir()) || this.adres.getYabanciadres() == null) {
							createGenericMessage("Yurdışı adresleri için ülke, şehir ve adres alanları zorunludur", FacesMessage.SEVERITY_ERROR);
							return;
						}
					} else if (this.adres.getAdrestipi() != AdresTipi._YURTDISIADRESI) {

						if (this.adres.getUlkeRef() == null || isEmpty(this.adres.getAcikAdres()) || this.adres.getSehirRef() == null || this.adres.getIlceRef() == null) {
							createGenericMessage("Adres için il, ilçe ve açık adres alanları zorunludur", FacesMessage.SEVERITY_ERROR);
							return;
						}
					}

					getDBOperator().insert(this.adres);
					this.oldValueForAdres = this.adres.getValue();
					if (this.adresController.isLoggable()) {
						logKaydet(this.adres, IslemTuru._EKLEME, "");
					}
					createGenericMessage(KeyUtil.getMessageValue("kayit.eklendi"), FacesMessage.SEVERITY_INFO);
					this.adres = new Adres();
				}

				adresListesiGuncelle(this.kisi);
				this.wizardStep++;
			} else if (this.wizardStep == 5) {
				this.telefon.setKisiRef(this.kisi);
				this.internetadres.setNetadresturu(NetAdresTuru._EPOSTA);
				if (this.telefon.getVarsayilan() == EvetHayir._EVET) {
					String whereCondition = "o.rID <> " + this.telefon.getRID() + " AND o.varsayilan =" + EvetHayir._EVET.getCode();
					if (this.telefon.getKisiRef() != null) {
						whereCondition += " AND o.kisiRef.rID = " + this.telefon.getKisiRef().getRID();
					}
					try {
						if (getDBOperator().recordCount(Telefon.class.getSimpleName(), whereCondition) > 0) {
							for (Object obj : getDBOperator().load(Telefon.class.getSimpleName(), whereCondition, "o.rID")) {
								Telefon telefon1 = (Telefon) obj;
								telefon1.setVarsayilan(EvetHayir._HAYIR);
								getDBOperator().update(telefon1);
							}
						}
					} catch (Exception e) {
						logYaz("Exception @" + getModelName() + "Controller :", e);
					}
				} else if (this.telefon.getVarsayilan() == EvetHayir._HAYIR) {
					String whereCondition = "o.rID <> " + this.telefon.getRID() + " AND o.varsayilan =" + EvetHayir._HAYIR.getCode();
					if (this.telefon.getKisiRef() != null) {
						whereCondition += " AND o.kisiRef.rID = " + this.telefon.getKisiRef().getRID();
					}
					String whereCondition1 = "o.rID <> " + this.telefon.getRID() + " AND o.varsayilan =" + EvetHayir._EVET.getCode();
					try {
						if (getDBOperator().recordCount(Telefon.class.getSimpleName(), whereCondition) == 0 && getDBOperator().recordCount(Telefon.class.getSimpleName(), whereCondition1) == 0) {
							telefon.setVarsayilan(EvetHayir._EVET);
							// getDBOperator().update(telefon);
						}
						whereCondition = "o.rID <> " + this.telefon.getRID() + " AND o.varsayilan =" + EvetHayir._EVET.getCode();
						if (this.telefon.getKisiRef() != null) {
							whereCondition += " AND o.kisiRef.rID = " + this.telefon.getKisiRef().getRID();
						}
						if (getDBOperator().recordCount(Telefon.class.getSimpleName(), whereCondition) == 0) {
							createGenericMessage(KeyUtil.getMessageValue("varsayilan.hata"), FacesMessage.SEVERITY_INFO);
							telefon.setVarsayilan(EvetHayir._EVET);
							// getDBOperator().update(telefon);
						}

					} catch (Exception e) {
						logYaz("Exception @" + getModelName() + "Controller :", e);
					}
				}
				if (this.telefon.getRID() == null) {
					getDBOperator().insert(this.telefon);
					this.oldValueForTelefon = this.telefon.getValue();
					if (this.telefonController.isLoggable()) {
						logKaydet(this.telefon, IslemTuru._EKLEME, "");
					}
					createGenericMessage(KeyUtil.getMessageValue("kayit.eklendi"), FacesMessage.SEVERITY_INFO);
					this.telefon = new Telefon();
				}
				telefonListesiGuncelle(this.kisi);
				this.wizardStep++;
			} else if (this.wizardStep == 6) {
				this.internetadres.setKisiRef(this.kisi);
				if (this.internetadres.getVarsayilan() == EvetHayir._EVET) {
					String whereCondition = "o.rID <> " + this.internetadres.getRID() + " AND o.varsayilan =" + EvetHayir._EVET.getCode();
					if (this.internetadres.getKisiRef() != null) {
						whereCondition += " AND o.kisiRef.rID = " + this.internetadres.getKisiRef().getRID();
					}
					try {
						if (getDBOperator().recordCount(Internetadres.class.getSimpleName(), whereCondition) > 0) {
							for (Object obj : getDBOperator().load(Internetadres.class.getSimpleName(), whereCondition, "o.rID")) {
								Internetadres internetadres1 = (Internetadres) obj;
								internetadres1.setVarsayilan(EvetHayir._HAYIR);
								getDBOperator().update(internetadres1);
							}
						}
					} catch (Exception e) {
						logYaz("Exception @" + getModelName() + "Controller :", e);
					}
				} else if (this.internetadres.getVarsayilan() == EvetHayir._HAYIR) {
					String whereCondition = "o.rID <> " + this.internetadres.getRID() + " AND o.varsayilan =" + EvetHayir._HAYIR.getCode();
					if (this.internetadres.getKisiRef() != null) {
						whereCondition += " AND o.kisiRef.rID = " + this.internetadres.getKisiRef().getRID();
					}
					String whereCondition1 = "o.rID <> " + this.internetadres.getRID() + " AND o.varsayilan =" + EvetHayir._EVET.getCode();

					try {
						if (getDBOperator().recordCount(Internetadres.class.getSimpleName(), whereCondition) == 0 && getDBOperator().recordCount(Internetadres.class.getSimpleName(), whereCondition1) == 0) {
							telefon.setVarsayilan(EvetHayir._EVET);
							// getDBOperator().update(telefon);
						}
						whereCondition = "o.rID <> " + this.internetadres.getRID() + " AND o.varsayilan =" + EvetHayir._EVET.getCode();
						if (this.internetadres.getKisiRef() != null) {
							whereCondition += " AND o.kisiRef.rID = " + this.internetadres.getKisiRef().getRID();
						}
						if (getDBOperator().recordCount(Internetadres.class.getSimpleName(), whereCondition) == 0) {
							createGenericMessage(KeyUtil.getMessageValue("varsayilan.hata"), FacesMessage.SEVERITY_INFO);
							internetadres.setVarsayilan(EvetHayir._EVET);
							// getDBOperator().update(telefon);
						}

					} catch (Exception e) {
						logYaz("Exception @" + getModelName() + "Controller :", e);
					}
				}

				if (this.internetadres.getRID() == null) {
					getDBOperator().insert(this.internetadres);
					this.oldValueForInternetadres = this.internetadres.getValue();
					if (this.internetadresController.isLoggable()) {
						logKaydet(this.internetadres, IslemTuru._EKLEME, "");
					}
					createGenericMessage(KeyUtil.getMessageValue("kayit.eklendi"), FacesMessage.SEVERITY_INFO);
					this.internetadres = new Internetadres();
				}
				internetadresListesiGuncelle(this.kisi);
				this.wizardStep++;
			} else if (this.wizardStep == 7) {
				if (this.kisifotograf.getRID() == null) {
					this.kisifotograf.setKisiRef(this.kisi);
					getDBOperator().insert(this.kisifotograf);
					this.oldValueForKisiFotograf = this.kisifotograf.getValue();
					if (this.kisifotografController.isLoggable()) {
						logKaydet(this.kisifotograf, IslemTuru._EKLEME, "");
					}
					createGenericMessage(KeyUtil.getMessageValue("kayit.eklendi"), FacesMessage.SEVERITY_INFO);
					this.kisifotograf = new Kisifotograf();
				}
				kisifotografListesiGuncelle(this.kisi);
			} else {
				if (this.wizardStep == 3 && kisi.isEskiKayitMi()) {
					this.wizardStep = 6;
				} else {
					this.wizardStep++;
				}
			}
		} catch (Exception e) {
			logYaz("Exception @" + getModelName() + "Controller :", e);
			createGenericMessage(KeyUtil.getMessageValue("islem.hata"), FacesMessage.SEVERITY_ERROR);
		}
	}

	@Override
	public void wizardBackward() {
		if (this.wizardStep == 6 && kisi.isEskiKayitMi()) {
			this.wizardStep = 3;
		} else {
			this.wizardStep = wizardStep - 1;
		}
	}

	public String finishWizard() {

		try {
			if (this.kurumKisi != null) {
				this.kurumKisi.setVarsayilan(EvetHayir._EVET);
				getDBOperator().insert(this.kurumKisi);
			}
			if (isUyeYap()) {
				this.kisiUye.setUyedurum(UyeDurum._PASIFUYE);
				this.kisiUye.setKayitdurum(UyeKayitDurum._INCELEMEDE);
				getDBOperator().insert(this.kisiUye);
			}

			if (this.isyeriadresi.getAdrestipi() == AdresTipi._YURTDISIADRESI) {
				if (this.isyeriadresi.getYabanciulke() != null && !isEmpty(this.isyeriadresi.getYabanciadres()) && this.isyeriadresi.getYabancisehir() != null) {
					this.isyeriadresi.setKisiRef(getKisi());
					this.isyeriadresi.setVarsayilan(EvetHayir._HAYIR);
					getDBOperator().insert(this.isyeriadresi);
				}
			} else if (this.isyeriadresi.getAdrestipi() != AdresTipi._YURTDISIADRESI) {
				if (this.isyeriadresi.getUlkeRef() != null && !isEmpty(this.isyeriadresi.getAcikAdres()) && this.isyeriadresi.getSehirRef() != null && this.isyeriadresi.getIlceRef() != null) {
					this.isyeriadresi.setKisiRef(getKisi());
					this.isyeriadresi.setVarsayilan(EvetHayir._HAYIR);
					getDBOperator().insert(this.isyeriadresi);
				}
			}
			if (this.kisiogrenim.getBolumRef() != null) {
				this.kisiogrenim.setKisiRef(getKisi());
				this.kisiogrenim.setVarsayilan(EvetHayir._EVET);
				getDBOperator().insert(this.kisiogrenim);
			}
			if (this.internetadres.getNetadresmetni() != null) {
				this.internetadres.setKisiRef(this.kisi);
				this.internetadres.setVarsayilan(EvetHayir._EVET);
				getDBOperator().insert(this.internetadres);
			}

		} catch (DBException e) {
			logYaz("Exception @" + getModelName() + "Controller :", e);
		}
		ManagedBeanLocator.locateSessionUser().addToSessionFilters(getModelName(), getKisi());
		return ManagedBeanLocator.locateMenuController().gotoPage("menu_" + getModelName() + "Detay");
	}

	public SelectItem[] selectOrderedItemListForBolum(String modelNameForSelectItems, String orderField) {
		String staticWhereCon = "o.lisansKaydolunabilir=true";
		return createSelectItems(modelNameForSelectItems, staticWhereCon, true, orderField);
	}

	public String getSelectedTab() {
		return selectedTab;
	}

	public void setSelectedTab(String selectedTab) {
		this.selectedTab = selectedTab;
	}

	public InternetadresController getInternetadresController() {
		return internetadresController;
	}

	public void setInternetadresController(InternetadresController internetadresController) {
		this.internetadresController = internetadresController;
	}

	public AdresController getAdresController() {
		return adresController;
	}

	public TelefonController getTelefonController() {
		return telefonController;
	}

	public void setTelefonController(TelefonController telefonController) {
		this.telefonController = telefonController;
	}

	@Override
	public int getWizardStep() {
		return wizardStep;
	}

	@Override
	public void setWizardStep(int wizardStep) {
		this.wizardStep = wizardStep;
	}

	public Adres getAdres() {
		return adres;
	}

	public void setAdres(Adres adres) {
		this.adres = adres;
	}

	public Telefon getTelefon() {
		return telefon;
	}

	public void setTelefon(Telefon telefon) {
		this.telefon = telefon;
	}

	public Internetadres getInternetadres() {
		return internetadres;
	}

	public void setInternetadres(Internetadres internetadres) {
		this.internetadres = internetadres;
	}

	public String getDetailEditType() {
		return detailEditType;
	}

	public void setDetailEditType(String detailEditType) {
		this.detailEditType = detailEditType;
	}

	public String getOldValueForAdres() {
		return oldValueForAdres;
	}

	public void setOldValueForAdres(String oldValueForAdres) {
		this.oldValueForAdres = oldValueForAdres;
	}

	public String getOldValueForInternetadres() {
		return oldValueForInternetadres;
	}

	public void setOldValueForInternetadres(String oldValueForInternetadres) {
		this.oldValueForInternetadres = oldValueForInternetadres;
	}

	public String getOldValueForTelefon() {
		return oldValueForTelefon;
	}

	public void setOldValueForTelefon(String oldValueForTelefon) {
		this.oldValueForTelefon = oldValueForTelefon;
	}

	public KisifotografController getKisifotografController() {
		return kisifotografController;
	}

	public void setKisifotografController(KisifotografController kisifotografController) {
		this.kisifotografController = kisifotografController;
	}

	public KisikimlikController getKisikimlikController() {
		return kisikimlikController;
	}

	public void setKisikimlikController(KisikimlikController kisikimlikController) {
		this.kisikimlikController = kisikimlikController;
	}

	public Kisifotograf getKisifotograf() {
		return kisifotograf;
	}

	public void setKisifotograf(Kisifotograf kisifotograf) {
		this.kisifotograf = kisifotograf;
	}

	public Kisikimlik getKisikimlik() {
		return kisikimlik;
	}

	public void setKisikimlik(Kisikimlik kisikimlik) {
		this.kisikimlik = kisikimlik;
	}

	public Personel getPersonel() {
		return personel;
	}

	public void setPersonel(Personel personel) {
		this.personel = personel;
	}

	public Uye getUye() {
		return uye;
	}

	public void setUye(Uye uye) {
		this.uye = uye;
	}

	public String getOldValueForPersonel() {
		return oldValueForPersonel;
	}

	public void setOldValueForPersonel(String oldValueForPersonel) {
		this.oldValueForPersonel = oldValueForPersonel;
	}

	public String getOldValueForUye() {
		return oldValueForUye;
	}

	public void setOldValueForUye(String oldValueForUye) {
		this.oldValueForUye = oldValueForUye;
	}

	public PersonelController getPersonelController() {
		return personelController;
	}

	public void setPersonelController(PersonelController personelController) {
		this.personelController = personelController;
	}

	public UyeController getUyeController() {
		return uyeController;
	}

	public void setUyeController(UyeController uyeController) {
		this.uyeController = uyeController;
	}

	public String getOldValueForKisi() {
		return oldValueForKisi;
	}

	public void setOldValueForKisi(String oldValueForKisi) {
		this.oldValueForKisi = oldValueForKisi;
	}

	public String getOldValueForKisikimlik() {
		return oldValueForKisikimlik;
	}

	public void setOldValueForKisikimlik(String oldValueForKisikimlik) {
		this.oldValueForKisikimlik = oldValueForKisikimlik;
	}

	public String getOldValueForKisiFotograf() {
		return oldValueForKisiFotograf;
	}

	public void setOldValueForKisiFotograf(String oldValueForKisiFotograf) {
		this.oldValueForKisiFotograf = oldValueForKisiFotograf;
	}

	@Override
	public String detailPage(Long selectedRID) {
		if (selectedRID != null && selectedRID > 0) {
			setSelectedRID(selectedRID + "");
		} else if (getSelectedRID() == null) {
			return "";
		}
		ManagedBeanLocator.locateSessionUser().addToSessionFilters(getModelName(), getEntity());
		return ManagedBeanLocator.locateMenuController().gotoPage("menu_" + getModelName() + "Detay");
	}

	public boolean isKpsSorguSonucDondu() {
		return kpsSorguSonucDondu;
	}

	public void setKpsSorguSonucDondu(boolean kpsSorguSonucDondu) {
		this.kpsSorguSonucDondu = kpsSorguSonucDondu;
	}

	public boolean isKpsKaydiBulundu() {
		return kpsKaydiBulundu;
	}

	public void setKpsKaydiBulundu(boolean kpsKaydiBulundu) {
		this.kpsKaydiBulundu = kpsKaydiBulundu;
	}

	public Adres getKpsAdres() {
		return kpsAdres;
	}

	public void setKpsAdres(Adres kpsAdres) {
		this.kpsAdres = kpsAdres;
	}

	public Adres getKpsDigerAdres() {
		return kpsDigerAdres;
	}

	public void setKpsDigerAdres(Adres kpsDigerAdres) {
		this.kpsDigerAdres = kpsDigerAdres;
	}

	public String getKisiDetayFotograf() {
		if (getKisiDetay() != null && getKisiDetay().getRID() != null) {
			try {
				if (getDBOperator().recordCount(Kisifotograf.class.getSimpleName(), "o.kisiRef.rID=" + getKisiDetay().getRID()) > 0) {
					Kisifotograf foto = (Kisifotograf) getDBOperator().load(Kisifotograf.class.getSimpleName(), "o.kisiRef.rID=" + getKisiDetay().getRID(), "o.varsayilan ASC").get(0);
					if (!isEmpty(foto.getPath())) {
						return foto.getPath();
					} else {
						return getNoImagePath();
					}
				} else {
					return getNoImagePath();
				}
			} catch (Exception e) {
				logYaz("@Error Kisi fotograf hatasi :", e);
				return "";
			}
		}
		return "";
	}

	@Transient
	public String getUyeBulundu() {

		try {
			if (getDBOperator().recordCount(Uye.class.getSimpleName(), "o.kisiRef.rID=" + getKisiDetay().getRID()) > 0) {
				Uye uye = (Uye) getDBOperator().load(Uye.class.getSimpleName(), "o.kisiRef.rID=" + getKisiDetay().getRID(), "").get(0);
				if (getDBOperator().recordCount(KurumKisi.class.getSimpleName(), "o.kisiRef.rID=" + uye.getKisiRef().getRID() + " AND o.varsayilan=" + EvetHayir._EVET.getCode()) > 0) {
					KurumKisi uyeKurum = (KurumKisi) getDBOperator().load(KurumKisi.class.getSimpleName(), "o.kisiRef.rID=" + uye.getKisiRef().getRID() + " AND o.varsayilan=" + EvetHayir._EVET.getCode(), "").get(0);
					return uyeKurum.getKurumRef().getAd();
				}
			}
		} catch (Exception e) {
			logYaz("Exception @" + getModelName() + "Controller :", e);
			return "";
		}
		return "";
	}

	@Transient
	public Long getKisiKimlikNo() {
		if (getKisiDetay() != null) {
			try {
				if (getDBOperator().recordCount(Kisi.class.getSimpleName(), "o.rID=" + getKisiDetay().getRID()) > 0) {
					Kisi kisi = (Kisi) getDBOperator().load(Kisi.class.getSimpleName(), "o.rID=" + getKisiDetay().getRID(), "").get(0);
					return kisi.getKimlikno();
				} else {
					return null;
				}
			} catch (Exception e) {
				logYaz("Exception @" + getModelName() + "Controller :", e);
				return null;
			}
		} else {
			return null;
		}
	}

	public String callDetailFromWizard() {
		return this.detailPage(null);
	}

	public boolean isKisiBulundu() {
		if (!isEmpty(this.getSelectedRID())) {
			return true;
		} else {
			return false;
		}
	}

	public String eposta() {
		if (!setSelected()) {
			return "";
		}
		getSessionUser().setLegalAccess(1);
		putObjectToSessionFilter(ApplicationDescriptor._KISI_MODEL, getKisi());
		return ManagedBeanLocator.locateMenuController().gotoPage("menu_Kisianlikeposta");
	}

	public String sms() {
		if (!setSelected()) {
			return "";
		}
		getSessionUser().setLegalAccess(1);
		putObjectToSessionFilter(ApplicationDescriptor._KISI_MODEL, getKisi());
		return ManagedBeanLocator.locateMenuController().gotoPage("menu_Kisianliksms");
	}

	public void kpsGuncelleme() throws Exception {
		this.kpsKaydiBulundu = false;
		this.kpsSorguSonucDondu = false;
		String kpsKisi;
		KPSService kpsService = KPSService.getInstance();
		kpsKisi = kpsService.kisiKimlikBilgisiGetirYeni(this.kisiDetay.getKimlikno());
		PojoKisi kisikimlikPojo = ToysXMLReader.readXmlStringForKimlik(kpsKisi.trim());
		this.kpsSorguSonucDondu = true;
		if (getDBOperator().recordCount(Kisikimlik.class.getSimpleName(), "o.kimlikno=" + this.kisiDetay.getKimlikno()) == 0) {
			this.kisikimlik = new Kisikimlik();
			if (!isEmpty(kisikimlikPojo.getHataBilgisiKodu())) {
				createCustomMessage("Girdiğiniz kimlik numarası KPS sisteminde bulunamadı!", FacesMessage.SEVERITY_ERROR, "kimliknoInputTextKisi:kimliknoKisi");
				this.kisikimlik.setKimlikno(this.kisiDetay.getKimlikno());
				return;
			}
			this.kpsKaydiBulundu = true;
			this.kisiDetay.setAd(kisikimlikPojo.getAd());
			this.kisiDetay.setSoyad(kisikimlikPojo.getSoyad());

			if (isNumber(kisikimlikPojo.getCinsiyetKod())) {
				this.kisikimlik.setCinsiyet(Cinsiyet.getWithCode(Integer.parseInt(kisikimlikPojo.getCinsiyetKod())));
			}
			this.kisikimlik.setKisiRef(kisiDetay);
			this.kisikimlik.setKimlikno(kisikimlikPojo.getTcKimlikNo());
			this.kisikimlik.setBabaad(kisikimlikPojo.getBabaAdi());
			this.kisikimlik.setAnaad(kisikimlikPojo.getAnaAdi());
			if (isNumber(kisikimlikPojo.getMedeniHalKodu())) {
				this.kisikimlik.setMedenihal(MedeniHal.getWithCode(Integer.parseInt(kisikimlikPojo.getMedeniHalKodu())));
			}
			this.kisikimlik.setDogumyer(kisikimlikPojo.getDogumYeri());
			if (kisikimlikPojo.getDogumTarihi() != null && !kisikimlikPojo.getDogumTarihi().toString().equalsIgnoreCase("null/null/null")) {
				this.kisikimlik.setDogumtarih(DateUtil.createDateObject(kisikimlikPojo.getDogumTarihi()));
			}
			if (kisikimlikPojo.getOlumTarihi() != null && !kisikimlikPojo.getOlumTarihi().toString().equalsIgnoreCase("null/null/null")) {
				this.kisikimlik.setOlumtarih(DateUtil.createDateObject(kisikimlikPojo.getOlumTarihi()));
			}
			if (isNumber(kisikimlikPojo.getDurumKod())) {
				this.kisikimlik.setKisidurum(KisiDurum.getWithCode(Integer.parseInt(kisikimlikPojo.getDurumKod())));
			}
			String plakakodu = kisikimlikPojo.getNufusaKayitliOlduguIlKodu();
			if (getDBOperator().recordCount(Sehir.class.getSimpleName(), "o.ilKodu='" + plakakodu + "'") > 0) {
				this.kisikimlik.setSehirRef((Sehir) getDBOperator().find(Sehir.class.getSimpleName(), "o.ilKodu='" + plakakodu + "'"));
				changeIlce(this.kisikimlik.getSehirRef().getRID());
				this.kisikimlik.setIlceRef((Ilce) getDBOperator().find(Ilce.class.getSimpleName(), "o.ilcekodu='" + kisikimlikPojo.getNufusaKayitliOlduguIlceKodu() + "'"));
				this.kisikimlik.setMahalle(kisikimlikPojo.getMahalle());
			}
			this.kisikimlik.setCiltno(kisikimlikPojo.getCiltNo());
			this.kisikimlik.setAileno(kisikimlikPojo.getAileSiraNo());
			this.kisikimlik.setSirano(kisikimlikPojo.getBireySiraNo());
			this.kisikimlik.setKpsdogrulamatarih(new Date());
			getDBOperator().update(this.kisiDetay);
			getDBOperator().insert(kisikimlik);
			kisikimlikController.queryAction();
		} else {
			if (!isEmpty(kisikimlikPojo.getHataBilgisiKodu())) {
				logger.error("kisikimlikPojo.getHataBilgisiKodu() = " + kisikimlikPojo.getHataBilgisiKodu());
				createGenericMessage("Girdiğiniz kimlik numarası KPS sisteminde bulunamadığından dolayı KPS'den güncelleme yapamazsınız!" + "Elle bilgileri güncelemek için Değiştir butonuna basınız !", FacesMessage.SEVERITY_INFO);
				return;
			}
			this.kpsKaydiBulundu = true;
			this.kisiDetay.setAd(kisikimlikPojo.getAd());
			this.kisiDetay.setSoyad(kisikimlikPojo.getSoyad());

			if (isNumber(kisikimlikPojo.getCinsiyetKod())) {
				this.kisikimlik.setCinsiyet(Cinsiyet.getWithCode(Integer.parseInt(kisikimlikPojo.getCinsiyetKod())));
			}
			this.kisikimlik.setKisiRef(getKisiDetay());
			this.kisikimlik.setKimlikno(kisikimlikPojo.getTcKimlikNo());
			this.kisikimlik.setBabaad(kisikimlikPojo.getBabaAdi());
			this.kisikimlik.setAnaad(kisikimlikPojo.getAnaAdi());
			if (isNumber(kisikimlikPojo.getMedeniHalKodu())) {
				this.kisikimlik.setMedenihal(MedeniHal.getWithCode(Integer.parseInt(kisikimlikPojo.getMedeniHalKodu())));
			}
			this.kisikimlik.setDogumyer(kisikimlikPojo.getDogumYeri());
			if (kisikimlikPojo.getDogumTarihi() != null && !kisikimlikPojo.getDogumTarihi().toString().equalsIgnoreCase("null/null/null")) {
				this.kisikimlik.setDogumtarih(DateUtil.createDateObject(kisikimlikPojo.getDogumTarihi()));
			}
			if (kisikimlikPojo.getOlumTarihi() != null && !kisikimlikPojo.getOlumTarihi().toString().equalsIgnoreCase("null/null/null")) {
				this.kisikimlik.setOlumtarih(DateUtil.createDateObject(kisikimlikPojo.getOlumTarihi()));
			}
			if (isNumber(kisikimlikPojo.getDurumKod())) {
				this.kisikimlik.setKisidurum(KisiDurum.getWithCode(Integer.parseInt(kisikimlikPojo.getDurumKod())));
			}
			String plakakodu = kisikimlikPojo.getNufusaKayitliOlduguIlKodu();
			if (getDBOperator().recordCount(Sehir.class.getSimpleName(), "o.ilKodu='" + plakakodu + "'") > 0) {
				Sehir sehirRef = (Sehir) getDBOperator().find(Sehir.class.getSimpleName(), "o.ilKodu='" + plakakodu + "'");
				this.kisikimlik.setSehirRef(sehirRef);
				changeIlce(this.kisikimlik.getSehirRef().getRID());
				this.kisikimlik.setIlceRef((Ilce) getDBOperator().find(Ilce.class.getSimpleName(), "o.ilcekodu='" + kisikimlikPojo.getNufusaKayitliOlduguIlceKodu() + "'"));
				this.kisikimlik.setMahalle(kisikimlikPojo.getMahalle());
			}
			this.kisikimlik.setCiltno(kisikimlikPojo.getCiltNo());
			this.kisikimlik.setAileno(kisikimlikPojo.getAileSiraNo());
			this.kisikimlik.setSirano(kisikimlikPojo.getBireySiraNo());
			this.kisikimlik.setKpsdogrulamatarih(new Date());
			getDBOperator().update(this.kisiDetay);
			getDBOperator().update(kisikimlik);
			kisikimlikController.queryAction();
		}
		createGenericMessage("KPS'den güncelleme işlemi tamamlandı...", FacesMessage.SEVERITY_INFO);
		queryAction();
	}

	public String detailUyePage() {
		Uye uye = (Uye) getDBOperator().load(Uye.class.getSimpleName(), "o.kisiRef.rID=" + getKisiDetay().getRID(), "o.rID").get(0);
		if (uye != null) {
			ManagedBeanLocator.locateSessionUser().addToSessionFilters(Uye.class.getSimpleName(), uye);
			return ManagedBeanLocator.locateMenuController().gotoPage("menu_" + Uye.class.getSimpleName() + "Detay");
		}

		return "";
	}

	public void varsayilanyap(Kisifotograf foto) {
		RequestContext context = RequestContext.getCurrentInstance();
		if (foto != null) {
			try {
				if (getDBOperator().recordCount(Kisifotograf.class.getSimpleName(), "o.rID <> " + foto.getRID() + " AND o.varsayilan =" + EvetHayir._EVET.getCode()) > 0) {
					for (Object obj : getDBOperator().load(Kisifotograf.class.getSimpleName(), "o.rID <> " + foto.getRID() + " AND o.varsayilan =" + EvetHayir._EVET.getCode(), "o.rID")) {
						kisifotograf = (Kisifotograf) obj;
						kisifotograf.setVarsayilan(EvetHayir._HAYIR);
						getDBOperator().update(kisifotograf);

					}
				}
				if (getDBOperator().recordCount(Kisifotograf.class.getSimpleName(), "o.rID = " + foto.getRID() + " AND o.varsayilan =" + EvetHayir._HAYIR.getCode()) > 0) {
					for (Object obj : getDBOperator().load(Kisifotograf.class.getSimpleName(), "o.rID = " + foto.getRID() + " AND o.varsayilan =" + EvetHayir._HAYIR.getCode(), "o.rID")) {
						kisifotograf = (Kisifotograf) obj;
						kisifotograf.setVarsayilan(EvetHayir._EVET);
						getDBOperator().update(kisifotograf);

					}
				}
			} catch (DBException e) {
				logYaz("Exception @" + getModelName() + "Controller :", e);
			}
			kisifotografController.queryAction();
			updateDialogAndForm(context);
			context.update("kisiPanel");
		}

	}

	public boolean varsayilan(Kisifotograf foto) {
		if (foto != null) {
			try {
				if (getDBOperator().recordCount(Kisifotograf.class.getSimpleName(), "o.rID = " + foto.getRID() + " AND o.varsayilan =" + EvetHayir._EVET.getCode()) > 0) {
					return false;
				}
			} catch (DBException e) {
				logYaz("Exception @" + getModelName() + "Controller :", e);
			}
		}
		return true;
	}

	public void kisiKpsAdresGuncelle() {
		Adres kpsAdresGuncelle = new Adres();
		String kpsAdres = "";
		try {
			if (getDBOperator().recordCount(Adres.class.getSimpleName(), "o.kisiRef.rID=" + getKisiDetay().getRID() + " AND o.adresturu=" + AdresTuru._KPSADRESI.getCode()) > 0) {
				kpsAdresGuncelle = (Adres) getDBOperator().load(Adres.class.getSimpleName(), "o.kisiRef.rID=" + getKisiDetay().getRID() + " AND o.adresturu=" + AdresTuru._KPSADRESI.getCode(), "").get(0);
				KPSService kpsService = KPSService.getInstance();
				kpsAdres = kpsService.kisiAdresBilgisiGetirYeni(getKisiDetay().getKimlikno());
				// PojoAdres kpsPojo = XmlHelper.readXmlStringForAdres(kpsAdres, AdresTuru._KPSADRESI);
				PojoAdres kpsPojo = ToysXMLReader.readXmlStringForAdres(kpsAdres, "YerlesimYeriAdresi");
				if (kpsPojo != null) {
					// logYaz("kpsPojo hata bilgisi kodu :" +
					// kpsPojo.getHataBilgisiKodu());
					if (kpsPojo.getHataBilgisiKodu() == null || kpsPojo.getHataBilgisiKodu().trim().equals("")) {
						kpsAdresGuncelle.setKisiRef(getKisiDetay());
						kpsAdresGuncelle.setAdresturu(AdresTuru._KPSADRESI);
						kpsAdresGuncelle.setAdrestipi(kpsPojo.getAdrestipi());
						kpsAdresGuncelle.setAcikAdres(kpsPojo.getAcikAdres());
						kpsAdresGuncelle.setAdresNo(kpsPojo.getAdresNo());
						if (!isEmpty(kpsPojo.getBeyanTarihi()) && !kpsPojo.getBeyanTarihi().toString().equalsIgnoreCase("null/null/null")) {
							kpsAdresGuncelle.setBeyanTarihi(DateUtil.createDateObject(kpsPojo.getBeyanTarihi()));
						}
						kpsAdresGuncelle.setBinaAda(kpsPojo.getBinaAda());
						kpsAdresGuncelle.setBinaBlokAdi(kpsPojo.getBinaBlokAdi());
						kpsAdresGuncelle.setBinaKodu(kpsPojo.getBinaKodu());
						kpsAdresGuncelle.setBinaPafta(kpsPojo.getBinaPafta());
						kpsAdresGuncelle.setBinaParsel(kpsPojo.getBinaParsel());
						kpsAdresGuncelle.setBinaPafta(kpsPojo.getBinaPafta());
						kpsAdresGuncelle.setBinaSiteAdi(kpsPojo.getBinaSiteAdi());
						kpsAdresGuncelle.setCsbm(kpsPojo.getCsbm());
						kpsAdresGuncelle.setCsbmKodu(kpsPojo.getCsbmKodu());
						kpsAdresGuncelle.setDisKapiNo(kpsPojo.getDisKapiNo());
						kpsAdresGuncelle.setIcKapiNo(kpsPojo.getIcKapiNo());
						if (!isEmpty(kpsPojo.getIlKodu()) && isNumber(kpsPojo.getIlKodu().trim())) {
							if (getDBOperator().recordCount(Sehir.class.getSimpleName(), "o.ilKodu='" + kpsPojo.getIlKodu() + "'") > 0) {
								kpsAdresGuncelle.setSehirRef((Sehir) getDBOperator().find(Sehir.class.getSimpleName(), "o.ilKodu='" + kpsPojo.getIlKodu() + "'"));
							}
						}
						if (!isEmpty(kpsPojo.getIlceKodu()) && isNumber(kpsPojo.getIlceKodu().trim())) {
							if (getDBOperator().recordCount(Ilce.class.getSimpleName(), "o.ilcekodu='" + kpsPojo.getIlceKodu() + "'") > 0) {
								kpsAdresGuncelle.setIlceRef((Ilce) getDBOperator().find(Ilce.class.getSimpleName(), "o.ilcekodu='" + kpsPojo.getIlceKodu() + "'"));
							}
						}
						kpsAdresGuncelle.setKpsGuncellemeTarihi(new Date());
						kpsAdresGuncelle.setMahalle(kpsPojo.getMahalle());
						kpsAdresGuncelle.setMahalleKodu(kpsPojo.getMahalleKodu());
						if (kpsPojo.getTasinmaTarihi() != null && !kpsPojo.getTasinmaTarihi().toString().equalsIgnoreCase("null/null/null")) {
							kpsAdresGuncelle.setTasinmaTarihi(DateUtil.createDateObject(kpsPojo.getTasinmaTarihi()));
						}
						if (kpsPojo.getTescilTarihi() != null && !kpsPojo.getTescilTarihi().toString().equalsIgnoreCase("null/null/null")) {
							kpsAdresGuncelle.setTescilTarihi(DateUtil.createDateObject(kpsPojo.getTescilTarihi()));
						}
						kpsAdresGuncelle.setVarsayilan(EvetHayir._EVET);
						getDBOperator().update(kpsAdresGuncelle);

					}
				}
			} else {
				KPSService kpsService = KPSService.getInstance();
				kpsAdres = kpsService.kisiAdresBilgisiGetirYeni(getKisiDetay().getKimlikno());
				Kpslog kpslog = new Kpslog();
				kpslog.setKpsturu(KpsTuru._KPSADRES);
				kpslog.setKullaniciRef(getSessionUser().getKullanici());
				kpslog.setKimlikno(kisiDetay.getKimlikno());
				kpslog.setTarih(new Date());
				getDBOperator().insert(kpslog);
				// PojoAdres kpsDigerPojo = XmlHelper.readXmlStringForAdres(kpsAdres, AdresTuru._KPSDIGERADRESI);
				// PojoAdres kpsPojo = XmlHelper.readXmlStringForAdres(kpsAdres, AdresTuru._KPSADRESI);
				PojoAdres kpsPojo = ToysXMLReader.readXmlStringForAdres(kpsAdres, "YerlesimYeriAdresi");
				PojoAdres kpsDigerPojo = ToysXMLReader.readXmlStringForAdres(kpsAdres, "DigerAdresBilgileri");
				if (kpsPojo != null && (kpsPojo.getHataBilgisiKodu() == null || kpsPojo.getHataBilgisiKodu().trim().equals(""))) {
					setKpsAdres(kisiAdresAktar(kpsPojo, AdresTuru._KPSADRESI));
					getKpsAdres().setKisiRef(getKisiDetay());
					getDBOperator().insert(getKpsAdres());
					Adres evadresi = new Adres();
					evadresi = (Adres) getKpsAdres().cloneObject();
					evadresi.setRID(null);
					evadresi.setVarsayilan(EvetHayir._HAYIR);
					evadresi.setAdresturu(AdresTuru._EVADRESI);
					getDBOperator().insert(evadresi);
				}
				if (kpsDigerPojo != null && (kpsDigerPojo.getHataBilgisiKodu() == null || kpsDigerPojo.getHataBilgisiKodu().trim().equals(""))) {
					setKpsDigerAdres(kisiAdresAktar(kpsDigerPojo, AdresTuru._KPSDIGERADRESI));
					getDBOperator().insert(getKpsDigerAdres());
				}
			}
			createGenericMessage("KPS Adres Bilgileriniz Güncellendi!", FacesMessage.SEVERITY_INFO);
		} catch (DBException e) {
			logYaz("Exception @" + getModelName() + "Controller :", e);
			createGenericMessage("Hata meydana geldi!Bu hata devam ederse lütfen sistem yöneticisi ile irtibata geçiniz!", FacesMessage.SEVERITY_INFO);
		} catch (RemoteException e) {
			logYaz("Exception @" + getModelName() + "Controller :", e);
			createGenericMessage("KPS Adres güncellemede hata meydana geldi!Bu hata devam ederse lütfen sistem yöneticisi ile irtibata geçiniz!", FacesMessage.SEVERITY_INFO);
		} catch (ParseException e) {
			logYaz("Exception @" + getModelName() + "Controller :", e);
			createGenericMessage("KPS Adres güncellemede hata meydana geldi!Bu hata devam ederse lütfen sistem yöneticisi ile irtibata geçiniz!", FacesMessage.SEVERITY_INFO);
		} catch (Exception e) {
			logYaz("Exception @" + getModelName() + "Controller :", e);
			createGenericMessage("KPS Adres güncellemede hata meydana geldi!Bu hata devam ederse lütfen sistem yöneticisi ile irtibata geçiniz!", FacesMessage.SEVERITY_INFO);
		}
		this.adresController.queryAction();
		this.detailEditType = null;
	}

	public String kisiListesineDon() {
		ManagedBeanLocator.locateSessionUser().clearSessionFilters();
		return ManagedBeanLocator.locateMenuController().gotoPage("menu_" + getModelName());
	}

	public Adres getIsyeriadres() {
		if (getKisiDetay() == null) {
			return null;
		} else {
			try {
				if (getDBOperator().recordCount(Adres.class.getSimpleName(), "o.kisiRef.rID=" + getKisiDetay().getRID() + " AND o.adresturu=" + AdresTuru._ISADRESI.getCode()) > 0) {
					Adres adres = (Adres) getDBOperator().load(Adres.class.getSimpleName(), "o.kisiRef.rID=" + getKisiDetay().getRID() + " AND o.adresturu=" + AdresTuru._ISADRESI.getCode(), "").get(0);
					return adres;
				}
			} catch (DBException e) {
				logYaz("Exception @" + getModelName() + "Controller :", e);
			}
		}
		return null;
	}

	public Telefon getIstelefon() {
		if (getKisiDetay() == null) {
			return null;
		} else {
			try {
				if (getDBOperator().recordCount(Telefon.class.getSimpleName(), "o.kisiRef.rID=" + getKisiDetay().getRID() + " AND o.telefonturu=" + TelefonTuru._ISTELEFONU.getCode()) > 0) {
					Telefon telefon = (Telefon) getDBOperator().load(Telefon.class.getSimpleName(), "o.kisiRef.rID=" + getKisiDetay().getRID() + " AND o.telefonturu=" + TelefonTuru._ISTELEFONU.getCode(), "").get(0);

					return telefon;
				}
			} catch (DBException e) {
				logYaz("Exception @" + getModelName() + "Controller :", e);
			}
		}
		return null;
	}

	public Adres getEvadresi() {
		if (getKisiDetay() == null) {
			return null;
		} else {
			try {
				if (getDBOperator().recordCount(Adres.class.getSimpleName(), "o.kisiRef.rID=" + getKisiDetay().getRID() + " AND o.adresturu=" + AdresTuru._EVADRESI.getCode()) > 0) {
					Adres adres = (Adres) getDBOperator().load(Adres.class.getSimpleName(), "o.kisiRef.rID=" + getKisiDetay().getRID() + " AND o.adresturu=" + AdresTuru._EVADRESI.getCode(), "").get(0);
					return adres;
				}
			} catch (DBException e) {
				logYaz("Exception @" + getModelName() + "Controller :", e);
			}
		}
		return null;
	}

	public Telefon getEvtelefon() {
		if (getKisiDetay() == null) {
			return null;
		} else {
			try {
				if (getDBOperator().recordCount(Telefon.class.getSimpleName(),
						"o.kisiRef.rID=" + getKisiDetay().getRID() + " AND o.varsayilan =" + EvetHayir._EVET.getCode() + " AND o.telefonturu=" + TelefonTuru._EVTELEFONU.getCode()) > 0) {
					Telefon telefon = (Telefon) getDBOperator()
							.load(Telefon.class.getSimpleName(), "o.kisiRef.rID=" + getKisiDetay().getRID() + " AND o.varsayilan =" + EvetHayir._EVET.getCode() + " AND o.telefonturu=" + TelefonTuru._EVTELEFONU.getCode(), "").get(0);

					return telefon;
				}
			} catch (DBException e) {
				logYaz("Exception @" + getModelName() + "Controller :", e);
			}
		}
		return null;
	}

	public KurumKisi getKurumKisi() {
		return kurumKisi;
	}

	public void setKurumKisi(KurumKisi kurumKisi) {
		this.kurumKisi = kurumKisi;
	}

	public Kurum getKurum() {
		return kurum;
	}

	public void setKurum(Kurum kurum) {
		this.kurum = kurum;
	}

	public Adres getIsyeriadresi() {
		return isyeriadresi;
	}

	public void setIsyeriadresi(Adres isyeriadresi) {
		this.isyeriadresi = isyeriadresi;
	}

	public Boolean getIsyeriadresvarsayilan() {
		return isyeriadresvarsayilan;
	}

	public void setIsyeriadresvarsayilan(Boolean isyeriadresvarsayilan) {
		this.isyeriadresvarsayilan = isyeriadresvarsayilan;
	}

	public Boolean getBeyanadresvarsayilan() {
		return beyanadresvarsayilan;
	}

	public void setBeyanadresvarsayilan(Boolean beyanadresvarsayilan) {
		this.beyanadresvarsayilan = beyanadresvarsayilan;
	}

	public void setAdresController(AdresController adresController) {
		this.adresController = adresController;
	}

	public boolean fakulteZorunluMu() {
		return !kisi.isYabanciMi();
	}

	public SelectItem[] getAdresTuruItems() {
		if (getAdres().getRID() != null) {
			return new SelectItem[] { new SelectItem(getAdres().getAdresturu(), getAdres().getAdresturu().getLabel()) };
		}
		if (getKisiDetay() == null) {
			return ManagedBeanLocator.locateEnumeratedController().getAdresTuruItems();
		} else {
			return getDBOperator().getAdresTuruItems(getKisi());
		}
	}
} // class
