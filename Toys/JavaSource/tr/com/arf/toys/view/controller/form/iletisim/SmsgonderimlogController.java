package tr.com.arf.toys.view.controller.form.iletisim; 
  
 
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.toys.db.filter.iletisim.SmsgonderimlogFilter;
import tr.com.arf.toys.db.model.iletisim.Gonderim;
import tr.com.arf.toys.db.model.iletisim.Smsgonderimlog;
import tr.com.arf.toys.service.application.ApplicationDescriptor;
import tr.com.arf.toys.view.controller.form._base.ToysBaseFilteredController;
  
  
@ManagedBean 
@ViewScoped 
public class SmsgonderimlogController extends ToysBaseFilteredController {  
  
  private static final long serialVersionUID = 6531152165884980748L; 
  
	private Smsgonderimlog smsgonderimlog;  
	private SmsgonderimlogFilter smsgonderimlogFilter = new SmsgonderimlogFilter();  
  
	public SmsgonderimlogController() { 
		super(Smsgonderimlog.class);  
		setDefaultValues(false);  
		setOrderField("icerik");  
		setLoggable(false);  
		setAutoCompleteSearchColumns(new String[]{"icerik"}); 
		setTable(null); 
		if(getMasterEntity() != null){ 
			setMasterObjectName(ApplicationDescriptor._GONDERIM_MODEL);
			setMasterOutcome(ApplicationDescriptor._GONDERIM_MODEL);
			getSmsgonderimlogFilter().setGonderimRef(getMasterEntity());
			getSmsgonderimlogFilter().createQueryCriterias();   
		} 
		queryAction(); 
	} 
 
	public Gonderim getMasterEntity(){
		if(getSmsgonderimlogFilter().getGonderimRef() != null){
			return getSmsgonderimlogFilter().getGonderimRef();
		} else {
			return (Gonderim) getObjectFromSessionFilter(ApplicationDescriptor._GONDERIM_MODEL);
		}			
	} 
 
	public Smsgonderimlog getSmsgonderimlog() { 
		smsgonderimlog = (Smsgonderimlog) getEntity(); 
		return smsgonderimlog; 
	} 
	
	public void setSmsgonderimlog(Smsgonderimlog smsgonderimlog) { 
		this.smsgonderimlog = smsgonderimlog; 
	} 
	
	public SmsgonderimlogFilter getSmsgonderimlogFilter() { 
		return smsgonderimlogFilter; 
	} 
	 
	public void setSmsgonderimlogFilter(SmsgonderimlogFilter smsgonderimlogFilter) {  
		this.smsgonderimlogFilter = smsgonderimlogFilter;  
	}  
	
	private Boolean onizlemeDialog=false;
	
	public Boolean getOnizlemeDialog() {
		return onizlemeDialog;
	}

	public void setOnizlemeDialog(Boolean onizlemeDialog) {
		this.onizlemeDialog = onizlemeDialog;
	}

	@Override
	public void setSelectedRID(String selectedRID) {
		super.setSelectedRID(selectedRID);
		setOnizlemeDialog(true);
	}
	public void onizlemedialogkapat(){
		setOnizlemeDialog(false);
	}

	 
	@Override  
	public BaseFilter getFilter() {  
		return getSmsgonderimlogFilter();  
	}  
	
	@Override 
	public void resetFilter() {
		super.resetFilterOnly();
		if(getMasterEntity() != null){
			getSmsgonderimlogFilter().setGonderimRef(getMasterEntity()); 
		}
		queryAction();
	}
	@Override	
	public void insert() {	
		super.insert();	
		getSmsgonderimlog().setGonderimRef(getSmsgonderimlogFilter().getGonderimRef());	
		
	}	
	
} // class 
