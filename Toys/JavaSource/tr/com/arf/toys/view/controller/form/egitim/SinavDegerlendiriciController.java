package tr.com.arf.toys.view.controller.form.egitim; 
 
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.toys.db.filter.egitim.SinavDegerlendiriciFilter;
import tr.com.arf.toys.db.model.egitim.Sinav;
import tr.com.arf.toys.db.model.egitim.SinavDegerlendirici;
import tr.com.arf.toys.service.application.ApplicationDescriptor;
import tr.com.arf.toys.view.controller.form._base.ToysBaseFilteredController;
  
  
@ManagedBean 
@ViewScoped 
public class SinavDegerlendiriciController extends ToysBaseFilteredController {  
  
  private static final long serialVersionUID = 6531152165884980748L; 
  
	private SinavDegerlendirici sinavDegerlendirici;  
	private SinavDegerlendiriciFilter sinavDegerlendiriciFilter = new SinavDegerlendiriciFilter();  
  
	public SinavDegerlendiriciController() { 
		super(SinavDegerlendirici.class);  
		setDefaultValues(false);  
		setLoggable(false);  
		setTable(null); 
		if (getMasterEntity() != null) {
			setMasterObjectName(ApplicationDescriptor._SINAV_MODEL);
			setMasterOutcome(ApplicationDescriptor._SINAV_MODEL);
			getSinavDegerlendiriciFilter().setSinavRef(getMasterEntity());
			getSinavDegerlendiriciFilter().createQueryCriterias();
		}
		queryAction();
	}
	
	public Sinav getMasterEntity() {
		if (getSinavDegerlendiriciFilter().getSinavRef() != null) {
			return getSinavDegerlendiriciFilter().getSinavRef();
		} else {
			return (Sinav) getObjectFromSessionFilter(ApplicationDescriptor._SINAV_MODEL);
		}
	}
	
	public SinavDegerlendirici getSinavDegerlendirici() {
		sinavDegerlendirici=(SinavDegerlendirici) getEntity();
		return sinavDegerlendirici;
	}

	public void setSinavDegerlendirici(SinavDegerlendirici sinavDegerlendirici) {
		this.sinavDegerlendirici = sinavDegerlendirici;
	}

	public SinavDegerlendiriciFilter getSinavDegerlendiriciFilter() {
		return sinavDegerlendiriciFilter;
	}

	public void setSinavDegerlendiriciFilter(
			SinavDegerlendiriciFilter sinavDegerlendiriciFilter) {
		this.sinavDegerlendiriciFilter = sinavDegerlendiriciFilter;
	}

	@Override  
	public BaseFilter getFilter() {  
		return getSinavDegerlendiriciFilter(); 
	}  
	
	@Override
		public void save() {
			
			if (getMasterEntity()!=null){
				sinavDegerlendirici.setSinavRef(getMasterEntity());
			}else {
				createGenericMessage("Sinav Değerlendirici Bilgileri Boş Geldiğinden Dolayı Kaydetme İşlemini"
						+ " Gerçekleştiremiyoruz!", FacesMessage.SEVERITY_INFO);
				return ;
			}
			super.save();
			queryAction();
		}
	
} // class 
