package tr.com.arf.toys.view.controller.form.iletisim; 
  
 
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import tr.com.arf.framework.db.enumerated._common.EvetHayir;
import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.framework.db.model._base.BaseEntity;
import tr.com.arf.framework.utility.tool.KeyUtil;
import tr.com.arf.toys.db.filter.iletisim.TelefonFilter;
import tr.com.arf.toys.db.model.iletisim.Telefon;
import tr.com.arf.toys.db.model.kisi.Kisi;
import tr.com.arf.toys.db.model.kisi.Kurum;
import tr.com.arf.toys.db.model.system.Birim;
import tr.com.arf.toys.service.application.ApplicationDescriptor;
import tr.com.arf.toys.view.controller.form._base.ToysBaseFilteredController;
  
  
@ManagedBean 
@ViewScoped 
public class TelefonController extends ToysBaseFilteredController {  
  
  private static final long serialVersionUID = 6531152165884980748L; 
  
	private Telefon telefon;  
	private TelefonFilter telefonFilter = new TelefonFilter();  
  
	public TelefonController() { 
		super(Telefon.class);  
		setDefaultValues(false);  
		setOrderField("telefonno");  
		setAutoCompleteSearchColumns(new String[]{"telefonno"}); 
		setTable(null);
		setLoggable(true);
		if(getMasterEntity() != null){ 
			if(getMasterEntity().getClass() == Kisi.class){
				setMasterObjectName(ApplicationDescriptor._KISI_MODEL);
				setMasterOutcome(ApplicationDescriptor._KISI_MODEL);
				getTelefonFilter().setKisiRef((Kisi) getMasterEntity());				 
			} else if(getMasterEntity().getClass() == Kurum.class){
				setMasterObjectName(ApplicationDescriptor._KURUM_MODEL);
				getTelefonFilter().setKurumRef((Kurum) getMasterEntity());
				setMasterOutcome(ApplicationDescriptor._KURUM_MODEL);				 
			} else if(getMasterEntity().getClass() == Birim.class){
				setMasterObjectName(ApplicationDescriptor._BIRIM_MODEL);
				getTelefonFilter().setKurumRef(((Birim) getMasterEntity()).getKurumRef());
				setMasterOutcome(ApplicationDescriptor._BIRIM_MODEL);				 
			}  
			getTelefonFilter().createQueryCriterias();  
		} 
		queryAction(); 
	}
	
	public TelefonController(Object object) { 
		super(Telefon.class);   
		setLoggable(true); 
	}
	
	public BaseEntity getMasterEntity(){
		if(getObjectFromSessionFilter(ApplicationDescriptor._KISI_MODEL) != null){
			return (Kisi) getObjectFromSessionFilter(ApplicationDescriptor._KISI_MODEL);
		} else if(getObjectFromSessionFilter(ApplicationDescriptor._KURUM_MODEL) != null){
			return (Kurum) getObjectFromSessionFilter(ApplicationDescriptor._KURUM_MODEL);
		} else if(getObjectFromSessionFilter(ApplicationDescriptor._BIRIM_MODEL) != null){
			return (Birim) getObjectFromSessionFilter(ApplicationDescriptor._BIRIM_MODEL);
		} else {
			return null;
		}			
	} 
	
	public Telefon getTelefon() { 
		telefon = (Telefon) getEntity(); 
		return telefon; 
	} 
	
	public void setTelefon(Telefon telefon) { 
		this.telefon = telefon; 
	} 
	
	public TelefonFilter getTelefonFilter() { 
		return telefonFilter; 
	} 
	 
	public void setTelefonFilter(TelefonFilter telefonFilter) {  
		this.telefonFilter = telefonFilter;  
	}  
	 
	@Override  
	public BaseFilter getFilter() {  
		return getTelefonFilter();  
	}  
	
	@Override	
	public void save() { 
		super.justSave();	
		if(this.telefon.getVarsayilan() == EvetHayir._EVET){
			String whereCondition = "o.rID <> " + this.telefon.getRID() + " AND o.varsayilan =" + EvetHayir._EVET.getCode();
			if(this.telefon.getKurumRef() != null) {
				whereCondition += " AND o.kurumRef.rID = " + this.telefon.getKurumRef().getRID();
			} else if(this.telefon.getKisiRef() != null){ 
				whereCondition += " AND o.kisiRef.rID = " + this.telefon.getKisiRef().getRID();
			}
			
			try {
				if(getDBOperator().recordCount(getModelName(), whereCondition) > 0){
					for(Object obj : getDBOperator().load(getModelName(), whereCondition, "o.rID")){
						Telefon telefon = (Telefon) obj;
						telefon.setVarsayilan(EvetHayir._HAYIR);
						getDBOperator().update(telefon);
					}					
				}
			} catch (Exception e) { 
				logYaz("Exception @" + getModelName() + "Controller :", e);
			} 
		} else if (this.telefon.getVarsayilan() == EvetHayir._HAYIR){
			String whereCondition = "o.rID <> " + this.telefon.getRID() + " AND o.varsayilan =" + EvetHayir._HAYIR.getCode();
			if(this.telefon.getKurumRef() != null) {
				whereCondition += " AND o.kurumRef.rID = " + this.telefon.getKurumRef().getRID();
			} else if(this.telefon.getKisiRef() != null){ 
				whereCondition += " AND o.kisiRef.rID = " + this.telefon.getKisiRef().getRID();
			}
			String whereCondition1 = "o.rID <> " + this.telefon.getRID() + " AND o.varsayilan =" + EvetHayir._EVET.getCode();
			if(this.telefon.getKurumRef() != null) {
				whereCondition1 += " AND o.kurumRef.rID = " + this.telefon.getKurumRef().getRID();
			} else if(this.telefon.getKisiRef() != null){ 
				whereCondition1 += " AND o.kisiRef.rID = " + this.telefon.getKisiRef().getRID();
			}
			
			try {
				if(getDBOperator().recordCount(getModelName(), whereCondition) == 0 && getDBOperator().recordCount(getModelName(), whereCondition1) == 0){
					telefon.setVarsayilan(EvetHayir._EVET);
					getDBOperator().update(telefon);
				}	
				whereCondition = "o.rID <> " + this.telefon.getRID() + " AND o.varsayilan =" + EvetHayir._EVET.getCode();
				if(this.telefon.getKurumRef() != null) {
					whereCondition += " AND o.kurumRef.rID = " + this.telefon.getKurumRef().getRID();
				} else if(this.telefon.getKisiRef() != null){ 
					whereCondition += " AND o.kisiRef.rID = " + this.telefon.getKisiRef().getRID();
				}
				if(getDBOperator().recordCount(getModelName(), whereCondition) == 0){ 
					createGenericMessage(KeyUtil.getMessageValue("varsayilan.hata"), FacesMessage.SEVERITY_INFO);  
					telefon.setVarsayilan(EvetHayir._EVET);
					getDBOperator().update(telefon);
				}
				
			}catch (Exception e) { 
				logYaz("Exception @" + getModelName() + "Controller :", e);
			}  
		}
		queryAction();
	}	
	
	@Override
	public void delete() {
		if (!setSelected()) {
			return;
		} 	    
		Long kurumRef = 0L;
		Long kisiRef = 0L; 
		if(getTelefon().getKisiRef() != null){
			kisiRef = this.telefon.getKisiRef().getRID();
		} else if(getTelefon().getKurumRef() != null){
			kurumRef = this.telefon.getKurumRef().getRID();
		}  		
		try {
			getDBOperator().delete(super.getEntity()); 
			setSelection(null); 
			setTable(null);
			createGenericMessage(KeyUtil.getMessageValue("kayit.silindi"), FacesMessage.SEVERITY_INFO); 
		} catch (Exception e) {
			createGenericMessage(KeyUtil.getMessageValue("islem.hata") + " Mes : " + e.getMessage(), FacesMessage.SEVERITY_ERROR);  
			return;
		} 
		 
		String whereCondition ="o.varsayilan =" + EvetHayir._EVET.getCode();
			if(kurumRef != 0) {
				whereCondition += " AND o.kurumRef.rID = " + kurumRef;
			} else if(kisiRef != 0){ 
				whereCondition += " AND o.kisiRef.rID = " + kisiRef;
			}
			
			try {
				if(getDBOperator().recordCount(getModelName(), whereCondition) == 0){
					whereCondition = "o.rID <> " + this.telefon.getRID() + " AND o.varsayilan =" + EvetHayir._HAYIR.getCode();
					if(kurumRef != 0) {
						whereCondition += " AND o.kurumRef.rID = " + kurumRef;
					} else if(kisiRef != 0){ 
						whereCondition += " AND o.kisiRef.rID = " + kisiRef;
					}
					if(getDBOperator().recordCount(getModelName(), whereCondition)> 0){ 
						Telefon telefn = (Telefon) getDBOperator().load(getModelName(), whereCondition, "o.rID DESC").get(0);
						if (telefn!=null){
							telefn.setVarsayilan(EvetHayir._EVET);
							getDBOperator().update(telefn);
						}
					}
					
				}	 
			}catch (Exception e) { 
				logYaz("Exception @" + getModelName() + "Controller :", e);
			} 
			queryAction();	 
		}

 
  
	@Override	
	public void insert() {	
		super.insert();	
		
		getTelefon().setVarsayilan(EvetHayir._HAYIR);
		
		if(getTelefonFilter().getKisiRef() != null){ 
			getTelefon().setKisiRef(getTelefonFilter().getKisiRef());
		} else if(getTelefonFilter().getKurumRef() != null) {
			getTelefon().setKurumRef(getTelefonFilter().getKurumRef());
		} 
	}
	
} // class 
