package tr.com.arf.toys.view.controller.form.system; 
  
 
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.model.SelectItem;

import tr.com.arf.framework.db.enumerated._common.EvetHayir;
import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.framework.db.model._base.BaseEntity;
import tr.com.arf.framework.utility.exception.data.DBException;
import tr.com.arf.framework.utility.tool.KeyUtil;
import tr.com.arf.toys.db.filter.kisi.KurumRaporlamaFilter;
import tr.com.arf.toys.db.model.kisi.Kurum;
import tr.com.arf.toys.db.model.system.Kullanicirapor;
import tr.com.arf.toys.view.controller.form._base.ToysBaseFilteredController;
  
  
@ManagedBean 
@ViewScoped 
public class KurumRaporlamaController extends ToysBaseFilteredController {  
  
  private static final long serialVersionUID = 6531152165884980748L; 
  
	private Kurum kurum;   
	private KurumRaporlamaFilter kurumRaporlamaFilter = new KurumRaporlamaFilter();  
	 
	public KurumRaporlamaController() { 
		super(Kurum.class);  
		setDefaultValues(false);  
		setOrderField("ad"); 
		setTable(null);
		this.kurumRaporlamaFilter = new KurumRaporlamaFilter(); 
		setLoggable(false); 
	}  	  
		 
	
	public Kurum getKurum() {
		kurum=(Kurum) getEntity();
		return kurum;
	}

	public void setKurum(Kurum kurum) {
		this.kurum = kurum;
	}

	public KurumRaporlamaFilter getKurumRaporlamaFilter() {
		return kurumRaporlamaFilter;
	}

	public void setKurumRaporlamaFilter(KurumRaporlamaFilter kurumRaporlamaFilter) {
		this.kurumRaporlamaFilter = kurumRaporlamaFilter;
	}

	public void collapseAllPanels(){
		this.kurumPanelCollapsed = true;
	}
	
	private boolean kurumPanelCollapsed = false;
	
	public boolean isKurumPanelCollapsed() {
		return kurumPanelCollapsed;
	}

	public void setKurumPanelCollapsed(boolean kurumPanelCollapsed) {
		this.kurumPanelCollapsed = kurumPanelCollapsed;
	}

	@Override
	public BaseFilter getFilter() {
		return null;
	}
	
	@Override
		@PostConstruct
		public void init() {
			super.init();
			getKurumRaporlamaFilter().setEpostaYok(false);
			getKurumRaporlamaFilter().setTelefonYok(false);
		}

	@SuppressWarnings("unchecked")
	public void filterKurum() throws DBException{
		
		if (getCalistirilacakRaporRID()!=null && getCalistirilacakRaporRID()!=0L){
			logYaz("Calistirilacak rapor id = " +getCalistirilacakRaporRID());
			Kullanicirapor rapor = (Kullanicirapor) getDBOperator().find(Kullanicirapor.class.getSimpleName(), "rID", getCalistirilacakRaporRID() + "").get(0);	
			setList(getDBOperator().load(Kurum.class.getSimpleName(), rapor.getWherecondition(), "o.rID"));
			return;
		}
		
		StringBuilder whereCon = new StringBuilder(" 1 = 1 "); 
		if (getKurumRaporlamaFilter().getKurumadi()!=null && !getKurumRaporlamaFilter().getKurumadi().isEmpty()){
			whereCon.append(" AND (UPPER(o.ad) LIKE UPPER('%" + getKurumRaporlamaFilter().getKurumadi() + "%'))" );
		}
		
		if (getKurumRaporlamaFilter().getKurumturu()!=null){
			whereCon.append(" AND o.kurumTuruRef.rID="+ getKurumRaporlamaFilter().getKurumturu().getRID());
		}
		
		if (getKurumRaporlamaFilter().getSorumlu()!=null){
			whereCon.append(" AND o.sorumluRef.rID="+ getKurumRaporlamaFilter().getSorumlu().getRID());
		}
		
		if (getKurumRaporlamaFilter().getIl()!=null){
			whereCon.append(" AND (SELECT COUNT(kurumadres.rID) FROM Adres AS kurumadres WHERE kurumadres.kurumRef.rID = o.rID "+
				" AND kurumadres.sehirRef.rID=" + getKurumRaporlamaFilter().getIl().getRID() + ") > 0 ");
		}
		if (getKurumRaporlamaFilter().getIlce()!=null){
			whereCon.append(" AND (SELECT COUNT(kurumadres.rID) FROM Adres AS kurumadres WHERE kurumadres.kurumRef.rID = o.rID "+
					" AND kurumadres.ilceRef.rID=" + getKurumRaporlamaFilter().getIlce().getRID() + ") > 0 ");
		}
		
		if (getKurumRaporlamaFilter().getTelefonYok()){
			whereCon.append(" AND (SELECT COUNT(tlfn.rID) FROM Telefon AS tlfn WHERE tlfn.kurumRef.rID=o.rID AND tlfn.varsayilan=" + EvetHayir._EVET.getCode()+") = 0");
		}

		if (getKurumRaporlamaFilter().getEpostaYok()){
			whereCon.append(" AND (SELECT COUNT(eposta.rID) FROM Internetadres AS eposta WHERE eposta.kurumRef.rID=o.rID AND eposta.varsayilan=" + EvetHayir._EVET.getCode()+") = 0");
		}
		if (getKurumRaporlamaFilter().getIsyeritemsilcileri()!=null){
			whereCon.append(" AND (SELECT COUNT(temsilcileri.rID) FROM Isyeritemsilcileri AS temsilcileri WHERE temsilcileri.kurumRef.rID = o.rID "+
					" AND temsilcileri.uyeRef.rID=" + getKurumRaporlamaFilter().getIsyeritemsilcileri().getRID() + ") > 0 ");
		}

		
//		logYaz("SORGU KRITERI : " + whereCon.toString()); 
		int count = getDBOperator().recordCount(Kurum.class.getSimpleName(), whereCon.toString());
//		logYaz("BULUNAN UYE SAYISI :" + count); 
		if(count > 1500){
			createGenericMessage("DIKKAT! Cok fazla kayit (" + count + ") bulundu! Lutfen daha fazla kriter giriniz!", FacesMessage.SEVERITY_ERROR);
			return;
		}
		setList(getDBOperator().load(Kurum.class.getSimpleName(), whereCon.toString(), "o.rID"));
		
		if(isEmpty(getList())) {
			createGenericMessage(KeyUtil.getMessageValue("kayit.bulunamadi"), FacesMessage.SEVERITY_WARN);
		} else {
			setWhereCondition(whereCon.toString());
		}
	}  
	
	
	/* KULLANICI RAPOR KAYDETME */
	private String whereCondition;
	private String kullaniciRaporAdi;
	
	public String getKullaniciRaporAdi() {
		return kullaniciRaporAdi;
	}

	public void setKullaniciRaporAdi(String kullaniciRaporAdi) {
		this.kullaniciRaporAdi = kullaniciRaporAdi;
	}	

	public String getWhereCondition() {
		return whereCondition;
	}

	public void setWhereCondition(String whereCondition) {
		this.whereCondition = whereCondition;
	}
	
	public void saveKullaniciRapor() throws DBException{
		if(isEmpty(getList())) {
			createGenericMessage(KeyUtil.getMessageValue("kayit.bulunamadi"), FacesMessage.SEVERITY_WARN);
		} else if(isEmpty(getKullaniciRaporAdi())){
			createGenericMessage(KeyUtil.getMessageValue("kullaniciRaporAdi.giriniz"), FacesMessage.SEVERITY_ERROR);
		} else {
			 Kullanicirapor rapor = new Kullanicirapor();
			 rapor.setKullaniciRef(getSessionUser().getKullanici());
			 rapor.setRaporadi(getKullaniciRaporAdi());
			 rapor.setTarih(new Date());
			 rapor.setRaporTuru("Kurum");
			 rapor.setWherecondition(getWhereCondition());
			 if(getDBOperator().recordCount(Kullanicirapor.class.getSimpleName(), "o.kullaniciRef.rID=" + rapor.getKullaniciRef().getRID() + " AND o.raporadi='" + rapor.getRaporadi() + "'") > 0){
				 createGenericMessage(KeyUtil.getMessageValue("kullaniciRaporAdi.mukerrer"), FacesMessage.SEVERITY_WARN);
				 return;
			 }
			 getDBOperator().insert(rapor);
			 createGenericMessage(KeyUtil.getMessageValue("kayit.eklendi"), FacesMessage.SEVERITY_INFO);
			 setKullaniciRaporAdi("");
		}
	}
	/*********************************************************************/
	
	/* SECILEN RAPORUN CALISTIRILMASI */
	private Long calistirilacakRaporRID;
	

	public Long getCalistirilacakRaporRID() {
		return calistirilacakRaporRID;
	}

	public void setCalistirilacakRaporRID(Long calistirilacakRaporRID) {
		this.calistirilacakRaporRID = calistirilacakRaporRID;
	}
	
	private List<Kullanicirapor> kullaniciRaporListesi;
	 
	public List<Kullanicirapor> getKullaniciRaporListesi() {
		return kullaniciRaporListesi;
	}

	public void setKullaniciRaporListesi(List<Kullanicirapor> kullaniciRaporListesi) {
		this.kullaniciRaporListesi = kullaniciRaporListesi;
	}

	public SelectItem[] kullaniciRaporList;
	
	public void setKullaniciRaporList(SelectItem[] kullaniciRaporList) {
		this.kullaniciRaporList = kullaniciRaporList;
	}
	@SuppressWarnings("unchecked")
	public SelectItem[] getKullaniciRaporList() {  
		List<Kullanicirapor> entityList = null;
		if(kullaniciRaporList != null && kullaniciRaporList.length > 0){ 
			return kullaniciRaporList;
		}
		if(getKullaniciRaporListesi() != null) { 
			entityList =  getKullaniciRaporListesi();
		} else { 
			entityList = getDBOperator().load(Kullanicirapor.class.getSimpleName(), " o.kullaniciRef = " + getSessionUser().getKullanici().getRID() + " AND o.raporTuru = 'Kurum'", "o.raporadi");
		} 
		if (entityList != null) {
			int i = 0;
			SelectItem[] raporList = null; 
			raporList = new SelectItem[entityList.size() + 1];
			raporList[0] = new SelectItem(null, "");
			i = 0;  
			for (BaseEntity entity : entityList) {
				i++;
				raporList[i] = new SelectItem(entity, entity.getUIString());
			}
			setKullaniciRaporList(raporList);
			return raporList; 
		}
		return new SelectItem[0];
	}	

	@SuppressWarnings("unchecked")
	public void executeKullaniciRapor(){
		if (getKurumRaporlamaFilter().getKullanicirapor()!=null){
			logYaz("Calistirilacak rapor adi = " +getKurumRaporlamaFilter().getKullanicirapor().getRaporadi());
			Kullanicirapor rapor = (Kullanicirapor) getDBOperator().find(Kullanicirapor.class.getSimpleName(), "rID", getKurumRaporlamaFilter().getKullanicirapor().getRID() + "").get(0);		
			setList(getDBOperator().load(Kurum.class.getSimpleName(), rapor.getWherecondition(), "o.rID"));
		}
		if(isEmpty(getList())) {
			createGenericMessage(KeyUtil.getMessageValue("kayit.bulunamadi"), FacesMessage.SEVERITY_WARN);
		} 
		getKurumRaporlamaFilter().setKullanicirapor(null);
	}
	/*********************************************************************/
	
	
	private SelectItem[] kullaniciRaporDegistir;
	
	public SelectItem[] getKullaniciRaporDegistir() {
		return kullaniciRaporDegistir;
	}

	public void setKullaniciRaporDegistir(SelectItem[] kullaniciRaporDegistir) {
		this.kullaniciRaporDegistir = kullaniciRaporDegistir;
	}

	public void handleChangeKullaniciRapor(AjaxBehaviorEvent event) {
		try { 
			HtmlSelectOneMenu menu = (HtmlSelectOneMenu) event.getComponent(); 			
			if(menu.getValue() instanceof Kullanicirapor){ 
				setCalistirilacakRaporRID(((Kullanicirapor) menu.getValue()).getRID());
			}else {
				setCalistirilacakRaporRID(0L);
			}
			
		} catch(Exception e){
			logYaz("Error KurumRaporlamaController @handleChangeKullaniciRapor :" + e.getMessage());
			logYaz("Exception @" + getModelName() + "Controller :", e);
		}
	}

	
} // class 
