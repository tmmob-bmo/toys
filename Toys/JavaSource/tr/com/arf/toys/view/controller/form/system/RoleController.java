package tr.com.arf.toys.view.controller.form.system;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import tr.com.arf.framework.db._management.DBOperator;
import tr.com.arf.framework.db.enumerated._common.EvetHayir;
import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.framework.utility.tool.KeyUtil;
import tr.com.arf.toys.db.filter.system.RoleFilter;
import tr.com.arf.toys.db.model.system.Islem;
import tr.com.arf.toys.db.model.system.IslemRole;
import tr.com.arf.toys.db.model.system.Role;
import tr.com.arf.toys.service.application.ApplicationDescriptor;
import tr.com.arf.toys.service.application.ManagedBeanLocator;
import tr.com.arf.toys.view.controller.form._base.ToysFilteredControllerNoPaging;

@ManagedBean
@ViewScoped
public class RoleController extends ToysFilteredControllerNoPaging {

	private static final long serialVersionUID = 6531152165884980748L;

	private Role role; 
	private RoleFilter roleFilter = new RoleFilter();
	private EvetHayir varsayilanYetki;

	public RoleController() {
		super(Role.class); 
		setDefaultValues(false); 
		queryAction();  
		setOrderField("name");
		setAutoCompleteSearchColumns(new String[]{"name"});
	} 

	public Role getRole() {
		role = (Role) getEntity();
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public RoleFilter getRoleFilter() {
		return roleFilter;
	}

	public void setRoleFilter(RoleFilter roleFilter) {
		this.roleFilter = roleFilter;
	}

	public EvetHayir getVarsayilanYetki() {
		return varsayilanYetki;
	}

	public void setVarsayilanYetki(EvetHayir varsayilanYetki) {
		this.varsayilanYetki = varsayilanYetki;
	}

	@Override
	public BaseFilter getFilter() {
		return getRoleFilter();
	} 

	public String islemRole() {	
		if (!setSelected()) {
			return "";
		}	 
		putObjectToSessionFilter(ApplicationDescriptor._ROLE_MODEL, getRole());
		return ManagedBeanLocator.locateMenuController().gotoPage("menu_IslemRole"); 
	} 
	
	@Override
	public void insert() {
		super.insert();
		varsayilanYetki = EvetHayir._HAYIR;
	}   

	@Override
	public void save() {  
		DBOperator dbOperator = getDBOperator(); 
		if (getRole().getRID() == null) {
			try { 
				dbOperator.insert(getRole());  
				@SuppressWarnings("unchecked")				
				List<Islem> islemList = dbOperator.load(
						Islem.class.getSimpleName(), "o.rID");
				IslemRole islemRole = new IslemRole();
				if (!isEmpty(islemList)) { 
					for (Islem islem : islemList) {
						islemRole = new IslemRole();
						islemRole.setRoleRef(getRole());
						islemRole.setIslemRef(islem);
						islemRole.setListPermission(varsayilanYetki);
						islemRole.setUpdatePermission(varsayilanYetki);
						islemRole.setDeletePermission(varsayilanYetki);
						dbOperator.insert(islemRole);
					}
				}
			} catch (Exception e) {
				logYaz("Error writing role :" + e.getMessage());
				logYaz("Exception @" + getModelName() + "Controller :", e);
				getRole().setRID(null);
				createGenericMessage(KeyUtil.getMessageValue("islem.hata"), FacesMessage.SEVERITY_ERROR);
			}
			createGenericMessage(KeyUtil.getMessageValue("kayit.eklendi"), FacesMessage.SEVERITY_INFO);
			insert();
			browse();
		} else {
			try {
				if(getRole().getValue().hashCode() == getOldValue().hashCode()){
					createGenericMessage(KeyUtil.getMessageValue("islem.degisiklikYok"), FacesMessage.SEVERITY_INFO); 
					return;
				}
				dbOperator.update(getRole()); 
				createGenericMessage(KeyUtil.getMessageValue("kayit.guncellendi"), FacesMessage.SEVERITY_INFO);
				browse();
			} catch (Exception e) {
				createGenericMessage(KeyUtil.getMessageValue("islem.hata"), FacesMessage.SEVERITY_ERROR);
			}
		}
	}

} // class 
