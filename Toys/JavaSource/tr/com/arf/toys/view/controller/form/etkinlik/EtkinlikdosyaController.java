package tr.com.arf.toys.view.controller.form.etkinlik; 
  
 
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.toys.db.filter.etkinlik.EtkinlikdosyaFilter;
import tr.com.arf.toys.db.model.etkinlik.Etkinlik;
import tr.com.arf.toys.db.model.etkinlik.Etkinlikdosya;
import tr.com.arf.toys.service.application.ApplicationDescriptor;
import tr.com.arf.toys.view.controller.form._base.ToysBaseFilteredController;
  
  
@ManagedBean 
@ViewScoped 
public class EtkinlikdosyaController extends ToysBaseFilteredController {  
  
  private static final long serialVersionUID = 6531152165884980748L; 
  
	private Etkinlikdosya etkinlikdosya;  
	private EtkinlikdosyaFilter etkinlikdosyaFilter = new EtkinlikdosyaFilter();  
  
	public EtkinlikdosyaController() { 
		super(Etkinlikdosya.class);  
		setDefaultValues(false);  
		setOrderField("ad");  
		setLoggable(false);  
		setAutoCompleteSearchColumns(new String[]{"ad"}); 
		setTable(null); 
		if(getMasterEntity() != null){ 
			setMasterObjectName(ApplicationDescriptor._ETKINLIK_MODEL);
			setMasterOutcome(ApplicationDescriptor._ETKINLIK_MODEL);
			getEtkinlikdosyaFilter().setEtkinlikRef(getMasterEntity());
			getEtkinlikdosyaFilter().createQueryCriterias();   
		} 
		queryAction(); 
	} 
 
	public Etkinlik getMasterEntity(){
		if(getEtkinlikdosyaFilter().getEtkinlikRef() != null){
			return getEtkinlikdosyaFilter().getEtkinlikRef();
		} else {
			return (Etkinlik) getObjectFromSessionFilter(ApplicationDescriptor._ETKINLIK_MODEL);
		}			
	} 
	
	public Etkinlikdosya getEtkinlikdosya() { 
		etkinlikdosya = (Etkinlikdosya) getEntity(); 
		return etkinlikdosya; 
	} 
	
	public void setEtkinlikdosya(Etkinlikdosya etkinlikdosya) { 
		this.etkinlikdosya = etkinlikdosya; 
	} 
	
	public EtkinlikdosyaFilter getEtkinlikdosyaFilter() { 
		return etkinlikdosyaFilter; 
	} 
	 
	public void setEtkinlikdosyaFilter(EtkinlikdosyaFilter etkinlikdosyaFilter) {  
		this.etkinlikdosyaFilter = etkinlikdosyaFilter;  
	}  
	 
	@Override  
	public BaseFilter getFilter() {  
		return getEtkinlikdosyaFilter();  
	}  
		 
	@Override 
	public void resetFilter() {
		super.resetFilterOnly();
		if(getMasterEntity() != null){
			getEtkinlikdosyaFilter().setEtkinlikRef(getMasterEntity()); 
		}
		queryAction();
	}
 
	@Override	
	public void insert() {	
		super.insert();	
		getEtkinlikdosya().setEtkinlikRef(getEtkinlikdosyaFilter().getEtkinlikRef());	
	}	
	
} // class 
