package tr.com.arf.toys.view.controller._common;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Collections;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.model.SelectItem;

import tr.com.arf.framework.db.enumerated._common.EvetHayir;
import tr.com.arf.framework.db.enumerated._common.IslemTuru;
import tr.com.arf.framework.db.enumerated._common.VarYokTumu;
import tr.com.arf.toys.db.enumerated.egitim.Cevap;
import tr.com.arf.toys.db.enumerated.egitim.EgitimDurum;
import tr.com.arf.toys.db.enumerated.egitim.EgitimUcretTipi;
import tr.com.arf.toys.db.enumerated.egitim.EgitimkatilimciDurum;
import tr.com.arf.toys.db.enumerated.egitim.Egitimturu;
import tr.com.arf.toys.db.enumerated.egitim.EtkinDurum;
import tr.com.arf.toys.db.enumerated.egitim.KatilimTuru;
import tr.com.arf.toys.db.enumerated.egitim.Katilimbelgesi;
import tr.com.arf.toys.db.enumerated.egitim.KatilimciTuru;
import tr.com.arf.toys.db.enumerated.egitim.Sertifika;
import tr.com.arf.toys.db.enumerated.egitim.SinavDurum;
import tr.com.arf.toys.db.enumerated.egitim.SinavTuru;
import tr.com.arf.toys.db.enumerated.egitim.SinavbasariDurum;
import tr.com.arf.toys.db.enumerated.egitim.SonucTuru;
import tr.com.arf.toys.db.enumerated.egitim.Soru;
import tr.com.arf.toys.db.enumerated.etkinlik.Dosyaturu;
import tr.com.arf.toys.db.enumerated.etkinlik.EtkinlikDurum;
import tr.com.arf.toys.db.enumerated.etkinlik.EtkinlikKurumTuru;
import tr.com.arf.toys.db.enumerated.etkinlik.EtkinlikTipi;
import tr.com.arf.toys.db.enumerated.etkinlik.KatilimciDurum;
import tr.com.arf.toys.db.enumerated.etkinlik.Kurulturu;
import tr.com.arf.toys.db.enumerated.etkinlik.Uyeturu;
import tr.com.arf.toys.db.enumerated.evrak.AciliyetTuru;
import tr.com.arf.toys.db.enumerated.evrak.EvrakDurum;
import tr.com.arf.toys.db.enumerated.evrak.EvrakHareketTuru;
import tr.com.arf.toys.db.enumerated.evrak.EvrakTuru;
import tr.com.arf.toys.db.enumerated.evrak.GizlilikDerecesi;
import tr.com.arf.toys.db.enumerated.evrak.HazirEvrakTuru;
import tr.com.arf.toys.db.enumerated.evrak.TeslimTuru;
import tr.com.arf.toys.db.enumerated.finans.BorcDurum;
import tr.com.arf.toys.db.enumerated.finans.OdemeTuru;
import tr.com.arf.toys.db.enumerated.iletisim.AdresTipi;
import tr.com.arf.toys.db.enumerated.iletisim.AdresTuru;
import tr.com.arf.toys.db.enumerated.iletisim.GonderimDurum;
import tr.com.arf.toys.db.enumerated.iletisim.GonderimTuru;
import tr.com.arf.toys.db.enumerated.iletisim.IcerikTuru;
import tr.com.arf.toys.db.enumerated.iletisim.ListeGizlilikTuru;
import tr.com.arf.toys.db.enumerated.iletisim.NetAdresTuru;
import tr.com.arf.toys.db.enumerated.iletisim.ReferansTipi;
import tr.com.arf.toys.db.enumerated.iletisim.TelefonTuru;
import tr.com.arf.toys.db.enumerated.kisi.Cinsiyet;
import tr.com.arf.toys.db.enumerated.kisi.GorevTuru;
import tr.com.arf.toys.db.enumerated.kisi.KanGrubu;
import tr.com.arf.toys.db.enumerated.kisi.KimlikTip;
import tr.com.arf.toys.db.enumerated.kisi.KisiDurum;
import tr.com.arf.toys.db.enumerated.kisi.KisiIzinDurum;
import tr.com.arf.toys.db.enumerated.kisi.KisiTuru;
import tr.com.arf.toys.db.enumerated.kisi.KisiUyrukTip;
import tr.com.arf.toys.db.enumerated.kisi.KomisyonGorev;
import tr.com.arf.toys.db.enumerated.kisi.KomisyonUyelikDurumu;
import tr.com.arf.toys.db.enumerated.kisi.KurulTuru;
import tr.com.arf.toys.db.enumerated.kisi.KurulUyelikDurumu;
import tr.com.arf.toys.db.enumerated.kisi.KurumTipi;
import tr.com.arf.toys.db.enumerated.kisi.MedeniHal;
import tr.com.arf.toys.db.enumerated.kisi.ToplantiTuru;
import tr.com.arf.toys.db.enumerated.kisi.UyeTuru;
import tr.com.arf.toys.db.enumerated.kisi.VerilisNedeni;
import tr.com.arf.toys.db.enumerated.system.BirimDurum;
import tr.com.arf.toys.db.enumerated.system.BirimTip;
import tr.com.arf.toys.db.enumerated.system.BirimYetkiTuru;
import tr.com.arf.toys.db.enumerated.system.DemirbasDurum;
import tr.com.arf.toys.db.enumerated.system.DemirbasTuru;
import tr.com.arf.toys.db.enumerated.system.IdariYapiTip;
import tr.com.arf.toys.db.enumerated.system.KomisyonDurum;
import tr.com.arf.toys.db.enumerated.system.KpsTuru;
import tr.com.arf.toys.db.enumerated.system.OnemDerece;
import tr.com.arf.toys.db.enumerated.system.PersonelDurum;
import tr.com.arf.toys.db.enumerated.system.PersonelGorevDurum;
import tr.com.arf.toys.db.enumerated.system.RaporTuru;
import tr.com.arf.toys.db.enumerated.system.UniversiteDurum;
import tr.com.arf.toys.db.enumerated.system.UyariDurum;
import tr.com.arf.toys.db.enumerated.system.UyariTipi;
import tr.com.arf.toys.db.enumerated.system.YayinTip;
import tr.com.arf.toys.db.enumerated.uye.Formturu;
import tr.com.arf.toys.db.enumerated.uye.IletisimTip;
import tr.com.arf.toys.db.enumerated.uye.OdemeSekli;
import tr.com.arf.toys.db.enumerated.uye.OdemeTip;
import tr.com.arf.toys.db.enumerated.uye.OgrenimTip;
import tr.com.arf.toys.db.enumerated.uye.SigortaTip;
import tr.com.arf.toys.db.enumerated.uye.SiparisDurum;
import tr.com.arf.toys.db.enumerated.uye.UyeAboneDurum;
import tr.com.arf.toys.db.enumerated.uye.UyeAidatDurum;
import tr.com.arf.toys.db.enumerated.uye.UyeBelgeDurum;
import tr.com.arf.toys.db.enumerated.uye.UyeBilirkisiDurum;
import tr.com.arf.toys.db.enumerated.uye.UyeDurum;
import tr.com.arf.toys.db.enumerated.uye.UyeKayitDurum;
import tr.com.arf.toys.db.enumerated.uye.UyeMuafiyetTip;
import tr.com.arf.toys.db.enumerated.uye.UyeRaporlamaAboneDurum;
import tr.com.arf.toys.db.enumerated.uye.UyeSigortaDurum;
import tr.com.arf.toys.db.enumerated.uye.UyeTip;
import tr.com.arf.toys.db.enumerated.uye.UyekurumDurum;

@ManagedBean(eager = true)
@ApplicationScoped
public class EnumeratedController implements Serializable {

	public EnumeratedController() {
		setSiparisDurumItems();
		setFormturuItems();
		setVarYokTumuItems();
		setRaporTuruItems();
		setRaporTuruItems();
		setEvrakHareketTuruItems();
		setCevapItems();
		setSoruItems();
		setSinavbasariDurumItems();
		setSertifikaItems();
		setKatilimbelgesiItems();
		setEgitimkatilimciDurumItems();
		setEgitimDurumItems();
		setEgitimturuItems();
		setDosyaturuItems();
		setEtkinlikKurumTuruItems();
		setEtkinlikTipiItems();
		setUyeturuItems();
		setKurulturuItems();
		setKatilimciDurumItems();
		setEtkinlikDurumItems();
		setIcerikTuruItems();
		setGonderimDurumItems();
		setGonderimTuruItems();
		setReferansTipiItems();
		setListeGizlilikTuruItems();
		setToplantiTuruItems();
		setUyeTuruItems();
		setKomisyonGorevItems();
		setEvrakHareketTuruItems();
		setEvrakDurumItems();
		setAciliyetTuruItems();
		setGizlilikDerecesiItems();
		setTeslimTuruItems();
		setEvrakTuruItems();
		setOdemeTuruItems();
		setAdresTipiItems();
		setAdresTuruItems();
		setAdresTuruKPSYokItems();
		setUyekurumDurumItems();
		setIslemTuruItems();
		setKomisyonDurumItems();
		setPersonelGorevDurumItems();
		setYayinTipItems();
		setBorcDurumItems();
		setUyeAboneDurumItems();
		setUyariDurumItems();
		setUyariTipiItems();
		setUyeSigortaDurumItems();
		setSigortaTipItems();
		setUyeBilirkisiDurumItems();
		setUyeBelgeDurumItems();
		setUyeBelgeDurumSecilebilenItems();
		// setUyekurumDurumItems();
		setOdemeTipItems();
		setUyeMuafiyetTipItems();
		setUyeAidatDurumItems();
		setUyeKayitDurumItems();
		setUyeDurumItems();
		setUyeTipItems();
		setUyeTipCalisanItems();
		setKanGrubuItems();
		setCinsiyetItems();
		setKisiIzinDurumItems();
		setKurumTipiItems();
		setKimlikTipItems();
		setNetAdresTuruItems();
		setTelefonTuruItems();
		setPersonelDurumItems();
		setBirimTipItems();
		setBirimDurumItems();
		setPersonelDurumItems();
		setBirimDurumItems();
		setEvetHayirItems();
		setIdariYapiTipItems();
		setOgrenimTipItems();
		setIletisimTipItems();
		setKisiUyrukTipItems();
		setAdresTuruItems();
		setMedeniHalItems();
		setVerilisNedeniItems();
		setAdresTipiItems();
		setKisiDurumItems();
		setOnemDereceItems();
		setKurulTuruItems();
		setGorevTuruItems();
		setUniversiteDurumItems();
		setTelefonTuruKurumItems();
		setKurulUyelikDurumuItems();
		setKomisyonUyelikDurumuItems();
		setUyeRaporlamaAboneDurumItems();
		setBirimYetkiTuruItems();
		setAdresTipiItemsForKisi();
		setHazirEvrakTuruItems();
		setOgrenimTipLisansYokItems();
		setOgrenciTelefonTuruItems();
		setOgrenciAdresTuruItems();
		setOgrenciOgrenimTipItems();
		setDemirbasTuruItems();
		setDemirbasDurumItems();
		setOdemeSekliItems();
		setOdemeSekliVeznedenItems();
		setKpsTuruItems();
		setSinavTuruItems();
		setSinavDurumItems();
		setKisiTuruItems();
		setSonucTuruItems();
		setKatilimTuruItems();
		setEtkinDurumItems();
		setKatilimciTuruItems();
		setEgitimUcretTipiItems();
	}

	private SelectItem[] HazirEvrakTuruItems;

	public SelectItem[] getHazirEvrakTuruItems() {
		return HazirEvrakTuruItems;
	}

	public void setHazirEvrakTuruItems() {
		HazirEvrakTuruItems = new SelectItem[HazirEvrakTuru.values().length - 1];
		for (int x = 0; x < HazirEvrakTuru.values().length - 1; x++) {
			HazirEvrakTuruItems[x] = new SelectItem(HazirEvrakTuru.values()[x + 1], HazirEvrakTuru.values()[x + 1].getLabel());
		}
	}

	private static final long serialVersionUID = 2517780123112750303L;

	private SelectItem[] BirimYetkiTuruItems;

	public SelectItem[] getBirimYetkiTuruItems() {
		return BirimYetkiTuruItems;
	}

	public void setBirimYetkiTuruItems() {
		BirimYetkiTuruItems = new SelectItem[BirimYetkiTuru.values().length - 1];
		for (int x = 0; x < BirimYetkiTuru.values().length - 1; x++) {
			BirimYetkiTuruItems[x] = new SelectItem(BirimYetkiTuru.values()[x + 1], BirimYetkiTuru.values()[x + 1].getLabel());
		}
	}

	private SelectItem[] UyeRaporlamaAboneDurumItems;

	public SelectItem[] getUyeRaporlamaAboneDurumItems() {
		return UyeRaporlamaAboneDurumItems;
	}

	public void setUyeRaporlamaAboneDurumItems() {
		UyeRaporlamaAboneDurumItems = new SelectItem[UyeRaporlamaAboneDurum.values().length - 1];
		for (int x = 0; x < UyeRaporlamaAboneDurum.values().length - 1; x++) {
			UyeRaporlamaAboneDurumItems[x] = new SelectItem(UyeRaporlamaAboneDurum.values()[x + 1], UyeRaporlamaAboneDurum.values()[x + 1].getLabel());
		}
	}

	/* Ortak */
	private SelectItem[] evetHayirItems;

	public SelectItem[] getEvetHayirItems() {
		return evetHayirItems;
	}

	public SelectItem[] getEvetHayirItemsReverse() {
		SelectItem[] temp = getEvetHayirItems();
		Collections.reverse(Arrays.asList(temp));
		return temp;
	}

	public void setEvetHayirItems() {
		evetHayirItems = new SelectItem[EvetHayir.values().length - 1];
		for (int x = 0; x < EvetHayir.values().length - 1; x++) {
			evetHayirItems[x] = new SelectItem(EvetHayir.values()[x + 1], EvetHayir.values()[x + 1].getLabel());
		}
	}

	private SelectItem[] varYokTumuItems;

	public SelectItem[] getVarYokTumuItems() {
		return varYokTumuItems;
	}

	public void setVarYokTumuItems() {
		varYokTumuItems = new SelectItem[VarYokTumu.values().length - 1];
		for (int x = 0; x < VarYokTumu.values().length - 1; x++) {
			varYokTumuItems[x] = new SelectItem(VarYokTumu.values()[x + 1], VarYokTumu.values()[x + 1].getLabel());
		}
	}

	private SelectItem[] BirimDurumItems;

	public SelectItem[] getBirimDurumItems() {
		return BirimDurumItems;
	}

	public void setBirimDurumItems() {
		BirimDurumItems = new SelectItem[BirimDurum.values().length - 1];
		for (int x = 0; x < BirimDurum.values().length - 1; x++) {
			BirimDurumItems[x] = new SelectItem(BirimDurum.values()[x + 1], BirimDurum.values()[x + 1].getLabel());
		}
	}

	private SelectItem[] PersonelDurumItems;

	public SelectItem[] getPersonelDurumItems() {
		return PersonelDurumItems;
	}

	public void setPersonelDurumItems() {
		PersonelDurumItems = new SelectItem[PersonelDurum.values().length - 1];
		for (int x = 0; x < PersonelDurum.values().length - 1; x++) {
			PersonelDurumItems[x] = new SelectItem(PersonelDurum.values()[x + 1], PersonelDurum.values()[x + 1].getLabel());
		}
	}

	private SelectItem[] KurumTuruItems;

	public SelectItem[] getKurumTuruItems() {
		return KurumTuruItems;
	}

	private SelectItem[] idariYapiTipItems;

	public SelectItem[] getIdariYapiTipItems() {
		return idariYapiTipItems;
	}

	public void setIdariYapiTipItems() {
		idariYapiTipItems = new SelectItem[IdariYapiTip.values().length - 1];
		for (int x = 0; x < IdariYapiTip.values().length - 1; x++) {
			idariYapiTipItems[x] = new SelectItem(IdariYapiTip.values()[x + 1], IdariYapiTip.values()[x + 1].getLabel());
		}
	}

	private SelectItem[] BirimTipItems;

	public SelectItem[] getBirimTipItems() {
		return BirimTipItems;
	}

	public void setBirimTipItems() {

		BirimTipItems = new SelectItem[BirimTip.values().length - 1];
		for (int x = 0; x < BirimTip.values().length - 1; x++) {
			BirimTipItems[x] = new SelectItem(BirimTip.values()[x + 1], BirimTip.values()[x + 1].getLabel());
		}
	}

	private SelectItem[] TelefonTuruItems;

	public SelectItem[] getTelefonTuruItems() {
		return TelefonTuruItems;
	}

	public void setTelefonTuruItems() {
		TelefonTuruItems = new SelectItem[TelefonTuru.values().length - 1];
		for (int x = 0; x < TelefonTuru.values().length - 1; x++) {
			TelefonTuruItems[x] = new SelectItem(TelefonTuru.values()[x + 1], TelefonTuru.values()[x + 1].getLabel());
		}
	}

	private SelectItem[] OgrenciTelefonTuruItems;

	public SelectItem[] getOgrenciTelefonTuruItems() {
		return OgrenciTelefonTuruItems;
	}

	public void setOgrenciTelefonTuruItems() {
		OgrenciTelefonTuruItems = new SelectItem[2];
		OgrenciTelefonTuruItems[0] = new SelectItem(TelefonTuru.values()[1], TelefonTuru.values()[1].getLabel());
		OgrenciTelefonTuruItems[1] = new SelectItem(TelefonTuru.values()[3], TelefonTuru.values()[3].getLabel());
	}

	private SelectItem[] NetAdresTuruItems;

	public SelectItem[] getNetAdresTuruItems() {
		return NetAdresTuruItems;
	}

	public void setNetAdresTuruItems() {
		NetAdresTuruItems = new SelectItem[NetAdresTuru.values().length - 1];
		for (int x = 0; x < NetAdresTuru.values().length - 1; x++) {
			NetAdresTuruItems[x] = new SelectItem(NetAdresTuru.values()[x + 1], NetAdresTuru.values()[x + 1].getLabel());
		}
	}

	private SelectItem[] AdresTuruItems;

	public SelectItem[] getAdresTuruItems() {
		return AdresTuruItems;
	}

	public void setAdresTuruItems() {
		AdresTuruItems = new SelectItem[AdresTuru.values().length - 2];
		for (int x = 0; x < AdresTuru.values().length - 2; x++) {
			AdresTuruItems[x] = new SelectItem(AdresTuru.values()[x + 1], AdresTuru.values()[x + 1].getLabel());
		}
	}

	private SelectItem[] AdresTuruKPSYokItems;

	public SelectItem[] getAdresTuruKPSYokItems() {
		return AdresTuruKPSYokItems;
	}

	public void setAdresTuruKPSYokItems() {
		AdresTuruKPSYokItems = new SelectItem[AdresTuru.values().length - 3];
		for (int x = 0; x < AdresTuru.values().length - 3; x++) {
			AdresTuruKPSYokItems[x] = new SelectItem(AdresTuru.values()[x + 1], AdresTuru.values()[x + 1].getLabel());
		}
	}

	private SelectItem[] KimlikTipItems;

	public SelectItem[] getKimlikTipItems() {
		return KimlikTipItems;
	}

	public void setKimlikTipItems() {
		KimlikTipItems = new SelectItem[KimlikTip.values().length - 1];
		for (int x = 0; x < KimlikTip.values().length - 1; x++) {
			KimlikTipItems[x] = new SelectItem(KimlikTip.values()[x + 1], KimlikTip.values()[x + 1].getLabel());
		}
	}

	private SelectItem[] KurumTipiItems;

	public SelectItem[] getKurumTipiItems() {
		return KurumTipiItems;
	}

	public void setKurumTipiItems() {
		KurumTipiItems = new SelectItem[KurumTipi.values().length - 1];
		for (int x = 0; x < KurumTipi.values().length - 1; x++) {
			KurumTipiItems[x] = new SelectItem(KurumTipi.values()[x + 1], KurumTipi.values()[x + 1].getLabel());
		}
	}

	private SelectItem[] KisiIzinDurumItems;

	public SelectItem[] getKisiIzinDurumItems() {
		return KisiIzinDurumItems;
	}

	public void setKisiIzinDurumItems() {
		KisiIzinDurumItems = new SelectItem[KisiIzinDurum.values().length - 1];
		for (int x = 0; x < KisiIzinDurum.values().length - 1; x++) {
			KisiIzinDurumItems[x] = new SelectItem(KisiIzinDurum.values()[x + 1], KisiIzinDurum.values()[x + 1].getLabel());
		}
	}

	private SelectItem[] CinsiyetItems;

	public SelectItem[] getCinsiyetItems() {
		return CinsiyetItems;
	}

	public void setCinsiyetItems() {
		CinsiyetItems = new SelectItem[Cinsiyet.values().length - 1];
		for (int x = 0; x < Cinsiyet.values().length - 1; x++) {
			CinsiyetItems[x] = new SelectItem(Cinsiyet.values()[x + 1], Cinsiyet.values()[x + 1].getLabel());
		}
	}

	private SelectItem[] KanGrubuItems;

	public SelectItem[] getKanGrubuItems() {
		return KanGrubuItems;
	}

	public void setKanGrubuItems() {
		KanGrubuItems = new SelectItem[KanGrubu.values().length];
		KanGrubu[] kanGrubus = KanGrubu.values();
		for (int x = 1; x < kanGrubus.length; x++) {
			KanGrubuItems[x] = new SelectItem(kanGrubus[x], kanGrubus[x].getLabel());
		}
		KanGrubuItems[0] = new SelectItem(null, "Seçiniz");
	}

	private SelectItem[] UyeTipItems;

	public SelectItem[] getUyeTipItems() {
		return UyeTipItems;
	}

	public void setUyeTipItems() {
		UyeTipItems = new SelectItem[UyeTip.values().length - 1];
		for (int x = 0; x < UyeTip.values().length - 1; x++) {
			UyeTipItems[x] = new SelectItem(UyeTip.values()[x + 1], UyeTip.values()[x + 1].getLabel());
		}
	}

	private SelectItem[] UyeTipCalisanItems;

	public SelectItem[] getUyeTipCalisanItems() {
		return UyeTipCalisanItems;
	}

	public void setUyeTipCalisanItems() {
		UyeTipCalisanItems = new SelectItem[UyeTip.values().length - 2];
		for (int x = 0; x < UyeTip.values().length - 2; x++) {
			UyeTipCalisanItems[x] = new SelectItem(UyeTip.values()[x + 1], UyeTip.values()[x + 1].getLabel());
		}
	}

	private SelectItem[] UyeDurumItems;

	public SelectItem[] getUyeDurumItems() {
		return UyeDurumItems;
	}

	public void setUyeDurumItems() {
		UyeDurumItems = new SelectItem[UyeDurum.values().length - 1];
		for (int x = 0; x < UyeDurum.values().length - 1; x++) {
			UyeDurumItems[x] = new SelectItem(UyeDurum.values()[x + 1], UyeDurum.values()[x + 1].getLabel());
		}
	}

	private SelectItem[] UyeKayitDurumItems;

	public SelectItem[] getUyeKayitDurumItems() {
		return UyeKayitDurumItems;
	}

	public void setUyeKayitDurumItems() {
		UyeKayitDurumItems = new SelectItem[UyeKayitDurum.values().length - 1];
		for (int x = 0; x < UyeKayitDurum.values().length - 1; x++) {
			UyeKayitDurumItems[x] = new SelectItem(UyeKayitDurum.values()[x + 1], UyeKayitDurum.values()[x + 1].getLabel());
		}
	}

	private SelectItem[] UyeAidatDurumItems;

	public SelectItem[] getUyeAidatDurumItems() {
		return UyeAidatDurumItems;
	}

	public void setUyeAidatDurumItems() {
		UyeAidatDurumItems = new SelectItem[UyeAidatDurum.values().length - 1];
		for (int x = 0; x < UyeAidatDurum.values().length - 1; x++) {
			UyeAidatDurumItems[x] = new SelectItem(UyeAidatDurum.values()[x + 1], UyeAidatDurum.values()[x + 1].getLabel());
		}
	}

	private SelectItem[] UyeMuafiyetTipItems;

	public SelectItem[] getUyeMuafiyetTipItems() {
		return UyeMuafiyetTipItems;
	}

	public void setUyeMuafiyetTipItems() {
		UyeMuafiyetTipItems = new SelectItem[UyeMuafiyetTip.values().length - 1];
		for (int x = 0; x < UyeMuafiyetTip.values().length - 1; x++) {
			UyeMuafiyetTipItems[x] = new SelectItem(UyeMuafiyetTip.values()[x + 1], UyeMuafiyetTip.values()[x + 1].getLabel());
		}
	}

	private SelectItem[] OdemeTipItems;

	public SelectItem[] getOdemeTipItems() {
		return OdemeTipItems;
	}

	public void setOdemeTipItems() {
		OdemeTipItems = new SelectItem[OdemeTip.values().length - 1];
		for (int x = 0; x < OdemeTip.values().length - 1; x++) {
			OdemeTipItems[x] = new SelectItem(OdemeTip.values()[x + 1], OdemeTip.values()[x + 1].getLabel());
		}
	}

	// private SelectItem[] UyekurumDurumItems;
	//
	// public SelectItem[] getUyekurumDurumItems() {
	// return UyekurumDurumItems;
	// }

	// public void setUyekurumDurumItems() {
	// UyekurumDurumItems = new SelectItem[UyekurumDurum.values().length - 1];
	// for (int x = 0; x < UyekurumDurum.values().length - 1; x++) {
	// UyekurumDurumItems[x] = new SelectItem(
	// UyekurumDurum.values()[x + 1],
	// UyekurumDurum.values()[x + 1].getLabel());
	// }
	// }

	private SelectItem[] UyeBelgeDurumItems;

	public SelectItem[] getUyeBelgeDurumItems() {
		return UyeBelgeDurumItems;
	}

	public void setUyeBelgeDurumItems() {
		UyeBelgeDurumItems = new SelectItem[UyeBelgeDurum.values().length - 1];
		for (int x = 0; x < UyeBelgeDurum.values().length - 1; x++) {
			UyeBelgeDurumItems[x] = new SelectItem(UyeBelgeDurum.values()[x + 1], UyeBelgeDurum.values()[x + 1].getLabel());
		}
	}

	private SelectItem[] UyeBelgeDurumSecilebilenItems;

	public SelectItem[] getUyeBelgeDurumSecilebilenItems() {
		return UyeBelgeDurumSecilebilenItems;
	}

	public void setUyeBelgeDurumSecilebilenItems() {
		UyeBelgeDurumSecilebilenItems = new SelectItem[2];
		UyeBelgeDurumSecilebilenItems[0] = new SelectItem(UyeBelgeDurum._TASLAK, UyeBelgeDurum._TASLAK.getLabel());
		UyeBelgeDurumSecilebilenItems[1] = new SelectItem(UyeBelgeDurum._TALEPEDILMIS, UyeBelgeDurum._TALEPEDILMIS.getLabel());
	}

	private SelectItem[] UyeBilirkisiDurumItems;

	public SelectItem[] getUyeBilirkisiDurumItems() {
		return UyeBilirkisiDurumItems;
	}

	public void setUyeBilirkisiDurumItems() {
		UyeBilirkisiDurumItems = new SelectItem[UyeBilirkisiDurum.values().length - 1];
		for (int x = 0; x < UyeBilirkisiDurum.values().length - 1; x++) {
			UyeBilirkisiDurumItems[x] = new SelectItem(UyeBilirkisiDurum.values()[x + 1], UyeBilirkisiDurum.values()[x + 1].getLabel());
		}
	}

	private SelectItem[] SigortaTipItems;

	public SelectItem[] getSigortaTipItems() {
		return SigortaTipItems;
	}

	public void setSigortaTipItems() {
		SigortaTipItems = new SelectItem[SigortaTip.values().length - 1];
		for (int x = 0; x < SigortaTip.values().length - 1; x++) {
			SigortaTipItems[x] = new SelectItem(SigortaTip.values()[x + 1], SigortaTip.values()[x + 1].getLabel());
		}
	}

	private SelectItem[] UyeSigortaDurumItems;

	public SelectItem[] getUyeSigortaDurumItems() {
		return UyeSigortaDurumItems;
	}

	public void setUyeSigortaDurumItems() {
		UyeSigortaDurumItems = new SelectItem[UyeSigortaDurum.values().length - 1];
		for (int x = 0; x < UyeSigortaDurum.values().length - 1; x++) {
			UyeSigortaDurumItems[x] = new SelectItem(UyeSigortaDurum.values()[x + 1], UyeSigortaDurum.values()[x + 1].getLabel());
		}
	}

	private SelectItem[] UyariTipiItems;

	public SelectItem[] getUyariTipiItems() {
		return UyariTipiItems;
	}

	public void setUyariTipiItems() {
		UyariTipiItems = new SelectItem[UyariTipi.values().length - 1];
		for (int x = 0; x < UyariTipi.values().length - 1; x++) {
			UyariTipiItems[x] = new SelectItem(UyariTipi.values()[x + 1], UyariTipi.values()[x + 1].getLabel());
		}
	}

	private SelectItem[] UyariDurumItems;

	public SelectItem[] getUyariDurumItems() {
		return UyariDurumItems;
	}

	public void setUyariDurumItems() {
		UyariDurumItems = new SelectItem[UyariDurum.values().length - 1];
		for (int x = 0; x < UyariDurum.values().length - 1; x++) {
			UyariDurumItems[x] = new SelectItem(UyariDurum.values()[x + 1], UyariDurum.values()[x + 1].getLabel());
		}
	}

	private SelectItem[] UyeAboneDurumItems;

	public SelectItem[] getUyeAboneDurumItems() {
		return UyeAboneDurumItems;
	}

	public void setUyeAboneDurumItems() {
		UyeAboneDurumItems = new SelectItem[UyeAboneDurum.values().length - 1];
		for (int x = 0; x < UyeAboneDurum.values().length - 1; x++) {
			UyeAboneDurumItems[x] = new SelectItem(UyeAboneDurum.values()[x + 1], UyeAboneDurum.values()[x + 1].getLabel());
		}
	}

	private SelectItem[] BorcDurumItems;

	public SelectItem[] getBorcDurumItems() {
		return BorcDurumItems;
	}

	public void setBorcDurumItems() {
		BorcDurumItems = new SelectItem[BorcDurum.values().length - 1];
		for (int x = 0; x < BorcDurum.values().length - 1; x++) {
			BorcDurumItems[x] = new SelectItem(BorcDurum.values()[x + 1], BorcDurum.values()[x + 1].getLabel());
		}
	}

	private SelectItem[] YayinTipItems;

	public SelectItem[] getYayinTipItems() {
		return YayinTipItems;
	}

	public void setYayinTipItems() {
		YayinTipItems = new SelectItem[YayinTip.values().length - 1];
		for (int x = 0; x < YayinTip.values().length - 1; x++) {
			YayinTipItems[x] = new SelectItem(YayinTip.values()[x + 1], YayinTip.values()[x + 1].getLabel());
		}
	}

	private SelectItem[] OgrenimTipItems;

	public SelectItem[] getOgrenimTipItems() {
		return OgrenimTipItems;
	}

	public void setOgrenimTipItems() {
		OgrenimTipItems = new SelectItem[OgrenimTip.values().length - 1];
		for (int x = 0; x < OgrenimTip.values().length - 1; x++) {
			OgrenimTipItems[x] = new SelectItem(OgrenimTip.values()[x + 1], OgrenimTip.values()[x + 1].getLabel());
		}
	}

	private SelectItem[] OgrenciOgrenimTipItems;

	public SelectItem[] getOgrenciOgrenimTipItems() {
		return OgrenciOgrenimTipItems;
	}

	public void setOgrenciOgrenimTipItems() {
		OgrenciOgrenimTipItems = new SelectItem[1];
		OgrenciOgrenimTipItems[0] = new SelectItem(OgrenimTip.values()[1], OgrenimTip.values()[1].getLabel());
	}

	private SelectItem[] OgrenimTipLisansYokItems;

	public SelectItem[] getOgrenimTipLisansYokItems() {
		return OgrenimTipLisansYokItems;
	}

	public void setOgrenimTipLisansYokItems() {
		OgrenimTipLisansYokItems = new SelectItem[OgrenimTip.values().length - 2];
		for (int x = 0; x < OgrenimTip.values().length - 1; x++) {
			if (x == 1) {
				continue;
			} else if (x == 2) {
				OgrenimTipLisansYokItems[x - 1] = new SelectItem(OgrenimTip.values()[x + 1], OgrenimTip.values()[x + 1].getLabel());
			} else {
				OgrenimTipLisansYokItems[x] = new SelectItem(OgrenimTip.values()[x + 2], OgrenimTip.values()[x + 2].getLabel());
			}
		}
	}

	private SelectItem[] IletisimTipItems;

	public SelectItem[] getIletisimTipItems() {
		return IletisimTipItems;
	}

	public void setIletisimTipItems() {
		IletisimTipItems = new SelectItem[IletisimTip.values().length - 1];
		for (int x = 0; x < IletisimTip.values().length - 1; x++) {
			IletisimTipItems[x] = new SelectItem(IletisimTip.values()[x + 1], IletisimTip.values()[x + 1].getLabel());
		}
	}

	private SelectItem[] KisiUyrukTipItems;

	public SelectItem[] getKisiUyrukTipItems() {
		return KisiUyrukTipItems;
	}

	public void setKisiUyrukTipItems() {
		KisiUyrukTipItems = new SelectItem[KisiUyrukTip.values().length - 1];
		for (int x = 0; x < KisiUyrukTip.values().length - 1; x++) {
			KisiUyrukTipItems[x] = new SelectItem(KisiUyrukTip.values()[x + 1], KisiUyrukTip.values()[x + 1].getLabel());
		}
	}

	private SelectItem[] MedeniHalItems;

	public SelectItem[] getMedeniHalItems() {
		return MedeniHalItems;
	}

	public void setMedeniHalItems() {
		MedeniHalItems = new SelectItem[MedeniHal.values().length - 1];
		for (int x = 0; x < MedeniHal.values().length - 1; x++) {
			MedeniHalItems[x] = new SelectItem(MedeniHal.values()[x + 1], MedeniHal.values()[x + 1].getLabel());
		}
	}

	private SelectItem[] VerilisNedeniItems;

	public SelectItem[] getVerilisNedeniItems() {
		return VerilisNedeniItems;
	}

	public void setVerilisNedeniItems() {
		VerilisNedeniItems = new SelectItem[VerilisNedeni.values().length - 1];
		for (int x = 0; x < VerilisNedeni.values().length - 1; x++) {
			VerilisNedeniItems[x] = new SelectItem(VerilisNedeni.values()[x + 1], VerilisNedeni.values()[x + 1].getLabel());
		}
	}

	private SelectItem[] PersonelGorevDurumItems;

	public SelectItem[] getPersonelGorevDurumItems() {
		return PersonelGorevDurumItems;
	}

	public void setPersonelGorevDurumItems() {
		PersonelGorevDurumItems = new SelectItem[PersonelGorevDurum.values().length - 1];
		for (int x = 0; x < PersonelGorevDurum.values().length - 1; x++) {
			PersonelGorevDurumItems[x] = new SelectItem(PersonelGorevDurum.values()[x + 1], PersonelGorevDurum.values()[x + 1].getLabel());
		}
	}

	private SelectItem[] KomisyonDurumItems;

	public SelectItem[] getKomisyonDurumItems() {
		return KomisyonDurumItems;
	}

	public void setKomisyonDurumItems() {
		KomisyonDurumItems = new SelectItem[KomisyonDurum.values().length - 1];
		for (int x = 0; x < KomisyonDurum.values().length - 1; x++) {
			KomisyonDurumItems[x] = new SelectItem(KomisyonDurum.values()[x + 1], KomisyonDurum.values()[x + 1].getLabel());
		}
	}

	private SelectItem[] IslemTuruItems;

	public SelectItem[] getIslemTuruItems() {
		return IslemTuruItems;
	}

	public void setIslemTuruItems() {
		IslemTuruItems = new SelectItem[IslemTuru.values().length - 1];
		for (int x = 0; x < IslemTuru.values().length - 1; x++) {
			IslemTuruItems[x] = new SelectItem(IslemTuru.values()[x + 1], IslemTuru.values()[x + 1].getLabel());
		}
	}

	private SelectItem[] AdresTipiItems;

	public SelectItem[] getAdresTipiItems() {
		return AdresTipiItems;
	}

	public void setAdresTipiItems() {
		AdresTipiItems = new SelectItem[AdresTipi.values().length - 1];
		for (int x = 0; x < AdresTipi.values().length - 1; x++) {
			AdresTipiItems[x] = new SelectItem(AdresTipi.values()[x + 1], AdresTipi.values()[x + 1].getLabel());
		}
	}

	private SelectItem[] OgrenciAdresTuruItems;

	public SelectItem[] getOgrenciAdresTuruItems() {
		return OgrenciAdresTuruItems;
	}

	public void setOgrenciAdresTuruItems() {
		OgrenciAdresTuruItems = new SelectItem[1];
		OgrenciAdresTuruItems[0] = new SelectItem(AdresTuru.values()[1], AdresTuru.values()[1].getLabel());
	}

	private SelectItem[] KisiDurumItems;

	public SelectItem[] getKisiDurumItems() {
		return KisiDurumItems;
	}

	public void setKisiDurumItems() {
		KisiDurumItems = new SelectItem[KisiDurum.values().length - 1];
		for (int x = 0; x < KisiDurum.values().length - 1; x++) {
			KisiDurumItems[x] = new SelectItem(KisiDurum.values()[x + 1], KisiDurum.values()[x + 1].getLabel());
		}
	}

	private SelectItem[] OnemDereceItems;

	public SelectItem[] getOnemDereceItems() {
		return OnemDereceItems;
	}

	public void setOnemDereceItems() {
		OnemDereceItems = new SelectItem[OnemDerece.values().length - 1];
		for (int x = 0; x < OnemDerece.values().length - 1; x++) {
			OnemDereceItems[x] = new SelectItem(OnemDerece.values()[x + 1], OnemDerece.values()[x + 1].getLabel());
		}
	}

	private SelectItem[] KurulTuruItems;

	public SelectItem[] getKurulTuruItems() {
		return KurulTuruItems;
	}

	public void setKurulTuruItems() {
		KurulTuruItems = new SelectItem[KurulTuru.values().length - 1];
		for (int x = 0; x < KurulTuru.values().length - 1; x++) {
			KurulTuruItems[x] = new SelectItem(KurulTuru.values()[x + 1], KurulTuru.values()[x + 1].getLabel());
		}
	}

	private SelectItem[] GorevTuruItems;

	public SelectItem[] getGorevTuruItems() {
		return GorevTuruItems;
	}

	public void setGorevTuruItems() {
		GorevTuruItems = new SelectItem[GorevTuru.values().length - 1];
		for (int x = 0; x < GorevTuru.values().length - 1; x++) {
			GorevTuruItems[x] = new SelectItem(GorevTuru.values()[x + 1], GorevTuru.values()[x + 1].getLabel());
		}
	}

	private SelectItem[] UniversiteDurumItems;

	public SelectItem[] getUniversiteDurumItems() {
		return UniversiteDurumItems;
	}

	public void setUniversiteDurumItems() {
		UniversiteDurumItems = new SelectItem[UniversiteDurum.values().length - 1];
		for (int x = 0; x < UniversiteDurum.values().length - 1; x++) {
			UniversiteDurumItems[x] = new SelectItem(UniversiteDurum.values()[x + 1], UniversiteDurum.values()[x + 1].getLabel());
		}
	}

	private SelectItem[] UyekurumDurumItems;

	public SelectItem[] getUyekurumDurumItems() {
		return UyekurumDurumItems;
	}

	public void setUyekurumDurumItems() {
		UyekurumDurumItems = new SelectItem[UyekurumDurum.values().length - 1];
		for (int x = 0; x < UyekurumDurum.values().length - 1; x++) {
			UyekurumDurumItems[x] = new SelectItem(UyekurumDurum.values()[x + 1], UyekurumDurum.values()[x + 1].getLabel());
		}
	}

	private SelectItem[] OdemeTuruItems;

	public SelectItem[] getOdemeTuruItems() {
		return OdemeTuruItems;
	}

	public void setOdemeTuruItems() {
		OdemeTuruItems = new SelectItem[OdemeTuru.values().length - 1];
		for (int x = 0; x < OdemeTuru.values().length - 1; x++) {
			OdemeTuruItems[x] = new SelectItem(OdemeTuru.values()[x + 1], OdemeTuru.values()[x + 1].getLabel());
		}
	}

	private SelectItem[] EvrakTuruItems;

	public SelectItem[] getEvrakTuruItems() {
		return EvrakTuruItems;
	}

	public void setEvrakTuruItems() {
		EvrakTuruItems = new SelectItem[EvrakTuru.values().length - 1];
		for (int x = 0; x < EvrakTuru.values().length - 1; x++) {
			EvrakTuruItems[x] = new SelectItem(EvrakTuru.values()[x + 1], EvrakTuru.values()[x + 1].getLabel());
		}
	}

	private SelectItem[] TeslimTuruItems;

	public SelectItem[] getTeslimTuruItems() {
		return TeslimTuruItems;
	}

	public void setTeslimTuruItems() {
		TeslimTuruItems = new SelectItem[TeslimTuru.values().length - 1];
		for (int x = 0; x < TeslimTuru.values().length - 1; x++) {
			TeslimTuruItems[x] = new SelectItem(TeslimTuru.values()[x + 1], TeslimTuru.values()[x + 1].getLabel());
		}
	}

	private SelectItem[] GizlilikDerecesiItems;

	public SelectItem[] getGizlilikDerecesiItems() {
		return GizlilikDerecesiItems;
	}

	public void setGizlilikDerecesiItems() {
		GizlilikDerecesiItems = new SelectItem[GizlilikDerecesi.values().length - 1];
		for (int x = 0; x < GizlilikDerecesi.values().length - 1; x++) {
			GizlilikDerecesiItems[x] = new SelectItem(GizlilikDerecesi.values()[x + 1], GizlilikDerecesi.values()[x + 1].getLabel());
		}
	}

	private SelectItem[] AciliyetTuruItems;

	public SelectItem[] getAciliyetTuruItems() {
		return AciliyetTuruItems;
	}

	public void setAciliyetTuruItems() {
		AciliyetTuruItems = new SelectItem[AciliyetTuru.values().length - 1];
		for (int x = 0; x < AciliyetTuru.values().length - 1; x++) {
			AciliyetTuruItems[x] = new SelectItem(AciliyetTuru.values()[x + 1], AciliyetTuru.values()[x + 1].getLabel());
		}
	}

	private SelectItem[] EvrakDurumItems;

	public SelectItem[] getEvrakDurumItems() {
		return EvrakDurumItems;
	}

	public void setEvrakDurumItems() {
		EvrakDurumItems = new SelectItem[EvrakDurum.values().length - 1];
		for (int x = 0; x < EvrakDurum.values().length - 1; x++) {
			EvrakDurumItems[x] = new SelectItem(EvrakDurum.values()[x + 1], EvrakDurum.values()[x + 1].getLabel());
		}
	}

	private SelectItem[] EvrakHareketTuruItems;

	public SelectItem[] getEvrakHareketTuruItems() {
		return EvrakHareketTuruItems;
	}

	public void setEvrakHareketTuruItems() {
		EvrakHareketTuruItems = new SelectItem[EvrakHareketTuru.values().length - 1];
		for (int x = 0; x < EvrakHareketTuru.values().length - 1; x++) {
			EvrakHareketTuruItems[x] = new SelectItem(EvrakHareketTuru.values()[x + 1], EvrakHareketTuru.values()[x + 1].getLabel());
		}
	}

	private SelectItem[] KomisyonGorevItems;

	public SelectItem[] getKomisyonGorevItems() {
		return KomisyonGorevItems;
	}

	public void setKomisyonGorevItems() {
		KomisyonGorevItems = new SelectItem[KomisyonGorev.values().length - 1];
		for (int x = 0; x < KomisyonGorev.values().length - 1; x++) {
			KomisyonGorevItems[x] = new SelectItem(KomisyonGorev.values()[x + 1], KomisyonGorev.values()[x + 1].getLabel());
		}
	}

	private SelectItem[] UyeTuruItems;

	public SelectItem[] getUyeTuruItems() {
		return UyeTuruItems;
	}

	public void setUyeTuruItems() {
		UyeTuruItems = new SelectItem[UyeTuru.values().length - 1];
		for (int x = 0; x < UyeTuru.values().length - 1; x++) {
			UyeTuruItems[x] = new SelectItem(UyeTuru.values()[x + 1], UyeTuru.values()[x + 1].getLabel());
		}
	}

	private SelectItem[] ToplantiTuruItems;

	public SelectItem[] getToplantiTuruItems() {
		return ToplantiTuruItems;
	}

	public void setToplantiTuruItems() {
		ToplantiTuruItems = new SelectItem[ToplantiTuru.values().length - 1];
		for (int x = 0; x < ToplantiTuru.values().length - 1; x++) {
			ToplantiTuruItems[x] = new SelectItem(ToplantiTuru.values()[x + 1], ToplantiTuru.values()[x + 1].getLabel());
		}
	}

	private SelectItem[] TelefonTuruKurumItems;

	public SelectItem[] getTelefonTuruKurumItems() {
		TelefonTuruKurumItems = new SelectItem[TelefonTuru.values().length - 2];
		int flag = 0;
		for (int x = 0; x < TelefonTuru.values().length - 2; x++) {
			flag = x;
			if (flag != 2) {
				TelefonTuruKurumItems[x] = new SelectItem(TelefonTuru.values()[x + 1], TelefonTuru.values()[x + 1].getLabel());
			} else {
				TelefonTuruKurumItems[x] = new SelectItem(TelefonTuru.values()[x + 2], TelefonTuru.values()[x + 2].getLabel());
			}
			;
		}
		return TelefonTuruKurumItems;
	}

	public void setTelefonTuruKurumItems() {
		TelefonTuruKurumItems = new SelectItem[TelefonTuru.values().length - 2];
		int flag = 0;
		for (int x = 0; x < TelefonTuru.values().length - 2; x++) {
			flag = x;
			if (flag != 2) {
				TelefonTuruKurumItems[x] = new SelectItem(TelefonTuru.values()[x + 1], TelefonTuru.values()[x + 1].getLabel());
			} else {
				TelefonTuruKurumItems[x] = new SelectItem(TelefonTuru.values()[x + 2], TelefonTuru.values()[x + 2].getLabel());
			}
		}
	}

	private SelectItem[] KurulUyelikDurumuItems;

	public SelectItem[] getKurulUyelikDurumuItems() {
		return KurulUyelikDurumuItems;
	}

	public void setKurulUyelikDurumuItems() {
		KurulUyelikDurumuItems = new SelectItem[KurulUyelikDurumu.values().length - 1];
		for (int x = 0; x < KurulUyelikDurumu.values().length - 1; x++) {
			KurulUyelikDurumuItems[x] = new SelectItem(KurulUyelikDurumu.values()[x + 1], KurulUyelikDurumu.values()[x + 1].getLabel());
		}
	}

	private SelectItem[] KomisyonUyelikDurumuItems;

	public SelectItem[] getKomisyonUyelikDurumuItems() {
		return KomisyonUyelikDurumuItems;
	}

	public void setKomisyonUyelikDurumuItems() {
		KomisyonUyelikDurumuItems = new SelectItem[KomisyonUyelikDurumu.values().length - 1];
		for (int x = 0; x < KomisyonUyelikDurumu.values().length - 1; x++) {
			KomisyonUyelikDurumuItems[x] = new SelectItem(KomisyonUyelikDurumu.values()[x + 1], KomisyonUyelikDurumu.values()[x + 1].getLabel());
		}
	}

	private SelectItem[] ListeGizlilikTuruItems;

	public SelectItem[] getListeGizlilikTuruItems() {
		return ListeGizlilikTuruItems;
	}

	public void setListeGizlilikTuruItems() {
		ListeGizlilikTuruItems = new SelectItem[ListeGizlilikTuru.values().length - 1];
		for (int x = 0; x < ListeGizlilikTuru.values().length - 1; x++) {
			ListeGizlilikTuruItems[x] = new SelectItem(ListeGizlilikTuru.values()[x + 1], ListeGizlilikTuru.values()[x + 1].getLabel());
		}
	}

	private SelectItem[] ReferansTipiItems;

	public SelectItem[] getReferansTipiItems() {
		return ReferansTipiItems;
	}

	public void setReferansTipiItems() {
		ReferansTipiItems = new SelectItem[ReferansTipi.values().length - 1];
		for (int x = 0; x < ReferansTipi.values().length - 1; x++) {
			ReferansTipiItems[x] = new SelectItem(ReferansTipi.values()[x + 1], ReferansTipi.values()[x + 1].getLabel());
		}
	}

	private SelectItem[] GonderimTuruItems;

	public SelectItem[] getGonderimTuruItems() {
		return GonderimTuruItems;
	}

	public void setGonderimTuruItems() {
		GonderimTuruItems = new SelectItem[GonderimTuru.values().length - 1];
		for (int x = 0; x < GonderimTuru.values().length - 1; x++) {
			GonderimTuruItems[x] = new SelectItem(GonderimTuru.values()[x + 1], GonderimTuru.values()[x + 1].getLabel());
		}
	}

	private SelectItem[] GonderimDurumItems;

	public SelectItem[] getGonderimDurumItems() {
		return GonderimDurumItems;
	}

	public void setGonderimDurumItems() {
		GonderimDurumItems = new SelectItem[GonderimDurum.values().length - 1];
		for (int x = 0; x < GonderimDurum.values().length - 1; x++) {
			GonderimDurumItems[x] = new SelectItem(GonderimDurum.values()[x + 1], GonderimDurum.values()[x + 1].getLabel());
		}
	}

	private SelectItem[] IcerikTuruItems;

	public SelectItem[] getIcerikTuruItems() {
		return IcerikTuruItems;
	}

	public void setIcerikTuruItems() {
		IcerikTuruItems = new SelectItem[IcerikTuru.values().length - 1];
		for (int x = 0; x < IcerikTuru.values().length - 1; x++) {
			IcerikTuruItems[x] = new SelectItem(IcerikTuru.values()[x + 1], IcerikTuru.values()[x + 1].getLabel());
		}
	}

	private SelectItem[] EtkinlikDurumItems;

	public SelectItem[] getEtkinlikDurumItems() {
		return EtkinlikDurumItems;
	}

	public void setEtkinlikDurumItems() {
		EtkinlikDurumItems = new SelectItem[EtkinlikDurum.values().length - 1];
		for (int x = 0; x < EtkinlikDurum.values().length - 1; x++) {
			EtkinlikDurumItems[x] = new SelectItem(EtkinlikDurum.values()[x + 1], EtkinlikDurum.values()[x + 1].getLabel());
		}
	}

	private SelectItem[] KatilimciDurumItems;

	public SelectItem[] getKatilimciDurumItems() {
		return KatilimciDurumItems;
	}

	public void setKatilimciDurumItems() {
		KatilimciDurumItems = new SelectItem[KatilimciDurum.values().length - 1];
		for (int x = 0; x < KatilimciDurum.values().length - 1; x++) {
			KatilimciDurumItems[x] = new SelectItem(KatilimciDurum.values()[x + 1], KatilimciDurum.values()[x + 1].getLabel());
		}
	}

	private SelectItem[] KurulturuItems;

	public SelectItem[] getKurulturuItems() {
		return KurulturuItems;
	}

	public void setKurulturuItems() {
		KurulturuItems = new SelectItem[Kurulturu.values().length - 1];
		for (int x = 0; x < Kurulturu.values().length - 1; x++) {
			KurulturuItems[x] = new SelectItem(Kurulturu.values()[x + 1], Kurulturu.values()[x + 1].getLabel());
		}
	}

	private SelectItem[] UyeturuItems;

	public SelectItem[] getUyeturuItems() {
		return UyeturuItems;
	}

	public void setUyeturuItems() {
		UyeturuItems = new SelectItem[Uyeturu.values().length - 1];
		for (int x = 0; x < Uyeturu.values().length - 1; x++) {
			UyeturuItems[x] = new SelectItem(Uyeturu.values()[x + 1], Uyeturu.values()[x + 1].getLabel());
		}
	}

	private SelectItem[] EtkinlikKurumTuruItems;

	public SelectItem[] getEtkinlikKurumTuruItems() {
		return EtkinlikKurumTuruItems;
	}

	public void setEtkinlikKurumTuruItems() {
		EtkinlikKurumTuruItems = new SelectItem[EtkinlikKurumTuru.values().length - 1];
		for (int x = 0; x < EtkinlikKurumTuru.values().length - 1; x++) {
			EtkinlikKurumTuruItems[x] = new SelectItem(EtkinlikKurumTuru.values()[x + 1], EtkinlikKurumTuru.values()[x + 1].getLabel());
		}
	}

	private SelectItem[] EtkinlikTipiItems;

	public SelectItem[] getEtkinlikTipiItems() {
		return EtkinlikTipiItems;
	}

	public void setEtkinlikTipiItems() {
		EtkinlikTipiItems = new SelectItem[EtkinlikTipi.values().length - 1];
		for (int x = 0; x < EtkinlikTipi.values().length - 1; x++) {
			EtkinlikTipiItems[x] = new SelectItem(EtkinlikTipi.values()[x + 1], EtkinlikTipi.values()[x + 1].getLabel());
		}
	}

	private SelectItem[] DosyaturuItems;

	public SelectItem[] getDosyaturuItems() {
		return DosyaturuItems;
	}

	public void setDosyaturuItems() {
		DosyaturuItems = new SelectItem[Dosyaturu.values().length - 1];
		for (int x = 0; x < Dosyaturu.values().length - 1; x++) {
			DosyaturuItems[x] = new SelectItem(Dosyaturu.values()[x + 1], Dosyaturu.values()[x + 1].getLabel());
		}
	}

	private SelectItem[] EgitimturuItems;

	public SelectItem[] getEgitimturuItems() {
		return EgitimturuItems;
	}

	public void setEgitimturuItems() {
		EgitimturuItems = new SelectItem[Egitimturu.values().length - 1];
		for (int x = 0; x < Egitimturu.values().length - 1; x++) {
			EgitimturuItems[x] = new SelectItem(Egitimturu.values()[x + 1], Egitimturu.values()[x + 1].getLabel());
		}
	}

	private SelectItem[] EgitimDurumItems;

	public SelectItem[] getEgitimDurumItems() {
		return EgitimDurumItems;
	}

	public void setEgitimDurumItems() {
		EgitimDurumItems = new SelectItem[EgitimDurum.values().length - 1];
		for (int x = 0; x < EgitimDurum.values().length - 1; x++) {
			EgitimDurumItems[x] = new SelectItem(EgitimDurum.values()[x + 1], EgitimDurum.values()[x + 1].getLabel());
		}
	}

	private SelectItem[] EgitimkatilimciDurumItems;

	public SelectItem[] getEgitimkatilimciDurumItems() {
		return EgitimkatilimciDurumItems;
	}

	public void setEgitimkatilimciDurumItems() {
		EgitimkatilimciDurumItems = new SelectItem[EgitimkatilimciDurum.values().length - 1];
		for (int x = 0; x < EgitimkatilimciDurum.values().length - 1; x++) {
			EgitimkatilimciDurumItems[x] = new SelectItem(EgitimkatilimciDurum.values()[x + 1], EgitimkatilimciDurum.values()[x + 1].getLabel());
		}
	}

	private SelectItem[] KatilimbelgesiItems;

	public SelectItem[] getKatilimbelgesiItems() {
		return KatilimbelgesiItems;
	}

	public void setKatilimbelgesiItems() {
		KatilimbelgesiItems = new SelectItem[Katilimbelgesi.values().length - 1];
		for (int x = 0; x < Katilimbelgesi.values().length - 1; x++) {
			KatilimbelgesiItems[x] = new SelectItem(Katilimbelgesi.values()[x + 1], Katilimbelgesi.values()[x + 1].getLabel());
		}
	}

	private SelectItem[] SertifikaItems;

	public SelectItem[] getSertifikaItems() {
		return SertifikaItems;
	}

	public void setSertifikaItems() {
		SertifikaItems = new SelectItem[Sertifika.values().length - 1];
		for (int x = 0; x < Sertifika.values().length - 1; x++) {
			SertifikaItems[x] = new SelectItem(Sertifika.values()[x + 1], Sertifika.values()[x + 1].getLabel());
		}
	}

	private SelectItem[] SinavbasariDurumItems;

	public SelectItem[] getSinavbasariDurumItems() {
		return SinavbasariDurumItems;
	}

	public void setSinavbasariDurumItems() {
		SinavbasariDurumItems = new SelectItem[SinavbasariDurum.values().length - 1];
		for (int x = 0; x < SinavbasariDurum.values().length - 1; x++) {
			SinavbasariDurumItems[x] = new SelectItem(SinavbasariDurum.values()[x + 1], SinavbasariDurum.values()[x + 1].getLabel());
		}
	}

	private SelectItem[] SoruItems;

	public SelectItem[] getSoruItems() {
		return SoruItems;
	}

	public void setSoruItems() {
		SoruItems = new SelectItem[Soru.values().length - 1];
		for (int x = 0; x < Soru.values().length - 1; x++) {
			SoruItems[x] = new SelectItem(Soru.values()[x + 1], Soru.values()[x + 1].getLabel());
		}
	}

	private SelectItem[] CevapItems;

	public SelectItem[] getCevapItems() {
		return CevapItems;
	}

	public void setCevapItems() {
		CevapItems = new SelectItem[Cevap.values().length - 1];
		for (int x = 0; x < Cevap.values().length - 1; x++) {
			CevapItems[x] = new SelectItem(Cevap.values()[x + 1], Cevap.values()[x + 1].getLabel());
		}
	}

	private SelectItem[] RaporTuruItems;

	public SelectItem[] getRaporTuruItems() {
		return RaporTuruItems;
	}

	public void setRaporTuruItems() {
		RaporTuruItems = new SelectItem[RaporTuru.values().length - 1];
		for (int x = 0; x < RaporTuru.values().length - 1; x++) {
			RaporTuruItems[x] = new SelectItem(RaporTuru.values()[x + 1], RaporTuru.values()[x + 1].getLabel());
		}
	}

	private SelectItem[] AdresTipiItemsForKisi;

	public SelectItem[] getAdresTipiItemsForKisi() {
		return AdresTipiItemsForKisi;
	}

	public void setAdresTipiItemsForKisi() {
		AdresTipiItemsForKisi = new SelectItem[2];
		int i = -1;
		for (int x = 0; x < AdresTipi.values().length - 1; x++) {
			if (AdresTipi.values()[x + 1].getCode() == AdresTipi._ILILCEMERKEZI.getCode()) {
				i++;
				AdresTipiItemsForKisi[i] = new SelectItem(AdresTipi.values()[x + 1], AdresTipi.values()[x + 1].getLabel());
			} else if (AdresTipi.values()[x + 1].getCode() == AdresTipi._YURTDISIADRESI.getCode()) {
				i++;
				AdresTipiItemsForKisi[i] = new SelectItem(AdresTipi.values()[x + 1], AdresTipi.values()[x + 1].getLabel());
			}

		}
	}

	private SelectItem[] DemirbasTuruItems;

	public SelectItem[] getDemirbasTuruItems() {
		return DemirbasTuruItems;
	}

	public void setDemirbasTuruItems() {
		DemirbasTuruItems = new SelectItem[DemirbasTuru.values().length - 1];
		for (int x = 0; x < DemirbasTuru.values().length - 1; x++) {
			DemirbasTuruItems[x] = new SelectItem(DemirbasTuru.values()[x + 1], DemirbasTuru.values()[x + 1].getLabel());
		}
	}

	private SelectItem[] DemirbasDurumItems;

	public SelectItem[] getDemirbasDurumItems() {
		return DemirbasDurumItems;
	}

	public void setDemirbasDurumItems() {
		DemirbasDurumItems = new SelectItem[DemirbasDurum.values().length - 1];
		for (int x = 0; x < DemirbasDurum.values().length - 1; x++) {
			DemirbasDurumItems[x] = new SelectItem(DemirbasDurum.values()[x + 1], DemirbasDurum.values()[x + 1].getLabel());
		}
	}

	private SelectItem[] OdemeSekliItems;

	public SelectItem[] getOdemeSekliItems() {
		return OdemeSekliItems;
	}

	public void setOdemeSekliItems() {
		OdemeSekliItems = new SelectItem[OdemeSekli.values().length - 1];
		for (int x = 0; x < OdemeSekli.values().length - 1; x++) {
			OdemeSekliItems[x] = new SelectItem(OdemeSekli.values()[x + 1], OdemeSekli.values()[x + 1].getLabel());
		}
	}

	private SelectItem[] OdemeSekliVeznedenItems;

	public SelectItem[] getOdemeSekliVeznedenItems() {
		return OdemeSekliVeznedenItems;
	}

	public void setOdemeSekliVeznedenItems() {
		OdemeSekliVeznedenItems = new SelectItem[OdemeSekli.values().length - 2];
		for (int x = 0; x < OdemeSekli.values().length - 2; x++) {
			OdemeSekliVeznedenItems[x] = new SelectItem(OdemeSekli.values()[x + 1], OdemeSekli.values()[x + 1].getLabel());
		}
	}

	private SelectItem[] KpsTuruItems;

	public SelectItem[] getKpsTuruItems() {
		return KpsTuruItems;
	}

	public void setKpsTuruItems() {
		KpsTuruItems = new SelectItem[KpsTuru.values().length - 1];
		for (int x = 0; x < KpsTuru.values().length - 1; x++) {
			KpsTuruItems[x] = new SelectItem(KpsTuru.values()[x + 1], KpsTuru.values()[x + 1].getLabel());
		}
	}

	private SelectItem[] SinavTuruItems;

	public SelectItem[] getSinavTuruItems() {
		return SinavTuruItems;
	}

	public void setSinavTuruItems() {
		SinavTuruItems = new SelectItem[SinavTuru.values().length - 1];
		for (int x = 0; x < SinavTuru.values().length - 1; x++) {
			SinavTuruItems[x] = new SelectItem(SinavTuru.values()[x + 1], SinavTuru.values()[x + 1].getLabel());
		}
	}

	private SelectItem[] SinavDurumItems;

	public SelectItem[] getSinavDurumItems() {
		return SinavDurumItems;
	}

	public void setSinavDurumItems() {
		SinavDurumItems = new SelectItem[SinavDurum.values().length - 1];
		for (int x = 0; x < SinavDurum.values().length - 1; x++) {
			SinavDurumItems[x] = new SelectItem(SinavDurum.values()[x + 1], SinavDurum.values()[x + 1].getLabel());
		}
	}

	private SelectItem[] KisiTuruItems;

	public SelectItem[] getKisiTuruItems() {
		return KisiTuruItems;
	}

	public void setKisiTuruItems() {
		KisiTuruItems = new SelectItem[KisiTuru.values().length - 1];
		for (int x = 0; x < KisiTuru.values().length - 1; x++) {
			KisiTuruItems[x] = new SelectItem(KisiTuru.values()[x + 1], KisiTuru.values()[x + 1].getLabel());
		}
	}

	private SelectItem[] SonucTuruItems;

	public SelectItem[] getSonucTuruItems() {
		return SonucTuruItems;
	}

	public void setSonucTuruItems() {
		SonucTuruItems = new SelectItem[SonucTuru.values().length - 1];
		for (int x = 0; x < SonucTuru.values().length - 1; x++) {
			SonucTuruItems[x] = new SelectItem(SonucTuru.values()[x + 1], SonucTuru.values()[x + 1].getLabel());
		}
	}

	private SelectItem[] KatilimTuruItems;

	public SelectItem[] getKatilimTuruItems() {
		return KatilimTuruItems;
	}

	public void setKatilimTuruItems() {
		KatilimTuruItems = new SelectItem[KatilimTuru.values().length - 1];
		for (int x = 0; x < KatilimTuru.values().length - 1; x++) {
			KatilimTuruItems[x] = new SelectItem(KatilimTuru.values()[x + 1], KatilimTuru.values()[x + 1].getLabel());
		}
	}

	private SelectItem[] EtkinDurumItems;

	public SelectItem[] getEtkinDurumItems() {
		return EtkinDurumItems;
	}

	public void setEtkinDurumItems() {
		EtkinDurumItems = new SelectItem[EtkinDurum.values().length - 1];
		for (int x = 0; x < EtkinDurum.values().length - 1; x++) {
			EtkinDurumItems[x] = new SelectItem(EtkinDurum.values()[x + 1], EtkinDurum.values()[x + 1].getLabel());
		}
	}

	private SelectItem[] KatilimciTuruItems;

	public SelectItem[] getKatilimciTuruItems() {
		return KatilimciTuruItems;
	}

	public void setKatilimciTuruItems() {
		KatilimciTuruItems = new SelectItem[KatilimciTuru.values().length - 1];
		for (int x = 0; x < KatilimciTuru.values().length - 1; x++) {
			KatilimciTuruItems[x] = new SelectItem(KatilimciTuru.values()[x + 1], KatilimciTuru.values()[x + 1].getLabel());
		}
	}

	private SelectItem[] EgitimUcretTipiItems;

	public SelectItem[] getEgitimUcretTipiItems() {
		return EgitimUcretTipiItems;
	}

	public void setEgitimUcretTipiItems() {
		EgitimUcretTipiItems = new SelectItem[EgitimUcretTipi.values().length - 1];
		for (int x = 0; x < EgitimUcretTipi.values().length - 1; x++) {
			EgitimUcretTipiItems[x] = new SelectItem(EgitimUcretTipi.values()[x + 1], EgitimUcretTipi.values()[x + 1].getLabel());
		}
	}

	private SelectItem[] FormturuItems;

	public SelectItem[] getFormturuItems() {
		return FormturuItems;
	}

	public void setFormturuItems() {
		FormturuItems = new SelectItem[Formturu.values().length - 1];
		for (int x = 0; x < Formturu.values().length - 1; x++) {
			FormturuItems[x] = new SelectItem(Formturu.values()[x + 1], Formturu.values()[x + 1].getLabel());
		}
	}

	private SelectItem[] SiparisDurumItems;

	public SelectItem[] getSiparisDurumItems() {
		return SiparisDurumItems;
	}

	public void setSiparisDurumItems() {
		SiparisDurumItems = new SelectItem[SiparisDurum.values().length - 1];
		for (int x = 0; x < SiparisDurum.values().length - 1; x++) {
			SiparisDurumItems[x] = new SelectItem(SiparisDurum.values()[x + 1], SiparisDurum.values()[x + 1].getLabel());
		}
	}

}
