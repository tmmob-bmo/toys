package tr.com.arf.toys.view.controller.form.evrak; 
  
 
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.toys.db.filter.evrak.DosyakoduFilter;
import tr.com.arf.toys.db.model.evrak.Dosyakodu;
import tr.com.arf.toys.service.application.ApplicationDescriptor;
import tr.com.arf.toys.view.controller.form._base.ToysBaseTreeController;
  
  
@ManagedBean 
@ViewScoped 
public class DosyakoduController extends ToysBaseTreeController {  
  
  private static final long serialVersionUID = 6531152165884980748L; 
  
	private Dosyakodu dosyakodu;  
	private DosyakoduFilter dosyakoduFilter = new DosyakoduFilter();  
  
	public DosyakoduController() { 
		super(Dosyakodu.class);  
		setDefaultValues(false);  
		setOrderField("ad");  
		setAutoCompleteSearchColumns(new String[]{"ad"}); 
		setSelectedTabName("dosyakoduListTab"); 
		setTable(null); 
		queryAction(); 
	} 
	
	
	public Dosyakodu getDosyakodu() { 
		dosyakodu = (Dosyakodu) getEntity(); 
		return dosyakodu; 
	} 
	
	public void setDosyakodu(Dosyakodu dosyakodu) { 
		this.dosyakodu = dosyakodu; 
	} 
	
	public DosyakoduFilter getDosyakoduFilter() { 
		return dosyakoduFilter; 
	} 
	 
	public void setDosyakoduFilter(DosyakoduFilter dosyakoduFilter) {  
		this.dosyakoduFilter = dosyakoduFilter;  
	}  
	 
	@Override  
	public BaseFilter getFilter() {  
		return getDosyakoduFilter();  
	}  
//	 
//	@Override 
//	public synchronized void save() {  
//		setTable(null);
//		
//		if (getDosyakodu().getRID() == null) { 
//			try {  
//				getDosyakodu().setErisimKodu("-"); 
//				getDBOperator().insert(getDosyakodu()); 
//				if(getDosyakodu().getUstRef() != null) {
//					getDosyakodu().setErisimKodu(getDosyakodu().getUstRef().getErisimKodu() + "." + getDosyakodu().getRID());
//				} else {
//					getDosyakodu().setErisimKodu(getDosyakodu().getRID()+ "");
//				}
//				getDBOperator().update(getDosyakodu());
//				createGenericMessage(KeyUtil.getMessageValue("kayit.eklendi"), FacesMessage.SEVERITY_INFO);
//			} catch (DBException e) { 
//				getDosyakodu().setRID(null); 
//				createGenericMessage(KeyUtil.getMessageValue("islem.hata"), FacesMessage.SEVERITY_ERROR);
//				return;
//			} 
//		} 
//		else { 
//			try { 
//				if(getDosyakodu().getUstRef() != null) {
//					getDosyakodu().setErisimKodu(getDosyakodu().getUstRef().getErisimKodu() + "." + getDosyakodu().getRID());
//				} else {
//					getDosyakodu().setErisimKodu(getDosyakodu().getRID()+ "");
//				}
//				getDBOperator().update(getDosyakodu()); 
//				createGenericMessage(KeyUtil.getMessageValue("kayit.guncellendi"), FacesMessage.SEVERITY_INFO);
//			} catch (DBException e) { 
//				createGenericMessage(KeyUtil.getMessageValue("islem.hata"), FacesMessage.SEVERITY_ERROR);
//				return; 
//			} 
//	
//		} 
//		insert(); 
//		cancel(); 
//		queryAction(); 
//		return; 
//	}
	
	public String tree() {	
		getSessionUser().setLegalAccess(1); 
		return ApplicationDescriptor._DOSYAKODU_TREE_OUTCOME;	
	}	
	
} // class 
