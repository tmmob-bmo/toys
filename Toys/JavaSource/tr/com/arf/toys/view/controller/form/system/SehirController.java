package tr.com.arf.toys.view.controller.form.system; 
  
 
import java.util.ArrayList;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.model.SelectItem;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.toys.db.filter.system.SehirFilter;
import tr.com.arf.toys.db.model.system.Sehir;
import tr.com.arf.toys.db.model.system.Ulke;
import tr.com.arf.toys.service.application.ApplicationDescriptor;
import tr.com.arf.toys.service.application.ManagedBeanLocator;
import tr.com.arf.toys.utility.tool.KPSSehirIlceUpdater;
import tr.com.arf.toys.view.controller.form._base.ToysFilteredControllerNoPaging;
  
  
@ManagedBean 
@ViewScoped 
public class SehirController extends ToysFilteredControllerNoPaging {  
   
	private static final long serialVersionUID = -1908954956039481387L;
	
	private Sehir sehir;   
	private SehirFilter sehirFilter = new SehirFilter();  
  
	public SehirController() { 
		super(Sehir.class);   
		setDefaultValues(false);    
		setOrderField("ad");  
		queryAction();  
		// setPostAddOutcome(getEditOutcome());  
	} 
	
	public Sehir getSehir() { 
		sehir = (Sehir) getEntity(); 
		return sehir; 
	} 
	
	public void setIl(Sehir sehir) { 
		this.sehir = sehir; 
	} 
	
	public SehirFilter getSehirFilter() { 
		return sehirFilter; 
	} 
	 
	public void setIlFilter(SehirFilter sehirFilter) {  
		this.sehirFilter = sehirFilter;  
	}  
	 
	@Override  
	public BaseFilter getFilter() {  
		return getSehirFilter();  
	}  
	
	public String ilce() {	
		if (!setSelected()) {
			return "";
		}	 
		getSessionUser().setLegalAccess(1);	
		putObjectToSessionFilter(ApplicationDescriptor._SEHIR_MODEL, getSehir());
		return ManagedBeanLocator.locateMenuController().gotoPage("menu_Ilce"); 
	}
	
	public ArrayList<SelectItem> getPlakaKoduList(){ 
		Sehir maxPlakaKodluSehir = (Sehir) getDBOperator().load(Sehir.class.getSimpleName(), "o.ilKodu= (Select MAX(m.ilKodu) From " + getModelName() + " m)", "o.rID").get(0);
		Integer maxPlakaKodu = Integer.parseInt(maxPlakaKodluSehir.getIlKodu()) + 1;
		
		ArrayList<SelectItem> plakaKoduList = new ArrayList<SelectItem>();
		for(int x = maxPlakaKodu; x < 100; x++ ){
			plakaKoduList.add(new SelectItem(formatPlakaKodu(x + ""), formatPlakaKodu(x + "")));
		} 
		return plakaKoduList; 
	}
	
	private String formatPlakaKodu(String s){
		if(s.length() > 1){
			return s;
		} else {
			return "0" + s;
		}
	}
	
	public void kpsGuncelleme() throws Exception{ 
		if (getDBOperator().recordCount(Ulke.class.getSimpleName(), "o.ulkekod='9980'")>0){
			Ulke ulke =(Ulke) getDBOperator().load(Ulke.class.getSimpleName(), "o.ulkekod='9980'","").get(0);
			KPSSehirIlceUpdater.sehirGuncelle(ulke);
			createGenericMessage("KPS'den güncelleme işlemi tamamlandı...", FacesMessage.SEVERITY_INFO);
		}
		else {
			createGenericMessage("KPS'den güncelleme işlemi başarısız oldu...", FacesMessage.SEVERITY_INFO);
		}
	}
} // class 
