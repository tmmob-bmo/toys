package tr.com.arf.toys.view.controller.form.egitim;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.framework.utility.tool.KeyUtil;
import tr.com.arf.toys.db.enumerated.egitim.EgitimDurum;
import tr.com.arf.toys.db.filter.egitim.EgitimogretmenFilter;
import tr.com.arf.toys.db.model.egitim.Egitim;
import tr.com.arf.toys.db.model.egitim.Egitimogretmen;
import tr.com.arf.toys.service.application.ApplicationDescriptor;
import tr.com.arf.toys.view.controller.form._base.ToysBaseFilteredController;

@ManagedBean
@ViewScoped
public class EgitimogretmenController extends ToysBaseFilteredController {

	private static final long serialVersionUID = 6531152165884980748L;

	private Egitimogretmen egitimogretmen;
	private EgitimogretmenFilter egitimogretmenFilter = new EgitimogretmenFilter();

	public EgitimogretmenController() {
		super(Egitimogretmen.class);
		setDefaultValues(false);
		setOrderField("aciklama");
		setLoggable(false);
		setAutoCompleteSearchColumns(new String[] { "aciklama" });
		setTable(null);
		if (getMasterEntity() != null) {
			setMasterObjectName(ApplicationDescriptor._EGITIM_MODEL);
			setMasterOutcome(ApplicationDescriptor._EGITIM_MODEL);
			getEgitimogretmenFilter().setEgitimRef(getMasterEntity());
			getEgitimogretmenFilter().createQueryCriterias();
		}
		queryAction();
	}

	public Egitim getMasterEntity() {
		if (getEgitimogretmenFilter().getEgitimRef() != null) {
			return getEgitimogretmenFilter().getEgitimRef();
		} else {
			return (Egitim) getObjectFromSessionFilter(ApplicationDescriptor._EGITIM_MODEL);
		}
	}

	public Egitimogretmen getEgitimogretmen() {
		egitimogretmen = (Egitimogretmen) getEntity();
		return egitimogretmen;
	}

	public void setEgitimogretmen(Egitimogretmen egitimogretmen) {
		this.egitimogretmen = egitimogretmen;
	}

	public EgitimogretmenFilter getEgitimogretmenFilter() {
		return egitimogretmenFilter;
	}

	public void setEgitimogretmenFilter(
			EgitimogretmenFilter egitimogretmenFilter) {
		this.egitimogretmenFilter = egitimogretmenFilter;
	}

	@Override
	public BaseFilter getFilter() {
		return getEgitimogretmenFilter();
	}

	@Override
	public void resetFilter() {
		super.resetFilterOnly();
		if (getMasterEntity() != null) {
			getEgitimogretmenFilter().setEgitimRef(getMasterEntity());
		}
		queryAction();
	}

	@Override
	public void insert() {
		if (this.getMasterEntity().getDurum() == EgitimDurum._Planlanan) {
			super.insert();
		}
		else{
			createGenericMessage(KeyUtil.getMessageValue("egitim.ogretmenPlanlanan"), FacesMessage.SEVERITY_ERROR);
			queryAction();
		}
	}

	@Override
	public void save() {
		this.egitimogretmen.setEgitimRef(this.egitimogretmenFilter.getEgitimRef());
			super.save();
	}
	@Override
	public void update() {
		if (this.getMasterEntity().getDurum() == EgitimDurum._Planlanan) {
			super.update();
		}
		else{
			createGenericMessage(KeyUtil.getMessageValue("egitim.ogretmenPlanlanan"), FacesMessage.SEVERITY_ERROR);
			queryAction();
		}
	}

} // class 
