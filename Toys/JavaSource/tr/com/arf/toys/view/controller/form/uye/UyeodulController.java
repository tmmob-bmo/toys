package tr.com.arf.toys.view.controller.form.uye; 
  
 
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.toys.db.filter.uye.UyeodulFilter;
import tr.com.arf.toys.db.model.uye.Uye;
import tr.com.arf.toys.db.model.uye.Uyeodul;
import tr.com.arf.toys.service.application.ApplicationDescriptor;
import tr.com.arf.toys.view.controller.form._base.ToysBaseFilteredController;
  
  
@ManagedBean 
@ViewScoped 
public class UyeodulController extends ToysBaseFilteredController {  
  
  private static final long serialVersionUID = 6531152165884980748L; 
  
	private Uyeodul uyeodul;  
	private UyeodulFilter uyeodulFilter = new UyeodulFilter();  
  
	public UyeodulController() { 
		super(Uyeodul.class);  
		setDefaultValues(false);  
		setOrderField("aciklama");  
		setAutoCompleteSearchColumns(new String[]{"aciklama"}); 
		setTable(null); 
		if(getMasterEntity() != null){ 
			setMasterObjectName(ApplicationDescriptor._UYE_MODEL);
			setMasterOutcome(ApplicationDescriptor._UYE_MODEL);
			getUyeodulFilter().setUyeRef(getMasterEntity());
			getUyeodulFilter().createQueryCriterias();   
		} 
		queryAction(); 
	} 
	
	public UyeodulController(Object object) { 
		super(Uyeodul.class);    
	}
 
	public Uye getMasterEntity(){
		if(getUyeodulFilter().getUyeRef() != null){
			return getUyeodulFilter().getUyeRef();
		} else {
			return (Uye) getObjectFromSessionFilter(ApplicationDescriptor._UYE_MODEL);
		}			
	} 
 
	
	public Uyeodul getUyeodul() { 
		uyeodul = (Uyeodul) getEntity(); 
		return uyeodul; 
	} 
	
	public void setUyeodul(Uyeodul uyeodul) { 
		this.uyeodul = uyeodul; 
	} 
	
	public UyeodulFilter getUyeodulFilter() { 
		return uyeodulFilter; 
	} 
	 
	public void setUyeodulFilter(UyeodulFilter uyeodulFilter) {  
		this.uyeodulFilter = uyeodulFilter;  
	}  
	 
	@Override  
	public BaseFilter getFilter() {  
		return getUyeodulFilter();  
	}  
	
	
 
 
	@Override	
	public void insert() {	
		super.insert();	
		getUyeodul().setUyeRef(getUyeodulFilter().getUyeRef());	
	}	
	
} // class 
