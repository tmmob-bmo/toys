package tr.com.arf.toys.view.controller.form.system; 
  
 
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.framework.db.model._base.BaseEntity;
import tr.com.arf.toys.db.filter.system.KurumHesapFilter;
import tr.com.arf.toys.db.model.kisi.Kurum;
import tr.com.arf.toys.db.model.system.KurumHesap;
import tr.com.arf.toys.service.application.ApplicationDescriptor;
import tr.com.arf.toys.view.controller.form._base.ToysBaseFilteredController;
  
  
@ManagedBean 
@ViewScoped 
public class KurumHesapController extends ToysBaseFilteredController {  
  
  private static final long serialVersionUID = 6531152165884980748L; 
  
	private KurumHesap kurumHesap;  
	private KurumHesapFilter kurumHesapFilter = new KurumHesapFilter();  
  
	public KurumHesapController() { 
		super(KurumHesap.class);  
		setDefaultValues(false);  
		setTable(null); 
		if(getMasterEntity() != null){ 
			  if(getMasterEntity().getClass() == Kurum.class){
				setMasterObjectName(ApplicationDescriptor._KURUM_MODEL);
				setMasterOutcome(ApplicationDescriptor._KURUM_MODEL);	
				getKurumHesapFilter().setKurumRef((Kurum) getMasterEntity());
				} 
			  else {
					setMasterOutcome(null);
			}
		}
		getKurumHesapFilter().createQueryCriterias();  
		queryAction(); 
	} 
	
	public KurumHesapController(Object object) { 
		super(KurumHesap.class); 
		setLoggable(false);
	}
	
	public BaseEntity getMasterEntity(){
		if(getObjectFromSessionFilter(ApplicationDescriptor._KURUM_MODEL) != null){
			return (Kurum) getObjectFromSessionFilter(ApplicationDescriptor._KURUM_MODEL);
		}  
		else {
			return null;
		}			
	} 
	
	public KurumHesap getKurumHesap() { 
		kurumHesap = (KurumHesap) getEntity(); 
		return kurumHesap; 
	} 
	
	public void setKurumHesap(KurumHesap kurumHesap) { 
		this.kurumHesap = kurumHesap; 
	} 
	
	public KurumHesapFilter getKurumHesapFilter() { 
		return kurumHesapFilter; 
	} 
	 
	public void setKurumHesapFilter(KurumHesapFilter kurumHesapFilter) {  
		this.kurumHesapFilter = kurumHesapFilter;  
	}  
	 
	@Override  
	public BaseFilter getFilter() {  
		return getKurumHesapFilter();  
	}   
	
} // class 
