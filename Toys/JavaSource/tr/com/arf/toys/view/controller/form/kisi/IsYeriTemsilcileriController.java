package tr.com.arf.toys.view.controller.form.kisi; 
  
 
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.framework.db.model._base.BaseEntity;
import tr.com.arf.toys.db.filter.kisi.IsYeriTemsilcileriFilter;
import tr.com.arf.toys.db.model.kisi.IsYeriTemsilcileri;
import tr.com.arf.toys.db.model.kisi.Kurum;
import tr.com.arf.toys.service.application.ApplicationDescriptor;
import tr.com.arf.toys.view.controller.form._base.ToysBaseFilteredController;
  
  
@ManagedBean 
@ViewScoped 
public class IsYeriTemsilcileriController extends ToysBaseFilteredController {  
  
  private static final long serialVersionUID = 6531152165884980748L; 
  
	private IsYeriTemsilcileri isYeriTemsilcileri;  
	private IsYeriTemsilcileriFilter isYeriTemsilcileriFilter = new IsYeriTemsilcileriFilter();  
  
	public IsYeriTemsilcileriController() { 
		super(IsYeriTemsilcileri.class);  
		setDefaultValues(false);  
		setTable(null); 
		if(getMasterEntity() != null){ 
			  if(getMasterEntity().getClass() == Kurum.class){
				setMasterObjectName(ApplicationDescriptor._KURUM_MODEL);
				setMasterOutcome(ApplicationDescriptor._KURUM_MODEL);	
				getIsYeriTemsilcileriFilter().setKurumRef((Kurum) getMasterEntity());
				} 
			  else {
					setMasterOutcome(null);
				}
			}
		getIsYeriTemsilcileriFilter().createQueryCriterias();  
		queryAction(); 
	} 
	
	public IsYeriTemsilcileriController(Object object) { 
		super(IsYeriTemsilcileri.class);  
		setLoggable(false);
	}
	
	public BaseEntity getMasterEntity(){
		if(getObjectFromSessionFilter(ApplicationDescriptor._KURUM_MODEL) != null){
			return (Kurum) getObjectFromSessionFilter(ApplicationDescriptor._KURUM_MODEL);
		}  
		else {
			return null;
		}			
	} 

	
	public IsYeriTemsilcileri getIsYeriTemsilcileri() {
		isYeriTemsilcileri = (IsYeriTemsilcileri) getEntity(); 
		return isYeriTemsilcileri;
	}

	public void setIsYeriTemsilcileri(IsYeriTemsilcileri isYeriTemsilcileri) {
		this.isYeriTemsilcileri = isYeriTemsilcileri;
	}

	public IsYeriTemsilcileriFilter getIsYeriTemsilcileriFilter() {
		return isYeriTemsilcileriFilter;
	}

	public void setIsYeriTemsilcileriFilter(
			IsYeriTemsilcileriFilter isYeriTemsilcileriFilter) {
		this.isYeriTemsilcileriFilter = isYeriTemsilcileriFilter;
	}

	@Override  
	public BaseFilter getFilter() {  
		return getIsYeriTemsilcileriFilter();  
	}  
	

	
 
} // class 
