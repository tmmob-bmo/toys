package tr.com.arf.toys.view.controller.form.system;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.toys.db.filter.system.YprojeFilter;
import tr.com.arf.toys.db.model.system.Yproje;
import tr.com.arf.toys.view.controller.form._base.ToysBaseTreeController;

@ManagedBean 
@ViewScoped 
public class YprojeController extends ToysBaseTreeController {
	
	private static final long serialVersionUID = -3733515992134894810L;
	
	private Yproje proje;
	private YprojeFilter projeFilter = new YprojeFilter();
	
	public YprojeController() { 
		super(Yproje.class);  
		setDefaultValues(false);  
		setOrderField("ad");  
		setAutoCompleteSearchColumns(new String[]{"ad"}); 
		setTable(null); 
		queryAction(); 
	} 
	
	
	
	public Yproje getProje() {
		proje = (Yproje) getEntity();
		return proje;
	}
	
	public void setProje(Yproje proje) { 
		this.proje = proje; 
	} 
	
	public YprojeFilter getProjeFilter() {
		return projeFilter;
	}

	public void setProjeFilter(YprojeFilter projeFilter) {
		this.projeFilter = projeFilter;
	}
	
	@Override  
	public BaseFilter getFilter() {  
		return getProjeFilter();  
	}  	
	
	/*@Override 
	public synchronized void save() {   
		setTable(null);		
		if (getProje().getRID() == null) { 
			try {  
				getProje().setErisimKodu("-"); 
				getDBOperator().insert(getProje()); 
				if(getProje().getUstRef() != null) {
					getProje().setErisimKodu(getProje().getUstRef().getErisimKodu() + "." + getProje().getRID());
				} else {
					getProje().setErisimKodu(getProje().getRID()+ "");
				}
				getDBOperator().update(getProje());
				createGenericMessage(KeyUtil.getMessageValue("kayit.eklendi"), FacesMessage.SEVERITY_INFO);
			} catch (DBException e) { 
				getProje().setRID(null); 
				createGenericMessage(KeyUtil.getMessageValue("islem.hata"), FacesMessage.SEVERITY_ERROR);
				return;
			} 
		} 
		else { 
			try { 
				if(getProje().getUstRef() != null) {
					getProje().setErisimKodu(getProje().getUstRef().getErisimKodu() + "." + getProje().getRID());
				} else {
					getProje().setErisimKodu(getProje().getRID() + "");
				}
				if(getProje().getValue().hashCode() == getOldValue().hashCode()){
					createGenericMessage(KeyUtil.getMessageValue("islem.degisiklikYok"), FacesMessage.SEVERITY_INFO); 
					return;
				}
				getDBOperator().update(getProje()); 
				createGenericMessage(KeyUtil.getMessageValue("kayit.guncellendi"), FacesMessage.SEVERITY_ERROR);
			} catch (DBException e) { 
				createGenericMessage(KeyUtil.getMessageValue("islem.hata"), FacesMessage.SEVERITY_ERROR);
				return; 
			} 
	
		} 
		insert(); 
		cancel(); 
		queryAction(); 
		return; 
	}*/
	
	/*public String tree() {	
		getSessionUser().setLegalAccess(1); 
		return ApplicationDescriptor._GOREV_TREE_OUTCOME;	
	}	*/
	
	
	
	

}
