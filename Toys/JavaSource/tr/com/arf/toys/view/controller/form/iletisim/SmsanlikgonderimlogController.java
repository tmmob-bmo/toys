package tr.com.arf.toys.view.controller.form.iletisim;

import java.util.Date;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.event.AjaxBehaviorEvent;

import tr.com.arf.framework.db.enumerated._common.EvetHayir;
import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.framework.db.model._base.BaseEntity;
import tr.com.arf.toys.db.enumerated.iletisim.GonderimDurum;
import tr.com.arf.toys.db.filter.iletisim.SmsanlikgonderimlogFilter;
import tr.com.arf.toys.db.model.iletisim.Smsanlikgonderimlog;
import tr.com.arf.toys.db.model.kisi.Kisi;
import tr.com.arf.toys.db.model.system.SistemParametre;
import tr.com.arf.toys.db.model.uye.Uye;
import tr.com.arf.toys.service.application.ApplicationDescriptor;
import tr.com.arf.toys.view.controller.form._base.ToysBaseFilteredController;

@ManagedBean
@ViewScoped
public class SmsanlikgonderimlogController extends ToysBaseFilteredController {

	private static final long serialVersionUID = 6531152165884980748L;

	private Smsanlikgonderimlog smsanlikgonderimlog;
	private SmsanlikgonderimlogFilter smsanlikgonderimlogFilter = new SmsanlikgonderimlogFilter();
	public EvetHayir icerikOrEditor;

	public SmsanlikgonderimlogController() {
		super(Smsanlikgonderimlog.class);
		setDefaultValues(false);
		setOrderField("aciklama");
		setLoggable(false);
		setAutoCompleteSearchColumns(new String[] { "aciklama" });
		setTable(null);
		if (getMasterEntity() != null) {
			if (getMasterEntity().getClass() == Kisi.class){
				setMasterObjectName(ApplicationDescriptor._KISI_MODEL);
				getSmsanlikgonderimlogFilter().setKisiRef((Kisi)getMasterEntity());
				setMasterOutcome(ApplicationDescriptor._KISI_MODEL);
			} else if(getMasterEntity().getClass() == Uye.class){ 
			setMasterObjectName(ApplicationDescriptor._UYE_MODEL);
			getSmsanlikgonderimlogFilter().setKisiRef(((Uye) getMasterEntity()).getKisiRef());  
			setMasterOutcome(ApplicationDescriptor._UYE_MODEL);	
			}
		}
		getSmsanlikgonderimlogFilter().createQueryCriterias();
		queryAction();
	}

	public BaseEntity getMasterEntity() {
		
		if (getSmsanlikgonderimlogFilter().getKisiRef() != null) {
			return getSmsanlikgonderimlogFilter().getKisiRef();
		}else if (getObjectFromSessionFilter(ApplicationDescriptor._KISI_MODEL) != null){
			return (Kisi) getObjectFromSessionFilter(ApplicationDescriptor._KISI_MODEL);
		}else if(getObjectFromSessionFilter(ApplicationDescriptor._UYE_MODEL) != null){
			return (Uye) getObjectFromSessionFilter(ApplicationDescriptor._UYE_MODEL);
		}else {
			return null;
		}
	}

	public Smsanlikgonderimlog getSmsanlikgonderimlog() {
		smsanlikgonderimlog = (Smsanlikgonderimlog) getEntity();
		return smsanlikgonderimlog;
	}

	public void setSmsanlikgonderimlog(Smsanlikgonderimlog smsanlikgonderimlog) {
		this.smsanlikgonderimlog = smsanlikgonderimlog;
	}

	public SmsanlikgonderimlogFilter getSmsanlikgonderimlogFilter() {
		return smsanlikgonderimlogFilter;
	}

	public void setSmsanlikgonderimlogFilter(
			SmsanlikgonderimlogFilter smsanlikgonderimlogFilter) {
		this.smsanlikgonderimlogFilter = smsanlikgonderimlogFilter;
	}
	
	private Boolean onizlemeDialog=false;
	
	public Boolean getOnizlemeDialog() {
		return onizlemeDialog;
	}

	public void setOnizlemeDialog(Boolean onizlemeDialog) {
		this.onizlemeDialog = onizlemeDialog;
	}

	@Override
	public void setSelectedRID(String selectedRID) {
		super.setSelectedRID(selectedRID);
		setOnizlemeDialog(true);
	}
	public void onizlemedialogkapat(){
		setOnizlemeDialog(false);
	}


	@Override
	public BaseFilter getFilter() {
		return getSmsanlikgonderimlogFilter();
	}

	public EvetHayir getIcerikOrEditor() {
		return icerikOrEditor;
	}

	public void setIcerikOrEditor(EvetHayir icerikOrEditor) {
		this.icerikOrEditor = icerikOrEditor;
	}

	public boolean handleIcerikOrEditor(AjaxBehaviorEvent event1) {
		try {
			HtmlSelectOneMenu menuForIcerikOrEditor = (HtmlSelectOneMenu) event1
					.getComponent();
			if (menuForIcerikOrEditor.getValue() instanceof EvetHayir) {
				if (getIcerikOrEditor().equals(EvetHayir._EVET)) {
					return true;
				} else {
					return false;
				}

			} else {
				return false;
			}
		} catch (Exception e) {
			logYaz("Exception @" + getModelName() + "Controller :", e);
		}
		return true;
	}

	@Override
	public void save() {
		Date d = new Date();
		this.smsanlikgonderimlog.setTarih(d);
		this.smsanlikgonderimlog.setKisiRef((Kisi)getMasterEntity());
		this.smsanlikgonderimlog.setDurum(GonderimDurum._ILETILDI);
		super.save();
	}

	public long getSmsParameter() { 
		Long staticParamForRid = (long) 1;
		Long returnedParameter = (long) 0;
		@SuppressWarnings("unchecked")
		List<SistemParametre> sistemParameters = getDBOperator().load(
				SistemParametre.class.getSimpleName(), "o.rID="
						+ staticParamForRid, "o.rID");
		for (SistemParametre sistemParameter : sistemParameters) {
			returnedParameter = Long.parseLong(sistemParameter.getSmsuzunluk());

		}
		// logYaz(returnedParameter * 160);
		return returnedParameter * 160;
	}
} // class
