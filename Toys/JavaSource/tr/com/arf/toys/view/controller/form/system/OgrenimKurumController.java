package tr.com.arf.toys.view.controller.form.system; 
  
 
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.toys.db.filter.system.OgrenimKurumFilter;
import tr.com.arf.toys.db.model.system.OgrenimKurum;
import tr.com.arf.toys.view.controller.form._base.ToysBaseFilteredController;
  
  
@ManagedBean 
@ViewScoped 
public class OgrenimKurumController extends ToysBaseFilteredController {  
  
  private static final long serialVersionUID = 6531152165884980748L; 
  
	private OgrenimKurum ogrenimKurum;  
	private OgrenimKurumFilter ogrenimKurumFilter = new OgrenimKurumFilter();  
  
	public OgrenimKurumController() { 
		super(OgrenimKurum.class);  
		setDefaultValues(false);  
		setOrderField("kisaad");  
		setAutoCompleteSearchColumns(new String[]{"kisaad" , "ad"}); 
		setTable(null); 
		queryAction(); 
	} 
	
	
	public OgrenimKurum getOgrenimKurum() { 
		ogrenimKurum = (OgrenimKurum) getEntity(); 
		return ogrenimKurum; 
	} 
	
	public void setOgrenimKurum(OgrenimKurum ogrenimKurum) { 
		this.ogrenimKurum = ogrenimKurum; 
	} 
	
	public OgrenimKurumFilter getOgrenimKurumFilter() { 
		return ogrenimKurumFilter; 
	} 
	 
	public void setOgrenimKurumFilter(OgrenimKurumFilter ogrenimKurumFilter) {  
		this.ogrenimKurumFilter = ogrenimKurumFilter;  
	}  
	 
	@Override  
	public BaseFilter getFilter() {  
		return getOgrenimKurumFilter();  
	}   
	
} // class 
