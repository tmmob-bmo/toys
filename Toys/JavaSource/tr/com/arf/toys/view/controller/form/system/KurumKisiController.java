package tr.com.arf.toys.view.controller.form.system; 
  
 

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.primefaces.component.datatable.DataTable;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.framework.db.model._base.BaseEntity;
import tr.com.arf.toys.db.filter.system.KurumKisiFilter;
import tr.com.arf.toys.db.model.kisi.Kurum;
import tr.com.arf.toys.db.model.system.KurumKisi;
import tr.com.arf.toys.service.application.ApplicationDescriptor;
import tr.com.arf.toys.view.controller.form._base.ToysBaseFilteredController;
  
  
@ManagedBean 
@ViewScoped 
public class KurumKisiController extends ToysBaseFilteredController {  
  
  private static final long serialVersionUID = 6531152165884980748L; 
  	private DataTable table2 = new DataTable();
	private KurumKisi kurumKisi;  
	private KurumKisiFilter kurumKisiFilter = new KurumKisiFilter();  
  
	public KurumKisiController() { 
		super(KurumKisi.class);  
		setDefaultValues(false);  
		setTable(null); 
		if(getMasterEntity() != null){ 
			 if(getMasterEntity().getClass() == Kurum.class){
				setMasterObjectName(ApplicationDescriptor._KURUM_MODEL);
				setMasterOutcome(ApplicationDescriptor._KURUM_MODEL);	
				getKurumKisiFilter().setKurumRef((Kurum) getMasterEntity()); 
			}  
		}
		getKurumKisiFilter().createQueryCriterias();  
		queryAction(); 
	} 
	
	public KurumKisiController(Object object) { 
		super(KurumKisi.class);  
		setLoggable(false);
	}
	
	public BaseEntity getMasterEntity(){
		if(getObjectFromSessionFilter(ApplicationDescriptor._KURUM_MODEL) != null){
			return (Kurum) getObjectFromSessionFilter(ApplicationDescriptor._KURUM_MODEL);
		}  
		else {
			return null;
		}			
	} 
	
	
	public KurumKisi getKurumKisi() { 
		kurumKisi = (KurumKisi) getEntity(); 
		return kurumKisi; 
	} 
	
	public KurumKisiFilter getKurumKisiFilter() {
		return kurumKisiFilter;
	}


	public void setKurumKisiFilter(KurumKisiFilter kurumKisiFilter) {
		this.kurumKisiFilter = kurumKisiFilter;
	}


	public void setKurumKisi(KurumKisi kurumKisi) {
		this.kurumKisi = kurumKisi;
	}


	@Override  
	public BaseFilter getFilter() {  
		return getKurumKisiFilter();  
	}

	public DataTable getTable2() {
		return table2;
	}

	public void setTable2(DataTable table2) {
		this.table2 = table2;
	}   
	
} // class 
