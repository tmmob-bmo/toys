package tr.com.arf.toys.view.controller.form.egitim; 
 
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.toys.db.filter.egitim.GozetmenFilter;
import tr.com.arf.toys.db.model.egitim.Gozetmen;
import tr.com.arf.toys.view.controller.form._base.ToysBaseFilteredController;
  
  
@ManagedBean 
@ViewScoped 
public class GozetmenController extends ToysBaseFilteredController {  
  
  private static final long serialVersionUID = 6531152165884980748L; 
  
	private Gozetmen gozetmen;  
	private GozetmenFilter gozetmenFilter = new GozetmenFilter();  
  
	public GozetmenController() { 
		super(Gozetmen.class);  
		setDefaultValues(false);  
		setLoggable(false);  
		setTable(null); 
		queryAction();
	}
	
	public Gozetmen getGozetmen() {
		gozetmen=(Gozetmen) getEntity();
		return gozetmen;
	}

	public void setGozetmen(Gozetmen gozetmen) {
		this.gozetmen = gozetmen;
	}

	public GozetmenFilter getGozetmenFilter() {
		return gozetmenFilter;
	}

	public void setGozetmenFilter(GozetmenFilter gozetmenFilter) {
		this.gozetmenFilter = gozetmenFilter;
	}

	@Override
	public BaseFilter getFilter() {
		return getGozetmenFilter();
	}
	

	
} // class 
