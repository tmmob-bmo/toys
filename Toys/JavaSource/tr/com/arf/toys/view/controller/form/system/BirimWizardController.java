package tr.com.arf.toys.view.controller.form.system;

import java.util.regex.Pattern;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import tr.com.arf.framework.db.enumerated._common.EvetHayir;
import tr.com.arf.toys.db.enumerated.kisi.KimlikTip;
import tr.com.arf.toys.db.model.kisi.Kisi;
import tr.com.arf.toys.db.model.kisi.Kisikimlik;
import tr.com.arf.toys.db.model.kisi.Kurum;
import tr.com.arf.toys.db.model.system.Birim;
import tr.com.arf.toys.db.nonEntityModel.PojoKisi;
import tr.com.arf.toys.service.application.ApplicationDescriptor;
import tr.com.arf.toys.view.controller.form._base.ToysBaseController;

@ManagedBean(name = "birimWizardController")
@ViewScoped
public class BirimWizardController extends ToysBaseController {

	
	private static final long serialVersionUID = 6531152165884980748L;

	private Birim birim;
	private int wizardStep = 1;
	private Kisi kisi;
	private Kisikimlik kisikimlik;
//
//	
	private Kurum kurum;
	private String detailEditType; 
	
	private boolean kpsSorguSonucDondu = false;
	private boolean kpsKaydiBulundu = false;
	private boolean kisiVeritabanindaBulundu = false;
	
	private PojoKisi kisikimlikPojo = new PojoKisi();
//	
//	private Telefon telefon1;
//	private Telefon telefon2;
//	private Telefon telefon3;
//	private Telefon telefon4;
//
//	private Internetadres internetAdres1; 
//	
	private static final String PHONE_PATTERN = "0\\d{3}\\d{7}";
	private final Pattern pattern;
	
	public BirimWizardController() {
		super(Birim.class);
		setDefaultValues(false);
		pattern = Pattern.compile(PHONE_PATTERN);
		prepareForWizard();
		setTable(null); 
		queryAction();
	}

	private void prepareForWizard() {
		this.wizardStep = 1;
		super.insert();
		
		this.kisi = new Kisi(); 
		this.kisi.setKimliknotip(KimlikTip._TCKIMLIKNO);
		this.kisi.setAktif(EvetHayir._EVET); 
		this.kisikimlik = new Kisikimlik();
		this.birim = new Birim();
		removeObjectFromSessionFilter(ApplicationDescriptor._BIRIM_MODEL);
		
		this.kisikimlikPojo = new PojoKisi();

		this.detailEditType = "";
		this.kpsSorguSonucDondu = false;
		this.kpsKaydiBulundu = false;
		this.kisiVeritabanindaBulundu = false;
		
	}

	public Birim getBirim() {
		return birim;
	}

	public void setBirim(Birim birim) {
		this.birim = birim;
	}

	@Override
	public int getWizardStep() {
		return wizardStep;
	}

	@Override
	public void setWizardStep(int wizardStep) {
		this.wizardStep = wizardStep;
	}

	public Kisi getKisi() {
		return kisi;
	}

	public void setKisi(Kisi kisi) {
		this.kisi = kisi;
	}

	public Kisikimlik getKisikimlik() {
		return kisikimlik;
	}

	public void setKisikimlik(Kisikimlik kisikimlik) {
		this.kisikimlik = kisikimlik;
	}

	public Kurum getKurum() {
		return kurum;
	}

	public void setKurum(Kurum kurum) {
		this.kurum = kurum;
	}

	public String getDetailEditType() {
		return detailEditType;
	}

	public void setDetailEditType(String detailEditType) {
		this.detailEditType = detailEditType;
	}

	public boolean isKpsSorguSonucDondu() {
		return kpsSorguSonucDondu;
	}

	public void setKpsSorguSonucDondu(boolean kpsSorguSonucDondu) {
		this.kpsSorguSonucDondu = kpsSorguSonucDondu;
	}

	public boolean isKpsKaydiBulundu() {
		return kpsKaydiBulundu;
	}

	public void setKpsKaydiBulundu(boolean kpsKaydiBulundu) {
		this.kpsKaydiBulundu = kpsKaydiBulundu;
	}

	public boolean isKisiVeritabanindaBulundu() {
		return kisiVeritabanindaBulundu;
	}

	public void setKisiVeritabanindaBulundu(boolean kisiVeritabanindaBulundu) {
		this.kisiVeritabanindaBulundu = kisiVeritabanindaBulundu;
	}

	public PojoKisi getKisikimlikPojo() {
		return kisikimlikPojo;
	}

	public void setKisikimlikPojo(PojoKisi kisikimlikPojo) {
		this.kisikimlikPojo = kisikimlikPojo;
	}

	public static String getPhonePattern() {
		return PHONE_PATTERN;
	}

	public Pattern getPattern() {
		return pattern;
	}
	
	
	
	
} // class 
