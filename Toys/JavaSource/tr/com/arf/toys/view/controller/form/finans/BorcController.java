package tr.com.arf.toys.view.controller.form.finans;

import java.math.BigDecimal;
import java.math.MathContext;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.framework.db.model._base.BaseEntity;
import tr.com.arf.framework.utility.tool.BigDecimalUtil;
import tr.com.arf.toys.db.enumerated.finans.BorcDurum;
import tr.com.arf.toys.db.filter.finans.BorcFilter;
import tr.com.arf.toys.db.model.finans.Borc;
import tr.com.arf.toys.db.model.kisi.Kisi;
import tr.com.arf.toys.db.model.system.Birim;
import tr.com.arf.toys.service.application.ApplicationDescriptor;
import tr.com.arf.toys.utility.logUtils.LogService;
import tr.com.arf.toys.view.controller.form._base.ToysBaseFilteredController;

@ManagedBean
@ViewScoped
public class BorcController extends ToysBaseFilteredController {

	private static final long serialVersionUID = 6531152165884980748L;

	private Borc borc;
	private BorcFilter borcFilter = new BorcFilter();

	public BorcController() {
		super(Borc.class);
		setDefaultValues(false);
		setOrderField("miktar");
		setAutoCompleteSearchColumns(new String[] { "miktar" });
		setTable(null);
		if (getMasterEntity() != null) {
			if (getMasterEntity().getClass() == Kisi.class) {
				setMasterObjectName(ApplicationDescriptor._KISI_MODEL);
				setMasterOutcome(ApplicationDescriptor._KISI_MODEL);
				getBorcFilter().setKisiRef((Kisi) getMasterEntity());
			} else {
				setMasterOutcome(null);
			}
			getBorcFilter().createQueryCriterias();
		}
		queryAction();
	}

	public BorcController(Object object) {
		super(Borc.class);
	}

	public BaseEntity getMasterEntity() {
		if (getObjectFromSessionFilter(ApplicationDescriptor._KISI_MODEL) != null) {
			return (Kisi) getObjectFromSessionFilter(ApplicationDescriptor._KISI_MODEL);
		} else {
			return null;
		}
	}

	public Borc getBorc() {
		borc = (Borc) getEntity();
		return borc;
	}

	public void setBorc(Borc borc) {
		this.borc = borc;
	}

	public BorcFilter getBorcFilter() {
		return borcFilter;
	}

	public void setBorcFilter(BorcFilter borcFilter) {
		this.borcFilter = borcFilter;
	}

	@Override
	public BaseFilter getFilter() {
		return getBorcFilter();
	}

	public String borcodeme() {
		if (!setSelected()) {
			return "";
		}
		getSessionUser().setLegalAccess(1);
		putObjectToSessionFilter(ApplicationDescriptor._BORC_MODEL, getBorc());
		return ApplicationDescriptor._BORCODEME_OUTCOME;
	}

	@Override
	public void insert() {
		super.insert();
		if (getMasterEntity() != null) {
			if (getMasterEntity().getClass() == Kisi.class) {
				getBorc().setKisiRef((Kisi) getMasterEntity());
			} else if (getMasterEntity().getClass() == Birim.class) {
				getBorc().setKisiRef((Kisi) getMasterEntity());
			}
		}
	}

	@Override
	public void save() {
		setBorc(borcDurumuGuncelle(getBorc()));
		super.justSave();
	}

	public static Borc borcDurumuGuncelle(Borc borc) {
		try {
			BigDecimal kalan = BigDecimalUtil.changeToCurrency(borc.getMiktar().subtract(borc.getOdenen(), MathContext.DECIMAL32));
			if (kalan.doubleValue() > 0) {
				if (borc.getBorcdurum() != BorcDurum._BORCLU) {
					borc.setBorcdurum(BorcDurum._BORCLU);
				}
			} else if (kalan.doubleValue() == 0) {
				if (borc.getBorcdurum() != BorcDurum._ODENMIS) {
					borc.setBorcdurum(BorcDurum._ODENMIS);
				}
			} else if (kalan.doubleValue() < 0) {
				if (borc.getBorcdurum() != BorcDurum._ALACAKLI) {
					borc.setBorcdurum(BorcDurum._ALACAKLI);
				}
			}
		} catch (Exception e) {
			LogService.logYaz("Exception @BorcController :", e);
		}
		return borc;
	}

} // class
