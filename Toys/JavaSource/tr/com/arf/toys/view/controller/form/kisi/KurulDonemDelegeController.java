package tr.com.arf.toys.view.controller.form.kisi;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.model.SelectItem;

import tr.com.arf.framework.db.enumerated._common.EvetHayir;
import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.framework.utility.exception.data.DBException;
import tr.com.arf.toys.db.enumerated.kisi.KurulTuru;
import tr.com.arf.toys.db.enumerated.kisi.UyeTuru;
import tr.com.arf.toys.db.enumerated.uye.UyeDurum;
import tr.com.arf.toys.db.enumerated.uye.UyeKayitDurum;
import tr.com.arf.toys.db.filter.kisi.KurulDonemDelegeFilter;
import tr.com.arf.toys.db.model.kisi.Kisi;
import tr.com.arf.toys.db.model.kisi.KurulDonem;
import tr.com.arf.toys.db.model.kisi.KurulDonemDelege;
import tr.com.arf.toys.db.model.kisi.KurulDonemUye;
import tr.com.arf.toys.db.model.uye.Uye;
import tr.com.arf.toys.service.application.ApplicationDescriptor;
import tr.com.arf.toys.view.controller.form._base.ToysBaseFilteredController;

@ManagedBean
@ViewScoped
public class KurulDonemDelegeController extends ToysBaseFilteredController {

	private static final long serialVersionUID = 6531152165884980748L;

	private KurulDonemDelege kurulDonemDelege;
	private KurulDonemDelegeFilter kurulDonemDelegeFilter = new KurulDonemDelegeFilter();
	private boolean dogalDelegePanelRendered = false;
	private Long kurulDonemId;
	private String sicilno = "";
	private String adsoyad = "";
	private boolean tumuyeleridelegeyapPanelRendered;
	private String bulunanUyeMesaj = " ";
	private int progress = 0;
	private boolean stopPoll;
	private boolean filteredPanelRendered = true;

	public KurulDonemDelegeController() {

		super(KurulDonemDelege.class);

		setDefaultValues(false);
		setOrderField("kurulDonemRef.rID");
		setLoggable(false);
		setAutoCompleteSearchColumns(new String[] { "kurulDonemRef" });
		setTable(null);

		if (getMasterEntity() != null) {
			setMasterObjectName(ApplicationDescriptor._KURULDONEM_MODEL);
			setMasterOutcome(ApplicationDescriptor._KURULDONEM_MODEL);
			getKurulDonemDelegeFilter().setKurulDonemRef(getMasterEntity());
			getKurulDonemDelegeFilter().createQueryCriterias();
		}
		this.stopPoll = true;
		setFilteredPanelRendered(true);
		queryAction();
	}

	public KurulDonem getMasterEntity() {
		if (getKurulDonemDelegeFilter().getKurulDonemRef() != null) {
			return getKurulDonemDelegeFilter().getKurulDonemRef();
		} else {
			return (KurulDonem) getObjectFromSessionFilter(ApplicationDescriptor._KURULDONEM_MODEL);
		}
	}

	public KurulDonemDelege getKurulDonemDelege() {
		kurulDonemDelege = (KurulDonemDelege) getEntity();
		return kurulDonemDelege;
	}

	public void setKurulDonemDelege(KurulDonemDelege kurulDonemDelege) {
		this.kurulDonemDelege = kurulDonemDelege;
	}

	public KurulDonemDelegeFilter getKurulDonemDelegeFilter() {
		return kurulDonemDelegeFilter;
	}

	public void setKurulDonemDelegeFilter(KurulDonemDelegeFilter kurulDonemDelegeFilter) {
		this.kurulDonemDelegeFilter = kurulDonemDelegeFilter;
	}

	public boolean isDogalDelegePanelRendered() {
		return dogalDelegePanelRendered;
	}

	public void setDogalDelegePanelRendered(boolean dogalDelegePanelRendered) {
		this.dogalDelegePanelRendered = dogalDelegePanelRendered;
	}

	public Long getKurulDonemId() {
		return kurulDonemId;
	}

	public void setKurulDonemId(Long kurulDonemId) {
		this.kurulDonemId = kurulDonemId;
	}

	public String getAdsoyad() {
		return adsoyad;
	}

	public void setAdsoyad(String adsoyad) {
		this.adsoyad = adsoyad;
	}

	public String getSicilno() {
		return sicilno;
	}

	public void setSicilno(String sicilno) {
		this.sicilno = sicilno;
	}

	public String getBulunanUyeMesaj() {
		return bulunanUyeMesaj;
	}

	public void setBulunanUyeMesaj(String bulunanUyeMesaj) {
		this.bulunanUyeMesaj = bulunanUyeMesaj;
	}

	public int getProgress() {
		return progress;
	}

	public void setProgress(int progress) {
		this.progress = progress;
	}

	public boolean isTumuyeleridelegeyapPanelRendered() {
		return tumuyeleridelegeyapPanelRendered;
	}

	public void setTumuyeleridelegeyapPanelRendered(boolean tumuyeleridelegeyapPanelRendered) {
		this.tumuyeleridelegeyapPanelRendered = tumuyeleridelegeyapPanelRendered;
	}

	public boolean isStopPoll() {
		return stopPoll;
	}

	public void setStopPoll(boolean stopPoll) {
		this.stopPoll = stopPoll;
	}

	public boolean isFilteredPanelRendered() {
		return filteredPanelRendered;
	}

	public void setFilteredPanelRendered(boolean filteredPanelRendered) {
		this.filteredPanelRendered = filteredPanelRendered;
	}

	public void closeAllPanels() {
		setDogalDelegePanelRendered(false);
		setEditPanelRendered(false);
	}

	public void dogalDelegePanelAc() {
		closeAllPanels();
		setFilteredPanelRendered(false);
		setDogalDelegePanelRendered(true);
	}

	public void dogalDelegePanelKapat() {
		setFilteredPanelRendered(true);
		setDogalDelegePanelRendered(false);
	}

	@Override
	public BaseFilter getFilter() {
		return getKurulDonemDelegeFilter();
	}

	@Override
	public void resetFilter() {
		super.resetFilterOnly();
		if (getMasterEntity() != null) {
			getKurulDonemDelegeFilter().setKurulDonemRef(getMasterEntity());
		}
		queryAction();
	}

	public boolean isEditable(){
		if(getMasterEntity().getDonemRef().getAnadonemRef().getAktif() == EvetHayir._EVET && getMasterEntity().getKurulRef().getKurulturu() == KurulTuru._GENELKURUL){
			return true;
		} else {
			return false;
		}
	}

	@Override
	public void insert() {
			super.insert();
		// fillDualListFor(Uye.class.getSimpleName(),"o.rID < 0", "o.rID");
		setFilteredPanelRendered(false);
		getKurulDonemDelege().setKurulDonemRef(getKurulDonemDelegeFilter().getKurulDonemRef());
	}

	@SuppressWarnings("unchecked")
	public SelectItem[] getSelectItemListForUye() {
		List<KurulDonemUye> entityList = getDBOperator().load(KurulDonemUye.class.getSimpleName(), "o.kurulDonemRef.rID = " + getKurulDonemDelege().getKurulDonemRef().getRID(),
				"rID");
		if (entityList != null) {
			SelectItem[] selectItemList = new SelectItem[entityList.size() + 1];
			selectItemList[0] = new SelectItem(null, "");
			int i = 1;
			for (KurulDonemUye entity : entityList) {
				selectItemList[i++] = new SelectItem(entity.getUyeRef(), entity.getUyeRef().getUIString());
			}
			return selectItemList;
		}
		return new SelectItem[0];
	}

	@Override
	public void save() {

		if (getKurulDonemDelege().getUyeRef() != null && getMasterEntity() != null) {
			super.justSave();
			getKurulDonemDelege().setKurulDonemRef(getMasterEntity());
			super.insert();
		} else {
			createGenericMessage("Delege Eklenmedi !", FacesMessage.SEVERITY_INFO);
		}
		queryAction();

	}

	private SelectItem[] selectKurulDonemListesi = new SelectItem[0];

	public SelectItem[] getSelectKurulDonemListesi() {
		return selectKurulDonemListesi;
	}

	public void setSelectKurulDonemListesi(SelectItem[] selectKurulDonemListesi) {
		this.selectKurulDonemListesi = selectKurulDonemListesi;
	}

	@SuppressWarnings("unchecked")
	public SelectItem[] getKurulDonemListesi() {
		List<KurulDonem> kurulDonemList = getDBOperator().load(
				KurulDonem.class.getSimpleName(),
				"o.donemRef.anadonemRef.aktif=" + EvetHayir._EVET.getCode() + " " + " AND ( o.kurulRef.kurulturu = " + KurulTuru._YONETIMKURULU.getCode()
						+ " OR o.kurulRef.kurulturu= " + KurulTuru._DENETLEMEKURULU.getCode() + " OR o.kurulRef.kurulturu = " + KurulTuru._ONURKURULU.getCode() + " ) ", "");
		if (!isEmpty(kurulDonemList)) {
			selectKurulDonemListesi = new SelectItem[kurulDonemList.size() + 1];
			selectKurulDonemListesi[0] = new SelectItem(null, "kayit seciniz");
			for (int x = 0; x < kurulDonemList.size(); x++) {
				selectKurulDonemListesi[x + 1] = new SelectItem(kurulDonemList.get(x).getRID(), kurulDonemList.get(x).getUIString());
			}
		} else {
			selectKurulDonemListesi = new SelectItem[0];
		}
		return selectKurulDonemListesi;

	}

	@SuppressWarnings("unchecked")
	public void dogalDelegeler() {
		List<KurulDonemUye> kurulDonemUyeListesi = getDBOperator().load(KurulDonemUye.class.getSimpleName(),
				"o.kurulDonemRef.rID=" + getKurulDonemId() + " AND o.uyeTuru=" + UyeTuru._ASIL.getCode() + " AND o.aktif=" + EvetHayir._EVET.getCode(), "");
		if (!isEmpty(kurulDonemUyeListesi)) {
			for (KurulDonemUye kurulDonemUye : kurulDonemUyeListesi) {
				try {
					if (getDBOperator().recordCount(KurulDonemDelege.class.getSimpleName(), "o.uyeRef=" + kurulDonemUye.getUyeRef().getRID()) <= 0) {
						kurulDonemDelege = new KurulDonemDelege();
						kurulDonemDelege.setUyeRef(kurulDonemUye.getUyeRef());
						kurulDonemDelege.setKurulDonemRef(getMasterEntity());
						getDBOperator().insert(kurulDonemDelege);
					}

				} catch (DBException e) {
					logYaz("Exception @" + getModelName() + "Controller :", e);
				}
			}

		}

		dogalDelegePanelKapat();
		queryAction();
	}

	public void uyeSorgula() {
		if (getSicilno() != null) {
			try {
				if (getDBOperator().recordCount(
						Uye.class.getSimpleName(),
						"o.kayitdurum=" + UyeKayitDurum._ONAYLANMIS.getCode() + " AND o.uyedurum=" + UyeDurum._AKTIFUYE.getCode() + " AND UPPER(o.sicilno) LIKE UPPER('%"
								+ getSicilno() + "%')" + " AND o.rID IN ( SELECT k.uyeRef.rID FROM " + KurulDonemDelege.class.getSimpleName() + " AS k )"
								+ " AND o.kisiRef.rID IN ( SELECT m.rID FROM  " + Kisi.class.getSimpleName() + " AS m  " + "WHERE m.aktif=" + EvetHayir._EVET.getCode() + " )") > 0) {
					setAdsoyad("Bu üye delegelerde kayıtlı olduğu için tekrardan ekleyemezsiniz !");
				} else if (getDBOperator().recordCount(
						Uye.class.getSimpleName(),
						"o.kayitdurum=" + UyeKayitDurum._ONAYLANMIS.getCode() + " AND o.uyedurum=" + UyeDurum._AKTIFUYE.getCode() + " AND UPPER(o.sicilno) LIKE UPPER('%"
								+ getSicilno() + "%')" + " AND o.rID NOT IN ( SELECT k.uyeRef.rID FROM " + KurulDonemDelege.class.getSimpleName() + " AS k )"
								+ " AND o.kisiRef.rID IN ( SELECT m.rID FROM  " + Kisi.class.getSimpleName() + " AS m  " + "WHERE m.aktif=" + EvetHayir._EVET.getCode() + " )") > 0) {
					Uye uye = (Uye) getDBOperator().load(
							Uye.class.getSimpleName(),
							"o.kayitdurum=" + UyeKayitDurum._ONAYLANMIS.getCode() + " AND o.uyedurum=" + UyeDurum._AKTIFUYE.getCode() + " AND UPPER(o.sicilno) LIKE UPPER('%"
									+ getSicilno() + "%')" + " AND o.rID NOT IN ( SELECT k.uyeRef.rID FROM " + KurulDonemDelege.class.getSimpleName() + " AS k )"
									+ " AND o.kisiRef.rID IN ( SELECT m.rID FROM  " + Kisi.class.getSimpleName() + " AS m  " + "WHERE m.aktif=" + EvetHayir._EVET.getCode() + " )",
							"").get(0);
					getKurulDonemDelege().setUyeRef(uye);
					setAdsoyad(uye.getKisiRef().getUIStringShort());
				} else {
					setAdsoyad("Üye Bulunamadı !");
				}

			} catch (DBException e) {
				logYaz("Error @Kurul Donem Delege Controller Uye Sorgula" + e.getMessage());
				logYaz("Exception @" + getModelName() + "Controller :", e);
			}
		}
	}

	@SuppressWarnings("unchecked")
	public void tumUyeleriDelegeYap() throws Exception {

		if (getDBOperator().recordCount(
				Uye.class.getSimpleName(),
				"o.kayitdurum=" + UyeKayitDurum._ONAYLANMIS.getCode() + " AND o.uyedurum=" + UyeDurum._AKTIFUYE.getCode() + " AND o.rID NOT IN ( SELECT k.uyeRef.rID FROM "
						+ KurulDonemDelege.class.getSimpleName() + " AS k )" + " AND o.kisiRef.rID IN ( SELECT m.rID FROM  " + Kisi.class.getSimpleName() + " AS m  "
						+ "WHERE m.aktif=" + EvetHayir._EVET.getCode() + " )") > 0) {
			stopPoll = true;
			List<Uye> uyeListesi = getDBOperator().load(
					Uye.class.getSimpleName(),
					"o.kayitdurum=" + UyeKayitDurum._ONAYLANMIS.getCode() + " AND o.uyedurum=" + UyeDurum._AKTIFUYE.getCode() + " AND o.rID NOT IN ( SELECT k.uyeRef.rID FROM "
							+ KurulDonemDelege.class.getSimpleName() + " AS k )" + " AND o.kisiRef.rID IN ( SELECT m.rID FROM  " + Kisi.class.getSimpleName() + " AS m  "
							+ "WHERE m.aktif=" + EvetHayir._EVET.getCode() + " )", "");
			int ax = 0;
			if (getMasterEntity() != null) {
				for (Uye uye : uyeListesi) {
					ax++;
					setProgress(ax * 100 / uyeListesi.size());
					KurulDonemDelege kurulDonemDlg = new KurulDonemDelege();
					kurulDonemDlg.setUyeRef(uye);
					kurulDonemDlg.setKurulDonemRef(getMasterEntity());
					getDBOperator().insert(kurulDonemDlg);
				}
				createGenericMessage("Tüm Üyeler Delegelere Eklendi!", FacesMessage.SEVERITY_INFO);
			} else {
				createGenericMessage("Üyeler Eklenirken Hata Meydana Geldi! Lütfen Sistem Yönetici İle İrtibata Geçiniz!", FacesMessage.SEVERITY_INFO);
			}

		}
		queryAction();

	}

	@Override
	public void cancel() {
		setTumuyeleridelegeyapPanelRendered(false);
		setFilteredPanelRendered(true);
		setProgress(0);
		this.stopPoll = true;
		super.cancel();
	}

	public void tumuyeleridelegeyapmaSayfa() {

		String whereCond = "o.kayitdurum=" + UyeKayitDurum._ONAYLANMIS.getCode() + " AND o.uyedurum=" + UyeDurum._AKTIFUYE.getCode()
				+ " AND o.rID NOT IN ( SELECT k.uyeRef.rID FROM " + KurulDonemDelege.class.getSimpleName() + " AS k )" + " AND o.kisiRef.rID IN ( SELECT m.rID FROM  "
				+ Kisi.class.getSimpleName() + " AS m  " + "WHERE m.aktif=" + EvetHayir._EVET.getCode() + " )";
		try {
			if (getDBOperator().recordCount(Uye.class.getSimpleName(), whereCond) > 0) {
				setTumuyeleridelegeyapPanelRendered(true);
				setEditPanelRendered(false);
				setFilteredPanelRendered(false);
				progress = 0;
				this.stopPoll = false;
				setBulunanUyeMesaj(getDBOperator().recordCount(Uye.class.getSimpleName(), whereCond) + " üye delege yapılacak!");
			} else {
				createGenericMessage("Tüm Üyeler Delege Listesine Ekli Olduğu İçin Tekrar Ekleme Yapamazsınız ! ", FacesMessage.SEVERITY_INFO);
			}
		} catch (DBException e) {
			logYaz("Exception @" + getModelName() + "Controller :", e);
		}

	}

} // class
