package tr.com.arf.toys.view.controller.form.evrak; 
  
 
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.toys.db.filter.evrak.EvrakekFilter;
import tr.com.arf.toys.db.model.evrak.Evrak;
import tr.com.arf.toys.db.model.evrak.Evrakek;
import tr.com.arf.toys.service.application.ApplicationDescriptor;
import tr.com.arf.toys.view.controller.form._base.ToysBaseFilteredController;
  
  
@ManagedBean 
@ViewScoped 
public class EvrakekController extends ToysBaseFilteredController {  
  
  private static final long serialVersionUID = 6531152165884980748L; 
  
	private Evrakek evrakek;  
	private EvrakekFilter evrakekFilter = new EvrakekFilter();  
  
	public EvrakekController() { 
		super(Evrakek.class);  
		setDefaultValues(false);  
		setOrderField("path");  
		setLoggable(false);  
		setAutoCompleteSearchColumns(new String[]{"path"}); 
		setTable(null); 
		if(getMasterEntity() != null){ 
			setMasterObjectName(ApplicationDescriptor._EVRAK_MODEL);
			setMasterOutcome(ApplicationDescriptor._EVRAK_MODEL);
			getEvrakekFilter().setEvrakRef(getMasterEntity());
			getEvrakekFilter().createQueryCriterias();   
		} 
		queryAction(); 
	} 
	
	public EvrakekController(Object object) {
		super(Evrakek.class);
		setLoggable(true);
	}
 
	public Evrak getMasterEntity(){
		if(getEvrakekFilter().getEvrakRef() != null){
			return getEvrakekFilter().getEvrakRef();
		} else {
			return (Evrak) getObjectFromSessionFilter(ApplicationDescriptor._EVRAK_MODEL);
		}			
	} 
 
	
	public Evrakek getEvrakek() { 
		evrakek = (Evrakek) getEntity(); 
		return evrakek; 
	} 
	
	public void setEvrakek(Evrakek evrakek) { 
		this.evrakek = evrakek; 
	} 
	
	public EvrakekFilter getEvrakekFilter() { 
		return evrakekFilter; 
	} 
	 
	public void setEvrakekFilter(EvrakekFilter evrakekFilter) {  
		this.evrakekFilter = evrakekFilter;  
	}  
	 
	@Override  
	public BaseFilter getFilter() {  
		return getEvrakekFilter();  
	}  	
 
	@Override 
	public void resetFilter() {
		super.resetFilterOnly();
		if(getMasterEntity() != null){
			getEvrakekFilter().setEvrakRef(getMasterEntity()); 
		}
		queryAction();
	}
 
	@Override	
	public void insert() {	
		super.insert();	
		getEvrakek().setEvrakRef(getEvrakekFilter().getEvrakRef());	
	}	
	
	/*public String evrakek() {	
		if (!setSelected()) return "";	
		getSessionUser().setLegalAccess(1);	
		putObjectToSessionFilter(ApplicationDescriptor._EVRAK_MODEL, getEvrak());
		return ManagedBeanLocator.locateMenuController().gotoPage("menu_Evrakek"); 
	} */
} // class 
