package tr.com.arf.toys.view.controller.form.egitim; 
 
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import tr.com.arf.framework.db.enumerated._common.EvetHayir;
import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.framework.db.model._base.BaseEntity;
import tr.com.arf.framework.utility.exception.data.DBException;
import tr.com.arf.toys.db.filter.egitim.SinavFilter;
import tr.com.arf.toys.db.model.egitim.Sinav;
import tr.com.arf.toys.service.application.ApplicationDescriptor;
import tr.com.arf.toys.service.application.ManagedBeanLocator;
import tr.com.arf.toys.view.controller.form._base.ToysBaseFilteredController;
  
  
@ManagedBean 
@ViewScoped 
public class SinavController extends ToysBaseFilteredController {  
  
  private static final long serialVersionUID = 6531152165884980748L; 
  
	private Sinav sinav;  
	private SinavFilter sinavFilter = new SinavFilter();  
	private Boolean yayinda;
	private EgitimkatilimciController egitimkatilimciController = new EgitimkatilimciController();
	
	public SinavController() { 
		super(Sinav.class);  
		setDefaultValues(false);  
		setLoggable(false);  
		setTable(null); 
		queryAction();
	}
	
	public Sinav getSinav() {
		sinav=(Sinav) getEntity();
		return sinav;
	}

	public void setSinav(Sinav sinav) {
		this.sinav = sinav;
	}

	public SinavFilter getSinavFilter() {
		return sinavFilter;
	}

	public void setSinavFilter(SinavFilter sinavFilter) {
		this.sinavFilter = sinavFilter;
	}

	public Boolean getYayinda() {
		return yayinda;
	}

	public void setYayinda(Boolean yayinda) {
		this.yayinda = yayinda;
	}

	public EgitimkatilimciController getEgitimkatilimciController() {
		return egitimkatilimciController;
	}

	public void setEgitimkatilimciController(
			EgitimkatilimciController egitimkatilimciController) {
		this.egitimkatilimciController = egitimkatilimciController;
	}

	@Override
	public BaseFilter getFilter() {
		return getSinavFilter();
	}
	
	@Override
		public void update() {
			if (this.sinav.getSehirRef()!=null){
				changeIlce(this.sinav.getSehirRef().getRID());
			}	
			super.update();
		}
	@Override
		public void insert() {
			if (sinav!=null){
					if(sinav.getYayinda()==EvetHayir._EVET){
						setYayinda(true);
					}else {
						setYayinda(false);
					}
				}
				super.insert();
		}
	
	@Override
		public void save() {
			if (yayinda){
				sinav.setYayinda(EvetHayir._EVET);
			}else{
				sinav.setYayinda(EvetHayir._HAYIR);
			}
			super.save();
		}
	
	public String sinavDegerlendirici() {	
		if (!setSelected()) {
			return "";
		}	
		getSessionUser().setLegalAccess(1);	
		putObjectToSessionFilter(ApplicationDescriptor._SINAV_MODEL, getSinav());
		return ManagedBeanLocator.locateMenuController().gotoPage("menu_SinavDegerlendirici"); 
	} 
	
	public String sinavGozetmen() {	
		if (!setSelected()) {
			return "";
		}	
		getSessionUser().setLegalAccess(1);	
		putObjectToSessionFilter(ApplicationDescriptor._SINAV_MODEL, getSinav());
		return ManagedBeanLocator.locateMenuController().gotoPage("menu_SinavGozetmen"); 
	} 
	
	public String sinavKatilimci() {	
		if (!setSelected()) {
			return "";
		}	
		getSessionUser().setLegalAccess(1);	
		putObjectToSessionFilter(ApplicationDescriptor._SINAV_MODEL, getSinav());
		return ManagedBeanLocator.locateMenuController().gotoPage("menu_SinavKatilimci"); 
	} 
	
	private Boolean egitimNotuPanelAc=false;
	
	public Boolean getEgitimNotuPanelAc() {
		return egitimNotuPanelAc;
	}

	public void setEgitimNotuPanelAc(Boolean egitimNotuPanelAc) {
		this.egitimNotuPanelAc = egitimNotuPanelAc;
	}
	
	@SuppressWarnings("unchecked")
	public void katilimNotGuncelle() {
		setEgitimNotuPanelAc(true);		
		try {
			if (getDBOperator().recordCount(ApplicationDescriptor._EGITIMKATILIMCI_MODEL,"o.egitimRef.rID=" +
					this.sinav.getEgitimRef().getRID())>0){
				List<BaseEntity> itemList = new ArrayList<BaseEntity>();   
				itemList=getDBOperator().load(ApplicationDescriptor._EGITIMKATILIMCI_MODEL,
						"o.egitimRef.rID=" +sinav.getEgitimRef().getRID(),"o.rID");
				this.egitimkatilimciController.setTable(null);
				this.egitimkatilimciController.getEgitimkatilimciFilter().setEgitimRef(this.sinav.getEgitimRef());
				this.egitimkatilimciController.setList(itemList);
				this.egitimkatilimciController.getEgitimkatilimciFilter().createQueryCriterias();
				this.egitimkatilimciController.queryAction();
			}
		} catch (DBException e) {
			logYaz("SinavController @katilimNotGuncelle = " + e.getMessage());
			e.printStackTrace();
		}
	}
	
	public void notVer(){
		try {
			if (!getList().isEmpty()){
				for (BaseEntity base : getList()){
					getDBOperator().update(base);
				}
			}
		} catch (DBException e) {
			logYaz("SinavController @notVer =" + e.getMessage());
			e.printStackTrace();
		}
		setEgitimNotuPanelAc(false);
		this.queryAction();
		
	}
	
	public void dialogKapat(){
		setEgitimNotuPanelAc(false);
		this.queryAction();
	}


	
} // class 
