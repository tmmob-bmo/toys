package tr.com.arf.toys.view.controller.form.egitim; 
 
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.toys.db.filter.egitim.SinavKatilimciFilter;
import tr.com.arf.toys.db.model.egitim.Sinav;
import tr.com.arf.toys.db.model.egitim.SinavKatilimci;
import tr.com.arf.toys.service.application.ApplicationDescriptor;
import tr.com.arf.toys.view.controller.form._base.ToysBaseFilteredController;
  
  
@ManagedBean 
@ViewScoped 
public class SinavKatilimciController extends ToysBaseFilteredController {  
  
  private static final long serialVersionUID = 6531152165884980748L; 
  
	private SinavKatilimci sinavKatilimci;  
	private SinavKatilimciFilter sinavKatilimciFilter = new SinavKatilimciFilter();  
  
	public SinavKatilimciController() { 
		super(SinavKatilimci.class);  
		setDefaultValues(false);  
		setLoggable(false);  
		setTable(null); 
		if (getMasterEntity() != null) {
			setMasterObjectName(ApplicationDescriptor._SINAV_MODEL);
			setMasterOutcome(ApplicationDescriptor._SINAV_MODEL);
			getSinavKatilimciFilter().setSinavRef(getMasterEntity());
			getSinavKatilimciFilter().createQueryCriterias();
		}
		queryAction();
	}
	
	public Sinav getMasterEntity() {
		if (getSinavKatilimciFilter().getSinavRef() != null) {
			return getSinavKatilimciFilter().getSinavRef();
		} else {
			return (Sinav) getObjectFromSessionFilter(ApplicationDescriptor._SINAV_MODEL);
		}
	}
	
	public SinavKatilimci getSinavKatilimci() {
		sinavKatilimci=(SinavKatilimci) getEntity();
		return sinavKatilimci;
	}

	public void setSinavKatilimci(SinavKatilimci sinavKatilimci) {
		this.sinavKatilimci = sinavKatilimci;
	}

	public SinavKatilimciFilter getSinavKatilimciFilter() {
		return sinavKatilimciFilter;
	}

	public void setSinavKatilimciFilter(
			SinavKatilimciFilter sinavKatilimciFilter) {
		this.sinavKatilimciFilter = sinavKatilimciFilter;
	}

	@Override  
	public BaseFilter getFilter() {  
		return getSinavKatilimciFilter(); 
	}  
	
	@Override
		public void save() {
			
			if (getMasterEntity()!=null){
				sinavKatilimci.setSinavRef(getMasterEntity());
			}else {
				createGenericMessage("Sinav Katilimci Bilgileri Boş Geldiğinden Dolayı Kaydetme İşlemini"
						+ " Gerçekleştiremiyoruz!", FacesMessage.SEVERITY_INFO);
				return ;
			}
			super.save();
			queryAction();
		}
	
} // class 
