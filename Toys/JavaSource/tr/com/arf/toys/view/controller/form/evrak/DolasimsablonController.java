package tr.com.arf.toys.view.controller.form.evrak; 
  
 
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.toys.db.filter.evrak.DolasimsablonFilter;
import tr.com.arf.toys.db.model.evrak.Dolasimsablon;
import tr.com.arf.toys.db.model.evrak.Dolasimsablondetay;
import tr.com.arf.toys.service.application.ApplicationDescriptor;
import tr.com.arf.toys.service.application.ManagedBeanLocator;
import tr.com.arf.toys.view.controller.form._base.ToysBaseFilteredController;
  
  
@ManagedBean 
@ViewScoped 
public class DolasimsablonController extends ToysBaseFilteredController {  
  
  private static final long serialVersionUID = 6531152165884980748L; 
  
	private Dolasimsablon dolasimsablon;  
	private DolasimsablonFilter dolasimsablonFilter = new DolasimsablonFilter();  
  
	public DolasimsablonController() { 
		super(Dolasimsablon.class);  
		setDefaultValues(false);  
		setOrderField("ad");  
		setLoggable(false);  
		setAutoCompleteSearchColumns(new String[]{"ad"}); 
		setTable(null); 
		if(!ManagedBeanLocator.locateSessionUser().isSuperUser()){
			getDolasimsablonFilter().setBirimRef(getSessionUser().getPersonelRef().getBirimRef());
		}
		queryAction(); 
	} 
	
	
	public Dolasimsablon getDolasimsablon() { 
		dolasimsablon = (Dolasimsablon) getEntity(); 
		return dolasimsablon; 
	} 
	
	public void setDolasimsablon(Dolasimsablon dolasimsablon) { 
		this.dolasimsablon = dolasimsablon; 
	} 
	
	public DolasimsablonFilter getDolasimsablonFilter() { 
		return dolasimsablonFilter; 
	} 
	 
	public void setDolasimsablonFilter(DolasimsablonFilter dolasimsablonFilter) {  
		this.dolasimsablonFilter = dolasimsablonFilter;  
	}  
	 
	@Override  
	public BaseFilter getFilter() {  
		return getDolasimsablonFilter();  
	} 
	
	@Override  
	public void insert(){
		super.insert();
		if(!ManagedBeanLocator.locateSessionUser().isSuperUser()){
			getDolasimsablon().setBirimRef(getSessionUser().getPersonelRef().getBirimRef());
		}
	}
	
 	@Override  
	public void resetFilter(){
		super.resetFilterOnly();
		if(!ManagedBeanLocator.locateSessionUser().isSuperUser()){
			getDolasimsablon().setBirimRef(getSessionUser().getPersonelRef().getBirimRef());
		}
		queryAction(); 	 
	}
		
	public String dolasimsablondetay() {	
		if (!setSelected()) {
			return "";
		}	
		getSessionUser().setLegalAccess(1);	
		putObjectToSessionFilter(ApplicationDescriptor._DOLASIMSABLON_MODEL, getDolasimsablon());
		return ManagedBeanLocator.locateMenuController().gotoPage("menu_Dolasimsablondetay"); 
	} 
	
	@SuppressWarnings("unchecked")
	public List<Dolasimsablondetay> getDolasimsablondetayList(){
		if(getDolasimsablon() != null){
			return getDBOperator().load(Dolasimsablondetay.class.getSimpleName(), "o.dolasimsablonRef.rID=" + getDolasimsablon().getRID(), "o.sirano");
		} else {
			return null;
		}
	}
} // class 
