package tr.com.arf.toys.view.controller.form.kisi; 
  
 
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.toys.db.filter.kisi.KurulGorevFilter;
import tr.com.arf.toys.db.model.kisi.KurulGorev;
import tr.com.arf.toys.view.controller.form._base.ToysBaseFilteredController;
  
  
@ManagedBean 
@ViewScoped 
public class KurulGorevController extends ToysBaseFilteredController {  
  
  private static final long serialVersionUID = 6531152165884980748L; 
  
	private KurulGorev kurulGorev;  
	private KurulGorevFilter kurulGorevFilter = new KurulGorevFilter();  
  
	public KurulGorevController() { 
		super(KurulGorev.class);  
		setDefaultValues(false);  
		setOrderField("tanim");  
		setAutoCompleteSearchColumns(new String[]{"tanim"}); 
		setTable(null); 
		queryAction(); 
	} 
	
	public KurulGorev getKurulGorev() {
		kurulGorev = (KurulGorev) getEntity(); 
		return kurulGorev;
	}

	public void setKurulGorev(KurulGorev kurulGorev) {
		this.kurulGorev = kurulGorev;
	}

	public KurulGorevFilter getKurulGorevFilter() {
		return kurulGorevFilter;
	}

	public void setKurulGorevFilter(KurulGorevFilter kurulGorevFilter) {
		this.kurulGorevFilter = kurulGorevFilter;
	}


	@Override  
	public BaseFilter getFilter() {  
		return getKurulGorevFilter();  
	}  
	
//	public String kurulDonem() {	
//		if (!setSelected()) {
//			return "";
//		}	
//		getSessionUser().setLegalAccess(1);	
//		putObjectToSessionFilter(ApplicationDescriptor._DONEM_MODEL, , getDonem());
//		return ManagedBeanLocator.locateMenuController().gotoPage("menu_KurulDonem"); 
//	} 
	
 
} // class 
