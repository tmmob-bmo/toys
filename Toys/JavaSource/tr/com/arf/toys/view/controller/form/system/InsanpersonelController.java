package tr.com.arf.toys.view.controller.form.system;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.toys.db.filter.system.InsanpersonelFilter;
import tr.com.arf.toys.db.model.system.Insanpersonel;
import tr.com.arf.toys.view.controller.form._base.ToysBaseFilteredController;
  
  
@ManagedBean 
@ViewScoped 
public class InsanpersonelController extends ToysBaseFilteredController {  
  
   
  
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 225831826527483533L;
	private Insanpersonel insanpersonel;  
	private InsanpersonelFilter insanpersonelFilter = new InsanpersonelFilter();  
  
	public InsanpersonelController() { 
		super(Insanpersonel.class);  
		setDefaultValues(false);  
		setOrderField("sicilno");  
		setAutoCompleteSearchColumns(new String[]{"sicilno"}); 
		setTable(null); 
		queryAction(); 
	} 
	
	
	public Insanpersonel getInsanpersonel() { 
		insanpersonel = (Insanpersonel) getEntity(); 
		return insanpersonel; 
	} 
	
	public void setInsanpersonel(Insanpersonel insanpersonel) {
		this.insanpersonel = insanpersonel;
	}

	 
	public InsanpersonelFilter getInsanpersonelFilter() {
		return insanpersonelFilter;
	}


	public void setInsanpersonelFilter(InsanpersonelFilter insanpersonelFilter) {
		this.insanpersonelFilter = insanpersonelFilter;
	}


	

	@Override  
	public BaseFilter getFilter() {  
		return getInsanpersonelFilter();  
	}  
	
	
 
	
} // class 
