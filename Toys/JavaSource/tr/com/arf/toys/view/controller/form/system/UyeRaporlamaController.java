package tr.com.arf.toys.view.controller.form.system;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.component.html.HtmlSelectBooleanCheckbox;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.model.SelectItem;

import tr.com.arf.framework.db.enumerated._base.BaseEnumerated;
import tr.com.arf.framework.db.enumerated._common.EvetHayir;
import tr.com.arf.framework.db.enumerated._common.VarYokTumu;
import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.framework.db.model._base.BaseEntity;
import tr.com.arf.framework.utility.exception.data.DBException;
import tr.com.arf.framework.utility.tool.DateUtil;
import tr.com.arf.framework.utility.tool.KeyUtil;
import tr.com.arf.toys.db.enumerated.iletisim.AdresTuru;
import tr.com.arf.toys.db.enumerated.kisi.Cinsiyet;
import tr.com.arf.toys.db.enumerated.uye.OgrenimTip;
import tr.com.arf.toys.db.enumerated.uye.SigortaTip;
import tr.com.arf.toys.db.enumerated.uye.UyeAboneDurum;
import tr.com.arf.toys.db.enumerated.uye.UyeDurum;
import tr.com.arf.toys.db.enumerated.uye.UyeKayitDurum;
import tr.com.arf.toys.db.enumerated.uye.UyeMuafiyetTip;
import tr.com.arf.toys.db.enumerated.uye.UyeRaporlamaAboneDurum;
import tr.com.arf.toys.db.enumerated.uye.UyeTip;
import tr.com.arf.toys.db.filter.uye.UyeRaporlamaFilter;
import tr.com.arf.toys.db.model.kisi.Kurum;
import tr.com.arf.toys.db.model.system.Cezatip;
import tr.com.arf.toys.db.model.system.Kullanicirapor;
import tr.com.arf.toys.db.model.system.Odultip;
import tr.com.arf.toys.db.model.system.Uzmanliktip;
import tr.com.arf.toys.db.model.uye.Uye;
import tr.com.arf.toys.view.controller.form._base.ToysBaseFilteredController;

import com.lowagie.text.Document;
import com.lowagie.text.PageSize;

@ManagedBean
@ViewScoped
public class UyeRaporlamaController extends ToysBaseFilteredController {

	private static final long serialVersionUID = 6531152165884980748L;

	private Uye uye;
	private UyeRaporlamaFilter uyeRaporlamaFilter = new UyeRaporlamaFilter();

	public UyeRaporlamaController() {
		super(Uye.class);
		setDefaultValues(false);
		setOrderField("kisiRef");
		setAutoCompleteSearchColumns(new String[] { "kisiRef.ad", "kisiRef.soyad" });
		setTable(null);
		this.uyeRaporlamaFilter = new UyeRaporlamaFilter();
		setLoggable(false);
	}

	public Uye getUye() {
		uye = (Uye) getEntity();
		return uye;
	}

	public void setUye(Uye uye) {
		this.uye = uye;
	}

	public UyeRaporlamaFilter getUyeRaporlamaFilter() {
		return uyeRaporlamaFilter;
	}

	public void setUyeRaporlamaFilter(UyeRaporlamaFilter uyeRaporlamaFilter) {
		this.uyeRaporlamaFilter = uyeRaporlamaFilter;
	}

	public void collapseAllPanels() {
		this.uyePanelCollapsed = true;
		this.uyeDetayPanelBirCollapsed = true;
		this.uyeIletisimPanelCollapsed = true;
		this.kisiOgrenimPanelCollapsed = true;
		this.listeKriterleriPanelCollapsed = true;
	}

	private boolean uyePanelCollapsed = false;
	private boolean uyeDetayPanelBirCollapsed = true;
	private boolean uyeIletisimPanelCollapsed = true;
	private boolean kisiOgrenimPanelCollapsed = true;
	private boolean listeKriterleriPanelCollapsed = true;

	public boolean isUyePanelCollapsed() {
		return uyePanelCollapsed;
	}

	public void setUyePanelCollapsed(boolean uyePanelCollapsed) {
		this.uyePanelCollapsed = uyePanelCollapsed;
	}

	public boolean isUyeDetayPanelBirCollapsed() {
		return uyeDetayPanelBirCollapsed;
	}

	public void setUyeDetayPanelBirCollapsed(boolean uyeDetayPanelBirCollapsed) {
		this.uyeDetayPanelBirCollapsed = uyeDetayPanelBirCollapsed;
	}

	public boolean isUyeIletisimPanelCollapsed() {
		return uyeIletisimPanelCollapsed;
	}

	public void setUyeIletisimPanelCollapsed(boolean uyeIletisimPanelCollapsed) {
		this.uyeIletisimPanelCollapsed = uyeIletisimPanelCollapsed;
	}

	public boolean isKisiOgrenimPanelCollapsed() {
		return kisiOgrenimPanelCollapsed;
	}

	public void setKisiOgrenimPanelCollapsed(boolean kisiOgrenimPanelCollapsed) {
		this.kisiOgrenimPanelCollapsed = kisiOgrenimPanelCollapsed;
	}

	public void handleUyeRaporValueChanges(AjaxBehaviorEvent event) {
		try {
			HtmlSelectOneMenu menu = (HtmlSelectOneMenu) event.getComponent();
			if (menu.getValue() instanceof VarYokTumu) {
				// logYaz(menu.getClientId());
				if (menu.getClientId().equalsIgnoreCase("uyeUzmanlikVarYok")) {
					if (menu.getValue() != VarYokTumu._VAR) {
						getUyeRaporlamaFilter().setUyeUzmanlikList(new Uzmanliktip[0]);
					}
				} else if (menu.getClientId().equalsIgnoreCase("uyeCezaVarYok")) {
					if (menu.getValue() != VarYokTumu._VAR) {
						getUyeRaporlamaFilter().setUyeCezaList(new Cezatip[0]);
					}
				} else if (menu.getClientId().equalsIgnoreCase("uyeCezaVarYok")) {
					if (menu.getValue() != VarYokTumu._VAR) {
						getUyeRaporlamaFilter().setUyeCezaList(new Cezatip[0]);
					}
				} else if (menu.getClientId().equalsIgnoreCase("uyeMuafiyetVarYok")) {
					if (menu.getValue() != VarYokTumu._VAR) {
						getUyeRaporlamaFilter().setUyeMuafiyetList(new UyeMuafiyetTip[0]);
					}
				} else if (menu.getClientId().equalsIgnoreCase("uyeOdulVarYok")) {
					if (menu.getValue() != VarYokTumu._VAR) {
						getUyeRaporlamaFilter().setUyeOdulList(new Odultip[0]);
					}
				} else if (menu.getClientId().equalsIgnoreCase("kisiOgrenimTipVarYok")) {
					if (menu.getValue() != VarYokTumu._VAR) {
						getUyeRaporlamaFilter().setKisiOgrenimList(new OgrenimTip[0]);
					}
				} else if (menu.getClientId().equalsIgnoreCase("uyeSigortaVarYok")) {
					if (menu.getValue() != VarYokTumu._VAR) {
						getUyeRaporlamaFilter().setUyeSigortaList(new SigortaTip[0]);
					}
				}
			}
		} catch (Exception e) {
			logYaz("Error @handleUyeRaporValueChanges :" + e.getMessage());
			logYaz("Exception @" + getModelName() + "Controller :", e);
		}
	}

	public void handleUyeAidatBorcDurumValueChange(AjaxBehaviorEvent event) {
		try {
			HtmlSelectBooleanCheckbox menu = (HtmlSelectBooleanCheckbox) event.getComponent();
			if (menu.getClientId().equalsIgnoreCase("uyeAidatBorcSifirla")) {
				if ((boolean) menu.getValue() == true) {
					getUyeRaporlamaFilter().setUyeAidatBorcMax(new BigDecimal("0"));
					getUyeRaporlamaFilter().setUyeAidatBorcMin(new BigDecimal("0"));
				}
			}
		} catch (Exception e) {
			logYaz("Error @handleUyeAidatBorcDurumValueChange :" + e.getMessage());
			logYaz("Exception @" + getModelName() + "Controller :", e);
		}
	}

	@SuppressWarnings("unchecked")
	public List<Kurum> completeKurum(String value) {
		return getDBOperator().load(Kurum.class.getSimpleName(), "UPPER(o.ad) LIKE UPPER('%" + value + "%')", "o.rID");
	}

	public void addSecilenKurum() {
		if (getUyeRaporlamaFilter().getSecilenKurum() != null) {
			if (!getUyeRaporlamaFilter().getKurumListesi().contains(getUyeRaporlamaFilter().getSecilenKurum())) {
				getUyeRaporlamaFilter().getKurumListesi().add(getUyeRaporlamaFilter().getSecilenKurum());
			}
		}
	}

	public void clearKurumList() {
		getUyeRaporlamaFilter().setKurumListesi(new ArrayList<Kurum>());
	}

	@Override
	public BaseFilter getFilter() {
		return null;
	}

	private String createQueryStringForEnum(BaseEnumerated enumArray[], String fieldName) {
		if (!isEmpty(enumArray)) {
			if (enumArray.length == 1) {
				return " AND " + fieldName + " = " + enumArray[0].getCode();
			} else {
				String result = "AND " + fieldName + " IN  (";
				int listLength = enumArray.length;
				for (int a = 0; a < listLength; a++) {
					if (a == listLength - 1) {
						result += enumArray[a].getCode();
					} else {
						result += enumArray[a].getCode() + ",";
					}
				}
				return result + ")";
			}
		} else {
			return "";
		}
	}

	private String createQueryStringForEntity(BaseEntity entityArray[], String fieldName) {
		if (!isEmpty(entityArray)) {
			if (entityArray.length == 1) {
				return " AND " + fieldName + ".rID = " + entityArray[0].getRID();
			} else {
				String result = "AND " + fieldName + ".rID IN  (";
				int listLength = entityArray.length;
				for (int a = 0; a < listLength; a++) {
					if (a == listLength - 1) {
						result += entityArray[a].getRID();
					} else {
						result += entityArray[a].getRID() + ",";
					}
				}
				return result + ")";
			}
		} else {
			return "";
		}
	}

	public String createQueryStringForDateFields(Date beginDate, Date endDate, String fieldName) {
		if (beginDate != null || endDate != null) {
			if (beginDate != null && endDate == null) {
				return " AND to_char(o." + fieldName + ", 'yyyy-mm-dd') >= '" + DateUtil.dateToYMD(beginDate) + "'";
			} else if (beginDate == null && endDate != null) {
				return " AND to_char(o." + fieldName + ", 'yyyy-mm-dd') <= '" + DateUtil.dateToYMD(endDate) + "'";
			} else {
				return " AND to_char(o." + fieldName + ", 'yyyy-mm-dd') >= '" + DateUtil.dateToYMD(beginDate) + "' AND to_char(o." + fieldName + ", 'yyyy-mm-dd') <= '" + DateUtil.dateToYMD(endDate) + "'";
			}
		} else {
			return "";
		}
	}

	@Override
	@PostConstruct
	public void init() {
		super.init();
		getUyeRaporlamaFilter().setMezuniyettarihi(null);
		getUyeRaporlamaFilter().setEpostaYok(false);
		getUyeRaporlamaFilter().setTelefonYok(false);
		getUyeRaporlamaFilter().setOgrenimtip(OgrenimTip._NULL);
		// getUyeRaporlamaFilter().setMezuniyettarihi(Calendar.getInstance().get(Calendar.YEAR));
		getUyeRaporlamaFilter().setSicilnoyok(true);
		getUyeRaporlamaFilter().setAdyok(true);
		getUyeRaporlamaFilter().setSoyadyok(true);
		getUyeRaporlamaFilter().setBirimyok(true);
		getUyeRaporlamaFilter().setAidatborcuyok(true);
		getUyeRaporlamaFilter().setIsyeriyok(true);
		getUyeRaporlamaFilter().setYasadigiilyok(true);
		getUyeRaporlamaFilter().setCalistigiilyok(true);
		getUyeRaporlamaFilter().setMouniversiteyok(true);
		getUyeRaporlamaFilter().setMobolumyok(true);
		getUyeRaporlamaFilter().setMezuniyetyiliyok(true);
	}

	@SuppressWarnings("unchecked")
	public void filterUye() throws DBException {

		if (getCalistirilacakRaporRID() != null && getCalistirilacakRaporRID() != 0L) {
			logYaz("Calistirilacak rapor id = " + getCalistirilacakRaporRID());
			Kullanicirapor rapor = (Kullanicirapor) getDBOperator().find(Kullanicirapor.class.getSimpleName(), "rID", getCalistirilacakRaporRID() + "").get(0);
			setList(getDBOperator().load(Uye.class.getSimpleName(), rapor.getWherecondition(), "o.rID"));
			return;
		}

		getUyeRaporlamaFilter().setKullanicirapor(null);
		if (getUyeRaporlamaFilter().getMezuniyettarihi() != null && getUyeRaporlamaFilter().getMezuniyettarihi() == 0) {
			getUyeRaporlamaFilter().setMezuniyettarihi(null);
		}
		StringBuilder whereCon = new StringBuilder(" 1 = 1 ");

		whereCon.append(createQueryStringForDateFields(getUyeRaporlamaFilter().getUyeliktarih(), getUyeRaporlamaFilter().getUyeliktarihMax(), "uyeliktarih"));
		whereCon.append(createQueryStringForDateFields(getUyeRaporlamaFilter().getAyrilmatarih(), getUyeRaporlamaFilter().getAyrilmatarihMax(), "ayrilmatarih"));
		whereCon.append(createQueryStringForDateFields(getUyeRaporlamaFilter().getOnaytarih(), getUyeRaporlamaFilter().getOnaytarihMax(), "onaytarih"));

		if (!isEmpty(getUyeRaporlamaFilter().getKurumListesi())) {
			Kurum[] kurumArray = new Kurum[getUyeRaporlamaFilter().getKurumListesi().size()];
			kurumArray = getUyeRaporlamaFilter().getKurumListesi().toArray(kurumArray);
			whereCon.append(" AND (SELECT COUNT(krmkisi.rID) FROM KurumKisi AS krmkisi WHERE krmkisi.kisiRef.rID = o.kisiRef.rID " + createQueryStringForEntity(kurumArray, "krmkisi.kurumRef") + " ) > 0 ");
		}
		if (!isEmpty(getUyeRaporlamaFilter().getKurumadi())) {
			whereCon.append(" AND (SELECT COUNT(krmkisi.rID) FROM KurumKisi AS krmkisi WHERE krmkisi.kisiRef.rID = o.kisiRef.rID AND " + "(UPPER(krmkisi.kurumRef.ad) LIKE UPPER('%" + getUyeRaporlamaFilter().getKurumadi() + "%')))>0");
		}
		if (!isEmpty(getUyeRaporlamaFilter().getUniversiteList())) {
			String universiteRIDInCond = "(";
			for (int x = 0; x < getUyeRaporlamaFilter().getUniversiteList().length; x++) {
				if (x < getUyeRaporlamaFilter().getUniversiteList().length - 1) {
					universiteRIDInCond += (String) getUyeRaporlamaFilter().getUniversiteList()[x] + ",";
				} else {
					universiteRIDInCond += (String) getUyeRaporlamaFilter().getUniversiteList()[x];
				}
			}
			universiteRIDInCond += ")";
			whereCon.append(" AND (SELECT COUNT(uyeogrnm.rID) FROM Kisiogrenim AS uyeogrnm WHERE uyeogrnm.kisiRef.rID=o.kisiRef.rID AND uyeogrnm.universiteRef.rID IN " + universiteRIDInCond + ") > 0 ");
		}

		if (!isEmpty(getUyeRaporlamaFilter().getBolumList())) {
			String bolumRIDInCond = "(";
			for (int x = 0; x < getUyeRaporlamaFilter().getBolumList().length; x++) {
				if (x < getUyeRaporlamaFilter().getBolumList().length - 1) {
					bolumRIDInCond += (String) getUyeRaporlamaFilter().getBolumList()[x] + ",";
				} else {
					bolumRIDInCond += (String) getUyeRaporlamaFilter().getBolumList()[x];
				}
			}
			bolumRIDInCond += ")";
			whereCon.append(" AND (SELECT COUNT(uyeogrnm.rID) FROM Kisiogrenim AS uyeogrnm WHERE uyeogrnm.kisiRef.rID=o.kisiRef.rID AND uyeogrnm.bolumRef.rID IN " + bolumRIDInCond + ") > 0 ");
		}

		if (getUyeRaporlamaFilter().getUyeTip() != null && getUyeRaporlamaFilter().getUyeTip() != UyeTip._NULL) {
			whereCon.append(" AND o.uyetip=" + getUyeRaporlamaFilter().getUyeTip().getCode());
		}
		if (getUyeRaporlamaFilter().getUyeDurum() != null && getUyeRaporlamaFilter().getUyeDurum() != UyeDurum._NULL) {
			whereCon.append(" AND o.uyedurum=" + getUyeRaporlamaFilter().getUyeDurum().getCode());
		}
		if (getUyeRaporlamaFilter().getUyeKayitDurum() != null && getUyeRaporlamaFilter().getUyeKayitDurum() != UyeKayitDurum._NULL) {
			whereCon.append(" AND o.kayitdurum=" + getUyeRaporlamaFilter().getUyeKayitDurum().getCode());
		}
		if (getUyeRaporlamaFilter().getSecilenBirim() != null) {
			whereCon.append(" AND o.birimRef.rID=" + getUyeRaporlamaFilter().getSecilenBirim().getRID());
		}
		if (getUyeRaporlamaFilter().getCinsiyet() != null && getUyeRaporlamaFilter().getCinsiyet() != Cinsiyet._NULL) {
			whereCon.append(" AND (SELECT COUNT(kmlk.rID) FROM Kisikimlik AS kmlk WHERE kmlk.kisiRef.rID = o.kisiRef.rID AND kmlk.cinsiyet=" + getUyeRaporlamaFilter().getCinsiyet().getCode() + " ) > 0 ");
		}
		if (getUyeRaporlamaFilter().getYasadigiIl() != null) {
			whereCon.append(" AND (SELECT COUNT(evadres.rID) FROM Adres AS evadres WHERE evadres.kisiRef.rID = o.kisiRef.rID AND evadres.adresturu=" + AdresTuru._EVADRESI.getCode() + " AND evadres.sehirRef.rID="
					+ getUyeRaporlamaFilter().getYasadigiIl().getRID() + ") > 0 ");
		}
		if (getUyeRaporlamaFilter().getCalistigiIl() != null) {
			whereCon.append(" AND (SELECT COUNT(adrs.rID) FROM Adres AS adrs WHERE adrs.kisiRef.rID = o.kisiRef.rID AND adrs.adresturu=" + AdresTuru._ISADRESI.getCode() + " AND adrs.sehirRef.rID=" + getUyeRaporlamaFilter().getCalistigiIl().getRID()
					+ " ) > 0 ");
		}

		if (getUyeRaporlamaFilter().getDogumtarih() != null && getUyeRaporlamaFilter().getDogumtarihMax() != null) {
			whereCon.append(" AND (SELECT COUNT(kisikmlk.rID) FROM Kisikimlik AS kisikmlk WHERE kisikmlk.kisiRef.rID = o.kisiRef.rID  AND" + " to_char(kisikmlk.dogumtarih, 'yyyy-mm-dd') >= '"
					+ DateUtil.dateToYMD(getUyeRaporlamaFilter().getDogumtarih()) + "' AND to_char(kisikmlk.dogumtarih, 'yyyy-mm-dd') <= '" + DateUtil.dateToYMD(getUyeRaporlamaFilter().getDogumtarihMax()) + "') > 0 ");
		}

		if (getUyeRaporlamaFilter().getOgrenimtip() != null && getUyeRaporlamaFilter().getOgrenimtip() != OgrenimTip._NULL) {
			whereCon.append(" AND (SELECT COUNT(uyeogrnm.rID) FROM Kisiogrenim AS uyeogrnm WHERE uyeogrnm.kisiRef.rID=o.kisiRef.rID AND uyeogrnm.ogrenimtip=" + getUyeRaporlamaFilter().getOgrenimtip().getCode() + " ) > 0 ");
		}

		if (getUyeRaporlamaFilter().getMezuniyettarihi() != null && getUyeRaporlamaFilter().getMezuniyettarihi() != 0) {
			whereCon.append(" AND (SELECT COUNT(uyeogrnm.rID) FROM Kisiogrenim AS uyeogrnm WHERE uyeogrnm.kisiRef.rID=o.kisiRef.rID AND uyeogrnm.mezuniyettarihi=" + getUyeRaporlamaFilter().getMezuniyettarihi() + " ) > 0 ");
		}

		if (getUyeRaporlamaFilter().getTelefonYok()) {
			whereCon.append(" AND (SELECT COUNT(tlfn.rID) FROM Telefon AS tlfn WHERE tlfn.kisiRef.rID=o.kisiRef.rID AND tlfn.varsayilan=" + EvetHayir._EVET.getCode() + ") = 0");
		}

		if (getUyeRaporlamaFilter().getEpostaYok()) {
			whereCon.append(" AND (SELECT COUNT(eposta.rID) FROM Internetadres AS eposta WHERE eposta.kisiRef.rID=o.kisiRef.rID AND eposta.varsayilan=" + EvetHayir._EVET.getCode() + ") = 0");
		}

		if (getUyeRaporlamaFilter().getOgrenciDurum() != null && getUyeRaporlamaFilter().getOgrenciDurum() != EvetHayir._NULL) {
			if (getUyeRaporlamaFilter().getOgrenciDurum() == EvetHayir._EVET) {
				whereCon.append(" AND LENGTH(o.ogrenciNo) > 0 ");
			} else if (getUyeRaporlamaFilter().getOgrenciDurum() == EvetHayir._HAYIR) {
				whereCon.append(" AND LENGTH(o.ogrenciNo) = 0 ");
			}
		}

		if (getUyeRaporlamaFilter().getAbonelikDurum() != null && getUyeRaporlamaFilter().getAbonelikDurum() != UyeRaporlamaAboneDurum._NULL) {
			if (getUyeRaporlamaFilter().getAbonelikDurum() == UyeRaporlamaAboneDurum._ABONELIKLERVAR) {
				whereCon.append(" AND (SELECT COUNT(m.rID) FROM Uyeabone AS m WHERE m.uyeRef.rID = o.rID) > 0 ");
			} else if (getUyeRaporlamaFilter().getAbonelikDurum() == UyeRaporlamaAboneDurum._ABONELIGIYOK) {
				whereCon.append(" AND (SELECT COUNT(m.rID) FROM Uyeabone AS m WHERE m.uyeRef.rID = o.rID) = 0 ");
			} else if (getUyeRaporlamaFilter().getAbonelikDurum() == UyeRaporlamaAboneDurum._IPTALEDILMISABONELIGIVAR) {
				whereCon.append(" AND (SELECT COUNT(m.rID) FROM Uyeabone AS m WHERE m.uyeRef.rID = o.rID AND m.uyeabonedurum = " + UyeAboneDurum._IPTALEDILMIS.getCode() + ") > 0 ");
			}
		}

		if (!getUyeRaporlamaFilter().getUyeAidatBorcYok()) {
			if (!checkBigDecimal(getUyeRaporlamaFilter().getUyeAidatBorcMin()) || !checkBigDecimal(getUyeRaporlamaFilter().getUyeAidatBorcMax())) {
				if (!checkBigDecimal(getUyeRaporlamaFilter().getUyeAidatBorcMin()) && checkBigDecimal(getUyeRaporlamaFilter().getUyeAidatBorcMax())) {
					whereCon.append(" AND (SELECT COALESCE(SUM(n.miktar)-SUM(n.odenen),0) FROM Uyeaidat AS n WHERE n.uyemuafiyetRef IS NULL AND n.uyeRef.rID = o.rID) >= " + getUyeRaporlamaFilter().getUyeAidatBorcMin());
				} else if (checkBigDecimal(getUyeRaporlamaFilter().getUyeAidatBorcMin()) && !checkBigDecimal(getUyeRaporlamaFilter().getUyeAidatBorcMax())) {
					whereCon.append(" AND (SELECT COALESCE(SUM(n.miktar)-SUM(n.odenen),0) FROM Uyeaidat AS n WHERE n.uyemuafiyetRef IS NULL AND n.uyeRef.rID = o.rID) <= " + getUyeRaporlamaFilter().getUyeAidatBorcMax());
				} else if (!checkBigDecimal(getUyeRaporlamaFilter().getUyeAidatBorcMin()) && !checkBigDecimal(getUyeRaporlamaFilter().getUyeAidatBorcMax())) {
					whereCon.append(" AND (SELECT COALESCE(SUM(n.miktar)-SUM(n.odenen),0) FROM Uyeaidat AS n WHERE n.uyemuafiyetRef IS NULL AND n.uyeRef.rID = o.rID) " + "BETWEEN " + getUyeRaporlamaFilter().getUyeAidatBorcMin() + " AND "
							+ getUyeRaporlamaFilter().getUyeAidatBorcMax());
				}
			}
		} else {
			whereCon.append(" AND (SELECT COALESCE(SUM(n.miktar)-SUM(n.odenen),0) FROM Uyeaidat AS n WHERE n.uyemuafiyetRef IS NULL AND n.uyeRef.rID = o.rID) = 0");
		}

		if (getUyeRaporlamaFilter().getUyeUzmanlikVarYok() != null && getUyeRaporlamaFilter().getUyeUzmanlikVarYok() != VarYokTumu._NULL && getUyeRaporlamaFilter().getUyeUzmanlikVarYok() != VarYokTumu._TUMU) {
			if (getUyeRaporlamaFilter().getUyeUzmanlikVarYok() == VarYokTumu._YOK) {
				whereCon.append(" AND (SELECT COUNT(p.rID) FROM Uyeuzmanlik AS p WHERE p.uyeRef.rID = o.rID) = 0 ");
			} else if (getUyeRaporlamaFilter().getUyeUzmanlikVarYok() == VarYokTumu._VAR) {
				if (isEmpty(getUyeRaporlamaFilter().getUyeUzmanlikList())) {
					whereCon.append(" AND (SELECT COUNT(p.rID) FROM Uyeuzmanlik AS p WHERE p.uyeRef.rID = o.rID) > 0 ");
				} else {
					whereCon.append(" AND (SELECT COUNT(p.rID) FROM Uyeuzmanlik AS p WHERE p.uyeRef.rID = o.rID " + createQueryStringForEntity(getUyeRaporlamaFilter().getUyeUzmanlikList(), "p.uzmanliktipRef") + " ) > 0 ");
					;
				}
			}
		}

		if (getUyeRaporlamaFilter().getUyeCezaVarYok() != null && getUyeRaporlamaFilter().getUyeCezaVarYok() != VarYokTumu._NULL && getUyeRaporlamaFilter().getUyeCezaVarYok() != VarYokTumu._TUMU) {
			if (getUyeRaporlamaFilter().getUyeCezaVarYok() == VarYokTumu._YOK) {
				whereCon.append(" AND (SELECT COUNT(c.rID) FROM Uyeceza AS c WHERE c.uyeRef.rID = o.rID) = 0 ");
			} else if (getUyeRaporlamaFilter().getUyeCezaVarYok() == VarYokTumu._VAR) {
				if (isEmpty(getUyeRaporlamaFilter().getUyeCezaList())) {
					whereCon.append(" AND (SELECT COUNT(c.rID) FROM Uyeceza AS c WHERE c.uyeRef.rID = o.rID) > 0 ");
				} else {
					whereCon.append(" AND (SELECT COUNT(c.rID) FROM Uyeceza AS c WHERE c.uyeRef.rID = o.rID " + createQueryStringForEntity(getUyeRaporlamaFilter().getUyeCezaList(), "c.cezatipRef") + " ) > 0 ");
				}
			}
		}

		if (getUyeRaporlamaFilter().getUyeMuafiyetVarYok() != null && getUyeRaporlamaFilter().getUyeMuafiyetVarYok() != VarYokTumu._NULL && getUyeRaporlamaFilter().getUyeMuafiyetVarYok() != VarYokTumu._TUMU) {
			if (getUyeRaporlamaFilter().getUyeMuafiyetVarYok() == VarYokTumu._YOK) {
				whereCon.append(" AND (SELECT COUNT(r.rID) FROM Uyemuafiyet AS r WHERE r.uyeRef.rID = o.rID) = 0 ");
			} else if (getUyeRaporlamaFilter().getUyeMuafiyetVarYok() == VarYokTumu._VAR) {
				if (isEmpty(getUyeRaporlamaFilter().getUyeMuafiyetList())) {
					whereCon.append(" AND (SELECT COUNT(r.rID) FROM Uyemuafiyet AS r WHERE r.uyeRef.rID = o.rID) > 0 ");
				} else {
					whereCon.append(" AND (SELECT COUNT(r.rID) FROM Uyemuafiyet AS r WHERE r.uyeRef.rID = o.rID " + createQueryStringForEnum(getUyeRaporlamaFilter().getUyeMuafiyetList(), "r.muafiyettip") + " ) > 0 ");
				}
			}
		}

		if (getUyeRaporlamaFilter().getUyeOdulVarYok() != null && getUyeRaporlamaFilter().getUyeOdulVarYok() != VarYokTumu._NULL && getUyeRaporlamaFilter().getUyeOdulVarYok() != VarYokTumu._TUMU) {
			if (getUyeRaporlamaFilter().getUyeOdulVarYok() == VarYokTumu._YOK) {
				whereCon.append(" AND (SELECT COUNT(od.rID) FROM Uyeodul AS od WHERE od.uyeRef.rID = o.rID) = 0 ");
			} else if (getUyeRaporlamaFilter().getUyeOdulVarYok() == VarYokTumu._VAR) {
				if (isEmpty(getUyeRaporlamaFilter().getUyeOdulList())) {
					whereCon.append(" AND (SELECT COUNT(od.rID) FROM Uyeodul AS od WHERE od.uyeRef.rID = o.rID) > 0 ");
				} else {
					whereCon.append(" AND (SELECT COUNT(od.rID) FROM Uyeodul AS od WHERE od.uyeRef.rID = o.rID " + createQueryStringForEntity(getUyeRaporlamaFilter().getUyeOdulList(), "od.odultipRef") + " ) > 0 ");
				}
			}
		}

		if (getUyeRaporlamaFilter().getKisiOgrenimVarYok() != null && getUyeRaporlamaFilter().getKisiOgrenimVarYok() != VarYokTumu._NULL && getUyeRaporlamaFilter().getKisiOgrenimVarYok() != VarYokTumu._TUMU) {
			if (getUyeRaporlamaFilter().getKisiOgrenimVarYok() == VarYokTumu._YOK) {
				whereCon.append(" AND (SELECT COUNT(ogr.rID) FROM Kisiogrenim AS ogr WHERE ogr.kisiRef.rID = o.kisiRef.rID) = 0 ");
			} else if (getUyeRaporlamaFilter().getKisiOgrenimVarYok() == VarYokTumu._VAR) {
				if (isEmpty(getUyeRaporlamaFilter().getKisiOgrenimList())) {
					whereCon.append(" AND (SELECT COUNT(ogr.rID) FROM Kisiogrenim AS ogr WHERE ogr.kisiRef.rID = o.kisiRef.rID) > 0 ");
				} else {
					whereCon.append(" AND (SELECT COUNT(ogr.rID) FROM Kisiogrenim AS ogr WHERE ogr.kisiRef.rID = o.kisiRef.rID " + createQueryStringForEnum(getUyeRaporlamaFilter().getKisiOgrenimList(), "ogr.ogrenimtip") + " ) > 0 ");
				}
			}
		}

		if (getUyeRaporlamaFilter().getUyeSigortaVarYok() != null && getUyeRaporlamaFilter().getUyeSigortaVarYok() != VarYokTumu._NULL && getUyeRaporlamaFilter().getUyeSigortaVarYok() != VarYokTumu._TUMU) {
			if (getUyeRaporlamaFilter().getUyeSigortaVarYok() == VarYokTumu._YOK) {
				whereCon.append(" AND (SELECT COUNT(sig.rID) FROM Uyesigorta AS sig WHERE sig.uyeRef.rID = o.rID) = 0 ");
			} else if (getUyeRaporlamaFilter().getUyeSigortaVarYok() == VarYokTumu._VAR) {
				if (isEmpty(getUyeRaporlamaFilter().getUyeSigortaList())) {
					whereCon.append(" AND (SELECT COUNT(sig.rID) FROM Uyesigorta AS sig WHERE sig.uyeRef.rID = o.rID) > 0 ");
				} else {
					whereCon.append(" AND (SELECT COUNT(sig.rID) FROM Uyesigorta AS sig WHERE sig.uyeRef.rID = o.rID " + createQueryStringForEnum(getUyeRaporlamaFilter().getUyeSigortaList(), "sig.sigortatip") + " ) > 0 ");
				}
			}
		}

		// logYaz("SORGU KRITERI : " + whereCon.toString());
		int count = getDBOperator().recordCount(Uye.class.getSimpleName(), whereCon.toString());
		// logYaz("BULUNAN UYE SAYISI :" + count);
		if (count > 1500) {
			createGenericMessage("DIKKAT! Cok fazla kayit (" + count + ") bulundu! Lutfen daha fazla kriter giriniz!", FacesMessage.SEVERITY_ERROR);
			return;
		}
		setList(getDBOperator().load(Uye.class.getSimpleName(), whereCon.toString(), "o.rID"));

		if (isEmpty(getList())) {
			createGenericMessage(KeyUtil.getMessageValue("kayit.bulunamadi"), FacesMessage.SEVERITY_WARN);
		} else {
			setWhereCondition(whereCon.toString());
		}
	}

	/* KULLANICI RAPOR KAYDETME */
	private String whereCondition;
	private String kullaniciRaporAdi;

	public String getKullaniciRaporAdi() {
		return kullaniciRaporAdi;
	}

	public void setKullaniciRaporAdi(String kullaniciRaporAdi) {
		this.kullaniciRaporAdi = kullaniciRaporAdi;
	}

	public String getWhereCondition() {
		return whereCondition;
	}

	public void setWhereCondition(String whereCondition) {
		this.whereCondition = whereCondition;
	}

	public void saveKullaniciRapor() throws DBException {
		if (isEmpty(getList())) {
			createGenericMessage(KeyUtil.getMessageValue("kayit.bulunamadi"), FacesMessage.SEVERITY_WARN);
		} else if (isEmpty(getKullaniciRaporAdi())) {
			createGenericMessage(KeyUtil.getMessageValue("kullaniciRaporAdi.giriniz"), FacesMessage.SEVERITY_ERROR);
		} else {
			Kullanicirapor rapor = new Kullanicirapor();
			rapor.setKullaniciRef(getSessionUser().getKullanici());
			rapor.setRaporadi(getKullaniciRaporAdi());
			rapor.setTarih(new Date());
			rapor.setRaporTuru("Üye");
			rapor.setWherecondition(getWhereCondition());
			if (getDBOperator().recordCount(Kullanicirapor.class.getSimpleName(), "o.kullaniciRef.rID=" + rapor.getKullaniciRef().getRID() + " AND o.raporadi='" + rapor.getRaporadi() + "'") > 0) {
				createGenericMessage(KeyUtil.getMessageValue("kullaniciRaporAdi.mukerrer"), FacesMessage.SEVERITY_WARN);
				return;
			}
			getDBOperator().insert(rapor);
			createGenericMessage(KeyUtil.getMessageValue("kayit.eklendi"), FacesMessage.SEVERITY_INFO);
			setKullaniciRaporAdi("");
		}
	}

	/*********************************************************************/

	/* SECILEN RAPORUN CALISTIRILMASI */
	private Long calistirilacakRaporRID;

	public Long getCalistirilacakRaporRID() {
		return calistirilacakRaporRID;
	}

	public void setCalistirilacakRaporRID(Long calistirilacakRaporRID) {
		this.calistirilacakRaporRID = calistirilacakRaporRID;
	}

	// public SelectItem[] getKullaniciRaporList1(){
	// @SuppressWarnings("unchecked")
	// List<Kullanicirapor> entityList = getDBOperator().load(Kullanicirapor.class.getSimpleName(), " o.kullaniciRef = " +
	// getSessionUser().getKullanici().getRID() + " AND o.raporTuru = 'Üye'", "o.raporadi");
	// if (entityList != null) {
	// SelectItem[] selectItemList = new SelectItem[entityList.size() + 1];
	// selectItemList[0] = new SelectItem(null, "");
	// int i = 1;
	// for (Kullanicirapor entity : entityList) {
	// selectItemList[i++] = new SelectItem(entity.getRID(), entity.getUIString());
	// }
	// return selectItemList;
	// }
	// return new SelectItem[0];
	// }

	private List<Kullanicirapor> kullaniciRaporListesi;

	public List<Kullanicirapor> getKullaniciRaporListesi() {
		return kullaniciRaporListesi;
	}

	public void setKullaniciRaporListesi(List<Kullanicirapor> kullaniciRaporListesi) {
		this.kullaniciRaporListesi = kullaniciRaporListesi;
	}

	public SelectItem[] kullaniciRaporList;

	public void setKullaniciRaporList(SelectItem[] kullaniciRaporList) {
		this.kullaniciRaporList = kullaniciRaporList;
	}

	@SuppressWarnings("unchecked")
	public SelectItem[] getKullaniciRaporList() {
		List<Kullanicirapor> entityList = null;
		if (kullaniciRaporList != null && kullaniciRaporList.length > 0) {
			return kullaniciRaporList;
		}
		if (getKullaniciRaporListesi() != null) {
			entityList = getKullaniciRaporListesi();
		} else {
			entityList = getDBOperator().load(Kullanicirapor.class.getSimpleName(), " o.kullaniciRef = " + getSessionUser().getKullanici().getRID() + " AND o.raporTuru = 'Üye'", "o.raporadi");
		}
		if (entityList != null) {
			int i = 0;
			SelectItem[] raporList = null;
			raporList = new SelectItem[entityList.size() + 1];
			raporList[0] = new SelectItem(null, "");
			i = 0;
			for (BaseEntity entity : entityList) {
				i++;
				raporList[i] = new SelectItem(entity, entity.getUIString());
			}
			setKullaniciRaporList(raporList);
			return raporList;
		}
		return new SelectItem[0];
	}

	@SuppressWarnings("unchecked")
	public void executeKullaniciRapor() {
		if (getUyeRaporlamaFilter().getKullanicirapor() != null) {
			logYaz("Calistirilacak rapor adi = " + getUyeRaporlamaFilter().getKullanicirapor().getRaporadi());
			Kullanicirapor rapor = (Kullanicirapor) getDBOperator().find(Kullanicirapor.class.getSimpleName(), "rID", getUyeRaporlamaFilter().getKullanicirapor().getRID() + "").get(0);
			setList(getDBOperator().load(Uye.class.getSimpleName(), rapor.getWherecondition(), "o.rID"));
		}
		if (isEmpty(getList())) {
			createGenericMessage(KeyUtil.getMessageValue("kayit.bulunamadi"), FacesMessage.SEVERITY_WARN);
		}
		getUyeRaporlamaFilter().setKullanicirapor(null);
	}

	/*********************************************************************/

	private boolean checkBigDecimal(BigDecimal val) {
		return val == null || val.longValue() == 0;
	}

	private SelectItem[] kullaniciRaporDegistir;

	public SelectItem[] getKullaniciRaporDegistir() {
		return kullaniciRaporDegistir;
	}

	public void setKullaniciRaporDegistir(SelectItem[] kullaniciRaporDegistir) {
		this.kullaniciRaporDegistir = kullaniciRaporDegistir;
	}

	public void handleChangeKullaniciRapor(AjaxBehaviorEvent event) {
		try {
			HtmlSelectOneMenu menu = (HtmlSelectOneMenu) event.getComponent();
			if (menu.getValue() instanceof Kullanicirapor) {
				setCalistirilacakRaporRID(((Kullanicirapor) menu.getValue()).getRID());
				logYaz("Kullanici rapor rid = " + getCalistirilacakRaporRID());
			} else {
				setCalistirilacakRaporRID(0L);
				logYaz("Kullanici rapor rid = " + getCalistirilacakRaporRID());
			}

		} catch (Exception e) {
			logYaz("Error UyeRaporlamaController @handleChangeKullaniciRapor :" + e.getMessage());
			logYaz("Exception @" + getModelName() + "Controller :", e);
		}
	}

	public boolean isListeKriterleriPanelCollapsed() {
		return listeKriterleriPanelCollapsed;
	}

	public void setListeKriterleriPanelCollapsed(boolean listeKriterleriPanelCollapsed) {
		this.listeKriterleriPanelCollapsed = listeKriterleriPanelCollapsed;
	}

	// pdf ciktisini landscape yapar
	public void preProcessPDF(Object document) {
		Document doc = (Document) document;
		doc.setPageSize(PageSize.A3.rotate());
	}

} // class
