package tr.com.arf.toys.view.controller.form.egitim; 
  
 
import java.io.File;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.framework.db.model._base.BaseEntity;
import tr.com.arf.toys.db.filter.egitim.EgitimdosyaFilter;
import tr.com.arf.toys.db.model.egitim.Egitimdosya;
import tr.com.arf.toys.db.model.egitim.Egitimtanim;
import tr.com.arf.toys.service.application.ApplicationDescriptor;
import tr.com.arf.toys.view.controller.form._base.ToysBaseFilteredController;
  
  
@ManagedBean 
@ViewScoped 
public class EgitimdosyaController extends ToysBaseFilteredController {  
  
  private static final long serialVersionUID = 6531152165884980748L; 
  
	private Egitimdosya egitimdosya;  
	private EgitimdosyaFilter egitimdosyaFilter = new EgitimdosyaFilter();  
  
	private String filename;
	private Boolean egitimTanim=false;
	
	public EgitimdosyaController() { 
		super(Egitimdosya.class);  
		setDefaultValues(false);  
		setOrderField("ad");  
		setLoggable(false);  
		setAutoCompleteSearchColumns(new String[]{"ad"}); 
		setTable(null); 
		if(getMasterEntity() != null){ 
			if(getMasterEntity().getClass() == Egitimtanim.class){
				setMasterObjectName(ApplicationDescriptor._EGITIMTANIM_MODEL);
				setMasterOutcome(ApplicationDescriptor._EGITIMTANIM_MODEL);	
				getEgitimdosyaFilter().setEgitimtanimRef((Egitimtanim) getMasterEntity()); 
				setEgitimTanim(true);
			}  
			getEgitimdosyaFilter().createQueryCriterias();   
		} 
		queryAction(); 
	} 
	
	public EgitimdosyaController(Object object) { 
		super(Egitimdosya.class);  
		setLoggable(true); 
	}
 
	public BaseEntity getMasterEntity(){
		if(getObjectFromSessionFilter(ApplicationDescriptor._EGITIMTANIM_MODEL) != null){
			return (Egitimtanim) getObjectFromSessionFilter(ApplicationDescriptor._EGITIMTANIM_MODEL);
		}else {
			return null;
		}			
	} 
	
	
	public Egitimdosya getEgitimdosya() { 
		egitimdosya = (Egitimdosya) getEntity(); 
		return egitimdosya; 
	} 
	
	public void setEgitimdosya(Egitimdosya egitimdosya) { 
		this.egitimdosya = egitimdosya; 
	} 
	
	public EgitimdosyaFilter getEgitimdosyaFilter() { 
		return egitimdosyaFilter; 
	} 
	 
	public void setEgitimdosyaFilter(EgitimdosyaFilter egitimdosyaFilter) {  
		this.egitimdosyaFilter = egitimdosyaFilter;  
	}  
	 
	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

	public Boolean getEgitimTanim() {
		return egitimTanim;
	}

	public void setEgitimTanim(Boolean egitimTanim) {
		this.egitimTanim = egitimTanim;
	}

	@Override  
	public BaseFilter getFilter() {  
		return getEgitimdosyaFilter();  
	}  
	
	
 
	@Override 
	public void resetFilter() {
		super.resetFilterOnly();
		if(getMasterEntity() != null){
			getEgitimdosyaFilter().setEgitimtanimRef((Egitimtanim)getMasterEntity()); 
		}
		queryAction();
	}
 
	@Override	
	public void insert() {	
		super.insert();	
		getEgitimdosya().setEgitimtanimRef(getEgitimdosyaFilter().getEgitimtanimRef());	
	}	
	
	public void fileUploadListenerForEgitimDosya(FileUploadEvent event) throws Exception { 
//		UploadedFile item = event.getFile(); 
//		File creationFile = createFile(item);  
//		File creationFile = super.createFile(event.getFile(),Egitimdosya.class.getSimpleName());
		UploadedFile item = event.getFile();
		File creationFile = createFile(item);
		getEgitimdosya().setPath(creationFile.getAbsolutePath()); 
	} 
	
	@Override
	public void fileUploadListener(FileUploadEvent event) throws Exception {
		UploadedFile item = event.getFile(); 
        File creationFile = createFile(item); 
        getEntity().setPath(creationFile.getAbsolutePath()); 
        String path1 = creationFile.getAbsolutePath();
        setFilename(path1.substring(41));
	}
	
} // class 
