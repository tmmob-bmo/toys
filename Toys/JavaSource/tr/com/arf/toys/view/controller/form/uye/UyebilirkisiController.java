package tr.com.arf.toys.view.controller.form.uye; 
  
 
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.model.SelectItem;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.framework.db.model._base.BaseEntity;
import tr.com.arf.toys.db.filter.uye.UyebilirkisiFilter;
import tr.com.arf.toys.db.model.uye.Uye;
import tr.com.arf.toys.db.model.uye.Uyebilirkisi;
import tr.com.arf.toys.db.model.uye.Uyeuzmanlik;
import tr.com.arf.toys.service.application.ApplicationDescriptor;
import tr.com.arf.toys.view.controller.form._base.ToysBaseFilteredController;
  
  
@ManagedBean 
@ViewScoped 
public class UyebilirkisiController extends ToysBaseFilteredController {  
  
  private static final long serialVersionUID = 6531152165884980748L; 
  
	private Uyebilirkisi uyebilirkisi;  
	private UyebilirkisiFilter uyebilirkisiFilter = new UyebilirkisiFilter();  
  
	public UyebilirkisiController() { 
		super(Uyebilirkisi.class);  
		setDefaultValues(false);  
		setOrderField("aciklama");  
		setAutoCompleteSearchColumns(new String[]{"aciklama"}); 
		setTable(null); 
		if(getMasterEntity() != null){ 
			setMasterObjectName(ApplicationDescriptor._UYE_MODEL);
			setMasterOutcome(ApplicationDescriptor._UYE_MODEL);
			getUyebilirkisiFilter().setUyeRef(getMasterEntity());
			getUyebilirkisiFilter().createQueryCriterias();   
		} 
		queryAction(); 
	} 
	
	public UyebilirkisiController(Object object) { 
		super(Uyebilirkisi.class);   
	}  
 
	public Uye getMasterEntity(){
		if(getUyebilirkisiFilter().getUyeRef() != null){
			return getUyebilirkisiFilter().getUyeRef();
		} else {
			return (Uye) getObjectFromSessionFilter(ApplicationDescriptor._UYE_MODEL);
		}			
	}

	public Uyebilirkisi getUyebilirkisi() {
		uyebilirkisi = (Uyebilirkisi) getEntity();
		return uyebilirkisi;
	}

	public void setUyebilirkisi(Uyebilirkisi uyebilirkisi) {
		this.uyebilirkisi = uyebilirkisi;
	}

	public UyebilirkisiFilter getUyebilirkisiFilter() {
		return uyebilirkisiFilter;
	}

	public void setUyebilirkisiFilter(UyebilirkisiFilter uyebilirkisiFilter) {
		this.uyebilirkisiFilter = uyebilirkisiFilter;
	}

	@Override
	public BaseFilter getFilter() {
		return getUyebilirkisiFilter();
	}

	@Override
	public void insert() {
		super.insert();
		getUyebilirkisi().setUyeRef(getUyebilirkisiFilter().getUyeRef());
	}
	
	public SelectItem[] getSelectItemListForUyeuzmanlik(){
		@SuppressWarnings("unchecked")
		List<Uyeuzmanlik> entityList = getDBOperator().load(Uyeuzmanlik.class.getSimpleName(), "o.uyeRef.rID=" + getUyebilirkisiFilter().getUyeRef().getRID(), "o.rID");
		if (entityList != null) {
			SelectItem[] selectItemList = new SelectItem[entityList.size() + 1];
			selectItemList[0] = new SelectItem(null, "");
			int i = 1;
			for (BaseEntity entity : entityList) {
				selectItemList[i++] = new SelectItem(entity,
						entity.getUIString());
			}
			return selectItemList;
		}
		return new SelectItem[0]; 
	}
	
} // class 
