package tr.com.arf.toys.view.controller.form.system; 
  
 
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.toys.db.filter.system.DemirbasFilter;
import tr.com.arf.toys.db.model.system.Demirbas;
import tr.com.arf.toys.view.controller.form._base.ToysBaseFilteredController;
  
  
@ManagedBean 
@ViewScoped 
public class DemirbasController extends ToysBaseFilteredController {  
  
  private static final long serialVersionUID = 6531152165884980748L; 
  
	private Demirbas demirbas;  
	private DemirbasFilter demirbasFilter = new DemirbasFilter();  
	
	public DemirbasController() { 
		super(Demirbas.class);  
		setDefaultValues(false);  
		setOrderField("cinsi");  
		setAutoCompleteSearchColumns(new String[]{"cinsi"}); 
		setTable(null); 
		queryAction(); 
	} 


	public Demirbas getDemirbas() {
		demirbas=(Demirbas) getEntity();
		return demirbas;
	}

	public void setDemirbas(Demirbas demirbas) {
		this.demirbas = demirbas;
	}

	public DemirbasFilter getDemirbasFilter() {
		return demirbasFilter;
	}

	public void setDemirbasFilter(DemirbasFilter demirbasFilter) {
		this.demirbasFilter = demirbasFilter;
	}

	@Override  
	public BaseFilter getFilter() {  
		return getDemirbasFilter();  
	}  
	
	@Override	
	public void insert() {	
		super.insert();	
	}	
	
} // class 
