package tr.com.arf.toys.view.controller.form.system; 
  
 
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.toys.db.filter.system.KpslogFilter;
import tr.com.arf.toys.db.model.system.Kpslog;
import tr.com.arf.toys.view.controller.form._base.ToysBaseFilteredController;
  
  
@ManagedBean 
@ViewScoped 
public class KpslogController extends ToysBaseFilteredController {  
  
  private static final long serialVersionUID = 6531152165884980748L; 
  
	private Kpslog kpslog;  
	private KpslogFilter kpslogFilter = new KpslogFilter();  
  
	public KpslogController() { 
		super(Kpslog.class);  
		setDefaultValues(false);  
		setOrderField("tarih DESC");  
		setLoggable(false);  
		setAutoCompleteSearchColumns(new String[]{"kullaniciRef"}); 
		setTable(null); 
		queryAction(); 
	} 
	
	public Kpslog getKpslog() {
		kpslog=(Kpslog) getEntity();
		return kpslog;
	}

	public void setKpslog(Kpslog kpslog) {
		this.kpslog = kpslog;
	}
		 
	public KpslogFilter getKpslogFilter() {
		return kpslogFilter;
	}

	public void setKpslogFilter(KpslogFilter kpslogFilter) {
		this.kpslogFilter = kpslogFilter;
	}

	@Override  
	public BaseFilter getFilter() {  
		return getKpslogFilter();
	}  
	
	
 
	
} // class 