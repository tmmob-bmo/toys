package tr.com.arf.toys.view.controller.form.egitim;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.primefaces.event.SelectEvent;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.framework.db.model._base.BaseEntity;
import tr.com.arf.toys.db.filter.egitim.EgitimkatilimciFilter;
import tr.com.arf.toys.db.model.egitim.Egitim;
import tr.com.arf.toys.db.model.egitim.Egitimkatilimci;
import tr.com.arf.toys.service.application.ApplicationDescriptor;
import tr.com.arf.toys.service.application.ManagedBeanLocator;
import tr.com.arf.toys.view.controller.form._base.ToysBaseFilteredController;

@ManagedBean
@ViewScoped
public class EgitimkatilimcilistesiController extends ToysBaseFilteredController {

	private static final long serialVersionUID = 6531152165884980748L;

	private Egitimkatilimci egitimkatilimci;
	private EgitimkatilimciFilter egitimkatilimciFilter = new EgitimkatilimciFilter();
//	private Egitim egitim = new Egitim();
	

	public EgitimkatilimcilistesiController() {
		super(Egitimkatilimci.class);
		setDefaultValues(false);
		setOrderField("kisiRef.rID");
		setLoggable(false);
		setAutoCompleteSearchColumns(new String[] { "kisiRef" });
		setTable(null);
		if (getMasterEntity() != null) {
			setMasterObjectName(ApplicationDescriptor._EGITIM_MODEL);
			setMasterOutcome(ApplicationDescriptor._EGITIM_MODEL);
			getEgitimkatilimciFilter().setEgitimRef(getMasterEntity());
			getEgitimkatilimciFilter().createQueryCriterias();
		}
		queryAction();
	}

	public Egitim getMasterEntity() {
		if (getEgitimkatilimciFilter().getEgitimRef() != null) {
			return getEgitimkatilimciFilter().getEgitimRef();
		} else {
			return (Egitim) getObjectFromSessionFilter(ApplicationDescriptor._EGITIM_MODEL);
		}
	}

	public Egitimkatilimci getEgitimkatilimci() {
		egitimkatilimci = (Egitimkatilimci) getEntity();
		return egitimkatilimci;
	}

	public void setEgitimkatilimci(Egitimkatilimci egitimkatilimci) {
		this.egitimkatilimci = egitimkatilimci;
	}

	public EgitimkatilimciFilter getEgitimkatilimciFilter() {
		return egitimkatilimciFilter;
	}

	public void setEgitimkatilimciFilter(
			EgitimkatilimciFilter egitimkatilimciFilter) {
		this.egitimkatilimciFilter = egitimkatilimciFilter;
	}
//	public Egitim getEgitim() {
//		egitim = (Egitim) getEntity();
//		return egitim;
//	}
//
//	public void setEgitim(Egitim egitim) {
//		this.egitim = egitim;
//	}
	@Override
	public BaseFilter getFilter() {
		return getEgitimkatilimciFilter();
	}

	@Override
	public void resetFilter() {
		super.resetFilterOnly();
		if (getMasterEntity() != null) {
			getEgitimkatilimciFilter().setEgitimRef(getMasterEntity());
		}
		queryAction();
	}

	@Override
	public void insert() {
		super.insert();
		getEgitimkatilimci().setEgitimRef(
				getEgitimkatilimciFilter().getEgitimRef());
	}

	@Override
	public void save() {
			super.save();
	}
	public String egitimdegerlendirme() {
		if (!setSelected()) {
			return "";
		}
		getSessionUser().setLegalAccess(1);
		putObjectToSessionFilter(ApplicationDescriptor._EGITIMKATILIMCI_MODEL,	getEgitimkatilimci());
		return ManagedBeanLocator.locateMenuController().gotoPage("menu_Egitimdegerlendirme");
	}
	@Override
	public void tableRowSelect(SelectEvent event) {
		setEditPanelRendered(false);
		setEntity((BaseEntity) event.getObject());
		((Egitimkatilimci)event.getObject()).setEgitimRef(((Egitimkatilimci)event.getObject()).getEgitimRef());
		if(loggable){
			if(getEntity() != null){   
				putObjectToSessionFilter("logEntity", getEntity()); 	
			} 
		}
	}

} // class 
