package tr.com.arf.toys.view.controller.form.etkinlik;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import tr.com.arf.framework.db.enumerated._common.EvetHayir;
import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.toys.db.enumerated.etkinlik.EtkinlikDurum;
import tr.com.arf.toys.db.filter.etkinlik.EtkinliktanimFilter;
import tr.com.arf.toys.db.model.etkinlik.Etkinlik;
import tr.com.arf.toys.db.model.etkinlik.Etkinliktanim;
import tr.com.arf.toys.view.controller.form._base.ToysBaseFilteredController;

@ManagedBean
@ViewScoped
public class EtkinliktanimController extends ToysBaseFilteredController {

	private static final long serialVersionUID = 6531152165884980748L;

	private Etkinliktanim etkinliktanim;
	private EtkinliktanimFilter etkinliktanimFilter = new EtkinliktanimFilter();
	private Etkinlik etkinlik;

	public EtkinliktanimController() {
		super(Etkinliktanim.class);
		setDefaultValues(false);
		setOrderField("ad");
		setLoggable(false);
		setAutoCompleteSearchColumns(new String[] { "ad" });
		setTable(null);
		queryAction();
	}

	public Etkinliktanim getEtkinliktanim() {
		etkinliktanim = (Etkinliktanim) getEntity();
		return etkinliktanim;
	}

	public void setEtkinliktanim(Etkinliktanim etkinliktanim) {
		this.etkinliktanim = etkinliktanim;
	}

	public EtkinliktanimFilter getEtkinliktanimFilter() {
		return etkinliktanimFilter;
	}

	public void setEtkinliktanimFilter(EtkinliktanimFilter etkinliktanimFilter) {
		this.etkinliktanimFilter = etkinliktanimFilter;
	}

	@Override
	public BaseFilter getFilter() {
		return getEtkinliktanimFilter();
	}

	public Etkinlik getEtkinlik() {
		return etkinlik;
	}

	public void setEtkinlik(Etkinlik etkinlik) {
		this.etkinlik = etkinlik;
	}
	
	@Override
	public void insert(){
		super.insertOnly();
		try {
			etkinlik = Etkinlik.class.newInstance();
			etkinlik.setDurum(EtkinlikDurum._PLANLANMIS);
		} catch ( Exception e) {
			// TODO Auto-generated catch block
			logYaz("Exception @" + getModelName() + "Controller :", e);
		} 
		setEditPanelRendered(true);
		setAddScreen(true);
		setActiveTab("edit");
	}
	
	@Override
	public void save(){
		boolean newEtkinlik = getEtkinliktanim().getRID() == null;
		super.justSave();
		if(newEtkinlik == true){
			if(getEtkinliktanim().getPeriyodik() == EvetHayir._HAYIR){
				this.etkinlik.setEtkinliktanimRef(getEtkinliktanim());
				this.etkinlik.setAd(getEtkinliktanim().getAd());
				super.justSave(this.etkinlik, false, "");
			}
		}
		queryAction();
		setActiveTab("list");
		insertOnly();
	}
 

} // class 
