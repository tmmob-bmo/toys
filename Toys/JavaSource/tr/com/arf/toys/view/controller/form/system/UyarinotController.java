package tr.com.arf.toys.view.controller.form.system; 
  
 
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.toys.db.filter.system.UyarinotFilter;
import tr.com.arf.toys.db.model.system.Uyarinot;
import tr.com.arf.toys.view.controller.form._base.ToysBaseFilteredController;
  
  
@ManagedBean 
@ViewScoped 
public class UyarinotController extends ToysBaseFilteredController {  
  
  private static final long serialVersionUID = 6531152165884980748L; 
  
	private Uyarinot uyarinot;  
	private UyarinotFilter uyarinotFilter = new UyarinotFilter();  
  
	public UyarinotController() { 
		super(Uyarinot.class);  
		setDefaultValues(false);  
		setOrderField("uyarimetin");  
		setAutoCompleteSearchColumns(new String[]{"uyarimetin"}); 
		setStaticWhereCondition(getSessionUser().createStaticWhereCondition("o."));
		setTable(null); 
		queryAction(); 
	} 
	
	
	public Uyarinot getUyarinot() { 
		uyarinot = (Uyarinot) getEntity(); 
		return uyarinot; 
	} 
	
	public void setUyarinot(Uyarinot uyarinot) { 
		this.uyarinot = uyarinot; 
	} 
	
	public UyarinotFilter getUyarinotFilter() { 
		return uyarinotFilter; 
	} 
	 
	public void setUyarinotFilter(UyarinotFilter uyarinotFilter) {  
		this.uyarinotFilter = uyarinotFilter;  
	}  
	 
	@Override  
	public BaseFilter getFilter() {  
		return getUyarinotFilter();  
	}  
	
	
 
	
} // class 
