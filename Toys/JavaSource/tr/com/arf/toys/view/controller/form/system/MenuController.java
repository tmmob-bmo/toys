package tr.com.arf.toys.view.controller.form.system;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.primefaces.component.menuitem.MenuItem;

import tr.com.arf.framework.db._management.DBOperator;
import tr.com.arf.toys.db.model.system.Menuitem;
import tr.com.arf.toys.service.application.ApplicationDescriptor;
import tr.com.arf.toys.service.application.ManagedBeanLocator;
import tr.com.arf.toys.utility.logUtils.LogService;
import tr.com.arf.toys.view.controller._common.ApplicationController;
import tr.com.arf.toys.view.controller._common.SessionUser;

@ManagedBean(name = "menuController", eager = true)
@SessionScoped
public class MenuController {

	protected static Logger logger = Logger.getLogger("menuController");

	private List<MenuItem> menuItems;

	private List<MenuItem> evrakMenuItems;
	private List<MenuItem> finansMenuItems;
	private List<MenuItem> tanimlamalarMenuItems;
	private List<MenuItem> kullanicilarMenuItems;
	private List<MenuItem> raporlamaMenuItems;
	private List<MenuItem> uyeYonetimiMenuItems;
	private List<MenuItem> kisiKurumYonetimiMenuItems;
	private List<MenuItem> odaYonetimiMenuItems;
	private List<MenuItem> iletisimMenuItems;
	private List<MenuItem> etkinlikMenuItems;
	private List<MenuItem> egitimMenuItems;
	private List<MenuItem> yayinVeKutuphaneMenuItems;
	private List<MenuItem> iletisimLogMenuItems;
	private List<MenuItem> logMenuItems;

	@ManagedProperty(value = "#{applicationController}")
	private ApplicationController applicationController;

	private static DBOperator dbOperator = new DBOperator();

	private Map<String, Object> menuItemArray;

	public MenuController() {
		createHomePageLink();
		setEvrakMenuItems(this.evrakMenuItems);
		setTanimlamalarMenuItems(this.tanimlamalarMenuItems);
		setKullanicilarMenuItems(this.kullanicilarMenuItems);
		setFinansMenuItems(this.finansMenuItems);
		setRaporlamaMenuItems(this.raporlamaMenuItems);
		setUyeYonetimiMenuItems(this.uyeYonetimiMenuItems);
		setKisiKurumYonetimiMenuItems(this.kisiKurumYonetimiMenuItems);
		setOdaYonetimiMenuItems(this.odaYonetimiMenuItems);
		setIletisimMenuItems(this.iletisimMenuItems);
		setEtkinlikMenuItems(this.etkinlikMenuItems);
		setEgitimMenuItems(this.egitimMenuItems);
		setYayinVeKutuphaneMenuItems(this.yayinVeKutuphaneMenuItems);
		setLogMenuItems(this.logMenuItems);
	}

	public List<MenuItem> getMenuItems() {
		return menuItems;
	}

	public void setMenuItems(List<MenuItem> menuItems) {
		this.menuItems = menuItems;
	}

	public void createHomePageLink() {
		menuItems = new ArrayList<MenuItem>();
		this.menuItems.add(createMenuItem("Anasayfa", "homePage"));
	}

	private MenuItem createMenuItem(String value, String url) {
		MenuItem item = new MenuItem();
		try {
			item.setValue(value);
			item.setUrl(url);
		} catch (Exception e) {
			LogService.logYaz("Error @createMenuItem of :" + e.getMessage());
		}
		return item;
	}

	public List<Menuitem> findParentItems(Menuitem calledItem) {
		try {
			ArrayList<Menuitem> menuItems = new ArrayList<Menuitem>();
			String x = calledItem.getErisimKodu();
			while (StringUtils.countMatches(x, ".") > 0 && x.lastIndexOf('.') > 0) {
				x = x.substring(0, x.lastIndexOf('.'));
				menuItems.add((Menuitem) menuItemArray.get(x));
			}
			ArrayList<Menuitem> copy = new ArrayList<Menuitem>(menuItems);
			Collections.reverse(copy);
			copy.add(calledItem);
			return copy;
		} catch (Exception e) {
			LogService.logYaz("Error @findParentItems of MenuController : " + e.getMessage());
			return new ArrayList<Menuitem>();
		}
	}

	@SuppressWarnings("unchecked")
	public Map<String, Object> createMenuItemList() {
		Map<String, Object> menuItems = new HashMap<String, Object>();
		@SuppressWarnings("static-access")
		List<Menuitem> menuItemList = this.dbOperator.load(Menuitem.class.getSimpleName());
		if (menuItemList != null && menuItemList.size() > 0) {
			for (Menuitem menuItem : menuItemList) {
				menuItems.put(menuItem.getErisimKodu(), menuItem);
			}
		}
		return menuItems;
	}

	public String gotoPageForMenu(String pageCode) {
		ManagedBeanLocator.locateSessionUser().clearSessionFilters();
		return gotoPage(pageCode);
	}

	public String gotoPage(String pageCode) {
		Menuitem calledItem = new Menuitem();
		if (menuItemArray == null || menuItemArray.size() == 0) {
			// TODO, TEST ICIN
			menuItemArray = createMenuItemList();
			// menuItemArray = getApplicationController().getMenuItems();
		}
		try {
			calledItem = (Menuitem) dbOperator.find(Menuitem.class.getSimpleName(), "LOWER(o.kod) = LOWER('" + pageCode + "') or UPPER(o.kod) = UPPER('" + pageCode + "')");
			List<Menuitem> menuItemList = findParentItems(calledItem);
			menuItems = new ArrayList<MenuItem>();
			for (Menuitem item : menuItemList) {
				this.menuItems.add(createMenuItem(item.getAd(), item.getKod()));
			}
			if (calledItem.getUrl().equalsIgnoreCase(ApplicationDescriptor._DASHBOARD_OUTCOME) || calledItem.getAd().equalsIgnoreCase("Anasayfa")) {
				return homePage();
			} else {
				// LogService.logYaz("pageCode :" + pageCode);
				// LogService.logYaz("url :" + calledItem.getUrl());

				/* OGRENCI UYE LISTESI CAGRILMISSA, SESSION'A BUNUNLA ILGILI BIR PARAMETRE SET EDILIYOR... */
				if (calledItem.getKod().equalsIgnoreCase("menu_OgrenciUyeListesi")) {
					ManagedBeanLocator.locateSessionUser().removeSessionFilterObject("YabanciOnly");
					ManagedBeanLocator.locateSessionUser().addToSessionFilters("OgrenciOnly", "true");
				} else if (calledItem.getKod().equalsIgnoreCase("menu_YabanciUyeListesi")) {
					ManagedBeanLocator.locateSessionUser().removeSessionFilterObject("OgrenciOnly");
					ManagedBeanLocator.locateSessionUser().addToSessionFilters("YabanciOnly", "true");
				} else if (calledItem.getKod().equalsIgnoreCase("menu_Uye")) {
					ManagedBeanLocator.locateSessionUser().removeSessionFilterObject("OgrenciOnly");
					ManagedBeanLocator.locateSessionUser().removeSessionFilterObject("YabanciOnly");
				}
				LogService.logYaz("Cagrilan sayfa :" + calledItem.getUrl());
				return calledItem.getUrl();
			}
		} catch (Exception e) {
			LogService.logYaz("ERROR @gotoPage : " + e.getMessage() +"\npageCode " + pageCode  + "\ncalledItem url :" + calledItem.getUrl());
			return homePage();
		}
	}

	public String homePage() {
		SessionUser sessionUser = ManagedBeanLocator.locateSessionUser();
		sessionUser.clearSessionFilters();
		ManagedBeanLocator.locateSessionController().setDetailEditType(null);
		ManagedBeanLocator.locateMenuController().createHomePageLink();
		return ApplicationDescriptor._DASHBOARD_OUTCOME;
	}

	public String uyeHomePage() {
		SessionUser sessionUser = ManagedBeanLocator.locateSessionUser();
		sessionUser.clearSessionFilters();
		ManagedBeanLocator.locateSessionController().setDetailEditType(null);
		ManagedBeanLocator.locateMenuController().createHomePageLink();
		return ApplicationDescriptor._UYEDASHBOARD_OUTCOME;
	}

	public String logout() {
		FacesContext context = FacesContext.getCurrentInstance();
		LogService.logYaz("KULLANICI CIKIS YAPTI...", "MenuController");
		HttpSession session = (HttpSession) context.getExternalContext().getSession(false);
		if (session != null) {
			session.invalidate();
		}
		return "login.do";
	}

	public String getWhereCondition() {
		if (ManagedBeanLocator.locateSessionUser() == null || ManagedBeanLocator.locateSessionUser().getSelectedRole() == null || ManagedBeanLocator.locateSessionUser().getSelectedRole().getRID() == null) {
			logout();
		}
		if (ManagedBeanLocator.locateSessionUser().isSuperUser()) {
			return "";
		} else {
			return " AND (SELECT m.listPermission FROM IslemKullanici m WHERE m.kullaniciRoleRef.rID = " + ManagedBeanLocator.locateSessionUser().getSelectedRole().getRID() + " " + "AND UPPER(m.islemRef.name) = UPPER(o.tablo) ) = 1 ";
		}
	}

	public ApplicationController getApplicationController() {
		return applicationController;
	}

	public void setApplicationController(ApplicationController applicationController) {
		this.applicationController = applicationController;
	}

	public Map<String, Object> getMenuItemArray() {
		return menuItemArray;
	}

	public void setMenuItemArray(Map<String, Object> menuItemArray) {
		this.menuItemArray = menuItemArray;
	}

	public List<MenuItem> getEvrakMenuItems() {
		return evrakMenuItems;
	}

	@SuppressWarnings("unchecked")
	public void setEvrakMenuItems(List<MenuItem> evrakMenuItems) {
		evrakMenuItems = dbOperator.load(Menuitem.class.getSimpleName(), "o.rID > 1 AND o.ustRef.kod='menu_EvrakBaslik' " + getWhereCondition(), "o.erisimKodu ASC");
		this.evrakMenuItems = evrakMenuItems;
	}

	public List<MenuItem> getFinansMenuItems() {
		return finansMenuItems;
	}

	@SuppressWarnings("unchecked")
	public void setFinansMenuItems(List<MenuItem> finansMenuItems) {
		finansMenuItems = dbOperator.load(Menuitem.class.getSimpleName(), "o.rID > 1 AND o.ustRef.kod='menu_FinansBaslik' " + getWhereCondition(), "o.erisimKodu ASC");
		this.finansMenuItems = finansMenuItems;
	}

	public List<MenuItem> getTanimlamalarMenuItems() {
		return tanimlamalarMenuItems;
	}

	@SuppressWarnings("unchecked")
	public void setTanimlamalarMenuItems(List<MenuItem> tanimlamalarMenuItems) {
		tanimlamalarMenuItems = dbOperator.load(Menuitem.class.getSimpleName(), "o.rID > 1 AND o.ustRef.kod='menu_TanimlamaBaslik' " + getWhereCondition(), "o.erisimKodu ASC");
		this.tanimlamalarMenuItems = tanimlamalarMenuItems;
	}

	public List<MenuItem> getLogMenuItems() {
		return logMenuItems;
	}

	@SuppressWarnings("unchecked")
	public void setLogMenuItems(List<MenuItem> logMenuItems) {
		logMenuItems = dbOperator.load(Menuitem.class.getSimpleName(), "o.rID > 1 AND o.ustRef.kod='menu_LogBaslik'" + getWhereCondition(), "o.erisimKodu ASC");
		this.logMenuItems = logMenuItems;
	}

	public List<MenuItem> getKullanicilarMenuItems() {
		return kullanicilarMenuItems;
	}

	@SuppressWarnings("unchecked")
	public void setKullanicilarMenuItems(List<MenuItem> kullanicilarMenuItems) {
		kullanicilarMenuItems = dbOperator.load(Menuitem.class.getSimpleName(), "o.rID > 1 AND o.ustRef.kod='menu_KullaniciBaslik' " + getWhereCondition(), "o.erisimKodu ASC");
		this.kullanicilarMenuItems = kullanicilarMenuItems;
	}

	public List<MenuItem> getRaporlamaMenuItems() {
		return raporlamaMenuItems;
	}

	@SuppressWarnings("unchecked")
	public void setRaporlamaMenuItems(List<MenuItem> raporlamaMenuItems) {
		raporlamaMenuItems = dbOperator.load(Menuitem.class.getSimpleName(), "o.rID > 1 AND o.ustRef.kod='menu_RaporBaslik' " + getWhereCondition(), "o.erisimKodu ASC");
		this.raporlamaMenuItems = raporlamaMenuItems;
	}

	public List<MenuItem> getUyeYonetimiMenuItems() {
		return uyeYonetimiMenuItems;
	}

	@SuppressWarnings("unchecked")
	public void setUyeYonetimiMenuItems(List<MenuItem> uyeYonetimiMenuItems) {
		uyeYonetimiMenuItems = dbOperator.load(Menuitem.class.getSimpleName(), "o.rID > 1 AND o.ustRef.kod='menu_UyeYonetimi'" + getWhereCondition(), "o.erisimKodu ASC");
		this.uyeYonetimiMenuItems = uyeYonetimiMenuItems;
	}

	public List<MenuItem> getKisiKurumYonetimiMenuItems() {
		return kisiKurumYonetimiMenuItems;
	}

	@SuppressWarnings("unchecked")
	public void setKisiKurumYonetimiMenuItems(List<MenuItem> kisiKurumYonetimiMenuItems) {
		kisiKurumYonetimiMenuItems = dbOperator.load(Menuitem.class.getSimpleName(), "o.rID > 1 AND o.ustRef.kod='menu_KisiKurumYonetimi'" + getWhereCondition(), "o.erisimKodu ASC");
		this.kisiKurumYonetimiMenuItems = kisiKurumYonetimiMenuItems;
	}

	public List<MenuItem> getOdaYonetimiMenuItems() {
		return odaYonetimiMenuItems;
	}

	@SuppressWarnings("unchecked")
	public void setOdaYonetimiMenuItems(List<MenuItem> odaYonetimiMenuItems) {
		odaYonetimiMenuItems = dbOperator.load(Menuitem.class.getSimpleName(), "o.rID > 1 AND o.ustRef.kod='menu_OdaYonetimi'" + getWhereCondition(), "o.erisimKodu ASC");
		this.odaYonetimiMenuItems = odaYonetimiMenuItems;
	}

	public List<MenuItem> getIletisimMenuItems() {
		return iletisimMenuItems;
	}

	@SuppressWarnings("unchecked")
	public void setIletisimMenuItems(List<MenuItem> iletisimMenuItems) {
		iletisimMenuItems = dbOperator.load(Menuitem.class.getSimpleName(), "o.rID > 1 AND o.ustRef.kod LIKE 'menu_Iletisim%'" + getWhereCondition(), "o.erisimKodu ASC");
		this.iletisimMenuItems = iletisimMenuItems;
	}

	public List<MenuItem> getEtkinlikMenuItems() {
		return etkinlikMenuItems;
	}

	@SuppressWarnings("unchecked")
	public void setEtkinlikMenuItems(List<MenuItem> etkinlikMenuItems) {
		etkinlikMenuItems = dbOperator.load(Menuitem.class.getSimpleName(), "o.rID > 1 AND o.ustRef.kod='menu_Etkinlik'" + getWhereCondition(), "o.erisimKodu ASC");
		this.etkinlikMenuItems = etkinlikMenuItems;
	}

	public List<MenuItem> getEgitimMenuItems() {
		return egitimMenuItems;
	}

	@SuppressWarnings("unchecked")
	public void setEgitimMenuItems(List<MenuItem> egitimMenuItems) {
		egitimMenuItems = dbOperator.load(Menuitem.class.getSimpleName(), "o.rID > 1 AND o.ustRef.kod='menu_EgitimYonetimi'" + getWhereCondition(), "o.erisimKodu ASC");
		this.egitimMenuItems = egitimMenuItems;
	}

	public List<MenuItem> getYayinVeKutuphaneMenuItems() {
		return yayinVeKutuphaneMenuItems;
	}

	@SuppressWarnings("unchecked")
	public void setYayinVeKutuphaneMenuItems(List<MenuItem> yayinVeKutuphaneMenuItems) {
		yayinVeKutuphaneMenuItems = dbOperator.load(Menuitem.class.getSimpleName(), "o.rID > 1 AND o.ustRef.kod='menu_YayinVeKutuphane'" + getWhereCondition(), "o.erisimKodu ASC");
		this.yayinVeKutuphaneMenuItems = yayinVeKutuphaneMenuItems;
	}

	public List<MenuItem> getIletisimLogMenuItems() {
		return iletisimLogMenuItems;
	}

	@SuppressWarnings("unchecked")
	public void setIletisimLogMenuItems(List<MenuItem> iletisimLogMenuItems) {
		iletisimLogMenuItems = dbOperator.load(Menuitem.class.getSimpleName(), "o.rID > 1 AND o.ustRef.kod='menu_Loglar' " + getWhereCondition(), "o.erisimKodu ASC");
		this.iletisimLogMenuItems = iletisimLogMenuItems;
	}

}
