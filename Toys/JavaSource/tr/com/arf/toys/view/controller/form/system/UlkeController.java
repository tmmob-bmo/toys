package tr.com.arf.toys.view.controller.form.system; 
  
 
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.toys.db.filter.system.UlkeFilter;
import tr.com.arf.toys.db.model.system.Ulke;
import tr.com.arf.toys.service.application.ApplicationDescriptor;
import tr.com.arf.toys.service.application.ManagedBeanLocator;
import tr.com.arf.toys.utility.tool.KPSUlkeUpdater;
import tr.com.arf.toys.view.controller.form._base.ToysFilteredControllerNoPaging;
  
  
@ManagedBean 
@ViewScoped 
public class UlkeController extends ToysFilteredControllerNoPaging {  
  
  private static final long serialVersionUID = 6531152165884980748L; 
  
	private Ulke ulke;  
	private UlkeFilter ulkeFilter = new UlkeFilter();  
  
	public UlkeController() { 
		super(Ulke.class);  
		setDefaultValues(false);  
		setOrderField("ad");  
		setAutoCompleteSearchColumns(new String[]{"ad"}); 
		setTable(null); 
		queryAction(); 
	} 
	
	
	public Ulke getUlke() { 
		ulke = (Ulke) getEntity(); 
		return ulke; 
	} 
	
	public void setUlke(Ulke ulke) { 
		this.ulke = ulke; 
	} 
	
	public UlkeFilter getUlkeFilter() { 
		return ulkeFilter; 
	} 
	 
	public void setUlkeFilter(UlkeFilter ulkeFilter) {  
		this.ulkeFilter = ulkeFilter;  
	}  
	 
	@Override  
	public BaseFilter getFilter() {  
		return getUlkeFilter();  
	}  
	
	public String sehir() {	
		if (!setSelected()) return "";	
		getSessionUser().setLegalAccess(1);	
		putObjectToSessionFilter(ApplicationDescriptor._ULKE_MODEL, getUlke());
		return ManagedBeanLocator.locateMenuController().gotoPage("menu_Sehir"); 
	} 
	
	public void kpsGuncelleme() throws Exception{ 
			KPSUlkeUpdater.ulkeGuncelle();
			queryAction();
			createGenericMessage("KPS'den güncelleme işlemi tamamlandı!", FacesMessage.SEVERITY_INFO);
	}
	
} // class 
