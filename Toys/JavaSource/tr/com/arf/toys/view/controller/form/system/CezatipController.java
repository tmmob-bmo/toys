package tr.com.arf.toys.view.controller.form.system; 
  
 
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.toys.db.filter.system.CezatipFilter;
import tr.com.arf.toys.db.model.system.Cezatip;
import tr.com.arf.toys.view.controller.form._base.ToysBaseFilteredController;
  
  
@ManagedBean 
@ViewScoped 
public class CezatipController extends ToysBaseFilteredController {  
  
  private static final long serialVersionUID = 6531152165884980748L; 
  
	private Cezatip cezatip;  
	private CezatipFilter cezatipFilter = new CezatipFilter();  
  
	public CezatipController() { 
		super(Cezatip.class);  
		setDefaultValues(false);  
		setOrderField("kisaad");  
		setAutoCompleteSearchColumns(new String[]{"kisaad"}); 
		setTable(null); 
		queryAction(); 
	} 
	
	
	public Cezatip getCezatip() { 
		cezatip = (Cezatip) getEntity(); 
		return cezatip; 
	} 
	
	public void setCezatip(Cezatip cezatip) { 
		this.cezatip = cezatip; 
	} 
	
	public CezatipFilter getCezatipFilter() { 
		return cezatipFilter; 
	} 
	 
	public void setCezatipFilter(CezatipFilter cezatipFilter) {  
		this.cezatipFilter = cezatipFilter;  
	}  
	 
	@Override  
	public BaseFilter getFilter() {  
		return getCezatipFilter();  
	}  
	
	
 
	
} // class 
