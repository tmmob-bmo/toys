package tr.com.arf.toys.view.controller.form.kisi; 
  
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.model.SelectItem;
import javax.persistence.Transient;

import org.primefaces.model.DualListModel;

import tr.com.arf.framework.db.enumerated._common.EvetHayir;
import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.framework.db.model._base.BaseEntity;
import tr.com.arf.framework.utility.exception.data.DBException;
import tr.com.arf.toys.db.filter.kisi.KurulDonemToplantiKatilimciFilter;
import tr.com.arf.toys.db.model.kisi.KurulDonemToplanti;
import tr.com.arf.toys.db.model.kisi.KurulDonemToplantiKatilimci;
import tr.com.arf.toys.db.model.kisi.KurulDonemUye;
import tr.com.arf.toys.service.application.ApplicationDescriptor;
import tr.com.arf.toys.view.controller.form._base.ToysBaseFilteredController;
  
  
@ManagedBean 
@ViewScoped 
public class KurulDonemToplantiKatilimciController extends ToysBaseFilteredController {  
  
  private static final long serialVersionUID = 6531152165884980748L; 
  
	private KurulDonemToplantiKatilimci kurulDonemToplantiKatilimci;  
	private KurulDonemToplantiKatilimciFilter kurulDonemToplantiKatilimciFilter = new KurulDonemToplantiKatilimciFilter();  
  
	public KurulDonemToplantiKatilimciController() { 
		super(KurulDonemToplantiKatilimci.class);  
		setDefaultValues(false);  
		setTable(null); 
		if(getMasterEntity() != null){ 
			setMasterObjectName(ApplicationDescriptor._KURULDONEMTOPLANTI_MODEL);
			setMasterOutcome(ApplicationDescriptor._KURULDONEMTOPLANTI_MODEL);
			getKurulDonemToplantiKatilimciFilter().setKurulDonemToplantiRef(getMasterEntity());
			getKurulDonemToplantiKatilimciFilter().createQueryCriterias();   
		} 
		
		queryAction(); 
	} 
	
	public KurulDonemToplanti getMasterEntity(){
		if(getKurulDonemToplantiKatilimciFilter().getKurulDonemToplantiRef() != null){
			return getKurulDonemToplantiKatilimciFilter().getKurulDonemToplantiRef();
		} else {
			return (KurulDonemToplanti) getObjectFromSessionFilter(ApplicationDescriptor._KURULDONEMTOPLANTI_MODEL);
		}			
	} 
	
	public KurulDonemToplantiKatilimci getKurulDonemToplantiKatilimci() {
		kurulDonemToplantiKatilimci = (KurulDonemToplantiKatilimci) getEntity(); 
		return kurulDonemToplantiKatilimci;
	}

	public void setKurulDonemToplantiKatilimci(
			KurulDonemToplantiKatilimci kurulDonemToplantiKatilimci) {
		this.kurulDonemToplantiKatilimci = kurulDonemToplantiKatilimci;
	}

	public KurulDonemToplantiKatilimciFilter getKurulDonemToplantiKatilimciFilter() {
		return kurulDonemToplantiKatilimciFilter;
	}

	public void setKurulDonemToplantiKatilimciFilter(
			KurulDonemToplantiKatilimciFilter kurulDonemToplantiKatilimciFilter) {
		this.kurulDonemToplantiKatilimciFilter = kurulDonemToplantiKatilimciFilter;
	}

	@Override  
	public BaseFilter getFilter() {  
		return getKurulDonemToplantiKatilimciFilter();  
	} 
	
	private SelectItem[] kurulDonemUyeItems = new SelectItem[0];
	
	@Transient
	public SelectItem[] getKurulDonemUye(){ 
			this.kurulDonemToplantiKatilimci.setKurulDonemToplantiRef(kurulDonemToplantiKatilimciFilter.getKurulDonemToplantiRef());
			@SuppressWarnings("unchecked")
			List <KurulDonemUye> kurulDonemUyeListesi=getDBOperator().load(KurulDonemUye.class.getSimpleName(), "o.kurulDonemRef.rID=" + kurulDonemToplantiKatilimciFilter.getKurulDonemToplantiRef().getKurulDonemRef().getRID() , "");
			if (!isEmpty(kurulDonemUyeListesi)){ 
				int x=1;
				kurulDonemUyeItems = new SelectItem[kurulDonemUyeListesi.size() + 1];
				kurulDonemUyeItems[0] = new SelectItem(null, "");
				for (KurulDonemUye kurulDonemUye : kurulDonemUyeListesi){
					kurulDonemUyeItems[x]=new SelectItem(kurulDonemUye, kurulDonemUye.getUIString());
					x++; 
				} 
			} else { 
				kurulDonemUyeItems = new SelectItem[0];
			}
			return kurulDonemUyeItems;
	}
	
	@Override
		public void insert() {
			super.insert();
			try {
				if (getDBOperator().recordCount(KurulDonemUye.class.getSimpleName(), "o.kurulDonemRef.rID=" +getMasterEntity().getKurulDonemRef().getRID())<=0){
					super.fillDualListFor(KurulDonemUye.class.getSimpleName(), "o.kurulDonemRef.rID=" +getMasterEntity().getKurulDonemRef().getRID(), "");
				}
				else{
					update();
				}
			} catch (DBException e) {
				// TODO Auto-generated catch block
				logYaz("Exception @" + getModelName() + "Controller :", e);
			}
		}
	
	@Override
		public void save() {
		try {	
			if(!isEmpty(getDualList().getTarget())){
				List<BaseEntity> kurulDonemUyeList = getDualList().getTarget(); 			
				for(BaseEntity b : kurulDonemUyeList){
					
						if (getDBOperator().recordCount(KurulDonemToplantiKatilimci.class.getSimpleName(),"o.kurulDonemToplantiRef.rID=" +getMasterEntity().getRID() 
								+" AND o.kurulDonemUyeRef.rID=" + ((KurulDonemUye)b).getRID() )>0){
							KurulDonemToplantiKatilimci kdtk=(KurulDonemToplantiKatilimci) getDBOperator().load(KurulDonemToplantiKatilimci.class.getSimpleName(),"o.kurulDonemToplantiRef.rID=" +getMasterEntity().getRID() 
								+" AND o.kurulDonemUyeRef.rID=" + ((KurulDonemUye)b).getRID(),"").get(0);
							getDBOperator().update(kdtk);
						}
						else {
							kurulDonemToplantiKatilimci=new KurulDonemToplantiKatilimci();
							kurulDonemToplantiKatilimci.setKatilim(EvetHayir._EVET);
							kurulDonemToplantiKatilimci.setKurulDonemToplantiRef(getMasterEntity());
							kurulDonemToplantiKatilimci.setKurulDonemUyeRef((KurulDonemUye)b);
							getDBOperator().insert(kurulDonemToplantiKatilimci);
						}
				}
			}
			if (!isEmpty(getDualList().getSource())){
				List<BaseEntity> kurulDonemUyeList = getDualList().getSource(); 
				for(BaseEntity b : kurulDonemUyeList){
					if (getDBOperator().recordCount(KurulDonemToplantiKatilimci.class.getSimpleName(),"o.kurulDonemToplantiRef.rID=" +getMasterEntity().getRID() 
							+" AND o.kurulDonemUyeRef.rID=" + ((KurulDonemUye)b).getRID() )>0){
						KurulDonemToplantiKatilimci kdtk=(KurulDonemToplantiKatilimci) getDBOperator().load(KurulDonemToplantiKatilimci.class.getSimpleName(),"o.kurulDonemToplantiRef.rID=" +getMasterEntity().getRID() 
							+" AND o.kurulDonemUyeRef.rID=" + ((KurulDonemUye)b).getRID(),"").get(0);
						getDBOperator().delete(kdtk);
					}
				}
			}
		} catch (DBException e) {
			// TODO Auto-generated catch block
			logYaz("Exception @" + getModelName() + "Controller :", e);
		}
			queryAction();
		}
	
	@SuppressWarnings("unchecked")
	@Override
		public void update() {
			super.update();
			try {
				if (getDBOperator().recordCount(KurulDonemToplantiKatilimci.class.getSimpleName(),"o.kurulDonemToplantiRef.kurulDonemRef.rID=" + getMasterEntity().getKurulDonemRef().getRID())>0){
					List<BaseEntity> target =new ArrayList<BaseEntity>();
					List<BaseEntity> source =new ArrayList<BaseEntity>();
					List<Long> kurulDonemUyeRID=new ArrayList<Long>();
					List <KurulDonemToplantiKatilimci> kurulToplantiKatilimciList =getDBOperator().load(KurulDonemToplantiKatilimci.class.getSimpleName(),
							"o.kurulDonemToplantiRef.kurulDonemRef.rID=" + getMasterEntity().getKurulDonemRef().getRID(),"");
					for (KurulDonemToplantiKatilimci entity : kurulToplantiKatilimciList){
						target.add(entity.getKurulDonemUyeRef());
						kurulDonemUyeRID.add(entity.getKurulDonemUyeRef().getRID());
					}
					Long rID=0L;
					if (getDBOperator().recordCount(KurulDonemUye.class.getSimpleName() , "  o.kurulDonemRef.rID=" +getMasterEntity().getKurulDonemRef().getRID())>0){
						List <KurulDonemUye> kurulDonemUyeList=getDBOperator().load(KurulDonemUye.class.getSimpleName() , "o.kurulDonemRef.rID=" +getMasterEntity().getKurulDonemRef().getRID(),"");
						for (KurulDonemUye entity : kurulDonemUyeList){
							for (Long kuruluyeRID : kurulDonemUyeRID){
								rID=kuruluyeRID;
								if (entity.getRID() ==kuruluyeRID){
									break;
								}
							}
							if (entity.getRID()!=rID){
								source.add(entity);
							}
						}
						setDualList(new DualListModel<BaseEntity>(source, target)); 
					}
				} else {
					super.fillDualListFor(KurulDonemUye.class.getSimpleName(), "o.kurulDonemRef.rID=" +getMasterEntity().getKurulDonemRef().getRID(), "");
				}
			} catch (DBException e) {
				logYaz("Exception @" + getModelName() + "Controller :", e);
			}
		}
	
	
 
} // class 
