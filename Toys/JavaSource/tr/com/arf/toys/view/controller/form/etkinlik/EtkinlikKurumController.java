package tr.com.arf.toys.view.controller.form.etkinlik; 
  
 
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.toys.db.filter.etkinlik.EtkinlikKurumFilter;
import tr.com.arf.toys.db.model.etkinlik.Etkinlik;
import tr.com.arf.toys.db.model.etkinlik.EtkinlikKurum;
import tr.com.arf.toys.service.application.ApplicationDescriptor;
import tr.com.arf.toys.view.controller.form._base.ToysBaseFilteredController;
  
  
@ManagedBean 
@ViewScoped 
public class EtkinlikKurumController extends ToysBaseFilteredController {  
  
  private static final long serialVersionUID = 6531152165884980748L; 
  
	private EtkinlikKurum etkinlikKurum;  
	private EtkinlikKurumFilter etkinlikKurumFilter = new EtkinlikKurumFilter();  
  
	public EtkinlikKurumController() { 
		super(EtkinlikKurum.class);  
		setDefaultValues(false);  
		setOrderField("etkinlikRef.rID");  
		setLoggable(false);  
		setAutoCompleteSearchColumns(new String[]{"etkinlikRef"}); 
		setTable(null); 
		if(getMasterEntity() != null){ 
			setMasterObjectName(ApplicationDescriptor._ETKINLIK_MODEL);
			setMasterOutcome(ApplicationDescriptor._ETKINLIK_MODEL);
			getEtkinlikKurumFilter().setEtkinlikRef(getMasterEntity());
			getEtkinlikKurumFilter().createQueryCriterias();   
		} 
		queryAction(); 
	} 
 
	public Etkinlik getMasterEntity(){
		if(getEtkinlikKurumFilter().getEtkinlikRef() != null){
			return getEtkinlikKurumFilter().getEtkinlikRef();
		} else {
			return (Etkinlik) getObjectFromSessionFilter(ApplicationDescriptor._ETKINLIK_MODEL);
		}			
	} 
	
	public EtkinlikKurum getEtkinlikKurum() { 
		etkinlikKurum = (EtkinlikKurum) getEntity(); 
		return etkinlikKurum; 
	} 
	
	public void setEtkinlikKurum(EtkinlikKurum etkinlikKurum) { 
		this.etkinlikKurum = etkinlikKurum; 
	} 
	
	public EtkinlikKurumFilter getEtkinlikKurumFilter() { 
		return etkinlikKurumFilter; 
	} 
	 
	public void setEtkinlikKurumFilter(EtkinlikKurumFilter etkinlikKurumFilter) {  
		this.etkinlikKurumFilter = etkinlikKurumFilter;  
	}  
	 
	@Override  
	public BaseFilter getFilter() {  
		return getEtkinlikKurumFilter();  
	}  
	
	@Override	
	public void insert() {	
		super.insert();	
		getEtkinlikKurum().setEtkinlikRef(getEtkinlikKurumFilter().getEtkinlikRef());	
	}	
	
} // class 
