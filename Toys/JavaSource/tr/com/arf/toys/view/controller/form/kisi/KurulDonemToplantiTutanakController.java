package tr.com.arf.toys.view.controller.form.kisi; 
  
 
import java.io.File;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.toys.db.filter.kisi.KurulDonemToplantiTutanakFilter;
import tr.com.arf.toys.db.model.kisi.KurulDonemToplanti;
import tr.com.arf.toys.db.model.kisi.KurulDonemToplantiTutanak;
import tr.com.arf.toys.service.application.ApplicationDescriptor;
import tr.com.arf.toys.view.controller.form._base.ToysBaseFilteredController;
  
  
@ManagedBean 
@ViewScoped 
public class KurulDonemToplantiTutanakController extends ToysBaseFilteredController {  
  
  private static final long serialVersionUID = 6531152165884980748L; 
  
	private KurulDonemToplantiTutanak kurulDonemToplantiTutanak;  
	private KurulDonemToplantiTutanakFilter kurulDonemToplantiTutanakFilter = new KurulDonemToplantiTutanakFilter();  
	
	private String filename;
  
	public KurulDonemToplantiTutanakController() { 
		super(KurulDonemToplantiTutanak.class);  
		setDefaultValues(false);  
		setOrderField("path");  
		setLoggable(false);  
		setAutoCompleteSearchColumns(new String[]{"path"}); 
		setTable(null); 
		if(getMasterEntity() != null){ 
			setMasterObjectName(ApplicationDescriptor._KURULDONEMTOPLANTI_MODEL);
			setMasterOutcome(ApplicationDescriptor._KURULDONEMTOPLANTI_MODEL);
			getKurulDonemToplantiTutanakFilter().setKurulDonemToplantiRef(getMasterEntity());
			getKurulDonemToplantiTutanakFilter().createQueryCriterias();   
		} 
		queryAction(); 
	} 
 
	public KurulDonemToplanti getMasterEntity(){
		if(getKurulDonemToplantiTutanakFilter().getKurulDonemToplantiRef() != null){
			return getKurulDonemToplantiTutanakFilter().getKurulDonemToplantiRef();
		} else {
			return (KurulDonemToplanti) getObjectFromSessionFilter(ApplicationDescriptor._KURULDONEMTOPLANTI_MODEL);
		}			
	} 
	
	public KurulDonemToplantiTutanak getKurulDonemToplantiTutanak() { 
		kurulDonemToplantiTutanak = (KurulDonemToplantiTutanak) getEntity(); 
		return kurulDonemToplantiTutanak; 
	} 
	
	public void setKurulDonemToplantiTutanak(KurulDonemToplantiTutanak kurulDonemToplantiTutanak) { 
		this.kurulDonemToplantiTutanak = kurulDonemToplantiTutanak; 
	} 
	
	public KurulDonemToplantiTutanakFilter getKurulDonemToplantiTutanakFilter() { 
		return kurulDonemToplantiTutanakFilter; 
	} 
	 
	public void setKurulDonemToplantiTutanakFilter(KurulDonemToplantiTutanakFilter kurulDonemToplantiTutanakFilter) {  
		this.kurulDonemToplantiTutanakFilter = kurulDonemToplantiTutanakFilter;  
	}
	 
	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

	@Override  
	public BaseFilter getFilter() {  
		return getKurulDonemToplantiTutanakFilter();  
	}  
	 
 
	@Override	
	public void insert() {	
		super.insert();	
		getKurulDonemToplantiTutanak().setKurulDonemToplantiRef(getKurulDonemToplantiTutanakFilter().getKurulDonemToplantiRef());	
		getKurulDonemToplantiTutanak().setYukleyenRef(getSessionUser().getPersonelRef()); 
	}	
	
	
	
	@Override
		public void fileUploadListener(FileUploadEvent event) throws Exception {
			UploadedFile item = event.getFile(); 
	        File creationFile = createFile(item); 
	        getEntity().setPath(creationFile.getAbsolutePath()); 
	        String path1 = creationFile.getAbsolutePath();
	        setFilename(path1.substring(41));
		}
	
	
} // class 
