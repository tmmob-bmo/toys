package tr.com.arf.toys.view.controller.form.gorev; 
  
 
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.framework.utility.exception.data.DBException;
import tr.com.arf.framework.utility.tool.KeyUtil;
import tr.com.arf.toys.db.filter.gorev.GorevFilter;
import tr.com.arf.toys.db.model.gorev.Gorev;
import tr.com.arf.toys.service.application.ApplicationDescriptor;
import tr.com.arf.toys.view.controller.form._base.ToysBaseTreeController;
  
  
@ManagedBean 
@ViewScoped 
public class GorevController extends ToysBaseTreeController {  
  
  private static final long serialVersionUID = 6531152165884980748L; 
  
	private Gorev gorev;  
	private GorevFilter gorevFilter = new GorevFilter();  
  
	public GorevController() { 
		super(Gorev.class);  
		setDefaultValues(false);  
		setOrderField("ad");  
		setAutoCompleteSearchColumns(new String[]{"ad"}); 
		setTable(null); 
		queryAction(); 
	} 
	
	
	public Gorev getGorev() { 
		gorev = (Gorev) getEntity(); 
		return gorev; 
	} 
	
	public void setGorev(Gorev gorev) { 
		this.gorev = gorev; 
	} 
	
	public GorevFilter getGorevFilter() { 
		return gorevFilter; 
	} 
	 
	public void setGorevFilter(GorevFilter gorevFilter) {  
		this.gorevFilter = gorevFilter;  
	}  
	 
	@Override  
	public BaseFilter getFilter() {  
		return getGorevFilter();  
	}  	
 
	@Override 
	public synchronized void save() {   
		setTable(null);		
		if (getGorev().getRID() == null) { 
			try {  
				getGorev().setErisimKodu("-"); 
				getDBOperator().insert(getGorev()); 
				if(getGorev().getUstRef() != null) {
					getGorev().setErisimKodu(getGorev().getUstRef().getErisimKodu() + "." + getGorev().getRID());
				} else {
					getGorev().setErisimKodu(getGorev().getRID()+ "");
				}
				getDBOperator().update(getGorev());
				createGenericMessage(KeyUtil.getMessageValue("kayit.eklendi"), FacesMessage.SEVERITY_INFO);
			} catch (DBException e) { 
				getGorev().setRID(null); 
				createGenericMessage(KeyUtil.getMessageValue("islem.hata"), FacesMessage.SEVERITY_ERROR);
				return;
			} 
		} 
		else { 
			try { 
				if(getGorev().getUstRef() != null) {
					getGorev().setErisimKodu(getGorev().getUstRef().getErisimKodu() + "." + getGorev().getRID());
				} else {
					getGorev().setErisimKodu(getGorev().getRID() + "");
				}
				if(getGorev().getValue().hashCode() == getOldValue().hashCode()){
					createGenericMessage(KeyUtil.getMessageValue("islem.degisiklikYok"), FacesMessage.SEVERITY_INFO); 
					return;
				}
				getDBOperator().update(getGorev()); 
				createGenericMessage(KeyUtil.getMessageValue("kayit.guncellendi"), FacesMessage.SEVERITY_ERROR);
			} catch (DBException e) { 
				createGenericMessage(KeyUtil.getMessageValue("islem.hata"), FacesMessage.SEVERITY_ERROR);
				return; 
			} 
	
		} 
		insert(); 
		cancel(); 
		queryAction(); 
		return; 
	}
	
	public String tree() {	
		getSessionUser().setLegalAccess(1); 
		return ApplicationDescriptor._GOREV_TREE_OUTCOME;	
	}	
	
} // class 
