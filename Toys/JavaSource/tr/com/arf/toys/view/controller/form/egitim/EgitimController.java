package tr.com.arf.toys.view.controller.form.egitim;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.event.AjaxBehaviorEvent;

import tr.com.arf.framework.db.enumerated._common.EvetHayir;
import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.framework.db.model._base.BaseEntity;
import tr.com.arf.framework.utility.exception.data.DBException;
import tr.com.arf.framework.utility.tool.KeyUtil;
import tr.com.arf.toys.db.enumerated.egitim.Cevap;
import tr.com.arf.toys.db.enumerated.egitim.EgitimDurum;
import tr.com.arf.toys.db.enumerated.egitim.Soru;
import tr.com.arf.toys.db.filter.egitim.EgitimFilter;
import tr.com.arf.toys.db.filter.egitim.EgitimkatilimciFilter;
import tr.com.arf.toys.db.filter.egitim.EgitimogretmenFilter;
import tr.com.arf.toys.db.model.egitim.Egitim;
import tr.com.arf.toys.db.model.egitim.EgitimGunleri;
import tr.com.arf.toys.db.model.egitim.Egitimkatilimci;
import tr.com.arf.toys.db.model.egitim.Egitimogretmen;
import tr.com.arf.toys.db.model.egitim.Egitimtanim;
import tr.com.arf.toys.db.nonEntityModel.EgitimDegerlendirmeSonucNonEntity;
import tr.com.arf.toys.service.application.ApplicationDescriptor;
import tr.com.arf.toys.service.application.ManagedBeanLocator;
import tr.com.arf.toys.view.controller.form._base.ToysBaseFilteredController;

@ManagedBean
@ViewScoped
public class EgitimController extends ToysBaseFilteredController {

	private static final long serialVersionUID = 6531152165884980748L;

	private Egitim egitim;
	private EgitimFilter egitimFilter = new EgitimFilter();
	public Egitimkatilimci egitimkatilimci;
	public final EgitimkatilimciFilter egitimkatilimciFilter = new EgitimkatilimciFilter();
	public Egitimogretmen egitimogretmen;
	public final EgitimogretmenFilter egitimogretmenFilter = new EgitimogretmenFilter();
	public EgitimkatilimciController egitimkatilimciController = new EgitimkatilimciController();
	protected boolean update = true;
	private Boolean yayinda;
	int durumCheck = 0;
	private java.util.Date[] dates;

	public EgitimController() {
		super(Egitim.class);
		setDefaultValues(false);
		setOrderField("ad");
		setLoggable(false);
		setAutoCompleteSearchColumns(new String[] { "ad" });
		setTable(null);
		if (getMasterEntity() != null) {
			setMasterObjectName(ApplicationDescriptor._EGITIMTANIM_MODEL);
			setMasterOutcome(ApplicationDescriptor._EGITIMTANIM_MODEL);
			getEgitimFilter().setEgitimtanimRef(getMasterEntity());
			getEgitimFilter().createQueryCriterias();
		}
		queryAction();
	}

	public EgitimController(Object object) { 
		super(Egitim.class);   
	}
	public Egitimtanim getMasterEntity() {
		if (getEgitimFilter().getEgitimtanimRef() != null) {
			return getEgitimFilter().getEgitimtanimRef();
		} else {
			return (Egitimtanim) getObjectFromSessionFilter(ApplicationDescriptor._EGITIMTANIM_MODEL);
		}
	}

	public Egitim getEgitim() {
		egitim = (Egitim) getEntity();
		return egitim;
	}

	public void setEgitim(Egitim egitim) {
		this.egitim = egitim;
	}

	public EgitimFilter getEgitimFilter() {
		return egitimFilter;
	}

	public void setEgitimFilter(EgitimFilter egitimFilter) {
		this.egitimFilter = egitimFilter;
	}

	public EgitimkatilimciController getEgitimkatilimciController() {
		return egitimkatilimciController;
	}

	public void setEgitimkatilimciController(EgitimkatilimciController egitimkatilimciController) {
		this.egitimkatilimciController = egitimkatilimciController;
	}

	public EgitimkatilimciFilter getEgitimkatilimciFilter() {
		return egitimkatilimciFilter;
	}

	public boolean getDurumKontrolForGerceklesen() {
		if (this.egitim.getDurum() == EgitimDurum._Gerceklesen) {
			return true;
		}
		return false;
	}

	public boolean getDurumKontrolForIptalEdilen() {
		if (this.egitim.getDurum() == EgitimDurum._Iptal) {
			return true;
		}
		return false;
	}

	public Boolean getYayinda() {
		return yayinda;
	}

	public void setYayinda(Boolean yayinda) {
		this.yayinda = yayinda;
	}

	public java.util.Date[] getDates() {
		return dates;
	}

	public void setDates(java.util.Date[] dates) {
		this.dates = dates;
	}

	@Override
	public BaseFilter getFilter() {
		return getEgitimFilter();
	}

	@Override
	public void resetFilter() {
		super.resetFilterOnly();
		if (getMasterEntity() != null) {
			getEgitimFilter().setEgitimtanimRef(getMasterEntity());
		}
		queryAction();
	}

	@Override
	public void delete() {
		if (this.egitim.getDurum() == EgitimDurum._Gerceklesen) {
			createGenericMessage(KeyUtil.getMessageValue("egitim.gerceklesen"), FacesMessage.SEVERITY_ERROR);
		} else {
			super.delete();
		}
	}

	@Override
	public void insert() {
		if (this.egitim != null) {
			if (this.egitim.getYayinda() == EvetHayir._EVET) {
				setYayinda(true);
			}
		} else {
			setYayinda(false);
		}
		super.insert();
	}

	@Override
	public void update() {
		// if (this.egitim.getSehirRef()!=null){
		// changeIlce(this.egitim.getSehirRef().getRID());
		// }
		if (this.egitim.getDurum() == EgitimDurum._Gerceklesen) {
			createGenericMessage(KeyUtil.getMessageValue("egitim.gerceklesenDurum"), FacesMessage.SEVERITY_ERROR);
		} else {
			super.update();
		}
	}

	@SuppressWarnings("unused")
	@Override
	public void save() {
		BaseEntity ch = this.getEntity();
		if (((Egitim) ch).getDurum() == EgitimDurum._Gerceklesen) {
			int check = -1;
			String criter = new String("EGITIMREF = " + this.egitim.getRID().toString());
			egitimkatilimciFilter.setEgitimRef(getEgitim());
			egitimkatilimciFilter.createQueryCriterias();

			try {
				check = getDBOperator().recordCount("Egitimogretmen", criter);
			} catch (Exception e) {
				logYaz("Exception @" + getModelName() + "Controller :", e);
			}
			if (check < 1) {
				createGenericMessage(KeyUtil.getMessageValue("egitim.birOgretmen"), FacesMessage.SEVERITY_ERROR);
				this.egitim.setDurum(EgitimDurum._Planlanan);
			} else {
				try {
					check = getDBOperator().recordCount("Egitimkatilimci", criter);
				} catch (Exception e) {
					logYaz("Exception @" + getModelName() + "Controller :", e);
				}
				if (check < 1) {
					createGenericMessage(KeyUtil.getMessageValue("egitim.birKatilimci"), FacesMessage.SEVERITY_ERROR);
				} else {
					update = true;
				}
			}
		} else {
			update = true;
		}

		if (update) {
			if (yayinda) {
				getEgitim().setYayinda(EvetHayir._EVET);
			} else {
				getEgitim().setYayinda(EvetHayir._HAYIR);
			}

			if (this.getEgitim().getBaslangictarih().before(this.getEgitim().getBitistarih())) {
				// createGenericMessage(
				// KeyUtil.getMessageValue("egitim.planlananEklendi"),
				// FacesMessage.SEVERITY_INFO);
				// if (this.egitim.getSinavli() == EvetHayir._HAYIR) {
				// Date date = new Date();
				// this.egitim.setSinavtarihi(date);
				// this.egitim.setSinavyeri("");
				// HH converts hour in 24 hours format (0-23), day calculation
				SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
				String baslangictarihi = format.format(this.getEgitim().getBaslangictarih());
				String bitistarihi = format.format(this.getEgitim().getBitistarih());
				Date d2 = null;
				Date d1 = null;
				// try {
				// d1 = format.parse(baslangictarihi);
				// d2 = format.parse(bitistarihi);

				// in milliseconds
				// long diff = d2.getTime() - d1.getTime();
				//
				// // long diffSeconds = diff / 1000 % 60;
				// long diffMinutes = diff / (60 * 1000) % 60;
				// long diffHours = diff / (60 * 60 * 1000) % 24;
				// long diffDays = diff / (24 * 60 * 60 * 1000);
				//
				// //Saat cevrilme
				// double convertminutestohours=diffMinutes/60.0;
				// long saat=(long) (diffDays*24+diffHours+convertminutestohours);
				// this.egitim.setSure(new BigDecimal(saat));

				// logYaz("Toplam saat = "+saat);
				// System.out.print(diffDays + " days, ");
				// System.out.print(diffHours + " hours, ");
				// System.out.print(diffMinutes + " minutes, ");
				// System.out.print(diffSeconds + " seconds.");

				// } catch (Exception e) {
				// logYaz("Exception @" + getModelName() + "Controller :", e);
				// }
				super.save();
				// }else{
				// super.save();
				// }
				EgitimGunleri egtmGunleri;
				if (dates != null) {
					if (dates.length != 0) {
						for (java.util.Date date : dates) {
							try {
								egtmGunleri = new EgitimGunleri();
								egtmGunleri.setEgitimRef(getEgitim());
								egtmGunleri.setTarih(date);
								getDBOperator().insert(egtmGunleri);
							} catch (DBException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
					}
				}

			} else {
				createGenericMessage(KeyUtil.getMessageValue("egitim.tarihAralikKontrol"), FacesMessage.SEVERITY_ERROR);
			}
		}
		update = true;
	}

	public boolean handleSinavliChange(AjaxBehaviorEvent event) {
		try {
			HtmlSelectOneMenu menu = (HtmlSelectOneMenu) event.getComponent();
			if (menu.getValue() instanceof EvetHayir) {
				// if (this.egitim.getSinavli() == EvetHayir._EVET) {
				return true;
				// }

			} else {
				return false;
			}
		} catch (Exception e) {
			logYaz("Exception @" + getModelName() + "Controller :", e);
		}
		return true;
	}

	public String egitimkatilimci() {
		if (!setSelected()) {
			return "";
		}
		getSessionUser().setLegalAccess(1);
		putObjectToSessionFilter(ApplicationDescriptor._EGITIM_MODEL, getEgitim());
		return ManagedBeanLocator.locateMenuController().gotoPage("menu_Egitimkatilimci");
	}

	public String egitimders() {
		if (!setSelected()) {
			return "";
		}
		getSessionUser().setLegalAccess(1);
		putObjectToSessionFilter(ApplicationDescriptor._EGITIM_MODEL, getEgitim());
		return ManagedBeanLocator.locateMenuController().gotoPage("menu_Egitimders");
	}

	public String egitimogretmen() {
		if (!setSelected()) {
			return "";
		}
		getSessionUser().setLegalAccess(1);
		putObjectToSessionFilter(ApplicationDescriptor._EGITIM_MODEL, getEgitim());
		return ManagedBeanLocator.locateMenuController().gotoPage("menu_Egitimogretmen");
	}

	public String egitimdosya() {
		if (!setSelected()) {
			return "";
		}
		getSessionUser().setLegalAccess(1);
		putObjectToSessionFilter(ApplicationDescriptor._EGITIM_MODEL, getEgitim());
		return ManagedBeanLocator.locateMenuController().gotoPage("menu_Egitimdosya");
	}

	public String egitimGunleri() {
		if (!setSelected()) {
			return "";
		}
		getSessionUser().setLegalAccess(1);
		putObjectToSessionFilter(ApplicationDescriptor._EGITIM_MODEL, getEgitim());
		return ManagedBeanLocator.locateMenuController().gotoPage("menu_EgitimGunleri");
	}
	
	private Boolean degerlendirmesonuclariPanelAc=false ; 
	
	public Boolean getDegerlendirmesonuclariPanelAc() {
		return degerlendirmesonuclariPanelAc;
	}

	public void setDegerlendirmesonuclariPanelAc(
			Boolean degerlendirmesonuclariPanelAc) {
		this.degerlendirmesonuclariPanelAc = degerlendirmesonuclariPanelAc;
	}
	private static List<EgitimDegerlendirmeSonucNonEntity> degerlendirmesonuclistesi=new ArrayList<EgitimDegerlendirmeSonucNonEntity>() ;
	
	public List<EgitimDegerlendirmeSonucNonEntity> getDegerlendirmesonuclistesi() {
		return degerlendirmesonuclistesi;
	}

	@SuppressWarnings("static-access")
	public void setDegerlendirmesonuclistesi(
			List<EgitimDegerlendirmeSonucNonEntity> degerlendirmesonuclistesi) {
		this.degerlendirmesonuclistesi = degerlendirmesonuclistesi;
	}

	public void  degerlendirmesonuclari(){
		degerlendirmesonuclariPanelAc=true;
		degerlendirmesonuclistesi=new ArrayList<EgitimDegerlendirmeSonucNonEntity>() ; 
		String wherequery = "o.egitimRef.rID= " +getEgitim().getRID() ;
		EgitimDegerlendirmeSonucNonEntity sonuc;
		int evetsayisi=0,hayirsayisi=0,bossayisi=0;
		try {
			if (getDBOperator().recordCount(ApplicationDescriptor._EGITIMDEGERLENDIRME_MODEL,wherequery + 
					" AND o.cevap=" + Cevap._TIP1.getCode() + " AND o.soru =" + Soru._TIP1.getCode())>0){
				evetsayisi=getDBOperator().load(ApplicationDescriptor._EGITIMDEGERLENDIRME_MODEL,wherequery + 
						" AND o.cevap=" + Cevap._TIP1.getCode() + " AND o.soru =" + Soru._TIP1.getCode(),"").size();
			}else {
				evetsayisi=0;
			}
			if (getDBOperator().recordCount(ApplicationDescriptor._EGITIMDEGERLENDIRME_MODEL,wherequery + 
					" AND o.cevap=" + Cevap._TIP2.getCode() + " AND o.soru =" + Soru._TIP1.getCode())>0){
				hayirsayisi=getDBOperator().load(ApplicationDescriptor._EGITIMDEGERLENDIRME_MODEL,wherequery + 
						" AND o.cevap=" + Cevap._TIP2.getCode() + " AND o.soru =" + Soru._TIP1.getCode(),"").size();
			}else {
				hayirsayisi=0;
			}
			if (getDBOperator().recordCount(ApplicationDescriptor._EGITIMDEGERLENDIRME_MODEL,wherequery + 
					" AND o.cevap=" + Cevap._NULL.getCode() + " AND o.soru =" + Soru._TIP1.getCode())>0){
				bossayisi=getDBOperator().load(ApplicationDescriptor._EGITIMDEGERLENDIRME_MODEL,wherequery + 
						" AND o.cevap=" + Cevap._NULL.getCode() + " AND o.soru =" + Soru._TIP1.getCode(),"").size();
			}else {
				bossayisi=0;
			}
			sonuc = new EgitimDegerlendirmeSonucNonEntity(Soru._TIP1,evetsayisi,hayirsayisi,bossayisi);
			degerlendirmesonuclistesi.add(sonuc);
			
			if (getDBOperator().recordCount(ApplicationDescriptor._EGITIMDEGERLENDIRME_MODEL,wherequery + 
					" AND o.cevap=" + Cevap._TIP1.getCode() + " AND o.soru =" + Soru._TIP2.getCode())>0){
				evetsayisi=getDBOperator().load(ApplicationDescriptor._EGITIMDEGERLENDIRME_MODEL,wherequery + 
						" AND o.cevap=" + Cevap._TIP1.getCode() + " AND o.soru =" + Soru._TIP2.getCode(),"").size();
			}else {
				evetsayisi=0;
			}
			if (getDBOperator().recordCount(ApplicationDescriptor._EGITIMDEGERLENDIRME_MODEL,wherequery + 
					" AND o.cevap=" + Cevap._TIP2.getCode() + " AND o.soru =" + Soru._TIP2.getCode())>0){
				hayirsayisi=getDBOperator().load(ApplicationDescriptor._EGITIMDEGERLENDIRME_MODEL,wherequery + 
						" AND o.cevap=" + Cevap._TIP2.getCode() + " AND o.soru =" + Soru._TIP2.getCode(),"").size();
			}else {
				hayirsayisi=0;
			}
			if (getDBOperator().recordCount(ApplicationDescriptor._EGITIMDEGERLENDIRME_MODEL,wherequery + 
					" AND o.cevap=" + Cevap._NULL.getCode() + " AND o.soru =" + Soru._TIP2.getCode())>0){
				bossayisi=getDBOperator().load(ApplicationDescriptor._EGITIMDEGERLENDIRME_MODEL,wherequery + 
						" AND o.cevap=" + Cevap._NULL.getCode() + " AND o.soru =" + Soru._TIP2.getCode(),"").size();
			}else {
				bossayisi=0;
			}
			
			sonuc = new EgitimDegerlendirmeSonucNonEntity(Soru._TIP2,evetsayisi,hayirsayisi,bossayisi);
			degerlendirmesonuclistesi.add(sonuc);
			
			if (getDBOperator().recordCount(ApplicationDescriptor._EGITIMDEGERLENDIRME_MODEL,wherequery + 
					" AND o.cevap=" + Cevap._TIP1.getCode() + " AND o.soru =" + Soru._TIP3.getCode())>0){
				evetsayisi=getDBOperator().load(ApplicationDescriptor._EGITIMDEGERLENDIRME_MODEL,wherequery + 
						" AND o.cevap=" + Cevap._TIP1.getCode() + " AND o.soru =" + Soru._TIP3.getCode(),"").size();
			}else {
				evetsayisi=0;
			}
			if (getDBOperator().recordCount(ApplicationDescriptor._EGITIMDEGERLENDIRME_MODEL,wherequery + 
					" AND o.cevap=" + Cevap._TIP2.getCode() + " AND o.soru =" + Soru._TIP3.getCode())>0){
				hayirsayisi=getDBOperator().load(ApplicationDescriptor._EGITIMDEGERLENDIRME_MODEL,wherequery + 
						" AND o.cevap=" + Cevap._TIP2.getCode() + " AND o.soru =" + Soru._TIP3.getCode(),"").size();
			}else {
				hayirsayisi=0;
			}
			if (getDBOperator().recordCount(ApplicationDescriptor._EGITIMDEGERLENDIRME_MODEL,wherequery + 
					" AND o.cevap=" + Cevap._NULL.getCode() + " AND o.soru =" + Soru._TIP3.getCode())>0){
				bossayisi=getDBOperator().load(ApplicationDescriptor._EGITIMDEGERLENDIRME_MODEL,wherequery + 
						" AND o.cevap=" + Cevap._NULL.getCode() + " AND o.soru =" + Soru._TIP3.getCode(),"").size();
			}else {
				bossayisi=0;
			}
			
			sonuc = new EgitimDegerlendirmeSonucNonEntity(Soru._TIP3,evetsayisi,hayirsayisi,bossayisi);
			degerlendirmesonuclistesi.add(sonuc);
			
			if (getDBOperator().recordCount(ApplicationDescriptor._EGITIMDEGERLENDIRME_MODEL,wherequery + 
					" AND o.cevap=" + Cevap._TIP1.getCode() + " AND o.soru =" + Soru._TIP4.getCode())>0){
				evetsayisi=getDBOperator().load(ApplicationDescriptor._EGITIMDEGERLENDIRME_MODEL,wherequery + 
						" AND o.cevap=" + Cevap._TIP1.getCode() + " AND o.soru =" + Soru._TIP4.getCode(),"").size();
			}else {
				evetsayisi=0;
			}
			if (getDBOperator().recordCount(ApplicationDescriptor._EGITIMDEGERLENDIRME_MODEL,wherequery + 
					" AND o.cevap=" + Cevap._TIP2.getCode() + " AND o.soru =" + Soru._TIP4.getCode())>0){
				hayirsayisi=getDBOperator().load(ApplicationDescriptor._EGITIMDEGERLENDIRME_MODEL,wherequery + 
						" AND o.cevap=" + Cevap._TIP2.getCode() + " AND o.soru =" + Soru._TIP4.getCode(),"").size();
			}else {
				hayirsayisi=0;
			}
			if (getDBOperator().recordCount(ApplicationDescriptor._EGITIMDEGERLENDIRME_MODEL,wherequery + 
					" AND o.cevap=" + Cevap._NULL.getCode() + " AND o.soru =" + Soru._TIP4.getCode())>0){
				bossayisi=getDBOperator().load(ApplicationDescriptor._EGITIMDEGERLENDIRME_MODEL,wherequery + 
						" AND o.cevap=" + Cevap._NULL.getCode() + " AND o.soru =" + Soru._TIP4.getCode(),"").size();
			}else {
				bossayisi=0;
			}
			
			sonuc = new EgitimDegerlendirmeSonucNonEntity(Soru._TIP4,evetsayisi,hayirsayisi,bossayisi);
			degerlendirmesonuclistesi.add(sonuc);
			
			if (getDBOperator().recordCount(ApplicationDescriptor._EGITIMDEGERLENDIRME_MODEL,wherequery + 
					" AND o.cevap=" + Cevap._TIP1.getCode() + " AND o.soru =" + Soru._TIP5.getCode())>0){
				evetsayisi=getDBOperator().load(ApplicationDescriptor._EGITIMDEGERLENDIRME_MODEL,wherequery + 
						" AND o.cevap=" + Cevap._TIP1.getCode() + " AND o.soru =" + Soru._TIP5.getCode(),"").size();
			}else {
				evetsayisi=0;
			}
			if (getDBOperator().recordCount(ApplicationDescriptor._EGITIMDEGERLENDIRME_MODEL,wherequery + 
					" AND o.cevap=" + Cevap._TIP2.getCode() + " AND o.soru =" + Soru._TIP5.getCode())>0){
				hayirsayisi=getDBOperator().load(ApplicationDescriptor._EGITIMDEGERLENDIRME_MODEL,wherequery + 
						" AND o.cevap=" + Cevap._TIP2.getCode() + " AND o.soru =" + Soru._TIP5.getCode(),"").size();
			}else {
				hayirsayisi=0;
			}
			if (getDBOperator().recordCount(ApplicationDescriptor._EGITIMDEGERLENDIRME_MODEL,wherequery + 
					" AND o.cevap=" + Cevap._NULL.getCode() + " AND o.soru =" + Soru._TIP5.getCode())>0){
				bossayisi=getDBOperator().load(ApplicationDescriptor._EGITIMDEGERLENDIRME_MODEL,wherequery + 
						" AND o.cevap=" + Cevap._NULL.getCode() + " AND o.soru =" + Soru._TIP5.getCode(),"").size();
			}else {
				bossayisi=0;
			}
			
			sonuc = new EgitimDegerlendirmeSonucNonEntity(Soru._TIP5,evetsayisi,hayirsayisi,bossayisi);
			degerlendirmesonuclistesi.add(sonuc);
			
			if (getDBOperator().recordCount(ApplicationDescriptor._EGITIMDEGERLENDIRME_MODEL,wherequery + 
					" AND o.cevap=" + Cevap._TIP1.getCode() + " AND o.soru =" + Soru._TIP6.getCode())>0){
				evetsayisi=getDBOperator().load(ApplicationDescriptor._EGITIMDEGERLENDIRME_MODEL,wherequery + 
						" AND o.cevap=" + Cevap._TIP1.getCode() + " AND o.soru =" + Soru._TIP6.getCode(),"").size();
			}else {
				evetsayisi=0;
			}
			if (getDBOperator().recordCount(ApplicationDescriptor._EGITIMDEGERLENDIRME_MODEL,wherequery + 
					" AND o.cevap=" + Cevap._TIP2.getCode() + " AND o.soru =" + Soru._TIP6.getCode())>0){
				hayirsayisi=getDBOperator().load(ApplicationDescriptor._EGITIMDEGERLENDIRME_MODEL,wherequery + 
						" AND o.cevap=" + Cevap._TIP2.getCode() + " AND o.soru =" + Soru._TIP6.getCode(),"").size();
			}else {
				hayirsayisi=0;
			}
			if (getDBOperator().recordCount(ApplicationDescriptor._EGITIMDEGERLENDIRME_MODEL,wherequery + 
					" AND o.cevap=" + Cevap._NULL.getCode() + " AND o.soru =" + Soru._TIP6.getCode())>0){
				bossayisi=getDBOperator().load(ApplicationDescriptor._EGITIMDEGERLENDIRME_MODEL,wherequery + 
						" AND o.cevap=" + Cevap._NULL.getCode() + " AND o.soru =" + Soru._TIP6.getCode(),"").size();
			}else {
				bossayisi=0;
			}
			
			sonuc = new EgitimDegerlendirmeSonucNonEntity(Soru._TIP6,evetsayisi,hayirsayisi,bossayisi);
			degerlendirmesonuclistesi.add(sonuc);
			
			if (getDBOperator().recordCount(ApplicationDescriptor._EGITIMDEGERLENDIRME_MODEL,wherequery + 
					" AND o.cevap=" + Cevap._TIP1.getCode() + " AND o.soru =" + Soru._TIP7.getCode())>0){
				evetsayisi=getDBOperator().load(ApplicationDescriptor._EGITIMDEGERLENDIRME_MODEL,wherequery + 
						" AND o.cevap=" + Cevap._TIP1.getCode() + " AND o.soru =" + Soru._TIP7.getCode(),"").size();
			}else {
				evetsayisi=0;
			}
			if (getDBOperator().recordCount(ApplicationDescriptor._EGITIMDEGERLENDIRME_MODEL,wherequery + 
					" AND o.cevap=" + Cevap._TIP2.getCode() + " AND o.soru =" + Soru._TIP7.getCode())>0){
				hayirsayisi=getDBOperator().load(ApplicationDescriptor._EGITIMDEGERLENDIRME_MODEL,wherequery + 
						" AND o.cevap=" + Cevap._TIP2.getCode() + " AND o.soru =" + Soru._TIP7.getCode(),"").size();
			}else {
				hayirsayisi=0;
			}
			if (getDBOperator().recordCount(ApplicationDescriptor._EGITIMDEGERLENDIRME_MODEL,wherequery + 
					" AND o.cevap=" + Cevap._NULL.getCode() + " AND o.soru =" + Soru._TIP7.getCode())>0){
				bossayisi=getDBOperator().load(ApplicationDescriptor._EGITIMDEGERLENDIRME_MODEL,wherequery + 
						" AND o.cevap=" + Cevap._NULL.getCode() + " AND o.soru =" + Soru._TIP7.getCode(),"").size();
			}else {
				bossayisi=0;
			}
			
			sonuc = new EgitimDegerlendirmeSonucNonEntity(Soru._TIP7,evetsayisi,hayirsayisi,bossayisi);
			degerlendirmesonuclistesi.add(sonuc);
			
			if (getDBOperator().recordCount(ApplicationDescriptor._EGITIMDEGERLENDIRME_MODEL,wherequery + 
					" AND o.cevap=" + Cevap._TIP1.getCode() + " AND o.soru =" + Soru._TIP8.getCode())>0){
				evetsayisi=getDBOperator().load(ApplicationDescriptor._EGITIMDEGERLENDIRME_MODEL,wherequery + 
						" AND o.cevap=" + Cevap._TIP1.getCode() + " AND o.soru =" + Soru._TIP8.getCode(),"").size();
			}else {
				evetsayisi=0;
			}
			if (getDBOperator().recordCount(ApplicationDescriptor._EGITIMDEGERLENDIRME_MODEL,wherequery + 
					" AND o.cevap=" + Cevap._TIP2.getCode() + " AND o.soru =" + Soru._TIP8.getCode())>0){
				hayirsayisi=getDBOperator().load(ApplicationDescriptor._EGITIMDEGERLENDIRME_MODEL,wherequery + 
						" AND o.cevap=" + Cevap._TIP2.getCode() + " AND o.soru =" + Soru._TIP8.getCode(),"").size();
			}else {
				hayirsayisi=0;
			}
			if (getDBOperator().recordCount(ApplicationDescriptor._EGITIMDEGERLENDIRME_MODEL,wherequery + 
					" AND o.cevap=" + Cevap._NULL.getCode() + " AND o.soru =" + Soru._TIP8.getCode())>0){
				bossayisi=getDBOperator().load(ApplicationDescriptor._EGITIMDEGERLENDIRME_MODEL,wherequery + 
						" AND o.cevap=" + Cevap._NULL.getCode() + " AND o.soru =" + Soru._TIP8.getCode(),"").size();
			}else {
				bossayisi=0;
			}
			
			sonuc = new EgitimDegerlendirmeSonucNonEntity(Soru._TIP8,evetsayisi,hayirsayisi,bossayisi);
			degerlendirmesonuclistesi.add(sonuc);
			
			if (getDBOperator().recordCount(ApplicationDescriptor._EGITIMDEGERLENDIRME_MODEL,wherequery + 
					" AND o.cevap=" + Cevap._TIP1.getCode() + " AND o.soru =" + Soru._TIP9.getCode())>0){
				evetsayisi=getDBOperator().load(ApplicationDescriptor._EGITIMDEGERLENDIRME_MODEL,wherequery + 
						" AND o.cevap=" + Cevap._TIP1.getCode() + " AND o.soru =" + Soru._TIP9.getCode(),"").size();
			}else {
				evetsayisi=0;
			}
			if (getDBOperator().recordCount(ApplicationDescriptor._EGITIMDEGERLENDIRME_MODEL,wherequery + 
					" AND o.cevap=" + Cevap._TIP2.getCode() + " AND o.soru =" + Soru._TIP9.getCode())>0){
				hayirsayisi=getDBOperator().load(ApplicationDescriptor._EGITIMDEGERLENDIRME_MODEL,wherequery + 
						" AND o.cevap=" + Cevap._TIP2.getCode() + " AND o.soru =" + Soru._TIP9.getCode(),"").size();
			}else {
				hayirsayisi=0;
			}
			if (getDBOperator().recordCount(ApplicationDescriptor._EGITIMDEGERLENDIRME_MODEL,wherequery + 
					" AND o.cevap=" + Cevap._NULL.getCode() + " AND o.soru =" + Soru._TIP9.getCode())>0){
				bossayisi=getDBOperator().load(ApplicationDescriptor._EGITIMDEGERLENDIRME_MODEL,wherequery + 
						" AND o.cevap=" + Cevap._NULL.getCode() + " AND o.soru =" + Soru._TIP9.getCode(),"").size();
			}else {
				bossayisi=0;
			}
			
			sonuc = new EgitimDegerlendirmeSonucNonEntity(Soru._TIP9,evetsayisi,hayirsayisi,bossayisi);
			degerlendirmesonuclistesi.add(sonuc);
			
			if (getDBOperator().recordCount(ApplicationDescriptor._EGITIMDEGERLENDIRME_MODEL,wherequery + 
					" AND o.cevap=" + Cevap._TIP1.getCode() + " AND o.soru =" + Soru._TIP10.getCode())>0){
				evetsayisi=getDBOperator().load(ApplicationDescriptor._EGITIMDEGERLENDIRME_MODEL,wherequery + 
						" AND o.cevap=" + Cevap._TIP1.getCode() + " AND o.soru =" + Soru._TIP10.getCode(),"").size();
			}else {
				evetsayisi=0;
			}
			if (getDBOperator().recordCount(ApplicationDescriptor._EGITIMDEGERLENDIRME_MODEL,wherequery + 
					" AND o.cevap=" + Cevap._TIP2.getCode() + " AND o.soru =" + Soru._TIP10.getCode())>0){
				hayirsayisi=getDBOperator().load(ApplicationDescriptor._EGITIMDEGERLENDIRME_MODEL,wherequery + 
						" AND o.cevap=" + Cevap._TIP2.getCode() + " AND o.soru =" + Soru._TIP10.getCode(),"").size();
			}else {
				hayirsayisi=0;
			}
			if (getDBOperator().recordCount(ApplicationDescriptor._EGITIMDEGERLENDIRME_MODEL,wherequery + 
					" AND o.cevap=" + Cevap._NULL.getCode() + " AND o.soru =" + Soru._TIP10.getCode())>0){
				bossayisi=getDBOperator().load(ApplicationDescriptor._EGITIMDEGERLENDIRME_MODEL,wherequery + 
						" AND o.cevap=" + Cevap._NULL.getCode() + " AND o.soru =" + Soru._TIP10.getCode(),"").size();
			}else {
				bossayisi=0;
			}
			
			sonuc = new EgitimDegerlendirmeSonucNonEntity(Soru._TIP10,evetsayisi,hayirsayisi,bossayisi);
			degerlendirmesonuclistesi.add(sonuc);
			
			if (getDBOperator().recordCount(ApplicationDescriptor._EGITIMDEGERLENDIRME_MODEL,wherequery + 
					" AND o.cevap=" + Cevap._TIP1.getCode() + " AND o.soru =" + Soru._TIP11.getCode())>0){
				evetsayisi=getDBOperator().load(ApplicationDescriptor._EGITIMDEGERLENDIRME_MODEL,wherequery + 
						" AND o.cevap=" + Cevap._TIP1.getCode() + " AND o.soru =" + Soru._TIP11.getCode(),"").size();
			}else {
				evetsayisi=0;
			}
			if (getDBOperator().recordCount(ApplicationDescriptor._EGITIMDEGERLENDIRME_MODEL,wherequery + 
					" AND o.cevap=" + Cevap._TIP2.getCode() + " AND o.soru =" + Soru._TIP11.getCode())>0){
				hayirsayisi=getDBOperator().load(ApplicationDescriptor._EGITIMDEGERLENDIRME_MODEL,wherequery + 
						" AND o.cevap=" + Cevap._TIP2.getCode() + " AND o.soru =" + Soru._TIP11.getCode(),"").size();
			}else {
				hayirsayisi=0;
			}
			if (getDBOperator().recordCount(ApplicationDescriptor._EGITIMDEGERLENDIRME_MODEL,wherequery + 
					" AND o.cevap=" + Cevap._NULL.getCode() + " AND o.soru =" + Soru._TIP11.getCode())>0){
				bossayisi=getDBOperator().load(ApplicationDescriptor._EGITIMDEGERLENDIRME_MODEL,wherequery + 
						" AND o.cevap=" + Cevap._NULL.getCode() + " AND o.soru =" + Soru._TIP11.getCode(),"").size();
			}else {
				bossayisi=0;
			}
			
			sonuc = new EgitimDegerlendirmeSonucNonEntity(Soru._TIP11,evetsayisi,hayirsayisi,bossayisi);
			degerlendirmesonuclistesi.add(sonuc);
			
			if (getDBOperator().recordCount(ApplicationDescriptor._EGITIMDEGERLENDIRME_MODEL,wherequery + 
					" AND o.cevap=" + Cevap._TIP1.getCode() + " AND o.soru =" + Soru._TIP12.getCode())>0){
				evetsayisi=getDBOperator().load(ApplicationDescriptor._EGITIMDEGERLENDIRME_MODEL,wherequery + 
						" AND o.cevap=" + Cevap._TIP1.getCode() + " AND o.soru =" + Soru._TIP12.getCode(),"").size();
			}else {
				evetsayisi=0;
			}
			if (getDBOperator().recordCount(ApplicationDescriptor._EGITIMDEGERLENDIRME_MODEL,wherequery + 
					" AND o.cevap=" + Cevap._TIP2.getCode() + " AND o.soru =" + Soru._TIP12.getCode())>0){
				hayirsayisi=getDBOperator().load(ApplicationDescriptor._EGITIMDEGERLENDIRME_MODEL,wherequery + 
						" AND o.cevap=" + Cevap._TIP2.getCode() + " AND o.soru =" + Soru._TIP12.getCode(),"").size();
			}else {
				hayirsayisi=0;
			}
			if (getDBOperator().recordCount(ApplicationDescriptor._EGITIMDEGERLENDIRME_MODEL,wherequery + 
					" AND o.cevap=" + Cevap._NULL.getCode() + " AND o.soru =" + Soru._TIP12.getCode())>0){
				bossayisi=getDBOperator().load(ApplicationDescriptor._EGITIMDEGERLENDIRME_MODEL,wherequery + 
						" AND o.cevap=" + Cevap._NULL.getCode() + " AND o.soru =" + Soru._TIP12.getCode(),"").size();
			}else {
				bossayisi=0;
			}
			
			sonuc = new EgitimDegerlendirmeSonucNonEntity(Soru._TIP12,evetsayisi,hayirsayisi,bossayisi);
			degerlendirmesonuclistesi.add(sonuc);
			
			if (getDBOperator().recordCount(ApplicationDescriptor._EGITIMDEGERLENDIRME_MODEL,wherequery + 
					" AND o.cevap=" + Cevap._TIP1.getCode() + " AND o.soru =" + Soru._TIP13.getCode())>0){
				evetsayisi=getDBOperator().load(ApplicationDescriptor._EGITIMDEGERLENDIRME_MODEL,wherequery + 
						" AND o.cevap=" + Cevap._TIP1.getCode() + " AND o.soru =" + Soru._TIP13.getCode(),"").size();
			}else {
				evetsayisi=0;
			}
			if (getDBOperator().recordCount(ApplicationDescriptor._EGITIMDEGERLENDIRME_MODEL,wherequery + 
					" AND o.cevap=" + Cevap._TIP2.getCode() + " AND o.soru =" + Soru._TIP13.getCode())>0){
				hayirsayisi=getDBOperator().load(ApplicationDescriptor._EGITIMDEGERLENDIRME_MODEL,wherequery + 
						" AND o.cevap=" + Cevap._TIP2.getCode() + " AND o.soru =" + Soru._TIP13.getCode(),"").size();
			}else {
				hayirsayisi=0;
			}
			if (getDBOperator().recordCount(ApplicationDescriptor._EGITIMDEGERLENDIRME_MODEL,wherequery + 
					" AND o.cevap=" + Cevap._NULL.getCode() + " AND o.soru =" + Soru._TIP13.getCode())>0){
				bossayisi=getDBOperator().load(ApplicationDescriptor._EGITIMDEGERLENDIRME_MODEL,wherequery + 
						" AND o.cevap=" + Cevap._NULL.getCode() + " AND o.soru =" + Soru._TIP13.getCode(),"").size();
			}else {
				bossayisi=0;
			}
			
			sonuc = new EgitimDegerlendirmeSonucNonEntity(Soru._TIP13,evetsayisi,hayirsayisi,bossayisi);
			degerlendirmesonuclistesi.add(sonuc);
			
			if (getDBOperator().recordCount(ApplicationDescriptor._EGITIMDEGERLENDIRME_MODEL,wherequery + 
					" AND o.cevap=" + Cevap._TIP1.getCode() + " AND o.soru =" + Soru._TIP14.getCode())>0){
				evetsayisi=getDBOperator().load(ApplicationDescriptor._EGITIMDEGERLENDIRME_MODEL,wherequery + 
						" AND o.cevap=" + Cevap._TIP1.getCode() + " AND o.soru =" + Soru._TIP14.getCode(),"").size();
			}else {
				evetsayisi=0;
			}
			if (getDBOperator().recordCount(ApplicationDescriptor._EGITIMDEGERLENDIRME_MODEL,wherequery + 
					" AND o.cevap=" + Cevap._TIP2.getCode() + " AND o.soru =" + Soru._TIP14.getCode())>0){
				hayirsayisi=getDBOperator().load(ApplicationDescriptor._EGITIMDEGERLENDIRME_MODEL,wherequery + 
						" AND o.cevap=" + Cevap._TIP2.getCode() + " AND o.soru =" + Soru._TIP14.getCode(),"").size();
			}else {
				hayirsayisi=0;
			}
			if (getDBOperator().recordCount(ApplicationDescriptor._EGITIMDEGERLENDIRME_MODEL,wherequery + 
					" AND o.cevap=" + Cevap._NULL.getCode() + " AND o.soru =" + Soru._TIP14.getCode())>0){
				bossayisi=getDBOperator().load(ApplicationDescriptor._EGITIMDEGERLENDIRME_MODEL,wherequery + 
						" AND o.cevap=" + Cevap._NULL.getCode() + " AND o.soru =" + Soru._TIP14.getCode(),"").size();
			}else {
				bossayisi=0;
			}
			
			sonuc = new EgitimDegerlendirmeSonucNonEntity(Soru._TIP14,evetsayisi,hayirsayisi,bossayisi);
			degerlendirmesonuclistesi.add(sonuc);
			
			if (getDBOperator().recordCount(ApplicationDescriptor._EGITIMDEGERLENDIRME_MODEL,wherequery + 
					" AND o.cevap=" + Cevap._TIP1.getCode() + " AND o.soru =" + Soru._TIP15.getCode())>0){
				evetsayisi=getDBOperator().load(ApplicationDescriptor._EGITIMDEGERLENDIRME_MODEL,wherequery + 
						" AND o.cevap=" + Cevap._TIP1.getCode() + " AND o.soru =" + Soru._TIP15.getCode(),"").size();
			}else {
				evetsayisi=0;
			}
			if (getDBOperator().recordCount(ApplicationDescriptor._EGITIMDEGERLENDIRME_MODEL,wherequery + 
					" AND o.cevap=" + Cevap._TIP2.getCode() + " AND o.soru =" + Soru._TIP15.getCode())>0){
				hayirsayisi=getDBOperator().load(ApplicationDescriptor._EGITIMDEGERLENDIRME_MODEL,wherequery + 
						" AND o.cevap=" + Cevap._TIP2.getCode() + " AND o.soru =" + Soru._TIP15.getCode(),"").size();
			}else {
				hayirsayisi=0;
			}
			if (getDBOperator().recordCount(ApplicationDescriptor._EGITIMDEGERLENDIRME_MODEL,wherequery + 
					" AND o.cevap=" + Cevap._NULL.getCode() + " AND o.soru =" + Soru._TIP15.getCode())>0){
				bossayisi=getDBOperator().load(ApplicationDescriptor._EGITIMDEGERLENDIRME_MODEL,wherequery + 
						" AND o.cevap=" + Cevap._NULL.getCode() + " AND o.soru =" + Soru._TIP15.getCode(),"").size();
			}else {
				bossayisi=0;
			}
			
			sonuc = new EgitimDegerlendirmeSonucNonEntity(Soru._TIP15,evetsayisi,hayirsayisi,bossayisi);
			degerlendirmesonuclistesi.add(sonuc);
			
			if (getDBOperator().recordCount(ApplicationDescriptor._EGITIMDEGERLENDIRME_MODEL,wherequery + 
					" AND o.cevap=" + Cevap._TIP1.getCode() + " AND o.soru =" + Soru._TIP16.getCode())>0){
				evetsayisi=getDBOperator().load(ApplicationDescriptor._EGITIMDEGERLENDIRME_MODEL,wherequery + 
						" AND o.cevap=" + Cevap._TIP1.getCode() + " AND o.soru =" + Soru._TIP16.getCode(),"").size();
			}else {
				evetsayisi=0;
			}
			if (getDBOperator().recordCount(ApplicationDescriptor._EGITIMDEGERLENDIRME_MODEL,wherequery + 
					" AND o.cevap=" + Cevap._TIP2.getCode() + " AND o.soru =" + Soru._TIP16.getCode())>0){
				hayirsayisi=getDBOperator().load(ApplicationDescriptor._EGITIMDEGERLENDIRME_MODEL,wherequery + 
						" AND o.cevap=" + Cevap._TIP2.getCode() + " AND o.soru =" + Soru._TIP16.getCode(),"").size();
			}else {
				hayirsayisi=0;
			}
			if (getDBOperator().recordCount(ApplicationDescriptor._EGITIMDEGERLENDIRME_MODEL,wherequery + 
					" AND o.cevap=" + Cevap._NULL.getCode() + " AND o.soru =" + Soru._TIP16.getCode())>0){
				bossayisi=getDBOperator().load(ApplicationDescriptor._EGITIMDEGERLENDIRME_MODEL,wherequery + 
						" AND o.cevap=" + Cevap._NULL.getCode() + " AND o.soru =" + Soru._TIP16.getCode(),"").size();
			}else {
				bossayisi=0;
			}
			
			sonuc = new EgitimDegerlendirmeSonucNonEntity(Soru._TIP16,evetsayisi,hayirsayisi,bossayisi);
			degerlendirmesonuclistesi.add(sonuc);
			
			if (getDBOperator().recordCount(ApplicationDescriptor._EGITIMDEGERLENDIRME_MODEL,wherequery + 
					" AND o.cevap=" + Cevap._TIP1.getCode() + " AND o.soru =" + Soru._TIP17.getCode())>0){
				evetsayisi=getDBOperator().load(ApplicationDescriptor._EGITIMDEGERLENDIRME_MODEL,wherequery + 
						" AND o.cevap=" + Cevap._TIP1.getCode() + " AND o.soru =" + Soru._TIP17.getCode(),"").size();
			}else {
				evetsayisi=0;
			}
			if (getDBOperator().recordCount(ApplicationDescriptor._EGITIMDEGERLENDIRME_MODEL,wherequery + 
					" AND o.cevap=" + Cevap._TIP2.getCode() + " AND o.soru =" + Soru._TIP17.getCode())>0){
				hayirsayisi=getDBOperator().load(ApplicationDescriptor._EGITIMDEGERLENDIRME_MODEL,wherequery + 
						" AND o.cevap=" + Cevap._TIP2.getCode() + " AND o.soru =" + Soru._TIP17.getCode(),"").size();
			}else {
				hayirsayisi=0;
			}
			if (getDBOperator().recordCount(ApplicationDescriptor._EGITIMDEGERLENDIRME_MODEL,wherequery + 
					" AND o.cevap=" + Cevap._NULL.getCode() + " AND o.soru =" + Soru._TIP17.getCode())>0){
				bossayisi=getDBOperator().load(ApplicationDescriptor._EGITIMDEGERLENDIRME_MODEL,wherequery + 
						" AND o.cevap=" + Cevap._NULL.getCode() + " AND o.soru =" + Soru._TIP17.getCode(),"").size();
			}else {
				bossayisi=0;
			}
			
			sonuc = new EgitimDegerlendirmeSonucNonEntity(Soru._TIP17,evetsayisi,hayirsayisi,bossayisi);
			degerlendirmesonuclistesi.add(sonuc);
			
			if (getDBOperator().recordCount(ApplicationDescriptor._EGITIMDEGERLENDIRME_MODEL,wherequery + 
					" AND o.cevap=" + Cevap._TIP1.getCode() + " AND o.soru =" + Soru._TIP18.getCode())>0){
				evetsayisi=getDBOperator().load(ApplicationDescriptor._EGITIMDEGERLENDIRME_MODEL,wherequery + 
						" AND o.cevap=" + Cevap._TIP1.getCode() + " AND o.soru =" + Soru._TIP18.getCode(),"").size();
			}else {
				evetsayisi=0;
			}
			if (getDBOperator().recordCount(ApplicationDescriptor._EGITIMDEGERLENDIRME_MODEL,wherequery + 
					" AND o.cevap=" + Cevap._TIP2.getCode() + " AND o.soru =" + Soru._TIP18.getCode())>0){
				hayirsayisi=getDBOperator().load(ApplicationDescriptor._EGITIMDEGERLENDIRME_MODEL,wherequery + 
						" AND o.cevap=" + Cevap._TIP2.getCode() + " AND o.soru =" + Soru._TIP18.getCode(),"").size();
			}else {
				hayirsayisi=0;
			}
			if (getDBOperator().recordCount(ApplicationDescriptor._EGITIMDEGERLENDIRME_MODEL,wherequery + 
					" AND o.cevap=" + Cevap._NULL.getCode() + " AND o.soru =" + Soru._TIP18.getCode())>0){
				bossayisi=getDBOperator().load(ApplicationDescriptor._EGITIMDEGERLENDIRME_MODEL,wherequery + 
						" AND o.cevap=" + Cevap._NULL.getCode() + " AND o.soru =" + Soru._TIP18.getCode(),"").size();
			}else {
				bossayisi=0;
			}
			
			sonuc = new EgitimDegerlendirmeSonucNonEntity(Soru._TIP18,evetsayisi,hayirsayisi,bossayisi);
			degerlendirmesonuclistesi.add(sonuc);
			
			if (getDBOperator().recordCount(ApplicationDescriptor._EGITIMDEGERLENDIRME_MODEL,wherequery + 
					" AND o.cevap=" + Cevap._TIP1.getCode() + " AND o.soru =" + Soru._TIP19.getCode())>0){
				evetsayisi=getDBOperator().load(ApplicationDescriptor._EGITIMDEGERLENDIRME_MODEL,wherequery + 
						" AND o.cevap=" + Cevap._TIP1.getCode() + " AND o.soru =" + Soru._TIP19.getCode(),"").size();
			}else {
				evetsayisi=0;
			}
			if (getDBOperator().recordCount(ApplicationDescriptor._EGITIMDEGERLENDIRME_MODEL,wherequery + 
					" AND o.cevap=" + Cevap._TIP2.getCode() + " AND o.soru =" + Soru._TIP19.getCode())>0){
				hayirsayisi=getDBOperator().load(ApplicationDescriptor._EGITIMDEGERLENDIRME_MODEL,wherequery + 
						" AND o.cevap=" + Cevap._TIP2.getCode() + " AND o.soru =" + Soru._TIP19.getCode(),"").size();
			}else {
				hayirsayisi=0;
			}
			if (getDBOperator().recordCount(ApplicationDescriptor._EGITIMDEGERLENDIRME_MODEL,wherequery + 
					" AND o.cevap=" + Cevap._NULL.getCode() + " AND o.soru =" + Soru._TIP19.getCode())>0){
				bossayisi=getDBOperator().load(ApplicationDescriptor._EGITIMDEGERLENDIRME_MODEL,wherequery + 
						" AND o.cevap=" + Cevap._NULL.getCode() + " AND o.soru =" + Soru._TIP19.getCode(),"").size();
			}else {
				bossayisi=0;
			}
			
			sonuc = new EgitimDegerlendirmeSonucNonEntity(Soru._TIP19,evetsayisi,hayirsayisi,bossayisi);
			degerlendirmesonuclistesi.add(sonuc);
			
			if (getDBOperator().recordCount(ApplicationDescriptor._EGITIMDEGERLENDIRME_MODEL,wherequery + 
					" AND o.cevap=" + Cevap._TIP1.getCode() + " AND o.soru =" + Soru._TIP20.getCode())>0){
				evetsayisi=getDBOperator().load(ApplicationDescriptor._EGITIMDEGERLENDIRME_MODEL,wherequery + 
						" AND o.cevap=" + Cevap._TIP1.getCode() + " AND o.soru =" + Soru._TIP20.getCode(),"").size();
			}else {
				evetsayisi=0;
			}
			if (getDBOperator().recordCount(ApplicationDescriptor._EGITIMDEGERLENDIRME_MODEL,wherequery + 
					" AND o.cevap=" + Cevap._TIP2.getCode() + " AND o.soru =" + Soru._TIP20.getCode())>0){
				hayirsayisi=getDBOperator().load(ApplicationDescriptor._EGITIMDEGERLENDIRME_MODEL,wherequery + 
						" AND o.cevap=" + Cevap._TIP2.getCode() + " AND o.soru =" + Soru._TIP20.getCode(),"").size();
			}else {
				hayirsayisi=0;
			}
			if (getDBOperator().recordCount(ApplicationDescriptor._EGITIMDEGERLENDIRME_MODEL,wherequery + 
					" AND o.cevap=" + Cevap._NULL.getCode() + " AND o.soru =" + Soru._TIP20.getCode())>0){
				bossayisi=getDBOperator().load(ApplicationDescriptor._EGITIMDEGERLENDIRME_MODEL,wherequery + 
						" AND o.cevap=" + Cevap._NULL.getCode() + " AND o.soru =" + Soru._TIP20.getCode(),"").size();
			}else {
				bossayisi=0;
			}
			
			sonuc = new EgitimDegerlendirmeSonucNonEntity(Soru._TIP20,evetsayisi,hayirsayisi,bossayisi);
			degerlendirmesonuclistesi.add(sonuc);
			
			this.queryAction();
		} catch (DBException e) {
			e.printStackTrace();
		}
	}
	
	

} // class
