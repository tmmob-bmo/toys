package tr.com.arf.toys.view.controller.form.system; 
  
 
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.framework.db.model._base.BaseEntity;
import tr.com.arf.toys.db.filter.kisi.KurulDonemFilter;
import tr.com.arf.toys.db.model.kisi.KurulDonem;
import tr.com.arf.toys.db.model.system.Birim;
import tr.com.arf.toys.service.application.ApplicationDescriptor;
import tr.com.arf.toys.view.controller.form._base.ToysBaseFilteredController;
  
  
@ManagedBean 
@ViewScoped 
public class BirimDonemleriController extends ToysBaseFilteredController {  
  
  private static final long serialVersionUID = 6531152165884980748L; 
  
	private KurulDonem kurulDonem;   
	private KurulDonemFilter kurulDonemFilter = new KurulDonemFilter();
  
	public BirimDonemleriController() { 
		super(KurulDonem.class);  
		setDefaultValues(false);  
		setTable(null);  
		if(getMasterEntity() != null){ 
			  if(getMasterEntity().getClass() == Birim.class){
				setMasterObjectName(ApplicationDescriptor._BIRIM_MODEL);
				setMasterOutcome(ApplicationDescriptor._BIRIM_MODEL);	
				getKurulDonemFilter().setBirimRef((Birim) getMasterEntity());
				} 
			  else {
					setMasterOutcome(null);
				}
			}
		getKurulDonemFilter().createQueryCriterias();  
		queryAction(); 
	} 

	
	public BaseEntity getMasterEntity() {
		if (getKurulDonemFilter().getBirimRef() != null) {
			return getKurulDonemFilter().getBirimRef();
		} else {
			return (Birim) getObjectFromSessionFilter(ApplicationDescriptor._BIRIM_MODEL);
		}
	}
	
	public KurulDonem getKurulDonem() {
		kurulDonem=(KurulDonem) getEntity();
		return kurulDonem;
	}


	public void setKurulDonem(KurulDonem kurulDonem) {
		this.kurulDonem = kurulDonem;
	}


	public KurulDonemFilter getKurulDonemFilter() {
		return kurulDonemFilter;
	}


	public void setKurulDonemFilter(KurulDonemFilter kurulDonemFilter) {
		this.kurulDonemFilter = kurulDonemFilter;
	}


	@Override  
	public BaseFilter getFilter() {  
		return getKurulDonemFilter();  
	}  
	
	
	

} // class 
