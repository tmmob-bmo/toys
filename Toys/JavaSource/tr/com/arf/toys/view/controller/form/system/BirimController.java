package tr.com.arf.toys.view.controller.form.system;


import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.model.SelectItem;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.toys.db.enumerated.system.BirimDurum;
import tr.com.arf.toys.db.filter.system.BirimFilter;
import tr.com.arf.toys.db.model.kisi.Kurum;
import tr.com.arf.toys.db.model.system.Birim;
import tr.com.arf.toys.db.model.system.Ilce;
import tr.com.arf.toys.db.model.system.Sehir;
import tr.com.arf.toys.service.application.ApplicationDescriptor;
import tr.com.arf.toys.service.application.ManagedBeanLocator;
import tr.com.arf.toys.utility.application.ApplicationConstant;
import tr.com.arf.toys.view.controller.form._base.ToysBaseTreeController;


@ManagedBean
@ViewScoped
public class BirimController extends ToysBaseTreeController {

  private static final long serialVersionUID = 6531152165884980748L;

	private Birim birim;
	private BirimFilter birimFilter = new BirimFilter();
	private Kurum previousKurumRef;
	private Birim previousUstKayitRef;
	private Boolean birimupdate=false;

	public BirimController() {
		super(Birim.class);
		setDefaultValues(false);
		setOrderField("erisimKodu");
		setStaticWhereCondition(getSessionUser().createStaticWhereCondition("o.").replace("birimRef.", "")); // Birim tablosunda arama yapilacagindan birimRef. siliniyor.
		setAutoCompleteSearchColumns(ApplicationConstant._birimAutoCompleteSearchColumns);
		setSelectedTabName("birimListTab");
		setTable(null);
		if(checkSessionForPreSelected() != null){
			getBirimFilter().setRid(checkSessionForPreSelected().getRID());
			setEntity(checkSessionForPreSelected());
		}
		getBirimFilter().setDurum(BirimDurum._ACIK);
		queryAction();
	}

	public BirimController(Object object) {
		super(Birim.class);
		setLoggable(true);
	}

	public Birim getBirim() {
		birim = (Birim) getEntity();
		return birim;
	}

	public void setBirim(Birim birim) {
		this.birim = birim;
	}

	public BirimFilter getBirimFilter() {
		return birimFilter;
	}

	public void setBirimFilter(BirimFilter birimFilter) {
		this.birimFilter = birimFilter;
	}

	public Kurum getPreviousKurumRef() {
		return previousKurumRef;
	}

	public void setPreviousKurumRef(Kurum previousKurumRef) {
		this.previousKurumRef = previousKurumRef;
	}

	public Boolean getBirimupdate() {
		return birimupdate;
	}

	public void setBirimupdate(Boolean birimupdate) {
		this.birimupdate = birimupdate;
	}

	public Birim getPreviousUstKayitRef() {
		return previousUstKayitRef;
	}

	public void setPreviousUstKayitRef(Birim previousUstKayitRef) {
		this.previousUstKayitRef = previousUstKayitRef;
	}

	@Override
	public BaseFilter getFilter() {
		return getBirimFilter();
	}

	public String internetadres() {
		if (!setSelected()) {
			return "";
		}
		getSessionUser().setLegalAccess(1);
		removeObjectFromSessionFilter(ApplicationDescriptor._KISI_MODEL);
		removeObjectFromSessionFilter(ApplicationDescriptor._KURUM_MODEL);
		putObjectToSessionFilter(ApplicationDescriptor._BIRIM_MODEL, getBirim());
		return ManagedBeanLocator.locateMenuController().gotoPage("menu_Internetadres_Birim");
	}

	public String adres() {
		if (!setSelected()) {
			return "";
		}
		getSessionUser().setLegalAccess(1);
		removeObjectFromSessionFilter(ApplicationDescriptor._KISI_MODEL);
		removeObjectFromSessionFilter(ApplicationDescriptor._KURUM_MODEL);
		putObjectToSessionFilter(ApplicationDescriptor._BIRIM_MODEL, getBirim());
		return ManagedBeanLocator.locateMenuController().gotoPage("menu_Adres_Birim");
	}

	public String telefon() {
		if (!setSelected()) {
			return "";
		}
		getSessionUser().setLegalAccess(1);
		removeObjectFromSessionFilter(ApplicationDescriptor._KISI_MODEL);
		removeObjectFromSessionFilter(ApplicationDescriptor._KURUM_MODEL);
		putObjectToSessionFilter(ApplicationDescriptor._BIRIM_MODEL, getBirim());
		return ManagedBeanLocator.locateMenuController().gotoPage("menu_Telefon_Birim");
	}

	@Override
	public void update(){
		super.update();
		setBirimupdate(true);
		if(getBirim().getSehirRef() != null){
			changeIlceForSehir(getBirim().getSehirRef().getRID());
		}
		if (getBirim().getKurumRef()!=null) {
			setPreviousKurumRef(getBirim().getKurumRef());
		}
		if (getBirim().getUstRef()!=null){
			setPreviousUstKayitRef(getBirim().getUstRef());
		}else {
			setPreviousUstKayitRef(null);
		}
	}

	@Override
	public void editFromTree(String selectedRID) {
		if(!isEmpty(selectedRID)){
			setBirimupdate(true);
			setSelectedRID(selectedRID);
			setTable(null);
			setOldValue(getEntity().getValue());
			if(getBirim().getSehirRef() != null){
				changeIlceForSehir(getBirim().getSehirRef().getRID());
			}
			if (getBirim().getUstRef()!=null){
				setPreviousUstKayitRef(getBirim().getUstRef());
			}else {
				setPreviousUstKayitRef(null);
			}
			if (getBirim().getKurumRef()!=null) {
				setPreviousKurumRef(getBirim().getKurumRef());
			}
			setEditPanelRendered(true);
			getBirim().getAd();
			setPreviousUstRef(getBirim().getUstRef());
			return;
		}
	}

	public String birimHesap() {
		if (!setSelected()) {
			return "";
		}
		getSessionUser().setLegalAccess(1);
		putObjectToSessionFilter(ApplicationDescriptor._BIRIM_MODEL, getBirim());
		return ManagedBeanLocator.locateMenuController().gotoPage("menu_BirimHesap");
	}

	@Override
	public String detailPage(Long selectedRID) {
		if (selectedRID != null && selectedRID > 0) {
			setSelectedRID(selectedRID + "");
		} else if (getSelectedRID() == null) {
			return "";
		}
		ManagedBeanLocator.locateSessionUser().addToSessionFilters(getModelName(),	getEntity());
		return ManagedBeanLocator.locateMenuController().gotoPage("menu_" + getModelName() + "Detay");
	}

	@Override
	public void addToTree(String selectedRID) {
		super.addToTree(selectedRID);
		setBirimupdate(true);
		if(getBirim().getSehirRef() != null){
			changeIlceForSehir(getBirim().getSehirRef().getRID());
		}
		if (getBirim().getUstRef()!=null){
			setPreviousUstKayitRef(getBirim().getUstRef());
		}else {
			setPreviousUstKayitRef(null);
		}
		if (getBirim().getKurumRef()!=null) {
			setPreviousKurumRef(getBirim().getKurumRef());
		}
		setEditPanelRendered(true);
		return;
	}

	@Override
	public synchronized void save() {
		if (getBirimupdate()){
			if (getBirim().getKurumRef()==null){
				birim.setKurumRef(previousKurumRef);
			}
			if (getBirim().getUstRef()==null){
				birim.setUstRef(previousUstKayitRef);
			}
			setBirimupdate(false);
			setPreviousKurumRef(null);
			setPreviousUstKayitRef(null);
		}
		super.save();
		queryAction();
	}

	public String birimDonemleri() {
		if (!setSelected()) {
			return "";
		}
		getSessionUser().setLegalAccess(1);
		putObjectToSessionFilter(ApplicationDescriptor._BIRIM_MODEL, getBirim());
		return ManagedBeanLocator.locateMenuController().gotoPage("menu_BirimDonemleri");
	}

	@Override
		public void handleChange(AjaxBehaviorEvent event) {
			try {
				HtmlSelectOneMenu menu = (HtmlSelectOneMenu) event.getComponent();
				if (menu.getValue() instanceof Sehir) {
					changeIlceForSehir(((Sehir) menu.getValue()).getRID());
				}
			} catch (Exception e) {
				logYaz("Exception @" + getModelName() + "Controller :", e);
			}
		}

	protected void changeIlceForSehir(Long selectedRecId) {
		if (selectedRecId.longValue() == 0) {
			setIlceItemListForSehir(new SelectItem[0]);
		} else {
			@SuppressWarnings("unchecked")
			List<Ilce> entityList = getDBOperator().load(Ilce.class.getSimpleName(), "o.sehirRef.rID=" + selectedRecId, "o.ad");
			if (entityList != null) {
				ilceItemListForSehir = new SelectItem[entityList.size() + 1];
				ilceItemListForSehir[0] = new SelectItem(null, "");
				int i = 1;
				for (Ilce entity : entityList) {
					ilceItemListForSehir[i++] = new SelectItem(entity, entity.getUIString());
				}
			} else {
				setIlceItemListForSehir(new SelectItem[0]);
			}
		}
	}

	private SelectItem[] ilceItemListForSehir;

	public SelectItem[] getIlceItemListForSehir() {
		return ilceItemListForSehir;
	}

	public void setIlceItemListForSehir(SelectItem[] ilceItemListForSehir) {
		this.ilceItemListForSehir = ilceItemListForSehir;
	}



} // class
