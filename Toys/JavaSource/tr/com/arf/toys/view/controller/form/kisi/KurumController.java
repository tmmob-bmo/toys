package tr.com.arf.toys.view.controller.form.kisi;

import java.util.List;
import java.util.Locale;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.model.SelectItem;

import org.primefaces.context.RequestContext;

import tr.com.arf.framework.db.enumerated._common.EvetHayir;
import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.framework.utility.exception.data.DBException;
import tr.com.arf.framework.utility.tool.KeyUtil;
import tr.com.arf.toys.db.enumerated.iletisim.AdresTipi;
import tr.com.arf.toys.db.enumerated.iletisim.AdresTuru;
import tr.com.arf.toys.db.enumerated.iletisim.NetAdresTuru;
import tr.com.arf.toys.db.enumerated.iletisim.TelefonTuru;
import tr.com.arf.toys.db.enumerated.uye.UyeDurum;
import tr.com.arf.toys.db.enumerated.uye.UyekurumDurum;
import tr.com.arf.toys.db.filter.kisi.KurumFilter;
import tr.com.arf.toys.db.model.iletisim.Adres;
import tr.com.arf.toys.db.model.iletisim.Internetadres;
import tr.com.arf.toys.db.model.iletisim.Telefon;
import tr.com.arf.toys.db.model.kisi.IsYeriTemsilcileri;
import tr.com.arf.toys.db.model.kisi.Kisi;
import tr.com.arf.toys.db.model.kisi.Kurum;
import tr.com.arf.toys.db.model.system.Ilce;
import tr.com.arf.toys.db.model.system.KurumHesap;
import tr.com.arf.toys.db.model.system.KurumKisi;
import tr.com.arf.toys.db.model.system.Sehir;
import tr.com.arf.toys.db.model.system.Ulke;
import tr.com.arf.toys.db.model.uye.Uye;
import tr.com.arf.toys.service.application.ApplicationDescriptor;
import tr.com.arf.toys.service.application.ManagedBeanLocator;
import tr.com.arf.toys.view.controller.form._base.ToysBaseFilteredController;
import tr.com.arf.toys.view.controller.form.iletisim.AdresController;
import tr.com.arf.toys.view.controller.form.iletisim.InternetadresController;
import tr.com.arf.toys.view.controller.form.iletisim.TelefonController;
import tr.com.arf.toys.view.controller.form.system.KurumHesapController;
import tr.com.arf.toys.view.controller.form.system.KurumKisiController;

@ManagedBean
@ViewScoped
public class KurumController extends ToysBaseFilteredController {

	private static final long serialVersionUID = 6531152165884980748L;

	private Kurum kurum;
	private KurumFilter kurumFilter = new KurumFilter();

	private Kurum kurumDetay;
	private KurumHesap kurumHesap = new KurumHesap();
	private KurumKisi kurumKisi = new KurumKisi();
	private IsYeriTemsilcileri isYeriTemsilcileri = new IsYeriTemsilcileri();
	private String selectedTab;

	private String oldValueForKurumHesap;
	private String oldValueForAdres;
	private String oldValueForInternetadres;
	private String oldValueForTelefon;
	private String oldValueForKurumKisi;
	private String oldValueForIsYeriTemsilcileri;
	private String kurumkisiad=null;
	private String kurumkisisoyad=null;

	private String detailEditType;
	
	private Adres adres = new Adres();
	private Telefon telefon = new Telefon();
	private Internetadres internetadres = new Internetadres(); 
	 
	private KurumHesapController kurumHesapController = new KurumHesapController(null);	 
    private InternetadresController internetadresController = new InternetadresController(null); 
    private AdresController adresController = new AdresController(null);  
    private TelefonController telefonController = new TelefonController(null);   
	private KurumKisiController kurumKisiController = new KurumKisiController(null); 
	private IsYeriTemsilcileriController isYeriTemsilcileriController = new IsYeriTemsilcileriController(null);

	public KurumController() {
		super(Kurum.class);
		setDefaultValues(false);
		setOrderField("ad");
		setAutoCompleteSearchColumns(new String[] { "ad" });
		setTable(null);
		if(getMasterEntity() != null && getMasterEntity().getRID() != null){  
			this.kurumDetay = getMasterEntity();
			this.selectedTab = "kurum";
			createKurumDetay("kurum");
		}
		queryAction();
	}
	
	public KurumController(Object object) { 
		super(Kurum.class);   
		setLoggable(true); 
	}
	
	public Kurum getMasterEntity(){
		if(getKurum() != null && getKurum().getRID() != null){ 
			return getKurum();
		} else if(getObjectFromSessionFilter(getModelName()) != null) {
			return (Kurum) getObjectFromSessionFilter(getModelName());
		} else {
			return null;
		}			
	}

	public Kurum getKurum() {
		kurum = (Kurum) getEntity();
		return kurum;
	}
	
	public void setKurum(Kurum kurum) {
		this.kurum = kurum;
	}

	public KurumFilter getKurumFilter() {
		return kurumFilter;
	}

	public void setKurumFilter(KurumFilter kurumFilter) {
		this.kurumFilter = kurumFilter;
	}

	public KurumHesap getKurumHesap() {
		return kurumHesap;
	}

	public void setKurumHesap(KurumHesap kurumHesap) {
		this.kurumHesap = kurumHesap;
	}

	public KurumHesapController getKurumHesapController() {
		return kurumHesapController;
	}

	public void setKurumHesapController(
			KurumHesapController kurumHesapController) {
		this.kurumHesapController = kurumHesapController;
	}

	public String getSelectedTab() {
		return selectedTab;
	}

	public void setSelectedTab(String selectedTab) {
		this.selectedTab = selectedTab;
	}

	public Kurum getKurumDetay() {
		return kurumDetay;
	}

	public void setKurumDetay(Kurum kurumDetay) {
		this.kurumDetay = kurumDetay;
	}

	public String getOldValueForKurumHesap() {
		return oldValueForKurumHesap;
	}

	public void setOldValueForKurumHesap(String oldValueForKurumHesap) {
		this.oldValueForKurumHesap = oldValueForKurumHesap;
	}

	public String getDetailEditType() {
		return detailEditType;
	}

	public void setDetailEditType(String detailEditType) {
		this.detailEditType = detailEditType;
	}

	public String getOldValueForAdres() {
		return oldValueForAdres;
	}

	public void setOldValueForAdres(String oldValueForAdres) {
		this.oldValueForAdres = oldValueForAdres;
	}

	public String getOldValueForInternetadres() {
		return oldValueForInternetadres;
	}

	public void setOldValueForInternetadres(String oldValueForInternetadres) {
		this.oldValueForInternetadres = oldValueForInternetadres;
	}

	public String getOldValueForTelefon() {
		return oldValueForTelefon;
	}

	public void setOldValueForTelefon(String oldValueForTelefon) {
		this.oldValueForTelefon = oldValueForTelefon;
	}

	public Adres getAdres() {
		return adres;
	}

	public void setAdres(Adres adres) {
		this.adres = adres;
	}

	public Telefon getTelefon() {
		return telefon;
	}

	public void setTelefon(Telefon telefon) {
		this.telefon = telefon;
	}

	public Internetadres getInternetadres() {
		return internetadres;
	}

	public void setInternetadres(Internetadres internetadres) {
		this.internetadres = internetadres;
	}

	public InternetadresController getInternetadresController() {
		return internetadresController;
	}

	public void setInternetadresController(
			InternetadresController internetadresController) {
		this.internetadresController = internetadresController;
	}

	public AdresController getAdresController() {
		return adresController;
	}

	public void setAdresController(AdresController adresController) {
		this.adresController = adresController;
	}

	public TelefonController getTelefonController() {
		return telefonController;
	}

	public void setTelefonController(TelefonController telefonController) {
		this.telefonController = telefonController;
	}

	public KurumKisi getKurumKisi() {
		return kurumKisi;
	}

	public void setKurumKisi(KurumKisi kurumKisi) {
		this.kurumKisi = kurumKisi;
	}

	public String getOldValueForKurumKisi() {
		return oldValueForKurumKisi;
	}

	public void setOldValueForKurumKisi(String oldValueForKurumKisi) {
		this.oldValueForKurumKisi = oldValueForKurumKisi;
	}

	public KurumKisiController getKurumKisiController() {
		return kurumKisiController;
	}

	public void setKurumKisiController(KurumKisiController kurumKisiController) {
		this.kurumKisiController = kurumKisiController;
	}

	public IsYeriTemsilcileri getIsYeriTemsilcileri() {
		return isYeriTemsilcileri;
	}

	public void setIsYeriTemsilcileri(IsYeriTemsilcileri isYeriTemsilcileri) {
		this.isYeriTemsilcileri = isYeriTemsilcileri;
	}

	public IsYeriTemsilcileriController getIsYeriTemsilcileriController() {
		return isYeriTemsilcileriController;
	}

	public void setIsYeriTemsilcileriController(
			IsYeriTemsilcileriController isYeriTemsilcileriController) {
		this.isYeriTemsilcileriController = isYeriTemsilcileriController;
	}

	public String getOldValueForIsYeriTemsilcileri() {
		return oldValueForIsYeriTemsilcileri;
	}

	public void setOldValueForIsYeriTemsilcileri(
			String oldValueForIsYeriTemsilcileri) {
		this.oldValueForIsYeriTemsilcileri = oldValueForIsYeriTemsilcileri;
	}

	public String getKurumkisiad() {
		return kurumkisiad;
	}

	public void setKurumkisiad(String kurumkisiad) {
		this.kurumkisiad = kurumkisiad;
	}

	public String getKurumkisisoyad() {
		return kurumkisisoyad;
	}

	public void setKurumkisisoyad(String kurumkisisoyad) {
		this.kurumkisisoyad = kurumkisisoyad;
	}

	@Override
	public BaseFilter getFilter() {
		return getKurumFilter();
	}

	public String kurumHesap() {
		if (!setSelected()) {
			return "";
		}
		getSessionUser().setLegalAccess(1);
		putObjectToSessionFilter(ApplicationDescriptor._KURUM_MODEL, getKurum());
		return ManagedBeanLocator.locateMenuController().gotoPage(
				"menu_KurumHesap");
	}

	public String kurumKisi() {
		if (!setSelected()) {
			return "";
		}
		getSessionUser().setLegalAccess(1);
		putObjectToSessionFilter(ApplicationDescriptor._KURUM_MODEL, getKurum());
		return ManagedBeanLocator.locateMenuController().gotoPage(
				"menu_KurumKisi");
	}


	public String internetadres() {
		if (!setSelected()) {
			return "";
		}
		getSessionUser().setLegalAccess(1);
		removeObjectFromSessionFilter(ApplicationDescriptor._KISI_MODEL);
		removeObjectFromSessionFilter(ApplicationDescriptor._BIRIM_MODEL);
		putObjectToSessionFilter(ApplicationDescriptor._KURUM_MODEL, getKurum());
		return ManagedBeanLocator.locateMenuController().gotoPage("menu_Internetadres_Kurum");
	}

	public String adres() {
		if (!setSelected()) {
			return "";
		}
		getSessionUser().setLegalAccess(1);
		removeObjectFromSessionFilter(ApplicationDescriptor._KISI_MODEL);
		removeObjectFromSessionFilter(ApplicationDescriptor._BIRIM_MODEL);
		putObjectToSessionFilter(ApplicationDescriptor._KURUM_MODEL, getKurum());
		return ManagedBeanLocator.locateMenuController().gotoPage("menu_Adres_Kurum");
	}

	public String telefon() {
		if (!setSelected()) {
			return "";
		}
		getSessionUser().setLegalAccess(1);
		removeObjectFromSessionFilter(ApplicationDescriptor._KISI_MODEL);
		removeObjectFromSessionFilter(ApplicationDescriptor._BIRIM_MODEL);
		putObjectToSessionFilter(ApplicationDescriptor._KURUM_MODEL, getKurum());
		return ManagedBeanLocator.locateMenuController().gotoPage("menu_Telefon_Kurum");
	}

	public String isYeriTemsilcileri() {
		if (!setSelected()) {
			return "";
		}
		getSessionUser().setLegalAccess(1);
		putObjectToSessionFilter(ApplicationDescriptor._KURUM_MODEL, getKurum());
		return ManagedBeanLocator.locateMenuController().gotoPage("menu_IsYeriTemsilcileri");
	}

	
	@Override
	public String detailPage(Long selectedRID) {
		if (selectedRID != null && selectedRID > 0) {
			setSelectedRID(selectedRID + "");
		} else if (getSelectedRID() == null) {
			return "";
		}
		
		ManagedBeanLocator.locateSessionUser().addToSessionFilters(getModelName(),	getEntity());
		return ManagedBeanLocator.locateMenuController().gotoPage("menu_" + getModelName() + "Detay");
	}

	public void createKurumDetay(String tabSelected) {
		setSelectedTab(tabSelected);
		getSessionUser().setLegalAccess(1);
		
		if(tabSelected.equalsIgnoreCase("adres") || tabSelected.equalsIgnoreCase("telefon")
				|| tabSelected.equalsIgnoreCase("internetadres")){
			adresListesiGuncelle(getKurumDetay()); 
			telefonListesiGuncelle(getKurumDetay()); 
			internetadresListesiGuncelle(getKurumDetay());
		} else if (tabSelected.equalsIgnoreCase("kurumHesap")) {
			kurumHesapGuncelle(getKurumDetay());
		} else if (tabSelected.equalsIgnoreCase("kurumKisi")) {
			kurumKisiGuncelle(getKurumDetay());
		} else if (tabSelected.equalsIgnoreCase("isYeriTemsilcileri")) {
			isYeriTemsilcileriGuncelle(getKurumDetay());
		}
	}
	
	private void adresListesiGuncelle(Kurum refKurum){
		this.adresController.setTable(null);
		this.adresController.getAdresFilter().setKurumRef(refKurum);
		this.adresController.getAdresFilter().createQueryCriterias();
		this.adresController.queryAction();
	}
	
	private void telefonListesiGuncelle(Kurum refKurum){
		this.telefonController.setTable(null);
		this.telefonController.getTelefonFilter().setKurumRef(refKurum);
		this.telefonController.getTelefonFilter().createQueryCriterias();
		this.telefonController.queryAction();
	}
	
	private void internetadresListesiGuncelle(Kurum refKurum){
		this.internetadresController.setTable(null);
		this.internetadresController.getInternetadresFilter().setKurumRef(refKurum);
		this.internetadresController.getInternetadresFilter().createQueryCriterias();
		this.internetadresController.queryAction();
	}
	
	private void kurumHesapGuncelle(Kurum refKurum) {
		this.kurumHesapController.setTable(null);
		this.kurumHesapController.getKurumHesapFilter().setKurumRef(refKurum);
		this.kurumHesapController.getKurumHesapFilter().createQueryCriterias();
		this.kurumHesapController.queryAction();
	}
	
	private void kurumKisiGuncelle(Kurum refKurum) {
		this.kurumKisiController.setTable(null);
		this.kurumKisiController.getKurumKisiFilter().setKurumRef(refKurum);
		this.kurumKisiController.getKurumKisiFilter().createQueryCriterias();
		this.kurumKisiController.getKurumKisiFilter().setUyekurumDurum(UyekurumDurum._CALISIYOR);
		this.kurumKisiController.queryAction();
	}
	
	private void isYeriTemsilcileriGuncelle(Kurum refKurum) {
		this.isYeriTemsilcileriController.setTable(null);
		this.isYeriTemsilcileriController.getIsYeriTemsilcileriFilter().setKurumRef(refKurum);
		this.isYeriTemsilcileriController.getIsYeriTemsilcileriFilter().createQueryCriterias();
		this.isYeriTemsilcileriController.queryAction();
	}


	public String getHeightOfEditDialog() {
		if (this.detailEditType == null) {
			return "400";
		} else if(this.detailEditType.equalsIgnoreCase("adres")){
			return "600";
		} else if(this.detailEditType.equalsIgnoreCase("telefon")){
			return "300";
		} else if(this.detailEditType.equalsIgnoreCase("internetadres")){
			return "300";
		} else if (this.detailEditType.equalsIgnoreCase("kurumHesap")) {
			return "400";
		} else if (this.detailEditType.equalsIgnoreCase("kurumKisi")) {
			return "400";
		} else if (this.detailEditType.equalsIgnoreCase("isYeriTemsilcileri")) {
			return "400";
		} else {
			return "400";
		}
	}
	
	public void kurum() {	   
		setSelectedTab("kurum");
	}
	
	@Override
	public void save(){		
		if(getKurum().getRID() != null){
            // Guncelleme yapiliyorsa, suggestbox icin duzeltmeler yapiliyor.
            if(getKurum().getSorumluRef() == null){
                if(getKurum().getSorumluRefPrev() != null){
                	getKurum().setSorumluRef(getKurum().getSorumluRefPrev());
                }
            }
        }
		super.save(); 
	}
	
	public void saveDetails() throws Exception{ 
		RequestContext context = RequestContext.getCurrentInstance();   
		if(this.detailEditType.equalsIgnoreCase("adres")){
			if(this.adres.getAdrestipi() == AdresTipi._YURTDISIADRESI){
				if(this.adres.getYabanciulke() == null || isEmpty(this.adres.getYabanciadres()) || this.adres.getYabancisehir() == null){
					createGenericMessage("Yurdışı adresleri için ülke,şehir ve adres alanları zorunludur", FacesMessage.SEVERITY_ERROR);
					return;
				}
			} else if(this.adres.getAdrestipi() != AdresTipi._YURTDISIADRESI){
				if(isEmpty(this.adres.getAcikAdres()) || this.adres.getSehirRef() == null || this.adres.getIlceRef() == null ){
					createGenericMessage("Adres için ülke, il, ilçe, mahalle ve açık adres alanları zorunludur", FacesMessage.SEVERITY_ERROR);
					return;
				} 
				if (this.adres.getUlkeRef() == null){
					this.adres.setUlkeRef((Ulke)getDBOperator().find(Ulke.class.getSimpleName(),"o.ulkekod='9980'" ));
				}
			}	
			if(this.adres.getAdresturu() != AdresTuru._NULL){
				this.adres.setKurumRef(getKurumDetay());   
				if(this.adres.getVarsayilan() == EvetHayir._HAYIR){
					if(getDBOperator().recordCount(Adres.class.getSimpleName(), "o.kurumRef = " + getKurumDetay().getRID() + " AND o.varsayilan = " + EvetHayir._EVET.getCode()) == 0){
						this.adres.setVarsayilan(EvetHayir._EVET);
					}
				}
				super.justSave(this.adres, this.adresController.isLoggable(), this.oldValueForAdres); 
				if(this.adres.getVarsayilan() == EvetHayir._EVET){
					getDBOperator().executeQuery("UPDATE " + Adres.class.getSimpleName() + " o set o.varsayilan = " + EvetHayir._HAYIR.getCode() + " " +
							"	WHERE o.kurumRef = " + this.adres.getKurumRef().getRID() + " AND o.rID <> " + this.adres.getRID());
				} 
				this.adres = new Adres();
				this.adresController.queryAction();
				updateDialogAndForm(context);
			}
		} else if(this.detailEditType.equalsIgnoreCase("telefon")){
			if(!isEmpty(this.telefon.getTelefonno()) && this.telefon.getTelefonturu() != TelefonTuru._NULL){
				this.telefon.setKurumRef(getKurumDetay()); 
				if(this.telefon.getTelefonturu() == TelefonTuru._FAKS){
					this.telefon.setVarsayilan(EvetHayir._HAYIR);
				}
				if(this.telefon.getVarsayilan() == EvetHayir._HAYIR){
					if(getDBOperator().recordCount(Telefon.class.getSimpleName(), "o.kurumRef = " + getKurumDetay().getRID() + " AND o.varsayilan = " + EvetHayir._EVET.getCode()) == 0){
						this.telefon.setVarsayilan(EvetHayir._EVET);
					}
				}
				super.justSave(this.telefon, this.telefonController.isLoggable(), this.oldValueForTelefon);
				if(this.telefon.getVarsayilan() == EvetHayir._EVET){
					getDBOperator().executeQuery("UPDATE " + Telefon.class.getSimpleName() + " o set o.varsayilan = " + EvetHayir._HAYIR.getCode() + " " +
							"	WHERE o.kurumRef = " + this.telefon.getKurumRef().getRID() + " AND o.rID <> " + this.telefon.getRID());
				}
				this.telefon = new Telefon();
				this.telefonController.queryAction();
				updateDialogAndForm(context);
			}
		} else if(this.detailEditType.equalsIgnoreCase("internetadres")){
			if(!isEmpty(this.internetadres.getNetadresmetni()) && this.internetadres.getNetadresturu() != NetAdresTuru._NULL){
				this.internetadres.setKurumRef(getKurumDetay());
				if(this.internetadres.getNetadresturu() != NetAdresTuru._EPOSTA){
					this.internetadres.setVarsayilan(EvetHayir._HAYIR);
				}
				String mail=null;
				if(this.internetadres.getNetadresturu() == NetAdresTuru._WEBSAYFASI){
					if (internetadres.getNetadresmetni()!=null){
						mail=internetadres.getNetadresmetni();
						if (mail.contains("www.")==false){
							createCustomMessage("Geçersiz Web Sayfası !", FacesMessage.SEVERITY_ERROR, "detailEditForm:netadresmetniInputText:netadresmetni");
							return;
						}
					}	
				} else {
					if (internetadres.getNetadresmetni()!=null){
						mail=internetadres.getNetadresmetni();
						if (mail.contains("@")==false){
							createCustomMessage("Geçersiz Eposta Adresi !", FacesMessage.SEVERITY_ERROR, "detailEditForm:netadresmetniInputText:netadresmetni");
							return;
						}
					}
				}
				if(this.internetadres.getVarsayilan() == EvetHayir._HAYIR){
					if(getDBOperator().recordCount(Internetadres.class.getSimpleName(), "o.kisiRef = " + getKurumDetay().getRID() + " AND o.varsayilan = " + EvetHayir._EVET.getCode()) == 0){
						this.internetadres.setVarsayilan(EvetHayir._EVET);
					}
				}
				super.justSave(this.internetadres, this.internetadresController.isLoggable(), this.oldValueForInternetadres);
				if(this.internetadres.getVarsayilan() == EvetHayir._EVET){
					getDBOperator().executeQuery("UPDATE " + Internetadres.class.getSimpleName() + " o set o.varsayilan = " + EvetHayir._HAYIR.getCode() + " " +
							"	WHERE o.kurumRef = " + this.internetadres.getKurumRef().getRID() + " AND o.rID <> " + this.internetadres.getRID());
				}
				this.internetadres = new Internetadres(); 
				this.internetadresController.queryAction();
				updateDialogAndForm(context);
			}
		} else if(this.detailEditType.equalsIgnoreCase("kurumHesap")){
			this.kurumHesap.setKurumRef(getKurumDetay());
			super.justSave(this.kurumHesap, this.kurumHesapController.isLoggable(), this.oldValueForKurumHesap);
			
			
			if(this.kurumHesap.getGecerli() == EvetHayir._EVET){
				String whereCondition = "o.rID <> " + this.kurumHesap.getRID() + " AND o.gecerli =" + EvetHayir._EVET.getCode();
				if(this.kurumHesap.getKurumRef() != null) {
					whereCondition += " AND o.kurumRef.rID = " + this.kurumHesap.getKurumRef().getRID();
				} 
				try {
						if(getDBOperator().recordCount(KurumHesap.class.getSimpleName(), whereCondition) > 0){
							for(Object obj : getDBOperator().load(KurumHesap.class.getSimpleName(), whereCondition, "o.rID")){
								KurumHesap kurumHesap1 = (KurumHesap) obj;
								kurumHesap1.setGecerli(EvetHayir._HAYIR);
								getDBOperator().update(kurumHesap1);
							}					
						}
				} catch (Exception e) { 
					logYaz("Exception @" + getModelName() + "Controller :", e);
				} 
			} else if (this.kurumHesap.getGecerli() == EvetHayir._HAYIR){
				String whereCondition = "o.rID <> " + this.kurumHesap.getRID() + " AND o.gecerli =" + EvetHayir._HAYIR.getCode();
				if(this.kurumHesap.getKurumRef() != null) {
					whereCondition += " AND o.kurumRef.rID = " + this.kurumHesap.getKurumRef().getRID();
				} 
				String whereCondition1 = "o.rID <> " + this.kurumHesap.getRID() + " AND o.gecerli =" + EvetHayir._EVET.getCode();
				if(this.kurumHesap.getKurumRef() != null) {
					whereCondition += " AND o.kurumRef.rID = " + this.kurumHesap.getKurumRef().getRID();
				} 
				try {
					if(getDBOperator().recordCount(KurumHesap.class.getSimpleName(), whereCondition) == 0 && getDBOperator().recordCount(KurumHesap.class.getSimpleName(), whereCondition1) == 0){
						kurumHesap.setGecerli(EvetHayir._EVET);
						getDBOperator().update(kurumHesap);
					}	
					whereCondition = "o.rID <> " + this.kurumHesap.getRID() + " AND o.gecerli =" + EvetHayir._EVET.getCode();
					if(this.kurumHesap.getKurumRef() != null) {
						whereCondition += " AND o.kurumRef.rID = " + this.kurumHesap.getKurumRef().getRID();
					} 
					if(getDBOperator().recordCount(KurumHesap.class.getSimpleName(), whereCondition) == 0){ 
						createGenericMessage(KeyUtil.getMessageValue("gecerli.hata"), FacesMessage.SEVERITY_INFO);  
						kurumHesap.setGecerli(EvetHayir._EVET);
						getDBOperator().update(kurumHesap);
					}
					
				}catch (Exception e) { 
					logYaz("Exception @" + getModelName() + "Controller :", e);
				} 
				queryAction();
			}
			
			this.kurumHesap = new KurumHesap();
			this.kurumHesap.setKurumRef(getKurumDetay());
			this.kurumHesapController.queryAction(); 
			updateDialogAndForm(context); 
		} else if(this.detailEditType.equalsIgnoreCase("kurumKisi")){
			this.kurumKisi.setKurumRef(getKurumDetay());
			super.justSave(this.kurumKisi, this.kurumKisiController.isLoggable(), this.oldValueForKurumKisi);
			this.kurumKisi = new KurumKisi(); 
			this.kurumKisiController.queryAction(); 
			updateDialogAndForm(context); 
		} else if(this.detailEditType.equalsIgnoreCase("isYeriTemsilcileri")){
			this.isYeriTemsilcileri.setKurumRef(getKurumDetay());
			super.justSave(this.isYeriTemsilcileri, this.isYeriTemsilcileriController.isLoggable(), this.oldValueForIsYeriTemsilcileri);
			this.isYeriTemsilcileri = new IsYeriTemsilcileri(); 
			this.isYeriTemsilcileriController.queryAction(); 
			updateDialogAndForm(context); 
		}  
		
		context.update("kurumPanel");
		this.detailEditType = null;
		this.kurumHesapController.queryAction();
		this.kurumKisiController.queryAction();
		this.isYeriTemsilcileriController.queryAction();
	}
	
	
	private void updateDialogAndForm(RequestContext context){
		context.update("dialogUpdateBox"); 
		context.update("kurumForm");
	}
	
	@Override
	public void newFilter(){ 
		super.newFilter();
	}
	
	public void newDetail(String detailType) {  
		detailType = detailType.toLowerCase(Locale.ENGLISH);
		this.detailEditType = detailType; 
		RequestContext context = RequestContext.getCurrentInstance();
		context.update("dialogUpdateBox");
		 if(detailType.equalsIgnoreCase("adres")){
	    	this.adres = new Adres();
	    	this.adres.setAdrestipi(AdresTipi._ILILCEMERKEZI);
	    	this.adres.setVarsayilan(EvetHayir._EVET);
	    	this.adres.setAdresturu(AdresTuru._ISADRESI);
	    	this.oldValueForAdres = "";
		 } else if(detailType.equalsIgnoreCase("telefon")){
	    	this.telefon = new Telefon();
	    	this.telefon.setVarsayilan(EvetHayir._HAYIR);
	    	this.oldValueForTelefon = "";
	     } else if(detailType.equalsIgnoreCase("internetadres")){
	    	this.internetadres = new Internetadres();
	    	this.internetadres.setVarsayilan(EvetHayir._HAYIR);
	    	this.oldValueForInternetadres = "";
	     } else if (detailType.equalsIgnoreCase("kurumhesap")) {
    	 	this.kurumHesap = new KurumHesap();
    	 	this.kurumHesap.setKurumRef(getKurumDetay());
    	 	this.oldValueForKurumHesap = "";
	     } else if (detailType.equalsIgnoreCase("kurumkisi")) {
    	 	this.kurumKisi = new KurumKisi();
    	 	this.oldValueForKurumKisi = "";
	     } else if (detailType.equalsIgnoreCase("isyeritemsilcileri")) {
    	 	this.isYeriTemsilcileri = new IsYeriTemsilcileri();
    	 	this.isYeriTemsilcileri.setKurumRef(kurumDetay);
    	 	this.oldValueForIsYeriTemsilcileri = "";
	     }
	}
	
	public void updateDetail(String detailType, Object entity){
		detailType = detailType.toLowerCase(Locale.ENGLISH);
		this.detailEditType = detailType;   
		RequestContext context = RequestContext.getCurrentInstance();  
	    context.update("dialogUpdateBox");
	    
	    if(detailType.equalsIgnoreCase("adres")){
	    	this.adres = (Adres) entity;
	    	if (this.adres.getUlkeRef()!=null){
	    		changeIl(this.adres.getUlkeRef().getRID());
	    	}
	    	if (this.adres.getUlkeRef()!=null){
	    		changeIlce(this.adres.getSehirRef().getRID());
	    	}
	    	this.oldValueForAdres = this.adres.getValue();
	    } else if(detailType.equalsIgnoreCase("telefon")){
	    	this.telefon = (Telefon) entity;
	    	this.oldValueForTelefon = this.telefon.getValue();
	    } else if(detailType.equalsIgnoreCase("internetadres")){
	    	this.internetadres = (Internetadres) entity;
	    	this.oldValueForInternetadres = this.internetadres.getValue();
	    } else if (detailType.equalsIgnoreCase("kurumhesap")) {
			this.kurumHesap= (KurumHesap) entity;
			this.kurumHesap.setKurumRef(getKurumDetay());
			this.oldValueForKurumHesap = this.kurumHesap.getValue();
		} else if (detailType.equalsIgnoreCase("kurumkisi")) {
			this.kurumKisi= (KurumKisi) entity;
			this.oldValueForKurumKisi = this.kurumKisi.getValue();
		} else if (detailType.equalsIgnoreCase("isyeritemsilcileri")) {
			this.isYeriTemsilcileri= (IsYeriTemsilcileri) entity;
			this.oldValueForIsYeriTemsilcileri = this.isYeriTemsilcileri.getValue();
		}
	} 
	
	public void openDetail(Object entitySent){ 
		if(entitySent != null){   
			putObjectToSessionFilter("logEntity", entitySent); 	
		}  
	}
	
	public void deleteDetail(String detailType, Object entity) throws Exception{ 
		this.detailEditType = null;  
		detailType = detailType.toLowerCase(Locale.ENGLISH);
		
		if(detailType.equalsIgnoreCase("adres")){
	    	this.adres = (Adres) entity;
	    	if(this.adres.getVarsayilan() == EvetHayir._EVET){ 
	    		if(getDBOperator().recordCount(Adres.class.getSimpleName(), "o.rID <> " + this.adres.getRID() + " AND o.kurumRef.rID = " + this.adres.getKurumRef().getRID()) > 0){	    			 
	    			Adres varsayilanYapilacakAdres = (Adres) getDBOperator().load(Adres.class.getSimpleName(), 
	    					"o.rID <> " + this.adres.getRID() + " AND o.kurumRef.rID = " + this.adres.getKurumRef().getRID(), "o.rID DESC").get(0);
	    			varsayilanYapilacakAdres.setVarsayilan(EvetHayir._EVET);
	    			getDBOperator().update(varsayilanYapilacakAdres);
	    		} 
			} 
	    	super.justDelete(this.adres, this.adresController.isLoggable());
	    	this.adresController.queryAction();
	    } else if(detailType.equalsIgnoreCase("telefon")){
	    	this.telefon = (Telefon) entity;
	    	if(this.telefon.getVarsayilan() == EvetHayir._EVET){ 
	    		if(getDBOperator().recordCount(Telefon.class.getSimpleName(), "o.rID <> " + this.telefon.getRID() + " AND o.kurumRef.rID = " + this.telefon.getKurumRef().getRID()) > 0){	    			 
	    			Telefon varsayilanYapilacakTelefon = (Telefon) getDBOperator().load(Telefon.class.getSimpleName(), 
	    					"o.rID <> " + this.telefon.getRID() + " AND o.kurumRef.rID = " + this.telefon.getKurumRef().getRID(), "o.rID DESC").get(0);
	    			varsayilanYapilacakTelefon.setVarsayilan(EvetHayir._EVET);
	    			getDBOperator().update(varsayilanYapilacakTelefon);
	    		} 
			} 
	    	super.justDelete(this.telefon, this.telefonController.isLoggable());
	    	this.telefonController.queryAction();
	    } else if(detailType.equalsIgnoreCase("internetadres")){
	    	this.internetadres = (Internetadres) entity;
	    	if(this.internetadres.getVarsayilan() == EvetHayir._EVET){ 
	    		if(getDBOperator().recordCount(Internetadres.class.getSimpleName(),"o.rID <> " + this.internetadres.getRID() + " AND o.kurumRef.rID = " + this.internetadres.getKurumRef().getRID()) > 0){	    			 
	    			Internetadres varsayilanYapilacakInternetadres = (Internetadres) getDBOperator().load(Internetadres.class.getSimpleName(), 
	    					"o.rID <> " + this.internetadres.getRID() + " AND o.kurumRef.rID = " + this.internetadres.getKurumRef().getRID(), "o.rID DESC").get(0);
	    			varsayilanYapilacakInternetadres.setVarsayilan(EvetHayir._EVET);
	    			getDBOperator().update(varsayilanYapilacakInternetadres);
	    		} 
			} 
	    	super.justDelete(this.internetadres, this.internetadresController.isLoggable());
	    	this.internetadresController.queryAction();
	    } else if (detailType.equalsIgnoreCase("kurumHesap")) {
			this.kurumHesap = (KurumHesap) entity;
//			super.justDelete(this.kurumHesap, this.kurumHesapController.isLoggable());
			
			Long kurumRef = 0L; 
			if(getKurumHesap().getKurumRef() != null){
				kurumRef = this.kurumHesap.getKurumRef().getRID();
			} 	
			try {
				super.justDelete(this.kurumHesap, this.kurumHesapController.isLoggable());
				setSelection(null); 
				setTable(null);
				createGenericMessage(KeyUtil.getMessageValue("kayit.silindi"), FacesMessage.SEVERITY_INFO); 
			} catch (Exception e) {
				createGenericMessage(KeyUtil.getMessageValue("islem.hata") + " Mes : " + e.getMessage(), FacesMessage.SEVERITY_ERROR);  
				return;
			} 
			String whereCondition ="o.gecerli =" + EvetHayir._EVET.getCode() + "AND o.kurumRef.rID=" + kurumRef ;
			try {
				if(getDBOperator().recordCount(KurumHesap.class.getSimpleName(), whereCondition) == 0){
					whereCondition = "o.rID <> " + this.kurumHesap.getRID() + " AND o.gecerli =" + EvetHayir._HAYIR.getCode();
					if(kurumRef != 0) {
						whereCondition += " AND o.kurumRef.rID = " + kurumRef;
					} 
					if(getDBOperator().recordCount(KurumHesap.class.getSimpleName(), whereCondition)> 0){
						KurumHesap kurumHesap1 = (KurumHesap) getDBOperator().load(KurumHesap.class.getSimpleName(), whereCondition, "o.rID DESC").get(0);
						if (kurumHesap1!=null){
							kurumHesap1.setGecerli(EvetHayir._EVET);
							getDBOperator().update(kurumHesap1);
						}
					}
				}	 
			}catch (Exception e) { 
				logYaz("Exception @" + getModelName() + "Controller :", e);
			} 
			
	    	this.kurumHesapController.queryAction();
		} else if (detailType.equalsIgnoreCase("kurumKisi")) {
			this.kurumKisi = (KurumKisi) entity;
			super.justDelete(this.kurumKisi, this.kurumKisiController.isLoggable());
	    	this.kurumKisiController.queryAction();
		} else if (detailType.equalsIgnoreCase("isYeriTemsilcileri")) {
			this.isYeriTemsilcileri = (IsYeriTemsilcileri) entity;
			super.justDelete(this.isYeriTemsilcileri, this.isYeriTemsilcileriController.isLoggable());
	    	this.isYeriTemsilcileriController.queryAction();
		} 
	} 
	
	@Override
	public void update() {
		super.update();
		if(getKurum().getSorumluRef() != null) {
			getKurum().setSorumluRefPrev(getKurum().getSorumluRef());
		}
	} 
	
	public String wizardPage() {	 
		getSessionUser().setLegalAccess(1);	 
		removeObjectFromSessionFilter(ApplicationDescriptor._KURUM_MODEL);
		return "kurum_Wizard.do";		
	} 
	
	public void deletePreviousSorumlu(){
		getKurum().setSorumluRef(null);
		getKurum().setSorumluRefPrev(null);
	}
	

	public SelectItem[] uyeItemList;

	@SuppressWarnings("unchecked")
	public SelectItem[] getUyeItemList() {
		int i = 0;
		List<Uye> uyeListesi = getDBOperator().load(Uye.class.getSimpleName(), "o.uyedurum = " + UyeDurum._AKTIFUYE.getCode() + " AND o.kisiRef.rID IN" +
				"( SELECT m.kisiRef.rID FROM KurumKisi m WHERE m.kurumRef.rID = " + getKurumDetay().getRID() + ")", "o.rID"); 
		if (!isEmpty(uyeListesi)){
			uyeItemList = new SelectItem[uyeListesi.size() + 1];
			uyeItemList[0] = new SelectItem(null, "");
			for (Uye uye :  uyeListesi) { 
				uyeItemList[i] = new SelectItem(uye, uye.getUIString());					 
			}
		} else {
			setUyeItemList(new SelectItem[0]);
		}
				 
		return uyeItemList;
	}

	public void setUyeItemList(SelectItem[] uyeItemList) {
		this.uyeItemList = uyeItemList;
	} 
	
	@SuppressWarnings("unchecked")
	public String detailUyePage() {
		
		List<KurumKisi> kurumKisiListesi = getDBOperator().load(KurumKisi.class.getSimpleName(), "o.kurumRef.rID=" + kurumDetay.getRID() ,"o.rID"); 
		if (!isEmpty(kurumKisiListesi)){
			for (KurumKisi kurumKisi : kurumKisiListesi) {
				Uye uye = (Uye) getDBOperator().load(Uye.class.getSimpleName(),"o.kisiRef.rID=" + kurumKisi.getKisiRef().getRID(), "o.rID").get(0);
				if(uye != null){
					ManagedBeanLocator.locateSessionUser().addToSessionFilters(
							Uye.class.getSimpleName(), uye);
					return ManagedBeanLocator.locateMenuController().gotoPage(
							"menu_" + Uye.class.getSimpleName() + "Detay");
					
				}
			
			}
		}
		return "";
	}
	 
	public String uyeDetay(KurumKisi kurumKisi) throws DBException {		
		if (kurumKisi != null){
			Kisi kisi = kurumKisi.getKisiRef(); 
			if(getDBOperator().recordCount(Uye.class.getSimpleName(), "o.kisiRef.rID=" + kisi.getRID()) > 0){
				Uye uye = (Uye) getDBOperator().load(Uye.class.getSimpleName(), "o.kisiRef.rID=" + kisi.getRID(), "o.rID").get(0);
				getSessionUser().addToSessionFilters(Uye.class.getSimpleName(), uye);
				return ManagedBeanLocator.locateMenuController().gotoPage("menu_" + Uye.class.getSimpleName() + "Detay");
			} else {
				createGenericMessage("Kişi üye değil!", FacesMessage.SEVERITY_WARN);
				return "";
			} 
		}
		return "";
	}
	
	public String kisiDetay(KurumKisi kurumKisi) throws DBException {		
		if (kurumKisi != null){
			Kisi kisi = kurumKisi.getKisiRef(); 
			getSessionUser().addToSessionFilters(Kisi.class.getSimpleName(), kisi);
			return ManagedBeanLocator.locateMenuController().gotoPage("menu_" + Kisi.class.getSimpleName() + "Detay");
		} 
		return "";
	}
	
	public void handleChangeAdres(AjaxBehaviorEvent event) {
		try { 
			HtmlSelectOneMenu menu = (HtmlSelectOneMenu) event.getComponent(); 			
			if(menu.getValue() instanceof Sehir){ 
				changeIlce(((Sehir) menu.getValue()).getRID());
			}  else if(menu.getValue() instanceof Ulke){ 
				changeIl(((Ulke) menu.getValue()).getRID());
			}  else if(menu.getValue() instanceof AdresTipi){ 
				changeUlke((AdresTipi) menu.getValue());
			}
			 
		} catch(Exception e){
			logYaz("Error @handleChange :" + e.getMessage());
		}
	}  
	
	@Override
	protected void changeIlce(Long selectedRecId){  
		if(selectedRecId.longValue() == 0){ 
			setIlceItemList(new SelectItem[0]);
		} else {  
			@SuppressWarnings("unchecked")
			List<Ilce> entityList = getDBOperator().load(Ilce.class.getSimpleName(), "o.sehirRef.rID=" + selectedRecId ,"o.ad"); 
			if(entityList != null){	
				ilceItemList = new SelectItem[entityList.size() + 1];
				ilceItemList[0] = new SelectItem(null, "");
				int i = 1;
				for(Ilce entity : entityList) {
					ilceItemList[i++] = new SelectItem(entity, entity.getUIString());
				} 
			} else {
				setIlceItemList(new SelectItem[0]);
			}
		} 
	}
	
	
	@Override
	protected void changeUlke(AdresTipi adresTipi){  
		if(adresTipi == null || adresTipi == AdresTipi._NULL){ 
			setUlkeItemList(new SelectItem[0]);
		} else {  
			String whereCon = "";
			if(adresTipi == AdresTipi._YURTDISIADRESI){
				whereCon = "o.rID > " + 1;			
			} else {
				whereCon = "o.rID = " + 1;
			} 
			@SuppressWarnings("unchecked")
			List<Ulke> entityList = getDBOperator().load(Ulke.class.getSimpleName(), whereCon,"o.ad"); 
			if(entityList != null){	
				ulkeItemList = new SelectItem[entityList.size() + 1];
				ulkeItemList[0] = new SelectItem(null, "");
				int i = 1;
				for(Ulke entity : entityList) {
					ulkeItemList[i++] = new SelectItem(entity, entity.getUIString());
				} 
			} else {
				setUlkeItemList(new SelectItem[0]);
			}
		} 
	} 

	@Override
	protected void changeIl(Long selectedRecId){  
		if(selectedRecId.longValue() == 0){ 
			setIlceItemList(new SelectItem[0]);
		} else {  
			@SuppressWarnings("unchecked")
//			List<Sehir> entityList = getDBOperator().load(Sehir.class.getSimpleName(), "o.ulkeRef.rID=" + selectedRecId ,"o.ad"); 
			List<Sehir> entityList = getDBOperator().load(Sehir.class.getSimpleName(), "","o.ad"); 
			if(entityList != null){	
				ilItemList = new SelectItem[entityList.size() + 1];
				ilItemList[0] = new SelectItem(null, "");
				int i = 1;
				for(Sehir entity : entityList) {
					ilItemList[i++] = new SelectItem(entity, entity.getUIString());
				} 
			} else {
				setIlItemList(new SelectItem[0]);
			}
		} 
	}
	
	public void kurumKisiSorgula(){
 		if (getKurumDetay()==null){
			return ;
		}
		RequestContext context = RequestContext.getCurrentInstance();
			if (getKurumkisiad()!=null || getKurumkisisoyad()!=null){
				this.kurumKisiController.getKurumKisiFilter().clearQueryCriterias();
				this.kurumKisiController.setTable(null);
				this.kurumKisiController.getKurumKisiFilter().setKurumRef(getKurumDetay());
				this.kurumKisiController.getKurumKisiFilter().setAd(getKurumkisiad());
				this.kurumKisiController.getKurumKisiFilter().setSoyad(getKurumkisisoyad());
				this.kurumKisiController.getKurumKisiFilter().createQueryCriterias();
				this.kurumKisiController.queryAction();
				updateDialogAndForm(context);
			}
	}
	
	@SuppressWarnings("unchecked")
	public List<Uye> completeUyeFromKurumKisi(String value) {
		String[] values = new String[getSessionUser().getAutoCompleteSearchColumns(Uye.class.getSimpleName()).length];
		for(int x = 0; x < getSessionUser().getAutoCompleteSearchColumns(Uye.class.getSimpleName()).length; x++){
			values[x] = value;
		} 
		try {
			if (getDBOperator().recordCount(Uye.class.getSimpleName(),"o.kisiRef.rID IN ( SELECT m.kisiRef.rID FROM  " + KurumKisi.class.getSimpleName() +" AS m  " 
					+ " WHERE m.kurumRef.rID=" + getKurumDetay().getRID() + "AND o.kisiRef.aktif=" + EvetHayir._EVET.getCode() + " AND (UPPER(o.kisiRef.ad) LIKE UPPER('%" + value + "%') OR" +
					" UPPER(o.kisiRef.soyad) LIKE UPPER ('%" + value + "%'))  )")>0){
				List <Uye> uyeList=getDBOperator().load(Uye.class.getSimpleName(),"o.kisiRef.rID IN ( SELECT m.kisiRef.rID FROM  " + KurumKisi.class.getSimpleName() +" AS m  " 
				+ " WHERE m.kurumRef.rID=" + getKurumDetay().getRID() + "AND o.kisiRef.aktif=" + EvetHayir._EVET.getCode() + " AND (UPPER(o.kisiRef.ad) LIKE UPPER('%" + value + "%') OR" +
						" UPPER(o.kisiRef.soyad) LIKE UPPER ('%" + value + "%'))  )" ,"o.rID");
				return uyeList;
			}
		} catch (DBException e) {
			logYaz("KurumController @completeUyeKurumKisi "+e.getMessage());
			logYaz("Exception @" + getModelName() + "Controller :", e);
		}
		
		return null; 
	}
	
	@SuppressWarnings("unchecked")
	public List<Kisi> completeKisiFromKurumKisi(String value) {
		if (getKurum()==null){
			return null;
		}
		else {
			String[] values = new String[getSessionUser().getAutoCompleteSearchColumns(Uye.class.getSimpleName()).length];
			for(int x = 0; x < getSessionUser().getAutoCompleteSearchColumns(Uye.class.getSimpleName()).length; x++){
				values[x] = value;
			} 
			
			try {
				if (getDBOperator().recordCount(Kisi.class.getSimpleName(),"o.rID IN ( SELECT m.kisiRef.rID FROM  " + KurumKisi.class.getSimpleName() +" AS m  " 
						+ " WHERE m.kurumRef.rID=" + getKurum().getRID() + " AND o.aktif=" + EvetHayir._EVET.getCode() + " AND (UPPER(o.ad) LIKE UPPER('%" + value + "%') OR" +
						" UPPER(o.soyad) LIKE UPPER ('%" + value + "%'))  )")>0){
					
					List <Kisi> kisiList=getDBOperator().load(Kisi.class.getSimpleName(),"o.rID IN ( SELECT m.kisiRef.rID FROM  " + KurumKisi.class.getSimpleName() +" AS m  " 
							+ " WHERE m.kurumRef.rID=" + getKurum().getRID() + " AND o.aktif=" + EvetHayir._EVET.getCode() + " AND (UPPER(o.ad) LIKE UPPER('%" + value + "%') OR" +
							" UPPER(o.soyad) LIKE UPPER ('%" + value + "%'))  )" ,"o.rID");
					return kisiList;
				}
			} catch (DBException e) {
				logYaz("KurumController @completeKisiKurumKisi "+e.getMessage());
				logYaz("Exception @" + getModelName() + "Controller :", e);
			}
		}
		
		
		return null; 
	}
	
	@SuppressWarnings("unchecked")
	public boolean  uyemi(KurumKisi k) {
		 if (k!=null) {
			List<Uye> uyeListesi=getDBOperator().load(Uye.class.getSimpleName(), "o.kisiRef.rID=" + k.getKisiRef().getRID(), "");
			if (!isEmpty(uyeListesi)){
				return true ;
			}
		}
		return false;
	}
	

} // class 
