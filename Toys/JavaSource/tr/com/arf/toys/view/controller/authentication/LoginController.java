package tr.com.arf.toys.view.controller.authentication;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.faces.validator.ValidatorException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.jsoup.helper.StringUtil;

import tr.com.arf.framework.db._management.DBOperator;
import tr.com.arf.framework.db.enumerated._common.EvetHayir;
import tr.com.arf.framework.utility.exception.data.SessionException;
import tr.com.arf.framework.utility.security.PasswordFormatException;
import tr.com.arf.framework.utility.security.ScryptPasswordHashing;
import tr.com.arf.framework.utility.tool.DateUtil;
import tr.com.arf.framework.utility.tool.KeyUtil;
import tr.com.arf.framework.utility.tool.QueryUtil;
import tr.com.arf.toys.db.enumerated.system.PersonelDurum;
import tr.com.arf.toys.db.model.system.IslemKullanici;
import tr.com.arf.toys.db.model.system.Kullanici;
import tr.com.arf.toys.db.model.system.KullaniciRole;
import tr.com.arf.toys.db.model.system.Menuitem;
import tr.com.arf.toys.db.model.system.Personel;
import tr.com.arf.toys.db.model.uye.Uye;
import tr.com.arf.toys.service.application.ApplicationDescriptor;
import tr.com.arf.toys.service.application.ManagedBeanLocator;
import tr.com.arf.toys.utility.application.NavigationConstant;
import tr.com.arf.toys.utility.logUtils.LogService;
import tr.com.arf.toys.view.controller._common.SessionUser;
import tr.com.arf.toys.view.controller.form.uye.UyeController;

@ManagedBean
@SessionScoped
public final class LoginController {

	/* User Login Attributes */
	private String userName = "";
	private String password = null;
	private String role = "";

	private boolean comboDisabled = false;

	private BigDecimal selectedRoleId;
	private KullaniciRole selectedRole;
	private byte attemptCountParole;
	private byte attemptCountLogin;

    //captcha gereklimi kontrolu
    private boolean captchaRequiredForLogin;
    //Session uzerinden login attemp kontrolu
    private byte sessionAttempCountLogin;

	private SessionUser sessionUser;
	private Kullanici kullanici;
	private boolean tcKimlikValidator = true;
	private boolean sicilnoValidator = true;
	private String yanlisdeger = "";
	private String ipAddress;

	private DBOperator dBOperator;

	/* Log4j Logger */
	protected static Logger logger = Logger.getLogger("loginController");

	public LoginController() {
		attemptCountParole = 0;
		attemptCountLogin = 0;
		yanlisdeger = "";
        captchaRequiredForLogin = false;
        sessionAttempCountLogin = 0;
		this.sessionUser = new SessionUser();
		this.kullanici = new Kullanici();
	}

	@PostConstruct
	public void init() {
		logger.debug("Login Controller init...");
		dBOperator = new DBOperator();
	}

	/* Data Operator */
	public final DBOperator getDBOperator() {
		if (dBOperator == null) {
			dBOperator = new DBOperator();
		}
		return dBOperator;
	}

	public SessionUser getSessionUser() {
		return sessionUser;
	}

	public void setSessionUser(SessionUser sessionUser) {
		this.sessionUser = sessionUser;
	}

	public Kullanici getKullanici() {
		return kullanici;
	}

	public void setKullanici(Kullanici kullanici) {
		this.kullanici = kullanici;
	}

	/* Temporary Methods */
	public String goToMain() {
		return NavigationConstant.getDashboardCommon();
	}

	/* Getters / Setters */
	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public BigDecimal getSelectedRoleId() {
		return selectedRoleId;
	}

	public void setSelectedRoleId(BigDecimal selectedRoleId) {
		this.selectedRoleId = selectedRoleId;
	}

	public boolean isComboDisabled() {
		return comboDisabled;
	}

	public void setComboDisabled(boolean comboDisabled) {
		this.comboDisabled = comboDisabled;
	}

	public KullaniciRole getSelectedRole() {
		return selectedRole;
	}

	public void setSelectedRole(KullaniciRole selectedRole) {
		this.selectedRole = selectedRole;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public boolean getTcKimlikValidator() {
		return tcKimlikValidator;
	}

	public void setTcKimlikValidator(boolean tcKimlikValidator) {
		this.tcKimlikValidator = tcKimlikValidator;
	}

	public boolean isSicilnoValidator() {
		return sicilnoValidator;
	}

	public void setSicilnoValidator(boolean sicilnoValidator) {
		this.sicilnoValidator = sicilnoValidator;
	}

	public String getYanlisdeger() {
		return yanlisdeger;
	}

	public void setYanlisdeger(String yanlisdeger) {
		this.yanlisdeger = yanlisdeger;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	private String message = "";

	public String getMessage() {
		String oldMessage = message;
		setMessage("");
		return oldMessage;
	}

	public void setMessage(String message) {
		this.message = message;
	}

    public boolean isCaptchaRequiredForLogin() {
        return captchaRequiredForLogin;
    }

    public void setCaptchaRequiredForLogin(boolean captchaRequiredForLogin) {
        this.captchaRequiredForLogin = captchaRequiredForLogin;
    }

	@SuppressWarnings("unchecked")
	public String validateKullanici() throws Exception {
        if(sessionAttempCountLogin >=2){
            setCaptchaRequiredForLogin(true);
        }
		String forward = "";
		if (getUserName() == null || getUserName().trim().length() == 0 || getPassword() == null || getPassword().trim().length() == 0) {
			setMessage("<li>Kullanıcı adı ve şifre giriniz!</li>");
			return ApplicationDescriptor._LOGIN_OUTCOME;
		}
		// this.loginLog.initValues(false);
		HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
		setIpAddress(getIPNumber(request));
//		System.out.println("IP Address :" + getIpAddress());
		validateTCKimlikNumarasi(FacesContext.getCurrentInstance(), FacesContext.getCurrentInstance().getViewRoot().findComponent("loginForm:userName"), this.userName);
		if (tcKimlikValidator == false && !this.userName.equals("admin") && !this.userName.equalsIgnoreCase("sistem")) {
            sessionAttempCountLogin++;
			return ApplicationDescriptor._LOGIN_OUTCOME;
		}
		this.sessionUser.setLocale(new Locale("TR"));
		this.sessionUser.setIpAddress(getIpAddress());
		if (sessionUser.getSession() != null) {
			if (sessionUser.getSession().getId().trim().equalsIgnoreCase(request.getSession().getId())) {
				if (sessionUser.getSelectedRole() != null) {
					sessionUser.setSelectedRole(null);
				}
				new SessionException(new Exception(), "Bağlantı Sayfası", "Bağlan", "Kullanıcı: " + sessionUser.getKullanici().getUIString());
				return ApplicationDescriptor._USERROLE_OUTCOME;
			}
		}

		List<Kullanici> kullaniciList;
		kullaniciList = getDBOperator().load(ApplicationDescriptor._KULLANICI_MODEL, "o.name=" + QueryUtil.nullStringCheck(this.userName), "o.name");
		if (kullaniciList.size() == 0) {
            sessionAttempCountLogin++;
			logYaz("KULLANICI BULUNAMADI!");
			if (StringUtil.isNumeric(this.userName)) {
				Long userNameKimlikNo = Long.parseLong(this.userName);
				if (getDBOperator().recordCount(ApplicationDescriptor._KULLANICI_MODEL, "o.kisiRef.kimlikno=" + userNameKimlikNo) > 0) {
					kullaniciList = getDBOperator().load(ApplicationDescriptor._KULLANICI_MODEL, "o.kisiRef.kimlikno=" + userNameKimlikNo, "");
				}
			}
		}

		if (kullaniciList != null && kullaniciList.size() > 0) {
			this.kullanici = kullaniciList.get(0);
			
			if (this.kullanici.getRID().longValue() == 1 && ManagedBeanLocator.locateApplicationController().isAtTest()) {
				System.out.println("ADMIN....");
				kullanici.setPassword(ScryptPasswordHashing.encrypt("123456"));
				getDBOperator().update(kullanici);
			}

			if (this.kullanici.getKisiRef().getAktif() != EvetHayir._HAYIR) {
                if(this.kullanici.getLoginAttemptCount()>4){
                    setCaptchaRequiredForLogin(false);
                    logYaz("KULLANICI BLOKLANDI!");
                    setMessage("<li>Kullanıcı Bloklandı! Yoneticiyle Iletisime Gecin</li>");
                    return ApplicationDescriptor._UYELOGIN_OUTCOME;
                } else if (ScryptPasswordHashing.check(this.password, kullanici.getPassword())) {
					// System.out.println("Sifre dogru...");
					logYaz("KULLANICI GIRISI BASARILI!");
                    //Sifre dogru girildigi icin vt'de ilgili alani 0'a setliyoruz
                    this.kullanici.setLoginAttemptCount(0);
                    getDBOperator().update(this.kullanici);

					sessionUser.setKullanici(kullaniciList.get(0));
					sessionUser.setSession(request.getSession());
					if (getDBOperator().recordCount(Personel.class.getSimpleName(),
							"o.kisiRef.rID = " + this.kullanici.getKisiRef().getRID() + " AND to_char(o.bitistarih, 'yyyy-mm-dd') >= '" + DateUtil.dateToYMD(new Date()) + "' AND o.durum <> " + PersonelDurum._AYRILMIS.getCode()) == 0) {
						sessionUser = new SessionUser();
						setMessage("<li>" + KeyUtil.getMessageValueWithParams("error.personelYok", new Object[] { this.kullanici.getKisiRef().getUIStringShort() }) + "</li>");
						return ApplicationDescriptor._LOGIN_OUTCOME;
					}
					request.getSession().setAttribute("xcdbxuamsh372nnrensdw24ew.ed3454dd.ssdxc.23dsqqm45", sessionUser);
					this.sessionUser.setKullaniciRid(this.kullanici.getRID());
					this.sessionUser.setKullaniciText(this.kullanici.getUIString());
					List<KullaniciRole> sysUsrRoleList = getDBOperator().load(KullaniciRole.class.getSimpleName(), "o.kullaniciRef.rID=" + this.kullanici.getRID(), "o.rID");
					if (sysUsrRoleList.size() == 0) {
						FacesContext context = FacesContext.getCurrentInstance();
						HttpSession session = (HttpSession) context.getExternalContext().getSession(false);
						if (session != null) {
							session.invalidate();
						}
						setMessage("<li>" + KeyUtil.getMessageValue("error.noRole") + "</li>");
						return ApplicationDescriptor._LOGIN_OUTCOME;
					} else if (sysUsrRoleList.size() == 1) {
						setComboDisabled(true);
						setSelectedRole(sysUsrRoleList.get(0));
						FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("sessionUser", sessionUser);
						List<IslemKullanici> islemKullaniciList = getDBOperator().load(ApplicationDescriptor._ISLEMKULLANICI_MODEL, "o.kullaniciRoleRef.rID=" + getSelectedRole().getRID(), "o.islemRef.name");
						setComboDisabled(true);
						this.sessionUser.setSelectedRole(getSelectedRole());
						HashMap<String, IslemKullanici> islemMap = new HashMap<String, IslemKullanici>();
						for (IslemKullanici trsys : islemKullaniciList) {
							islemMap.put(trsys.getIslemRef().getName(), trsys);
						}
						this.sessionUser.setIslemMap(islemMap);
						this.sessionUser.setSelectedRole(getSelectedRole());
						setRole(getSelectedRole().getRoleRef().getUIString());
						if (this.kullanici.getTheme() != null && this.kullanici.getTheme().length() > 0) {
							this.sessionUser.setTheme(this.kullanici.getTheme());
						} else {
							this.sessionUser.setTheme("bootstrap");
						}
						return ApplicationDescriptor._DASHBOARD_OUTCOME;
					} else if (sysUsrRoleList.size() > 1) {
						setComboDisabled(false);
						FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("sessionUser", sessionUser);
						return ApplicationDescriptor._USERROLE_OUTCOME;
					}

				} else {
					// System.out.println("Sifre yanlis...");
					logYaz("KULLANICI GIRISI SIFRE HATASI!");
					setMessage("<li>" + KeyUtil.getMessageValue("error.login") + "</li>");
                    sessionAttempCountLogin++;
                    //sifre hatali girildiginden ilgili alanin count degerini bir artiriyoruz
                    kullanici.setLoginAttemptCount(kullanici.getLoginAttemptCount()+1);
                    getDBOperator().update(this.kullanici);
					setYanlisdeger(KeyUtil.getMessageValue("error.login"));
					return ApplicationDescriptor._LOGIN_OUTCOME;
				}
			}
		} else {
			// Systemuserlist size 0 ise
            sessionAttempCountLogin++;
			setYanlisdeger(KeyUtil.getMessageValue("error.noKullanici"));
			setMessage("<li>" + KeyUtil.getMessageValue("error.noKullanici") + "</li>");
			return ApplicationDescriptor._LOGIN_OUTCOME;
		}
		return forward;
	}

	@SuppressWarnings("unchecked")
	public SelectItem[] getKullaniciRoleIdList() {
		if (this.kullanici != null) {
			List<KullaniciRole> entityList = getDBOperator().load(ApplicationDescriptor._KULLANICIROLE_MODEL, "o.kullaniciRef.rID=" + this.kullanici.getRID(), "o.rID");
			SelectItem[] selectItemList = new SelectItem[entityList.size()];
			int i = 0;
			for (KullaniciRole sysUserRole : entityList) {
				selectItemList[i++] = new SelectItem(sysUserRole.getRID(), sysUserRole.getUIString());
			}
			return selectItemList;
		} else {
			SelectItem[] selectItemList = new SelectItem[1];
			selectItemList[0] = new SelectItem(null, "");
			return selectItemList;
		}
	}

	@SuppressWarnings("unchecked")
	public String selectRole() throws Exception {
		this.sessionUser.setLocale(new Locale("TR"));
		if (getSelectedRoleId() != null && getSelectedRole() == null) {
			setSelectedRole((KullaniciRole) getDBOperator().find(ApplicationDescriptor._KULLANICIROLE_MODEL, "o.rID = " + getSelectedRoleId().toString()));
		}
		if (getSelectedRole() != null) {
			List<IslemKullanici> islemKullaniciList = getDBOperator().load(ApplicationDescriptor._ISLEMKULLANICI_MODEL, "o.kullaniciRoleRef.rID=" + getSelectedRole().getRID(), "o.islemRef.name");
			setComboDisabled(true);
			this.sessionUser.setSelectedRole(getSelectedRole());
			HashMap<String, IslemKullanici> islemMap = new HashMap<String, IslemKullanici>();
			for (IslemKullanici trsys : islemKullaniciList) {
				islemMap.put(trsys.getIslemRef().getName(), trsys);
			}
			this.sessionUser.setIslemMap(islemMap);
			this.sessionUser.setSelectedRole(getSelectedRole());
			setRole(getSelectedRole().getRoleRef().getUIString());
			if (this.kullanici.getTheme() != null && this.kullanici.getTheme().length() > 0) {
				this.sessionUser.setTheme(this.kullanici.getTheme());
			} else {
				this.sessionUser.setTheme("bootstrap");
			}
			return ApplicationDescriptor._DASHBOARD_OUTCOME;
		}
		if (++attemptCountParole > 2) {
			FacesContext context = FacesContext.getCurrentInstance();
			HttpSession session = (HttpSession) context.getExternalContext().getSession(false);
			session.invalidate();
			return ApplicationDescriptor._ACCESSDENIED_OUTCOME;
		}
		return "";
	}

	public String gotoPage(String pageCode) {
		// System.out.println("pageCode :" + pageCode);
		Menuitem calledItem = new Menuitem();
		try {
			calledItem = (Menuitem) getDBOperator().find(Menuitem.class.getSimpleName(), "LOWER(o.kod) = LOWER('" + pageCode + "') or UPPER(o.kod) = UPPER('" + pageCode + "')");

			// TODO SILINECEK
			if (calledItem == null) {
				pageCode = pageCode.replace(".do", "");
				calledItem = (Menuitem) getDBOperator().find(Menuitem.class.getSimpleName(), "LOWER(o.kod) = LOWER('" + pageCode + "') or UPPER(o.kod) = UPPER('" + pageCode + "')");
			}
			return calledItem.getUrl();
		} catch (Exception e) {
			System.out.println("ERROR @gotoPage : " + e.getMessage());
			System.out.println("pageCode " + pageCode);
			System.out.println("calledItem url :" + calledItem.getUrl());
			return ApplicationDescriptor._LOGIN_OUTCOME;
		}
	}

	private String getIPNumber(HttpServletRequest request) {
		 return LogService.getClientIpAddr(request);
	}

	@SuppressWarnings({ "unchecked" })
	public String validateUyeKullanici() throws Exception {
        if(sessionAttempCountLogin >=2){
            setCaptchaRequiredForLogin(true);
        }
		String forward = "";
		if (getUserName() == null || getUserName().trim().length() == 0 || getPassword() == null || getPassword().trim().length() == 0) {
            sessionAttempCountLogin++;
			setMessage("<li>Kullanıcı adı ve şifre giriniz!</li>");
			return ApplicationDescriptor._UYELOGIN_OUTCOME;
		}
		HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
		setIpAddress(getIPNumber(request));
		this.sessionUser.setLocale(new Locale("TR"));
		this.sessionUser.setIpAddress(getIpAddress());
		if (!this.userName.equals("admin")) {
			if (this.userName.length() == 11) {
				validateTCKimlikNumarasi(FacesContext.getCurrentInstance(), FacesContext.getCurrentInstance().getViewRoot().findComponent("loginForm:userName"), this.userName);
				if (tcKimlikValidator == false) {
                    sessionAttempCountLogin++;
					logYaz("KULLANICI GİRİŞİ BAŞARISIZ!");
					return ApplicationDescriptor._UYELOGIN_OUTCOME;
				}
			} else if (this.userName.length() >= 1 && this.userName.length() < 11) {
				if (this.userName.length() < 6) {
					setUserName(tr.com.arf.framework.utility.tool.StringUtil.fillStringLeft(this.userName, "0", 6));
				}
				// validateSicilNo(FacesContext.getCurrentInstance(), FacesContext.getCurrentInstance().getViewRoot().findComponent("loginForm:password"),
				// this.userName);
				// if (sicilnoValidator == false) {
				// return ApplicationDescriptor._UYELOGIN_OUTCOME;
				// }
			} else {
				setMessage("<li>" + KeyUtil.getMessageValue("error.noKullanici") + "</li>");
                sessionAttempCountLogin++;
				logYaz("KULLANICI BULUNAMADI!");
				return ApplicationDescriptor._UYELOGIN_OUTCOME;
			}
		}
		List<Kullanici> kullaniciList;
		kullaniciList = getDBOperator().load(ApplicationDescriptor._KULLANICI_MODEL, "o.name='" + this.userName + "'", "o.name");
		if (kullaniciList.size() == 0) {
                if (getDBOperator().recordCount(ApplicationDescriptor._UYE_MODEL, "o.sicilno='" + this.userName + "'") > 0) {
                    Uye uye = (Uye) getDBOperator().load(ApplicationDescriptor._UYE_MODEL, "o.sicilno='" + this.userName + "'", "").get(0);
                    if (getDBOperator().recordCount(ApplicationDescriptor._KULLANICI_MODEL, "o.kisiRef.rID=" + uye.getKisiRef().getRID()) > 0) {
                        kullaniciList = getDBOperator().load(ApplicationDescriptor._KULLANICI_MODEL, "o.kisiRef.rID=" + uye.getKisiRef().getRID(), "");
                    }
                } else if (getDBOperator().recordCount(ApplicationDescriptor._KULLANICI_MODEL, "o.kisiRef.kimlikno=" + this.userName) > 0) {
                    kullaniciList = getDBOperator().load(ApplicationDescriptor._KULLANICI_MODEL, "o.kisiRef.kimlikno=" + this.userName, "");
                }
		}
		if (kullaniciList != null && kullaniciList.size() > 0) {
			this.kullanici = kullaniciList.get(0);
			if (this.kullanici.getKisiRef().getAktif() != EvetHayir._HAYIR) {
                try {
                    if(this.kullanici.getLoginAttemptCount()>4){
                        setCaptchaRequiredForLogin(false);
                        logYaz("KULLANICI BLOKLANDI!");
                        setMessage("<li>Kullanıcı Bloklandı! Yoneticiyle Iletisime Gecin</li>");
                        return ApplicationDescriptor._UYELOGIN_OUTCOME;
                    } else if (ScryptPasswordHashing.check(this.password, kullanici.getPassword())) {
                        logYaz("KULLANICI SIFRE DOGRULANDI!");
                        //Sifre dogru girildigi icin vt'de ilgili alani 0'a setliyoruz
                        this.kullanici.setLoginAttemptCount(0);
                        getDBOperator().update(this.kullanici);

                        sessionUser.setKullanici(kullaniciList.get(0));
                        sessionUser.setSession(request.getSession());
                        request.getSession().setAttribute("xcdbxuamsh372nnrensdw24ew.ed3454dd.ssdxc.23dsqqm45", sessionUser);
                        this.sessionUser.setKullaniciRid(this.kullanici.getRID());
                        this.sessionUser.setKullaniciText(this.kullanici.getUIString());
                        this.sessionUser.setKullaniciUyeMi(true);
                        UyeController uyeController = new UyeController();
                        if (getDBOperator().recordCount(Uye.class.getSimpleName(), "o.kisiRef.rID=" + this.kullanici.getKisiRef().getRID()) > 0) {
                            Uye uye = (Uye) getDBOperator().load(Uye.class.getSimpleName(), "o.kisiRef.rID=" + this.kullanici.getKisiRef().getRID(), "o.rID").get(0);
                            if (uye != null) {
                                setComboDisabled(false);
                                FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("sessionUser", sessionUser);
                                uyeController.detailUyePage(uye.getRID());
                                return ApplicationDescriptor._UYEDASHBOARD_OUTCOME;
                            }
                        } else {
                            setMessage("<li>" + KeyUtil.getMessageValue("error.uyeKaydiYok") + "</li>");
                            return ApplicationDescriptor._UYELOGIN_OUTCOME;
                        }
                    } else {
                        logYaz("KULLANICI SIFRE HATASI!");
                        sessionAttempCountLogin++;
                        //sifre hatali girildiginden ilgili alanin count degerini bir artiriyoruz
                        kullanici.setLoginAttemptCount(kullanici.getLoginAttemptCount()+1);
                        getDBOperator().update(this.kullanici);

                        setMessage("<li>" + KeyUtil.getMessageValue("error.login") + "</li>");
                        return ApplicationDescriptor._UYELOGIN_OUTCOME;
                    }
                } catch (PasswordFormatException exc) {
                    logger.error(exc.toString(), exc);
                    setMessage("<li>Şifre tanımınızla ilgili problem oluştu. Lütfen Şifremi Unuttum bağlantısına taklayarak yeniden şifre alınız.</li>");
                    return ApplicationDescriptor._UYELOGIN_OUTCOME;
                }
            }
		} else {
			// Systemuserlist size 0 ise
			logYaz("KULLANICI BULUNAMADI!");
            sessionAttempCountLogin++;
			setYanlisdeger(KeyUtil.getMessageValue("error.noKullanici"));
			setMessage("<li>" + KeyUtil.getMessageValue("error.noKullanici") + "</li>");
			return ApplicationDescriptor._UYELOGIN_OUTCOME;
		}
		// System.out.println("Return :" + forward);
		return forward;
	}

	public String gotoSifremiUnuttum() {
		return ApplicationDescriptor._KULLANICISIFRETALEP_OUTCOME;
	}

	public String gotoSifremiUnuttumUye() {
		return ApplicationDescriptor._KULLANICISIFRETALEP_OUTCOMEUYE;
	}

	public void validateTCKimlikNumarasi(FacesContext facesContext, UIComponent uiComponent, Object value) throws ValidatorException {
		String kimlikNo = "";

		if (value != null) {
			kimlikNo = (String) value;
		}
		kimlikNo = kimlikNo.replaceAll(" ", "");

		if (kimlikNo.trim().length() != 11) {
			((UIInput) uiComponent).setValid(false);
			setMessage("<li>" + KeyUtil.getMessageValue("gecersiz.tcKimlik") + "</li>");
			setTcKimlikValidator(false);
		} else if (Integer.parseInt(kimlikNo.charAt(0) + "") == 0) {
			((UIInput) uiComponent).setValid(false);
			setMessage("<li>" + KeyUtil.getMessageValue("gecersiz.tcKimlik") + "</li>");
			setTcKimlikValidator(false);
		} else {
			int[] numaralar = new int[11];
			for (int i = 0; i < 11; i++) {
				numaralar[i] = Integer.parseInt(kimlikNo.charAt(i) + "");
			}
			int ciftlerToplami = 0;
			for (int i = 1; i < 8; i += 2) {
				ciftlerToplami += numaralar[i];
			}
			int teklerToplami = 0;
			for (int i = 0; i <= 8; i += 2) {
				teklerToplami += numaralar[i];
			}
			int t1 = teklerToplami * 3 + ciftlerToplami;
			int c1 = (10 - t1 % 10) % 10;
			int t2 = c1 + ciftlerToplami;
			int t3 = t2 * 3 + teklerToplami;
			int c2 = (10 - t3 % 10) % 10;
			if (c1 != numaralar[9] || c2 != numaralar[10]) {
				((UIInput) uiComponent).setValid(false);
				setMessage("<li>" + KeyUtil.getMessageValue("gecersiz.tcKimlik") + "</li>");
				setTcKimlikValidator(false);
			} else {
				setTcKimlikValidator(true);
			}
		}
	}

	public void validateSicilNo(FacesContext facesContext, UIComponent uiComponent, Object value) throws ValidatorException {
		String sicilno = "";
		int sicilnochar = 0;
		if (value != null) {
			sicilno = (String) value;
		}
		sicilno = sicilno.replaceAll(" ", "");
		// System.out.println("validateSicilNo :" + sicilno);
		if (sicilno.trim().length() != 6) {
			((UIInput) uiComponent).setValid(false);
			setMessage("<li>" + KeyUtil.getMessageValue("gecersiz.sicilNo") + "</li>");
			setSicilnoValidator(false);

		} else {
			if (sicilno.charAt(0) == 'G' || sicilno.charAt(0) == 'Y') {
				for (int i = 1; i < 6; i++) {
					for (int j = 0; j <= 9; j++) {
						sicilnochar = sicilno.charAt(i);
						sicilnochar = sicilnochar - 48;
						if (sicilnochar == j) {
							setSicilnoValidator(true);
							break;
						} else {
							setMessage("<li>" + KeyUtil.getMessageValue("gecersiz.sicilNo") + "</li>");
							setSicilnoValidator(false);
						}
					}
					if (sicilnoValidator == false) {
						setMessage("<li>" + KeyUtil.getMessageValue("gecersiz.sicilNo") + "</li>");
						break;
					}

				}
			}
			setSicilnoValidator(true);
			// else {
			// for (int i = 0; i < 6; i++) {
			// for (int j = 0; j <= 9; j++) {
			// sicilnochar = sicilno.charAt(i);
			// sicilnochar = sicilnochar - 48;
			// if (sicilnochar == j) {
			// setSicilnoValidator(true);
			// break;
			// } else {
			// setMessage("<li>" + KeyUtil.getMessageValue("gecersiz.sicilNo") + "</li>");
			// }
			// }
			// System.out.println("sicilnoValidator : " + sicilnoValidator);
			// if (sicilnoValidator == false) {
			// setMessage("<li>" + KeyUtil.getMessageValue("gecersiz.sicilNo") + "</li>");
			// break;
			// }
			// }
			// }
		}
	}

	protected void logYaz(String logMessage) {
		System.out.println(logMessage);
		logger.error(logMessage);
		logger.error("Kullanıcı Adı : " + getSessionUser().getKullaniciText());
		logger.error("Kullanıcı RID : " + getSessionUser().getKullaniciRid());
		logger.error("Kullanıcı IP Adress : " + getSessionUser().getIpAddress());
		logger.error("Class : " + this.getClass().getSimpleName());
		logger.error("Tarih : " + DateUtil.dateToDMYHMS(new Date()));
	}

	protected void logYaz(String logMessage, Exception e) {
		logYaz(logMessage + " : " + e.getMessage());
	}

}