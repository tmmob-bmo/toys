package tr.com.arf.toys.view.controller.form.system; 
  
 
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.persistence.EntityManager;
import javax.persistence.Query;

import tr.com.arf.framework.db._management.DBOperator;
import tr.com.arf.framework.db.enumerated._common.EvetHayir;
import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.toys.db.filter.system.IslemKullaniciFilter;
import tr.com.arf.toys.db.model.system.IslemKullanici;
import tr.com.arf.toys.db.model.system.KullaniciRole;
import tr.com.arf.toys.service.application.ApplicationDescriptor;
import tr.com.arf.toys.view.controller.form._base.ToysFilteredControllerNoPaging;
  
  
@ManagedBean 
@ViewScoped 
public class IslemKullaniciController extends ToysFilteredControllerNoPaging {  
  
  private static final long serialVersionUID = 6531152165884980748L; 
  
	private IslemKullanici islemKullanici;   
	private IslemKullaniciFilter islemKullaniciFilter = new IslemKullaniciFilter();  
  
	public IslemKullaniciController() { 
		super(IslemKullanici.class);  
		setDefaultValues(false);  
		setOrderField("islemRef.name");  
		setAutoCompleteSearchColumns(new String[]{"kullaniciRoleRef"}); 
		setTable(null); 
		if(getMasterEntity() != null){
			setMasterObjectName(ApplicationDescriptor._KULLANICIROLE_MODEL);
			setMasterOutcome(ApplicationDescriptor._KULLANICIROLE_MODEL);
			getIslemKullaniciFilter().setKullaniciRoleRef(getMasterEntity());
			getIslemKullaniciFilter().createQueryCriterias();
		}
		queryAction(); 
	} 
 
	public KullaniciRole getMasterEntity(){
		if(getIslemKullaniciFilter().getIslemRef() != null){
			return getIslemKullaniciFilter().getKullaniciRoleRef();
		} else {
			return (KullaniciRole) getObjectFromSessionFilter(ApplicationDescriptor._KULLANICIROLE_MODEL);
		}			
	}  
	
	public IslemKullanici getIslemKullanici() { 
		islemKullanici = (IslemKullanici) getEntity(); 
		return islemKullanici; 
	} 
	
	public void setIslemKullanici(IslemKullanici islemKullanici) { 
		this.islemKullanici = islemKullanici; 
	} 
	
	public IslemKullaniciFilter getIslemKullaniciFilter() { 
		return islemKullaniciFilter; 
	} 
	 
	public void setIslemKullaniciFilter(IslemKullaniciFilter islemKullaniciFilter) {  
		this.islemKullaniciFilter = islemKullaniciFilter;  
	}  
	 
	@Override  
	public BaseFilter getFilter() {  
		return getIslemKullaniciFilter();  
	} 
	
	@Override
	public void resetFilter(){
		super.resetFilterOnly();
		if(getMasterEntity() != null){
			getIslemKullaniciFilter().setKullaniciRoleRef(getMasterEntity()); 
		}
		queryAction();
	} 
 
	@Override	
	public void insert() {	
		super.insert();	
		getIslemKullanici().setKullaniciRoleRef(getIslemKullaniciFilter().getKullaniciRoleRef());	
	}	
	
	public void enableDisableSelected(String operationType){
		if(!setSelected()) {
			return;
		}
		
		DBOperator dbOperator = getDBOperator();
		EntityManager entityManager = dbOperator.getEntityManagerFactory().createEntityManager();
		entityManager.getTransaction().begin();
		Query query = entityManager
			.createQuery("UPDATE " + getModelName() + " o SET "
				+ " o.listPermission = :listPermission, " 
				+ " o.updatePermission = :updatePermission, "
				+ " o.deletePermission = :deletePermission " 
				+ " WHERE o.rID = :islemKullanici ");
		if(operationType.equalsIgnoreCase("enable")){
			query.setParameter("listPermission", EvetHayir._EVET); 
			query.setParameter("updatePermission", EvetHayir._EVET);
			query.setParameter("deletePermission", EvetHayir._EVET); 
		} else {
			query.setParameter("listPermission", EvetHayir._HAYIR);				 
			query.setParameter("updatePermission", EvetHayir._HAYIR);
			query.setParameter("deletePermission", EvetHayir._HAYIR);
		}	
		
		query.setParameter("islemKullanici", getIslemKullanici().getRID());
		query.executeUpdate();
		entityManager.getTransaction().commit();
		entityManager.close(); 	
		setTable(null); 
		queryAction();
	}
	
	public void changePermission(String islem){
		DBOperator dbOperator = getDBOperator(); 
		getIslemKullanici().reverseEnumerated(islem);		
		try {
			dbOperator.update(getIslemKullanici());  
		} catch (Exception e) {
			logYaz("HATA :" + e.getMessage()); 
		}
		setTable(null); 
		queryAction();
	}
	
} // class 
