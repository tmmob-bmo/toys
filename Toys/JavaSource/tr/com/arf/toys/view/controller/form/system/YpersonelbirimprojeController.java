package tr.com.arf.toys.view.controller.form.system;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.toys.db.enumerated.system.YpersonelProjeDurum;
import tr.com.arf.toys.db.filter.system.YpersonelbirimprojeFilter;
import tr.com.arf.toys.db.model.system.Ypersonel;
import tr.com.arf.toys.db.model.system.Ypersonelbirimproje;
import tr.com.arf.toys.service.application.ApplicationDescriptor;
import tr.com.arf.toys.view.controller.form._base.ToysBaseFilteredController;

@ManagedBean 
@ViewScoped
public class YpersonelbirimprojeController extends ToysBaseFilteredController{

	private static final long serialVersionUID = 9113006214161222041L;
	
	private Ypersonelbirimproje personelBirimProje;
	

	private YpersonelbirimprojeFilter personelBirimProjeFilter = new YpersonelbirimprojeFilter();
	
	public YpersonelbirimprojeController() {
		super(Ypersonelbirimproje.class);  
		setDefaultValues(false);  
		setOrderField("baslangictarih DESC");  
		setAutoCompleteSearchColumns(new String[]{"aciklama"}); 
		setTable(null); 
		setStaticWhereCondition(getSessionUser().createStaticWhereCondition("o."));
		if(getMasterEntity() != null){ 
			setMasterObjectName(ApplicationDescriptor._PERSONEL_MODEL);
			setMasterOutcome(ApplicationDescriptor._PERSONEL_MODEL);
			getPersonelBirimProjeFilter().setPersonelRef(getMasterEntity());
			getPersonelBirimProjeFilter().createQueryCriterias();   
		} 
		queryAction(); 
		
	}
	
	public YpersonelbirimprojeController(Object object) { 
		super(Ypersonelbirimproje.class);  
		setLoggable(false);
	}
	
	public Ypersonel getMasterEntity(){
		if(getPersonelBirimProjeFilter().getPersonelRef() != null){
			return getPersonelBirimProjeFilter().getPersonelRef();
		} else {
			return (Ypersonel) getObjectFromSessionFilter(ApplicationDescriptor._PERSONEL_MODEL);
		}			
	} 

	public Ypersonelbirimproje getPersonelBirimProje() { 
		personelBirimProje = (Ypersonelbirimproje) getEntity(); 
		return personelBirimProje; 
	} 
	
	public void setPersonelBirimProje(Ypersonelbirimproje personelBirimProje) {
		this.personelBirimProje = personelBirimProje;
	}

	public YpersonelbirimprojeFilter getPersonelBirimProjeFilter() {
		return personelBirimProjeFilter;
	}

	public void setPersonelBirimProjeFilter(YpersonelbirimprojeFilter personelBirimProjeFilter) {
		this.personelBirimProjeFilter = personelBirimProjeFilter;
	}
	
	@Override  
	public BaseFilter getFilter() {  
		return getPersonelBirimProjeFilter();  
	}   
 
	@Override	
	public void insert() {	
		super.insert();	
		getPersonelBirimProje().setPersonelRef(getPersonelBirimProjeFilter().getPersonelRef());
		getPersonelBirimProje().setDurum(YpersonelProjeDurum._AKTIF);
	}	

}
