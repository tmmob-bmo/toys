package tr.com.arf.toys.view.controller.form.iletisim;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.toys.db.enumerated.iletisim.IcerikTuru;
import tr.com.arf.toys.db.filter.iletisim.IcerikFilter;
import tr.com.arf.toys.db.model.iletisim.Icerik;
import tr.com.arf.toys.view.controller.form._base.ToysBaseFilteredController;

@ManagedBean
@ViewScoped
public class IcerikController extends ToysBaseFilteredController {

	private static final long serialVersionUID = 6531152165884980748L;

	private Icerik icerik;
	private IcerikFilter icerikFilter = new IcerikFilter();

	public IcerikController() {
		super(Icerik.class);
		setDefaultValues(false);
		setOrderField("");
		setLoggable(false);
		setAutoCompleteSearchColumns(new String[] { "gonderimturu" });
		setTable(null);
		queryAction();
	}

	public Icerik getIcerik() {
		icerik = (Icerik) getEntity();
		return icerik;
	}

	public void setIcerik(Icerik icerik) {
		this.icerik = icerik;
	}

	public IcerikFilter getIcerikFilter() {
		return icerikFilter;
	}

	public void setIcerikFilter(IcerikFilter icerikFilter) {
		this.icerikFilter = icerikFilter;
	}

	@Override
	public BaseFilter getFilter() {
		return getIcerikFilter();
	}

	@Override
	public void save() {
		this.icerik.setIcerikturu(IcerikTuru._HAZIRICERIK);
		super.save();
	}
} // class
