package tr.com.arf.toys.view.controller.form._base;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import javax.faces.FacesException;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.model.SelectItem;

import org.primefaces.component.export.Exporter;

import tr.com.arf.framework.utility.tool.StringUtil;
import tr.com.arf.framework.view.controller.form._base.BaseMustFilteredController;
import tr.com.arf.toys.db.enumerated.iletisim.AdresTipi;
import tr.com.arf.toys.db.model.system.Fakulte;
import tr.com.arf.toys.db.model.system.FakulteBolum;
import tr.com.arf.toys.db.model.system.Ilce;
import tr.com.arf.toys.db.model.system.Koy;
import tr.com.arf.toys.db.model.system.Sehir;
import tr.com.arf.toys.db.model.system.Ulke;
import tr.com.arf.toys.db.model.system.Universite;
import tr.com.arf.toys.db.model.system.UniversiteFakulte;
import tr.com.arf.toys.service.application.ApplicationDescriptor;
import tr.com.arf.toys.service.application.ManagedBeanLocator;
import tr.com.arf.toys.utility.tool.OpenOfficeExporter;
import tr.com.arf.toys.utility.tool.WordExporter;
import tr.com.arf.toys.view.controller._common.SessionUser;

public abstract class ToysBaseMustFilteredController extends BaseMustFilteredController {

	@SuppressWarnings("rawtypes")
	protected ToysBaseMustFilteredController(Class c) {
		super(c);
	}

	private static final long serialVersionUID = 2096021367264048635L;


	@Override
	public void queryAction(){
		super.queryAction();
		removeObjectFromSessionFilter("logEntity");
	}

	@Override
	public SessionUser getSessionUser(){
		return ManagedBeanLocator.locateSessionUser();
	}

	@Override
	public void removeObjectFromSessionFilter(String objectName){
		getSessionUser().removeSessionFilterObject(objectName);
	}

	@Override
	public void putObjectToSessionFilter(String objectName, Object object){
		getSessionUser().addToSessionFilters(objectName, object);
	}

	@Override
	public Object getObjectFromSessionFilter(String objectName){
		return getSessionUser().getSessionFilterObject(objectName);
	}

	@Override
	public String backToMaster(){
		if(getMasterOutcome() != null){
			return StringUtil.makeFirstCharLowerCase(getMasterOutcome()) + ".do";
		} else {
			return ApplicationDescriptor._DASHBOARD_OUTCOME;
		}
	}

	public void wordExport() {
		String encodingType = "UTF-8";
		FacesContext facesContext = FacesContext.getCurrentInstance();
		try {
			String outputFileName = getModelName().toLowerCase(Locale.ENGLISH) + "Listesi";
			Exporter exporter=new WordExporter();
			int[] excludedColumns = new int[0];
			exporter.export(facesContext, getTable(), outputFileName, false, false, excludedColumns, encodingType, null, null);
			facesContext.responseComplete();
		} catch (IOException e) {
			throw new FacesException(e);
		}
	}

	public void openOfficeExport() {
		String encodingType = "UTF-8";
		FacesContext facesContext = FacesContext.getCurrentInstance();
		try {
			String outputFileName = getModelName().toLowerCase(Locale.ENGLISH) + "Listesi";
			Exporter exporter=new OpenOfficeExporter();
			int[] excludedColumns = new int[0];
			exporter.export(facesContext, getTable(), outputFileName, false, false, excludedColumns, encodingType, null, null);
			facesContext.responseComplete();
		} catch (IOException e) {
			throw new FacesException(e);
		}
	}

	public SelectItem[] ilceItemList;
	private SelectItem[] mahalleItemList;
	public SelectItem[] ilItemList;
	public SelectItem[] fakulteItemList;
	public SelectItem[] bolumItemList;
	public SelectItem[] ulkeItemList;


	@Override
	public void handleChange(AjaxBehaviorEvent event) {
		try {
			HtmlSelectOneMenu menu = (HtmlSelectOneMenu) event.getComponent();
			if(menu.getValue() instanceof Sehir){
				changeIlce(((Sehir) menu.getValue()).getRID());
			} else if(menu.getValue() instanceof Ilce){
				changeMahalle(((Ilce) menu.getValue()).getRID());
			} else if(menu.getValue() instanceof Ulke){
				changeIl(((Ulke) menu.getValue()).getRID());
			}  else if(menu.getValue() instanceof AdresTipi){
				changeUlke((AdresTipi) menu.getValue());
			} else if(menu.getValue() instanceof Universite){
				changeFakulte(((Universite) menu.getValue()).getRID());
			} else if(menu.getValue() instanceof Fakulte){
				changeBolum(((Fakulte) menu.getValue()).getRID());
			}
		} catch(Exception e){
			logYaz("Exception @" + getModelName() + "Controller :", e);
		}
	}

	protected void changeIlce(Long selectedRecId){
		if(selectedRecId.longValue() == 0){
			setIlceItemList(new SelectItem[0]);
		} else {
			@SuppressWarnings("unchecked")
			List<Ilce> entityList = getDBOperator().load(Ilce.class.getSimpleName(), "o.sehirRef.rID=" + selectedRecId ,"o.ad");
			if(entityList != null){
				ilceItemList = new SelectItem[entityList.size() + 1];
				ilceItemList[0] = new SelectItem(null, "");
				int i = 1;
				for(Ilce entity : entityList) {
					ilceItemList[i++] = new SelectItem(entity, entity.getUIString());
				}
			} else {
				setIlceItemList(new SelectItem[0]);
			}
		}
	}


	protected void changeMahalle(Long selectedRecId) {
		if(selectedRecId.longValue() == 0){
			setMahalleItemList(new SelectItem[0]);
		} else {
			@SuppressWarnings("unchecked")
			List<Koy> entityList = getDBOperator().load(Koy.class.getSimpleName(), "o.ilceRef.rID=" + selectedRecId ,"o.rID");
			if(entityList != null){
				mahalleItemList = new SelectItem[entityList.size() + 1];
				mahalleItemList[0] = new SelectItem(null, "");
				int i = 1;
				for(Koy entity : entityList) {
					mahalleItemList[i++] = new SelectItem(entity, entity.getUIString());
				}
			} else {
				setMahalleItemList(new SelectItem[0]);
			}
		}
	}

	protected void changeUlke(AdresTipi adresTipi){
		if(adresTipi == null || adresTipi == AdresTipi._NULL){
			setUlkeItemList(new SelectItem[0]);
		} else {
			String whereCon = "";
			if(adresTipi == AdresTipi._YURTDISIADRESI){
				whereCon = "o.rID > " + 1;
			} else {
				whereCon = "o.rID = " + 1;
			}
			@SuppressWarnings("unchecked")
			List<Ulke> entityList = getDBOperator().load(Ulke.class.getSimpleName(), whereCon,"o.ad");
			if(entityList != null){
				ulkeItemList = new SelectItem[entityList.size() + 1];
				ulkeItemList[0] = new SelectItem(null, "");
				int i = 1;
				for(Ulke entity : entityList) {
					ulkeItemList[i++] = new SelectItem(entity, entity.getUIString());
				}
			} else {
				setUlkeItemList(new SelectItem[0]);
			}
		}
	}

	protected void changeIl(Long selectedRecId){
		if(selectedRecId.longValue() == 0){
			setIlceItemList(new SelectItem[0]);
		} else {
			@SuppressWarnings("unchecked")
//			List<Sehir> entityList = getDBOperator().load(Sehir.class.getSimpleName(), "o.ulkeRef.rID=" + selectedRecId ,"o.ad");
			List<Sehir> entityList = getDBOperator().load(Sehir.class.getSimpleName(), "" ,"o.ad");
			if(entityList != null){
				ilItemList = new SelectItem[entityList.size() + 1];
				ilItemList[0] = new SelectItem(null, "");
				int i = 1;
				for(Sehir entity : entityList) {
					ilItemList[i++] = new SelectItem(entity, entity.getUIString());
				}
			} else {
				setIlItemList(new SelectItem[0]);
			}
		}
	}

	protected void changeFakulte(Long selectedRecId){
		if(selectedRecId.longValue() == 0){
			setIlceItemList(new SelectItem[0]);
		} else {
			@SuppressWarnings("unchecked")
			List<UniversiteFakulte> entityList = getDBOperator().load(UniversiteFakulte.class.getSimpleName(), "o.universiteRef.rID=" + selectedRecId ,"o.fakulteRef.ad");
			if(entityList != null){
				fakulteItemList = new SelectItem[entityList.size() + 1];
				fakulteItemList[0] = new SelectItem(null, "");
				int i = 1;
				for(UniversiteFakulte entity : entityList) {
					fakulteItemList[i++] = new SelectItem(entity.getFakulteRef(), entity.getFakulteRef().getAd());
				}
			} else {
				setFakulteItemList(new SelectItem[0]);
			}
		}
	}

	protected void changeBolum(Long selectedRecId){
		if(selectedRecId.longValue() == 0){
			setIlceItemList(new SelectItem[0]);
		} else {
			@SuppressWarnings("unchecked")
			List<FakulteBolum> entityList = getDBOperator().load(FakulteBolum.class.getSimpleName(), "o.fakulteRef.rID=" + selectedRecId ,"o.bolumRef.ad");
			if(entityList != null){
				bolumItemList = new SelectItem[entityList.size() + 1];
				bolumItemList[0] = new SelectItem(null, "");
				int i = 1;
				for(FakulteBolum entity : entityList) {
					bolumItemList[i++] = new SelectItem(entity.getBolumRef(), entity.getBolumRef().getAd());
				}
			} else {
				setBolumItemList(new SelectItem[0]);
			}
		}
	}

	public SelectItem[] ilceItemList() {
		return getIlceItemList();
	}

	public SelectItem[] getIlceItemList() {
		return ilceItemList;
	}

	public void setIlceItemList(SelectItem[] ilceItemList) {
		this.ilceItemList = ilceItemList;
	}

	public SelectItem[] getMahalleItemList() {
		return mahalleItemList;
	}

	public void setMahalleItemList(SelectItem[] mahalleItemList) {
		this.mahalleItemList = mahalleItemList;
	}

	public SelectItem[] getIlItemList() {
		return ilItemList;
	}

	public void setIlItemList(SelectItem[] ilItemList) {
		this.ilItemList = ilItemList;
	}

	public SelectItem[] getUlkeItemList() {
		return ulkeItemList;
	}

	public void setUlkeItemList(SelectItem[] ulkeItemList) {
		this.ulkeItemList = ulkeItemList;
	}

	public SelectItem[] getFakulteItemList() {
		return fakulteItemList;
	}

	public void setFakulteItemList(SelectItem[] fakulteItemList) {
		this.fakulteItemList = fakulteItemList;
	}

	public SelectItem[] getBolumItemList() {
		return bolumItemList;
	}

	public void setBolumItemList(SelectItem[] bolumItemList) {
		this.bolumItemList = bolumItemList;
	}

	@Override
	public String getStaticWhereCondition(String modelName) {
		return getSessionUser().getStaticWhereCondition(modelName);
	}

	@Override
	public String[] getAutoCompleteSearchColumns(String modelName) {
		return getSessionUser().getAutoCompleteSearchColumns(modelName);
	}

	@Override
	public String detaySayfa(String outcome) {
		logYaz(outcome + "'a gidiyor...");
		return super.detaySayfa(outcome);
	}
}
