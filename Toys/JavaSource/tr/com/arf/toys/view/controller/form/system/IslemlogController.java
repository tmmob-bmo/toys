package tr.com.arf.toys.view.controller.form.system; 
  
 
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.framework.db.model._base.BaseEntity;
import tr.com.arf.framework.db.model.sistem.Islemlog;
import tr.com.arf.toys.db.filter.system.IslemlogFilter;
import tr.com.arf.toys.view.controller.form._base.ToysBaseFilteredController;
  
  
@ManagedBean 
@SessionScoped
public class IslemlogController extends ToysBaseFilteredController {  
  
  private static final long serialVersionUID = 6531152165884980748L; 
  
	private Islemlog islemlog;  
	private IslemlogFilter islemlogFilter = new IslemlogFilter();  
  
	public IslemlogController() { 
		super(Islemlog.class);  
		setDefaultValues(false);  
		setOrderField("rID");  
		setAutoCompleteSearchColumns(new String[]{"tabloadi"}); 
		setTable(null); 
	} 
	
	public Islemlog getIslemlog() { 
		islemlog = (Islemlog) getEntity(); 
		return islemlog; 
	} 
	
	public void setIslemlog(Islemlog islemlog) { 
		this.islemlog = islemlog; 
	} 
	
	public IslemlogFilter getIslemlogFilter() { 
		return islemlogFilter; 
	} 
	 
	public void setIslemlogFilter(IslemlogFilter islemlogFilter) {  
		this.islemlogFilter = islemlogFilter;  
	}  
	 
	@Override  
	public BaseFilter getFilter() {  
		return getIslemlogFilter();  
	}  	 
	
	private String islemLogBaslik;
 
	 
	public String getIslemLogBaslik() {
		return islemLogBaslik;
	}

	public void setIslemLogBaslik(String tabloAdi, String uiValue) {
		this.islemLogBaslik = tabloAdi + " : " + uiValue;
	}

	private String referansId;

	private String tablo;

	@Override
	@PostConstruct
	public void init() {   
		if(FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("tablo") != null){  
			setTablo(FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("tablo"));       
			setReferansId(FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("referansId"));
			getIslemlogFilter().init();
			getIslemlogFilter().setTabloadi(getTablo());
			getIslemlogFilter().setReferansRID(Long.parseLong(getReferansId()));
			getIslemlogFilter().createQueryCriterias();
			BaseEntity obj = getDBOperator().find(getTablo(), "o.rID=" + getReferansId());
			setIslemLogBaslik(getTablo(), obj.getUIString());
			queryAction();
		} else if(getObjectFromSessionFilter("logEntity") != null){ 
			if(getObjectFromSessionFilter("logEntity") instanceof BaseEntity){ 
				BaseEntity logEntity = (BaseEntity) getObjectFromSessionFilter("logEntity");
				getIslemlogFilter().init();
				getIslemlogFilter().setTabloadi(logEntity.getClass().getSimpleName());
				getIslemlogFilter().setReferansRID(logEntity.getRID());
				getIslemlogFilter().createQueryCriterias(); 
				setIslemLogBaslik(logEntity.getClass().getSimpleName(), logEntity.getUIString());
				queryAction();
			}			
		}    
	}

	public String getReferansId() {
		return referansId;
	}

	public void setReferansId(String referansId) {
		this.referansId = referansId;
	}

	public String getTablo() {
		return tablo;
	}

	public void setTablo(String tablo) {
		this.tablo = tablo;
	} 

} // class 
