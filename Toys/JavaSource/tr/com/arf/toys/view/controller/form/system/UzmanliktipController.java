package tr.com.arf.toys.view.controller.form.system;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.framework.utility.exception.data.DBException;
import tr.com.arf.framework.utility.tool.KeyUtil;
import tr.com.arf.toys.db.filter.system.UzmanliktipFilter;
import tr.com.arf.toys.db.model.system.Uzmanliktip;
import tr.com.arf.toys.view.controller.form._base.ToysBaseFilteredController;

@ManagedBean
@ViewScoped
public class UzmanliktipController extends ToysBaseFilteredController {

	private static final long serialVersionUID = 6531152165884980748L;

	private Uzmanliktip uzmanliktip;
	private UzmanliktipFilter uzmanliktipFilter = new UzmanliktipFilter();

	public UzmanliktipController() {
		super(Uzmanliktip.class);
		setDefaultValues(false);
		setOrderField("kod");
		setAutoCompleteSearchColumns(new String[] { "kod" });
		setTable(null);
		queryAction();
	}

	public Uzmanliktip getUzmanliktip() {
		uzmanliktip = (Uzmanliktip) getEntity();
		return uzmanliktip;
	}

	public void setUzmanliktip(Uzmanliktip uzmanliktip) {
		this.uzmanliktip = uzmanliktip;
	}

	public UzmanliktipFilter getUzmanliktipFilter() {
		return uzmanliktipFilter;
	}

	public void setUzmanliktipFilter(UzmanliktipFilter uzmanliktipFilter) {
		this.uzmanliktipFilter = uzmanliktipFilter;
	}

	@Override
	public BaseFilter getFilter() {
		return getUzmanliktipFilter();
	}

	@Override
	public void save() {
		try {
			if (getDBOperator().recordCount(Uzmanliktip.class.getSimpleName(), "(LOWER(o.ad) = LOWER('" + getUzmanliktip().getAd() + "') OR UPPER(o.ad) = UPPER('" + getUzmanliktip().getAd() + "') ) ") > 0) {
				createGenericMessage(KeyUtil.getMessageValueWithParams("uzmanliktip.admukerrer", new Object[] { getUzmanliktip().getAd() }), FacesMessage.SEVERITY_ERROR);
				return;
			} else if (getDBOperator().recordCount(Uzmanliktip.class.getSimpleName(), "(LOWER(o.kod) = LOWER('" + getUzmanliktip().getKod() + "') OR UPPER(o.kod) = UPPER('" + getUzmanliktip().getKod() + "') )") > 0) {
				createGenericMessage(KeyUtil.getMessageValueWithParams("uzmanliktip.kodmukerrer", new Object[] { getUzmanliktip().getKod() }), FacesMessage.SEVERITY_ERROR);
				return;
			}
			super.save();
		} catch (DBException e) {
			logYaz("ERROR @save of "+ getModelName() + "Controller :" + e.getMessage());
			createGenericMessage(KeyUtil.getMessageValue("islem.hata"), FacesMessage.SEVERITY_ERROR);
		}
	}

} // class
