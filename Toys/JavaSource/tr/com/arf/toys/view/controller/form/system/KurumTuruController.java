package tr.com.arf.toys.view.controller.form.system;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.toys.db.filter.system.KurumTuruFilter;
import tr.com.arf.toys.db.model.system.KurumTuru;
import tr.com.arf.toys.view.controller.form._base.ToysBaseFilteredController;

@ManagedBean
@ViewScoped
public class KurumTuruController extends ToysBaseFilteredController {

	private static final long serialVersionUID = 6531152165884980748L;

	public KurumTuruController() {
		super(KurumTuru.class);
		setDefaultValues(false);
		setOrderField("ad");
		setAutoCompleteSearchColumns(new String[] { "ad" });
		setTable(null);
		queryAction();
	}

	private KurumTuru kurumTuru;
	private KurumTuruFilter kurumTuruFilter = new KurumTuruFilter();

	public KurumTuru getKurumTuru() {
		kurumTuru = (KurumTuru) getEntity();
		return kurumTuru;
	}

	public void setKurumTuru(KurumTuru kurumTuru) {
		this.kurumTuru = kurumTuru;
	}

	public KurumTuruFilter getKurumTuruFilter() {
		return kurumTuruFilter;
	}

	public void setKurumTuruFilter(KurumTuruFilter kurumTuruFilter) {
		this.kurumTuruFilter = kurumTuruFilter;
	}

	@Override
	public BaseFilter getFilter() {
		return getKurumTuruFilter();
	}

} // class 
