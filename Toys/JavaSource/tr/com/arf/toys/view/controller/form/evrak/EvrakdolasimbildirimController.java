package tr.com.arf.toys.view.controller.form.evrak; 
  
 
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.toys.db.filter.evrak.EvrakdolasimbildirimFilter;
import tr.com.arf.toys.db.model.evrak.Evrak;
import tr.com.arf.toys.db.model.evrak.Evrakdolasimbildirim;
import tr.com.arf.toys.service.application.ApplicationDescriptor;
import tr.com.arf.toys.view.controller.form._base.ToysBaseFilteredController;
  
  
@ManagedBean 
@ViewScoped 
public class EvrakdolasimbildirimController extends ToysBaseFilteredController {  
  
  private static final long serialVersionUID = 6531152165884980748L; 
  
	private Evrakdolasimbildirim evrakdolasimbildirim;  
	private EvrakdolasimbildirimFilter evrakdolasimbildirimFilter = new EvrakdolasimbildirimFilter();  
  
	public EvrakdolasimbildirimController() { 
		super(Evrakdolasimbildirim.class);  
		setDefaultValues(false);  
		setOrderField("rID");  
		setLoggable(false);  
		setAutoCompleteSearchColumns(new String[]{"aciklama"}); 
		setTable(null); 
		if(getMasterEntity() != null){ 
			setMasterObjectName(ApplicationDescriptor._EVRAK_MODEL);
			setMasterOutcome(ApplicationDescriptor._EVRAK_MODEL);
			getEvrakdolasimbildirimFilter().setEvrakRef(getMasterEntity());
			getEvrakdolasimbildirimFilter().createQueryCriterias();   
		} 
		queryAction(); 
	} 
 
	public Evrak getMasterEntity(){
		if(getEvrakdolasimbildirimFilter().getEvrakRef() != null){
			return getEvrakdolasimbildirimFilter().getEvrakRef();
		} else {
			return (Evrak) getObjectFromSessionFilter(ApplicationDescriptor._EVRAK_MODEL);
		}			
	}  
	
	
	public Evrakdolasimbildirim getEvrakdolasimbildirim() { 
		evrakdolasimbildirim = (Evrakdolasimbildirim) getEntity(); 
		return evrakdolasimbildirim; 
	} 
	
	public void setEvrakdolasimbildirim(Evrakdolasimbildirim evrakdolasimbildirim) { 
		this.evrakdolasimbildirim = evrakdolasimbildirim; 
	} 
	
	public EvrakdolasimbildirimFilter getEvrakdolasimbildirimFilter() { 
		return evrakdolasimbildirimFilter; 
	} 
	 
	public void setEvrakdolasimbildirimFilter(EvrakdolasimbildirimFilter evrakdolasimbildirimFilter) {  
		this.evrakdolasimbildirimFilter = evrakdolasimbildirimFilter;  
	}  
	 
	@Override  
	public BaseFilter getFilter() {  
		return getEvrakdolasimbildirimFilter();  
	}  
	
 
	@Override	
	public void insert() {	
		super.insert();	
		getEvrakdolasimbildirim().setEvrakRef(getEvrakdolasimbildirimFilter().getEvrakRef());	
	}	
	
} // class 