package tr.com.arf.toys.view.controller.form.uye;

import java.math.BigDecimal;
import java.math.MathContext;
import java.util.Calendar;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.framework.utility.tool.BigDecimalUtil;
import tr.com.arf.toys.db.enumerated.uye.UyeAidatDurum;
import tr.com.arf.toys.db.filter.uye.UyeaidatFilter;
import tr.com.arf.toys.db.model.uye.Uye;
import tr.com.arf.toys.db.model.uye.Uyeaidat;
import tr.com.arf.toys.service.application.ApplicationDescriptor;
import tr.com.arf.toys.service.application.ManagedBeanLocator;
import tr.com.arf.toys.utility.logUtils.LogService;
import tr.com.arf.toys.view.controller.form._base.ToysBaseFilteredController;

@ManagedBean
@ViewScoped
public class UyeaidatController extends ToysBaseFilteredController {

	private static final long serialVersionUID = 6531152165884980748L;

	private Uyeaidat uyeaidat;
	private UyeaidatFilter uyeaidatFilter = new UyeaidatFilter();

	public UyeaidatController() {
		super(Uyeaidat.class);
		setDefaultValues(false);
		setOrderField("yil, ay");
		setAutoCompleteSearchColumns(new String[] { "yil" });
		setTable(null);
		if (getMasterEntity() != null) {
			setMasterObjectName(ApplicationDescriptor._UYE_MODEL);
			setMasterOutcome(ApplicationDescriptor._UYE_MODEL);
			getUyeaidatFilter().setUyeRef(getMasterEntity());
			getUyeaidatFilter().createQueryCriterias();
		}
		queryAction();
	}

	public UyeaidatController(Object object) {
		super(Uyeaidat.class);
	}

	public Uye getMasterEntity() {
		if (getUyeaidatFilter().getUyeRef() != null) {
			return getUyeaidatFilter().getUyeRef();
		} else {
			return (Uye) getObjectFromSessionFilter(ApplicationDescriptor._UYE_MODEL);
		}
	}

	public Uyeaidat getUyeaidat() {
		uyeaidat = (Uyeaidat) getEntity();
		return uyeaidat;
	}

	public void setUyeaidat(Uyeaidat uyeaidat) {
		this.uyeaidat = uyeaidat;
	}

	public UyeaidatFilter getUyeaidatFilter() {
		return uyeaidatFilter;
	}

	public void setUyeaidatFilter(UyeaidatFilter uyeaidatFilter) {
		this.uyeaidatFilter = uyeaidatFilter;
	}

	@Override
	public BaseFilter getFilter() {
		return getUyeaidatFilter();
	}

	@Override
	public void insert() {
		super.insert();
		getUyeaidat().setUyeRef(getUyeaidatFilter().getUyeRef());
		getUyeaidat().setYil(Calendar.getInstance().get(Calendar.YEAR));
	}

	public String uyemuafiyet() {
		if (!setSelected()) {
			return "";
		}
		if (getUyeaidat().getUyemuafiyetRef() == null) {
			return "";
		}
		getSessionUser().setLegalAccess(1);
		putObjectToSessionFilter(ApplicationDescriptor._UYEAIDAT_MODEL, getUyeaidat());
		return ManagedBeanLocator.locateMenuController().gotoPage("menu_Uyemuafiyet");
	}

	public String uyeodeme() {
		if (!setSelected()) {
			return "";
		}
		getSessionUser().setLegalAccess(1);
		putObjectToSessionFilter(ApplicationDescriptor._UYEAIDAT_MODEL, getUyeaidat());
		return ManagedBeanLocator.locateMenuController().gotoPage("menu_Uyeodeme");
	}

	@Override
	public void save() {
		setUyeaidat(uyeAidatDurumGuncelle(getUyeaidat()));
		super.justSave();
	}

	public static Uyeaidat uyeAidatDurumGuncelle(Uyeaidat aidat) {
		try {
			BigDecimal kalan = BigDecimalUtil.changeToCurrency(aidat.getMiktar().subtract(aidat.getOdenen(), MathContext.DECIMAL32));
			if (aidat.getUyemuafiyetRef() != null) {
				if (aidat.getUyeaidatdurum() != UyeAidatDurum._MUAF) {
					aidat.setUyeaidatdurum(UyeAidatDurum._MUAF);
				}
			} else {
				if (kalan.doubleValue() > 0) {
					if (aidat.getUyeaidatdurum() != UyeAidatDurum._BORCLU) {
						aidat.setUyeaidatdurum(UyeAidatDurum._BORCLU);
					}
				} else if (kalan.doubleValue() == 0) {
					if (aidat.getUyeaidatdurum() != UyeAidatDurum._ODENMIS) {
						aidat.setUyeaidatdurum(UyeAidatDurum._ODENMIS);
					}
				} else if (kalan.doubleValue() < 0) {
					if (aidat.getUyeaidatdurum() != UyeAidatDurum._ALACAKLI) {
						aidat.setUyeaidatdurum(UyeAidatDurum._ALACAKLI);
					}
				}
			}
		} catch (Exception e) {
			LogService.logYaz("Exception @UyeaidatController :", e);
		}
		return aidat;
	}

	public void uyeTumAidatDurumGuncelle(Long uyeRID) {
		// Veride olusabilecek tutarsizliklari gidermek icin, odenen-borc durumlarina gore uye aidat kayitlarini gunceller.
		String query = "UPDATE " + Uyeaidat.class.getSimpleName() + " o SET o.uyeaidatdurum=" + UyeAidatDurum._ODENMIS.getCode() + " WHERE o.odenen = o.miktar AND o.uyeaidatdurum <> " + UyeAidatDurum._MUAF.getCode() + " AND o.uyeRef.rID = " + uyeRID;
		try {
			getDBOperator().executeQuery(query);
			query = "UPDATE " + Uyeaidat.class.getSimpleName() + " o SET o.uyeaidatdurum=" + UyeAidatDurum._ALACAKLI.getCode() + " WHERE o.odenen > o.miktar AND o.uyeaidatdurum <> " + UyeAidatDurum._MUAF.getCode() + " AND o.uyeRef.rID = " + uyeRID;
			getDBOperator().executeQuery(query);

			query = "UPDATE " + Uyeaidat.class.getSimpleName() + " o SET o.uyeaidatdurum=" + UyeAidatDurum._BORCLU.getCode() + " WHERE o.odenen < o.miktar AND o.uyeaidatdurum <> " + UyeAidatDurum._MUAF.getCode() + " AND o.uyeRef.rID = " + uyeRID;
			getDBOperator().executeQuery(query);
		} catch (Exception e) {
			logYaz("ERROR @uyeAidatDurumGuncelle :" + e.getMessage());
		}
	}
} // class
