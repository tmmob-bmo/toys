package tr.com.arf.toys.view.controller.form._base;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map.Entry;

import javax.faces.FacesException;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.model.SelectItem;

import org.primefaces.component.export.Exporter;
import org.primefaces.event.SelectEvent;

import tr.com.arf.framework.db.enumerated._common.EvetHayir;
import tr.com.arf.framework.db.model._base.BaseEntity;
import tr.com.arf.framework.utility.exception.data.DBException;
import tr.com.arf.framework.view.controller.form._base.BaseController;
import tr.com.arf.toys.db.enumerated.iletisim.AdresTipi;
import tr.com.arf.toys.db.model.system.Fakulte;
import tr.com.arf.toys.db.model.system.FakulteBolum;
import tr.com.arf.toys.db.model.system.Ilce;
import tr.com.arf.toys.db.model.system.IslemKullanici;
import tr.com.arf.toys.db.model.system.Koy;
import tr.com.arf.toys.db.model.system.Sehir;
import tr.com.arf.toys.db.model.system.Ulke;
import tr.com.arf.toys.db.model.system.Universite;
import tr.com.arf.toys.db.model.system.UniversiteFakulte;
import tr.com.arf.toys.service.application.ManagedBeanLocator;
import tr.com.arf.toys.utility.tool.OpenOfficeExporter;
import tr.com.arf.toys.utility.tool.WordExporter;
import tr.com.arf.toys.view.controller._common.SessionUser;

public abstract class ToysBaseController extends BaseController {

	private static final long serialVersionUID = 3483304400136924177L;
	private final SessionUser sessionUser = ManagedBeanLocator.locateSessionUser();

	private final HashMap<String, IslemKullanici> islemMap = ManagedBeanLocator.locateSessionUser().getIslemMap();

	protected ToysBaseController(@SuppressWarnings("rawtypes") Class c) {
		super(c);
	}

	@Override
	public boolean isListRendered() {
		if (sessionUser.isSuperUser()) {
			return true;
		}
		try {
			return islemMap.get(getModelName()).getListPermission() == EvetHayir._EVET;
		} catch (NullPointerException e) {
			return false;
		}
	}

	@Override
	public boolean listRenderedFor(String modelNameForListControl) {
		if (sessionUser.isSuperUser()) {
			return true;
		}
		try {
			return islemMap.get(modelNameForListControl).getListPermission() == EvetHayir._EVET;
		} catch (NullPointerException e) {
			return false;
		}
	}

	@Override
	public boolean isUpdateRecordRendered() {
		if (sessionUser.isSuperUser()) {
			return true;
		}
		try {
			return islemMap.get(getModelName()).getUpdatePermission() == EvetHayir._EVET;
		} catch (NullPointerException e) {
			return false;
		}
	}

	@Override
	public boolean updateRecordRenderedFor(String modelNameForListControl) {
		if (sessionUser.isSuperUser()) {
			return true;
		}
		try {
			return islemMap.get(modelNameForListControl).getUpdatePermission() == EvetHayir._EVET;
		} catch (NullPointerException e) {
			return false;
		}
	}

	@Override
	public boolean isDeleteRendered() {
		if (sessionUser.isSuperUser()) {
			return true;
		}
		try {
			return islemMap.get(getModelName()).getDeletePermission() == EvetHayir._EVET;
		} catch (NullPointerException e) {
			return false;
		}
	}

	@Override
	public boolean deleteRenderedFor(String modelNameForListControl) {
		if (sessionUser.isSuperUser()) {
			return true;
		}
		try {
			return islemMap.get(modelNameForListControl).getDeletePermission() == EvetHayir._EVET;
		} catch (NullPointerException e) {
			return false;
		}
	}

	@Override
	public void setSelectedRID(String selectedRID) {
		super.selectedRID = selectedRID;
		setEntity((BaseEntity) getDBOperator().find(getModelName(), "rID", selectedRID).get(0));
		if (loggable) {
			if (getEntity() != null) {
				putObjectToSessionFilter("logEntity", getEntity());
			}
		}
	}

	@Override
	public void tableRowSelect(SelectEvent event) {
		setEditPanelRendered(false);
		setEntity((BaseEntity) event.getObject());
		if (loggable) {
			if (getEntity() != null) {
				putObjectToSessionFilter("logEntity", getEntity());
			}
		}
	}

	@Override
	public SessionUser getSessionUser() {
		return sessionUser;
	}

	@Override
	public void putObjectToSessionFilter(String objectName, Object object) {
		getSessionUser().addToSessionFilters(objectName, object);
	}

	@Override
	public Object getObjectFromSessionFilter(String objectName) {
		return getSessionUser().getSessionFilterObject(objectName);
	}

	@Override
	public void removeObjectFromSessionFilter(String objectName) {
		getSessionUser().removeSessionFilterObject(objectName);
	}

	@Override
	public String detailPage(Long selectedRID) {
		String result = super.detailPage(selectedRID);
		ManagedBeanLocator.locateSessionUser().addToSessionFilters(getModelName(), getEntity());
		return result;
	}

	@Override
	protected BaseEntity checkSessionForPreSelected() {
		if (getSessionUser().getSessionFilterItemSize() == 0) {
			return null;
		}

		for (Entry<String, Object> entry : getSessionUser().getSessionFilters().entrySet()) {
			if (entry.getValue().getClass().getSimpleName().equalsIgnoreCase(getModelName())) {
				return (BaseEntity) entry.getValue();
			}
		}
		return null;
	}

	public void wordExport() {
		String encodingType = "UTF-8";
		FacesContext facesContext = FacesContext.getCurrentInstance();
		try {
			String outputFileName = getModelName().toLowerCase(Locale.ENGLISH) + "Listesi";
			Exporter exporter = new WordExporter();
			int[] excludedColumns = new int[0];
			exporter.export(facesContext, getTable(), outputFileName, false, false, excludedColumns, encodingType, null, null);
			facesContext.responseComplete();
		} catch (IOException e) {
			throw new FacesException(e);
		}
	}

	public void openOfficeExport() {
		String encodingType = "UTF-8";
		FacesContext facesContext = FacesContext.getCurrentInstance();
		try {
			String outputFileName = getModelName().toLowerCase(Locale.ENGLISH) + "Listesi";
			Exporter exporter = new OpenOfficeExporter();
			int[] excludedColumns = new int[0];
			exporter.export(facesContext, getTable(), outputFileName, false, false, excludedColumns, encodingType, null, null);
			facesContext.responseComplete();
		} catch (IOException e) {
			throw new FacesException(e);
		}
	}

	@Override
	public String getStaticWhereCondition(String modelName) {
		return getSessionUser().getStaticWhereCondition(modelName);
	}

	@Override
	public String[] getAutoCompleteSearchColumns(String modelName) {
		return getSessionUser().getAutoCompleteSearchColumns(modelName);
	}

	public SelectItem[] ilceItemList;
	private SelectItem[] mahalleItemList;
	public SelectItem[] ilItemList;
	public SelectItem[] fakulteItemList;
	public SelectItem[] bolumItemList;
	public SelectItem[] ulkeItemList;

	@Override
	public void handleChange(AjaxBehaviorEvent event) {
		try {
			HtmlSelectOneMenu menu = (HtmlSelectOneMenu) event.getComponent();
			if (menu.getValue() instanceof Sehir) {
				changeIlce(((Sehir) menu.getValue()).getRID());
			} else if (menu.getValue() instanceof Ilce) {
				changeMahalle(((Ilce) menu.getValue()).getRID());
			} else if (menu.getValue() instanceof Ulke) {
				changeIl(((Ulke) menu.getValue()).getRID());
			} else if (menu.getValue() instanceof AdresTipi) {
				changeUlke((AdresTipi) menu.getValue());
			} else if (menu.getValue() instanceof Universite) {
				changeFakulte(((Universite) menu.getValue()).getRID());
			} else if (menu.getValue() instanceof Fakulte) {
				changeBolum(((Fakulte) menu.getValue()).getRID());
			}
		} catch (Exception e) {
			logYaz("Exception @" + getModelName() + "Controller :" + e.getMessage());
		}
	}

	protected void changeIlce(Long selectedRecId) {
		if (selectedRecId.longValue() == 0) {
			setIlceItemList(new SelectItem[0]);
		} else {
			@SuppressWarnings("unchecked")
			List<Ilce> entityList = getDBOperator().load(Ilce.class.getSimpleName(), "o.sehirRef.rID=" + selectedRecId, "o.ad");
			if (entityList != null) {
				ilceItemList = new SelectItem[entityList.size() + 1];
				ilceItemList[0] = new SelectItem(null, "");
				int i = 1;
				for (Ilce entity : entityList) {
					ilceItemList[i++] = new SelectItem(entity, entity.getUIString());
				}
			} else {
				setIlceItemList(new SelectItem[0]);
			}
		}
	}

	protected void changeMahalle(Long selectedRecId) {
		if (selectedRecId.longValue() == 0) {
			setMahalleItemList(new SelectItem[0]);
		} else {
			@SuppressWarnings("unchecked")
			List<Koy> entityList = getDBOperator().load(Koy.class.getSimpleName(), "o.ilceRef.rID=" + selectedRecId, "o.rID");
			if (entityList != null) {
				mahalleItemList = new SelectItem[entityList.size() + 1];
				mahalleItemList[0] = new SelectItem(null, "");
				int i = 1;
				for (Koy entity : entityList) {
					mahalleItemList[i++] = new SelectItem(entity, entity.getUIString());
				}
			} else {
				setMahalleItemList(new SelectItem[0]);
			}
		}
	}

	protected void changeUlke(AdresTipi adresTipi) {
		if (adresTipi == null || adresTipi == AdresTipi._NULL) {
			setUlkeItemList(new SelectItem[0]);
		} else {
			String whereCon = "";
			if (adresTipi == AdresTipi._YURTDISIADRESI) {
				whereCon = "o.rID > " + 1;
			} else {
				whereCon = "o.rID = " + 1;
			}
			@SuppressWarnings("unchecked")
			List<Ulke> entityList = getDBOperator().load(Ulke.class.getSimpleName(), whereCon, "o.ad");
			if (entityList != null) {
				ulkeItemList = new SelectItem[entityList.size() + 1];
				ulkeItemList[0] = new SelectItem(null, "");
				int i = 1;
				for (Ulke entity : entityList) {
					ulkeItemList[i++] = new SelectItem(entity, entity.getUIString());
				}
			} else {
				setUlkeItemList(new SelectItem[0]);
			}
		}
	}

	protected void changeIl(Long selectedRecId) {
		if (selectedRecId.longValue() == 0) {
			setIlceItemList(new SelectItem[0]);
		} else {
			@SuppressWarnings("unchecked")
			// List<Sehir> entityList =
			// getDBOperator().load(Sehir.class.getSimpleName(),
			// "o.ulkeRef.rID=" + selectedRecId ,"o.ad");
			List<Sehir> entityList = getDBOperator().load(Sehir.class.getSimpleName(), "", "o.ad");
			if (entityList != null) {
				ilItemList = new SelectItem[entityList.size() + 1];
				ilItemList[0] = new SelectItem(null, "");
				int i = 1;
				for (Sehir entity : entityList) {
					ilItemList[i++] = new SelectItem(entity, entity.getUIString());
				}
			} else {
				setIlItemList(new SelectItem[0]);
			}
		}
	}

	@SuppressWarnings("unchecked")
	protected void changeFakulte(Long selectedRecId) {
		if (selectedRecId.longValue() == 0) {
			setFakulteItemList(new SelectItem[0]);
		} else {

			try {
				if (getDBOperator().recordCount(UniversiteFakulte.class.getSimpleName(), "o.universiteRef.rID=" + selectedRecId) > 0) {
					List<UniversiteFakulte> entityList = getDBOperator().load(UniversiteFakulte.class.getSimpleName(), "o.universiteRef.rID=" + selectedRecId, "");
					fakulteItemList = new SelectItem[entityList.size() + 1];
					fakulteItemList[0] = new SelectItem(null, "");
					int i = 1;
					for (UniversiteFakulte entity : entityList) {
						fakulteItemList[i++] = new SelectItem(entity.getFakulteRef(), entity.getFakulteRef().getUIString());
					}
				} else {
					setFakulteItemList(new SelectItem[0]);
				}
			} catch (DBException e) {
				logYaz("Exception @" + getModelName() + "Controller :" + e.getMessage());
			}

		}
	}

	protected void changeBolum(Long selectedRecId) {
		if (selectedRecId.longValue() == 0) {
			setBolumItemList(new SelectItem[0]);
		} else {
			try {
				if (getDBOperator().recordCount(UniversiteFakulte.class.getSimpleName(), "o.fakulteRef.rID=" + selectedRecId) > 0) {
					@SuppressWarnings("unchecked")
					List<FakulteBolum> entityList = getDBOperator().load(FakulteBolum.class.getSimpleName(), "o.fakulteRef.rID=" + selectedRecId, "o.rID");
					if (entityList != null) {
						bolumItemList = new SelectItem[entityList.size() + 1];
						bolumItemList[0] = new SelectItem(null, "");
						int i = 1;
						for (FakulteBolum entity : entityList) {
							bolumItemList[i++] = new SelectItem(entity.getBolumRef(), entity.getUIString());
						}
					} else {
						setBolumItemList(new SelectItem[0]);
					}
				}
			} catch (DBException e) {
				logYaz("Exception @" + getModelName() + "Controller :" + e.getMessage());
			}
		}
	}

	public SelectItem[] ilceItemList() {
		return getIlceItemList();
	}

	public SelectItem[] getIlceItemList() {
		return ilceItemList;
	}

	public void setIlceItemList(SelectItem[] ilceItemList) {
		this.ilceItemList = ilceItemList;
	}

	public SelectItem[] getMahalleItemList() {
		return mahalleItemList;
	}

	public void setMahalleItemList(SelectItem[] mahalleItemList) {
		this.mahalleItemList = mahalleItemList;
	}

	public SelectItem[] getIlItemList() {
		return ilItemList;
	}

	public void setIlItemList(SelectItem[] ilItemList) {
		this.ilItemList = ilItemList;
	}

	public SelectItem[] getUlkeItemList() {
		return ulkeItemList;
	}

	public void setUlkeItemList(SelectItem[] ulkeItemList) {
		this.ulkeItemList = ulkeItemList;
	}

	public SelectItem[] getFakulteItemList() {
		return fakulteItemList;
	}

	public void setFakulteItemList(SelectItem[] fakulteItemList) {
		this.fakulteItemList = fakulteItemList;
	}

	public SelectItem[] getBolumItemList() {
		return bolumItemList;
	}

	public void setBolumItemList(SelectItem[] bolumItemList) {
		this.bolumItemList = bolumItemList;
	}

	@Override
	public String detaySayfa(String outcome) {
		logYaz(outcome + "'a gidiyor...");
		return super.detaySayfa(outcome);
	}

}