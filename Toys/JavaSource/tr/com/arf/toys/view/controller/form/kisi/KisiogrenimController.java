package tr.com.arf.toys.view.controller.form.kisi;


import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.toys.db.filter.kisi.KisiogrenimFilter;
import tr.com.arf.toys.db.model.kisi.Kisi;
import tr.com.arf.toys.db.model.kisi.Kisiogrenim;
import tr.com.arf.toys.service.application.ApplicationDescriptor;
import tr.com.arf.toys.view.controller.form._base.ToysBaseFilteredController;


@ManagedBean
@ViewScoped
public class KisiogrenimController extends ToysBaseFilteredController {

  private static final long serialVersionUID = 6531152165884980748L;

	private Kisiogrenim kisiogrenim;
	private KisiogrenimFilter kisiogrenimFilter = new KisiogrenimFilter();

	public KisiogrenimController() {
		super(Kisiogrenim.class);
		setDefaultValues(false);
		setTable(null);
		if(getMasterEntity() != null){
			setMasterObjectName(ApplicationDescriptor._KISI_MODEL);
			setMasterOutcome(ApplicationDescriptor._KISI_MODEL);
			getKisiogrenimFilter().setKisiRef(getMasterEntity());
			getKisiogrenimFilter().createQueryCriterias();
		}
		queryAction();
	}

	public KisiogrenimController(Object object) {
		super(Kisiogrenim.class);
	}

	public Kisi getMasterEntity(){
		if(getKisiogrenimFilter().getKisiRef() != null){
			return getKisiogrenimFilter().getKisiRef();
		} else {
			return (Kisi) getObjectFromSessionFilter(ApplicationDescriptor._KISI_MODEL);
		}
	}

	public Kisiogrenim getKisiogrenim() {
		this.kisiogrenim=(Kisiogrenim) getEntity();
		return kisiogrenim;
	}

	public void setKisiogrenim(Kisiogrenim kisiogrenim) {
		this.kisiogrenim = kisiogrenim;
	}

	public KisiogrenimFilter getKisiogrenimFilter() {
		return kisiogrenimFilter;
	}

	public void setKisiogrenimFilter(KisiogrenimFilter kisiogrenimFilter) {
		this.kisiogrenimFilter = kisiogrenimFilter;
	}

	@Override
	public BaseFilter getFilter() {
		return getKisiogrenimFilter();
	}

	@Override
	public void insert() {
		super.insert();
		getKisiogrenim().setKisiRef(getKisiogrenimFilter().getKisiRef());
	}
} // class
