package tr.com.arf.toys.view.controller.form.system;

import java.io.IOException;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.framework.db.model._base.BaseEntity;
import tr.com.arf.framework.utility.exception.data.DBException;
import tr.com.arf.framework.utility.security.ScryptPasswordHashing;
import tr.com.arf.framework.utility.tool.KeyUtil;
import tr.com.arf.toys.db.filter.system.KullaniciFilter;
import tr.com.arf.toys.db.model.system.Kullanici;
import tr.com.arf.toys.db.model.system.Personel;
import tr.com.arf.toys.service.application.ApplicationDescriptor;
import tr.com.arf.toys.service.application.ManagedBeanLocator;
import tr.com.arf.toys.view.controller.form._base.ToysBaseFilteredController;

@ManagedBean
@ViewScoped
public class KullaniciController extends ToysBaseFilteredController {

	private static final long serialVersionUID = 6531152165884980748L;

	private Kullanici kullanici;
	private KullaniciFilter kullaniciFilter = new KullaniciFilter();

	public KullaniciController() {
		super(Kullanici.class);
		setDefaultValues(false);
		setOrderField("name");
		setAutoCompleteSearchColumns(new String[] { "name" });
		setTable(null);
		if (getObjectFromSessionFilter("KullaniciSifreDegistir") == null) {
			// Sifre degistirme sayfasi cagrilmamissa sorgu yapilir.
			queryAction();
		}
	}

	@Override
	@PostConstruct
	public void init() {
		super.init();
		if (getObjectFromSessionFilter("KullaniciSifreDegistir") != null) {
			// Sifre degistirme sayfasi cagrilmis
			setKullanici((Kullanici) getObjectFromSessionFilter("KullaniciSifreDegistir"));
			setEntity((BaseEntity) getObjectFromSessionFilter("KullaniciSifreDegistir"));
		}
	}

	public Kullanici getKullanici() {
		kullanici = (Kullanici) getEntity();
		return kullanici;
	}

	public void setKullanici(Kullanici kullanici) {
		this.kullanici = kullanici;
	}

	public KullaniciFilter getKullaniciFilter() {
		return kullaniciFilter;
	}

	public void setKullaniciFilter(KullaniciFilter kullaniciFilter) {
		this.kullaniciFilter = kullaniciFilter;
	}

	@Override
	public BaseFilter getFilter() {
		return getKullaniciFilter();
	}

	@SuppressWarnings("unchecked")
	public SelectItem[] getSelectPersonelItemList() {
		List<Personel> entityList = getDBOperator().load(Personel.class.getSimpleName(), " o.rID NOT IN (Select m.personelRef.rID FROM Kullanici m)", "o.ad");
		if (entityList != null) {
			SelectItem[] selectItemList = new SelectItem[entityList.size() + 1];
			selectItemList[0] = new SelectItem(null, "");
			int i = 1;
			for (Personel entity : entityList) {
				selectItemList[i++] = new SelectItem(entity, entity.getUIString());
			}
			return selectItemList;
		}
		return new SelectItem[0];
	}

	public String kullaniciRole() {
		if (!setSelected()) {
			return "";
		}
		putObjectToSessionFilter(ApplicationDescriptor._KULLANICI_MODEL, getKullanici());
		return ManagedBeanLocator.locateMenuController().gotoPage("menu_KullaniciRole");
	}

	@Override
	public void save() {
		String sifre;
		try {
			sifre = ScryptPasswordHashing.encrypt(kullanici.getPassword());
			kullanici.setPassword(sifre);
			if (getKullanici().getKisiRef() != null) {
				kullanici.setName(getKullanici().getKisiRef().getKimlikno().toString());
			}
			super.save();
		} catch (Exception e) {
			createGenericMessage("Şifreleme hatası!", FacesMessage.SEVERITY_ERROR);
			return;
		}
	}

	@Override
	public void update() {
		super.update();
		kullanici.setPassword("");
	}

	public String sifredegistirSayfa() {
		putObjectToSessionFilter("KullaniciSifreDegistir", getSessionUser().getKullanici());
		return ApplicationDescriptor._PASSWORDCHANGE_OUTCOME;
	}

	public void sifreDegistir() throws IOException {
		if (!getKullanici().getNewPassword().equals(getKullanici().getNewPasswordCheck())) {
			createCustomMessage(KeyUtil.getMessageValue("yenisifre.eslesmeHata"), FacesMessage.SEVERITY_ERROR, "newPasswordCheckInputText");
			return;
		} else if (!ScryptPasswordHashing.check(getKullanici().getPrevPassword(), getKullanici().getPassword())) {
			createCustomMessage(KeyUtil.getMessageValue("eskisifre.hata"), FacesMessage.SEVERITY_ERROR, "prevPasswordInputText");
			return;
		} else if (getKullanici().getPrevPassword().equals(getKullanici().getNewPassword())) {
			createCustomMessage(KeyUtil.getMessageValue("yenisifre.eskisifreTekrar"), FacesMessage.SEVERITY_ERROR, "newPasswordCheckInputText");
			return;
		} else {
			getKullanici().setPassword(ScryptPasswordHashing.encrypt(getKullanici().getNewPassword()));
			try {
				getDBOperator().update(getKullanici());
				getSessionUser().setMessage(KeyUtil.getMessageValue("sifreniz.degistirildi"));
				ExternalContext ectx = FacesContext.getCurrentInstance().getExternalContext();
					ectx.setRequest(ectx.getRequestContextPath() + ectx.getRequestServletPath());
					ectx.redirect(ectx.getRequestContextPath() + ectx.getRequestServletPath() + "/_page/_main/dashboard.xhtml");
			} catch (DBException e) {
				createGenericMessage(KeyUtil.getMessageValue("islem.hata"), FacesMessage.SEVERITY_ERROR);
			}
			setTable(null);
		}

	}

    public void blokKaldir(){
        getKullanici().setLoginAttemptCount(0);
        try {
            getDBOperator().update(getKullanici());
            getSessionUser().setMessage(KeyUtil.getMessageValue("Kullanici Uzerindeki Blok Kaldirildi"));
        } catch (DBException e) {
            e.printStackTrace();
        }

    }

} // class
