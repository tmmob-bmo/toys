package tr.com.arf.toys.view.controller.form.uye; 
  
 
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.framework.db.model._base.BaseEntity;
import tr.com.arf.toys.db.filter.uye.UyesigortaFilter;
import tr.com.arf.toys.db.model.uye.Uye;
import tr.com.arf.toys.db.model.uye.Uyesigorta;
import tr.com.arf.toys.service.application.ApplicationDescriptor;
import tr.com.arf.toys.view.controller.form._base.ToysBaseFilteredController;
  
  
@ManagedBean 
@ViewScoped 
public class UyesigortaController extends ToysBaseFilteredController {  
  
  private static final long serialVersionUID = 6531152165884980748L; 
  
	private Uyesigorta uyesigorta;  
	private UyesigortaFilter uyesigortaFilter = new UyesigortaFilter();  
  
	public UyesigortaController() { 
		super(Uyesigorta.class);  
		setDefaultValues(false);  
		setOrderField("sigortasirket");  
		setAutoCompleteSearchColumns(new String[]{"sigortasirket"}); 
		setTable(null); 
		if(getMasterEntity() != null && getMasterEntity().getClass() == Uye.class){ 
			setMasterObjectName(ApplicationDescriptor._UYE_MODEL);
			setMasterOutcome(ApplicationDescriptor._UYE_MODEL);	
			getUyesigortaFilter().setUyeRef((Uye) getMasterEntity());  
			getUyesigortaFilter().createQueryCriterias();
		}
		queryAction(); 
	} 
 
	public UyesigortaController(Object object) { 
		super(Uyesigorta.class);  
	}
	
	public BaseEntity getMasterEntity(){
		 if(getObjectFromSessionFilter(ApplicationDescriptor._UYE_MODEL) != null){
			return (Uye) getObjectFromSessionFilter(ApplicationDescriptor._UYE_MODEL);
		} else {
			return null;
		}			
	}  
	
	public Uyesigorta getUyesigorta() { 
		uyesigorta = (Uyesigorta) getEntity(); 
		return uyesigorta; 
	} 
	
	public void setUyesigorta(Uyesigorta uyesigorta) { 
		this.uyesigorta = uyesigorta; 
	} 
	
	public UyesigortaFilter getUyesigortaFilter() { 
		return uyesigortaFilter; 
	} 
	 
	public void setUyesigortaFilter(UyesigortaFilter uyesigortaFilter) {  
		this.uyesigortaFilter = uyesigortaFilter;  
	}  
	 
	@Override  
	public BaseFilter getFilter() {  
		return getUyesigortaFilter();  
	}  
	
	@Override
	public void insert(){
		super.insert();
		getUyesigorta().setUyeRef((Uye) getMasterEntity());
	}
	
} // class 
