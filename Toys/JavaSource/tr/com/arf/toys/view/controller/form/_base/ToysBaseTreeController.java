package tr.com.arf.toys.view.controller.form._base;

import java.util.List;

import javax.faces.model.SelectItem;

import tr.com.arf.framework.view.controller.form._base.BaseTreeController;
import tr.com.arf.toys.db.enumerated.iletisim.AdresTipi;
import tr.com.arf.toys.db.model.system.Bolum;
import tr.com.arf.toys.db.model.system.Fakulte;
import tr.com.arf.toys.db.model.system.Ilce;
import tr.com.arf.toys.db.model.system.Koy;
import tr.com.arf.toys.db.model.system.Sehir;
import tr.com.arf.toys.db.model.system.Ulke;
import tr.com.arf.toys.service.application.ManagedBeanLocator;
import tr.com.arf.toys.view.controller._common.SessionUser;

public abstract class ToysBaseTreeController extends BaseTreeController {

	protected ToysBaseTreeController(Class<?> c) {
		super(c);
	}

	private static final long serialVersionUID = -848374101934771284L;

	@Override
	public SessionUser getSessionUser(){
		return ManagedBeanLocator.locateSessionUser();
	}

	public SelectItem[] ilceItemList;
	private SelectItem[] mahalleItemList;
	public SelectItem[] ilItemList;
	public SelectItem[] fakulteItemList;
	public SelectItem[] bolumItemList;
	public SelectItem[] ulkeItemList;

	protected void changeIlce(Long selectedRecId){
		if(selectedRecId.longValue() == 0){
			setIlceItemList(new SelectItem[0]);
		} else {
			@SuppressWarnings("unchecked")
			List<Ilce> entityList = getDBOperator().load(Ilce.class.getSimpleName(), "o.sehirRef.rID=" + selectedRecId ,"o.ad");
			if(entityList != null){
				ilceItemList = new SelectItem[entityList.size() + 1];
				ilceItemList[0] = new SelectItem(null, "");
				int i = 1;
				for(Ilce entity : entityList) {
					ilceItemList[i++] = new SelectItem(entity, entity.getUIString());
				}
			} else {
				setIlceItemList(new SelectItem[0]);
			}
		}
	}


	protected void changeMahalle(Long selectedRecId){
		if(selectedRecId.longValue() == 0){
			setMahalleItemList(new SelectItem[0]);
		} else {
			@SuppressWarnings("unchecked")
			List<Koy> entityList = getDBOperator().load(Koy.class.getSimpleName(), "o.ilceRef.rID=" + selectedRecId ,"o.rID");
			if(entityList != null){
				mahalleItemList = new SelectItem[entityList.size() + 1];
				mahalleItemList[0] = new SelectItem(null, "");
				int i = 1;
				for(Koy entity : entityList) {
					mahalleItemList[i++] = new SelectItem(entity, entity.getUIString());
				}
			} else {
				setMahalleItemList(new SelectItem[0]);
			}
		}
	}

	protected void changeUlke(AdresTipi adresTipi){
		if(adresTipi == null || adresTipi == AdresTipi._NULL){
			setUlkeItemList(new SelectItem[0]);
		} else {
			String whereCon = "";
			if(adresTipi == AdresTipi._YURTDISIADRESI){
				whereCon = "o.rID > " + 1;
			} else {
				whereCon = "o.rID = " + 1;
			}
			@SuppressWarnings("unchecked")
			List<Ulke> entityList = getDBOperator().load(Ulke.class.getSimpleName(), whereCon,"o.ad");
			if(entityList != null){
				ulkeItemList = new SelectItem[entityList.size() + 1];
				ulkeItemList[0] = new SelectItem(null, "");
				int i = 1;
				for(Ulke entity : entityList) {
					ulkeItemList[i++] = new SelectItem(entity, entity.getUIString());
				}
			} else {
				setUlkeItemList(new SelectItem[0]);
			}
		}
	}

	protected void changeIl(Long selectedRecId){
		if(selectedRecId.longValue() == 0){
			setIlceItemList(new SelectItem[0]);
		} else {
			@SuppressWarnings("unchecked")
//			List<Sehir> entityList = getDBOperator().load(Sehir.class.getSimpleName(), "o.ulkeRef.rID=" + selectedRecId ,"o.ad");
			List<Sehir> entityList = getDBOperator().load(Sehir.class.getSimpleName(), "" ,"o.ad");
			if(entityList != null){
				ilItemList = new SelectItem[entityList.size() + 1];
				ilItemList[0] = new SelectItem(null, "");
				int i = 1;
				for(Sehir entity : entityList) {
					ilItemList[i++] = new SelectItem(entity, entity.getUIString());
				}
			} else {
				setIlItemList(new SelectItem[0]);
			}
		}
	}

	protected void changeFakulte(Long selectedRecId){
		if(selectedRecId.longValue() == 0){
			setIlceItemList(new SelectItem[0]);
		} else {
			@SuppressWarnings("unchecked")
			List<Fakulte> entityList = getDBOperator().load(Fakulte.class.getSimpleName(), "o.universiteRef.rID=" + selectedRecId ,"o.ad");
			if(entityList != null){
				fakulteItemList = new SelectItem[entityList.size() + 1];
				fakulteItemList[0] = new SelectItem(null, "");
				int i = 1;
				for(Fakulte entity : entityList) {
					fakulteItemList[i++] = new SelectItem(entity, entity.getUIString());
				}
			} else {
				setFakulteItemList(new SelectItem[0]);
			}
		}
	}

	protected void changeBolum(Long selectedRecId){
		if(selectedRecId.longValue() == 0){
			setIlceItemList(new SelectItem[0]);
		} else {
			@SuppressWarnings("unchecked")
			List<Bolum> entityList = getDBOperator().load(Bolum.class.getSimpleName(), "o.fakulteRef.rID=" + selectedRecId ,"o.ad");
			if(entityList != null){
				bolumItemList = new SelectItem[entityList.size() + 1];
				bolumItemList[0] = new SelectItem(null, "");
				int i = 1;
				for(Bolum entity : entityList) {
					bolumItemList[i++] = new SelectItem(entity, entity.getUIString());
				}
			} else {
				setBolumItemList(new SelectItem[0]);
			}
		}
	}

	public SelectItem[] ilceItemList() {
		return getIlceItemList();
	}

	public SelectItem[] getIlceItemList() {
		return ilceItemList;
	}

	public void setIlceItemList(SelectItem[] ilceItemList) {
		this.ilceItemList = ilceItemList;
	}

	public SelectItem[] getMahalleItemList() {
		return mahalleItemList;
	}

	public void setMahalleItemList(SelectItem[] mahalleItemList) {
		this.mahalleItemList = mahalleItemList;
	}

	public SelectItem[] getIlItemList() {
		return ilItemList;
	}

	public void setIlItemList(SelectItem[] ilItemList) {
		this.ilItemList = ilItemList;
	}

	public SelectItem[] getUlkeItemList() {
		return ulkeItemList;
	}

	public void setUlkeItemList(SelectItem[] ulkeItemList) {
		this.ulkeItemList = ulkeItemList;
	}

	public SelectItem[] getFakulteItemList() {
		return fakulteItemList;
	}

	public void setFakulteItemList(SelectItem[] fakulteItemList) {
		this.fakulteItemList = fakulteItemList;
	}

	public SelectItem[] getBolumItemList() {
		return bolumItemList;
	}

	public void setBolumItemList(SelectItem[] bolumItemList) {
		this.bolumItemList = bolumItemList;
	}

	@Override
	public String getStaticWhereCondition(String modelName) {
		return getSessionUser().getStaticWhereCondition(modelName);
	}

	@Override
	public String[] getAutoCompleteSearchColumns(String modelName) {
		return getSessionUser().getAutoCompleteSearchColumns(modelName);
	}

	@Override
	public void removeObjectFromSessionFilter(String objectName){
		getSessionUser().removeSessionFilterObject(objectName);
	}

	@Override
	public void putObjectToSessionFilter(String objectName, Object object){
		getSessionUser().addToSessionFilters(objectName, object);
	}

	@Override
	public Object getObjectFromSessionFilter(String objectName){
		return getSessionUser().getSessionFilterObject(objectName);
	}

	@Override
	public String detaySayfa(String outcome) {
		logYaz(outcome + "'a gidiyor...");
		return super.detaySayfa(outcome);
	}
}