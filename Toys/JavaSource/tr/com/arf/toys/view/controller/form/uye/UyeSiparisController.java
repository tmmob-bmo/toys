package tr.com.arf.toys.view.controller.form.uye;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.toys.db.filter.uye.UyeSiparisFilter;
import tr.com.arf.toys.db.model.uye.UyeSiparis;
import tr.com.arf.toys.view.controller.form._base.ToysBaseFilteredController;

@ManagedBean
@ViewScoped
public class UyeSiparisController extends ToysBaseFilteredController {

	private static final long serialVersionUID = 6531152165884980748L;

	private UyeSiparis uyeSiparis;
	private UyeSiparisFilter uyeSiparisFilter = new UyeSiparisFilter();

	public UyeSiparisController() {
		super(UyeSiparis.class);
		setOrderField("rID desc");
		setLoggable(false);
		setAutoCompleteSearchColumns(new String[] { "siparisno" });
		setTable(null);
		queryAction();
	}

	public UyeSiparis getUyeSiparis() {
		uyeSiparis = (UyeSiparis) getEntity();
		return uyeSiparis;
	}

	public void setUyeSiparis(UyeSiparis uyeSiparis) {
		this.uyeSiparis = uyeSiparis;
	}

	public UyeSiparisFilter getUyeSiparisFilter() {
		return uyeSiparisFilter;
	}

	public void setUyeSiparisFilter(UyeSiparisFilter uyeSiparisFilter) {
		this.uyeSiparisFilter = uyeSiparisFilter;
	}

	@Override
	public BaseFilter getFilter() {
		return getUyeSiparisFilter();
	}

} // class