package tr.com.arf.toys.view.controller.form.kisi;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.model.SelectItem;

import tr.com.arf.framework.db.enumerated._common.EvetHayir;
import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.framework.db.model._base.BaseEntity;
import tr.com.arf.framework.utility.exception.data.DBException;
import tr.com.arf.toys.db.enumerated.kisi.KurulTuru;
import tr.com.arf.toys.db.enumerated.system.BirimTip;
import tr.com.arf.toys.db.filter.kisi.KurulDonemFilter;
import tr.com.arf.toys.db.model.kisi.Donem;
import tr.com.arf.toys.db.model.kisi.Kurul;
import tr.com.arf.toys.db.model.kisi.KurulDonem;
import tr.com.arf.toys.db.model.kisi.KurulDonemUye;
import tr.com.arf.toys.db.model.system.Birim;
import tr.com.arf.toys.service.application.ApplicationDescriptor;
import tr.com.arf.toys.service.application.ManagedBeanLocator;
import tr.com.arf.toys.view.controller.form._base.ToysBaseFilteredController;

@ManagedBean
@ViewScoped
public class KurulDonemController extends ToysBaseFilteredController {

	private static final long serialVersionUID = 6531152165884980748L;

	private KurulDonem kurulDonem;
	private KurulDonemFilter kurulDonemFilter = new KurulDonemFilter();

	public KurulDonemController() {
		super(KurulDonem.class);
		setDefaultValues(false);
		setTable(null);
		setOrderField("donemRef.baslangictarih desc");
		setAutoCompleteSearchColumns(new String[] { "donemRef.baslangictarih desc" });
		setStaticWhereCondition(getSessionUser().createStaticWhereCondition("o."));
		queryAction();
	}

	public KurulDonem getKurulDonem() {
		kurulDonem = (KurulDonem) getEntity();
		return kurulDonem;
	}

	public void setKurulDonem(KurulDonem kurulDonem) {
		this.kurulDonem = kurulDonem;
	}

	public KurulDonemFilter getKurulDonemFilter() {
		return kurulDonemFilter;
	}

	public void setKurulDonemFilter(KurulDonemFilter kurulDonemFilter) {
		this.kurulDonemFilter = kurulDonemFilter;
	}

	@Override
	public BaseFilter getFilter() {
		return getKurulDonemFilter();
	}

	public String kurulDonemUye() {
		if (!setSelected()) {
			return "";
		}
		getSessionUser().setLegalAccess(1);
		putObjectToSessionFilter(ApplicationDescriptor._KURULDONEM_MODEL, getKurulDonem());
		// String result = ManagedBeanLocator.locateMenuController().gotoPage("menu_KurulDonemUye");
		return "kurulDonemUye.do";
	}

	public String kurulDonemDelege() {
		if (!setSelected()) {
			return "";
		}
		getSessionUser().setLegalAccess(1);
		putObjectToSessionFilter(ApplicationDescriptor._KURULDONEM_MODEL, getKurulDonem());
		return ManagedBeanLocator.locateMenuController().gotoPage("menu_KurulDonemDelege");
	}

	@Override
	public void insert() {
		super.insert();
		getKurulDonem().setBirimRef(getSessionUser().getPersonelRef().getBirimRef());
		try {
			if (getDBOperator().recordCount(Donem.class.getSimpleName(), "o.birimRef.rID=" + getSessionUser().getPersonelRef().getBirimRef().getRID() + " AND o.anadonemRef.aktif=" + EvetHayir._EVET.getCode()) > 0) {
				@SuppressWarnings("unchecked")
				List<Donem> donem = getDBOperator().load(Donem.class.getSimpleName(), "o.birimRef.rID=" + getSessionUser().getPersonelRef().getBirimRef().getRID() + " AND o.anadonemRef.aktif=" + EvetHayir._EVET.getCode(), "");
				changeDonem(donem);
				getKurulDonem().setDonemRef(donem.get(0));

			}
		} catch (DBException e) {
			// TODO Auto-generated catch block
			logYaz("Exception @" + getModelName() + "Controller :", e);
		}
		super.fillDualListFor(KurulDonemUye.class.getSimpleName(), "o.rID < 1", "");
	}

	@Override
	public void update() {
		super.update();
		super.fillDualListFor(KurulDonemUye.class.getSimpleName(), "o.kurulDonemRef.rID=" + kurulDonem.getRID(), "");

	}

	@Override
	public void save() {
		super.justSave();
		if (!isEmpty(getDualList().getSource())) {
			List<BaseEntity> kurulDonemUyeList = getDualList().getSource();
			for (BaseEntity b : kurulDonemUyeList) {
				try {
					getDBOperator().delete(b);
				} catch (DBException e) {
					logYaz("Exception @" + getModelName() + "Controller :", e);
				}
			}
		}
		queryAction();
	}

	@SuppressWarnings("unchecked")
	public void handleChangeForKurulDonem(AjaxBehaviorEvent event) {
		try {
			HtmlSelectOneMenu menu = (HtmlSelectOneMenu) event.getComponent();
			Kurul secilenKurul = (Kurul) menu.getValue();
			List<KurulDonem> secilenKurulDonemListesi = getDBOperator().load(KurulDonem.class.getSimpleName(), "o.kurulRef.rID=" + secilenKurul.getRID(), "");
			if (!isEmpty(secilenKurulDonemListesi)) {
				for (KurulDonem kurulDonem1 : secilenKurulDonemListesi) {
					super.fillDualListFor(KurulDonemUye.class.getSimpleName(), "o.kurulDonemRef.rID =" + kurulDonem1.getRID(), "");
				}
			} else {
				super.fillDualListFor(KurulDonemUye.class.getSimpleName(), "o.rID < 1", "");
			}

		} catch (Exception e) {
			logYaz("Error @handleChange :" + e.getMessage());
		}
	}

	@SuppressWarnings("unchecked")
	public void handleChangeForBirim(AjaxBehaviorEvent event) {
		try {
			List<Donem> changeDonemListesi = new ArrayList<Donem>();
			logYaz("Kurul Donem Controller handleChangeForBirim metodu calisti...");
			HtmlSelectOneMenu menu = (HtmlSelectOneMenu) event.getComponent();
			Birim secilenBirim = (Birim) menu.getValue();
			List<Donem> donemListesi = getDBOperator().load(Donem.class.getSimpleName(), "o.birimRef.rID=" + secilenBirim.getRID() + " AND o.anadonemRef.aktif=" + EvetHayir._EVET.getCode(), "");
			if (!isEmpty(donemListesi)) {
				changeDonem(donemListesi);
			} else {
				donemListesi = getDBOperator().load(Donem.class.getSimpleName(), "o.birimRef.rID=" + secilenBirim.getRID() + " AND o.anadonemRef.aktif=" + EvetHayir._HAYIR.getCode(), "");
				if (!isEmpty(donemListesi)) {
					for (Donem donem : donemListesi) {
						Date nowdate = new Date();
						if (donem.getBitistarih().after(nowdate) && donem.getAnadonemRef().getAktif() == EvetHayir._HAYIR) {
							changeDonemListesi.add(donem);
						}
					}
				}
				changeDonem(changeDonemListesi);
			}
		} catch (Exception e) {
			logYaz("Error @handleChange :" + e.getMessage());
		}
	}

	private SelectItem[] donemSelectItemList;

	public SelectItem[] getDonemSelectItemList() {
		return donemSelectItemList;
	}

	public void setDonemSelectItemList(SelectItem[] donemSelectItemList) {
		this.donemSelectItemList = donemSelectItemList;
	}

	public void changeDonem(List<Donem> donemListesi) {
		if (!isEmpty(donemListesi)) {
			for (Donem donem : donemListesi) {
				donemSelectItemList = new SelectItem[donemListesi.size() + 1];
				donemSelectItemList[0] = new SelectItem(null, "");
				int i = 1;
				donemSelectItemList[i++] = new SelectItem(donem, donem.getUIString());
			}
			setDonemSelectItemList(donemSelectItemList);
		} else {
			setDonemSelectItemList(new SelectItem[0]);
		}

	}

	public Boolean getAktifDonemandGenelKurul() {
		if (getKurulDonem() != null) {
			try {
				if (getDBOperator().recordCount(KurulDonem.class.getSimpleName(), "o.rID=" + getKurulDonem().getRID()) > 0) {
					KurulDonem kuruldonem = (KurulDonem) getDBOperator().load(KurulDonem.class.getSimpleName(), "o.rID=" + getKurulDonem().getRID(), "").get(0);
					if (kuruldonem.getDonemRef().getAnadonemRef().getAktif() == EvetHayir._EVET && kuruldonem.getKurulRef().getKurulturu() == KurulTuru._GENELKURUL) {
						return true;
					}
				}
			} catch (DBException e) {
				// TODO Auto-generated catch block
				logYaz("Exception @" + getModelName() + "Controller :", e);
			}
		}

		return false;
	}

	public Boolean getAktifDonemandOdaKoordinasyonKurulu() {
		if (getKurulDonem() != null) {
			try {
				if (getDBOperator().recordCount(
						KurulDonem.class.getSimpleName(),
						"o.rID=" + getKurulDonem().getRID() + " AND o.birimRef.birimtip=" + BirimTip._ODA.getCode() + " AND o.kurulRef.kurulturu=" + KurulTuru._KOORDINASYONKURULU.getCode() + " AND o.donemRef.anadonemRef.aktif= "
								+ EvetHayir._EVET.getCode()) > 0) {
					return true;
				}
			} catch (DBException e) {
				// TODO Auto-generated catch block
				logYaz("Exception @" + getModelName() + "Controller :", e);
			}
		}
		return false;
	}

	public Boolean getAktifDonemandSubeKoordinasyonKurulu() {
		if (getKurulDonem() != null) {
			try {
				if (getDBOperator().recordCount(
						KurulDonem.class.getSimpleName(),
						"o.rID=" + getKurulDonem().getRID() + " AND o.birimRef.birimtip=" + BirimTip._SUBE.getCode() + " AND o.kurulRef.kurulturu=" + KurulTuru._KOORDINASYONKURULU.getCode() + " AND o.donemRef.anadonemRef.aktif= "
								+ EvetHayir._EVET.getCode()) > 0) {
					return true;
				}
			} catch (DBException e) {
				// TODO Auto-generated catch block
				logYaz("Exception @" + getModelName() + "Controller :", e);
			}
		}
		return false;
	}

	public String kurulDonemToplanti() {
		if (!setSelected()) {
			return "";
		}
		getSessionUser().setLegalAccess(1);
		putObjectToSessionFilter(ApplicationDescriptor._KURULDONEM_MODEL, getKurulDonem());
		return ManagedBeanLocator.locateMenuController().gotoPage("menu_KurulDonemToplanti");
	}

	public String kurulDonemUyeDurum() {
		if (!setSelected()) {
			return "";
		}
		getSessionUser().setLegalAccess(1);
		putObjectToSessionFilter(ApplicationDescriptor._KURULDONEM_MODEL, getKurulDonem());
		// String result = ManagedBeanLocator.locateMenuController().gotoPage("menu_KurulDonemUye");
		return "kurulDonemUyeDurum.do";
	}
} // class
