package tr.com.arf.toys.view.controller.form.system; 
  
 
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.model.SelectItem;

import tr.com.arf.framework.db.enumerated._common.EvetHayir;
import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.framework.db.model._base.BaseEntity;
import tr.com.arf.framework.utility.exception.data.DBException;
import tr.com.arf.framework.utility.tool.DateUtil;
import tr.com.arf.framework.utility.tool.KeyUtil;
import tr.com.arf.toys.db.enumerated.iletisim.AdresTuru;
import tr.com.arf.toys.db.enumerated.kisi.Cinsiyet;
import tr.com.arf.toys.db.filter.kisi.KisiRaporlamaFilter;
import tr.com.arf.toys.db.model.kisi.Kisi;
import tr.com.arf.toys.db.model.kisi.Kurum;
import tr.com.arf.toys.db.model.system.Kullanicirapor;
import tr.com.arf.toys.view.controller.form._base.ToysBaseFilteredController;
  
  
@ManagedBean 
@ViewScoped 
public class KisiRaporlamaController extends ToysBaseFilteredController {  
  
  private static final long serialVersionUID = 6531152165884980748L; 
  
	private Kisi kisi;   
	private KisiRaporlamaFilter kisiRaporlamaFilter = new KisiRaporlamaFilter();  
	 
	public KisiRaporlamaController() { 
		super(Kisi.class);  
		setDefaultValues(false);  
		setOrderField("ad"); 
		setTable(null);
		this.kisiRaporlamaFilter = new KisiRaporlamaFilter(); 
		setLoggable(false); 
	}  	  
		 
	public Kisi getKisi() {
		kisi=(Kisi) getEntity();
		return kisi;
	}

	public void setKisi(Kisi kisi) {
		this.kisi = kisi;
	}

	public KisiRaporlamaFilter getKisiRaporlamaFilter() {
		return kisiRaporlamaFilter;
	}

	public void setKisiRaporlamaFilter(KisiRaporlamaFilter kisiRaporlamaFilter) {
		this.kisiRaporlamaFilter = kisiRaporlamaFilter;
	}

	public void collapseAllPanels(){
		this.kisiPanelCollapsed = true;
		this.kisiIletisimPanelCollapsed=true;
	}
	
	private boolean kisiPanelCollapsed = false;
	private boolean kisiIletisimPanelCollapsed = true; 
	
	public boolean isKisiPanelCollapsed() {
		return kisiPanelCollapsed;
	}

	public void setKisiPanelCollapsed(boolean kisiPanelCollapsed) {
		this.kisiPanelCollapsed = kisiPanelCollapsed;
	}

	public boolean isKisiIletisimPanelCollapsed() {
		return kisiIletisimPanelCollapsed;
	}

	public void setKisiIletisimPanelCollapsed(boolean kisiIletisimPanelCollapsed) {
		this.kisiIletisimPanelCollapsed = kisiIletisimPanelCollapsed;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<Kurum> completeKurum(String value) {
		return getDBOperator().load(Kurum.class.getSimpleName(), "UPPER(o.ad) LIKE UPPER('%" + value +"%')", "o.rID"); 
	}
	
	@Override
	public BaseFilter getFilter() {
		return null;
	}
	

	public String createQueryStringForDateFields(Date beginDate, Date endDate, String fieldName){ 
		if(beginDate != null || endDate != null){
			if(beginDate != null && endDate == null){
				return " AND to_char(o." + fieldName + ", 'yyyy-mm-dd') >= '" + DateUtil.dateToYMD(beginDate) + "'"; 
			} else if(beginDate == null && endDate != null){
				return " AND to_char(o." + fieldName + ", 'yyyy-mm-dd') <= '" + DateUtil.dateToYMD(endDate) + "'";
			} else {
				return " AND to_char(o." + fieldName + ", 'yyyy-mm-dd') >= '" + DateUtil.dateToYMD(beginDate) + "' AND to_char(o." + fieldName + ", 'yyyy-mm-dd') <= '" + DateUtil.dateToYMD(endDate) + "'"; 			
			} 
		} else {
			return "";
		}
	} 
	
	@Override
		@PostConstruct
		public void init() {
			super.init();
			getKisiRaporlamaFilter().setEpostaYok(false);
			getKisiRaporlamaFilter().setTelefonYok(false);
			getKisiRaporlamaFilter().setAdyok(true);
			getKisiRaporlamaFilter().setSoyadyok(true);
			getKisiRaporlamaFilter().setYasadigiilyok(true);
			getKisiRaporlamaFilter().setCalistigiilyok(true);
		}

	@SuppressWarnings("unchecked")
	public void filterKisi() throws DBException{
		
		if (getCalistirilacakRaporRID()!=null && getCalistirilacakRaporRID()!=0L){
			logYaz("Calistirilacak rapor id = " +getCalistirilacakRaporRID());
			Kullanicirapor rapor = (Kullanicirapor) getDBOperator().find(Kullanicirapor.class.getSimpleName(), "rID", getCalistirilacakRaporRID() + "").get(0);	
			setList(getDBOperator().load(Kisi.class.getSimpleName(), rapor.getWherecondition(), "o.rID"));
			return;
		}
		
		getKisiRaporlamaFilter().setKullanicirapor(null);
		StringBuilder whereCon = new StringBuilder(" 1 = 1 "); 
		
		if (getKisiRaporlamaFilter().getKurumadi()!=null){
			whereCon.append(" AND (SELECT COUNT(krmkisi.rID) FROM KurumKisi AS krmkisi WHERE krmkisi.kisiRef.rID = o.rID AND " +
					"(UPPER(krmkisi.kurumRef.ad) LIKE UPPER('%" + getKisiRaporlamaFilter().getKurumadi() + "%')))>0");
		}
		
		if (getKisiRaporlamaFilter().getCinsiyet()!=null && getKisiRaporlamaFilter().getCinsiyet()!=Cinsiyet._NULL){
			whereCon.append(" AND (SELECT COUNT(kmlk.rID) FROM Kisikimlik AS kmlk WHERE kmlk.kisiRef.rID = o.rID AND kmlk.cinsiyet=" + 
					getKisiRaporlamaFilter().getCinsiyet().getCode() + " ) > 0 ");
		}
		if (getKisiRaporlamaFilter().getYasadigiIl()!=null){
			whereCon.append(" AND (SELECT COUNT(evadres.rID) FROM Adres AS evadres WHERE evadres.kisiRef.rID = o.rID AND evadres.adresturu="+ AdresTuru._EVADRESI.getCode() +
				" AND evadres.sehirRef.rID=" + getKisiRaporlamaFilter().getYasadigiIl().getRID() + ") > 0 ");
		}
		if (getKisiRaporlamaFilter().getCalistigiIl()!=null){
			whereCon.append(" AND (SELECT COUNT(adrs.rID) FROM Adres AS adrs WHERE adrs.kisiRef.rID = o.rID AND adrs.adresturu="+ AdresTuru._ISADRESI.getCode() + 
				" AND adrs.sehirRef.rID=" + getKisiRaporlamaFilter().getCalistigiIl().getRID() + " ) > 0 ");
		}
		
		if (getKisiRaporlamaFilter().getDogumtarih()!=null && getKisiRaporlamaFilter().getDogumtarihMax()!=null){
			whereCon.append(" AND (SELECT COUNT(kisikmlk.rID) FROM Kisikimlik AS kisikmlk WHERE kisikmlk.kisiRef.rID = o.rID  AND" +
				" to_char(kisikmlk.dogumtarih, 'yyyy-mm-dd') >= '"+ DateUtil.dateToYMD(getKisiRaporlamaFilter().getDogumtarih())+ "' AND to_char(kisikmlk.dogumtarih, 'yyyy-mm-dd') <= '"+
				DateUtil.dateToYMD(getKisiRaporlamaFilter().getDogumtarihMax()) +"') > 0 ");
		}
		
		if (getKisiRaporlamaFilter().getTelefonYok()){
			whereCon.append(" AND (SELECT COUNT(tlfn.rID) FROM Telefon AS tlfn WHERE tlfn.kisiRef.rID=o.rID AND tlfn.varsayilan=" + EvetHayir._EVET.getCode()+") = 0");
		}

		if (getKisiRaporlamaFilter().getEpostaYok()){
			whereCon.append(" AND (SELECT COUNT(eposta.rID) FROM Internetadres AS eposta WHERE eposta.kisiRef.rID=o.rID AND eposta.varsayilan=" + EvetHayir._EVET.getCode()+") = 0");
		}

		
//		logYaz("SORGU KRITERI : " + whereCon.toString()); 
		int count = getDBOperator().recordCount(Kisi.class.getSimpleName(), whereCon.toString());
//		logYaz("BULUNAN UYE SAYISI :" + count); 
		if(count > 1500){
			createGenericMessage("DIKKAT! Cok fazla kayit (" + count + ") bulundu! Lutfen daha fazla kriter giriniz!", FacesMessage.SEVERITY_ERROR);
			return;
		}
		setList(getDBOperator().load(Kisi.class.getSimpleName(), whereCon.toString(), "o.rID"));
		
		if(isEmpty(getList())) {
			createGenericMessage(KeyUtil.getMessageValue("kayit.bulunamadi"), FacesMessage.SEVERITY_WARN);
		} else {
			setWhereCondition(whereCon.toString());
		}
	}  
	
	
	/* KULLANICI RAPOR KAYDETME */
	private String whereCondition;
	private String kullaniciRaporAdi;
	
	public String getKullaniciRaporAdi() {
		return kullaniciRaporAdi;
	}

	public void setKullaniciRaporAdi(String kullaniciRaporAdi) {
		this.kullaniciRaporAdi = kullaniciRaporAdi;
	}	

	public String getWhereCondition() {
		return whereCondition;
	}

	public void setWhereCondition(String whereCondition) {
		this.whereCondition = whereCondition;
	}
	
	public void saveKullaniciRapor() throws DBException{
		if(isEmpty(getList())) {
			createGenericMessage(KeyUtil.getMessageValue("kayit.bulunamadi"), FacesMessage.SEVERITY_WARN);
		} else if(isEmpty(getKullaniciRaporAdi())){
			createGenericMessage(KeyUtil.getMessageValue("kullaniciRaporAdi.giriniz"), FacesMessage.SEVERITY_ERROR);
		} else {
			 Kullanicirapor rapor = new Kullanicirapor();
			 rapor.setKullaniciRef(getSessionUser().getKullanici());
			 rapor.setRaporadi(getKullaniciRaporAdi());
			 rapor.setTarih(new Date());
			 rapor.setRaporTuru("Kişi");
			 rapor.setWherecondition(getWhereCondition());
			 if(getDBOperator().recordCount(Kullanicirapor.class.getSimpleName(), "o.kullaniciRef.rID=" + rapor.getKullaniciRef().getRID() + " AND o.raporadi='" + rapor.getRaporadi() + "'") > 0){
				 createGenericMessage(KeyUtil.getMessageValue("kullaniciRaporAdi.mukerrer"), FacesMessage.SEVERITY_WARN);
				 return;
			 }
			 getDBOperator().insert(rapor);
			 createGenericMessage(KeyUtil.getMessageValue("kayit.eklendi"), FacesMessage.SEVERITY_INFO);
			 setKullaniciRaporAdi("");
		}
	}
	/*********************************************************************/
	
	/* SECILEN RAPORUN CALISTIRILMASI */
	private Long calistirilacakRaporRID;
	

	public Long getCalistirilacakRaporRID() {
		return calistirilacakRaporRID;
	}

	public void setCalistirilacakRaporRID(Long calistirilacakRaporRID) {
		this.calistirilacakRaporRID = calistirilacakRaporRID;
	}
	
	private List<Kullanicirapor> kullaniciRaporListesi;
	 
	public List<Kullanicirapor> getKullaniciRaporListesi() {
		return kullaniciRaporListesi;
	}

	public void setKullaniciRaporListesi(List<Kullanicirapor> kullaniciRaporListesi) {
		this.kullaniciRaporListesi = kullaniciRaporListesi;
	}

	public SelectItem[] kullaniciRaporList;
	
	public void setKullaniciRaporList(SelectItem[] kullaniciRaporList) {
		this.kullaniciRaporList = kullaniciRaporList;
	}
	@SuppressWarnings("unchecked")
	public SelectItem[] getKullaniciRaporList() {  
		List<Kullanicirapor> entityList = null;
		if(kullaniciRaporList != null && kullaniciRaporList.length > 0){ 
			return kullaniciRaporList;
		}
		if(getKullaniciRaporListesi() != null) { 
			entityList =  getKullaniciRaporListesi();
		} else { 
			entityList = getDBOperator().load(Kullanicirapor.class.getSimpleName(), " o.kullaniciRef = " + getSessionUser().getKullanici().getRID() + " AND o.raporTuru = 'Kişi'", "o.raporadi");
		} 
		if (entityList != null) {
			int i = 0;
			SelectItem[] raporList = null; 
			raporList = new SelectItem[entityList.size() + 1];
			raporList[0] = new SelectItem(null, "");
			i = 0;  
			for (BaseEntity entity : entityList) {
				i++;
				raporList[i] = new SelectItem(entity, entity.getUIString());
			}
			setKullaniciRaporList(raporList);
			return raporList; 
		}
		return new SelectItem[0];
	}	

	@SuppressWarnings("unchecked")
	public void executeKullaniciRapor(){
		if (getKisiRaporlamaFilter().getKullanicirapor()!=null){
			logYaz("Calistirilacak rapor adi = " +getKisiRaporlamaFilter().getKullanicirapor().getRaporadi());
			Kullanicirapor rapor = (Kullanicirapor) getDBOperator().find(Kullanicirapor.class.getSimpleName(), "rID", getKisiRaporlamaFilter().getKullanicirapor().getRID() + "").get(0);		
			setList(getDBOperator().load(Kisi.class.getSimpleName(), rapor.getWherecondition(), "o.rID"));
		}
		if(isEmpty(getList())) {
			createGenericMessage(KeyUtil.getMessageValue("kayit.bulunamadi"), FacesMessage.SEVERITY_WARN);
		} 
		getKisiRaporlamaFilter().setKullanicirapor(null);
	}
	/*********************************************************************/
	
	
	private SelectItem[] kullaniciRaporDegistir;
	
	public SelectItem[] getKullaniciRaporDegistir() {
		return kullaniciRaporDegistir;
	}

	public void setKullaniciRaporDegistir(SelectItem[] kullaniciRaporDegistir) {
		this.kullaniciRaporDegistir = kullaniciRaporDegistir;
	}

	public void handleChangeKullaniciRapor(AjaxBehaviorEvent event) {
		try { 
			HtmlSelectOneMenu menu = (HtmlSelectOneMenu) event.getComponent(); 			
			if(menu.getValue() instanceof Kullanicirapor){ 
				setCalistirilacakRaporRID(((Kullanicirapor) menu.getValue()).getRID());
			}else {
				setCalistirilacakRaporRID(0L);
			}
			
		} catch(Exception e){
			logYaz("Error KisiRaporlamaController @handleChangeKullaniciRapor :" + e.getMessage());
			logYaz("Exception @" + getModelName() + "Controller :", e);
		}
	}

	
} // class 
