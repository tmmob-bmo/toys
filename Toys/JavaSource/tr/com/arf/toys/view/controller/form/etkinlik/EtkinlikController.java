package tr.com.arf.toys.view.controller.form.etkinlik; 
  
 
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.toys.db.filter.etkinlik.EtkinlikFilter;
import tr.com.arf.toys.db.model.etkinlik.Etkinlik;
import tr.com.arf.toys.service.application.ApplicationDescriptor;
import tr.com.arf.toys.service.application.ManagedBeanLocator;
import tr.com.arf.toys.view.controller.form._base.ToysBaseFilteredController;
  
  
@ManagedBean 
@ViewScoped 
public class EtkinlikController extends ToysBaseFilteredController {  
  
  private static final long serialVersionUID = 6531152165884980748L; 
  
	private Etkinlik etkinlik;  
	private EtkinlikFilter etkinlikFilter = new EtkinlikFilter();  
  
	public EtkinlikController() { 
		super(Etkinlik.class);  
		setDefaultValues(false);  
		setOrderField("ad");  
		setLoggable(false);  
		setAutoCompleteSearchColumns(new String[]{"ad"}); 
		setStaticWhereCondition(getSessionUser().createStaticWhereCondition("o."));
		setTable(null);  
		queryAction(); 
	} 
	
	
	public Etkinlik getEtkinlik() { 
		etkinlik = (Etkinlik) getEntity(); 
		return etkinlik; 
	} 
	
	public void setEtkinlik(Etkinlik etkinlik) { 
		this.etkinlik = etkinlik; 
	} 
	
	public EtkinlikFilter getEtkinlikFilter() { 
		return etkinlikFilter; 
	} 
	 
	public void setEtkinlikFilter(EtkinlikFilter etkinlikFilter) {  
		this.etkinlikFilter = etkinlikFilter;  
	}  
	 
	@Override  
	public BaseFilter getFilter() {  
		return getEtkinlikFilter();  
	}  
	
	@Override  
	public void update(){
		super.update();
		if(getEtkinlik().getSorumlukisiRef() != null) {
			getEtkinlik().setSorumlukisiRefPrev(getEtkinlik().getSorumlukisiRef());
		}
		if(getEtkinlik().getSehirRef() != null) {
			super.changeIlce(getEtkinlik().getSehirRef().getRID());
		}
	}
	
	@Override
    public void save(){ 
		if(getEtkinlik().getRID() != null){
            // Guncelleme yapiliyorsa, suggestbox icin duzeltmeler yapiliyor.
            if(getEtkinlik().getSorumlukisiRef() == null){
                if(getEtkinlik().getSorumlukisiRefPrev() != null){
                	getEtkinlik().setSorumlukisiRef(getEtkinlik().getSorumlukisiRefPrev());
                }
            }
        }
	    super.save();
	}

	public void deletePreviousSorumlu(){ 
		getEtkinlik().setSorumlukisiRef(null);
		getEtkinlik().setSorumlukisiRefPrev(null);		
	}
	
	public String etkinlikkatilimci() {	
		if (!setSelected()) {
			return "";
		}	 
		getSessionUser().setLegalAccess(1);	
		putObjectToSessionFilter(ApplicationDescriptor._ETKINLIK_MODEL, getEtkinlik());
		return ManagedBeanLocator.locateMenuController().gotoPage("menu_EtkinlikKatilimci"); 
	} 
	
	public String etkinlikkurul() {	
		if (!setSelected()) {
			return "";
		}	 
		getSessionUser().setLegalAccess(1);	
		putObjectToSessionFilter(ApplicationDescriptor._ETKINLIK_MODEL, getEtkinlik());
		return ManagedBeanLocator.locateMenuController().gotoPage("menu_EtkinlikKurul"); 
	} 
	
	public String etkinlikKurum() {	
		if (!setSelected()) {
			return "";
		}	 
		getSessionUser().setLegalAccess(1);	
		putObjectToSessionFilter(ApplicationDescriptor._ETKINLIK_MODEL, getEtkinlik());
		return ManagedBeanLocator.locateMenuController().gotoPage("menu_EtkinlikKurum"); 
	} 
	
	public String etkinlikdosya() {	
		if (!setSelected()) {
			return "";
		}	 
		getSessionUser().setLegalAccess(1);	
		putObjectToSessionFilter(ApplicationDescriptor._ETKINLIK_MODEL, getEtkinlik());
		return ManagedBeanLocator.locateMenuController().gotoPage("menu_EtkinlikDosya"); 
	} 
	
} // class 
