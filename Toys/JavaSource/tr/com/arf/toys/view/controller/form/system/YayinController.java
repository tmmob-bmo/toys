package tr.com.arf.toys.view.controller.form.system; 
  
 
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.toys.db.filter.system.YayinFilter;
import tr.com.arf.toys.db.model.system.Yayin;
import tr.com.arf.toys.view.controller.form._base.ToysBaseFilteredController;
  
  
@ManagedBean 
@ViewScoped 
public class YayinController extends ToysBaseFilteredController {  
  
  private static final long serialVersionUID = 6531152165884980748L; 
  
	private Yayin yayin;  
	private YayinFilter yayinFilter = new YayinFilter();  
  
	public YayinController() { 
		super(Yayin.class);  
		setDefaultValues(false);  
		setOrderField("ad");  
		setAutoCompleteSearchColumns(new String[]{"ad"}); 
		setTable(null); 
		queryAction(); 
	} 
	
	
	public Yayin getYayin() { 
		yayin = (Yayin) getEntity(); 
		return yayin; 
	} 
	
	public void setYayin(Yayin yayin) { 
		this.yayin = yayin; 
	} 
	
	public YayinFilter getYayinFilter() { 
		return yayinFilter; 
	} 
	 
	public void setYayinFilter(YayinFilter yayinFilter) {  
		this.yayinFilter = yayinFilter;  
	}  
	 
	@Override  
	public BaseFilter getFilter() {  
		return getYayinFilter();  
	}  
	
	
 
	
} // class 
