package tr.com.arf.toys.view.controller.form.kisi; 
  
 
import java.io.File;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.primefaces.event.FileUploadEvent;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.toys.db.filter.kisi.KisifotografFilter;
import tr.com.arf.toys.db.model.kisi.Kisi;
import tr.com.arf.toys.db.model.kisi.Kisifotograf;
import tr.com.arf.toys.service.application.ApplicationDescriptor;
import tr.com.arf.toys.view.controller.form._base.ToysBaseFilteredController;
  
  
@ManagedBean 
@ViewScoped 
public class KisifotografController extends ToysBaseFilteredController {  
  
  private static final long serialVersionUID = 6531152165884980748L; 
  
	private Kisifotograf kisifotograf;  
	private KisifotografFilter kisifotografFilter = new KisifotografFilter();  
  
	public KisifotografController() { 
		super(Kisifotograf.class);  
		setDefaultValues(false);  
		setOrderField("path");  
		setAutoCompleteSearchColumns(new String[]{"path"}); 
		setTable(null); 
		if(getMasterEntity() != null){ 
			setMasterObjectName(ApplicationDescriptor._KISI_MODEL);
			setMasterOutcome(ApplicationDescriptor._KISI_MODEL);
			getKisifotografFilter().setKisiRef(getMasterEntity());
			getKisifotografFilter().createQueryCriterias();   
		} 
		queryAction(); 
	} 
	
	public KisifotografController(Object object) { 
		super(Kisifotograf.class);  
	}
 
	public KisifotografController(Kisi kisi) {
		super(Kisifotograf.class);   
		setLoggable(true);
		if(kisi != null){  
			getKisifotografFilter().setKisiRef(kisi); 
		}  
	}

	public Kisi getMasterEntity(){
		if(getKisifotografFilter().getKisiRef() != null){
			return getKisifotografFilter().getKisiRef();
		} else {
			return (Kisi) getObjectFromSessionFilter(ApplicationDescriptor._KISI_MODEL);
		}			
	}	
	
	public Kisifotograf getKisifotograf() { 
		kisifotograf = (Kisifotograf) getEntity(); 
		return kisifotograf; 
	} 
	
	public void setKisifotograf(Kisifotograf kisifotograf) { 
		this.kisifotograf = kisifotograf; 
	} 
	
	public KisifotografFilter getKisifotografFilter() { 
		return kisifotografFilter; 
	} 
	 
	public void setKisifotografFilter(KisifotografFilter kisifotografFilter) {  
		this.kisifotografFilter = kisifotografFilter;  
	}  
	 
	@Override  
	public BaseFilter getFilter() {  
		return getKisifotografFilter();  
	}  
 
	@Override	
	public void insert() {	
		super.insert();	
		getKisifotograf().setKisiRef(getKisifotografFilter().getKisiRef());	
	}	
	
	@Override
	public void fileUploadListener(FileUploadEvent event) throws Exception { 
		File creationFile = super.createFile(event.getFile());
		getKisifotograf().setPath(creationFile.getName()); 
		super.save();
	} 
	
} // class 
