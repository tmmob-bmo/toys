package tr.com.arf.toys.view.controller.form.system; 
  
 
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.toys.db.filter.system.BankaFilter;
import tr.com.arf.toys.db.model.system.Banka;
import tr.com.arf.toys.view.controller.form._base.ToysBaseFilteredController;
  
  
@ManagedBean 
@ViewScoped 
public class BankaController extends ToysBaseFilteredController {  
  
  private static final long serialVersionUID = 6531152165884980748L; 
  
	private Banka banka;  
	private BankaFilter bankaFilter = new BankaFilter();  
	
	public BankaController() { 
		super(Banka.class);  
		setDefaultValues(false);  
		setOrderField("ad");  
		setAutoCompleteSearchColumns(new String[]{"ad"}); 
		setTable(null); 
		queryAction(); 
	} 

	
	public Banka getBanka() {
		banka=(Banka) getEntity();
		return banka;
	}


	public void setBanka(Banka banka) {
		this.banka = banka;
	}



	public BankaFilter getBankaFilter() {
		return bankaFilter;
	}



	public void setBankaFilter(BankaFilter bankaFilter) {
		this.bankaFilter = bankaFilter;
	}

	@Override  
	public BaseFilter getFilter() {  
		return getBankaFilter();  
	}  
	
	@Override	
	public void insert() {	
		super.insert();	
	}	
	
	
	
} // class 
