package tr.com.arf.toys.view.controller.form.system; 
  
 
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.model.SelectItem;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.framework.db.model._base.BaseEntity;
import tr.com.arf.framework.utility.exception.data.DBException;
import tr.com.arf.toys.db.filter.system.UniversiteFilter;
import tr.com.arf.toys.db.model.system.Ulke;
import tr.com.arf.toys.db.model.system.Universite;
import tr.com.arf.toys.db.model.system.UniversiteFakulte;
import tr.com.arf.toys.service.application.ApplicationDescriptor;
import tr.com.arf.toys.service.application.ManagedBeanLocator;
import tr.com.arf.toys.view.controller.form._base.ToysBaseFilteredController;
  
  
@ManagedBean 
@ViewScoped 
public class UniversiteController extends ToysBaseFilteredController {  
  
  private static final long serialVersionUID = 6531152165884980748L; 
  
	private Universite universite;  
	private UniversiteFilter universiteFilter = new UniversiteFilter();  
  
	public UniversiteController() { 
		super(Universite.class);  
		setDefaultValues(false);  
		setOrderField("ad");  
		setAutoCompleteSearchColumns(new String[]{"ad"}); 
		setTable(null); 
		queryAction(); 
	} 
 

	public Universite getUniversite() {
		universite=(Universite) getEntity();
		return universite;
	}

	public void setUniversite(Universite universite) {
		this.universite = universite;
	}

	public UniversiteFilter getUniversiteFilter() {
		return universiteFilter;
	}

	public void setUniversiteFilter(UniversiteFilter universiteFilter) {
		this.universiteFilter = universiteFilter;
	}

	@Override  
	public BaseFilter getFilter() {  
		return getUniversiteFilter();  
	}  
	
	public String universitefakulte() {	
		if (!setSelected()) {
			return "";
		}	
		getSessionUser().setLegalAccess(1);	
		putObjectToSessionFilter(ApplicationDescriptor._UNIVERSITE_MODEL, getUniversite());
		return ManagedBeanLocator.locateMenuController().gotoPage("menu_UniversiteFakulte"); 
	} 
	
	@SuppressWarnings("unchecked")
	@Override
		public void insert() {
			super.insert();
			ArrayList<Ulke> ulke = new ArrayList<Ulke>();
			ulke=(ArrayList<Ulke>) getDBOperator().load(Ulke.class.getSimpleName(), "o.rID=1", "");
			Iterator<Ulke> iterator=ulke.iterator();
			while(iterator.hasNext()){
				Ulke ulke1=iterator.next();
				getUniversite().setUlkeRef(ulke1);
				changeIl(ulke1.getRID());
			}
			
		}
	
	List<BaseEntity> fakulteler=new ArrayList<BaseEntity>();
	
	public List<BaseEntity> getFakulteler() {
		return fakulteler;
	}


	public void setFakulteler(List<BaseEntity> fakulteler) {
		this.fakulteler = fakulteler;
	}

	SelectItem [] fakulteList=new SelectItem[0];

	public SelectItem[] getFakulteList() {
		return fakulteList;
	}

	public void setFakulteList(SelectItem[] fakulteList) {
		this.fakulteList = fakulteList;
	}
	
	@SuppressWarnings("unchecked")
	public boolean getFakulteDondur(){
		List<BaseEntity> list =new ArrayList<BaseEntity>();
		try {
		if (getDBOperator().recordCount(UniversiteFakulte.class.getSimpleName(),"o.universiteRef.rID=" + getUniversite().getRID())>0){
			List<UniversiteFakulte> entityList=getDBOperator().load (UniversiteFakulte.class.getSimpleName(),"o.universiteRef.rID=" + getUniversite().getRID(),"");
			{
				if (entityList != null) {
					for (UniversiteFakulte entity : entityList) {
						list.add(entity.getFakulteRef());
					}
					setFakulteler(list);
					return true;
				}
				else {
					setFakulteler(list);
					return false;
				}
				
			}
		}
	} catch (DBException e) {
		// TODO Auto-generated catch block
		logYaz("Exception @" + getModelName() + "Controller :", e);
	}
		return false;
	}
	
	
	
	
	
	
	
} // class 
