package tr.com.arf.toys.view.controller.dashboard;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import org.primefaces.component.datatable.DataTable;
import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;

import tr.com.arf.framework.db._management.DBOperator;
import tr.com.arf.framework.db.enumerated._common.IslemTuru;
import tr.com.arf.framework.db.model._base.BaseEntity;
import tr.com.arf.framework.db.model.sistem.Islemlog;
import tr.com.arf.framework.utility.exception.data.DBException;
import tr.com.arf.framework.utility.tool.KeyUtil;
import tr.com.arf.toys.db.model.evrak.Evrak;
import tr.com.arf.toys.db.model.evrak.Evrakdolasimbildirim;
import tr.com.arf.toys.db.model.system.Duyuru;
import tr.com.arf.toys.db.model.uye.Uye;
import tr.com.arf.toys.service.application.ManagedBeanLocator;
import tr.com.arf.toys.utility.logUtils.LogService;
import tr.com.arf.toys.view.controller._common.SessionUser;

@ManagedBean(name = "dashboardCommon")
@SessionScoped
public class DashboardCommon implements Serializable {

	private static final long serialVersionUID = 8134502456117394440L;
	private SessionUser sessionUser = null;

	/* Data Operator */
	private DBOperator dBOperator;

	private boolean loggable = false;

	public boolean isLoggable() {
		return loggable;
	}

	public void setLoggable(boolean loggable) {
		this.loggable = loggable;
	}

	public DashboardCommon() {
		super();
		this.sessionUser = ManagedBeanLocator.locateSessionUser();
	}

	@PostConstruct
	public void init() {
		System.out.println("DashboardCommon init...");
		dBOperator = new DBOperator();
	}

	/* Data Operator */
	public final DBOperator getDBOperator() {
		if (dBOperator == null) {
			dBOperator = new DBOperator();
		}
		return dBOperator;
	}

	public SessionUser getSessionUser() {
		return sessionUser;
	}

	private DataTable evrakdolasimbildirimTable;

	public DataTable getEvrakdolasimbildirimTable() {
		return evrakdolasimbildirimTable;
	}

	public void setEvrakdolasimbildirimTable(DataTable evrakdolasimbildirimTable) {
		this.evrakdolasimbildirimTable = evrakdolasimbildirimTable;
	}

	@SuppressWarnings("rawtypes")
	public List getEvrakdolasimbildirimList() {
		try {
			String whereCon = "o.aliciRef ='" + getSessionUser().getKullanici().getKisiRef().getUIStringShort() + "'";
			if (getSessionUser().getPersonelRef() != null && getSessionUser().getPersonelRef().getBirimRef() != null) {
				whereCon += " OR o.aliciRef = '" + getSessionUser().getPersonelRef().getBirimRef().getAd() + "'";
			}
			if (getDBOperator().recordCount(Evrakdolasimbildirim.class.getSimpleName(), whereCon) > 0) {
				@SuppressWarnings("unchecked")
				List<Evrakdolasimbildirim> evrakdolasimbildirimList = getDBOperator().load(Evrakdolasimbildirim.class.getSimpleName(), whereCon, "", new Integer[] { 0, 20 });
				return evrakdolasimbildirimList;
			} else {
				return new ArrayList();
			}
		} catch (Exception e) {
			System.out.println("ERROR @getEvrakdolasimbildirimList of DashboardCommon :" + e.getMessage());
			return null;
		}
	}

	private DataTable uyeTable;

	@SuppressWarnings("rawtypes")
	public List getUyeList() {
		@SuppressWarnings("unchecked")
		List<Uye> uyeList = getDBOperator().load(Uye.class.getSimpleName(), "", "o.rID DESC");
		return uyeList;
	}

	public DataTable getUyeTable() {
		return uyeTable;
	}

	public void setUyeTable(DataTable uyeTable) {
		this.uyeTable = uyeTable;
	}

	private DataTable duyuruTable;

	@SuppressWarnings("rawtypes")
	public List getDuyuruList() {
		@SuppressWarnings("unchecked")
		List<Duyuru> duyuruList = getDBOperator().load(Duyuru.class.getSimpleName(), "", "o.rID DESC");
		return duyuruList;
	}

	public DataTable getDuyuruTable() {
		return duyuruTable;
	}

	public void setDuyuruTable(DataTable duyuruTable) {
		this.duyuruTable = duyuruTable;
	}

	private String detailEditType = null;
	private Duyuru duyuru = new Duyuru();

	public void newDetail(String detailType) {
		detailType = detailType.toLowerCase(Locale.ENGLISH);
		this.detailEditType = detailType;
		RequestContext context = RequestContext.getCurrentInstance();
		if (detailType.equalsIgnoreCase("duyuru")) {
			this.duyuru = new Duyuru();
		}
		context.update("dialogUpdateBox");
	}

	public String getDetailEditType() {
		return detailEditType;
	}

	public void setDetailEditType(String detailEditType) {
		this.detailEditType = detailEditType;
	}

	public Duyuru getDuyuru() {
		return duyuru;
	}

	public void setDuyuru(Duyuru duyuru) {
		this.duyuru = duyuru;
	}

	public void saveDetails() throws DBException, IOException {
		RequestContext context = RequestContext.getCurrentInstance();

		if (this.detailEditType.equalsIgnoreCase("duyuru")) {

			this.duyuru = getDuyuru();
			justSave(this.duyuru, false, "");

			this.duyuru = new Duyuru();
			updateDialogAndForm(context);
		}

		context.update("duyurular");
		this.detailEditType = null;
	}

	public void justSave(BaseEntity objectToBeSaved, boolean doLog, String oldVal) {
		saveGivenEntity(objectToBeSaved, doLog, oldVal);
	}

	private boolean editPanelRendered;

	private void saveGivenEntity(BaseEntity objectToBeSaved, boolean doLog, String oldVal) {
		setDuyuruTable(null);
		setEditPanelRendered(false);
		if (objectToBeSaved.getRID() == null) {
			try {
				getDBOperator().insert(objectToBeSaved);
				if (doLog) {
					logKaydet(objectToBeSaved, IslemTuru._EKLEME, oldVal);
				}
				createGenericMessage(KeyUtil.getMessageValue("kayit.eklendi"), FacesMessage.SEVERITY_INFO);
			} catch (Exception e) {
				objectToBeSaved.setRID(null);
				createGenericMessage(KeyUtil.getMessageValue("islem.hata"), FacesMessage.SEVERITY_INFO);
			}
		} else {
			try {
				if (objectToBeSaved.getValue().hashCode() == oldVal.hashCode()) {
					createGenericMessage(KeyUtil.getMessageValue("islem.degisiklikYok"), FacesMessage.SEVERITY_INFO);
					return;
				}
				getDBOperator().update(objectToBeSaved);
				if (doLog) {
					logKaydet(objectToBeSaved, IslemTuru._GUNCELLEME, oldVal);
				}
				createGenericMessage(KeyUtil.getMessageValue("kayit.guncellendi"), FacesMessage.SEVERITY_INFO);
			} catch (Exception e) {
				createGenericMessage(KeyUtil.getMessageValue("islem.hata"), FacesMessage.SEVERITY_INFO);
			}
		}
	}

	private void updateDialogAndForm(RequestContext context) {
		context.update("dialogUpdateBox");
		context.update("dashboardForm");
	}

	public boolean isEditPanelRendered() {
		return editPanelRendered;
	}

	public void setEditPanelRendered(boolean editPanelRendered) {
		this.editPanelRendered = editPanelRendered;
	}

	public void createGenericMessage(String messageValue, javax.faces.application.FacesMessage.Severity messageSeverity) {
		FacesMessage message = new FacesMessage();
		FacesContext context = FacesContext.getCurrentInstance();
		message.setSeverity(messageSeverity);
		message.setDetail(messageValue);
		message.setSummary(messageValue);
		context.addMessage(null, message);
	}

	protected void logKaydet(BaseEntity entity, IslemTuru islemTuru, String oldVal) throws DBException {
		Islemlog islemLog = new Islemlog();
		islemLog.setIslemtarihi(new Date());
		islemLog.setIslemturu(islemTuru);
		islemLog.setReferansRID(entity.getRID());
		islemLog.setTabloadi(entity.getClass().getSimpleName());
		if (islemTuru != IslemTuru._SILME) {
			islemLog.setEskideger(oldVal);
			islemLog.setYenideger(entity.getValue());
		} else {
			islemLog.setEskideger(entity.getValue());
			islemLog.setYenideger("");
		}
		islemLog.setKullaniciRid(getSessionUser().getKullanici().getRID());
		getDBOperator().insert(islemLog);
		islemLog = new Islemlog();
	}

	private BaseEntity entity;

	public BaseEntity getEntity() {
		return entity;
	}

	public void setEntity(BaseEntity entity) {
		this.entity = entity;
	}

	private String selectedRID;

	public String getSelectedRID() {
		return selectedRID;
	}

	public void setSelectedRID(String selectedRID) {
		this.selectedRID = selectedRID;
		setEntity((BaseEntity) getDBOperator().find(Evrak.class.getSimpleName(), "rID", selectedRID).get(0));
		if (loggable) {
			if (getEntity() != null) {
				putObjectToSessionFilter("logEntity", getEntity());
			}
		}
	}

	public void putObjectToSessionFilter(String objectName, Object object) {
		getSessionUser().addToSessionFilters(objectName, object);
	}

	private BaseEntity[] selection;

	public void setSelection(BaseEntity[] selection) {
		this.selection = selection;
	}

	public BaseEntity[] getSelection() {
		return selection;
	}

	public void onRowDblClckSelect(SelectEvent event) throws DBException {
		setEditPanelRendered(false);
		setEntity((BaseEntity) event.getObject());
		Evrak evrak = new Evrak();
		if (getDBOperator().recordCount(Evrakdolasimbildirim.class.getSimpleName(), "o.rID=" + ((Evrakdolasimbildirim) getEntity()).getRID()) > 0) {
			Evrakdolasimbildirim evrakdolasim = (Evrakdolasimbildirim) getDBOperator().load(Evrakdolasimbildirim.class.getSimpleName(), "o.rID=" + ((Evrakdolasimbildirim) getEntity()).getRID(), "").get(0);
			evrak = evrakdolasim.getEvrakRef();
		}
		if (getEntity() != null && evrak != null) {
			setSelectedRID(evrak.getRID().toString());
		}
		getSessionUser().setLegalAccess(1);
		ManagedBeanLocator.locateSessionUser().addToSessionFilters(Evrak.class.getSimpleName(), getEntity());
		ExternalContext ectx = FacesContext.getCurrentInstance().getExternalContext();
		ectx.setRequest(ectx.getRequestContextPath() + ectx.getRequestServletPath());
		try {
			ectx.redirect(ectx.getRequestContextPath() + ectx.getRequestServletPath() + "/_page/form/evrak/evrakDetay/evrakdolasimbildirim.xhtml");
		} catch (IOException e) {
			LogService.logYaz("Exception @DashboardCommon :" + e.getMessage());
		}
	}

	public void tableRowCancelSelection() {
		setEditPanelRendered(false);
		setEntity(null);
		setSelection(null);
	}

}
