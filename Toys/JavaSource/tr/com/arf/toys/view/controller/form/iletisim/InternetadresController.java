package tr.com.arf.toys.view.controller.form.iletisim; 
  
 
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import tr.com.arf.framework.db.enumerated._common.EvetHayir;
import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.framework.db.model._base.BaseEntity;
import tr.com.arf.framework.utility.tool.KeyUtil;
import tr.com.arf.toys.db.enumerated.iletisim.NetAdresTuru;
import tr.com.arf.toys.db.filter.iletisim.InternetadresFilter;
import tr.com.arf.toys.db.model.iletisim.Internetadres;
import tr.com.arf.toys.db.model.kisi.Kisi;
import tr.com.arf.toys.db.model.kisi.Kurum;
import tr.com.arf.toys.db.model.system.Birim;
import tr.com.arf.toys.service.application.ApplicationDescriptor;
import tr.com.arf.toys.view.controller.form._base.ToysBaseFilteredController;
  
  
@ManagedBean 
@ViewScoped 
public class InternetadresController extends ToysBaseFilteredController {  
  
  private static final long serialVersionUID = 6531152165884980748L; 
  
	private Internetadres internetadres;  
	private InternetadresFilter internetadresFilter = new InternetadresFilter();  
  
	public InternetadresController() { 
		super(Internetadres.class);  
		setDefaultValues(false);  
		setOrderField("netadresmetni");  
		setAutoCompleteSearchColumns(new String[]{"netadresmetni"}); 
		setTable(null); 
		setLoggable(true);
		if(getMasterEntity() != null){ 
			if(getMasterEntity().getClass() == Kisi.class){
				setMasterObjectName(ApplicationDescriptor._KISI_MODEL);
				setMasterOutcome(ApplicationDescriptor._KISI_MODEL);
				getInternetadresFilter().setKisiRef((Kisi) getMasterEntity());				 
			} else if(getMasterEntity().getClass() == Kurum.class){
				setMasterObjectName(ApplicationDescriptor._KURUM_MODEL);
				getInternetadresFilter().setKurumRef((Kurum) getMasterEntity()); 
				setMasterOutcome(ApplicationDescriptor._KURUM_MODEL);				 
			} else if(getMasterEntity().getClass() == Birim.class){
				setMasterObjectName(ApplicationDescriptor._BIRIM_MODEL);
				getInternetadresFilter().setKurumRef(((Birim) getMasterEntity()).getKurumRef());
				setMasterOutcome(ApplicationDescriptor._BIRIM_MODEL);					 
			}    
			getInternetadresFilter().createQueryCriterias();  
		} 
		queryAction(); 
	} 
 
	public InternetadresController(Object object) { 
		super(Internetadres.class);   
		setLoggable(true); 
	}
	
	public InternetadresController(Kisi kisi) { 
		super(Internetadres.class);   
		setLoggable(true);
		if(kisi != null){  
			getInternetadresFilter().setKisiRef(kisi); 
		}  
	}
	
	public BaseEntity getMasterEntity(){
		if(getObjectFromSessionFilter(ApplicationDescriptor._KISI_MODEL) != null){
			return (Kisi) getObjectFromSessionFilter(ApplicationDescriptor._KISI_MODEL);
		} else if(getObjectFromSessionFilter(ApplicationDescriptor._KURUM_MODEL) != null){
			return (Kurum) getObjectFromSessionFilter(ApplicationDescriptor._KURUM_MODEL);
		} else if(getObjectFromSessionFilter(ApplicationDescriptor._BIRIM_MODEL) != null){
			return (Birim) getObjectFromSessionFilter(ApplicationDescriptor._BIRIM_MODEL);
		} else {
			return null;
		}			
	} 
	
	public Internetadres getInternetadres() { 
		internetadres = (Internetadres) getEntity(); 
		return internetadres; 
	} 
	
	public void setInternetadres(Internetadres internetadres) { 
		this.internetadres = internetadres; 
	} 
	
	public InternetadresFilter getInternetadresFilter() { 
		return internetadresFilter; 
	} 
	 
	public void setInternetadresFilter(InternetadresFilter internetadresFilter) {  
		this.internetadresFilter = internetadresFilter;  
	}  
	 
	@Override  
	public BaseFilter getFilter() {  
		return getInternetadresFilter();  
	}  
	
	@Override	
	public void save() { 
		String mail=null;
		if(this.internetadres.getNetadresturu() == NetAdresTuru._WEBSAYFASI){
			if (internetadres.getNetadresmetni()!=null){
				mail=internetadres.getNetadresmetni();
				if (mail.contains("www.")==false){
					createGenericMessage("Geçersiz Web Sayfası !", FacesMessage.SEVERITY_ERROR);
					return;
				}
			}	
		} else {
			if (internetadres.getNetadresmetni()!=null){
				mail=internetadres.getNetadresmetni();
				if (mail.contains("@")==false){
					createGenericMessage("Geçersiz Eposta Adresi !", FacesMessage.SEVERITY_ERROR);
					return;
				}
			}
		}
		super.justSave();	
		if(this.internetadres.getVarsayilan() == EvetHayir._EVET){
			String whereCondition = "o.rID <> " + this.internetadres.getRID() + " AND o.varsayilan =" + EvetHayir._EVET.getCode();
			if(this.internetadres.getKurumRef() != null) {
				whereCondition += " AND o.kurumRef.rID = " + this.internetadres.getKurumRef().getRID();
			} else if(this.internetadres.getKisiRef() != null){ 
				whereCondition += " AND o.kisiRef.rID = " + this.internetadres.getKisiRef().getRID();
			}
			
			try {
				if(getDBOperator().recordCount(getModelName(), whereCondition) > 0){
					for(Object obj : getDBOperator().load(getModelName(), whereCondition, "o.rID")){
						Internetadres internetadres = (Internetadres) obj;
						internetadres.setVarsayilan(EvetHayir._HAYIR);
						getDBOperator().update(internetadres);
					}					
				}
			} catch (Exception e) { 
				logYaz("Exception @" + getModelName() + "Controller :", e);
			} 
		} else if (this.internetadres.getVarsayilan() == EvetHayir._HAYIR){
			String whereCondition = "o.rID <> " + this.internetadres.getRID() + " AND o.varsayilan =" + EvetHayir._HAYIR.getCode();
			if(this.internetadres.getKurumRef() != null) {
				whereCondition += " AND o.kurumRef.rID = " + this.internetadres.getKurumRef().getRID();
			} else if(this.internetadres.getKisiRef() != null){ 
				whereCondition += " AND o.kisiRef.rID = " + this.internetadres.getKisiRef().getRID();
			}
			String whereCondition1 = "o.rID <> " + this.internetadres.getRID() + " AND o.varsayilan =" + EvetHayir._EVET.getCode();
			if(this.internetadres.getKurumRef() != null) {
				whereCondition1 += " AND o.kurumRef.rID = " + this.internetadres.getKurumRef().getRID();
			} else if(this.internetadres.getKisiRef() != null){ 
				whereCondition1 += " AND o.kisiRef.rID = " + this.internetadres.getKisiRef().getRID();
			}
			
			try {
				if(getDBOperator().recordCount(getModelName(), whereCondition) == 0 && getDBOperator().recordCount(getModelName(), whereCondition1) == 0){
					internetadres.setVarsayilan(EvetHayir._EVET);
					getDBOperator().update(internetadres);
				}	
				whereCondition = "o.rID <> " + this.internetadres.getRID() + " AND o.varsayilan =" + EvetHayir._EVET.getCode();
				if(this.internetadres.getKurumRef() != null) {
					whereCondition += " AND o.kurumRef.rID = " + this.internetadres.getKurumRef().getRID();
				} else if(this.internetadres.getKisiRef() != null){ 
					whereCondition += " AND o.kisiRef.rID = " + this.internetadres.getKisiRef().getRID();
				}
				if(getDBOperator().recordCount(getModelName(), whereCondition) == 0){ 
					createGenericMessage(KeyUtil.getMessageValue("varsayilan.hata"), FacesMessage.SEVERITY_INFO);  
					internetadres.setVarsayilan(EvetHayir._EVET);
					getDBOperator().update(internetadres);
				}
				
			}catch (Exception e) { 
				logYaz("Exception @" + getModelName() + "Controller :", e);
			}  
		}
		queryAction();
	}	
	
	@Override
	public void delete() {
		if (!setSelected()) {
			return;
		} 	    
		Long kurumRef = 0L;
		Long kisiRef = 0L; 
		if(getInternetadres().getKisiRef() != null){
			kisiRef = this.internetadres.getKisiRef().getRID();
		} else if(getInternetadres().getKurumRef() != null){
			kurumRef = this.internetadres.getKurumRef().getRID();
		}  		
		try {
			getDBOperator().delete(super.getEntity()); 
			setSelection(null); 
			setTable(null);
			createGenericMessage(KeyUtil.getMessageValue("kayit.silindi"), FacesMessage.SEVERITY_INFO); 
		} catch (Exception e) {
			createGenericMessage(KeyUtil.getMessageValue("islem.hata") + " Mes : " + e.getMessage(), FacesMessage.SEVERITY_ERROR);  
			return;
		} 
		 
		String whereCondition ="o.varsayilan =" + EvetHayir._EVET.getCode();
			if(kurumRef != 0) {
				whereCondition += " AND o.kurumRef.rID = " + kurumRef;
			} else if(kisiRef != 0){ 
				whereCondition += " AND o.kisiRef.rID = " + kisiRef;
			}
			
			try {
				if(getDBOperator().recordCount(getModelName(), whereCondition) == 0){
					whereCondition = "o.rID <> " + this.internetadres.getRID() + " AND o.varsayilan =" + EvetHayir._HAYIR.getCode();
					if(kurumRef != 0) {
						whereCondition += " AND o.kurumRef.rID = " + kurumRef;
					} else if(kisiRef != 0){ 
						whereCondition += " AND o.kisiRef.rID = " + kisiRef;
					}
					if(getDBOperator().recordCount(getModelName(), whereCondition)> 0){ 
						Internetadres intadres = (Internetadres) getDBOperator().load(getModelName(), whereCondition, "o.rID DESC").get(0);
						if (intadres!=null){
						intadres.setVarsayilan(EvetHayir._EVET);
						getDBOperator().update(intadres);
						}
					}
					
				}	 
			}catch (Exception e) { 
				logYaz("Exception @" + getModelName() + "Controller :", e);
			} 
			queryAction();	 
		}

	@Override	
	public void insert() {	
		super.insert();	
		getInternetadres().setVarsayilan(EvetHayir._HAYIR);
		if(getInternetadresFilter().getKisiRef() != null){ 
			getInternetadres().setKisiRef(getInternetadresFilter().getKisiRef());
		} else if(getInternetadresFilter().getKurumRef() != null) {
			getInternetadres().setKurumRef(getInternetadresFilter().getKurumRef());
		} 
	}	
	
} // class 
