package tr.com.arf.toys.view.controller.form.kisi; 
  
 

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.framework.db.model._base.BaseEntity;
import tr.com.arf.toys.db.filter.kisi.KomisyonDonemUyeDurumFilter;
import tr.com.arf.toys.db.model.kisi.KomisyonDonem;
import tr.com.arf.toys.db.model.kisi.KomisyonDonemUyeDurum;
import tr.com.arf.toys.service.application.ApplicationDescriptor;
import tr.com.arf.toys.view.controller.form._base.ToysBaseFilteredController;
  
  
@ManagedBean 
@ViewScoped 
public class KomisyonDonemUyeDurumController extends ToysBaseFilteredController {  
  
  private static final long serialVersionUID = 6531152165884980748L; 
  
	private KomisyonDonemUyeDurum komisyonDonemUyeDurum;  
	private KomisyonDonemUyeDurumFilter komisyonDonemUyeDurumFilter = new KomisyonDonemUyeDurumFilter();  
  
	public KomisyonDonemUyeDurumController() { 
		super(KomisyonDonemUyeDurum.class);  
		setDefaultValues(false);  
		setTable(null); 
		setOrderField("rID desc");
		if(getMasterEntity() != null){ 
			  if(getMasterEntity().getClass() == KomisyonDonem.class){
				setMasterObjectName(ApplicationDescriptor._KOMISYONDONEM_MODEL);
				setMasterOutcome(ApplicationDescriptor._KOMISYONDONEM_MODEL);	
				getKomisyonDonemUyeDurumFilter().setKomisyonDonemRef((KomisyonDonem) getMasterEntity());
				} 
			  else {
					setMasterOutcome(null);
				}
			}
		getKomisyonDonemUyeDurumFilter().createQueryCriterias();  
		queryAction(); 
	} 
	
	public BaseEntity getMasterEntity(){
		if(getObjectFromSessionFilter(ApplicationDescriptor._KOMISYONDONEM_MODEL) != null){
			return (KomisyonDonem) getObjectFromSessionFilter(ApplicationDescriptor._KOMISYONDONEM_MODEL);
		}  
		else {
			return null;
		}			
	} 
	
	
	public KomisyonDonemUyeDurum getKomisyonDonemUyeDurum() {
		komisyonDonemUyeDurum=(KomisyonDonemUyeDurum) getEntity();
		return komisyonDonemUyeDurum;
	}

	public void setKomisyonDonemUyeDurum(KomisyonDonemUyeDurum komisyonDonemUyeDurum) {
		this.komisyonDonemUyeDurum = komisyonDonemUyeDurum;
	}

	public KomisyonDonemUyeDurumFilter getKomisyonDonemUyeDurumFilter() {
		return komisyonDonemUyeDurumFilter;
	}

	public void setKomisyonDonemUyeDurumFilter(
			KomisyonDonemUyeDurumFilter komisyonDonemUyeDurumFilter) {
		this.komisyonDonemUyeDurumFilter = komisyonDonemUyeDurumFilter;
	}

	@Override
	public BaseFilter getFilter() {
		return getKomisyonDonemUyeDurumFilter();
	}
	
	@Override
		public void save() {
			// TODO Auto-generated method stub
			super.save();
		}

	
} // class 
