package tr.com.arf.toys.view.controller.form.system; 
  
 
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.toys.db.filter.system.UniversiteFakulteFilter;
import tr.com.arf.toys.db.model.system.Universite;
import tr.com.arf.toys.db.model.system.UniversiteFakulte;
import tr.com.arf.toys.service.application.ApplicationDescriptor;
import tr.com.arf.toys.view.controller.form._base.ToysBaseFilteredController;
  
  
@ManagedBean 
@ViewScoped 
public class UniversiteFakulteController extends ToysBaseFilteredController {  
  
  private static final long serialVersionUID = 6531152165884980748L; 
  
	private UniversiteFakulte universiteFakulte;  
	private UniversiteFakulteFilter universiteFakulteFilter = new UniversiteFakulteFilter();  
  
	public UniversiteFakulteController() { 
		super(UniversiteFakulte.class);  
		setDefaultValues(false);  
		setTable(null); 
		if (getMasterEntity() != null) {
			setMasterObjectName(ApplicationDescriptor._UNIVERSITE_MODEL);
			setMasterOutcome(ApplicationDescriptor._UNIVERSITE_MODEL);
			getUniversiteFakulteFilter().setUniversiteRef(getMasterEntity());
			getUniversiteFakulteFilter().createQueryCriterias();
		}
		queryAction();
	} 
	
	public Universite getMasterEntity(){
		if(getUniversiteFakulteFilter().getUniversiteRef() != null){
			return getUniversiteFakulteFilter().getUniversiteRef();
		} else {
			return (Universite) getObjectFromSessionFilter(ApplicationDescriptor._UNIVERSITE_MODEL);
		}			
	}
	
	public UniversiteFakulte getUniversiteFakulte() {
		universiteFakulte=(UniversiteFakulte) getEntity();
		return universiteFakulte;
	}

	public void setUniversiteFakulte(UniversiteFakulte universiteFakulte) {
		this.universiteFakulte = universiteFakulte;
	}

	public UniversiteFakulteFilter getUniversiteFakulteFilter() {
		return universiteFakulteFilter;
	}

	public void setUniversiteFakulteFilter(
			UniversiteFakulteFilter universiteFakulteFilter) {
		this.universiteFakulteFilter = universiteFakulteFilter;
	}

	@Override  
	public BaseFilter getFilter() {  
		return getUniversiteFakulteFilter();
	}  
	@Override
		public void save() {
			super.justSave();
			if (getMasterEntity()!=null)
			universiteFakulte.setUniversiteRef(getMasterEntity());
			super.save();
			queryAction();
			
		}
	
} // class 
