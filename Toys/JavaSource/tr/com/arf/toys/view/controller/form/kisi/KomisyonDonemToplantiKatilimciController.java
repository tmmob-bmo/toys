package tr.com.arf.toys.view.controller.form.kisi; 
  
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.model.SelectItem;
import javax.persistence.Transient;

import org.primefaces.model.DualListModel;

import tr.com.arf.framework.db.enumerated._common.EvetHayir;
import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.framework.db.model._base.BaseEntity;
import tr.com.arf.framework.utility.exception.data.DBException;
import tr.com.arf.toys.db.filter.kisi.KomisyonDonemToplantiKatilimciFilter;
import tr.com.arf.toys.db.model.kisi.KomisyonDonemToplanti;
import tr.com.arf.toys.db.model.kisi.KomisyonDonemToplantiKatilimci;
import tr.com.arf.toys.db.model.kisi.KomisyonDonemUye;
import tr.com.arf.toys.service.application.ApplicationDescriptor;
import tr.com.arf.toys.view.controller.form._base.ToysBaseFilteredController;
  
  
@ManagedBean 
@ViewScoped 
public class KomisyonDonemToplantiKatilimciController extends ToysBaseFilteredController {  
  
  private static final long serialVersionUID = 6531152165884980748L; 
  
	private KomisyonDonemToplantiKatilimci komisyonDonemToplantiKatilimci;  
	private KomisyonDonemToplantiKatilimciFilter komisyonDonemToplantiKatilimciFilter = new KomisyonDonemToplantiKatilimciFilter();  
  
	public KomisyonDonemToplantiKatilimciController() { 
		super(KomisyonDonemToplantiKatilimci.class);  
		setDefaultValues(false);  
		setTable(null); 
		if(getMasterEntity() != null){ 
			setMasterObjectName(ApplicationDescriptor._KOMISYONDONEMTOPLANTI_MODEL);
			setMasterOutcome(ApplicationDescriptor._KOMISYONDONEMTOPLANTI_MODEL);
			getKomisyonDonemToplantiKatilimciFilter().setKomisyonDonemToplantiRef(getMasterEntity());
			getKomisyonDonemToplantiKatilimciFilter().createQueryCriterias();   
		} 
		queryAction(); 
	} 
	
	public KomisyonDonemToplanti getMasterEntity(){
		if(getKomisyonDonemToplantiKatilimciFilter().getKomisyonDonemToplantiRef() != null){
			return getKomisyonDonemToplantiKatilimciFilter().getKomisyonDonemToplantiRef();
		} else {
			return (KomisyonDonemToplanti) getObjectFromSessionFilter(ApplicationDescriptor._KOMISYONDONEMTOPLANTI_MODEL);
		}			
	} 
	

	public KomisyonDonemToplantiKatilimci getKomisyonDonemToplantiKatilimci() {
		komisyonDonemToplantiKatilimci = (KomisyonDonemToplantiKatilimci) getEntity(); 
		return komisyonDonemToplantiKatilimci;
	}

	public void setKomisyonDonemToplantiKatilimci(
			KomisyonDonemToplantiKatilimci komisyonDonemToplantiKatilimci) {
		this.komisyonDonemToplantiKatilimci = komisyonDonemToplantiKatilimci;
	}

	public KomisyonDonemToplantiKatilimciFilter getKomisyonDonemToplantiKatilimciFilter() {
		return komisyonDonemToplantiKatilimciFilter;
	}

	public void setKomisyonDonemToplantiKatilimciFilter(
			KomisyonDonemToplantiKatilimciFilter komisyonDonemToplantiKatilimciFilter) {
		this.komisyonDonemToplantiKatilimciFilter = komisyonDonemToplantiKatilimciFilter;
	}


	@Override  
	public BaseFilter getFilter() {  
		return getKomisyonDonemToplantiKatilimciFilter();  
	} 
	
	private SelectItem[] komisyonDonemUyeItems = new SelectItem[0];
	
	@Transient
	public SelectItem[] getKomisyonDonemUye(){ 
			this.komisyonDonemToplantiKatilimci.setKomisyonDonemToplantiRef(komisyonDonemToplantiKatilimciFilter.getKomisyonDonemToplantiRef());
			@SuppressWarnings("unchecked")
			List <KomisyonDonemUye> komisyonDonemUyeListesi=getDBOperator().load(KomisyonDonemUye.class.getSimpleName(), "o.komisyonDonemRef.rID=" + komisyonDonemToplantiKatilimciFilter.getKomisyonDonemToplantiRef().getKomisyonDonemRef().getRID() , "");
			if (!isEmpty(komisyonDonemUyeListesi)){ 
				int x=1;
				komisyonDonemUyeItems = new SelectItem[komisyonDonemUyeListesi.size() + 1];
				komisyonDonemUyeItems[0] = new SelectItem(null, "");
				for (KomisyonDonemUye komisyonDonemUye : komisyonDonemUyeListesi){
					komisyonDonemUyeItems[x]=new SelectItem(komisyonDonemUye, komisyonDonemUye.getUIString());
					x++; 
				} 
			} else { 
				komisyonDonemUyeItems = new SelectItem[0];
			}
			return komisyonDonemUyeItems;
	}
	
	@Override
	public void insert() {
		super.insert();
		try {
			if (getDBOperator().recordCount(KomisyonDonemUye.class.getSimpleName(), "o.komisyonDonemRef.rID=" +getMasterEntity().getKomisyonDonemRef().getRID())<=0){
				super.fillDualListFor(KomisyonDonemUye.class.getSimpleName(), "o.v.rID=" +getMasterEntity().getKomisyonDonemRef().getRID(), "");
			}
			else{
				update();
			}
		} catch (DBException e) {
			// TODO Auto-generated catch block
			logYaz("Exception @" + getModelName() + "Controller :", e);
		}
	}

@Override
	public void save() {
	try {	
		if(!isEmpty(getDualList().getTarget())){
			List<BaseEntity> komisyonDonemUyeList = getDualList().getTarget(); 			
			for(BaseEntity b : komisyonDonemUyeList){
				
					if (getDBOperator().recordCount(KomisyonDonemToplantiKatilimci.class.getSimpleName(),"o.komisyonDonemToplantiRef.rID=" +getMasterEntity().getRID() 
							+" AND o.komisyonDonemUyeRef.rID=" + ((KomisyonDonemUye)b).getRID() )>0){
						KomisyonDonemToplantiKatilimci kdtk=(KomisyonDonemToplantiKatilimci) getDBOperator().load(KomisyonDonemToplantiKatilimci.class.getSimpleName(),
								"o.komisyonDonemToplantiRef.rID=" +getMasterEntity().getRID() 	+" AND o.komisyonDonemUyeRef.rID=" + ((KomisyonDonemUye)b).getRID(),"").get(0);
						getDBOperator().update(kdtk);
					}
					else {
						komisyonDonemToplantiKatilimci=new KomisyonDonemToplantiKatilimci();
						komisyonDonemToplantiKatilimci.setKatilim(EvetHayir._EVET);
						komisyonDonemToplantiKatilimci.setKomisyonDonemToplantiRef(getMasterEntity());
						komisyonDonemToplantiKatilimci.setKomisyonDonemUyeRef((KomisyonDonemUye)b);
						getDBOperator().insert(komisyonDonemToplantiKatilimci);
					}
			}
		}
		if (!isEmpty(getDualList().getSource())){
			List<BaseEntity> komisyonDonemUyeList = getDualList().getSource(); 
			for(BaseEntity b : komisyonDonemUyeList){
				if (getDBOperator().recordCount(KomisyonDonemToplantiKatilimci.class.getSimpleName(),"o.komisyonDonemToplantiRef.rID=" +getMasterEntity().getRID() 
						+" AND o.komisyonDonemUyeRef.rID=" + ((KomisyonDonemUye)b).getRID() )>0){
					KomisyonDonemToplantiKatilimci kdtk=(KomisyonDonemToplantiKatilimci) getDBOperator().load(KomisyonDonemToplantiKatilimci.class.getSimpleName(),
							"o.komisyonDonemToplantiRef.rID=" +getMasterEntity().getRID() 	+" AND o.komisyonDonemUyeRef.rID=" + ((KomisyonDonemUye)b).getRID(),"").get(0);
					getDBOperator().delete(kdtk);
				}
			}
		}
	} catch (DBException e) {
		// TODO Auto-generated catch block
		logYaz("Exception @" + getModelName() + "Controller :", e);
	}
		queryAction();
	}

@SuppressWarnings("unchecked")
@Override
	public void update() {
		super.update();
		try {
			if (getDBOperator().recordCount(KomisyonDonemToplantiKatilimci.class.getSimpleName(),"o.komisyonDonemToplantiRef.komisyonDonemRef.rID="
					+ getMasterEntity().getKomisyonDonemRef().getRID())>0){
				List<BaseEntity> target =new ArrayList<BaseEntity>();
				List<BaseEntity> source =new ArrayList<BaseEntity>();
				List<Long> komisyonDonemUyeRID=new ArrayList<Long>();
				List <KomisyonDonemToplantiKatilimci> komisyonToplantiKatilimciList =getDBOperator().load(KomisyonDonemToplantiKatilimci.class.getSimpleName(),
						"o.komisyonDonemToplantiRef.komisyonDonemRef.rID=" + getMasterEntity().getKomisyonDonemRef().getRID(),"");
				for (KomisyonDonemToplantiKatilimci entity : komisyonToplantiKatilimciList){
					target.add(entity.getKomisyonDonemUyeRef());
					komisyonDonemUyeRID.add(entity.getKomisyonDonemUyeRef().getRID());
				}
				Long rID=0L;
				if (getDBOperator().recordCount(KomisyonDonemUye.class.getSimpleName() , "  o.komisyonDonemRef.rID=" +getMasterEntity().getKomisyonDonemRef().getRID())>0){
					List <KomisyonDonemUye> komisyonDonemUyeList=getDBOperator().load(KomisyonDonemUye.class.getSimpleName() , "o.komisyonDonemRef.rID=" +getMasterEntity().getKomisyonDonemRef().getRID(),"");
					for (KomisyonDonemUye entity : komisyonDonemUyeList){
						for (Long komisyonuyeRID : komisyonDonemUyeRID){
							rID=komisyonuyeRID;
							if (entity.getRID() ==komisyonuyeRID){
								break;
							}
						}
						if (entity.getRID()!=rID){
							source.add(entity);
						}
					}
					setDualList(new DualListModel<BaseEntity>(source, target)); 
				}
			} else {
				super.fillDualListFor(KomisyonDonemUye.class.getSimpleName(), "o.komisyonDonemRef.rID=" +getMasterEntity().getKomisyonDonemRef().getRID(), "");
			}
		} catch (DBException e) {
			logYaz("Exception @" + getModelName() + "Controller :", e);
		}
	}


	

 
} // class 
