package tr.com.arf.toys.view.controller.form.system;


import java.util.Locale;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.primefaces.component.datatable.DataTable;

import tr.com.arf.framework.db.enumerated._common.IslemTuru;
import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.framework.db.model._base.BaseEntity;
import tr.com.arf.framework.utility.exception.data.DBException;
import tr.com.arf.framework.utility.tool.KeyUtil;
import tr.com.arf.toys.db.filter.system.DuyuruFilter;
import tr.com.arf.toys.db.model.system.Duyuru;
import tr.com.arf.toys.view.controller.form._base.ToysBaseFilteredController;


@ManagedBean
@ViewScoped
public class DuyuruController extends ToysBaseFilteredController {

  private static final long serialVersionUID = 6531152165884980748L;

	private Duyuru duyuru;
	private DuyuruFilter duyuruFilter = new DuyuruFilter();

	public DuyuruController() {
		super(Duyuru.class);
		setDefaultValues(false);
		setOrderField("duyurubasligi");
		setAutoCompleteSearchColumns(new String[]{"duyurubasligi"});
		setTable(null);
		queryAction();
	}

	public Duyuru getDuyuru() {
		duyuru = (Duyuru) getEntity();
		return duyuru;
	}

	public void setDuyuru(Duyuru duyuru) {
		this.duyuru = duyuru;
	}

	public DuyuruFilter getDuyuruFilter() {
		return duyuruFilter;
	}

	public void setDuyuruFilter(DuyuruFilter duyuruFilter) {
		this.duyuruFilter = duyuruFilter;
	}

	@Override
	public BaseFilter getFilter() {
		return getDuyuruFilter();
	}

	private String detailEditType=null;
	private DataTable duyuruTable;

	public String getDetailEditType() {
		return detailEditType;
	}

	public void setDetailEditType(String detailEditType) {
		this.detailEditType = detailEditType;
	}

	public DataTable getDuyuruTable() {
		return duyuruTable;
	}

	public void setDuyuruTable(DataTable duyuruTable) {
		this.duyuruTable = duyuruTable;
	}

	public void deleteDetail(String detailType, Object entity) throws DBException{
		this.detailEditType = null;
		detailType = detailType.toLowerCase(Locale.ENGLISH);
		 if (detailType.equalsIgnoreCase("duyuru")) {
			this.duyuru = (Duyuru) entity;
			justDelete(this.duyuru, false);
		}
	}
	@Override
	public void justDelete(BaseEntity objectToBeDeleted, boolean doLog) {
		justDeleteGivenObject(objectToBeDeleted, doLog);
	}
	private boolean doDelete;

	public void setDoDelete(boolean doDelete) {
		this.doDelete = doDelete;
	}

	@Override
	public void justDeleteGivenObject(BaseEntity objectToBeDeleted, boolean doLog) {
		setDuyuruTable(null);
		setDoDelete(true);
		setEditPanelRendered(false);
		setEditPanelRendered(false);
		BaseEntity obj = null;
		Long rid = 0L;
		try {
			if(doLog){
				obj = (BaseEntity) objectToBeDeleted.cloneObject();
				rid = objectToBeDeleted.getRID();
			}
			if(getDBOperator().delete(objectToBeDeleted)){
				if(doLog){
					obj.setRID(rid);
					logKaydet(obj, IslemTuru._SILME, "");
				}
				createGenericMessage(KeyUtil.getMessageValue("kayit.silindi"), FacesMessage.SEVERITY_INFO);
			}  else {
				createGenericMessage(KeyUtil.getMessageValue("islem.hata"), FacesMessage.SEVERITY_INFO);
			}
		} catch (Exception e) {
			createGenericMessage(KeyUtil.getMessageValue("islem.hata"), FacesMessage.SEVERITY_INFO);
		}
	}

	public boolean isDoDelete() {
		return doDelete;
	}


} // class
