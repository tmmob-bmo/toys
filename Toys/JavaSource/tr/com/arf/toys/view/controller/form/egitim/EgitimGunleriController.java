package tr.com.arf.toys.view.controller.form.egitim; 
 
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.toys.db.filter.egitim.EgitimGunleriFilter;
import tr.com.arf.toys.db.model.egitim.Egitim;
import tr.com.arf.toys.db.model.egitim.EgitimGunleri;
import tr.com.arf.toys.service.application.ApplicationDescriptor;
import tr.com.arf.toys.view.controller.form._base.ToysBaseFilteredController;
  
  
@ManagedBean 
@ViewScoped 
public class EgitimGunleriController extends ToysBaseFilteredController {  
  
  private static final long serialVersionUID = 6531152165884980748L; 
  
	private EgitimGunleri egitimGunleri;  
	private EgitimGunleriFilter egitimGunleriFilter = new EgitimGunleriFilter();  
  
	public EgitimGunleriController() { 
		super(EgitimGunleri.class);  
		setDefaultValues(false);  
		setLoggable(false);  
		setTable(null); 
		if (getMasterEntity() != null) {
			setMasterObjectName(ApplicationDescriptor._EGITIM_MODEL);
			setMasterOutcome(ApplicationDescriptor._EGITIM_MODEL);
			getEgitimGunleriFilter().setEgitimRef(getMasterEntity());
			getEgitimGunleriFilter().createQueryCriterias();
		}
		queryAction();
	}
	
	public Egitim getMasterEntity() {
		if (getEgitimGunleriFilter().getEgitimRef() != null) {
			return getEgitimGunleriFilter().getEgitimRef();
		} else {
			return (Egitim) getObjectFromSessionFilter(ApplicationDescriptor._EGITIM_MODEL);
		}
	}
	
	public EgitimGunleri getEgitimGunleri() {
		egitimGunleri=(EgitimGunleri) getEntity();
		return egitimGunleri;
	}

	public void setEgitimGunleri(EgitimGunleri egitimGunleri) {
		this.egitimGunleri = egitimGunleri;
	}

	public EgitimGunleriFilter getEgitimGunleriFilter() {
		return egitimGunleriFilter;
	}

	public void setEgitimGunleriFilter(EgitimGunleriFilter egitimGunleriFilter) {
		this.egitimGunleriFilter = egitimGunleriFilter;
	}

	@Override  
	public BaseFilter getFilter() {  
		return getEgitimGunleriFilter(); 
	}  
	
	@Override
		public void save() {
			
			if (getMasterEntity()!=null){
				egitimGunleri.setEgitimRef(getMasterEntity());
			}else {
				createGenericMessage("Eğitim Bilgileri Boş Geldiğinden Dolayı Kaydetme İşlemini"
						+ " Gerçekleştiremiyoruz!", FacesMessage.SEVERITY_INFO);
				return ;
			}
			super.save();
			queryAction();
		}
	
} // class 
