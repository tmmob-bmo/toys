package tr.com.arf.toys.view.controller.form.evrak;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpServletResponse;

import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.framework.db.model._base.BaseEntity;
import tr.com.arf.framework.utility.exception.data.DBException;
import tr.com.arf.framework.utility.tool.DateUtil;
import tr.com.arf.framework.utility.tool.KeyUtil;
import tr.com.arf.toys.db.enumerated.evrak.AciliyetTuru;
import tr.com.arf.toys.db.enumerated.evrak.EvrakDurum;
import tr.com.arf.toys.db.enumerated.evrak.EvrakHareketTuru;
import tr.com.arf.toys.db.enumerated.evrak.EvrakTuru;
import tr.com.arf.toys.db.enumerated.evrak.GizlilikDerecesi;
import tr.com.arf.toys.db.enumerated.evrak.HazirEvrakTuru;
import tr.com.arf.toys.db.enumerated.evrak.TeslimTuru;
import tr.com.arf.toys.db.enumerated.iletisim.AdresTipi;
import tr.com.arf.toys.db.filter.evrak.EvrakFilter;
import tr.com.arf.toys.db.model.evrak.Dolasimsablon;
import tr.com.arf.toys.db.model.evrak.Dolasimsablondetay;
import tr.com.arf.toys.db.model.evrak.Evrak;
import tr.com.arf.toys.db.model.evrak.Evrakdolasim;
import tr.com.arf.toys.db.model.evrak.Evrakdolasimbildirim;
import tr.com.arf.toys.db.model.evrak.Evrakek;
import tr.com.arf.toys.db.model.iletisim.Adres;
import tr.com.arf.toys.db.model.kisi.Kisikimlik;
import tr.com.arf.toys.db.model.kisi.Kisiogrenim;
import tr.com.arf.toys.db.model.system.Kullanici;
import tr.com.arf.toys.db.model.system.Sehir;
import tr.com.arf.toys.db.model.system.SistemParametre;
import tr.com.arf.toys.db.model.uye.Uye;
import tr.com.arf.toys.service.application.ApplicationDescriptor;
import tr.com.arf.toys.service.application.ManagedBeanLocator;
import tr.com.arf.toys.service.export.CustomPdfExport;
import tr.com.arf.toys.utility.application.ApplicationConstant;
import tr.com.arf.toys.view.controller.form._base.ToysBaseFilteredController;

@ManagedBean
@ViewScoped
public class EvrakController extends ToysBaseFilteredController {

	private static final long serialVersionUID = 6531152165884980748L;

	private Evrak evrak;
	private Evrak evrakDetay;
	private EvrakFilter evrakFilter = new EvrakFilter();
	private String selectedTab;
	private String detailEditType;

	private boolean oncekiAdimPanelRendered = false;
	private boolean sonrakiAdimPanelRendered = false;
	private boolean onayPanelRendered = false;
	private String aciklama = "";

	private EvrakekController evrakekController = new EvrakekController(null);

	private Evrakek evrakek = new Evrakek();
	private String oldValueForEvrakek;

	public EvrakController() {
		super(Evrak.class);
		setDefaultValues(false);
		setOrderField("rID DESC");
		setStaticWhereCondition(ApplicationConstant._birimStaticWhereCondition);
		setAutoCompleteSearchColumns(new String[] { "evrakno" });
		setLoggable(true);
		setTable(null);
		getEvrakFilter().setYil(DateUtil.getYear(new Date()));
		setStaticWhereCondition(getSessionUser().createStaticWhereCondition("o.hazirlayanPersonelRef."));
		if (getObjectFromSessionFilter(getModelName() + "Filter") != null) {
			this.evrakFilter = (EvrakFilter) getObjectFromSessionFilter(getModelName() + "Filter");
			queryAction();
		}
		getEvrakFilter().createQueryCriterias();
	}

	@Override
	@PostConstruct
	public void init() {
		if (getObjectFromSessionFilter(getModelName()) != null) {
			setEntity((BaseEntity) getObjectFromSessionFilter(getModelName()));
		} else if (!isEmpty(getList())) {
			setEvrak((Evrak) getList().get(0));
			setEntity(getList().get(0));
		}
	}

	public void deletePreviousDosyaKodu() {
		getEvrak().setDosyaKoduRef(null);
		getEvrak().setDosyaKoduRefPrev(null);
	}

	public Evrak getMasterEntity() {
		if (getEvrak() != null && getEvrak().getRID() != null) {
			return getEvrak();
		} else if (getObjectFromSessionFilter(getModelName()) != null) {
			return (Evrak) getObjectFromSessionFilter(getModelName());
		} else {
			return null;
		}
	}

	public void createEvrakDetay(String tabSelected) {
		setSelectedTab(tabSelected);
		getSessionUser().setLegalAccess(1);
		if (tabSelected.equalsIgnoreCase("evrakek")) {
			evrakEkGuncelle(this.evrakDetay);
		}
	}

	public void newDetail(String detailType) {
		detailType = detailType.toLowerCase(Locale.ENGLISH);
		this.detailEditType = detailType;
		RequestContext context = RequestContext.getCurrentInstance();
		context.update("dialogUpdateBox");

		if (detailType.equalsIgnoreCase("evrakek")) {
			this.evrakek = new Evrakek();
			this.evrakek.setEvrakRef(this.evrakDetay);
			this.oldValueForEvrakek = "";
		}
	}

	public void updateDetail(String detailType, Object entity) {
		detailType = detailType.toLowerCase(Locale.ENGLISH);
		this.detailEditType = detailType;
		RequestContext context = RequestContext.getCurrentInstance();
		context.update("dialogUpdateBox");

		if (detailType.equalsIgnoreCase("evrakek")) {
			this.evrakek = (Evrakek) entity;
			this.oldValueForEvrakek = this.evrakek.getValue();
		}
	}

	@Override
	public void update() {
		if (!setSelected()) {
			return;
		}
		if (getEvrak().getDurum() != EvrakDurum._TASLAK) {
			createGenericMessage(KeyUtil.getMessageValue("evrak.taslakDegil"), FacesMessage.SEVERITY_ERROR);
			return;
		}
		if (getEvrak().getGittigiKurumRef() != null) {
			getEvrak().setGittigiKurumRefPrev(getEvrak().getGittigiKurumRef());
		}
		if (getEvrak().getGittigiUyeRef() != null) {
			getEvrak().setGittigiUyeRefPrev(getEvrak().getGittigiUyeRef());
		}
		if (getEvrak().getDosyaKoduRef() != null) {
			getEvrak().setDosyaKoduRefPrev(getEvrak().getDosyaKoduRefPrev());
		}
		closeAllPanels();
		super.update();
	}

	@Override
	public void delete() {
		if (!setSelected()) {
			return;
		}
		if (getEvrak().getDurum() != EvrakDurum._TASLAK) {
			createGenericMessage(KeyUtil.getMessageValue("evrak.taslakDegil"), FacesMessage.SEVERITY_ERROR);
			return;
		}
		super.delete();
	}

	public void deleteDetail(String detailType, Object entity) throws DBException {
		this.detailEditType = null;
		detailType = detailType.toLowerCase(Locale.ENGLISH);
		if (detailType.equalsIgnoreCase("evrakek")) {
			this.evrakek = (Evrakek) entity;
			String path = this.evrakek.getPath();
			super.justDelete(this.evrakek, this.evrakekController.isLoggable());
			super.deleteFile(path);
			this.evrakekController.queryAction();
		}
	}

	public void saveDetails() throws Exception {
		RequestContext context = RequestContext.getCurrentInstance();
		if (this.detailEditType.equalsIgnoreCase("evrakek")) {
			if (!isEmpty(this.evrakek.getPath())) {
				super.justSave(this.evrakek, this.evrakekController.isLoggable(), this.oldValueForEvrakek);
				this.evrakek = new Evrakek();
				evrakEkGuncelle(this.evrakDetay);
				updateDialogAndForm(context);
			}
		}
	}

	private void updateDialogAndForm(RequestContext context) {
		this.detailEditType = null;
		context.update("dialogUpdateBox");
		context.update("evrakForm");
	}

	private void evrakEkGuncelle(Evrak evrakRef) {
		this.evrakekController.setTable(null);
		this.evrakekController.getEvrakekFilter().setEvrakRef(evrakRef);
		this.evrakekController.getEvrakekFilter().createQueryCriterias();
		this.evrakekController.queryAction();
	}

	public Evrak getEvrak() {
		evrak = (Evrak) getEntity();
		return evrak;
	}

	public void setEvrak(Evrak evrak) {
		this.evrak = evrak;
	}

	public EvrakFilter getEvrakFilter() {
		return evrakFilter;
	}

	public void setEvrakFilter(EvrakFilter evrakFilter) {
		this.evrakFilter = evrakFilter;
	}

	@Override
	public BaseFilter getFilter() {
		return getEvrakFilter();
	}

	public String evrakek() {
		if (!setSelected()) {
			return "";
		}
		getSessionUser().setLegalAccess(1);
		putObjectToSessionFilter(ApplicationDescriptor._EVRAK_MODEL, getEvrak());
		putObjectToSessionFilter(getModelName() + "Filter", getEvrakFilter());
		return ManagedBeanLocator.locateMenuController().gotoPage("menu_Evrakek");
	}

	public String evrakdolasim() {
		if (!setSelected()) {
			return "";
		}
		getSessionUser().setLegalAccess(1);
		putObjectToSessionFilter(ApplicationDescriptor._EVRAK_MODEL, getEvrak());
		putObjectToSessionFilter(getModelName() + "Filter", getEvrakFilter());
		return ManagedBeanLocator.locateMenuController().gotoPage("menu_EvrakDolasim");
	}

	@Override
	public void insert() {
		closeAllPanels();
		super.insert();
		getEvrak().setYil(DateUtil.getYear(new Date()));
		getEvrak().setKayittarihi(new Date());
		getEvrak().setKayitno(getEvrak().getYil() + " - .......");
		getEvrak().setDurum(EvrakDurum._TASLAK);
		getEvrak().setEvrakHareketTuru(EvrakHareketTuru._GELEN);

		// TODO TEST
		getEvrak().setTeslimturu(TeslimTuru._KARGO);
		getEvrak().setGizlilikderecesi(GizlilikDerecesi._HIZMETEOZEL);
		getEvrak().setAciliyetturu(AciliyetTuru._NORMAL);
		getEvrak().setEvrakturu(EvrakTuru._RESMI);
	}

	public boolean isRenderPanelGrids() {
		if (getEvrak().getEvrakHareketTuru() == null || getEvrak().getHazirEvrakTuru() == null || getEvrak().getEvrakHareketTuru() == EvrakHareketTuru._NULL || getEvrak().getHazirEvrakTuru() == HazirEvrakTuru._NULL) {
			return false;
		} else {
			return true;
		}
	}

	public void createEvrakIcerik() {
		if (getEvrak() != null) {
			SistemParametre sistemParam = ManagedBeanLocator.locateSessionController().getSistemParametre();
			String odaAdi = ".... Odası";
			if (getEvrak().getHazirEvrakTuru() == HazirEvrakTuru._BILIRKISILIKCEVABI) {
				getEvrak().setKonusu("Bilirkişilik talebiniz hakkında");
				String icerik = ApplicationConstant._bilirkisilikTalepCevapEvrakMetin;
				icerik = icerik.replace("$YIL$", DateUtil.getYear(new Date()) + "");
				icerik = icerik.replace("$BILIRKISILIKHIZMETBEDELI$", sistemParam.getBilirkisiHizmetBedeli());
				icerik = icerik.replace("$BILIRKISILIKRAPORBEDELI$", sistemParam.getBilirkisiHizmetBedeli());
				icerik = icerik.replace("$BILIRKISILIKTOPLAMHIZMETBEDELI$", sistemParam.getBilirkisilikToplamHizmetBedeli());
				if (ManagedBeanLocator.locateSessionController().getSistemParametre() != null) {
					if (!isEmpty(ManagedBeanLocator.locateSessionController().getSistemParametre().getOdaadi())) {
						odaAdi = ManagedBeanLocator.locateSessionController().getSistemParametre().getOdaadi();
					}
					icerik = icerik.replace("$ODAADI$", odaAdi);
				}
				if (getSessionUser().getPersonelRef() != null) {
					icerik = icerik.replace("$PERSONEL$", getSessionUser().getPersonelRef().getKisiRef().getUIStringShort());
				} else {
					icerik = icerik.replace("$PERSONEL$", "....");
				}
				// Sistem Parametresi Verileri
				icerik = icerik.replace("$HESAPADI$", sistemParam.getHesapAdi());
				icerik = icerik.replace("$BANKAADI$", sistemParam.getBankaAdi());
				icerik = icerik.replace("$BANKASUBEADI$", sistemParam.getSubeAdi());
				icerik = icerik.replace("$IBANNO$", sistemParam.getIbanNo());
				icerik = icerik.replace("$HESAPNO$", sistemParam.getHesapNo());
				getEvrak().setIcerik(icerik);
			} else if (getEvrak().getGittigiKurumRef() != null) {
				if (getEvrak().getHazirEvrakTuru() == HazirEvrakTuru._GENELEVRAK) {
					String icerik = ApplicationConstant._kurumaGidenGenelEvrak;
					if (getEvrak().getGittigiKurumRef().getSehirRef() != null) {
						icerik = icerik.replace("$SEHIR$", getEvrak().getGittigiKurumRef().getSehirRef().getAd());
					} else {
						icerik = icerik.replace("", getEvrak().getGittigiKurumRef().getSehirRef().getAd());
					}
					icerik = icerik.replace("$KURUMADI$", getEvrak().getGittigiKurumRef().getAd());
					getEvrak().setIcerik(icerik);
				}
			} else if (getEvrak().getGittigiUyeRef() != null) {
				String icerik = "";
				Uye secilenUye = getEvrak().getGittigiUyeRef();
				if (getEvrak().getHazirEvrakTuru() == HazirEvrakTuru._UYELIKAYRILMATALEPCEVABI) {
					getEvrak().setKonusu("Üyelikten ayrılma talebiniz hakkında");
					icerik = ApplicationConstant._uyelikIptalTalepCevapEvrakMetin;
				} else if (getEvrak().getHazirEvrakTuru() == HazirEvrakTuru._BILIRKISILIKGOREVLENDIRME) {
					getEvrak().setKonusu("Bilirkişilik hizmeti görevlendirmesi hk.");
					icerik = ApplicationConstant._bilirkisilikGorevlendirmeMetin;
				} else if (getEvrak().getHazirEvrakTuru() == HazirEvrakTuru._EMEKLILIKTALEBI) {
					getEvrak().setKonusu("Üyelikten ayrılma talebiniz hakkında.");
					icerik = ApplicationConstant._emeklilikBildirimMetin;
				} else if (getEvrak().getHazirEvrakTuru() == HazirEvrakTuru._YABANCIUYEBILDIRIMYENILEME) {
					getEvrak().setKonusu("Üyelik Kaydı hakkında.");
					getEvrak().setIlgi("12 Nisan 2013 tarihli ve 759 sayılı TMMOB yazısı");
					icerik = ApplicationConstant._yabanciUyeBildirimYenileme;
					icerik = icerik.replace("$AYLIKNETASGARIUCRET$", sistemParam.getAsgariAylikNetUcret());
					icerik = icerik.replace("$YABANCIUYEKAYITUCRETI$", sistemParam.getYabanciUyeKayitUcreti());
					icerik = icerik.replace("$YABANCIUYEAIDATBEDELI$", sistemParam.getYabanciUyeAidatUcreti());
				} else if (getEvrak().getHazirEvrakTuru() == HazirEvrakTuru._BILIRKISILIKBELGESI) {
					getEvrak().setKonusu("Üye Bilirkişilik Belgesi.");
					icerik = ApplicationConstant._bilirkisilikBildirim;
				} else if (getEvrak().getHazirEvrakTuru() == HazirEvrakTuru._IHALEBELGESI && getEvrak().getGittigiKurumRef() != null) {
					getEvrak().setKonusu("Üyelik Belgesi. (İhale)");
					icerik = ApplicationConstant._ihaleBelgesi;
					icerik = icerik.replace("$KURUMADI$", getEvrak().getGittigiKurumRef().getAd());
					icerik = icerik.replace("$SEHIR$", getEvrak().getGittigiKurumRef().getSehirRef().getAd());
				} else if (getEvrak().getHazirEvrakTuru() == HazirEvrakTuru._ODAUYELIKBELGESI) {
					getEvrak().setKonusu("Üyelik Belgesi.");
					icerik = ApplicationConstant._odaUyelikBelgesi;
				} else if (getEvrak().getHazirEvrakTuru() == HazirEvrakTuru._ODAUYELIKBELGESIADALETKOMISYONU) {
					getEvrak().setKonusu("Üyelik Belgesi (Adalet Komisyonu).");
					icerik = ApplicationConstant._odaUyelikBelgesiAdaletKomisyonu;
				} else if (getEvrak().getHazirEvrakTuru() == HazirEvrakTuru._SANTIYESEFLIGIBELGESI && getEvrak().getGittigiKurumRef() != null) {
					getEvrak().setKonusu("Üyelik Belgesi. (Şantiye Şefliği)");
					icerik = ApplicationConstant._santiyeSefligiBelgesi;
					icerik = icerik.replace("$KURUMADI$", getEvrak().getGittigiKurumRef().getAd());
					icerik = icerik.replace("$SEHIR$", getEvrak().getGittigiKurumRef().getSehirRef().getAd());
				}

				// Kimlik Bilgileri degistiriliyor
				try {
					if (getDBOperator().recordCount(Kisikimlik.class.getSimpleName(), "o.kisiRef.rID=" + secilenUye.getKisiRef().getRID()) > 0) {
						Kisikimlik kimlik = (Kisikimlik) getDBOperator().load(Kisikimlik.class.getSimpleName(), "o.kisiRef.rID=" + secilenUye.getKisiRef().getRID(), "o.rID").get(0);
						icerik = icerik.replace("$UYEBABAAD$", kimlik.getBabaad());
						icerik = icerik.replace("$UYEDOGUMYERI$", kimlik.getDogumyer());
						icerik = icerik.replace("$UYEDOGUMTARIHI$", DateUtil.dateToDMY(kimlik.getDogumtarih()));
					} else {
						icerik = icerik.replace("$UYEBABAAD$", "....");
						icerik = icerik.replace("$UYEDOGUMYERI$", "....");
						icerik = icerik.replace("$UYEDOGUMTARIHI$", "../../....");
					}
					// Ogrenim Bilgileri degistiriliyor
					if (getDBOperator().recordCount(Kisiogrenim.class.getSimpleName(), "o.kisiRef.rID=" + secilenUye.getKisiRef().getRID()) > 0) {
						Kisiogrenim ogrenim = (Kisiogrenim) getDBOperator().load(Kisiogrenim.class.getSimpleName(), "o.kisiRef.rID=" + secilenUye.getKisiRef().getRID(), "o.rID").get(0);
						if (ogrenim.getUniversiteRef() != null) {
							icerik = icerik.replace("$UYEMEZUNIYETOKUL$", ogrenim.getUniversiteRef().getAd());
						} else {
							icerik = icerik.replace("$UYEMEZUNIYETOKUL$", "....");
						}
						icerik = icerik.replace("$UYEMEZUNIYETTARIH$", ogrenim.getMezuniyettarihi() + "");
					} else {
						icerik = icerik.replace("$UYEMEZUNIYETOKUL$", "....");
						icerik = icerik.replace("$UYEMEZUNIYETTARIH$", "....");
					}
				} catch (DBException e) {
					// TODO Auto-generated catch block
					logYaz("Exception @" + getModelName() + "Controller :", e);
				}

				// Uyenin genel bilgileri
				icerik = icerik.replace("$TARIH$", DateUtil.dateToDMY(new Date()));
				icerik = icerik.replace("$UYEADSOYAD$", secilenUye.getUIString());
				if (secilenUye.getUyeliktarih() != null) {
					icerik = icerik.replace("$UYEKAYITTARIH$", DateUtil.dateToDMY(secilenUye.getUyeliktarih()));
				} else {
					icerik = icerik.replace("$UYEKAYITTARIH$", "../../....");
				}
				icerik = icerik.replace("$UYESICILNO$", secilenUye.getSicilno());
				icerik = icerik.replace("$UYETCKIMLIKNO$", secilenUye.getKisiRef().getKimlikno() + "");
				icerik = icerik.replace("$YIL$", DateUtil.getYear(new Date()) + "");

				// Sistem Parametresi Verileri
				icerik = icerik.replace("$HESAPADI$", sistemParam.getHesapAdi());
				icerik = icerik.replace("$BANKAADI$", sistemParam.getBankaAdi());
				icerik = icerik.replace("$BANKASUBEADI$", sistemParam.getSubeAdi());
				icerik = icerik.replace("$IBANNO$", sistemParam.getIbanNo());
				icerik = icerik.replace("$HESAPNO$", sistemParam.getHesapNo());

				// Adres ve sehir bilgileri degistiriliyor
				if (secilenUye.getVarsayilanAdres() != null) {
					Adres adres = secilenUye.getVarsayilanAdres();
                    if (adres.getAdrestipi() == AdresTipi._ILILCEMERKEZI) {
                        icerik = icerik.replace("$ADRES$", adres.getAcikAdres());
                        Sehir sehirRef = adres.getSehirRef();
                        icerik = icerik.replace("$SEHIR$", sehirRef != null ? sehirRef.getAd() : "");
                    }
                    else if (adres.getAdrestipi() == AdresTipi._YURTDISIADRESI) {
                        icerik = icerik.replace("$ADRES$", adres.getYabanciadres());
                        icerik = icerik.replace("$SEHIR$", adres.getYabancisehir());
                    }
				} else {
					icerik = icerik.replace("$ADRES$", "...............");
					icerik = icerik.replace("$SEHIR$", ".......");
				}

				if (ManagedBeanLocator.locateSessionController().getSistemParametre() != null) {
					if (!isEmpty(ManagedBeanLocator.locateSessionController().getSistemParametre().getOdaadi())) {
						odaAdi = ManagedBeanLocator.locateSessionController().getSistemParametre().getOdaadi();
					}
					icerik = icerik.replace("$ODAADI$", odaAdi);
				}
				if (getSessionUser().getPersonelRef() != null) {
					icerik = icerik.replace("$PERSONEL$", getSessionUser().getPersonelRef().getKisiRef().getUIStringShort());
				} else {
					icerik = icerik.replace("$PERSONEL$", "....");
				}
				getEvrak().setIcerik(icerik);

			} // getEvrak().getGittigiUyeRef() != null
		}
	}

	public void handleChangeForUye() {
		createEvrakIcerik();
	}

	public void handleChangeForKurum() {
		if (getEvrak().getGittigiKurumRef() != null) {
			createEvrakIcerik();
		}
	}

	public void handleChangeForKonu(AjaxBehaviorEvent event) {
		logYaz("triggered...");
		logYaz("triggered 2..." + getEvrak().getKonusu());
	}

	public void handleChangeForBirim() {
		if (getEvrak().getGittigiBirimRef() != null) {

		}
	}

	public SelectItem[] getHazirEvrakTuruItems() {
		if (getEvrak() == null || getEvrak().getEvrakHareketTuru() == EvrakHareketTuru._NULL) {
			return new SelectItem[] { new SelectItem(null, "") };
		} else {
			if (getEvrak().getEvrakHareketTuru() != EvrakHareketTuru._GIDEN) {
				return new SelectItem[] { new SelectItem(HazirEvrakTuru._GENELEVRAK, HazirEvrakTuru._GENELEVRAK.getLabel()) };
			} else {
				SelectItem[] hazirEvrakTuruItems = new SelectItem[HazirEvrakTuru.values().length - 1];
				for (int x = 0; x < HazirEvrakTuru.values().length - 1; x++) {
					hazirEvrakTuruItems[x] = new SelectItem(HazirEvrakTuru.values()[x + 1], HazirEvrakTuru.values()[x + 1].getLabel());
				}
				return hazirEvrakTuruItems;
			}
		}
	}

	public void handleEvrakHareketTuruChange(AjaxBehaviorEvent event) {
		try {
			HtmlSelectOneMenu selected = (HtmlSelectOneMenu) event.getComponent();
			if (selected.getValue() instanceof EvrakHareketTuru) {
				EvrakHareketTuru evrakHareketTuru = (EvrakHareketTuru) selected.getValue();
				getEvrak().setKayitno(createKayitNo(evrakHareketTuru));
				if (evrakHareketTuru == EvrakHareketTuru._GIDEN || evrakHareketTuru == EvrakHareketTuru._ICYAZISMA) {
					getEvrak().setEvraktarihi(new Date());
					if (getSessionUser().getPersonelRef() != null) {
						getEvrak().setHazirlayanPersonelRef(getSessionUser().getPersonelRef());
					}
				}
			} else if (selected.getValue() instanceof HazirEvrakTuru) {
				HazirEvrakTuru hzrEvrakTuru = (HazirEvrakTuru) selected.getValue();
				getEvrak().setHazirEvrakTuru(hzrEvrakTuru);
				// TODO - SILINECEK... TEST
				/*
				 * if(getEvrak().getEvrakHareketTuru() == EvrakHareketTuru._GELEN){ String os = System.getProperty("os.name"); if
				 * (os.matches("(?i).*windows.*")) { getEvrak().setDosyaKoduRef((Dosyakodu) getDBOperator().find(Dosyakodu.class.getSimpleName(), "o.rID=1"));
				 * getEvrak().setGonderenKurumRef((Kurum) getDBOperator().find(Kurum.class.getSimpleName(), "o.rID=5")); getEvrak().setKonusu("Test");
				 * getEvrak().setEvrakno("553453"); getEvrak().setEvraktarihi(new Date()); getEvrak().setSablonRef((Dolasimsablon)
				 * getDBOperator().find(Dolasimsablon.class.getSimpleName(), "o.rID=5")); getEvrak().setIcerik("Test içeriği"); } }
				 */
			}
		} catch (Exception e) {
			logYaz("Exception @" + getModelName() + "Controller :", e);
		}
	}

	private String createKayitNo(EvrakHareketTuru hareketTuru) {
		if (hareketTuru == EvrakHareketTuru._GELEN) {
			return getEvrak().getYil() + " - GL - .......";
		} else if (hareketTuru == EvrakHareketTuru._GIDEN) {
			return getEvrak().getYil() + " - GD - .......";
		} else if (hareketTuru == EvrakHareketTuru._ICYAZISMA) {
			return getEvrak().getYil() + " - IY - .......";
		} else {
			return getEvrak().getYil() + "";
		}
	}

	@Override
	public void resetFilter() {
		super.tableRowCancelSelection();
		EvrakHareketTuru previous = getEvrakFilter().getEvrakHareketTuru();
		getEvrakFilter().clearQueryCriterias();
		getEvrakFilter().init();
		getEvrakFilter().setYil(DateUtil.getYear(new Date()));
		getEvrakFilter().setEvrakHareketTuru(previous);
		getFilter().createQueryCriterias();
		getSessionUser().setLegalAccess(1);
		setTable(null);
		queryAction();
	}

	@Override
	public void newFilter() {
		super.newFilter();
		putObjectToSessionFilter(getModelName() + "Filter", getEvrakFilter());
	}

	@Override
	public void forwardEdit() {
		if (getEvrak().getEvrakHareketTuru() == EvrakHareketTuru._GELEN) {
			if (getEvrak().getGonderenkisiRef() == null && getEvrak().getGonderenKurumRef() == null) {
				createCustomMessage(KeyUtil.getMessageValue("evrak.gelenGonderenHata"), FacesMessage.SEVERITY_ERROR, "gonderenKurumRefSuggestBox:gonderenKurumRef");
				createCustomMessage(KeyUtil.getMessageValue("evrak.gelenGonderenHata"), FacesMessage.SEVERITY_ERROR, "gonderenkisiRefSuggestBox:gonderenkisiRef");
				return;
			}
		}
		setEditStep(2);
	}

	@Override
	public void save() {
		try {
			if (getEvrak().getRID() != null) {
				if (getEvrak().getEvrakHareketTuru() == EvrakHareketTuru._GIDEN) {
					// Guncelleme yapiliyorsa, suggestbox icin duzeltmeler yapiliyor.
					if (getEvrak().getGittigiKurumRef() == null) {
						if (getEvrak().getGittigiKurumRefPrev() != null) {
							getEvrak().setGittigiKurumRef(getEvrak().getGittigiKurumRefPrev());
						}
					}
					if (getEvrak().getGittigiUyeRef() == null) {
						if (getEvrak().getGittigiUyeRefPrev() != null) {
							getEvrak().setGittigiUyeRef(getEvrak().getGittigiUyeRefPrev());
						}
					}
				}

				// Guncelleme yapiliyorsa, suggestbox icin duzeltmeler yapiliyor.
				if (getEvrak().getDosyaKoduRef() == null) {
					if (getEvrak().getDosyaKoduRefPrev() != null) {
						getEvrak().setDosyaKoduRef(getEvrak().getDosyaKoduRefPrev());
					}
				}
			}
			if (getEvrak().getEvrakHareketTuru() == EvrakHareketTuru._GIDEN) {
				if (getEvrak().getGittigiBirimRef() == null && getEvrak().getGittigiKurumRef() == null && getEvrak().getGittigiUyeRef() == null) {
					createCustomMessage(KeyUtil.getMessageValue("evrak.gidenGittigiHata"), FacesMessage.SEVERITY_ERROR, "gittigiKurumRefSuggestBox:gittigiKurumRef");
					createCustomMessage(KeyUtil.getMessageValue("evrak.gidenGittigiHata"), FacesMessage.SEVERITY_ERROR, "gittigiBirimRefDropDown:gittigiBirimRef");
					createCustomMessage(KeyUtil.getMessageValue("evrak.gidenGittigiHata"), FacesMessage.SEVERITY_ERROR, "gittigiUyeRefSuggestBox:gittigiUyeRef");
					return;
				}
			}
			if (getEvrak().getRID() == null) {
				getEvrak().setSistemeTanimlayan(getSessionUser().getPersonelRef());
				getEvrak().setKayitno(ManagedBeanLocator.locateSessionController().evrakKayitNoOlustur(getEvrak()));
			}
			getEvrakFilter().init();
			getEvrakFilter().setYil(getEvrak().getYil());
			getEvrakFilter().setEvrakHareketTuru(getEvrak().getEvrakHareketTuru());
			getEvrakFilter().setDurum(getEvrak().getDurum());
			if (getEvrak().getHazirEvrakTuru() != null && getEvrak().getHazirEvrakTuru() != HazirEvrakTuru._GENELEVRAK) {
				getEvrakFilter().setHazirEvrakTuru(getEvrak().getHazirEvrakTuru());
			}
			getEvrakFilter().createQueryCriterias();
			super.save();
		} catch (Exception e) {
			getEvrak().setRID(null);
			createGenericMessage(KeyUtil.getMessageValue("islem.hata"), FacesMessage.SEVERITY_INFO);
		}
	}

	public void fileUploadListenerForEvrakek(FileUploadEvent event) throws Exception {
		UploadedFile item = event.getFile();
		File creationFile = createFile(item);
		getEvrakek().setPath(creationFile.getAbsolutePath());
	}

	public String getHeightOfEditDialog() {
		if (this.detailEditType == null) {
			return "400";
		} else if (this.detailEditType.equalsIgnoreCase("evrakek")) {
			return "240";
		} else if (this.detailEditType.equalsIgnoreCase("evrakoncekiadimaciklama")) {
			return "240";
		} else if (this.detailEditType.equalsIgnoreCase("evraksonrakiadimaciklama")) {
			return "240";
		} else if (this.detailEditType.equalsIgnoreCase("evrakonayaciklama")) {
			return "240";
		} else {
			return "400";
		}
	}

	public void createPdfPreview() throws Exception {
		if (!setSelected()) {
			return;
		}
		Kullanici sysUser = ManagedBeanLocator.locateSessionUser().getKullanici();
		byte[] arr = CustomPdfExport.gidenEvrak(getEvrak().getKayitno() + ".pdf", getEvrak().getRID(), sysUser);
		download(arr, getEvrak().getKayitno());

	} // createPdfPreview method

	public void download(byte[] pdfData, String fileName) throws IOException {
		// Prepare.
		FacesContext facesContext = FacesContext.getCurrentInstance();
		ExternalContext externalContext = facesContext.getExternalContext();
		HttpServletResponse response = (HttpServletResponse) externalContext.getResponse();

		// Initialize response.
		response.reset(); // Some JSF component library or some Filter might have set some headers in the buffer beforehand. We want to get rid of them, else it
							// may collide.
		response.setContentType("application/pdf"); // Check http://www.iana.org/assignments/media-types for all types. Use if necessary
													// ServletContext#getMimeType() for auto-detection based on filename.
		response.setHeader("Content-disposition", "attachment; filename=\"" + fileName + ".pdf\""); // The Save As popup magic is done here. You can give it any
																									// filename you want, this only won't work in MSIE, it will
																									// use current request URL as filename instead.

		// Write file to response.
		OutputStream output = response.getOutputStream();
		output.write(pdfData);
		output.close();

		// Inform JSF to not take the response in hands.
		facesContext.responseComplete(); // Important! Else JSF will attempt to render the response which obviously will fail since it's already written with a
											// file and closed.
	}

	public String getSelectedTab() {
		return selectedTab;
	}

	public void setSelectedTab(String selectedTab) {
		this.selectedTab = selectedTab;
	}

	public Evrak getEvrakDetay() {
		return evrakDetay;
	}

	public void setEvrakDetay(Evrak evrakDetay) {
		this.evrakDetay = evrakDetay;
	}

	public EvrakekController getEvrakekController() {
		return evrakekController;
	}

	public void setEvrakekController(EvrakekController evrakekController) {
		this.evrakekController = evrakekController;
	}

	public Evrakek getEvrakek() {
		return evrakek;
	}

	public void setEvrakek(Evrakek evrakek) {
		this.evrakek = evrakek;
	}

	public String getDetailEditType() {
		return detailEditType;
	}

	public void setDetailEditType(String detailEditType) {
		this.detailEditType = detailEditType;
	}

	public String getOldValueForEvrakek() {
		return oldValueForEvrakek;
	}

	public void setOldValueForEvrakek(String oldValueForEvrakek) {
		this.oldValueForEvrakek = oldValueForEvrakek;
	}

	public String kime = "";

	public String getKime() throws DBException {
		if (getEvrak() == null) {
			return "";
		}
		if (getDBOperator().recordCount(Dolasimsablondetay.class.getSimpleName(), "o.dolasimsablonRef.rID=" + getEvrak().getSablonRef().getRID()) > 0) {
			Dolasimsablondetay ilkAdim = (Dolasimsablondetay) getDBOperator().load(Dolasimsablondetay.class.getSimpleName(), "o.dolasimsablonRef.rID=" + getEvrak().getSablonRef().getRID(), "o.sirano ASC").get(0);
			if (ilkAdim != null) {
				if (ilkAdim.getBirimRef() != null) {
					kime = ilkAdim.getBirimRef().getAd();
				} else {
					kime = ilkAdim.getPersonelRef().getUIString();
				}
			}
			return kime;
		} else {
			return "";
		}
	}

	public void onayla() throws DBException {
		getEvrak().setDurum(EvrakDurum._ONAYLANMIS);
		if (getEvrak().getSablonRef() != null && getDBOperator().recordCount(Dolasimsablondetay.class.getSimpleName(), "o.dolasimsablonRef.rID=" + getEvrak().getSablonRef().getRID()) > 0) {
			Evrakdolasim dolasim = new Evrakdolasim();
			dolasim.setEvrakRef(getEvrak());
			dolasim.setGelistarih(new Date());
			Dolasimsablondetay ilkAdim = (Dolasimsablondetay) getDBOperator().load(Dolasimsablondetay.class.getSimpleName(), "o.dolasimsablonRef.rID=" + getEvrak().getSablonRef().getRID(), "o.sirano ASC").get(0);
			dolasim.setDolasimSablonDetayRef(ilkAdim);
			getEvrak().setSonDolasimSablonDetayRef(ilkAdim);

			if (ilkAdim.getBirimRef() != null) {
				dolasim.setBirimRef(ilkAdim.getBirimRef());
			} else {
				dolasim.setPersonelRef(ilkAdim.getPersonelRef());
			}
			Evrakdolasimbildirim bildirim = ManagedBeanLocator.locateSessionController().bildirimOlustur(dolasim.getAliciText(), getEvrak(), getAciklama());
			try {
				getDBOperator().recordInOneTransaction(new BaseEntity[] { dolasim, bildirim }, new BaseEntity[] { getEvrak() }, null);
			} catch (DBException e) {
				createGenericMessage(KeyUtil.getMessageValue("islem.hata"), FacesMessage.SEVERITY_ERROR);
				logYaz("Exception @" + getModelName() + "Controller :", e);
				return;
			}
		}
		getEvrakFilter().setEvrakHareketTuru(getEvrak().getEvrakHareketTuru());
		detailEditType = null;

		queryAction();
		if (!isEmpty(getList())) {
			setEvrak((Evrak) getList().get(0));
			setEntity(getList().get(0));
		}
		RequestContext context = RequestContext.getCurrentInstance();
		context.update("evrakForm");
		getSessionUser().setMessage(KeyUtil.getMessageValue("evrak.onaylandi"));
	}

	public boolean isEvrakDolasimSonrakiAdimVar() {
		if (getEvrak() == null) {
			return false;
		}
		if (getEvrak().getSablonRef() == null) {
			return false;
		}
		if (getEvrak().getDurum() != EvrakDurum._ONAYLANMIS) {
			return false;
		}
		if (getEvrak().getSonDolasimSablonDetayRef() == null) {
			return false;
		}
		try {
			// Sablonda gidebilecegi bir adim kalmis mi kontrol ediliyor.
			if (getDBOperator().recordCount(Dolasimsablondetay.class.getSimpleName(), "o.dolasimsablonRef.rID=" + getEvrak().getSablonRef().getRID() + " AND o.sirano >" + getEvrak().getSonDolasimSablonDetayRef().getSirano()) > 0) {
				// Sablonda adim var ancak bir sonraki adima gonderebilmem icin evrakin dolasiminin benim uzerimde olmasi gerekir !!!
				if (getDBOperator().recordCount(Evrakdolasim.class.getSimpleName(), "o.evrakRef.rID= " + getEvrak().getRID() + " AND o.gidistarih IS NULL") > 0) {
					Evrakdolasim sonDolasim = (Evrakdolasim) getDBOperator().load(Evrakdolasim.class.getSimpleName(), "o.evrakRef.rID=" + getEvrak().getRID() + " AND o.gidistarih IS NULL", "o.rID").get(0);
					if (sonDolasim.getPersonelRef() != null) {
						if (sonDolasim.getPersonelRef().getRID() == getSessionUser().getPersonelRef().getRID()) {
							return true;
						}
					}
					if (sonDolasim.getBirimRef() != null) {
						if (sonDolasim.getBirimRef().getRID() == getSessionUser().getPersonelRef().getBirimRef().getRID()) {
							return true;
						}
					} else {
						return false;
					}
				}
				return false;
			}
		} catch (DBException e) {
			logYaz("ERROR @isEvrakDolasimSonrakiAdimVar, EvrakController :" + e.getMessage());
			return false;
		}
		return false;
	}

	public boolean isEvrakDolasimTamamlanmis() {
		if (getEvrak() == null) {
			return false;
		}
		if (getEvrak().getSablonRef() == null) {
			return false;
		}
		if (getEvrak().getDurum() != EvrakDurum._ONAYLANMIS) {
			return false;
		}
		try {
			if (getDBOperator().recordCount(Dolasimsablondetay.class.getSimpleName(), "o.dolasimsablonRef.rID=" + getEvrak().getSablonRef().getRID() + " AND o.sirano >" + getEvrak().getSonDolasimSablonDetayRef().getSirano()) > 0) {
				// logYaz("Count :" + getDBOperator().recordCount(Dolasimsablondetay.class.getSimpleName(), "o.dolasimsablonRef.rID=" +
				// getEvrak().getSablonRef().getRID() + " AND o.sirano >" + getEvrak().getSonDolasimSablonDetayRef().getSirano()));
				return false;
			} else {
				return true;
			}
		} catch (DBException e) {
			logYaz("ERROR @isEvrakDolasimTamamlanmis, EvrakController :" + e.getMessage());
			return false;
		}
	}

	public void closeAllPanels() {
		setOncekiAdimPanelRendered(false);
		setSonrakiAdimPanelRendered(false);
		setOnayPanelRendered(false);
		setEditPanelRendered(false);
		this.aciklama = "";
	}

	public boolean isOncekiAdimPanelRendered() {
		return oncekiAdimPanelRendered;
	}

	public void setOncekiAdimPanelRendered(boolean oncekiAdimPanelRendered) {
		this.oncekiAdimPanelRendered = oncekiAdimPanelRendered;
	}

	public boolean isSonrakiAdimPanelRendered() {
		return sonrakiAdimPanelRendered;
	}

	public void setSonrakiAdimPanelRendered(boolean sonrakiAdimPanelRendered) {
		this.sonrakiAdimPanelRendered = sonrakiAdimPanelRendered;
	}

	public boolean isOnayPanelRendered() {
		return onayPanelRendered;
	}

	public void setOnayPanelRendered(boolean onayPanelRendered) {
		this.onayPanelRendered = onayPanelRendered;
	}

	public String getAciklama() {
		return aciklama;
	}

	public void setAciklama(String aciklama) {
		this.aciklama = aciklama;
	}

	public void oncekiAdimPanelAc() {
		closeAllPanels();
		setOncekiAdimPanelRendered(true);
	}

	public void oncekiAdimPanelKapat() {
		setOncekiAdimPanelRendered(false);
	}

	public void sonrakiAdimPanelAc() {
		closeAllPanels();
		setSonrakiAdimPanelRendered(true);
	}

	public void sonrakiAdimPanelKapat() {
		setSonrakiAdimPanelRendered(false);
	}

	public void onayPanelAc() {
		closeAllPanels();
		setOnayPanelRendered(true);
	}

	public void onayPanelKapat() {
		setOnayPanelRendered(false);
	}

	public void oncekiAdim() {
		BaseEntity[] updateEdilecekler = new BaseEntity[0];
		BaseEntity[] silinecekler = new BaseEntity[0];
		BaseEntity[] eklenecekler = new BaseEntity[0];
		try {
			// Sablonda geri gidebilecegi bir adim var mi kontrol ediliyor.
			int count = getDBOperator().recordCount(Evrakdolasim.class.getSimpleName(), "o.evrakRef.rID=" + getEvrak().getRID());
			if (count > 1) {
				Evrakdolasim sonDolasim = (Evrakdolasim) getDBOperator().load(Evrakdolasim.class.getSimpleName(), "o.evrakRef.rID=" + getEvrak().getRID() + " AND o.gidistarih IS NULL", "o.rID DESC").get(0);
				silinecekler = new BaseEntity[] { sonDolasim };
				Evrakdolasim oncekiDolasim = (Evrakdolasim) getDBOperator().load(Evrakdolasim.class.getSimpleName(), "o.evrakRef.rID=" + getEvrak().getRID() + " AND o.rID < " + sonDolasim.getRID(), "o.rID DESC").get(0);
				oncekiDolasim.setGidistarih(null);
				Evrakdolasimbildirim bildirim = ManagedBeanLocator.locateSessionController().bildirimOlustur(oncekiDolasim.getAliciText(), getEvrak(), getAciklama());
				eklenecekler = new BaseEntity[] { bildirim };
				getEvrak().setSonDolasimSablonDetayRef(oncekiDolasim.getDolasimSablonDetayRef());
				updateEdilecekler = new BaseEntity[] { oncekiDolasim, getEvrak() };
				getDBOperator().recordInOneTransaction(eklenecekler, updateEdilecekler, silinecekler);
			} else if (count > 0) {
				Evrakdolasim silinecekKayit = (Evrakdolasim) getDBOperator().load(Evrakdolasim.class.getSimpleName(), "o.evrakRef.rID=" + getEvrak().getRID(), "o.rID DESC").get(0);
				silinecekler = new BaseEntity[] { silinecekKayit };
				getEvrak().setSonDolasimSablonDetayRef(null);
				getEvrak().setDurum(EvrakDurum._TASLAK);
				updateEdilecekler = new BaseEntity[] { getEvrak() };
				Evrakdolasimbildirim bildirim = ManagedBeanLocator.locateSessionController().bildirimOlustur(getEvrak().getSistemeTanimlayan().getKisiRef().getUIStringShort(), getEvrak(), getAciklama());
				eklenecekler = new BaseEntity[] { bildirim };
				getDBOperator().recordInOneTransaction(eklenecekler, updateEdilecekler, silinecekler);
			} else {
				silinecekler = new BaseEntity[0];
				getEvrak().setSonDolasimSablonDetayRef(null);
				getEvrak().setDurum(EvrakDurum._TASLAK);
				updateEdilecekler = new BaseEntity[] { getEvrak() };
				Evrakdolasimbildirim bildirim = ManagedBeanLocator.locateSessionController().bildirimOlustur(getEvrak().getSistemeTanimlayan().getKisiRef().getUIStringShort(), getEvrak(), getAciklama());
				eklenecekler = new BaseEntity[] { bildirim };
				getDBOperator().recordInOneTransaction(eklenecekler, updateEdilecekler, silinecekler);
			}
		} catch (DBException e) {
			logYaz("ERROR @oncekiAdim, EvrakController :" + e.getMessage());
			createGenericMessage(KeyUtil.getMessageValue("islem.hata"), FacesMessage.SEVERITY_ERROR);
			getSessionUser().setMessage(KeyUtil.getMessageValue("islem.hata"));
			return;
		}
		detailEditType = null;
		queryAction();
		if (!isEmpty(getList())) {
			setEvrak((Evrak) getList().get(0));
			setEntity(getList().get(0));
		}
		RequestContext context = RequestContext.getCurrentInstance();
		context.update("evrakForm");
		getSessionUser().setMessage(KeyUtil.getMessageValue("evrak.oncekiAdimaGonderildi"));
	}

	public void sonrakiAdim() {
		try {
			if (getDBOperator().recordCount(Dolasimsablondetay.class.getSimpleName(), "o.dolasimsablonRef.rID=" + getEvrak().getSablonRef().getRID() + " AND o.sirano >" + getEvrak().getSonDolasimSablonDetayRef().getSirano()) > 0) {
				BaseEntity[] updateEdilecekler = new BaseEntity[1];
				Dolasimsablondetay dolasimsablondetay = (Dolasimsablondetay) getDBOperator().load(Dolasimsablondetay.class.getSimpleName(),
						"o.dolasimsablonRef.rID=" + getEvrak().getSablonRef().getRID() + " AND o.sirano >" + getEvrak().getSonDolasimSablonDetayRef().getSirano(), "o.sirano ASC").get(0);
				getEvrak().setSonDolasimSablonDetayRef(dolasimsablondetay);
				if (getDBOperator().recordCount(Evrakdolasim.class.getSimpleName(), "o.evrakRef.rID=" + getEvrak().getRID() + " AND o.gidistarih IS NULL") > 0) {
					Evrakdolasim oncekiAdim = (Evrakdolasim) getDBOperator().load(Evrakdolasim.class.getSimpleName(), "o.evrakRef.rID=" + getEvrak().getRID() + " AND o.gidistarih IS NULL", "o.rID").get(0);
					oncekiAdim.setGidistarih(new Date());
					updateEdilecekler = new BaseEntity[2];
					updateEdilecekler[0] = getEvrak();
					updateEdilecekler[1] = oncekiAdim;
				} else {
					updateEdilecekler[0] = getEvrak();
				}

				Evrakdolasim yeniAdim = new Evrakdolasim();
				yeniAdim.setDolasimSablonDetayRef(dolasimsablondetay);
				yeniAdim.setEvrakRef(getEvrak());
				if (dolasimsablondetay.getBirimRef() != null) {
					yeniAdim.setBirimRef(dolasimsablondetay.getBirimRef());
				} else {
					yeniAdim.setPersonelRef(dolasimsablondetay.getPersonelRef());
				}
				Evrakdolasimbildirim bildirim = ManagedBeanLocator.locateSessionController().bildirimOlustur(yeniAdim.getAliciText(), getEvrak(), getAciklama());
				yeniAdim.setGelistarih(new Date());
				getDBOperator().recordInOneTransaction(new BaseEntity[] { yeniAdim, bildirim }, updateEdilecekler, null);
				queryAction();
				// createGenericMessage(KeyUtil.getMessageValue("kayit.guncellendi"), FacesMessage.SEVERITY_INFO);
			}
		} catch (DBException e) {
			logYaz("ERROR @sonrakiAdim, EvrakController :" + e.getMessage());
			return;
		}
		detailEditType = null;
		queryAction();
		if (!isEmpty(getList())) {
			setEvrak((Evrak) getList().get(0));
			setEntity(getList().get(0));
		}
		RequestContext context = RequestContext.getCurrentInstance();
		context.update("evrakForm");
		getSessionUser().setMessage(KeyUtil.getMessageValue("evrak.sonrakiAdimaGonderildi"));
	}

	public void evrakKapat() throws DBException {
		getEvrak().setDurum(EvrakDurum._TAMAMLANMIS);
		BaseEntity[] updateEdilecekler = new BaseEntity[2];
		if (getEvrak().getSablonRef() != null) {
			Evrakdolasim dolasim = (Evrakdolasim) getDBOperator().load(Evrakdolasim.class.getSimpleName(), "o.evrakRef.rID=" + getEvrak().getRID() + " AND o.gidistarih IS NULL", "o.rID").get(0);
			dolasim.setGidistarih(new Date());
			updateEdilecekler[0] = dolasim;
			updateEdilecekler[1] = getEvrak();
		} else {
			updateEdilecekler = new BaseEntity[1];
			updateEdilecekler[0] = getEvrak();
		}
		getDBOperator().recordInOneTransaction(null, updateEdilecekler, null);
		queryAction();
		createGenericMessage(KeyUtil.getMessageValue("evrak.tamamlandi"), FacesMessage.SEVERITY_INFO);
	}

	@Override
	public void queryAction() {
		super.queryAction();
		closeAllPanels();
	}

	public String evrakdolasimbildirim() {
		if (!setSelected()) {
			return "";
		}
		getSessionUser().setLegalAccess(1);
		putObjectToSessionFilter(ApplicationDescriptor._EVRAK_MODEL, getEvrak());
		return ManagedBeanLocator.locateMenuController().gotoPage("menu_Evrakdolasimbildirim");
	}

	public void deletePreviousKurum() {
		getEvrak().setGittigiKurumRefPrev(null);
		getEvrak().setGittigiKurumRef(null);
	}

	public void deletePreviousUye() {
		getEvrak().setGittigiUyeRefPrev(null);
		getEvrak().setGittigiUyeRef(null);
	}

	public SelectItem[] getDolasimsablonItemList() {
		String whereCon = "o.evrakHareketTuru =" + getEvrak().getEvrakHareketTuru().getCode();
		if (!ManagedBeanLocator.locateSessionUser().isSuperUser()) {
			whereCon += " AND o.birimRef.rID =" + ManagedBeanLocator.locateSessionUser().getPersonelRef().getBirimRef().getRID();
		}
		// logYaz("whereCon..." + whereCon);
		@SuppressWarnings("unchecked")
		List<Dolasimsablon> entityList = getDBOperator().load(Dolasimsablon.class.getSimpleName(), whereCon, "o.evrakHareketTuru");
		if (!isEmpty(entityList)) {
			SelectItem[] selectItemList = new SelectItem[entityList.size() + 1];
			selectItemList[0] = new SelectItem(null, "");
			int i = 1;
			for (Dolasimsablon entity : entityList) {
				selectItemList[i++] = new SelectItem(entity, entity.getUIString());
			}
			return selectItemList;
		} else {
			return new SelectItem[0];
		}
	}

	@Override
	public void setSelectedRID(String selectedRID){
		RequestContext.getCurrentInstance().execute("previewDialog.show();");
		super.setSelectedRID(selectedRID);
	}

} // class
