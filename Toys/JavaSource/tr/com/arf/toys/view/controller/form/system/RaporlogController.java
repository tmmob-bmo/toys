package tr.com.arf.toys.view.controller.form.system; 
  
 
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.toys.db.filter.system.RaporlogFilter;
import tr.com.arf.toys.db.model.system.Raporlog;
import tr.com.arf.toys.view.controller.form._base.ToysBaseFilteredController;
  
  
@ManagedBean 
@ViewScoped 
public class RaporlogController extends ToysBaseFilteredController {  
  
  private static final long serialVersionUID = 6531152165884980748L; 
  
	private Raporlog raporlog;  
	private RaporlogFilter raporlogFilter = new RaporlogFilter();  
  
	public RaporlogController() { 
		super(Raporlog.class);  
		setDefaultValues(false);  
		setOrderField("tarih DESC");  
		setLoggable(false);  
		setAutoCompleteSearchColumns(new String[]{"kullaniciRef"}); 
		setTable(null); 
		queryAction(); 
	} 
	
	
	public Raporlog getRaporlog() { 
		raporlog = (Raporlog) getEntity(); 
		return raporlog; 
	} 
	
	public void setRaporlog(Raporlog raporlog) { 
		this.raporlog = raporlog; 
	} 
	
	public RaporlogFilter getRaporlogFilter() { 
		return raporlogFilter; 
	} 
	 
	public void setRaporlogFilter(RaporlogFilter raporlogFilter) {  
		this.raporlogFilter = raporlogFilter;  
	}  
	 
	@Override  
	public BaseFilter getFilter() {  
		return getRaporlogFilter();  
	}  
	
	
 
	
} // class 