package tr.com.arf.toys.view.controller.form.system; 
  
 
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.framework.db.model._base.BaseEntity;
import tr.com.arf.toys.db.filter.system.BolumFilter;
import tr.com.arf.toys.db.model.system.Bolum;
import tr.com.arf.toys.db.model.system.Fakulte;
import tr.com.arf.toys.service.application.ApplicationDescriptor;
import tr.com.arf.toys.view.controller.form._base.ToysBaseFilteredController;
  
  
@ManagedBean 
@ViewScoped 
public class BolumController extends ToysBaseFilteredController {  
  
  private static final long serialVersionUID = 6531152165884980748L; 
  
	private Bolum bolum;  
	private BolumFilter bolumFilter = new BolumFilter();  
  
	public BolumController() { 
		super(Bolum.class);  
		setDefaultValues(false);  
		setOrderField("ad");  
		setAutoCompleteSearchColumns(new String[]{"ad"}); 
		setTable(null); 
		
		queryAction(); 
	} 
	
	public BaseEntity getMasterEntity(){
		if(getObjectFromSessionFilter(ApplicationDescriptor._FAKULTE_MODEL) != null){
			return (Fakulte) getObjectFromSessionFilter(ApplicationDescriptor._FAKULTE_MODEL);
		}  
		else {
			return null;
		}			
	} 
	public Bolum getBolum() {
		bolum=(Bolum) getEntity();
		return bolum;
	}

	public void setBolum(Bolum bolum) {
		this.bolum = bolum;
	}

	public BolumFilter getBolumFilter() {
		return bolumFilter;
	}

	public void setBolumFilter(BolumFilter bolumFilter) {
		this.bolumFilter = bolumFilter;
	}

	@Override  
	public BaseFilter getFilter() {  
		return getBolumFilter();  
	}  
	
	@Override
		public void insert() {
			super.insert();
		}
	
	
} // class 
