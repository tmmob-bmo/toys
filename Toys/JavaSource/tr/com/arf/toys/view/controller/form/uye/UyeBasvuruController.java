package tr.com.arf.toys.view.controller.form.uye;

import java.io.IOException;
import java.rmi.RemoteException;
import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

import org.primefaces.component.datatable.DataTable;
import org.primefaces.context.RequestContext;

import tr.com.arf.framework.db._management.DBOperator;
import tr.com.arf.framework.db.enumerated._common.EvetHayir;
import tr.com.arf.framework.db.model._base.BaseEntity;
import tr.com.arf.framework.utility.exception.data.DBException;
import tr.com.arf.framework.utility.security.ScryptPasswordHashing;
import tr.com.arf.framework.utility.tool.DateUtil;
import tr.com.arf.framework.utility.tool.KeyUtil;
import tr.com.arf.framework.utility.tool.RandomObjectGenerator;
import tr.com.arf.toys.db.enumerated.iletisim.AdresTipi;
import tr.com.arf.toys.db.enumerated.iletisim.AdresTuru;
import tr.com.arf.toys.db.enumerated.iletisim.NetAdresTuru;
import tr.com.arf.toys.db.enumerated.iletisim.TelefonTuru;
import tr.com.arf.toys.db.enumerated.kisi.Cinsiyet;
import tr.com.arf.toys.db.enumerated.kisi.KimlikTip;
import tr.com.arf.toys.db.enumerated.kisi.KisiDurum;
import tr.com.arf.toys.db.enumerated.kisi.MedeniHal;
import tr.com.arf.toys.db.enumerated.uye.OgrenimTip;
import tr.com.arf.toys.db.enumerated.uye.UyeDurum;
import tr.com.arf.toys.db.enumerated.uye.UyeMuafiyetTip;
import tr.com.arf.toys.db.enumerated.uye.UyeTip;
import tr.com.arf.toys.db.enumerated.uye.UyekurumDurum;
import tr.com.arf.toys.db.model.iletisim.Adres;
import tr.com.arf.toys.db.model.iletisim.Internetadres;
import tr.com.arf.toys.db.model.iletisim.Telefon;
import tr.com.arf.toys.db.model.kisi.Kisi;
import tr.com.arf.toys.db.model.kisi.Kisikimlik;
import tr.com.arf.toys.db.model.kisi.Kisiogrenim;
import tr.com.arf.toys.db.model.kisi.Kurum;
import tr.com.arf.toys.db.model.system.Birim;
import tr.com.arf.toys.db.model.system.Bolum;
import tr.com.arf.toys.db.model.system.Fakulte;
import tr.com.arf.toys.db.model.system.Ilce;
import tr.com.arf.toys.db.model.system.Kullanici;
import tr.com.arf.toys.db.model.system.KurumKisi;
import tr.com.arf.toys.db.model.system.Sehir;
import tr.com.arf.toys.db.model.system.SistemParametre;
import tr.com.arf.toys.db.model.system.Ulke;
import tr.com.arf.toys.db.model.uye.Uye;
import tr.com.arf.toys.db.model.uye.Uyemuafiyet;
import tr.com.arf.toys.db.nonEntityModel.PojoAdres;
import tr.com.arf.toys.db.nonEntityModel.PojoKisi;
import tr.com.arf.toys.service.application.ApplicationDescriptor;
import tr.com.arf.toys.service.application.KPSService;
import tr.com.arf.toys.service.application.ManagedBeanLocator;
import tr.com.arf.toys.utility.tool.SendEmail;
import tr.com.arf.toys.utility.tool.ToysXMLReader;
import tr.com.arf.toys.view.controller.form._base.ToysBaseController;
import tr.com.arf.toys.view.controller.form.iletisim.AdresController;
import tr.com.arf.toys.view.controller.form.iletisim.InternetadresController;
import tr.com.arf.toys.view.controller.form.iletisim.TelefonController;
import tr.com.arf.toys.view.controller.form.kisi.KisiController;
import tr.com.arf.toys.view.controller.form.kisi.KisikimlikController;

@ManagedBean(name = "uyeBasvuruController")
@ViewScoped
public class UyeBasvuruController extends ToysBaseController {
	private DataTable table2 = new DataTable();
	private static final long serialVersionUID = 6531152165884980748L;

	private Uye uye;
	private Kisiogrenim uyeogrenim;
	private int wizardStep = 1;
	private Kisi kisi;
	private Kisikimlik kisikimlik;

	private Adres kpsAdres;
	private Adres kpsDigerAdres;

	private Adres adres2;
	private Adres adres3;

	private Telefon telefon1;
	private Telefon telefon2;
	private Telefon telefon3;
	private Telefon telefon4;

	private Internetadres internetAdres1;

	private Kurum kurum;
	private Birim birim;
	private String detailEditType;

	private boolean kpsSorguSonucDondu = false;
	private boolean kpsKaydiBulundu = false;
	private boolean kisiVeritabanindaBulundu = false;
	private Boolean kpsAdresDondu = false;

	private KurumKisi kurumKisi;
	private SelectItem[] selectAdresListForUyeKurum = new SelectItem[0];
	private PojoKisi kisikimlikPojo = new PojoKisi();

	private Boolean iletisimEmail = true;
	private Boolean iletisimSms = true;
	private Boolean iletisimPosta = true;

	private Boolean isyeriadresvarsayilan = false;
	private Boolean beyanadresvarsayilan = false;

	public UyeBasvuruController() {
		super(Uye.class);
		setDefaultValues(false);
		setOrderField("sicilno");
		setAutoCompleteSearchColumns(new String[] { "sicilno" });
		prepareForWizard();
		setTable(null);
		setStaticWhereCondition(getSessionUser().createStaticWhereCondition("o."));
		queryAction();
	}

	private void prepareForWizard() {
		this.wizardStep = 1;
		super.insert();
		try {
			if (getSessionUser().getPersonelRef() != null) {
				if (getSessionUser().getPersonelRef().getBirimRef() != null) {
					getUye().setBirimRef(getSessionUser().getPersonelRef().getBirimRef());
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logYaz("Exception @" + getModelName() + "Controller :", e);
		}
		this.kisi = new Kisi();
		this.kisi.setKimliknotip(KimlikTip._TCKIMLIKNO);
		this.kisi.setAktif(EvetHayir._EVET);
		this.kisi.setAd("");
		this.kisi.setSoyad("");
		this.kisikimlik = new Kisikimlik();

		this.uyeogrenim = new Kisiogrenim();
		uyeogrenim.setOgrenimtip(OgrenimTip._LISANS);
		this.uyeogrenim.setDenklikdurum(EvetHayir._HAYIR);
		Calendar now = Calendar.getInstance();
		int year = now.get(Calendar.YEAR);
		this.uyeogrenim.setMezuniyettarihi(year);
		this.kurum = new Kurum();
		this.birim = new Birim();
		removeObjectFromSessionFilter(ApplicationDescriptor._UYE_MODEL);

		this.kurumKisi = new KurumKisi();
		selectAdresListForUyeKurum = new SelectItem[0];
		this.kisikimlikPojo = new PojoKisi();

		this.kpsAdres = new Adres();
		this.kpsDigerAdres = new Adres();
		this.adres2 = new Adres();
		this.adres3 = new Adres();
		this.telefon1 = new Telefon();
		telefon1.setTelefonturu(TelefonTuru._EVTELEFONU);
		this.telefon2 = new Telefon();
		telefon2.setTelefonturu(TelefonTuru._ISTELEFONU);
		this.telefon3 = new Telefon();
		telefon3.setTelefonturu(TelefonTuru._GSM);
		this.telefon4 = new Telefon();
		telefon4.setTelefonturu(TelefonTuru._FAKS);
		this.internetAdres1 = new Internetadres();
		internetAdres1.setNetadresturu(NetAdresTuru._EPOSTA);
		this.detailEditType = "";
		this.kpsSorguSonucDondu = false;
		this.kpsKaydiBulundu = false;
		this.kisiVeritabanindaBulundu = false;
	}

	public void newDetail(String detailType) {
		this.detailEditType = detailType;
		this.kurum = new Kurum();
		this.birim = new Birim();
		RequestContext context = RequestContext.getCurrentInstance();
		context.update("dialogUpdateBox");
	}

	public void saveKurum() throws DBException {
		getDBOperator().insert(this.kurum);
		this.kurumKisi.setKurumRef(this.kurum);
		RequestContext context = RequestContext.getCurrentInstance();
		setDetailEditType(null);
		context.update("dialogUpdateBox");
		context.update("kurumEditPanel");

	}

	public Boolean getKpsAdresDondu() {
		return kpsAdresDondu;
	}

	public void setKpsAdresDondu(Boolean kpsAdresDondu) {
		this.kpsAdresDondu = kpsAdresDondu;
	}

	@SuppressWarnings("unchecked")
	public String finishWizard() throws DBException {
		DBOperator dbOperator = getDBOperator();
		try {

			if (this.iletisimEmail) {
				this.uye.setIletisimEmail(EvetHayir._EVET);
			} else {
				this.uye.setIletisimEmail(EvetHayir._HAYIR);
			}
			if (this.iletisimSms) {
				this.uye.setIletisimSms(EvetHayir._EVET);
			} else {
				this.uye.setIletisimSms(EvetHayir._HAYIR);
			}
			if (this.iletisimPosta) {
				this.uye.setIletisimPosta(EvetHayir._EVET);
			} else {
				this.uye.setIletisimPosta(EvetHayir._HAYIR);
			}
			// TODO -- Tum Save'ler yapilacak
			this.uye.setKisiRef(getKisi());
			this.uye.setUyedurum(UyeDurum._PASIFUYE);
			this.uye.setUyeliktarih(new Date());
			String uyeSicil = ManagedBeanLocator.locateSessionController().uyeSicilNoOlustur(getUye(), false);
			// logYaz("Uye sicil geldi :" + uyeSicil);

			if (!isEmpty(uyeSicil)) {
				this.uye.setSicilno(uyeSicil);
			} else {
				createGenericMessage("Üye kaydedilirken bir hata meydana geldi!", FacesMessage.SEVERITY_ERROR);
				return "";
			}
			dbOperator.insert(this.uye);
			// if(getUyeController().isLoggable()){
			// logKaydet(this.uye, IslemTuru._EKLEME, "");
			// }
			ManagedBeanLocator.locateSessionController().uyeBorclandir(getUye());

			if (this.uyeogrenim.getBolumRef() != null && this.uyeogrenim.getMezuniyettarihi() != 0) {
				this.uyeogrenim.setKisiRef(this.uye.getKisiRef());
				this.uyeogrenim.setVarsayilan(EvetHayir._EVET);
				dbOperator.insert(this.uyeogrenim);
			}
			if (this.kurumKisi.getKurumRef() != null) {
				this.kurumKisi.setKisiRef(this.uye.getKisiRef());
				this.kurumKisi.setUyekurumDurum(UyekurumDurum._CALISIYOR);
				dbOperator.insert(this.kurumKisi);
			}

			if (this.adres3.getAdrestipi() == AdresTipi._YURTDISIADRESI) {
				if (this.adres3.getYabanciulke() != null && !isEmpty(this.adres3.getYabanciadres()) && this.adres3.getYabancisehir() != null) {
					this.adres3.setKisiRef(getKisi());
					this.adres3.setVarsayilan(EvetHayir._EVET);
					dbOperator.insert(this.adres3);
				}
			} else if (this.adres3.getAdrestipi() != AdresTipi._YURTDISIADRESI) {
				if (this.adres3.getUlkeRef() != null && !isEmpty(this.adres3.getAcikAdres()) && this.adres3.getSehirRef() != null && this.adres3.getIlceRef() != null) {
					if (getDBOperator().recordCount(Adres.class.getSimpleName(),
							"o.kisiRef.kimlikno=" + this.kisi.getKimlikno() + " AND  ( o.adresturu=" + AdresTuru._KPSADRESI.getCode() + " OR o.adresturu= " + AdresTuru._KPSDIGERADRESI.getCode() + " ) ") == 0) {
						this.adres3.setKisiRef(getKisi());
						this.adres3.setVarsayilan(EvetHayir._EVET);
						if (this.adres3.getAdresturu() == null) {
							this.adres3.setAdresturu(AdresTuru._EVADRESI);
						}
						dbOperator.insert(this.adres3);
					}

				}
			}

			if (this.adres2.getAdrestipi() == AdresTipi._YURTDISIADRESI) {
				if (this.adres3.getYabanciulke() != null && !isEmpty(this.adres3.getYabanciadres()) && this.adres3.getYabancisehir() != null) {
					this.adres2.setKisiRef(getKisi());
					this.adres2.setVarsayilan(EvetHayir._HAYIR);
					dbOperator.insert(this.adres2);
				}
			} else if (this.adres2.getAdrestipi() != AdresTipi._YURTDISIADRESI) {
				if (this.adres2.getUlkeRef() != null && !isEmpty(this.adres2.getAcikAdres()) && this.adres2.getSehirRef() != null && this.adres2.getIlceRef() != null) {
					this.adres2.setKisiRef(getKisi());
					this.adres2.setVarsayilan(EvetHayir._HAYIR);
					if (this.adres3.getAdresturu() == null) {
						this.adres3.setAdresturu(AdresTuru._EVADRESI);
					}
					dbOperator.insert(this.adres2);
				}
			}

			if (!isEmpty(this.telefon1.getTelefonno())) {
				if (getDBOperator().recordCount(Telefon.class.getSimpleName(), "o.kisiRef.rID=" + getKisi().getRID() + " AND o.telefonturu=" + this.telefon1.getTelefonturu().getCode()) > 0) {
					List<Telefon> telefon2Listesi = getDBOperator().load(Telefon.class.getSimpleName(), "o.kisiRef.rID=" + getKisi().getRID() + " AND o.telefonturu=" + this.telefon1.getTelefonturu().getCode(), "");
					for (Telefon entity : telefon2Listesi) {
						if (entity.getTelefonno().equals(this.telefon1.getTelefonno())) {
							entity.setVarsayilan(EvetHayir._EVET);
							getDBOperator().update(entity);
						} else {
							entity.setVarsayilan(EvetHayir._HAYIR);
							getDBOperator().update(entity);
						}
					}
				} else {
					this.telefon1.setKisiRef(this.kisi);
					this.telefon1.setVarsayilan(EvetHayir._HAYIR);
					dbOperator.insert(telefon1);
				}
			}
			if (!isEmpty(this.telefon2.getTelefonno())) {
				if (getDBOperator().recordCount(Telefon.class.getSimpleName(), "o.kisiRef.rID=" + getKisi().getRID() + " AND o.telefonturu=" + this.telefon2.getTelefonturu().getCode()) > 0) {
					List<Telefon> telefon2Listesi = getDBOperator().load(Telefon.class.getSimpleName(), "o.kisiRef.rID=" + getKisi().getRID() + " AND o.telefonturu=" + this.telefon2.getTelefonturu().getCode(), "");
					for (Telefon entity : telefon2Listesi) {
						if (entity.getTelefonno().equals(this.telefon2.getTelefonno())) {
							entity.setVarsayilan(EvetHayir._EVET);
							getDBOperator().update(entity);
						} else {
							entity.setVarsayilan(EvetHayir._HAYIR);
							getDBOperator().update(entity);
						}
					}
				} else {
					this.telefon2.setKisiRef(this.kisi);
					this.telefon2.setVarsayilan(EvetHayir._HAYIR);
					dbOperator.insert(telefon2);
				}
			}
			if (!isEmpty(this.telefon3.getTelefonno())) {
				if (getDBOperator().recordCount(Telefon.class.getSimpleName(), "o.kisiRef.rID=" + getKisi().getRID() + " AND o.telefonturu=" + this.telefon3.getTelefonturu().getCode()) > 0) {
					List<Telefon> telefon3Listesi = getDBOperator().load(Telefon.class.getSimpleName(), "o.kisiRef.rID=" + getKisi().getRID() + " AND o.telefonturu=" + this.telefon3.getTelefonturu().getCode(), "");
					for (Telefon entity : telefon3Listesi) {
						if (entity.getTelefonno().equals(this.telefon3.getTelefonno())) {
							entity.setVarsayilan(EvetHayir._EVET);
							getDBOperator().update(entity);
						} else {
							entity.setVarsayilan(EvetHayir._HAYIR);
							getDBOperator().update(entity);
						}
					}
				} else {
					this.telefon3.setKisiRef(this.kisi);
					this.telefon3.setVarsayilan(EvetHayir._EVET);
					dbOperator.insert(telefon3);
				}
			}
			if (!isEmpty(this.telefon4.getTelefonno())) {
				if (getDBOperator().recordCount(Telefon.class.getSimpleName(), "o.kisiRef.rID=" + getKisi().getRID() + " AND o.telefonturu=" + this.telefon4.getTelefonturu().getCode()) > 0) {
					List<Telefon> telefon4Listesi = getDBOperator().load(Telefon.class.getSimpleName(), "o.kisiRef.rID=" + getKisi().getRID() + " AND o.telefonturu=" + this.telefon4.getTelefonturu().getCode(), "");
					for (Telefon entity : telefon4Listesi) {
						if (entity.getTelefonno().equals(this.telefon4.getTelefonno())) {
							entity.setVarsayilan(EvetHayir._EVET);
							getDBOperator().update(entity);
						} else {
							entity.setVarsayilan(EvetHayir._HAYIR);
							getDBOperator().update(entity);
						}
					}
				} else {
					this.telefon4.setKisiRef(this.kisi);
					this.telefon4.setVarsayilan(EvetHayir._HAYIR);
					dbOperator.insert(telefon4);
				}
			}
			if (!isEmpty(this.internetAdres1.getNetadresmetni())) {
				if (getDBOperator().recordCount(Internetadres.class.getSimpleName(), "o.kisiRef.rID=" + getKisi().getRID()) > 0) {
					List<Internetadres> internetAdresListesi = getDBOperator().load(Internetadres.class.getSimpleName(), "o.kisiRef.rID=" + getKisi().getRID(), "");
					for (Internetadres entity : internetAdresListesi) {
						if (entity.getNetadresmetni().equals(this.internetAdres1.getNetadresmetni())) {
							entity.setVarsayilan(EvetHayir._EVET);
							getDBOperator().update(entity);
						} else {
							entity.setVarsayilan(EvetHayir._HAYIR);
							getDBOperator().update(entity);
						}
					}
				} else {
					this.internetAdres1.setKisiRef(this.kisi);
					this.internetAdres1.setVarsayilan(EvetHayir._EVET);
					dbOperator.insert(internetAdres1);
				}
			}

			if (getUye().getOgrenciNo() != null && getUye().getSinif() != null) {
				Uyemuafiyet uyeMuafiyet1 = new Uyemuafiyet();
				uyeMuafiyet1.setBaslangictarih(new Date());
				uyeMuafiyet1.setMuafiyettip(UyeMuafiyetTip._OGRENCI);
				uyeMuafiyet1.setUyeRef(getUye());
				getDBOperator().insert(uyeMuafiyet1);

			}
			if (getKurumKisi() != null) {
				getDBOperator().insert(getKurumKisi());

			}

			Kullanici kullanici = new Kullanici();
			kullanici.setKisiRef(getKisi());
			String kullaniciadi = "";
			if (getUye() != null) {
				if (getUye().getUyetip().getCode() == UyeTip._TURKUYRUKLU.getCode() || getUye().getUyetip().getCode() == UyeTip._YABANCILAR.getCode()) {
					kullaniciadi = getUye().getSicilno();
				} else {
					kullaniciadi = getKisi().getKimlikno().toString();
				}
			} else {
				kullaniciadi = getKisi().getKimlikno().toString();
			}

			kullanici.setName(kullaniciadi);
			String password = RandomObjectGenerator.generateRandomPassword(6);
			kullanici.setPassword(ScryptPasswordHashing.encrypt(password));
			kullanici.setTheme("bootstrap");
			getDBOperator().insert(kullanici);
			SendEmail email = new SendEmail();
			if (getDBOperator().recordCount(Internetadres.class.getSimpleName(), "o.kisiRef.rID=" + getKisi().getRID() + " AND o.varsayilan=" + EvetHayir._EVET.getCode()) > 0) {
				Internetadres varsayilanInternetAdd = (Internetadres) getDBOperator().load(Internetadres.class.getSimpleName(), "o.kisiRef.rID=" + getKisi().getRID() + " AND o.varsayilan=" + EvetHayir._EVET.getCode(), "").get(0);
				if (getUye() != null) {
					if (getUye().getUyetip().getCode() == UyeTip._TURKUYRUKLU.getCode() || getUye().getUyetip().getCode() == UyeTip._YABANCILAR.getCode()) {
						email.sendEmailToRecipent(varsayilanInternetAdd.getNetadresmetni(), "TOYS", "Kullanıcı Adı:" + kullanici.getName() + "\nŞifre :" + password + "");
					} else {
						email.sendEmailToRecipent(varsayilanInternetAdd.getNetadresmetni(), "TOYS", "Kullanıcı Adı: TC. Kimlik Numaranız\nŞifre :" + password + "");
					}
				} else {
					email.sendEmailToRecipent(varsayilanInternetAdd.getNetadresmetni(), "TOYS", "Kullanıcı Adı: TC. Kimlik Numaranız\nŞifre :" + password + "");
				}
			}
			FacesContext ctx = FacesContext.getCurrentInstance();
			ExternalContext ectx = ctx.getExternalContext();
			ectx.setRequest(ectx.getRequestContextPath() + ectx.getRequestServletPath());
			try {
				ectx.redirect(ectx.getRequestContextPath() + ectx.getRequestServletPath() + "/_page/authentication/login.xhtml");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				logYaz("Exception @" + getModelName() + "Controller :", e);
			}

		} catch (Exception e) {
			logYaz("Error @finishWizard:" + e.getMessage());
			logYaz("Exception @" + getModelName() + "Controller :", e);
			createGenericMessage(KeyUtil.getMessageValue("islem.hata"), FacesMessage.SEVERITY_ERROR);
		}
		return "";
	}

	public boolean isUyeBulundu() {
		if (!isEmpty(getUyeController().getSelectedRID())) {
			return true;
		} else {
			return false;
		}
	}

	private Boolean zorunlu = false;

	public Boolean getZorunlu() {
		return zorunlu;
	}

	public void setZorunlu(Boolean zorunlu) {
		this.zorunlu = zorunlu;
	}

	private Boolean kisiAdresDondu = false;

	public Boolean getKisiAdresDondu() {
		return kisiAdresDondu;
	}

	public void setKisiAdresDondu(Boolean kisiAdresDondu) {
		this.kisiAdresDondu = kisiAdresDondu;
	}

	private Boolean kpsZorla = false;

	public Boolean getKpsZorla() {
		return kpsZorla;
	}

	public void setKpsZorla(Boolean kpsZorla) {
		this.kpsZorla = kpsZorla;
	}

	public Kisikimlik getKisikimlik(Long kisiRID) {
		if (kisiRID != null) {
			@SuppressWarnings("unchecked")
			List<Kisikimlik> kimlikListesi = getDBOperator().load(ApplicationDescriptor._KISIKIMLIK_MODEL, "o.kisiRef.rID=" + kisiRID, "o.rID");
			if (kimlikListesi != null && kimlikListesi.size() > 0) {
				return kimlikListesi.get(0);
			} else {
				return null;
			}
		} else {
			return null;
		}
	}

	@SuppressWarnings("unchecked")
	public void kimlikBilgisiSorgula() throws ParseException {
		if (this.kisi.getKimlikno() != null) {
			setKisikimlik(new Kisikimlik());
			this.kpsKaydiBulundu = false;
			this.kpsSorguSonucDondu = false;
			this.kisiVeritabanindaBulundu = false;
			removeObjectFromSessionFilter(ApplicationDescriptor._UYE_MODEL);
			KPSService kpsService = KPSService.getInstance();
			String kpsKisi;
			try {
				// logYaz("RC 1 : " +
				// getDBOperator().recordCount(Uye.class.getSimpleName(),
				// "o.uyedurum between 1 and 2 AND o.kisiRef.kimlikno = " +
				// this.kisi.getKimlikno() + "") );
				if (getDBOperator().recordCount(Uye.class.getSimpleName(), "o.kisiRef.kimlikno = " + this.kisi.getKimlikno() + "") > 0) {
					// logYaz("Uye found...");
					createCustomMessage("Bu üye sistemde tanımlı!", FacesMessage.SEVERITY_ERROR, "kimliknoInputTextKisi:kimliknoKisi");
					// Uye varsa islem kesilecek
					getUyeController().setSelectedRID(((Uye) getDBOperator().load(Uye.class.getSimpleName(), "o.kisiRef.kimlikno = '" + this.kisi.getKimlikno() + "'", "o.rID").get(0)).getRID() + "");
					return;
				}
				// logYaz("RC 2 : " +
				// getDBOperator().recordCount(Kisi.class.getSimpleName(),
				// "o.kimlikno = " + this.kisi.getKimlikno()));
				if (getDBOperator().recordCount(Kisi.class.getSimpleName(), "o.kimlikno = " + this.kisi.getKimlikno()) > 0) {
					Kisi bulunanKisi = (Kisi) getDBOperator().find(Kisi.class.getSimpleName(), "o.kimlikno=" + this.kisi.getKimlikno() + "");
					setKisi(bulunanKisi);
					if (getKisikimlik(bulunanKisi.getRID()) != null) {
						setKisikimlik(getKisikimlik(bulunanKisi.getRID()));
						this.zorunlu = true;
						this.kisiVeritabanindaBulundu = true;
						if (getDBOperator().recordCount(Adres.class.getSimpleName(),
								"o.kisiRef.kimlikno=" + this.kisi.getKimlikno() + " AND  ( o.adresturu=" + AdresTuru._KPSADRESI.getCode() + " OR o.adresturu= " + AdresTuru._KPSDIGERADRESI.getCode() + " ) ") > 0) {
							this.zorunlu = true;
							List<Adres> adresList = getDBOperator().load(Adres.class.getSimpleName(),
									"o.kisiRef.kimlikno=" + this.kisi.getKimlikno() + " AND  ( o.adresturu=" + AdresTuru._KPSADRESI.getCode() + " OR o.adresturu= " + AdresTuru._KPSDIGERADRESI.getCode() + " ) ", "");
							for (Adres entity : adresList) {
								if (entity.getAdresturu().getCode() == AdresTuru._KPSADRESI.getCode()) {
									setKpsAdresDondu(true);
									setKpsAdres(entity);
								}
								if (entity.getAdresturu().getCode() == AdresTuru._KPSDIGERADRESI.getCode()) {
									setKpsDigerAdres(entity);
									setKpsAdresDondu(true);
								}
							}

						}
						if (getDBOperator().recordCount(Adres.class.getSimpleName(),
								"o.kisiRef.kimlikno=" + this.kisi.getKimlikno() + " AND o.adresturu=" + AdresTuru._EVADRESI.getCode() + " AND o.varsayilan=" + EvetHayir._EVET.getCode()) > 0) {
							setAdres3((Adres) getDBOperator()
									.load(Adres.class.getSimpleName(), "o.kisiRef.kimlikno=" + this.kisi.getKimlikno() + " AND o.adresturu=" + AdresTuru._EVADRESI.getCode() + " AND o.varsayilan=" + EvetHayir._EVET.getCode(), "").get(0));
						}

					} else {
						String kpsAdres = kpsService.kisiAdresBilgisiGetirYeni(this.kisi.getKimlikno());
						this.zorunlu = true;
						// PojoAdres kpsPojo = XmlHelper.readXmlStringForAdres(kpsAdres, AdresTuru._KPSADRESI);
						// PojoAdres kpsDigerPojo = XmlHelper.readXmlStringForAdres(kpsAdres, AdresTuru._KPSDIGERADRESI);
						PojoAdres kpsPojo = ToysXMLReader.readXmlStringForAdres(kpsAdres, "YerlesimYeriAdresi");
						PojoAdres kpsDigerPojo = ToysXMLReader.readXmlStringForAdres(kpsAdres, "DigerAdresBilgileri");
						if (kpsPojo != null && (kpsPojo.getHataBilgisiKodu() == null || kpsPojo.getHataBilgisiKodu().trim().equals(""))) {
							setKpsAdresDondu(true);
							setKpsAdres(kisiAdresAktar(kpsPojo, AdresTuru._KPSADRESI));
						}
						// logYaz("kpsDigerPojo hata bilgisi kodu :"
						// + kpsDigerPojo.getHataBilgisiKodu());
						if (kpsDigerPojo != null && (kpsDigerPojo.getHataBilgisiKodu() == null || kpsDigerPojo.getHataBilgisiKodu().trim().equals(""))) {
							setKpsAdresDondu(true);
							setKpsDigerAdres(kisiAdresAktar(kpsDigerPojo, AdresTuru._KPSDIGERADRESI));
						}
					}
				} else {
					kpsKisi = kpsService.kisiKimlikBilgisiGetirYeni(this.kisi.getKimlikno());
					// this.kisikimlikPojo = XmlHelper.readXmlString(kpsKisi.trim());
					this.kisikimlikPojo = ToysXMLReader.readXmlStringForKimlik(kpsKisi.trim());
					this.kpsSorguSonucDondu = true;
					this.zorunlu = true;
					SistemParametre sistemParametre = new SistemParametre();
					if (getDBOperator().recordCount(SistemParametre.class.getSimpleName(), "") > 0) {
						sistemParametre = (SistemParametre) getDBOperator().load(SistemParametre.class.getSimpleName(), "", "o.rID").get(0);
					}
					if (!isEmpty(this.kisikimlikPojo.getHataBilgisiKodu()) && sistemParametre != null) {
						if (sistemParametre.getKpszorla() == EvetHayir._HAYIR) {
							createCustomMessage("Girdiğiniz kimlik numarası KPS sisteminde bulunamadı!", FacesMessage.SEVERITY_ERROR, "kimliknoInputTextKisi:kimliknoKisi");
							this.kisikimlik.setKimlikno(this.kisi.getKimlikno());
							setUyeController(new UyeController());
							setKpsZorla(true);
							return;
						} else {
							setKpsZorla(false);
							createGenericMessage("Girdiğiniz kimlik numarası KPS sisteminde bulunamadı! \nSistem Yönetimi KPS'de bulunmayan " + "kimlik numarasıyla giriş yapılmasını engelledi.Bu yüzden kişiyi üye yapamazsınız!",
									FacesMessage.SEVERITY_ERROR);
							return;
						}
					}
					this.kpsKaydiBulundu = true;
					this.kisi.setAd(this.kisikimlikPojo.getAd());
					this.kisi.setSoyad(this.kisikimlikPojo.getSoyad());
					kisiKimlikBilgisiAktar();
					String kpsAdres = kpsService.kisiAdresBilgisiGetirYeni(this.kisi.getKimlikno());
					setKpsAdresDondu(true);
					// PojoAdres kpsPojo = XmlHelper.readXmlStringForAdres(kpsAdres, AdresTuru._KPSADRESI);
					// PojoAdres kpsDigerPojo = XmlHelper.readXmlStringForAdres(kpsAdres, AdresTuru._KPSDIGERADRESI);
					PojoAdres kpsPojo = ToysXMLReader.readXmlStringForAdres(kpsAdres, "YerlesimYeriAdresi");
					PojoAdres kpsDigerPojo = ToysXMLReader.readXmlStringForAdres(kpsAdres, "DigerAdresBilgileri");
					// logYaz("kpsPojo hata bilgisi kodu :" +
					// kpsPojo.getHataBilgisiKodu());
					if (kpsPojo != null && (kpsPojo.getHataBilgisiKodu() == null || kpsPojo.getHataBilgisiKodu().trim().equals(""))) {
						setKpsAdres(kisiAdresAktar(kpsPojo, AdresTuru._KPSADRESI));
					}
					// logYaz("kpsDigerPojo hata bilgisi kodu :"
					// + kpsDigerPojo.getHataBilgisiKodu());
					if (kpsDigerPojo != null && (kpsDigerPojo.getHataBilgisiKodu() == null || kpsDigerPojo.getHataBilgisiKodu().trim().equals(""))) {
						setKpsDigerAdres(kisiAdresAktar(kpsDigerPojo, AdresTuru._KPSDIGERADRESI));
					}
				}
			} catch (Exception e) {
				this.kpsSorguSonucDondu = false;
				this.kpsKaydiBulundu = false;
				createGenericMessage("KPS Sorgulaması Başarısız oldu!", FacesMessage.SEVERITY_ERROR);
				logYaz("Exception @" + getModelName() + "Controller :", e);
			}
		}

	}

	private void kisiKimlikBilgisiAktar() throws Exception {
		if (isNumber(this.kisikimlikPojo.getCinsiyetKod())) {
			this.kisikimlik.setCinsiyet(Cinsiyet.getWithCode(Integer.parseInt(this.kisikimlikPojo.getCinsiyetKod())));
		}
		this.kisikimlik.setKimlikno(this.kisikimlikPojo.getTcKimlikNo());
		this.kisikimlik.setBabaad(this.kisikimlikPojo.getBabaAdi());
		this.kisikimlik.setAnaad(this.kisikimlikPojo.getAnaAdi());
		if (isNumber(this.kisikimlikPojo.getMedeniHalKodu())) {
			this.kisikimlik.setMedenihal(MedeniHal.getWithCode(Integer.parseInt(this.kisikimlikPojo.getMedeniHalKodu())));
		}
		this.kisikimlik.setDogumyer(this.kisikimlikPojo.getDogumYeri());
		if (this.kisikimlikPojo.getDogumTarihi() != null && !this.kisikimlikPojo.getDogumTarihi().toString().equalsIgnoreCase("null/null/null")) {
			this.kisikimlik.setDogumtarih(DateUtil.createDateObject(this.kisikimlikPojo.getDogumTarihi()));
		}
		if (this.kisikimlikPojo.getOlumTarihi() != null && !this.kisikimlikPojo.getOlumTarihi().toString().equalsIgnoreCase("null/null/null")) {
			this.kisikimlik.setOlumtarih(DateUtil.createDateObject(this.kisikimlikPojo.getOlumTarihi()));
		}
		if (isNumber(this.kisikimlikPojo.getDurumKod())) {
			this.kisikimlik.setKisidurum(KisiDurum.getWithCode(Integer.parseInt(this.kisikimlikPojo.getDurumKod())));
		}
		String plakakodu = kisikimlikPojo.getNufusaKayitliOlduguIlKodu();
		int ilkodulength = kisikimlikPojo.getNufusaKayitliOlduguIlKodu().length();
		int zero = 0;
		if (ilkodulength == 1) {
			plakakodu = zero + plakakodu;
		}
		if (getDBOperator().recordCount(Sehir.class.getSimpleName(), "o.ilKodu='" + plakakodu + "'") > 0) {
			this.kisikimlik.setSehirRef((Sehir) getDBOperator().find(Sehir.class.getSimpleName(), "o.ilKodu='" + plakakodu + "'"));
			changeIlce(this.kisikimlik.getSehirRef().getRID());
			this.kisikimlik.setIlceRef((Ilce) getDBOperator().find(Ilce.class.getSimpleName(), "o.ilcekodu='" + this.kisikimlikPojo.getNufusaKayitliOlduguIlceKodu() + "'"));
			this.kisikimlik.setMahalle(kisikimlikPojo.getMahalle());
		}
		this.kisikimlik.setCiltno(this.kisikimlikPojo.getCiltNo());
		this.kisikimlik.setAileno(this.kisikimlikPojo.getAileSiraNo());
		this.kisikimlik.setSirano(this.kisikimlikPojo.getBireySiraNo());
	}

	private Adres kisiAdresAktar(PojoAdres kpsPojo, AdresTuru adresTuru) throws Exception {
		Adres yeniAdres = new Adres();
		yeniAdres.setAdresturu(adresTuru);
		yeniAdres.setAdrestipi(kpsPojo.getAdrestipi());
		yeniAdres.setAcikAdres(kpsPojo.getAcikAdres());
		yeniAdres.setAdresNo(kpsPojo.getAdresNo());
		if (!isEmpty(kpsPojo.getBeyanTarihi()) && !kpsPojo.getBeyanTarihi().toString().equalsIgnoreCase("null/null/null")) {
			yeniAdres.setBeyanTarihi(DateUtil.createDateObject(kpsPojo.getBeyanTarihi()));
		}
		yeniAdres.setBinaAda(kpsPojo.getBinaAda());
		yeniAdres.setBinaBlokAdi(kpsPojo.getBinaBlokAdi());
		yeniAdres.setBinaKodu(kpsPojo.getBinaKodu());
		yeniAdres.setBinaPafta(kpsPojo.getBinaPafta());
		yeniAdres.setBinaParsel(kpsPojo.getBinaParsel());
		yeniAdres.setBinaPafta(kpsPojo.getBinaPafta());
		yeniAdres.setBinaSiteAdi(kpsPojo.getBinaSiteAdi());
		yeniAdres.setCsbm(kpsPojo.getCsbm());
		yeniAdres.setCsbmKodu(kpsPojo.getCsbmKodu());
		yeniAdres.setDisKapiNo(kpsPojo.getDisKapiNo());
		yeniAdres.setIcKapiNo(kpsPojo.getIcKapiNo());
		if (!isEmpty(kpsPojo.getIlKodu()) && isNumber(kpsPojo.getIlKodu().trim())) {
			if (getDBOperator().recordCount(Sehir.class.getSimpleName(), "o.ilKodu='" + kpsPojo.getIlKodu() + "'") > 0) {
				yeniAdres.setSehirRef((Sehir) getDBOperator().find(Sehir.class.getSimpleName(), "o.ilKodu='" + kpsPojo.getIlKodu() + "'"));
			}
		}
		if (!isEmpty(kpsPojo.getIlceKodu()) && isNumber(kpsPojo.getIlceKodu().trim())) {
			if (getDBOperator().recordCount(Ilce.class.getSimpleName(), "o.ilcekodu='" + kpsPojo.getIlceKodu() + "'") > 0) {
				yeniAdres.setIlceRef((Ilce) getDBOperator().find(Ilce.class.getSimpleName(), "o.ilcekodu='" + kpsPojo.getIlceKodu() + "'"));
			}
		}
		yeniAdres.setKpsGuncellemeTarihi(new Date());
		yeniAdres.setMahalle(kpsPojo.getMahalle());
		yeniAdres.setMahalleKodu(kpsPojo.getMahalleKodu());
		if (kpsPojo.getTasinmaTarihi() != null && !kpsPojo.getTasinmaTarihi().toString().equalsIgnoreCase("null/null/null")) {
			yeniAdres.setTasinmaTarihi(DateUtil.createDateObject(kpsPojo.getTasinmaTarihi()));
		}
		if (kpsPojo.getTescilTarihi() != null && !kpsPojo.getTescilTarihi().toString().equalsIgnoreCase("null/null/null")) {
			yeniAdres.setTescilTarihi(DateUtil.createDateObject(kpsPojo.getTescilTarihi()));
		}
		yeniAdres.setVarsayilan(EvetHayir._HAYIR);
		return yeniAdres;
	}

	public KurumKisi getKurumKisi() {
		return kurumKisi;
	}

	public void setKurumKisi(KurumKisi kurumKisi) {
		this.kurumKisi = kurumKisi;
	}

	@Override
	public void wizardForward() {
		if (this.wizardStep == 1) {
			if (this.uye.getUyetip() == UyeTip._OGRENCI) {
				if (this.uye.getSinif() == null || this.uye.getSinif() == 0 || isEmpty(this.uye.getOgrenciNo())) {
					createGenericMessage("Öğrenciler için sınıf ve öğrenci numarası alanları zorunludur!", FacesMessage.SEVERITY_ERROR);
					return;
				}
			}
			wizardStep++;
		} else if (this.wizardStep == 2) {
			// Kisi ve kisikimlik bilgileri kaydedilecek...
			try {
				if (!kisiVeritabanindaBulundu && getKisi().getRID() == null) {
					getDBOperator().insert(getKisi());
					// if(getKisiController().isLoggable()){
					// logKaydet(getKisi(), IslemTuru._EKLEME, "");
					// }
					this.kisikimlik.setKisiRef(getKisi());
					getDBOperator().insert(getKisikimlik());
					// if(getKisikimlikController().isLoggable()){
					// logKaydet(this.kisikimlik, IslemTuru._EKLEME, "");
					// }

					if (this.kpsAdres.getKisiRef() != null) {
						this.kpsAdres.setKisiRef(getKisi());
						this.kpsAdres.setAdresturu(AdresTuru._KPSADRESI);
						getDBOperator().insert(this.kpsAdres);
						if (this.kpsDigerAdres.getSehirRef() != null) {
							this.kpsDigerAdres.setKisiRef(getKisi());
							this.kpsDigerAdres.setAdresturu(AdresTuru._KPSADRESI);
							getDBOperator().insert(this.kpsDigerAdres);
						}
					} else {
						KPSService kpsService = KPSService.getInstance();
						String kpsAdres;
						try {
							kpsAdres = kpsService.kisiAdresBilgisiGetirYeni(this.kisi.getKimlikno());
							setKpsAdresDondu(true);
							// PojoAdres kpsPojo = XmlHelper.readXmlStringForAdres(kpsAdres, AdresTuru._KPSADRESI);
							// PojoAdres kpsDigerPojo = XmlHelper.readXmlStringForAdres(kpsAdres, AdresTuru._KPSDIGERADRESI);
							PojoAdres kpsPojo = ToysXMLReader.readXmlStringForAdres(kpsAdres, "YerlesimYeriAdresi");
							PojoAdres kpsDigerPojo = ToysXMLReader.readXmlStringForAdres(kpsAdres, "DigerAdresBilgileri");

							if (kpsPojo != null && (kpsPojo.getHataBilgisiKodu() == null || kpsPojo.getHataBilgisiKodu().trim().equals(""))) {
								setKpsAdres(kisiAdresAktar(kpsPojo, AdresTuru._KPSADRESI));
								getKpsAdres().setKisiRef(this.kisi);
								getDBOperator().insert(getKpsAdres());
							}
							// logYaz("kpsDigerPojo hata bilgisi kodu :"
							// + kpsDigerPojo.getHataBilgisiKodu());
							if (kpsDigerPojo != null && (kpsDigerPojo.getHataBilgisiKodu() == null || kpsDigerPojo.getHataBilgisiKodu().trim().equals(""))) {
								setKpsDigerAdres(kisiAdresAktar(kpsDigerPojo, AdresTuru._KPSDIGERADRESI));
								getDBOperator().insert(getKpsDigerAdres());
							}
						} catch (RemoteException e) {
							logYaz("Exception @" + getModelName() + "Controller :", e);
						} catch (Exception e) {
							logYaz("Exception @" + getModelName() + "Controller :", e);
						}

					}

					createGenericMessage(KeyUtil.getMessageValue("kisi.kaydedildi"), FacesMessage.SEVERITY_INFO);
				} else {
					if (this.kisikimlikPojo.getTcKimlikNo() != null) {
						this.kisi.setKimlikno(this.kisikimlikPojo.getTcKimlikNo());
					}
				}
				this.uyeogrenim.setDenklikdurum(EvetHayir._HAYIR);
				wizardStep++;
			} catch (DBException e) {
				createGenericMessage(KeyUtil.getMessageValue("kisi.hata"), FacesMessage.SEVERITY_ERROR);
				logYaz("Exception @" + getModelName() + "Controller :", e);
			}
		} else if (this.wizardStep == 4) {
			// UYe kurum secildiyese, adres aktarilacak...
			try {
				// if(getKurumKisi().getKurumRef() != null &&
				// getKurumKisi().getBaslangictarih() != null){
				// setAdres2(getKurumKisi().getAdresRef());
				// } else if(getUyekurum().getKurumRef() != null &&
				// getUyekurum().getBaslangictarih() == null){
				// createCustomMessage("Görev başlangıç tarihi giriniz!",
				// FacesMessage.SEVERITY_ERROR,
				// "baslangictarihCalendarKurum:baslangictarihKurum");
				// return;
				// }
				this.adres3.setAdresturu(AdresTuru._EVADRESI);
				this.adres2.setAdresturu(AdresTuru._ISADRESI);
				if (getKpsAdres() != null && !isEmpty(getKpsAdres().getAcikAdres())) {
					setAdres3((Adres) kpsAdres.cloneObject());
					getAdres3().setRID(null);
					getAdres3().setAdresturu(AdresTuru._EVADRESI);
					getAdres3().setUlkeRef((Ulke) getDBOperator().find(Ulke.class.getSimpleName(), "o.rID = 1"));
					if (getAdres3().getUlkeRef() != null) {
						changeIl(getAdres3().getUlkeRef().getRID());
						if (getAdres3().getSehirRef() != null) {
							changeIlce(getAdres3().getSehirRef().getRID());
						}
					}
				} else if (getKisiAdresDondu() != null && !isEmpty(getAdres3().getAcikAdres())) {
					getAdres3().setVarsayilan(getAdres3().getVarsayilan());
				}
				// if(getKurumKisi().get() != null){
				// setAdres2((Adres) getUyekurum().getAdresRef().cloneObject());
				// getAdres2().setRID(null);
				// getAdres2().setAdresturu(AdresTuru._ISADRESI);
				// if(getAdres2().getUlkeRef() != null){
				// changeIlForUyeIsyeri(getAdres2().getUlkeRef().getRID());
				// if(getAdres2().getSehirRef() != null){
				// changeIlceForUyeIsyeri(getAdres2().getSehirRef().getRID());
				// getAdres2().setIlceRef(getAdres2().getIlceRef());
				// }
				// }
				// }

				if (getKisikimlik() != null) {
					kurumKisi.setKisiRef(getKisikimlik().getKisiRef());
				}
				kurumKisi.setKurumRef(getKurumKisi().getKurumRef());
				kurumKisi.setGecerli(EvetHayir._EVET);

				wizardStep++;
			} catch (Exception e) {
				logYaz("Exception @" + getModelName() + "Controller :", e);
			}
		} else if (this.wizardStep == 5) {
			try {
				if (this.isyeriadresvarsayilan) {
					this.adres2.setVarsayilan(EvetHayir._EVET);
				} else {
					this.adres2.setVarsayilan(EvetHayir._HAYIR);
				}
				if (this.beyanadresvarsayilan) {
					this.adres3.setVarsayilan(EvetHayir._EVET);
				} else {
					this.adres3.setVarsayilan(EvetHayir._HAYIR);
				}
				if (this.adres3.getAdrestipi().getCode() == AdresTipi._ILILCEMERKEZI.getCode()) {
					this.adres3.setUlkeRef((Ulke) getDBOperator().find(Ulke.class.getSimpleName(), "o.rID = 1"));
				}
				if (getAdres3().getAcikAdres() != null && getAdres3().getAdrestipi() != null && getAdres3().getAdresturu() != null && getAdres3().getIlceRef() != null && getAdres3().getSehirRef() != null) {
					this.adres3.setAcikAdres(getAdres3().getAcikAdres());
					this.adres3.setAdrestipi(getAdres3().getAdrestipi());
					this.adres3.setAdresturu(getAdres3().getAdresturu());
					this.adres3.setIlceRef(getAdres3().getIlceRef());
					this.adres3.setSehirRef(getAdres3().getSehirRef());
				}
			} catch (Exception e) {
				logYaz("Exception @" + getModelName() + "Controller :", e);
			}
			wizardStep++;
		} else {
			wizardStep++;
		}
	}

	@Override
	public void wizardBackward() {
		wizardStep--;
	}

	public SelectItem[] getSelectAdresListForUyeKurum() {
		if (getKurumKisi().getKurumRef() != null) {
			@SuppressWarnings("unchecked")
			List<Adres> entityList = getDBOperator().load(Adres.class.getSimpleName(), "o.kurumRef.rID=" + getKurumKisi().getKurumRef().getRID(), "o.varsayilan");
			if (entityList != null) {
				SelectItem[] selectItemList = new SelectItem[entityList.size() + 1];
				selectItemList[0] = new SelectItem(null, "");
				int i = 1;
				for (BaseEntity entity : entityList) {
					selectItemList[i++] = new SelectItem(entity, entity.getUIString());
				}
				setSelectAdresListForUyeKurum(selectItemList);
			} else {
				setSelectAdresListForUyeKurum(new SelectItem[0]);
			}
		} else {
			setSelectAdresListForUyeKurum(new SelectItem[0]);
		}
		return selectAdresListForUyeKurum;
	}

	public void setSelectAdresListForUyeKurum(SelectItem[] selectAdresListForUyeKurum) {
		this.selectAdresListForUyeKurum = selectAdresListForUyeKurum;
	}

	public Uye getUye() {
		uye = (Uye) getEntity();
		return uye;
	}

	public void setUye(Uye uye) {
		this.uye = uye;
	}

	@Override
	public int getWizardStep() {
		return wizardStep;
	}

	@Override
	public void setWizardStep(int wizardStep) {
		this.wizardStep = wizardStep;
	}

	public Kisi getKisi() {
		return kisi;
	}

	public void setKisi(Kisi kisi) {
		this.kisi = kisi;
	}

	public Adres getAdres2() {
		return adres2;
	}

	public void setAdres2(Adres adres2) {
		this.adres2 = adres2;
	}

	public Adres getAdres3() {
		return adres3;
	}

	public void setAdres3(Adres adres3) {
		this.adres3 = adres3;
	}

	public Telefon getTelefon1() {
		return telefon1;
	}

	public void setTelefon1(Telefon telefon1) {
		this.telefon1 = telefon1;
	}

	public Telefon getTelefon2() {
		return telefon2;
	}

	public void setTelefon2(Telefon telefon2) {
		this.telefon2 = telefon2;
	}

	public Telefon getTelefon3() {
		return telefon3;
	}

	public void setTelefon3(Telefon telefon3) {
		this.telefon3 = telefon3;
	}

	public Telefon getTelefon4() {
		return telefon4;
	}

	public void setTelefon4(Telefon telefon4) {
		this.telefon4 = telefon4;
	}

	public Internetadres getInternetAdres1() {
		return internetAdres1;
	}

	public void setInternetAdres1(Internetadres internetAdres1) {
		this.internetAdres1 = internetAdres1;
	}

	public Kisikimlik getKisikimlik() {
		return kisikimlik;
	}

	public void setKisikimlik(Kisikimlik kisikimlik) {
		this.kisikimlik = kisikimlik;
	}

	/* MANAGED PROPERTIES USED */
	private UyeController uyeController = new UyeController(null);
	private KisiController kisiController = new KisiController(null);
	private KisikimlikController kisikimlikController = new KisikimlikController(null);
	private InternetadresController internetadresController = new InternetadresController(null);
	private AdresController adresController = new AdresController(null);
	private TelefonController telefonController = new TelefonController(null);

	public UyeController getUyeController() {
		return uyeController;
	}

	public void setUyeController(UyeController uyeController) {
		this.uyeController = uyeController;
	}

	public KisiController getKisiController() {
		return kisiController;
	}

	public void setKisiController(KisiController kisiController) {
		this.kisiController = kisiController;
	}

	public KisikimlikController getKisikimlikController() {
		return kisikimlikController;
	}

	public void setKisikimlikController(KisikimlikController kisikimlikController) {
		this.kisikimlikController = kisikimlikController;
	}

	public InternetadresController getInternetadresController() {
		return internetadresController;
	}

	public void setInternetadresController(InternetadresController internetadresController) {
		this.internetadresController = internetadresController;
	}

	public TelefonController getTelefonController() {
		return telefonController;
	}

	public void setTelefonController(TelefonController telefonController) {
		this.telefonController = telefonController;
	}

	public Kisiogrenim getUyeogrenim() {
		return uyeogrenim;
	}

	public void setUyeogrenim(Kisiogrenim uyeogrenim) {
		this.uyeogrenim = uyeogrenim;
	}

	public Kurum getKurum() {
		return kurum;
	}

	public void setKurum(Kurum kurum) {
		this.kurum = kurum;
	}

	public String getDetailEditType() {
		return detailEditType;
	}

	public void setDetailEditType(String detailEditType) {
		this.detailEditType = detailEditType;
	}

	public Birim getBirim() {
		return birim;
	}

	public void setBirim(Birim birim) {
		this.birim = birim;
	}

	public boolean isKpsSorguSonucDondu() {
		return kpsSorguSonucDondu;
	}

	public void setKpsSorguSonucDondu(boolean kpsSorguSonucDondu) {
		this.kpsSorguSonucDondu = kpsSorguSonucDondu;
	}

	public boolean isKpsKaydiBulundu() {
		return kpsKaydiBulundu;
	}

	public void setKpsKaydiBulundu(boolean kpsKaydiBulundu) {
		this.kpsKaydiBulundu = kpsKaydiBulundu;
	}

	public Adres getKpsAdres() {
		return kpsAdres;
	}

	public void setKpsAdres(Adres kpsAdres) {
		this.kpsAdres = kpsAdres;
	}

	public Adres getKpsDigerAdres() {
		return kpsDigerAdres;
	}

	public void setKpsDigerAdres(Adres kpsDigerAdres) {
		this.kpsDigerAdres = kpsDigerAdres;
	}

	public AdresController getAdresController() {
		return adresController;
	}

	public void setAdresController(AdresController adresController) {
		this.adresController = adresController;
	}

	/* HANDLE CHANGE EVENTLERI */
	public SelectItem[] getSelectIlceListForIsyeriAdres() {
		if (getAdres2().getSehirRef() != null) {
			@SuppressWarnings("unchecked")
			List<Ilce> entityList = getDBOperator().load(Ilce.class.getSimpleName(), "o.sehirRef.rID=" + getAdres2().getSehirRef().getRID(), "o.ad");
			if (entityList != null) {
				SelectItem[] selectItemList = new SelectItem[entityList.size() + 1];
				selectItemList[0] = new SelectItem(null, "");
				int i = 1;
				for (BaseEntity entity : entityList) {
					selectItemList[i++] = new SelectItem(entity, entity.getUIString());
				}
				return selectItemList;
			} else {
				return new SelectItem[0];
			}
		} else {
			return new SelectItem[0];
		}
	}

	public SelectItem[] getSelectIlceListForBeyanAdres() {
		if (getAdres3().getSehirRef() != null) {
			@SuppressWarnings("unchecked")
			List<Ilce> entityList = getDBOperator().load(Ilce.class.getSimpleName(), "o.sehirRef.rID=" + getAdres3().getSehirRef().getRID(), "o.ad");
			if (entityList != null) {
				SelectItem[] selectItemList = new SelectItem[entityList.size() + 1];
				selectItemList[0] = new SelectItem(null, "");
				int i = 1;
				for (BaseEntity entity : entityList) {
					selectItemList[i++] = new SelectItem(entity, entity.getUIString());
				}
				return selectItemList;
			} else {
				return new SelectItem[0];
			}
		} else {
			return new SelectItem[0];
		}
	}

	public SelectItem[] getSelectFakulteList() {
		if (getUyeogrenim().getUniversiteRef() != null) {
			@SuppressWarnings("unchecked")
			List<Fakulte> entityList = getDBOperator().load(Fakulte.class.getSimpleName(), "o.universiteRef.rID=" + getUyeogrenim().getUniversiteRef().getRID(), "o.ad");
			if (entityList != null) {
				SelectItem[] selectItemList = new SelectItem[entityList.size() + 1];
				selectItemList[0] = new SelectItem(null, "");
				int i = 1;
				for (BaseEntity entity : entityList) {
					selectItemList[i++] = new SelectItem(entity, entity.getUIString());
				}
				return selectItemList;
			} else {
				return new SelectItem[0];
			}
		} else {
			return new SelectItem[0];
		}
	}

	public SelectItem[] getSelectBolumList() {
		if (getUyeogrenim().getFakulteRef() != null) {
			@SuppressWarnings("unchecked")
			List<Bolum> entityList = getDBOperator().load(Bolum.class.getSimpleName(), "o.fakulteRef.rID=" + getUyeogrenim().getFakulteRef().getRID(), "o.ad");
			if (entityList != null) {
				SelectItem[] selectItemList = new SelectItem[entityList.size() + 1];
				selectItemList[0] = new SelectItem(null, "");
				int i = 1;
				for (BaseEntity entity : entityList) {
					selectItemList[i++] = new SelectItem(entity, entity.getUIString());
				}
				return selectItemList;
			} else {
				return new SelectItem[0];
			}
		} else {
			return new SelectItem[0];
		}
	}

	public boolean isKisiVeritabanindaBulundu() {
		return kisiVeritabanindaBulundu;
	}

	public void setKisiVeritabanindaBulundu(boolean kisiVeritabanindaBulundu) {
		this.kisiVeritabanindaBulundu = kisiVeritabanindaBulundu;
	}

	public String callDetailFromWizard() {
		return getUyeController().detailPage(null);
	}

	public PojoKisi getKisikimlikPojo() {
		return kisikimlikPojo;
	}

	public void setKisikimlikPojo(PojoKisi kisikimlikPojo) {
		this.kisikimlikPojo = kisikimlikPojo;
	}

	public Boolean getIletisimEmail() {
		return iletisimEmail;
	}

	public void setIletisimEmail(Boolean iletisimEmail) {
		this.iletisimEmail = iletisimEmail;
	}

	public Boolean getIletisimSms() {
		return iletisimSms;
	}

	public void setIletisimSms(Boolean iletisimSms) {
		this.iletisimSms = iletisimSms;
	}

	public Boolean getIletisimPosta() {
		return iletisimPosta;
	}

	public void setIletisimPosta(Boolean iletisimPosta) {
		this.iletisimPosta = iletisimPosta;
	}

	public Boolean getIsyeriadresvarsayilan() {
		return isyeriadresvarsayilan;
	}

	public void setIsyeriadresvarsayilan(Boolean isyeriadresvarsayilan) {
		this.isyeriadresvarsayilan = isyeriadresvarsayilan;
	}

	public Boolean getBeyanadresvarsayilan() {
		return beyanadresvarsayilan;
	}

	public void setBeyanadresvarsayilan(Boolean beyanadresvarsayilan) {
		this.beyanadresvarsayilan = beyanadresvarsayilan;
	}

	private SelectItem[] ilItemListForIsyeriAdres;
	private SelectItem[] ilceItemListForIsyeriAdres;

	public SelectItem[] getIlItemListForIsyeriAdres() {
		return ilItemListForIsyeriAdres;
	}

	public void setIlItemListForIsyeriAdres(SelectItem[] ilItemListForIsyeriAdres) {
		this.ilItemListForIsyeriAdres = ilItemListForIsyeriAdres;
	}

	public SelectItem[] getIlceItemListForIsyeriAdres() {
		return ilceItemListForIsyeriAdres;
	}

	public void setIlceItemListForIsyeriAdres(SelectItem[] ilceItemListForIsyeriAdres) {
		this.ilceItemListForIsyeriAdres = ilceItemListForIsyeriAdres;
	}

	protected void changeIlForUyeIsyeri(Long selectedRecId) {
		if (selectedRecId.longValue() == 0) {
			setIlItemListForIsyeriAdres(new SelectItem[0]);
		} else {
			@SuppressWarnings("unchecked")
			// List<Sehir> entityList =
			// getDBOperator().load(Sehir.class.getSimpleName(),
			// "o.ulkeRef.rID=" + selectedRecId ,"o.ad");
			List<Sehir> entityList = getDBOperator().load(Sehir.class.getSimpleName(), "", "o.ad");
			if (entityList != null) {
				ilItemListForIsyeriAdres = new SelectItem[entityList.size() + 1];
				ilItemListForIsyeriAdres[0] = new SelectItem(null, "");
				int i = 1;
				for (Sehir entity : entityList) {
					ilItemListForIsyeriAdres[i++] = new SelectItem(entity, entity.getUIString());
				}
			} else {
				setIlItemListForIsyeriAdres(new SelectItem[0]);
			}
		}
	}

	protected void changeIlceForUyeIsyeri(Long selectedRecId) {
		if (selectedRecId.longValue() == 0) {
			setIlceItemListForIsyeriAdres(new SelectItem[0]);
		} else {
			@SuppressWarnings("unchecked")
			List<Ilce> entityList = getDBOperator().load(Ilce.class.getSimpleName(), "o.sehirRef.rID=" + selectedRecId, "o.ad");
			if (entityList != null) {
				ilceItemListForIsyeriAdres = new SelectItem[entityList.size() + 1];
				ilceItemListForIsyeriAdres[0] = new SelectItem(null, "");
				int i = 1;
				for (Ilce entity : entityList) {
					ilceItemListForIsyeriAdres[i++] = new SelectItem(entity, entity.getUIString());
				}
			} else {
				setIlceItemListForIsyeriAdres(new SelectItem[0]);
			}
		}
	}

	public DataTable getTable2() {
		return table2;
	}

	public void setTable2(DataTable table2) {
		this.table2 = table2;
	}

} // class
