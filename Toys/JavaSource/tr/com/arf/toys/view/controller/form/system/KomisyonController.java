package tr.com.arf.toys.view.controller.form.system; 
  
 
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.toys.db.filter.system.KomisyonFilter;
import tr.com.arf.toys.db.model.system.Komisyon;
import tr.com.arf.toys.view.controller.form._base.ToysBaseFilteredController;
  
@ManagedBean 
@ViewScoped 
public class KomisyonController extends ToysBaseFilteredController {  
  
  private static final long serialVersionUID = 6531152165884980748L; 
  
	private Komisyon komisyon;  
	private KomisyonFilter komisyonFilter = new KomisyonFilter();  
  
	public KomisyonController() { 
		super(Komisyon.class);  
		setDefaultValues(false);  
		setOrderField("ad");  
		if(!getSessionUser().isSuperUser()){
			setStaticWhereCondition(" o.birimRef.rID=" + getSessionUser().getPersonelRef().getBirimRef().getRID());
//			getKomisyonFilter().setBirimRef(getSessionUser().getPersonelRef().getBirimRef());
			getKomisyonFilter().createQueryCriterias();
			
		}
		setAutoCompleteSearchColumns(new String[]{"ad"}); 
		setTable(null); 
		queryAction(); 
	} 
	
	
	public Komisyon getKomisyon() { 
		komisyon = (Komisyon) getEntity(); 
		return komisyon; 
	} 
	
	public void setKomisyon(Komisyon komisyon) { 
		this.komisyon = komisyon; 
	} 
	
	public KomisyonFilter getKomisyonFilter() { 
		return komisyonFilter; 
	} 
	 
	public void setKomisyonFilter(KomisyonFilter komisyonFilter) {  
		this.komisyonFilter = komisyonFilter;  
	}  
	@Override
	public BaseFilter getFilter() {
		return getKomisyonFilter();
	}
	
 
} // class 
