package tr.com.arf.toys.view.controller.form.egitim; 
  
 
import java.io.File;
import java.util.Locale;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.persistence.EntityManager;

import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.framework.utility.exception.data.DBException;
import tr.com.arf.toys.db.filter.egitim.EgitimtanimFilter;
import tr.com.arf.toys.db.model.egitim.EgitimDegerlendirici;
import tr.com.arf.toys.db.model.egitim.EgitimLisans;
import tr.com.arf.toys.db.model.egitim.Egitimdosya;
import tr.com.arf.toys.db.model.egitim.Egitimtanim;
import tr.com.arf.toys.db.model.egitim.EgitmenEgitimtanim;
import tr.com.arf.toys.service.application.ApplicationDescriptor;
import tr.com.arf.toys.service.application.ManagedBeanLocator;
import tr.com.arf.toys.view.controller.form._base.ToysBaseFilteredController;
  
  
@ManagedBean 
@ViewScoped 
public class EgitimtanimController extends ToysBaseFilteredController {  
  
  private static final long serialVersionUID = 6531152165884980748L; 
  
	private Egitimtanim egitimtanim;  
	private EgitimtanimFilter egitimtanimFilter = new EgitimtanimFilter();  
  
	private Egitimtanim egitimtanimDetay;
	
	private EgitimLisans egitimLisans;
	private EgitmenEgitimtanim egitmenEgitimtanim;
	private EgitimDegerlendirici egitimDegerlendirici;
	private Egitimdosya egitimdosya;
	
	private String selectedTab;
	private String detailEditType;
	
	private EgitimLisansController egitimLisansController = new EgitimLisansController(new Egitimtanim());
	private EgitmenEgitimtanimController egitmenEgitimtanimController = new EgitmenEgitimtanimController(new Egitimtanim());
	private EgitimDegerlendiriciController egitimDegerlendiriciController=new EgitimDegerlendiriciController(new Egitimtanim());
	private EgitimdosyaController egitimdosyaController=new EgitimdosyaController(new Egitimtanim());
	
	private String oldValueForEgitimLisans;
	private String oldValueForEgitmenEgitimtanim;
	private String oldValueForEgitimDegerlendirici;
	private String oldValueForEgitimdosya;
	
	private String filename;
	private String filepath;
	
	public EgitimtanimController() { 
		super(Egitimtanim.class);  
		setDefaultValues(false);  
		setOrderField("ad");  
		setLoggable(false);  
		setAutoCompleteSearchColumns(new String[]{"ad"}); 
		setSelectedTab("egitimtanim");
		setTable(null); 
		if (getMasterEntity() != null && getMasterEntity().getRID() != null) {
			this.egitimtanimDetay = getMasterEntity();
			createEgitimtanimDetay("egitimtanim");
		}
		queryAction(); 
	} 
	
	public void egitimtanim() {
		setSelectedTab("egitimtanim");
	}
	
	public Egitimtanim getMasterEntity() {
		if (getEgitimtanim() != null && getEgitimtanim().getRID() != null) {
			return getEgitimtanim();
		} else if (getObjectFromSessionFilter(getModelName()) != null) {
			return (Egitimtanim) getObjectFromSessionFilter(getModelName());
		} else {
			return null;
		}
	}
	
	public EgitimtanimController(Object object) {
		super(Egitimtanim.class);
		setLoggable(true);
	}
	
	public Egitimtanim getEgitimtanim() { 
		egitimtanim = (Egitimtanim) getEntity(); 
		return egitimtanim; 
	} 
	
	public void setEgitimtanim(Egitimtanim egitimtanim) { 
		this.egitimtanim = egitimtanim; 
	} 
	
	public EgitimtanimFilter getEgitimtanimFilter() { 
		return egitimtanimFilter; 
	} 
	 
	public void setEgitimtanimFilter(EgitimtanimFilter egitimtanimFilter) {  
		this.egitimtanimFilter = egitimtanimFilter;  
	}
	 
	public Egitimtanim getEgitimtanimDetay() {
		return egitimtanimDetay;
	}

	public void setEgitimtanimDetay(Egitimtanim egitimtanimDetay) {
		this.egitimtanimDetay = egitimtanimDetay;
	}

	public String getSelectedTab() {
		return selectedTab;
	}

	public void setSelectedTab(String selectedTab) {
		this.selectedTab = selectedTab;
	}

	public String getDetailEditType() {
		return detailEditType;
	}

	public void setDetailEditType(String detailEditType) {
		this.detailEditType = detailEditType;
	}

	public EgitimLisansController getEgitimLisansController() {
		return egitimLisansController;
	}

	public void setEgitimLisansController(
			EgitimLisansController egitimLisansController) {
		this.egitimLisansController = egitimLisansController;
	}

	public EgitmenEgitimtanimController getEgitmenEgitimtanimController() {
		return egitmenEgitimtanimController;
	}

	public void setEgitmenEgitimtanimController(
			EgitmenEgitimtanimController egitmenEgitimtanimController) {
		this.egitmenEgitimtanimController = egitmenEgitimtanimController;
	}

	public EgitimDegerlendiriciController getEgitimDegerlendiriciController() {
		return egitimDegerlendiriciController;
	}

	public void setEgitimDegerlendiriciController(
			EgitimDegerlendiriciController egitimDegerlendiriciController) {
		this.egitimDegerlendiriciController = egitimDegerlendiriciController;
	}

	public EgitimdosyaController getEgitimdosyaController() {
		return egitimdosyaController;
	}

	public void setEgitimdosyaController(EgitimdosyaController egitimdosyaController) {
		this.egitimdosyaController = egitimdosyaController;
	}

	public EgitimLisans getEgitimLisans() {
		return egitimLisans;
	}


	public void setEgitimLisans(EgitimLisans egitimLisans) {
		this.egitimLisans = egitimLisans;
	}

	public EgitmenEgitimtanim getEgitmenEgitimtanim() {
		return egitmenEgitimtanim;
	}

	public void setEgitmenEgitimtanim(EgitmenEgitimtanim egitmenEgitimtanim) {
		this.egitmenEgitimtanim = egitmenEgitimtanim;
	}

	public EgitimDegerlendirici getEgitimDegerlendirici() {
		return egitimDegerlendirici;
	}

	public void setEgitimDegerlendirici(EgitimDegerlendirici egitimDegerlendirici) {
		this.egitimDegerlendirici = egitimDegerlendirici;
	}

	public String getOldValueForEgitimDegerlendirici() {
		return oldValueForEgitimDegerlendirici;
	}

	public void setOldValueForEgitimDegerlendirici(
			String oldValueForEgitimDegerlendirici) {
		this.oldValueForEgitimDegerlendirici = oldValueForEgitimDegerlendirici;
	}

	public Egitimdosya getEgitimdosya() {
		return egitimdosya;
	}

	public void setEgitimdosya(Egitimdosya egitimdosya) {
		this.egitimdosya = egitimdosya;
	}

	public String getOldValueForEgitimLisans() {
		return oldValueForEgitimLisans;
	}

	public void setOldValueForEgitimLisans(String oldValueForEgitimLisans) {
		this.oldValueForEgitimLisans = oldValueForEgitimLisans;
	}

	public String getOldValueForEgitmenEgitimtanim() {
		return oldValueForEgitmenEgitimtanim;
	}

	public void setOldValueForEgitmenEgitimtanim(
			String oldValueForEgitmenEgitimtanim) {
		this.oldValueForEgitmenEgitimtanim = oldValueForEgitmenEgitimtanim;
	}

	public String getOldValueForEgitimdosya() {
		return oldValueForEgitimdosya;
	}

	public void setOldValueForEgitimdosya(String oldValueForEgitimdosya) {
		this.oldValueForEgitimdosya = oldValueForEgitimdosya;
	}

	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

	public String getFilepath() {
		return filepath;
	}

	public void setFilepath(String filepath) {
		this.filepath = filepath;
	}

	@Override  
	public BaseFilter getFilter() {  
		return getEgitimtanimFilter();  
	}  
	public String egitimLisans() {	
		if (!setSelected()) {
			return "";
		}	
		getSessionUser().setLegalAccess(1);	
		putObjectToSessionFilter(ApplicationDescriptor._EGITIMTANIM_MODEL, getEgitimtanim());
		return ManagedBeanLocator.locateMenuController().gotoPage("menu_EgitimLisans"); 
	} 
	
	public String egitmenEgitimtanim() {	
		if (!setSelected()) {
			return "";
		}	
		getSessionUser().setLegalAccess(1);	
		putObjectToSessionFilter(ApplicationDescriptor._EGITIMTANIM_MODEL, getEgitimtanim());
		return ManagedBeanLocator.locateMenuController().gotoPage("menu_EgitmenEgitimTanim"); 
	} 
	
	public String egitimDegerlendirici() {	
		if (!setSelected()) {
			return "";
		}	
		getSessionUser().setLegalAccess(1);	
		putObjectToSessionFilter(ApplicationDescriptor._EGITIMTANIM_MODEL, getEgitimtanim());
		return ManagedBeanLocator.locateMenuController().gotoPage("menu_EgitimDegerlendirici"); 
	} 
	
	public String egitimUcreti() {	
		if (!setSelected()) {
			return "";
		}	
		getSessionUser().setLegalAccess(1);	
		putObjectToSessionFilter(ApplicationDescriptor._EGITIMTANIM_MODEL, getEgitimtanim());
		return ManagedBeanLocator.locateMenuController().gotoPage("menu_EgitimUcreti"); 
	}
	
	public String egitimdosya() {
		if (!setSelected()) {
			return "";
		}
		getSessionUser().setLegalAccess(1);
		putObjectToSessionFilter(ApplicationDescriptor._EGITIMTANIM_MODEL, getEgitimtanim());
		return ManagedBeanLocator.locateMenuController().gotoPage("menu_Egitimdosya");
	}
	
	public void createEgitimtanimDetay(String tabSelected) {
		setSelectedTab(tabSelected);
		getSessionUser().setLegalAccess(1);
		setDetailEditType(null);
		if (tabSelected.equalsIgnoreCase("egitimlisans")) {
			egitimLisansGuncelle(getEgitimtanimDetay());
		} else if (tabSelected.equalsIgnoreCase("egitmenegitimtanim")) {
			egitmenEgitimTanimGuncelle(getEgitimtanimDetay());
		} else if (tabSelected.equalsIgnoreCase("egitimdegerlendirici")) {
			egitimDegerlendiriciGuncelle(getEgitimtanimDetay());
		} else if (tabSelected.equalsIgnoreCase("egitimdosya")) {
			egitimDosyaGuncelle(getEgitimtanimDetay());
		}
	}
	
	private void egitimLisansGuncelle(Egitimtanim egitimTanimRef) {
		this.egitimLisansController.setTable(null);
		this.egitimLisansController.getEgitimLisansFilter().setEgitimtanimRef(egitimTanimRef);;
		this.egitimLisansController.getEgitimLisansFilter().createQueryCriterias();
		this.egitimLisansController.queryAction();
	}
	
	private void egitmenEgitimTanimGuncelle(Egitimtanim egitimTanimRef) {
		this.egitmenEgitimtanimController.setTable(null);
		this.egitmenEgitimtanimController.getEgitmenEgitimtanimFilter().setEgitimtanimRef(egitimTanimRef);;
		this.egitmenEgitimtanimController.getEgitmenEgitimtanimFilter().createQueryCriterias();
		this.egitmenEgitimtanimController.queryAction();
	}
	
	private void egitimDegerlendiriciGuncelle(Egitimtanim egitimTanimRef) {
		this.egitimDegerlendiriciController.setTable(null);
		this.egitimDegerlendiriciController.getEgitimDegerlendiriciFilter().setEgitimtanimRef(egitimTanimRef);;
		this.egitimDegerlendiriciController.getEgitimDegerlendiriciFilter().createQueryCriterias();
		this.egitimDegerlendiriciController.queryAction();
	}
	
	private void egitimDosyaGuncelle(Egitimtanim egitimTanimRef) {
		this.egitimdosyaController.setTable(null);
		this.egitimdosyaController.getEgitimdosyaFilter().setEgitimtanimRef(egitimTanimRef);;
		this.egitimdosyaController.getEgitimdosyaFilter().createQueryCriterias();
		this.egitimdosyaController.queryAction();
	}
	
	public void newDetail(String detailType) {
		detailType = detailType.toLowerCase(Locale.ENGLISH);
		this.detailEditType = detailType;
		RequestContext context = RequestContext.getCurrentInstance();
		context.update("dialogUpdateBox");
		if (detailType.equalsIgnoreCase("egitimtanim")) {
			EntityManager entityManager = getDBOperator().getEntityManagerFactory().createEntityManager();
			entityManager.detach(getEgitimtanimDetay());
			entityManager.persist(getEgitimtanimDetay());
			entityManager.close();
		} else if (detailType.equalsIgnoreCase("egitimlisans")) {
			this.egitimLisans = new EgitimLisans();
			this.oldValueForEgitimLisans = "";
		} else if (detailType.equalsIgnoreCase("egitmenegitimtanim")) {
			this.egitmenEgitimtanim = new EgitmenEgitimtanim();
			this.oldValueForEgitmenEgitimtanim = "";
		} else if (detailType.equalsIgnoreCase("egitimdegerlendirici")) {
			this.egitimDegerlendirici = new EgitimDegerlendirici();
			this.oldValueForEgitimDegerlendirici = "";
		} else if (detailType.equalsIgnoreCase("egitimdosya")) {
			this.egitimdosya = new Egitimdosya();
			this.oldValueForEgitimdosya = "";
		} 
	}

	public void updateDetail(String detailType, Object entity) {
		detailType = detailType.toLowerCase(Locale.ENGLISH);
		this.detailEditType = detailType;
		RequestContext context = RequestContext.getCurrentInstance();
		context.update("dialogUpdateBox");

		if (detailType.equalsIgnoreCase("egitimlisans")) {
			this.egitimLisans = (EgitimLisans) entity;
			this.oldValueForEgitimLisans = this.egitimLisans.getValue();
		} else if (detailType.equalsIgnoreCase("egitmenegitimtanim")) {
			this.egitmenEgitimtanim = (EgitmenEgitimtanim) entity;
			this.oldValueForEgitmenEgitimtanim = this.egitmenEgitimtanim.getValue();
		} else if (detailType.equalsIgnoreCase("egitimdegerlendirici")) {
			this.egitimDegerlendirici = (EgitimDegerlendirici) entity;
			this.oldValueForEgitimDegerlendirici = this.egitimDegerlendirici.getValue();
		} else if (detailType.equalsIgnoreCase("egitimdosya")) {
			this.egitimdosya = (Egitimdosya) entity;
			this.oldValueForEgitimdosya = this.egitimdosya.getValue();
		} 
	}
	
	public void deleteDetail(String detailType, Object entity) throws DBException {
		this.detailEditType = null;
		detailType = detailType.toLowerCase(Locale.ENGLISH);
		if (detailType.equalsIgnoreCase("egitimlisans")) {
			this.egitimLisans = (EgitimLisans) entity;
			super.justDelete(this.egitimLisans, this.egitimLisansController.isLoggable());
			this.egitimLisansController.queryAction();
		} else if (detailType.equalsIgnoreCase("egitmenegitimtanim")) {
			this.egitmenEgitimtanim = (EgitmenEgitimtanim) entity;
			super.justDelete(this.egitmenEgitimtanim, this.egitmenEgitimtanimController.isLoggable());
			this.egitmenEgitimtanimController.queryAction();
		} else if (detailType.equalsIgnoreCase("egitimdegerlendirici")) {
			this.egitimDegerlendirici = (EgitimDegerlendirici) entity;
			super.justDelete(this.egitimDegerlendirici, this.egitimDegerlendiriciController.isLoggable());
			this.egitimDegerlendiriciController.queryAction();
		} else if (detailType.equalsIgnoreCase("egitimdosya")) {
			this.egitimdosya = (Egitimdosya) entity;
			super.justDelete(this.egitimdosya, this.egitimdosyaController.isLoggable());
			this.egitimdosyaController.queryAction();
		}
	}
	
	private void updateDialogAndForm(RequestContext context) {
		context.update("dialogUpdateBox");
		context.update("egitimtanimForm");
	}
	public void saveDetails() throws Exception {
		RequestContext context = RequestContext.getCurrentInstance();
		if (this.detailEditType.equalsIgnoreCase("egitimlisans")) {
			this.egitimLisans.setEgitimtanimRef(getEgitimtanimDetay());
			super.justSave(this.egitimLisans, this.egitimLisansController.isLoggable(), this.oldValueForEgitimLisans);
			this.egitimLisans = new EgitimLisans();
			egitimLisansGuncelle(getEgitimtanimDetay());
			this.egitimLisansController.queryAction();
			updateDialogAndForm(context);
		} else if (this.detailEditType.equalsIgnoreCase("egitmenegitimtanim")) {
			this.egitmenEgitimtanim.setEgitimtanimRef(getEgitimtanimDetay());
			super.justSave(this.egitmenEgitimtanim, this.egitmenEgitimtanimController.isLoggable(), this.oldValueForEgitmenEgitimtanim);
			this.egitmenEgitimtanim = new EgitmenEgitimtanim();
			this.egitmenEgitimtanimController.queryAction();
			updateDialogAndForm(context);
		} else if (this.detailEditType.equalsIgnoreCase("egitimdegerlendirici")) {
			this.egitimDegerlendirici.setEgitimtanimRef(getEgitimtanimDetay());
			super.justSave(this.egitimDegerlendirici, this.egitimDegerlendiriciController.isLoggable(), this.oldValueForEgitimDegerlendirici);
			this.egitimDegerlendirici = new EgitimDegerlendirici();
			this.egitimDegerlendiriciController.queryAction();
			updateDialogAndForm(context);
		} else if (this.detailEditType.equalsIgnoreCase("egitimdosya")) {
			this.egitimdosya.setEgitimtanimRef(getEgitimtanimDetay());
			this.egitimdosya.setPath(getFilepath());
			super.justSave(this.egitimdosya, this.egitimdosyaController.isLoggable(), this.oldValueForEgitimdosya);
			this.egitimdosya = new Egitimdosya();
			this.egitimdosyaController.queryAction();
			updateDialogAndForm(context);
		}
		context.update("egitimtanimPanel");
		this.detailEditType = null;
	}
	
	public String getHeightOfEditDialog() {
		if (this.detailEditType == null) {
			return "400";
		} else if (this.detailEditType.equalsIgnoreCase("egitimlisans")) {
			return "400";
		} else if (this.detailEditType.equalsIgnoreCase("egitmenegitimtanim")) {
			return "400";
		} else if (this.detailEditType.equalsIgnoreCase("egitimdegerlendirici")) {
			return "400";
		} else if (this.detailEditType.equalsIgnoreCase("egitimdosya")) {
			return "400";
		} else {
			return "400";
		}
	}


	@Override
	public String detailPage(Long selectedRID) {
		if (selectedRID != null && selectedRID > 0) {
			setSelectedRID(selectedRID + "");
		} else if (getSelectedRID() == null) {
			return "";
		}
		ManagedBeanLocator.locateSessionUser().addToSessionFilters(getModelName(), getEgitimtanim());
		return ManagedBeanLocator.locateMenuController().gotoPage("menu_" + getModelName() + "Detay");
	}

	public String egitimtanimListesineDon() {
		ManagedBeanLocator.locateSessionUser().clearSessionFilters();
		return ManagedBeanLocator.locateMenuController().gotoPage("menu_EgitimTanim" );
	}
	
	@Override
	public void fileUploadListener(FileUploadEvent event) throws Exception {
		File creationFile = super.createFile(event.getFile(), Egitimdosya.class.getSimpleName());
		getEntity().setPath(creationFile.getName());
        String path1 = creationFile.getAbsolutePath();
//        setFilepath(path1.substring(41));
        setFilepath(path1);
	}
	
	
	

	
}
