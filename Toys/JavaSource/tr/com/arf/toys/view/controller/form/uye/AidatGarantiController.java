package tr.com.arf.toys.view.controller.form.uye;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.MathContext;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;
import java.util.Locale;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;
import javax.faces.event.AjaxBehaviorEvent;
import javax.net.ssl.HttpsURLConnection;
import javax.servlet.http.HttpServletRequest;

import tr.com.arf.framework.db._management.DBOperator;
import tr.com.arf.framework.db.enumerated._common.EvetHayir;
import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.framework.utility.exception.data.DBException;
import tr.com.arf.framework.utility.tool.DateUtil;
import tr.com.arf.framework.utility.tool.StringUtil;
import tr.com.arf.toys.db.enumerated.finans.OdemeTuru;
import tr.com.arf.toys.db.enumerated.iletisim.NetAdresTuru;
import tr.com.arf.toys.db.enumerated.uye.OdemeSekli;
import tr.com.arf.toys.db.enumerated.uye.SiparisDurum;
import tr.com.arf.toys.db.filter.finans.OdemeFilter;
import tr.com.arf.toys.db.model.finans.Odeme;
import tr.com.arf.toys.db.model.iletisim.Internetadres;
import tr.com.arf.toys.db.model.kisi.Kisi;
import tr.com.arf.toys.db.model.system.Birim;
import tr.com.arf.toys.db.model.system.SistemParametre;
import tr.com.arf.toys.db.model.uye.Uye;
import tr.com.arf.toys.db.model.uye.UyeSiparis;
import tr.com.arf.toys.db.model.uye.Uyeaidat;
import tr.com.arf.toys.db.nonEntityModel.Siparis;
import tr.com.arf.toys.service.application.ApplicationDescriptor;
import tr.com.arf.toys.service.application.ManagedBeanLocator;
import tr.com.arf.toys.utility.application.ApplicationConstant;
import tr.com.arf.toys.view.controller._common.SessionUser;
import tr.com.arf.toys.view.controller.form._base.ToysBaseFilteredController;

@ManagedBean
@ViewScoped
public class AidatGarantiController extends ToysBaseFilteredController {

	private static final long serialVersionUID = 1L;

	private Odeme odeme;
	private OdemeFilter odemeFilter = new OdemeFilter();
	private String hash = "";
	private UyeSiparis siparis;
	private int step = 1;
	private Siparis siparisSonuc;
	public boolean tahsilatBasarili = false;
    private String bankaCevap;

	private String txntimestampalan;

	private Uyeaidat uyeAidat;

	public AidatGarantiController() {
		super(Odeme.class);
		setDefaultValues(false);
		setTable(null);
		if (getMasterEntity() != null) {
			setMasterObjectName(ApplicationDescriptor._UYE_MODEL);
			setMasterOutcome(ApplicationDescriptor._UYE_MODEL);
			getOdemeFilter().setKisiRef(getMasterEntity().getKisiRef());
			getOdemeFilter().createQueryCriterias();
		}
		queryAction();
	}

	@SuppressWarnings("unchecked")
	@PostConstruct
	@Override
	public void init() {
		super.init();
		super.insertOnly();
		setStep(1);
		this.siparisSonuc = new Siparis();
		List<Uyeaidat> uyeAidatList = getDBOperator().load(Uyeaidat.class.getSimpleName(), "o.uyeRef.kisiRef.rID = " + getSessionUser().getKullanici().getKisiRef().getRID(), "o.yil DESC");
		if (uyeAidatList != null && uyeAidatList.size() > 0) {
			setUyeAidat(uyeAidatList.get(0));
			getOdeme().setMiktar(getUyeAidat().getMiktar().multiply(new BigDecimal(1), mc));
		} else {
			putObjectToSessionFilter("detailEditType", "uyeaidatbilgisi");
			getSessionUser().setMessage("Aidat kaydınız bulunamadı!");
			super.redirectPage("/faces/_page/_main/uyedashboard.xhtml");
		}

	}

	public AidatGarantiController(Object object) {
		super(Odeme.class);
	}

	public Uye getMasterEntity() {
		return (Uye) getObjectFromSessionFilter(ApplicationDescriptor._UYE_MODEL);
	}

	public Odeme getOdeme() {
		odeme = (Odeme) getEntity();
		return odeme;
	}

	public void setOdeme(Odeme odeme) {
		this.odeme = odeme;
	}

	public OdemeFilter getOdemeFilter() {
		return odemeFilter;
	}

	public void setOdemeFilter(OdemeFilter odemeFilter) {
		this.odemeFilter = odemeFilter;
	}

	@Override
	public BaseFilter getFilter() {
		return getOdemeFilter();
	}

	@Override
	public void insert() {
		super.insert();
		getOdeme().setKisiRef(getMasterEntity().getKisiRef());
	}

	public String getClientID() {
		return ApplicationConstant._clientId;
	}

	public String getOkURL() {
		return ManagedBeanLocator.locateApplicationController().getGarantiBankasiOKURL();
	}

	public String getFailURL() {
		return ManagedBeanLocator.locateApplicationController().getGarantiBankasiFAILURL();
	}

	public String getNowdate() {
		return new java.util.Date().toString();
	}

	public String getHash() {
		return hash;
	}

	public void setHash(String hash) {
		this.hash = hash;
	}

	public UyeSiparis getSiparis() {
		return siparis;
	}

	public void setSiparis(UyeSiparis siparis) {
		this.siparis = siparis;
	}

	public int getStep() {
		return step;
	}

	public void setStep(int step) {
		this.step = step;
	}

	public Siparis getSiparisSonuc() {
		return siparisSonuc;
	}

	public void setSiparisSonuc(Siparis siparisSonuc) {
		this.siparisSonuc = siparisSonuc;
	}

	public boolean isTahsilatBasarili() {
		return tahsilatBasarili;
	}

	public void setTahsilatBasarili(boolean tahsilatBasarili) {
		this.tahsilatBasarili = tahsilatBasarili;
	}

	public String getTerminalID() {
		return ManagedBeanLocator.locateApplicationController().getTerminalID();
	}

	public String getMerchantID() {
		return ManagedBeanLocator.locateApplicationController().getMerchantID();
	}

	public String getStoreKey() {
		return ManagedBeanLocator.locateApplicationController().getStoreKey();
	}

	public String getPassword() {
		return ManagedBeanLocator.locateApplicationController().getPassword();
	}

	public String getTerminalUserID() {
		return ManagedBeanLocator.locateApplicationController().getTerminalUserID();
	}

    public String getGarantiMode() {
        return ManagedBeanLocator.locateApplicationController().getGarantiMode();
    }

	public String getTxntimestampalan() {
		return txntimestampalan;
	}

	public void setTxntimestampalan(String txntimestampalan) {
		this.txntimestampalan = txntimestampalan;
	}

	public Uyeaidat getUyeAidat() {
		return uyeAidat;
	}

	public void setUyeAidat(Uyeaidat uyeAidat) {
		this.uyeAidat = uyeAidat;
	}

	public String getSHA1(String hashstr) throws NoSuchAlgorithmException, UnsupportedEncodingException {
		MessageDigest md;
		md = MessageDigest.getInstance("SHA-1");
		byte[] sha1hash = new byte[40];
		md.update(hashstr.getBytes("iso-8859-9"), 0, hashstr.length());
		sha1hash = md.digest();
		return convertToHex(sha1hash);
	}

	public String convertToHex(byte[] data) {
		StringBuffer buf = new StringBuffer();
		for (int i = 0; i < data.length; i++) {
			int halfbyte = data[i] >>> 4 & 0x0F;
			int two_halfs = 0;
			do {
				if (0 <= halfbyte && halfbyte <= 9) {
					buf.append((char) ('0' + halfbyte));
				} else {
					buf.append((char) ('a' + (halfbyte - 10)));
				}
				halfbyte = data[i] & 0x0F;
			} while (two_halfs++ < 1);
		}
		return buf.toString();
	}

	// public void testSHA() {
	// String test = "dene000111995";
	// System.out.println(generateHash(test).toUpperCase());
	//
	// test = "hash";
	// System.out.println(generateHash(test)); // k6U39nnXeY7mNMQdqpP/fSHWdp8=
	//
	// }

	public String generateHash(String input) {
		try {
			MessageDigest md = MessageDigest.getInstance("SHA1");
			md.reset();
			byte[] buffer = input.getBytes();
			md.update(buffer);
			byte[] digest = md.digest();
			String hexStr = "";
			for (int i = 0; i < digest.length; i++) {
				hexStr += Integer.toString((digest[i] & 0xff) + 0x100, 16).substring(1);
			}
			return hexStr;
		} catch (NoSuchAlgorithmException e) {
			// setError(ss,"SHA1",ss.translate("SHA1 Hashing Error 3DPay"));//Sorr
			logger.error("SHA1 Hashing Error 3DPay " + e);
			logger.error("EXCEPTION at SHA1 Hashing Error 3DPay  " + "\n" + e.toString());
			return null;
		}
	}

	public String getFormattedAmount(double amount) {
		DecimalFormat df = new DecimalFormat(".00");
		String formattedAmount = df.format(amount).replaceAll(",", "").replaceAll("\\.", "");
		return formattedAmount;
	}

	public void createHash() throws Exception {
		this.siparis = new UyeSiparis();
		setTxntimestampalan(DateUtil.dateToDMYHMS(new Date()).replace(" ", ""));
		siparis.setUyeRef(getMasterEntity());
		Object maxNum = getDBOperator().recordCountByNativeQuery("SELECT COALESCE(MAX(CAST(SIPARISNO AS BIGINT)),0) FROM oltp." + ApplicationDescriptor._UYESIPARIS_MODEL + "" + " WHERE uyeRef= " + getMasterEntity().getRID());
		BigInteger max = (BigInteger) maxNum;
		max = max.add(new BigInteger("1"));
		String sipNo = StringUtil.fillStringLeft(max + "", "0", 13);
		while (true) {
			if (getDBOperator().recordCount(UyeSiparis.class.getSimpleName(), "o.siparisno = '" + sipNo + "'") == 0) {
				break;
			} else {
				max = max.add(new BigInteger("1"));
				sipNo = StringUtil.fillStringLeft(max + "", "0", 13);
			}
		}
        siparis.setTarih(new Date());
		siparis.setSiparisno(sipNo);
		siparis.setDurum(SiparisDurum._YANITBEKLIYOR);
		siparis.setTutar(getOdeme().getMiktar());
		getDBOperator().insert(siparis);
		String amount = getFormattedAmount(getOdeme().getMiktar().doubleValue());
		String securityCode = getPassword() + StringUtil.fillStringLeft(getTerminalID(), "0", 9);// 9 karaktere tamamlanacak
		securityCode = generateHash(securityCode).toUpperCase();
		String hashData = getTerminalID() + getSiparis().getSiparisno() + amount + getOkURL() + getFailURL() + "sales" + "" + getStoreKey() + securityCode;
		String hash = generateHash(hashData).toUpperCase();
		try {
			siparis.setGarantiSecurityCode(securityCode);
			setHash(hash);
			getDBOperator().update(siparis);
			step++;
		} catch (Exception e) {
			logYaz("Exception @" + getModelName() + "Controller :", e);
			setHash("error");
		}
	}

	@SuppressWarnings({ "rawtypes", "unused", "unchecked" })
	public void parseGarantiReturnParams() {
		HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
		setTahsilatBasarili(false);
		Gate3DEngineCallBack gate3d = new Gate3DEngineCallBack();
		if (!gate3d.validateHash(request)) {
			createGenericMessage("Hash kodu hatalı!", FacesMessage.SEVERITY_ERROR);
			return;
		}

		Enumeration enu = request.getParameterNames();
		String siparisNo = null;
        String mdstatus = null;
		BigDecimal tutar = null;
		boolean islemBasarili = false;
		String hostmsg = "";
		// System.out.println("Parsing parameters for :" + getSiparis().getSiparisno());
		String islemSonucMesaji = "";
		while (enu.hasMoreElements()) {
			String param = (String) enu.nextElement();
			String val = request.getParameter(param);
			boolean ok = true;
			if (param.equalsIgnoreCase("orderid")) {
				siparisNo = val;
			} else if (param.equalsIgnoreCase("txnamount")) {
				tutar = new BigDecimal(val);
			}

			if (param.equalsIgnoreCase("hostmsg")) {
				if (val != null && val.trim().length() > 0) {
					hostmsg += " (" + val + ")";
				}
			}

			if (param.equalsIgnoreCase("mdstatus")) {
                mdstatus = val;
				if (val.equals("1")) {
                    islemSonucMesaji = "Tam Doğrulama";
                    islemBasarili = true;
				} else if (val.equals("2")) {
					islemSonucMesaji = "Kart Sahibi veya bankası sisteme kayıtlı değil";
                    islemBasarili = true;
				} else if (val.equals("3")) {
                    islemSonucMesaji = "Kartın bankası sisteme kayıtlı değil...";
                    islemBasarili = true;
                } else if (val.equals("4")) {
                    islemSonucMesaji = "Doğrulama denemesi, kart sahibi sisteme daha sonra kayıt olmayı seçmis";
                    islemBasarili = true;
                } else if (val.equals("5")) {
					islemSonucMesaji = "Doğrulama yapılamıyor.";
				} else if (val.equals("6")) {
					islemSonucMesaji = "3-D Secure Hatası";
				} else if (val.equals("7")) {
					islemSonucMesaji = "Sistem Hatası";
				} else if (val.equals("8")) {
					islemSonucMesaji = "Bilinmeyen Kart No";
				} else if (val.equals("0")) {
					islemSonucMesaji = "3-D Secure imzası geçersiz. 3-D doğrulamsı sırasında girdiğiniz bilgileri kontrol ediniz.";
				}
                bankaCevap = islemSonucMesaji;
				logYaz("ODEME DURUMU : " + islemSonucMesaji);
			}
		}
		// System.out.println("Parametreler alindi. Simdi siparis guncellenecek...");
		logger.debug("siparisNo : " + siparisNo);
		List<UyeSiparis> uyeSiparisListesi = getDBOperator().load(UyeSiparis.class.getSimpleName(), "o.siparisno='" + siparisNo + "'", "o.rID");
		if (isEmpty(uyeSiparisListesi)) {
			createGenericMessage("İlgili sipariş bulunamadı!", FacesMessage.SEVERITY_ERROR);
			return;
		} else {
            UyeSiparis uyeSiparis = uyeSiparisListesi.get(0);
            uyeSiparis.setMdstatus(mdstatus);
            siparisSonuc.setMdstatus(mdstatus);
            if (islemBasarili) {
                paraCek(request);
				tutar = tutar.divide(new BigDecimal(100), MathContext.DECIMAL32);
				// System.out.println("Tutar :" + tutar);
				// System.out.println("Tutar :" + uyeSiparis.getTutar());
				if (tutar.doubleValue() == uyeSiparis.getTutar().doubleValue() && uyeSiparis.getDurum() == SiparisDurum._YANITBEKLIYOR) {
                    logger.debug("tutar kontrol edildi. siparis onaylanacak.");
					uyeSiparis.setDurum(SiparisDurum._ONAYLANDI);
					this.siparisSonuc.setAmount(tutar.toString());
					Odeme odm = new Odeme();
					odm.setKisiRef(uyeSiparis.getUyeRef().getKisiRef());
					odm.setMiktar(uyeSiparis.getTutar());
					odm.setOdemeTuru(OdemeTuru._AIDAT);
					odm.setTarih(new Date());
					odm.setOdemeSekli(OdemeSekli._ONLINE);
					odm.setBirimRef((Birim) getDBOperator().load(Birim.class.getSimpleName(), "o.ustRef IS NULL", "o.erisimKodu ASC").get(0));
					setTahsilatBasarili(true);
					try {
						ManagedBeanLocator.locateSessionController().uyeOdeme(odm, uyeSiparis);
						createGenericMessage("Ödeme başarıyla kaydedildi.", FacesMessage.SEVERITY_INFO);
					} catch (DBException exc) {
						createGenericMessage("Ödeme kaydedilirken bir hata meydana geldi!", FacesMessage.SEVERITY_ERROR);
						logYaz("Exception @AidatGarantiController :", exc);
					}
				}
			} else {
				uyeSiparis.setDurum(SiparisDurum._ONAYLANMADI);
				try {
					getDBOperator().update(uyeSiparis);
					createGenericMessage("Ödeme banka tarafından reddedildi." + hostmsg, FacesMessage.SEVERITY_INFO);
				} catch (DBException e) {
					logYaz("Exception @AidatGarantiController :", e);
				}

			}
		}

	}

    public void paraCek(HttpServletRequest request) {
        try {
            String strMode = request.getParameter("mode");
            String strVersion = request.getParameter("apiversion");
            String strTerminalID = request.getParameter("clientid");
            String _strTerminalID = "0" + request.getParameter("clientid");
            String strProvisionPassword = getPassword(); // 'Terminal UserID sifresi
            String strProvUserID = request.getParameter("terminalprovuserid");
            String strUserID = request.getParameter("terminaluserid");
            String strMerchantID = request.getParameter("terminalmerchantid"); // 'Üye syeri Numarası
            String strIPAddress = request.getParameter("customeripaddress");
            String strEmailAddress = request.getParameter("customeremailaddress");
            String strOrderID = request.getParameter("orderid");
            String strNumber = ""; //'Kart bilgilerinin bos gitmesi gerekiyor
            String strExpireDate = ""; //'Kart bilgilerinin bos gitmesi gerekiyor
            String strCVV2 = ""; //'Kart bilgilerinin bos gitmesi gerekiyor
            String strAmount = request.getParameter("txnamount"); // ' slem Tutarı
            String strCurrencyCode = request.getParameter("txncurrencycode");
            String strCardholderPresentCode = "13"; //'3D Model islemde bu de er 13 olmalı
            String strType = request.getParameter("txntype");
            String strMotoInd = "N";
            String strAuthenticationCode = request.getParameter("cavv");
            String strSecurityLevel = request.getParameter("eci");
            String strTxnID = request.getParameter("xid");
            String strMD = request.getParameter("md");
            String SecurityData = getSHA1(strProvisionPassword + _strTerminalID).toUpperCase(Locale.ENGLISH);
            String HashData = getSHA1(strOrderID + strTerminalID + strAmount + SecurityData).toUpperCase(Locale.ENGLISH);
            //'Daha kısıtlı bilgileri HASH ediyoruz.
            String strHostAddress = ManagedBeanLocator.locateApplicationController().getStrHostAddress(); //"https://sanalposprov.garanti.com.tr/VPServlet"; // 'Provizyon için

            String strXML = "data=<?xml version=\"1.0\" encoding=\"ISO-8859-9\"?>" +
             "<GVPSRequest>" +
             "<Mode>" + strMode + "</Mode>" +
             "<Version>" + strVersion + "</Version>" +
             "<ChannelCode></ChannelCode>" +
             "<Terminal><ProvUserID>" + strProvUserID + "</ProvUserID><HashData>" + HashData +
            "</HashData><UserID>" + strUserID + "</UserID><ID>" + strTerminalID + "</ID><MerchantID>" + strMerchantID +
                    "</MerchantID></Terminal>" +
             "<Customer><IPAddress>" + strIPAddress + "</IPAddress><EmailAddress>" + strEmailAddress +
            "</EmailAddress></Customer>" +
             "<Card><Number></Number><ExpireDate></ExpireDate></Card>" +
             "<Order><OrderID>" + strOrderID +
            "</OrderID><GroupID></GroupID><Description></Description></Order>" +
             "<Transaction>" +
             "<Type>" + strType + "</Type><InstallmentCnt></InstallmentCnt><Amount>" + strAmount +
            "</Amount><CurrencyCode>" + strCurrencyCode + "</CurrencyCode><CardholderPresentCode>" + strCardholderPresentCode +
                    "</CardholderPresentCode><MotoInd>" + strMotoInd + "</MotoInd>" +
             "<Secure3D><AuthenticationCode>" + strAuthenticationCode +
            "</AuthenticationCode><SecurityLevel>" + strSecurityLevel + "</SecurityLevel><TxnID>" + strTxnID + "</TxnID><Md>" +
                    strMD + "</Md></Secure3D>" +
             "</Transaction>" +
             "</GVPSRequest>";

            URL url = new URL(strHostAddress);
            HttpsURLConnection connection = (HttpsURLConnection) url.openConnection();
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            byte[] bytes = strXML.getBytes("UTF8");
            connection.setRequestProperty("Content-Length", ""+bytes.length);
            connection.setDoOutput(true);
            connection.setDoInput(true);
            OutputStream outputStream = connection.getOutputStream();
            outputStream.write(bytes);
            outputStream.close();
            InputStream inputStream = connection.getInputStream();
            InputStreamReader irs = new InputStreamReader(inputStream);
            BufferedReader br = new BufferedReader(irs);
            String line = br.readLine();
            while (line != null) {
                logger.debug("line = " + line);
                line = br.readLine();
            }
        } catch (NoSuchAlgorithmException | IOException exc) {
            logger.error(exc.toString(), exc);
        }
    }

	public String returnUyeDashboard() {
		putObjectToSessionFilter("detailEditType", "uyeaidatbilgisi");
		return ApplicationDescriptor._UYEDASHBOARD_OUTCOME;
	}

	public String getGarantiPosUrl() {
		// return "https://sanalposprovtest.garanti.com.tr/servlet/gt3dengine";
		if (getDBOperator().recordCount(SistemParametre.class.getSimpleName()) > 0) {
			SistemParametre param = (SistemParametre) getDBOperator().load(SistemParametre.class.getSimpleName()).get(0);
			if (param.getGarantiaktif() == EvetHayir._EVET && param.getGarantiposadres() != null) {
				return param.getGarantiposadres();
			} else {
				logYaz("Garanti Pos Url hatası...");
				createGenericMessage("Hata oluştu!Lütfen ilgili birimi arayıp hatayı bildiriniz!", FacesMessage.SEVERITY_INFO);
			}
		} else {
			logYaz("Garanti Pos Url hatası...");
			createGenericMessage("Hata oluştu! Lütfen ilgili birimi arayıp hatayı bildiriniz!", FacesMessage.SEVERITY_INFO);
		}
		return null;
	}

	@Override
	public void handleChange(AjaxBehaviorEvent event) {
		try {
			HtmlSelectOneMenu menu = (HtmlSelectOneMenu) event.getComponent();
			if (menu.getValue() instanceof Integer && getUyeAidat() != null) {
				getOdeme().setMiktar(getUyeAidat().getMiktar().multiply(new BigDecimal(menu.getValue().toString()), mc));
			}
		} catch (Exception e) {
			logYaz("Error @handleChange :" + e.getMessage());
			logYaz("Exception @" + getModelName() + "Controller :", e);
		}
	}

	public List<String> getAylar() {
		List<String> aylar = new ArrayList<String>();
		for (int i = 1; i < 13; i++) {
			aylar.add(StringUtil.fillStringLeft(i + "", "0", 2));
		}
		return aylar;
	}

	public List<Integer> getYillar() {
		int baslangic = Calendar.getInstance().get(Calendar.YEAR) - 2000;
		return super.getIntegerArray(baslangic, baslangic + 20, 1);
	}

    public String getIPAdresi() {
        HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
        String ipAddress = request.getHeader("X-FORWARDED-FOR");
        if (ipAddress == null) {
            ipAddress = request.getRemoteAddr();
        }
        logger.debug("ip adres = " + ipAddress);
        return ipAddress;
    }

    public String getEposta() {
        try {
            SessionUser sessionUser = getSessionUser();
            Kisi kisiRef = sessionUser.getKullanici().getKisiRef();
            DBOperator operator = DBOperator.getInstance();
            if (operator.recordCount(Internetadres.class.getSimpleName(),
                    "o.netadresturu = " + NetAdresTuru._EPOSTA.getCode()
                            + " AND o.kisiRef.rID=" + kisiRef.getRID()
                            + " AND o.varsayilan=" + EvetHayir._EVET.getCode()) > 0) {
                Internetadres internetAdres = (Internetadres) operator.load(Internetadres.class.getSimpleName(),
                        "o.netadresturu = " + NetAdresTuru._EPOSTA.getCode()
                                + " AND o.kisiRef.rID=" + kisiRef.getRID()
                                + " AND o.varsayilan=" + EvetHayir._EVET.getCode()
                        , "o.rID DESC").get(0);
                String eposta = internetAdres.getNetadresmetni();
                logger.debug("eposta = " + eposta);
                return eposta;
            }
        } catch (DBException exc) {
            logger.error(exc.toString(), exc);
        }
        return "iletisim@bimo.org.tr";
    }

    public String getBankaCevap() {
        return bankaCevap;
    }

} // class
