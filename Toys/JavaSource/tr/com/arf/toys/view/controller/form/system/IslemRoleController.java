package tr.com.arf.toys.view.controller.form.system; 
  
 
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.persistence.EntityManager;
import javax.persistence.Query;

import tr.com.arf.framework.db._management.DBOperator;
import tr.com.arf.framework.db.enumerated._common.EvetHayir;
import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.toys.db.filter.system.IslemRoleFilter;
import tr.com.arf.toys.db.model.system.IslemKullanici;
import tr.com.arf.toys.db.model.system.IslemRole;
import tr.com.arf.toys.db.model.system.Role;
import tr.com.arf.toys.service.application.ApplicationDescriptor;
import tr.com.arf.toys.view.controller.form._base.ToysFilteredControllerNoPaging;
  
  
@ManagedBean 
@ViewScoped 
public class IslemRoleController extends ToysFilteredControllerNoPaging {  
  
  private static final long serialVersionUID = 6531152165884980748L; 
  
	private IslemRole islemRole;   
	private IslemRoleFilter islemRoleFilter = new IslemRoleFilter();  
	private Boolean kullaniciYetkileriniGuncelle = false;
  
	public IslemRoleController() { 
		super(IslemRole.class);  
		setDefaultValues(false);  
		setOrderField("roleRef.name");  
		setAutoCompleteSearchColumns(new String[]{"roleRef.name"}); 
		setTable(null); 
		setMasterObjectName(ApplicationDescriptor._ROLE_MODEL);
		setMasterOutcome(ApplicationDescriptor._ROLE_MODEL);
		getIslemRoleFilter().setRoleRef(getMasterEntity());
		getIslemRoleFilter().createQueryCriterias();    
		queryAction(); 
	} 
 
	public Role getMasterEntity(){
		if(getIslemRoleFilter().getRoleRef() != null){
			return getIslemRoleFilter().getRoleRef();
		} else {
			return (Role) getObjectFromSessionFilter(ApplicationDescriptor._ROLE_MODEL);
		}			
	} 	
	
	public IslemRole getIslemRole() { 
		islemRole = (IslemRole) getEntity(); 
		return islemRole; 
	} 
	
	public void setIslemRole(IslemRole islemRole) { 
		this.islemRole = islemRole; 
	} 
	
	public IslemRoleFilter getIslemRoleFilter() { 
		return islemRoleFilter; 
	} 
	 
	public void setIslemRoleFilter(IslemRoleFilter islemRoleFilter) {  
		this.islemRoleFilter = islemRoleFilter;  
	}  
	 
	@Override  
	public BaseFilter getFilter() {  
		return getIslemRoleFilter();  
	}   
	
	@Override
	public void resetFilter(){
		super.resetFilterOnly();
		if(getMasterEntity() != null){
			getIslemRoleFilter().setRoleRef(getMasterEntity()); 
		}
		queryAction();
	} 
 
	@Override	
	public void insert() {	
		super.insert();	
		getIslemRole().setRoleRef(getIslemRoleFilter().getRoleRef());	
	}	
	
	public void enableDisableSelected(String operationType){
		if(!setSelected()) {
			return;
		}
		DBOperator dbOperator = getDBOperator();
		EntityManager entityManager = dbOperator.getEntityManagerFactory().createEntityManager();
		entityManager.getTransaction().begin();
		try {
			
			Query query = entityManager
				.createQuery("UPDATE " + IslemRole.class.getSimpleName() + " o SET "
					+ " o.listPermission = :listPermission, " 
					+ " o.updatePermission = :updatePermission, "
					+ " o.deletePermission = :deletePermission " 
					+ " WHERE o.rID = :islemRole ");
			if(operationType.equalsIgnoreCase("enable")){
				query.setParameter("listPermission", EvetHayir._EVET); 
				query.setParameter("updatePermission", EvetHayir._EVET);
				query.setParameter("deletePermission", EvetHayir._EVET); 
			} else {
				query.setParameter("listPermission", EvetHayir._HAYIR);				 
				query.setParameter("updatePermission", EvetHayir._HAYIR);
				query.setParameter("deletePermission", EvetHayir._HAYIR);
			}	
			query.setParameter("islemRole", getIslemRole().getRID());
			query.executeUpdate(); 
			if(kullaniciYetkileriniGuncelle){
				Query query2 = entityManager
					.createQuery("UPDATE " + IslemKullanici.class.getSimpleName() + " o SET "
						+ " o.listPermission = :listPermission, " 
						+ " o.updatePermission = :updatePermission, "
						+ " o.deletePermission = :deletePermission " 
						+ " WHERE o.islemRef.rID =:islemRef AND o.kullaniciRoleRef.rID IN (SELECT m.rID From KullaniciRole m WHERE m.roleRef.rID = :roleRef ) ");
				if(operationType.equalsIgnoreCase("enable")){
					query2.setParameter("listPermission", EvetHayir._EVET); 
					query2.setParameter("updatePermission", EvetHayir._EVET);
					query2.setParameter("deletePermission", EvetHayir._EVET); 
				} else {
					query2.setParameter("listPermission", EvetHayir._HAYIR);				 
					query2.setParameter("updatePermission", EvetHayir._HAYIR);
					query2.setParameter("deletePermission", EvetHayir._HAYIR);
				}	
				query2.setParameter("islemRef", getIslemRole().getIslemRef().getRID());
				query2.setParameter("roleRef", getIslemRole().getRoleRef().getRID()); 
				query2.executeUpdate();  
			}
			entityManager.getTransaction().commit();  
		} catch(Exception e){
			logYaz("Error executing query2 :" + e.getMessage());
			logYaz("Exception @" + getModelName() + "Controller :", e);
			entityManager.getTransaction().rollback(); 
		} finally {
			entityManager.close(); 	
			setTable(null); 
			queryAction();
		} 
	}
	
	public void enableDisableAll(String operationType){
		if(!setSelected()) {
			return;
		}
		DBOperator dbOperator = getDBOperator();
		EntityManager entityManager = dbOperator.getEntityManagerFactory().createEntityManager();
		entityManager.getTransaction().begin();
		try {
			
			Query query = entityManager
				.createQuery("UPDATE " + IslemRole.class.getSimpleName() + " o SET "
					+ " o.listPermission = :listPermission, " 
					+ " o.updatePermission = :updatePermission, "
					+ " o.deletePermission = :deletePermission " 
					+ " WHERE o.roleRef.rID = :roleRef ");
			if(operationType.equalsIgnoreCase("enable")){
				query.setParameter("listPermission", EvetHayir._EVET); 
				query.setParameter("updatePermission", EvetHayir._EVET);
				query.setParameter("deletePermission", EvetHayir._EVET); 
			} else {
				query.setParameter("listPermission", EvetHayir._HAYIR);				 
				query.setParameter("updatePermission", EvetHayir._HAYIR);
				query.setParameter("deletePermission", EvetHayir._HAYIR);
			}	
			query.setParameter("roleRef", getIslemRole().getRoleRef().getRID());
			query.executeUpdate(); 
			if(kullaniciYetkileriniGuncelle){
				Query query2 = entityManager
					.createQuery("UPDATE " + IslemKullanici.class.getSimpleName() + " o SET "
						+ " o.listPermission = :listPermission, " 
						+ " o.updatePermission = :updatePermission, "
						+ " o.deletePermission = :deletePermission " 
						+ " WHERE o.kullaniciRoleRef.rID IN (SELECT m.rID From KullaniciRole m WHERE m.roleRef.rID = :roleRef ) ");
				if(operationType.equalsIgnoreCase("enable")){
					query2.setParameter("listPermission", EvetHayir._EVET); 
					query2.setParameter("updatePermission", EvetHayir._EVET);
					query2.setParameter("deletePermission", EvetHayir._EVET); 
				} else {
					query2.setParameter("listPermission", EvetHayir._HAYIR);				 
					query2.setParameter("updatePermission", EvetHayir._HAYIR);
					query2.setParameter("deletePermission", EvetHayir._HAYIR);
				}	
				query2.setParameter("roleRef", getIslemRole().getRoleRef().getRID()); 
				query2.executeUpdate();  
			}
			entityManager.getTransaction().commit();  
		} catch(Exception e){
			logYaz("Error executing query2 :" + e.getMessage());
			logYaz("Exception @" + getModelName() + "Controller :", e);
			entityManager.getTransaction().rollback(); 
		} finally {
			entityManager.close(); 	
			setTable(null); 
			queryAction();
		} 
	}
	
	public void changePermission(String islem){
		DBOperator dbOperator = getDBOperator(); 
		getIslemRole().reverseEnumerated(islem);		
		try {
			dbOperator.update(getIslemRole());  
			if(kullaniciYetkileriniGuncelle){
				EntityManager entityManager = dbOperator.getEntityManagerFactory().createEntityManager();
				entityManager.getTransaction().begin();
				Query query = entityManager
					.createQuery("UPDATE " + IslemKullanici.class.getSimpleName() + " o SET "
						+ " o.listPermission = :listPermission, " 
						+ " o.updatePermission = :updatePermission, "
						+ " o.deletePermission = :deletePermission " 
						+ " WHERE o.islemRef.rID =:islemRef AND o.kullaniciRoleRef.rID IN (SELECT m.rID From KullaniciRole m WHERE m.roleRef.rID = :roleRef ) ");
				 
				query.setParameter("listPermission", getIslemRole().getListPermission()); 
				query.setParameter("updatePermission", getIslemRole().getUpdatePermission());
				query.setParameter("deletePermission", getIslemRole().getDeletePermission()); 
				 
				query.setParameter("islemRef", getIslemRole().getIslemRef().getRID());
				query.setParameter("roleRef", getIslemRole().getRoleRef().getRID()); 
				query.executeUpdate();
				entityManager.getTransaction().commit();  
				entityManager.close(); 	 
			}
		} catch (Exception e) {
			logYaz("HATA :" + e.getMessage()); 
		}
		setTable(null); 
		queryAction();
	}

	public Boolean getKullaniciYetkileriniGuncelle() {
		return kullaniciYetkileriniGuncelle;
	}

	public void setKullaniciYetkileriniGuncelle(Boolean kullaniciYetkileriniGuncelle) {
		this.kullaniciYetkileriniGuncelle = kullaniciYetkileriniGuncelle;
	} 
	
} // class 
