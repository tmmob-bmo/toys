package tr.com.arf.toys.view.controller.form.system;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.component.html.HtmlSelectBooleanCheckbox;
import javax.faces.event.AjaxBehaviorEvent;

import tr.com.arf.framework.db.enumerated._common.EvetHayir;
import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.framework.utility.exception.data.DBException;
import tr.com.arf.framework.utility.tool.KeyUtil;
import tr.com.arf.toys.db.filter.system.SistemParametreFilter;
import tr.com.arf.toys.db.model.system.SistemParametre;
import tr.com.arf.toys.service.application.ManagedBeanLocator;
import tr.com.arf.toys.view.controller.form._base.ToysBaseFilteredController;

@ManagedBean
@ViewScoped
public class SistemParametreController extends ToysBaseFilteredController {

	private static final long serialVersionUID = 6531152165884980748L;

	private SistemParametre sistemParametre;
	private SistemParametreFilter sistemParametreFilter = new SistemParametreFilter();

	private Boolean kpsZorlama = false;
	private Boolean isbankasiaktif;
	private Boolean yapikrediaktif;
	private Boolean garantiaktif;

	public SistemParametreController() {
		super(SistemParametre.class);
		setDefaultValues(false);
		setTable(null);
		setOrderField("rID");
		if (getDBOperator().recordCount(SistemParametre.class.getSimpleName()) == 0) {
			sistemParametre = new SistemParametre();
			sistemParametre.setListeboyutu("20");
			sistemParametre.setSmsuzunluk("2");
			sistemParametre.setOdaadi("Bilgisayar Mühendisleri Odası");
			sistemParametre.setOdadetayi("Necatibey Cad. No:88/7  Çankaya/ ANKARA");
			sistemParametre.setOdawebadresi("http://bmo.org.tr/");
			sistemParametre.setLogodegeri("E:/Upload/Toys/mmo.jpg");
			sistemParametre.setKpszorla(EvetHayir._EVET);
			sistemParametre.setMailadresi("iletisim[at]bmo.org.tr");
			sistemParametre.setAsgariAylikNetUcret("1950");
			sistemParametre.setYabanciUyeKayitUcreti("700 (yedi yüz)");
			sistemParametre.setYabanciUyeAidatUcreti("170");
			sistemParametre.setHesapAdi("Bilgisayar Mühendisleri Odası");
			sistemParametre.setBankaAdi("Türkiye İş Bankası");
			sistemParametre.setSubeAdi("Yenişehir");
			sistemParametre.setBilirkisiHizmetBedeli("203,28 (iki yüz üç TL yirmi sekiz KR)");
			sistemParametre.setBilirkisiRaporBedeli("136,49 (yüz otuz altı TL kırk dokuz KR)");
			sistemParametre.setHesapNo("5950033");
			sistemParametre.setBilirkisilikToplamHizmetBedeli("279,98 (beş yüz üç TL yirmi sekiz KR)");
			sistemParametre.setIbanNo("TR00 0000 0000 0000 0000 0000 00");
			sistemParametre.setSistemmail("denizdamla13[at]mmo.org.tr");
			sistemParametre.setIsbankasiaktif(EvetHayir._HAYIR);
			sistemParametre.setYapikrediaktif(EvetHayir._HAYIR);
			sistemParametre.setYapikrediposadres("http://setmpos.ykb.com/3DSWebService/OOS");
			sistemParametre.setIsbankasiposadres("https://testsanalpos.est.com.tr/servlet/est3Dgate");
			try {
				getDBOperator().insert(sistemParametre);
			} catch (DBException e) {
				// TODO Auto-generated catch block
				logYaz("Exception @" + getModelName() + "Controller :", e);
			}
		}
		queryAction();
		if (!isEmpty(getList())) {
			setEditPanelRendered(true);
			setSistemParametre((SistemParametre) getList().get(0));
			setEntity(getList().get(0));
			if (getSistemParametre().getKpszorla() == EvetHayir._EVET) {
				setKpsZorlama(true);
			}
			if (getSistemParametre().getIsbankasiaktif() == EvetHayir._EVET) {
				setIsbankasiaktif(true);
			}
			if (getSistemParametre().getYapikrediaktif() == EvetHayir._EVET) {
				setYapikrediaktif(true);
			}
			if (getSistemParametre().getGarantiaktif() == EvetHayir._EVET) {
				setGarantiaktif(true);
			}
		}
	}

	public SistemParametre getSistemParametre() {
		sistemParametre = (SistemParametre) getEntity();
		return sistemParametre;
	}

	public void setSistemParametre(SistemParametre sistemParametre) {
		this.sistemParametre = sistemParametre;
	}

	public SistemParametreFilter getSistemParametreFilter() {
		return sistemParametreFilter;
	}

	public void setSistemParametreFilter(SistemParametreFilter sistemParametreFilter) {
		this.sistemParametreFilter = sistemParametreFilter;
	}

	public Boolean getKpsZorlama() {
		return kpsZorlama;
	}

	public void setKpsZorlama(Boolean kpsZorlama) {
		this.kpsZorlama = kpsZorlama;
	}

	public Boolean getIsbankasiaktif() {
		return isbankasiaktif;
	}

	public void setIsbankasiaktif(Boolean isbankasiaktif) {
		this.isbankasiaktif = isbankasiaktif;
	}

	public Boolean getYapikrediaktif() {
		return yapikrediaktif;
	}

	public void setYapikrediaktif(Boolean yapikrediaktif) {
		this.yapikrediaktif = yapikrediaktif;
	}

	public Boolean getGarantiaktif() {
		return garantiaktif;
	}

	public void setGarantiaktif(Boolean garantiaktif) {
		this.garantiaktif = garantiaktif;
	}

	@Override
	public BaseFilter getFilter() {
		return getSistemParametreFilter();
	}

	public void degistir() throws DBException {
		if (getKpsZorlama()) {
			getSistemParametre().setKpszorla(EvetHayir._EVET);
		} else {
			getSistemParametre().setKpszorla(EvetHayir._HAYIR);
		}
		if (getIsbankasiaktif()) {
			getSistemParametre().setIsbankasiaktif(EvetHayir._EVET);
		} else {
			getSistemParametre().setIsbankasiaktif(EvetHayir._HAYIR);
		}
		if (getYapikrediaktif()) {
			getSistemParametre().setYapikrediaktif(EvetHayir._EVET);
		} else {
			getSistemParametre().setYapikrediaktif(EvetHayir._HAYIR);
		}
		if (getGarantiaktif()) {
			getSistemParametre().setGarantiaktif(EvetHayir._EVET);
		} else {
			getSistemParametre().setGarantiaktif(EvetHayir._HAYIR);
		}
		getSistemParametre().setLogodegeri(getSistemParametre().getLogodegeri().replace("\\", "/"));
		if (getSistemParametre().getRID() != null) {
			getDBOperator().update(getSistemParametre());
		} else {
			getDBOperator().insert(getSistemParametre());
		}
		createGenericMessage(KeyUtil.getFrameworkLabel("kayit.guncellendi"), FacesMessage.SEVERITY_INFO);
		queryAction();
		if (!isEmpty(getList())) {
			setEditPanelRendered(true);
			setSistemParametre((SistemParametre) getList().get(0));
			setEntity(getList().get(0));
		}
		ManagedBeanLocator.locateApplicationController().createSistemParametre();
	}

	public void handleIsBankasiChange(AjaxBehaviorEvent event) {
		try {
			HtmlSelectBooleanCheckbox menu = (HtmlSelectBooleanCheckbox) event.getComponent();
			if ((Boolean) menu.getValue() == true) {
				getSistemParametre().setIsbankasiaktif(EvetHayir._EVET);
			} else {
				getSistemParametre().setIsbankasiaktif(EvetHayir._HAYIR);
			}

		} catch (Exception e) {
			logYaz("Error SistemParametreController @handleIsBankasiChange :" + e.getMessage());
			logYaz("Exception @" + getModelName() + "Controller :", e);
		}
	}

	public void handleYapikrediChange(AjaxBehaviorEvent event) {
		try {
			HtmlSelectBooleanCheckbox menu = (HtmlSelectBooleanCheckbox) event.getComponent();
			if ((Boolean) menu.getValue() == true) {
				getSistemParametre().setYapikrediaktif(EvetHayir._EVET);
			} else {
				getSistemParametre().setYapikrediaktif(EvetHayir._HAYIR);
			}

		} catch (Exception e) {
			logYaz("Error SistemParametreController @handleYapikrediChange :" + e.getMessage());
			logYaz("Exception @" + getModelName() + "Controller :", e);
		}
	}

	public void handleGarantiChange(AjaxBehaviorEvent event) {
		try {
			HtmlSelectBooleanCheckbox menu = (HtmlSelectBooleanCheckbox) event.getComponent();
			if ((Boolean) menu.getValue() == true) {
				getSistemParametre().setGarantiaktif(EvetHayir._EVET);
			} else {
				getSistemParametre().setGarantiaktif(EvetHayir._HAYIR);
			}

		} catch (Exception e) {
			logYaz("Error SistemParametreController @handleGarantiChange :" + e.getMessage());
			logYaz("Exception @" + getModelName() + "Controller :", e);
		}
	}

} // class
