package tr.com.arf.toys.view.controller.form.etkinlik;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.toys.db.filter.etkinlik.EtkinlikkurulFilter;
import tr.com.arf.toys.db.model.etkinlik.Etkinlik;
import tr.com.arf.toys.db.model.etkinlik.Etkinlikkurul;
import tr.com.arf.toys.service.application.ApplicationDescriptor;
import tr.com.arf.toys.service.application.ManagedBeanLocator;
import tr.com.arf.toys.view.controller.form._base.ToysBaseFilteredController;

@ManagedBean
@ViewScoped
public class EtkinlikkurulController extends ToysBaseFilteredController {

	private static final long serialVersionUID = 6531152165884980748L;

	private Etkinlikkurul etkinlikkurul;
	private EtkinlikkurulFilter etkinlikkurulFilter = new EtkinlikkurulFilter();

	public EtkinlikkurulController() {
		super(Etkinlikkurul.class);
		setDefaultValues(false);
		setOrderField("etkinlikRef.rID");
		setLoggable(false);
		setAutoCompleteSearchColumns(new String[] { "etkinlikRef" });
		setTable(null);
		if (getMasterEntity() != null) {
			setMasterObjectName(ApplicationDescriptor._ETKINLIK_MODEL);
			setMasterOutcome(ApplicationDescriptor._ETKINLIK_MODEL);
			getEtkinlikkurulFilter().setEtkinlikRef(getMasterEntity());
			getEtkinlikkurulFilter().createQueryCriterias();
		}
		queryAction();
	}

	public Etkinlik getMasterEntity() {
		if (getEtkinlikkurulFilter().getEtkinlikRef() != null) {
			return getEtkinlikkurulFilter().getEtkinlikRef();
		} else {
			return (Etkinlik) getObjectFromSessionFilter(ApplicationDescriptor._ETKINLIK_MODEL);
		}
	}

	public Etkinlikkurul getEtkinlikkurul() {
		etkinlikkurul = (Etkinlikkurul) getEntity();
		return etkinlikkurul;
	}

	public void setEtkinlikkurul(Etkinlikkurul etkinlikkurul) {
		this.etkinlikkurul = etkinlikkurul;
	}

	public EtkinlikkurulFilter getEtkinlikkurulFilter() {
		return etkinlikkurulFilter;
	}

	public void setEtkinlikkurulFilter(EtkinlikkurulFilter etkinlikkurulFilter) {
		this.etkinlikkurulFilter = etkinlikkurulFilter;
	}

	@Override
	public BaseFilter getFilter() {
		return getEtkinlikkurulFilter();
	}

	@Override
	public void insert() {
		super.insert();
		getEtkinlikkurul().setEtkinlikRef(
				getEtkinlikkurulFilter().getEtkinlikRef());
	}

	public String etkinlikkuruluye() {
		if (!setSelected()) {
			return "";
		}
		getSessionUser().setLegalAccess(1);
		putObjectToSessionFilter(ApplicationDescriptor._ETKINLIKKURUL_MODEL, getEtkinlikkurul());
		return ManagedBeanLocator.locateMenuController().gotoPage(
				"menu_Etkinlikkuruluye");
	}
 

} // class 
