package tr.com.arf.toys.view.controller.form.system;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.toys.db.filter.system.InsanFilter;
import tr.com.arf.toys.db.model.system.Insan;
import tr.com.arf.toys.view.controller.form._base.ToysBaseFilteredController;




public class InsanController extends ToysBaseFilteredController {
	
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Insan insan;
	private InsanFilter insanFilter = new InsanFilter();
	
	protected InsanController() {
		super(Insan.class);  
		setDefaultValues(false);  
		setOrderField("tckn"); //??  
		setAutoCompleteSearchColumns(new String[]{"tckn"});  //??
		setTable(null); 
		queryAction(); 
	}
	
	public Insan getInsan() { 
		insan = (Insan) getEntity(); 
		return insan; 
	} 
	
	public void setInsan(Insan insan) { 
		this.insan = insan; 
	} 
	
	public InsanFilter getInsanFilter() { 
		return insanFilter; 
	} 
	 
	public void setInsanFilter(InsanFilter insanFilter) {  
		this.insanFilter = insanFilter;  
	}  
	 
	@Override  
	public BaseFilter getFilter() {  
		return getInsanFilter();  
	}  
	
	
 
	
// class 

	

}
