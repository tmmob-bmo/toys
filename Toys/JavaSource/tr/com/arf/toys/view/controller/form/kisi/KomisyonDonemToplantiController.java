package tr.com.arf.toys.view.controller.form.kisi; 
  
 
import java.util.Calendar;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.toys.db.filter.kisi.KomisyonDonemToplantiFilter;
import tr.com.arf.toys.db.model.kisi.KomisyonDonem;
import tr.com.arf.toys.db.model.kisi.KomisyonDonemToplanti;
import tr.com.arf.toys.service.application.ApplicationDescriptor;
import tr.com.arf.toys.service.application.ManagedBeanLocator;
import tr.com.arf.toys.view.controller.form._base.ToysBaseFilteredController;
  
  
@ManagedBean 
@ViewScoped 
public class KomisyonDonemToplantiController extends ToysBaseFilteredController {  
  
  private static final long serialVersionUID = 6531152165884980748L; 
  
	private KomisyonDonemToplanti komisyonDonemToplanti;  
	private KomisyonDonemToplantiFilter komisyonDonemToplantiFilter = new KomisyonDonemToplantiFilter();  
  
	public KomisyonDonemToplantiController() { 
		super(KomisyonDonemToplanti.class);  
		setDefaultValues(false);  
		setOrderField("komisyonDonemRef.rID");  
		setLoggable(false);  
		setAutoCompleteSearchColumns(new String[]{"komisyonDonemRef"}); 
		setTable(null); 
		if(getMasterEntity() != null){ 
			setMasterObjectName(ApplicationDescriptor._KOMISYONDONEM_MODEL);
			setMasterOutcome(ApplicationDescriptor._KOMISYONDONEM_MODEL);
			getKomisyonDonemToplantiFilter().setKomisyonDonemRef(getMasterEntity());
			getKomisyonDonemToplantiFilter().createQueryCriterias();   
		} 
		queryAction(); 
	} 
 
	public KomisyonDonem getMasterEntity(){
		if(getKomisyonDonemToplantiFilter().getKomisyonDonemRef() != null){
			return getKomisyonDonemToplantiFilter().getKomisyonDonemRef();
		} else {
			return (KomisyonDonem) getObjectFromSessionFilter(ApplicationDescriptor._KOMISYONDONEM_MODEL);
		}			
	} 
 
	public KomisyonDonemToplanti getKomisyonDonemToplanti() { 
		komisyonDonemToplanti = (KomisyonDonemToplanti) getEntity(); 
		return komisyonDonemToplanti; 
	} 
	
	public void setKomisyonDonemToplanti(KomisyonDonemToplanti komisyonDonemToplanti) { 
		this.komisyonDonemToplanti = komisyonDonemToplanti; 
	} 
	
	public KomisyonDonemToplantiFilter getKomisyonDonemToplantiFilter() { 
		return komisyonDonemToplantiFilter; 
	} 
	 
	public void setKomisyonDonemToplantiFilter(KomisyonDonemToplantiFilter komisyonDonemToplantiFilter) {  
		this.komisyonDonemToplantiFilter = komisyonDonemToplantiFilter;  
	}  
	 
	@Override  
	public BaseFilter getFilter() {  
		return getKomisyonDonemToplantiFilter();  
	}   
 
	@Override	
	public void insert() {	
		super.insert();	
		getKomisyonDonemToplanti().setKomisyonDonemRef(getKomisyonDonemToplantiFilter().getKomisyonDonemRef());	
		Calendar c = Calendar.getInstance();
		c.set(Calendar.HOUR_OF_DAY, 19);
		c.set(Calendar.MINUTE, 00);
		getKomisyonDonemToplanti().setTarih(c.getTime());
	}	
	
	public String komisyonDonemToplantiTutanak() {	
		if (!setSelected()) {
			return "";
		}	
		getSessionUser().setLegalAccess(1);	
		putObjectToSessionFilter(ApplicationDescriptor._KOMISYONDONEMTOPLANTI_MODEL, getKomisyonDonemToplanti());
		return ManagedBeanLocator.locateMenuController().gotoPage("menu_KomisyonDonemToplantiTutanak"); 
	} 
	
	public String komisyonDonemToplantiKatilimci() {	
		if (!setSelected()){
			return "";	
		}
		getSessionUser().setLegalAccess(1);	
		putObjectToSessionFilter(ApplicationDescriptor._KOMISYONDONEMTOPLANTI_MODEL, getKomisyonDonemToplanti());
		return ManagedBeanLocator.locateMenuController().gotoPage("menu_KomisyonDonemToplantiKatilimci"); 
	} 
	
	
} // class 
