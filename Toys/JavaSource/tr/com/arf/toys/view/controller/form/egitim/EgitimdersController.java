package tr.com.arf.toys.view.controller.form.egitim;

import java.math.BigDecimal;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.framework.db.model._base.BaseEntity;
import tr.com.arf.framework.utility.tool.KeyUtil;
import tr.com.arf.toys.db.filter.egitim.EgitimdersFilter;
import tr.com.arf.toys.db.model.egitim.Egitim;
import tr.com.arf.toys.db.model.egitim.Egitimders;
import tr.com.arf.toys.service.application.ApplicationDescriptor;
import tr.com.arf.toys.view.controller.form._base.ToysBaseFilteredController;

@ManagedBean
@ViewScoped
public class EgitimdersController extends ToysBaseFilteredController {

	private static final long serialVersionUID = 6531152165884980748L;

	private Egitimders egitimders;
	private EgitimdersFilter egitimdersFilter = new EgitimdersFilter();

	public EgitimdersController() {
		super(Egitimders.class);
		setDefaultValues(false);
		setOrderField("aciklama");
		setLoggable(false);
		setAutoCompleteSearchColumns(new String[] { "aciklama" });
		setTable(null);
		if (getMasterEntity() != null) {
			setMasterObjectName(ApplicationDescriptor._EGITIM_MODEL);
			setMasterOutcome(ApplicationDescriptor._EGITIM_MODEL);
			getEgitimdersFilter().setEgitimRef(getMasterEntity());
			getEgitimdersFilter().createQueryCriterias();
		}
		queryAction();
	}

	public Egitim getMasterEntity() {
		if (getEgitimdersFilter().getEgitimRef() != null) {
			return getEgitimdersFilter().getEgitimRef();
		} else {
			return (Egitim) getObjectFromSessionFilter(ApplicationDescriptor._EGITIM_MODEL);
		}
	}

	public Egitimders getEgitimders() {
		egitimders = (Egitimders) getEntity();
		return egitimders;
	}

	public void setEgitimders(Egitimders egitimders) {
		this.egitimders = egitimders;
	}

	public EgitimdersFilter getEgitimdersFilter() {
		return egitimdersFilter;
	}

	public void setEgitimdersFilter(EgitimdersFilter egitimdersFilter) {
		this.egitimdersFilter = egitimdersFilter;
	}

	@Override
	public BaseFilter getFilter() {
		return getEgitimdersFilter();
	}

	@Override
	public void insert() {
		super.insert();
		getEgitimders().setEgitimRef(getEgitimdersFilter().getEgitimRef());
	}

	@Override
	public void save() {
		List<BaseEntity> list = getList();
		BigDecimal toplamSure = new BigDecimal(0);
		for (BaseEntity b : list) {
			toplamSure = toplamSure.add(((Egitimders) b).getSure());
		}
		
		BigDecimal sonSure = this.egitimders.getSure().add(toplamSure);
		BigDecimal testSure = this.egitimders.getEgitimRef().getEgitimtanimRef().getSure();
		
		if(sonSure.compareTo(testSure) < 0  && this.getEgitimders().getZaman().after(getMasterEntity().getBaslangictarih()))
		{
		 super.save();	
		}
		else if(this.getEgitimders().getZaman().before(getMasterEntity().getBaslangictarih())){
			createGenericMessage(KeyUtil.getMessageValue("egitim.tarihKontrol"), FacesMessage.SEVERITY_ERROR);
		}
		else{
			createGenericMessage(KeyUtil.getMessageValue("egitim.sureKontrol"), FacesMessage.SEVERITY_ERROR);
		}

	}
} // class 
