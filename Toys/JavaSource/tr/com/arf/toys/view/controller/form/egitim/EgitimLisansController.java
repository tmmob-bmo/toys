package tr.com.arf.toys.view.controller.form.egitim; 
 
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.toys.db.filter.egitim.EgitimLisansFilter;
import tr.com.arf.toys.db.model.egitim.EgitimLisans;
import tr.com.arf.toys.db.model.egitim.Egitimtanim;
import tr.com.arf.toys.service.application.ApplicationDescriptor;
import tr.com.arf.toys.view.controller.form._base.ToysBaseFilteredController;
  
  
@ManagedBean 
@ViewScoped 
public class EgitimLisansController extends ToysBaseFilteredController {  
  
  private static final long serialVersionUID = 6531152165884980748L; 
  
	private EgitimLisans egitimLisans;  
	private EgitimLisansFilter egitimLisansFilter = new EgitimLisansFilter();  
  
	public EgitimLisansController() { 
		super(EgitimLisans.class);  
		setDefaultValues(false);  
		setLoggable(false);  
		setTable(null); 
		if (getMasterEntity() != null) {
			setMasterObjectName(ApplicationDescriptor._EGITIMTANIM_MODEL);
			setMasterOutcome(ApplicationDescriptor._EGITIMTANIM_MODEL);
			getEgitimLisansFilter().setEgitimtanimRef(getMasterEntity());
			getEgitimLisansFilter().createQueryCriterias();
		}
		queryAction();
	}
	
	public EgitimLisansController(Object object) { 
		super(EgitimLisans.class);  
		setLoggable(true); 
	}
	
	public Egitimtanim getMasterEntity() {
		if (getEgitimLisansFilter().getEgitimtanimRef() != null) {
			return getEgitimLisansFilter().getEgitimtanimRef();
		} else {
			return (Egitimtanim) getObjectFromSessionFilter(ApplicationDescriptor._EGITIMTANIM_MODEL);
		}
	}
	public EgitimLisans getEgitimLisans() {
		egitimLisans=(EgitimLisans) getEntity();
		return egitimLisans;
	}

	public void setEgitimLisans(EgitimLisans egitimLisans) {
		this.egitimLisans = egitimLisans;
	}

	public EgitimLisansFilter getEgitimLisansFilter() {
		return egitimLisansFilter;
	}

	public void setEgitimLisansFilter(EgitimLisansFilter egitimLisansFilter) {
		this.egitimLisansFilter = egitimLisansFilter;
	}

	@Override  
	public BaseFilter getFilter() {  
		return getEgitimLisansFilter(); 
	}  
	
	@Override
		public void save() {
			
			if (getMasterEntity()!=null){
				egitimLisans.setEgitimtanimRef(getMasterEntity());
			}else {
				createGenericMessage("Eğitim Lisans Bilgileri Boş Geldiğinden Dolayı Kaydetme İşlemini"
						+ " Gerçekleştiremiyoruz!", FacesMessage.SEVERITY_INFO);
				return ;
			}
			super.save();
			queryAction();
		}
	
} // class 
