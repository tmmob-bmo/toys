package tr.com.arf.toys.view.controller.form.system;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import tr.com.arf.framework.db._management.DBOperator;
import tr.com.arf.framework.db.enumerated._common.EvetHayir;
import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.framework.utility.exception.data.DBException;
import tr.com.arf.framework.utility.tool.KeyUtil;
import tr.com.arf.toys.db.filter.system.IslemFilter;
import tr.com.arf.toys.db.model.system.Islem;
import tr.com.arf.toys.db.model.system.IslemKullanici;
import tr.com.arf.toys.db.model.system.IslemRole;
import tr.com.arf.toys.db.model.system.KullaniciRole;
import tr.com.arf.toys.db.model.system.Role;
import tr.com.arf.toys.view.controller.form._base.ToysBaseFilteredController;

@ManagedBean
@ViewScoped
public class IslemController extends ToysBaseFilteredController {

	private static final long serialVersionUID = 6531152165884980748L;

	private Islem islem;
	private IslemFilter islemFilter = new IslemFilter();

	public IslemController() {
		super(Islem.class);
		setDefaultValues(false);
		setOrderField("name");
		setAutoCompleteSearchColumns(new String[] { "name" });
		setTable(null);
		queryAction();
	}

	public Islem getIslem() {
		islem = (Islem) getEntity();
		return islem;
	}

	public void setIslem(Islem islem) {
		this.islem = islem;
	}

	public IslemFilter getIslemFilter() {
		return islemFilter;
	}

	public void setIslemFilter(IslemFilter islemFilter) {
		this.islemFilter = islemFilter;
	}

	@Override
	public BaseFilter getFilter() {
		return getIslemFilter();
	}

	@Override
	public void delete() {
		try {
			if (getDBOperator().recordCount(IslemRole.class.getSimpleName(), "o.islemRef.rID=" + getIslem().getRID()) > 0) {
				@SuppressWarnings("unchecked")
				List<IslemRole> islemRoleList = getDBOperator().load(IslemRole.class.getSimpleName(), "o.islemRef.rID=" + getIslem().getRID(), "o.rID");
				for (IslemRole entity : islemRoleList) {
					getDBOperator().delete(entity);
				}
			}
			if (getDBOperator().recordCount(IslemKullanici.class.getSimpleName(), "o.islemRef.rID=" + getIslem().getRID()) > 0) {
				@SuppressWarnings("unchecked")
				List<IslemKullanici> islemKullaniciList = getDBOperator().load(IslemKullanici.class.getSimpleName(), "o.islemRef.rID=" + getIslem().getRID(), "o.rID");
				for (IslemKullanici entity : islemKullaniciList) {
					getDBOperator().delete(entity);
				}
			}
		} catch (DBException e) {
			// TODO Auto-generated catch block
			logYaz("Exception @" + getModelName() + "Controller :", e);
		}
		super.delete();
		queryAction();
	}

	@Override
	@SuppressWarnings("unchecked")
	public void save() {
		DBOperator dbOperator = getDBOperator();
		try {
			if (getDBOperator().recordCount(Islem.class.getSimpleName(),
					" (UPPER(o.name) = UPPER('" + getIslem().getName() + "') OR LOWER(o.name) = LOWER('" + getIslem().getName() + "'))") > 0) {
				createGenericMessage(KeyUtil.getMessageValue("kayit.mukerrer"), FacesMessage.SEVERITY_ERROR);
				return;
			}
			dbOperator.insert(getIslem());
			IslemRole islemRole = new IslemRole();
			List<Role> roleList = dbOperator.load(Role.class.getSimpleName());
			for (Role rolObj : roleList) {
				try {
					islemRole = new IslemRole();
					islemRole.setRoleRef(rolObj);
					islemRole.setIslemRef(getIslem());
					islemRole.setListPermission(EvetHayir._EVET);
					islemRole.setUpdatePermission(EvetHayir._HAYIR);
					islemRole.setDeletePermission(EvetHayir._HAYIR);
					dbOperator.insert(islemRole);

					if (getDBOperator().recordCount(KullaniciRole.class.getSimpleName(), "o.roleRef.rID=" + rolObj.getRID()) > 0) {
						List<KullaniciRole> kullaniciRoleList = dbOperator.load(KullaniciRole.class.getSimpleName(), "o.roleRef.rID=" + rolObj.getRID(), "o.rID");

						// O role sahip her kullanici'a, o isleme ait yetkiler
						// ekleniyor.
						for (KullaniciRole kullaniciRole : kullaniciRoleList) {
							IslemKullanici islemKullanici = new IslemKullanici();
							islemKullanici.setDeletePermission(EvetHayir._HAYIR);
							islemKullanici.setUpdatePermission(EvetHayir._HAYIR);
							islemKullanici.setListPermission(EvetHayir._EVET);
							islemKullanici.setIslemRef(getIslem());
							islemKullanici.setKullaniciRoleRef(kullaniciRole);
							dbOperator.insert(islemKullanici);
						}
					}

				} catch (DBException e) {
					createGenericMessage(KeyUtil.getMessageValue("islem.hata"), FacesMessage.SEVERITY_ERROR);
					return;
				}
			}
		} catch (DBException e2) {
			createGenericMessage(KeyUtil.getMessageValue("islem.hata"), FacesMessage.SEVERITY_ERROR);
			return;
		}
		insertOnly();
		queryAction();
	}

	/*
	 * public String islemrole() { if (!setSelected()) return "";
	 * getSessionUser().setLegalAccess(1);
	 * putObjectToSessionFilter(ApplicationDescriptor._ISLEM_MODEL, getIslem());
	 * return ApplicationDescriptor._ISLEMROLE_OUTCOME; }
	 */

	/*
	 * public String islemkullanici() { if (!setSelected()) return "";
	 * getSessionUser().setLegalAccess(1);
	 * putObjectToSessionFilter(ApplicationDescriptor._ISLEM_MODEL, getIslem());
	 * return ApplicationDescriptor._ISLEMKULLANICI_OUTCOME; }
	 */

} // class
