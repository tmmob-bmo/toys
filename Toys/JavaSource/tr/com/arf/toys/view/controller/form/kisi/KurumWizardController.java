package tr.com.arf.toys.view.controller.form.kisi;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import tr.com.arf.framework.db.enumerated._common.EvetHayir;
import tr.com.arf.framework.db.enumerated._common.IslemTuru;
import tr.com.arf.framework.utility.exception.data.DBException;
import tr.com.arf.framework.utility.tool.KeyUtil;
import tr.com.arf.framework.utility.validator.EmailValidator;
import tr.com.arf.toys.db.enumerated.iletisim.AdresTipi;
import tr.com.arf.toys.db.enumerated.iletisim.AdresTuru;
import tr.com.arf.toys.db.enumerated.iletisim.NetAdresTuru;
import tr.com.arf.toys.db.enumerated.iletisim.TelefonTuru;
import tr.com.arf.toys.db.enumerated.kisi.KimlikTip;
import tr.com.arf.toys.db.model.iletisim.Adres;
import tr.com.arf.toys.db.model.iletisim.Internetadres;
import tr.com.arf.toys.db.model.iletisim.Telefon;
import tr.com.arf.toys.db.model.kisi.Kisi;
import tr.com.arf.toys.db.model.kisi.Kisikimlik;
import tr.com.arf.toys.db.model.kisi.Kurum;
import tr.com.arf.toys.db.model.system.Birim;
import tr.com.arf.toys.db.model.system.Ulke;
import tr.com.arf.toys.db.nonEntityModel.PojoKisi;
import tr.com.arf.toys.service.application.ApplicationDescriptor;
import tr.com.arf.toys.service.application.ManagedBeanLocator;
import tr.com.arf.toys.utility.application.ApplicationConstant;
import tr.com.arf.toys.view.controller.form._base.ToysBaseController;
import tr.com.arf.toys.view.controller.form.iletisim.AdresController;
import tr.com.arf.toys.view.controller.form.iletisim.InternetadresController;
import tr.com.arf.toys.view.controller.form.iletisim.TelefonController;
import tr.com.arf.toys.view.controller.form.system.BirimController;

@ManagedBean(name = "kurumWizardController")
@ViewScoped
public class KurumWizardController extends ToysBaseController {

	private static final long serialVersionUID = 6531152165884980748L;

	private int wizardStep = 1;
	private Kisi kisi;
	private Kisikimlik kisikimlik;

	private Adres kpsAdres;
	private Adres kpsDigerAdres;
	
	private Kurum kurum;
	private Birim birim=new Birim();
	private String detailEditType; 
	
	private boolean kpsSorguSonucDondu = false;
	private boolean kpsKaydiBulundu = false;
	private boolean kisiVeritabanindaBulundu = false;
	
	private PojoKisi kisikimlikPojo = new PojoKisi();
	
	
	private Telefon telefon2;
	private Telefon telefon3;
	private Telefon telefon4;
	private Adres adres;
	private Internetadres internetAdres1; 
	
	//private static final String PHONE_PATTERN = "0\\d{3}\\d{7}";
    private static final String PHONE_PATTERN = "0\\s[\\(]\\d{3}[\\)]\\s\\d{3}\\s\\d{4}";
	private final Pattern pattern;
	
	
    private KurumController kurumController=new KurumController(null);
    private BirimController birimController=new BirimController(null);
    private TelefonController telefonController=new TelefonController(null);
    private InternetadresController internetadresController=new InternetadresController(null);
    private AdresController adresController= new AdresController(null); 
    
    private String oldValueForAdres;
    private String oldValueForKurum;
	private boolean varsayilan =false;
	public KurumWizardController() {
		super(Kurum.class);
		setDefaultValues(false);
		setAutoCompleteSearchColumns(ApplicationConstant._birimAutoCompleteSearchColumns); 
		pattern = Pattern.compile(PHONE_PATTERN);
		prepareForWizard();
		setTable(null); 
		queryAction();
	}
	
	private void prepareForWizard(){
		this.wizardStep = 1;
		super.insert();
		
		this.kisi = new Kisi(); 
		this.kisi.setKimliknotip(KimlikTip._TCKIMLIKNO);
		this.kisi.setAktif(EvetHayir._EVET);
		this.kisikimlik = new Kisikimlik();
		this.birim = new Birim();
		this.kurum=new Kurum();
		this.adres=new Adres();
		removeObjectFromSessionFilter(ApplicationDescriptor._PERSONEL_MODEL);
		
		this.kisikimlikPojo = new PojoKisi();

		this.telefon2 = new Telefon();
		telefon2.setTelefonturu(TelefonTuru._ISTELEFONU);
		this.telefon3 = new Telefon();	
		telefon3.setTelefonturu(TelefonTuru._GSM);
		this.telefon4 = new Telefon();
		telefon4.setTelefonturu(TelefonTuru._FAKS);
		this.internetAdres1 = new Internetadres();   	
		internetAdres1.setNetadresturu(NetAdresTuru._EPOSTA);
		
		this.detailEditType = "";
		this.kpsSorguSonucDondu = false;
		this.kpsKaydiBulundu = false;
		this.kisiVeritabanindaBulundu = false;
	}
	
	
	public void validateTelefon() throws DBException{	
		
		
		if(!isEmpty(this.telefon2.getTelefonno())){
			Matcher matcher = pattern.matcher(this.telefon2.getTelefonno().toString());  
			if (!matcher.matches()) { 
				createCustomMessage("Geçersiz Telefon Numarası ! (0 (XXX) XXX XXXX şeklinde giriniz.) ", FacesMessage.SEVERITY_ERROR, "telefonnoInputText2:telefonno2");
				return;
			}
			this.telefon2.setKurumRef(this.kurum);
			this.telefon2.setVarsayilan(EvetHayir._HAYIR);
		}
		if(!isEmpty(this.telefon3.getTelefonno())){
			Matcher matcher = pattern.matcher(this.telefon3.getTelefonno().toString());  
			if (!matcher.matches()) { 
				createCustomMessage("Geçersiz Telefon Numarası ! (0 (XXX) XXX XXXX şeklinde giriniz.) ", FacesMessage.SEVERITY_ERROR, "telefonnoInputText3:telefonno3");
				return;
			}
			this.telefon3.setKurumRef(this.kurum);
			this.telefon3.setVarsayilan(EvetHayir._HAYIR);
		}
		if(!isEmpty(this.telefon4.getTelefonno())){
			Matcher matcher = pattern.matcher(this.telefon4.getTelefonno().toString());  
			if (!matcher.matches()) { 
				createCustomMessage("Geçersiz Telefon Numarası ! (0 (XXX) XXX XXXX şeklinde giriniz.) ", FacesMessage.SEVERITY_ERROR, "telefonnoInputText4:telefonno4");
				return;
			}
			this.telefon4.setKurumRef(this.kurum);
			this.telefon4.setVarsayilan(EvetHayir._HAYIR);
		}
		finishWizard(); 
	}
	
	public void finishWizard() throws DBException{  
		try {		
			
			if(this.adres.getAdrestipi() == AdresTipi._YURTDISIADRESI){
				if(this.adres.getYabanciulke() != null && !isEmpty(this.adres.getYabanciadres()) && this.adres.getYabancisehir() != null){
					this.adres.setKurumRef(getKurum());
					this.adres.setVarsayilan(EvetHayir._HAYIR); 
					getDBOperator().insert(this.adres);
				}
			} else if(this.adres.getAdrestipi() != AdresTipi._YURTDISIADRESI){ 
				if(this.adres.getUlkeRef() != null && !isEmpty(this.adres.getAcikAdres()) && this.adres.getSehirRef() != null
						&& this.adres.getIlceRef() != null){
					this.adres.setKurumRef(getKurum());
					this.adres.setVarsayilan(EvetHayir._HAYIR); 
					if (this.adres.getAdresturu()==null){
						this.adres.setAdresturu(AdresTuru._EVADRESI);
					}
					getDBOperator().update(this.adres);
				} 
			}	
			if(!isEmpty(this.telefon2.getTelefonno())){
				this.telefon2.setKurumRef(this.kurum);
				this.telefon2.setVarsayilan(EvetHayir._EVET);
				getDBOperator().insert(telefon2);
			}			
			if(!isEmpty(this.telefon3.getTelefonno())){
				this.telefon3.setKurumRef(this.kurum);
				this.telefon3.setVarsayilan(EvetHayir._HAYIR);
				getDBOperator().insert(telefon3);
			} 
			if(!isEmpty(this.telefon4.getTelefonno())){
				this.telefon4.setKurumRef(this.kurum);
				this.telefon4.setVarsayilan(EvetHayir._HAYIR);
				getDBOperator().insert(telefon4);
			}			
			if(!isEmpty(this.internetAdres1.getNetadresmetni())){
				EmailValidator emailValidator=new EmailValidator();
				emailValidator.validate(FacesContext.getCurrentInstance(),FacesContext.getCurrentInstance().getViewRoot().findComponent("kurumForm:netadresmetniInputText:netadresmetni"), this.internetAdres1.getNetadresmetni());
				this.internetAdres1.setKurumRef(this.kurum);
				this.internetAdres1.setVarsayilan(EvetHayir._EVET);
				getDBOperator().insert(internetAdres1);
			} 
			if(this.kurum.getRID() != null){ 
				getKurumController().setSelectedRID(this.kurum.getRID() + "");
				ManagedBeanLocator.locateSessionUser().addToSessionFilters(Kurum.class.getSimpleName(), this.kurum);
				FacesContext.getCurrentInstance().getExternalContext().redirect("kurum_Detay.xhtml"); 
			} else { 
				FacesContext.getCurrentInstance().getExternalContext().redirect("kurum.xhtml"); 
			}	 
			 
		} catch(Exception e){
			logYaz("Error @finishWizard:" + e.getMessage());
			createGenericMessage(KeyUtil.getMessageValue("islem.hata"), FacesMessage.SEVERITY_ERROR); 
		} 
	}
	
	
	public boolean isKurumBulundu(){
		if(getObjectFromSessionFilter(ApplicationDescriptor._KURUM_MODEL) != null){
			return true;					
		} else {
			return false;
		}
	}
	
	@Override
	public void wizardForward(){ 
		if (this.wizardStep==1){
				try {
					if (this.kurum!= null){
						getDBOperator().insert(this.kurum);
					}
					if(getKurumController().isLoggable()){
						logKaydet(this.kurum, IslemTuru._EKLEME, "");
					}
					this.adres.setAdresturu(AdresTuru._ISADRESI);
					wizardStep++;
				} catch (DBException e) {
					logYaz("KurumWizardController @wizardForward step 1 =" + e.getMessage());
					logYaz("Exception @" + getModelName() + "Controller :", e);
				}
		}else if (this.wizardStep == 2) {
			try {
			this.adres.setKurumRef(this.kurum);
				if (this.adres.getVarsayilan() == EvetHayir._EVET) {
					String whereCondition = "o.rID <> " + this.adres.getRID()+ " AND o.varsayilan =" + EvetHayir._EVET.getCode();
					if (this.adres.getKurumRef() != null) {
						whereCondition += " AND o.kurumRef.rID = "+ this.adres.getKurumRef().getRID();
					}
					try {
						if (getDBOperator().recordCount(Adres.class.getSimpleName(), whereCondition) > 0) {
							for (Object obj : getDBOperator().load(Adres.class.getSimpleName(), whereCondition,"o.rID")) {
								Adres adres = (Adres) obj;
								adres.setVarsayilan(EvetHayir._HAYIR);
								getDBOperator().update(adres);
							}
						}
					} catch (Exception e) {
						logYaz("Exception @" + getModelName() + "Controller :", e);
					}
				} else if (this.adres.getVarsayilan() == EvetHayir._HAYIR) {
					String whereCondition = "o.rID <> " + this.adres.getRID()+ " AND o.varsayilan =" + EvetHayir._HAYIR.getCode();
					if (this.adres.getKurumRef() != null) {
						whereCondition += " AND o.kurumRef.rID = "+ this.adres.getKurumRef().getRID();
					}
					String whereCondition1 = "o.rID <> " + this.adres.getRID()+ " AND o.varsayilan =" + EvetHayir._EVET.getCode();
				
					try {
						if (getDBOperator().recordCount(Adres.class.getSimpleName(), whereCondition) == 0
								&& getDBOperator().recordCount(Adres.class.getSimpleName(),	whereCondition1) == 0) {
							adres.setVarsayilan(EvetHayir._EVET);
//							getDBOperator().update(adres);
						}
						whereCondition = "o.rID <> " + this.adres.getRID()+ " AND o.varsayilan =" + EvetHayir._EVET.getCode();
						if (this.adres.getKurumRef() != null) {
							whereCondition += " AND o.kurumRef.rID = "+ this.adres.getKurumRef().getRID();
						}
						if (getDBOperator().recordCount(Adres.class.getSimpleName(), whereCondition) == 0) {
							createGenericMessage(KeyUtil.getMessageValue("varsayilan.hata"),FacesMessage.SEVERITY_INFO);
							adres.setVarsayilan(EvetHayir._EVET);
//							getDBOperator().update(adres);
						}

					} catch (Exception e) {
						logYaz("Exception @" + getModelName() + "Controller :", e);
					}
			}
			if (getDBOperator().recordCount(Ulke.class.getSimpleName(), "o.ulkekod='9980'")>0){
				this.adres.setUlkeRef((Ulke)getDBOperator().load(Ulke.class.getSimpleName(), "o.ulkekod='9980'","o.rID").get(0));
			}
			if (this.adres.getRID() == null) {
				if (this.adres.getAdrestipi() == AdresTipi._YURTDISIADRESI) {
					if (this.adres.getYabanciulke() == null	|| isEmpty(this.adres.getYabancisehir()) || this.adres.getYabanciadres() == null) {
						createGenericMessage("Yurdışı adresleri için ülke, şehir ve adres alanları zorunludur",FacesMessage.SEVERITY_ERROR);
						return;
					}
				} else if (this.adres.getAdrestipi() != AdresTipi._YURTDISIADRESI) {
					//Turkiyeyi set etsin diye ulke kodunu 1 diye set ediyorum
					
					if (this.adres.getUlkeRef() == null	|| isEmpty(this.adres.getAcikAdres())|| this.adres.getSehirRef() == null|| this.adres.getIlceRef() == null) {
						createGenericMessage("Adres için il, ilçe ve açık adres alanları zorunludur",FacesMessage.SEVERITY_ERROR);
						return;
					}
				}
				
				this.adres.setAdresturu(AdresTuru._ISADRESI);
				if (getVarsayilan()){
					
				}
				getDBOperator().insert(this.adres);
				this.oldValueForAdres = this.adres.getValue();
				if (this.adresController.isLoggable()) {
					logKaydet(this.adres, IslemTuru._EKLEME, "");
				}
				createGenericMessage(KeyUtil.getMessageValue("kayit.eklendi"),FacesMessage.SEVERITY_INFO);
				this.adres = new Adres();
			}
			
			adresListesiGuncelle(this.kisi);
			wizardStep++;
		}catch (Exception e) {
			logYaz("Error KurumWizardController @saveAndForward:" + e.getMessage());
			createGenericMessage(KeyUtil.getMessageValue("islem.hata"),FacesMessage.SEVERITY_ERROR);
		}
		} else {
			wizardStep++;
		}
			
	}
	
	@Override
	public void wizardBackward(){
		wizardStep--;
	}
	
		
	private void adresListesiGuncelle(Kisi refKisi) { 
		this.adresController.setTable(null); 
		this.adresController.getAdresFilter().setKisiRef(refKisi);
		this.adresController.getAdresFilter().createQueryCriterias(); 
		this.adresController.queryAction(); 
	}
	@Override
	public int getWizardStep() {
		return wizardStep;
	}

	@Override
	public void setWizardStep(int wizardStep) {
		this.wizardStep = wizardStep;
	}

	public Kisi getKisi() {
		return kisi;
	}

	public void setKisi(Kisi kisi) {
		this.kisi = kisi;
	}

	public Kisikimlik getKisikimlik() {
		return kisikimlik;
	}

	public void setKisikimlik(Kisikimlik kisikimlik) {
		this.kisikimlik = kisikimlik;
	}

	public Adres getKpsAdres() {
		return kpsAdres;
	}

	public void setKpsAdres(Adres kpsAdres) {
		this.kpsAdres = kpsAdres;
	}

	public Adres getKpsDigerAdres() {
		return kpsDigerAdres;
	}

	public void setKpsDigerAdres(Adres kpsDigerAdres) {
		this.kpsDigerAdres = kpsDigerAdres;
	}

	public Kurum getKurum() {
		kurum = (Kurum) getEntity();
		return kurum;
	}

	public void setKurum(Kurum kurum) {
		this.kurum = kurum;
	}

	public Birim getBirim() {
		return birim;
	}

	public void setBirim(Birim birim) {
		this.birim = birim;
	}

	public String getDetailEditType() {
		return detailEditType;
	}

	public void setDetailEditType(String detailEditType) {
		this.detailEditType = detailEditType;
	}

	public boolean isKpsSorguSonucDondu() {
		return kpsSorguSonucDondu;
	}

	public void setKpsSorguSonucDondu(boolean kpsSorguSonucDondu) {
		this.kpsSorguSonucDondu = kpsSorguSonucDondu;
	}

	public boolean isKpsKaydiBulundu() {
		return kpsKaydiBulundu;
	}

	public void setKpsKaydiBulundu(boolean kpsKaydiBulundu) {
		this.kpsKaydiBulundu = kpsKaydiBulundu;
	}

	public boolean isKisiVeritabanindaBulundu() {
		return kisiVeritabanindaBulundu;
	}

	public void setKisiVeritabanindaBulundu(boolean kisiVeritabanindaBulundu) {
		this.kisiVeritabanindaBulundu = kisiVeritabanindaBulundu;
	}

	public PojoKisi getKisikimlikPojo() {
		return kisikimlikPojo;
	}

	public void setKisikimlikPojo(PojoKisi kisikimlikPojo) {
		this.kisikimlikPojo = kisikimlikPojo;
	}

	
	public Telefon getTelefon2() {
		return telefon2;
	}

	public void setTelefon2(Telefon telefon2) {
		this.telefon2 = telefon2;
	}

	public Telefon getTelefon3() {
		return telefon3;
	}

	public void setTelefon3(Telefon telefon3) {
		this.telefon3 = telefon3;
	}

	public Telefon getTelefon4() {
		return telefon4;
	}

	public void setTelefon4(Telefon telefon4) {
		this.telefon4 = telefon4;
	}

	public Internetadres getInternetAdres1() {
		return internetAdres1;
	}

	public void setInternetAdres1(Internetadres internetAdres1) {
		this.internetAdres1 = internetAdres1;
	}

	public Pattern getPattern() {
		return pattern;
	}

	public KurumController getKurumController() {
		return kurumController;
	}

	public void setKurumController(KurumController kurumController) {
		this.kurumController = kurumController;
	}

	public TelefonController getTelefonController() {
		return telefonController;
	}

	public void setTelefonController(TelefonController telefonController) {
		this.telefonController = telefonController;
	}

	public InternetadresController getInternetadresController() {
		return internetadresController;
	}

	public void setInternetadresController(
			InternetadresController internetadresController) {
		this.internetadresController = internetadresController;
	}

	public BirimController getBirimController() {
		return birimController;
	}

	public void setBirimController(BirimController birimController) {
		this.birimController = birimController;
	}
	
	public String callDetailFromWizard(){ 
		return getKurumController().detailPage(null);
	}

	public Adres getAdres() {
		return adres;
	}

	public void setAdres(Adres adres) {
		this.adres = adres;
	}

	public AdresController getAdresController() {
		return adresController;
	}

	public void setAdresController(AdresController adresController) {
		this.adresController = adresController;
	}

	public String getOldValueForAdres() {
		return oldValueForAdres;
	}

	public void setOldValueForAdres(String oldValueForAdres) {
		this.oldValueForAdres = oldValueForAdres;
	}

	public String getOldValueForKurum() {
		return oldValueForKurum;
	}

	public void setOldValueForKurum(String oldValueForKurum) {
		this.oldValueForKurum = oldValueForKurum;
	}

	public boolean getVarsayilan() {
		return varsayilan;
	}

	public void setVarsayilan(boolean varsayilan) {
		this.varsayilan = varsayilan;
	}
} // class 
