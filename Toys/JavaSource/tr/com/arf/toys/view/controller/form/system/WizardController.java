package tr.com.arf.toys.view.controller.form.system;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.model.SelectItem;

import org.apache.log4j.Logger;
import org.primefaces.component.datatable.DataTable;
import org.primefaces.context.RequestContext;

import tr.com.arf.framework.db._management.DBOperator;
import tr.com.arf.framework.db.enumerated._common.EvetHayir;
import tr.com.arf.framework.db.enumerated._common.IslemTuru;
import tr.com.arf.framework.db.model._base.BaseEntity;
import tr.com.arf.framework.utility.exception.data.DBException;
import tr.com.arf.framework.utility.tool.DateUtil;
import tr.com.arf.framework.utility.tool.KeyUtil;
import tr.com.arf.toys.db.enumerated.iletisim.AdresTipi;
import tr.com.arf.toys.db.enumerated.iletisim.AdresTuru;
import tr.com.arf.toys.db.enumerated.iletisim.NetAdresTuru;
import tr.com.arf.toys.db.enumerated.iletisim.TelefonTuru;
import tr.com.arf.toys.db.enumerated.kisi.Cinsiyet;
import tr.com.arf.toys.db.enumerated.kisi.KimlikTip;
import tr.com.arf.toys.db.enumerated.kisi.KisiDurum;
import tr.com.arf.toys.db.enumerated.kisi.MedeniHal;
import tr.com.arf.toys.db.enumerated.uye.OgrenimTip;
import tr.com.arf.toys.db.enumerated.uye.UyeDurum;
import tr.com.arf.toys.db.enumerated.uye.UyeKayitDurum;
import tr.com.arf.toys.db.enumerated.uye.UyeMuafiyetTip;
import tr.com.arf.toys.db.enumerated.uye.UyeTip;
import tr.com.arf.toys.db.enumerated.uye.UyekurumDurum;
import tr.com.arf.toys.db.model.iletisim.Adres;
import tr.com.arf.toys.db.model.iletisim.Internetadres;
import tr.com.arf.toys.db.model.iletisim.Telefon;
import tr.com.arf.toys.db.model.kisi.Kisi;
import tr.com.arf.toys.db.model.kisi.Kisikimlik;
import tr.com.arf.toys.db.model.kisi.Kisiogrenim;
import tr.com.arf.toys.db.model.kisi.Kurum;
import tr.com.arf.toys.db.model.system.Birim;
import tr.com.arf.toys.db.model.system.Bolum;
import tr.com.arf.toys.db.model.system.Fakulte;
import tr.com.arf.toys.db.model.system.Ilce;
import tr.com.arf.toys.db.model.system.KurumKisi;
import tr.com.arf.toys.db.model.system.Sehir;
import tr.com.arf.toys.db.model.system.SistemParametre;
import tr.com.arf.toys.db.model.system.Ulke;
import tr.com.arf.toys.db.model.uye.Uye;
import tr.com.arf.toys.db.model.uye.Uyemuafiyet;
import tr.com.arf.toys.db.nonEntityModel.PojoAdres;
import tr.com.arf.toys.db.nonEntityModel.PojoKisi;
import tr.com.arf.toys.service.application.ApplicationDescriptor;
import tr.com.arf.toys.service.application.KPSService;
import tr.com.arf.toys.service.application.ManagedBeanLocator;
import tr.com.arf.toys.utility.tool.ToysXMLReader;
import tr.com.arf.toys.view.controller.form._base.ToysBaseController;
import tr.com.arf.toys.view.controller.form.iletisim.AdresController;
import tr.com.arf.toys.view.controller.form.iletisim.InternetadresController;
import tr.com.arf.toys.view.controller.form.iletisim.TelefonController;
import tr.com.arf.toys.view.controller.form.kisi.KisiController;
import tr.com.arf.toys.view.controller.form.kisi.KisikimlikController;
import tr.com.arf.toys.view.controller.form.uye.UyeController;

@ManagedBean(name = "wizardController")
@ViewScoped
public class WizardController extends ToysBaseController {
	private DataTable table2 = new DataTable();
	private static final long serialVersionUID = 6531152165884980748L;

	/* Log4j Logger */
	protected static Logger epostaLogger = Logger.getLogger("eposta");

	private Uye uye;
	private Kisiogrenim kisiogrenim;
	private int wizardStep = 1;
	private Kisi kisi;
	private Kisikimlik kisikimlik;

	private Adres kpsAdres;
	private Adres kpsDigerAdres;

	private Adres adres2;
	private Adres adres3;

	private Telefon telefon1;
	private Telefon telefon2;
	private Telefon telefon3;
	private Telefon telefon4;

	private Internetadres internetAdres1;

	private Kurum kurum;
	private Birim birim;
	private String detailEditType;
	private String sicilno = null;

	private boolean kpsSorguSonucDondu = false;
	private boolean kpsKaydiBulundu = false;
	private boolean kisiVeritabanindaBulundu = false;
	private Boolean kpsAdresDondu = false;

	private KurumKisi kurumKisi;
	private SelectItem[] selectAdresListForUyeKurum = new SelectItem[0];
	private PojoKisi kisikimlikPojo = new PojoKisi();

	private Boolean iletisimEmail = true;
	private Boolean iletisimSms = true;
	private Boolean iletisimPosta = true;

	private Boolean isyeriadresvarsayilan = false;
	private Boolean beyanadresvarsayilan = false;

	public WizardController() {
		super(Uye.class);
		setDefaultValues(false);
		prepareForWizard();
		setTable(null);
		queryAction();
	}

	private void prepareForWizard() {
		this.wizardStep = 1;
		super.insert();
		try {
			if (getSessionUser().getPersonelRef() != null) {
				if (getSessionUser().getPersonelRef().getBirimRef() != null) {
					getUye().setBirimRef(getSessionUser().getPersonelRef().getBirimRef());
				}
			}
		} catch (Exception e) {
			logYaz("Exception @" + getModelName() + "Controller :", e);
		}
		this.kisi = new Kisi();
		this.kisi.setIletisimtercih(EvetHayir._NULL);
		this.kisi.setKimliknotip(KimlikTip._TCKIMLIKNO);
		this.kisi.setAktif(EvetHayir._EVET);
		this.kisi.setAd("");
		this.kisi.setSoyad("");
		this.kisikimlik = new Kisikimlik();

		this.kisiogrenim = new Kisiogrenim();
		this.kisiogrenim.setOgrenimtip(OgrenimTip._LISANS);
		this.kisiogrenim.setDenklikdurum(EvetHayir._HAYIR);
		Calendar now = Calendar.getInstance();
		int year = now.get(Calendar.YEAR);
		this.kisiogrenim.setMezuniyettarihi(year);
		this.kurum = new Kurum();
		this.birim = new Birim();
		removeObjectFromSessionFilter(ApplicationDescriptor._UYE_MODEL);

		this.kurumKisi = new KurumKisi();
		selectAdresListForUyeKurum = new SelectItem[0];
		this.kisikimlikPojo = new PojoKisi();

		this.kpsAdres = new Adres();
		this.kpsDigerAdres = new Adres();
		this.adres2 = new Adres();
		this.adres3 = new Adres();
		this.telefon1 = new Telefon();
		telefon1.setTelefonturu(TelefonTuru._EVTELEFONU);
		this.telefon2 = new Telefon();
		telefon2.setTelefonturu(TelefonTuru._ISTELEFONU);
		this.telefon3 = new Telefon();
		telefon3.setTelefonturu(TelefonTuru._GSM);
		this.telefon4 = new Telefon();
		telefon4.setTelefonturu(TelefonTuru._FAKS);
		this.internetAdres1 = new Internetadres();
		internetAdres1.setNetadresturu(NetAdresTuru._EPOSTA);
		this.detailEditType = "";
		this.kpsSorguSonucDondu = false;
		this.kpsKaydiBulundu = false;
		this.kisiVeritabanindaBulundu = false;
	}

	public void newDetail(String detailType) {
		this.detailEditType = detailType;
		this.kurum = new Kurum();
		this.birim = new Birim();
		RequestContext context = RequestContext.getCurrentInstance();
		context.update("dialogUpdateBox");
	}

	public void saveKurum() throws DBException {
		getDBOperator().insert(this.kurum);
		this.kurumKisi.setKurumRef(this.kurum);
		RequestContext context = RequestContext.getCurrentInstance();
		this.detailEditType = null;
		context.update("dialogUpdateBox");
		queryAction();
		context.update("kurumEditPanel");
		context.update("uyeForm");
	}

	public Boolean getKpsAdresDondu() {
		return kpsAdresDondu;
	}

	public void setKpsAdresDondu(Boolean kpsAdresDondu) {
		this.kpsAdresDondu = kpsAdresDondu;
	}

	private java.util.Date uyeliktarihi = new Date();

	public java.util.Date getUyeliktarihi() {
		return uyeliktarihi;
	}

	public void setUyeliktarihi(java.util.Date uyeliktarihi) {
		this.uyeliktarihi = uyeliktarihi;
	}

	public Boolean getSistemYoneticisiRole() {
		try {
			if (getDBOperator().recordCount(ApplicationDescriptor._KULLANICIROLE_MODEL, "o.kullaniciRef.rID=" + getSessionUser().getKullanici().getRID() + " AND o.roleRef.rID=1") > 0) {
				return true;
			}
		} catch (DBException e) {
			logYaz("Exception @" + getModelName() + "Controller :", e);
		}
		return false;
	}

	@SuppressWarnings("unchecked")
	public String finishWizard() throws DBException {
		DBOperator dbOperator = getDBOperator();
		try {

			if (this.iletisimEmail) {
				this.uye.setIletisimEmail(EvetHayir._EVET);
			} else {
				this.uye.setIletisimEmail(EvetHayir._HAYIR);
			}
			if (this.iletisimSms) {
				this.uye.setIletisimSms(EvetHayir._EVET);
			} else {
				this.uye.setIletisimSms(EvetHayir._HAYIR);
			}
			if (this.iletisimPosta) {
				this.uye.setIletisimPosta(EvetHayir._EVET);
			} else {
				this.uye.setIletisimPosta(EvetHayir._HAYIR);
			}
			// Tum Save'ler yapilacak
			this.uye.setKisiRef(getKisi());
			this.uye.setUyedurum(UyeDurum._PASIFUYE);
			this.uye.setKayitdurum(UyeKayitDurum._TASLAK);
			if (getUyeliktarihi() != null) {
				this.uye.setUyeliktarih(getUyeliktarihi());
			} else {
				this.uye.setUyeliktarih(new Date());
			}
			if (sicilno == null || sicilno.isEmpty()) {
				this.uye.setSicilno(null);
			}
			dbOperator.insert(this.uye);
			if (getUyeController().isLoggable()) {
				logKaydet(this.uye, IslemTuru._EKLEME, "");
			}

			if (this.kurumKisi.getKurumRef() != null) {
				if (getDBOperator().recordCount(KurumKisi.class.getSimpleName(), "o.kisiRef.rID=" + this.uye.getKisiRef().getRID() + " AND o.kurumRef=  " + this.kurumKisi.getKurumRef().getRID()) > 0) {
					this.kurumKisi = (KurumKisi) getDBOperator().load(KurumKisi.class.getSimpleName(), "o.kisiRef.rID=" + this.uye.getKisiRef().getRID() + " AND o.kurumRef=  " + this.kurumKisi.getKurumRef().getRID(), "").get(0);
					this.kurumKisi.setVarsayilan(EvetHayir._EVET);
					getDBOperator().update(this.kurumKisi);
				} else {
					this.kurumKisi.setRID(null);
					this.kurumKisi.setKisiRef(this.uye.getKisiRef());
					this.kurumKisi.setVarsayilan(EvetHayir._EVET);
					this.kurumKisi.setUyekurumDurum(UyekurumDurum._CALISIYOR);
					if (getKurumKisi().getRID() == null) {
						if (getDBOperator().recordCount(KurumKisi.class.getSimpleName(), "o.kisiRef.rID=" + getKurumKisi().getKisiRef().getRID()) > 0) {
							List<KurumKisi> kurumKisiListesi = getDBOperator().load(KurumKisi.class.getSimpleName(), "o.kisiRef.rID=" + getKurumKisi().getKisiRef().getRID(), "");
							for (KurumKisi entity : kurumKisiListesi) {
								entity.setUyekurumDurum(UyekurumDurum._AYRILMIS);
								entity.setVarsayilan(EvetHayir._HAYIR);
								getDBOperator().update(entity);
							}
						}
					}
					dbOperator.insert(this.kurumKisi);
				}

			}

			if (getDBOperator().recordCount(Adres.class.getSimpleName(), "o.kisiRef.rID=" + this.kisi.getRID() + " AND o.adresturu=" + AdresTuru._EVADRESI.getCode()) == 0) {
				if (this.adres3.getAdrestipi() == AdresTipi._YURTDISIADRESI) {
					if (this.adres3.getYabanciulke() != null && !isEmpty(this.adres3.getYabanciadres()) && this.adres3.getYabancisehir() != null) {
						this.adres3.setKisiRef(getKisi());
						this.adres3.setVarsayilan(EvetHayir._EVET);
						dbOperator.insert(this.adres3);
					}
				} else if (this.adres3.getAdrestipi() != AdresTipi._YURTDISIADRESI) {
					if (this.adres3.getUlkeRef() != null && !isEmpty(this.adres3.getAcikAdres()) && this.adres3.getSehirRef() != null && this.adres3.getIlceRef() != null) {
						if (getDBOperator().recordCount(Adres.class.getSimpleName(),
								"o.kisiRef.rID=" + this.kisi.getRID() + " AND  ( o.adresturu=" + AdresTuru._KPSADRESI.getCode() + " OR o.adresturu= " + AdresTuru._KPSDIGERADRESI.getCode() + " ) ") > 0) {
							this.adres3.setKisiRef(getKisi());
							this.adres3.setVarsayilan(EvetHayir._EVET);
							if (this.adres3.getAdresturu() != null) {
								this.adres3.setAdresturu(AdresTuru._EVADRESI);
							}
							dbOperator.insert(this.adres3);
						}

					}
				}
			}

			if (getDBOperator().recordCount(Adres.class.getSimpleName(), "o.kisiRef.rID=" + this.kisi.getRID() + " AND o.adresturu=" + AdresTuru._ISADRESI.getCode()) == 0) {
				if (this.adres2.getAdrestipi() == AdresTipi._YURTDISIADRESI) {
					if (this.adres2.getYabanciulke() != null && !isEmpty(this.adres2.getYabanciadres()) && this.adres2.getYabancisehir() != null) {
						this.adres2.setKurumRef(null);
						this.adres2.setKisiRef(getKisi());
						this.adres2.setVarsayilan(EvetHayir._HAYIR);
						dbOperator.insert(this.adres2);
					}
				} else if (this.adres2.getAdrestipi() != AdresTipi._YURTDISIADRESI) {
					if (this.adres2.getUlkeRef() != null && !isEmpty(this.adres2.getAcikAdres()) && this.adres2.getSehirRef() != null && this.adres2.getIlceRef() != null) {
						this.adres2.setKurumRef(null);
						this.adres2.setKisiRef(getKisi());
						this.adres2.setVarsayilan(EvetHayir._HAYIR);
						if (this.adres2.getAdresturu() == null) {
							this.adres2.setAdresturu(AdresTuru._ISADRESI);
						}
						dbOperator.insert(this.adres2);
					}
				}
			}
			if (!isEmpty(this.telefon1.getTelefonno())) {
				if (getDBOperator().recordCount(Telefon.class.getSimpleName(), "o.kisiRef.rID=" + getKisi().getRID() + " AND o.telefonturu=" + this.telefon1.getTelefonturu().getCode()) > 0) {
					List<Telefon> telefon2Listesi = getDBOperator().load(Telefon.class.getSimpleName(), "o.kisiRef.rID=" + getKisi().getRID() + " AND o.telefonturu=" + this.telefon1.getTelefonturu().getCode(), "");
					for (Telefon entity : telefon2Listesi) {
						if (entity.getTelefonno().equals(this.telefon1.getTelefonno())) {
							entity.setVarsayilan(EvetHayir._EVET);
							getDBOperator().update(entity);
						} else {
							entity.setVarsayilan(EvetHayir._HAYIR);
							getDBOperator().update(entity);
						}
					}
				} else {
					this.telefon1.setKisiRef(this.kisi);
					this.telefon1.setVarsayilan(EvetHayir._HAYIR);
					dbOperator.insert(telefon1);
				}
			}
			if (!isEmpty(this.telefon2.getTelefonno())) {
				if (getDBOperator().recordCount(Telefon.class.getSimpleName(), "o.kisiRef.rID=" + getKisi().getRID() + " AND o.telefonturu=" + this.telefon2.getTelefonturu().getCode()) > 0) {
					List<Telefon> telefon2Listesi = getDBOperator().load(Telefon.class.getSimpleName(), "o.kisiRef.rID=" + getKisi().getRID() + " AND o.telefonturu=" + this.telefon2.getTelefonturu().getCode(), "");
					for (Telefon entity : telefon2Listesi) {
						if (entity.getTelefonno().equals(this.telefon2.getTelefonno())) {
							entity.setVarsayilan(EvetHayir._EVET);
							getDBOperator().update(entity);
						} else {
							entity.setVarsayilan(EvetHayir._HAYIR);
							getDBOperator().update(entity);
						}
					}
				} else {
					this.telefon2.setKisiRef(this.kisi);
					this.telefon2.setVarsayilan(EvetHayir._HAYIR);
					dbOperator.insert(telefon2);
				}
			}
			if (!isEmpty(this.telefon3.getTelefonno())) {
				if (getDBOperator().recordCount(Telefon.class.getSimpleName(), "o.kisiRef.rID=" + getKisi().getRID() + " AND o.telefonturu=" + this.telefon3.getTelefonturu().getCode()) > 0) {
					List<Telefon> telefon3Listesi = getDBOperator().load(Telefon.class.getSimpleName(), "o.kisiRef.rID=" + getKisi().getRID() + " AND o.telefonturu=" + this.telefon3.getTelefonturu().getCode(), "");
					for (Telefon entity : telefon3Listesi) {
						if (entity.getTelefonno().equals(this.telefon3.getTelefonno())) {
							entity.setVarsayilan(EvetHayir._EVET);
							getDBOperator().update(entity);
						} else {
							entity.setVarsayilan(EvetHayir._HAYIR);
							getDBOperator().update(entity);
						}
					}
				} else {
					this.telefon3.setKisiRef(this.kisi);
					this.telefon3.setVarsayilan(EvetHayir._EVET);
					dbOperator.insert(telefon3);
				}
			}
			if (!isEmpty(this.telefon4.getTelefonno())) {
				if (getDBOperator().recordCount(Telefon.class.getSimpleName(), "o.kisiRef.rID=" + getKisi().getRID() + " AND o.telefonturu=" + this.telefon4.getTelefonturu().getCode()) > 0) {
					List<Telefon> telefon4Listesi = getDBOperator().load(Telefon.class.getSimpleName(), "o.kisiRef.rID=" + getKisi().getRID() + " AND o.telefonturu=" + this.telefon4.getTelefonturu().getCode(), "");
					for (Telefon entity : telefon4Listesi) {
						if (entity.getTelefonno().equals(this.telefon4.getTelefonno())) {
							entity.setVarsayilan(EvetHayir._EVET);
							getDBOperator().update(entity);
						} else {
							entity.setVarsayilan(EvetHayir._HAYIR);
							getDBOperator().update(entity);
						}
					}
				} else {
					this.telefon4.setKisiRef(this.kisi);
					this.telefon4.setVarsayilan(EvetHayir._HAYIR);
					dbOperator.insert(telefon4);
				}
			}
			if (!isEmpty(this.internetAdres1.getNetadresmetni())) {
				if (getDBOperator().recordCount(Internetadres.class.getSimpleName(), "o.kisiRef.rID=" + getKisi().getRID()) > 0) {
					List<Internetadres> internetAdresListesi = getDBOperator().load(Internetadres.class.getSimpleName(), "o.kisiRef.rID=" + getKisi().getRID(), "");
					for (Internetadres entity : internetAdresListesi) {
						if (entity.getNetadresmetni().equals(this.internetAdres1.getNetadresmetni())) {
							entity.setVarsayilan(EvetHayir._EVET);
							getDBOperator().update(entity);
						} else {
							entity.setVarsayilan(EvetHayir._HAYIR);
							String eskiMail = entity.getNetadresmetni();
							getDBOperator().update(entity);
							logYaz("GUNCELLENDI | " + epostaKisiLog() + eskiMail + " | " + internetAdres1.getNetadresmetni());
							epostaLogger.info("GUNCELLENDI | " + epostaKisiLog() + eskiMail + " | " + internetAdres1.getNetadresmetni());
						}
					}
				} else {
					this.internetAdres1.setKisiRef(this.kisi);
					this.internetAdres1.setVarsayilan(EvetHayir._EVET);
					dbOperator.insert(internetAdres1);
					logYaz("EKLENDI | " + epostaKisiLog() + internetAdres1.getNetadresmetni());
					epostaLogger.info("EKLENDI | " + epostaKisiLog() + internetAdres1.getNetadresmetni());
				}
			}

			if (getUye().getOgrenciNo() != null && getUye().getSinif() != null) {
				Uyemuafiyet uyeMuafiyet1 = new Uyemuafiyet();
				uyeMuafiyet1.setBaslangictarih(new Date());
				uyeMuafiyet1.setMuafiyettip(UyeMuafiyetTip._OGRENCI);
				uyeMuafiyet1.setUyeRef(getUye());
				getDBOperator().insert(uyeMuafiyet1);

			}
			if (getDBOperator().recordCount(ApplicationDescriptor._KISIOGRENIM_MODEL,
					"o.kisiRef.rID=" + this.kisi.getRID() + " AND o.ogrenimtip=" + OgrenimTip._LISANS.getCode() + " AND o.varsayilan=" + EvetHayir._EVET.getCode() + " AND o.universiteRef.rID=" + this.kisiogrenim.getUniversiteRef().getRID()
							+ " AND o.bolumRef.rID=" + this.kisiogrenim.getBolumRef().getRID() + " AND o.fakulteRef.rID=" + this.kisiogrenim.getFakulteRef().getRID() + " AND o.lisansunvan='" + this.kisiogrenim.getLisansunvan()
							+ "' AND o.diplomano='" + this.kisiogrenim.getDiplomano() + "' AND o.denklikdurum=" + EvetHayir._EVET.getCode() + " AND o.mezuniyettarihi=" + this.kisiogrenim.getMezuniyettarihi() + " AND o.aciklama= '"
							+ this.kisiogrenim.getAciklama() + "'") <= 0) {

				this.kisiogrenim.setVarsayilan(EvetHayir._EVET);
				this.kisiogrenim.setKisiRef(this.kisi);
				getDBOperator().insert(this.kisiogrenim);

			}

			// Uye detay sayfasina gidiyor...
			if (getUye().getRID() != null) {
				getUyeController().setSelectedRID(getUye().getRID() + "");
				ManagedBeanLocator.locateSessionUser().addToSessionFilters(Uye.class.getSimpleName(), getUye());
				return ManagedBeanLocator.locateMenuController().gotoPage("menu_" + Uye.class.getSimpleName() + "Detay");
			} else {
				return ManagedBeanLocator.locateMenuController().gotoPage("menu_" + Uye.class.getSimpleName());
			}

		} catch (Exception e) {
			logYaz("Error @finishWizard:" + e.getMessage());
			logYaz("Exception @" + getModelName() + "Controller :", e);
			createGenericMessage(KeyUtil.getMessageValue("islem.hata"), FacesMessage.SEVERITY_ERROR);
		}
		return "";
	}

	private String epostaKisiLog() {
		StringBuilder builder = new StringBuilder();
		builder.append(kisi.getKimlikno());
		builder.append(" | ");
		builder.append(kisi.getAd());
		builder.append(" ");
		builder.append(kisi.getSoyad());
		builder.append(" | ");
		return builder.toString();
	}

	public boolean isUyeBulundu() {
		if (!isEmpty(getUyeController().getSelectedRID())) {
			return true;
		} else {
			return false;
		}
	}

	private Boolean zorunlu = false;

	public Boolean getZorunlu() {
		return zorunlu;
	}

	public void setZorunlu(Boolean zorunlu) {
		this.zorunlu = zorunlu;
	}

	private Boolean kisiAdresDondu = false;

	public Boolean getKisiAdresDondu() {
		return kisiAdresDondu;
	}

	public void setKisiAdresDondu(Boolean kisiAdresDondu) {
		this.kisiAdresDondu = kisiAdresDondu;
	}

	private Boolean kpsZorla = false;

	public Boolean getKpsZorla() {
		return kpsZorla;
	}

	public void setKpsZorla(Boolean kpsZorla) {
		this.kpsZorla = kpsZorla;
	}

	public Kisikimlik getKisikimlik(Long kisiRID) {
		if (kisiRID != null) {
			@SuppressWarnings("unchecked")
			List<Kisikimlik> kimlikListesi = getDBOperator().load(ApplicationDescriptor._KISIKIMLIK_MODEL, "o.kisiRef.rID=" + kisiRID, "o.rID");
			if (kimlikListesi != null && kimlikListesi.size() > 0) {
				return kimlikListesi.get(0);
			} else {
				return null;
			}
		} else {
			return null;
		}
	}

	public Boolean getSicilnosorgulamasi() {
		if (getSessionUser().isSuperUser()) {
			return true;
		}
		return false;
	}

	@SuppressWarnings("unchecked")
	public void kimlikBilgisiSorgula() throws ParseException {
		if (this.kisi.getKimlikno() != null) {
			setKisikimlik(new Kisikimlik());
			this.kpsKaydiBulundu = false;
			this.kpsSorguSonucDondu = false;
			this.kisiVeritabanindaBulundu = false;
			removeObjectFromSessionFilter(ApplicationDescriptor._UYE_MODEL);
			KPSService kpsService = KPSService.getInstance();
			String kpsKisi;
			try {
				if (getDBOperator().recordCount(Uye.class.getSimpleName(), "o.kisiRef.kimlikno = " + this.kisi.getKimlikno() + "") > 0) {
					// logYaz("Uye found...");
					createCustomMessage("Bu üye sistemde tanımlı! Detayını görmek için üye listesine tıklayınız!", FacesMessage.SEVERITY_ERROR, "kimliknoInputTextKisi:kimliknoKisi");
					// Uye varsa islem kesilecek
					getUyeController().setSelectedRID(((Uye) getDBOperator().load(Uye.class.getSimpleName(), "o.kisiRef.kimlikno = '" + this.kisi.getKimlikno() + "'", "o.rID").get(0)).getRID() + "");
					return;
				}
				if (getDBOperator().recordCount(Kisi.class.getSimpleName(), "o.kimlikno = " + this.kisi.getKimlikno()) > 0) {
					Kisi bulunanKisi = (Kisi) getDBOperator().find(Kisi.class.getSimpleName(), "o.kimlikno=" + this.kisi.getKimlikno() + "");
					setKisi(bulunanKisi);
					if (getKisikimlik(bulunanKisi.getRID()) != null) {
						setKisikimlik(getKisikimlik(bulunanKisi.getRID()));
						this.zorunlu = true;
						this.kisiVeritabanindaBulundu = true;
						getUyeController().selectedRIDNull();
						if (getDBOperator().recordCount(Adres.class.getSimpleName(),
								"o.kisiRef.kimlikno=" + this.kisi.getKimlikno() + " AND  ( o.adresturu=" + AdresTuru._KPSADRESI.getCode() + " OR o.adresturu= " + AdresTuru._KPSDIGERADRESI.getCode() + " ) ") > 0) {
							this.zorunlu = true;
							List<Adres> adresList = getDBOperator().load(Adres.class.getSimpleName(),
									"o.kisiRef.kimlikno=" + this.kisi.getKimlikno() + " AND  ( o.adresturu=" + AdresTuru._KPSADRESI.getCode() + " OR o.adresturu= " + AdresTuru._KPSDIGERADRESI.getCode() + " ) ", "");
							for (Adres entity : adresList) {
								if (entity.getAdresturu().getCode() == AdresTuru._KPSADRESI.getCode()) {
									setKpsAdresDondu(true);
									setKpsAdres(entity);
								}
								if (entity.getAdresturu().getCode() == AdresTuru._KPSDIGERADRESI.getCode()) {
									setKpsDigerAdres(entity);
									setKpsAdresDondu(true);
								}
							}

						}
						if (getDBOperator().recordCount(Adres.class.getSimpleName(),
								"o.kisiRef.kimlikno=" + this.kisi.getKimlikno() + " AND o.adresturu=" + AdresTuru._EVADRESI.getCode() + " AND o.varsayilan=" + EvetHayir._EVET.getCode()) > 0) {
							setAdres3((Adres) getDBOperator()
									.load(Adres.class.getSimpleName(), "o.kisiRef.kimlikno=" + this.kisi.getKimlikno() + " AND o.adresturu=" + AdresTuru._EVADRESI.getCode() + " AND o.varsayilan=" + EvetHayir._EVET.getCode(), "").get(0));
						}

					} else {
						String kpsAdres = kpsService.kisiAdresBilgisiGetirYeni(this.kisi.getKimlikno());
						this.zorunlu = true;
						// PojoAdres kpsPojo = XmlHelper.readXmlStringForAdres(kpsAdres, AdresTuru._KPSADRESI);
						// PojoAdres kpsDigerPojo = XmlHelper.readXmlStringForAdres(kpsAdres, AdresTuru._KPSDIGERADRESI);
						PojoAdres kpsPojo = ToysXMLReader.readXmlStringForAdres(kpsAdres, "YerlesimYeriAdresi");
						PojoAdres kpsDigerPojo = ToysXMLReader.readXmlStringForAdres(kpsAdres, "DigerAdresBilgileri");
						if (kpsPojo != null && (kpsPojo.getHataBilgisiKodu() == null || kpsPojo.getHataBilgisiKodu().trim().equals(""))) {
							setKpsAdresDondu(true);
							setKpsAdres(kisiAdresAktar(kpsPojo, AdresTuru._KPSADRESI));
							getKpsAdres().setKisiRef(this.kisi);
							getDBOperator().insert(getKpsAdres());
						}
						if (kpsDigerPojo != null && (kpsDigerPojo.getHataBilgisiKodu() == null || kpsDigerPojo.getHataBilgisiKodu().trim().equals(""))) {
							setKpsAdresDondu(true);
							setKpsDigerAdres(kisiAdresAktar(kpsDigerPojo, AdresTuru._KPSDIGERADRESI));
							getKpsDigerAdres().setKisiRef(this.kisi);
							getDBOperator().insert(getKpsAdres());
						}
					}

				} else {
					System.out.println("Kisi kimlik bilgisi aliniyor... From KPS...");
					kpsKisi = kpsService.kisiKimlikBilgisiGetirYeni(this.kisi.getKimlikno());
					if (kpsKisi == null) {
						SistemParametre sistemParametre = new SistemParametre();
						if (getDBOperator().recordCount(SistemParametre.class.getSimpleName(), "") > 0) {
							sistemParametre = (SistemParametre) getDBOperator().load(SistemParametre.class.getSimpleName(), "", "o.rID").get(0);
						}
						if (sistemParametre != null) {
							if (sistemParametre.getKpszorla() == EvetHayir._HAYIR) {
								createCustomMessage("Girdiğiniz kimlik numarası KPS sisteminde bulunamadı!", FacesMessage.SEVERITY_ERROR, "kimliknoInputTextKisi:kimliknoKisi");
								this.kisikimlik.setKimlikno(this.kisi.getKimlikno());
								setUyeController(new UyeController());
								setKpsZorla(true);
								return;
							} else {
								setKpsZorla(false);
								createGenericMessage("Girdiğiniz kimlik numarası KPS sisteminde bulunamadı! \nSistem Yönetimi KPS'de bulunmayan kimlik numarasıyla giriş yapılmasını engelledi.Bu yüzden kişiyi üye yapamazsınız!",
										FacesMessage.SEVERITY_ERROR);
								return;
							}
						}
					}
					System.out.println("KPS basarili...");
					// this.kisikimlikPojo = XmlHelper.readXmlString(kpsKisi.trim());
					this.kisikimlikPojo = ToysXMLReader.readXmlStringForKimlik(kpsKisi.trim());
					System.out.println("this.kisikimlikPojo dondu. Parse basarili.");
					this.kpsSorguSonucDondu = true;
					this.zorunlu = true;
					this.kpsKaydiBulundu = true;
					this.kisi.setAd(this.kisikimlikPojo.getAd());
					this.kisi.setSoyad(this.kisikimlikPojo.getSoyad());
					kisiKimlikBilgisiAktar();
					System.out.println("------------- 4 ----------------");
					String kpsAdres = kpsService.kisiAdresBilgisiGetirYeni(this.kisi.getKimlikno());
					System.out.println("------------- 5 ---------------- kpsAdres : " + kpsAdres);
					setKpsAdresDondu(true);
					// PojoAdres kpsPojo = XmlHelper.readXmlStringForAdres(kpsAdres, AdresTuru._KPSADRESI);
					// PojoAdres kpsDigerPojo = XmlHelper.readXmlStringForAdres(kpsAdres, AdresTuru._KPSDIGERADRESI);

					PojoAdres kpsPojo = ToysXMLReader.readXmlStringForAdres(kpsAdres, "YerlesimYeriAdresi");
					PojoAdres kpsDigerPojo = ToysXMLReader.readXmlStringForAdres(kpsAdres, "DigerAdresBilgileri");

					if (kpsPojo != null && (kpsPojo.getHataBilgisiKodu() == null || kpsPojo.getHataBilgisiKodu().trim().equals(""))) {
						setKpsAdres(kisiAdresAktar(kpsPojo, AdresTuru._KPSADRESI));
						getKpsAdres().setKisiRef(getKisi());
						// getDBOperator().insert(getKpsAdres());
					}
					if (kpsDigerPojo != null && (kpsDigerPojo.getHataBilgisiKodu() == null || kpsDigerPojo.getHataBilgisiKodu().trim().equals(""))) {
						setKpsDigerAdres(kisiAdresAktar(kpsDigerPojo, AdresTuru._KPSDIGERADRESI));
						getKpsDigerAdres().setKisiRef(getKisi());
						// getDBOperator().insert(getKpsAdres());
					}
				}

			} catch (Exception e) {
				this.kpsSorguSonucDondu = false;
				this.kpsKaydiBulundu = false;
				createGenericMessage("KPS Sorgulaması Başarısız oldu!", FacesMessage.SEVERITY_ERROR);
				logYaz("ERROR : " + e.getMessage());
				logYaz("Exception @WizardController :", e);
			}
		}

	}

	private void kisiKimlikBilgisiAktar() throws Exception {
		if (isNumber(this.kisikimlikPojo.getCinsiyetKod())) {
			this.kisikimlik.setCinsiyet(Cinsiyet.getWithCode(Integer.parseInt(this.kisikimlikPojo.getCinsiyetKod())));
		}
		this.kisikimlik.setKimlikno(this.kisikimlikPojo.getTcKimlikNo());
		this.kisikimlik.setBabaad(this.kisikimlikPojo.getBabaAdi());
		this.kisikimlik.setAnaad(this.kisikimlikPojo.getAnaAdi());
		if (isNumber(this.kisikimlikPojo.getMedeniHalKodu())) {
			this.kisikimlik.setMedenihal(MedeniHal.getWithCode(Integer.parseInt(this.kisikimlikPojo.getMedeniHalKodu())));
		}
		this.kisikimlik.setDogumyer(this.kisikimlikPojo.getDogumYeri());
		if (this.kisikimlikPojo.getDogumTarihi() != null && !this.kisikimlikPojo.getDogumTarihi().toString().equalsIgnoreCase("null/null/null")) {
			this.kisikimlik.setDogumtarih(DateUtil.createDateObject(this.kisikimlikPojo.getDogumTarihi()));
		}
		if (this.kisikimlikPojo.getOlumTarihi() != null && !this.kisikimlikPojo.getOlumTarihi().toString().equalsIgnoreCase("null/null/null")) {
			this.kisikimlik.setOlumtarih(DateUtil.createDateObject(this.kisikimlikPojo.getOlumTarihi()));
		}
		if (isNumber(this.kisikimlikPojo.getDurumKod())) {
			this.kisikimlik.setKisidurum(KisiDurum.getWithCode(Integer.parseInt(this.kisikimlikPojo.getDurumKod())));
		}
		String plakakodu = kisikimlikPojo.getNufusaKayitliOlduguIlKodu();
		if (kisikimlikPojo.getNufusaKayitliOlduguIlKodu() != null) {
			int ilkodulength = kisikimlikPojo.getNufusaKayitliOlduguIlKodu().length();
			int zero = 0;
			if (ilkodulength == 1) {
				plakakodu = zero + plakakodu;
			}
			if (getDBOperator().recordCount(Sehir.class.getSimpleName(), "o.ilKodu='" + plakakodu + "'") > 0) {
				this.kisikimlik.setSehirRef((Sehir) getDBOperator().find(Sehir.class.getSimpleName(), "o.ilKodu='" + plakakodu + "'"));
				changeIlce(this.kisikimlik.getSehirRef().getRID());
				this.kisikimlik.setIlceRef((Ilce) getDBOperator().find(Ilce.class.getSimpleName(), "o.ilcekodu='" + this.kisikimlikPojo.getNufusaKayitliOlduguIlceKodu() + "'"));
				this.kisikimlik.setMahalle(kisikimlikPojo.getMahalle());
			}
		}

		this.kisikimlik.setKpsdogrulamatarih(new Date());
		this.kisikimlik.setMahalle(this.kisikimlikPojo.getMahalle());
		this.kisikimlik.setCiltno(this.kisikimlikPojo.getCiltNo());
		this.kisikimlik.setAileno(this.kisikimlikPojo.getAileSiraNo());
		this.kisikimlik.setSirano(this.kisikimlikPojo.getBireySiraNo());
	}

	private Adres kisiAdresAktar(PojoAdres kpsPojo, AdresTuru adresTuru) throws Exception {
		Adres yeniAdres = new Adres();
		yeniAdres.setAdresturu(adresTuru);
		yeniAdres.setAdrestipi(kpsPojo.getAdrestipi());
		yeniAdres.setAcikAdres(kpsPojo.getAcikAdres());
		yeniAdres.setAdresNo(kpsPojo.getAdresNo());
		if (!isEmpty(kpsPojo.getBeyanTarihi()) && !kpsPojo.getBeyanTarihi().toString().equalsIgnoreCase("null/null/null")) {
			yeniAdres.setBeyanTarihi(DateUtil.createDateObject(kpsPojo.getBeyanTarihi()));
		}
		yeniAdres.setBinaAda(kpsPojo.getBinaAda());
		yeniAdres.setBinaBlokAdi(kpsPojo.getBinaBlokAdi());
		yeniAdres.setBinaKodu(kpsPojo.getBinaKodu());
		yeniAdres.setBinaPafta(kpsPojo.getBinaPafta());
		yeniAdres.setBinaParsel(kpsPojo.getBinaParsel());
		yeniAdres.setBinaPafta(kpsPojo.getBinaPafta());
		yeniAdres.setBinaSiteAdi(kpsPojo.getBinaSiteAdi());
		yeniAdres.setCsbm(kpsPojo.getCsbm());
		yeniAdres.setCsbmKodu(kpsPojo.getCsbmKodu());
		yeniAdres.setDisKapiNo(kpsPojo.getDisKapiNo());
		yeniAdres.setIcKapiNo(kpsPojo.getIcKapiNo());
		if (!isEmpty(kpsPojo.getIlKodu()) && isNumber(kpsPojo.getIlKodu().trim())) {
			if (kpsPojo.getIlKodu().charAt(0) == '0') {
				if (getDBOperator().recordCount(Sehir.class.getSimpleName(), "o.ilKodu='" + kpsPojo.getIlKodu().charAt(1) + "'") > 0) {
					yeniAdres.setSehirRef((Sehir) getDBOperator().find(Sehir.class.getSimpleName(), "o.ilKodu='" + kpsPojo.getIlKodu().charAt(1) + "'"));
				}
			} else {
				if (getDBOperator().recordCount(Sehir.class.getSimpleName(), "o.ilKodu='" + kpsPojo.getIlKodu() + "'") > 0) {
					yeniAdres.setSehirRef((Sehir) getDBOperator().find(Sehir.class.getSimpleName(), "o.ilKodu='" + kpsPojo.getIlKodu() + "'"));
				}
			}
		}
		if (!isEmpty(kpsPojo.getIlceKodu()) && isNumber(kpsPojo.getIlceKodu().trim())) {
			if (getDBOperator().recordCount(Ilce.class.getSimpleName(), "o.ilcekodu='" + kpsPojo.getIlceKodu() + "'") > 0) {
				// logYaz("Ilce " + ((Ilce)
				// getDBOperator().load(Ilce.class.getSimpleName(),
				// "o.ilcekodu='" + kpsPojo.getIlceKodu() +
				// "'","").get(0)).getAd());
				yeniAdres.setIlceRef((Ilce) getDBOperator().find(Ilce.class.getSimpleName(), "o.ilcekodu='" + kpsPojo.getIlceKodu() + "'"));
			}
		}
		yeniAdres.setKpsGuncellemeTarihi(new Date());
		yeniAdres.setMahalle(kpsPojo.getMahalle());
		yeniAdres.setMahalleKodu(kpsPojo.getMahalleKodu());
		if (kpsPojo.getTasinmaTarihi() != null && !kpsPojo.getTasinmaTarihi().toString().equalsIgnoreCase("null/null/null")) {
			yeniAdres.setTasinmaTarihi(DateUtil.createDateObject(kpsPojo.getTasinmaTarihi()));
		}
		if (kpsPojo.getTescilTarihi() != null && !kpsPojo.getTescilTarihi().toString().equalsIgnoreCase("null/null/null")) {
			yeniAdres.setTescilTarihi(DateUtil.createDateObject(kpsPojo.getTescilTarihi()));
		}
		yeniAdres.setVarsayilan(EvetHayir._HAYIR);
		return yeniAdres;
	}

	@Override
	public void wizardForward() {

		if (this.wizardStep == 1) {
			try {
				if (getSessionUser().isSuperUser()) {
					if (!sicilno.isEmpty()) {
						if (sicilno.length() != 6) {
							createGenericMessage("Girilen Değer 6 karakterden kısa olamaz!", FacesMessage.SEVERITY_ERROR);
							return;
						}
						if (getDBOperator().recordCount(Uye.class.getSimpleName(), "o.sicilno='" + sicilno + "'") > 0) {
							createGenericMessage("Girdiğiniz sicil numarası sistemde bulunmaktadır!Farklı sicil numarası giriniz!", FacesMessage.SEVERITY_ERROR);
							return;
						}
						if (this.uye.getUyetip() == UyeTip._CALISAN) {
							for (int i = 0; i < 6; i++) {
								if (sicilno.charAt(i) < '0' || sicilno.charAt(i) > '9') {
									createGenericMessage("Geçersiz Sicil Numarası Girdiniz!", FacesMessage.SEVERITY_ERROR);
									return;
								}
							}
						} else if (this.uye.getUyetip() == UyeTip._OGRENCI) {
							if (sicilno.charAt(0) != 'G') {
								createGenericMessage("Geçersiz Sicil Numarası Girdiniz!", FacesMessage.SEVERITY_ERROR);
								return;
							}
							for (int i = 1; i < 6; i++) {
								if (sicilno.charAt(i) < '0' || sicilno.charAt(i) > '9') {
									createGenericMessage("Geçersiz Sicil Numarası Girdiniz!", FacesMessage.SEVERITY_ERROR);
									return;
								}
							}
						} else if (this.uye.getUyetip() == UyeTip._YABANCILAR && this.uye.getUyetip() == UyeTip._TURKUYRUKLU) {
							if (sicilno.charAt(0) != 'Y') {
								createGenericMessage("Geçersiz Sicil Numarası Girdiniz!", FacesMessage.SEVERITY_ERROR);
								return;
							}
							for (int i = 1; i < 6; i++) {
								if (sicilno.charAt(i) < '0' || sicilno.charAt(i) > '9') {
									createGenericMessage("Geçersiz Sicil Numarası Girdiniz!", FacesMessage.SEVERITY_ERROR);
									return;
								}
							}
						}
						this.uye.setSicilno(sicilno);
					}
				}
			} catch (DBException e) {
				logYaz("Exception @" + getModelName() + "Controller :", e);
			}
			wizardStep++;
		} else if (this.wizardStep == 2) {
			// Kisi ve kisikimlik bilgileri kaydedilecek...
			try {
				if (!kisiVeritabanindaBulundu && getKisi().getRID() == null) {
					getDBOperator().insert(getKisi());
					if (getKisiController().isLoggable()) {
						logKaydet(getKisi(), IslemTuru._EKLEME, "");
					}
					this.kisikimlik.setKisiRef(getKisi());
					getDBOperator().insert(getKisikimlik());
					if (getKisikimlikController().isLoggable()) {
						logKaydet(this.kisikimlik, IslemTuru._EKLEME, "");
					}

					if (this.kpsAdres.getKisiRef() != null) {
						this.kpsAdres.setKisiRef(getKisi());
						this.kpsAdres.setAdresturu(AdresTuru._KPSADRESI);
						getDBOperator().insert(this.kpsAdres);
						if (this.kpsDigerAdres.getSehirRef() != null) {
							this.kpsDigerAdres.setKisiRef(getKisi());
							this.kpsDigerAdres.setAdresturu(AdresTuru._KPSADRESI);
							getDBOperator().insert(this.kpsDigerAdres);
						}
					}
					createGenericMessage(KeyUtil.getMessageValue("kisi.kaydedildi"), FacesMessage.SEVERITY_INFO);
				} else {
					if (getKisi() != null) {
						if (getDBOperator().recordCount(KurumKisi.class.getSimpleName(), "o.kisiRef.rID=" + getKisi().getRID() + " AND o.uyekurumDurum= " + UyekurumDurum._CALISIYOR.getCode()) > 0) {
							KurumKisi kurumkisicalisan = (KurumKisi) getDBOperator().load(KurumKisi.class.getSimpleName(), "o.kisiRef.rID=" + getKisi().getRID() + " AND o.uyekurumDurum= " + UyekurumDurum._CALISIYOR.getCode(), "").get(0);
							setKurumKisi(kurumkisicalisan);
						}
						if (getDBOperator().recordCount(Telefon.class.getSimpleName(),
								"o.kisiRef.rID=" + getKisi().getRID() + " AND o.varsayilan=" + EvetHayir._EVET.getCode() + " AND o.telefonturu=" + TelefonTuru._EVTELEFONU.getCode()) > 0) {
							Telefon evtelefon = (Telefon) getDBOperator()
									.load(Telefon.class.getSimpleName(), "o.kisiRef.rID=" + getKisi().getRID() + " AND o.varsayilan=" + EvetHayir._EVET.getCode() + " AND o.telefonturu=" + TelefonTuru._EVTELEFONU.getCode(), "").get(0);
							setTelefon1(evtelefon);
						}
						if (getDBOperator().recordCount(Telefon.class.getSimpleName(),
								"o.kisiRef.rID=" + getKisi().getRID() + " AND o.varsayilan=" + EvetHayir._EVET.getCode() + " AND o.telefonturu=" + TelefonTuru._ISTELEFONU.getCode()) > 0) {
							Telefon istelefon = (Telefon) getDBOperator()
									.load(Telefon.class.getSimpleName(), "o.kisiRef.rID=" + getKisi().getRID() + " AND o.varsayilan=" + EvetHayir._EVET.getCode() + " AND o.telefonturu=" + TelefonTuru._ISTELEFONU.getCode(), "").get(0);
							setTelefon2(istelefon);
						}
						if (getDBOperator().recordCount(Telefon.class.getSimpleName(), "o.kisiRef.rID=" + getKisi().getRID() + " AND o.varsayilan=" + EvetHayir._EVET.getCode() + " AND o.telefonturu=" + TelefonTuru._GSM.getCode()) > 0) {
							Telefon gsm = (Telefon) getDBOperator()
									.load(Telefon.class.getSimpleName(), "o.kisiRef.rID=" + getKisi().getRID() + " AND o.varsayilan=" + EvetHayir._EVET.getCode() + " AND o.telefonturu=" + TelefonTuru._GSM.getCode(), "").get(0);
							setTelefon3(gsm);
						}
						if (getDBOperator().recordCount(Telefon.class.getSimpleName(), "o.kisiRef.rID=" + getKisi().getRID() + " AND o.varsayilan=" + EvetHayir._EVET.getCode() + " AND o.telefonturu=" + TelefonTuru._FAKS.getCode()) > 0) {
							Telefon faks = (Telefon) getDBOperator()
									.load(Telefon.class.getSimpleName(), "o.kisiRef.rID=" + getKisi().getRID() + " AND o.varsayilan=" + EvetHayir._EVET.getCode() + " AND o.telefonturu=" + TelefonTuru._FAKS.getCode(), "").get(0);
							setTelefon4(faks);
						}
						if (getDBOperator().recordCount(Internetadres.class.getSimpleName(), "o.kisiRef.rID=" + getKisi().getRID() + " AND o.varsayilan=" + EvetHayir._EVET.getCode()) > 0) {
							Internetadres internetadress = (Internetadres) getDBOperator().load(Internetadres.class.getSimpleName(), "o.kisiRef.rID=" + getKisi().getRID() + " AND o.varsayilan=" + EvetHayir._EVET.getCode(), "").get(0);
							setInternetAdres1(internetadress);
						}
						if (getDBOperator().recordCount(Kisiogrenim.class.getSimpleName(),
								"o.kisiRef.rID=" + getKisi().getRID() + " AND o.ogrenimtip=" + OgrenimTip._LISANS.getCode() + " AND o.varsayilan=" + EvetHayir._EVET.getCode()) > 0) {
							Kisiogrenim kisiogrenim = (Kisiogrenim) getDBOperator()
									.load(Kisiogrenim.class.getSimpleName(), "o.kisiRef.rID=" + getKisi().getRID() + " AND o.ogrenimtip=" + OgrenimTip._LISANS.getCode() + " AND o.varsayilan=" + EvetHayir._EVET.getCode(), "").get(0);
							setKisiogrenim(kisiogrenim);
						}
					}
					if (this.kisikimlikPojo.getTcKimlikNo() != null) {
						this.kisi.setKimlikno(this.kisikimlikPojo.getTcKimlikNo());
					}
				}
				this.kisiogrenim.setDenklikdurum(EvetHayir._HAYIR);
				wizardStep++;
			} catch (DBException e) {
				createGenericMessage(KeyUtil.getMessageValue("kisi.hata"), FacesMessage.SEVERITY_ERROR);
				logYaz("Exception @" + getModelName() + "Controller :", e);
			}
		} else if (this.wizardStep == 3) {
			if (this.uye.getUyetip() == UyeTip._OGRENCI) {
				this.kisiogrenim.setMezuniyettarihi(null);
				this.adres3.setAdresturu(AdresTuru._EVADRESI);
				try {
					this.beyanadresvarsayilan = true;
					if (getKurumKisi().getKurumRef() != null && getKurumKisi().getKurumRef().getKurumAdres() != null) {
						setAdres2(getKurumKisi().getKurumRef().getKurumAdres());
						getAdres2().setKurumRef(null);
						getAdres2().setVarsayilan(EvetHayir._EVET);
					}
					this.adres3.setAdresturu(AdresTuru._EVADRESI);
					if (getKpsAdres() != null && !isEmpty(getKpsAdres().getAcikAdres())) {
						setAdres3((Adres) kpsAdres.cloneObject());
						getAdres3().setRID(null);
						getAdres3().setAdresturu(AdresTuru._EVADRESI);
						if (getDBOperator().recordCount(Ulke.class.getSimpleName(), "o.ulkekod = '9980'") > 0) {
							getAdres3().setUlkeRef((Ulke) getDBOperator().find(Ulke.class.getSimpleName(), "o.ulkekod = '9980'"));
						}
						if (getAdres3().getUlkeRef() != null) {
							changeIl(getAdres3().getUlkeRef().getRID());
							if (getAdres3().getIlceRef() != null) {
								getAdres3().setSehirRef(getAdres3().getIlceRef().getSehirRef());
								changeIlce(getAdres3().getSehirRef().getRID());
								getAdres3().setIlceRef(getAdres3().getIlceRef());
							}
						} else if (getAdres3().getIlceRef() != null) {
							getAdres3().setSehirRef(getAdres3().getIlceRef().getSehirRef());
							changeIlce(getAdres3().getSehirRef().getRID());
							getAdres3().setIlceRef(getAdres3().getIlceRef());
						}
					} else if (getKisiAdresDondu() != null && !isEmpty(getAdres3().getAcikAdres())) {
						getAdres3().setVarsayilan(getAdres3().getVarsayilan());
					}
				} catch (Exception e) {
					logYaz("Exception @" + getModelName() + "Controller :", e);
				}
				wizardStep = wizardStep + 2;
			} else {
				wizardStep++;
			}
		} else if (this.wizardStep == 4) {
			// UYe kurum secildiyese, adres aktarilacak...
			try {
				if (getKurumKisi().getKurumRef() != null && getKurumKisi().getKurumRef().getKurumAdres() != null) {
					setAdres2(getKurumKisi().getKurumRef().getKurumAdres());
				}
				this.adres3.setAdresturu(AdresTuru._EVADRESI);
				this.adres2.setAdresturu(AdresTuru._ISADRESI);
				this.beyanadresvarsayilan = true;
				this.adres2.setAdrestipi(AdresTipi._ILILCEMERKEZI);
				if (getKpsAdres() != null && !isEmpty(getKpsAdres().getAcikAdres())) {
					setAdres3((Adres) kpsAdres.cloneObject());
					getAdres3().setRID(null);
					getAdres3().setAdresturu(AdresTuru._EVADRESI);
					if (getDBOperator().recordCount(Ulke.class.getSimpleName(), "o.ulkekod = '9980'") > 0) {
						getAdres3().setUlkeRef((Ulke) getDBOperator().find(Ulke.class.getSimpleName(), "o.ulkekod = '9980'"));
					}

					if (getAdres3().getUlkeRef() != null) {
						changeIl(getAdres3().getUlkeRef().getRID());
						if (getAdres3().getIlceRef() != null) {
							getAdres3().setSehirRef(getAdres3().getIlceRef().getSehirRef());
							changeIlce(getAdres3().getSehirRef().getRID());
							getAdres3().setIlceRef(getAdres3().getIlceRef());
						}
					} else if (getAdres3().getIlceRef() != null) {
						getAdres3().setSehirRef(getAdres3().getIlceRef().getSehirRef());
						changeIlce(getAdres3().getSehirRef().getRID());
						getAdres3().setIlceRef(getAdres3().getIlceRef());
					}
				} else if (getKisiAdresDondu() != null && !isEmpty(getAdres3().getAcikAdres())) {
					getAdres3().setVarsayilan(getAdres3().getVarsayilan());
				}
				if (getKurumKisi().getKurumRef() != null) {
					if (getKurumKisi().getKurumRef().getKurumAdres() != null) {
						setAdres2((Adres) getKurumKisi().getKurumRef().getKurumAdres().cloneObject());
						getAdres2().setKurumRef(null);
						getAdres2().setRID(null);
						getAdres2().setAdresturu(AdresTuru._ISADRESI);
						if (getDBOperator().recordCount(Ulke.class.getSimpleName(), "o.ulkekod = '9980'") > 0) {
							getAdres2().setUlkeRef((Ulke) getDBOperator().find(Ulke.class.getSimpleName(), "o.ulkekod = '9980'"));
						}
						if (getAdres2().getUlkeRef() != null) {
							changeIlForUyeIsyeri(getAdres2().getUlkeRef().getRID());
							getAdres2().setSehirRef(getAdres2().getSehirRef());
							if (getAdres2().getSehirRef() != null) {
								changeIlceForUyeIsyeri(getAdres2().getSehirRef().getRID());
								getAdres2().setIlceRef(getAdres2().getIlceRef());
							}
						}
					} else {
						setAdres2(new Adres());
						getAdres2().setAdresturu(AdresTuru._ISADRESI);
						getAdres2().setAdrestipi(AdresTipi._ILILCEMERKEZI);
					}

				}

				if (getKisikimlik() != null) {
					kurumKisi.setKisiRef(getKisikimlik().getKisiRef());
				}
				kurumKisi.setKurumRef(getKurumKisi().getKurumRef());
				kurumKisi.setGecerli(EvetHayir._EVET);

				wizardStep++;
			} catch (Exception e) {
				logYaz("Exception @" + getModelName() + "Controller :", e);
			}
		} else if (this.wizardStep == 5) {
			try {
				if (this.isyeriadresvarsayilan) {
					this.adres2.setVarsayilan(EvetHayir._EVET);
				} else {
					this.adres2.setVarsayilan(EvetHayir._HAYIR);
				}
				if (this.beyanadresvarsayilan) {
					this.adres3.setVarsayilan(EvetHayir._EVET);
				} else {
					this.adres3.setVarsayilan(EvetHayir._HAYIR);
				}
				if (!this.isyeriadresvarsayilan && !this.beyanadresvarsayilan) {
					this.adres2.setVarsayilan(EvetHayir._EVET);
				}
				if (this.adres2.getVarsayilan() == EvetHayir._EVET && this.adres3.getVarsayilan() == EvetHayir._EVET) {
					this.adres3.setVarsayilan(EvetHayir._HAYIR);
				}
				if (this.adres3.getAdrestipi().getCode() == AdresTipi._ILILCEMERKEZI.getCode()) {
					this.adres3.setUlkeRef((Ulke) getDBOperator().find(Ulke.class.getSimpleName(), "o.ulkekod = '9980'"));
				}
				if (getAdres3().getAcikAdres() != null && getAdres3().getAdrestipi() != null && getAdres3().getAdresturu() != null && getAdres3().getIlceRef() != null && getAdres3().getSehirRef() != null) {
					this.adres3.setAcikAdres(getAdres3().getAcikAdres());
					this.adres3.setAdrestipi(getAdres3().getAdrestipi());
					this.adres3.setAdresturu(getAdres3().getAdresturu());
					this.adres3.setIlceRef(getAdres3().getIlceRef());
					this.adres3.setSehirRef(getAdres3().getSehirRef());
				}
				if (getKurumKisi().getKurumRef() != null && getKurumKisi().getKisiRef() != null) {
					if (getDBOperator().recordCount(Adres.class.getSimpleName(), "o.kisiRef.rID=" + getKurumKisi().getKisiRef().getRID() + " AND o.adresturu=" + AdresTuru._ISADRESI.getCode()) > 0) {
						Adres oldKurumAdres = (Adres) getDBOperator().load(Adres.class.getSimpleName(), "o.kisiRef.rID=" + getKurumKisi().getKisiRef().getRID() + " AND o.adresturu=" + AdresTuru._ISADRESI.getCode(), "").get(0);

						if (this.adres2.getAdrestipi().getCode() == oldKurumAdres.getAdrestipi().getCode() && (this.adres2.getAcikAdres() != oldKurumAdres.getAcikAdres() || this.adres2.getSehirRef() != oldKurumAdres.getSehirRef()
								|| this.adres2.getIlceRef() != oldKurumAdres.getIlceRef() || this.adres2.getYabancisehir() != oldKurumAdres.getYabanciadres() || this.adres2.getYabanciulke() != oldKurumAdres.getYabanciulke())) {
							getDBOperator().delete(oldKurumAdres);
						}
					}
				}
			} catch (Exception e) {
				logYaz("Exception @" + getModelName() + "Controller :", e);
			}
			wizardStep++;
		} else {
			wizardStep++;
		}
	}

	@Override
	public void wizardBackward() {
		if (this.wizardStep == 5 && this.uye.getUyetip() == UyeTip._OGRENCI) {
			wizardStep = wizardStep - 2;
		} else {
			wizardStep--;
		}

	}

	@SuppressWarnings({ "rawtypes" })
	public List completeMethodKurumAuto(String value) {
		String firstcharuppercase = null;
		// Ilk karakterini uppercase yaomak icin
		for (int i = 0; i < value.length(); ++i) {
			if (i == 0) {
				firstcharuppercase = (value.charAt(i) + "").toUpperCase();
			} else {
				firstcharuppercase += value.charAt(i);
			}
		}
		String[] values = new String[getSessionUser().getAutoCompleteSearchColumns(Kurum.class.getSimpleName()).length];
		for (int x = 0; x < getSessionUser().getAutoCompleteSearchColumns(Kurum.class.getSimpleName()).length; x++) {
			values[x] = value;
		}
		List entityList = new ArrayList<>();
		try {
			if (getDBOperator().recordCount(Kurum.class.getSimpleName(), " UPPER(o.ad) LIKE UPPER ('%" + value + "%') " + " OR  o.rID IN (SELECT m.kurumRef.rID FROM " + Adres.class.getSimpleName()
					+ "  m WHERE UPPER(m.acikAdres) LIKE UPPER ('%" + value + "%') OR UPPER(m.acikAdres) LIKE UPPER ('" + firstcharuppercase + "%') )") > 0) {

				entityList = getDBOperator().load(Kurum.class.getSimpleName(), " UPPER(o.ad) LIKE UPPER ('%" + value + "%') " + " OR  o.rID IN (SELECT m.kurumRef.rID FROM " + Adres.class.getSimpleName()
						+ "  m WHERE UPPER(m.acikAdres) LIKE UPPER ('%" + value + "%') OR UPPER(m.acikAdres) LIKE UPPER ('" + firstcharuppercase + "%') )", "");
			} else {
				createGenericMessage("Kayıt Bulunamadı!", FacesMessage.SEVERITY_INFO);
				return null;
			}
		} catch (DBException e) {
			logYaz("WizardController @completeMethodKurumAuto " + e.getMessage());
			logYaz("Exception @" + getModelName() + "Controller :", e);
		}
		return entityList;

	}

	public SelectItem[] getSelectAdresListForUyeKurum() {
		if (getKurumKisi().getKurumRef() != null) {
			@SuppressWarnings("unchecked")
			List<Adres> entityList = getDBOperator().load(Adres.class.getSimpleName(), "o.kurumRef.rID=" + getKurumKisi().getKurumRef().getRID(), "o.varsayilan");
			if (entityList != null) {
				SelectItem[] selectItemList = new SelectItem[entityList.size() + 1];
				selectItemList[0] = new SelectItem(null, "");
				int i = 1;
				for (BaseEntity entity : entityList) {
					selectItemList[i++] = new SelectItem(entity, entity.getUIString());
				}
				setSelectAdresListForUyeKurum(selectItemList);
			} else {
				setSelectAdresListForUyeKurum(new SelectItem[0]);
			}
		} else {
			setSelectAdresListForUyeKurum(new SelectItem[0]);
		}
		return selectAdresListForUyeKurum;
	}

	public void setSelectAdresListForUyeKurum(SelectItem[] selectAdresListForUyeKurum) {
		this.selectAdresListForUyeKurum = selectAdresListForUyeKurum;
	}

	public Uye getUye() {
		uye = (Uye) getEntity();
		return uye;
	}

	public void setUye(Uye uye) {
		this.uye = uye;
	}

	@Override
	public int getWizardStep() {
		return wizardStep;
	}

	@Override
	public void setWizardStep(int wizardStep) {
		this.wizardStep = wizardStep;
	}

	public Kisi getKisi() {
		return kisi;
	}

	public void setKisi(Kisi kisi) {
		this.kisi = kisi;
	}

	public Adres getAdres2() {
		return adres2;
	}

	public void setAdres2(Adres adres2) {
		this.adres2 = adres2;
	}

	public Adres getAdres3() {
		return adres3;
	}

	public void setAdres3(Adres adres3) {
		this.adres3 = adres3;
	}

	public Telefon getTelefon1() {
		return telefon1;
	}

	public void setTelefon1(Telefon telefon1) {
		this.telefon1 = telefon1;
	}

	public Telefon getTelefon2() {
		return telefon2;
	}

	public void setTelefon2(Telefon telefon2) {
		this.telefon2 = telefon2;
	}

	public Telefon getTelefon3() {
		return telefon3;
	}

	public void setTelefon3(Telefon telefon3) {
		this.telefon3 = telefon3;
	}

	public Telefon getTelefon4() {
		return telefon4;
	}

	public void setTelefon4(Telefon telefon4) {
		this.telefon4 = telefon4;
	}

	public Internetadres getInternetAdres1() {
		return internetAdres1;
	}

	public void setInternetAdres1(Internetadres internetAdres1) {
		this.internetAdres1 = internetAdres1;
	}

	public Kisikimlik getKisikimlik() {
		return kisikimlik;
	}

	public void setKisikimlik(Kisikimlik kisikimlik) {
		this.kisikimlik = kisikimlik;
	}

	/* MANAGED PROPERTIES USED */
	private UyeController uyeController = new UyeController(null);
	private KisiController kisiController = new KisiController(null);
	private KisikimlikController kisikimlikController = new KisikimlikController(null);
	private InternetadresController internetadresController = new InternetadresController(null);
	private AdresController adresController = new AdresController(null);
	private TelefonController telefonController = new TelefonController(null);

	public UyeController getUyeController() {
		return uyeController;
	}

	public void setUyeController(UyeController uyeController) {
		this.uyeController = uyeController;
	}

	public KisiController getKisiController() {
		return kisiController;
	}

	public void setKisiController(KisiController kisiController) {
		this.kisiController = kisiController;
	}

	public KisikimlikController getKisikimlikController() {
		return kisikimlikController;
	}

	public void setKisikimlikController(KisikimlikController kisikimlikController) {
		this.kisikimlikController = kisikimlikController;
	}

	public InternetadresController getInternetadresController() {
		return internetadresController;
	}

	public void setInternetadresController(InternetadresController internetadresController) {
		this.internetadresController = internetadresController;
	}

	public TelefonController getTelefonController() {
		return telefonController;
	}

	public void setTelefonController(TelefonController telefonController) {
		this.telefonController = telefonController;
	}

	public Kisiogrenim getKisiogrenim() {
		return kisiogrenim;
	}

	public void setKisiogrenim(Kisiogrenim kisiogrenim) {
		this.kisiogrenim = kisiogrenim;
	}

	public Kurum getKurum() {
		return kurum;
	}

	public void setKurum(Kurum kurum) {
		this.kurum = kurum;
	}

	public String getDetailEditType() {
		return detailEditType;
	}

	public void setDetailEditType(String detailEditType) {
		this.detailEditType = detailEditType;
	}

	public Birim getBirim() {
		return birim;
	}

	public void setBirim(Birim birim) {
		this.birim = birim;
	}

	public boolean isKpsSorguSonucDondu() {
		return kpsSorguSonucDondu;
	}

	public void setKpsSorguSonucDondu(boolean kpsSorguSonucDondu) {
		this.kpsSorguSonucDondu = kpsSorguSonucDondu;
	}

	public boolean isKpsKaydiBulundu() {
		return kpsKaydiBulundu;
	}

	public void setKpsKaydiBulundu(boolean kpsKaydiBulundu) {
		this.kpsKaydiBulundu = kpsKaydiBulundu;
	}

	public Adres getKpsAdres() {
		return kpsAdres;
	}

	public void setKpsAdres(Adres kpsAdres) {
		this.kpsAdres = kpsAdres;
	}

	public Adres getKpsDigerAdres() {
		return kpsDigerAdres;
	}

	public void setKpsDigerAdres(Adres kpsDigerAdres) {
		this.kpsDigerAdres = kpsDigerAdres;
	}

	public KurumKisi getKurumKisi() {
		return kurumKisi;
	}

	public void setKurumKisi(KurumKisi kurumKisi) {
		this.kurumKisi = kurumKisi;
	}

	public AdresController getAdresController() {
		return adresController;
	}

	public void setAdresController(AdresController adresController) {
		this.adresController = adresController;
	}

	public String getSicilno() {
		return sicilno;
	}

	public void setSicilno(String sicilno) {
		this.sicilno = sicilno;
	}

	/* HANDLE CHANGE EVENTLERI */
	public SelectItem[] getSelectIlceListForIsyeriAdres() {
		if (getAdres2().getSehirRef() != null) {
			@SuppressWarnings("unchecked")
			List<Ilce> entityList = getDBOperator().load(Ilce.class.getSimpleName(), "o.sehirRef.rID=" + getAdres2().getSehirRef().getRID(), "o.ad");
			if (entityList != null) {
				SelectItem[] selectItemList = new SelectItem[entityList.size() + 1];
				selectItemList[0] = new SelectItem(null, "");
				int i = 1;
				for (BaseEntity entity : entityList) {
					selectItemList[i++] = new SelectItem(entity, entity.getUIString());
				}
				return selectItemList;
			} else {
				return new SelectItem[0];
			}
		} else {
			return new SelectItem[0];
		}
	}

	public SelectItem[] getSelectIlceListForBeyanAdres() {
		if (getAdres3().getSehirRef() != null) {
			@SuppressWarnings("unchecked")
			List<Ilce> entityList = getDBOperator().load(Ilce.class.getSimpleName(), "o.sehirRef.rID=" + getAdres3().getSehirRef().getRID(), "o.ad");
			if (entityList != null) {
				SelectItem[] selectItemList = new SelectItem[entityList.size() + 1];
				selectItemList[0] = new SelectItem(null, "");
				int i = 1;
				for (BaseEntity entity : entityList) {
					selectItemList[i++] = new SelectItem(entity, entity.getUIString());
				}
				return selectItemList;
			} else {
				return new SelectItem[0];
			}
		} else {
			return new SelectItem[0];
		}
	}

	public SelectItem[] getSelectFakulteList() {
		if (getKisiogrenim().getUniversiteRef() != null) {
			@SuppressWarnings("unchecked")
			List<Fakulte> entityList = getDBOperator().load(Fakulte.class.getSimpleName(), "o.universiteRef.rID=" + getKisiogrenim().getUniversiteRef().getRID(), "o.ad");
			if (entityList != null) {
				SelectItem[] selectItemList = new SelectItem[entityList.size() + 1];
				selectItemList[0] = new SelectItem(null, "");
				int i = 1;
				for (BaseEntity entity : entityList) {
					selectItemList[i++] = new SelectItem(entity, entity.getUIString());
				}
				return selectItemList;
			} else {
				return new SelectItem[0];
			}
		} else {
			return new SelectItem[0];
		}
	}

	public SelectItem[] getSelectBolumList() {
		if (getKisiogrenim().getFakulteRef() != null) {
			@SuppressWarnings("unchecked")
			List<Bolum> entityList = getDBOperator().load(Bolum.class.getSimpleName(), "o.fakulteRef.rID=" + getKisiogrenim().getFakulteRef().getRID(), "o.ad");
			if (entityList != null) {
				SelectItem[] selectItemList = new SelectItem[entityList.size() + 1];
				selectItemList[0] = new SelectItem(null, "");
				int i = 1;
				for (BaseEntity entity : entityList) {
					selectItemList[i++] = new SelectItem(entity, entity.getUIString());
				}
				return selectItemList;
			} else {
				return new SelectItem[0];
			}
		} else {
			return new SelectItem[0];
		}
	}

	public boolean isKisiVeritabanindaBulundu() {
		return kisiVeritabanindaBulundu;
	}

	public void setKisiVeritabanindaBulundu(boolean kisiVeritabanindaBulundu) {
		this.kisiVeritabanindaBulundu = kisiVeritabanindaBulundu;
	}

	public String callDetailFromWizard() {
		return getUyeController().detailPage(null);
	}

	public PojoKisi getKisikimlikPojo() {
		return kisikimlikPojo;
	}

	public void setKisikimlikPojo(PojoKisi kisikimlikPojo) {
		this.kisikimlikPojo = kisikimlikPojo;
	}

	public Boolean getIletisimEmail() {
		return iletisimEmail;
	}

	public void setIletisimEmail(Boolean iletisimEmail) {
		this.iletisimEmail = iletisimEmail;
	}

	public Boolean getIletisimSms() {
		return iletisimSms;
	}

	public void setIletisimSms(Boolean iletisimSms) {
		this.iletisimSms = iletisimSms;
	}

	public Boolean getIletisimPosta() {
		return iletisimPosta;
	}

	public void setIletisimPosta(Boolean iletisimPosta) {
		this.iletisimPosta = iletisimPosta;
	}

	public Boolean getIsyeriadresvarsayilan() {
		return isyeriadresvarsayilan;
	}

	public void setIsyeriadresvarsayilan(Boolean isyeriadresvarsayilan) {
		this.isyeriadresvarsayilan = isyeriadresvarsayilan;
	}

	public Boolean getBeyanadresvarsayilan() {
		return beyanadresvarsayilan;
	}

	public void setBeyanadresvarsayilan(Boolean beyanadresvarsayilan) {
		this.beyanadresvarsayilan = beyanadresvarsayilan;
	}

	private SelectItem[] ilItemListForIsyeriAdres;
	private SelectItem[] ilceItemListForIsyeriAdres;

	public SelectItem[] getIlItemListForIsyeriAdres() {
		return ilItemListForIsyeriAdres;
	}

	public void setIlItemListForIsyeriAdres(SelectItem[] ilItemListForIsyeriAdres) {
		this.ilItemListForIsyeriAdres = ilItemListForIsyeriAdres;
	}

	public SelectItem[] getIlceItemListForIsyeriAdres() {
		return ilceItemListForIsyeriAdres;
	}

	public void setIlceItemListForIsyeriAdres(SelectItem[] ilceItemListForIsyeriAdres) {
		this.ilceItemListForIsyeriAdres = ilceItemListForIsyeriAdres;
	}

	protected void changeIlForUyeIsyeri(Long selectedRecId) {
		if (selectedRecId.longValue() == 0) {
			setIlItemListForIsyeriAdres(new SelectItem[0]);
		} else {
			@SuppressWarnings("unchecked")
			// List<Sehir> entityList =
			// getDBOperator().load(Sehir.class.getSimpleName(),
			// "o.ulkeRef.rID=" + selectedRecId ,"o.ad");
			List<Sehir> entityList = getDBOperator().load(Sehir.class.getSimpleName(), "", "o.ad");
			if (entityList != null) {
				ilItemListForIsyeriAdres = new SelectItem[entityList.size() + 1];
				ilItemListForIsyeriAdres[0] = new SelectItem(null, "");
				int i = 1;
				for (Sehir entity : entityList) {
					ilItemListForIsyeriAdres[i++] = new SelectItem(entity, entity.getUIString());
				}
			} else {
				setIlItemListForIsyeriAdres(new SelectItem[0]);
			}
		}
	}

	protected void changeIlceForUyeIsyeri(Long selectedRecId) {
		if (selectedRecId.longValue() == 0) {
			setIlceItemListForIsyeriAdres(new SelectItem[0]);
		} else {
			@SuppressWarnings("unchecked")
			List<Ilce> entityList = getDBOperator().load(Ilce.class.getSimpleName(), "o.sehirRef.rID=" + selectedRecId, "o.ad");
			if (entityList != null) {
				ilceItemListForIsyeriAdres = new SelectItem[entityList.size() + 1];
				ilceItemListForIsyeriAdres[0] = new SelectItem(null, "");
				int i = 1;
				for (Ilce entity : entityList) {
					ilceItemListForIsyeriAdres[i++] = new SelectItem(entity, entity.getUIString());
				}
			} else {
				setIlceItemListForIsyeriAdres(new SelectItem[0]);
			}
		}
	}

	public DataTable getTable2() {
		return table2;
	}

	public void setTable2(DataTable table2) {
		this.table2 = table2;
	}

	public void handleChangeKurumSehir(AjaxBehaviorEvent event) {
		try {
			HtmlSelectOneMenu menu = (HtmlSelectOneMenu) event.getComponent();
			if (menu.getValue() instanceof Sehir) {
				changeIlceForUyeIsyeri(((Sehir) menu.getValue()).getRID());
			}

		} catch (Exception e) {
			logYaz("Error Wizard Controller @handleChange :" + e.getMessage());
			logYaz("Exception @" + getModelName() + "Controller :", e);
		}
	}

} // class
