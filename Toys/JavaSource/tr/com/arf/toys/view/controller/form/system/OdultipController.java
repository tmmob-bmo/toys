package tr.com.arf.toys.view.controller.form.system; 
  
 
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.toys.db.filter.system.OdultipFilter;
import tr.com.arf.toys.db.model.system.Odultip;
import tr.com.arf.toys.view.controller.form._base.ToysBaseFilteredController;
  
  
@ManagedBean 
@ViewScoped 
public class OdultipController extends ToysBaseFilteredController {  
  
  private static final long serialVersionUID = 6531152165884980748L; 
  
	private Odultip odultip;  
	private OdultipFilter odultipFilter = new OdultipFilter();  
  
	public OdultipController() { 
		super(Odultip.class);  
		setDefaultValues(false);  
		setOrderField("kisaad");  
		setAutoCompleteSearchColumns(new String[]{"kisaad"}); 
		setTable(null); 
		queryAction(); 
	} 
	
	
	public Odultip getOdultip() { 
		odultip = (Odultip) getEntity(); 
		return odultip; 
	} 
	
	public void setOdultip(Odultip odultip) { 
		this.odultip = odultip; 
	} 
	
	public OdultipFilter getOdultipFilter() { 
		return odultipFilter; 
	} 
	 
	public void setOdultipFilter(OdultipFilter odultipFilter) {  
		this.odultipFilter = odultipFilter;  
	}  
	 
	@Override  
	public BaseFilter getFilter() {  
		return getOdultipFilter();  
	}  
	
	
 
	
} // class 
