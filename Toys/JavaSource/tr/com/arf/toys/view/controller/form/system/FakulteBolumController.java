package tr.com.arf.toys.view.controller.form.system; 
  
 
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.framework.db.model._base.BaseEntity;
import tr.com.arf.toys.db.filter.system.FakulteBolumFilter;
import tr.com.arf.toys.db.model.system.FakulteBolum;
import tr.com.arf.toys.db.model.system.Universite;
import tr.com.arf.toys.service.application.ApplicationDescriptor;
import tr.com.arf.toys.view.controller.form._base.ToysBaseFilteredController;
  
  
@ManagedBean 
@ViewScoped 
public class FakulteBolumController extends ToysBaseFilteredController {  
  
  private static final long serialVersionUID = 6531152165884980748L; 
  
	private FakulteBolum fakulteBolum;  
	private FakulteBolumFilter fakulteBolumFilter = new FakulteBolumFilter();  
  
	public FakulteBolumController() { 
		super(FakulteBolum.class);  
		setDefaultValues(false);  
		setTable(null); 
		queryAction(); 
	} 
	
	public BaseEntity getMasterEntity(){
		if(getObjectFromSessionFilter(ApplicationDescriptor._UNIVERSITE_MODEL) != null){
			return (Universite) getObjectFromSessionFilter(ApplicationDescriptor._UNIVERSITE_MODEL);
		}  
		else {
			return null;
		}			
	} 
	

	public FakulteBolum getFakulteBolum() {
		fakulteBolum=(FakulteBolum) getEntity();
		return fakulteBolum;
	}

	public void setFakulteBolum(FakulteBolum fakulteBolum) {
		this.fakulteBolum = fakulteBolum;
	}

	public FakulteBolumFilter getFakulteBolumFilter() {
		return fakulteBolumFilter;
	}

	public void setFakulteBolumFilter(FakulteBolumFilter fakulteBolumFilter) {
		this.fakulteBolumFilter = fakulteBolumFilter;
	}

	@Override  
	public BaseFilter getFilter() {  
		return getFakulteBolumFilter();  
	}  
	
} // class 
