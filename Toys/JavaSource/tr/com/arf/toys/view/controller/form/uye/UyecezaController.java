package tr.com.arf.toys.view.controller.form.uye; 
  
 
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.toys.db.filter.uye.UyecezaFilter;
import tr.com.arf.toys.db.model.uye.Uye;
import tr.com.arf.toys.db.model.uye.Uyeceza;
import tr.com.arf.toys.service.application.ApplicationDescriptor;
import tr.com.arf.toys.view.controller.form._base.ToysBaseFilteredController;
  
  
@ManagedBean 
@ViewScoped 
public class UyecezaController extends ToysBaseFilteredController {  
  
  private static final long serialVersionUID = 6531152165884980748L; 
  
	private Uyeceza uyeceza;  
	private UyecezaFilter uyecezaFilter = new UyecezaFilter();  
  
	public UyecezaController() { 
		super(Uyeceza.class);  
		setDefaultValues(false);  
		setOrderField("aciklama");  
		setAutoCompleteSearchColumns(new String[]{"aciklama"}); 
		setTable(null); 
		if(getMasterEntity() != null){ 
			setMasterObjectName(ApplicationDescriptor._UYE_MODEL);
			setMasterOutcome(ApplicationDescriptor._UYE_MODEL);
			getUyecezaFilter().setUyeRef(getMasterEntity());
			getUyecezaFilter().createQueryCriterias();   
		} 
		queryAction(); 
	} 
	
	public UyecezaController(Object object) { 
		super(Uyeceza.class);   
	} 
 
	public Uye getMasterEntity(){
		if(getUyecezaFilter().getUyeRef() != null){
			return getUyecezaFilter().getUyeRef();
		} else {
			return (Uye) getObjectFromSessionFilter(ApplicationDescriptor._UYE_MODEL);
		}			
	} 
 
	
	
	public Uyeceza getUyeceza() { 
		uyeceza = (Uyeceza) getEntity(); 
		return uyeceza; 
	} 
	
	public void setUyeceza(Uyeceza uyeceza) { 
		this.uyeceza = uyeceza; 
	} 
	
	public UyecezaFilter getUyecezaFilter() { 
		return uyecezaFilter; 
	} 
	 
	public void setUyecezaFilter(UyecezaFilter uyecezaFilter) {  
		this.uyecezaFilter = uyecezaFilter;  
	}  
	 
	@Override  
	public BaseFilter getFilter() {  
		return getUyecezaFilter();  
	}  
	
	
 
	@Override 
	public void resetFilter() {
		super.resetFilterOnly();
		if(getMasterEntity() != null){
			getUyecezaFilter().setUyeRef(getMasterEntity()); 
		}
		queryAction();
	}
 
	@Override	
	public void insert() {	
		super.insert();	
		getUyeceza().setUyeRef(getUyecezaFilter().getUyeRef());	
	}	
	
} // class 
