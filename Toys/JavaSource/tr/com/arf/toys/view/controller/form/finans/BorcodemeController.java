package tr.com.arf.toys.view.controller.form.finans; 
  
 
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.toys.db.filter.finans.BorcodemeFilter;
import tr.com.arf.toys.db.model.finans.Borc;
import tr.com.arf.toys.db.model.finans.Borcodeme;
import tr.com.arf.toys.service.application.ApplicationDescriptor;
import tr.com.arf.toys.view.controller.form._base.ToysBaseFilteredController;
  
  
@ManagedBean 
@ViewScoped 
public class BorcodemeController extends ToysBaseFilteredController {  
  
  private static final long serialVersionUID = 6531152165884980748L; 
  
	private Borcodeme borcodeme;  
	private BorcodemeFilter borcodemeFilter = new BorcodemeFilter();  
  
	public BorcodemeController() { 
		super(Borcodeme.class);  
		setDefaultValues(false);  
		setOrderField("miktar");  
		setAutoCompleteSearchColumns(new String[]{"miktar"}); 
		setTable(null); 
		if(getMasterEntity() != null){ 
			setMasterObjectName(ApplicationDescriptor._BORC_MODEL);
			setMasterOutcome(ApplicationDescriptor._BORC_OUTCOME);
			getBorcodemeFilter().setBorcRef(getMasterEntity());
			getBorcodemeFilter().createQueryCriterias();   
		} 
		queryAction(); 
	} 
 
	public Borc getMasterEntity(){
		if(getBorcodemeFilter().getBorcRef() != null){
			return getBorcodemeFilter().getBorcRef();
		} else {
			return (Borc) getObjectFromSessionFilter(ApplicationDescriptor._BORC_MODEL);
		}			
	} 
 
	
	
	public Borcodeme getBorcodeme() { 
		borcodeme = (Borcodeme) getEntity(); 
		return borcodeme; 
	} 
	
	public void setBorcodeme(Borcodeme borcodeme) { 
		this.borcodeme = borcodeme; 
	} 
	
	public BorcodemeFilter getBorcodemeFilter() { 
		return borcodemeFilter; 
	} 
	 
	public void setBorcodemeFilter(BorcodemeFilter borcodemeFilter) {  
		this.borcodemeFilter = borcodemeFilter;  
	}  
	 
	@Override  
	public BaseFilter getFilter() {  
		return getBorcodemeFilter();  
	}  
	
	
 
 
	@Override	
	public void insert() {	
		super.insert();	
		getBorcodeme().setBorcRef(getBorcodemeFilter().getBorcRef());	
	}	
	
} // class 
