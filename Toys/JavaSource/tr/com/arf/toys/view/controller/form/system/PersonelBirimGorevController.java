package tr.com.arf.toys.view.controller.form.system; 
  
 
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.toys.db.enumerated.system.PersonelGorevDurum;
import tr.com.arf.toys.db.filter.system.PersonelBirimGorevFilter;
import tr.com.arf.toys.db.model.system.Personel;
import tr.com.arf.toys.db.model.system.PersonelBirimGorev;
import tr.com.arf.toys.service.application.ApplicationDescriptor;
import tr.com.arf.toys.view.controller.form._base.ToysBaseFilteredController;
  
  
@ManagedBean 
@ViewScoped 
public class PersonelBirimGorevController extends ToysBaseFilteredController {  
  
  private static final long serialVersionUID = 6531152165884980748L; 
  
	private PersonelBirimGorev personelBirimGorev;  
	private PersonelBirimGorevFilter personelBirimGorevFilter = new PersonelBirimGorevFilter();  
  
	public PersonelBirimGorevController() { 
		super(PersonelBirimGorev.class);  
		setDefaultValues(false);  
		setOrderField("baslangictarih DESC");  
		setAutoCompleteSearchColumns(new String[]{"aciklama"}); 
		setTable(null); 
		setStaticWhereCondition(getSessionUser().createStaticWhereCondition("o."));
		if(getMasterEntity() != null){ 
			setMasterObjectName(ApplicationDescriptor._PERSONEL_MODEL);
			setMasterOutcome(ApplicationDescriptor._PERSONEL_MODEL);
			getPersonelBirimGorevFilter().setPersonelRef(getMasterEntity());
			getPersonelBirimGorevFilter().createQueryCriterias();   
		} 
		queryAction(); 
	} 
	
	public PersonelBirimGorevController(Object object) { 
		super(PersonelBirimGorev.class);  
		setLoggable(false);
	}
 
	public Personel getMasterEntity(){
		if(getPersonelBirimGorevFilter().getPersonelRef() != null){
			return getPersonelBirimGorevFilter().getPersonelRef();
		} else {
			return (Personel) getObjectFromSessionFilter(ApplicationDescriptor._PERSONEL_MODEL);
		}			
	} 
	
	public PersonelBirimGorev getPersonelBirimGorev() { 
		personelBirimGorev = (PersonelBirimGorev) getEntity(); 
		return personelBirimGorev; 
	} 
	
	public void setPersonelBirimGorev(PersonelBirimGorev personelBirimGorev) { 
		this.personelBirimGorev = personelBirimGorev; 
	} 
	
	public PersonelBirimGorevFilter getPersonelBirimGorevFilter() { 
		return personelBirimGorevFilter; 
	} 
	 
	public void setPersonelBirimGorevFilter(PersonelBirimGorevFilter personelBirimGorevFilter) {  
		this.personelBirimGorevFilter = personelBirimGorevFilter;  
	}  
	 
	@Override  
	public BaseFilter getFilter() {  
		return getPersonelBirimGorevFilter();  
	}   
 
	@Override	
	public void insert() {	
		super.insert();	
		getPersonelBirimGorev().setPersonelRef(getPersonelBirimGorevFilter().getPersonelRef());
		getPersonelBirimGorev().setDurum(PersonelGorevDurum._AKTIF);
	}	
	
} // class 
