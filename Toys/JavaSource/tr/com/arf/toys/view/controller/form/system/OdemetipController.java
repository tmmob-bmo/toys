package tr.com.arf.toys.view.controller.form.system; 
  
 
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.toys.db.filter.system.OdemetipFilter;
import tr.com.arf.toys.db.model.system.Odemetip;
import tr.com.arf.toys.view.controller.form._base.ToysBaseFilteredController;
  
  
@ManagedBean 
@ViewScoped 
public class OdemetipController extends ToysBaseFilteredController {  
  
  private static final long serialVersionUID = 6531152165884980748L; 
  
	private Odemetip odemetip;  
	private OdemetipFilter odemetipFilter = new OdemetipFilter();  
  
	public OdemetipController() { 
		super(Odemetip.class);  
		setDefaultValues(false);  
		setOrderField("kod");  
		setAutoCompleteSearchColumns(new String[]{"kod"}); 
		setTable(null); 
		queryAction(); 
	} 
	
	
	public Odemetip getOdemetip() { 
		odemetip = (Odemetip) getEntity(); 
		return odemetip; 
	} 
	
	public void setOdemetip(Odemetip odemetip) { 
		this.odemetip = odemetip; 
	} 
	
	public OdemetipFilter getOdemetipFilter() { 
		return odemetipFilter; 
	} 
	 
	public void setOdemetipFilter(OdemetipFilter odemetipFilter) {  
		this.odemetipFilter = odemetipFilter;  
	}  
	 
	@Override  
	public BaseFilter getFilter() {  
		return getOdemetipFilter();  
	}  
	
	
 
	
} // class 
