package tr.com.arf.toys.view.controller.form._base;

import tr.com.arf.framework.view.controller.form._base.BaseFilteredControllerNoPaging;
import tr.com.arf.toys.service.application.ManagedBeanLocator;
import tr.com.arf.toys.view.controller._common.SessionUser;

public abstract class ToysFilteredControllerNoPaging extends BaseFilteredControllerNoPaging {
	
	private static final long serialVersionUID = 7356771074807861973L;

	protected ToysFilteredControllerNoPaging(Class<?> c) {
		super(c); 
	}	 
	
	public SessionUser getSessionUser(){
		return ManagedBeanLocator.locateSessionUser();
	}
	
	@Override
	public String getStaticWhereCondition(String modelName) {
		return getSessionUser().getStaticWhereCondition(modelName);		
	}
	
	@Override
	public String[] getAutoCompleteSearchColumns(String modelName) {
		return getSessionUser().getAutoCompleteSearchColumns(modelName);		
	}
	
	@Override
	public void removeObjectFromSessionFilter(String objectName){		 
		getSessionUser().removeSessionFilterObject(objectName); 
	}
	
	@Override
	public void putObjectToSessionFilter(String objectName, Object object){
		getSessionUser().addToSessionFilters(objectName, object);
	}
	
	@Override
	public Object getObjectFromSessionFilter(String objectName){		 
		return getSessionUser().getSessionFilterObject(objectName); 
	}
	
}