package tr.com.arf.toys.view.controller.form.kisi; 
  
 
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.model.SelectItem;

import tr.com.arf.framework.db.enumerated._common.EvetHayir;
import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.framework.db.model._base.BaseEntity;
import tr.com.arf.framework.utility.exception.data.DBException;
import tr.com.arf.toys.db.filter.kisi.KomisyonDonemFilter;
import tr.com.arf.toys.db.model.kisi.Donem;
import tr.com.arf.toys.db.model.kisi.KomisyonDonem;
import tr.com.arf.toys.db.model.kisi.KomisyonDonemUye;
import tr.com.arf.toys.db.model.system.Birim;
import tr.com.arf.toys.db.model.system.Komisyon;
import tr.com.arf.toys.service.application.ApplicationDescriptor;
import tr.com.arf.toys.service.application.ManagedBeanLocator;
import tr.com.arf.toys.view.controller.form._base.ToysBaseFilteredController;
  

@ManagedBean 
@ViewScoped 
public class KomisyonDonemController extends ToysBaseFilteredController {  
  
  private static final long serialVersionUID = 6531152165884980748L; 
  
	private KomisyonDonem komisyonDonem;  
	private KomisyonDonemFilter komisyonDonemFilter = new KomisyonDonemFilter();  
  
	public KomisyonDonemController() { 
		super(KomisyonDonem.class);  
		setDefaultValues(false);  
		setTable(null); 
		setOrderField("donemRef.baslangictarih desc");  
		setAutoCompleteSearchColumns(new String[]{"donemRef.baslangictarih desc"});
		setStaticWhereCondition(getSessionUser().createStaticWhereCondition("o."));
		if (getMasterEntity() != null) {
			if (getMasterEntity().getClass() == Donem.class) {
				setMasterObjectName(ApplicationDescriptor._DONEM_MODEL);
				setMasterOutcome(ApplicationDescriptor._DONEM_MODEL);
				getKomisyonDonemFilter().setDonemRef((Donem) getMasterEntity());
			} else {
				setMasterOutcome(null);
			}
		}
		getKomisyonDonemFilter().createQueryCriterias();   
		queryAction(); 
	} 
	
	public BaseEntity getMasterEntity(){
		if(getObjectFromSessionFilter(ApplicationDescriptor._DONEM_MODEL) != null){
			return (Donem) getObjectFromSessionFilter(ApplicationDescriptor._DONEM_MODEL);
		}  
		else {
			return null;
		}			
	} 	
	
	public KomisyonDonem getKomisyonDonem() {
		komisyonDonem = (KomisyonDonem) getEntity();
		return komisyonDonem;
	}

	public void setKomisyonDonem(KomisyonDonem komisyonDonem) {
		this.komisyonDonem = komisyonDonem;
	}

	public KomisyonDonemFilter getKomisyonDonemFilter() {
		return komisyonDonemFilter;
	}

	public void setKomisyonDonemFilter(KomisyonDonemFilter komisyonDonemFilter) {
		this.komisyonDonemFilter = komisyonDonemFilter;
	}

	@Override  
	public BaseFilter getFilter() {  
		return getKomisyonDonemFilter();  
	}  
	
	public String komisyonDonemUye() {	
		if (!setSelected()) {
			return "";
		}	
		getSessionUser().setLegalAccess(1);	
		putObjectToSessionFilter(ApplicationDescriptor._KOMISYONDONEM_MODEL, getKomisyonDonem());
		return ManagedBeanLocator.locateMenuController().gotoPage("menu_KomisyonDonemUye"); 
	} 
	
	@Override  
	public void insert() {  
		super.insert();
		getKomisyonDonem().setBirimRef(getSessionUser().getPersonelRef().getBirimRef());
		try {
			if(getDBOperator().recordCount(Donem.class.getSimpleName(), "o.birimRef.rID=" + getSessionUser().getPersonelRef().getBirimRef().getRID() + 
					" AND o.anadonemRef.aktif=" + EvetHayir._EVET.getCode()) > 0){
				@SuppressWarnings("unchecked")
				List <Donem> donem = getDBOperator().load(Donem.class.getSimpleName(), "o.birimRef.rID=" + getSessionUser().getPersonelRef().getBirimRef().getRID() + 
						" AND o.anadonemRef.aktif=" + EvetHayir._EVET.getCode(),"");
				changeDonem(donem);
				getKomisyonDonem().setDonemRef(donem.get(0));
				
			}
		} catch (DBException e) {
			// TODO Auto-generated catch block
			logYaz("Exception @" + getModelName() + "Controller :", e);
		} 
		super.fillDualListFor(KomisyonDonemUye.class.getSimpleName(), "o.rID < 1", "");
	}  
	
	public String komisyonDonemToplanti() {	
		if (!setSelected()) {
			return "";
		}	
		getSessionUser().setLegalAccess(1);	
		putObjectToSessionFilter(ApplicationDescriptor._KOMISYONDONEM_MODEL, getKomisyonDonem());
		return ManagedBeanLocator.locateMenuController().gotoPage("menu_KomisyonDonemToplanti"); 
	}
	
	@Override
	public void save() {
		super.justSave();
		if(!isEmpty(getDualList().getSource())){
			List<BaseEntity> komisyonDonemUyeList = getDualList().getSource(); 			
			for(BaseEntity b : komisyonDonemUyeList){
				
				try {
					getDBOperator().delete(b);
				} catch (DBException e) {
					logYaz("Exception @" + getModelName() + "Controller :", e);
				}
			}
		}
		queryAction();
	}
	
	@SuppressWarnings("unchecked")
	public void handleChangeForKomisyonDonem(AjaxBehaviorEvent event) {
		try { 
			HtmlSelectOneMenu menu = (HtmlSelectOneMenu) event.getComponent(); 			 
			Komisyon secilenKomisyon = (Komisyon) menu.getValue();
			List <KomisyonDonem> secilenKomisyonDonemListesi=getDBOperator().load(KomisyonDonem.class.getSimpleName(), "o.komisyonRef.rID=" + secilenKomisyon.getRID(),"");
			if (!isEmpty(secilenKomisyonDonemListesi)){
				for (KomisyonDonem komisyonDonem1 : secilenKomisyonDonemListesi){
					super.fillDualListFor(KomisyonDonemUye.class.getSimpleName(), "o.komisyonDonemRef.rID =" + komisyonDonem1.getRID(), "");
				}
				
			} else {
				super.fillDualListFor(KomisyonDonemUye.class.getSimpleName(), "o.rID < 1", "");
			}
			
		} catch(Exception e){
			logYaz("Exception @" + getModelName() + "Controller :", e);
		}
	} 
	
	@SuppressWarnings("unchecked")
	public void handleChangeForBirim(AjaxBehaviorEvent event) {
		try {
			List <Donem> changeDonemListesi=new ArrayList<Donem>(); 
			HtmlSelectOneMenu menu = (HtmlSelectOneMenu) event.getComponent(); 			 
			Birim secilenBirim = (Birim) menu.getValue();
			List <Donem> donemListesi=getDBOperator().load(Donem.class.getSimpleName(), "o.birimRef.rID=" + secilenBirim.getRID() + 
					" AND o.anadonemRef.aktif=" + EvetHayir._EVET.getCode(),"");
			if (!isEmpty(donemListesi)){
				changeDonem(donemListesi);
			}
			else {
				donemListesi=getDBOperator().load(Donem.class.getSimpleName(), "o.birimRef.rID=" + secilenBirim.getRID() + 
						" AND o.anadonemRef.aktif=" + EvetHayir._HAYIR.getCode(),"");
				if (!isEmpty(donemListesi)){
					for (Donem donem :donemListesi){
							Date nowdate =new Date();
							if (donem.getBitistarih().after(nowdate) && donem.getAnadonemRef().getAktif() == EvetHayir._HAYIR){
								changeDonemListesi.add(donem);
						}
					}
				}
				changeDonem(changeDonemListesi);
			}
			
			
			
			
		} catch(Exception e){
			logYaz("Error @handleChange :" + e.getMessage());
			logYaz("Exception @" + getModelName() + "Controller :", e);
		}
	} 
	
	private SelectItem[] donemSelectItemList;
	
	public SelectItem[] getDonemSelectItemList() {
		return donemSelectItemList;
	}

	public void setDonemSelectItemList(SelectItem[] donemSelectItemList) {
		this.donemSelectItemList = donemSelectItemList;
	}

	public void changeDonem(List<Donem> donemListesi) {
		if (!isEmpty(donemListesi)) {
			for (Donem donem : donemListesi) {
				donemSelectItemList = new SelectItem[donemListesi.size() + 1];
				donemSelectItemList[0] = new SelectItem(null, "");
				int i = 1;
				donemSelectItemList[i++] = new SelectItem(donem, donem.getUIString());
			}
			setDonemSelectItemList(donemSelectItemList);
		} else {
			setDonemSelectItemList(new SelectItem[0]);
		} 
	}
	
	@Override
	public void update() {
		super.update();
		super.fillDualListFor(KomisyonDonemUye.class.getSimpleName(), "o.komisyonDonemRef.rID=" + komisyonDonem.getRID(), "");
	}
	
	public String komisyonDonemUyeDurum() {	
		if (!setSelected()) {
			return "";
		}	
		getSessionUser().setLegalAccess(1);	
		putObjectToSessionFilter(ApplicationDescriptor._KOMISYONDONEM_MODEL, getKomisyonDonem());
		return ManagedBeanLocator.locateMenuController().gotoPage("menu_KomisyonDonemUyeDurum"); 
	} 
 
} // class 
