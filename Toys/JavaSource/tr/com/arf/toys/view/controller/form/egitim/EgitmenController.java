package tr.com.arf.toys.view.controller.form.egitim;


import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.toys.db.filter.egitim.EgitmenFilter;
import tr.com.arf.toys.db.model.egitim.Egitmen;
import tr.com.arf.toys.view.controller.form._base.ToysBaseFilteredController;

@ManagedBean
@ViewScoped
public class EgitmenController extends ToysBaseFilteredController {

	private static final long serialVersionUID = 6531152165884980748L;

	private Egitmen egitmen;
	private EgitmenFilter egitmenFilter = new EgitmenFilter();

	public EgitmenController() {
		super(Egitmen.class);
		setDefaultValues(false);
		setLoggable(false);
		setTable(null);
		queryAction();
	}

	public Egitmen getEgitmen() {
		egitmen=(Egitmen) getEntity();
		return egitmen;
	}

	public void setEgitmen(Egitmen egitmen) {
		this.egitmen = egitmen;
	}

	public EgitmenFilter getEgitmenFilter() {
		return egitmenFilter;
	}

	public void setEgitmenFilter(EgitmenFilter egitmenFilter) {
		this.egitmenFilter = egitmenFilter;
	}

	@Override
	public BaseFilter getFilter() {
		return getEgitmenFilter();
	}
	
} // class 
