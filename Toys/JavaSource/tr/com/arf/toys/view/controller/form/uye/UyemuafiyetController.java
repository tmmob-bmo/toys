package tr.com.arf.toys.view.controller.form.uye; 
  
 
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.toys.db.filter.uye.UyemuafiyetFilter;
import tr.com.arf.toys.db.model.uye.Uyeaidat;
import tr.com.arf.toys.db.model.uye.Uyemuafiyet;
import tr.com.arf.toys.service.application.ApplicationDescriptor;
import tr.com.arf.toys.view.controller.form._base.ToysBaseFilteredController;
  
  
@ManagedBean 
@ViewScoped 
public class UyemuafiyetController extends ToysBaseFilteredController {  
  
  private static final long serialVersionUID = 6531152165884980748L; 
  
	private Uyemuafiyet uyemuafiyet;  
	private UyemuafiyetFilter uyemuafiyetFilter = new UyemuafiyetFilter();  
  
	public UyemuafiyetController() { 
		super(Uyemuafiyet.class);  
		setDefaultValues(false);  
		setOrderField("aciklama");  
		setAutoCompleteSearchColumns(new String[]{"aciklama"}); 
		setTable(null); 
		if(getMasterEntity() != null){ 
			setMasterObjectName(ApplicationDescriptor._UYEAIDAT_MODEL);
			setMasterOutcome(ApplicationDescriptor._UYEAIDAT_MODEL);
			getUyemuafiyetFilter().setrID(getMasterEntity().getUyemuafiyetRef().getRID());
			getUyemuafiyetFilter().createQueryCriterias();   
		} 
		queryAction(); 
	} 
 
	public UyemuafiyetController(Object object) { 
		super(Uyemuafiyet.class);   
	}
	
	public Uyeaidat getMasterEntity(){ 
		return (Uyeaidat) getObjectFromSessionFilter(ApplicationDescriptor._UYEAIDAT_MODEL); 
	} 	
	
	public Uyemuafiyet getUyemuafiyet() { 
		uyemuafiyet = (Uyemuafiyet) getEntity(); 
		return uyemuafiyet; 
	} 
	
	public void setUyemuafiyet(Uyemuafiyet uyemuafiyet) { 
		this.uyemuafiyet = uyemuafiyet; 
	} 
	
	public UyemuafiyetFilter getUyemuafiyetFilter() { 
		return uyemuafiyetFilter; 
	} 
	 
	public void setUyemuafiyetFilter(UyemuafiyetFilter uyemuafiyetFilter) {  
		this.uyemuafiyetFilter = uyemuafiyetFilter;  
	}  
	 
	@Override  
	public BaseFilter getFilter() {  
		return getUyemuafiyetFilter();  
	}  
	
	@Override	
	public void insert() {	
		super.insert();	 
		getUyemuafiyet().setUyeRef(getMasterEntity().getUyeRef());
	}	
	
} // class 
