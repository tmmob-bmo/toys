package tr.com.arf.toys.view.controller.form.uye; 
  
 
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.toys.db.enumerated.uye.UyeBelgeDurum;
import tr.com.arf.toys.db.filter.uye.UyebelgeFilter;
import tr.com.arf.toys.db.model.uye.Uye;
import tr.com.arf.toys.db.model.uye.Uyebelge;
import tr.com.arf.toys.service.application.ApplicationDescriptor;
import tr.com.arf.toys.view.controller.form._base.ToysBaseFilteredController;
  
  
@ManagedBean 
@ViewScoped 
public class UyebelgeController extends ToysBaseFilteredController {  
  
  private static final long serialVersionUID = 6531152165884980748L; 
  
	private Uyebelge uyebelge;  
	private UyebelgeFilter uyebelgeFilter = new UyebelgeFilter();  
  
	public UyebelgeController() { 
		super(Uyebelge.class);    
		setDefaultValues(false);  
		setOrderField("aciklama");  
		setAutoCompleteSearchColumns(new String[]{"aciklama"});
		setLoggable(true);
		setTable(null); 
		if(getMasterEntity() != null){ 
			setMasterObjectName(ApplicationDescriptor._UYE_MODEL);
			setMasterOutcome(ApplicationDescriptor._UYE_MODEL);
			getUyebelgeFilter().setUyeRef(getMasterEntity());
			getUyebelgeFilter().createQueryCriterias();   
		} 
		queryAction(); 
	}  
	
	public UyebelgeController(Object object) { 
		super(Uyebelge.class);  
		setLoggable(true); 
	}
 
	public Uye getMasterEntity(){
		if(getUyebelgeFilter().getUyeRef() != null){
			return getUyebelgeFilter().getUyeRef();
		} else {
			return (Uye) getObjectFromSessionFilter(ApplicationDescriptor._UYE_MODEL);
		}			
	}  
	
	public Uyebelge getUyebelge() { 
		uyebelge = (Uyebelge) getEntity(); 
		return uyebelge; 
	} 
	
	public void setUyebelge(Uyebelge uyebelge) { 
		this.uyebelge = uyebelge; 
	} 
	
	public UyebelgeFilter getUyebelgeFilter() { 
		return uyebelgeFilter; 
	} 
	 
	public void setUyebelgeFilter(UyebelgeFilter uyebelgeFilter) {  
		this.uyebelgeFilter = uyebelgeFilter;  
	}

	@Override
	public BaseFilter getFilter() {
		return getUyebelgeFilter();
	}

	@Override
	public void resetFilter() {
		super.resetFilterOnly();
		if (getMasterEntity() != null) {
			getUyebelgeFilter().setUyeRef(getMasterEntity());
		}
		queryAction();
	}

	@Override
	public void insert() {
		super.insert();
		getUyebelge().setUyeRef(getUyebelgeFilter().getUyeRef());
	}
	
	@Override
	public void save() {
		if(this.uyebelge.getTeslimtarih() != null){
			this.uyebelge.setUyebelgedurum(UyeBelgeDurum._TESLIMEDILMIS);
		} else if(this.uyebelge.getHazirlanmatarih() != null){
			this.uyebelge.setUyebelgedurum(UyeBelgeDurum._HAZIRLANMIS);
		} 
		super.save(); 
	}
	
	
	
} // class 
