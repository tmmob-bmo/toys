package tr.com.arf.toys.view.controller.form.iletisim;

import java.io.IOException;
import java.util.Date;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.component.html.HtmlCommandButton;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;
import javax.faces.event.AjaxBehaviorEvent;

import tr.com.arf.framework.db.enumerated._common.EvetHayir;
import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.framework.db.model._base.BaseEntity;
import tr.com.arf.framework.utility.exception.data.DBException;
import tr.com.arf.toys.db.enumerated.iletisim.GonderimDurum;
import tr.com.arf.toys.db.enumerated.iletisim.GonderimTuru;
import tr.com.arf.toys.db.enumerated.iletisim.ReferansTipi;
import tr.com.arf.toys.db.filter.iletisim.EpostaanlikgonderimlogFilter;
import tr.com.arf.toys.db.filter.iletisim.GonderimFilter;
import tr.com.arf.toys.db.filter.iletisim.IletisimlistedetayFilter;
import tr.com.arf.toys.db.model.egitim.Egitimkatilimci;
import tr.com.arf.toys.db.model.egitim.Egitimogretmen;
import tr.com.arf.toys.db.model.etkinlik.Etkinlikkatilimci;
import tr.com.arf.toys.db.model.iletisim.Epostagonderimlog;
import tr.com.arf.toys.db.model.iletisim.Gonderim;
import tr.com.arf.toys.db.model.iletisim.Iletisimliste;
import tr.com.arf.toys.db.model.iletisim.Iletisimlistedetay;
import tr.com.arf.toys.db.model.iletisim.Smsgonderimlog;
import tr.com.arf.toys.db.model.kisi.IsYeriTemsilcileri;
import tr.com.arf.toys.db.model.kisi.Kisi;
import tr.com.arf.toys.db.model.system.Birim;
import tr.com.arf.toys.db.model.system.KurumKisi;
import tr.com.arf.toys.db.model.system.SistemParametre;
import tr.com.arf.toys.db.model.uye.Uye;
import tr.com.arf.toys.service.application.ApplicationDescriptor;
import tr.com.arf.toys.service.application.ManagedBeanLocator;
import tr.com.arf.toys.view.controller.form._base.ToysBaseFilteredController;

@ManagedBean
@ViewScoped
public class GonderimController extends ToysBaseFilteredController {

	private static final long serialVersionUID = 6531152165884980748L;

	private Gonderim gonderim;
	public Gonderim gonderim1;
	private GonderimFilter gonderimFilter = new GonderimFilter();
	public EvetHayir icerikOrEditor;
	private final IletisimlistedetayFilter iletisimlistedetayFilter = new IletisimlistedetayFilter();
	private final EpostaanlikgonderimlogFilter epostaanlikgonderimlogFilter = new EpostaanlikgonderimlogFilter();
	Birim gonderenBirimRef=new Birim();
	Iletisimliste iletisimlisteRef = new Iletisimliste();
	String sEmailMi;
	private Boolean dialogRender;
	private Boolean handleSms;
	private Boolean handleEposta;

	public GonderimController() {
		super(Gonderim.class);
		setDefaultValues(false);
		setOrderField("iletisimlisteRef.rID");
		setLoggable(false);
		setTable(null);
		setAutoCompleteSearchColumns(new String[] { "iletisimlisteRef" });
		if (getMasterEntity() != null) {
			setMasterObjectName(ApplicationDescriptor._ILETISIMLISTE_MODEL);
			setMasterOutcome(ApplicationDescriptor._ILETISIMLISTE_MODEL);
			getGonderimFilter().setIletisimlisteRef(getMasterEntity());
			getGonderimFilter().createQueryCriterias();
		}
		queryAction();
	}

	public Iletisimliste getMasterEntity() {
		if (getGonderimFilter().getIletisimlisteRef() != null) {
			return getGonderimFilter().getIletisimlisteRef();
		} else {
			return (Iletisimliste) getObjectFromSessionFilter(ApplicationDescriptor._ILETISIMLISTE_MODEL);
		}
	}
	
	private Boolean onizlemeDialog=false;
	
	public Boolean getOnizlemeDialog() {
		return onizlemeDialog;
	}

	public void setOnizlemeDialog(Boolean onizlemeDialog) {
		this.onizlemeDialog = onizlemeDialog;
	}

	@Override
	public void setSelectedRID(String selectedRID) {
		super.setSelectedRID(selectedRID);
		setOnizlemeDialog(true);
	}
	public void onizlemedialogkapat(){
		setOnizlemeDialog(false);
	}

	public Gonderim getGonderim() {
		gonderim = (Gonderim) getEntity();
		return gonderim;
	}

	public void setGonderim(Gonderim gonderim) {
		this.gonderim = gonderim;
	}

	public GonderimFilter getGonderimFilter() {
		return gonderimFilter;
	}

	public void setGonderimFilter(GonderimFilter gonderimFilter) {
		this.gonderimFilter = gonderimFilter;
	}

	public EvetHayir getIcerikOrEditor() {
		return icerikOrEditor;
	}

	public void setIcerikOrEditor(EvetHayir icerikOrEditor) {
		this.icerikOrEditor = icerikOrEditor;
	}

	public Boolean getDialogRender() {
		return dialogRender;
	}

	public void setDialogRender(Boolean dialogRender) {
		this.dialogRender = dialogRender;
	}

	public Boolean getHandleSms() {
		return handleSms;
	}

	public void setHandleSms(Boolean handleSms) {
		this.handleSms = handleSms;
	}

	public Boolean getHandleEposta() {
		return handleEposta;
	}

	public void setHandleEposta(Boolean handleEposta) {
		this.handleEposta = handleEposta;
	}

	@Override
	public BaseFilter getFilter() {
		return getGonderimFilter();
	}

	public String epostagonderimlog() {
		if (!setSelected()) {
			return "";
		}
		getSessionUser().setLegalAccess(1);
		putObjectToSessionFilter(ApplicationDescriptor._GONDERIM_MODEL,
				getGonderim());
		return ManagedBeanLocator.locateMenuController().gotoPage(
				"menu_Epostagonderimlog");
	}

	public String smsgonderimlog() {
		if (!setSelected()) {
			return "";
		}
		getSessionUser().setLegalAccess(1);
		putObjectToSessionFilter(ApplicationDescriptor._GONDERIM_MODEL,
				getGonderim());
		return ManagedBeanLocator.locateMenuController().gotoPage(
				"menu_Smsgonderimlog");
	}

	public boolean handleZamanlamaliChange(AjaxBehaviorEvent event) {
		try {
			HtmlSelectOneMenu menu = (HtmlSelectOneMenu) event.getComponent();
			if (menu.getValue() instanceof EvetHayir) {
				if (this.gonderim.getZamanlamali() == EvetHayir._EVET) {
					return true;
				}

			} else {
				return false;
			}
		} catch (Exception e) {
			logYaz("Exception @" + getModelName() + "Controller :", e);
		}
		return true;
	}

	public boolean handleIcerikOrEditor(AjaxBehaviorEvent event1) {
		try {
			HtmlSelectOneMenu menuForIcerikOrEditor = (HtmlSelectOneMenu) event1
					.getComponent();
			if (menuForIcerikOrEditor.getValue() instanceof EvetHayir) {
				if (getIcerikOrEditor().equals(EvetHayir._EVET)) {
					return true;
				} else {
					return false;
				}

			} else {
				return false;
			}
		} catch (Exception e) {
			logYaz("Exception @" + getModelName() + "Controller :", e);
		}
		return true;
	}

//	public Kisi handleKisi(AjaxBehaviorEvent event2) {
//		try {
//			HtmlSelectOneMenu menuForKisi = (HtmlSelectOneMenu) event2
//					.getComponent();
//			if (menuForKisi.getValue() instanceof Kisi) {
//				gonderenRef = (Kisi) menuForKisi.getValue();
//				return gonderenRef;
//			} else {
//				return null;
//			}
//		} catch (Exception e) {
//			logYaz("Exception @" + getModelName() + "Controller :", e);
//		}
//		return null;
//	}

	public Iletisimliste handleIletisimListe(AjaxBehaviorEvent event2) {
		try {
			HtmlSelectOneMenu menuForIletisimListe = (HtmlSelectOneMenu) event2
					.getComponent();
			if (menuForIletisimListe.getValue() instanceof Iletisimliste) {
				iletisimlisteRef = (Iletisimliste) menuForIletisimListe
						.getValue();
				return iletisimlisteRef;
			} else {
				return null;
			}
		} catch (Exception e) {
			logYaz("Exception @" + getModelName() + "Controller :", e);
		}
		return null;
	}

	@SuppressWarnings("unused")
	public Iletisimliste handleSave(AjaxBehaviorEvent event2) {
		try {
			HtmlCommandButton button = (HtmlCommandButton) event2
					.getComponent();
			// logYaz(button.getValue().toString());
		} catch (Exception e) {
			logYaz("Exception @" + getModelName() + "Controller :", e);
		}
		return null;
	}

	public void handleSms(AjaxBehaviorEvent event2) {
		try {
			HtmlSelectOneMenu menuForSMS = (HtmlSelectOneMenu) event2
					.getComponent();
			if (menuForSMS.getValue() instanceof GonderimTuru) {
				if ((GonderimTuru) menuForSMS.getValue() == GonderimTuru._EPOSTA) {
					setHandleEposta(true);
					setHandleSms(false);
				} else {
					setHandleSms(true);
					setHandleEposta(false);
				}
			}
		} catch (Exception e) {
			logYaz("Exception @" + getModelName() + "Controller :", e);
		}
	}

	public boolean getSMSKontrol() {
		if (this.gonderim.getGonderimturu() == GonderimTuru._SMS) {
			return false;
		}
		return true;
	}

	public boolean getEMailKontrol() {
		if (this.gonderim.getGonderimturu() == GonderimTuru._EPOSTA) {
			return false;
		}
		return true;
	}

	@Override
	public void save() {
		this.gonderim.setIletisimlisteRef(getGonderimFilter().getIletisimlisteRef());
		if (this.gonderim.getZamanlamali() == EvetHayir._HAYIR) {
			Date d = new Date();
			this.gonderim.setGonderimzamani(d);
		}
		// /////// E-Mail ya da sms atildiktan sonra set edilmeli/////
		this.gonderim.setDurum(GonderimDurum._ILETILDI);
//		this.gonderim.setGonderenRef(getSessionUser().getKullanici().getKisiRef());
//		this.gonderim.setBirimRef();
		super.save();

		// //E-Mail Logger (Boolean True)
		if (this.gonderim.getGonderimturu() == GonderimTuru._EPOSTA) {
			saveRecursive(this.gonderim.getIletisimlisteRef().getRID(), true,false);
		}
		// //SMS LOGGER (Boolean False)
		else if (this.gonderim.getGonderimturu() == GonderimTuru._SMS) {
			saveRecursive(this.gonderim.getIletisimlisteRef().getRID(), false,false);
		}
	}

	// //// Kisinin iletisim tercihine bakilmadan gonderim yapmak icin
	public void saveTercihsiz() {
		this.gonderim.setIletisimlisteRef(getGonderimFilter()
				.getIletisimlisteRef());
		if (this.gonderim.getZamanlamali() == EvetHayir._HAYIR) {
			Date d = new Date();
			this.gonderim.setGonderimzamani(d);
		}
		// /////// E-Mail ya da sms atildiktan sonra set edilmeli/////
		this.gonderim.setDurum(GonderimDurum._ILETILDI);
		// //////////////////////////////////////////////// 
		save();
		// //E-Mail Logger (Boolean True)
		if (this.gonderim.getGonderimturu() == GonderimTuru._EPOSTA) {
			saveRecursive(this.gonderim.getIletisimlisteRef().getRID(), true,
					true);
		}
		// //SMS LOGGER (Boolean False)
		else if (this.gonderim.getGonderimturu() == GonderimTuru._SMS) {
			saveRecursive(this.gonderim.getIletisimlisteRef().getRID(), false,
					true);
		}
	}

	@SuppressWarnings("unchecked")
	public void saveRecursive(Long altRef, boolean gonderimTip,	boolean iletisimTercih) {
		iletisimlistedetayFilter.createQueryCriterias(); 
		try {
			if (getDBOperator().recordCount(Iletisimlistedetay.class.getSimpleName(),"o.iletisimlisteRef.rID=" + altRef)>0){
				List<Iletisimlistedetay> iletisimlistedetay = getDBOperator().load(Iletisimlistedetay.class.getSimpleName(),"o.iletisimlisteRef.rID=" + altRef, "o.rID");
				for (Iletisimlistedetay b : iletisimlistedetay) {
					if (b.getReferansTipi() == ReferansTipi._KISI) {
						persistKisiLog(b, gonderimTip, iletisimTercih);
					} else if (b.getReferansTipi() == ReferansTipi._UYE) {
						persistUyeLog(b, gonderimTip, iletisimTercih);
					} else if (b.getReferansTipi() == ReferansTipi._BIRIM) {
						persistBirimUyeLog(b, gonderimTip, iletisimTercih);
					} else if (b.getReferansTipi() == ReferansTipi._KURUM) {
						persistKurumUyeLog(b, gonderimTip, iletisimTercih);
					} else if (b.getReferansTipi() == ReferansTipi._KURUL) {

					} else if (b.getReferansTipi() == ReferansTipi._KOMISYON) {

					} else if (b.getReferansTipi() == ReferansTipi._EGITMEN) {
						persistEgitmenLog(b, gonderimTip, iletisimTercih);
					} else if (b.getReferansTipi() == ReferansTipi._EGITIMKATILIMCI) {
						persistEgitimKatilimciLog(b, gonderimTip, iletisimTercih);
					} else if (b.getReferansTipi() == ReferansTipi._ETKINLIKKATILIMCI) {
						persistEtkinlikKatilimciLog(b, gonderimTip, iletisimTercih);
					} else if (b.getReferansTipi() == ReferansTipi._ISYERITEMSILCISI) {
						persistIsyeritemsilcileriLog(b, gonderimTip, iletisimTercih);
					}
				}
			}
		} catch (DBException e) {
			logYaz("GonderimController @saveRecursive=" + e.getMessage());
			logYaz("Exception @" + getModelName() + "Controller :", e);
		}
		
//		List<Iletisimliste> iletisimliste = getDBOperator().load(Iletisimliste.class.getSimpleName(), "o.ustRef=" + altRef,"o.rID");
//		for (Iletisimliste d : iletisimliste) {
//			saveRecursive(d.getRID(), gonderimTip, false);
//		}

	}

	// iletisimlistedetay tablosunda bu gonderim ile ayni master
	// entitye sahip olan Kisilerin loglarini persist etmek icin
	// True--E.mail.... False--SMS
	public void persistKisiLog(Iletisimlistedetay b, boolean gonderimTip,boolean iletisimTercih) {
		try {
			if (gonderimTip == true) { 
				Epostagonderimlog e = new Epostagonderimlog();
				if(getDBOperator().recordCount(Kisi.class.getSimpleName(), "o.rID="+ b.getReferans())>0){
					Kisi kisi = (Kisi) getDBOperator().load(Kisi.class.getSimpleName(), "o.rID="+ b.getReferans(), "o.rID").get(0);
					e.setKisiRef(kisi);
				}
				timeSetterForEMail(this.gonderim, e);
				setLogDurum(this.gonderim, e);
				e.setAciklama(this.gonderim.getAciklama());
				e.setGonderimRef(this.gonderim);
//				e.setGonderenRef(this.gonderim.getGonderenRef());
				e.setBirimRef(this.gonderim.getBirimRef());
				epostaanlikgonderimlogFilter.createQueryCriterias();
				insertEMailLog(e, iletisimTercih);
			} else { 
				Smsgonderimlog sms = new Smsgonderimlog();
				if(getDBOperator().recordCount(Kisi.class.getSimpleName(), "o.rID="+ b.getReferans())>0){
					Kisi kisi = (Kisi) getDBOperator().load(Kisi.class.getSimpleName(), "o.rID="+ b.getReferans(), "o.rID").get(0);
					sms.setKisiRef(kisi);
					// Emailer emailer = new Emailer();
					// emailer.sendEmail("muratsumbul@ymail.com",
					// "anildogan@windowslive.com",
					// "Testing 1-2-3", "blah blah blah");
				}
				setLogDurum(this.gonderim, sms);
				timeSetterForSMS(this.gonderim, sms);
				sms.setAciklama(this.gonderim.getAciklama());
				sms.setGonderimRef(gonderim);
//				sms.setGonderenRef(this.gonderim.getGonderenRef());
				sms.setBirimRef(this.gonderim.getBirimRef());
				epostaanlikgonderimlogFilter.createQueryCriterias();
				insertSMSLog(sms, iletisimTercih);
			}
		} catch (DBException e1) {
			logYaz("GonderimController @persistKisiLog=" + e1.getMessage());
			e1.printStackTrace();
		}
	}

	// iletisimlistedetay tablosunda bu gonderim ile ayni master
	// entitye sahip olan UYELERin loglarini persist etmek icin
	// True--E.mail.... False--SMS
	public void persistUyeLog(Iletisimlistedetay b, boolean gonderimTip,boolean iletisimTercih) {
		try {
			if (gonderimTip == true) { 
				Epostagonderimlog e = new Epostagonderimlog();
				if(getDBOperator().recordCount(Uye.class.getSimpleName(),"o.rID=" + b.getReferans())>0){
					Uye uye = (Uye) getDBOperator().load(Uye.class.getSimpleName(),"o.rID=" + b.getReferans(), "o.rID").get(0);
							e.setKisiRef(uye.getKisiRef());
				}
				setLogDurum(this.gonderim, e);
				timeSetterForEMail(this.gonderim, e);
				e.setAciklama(this.gonderim.getAciklama());
				e.setGonderimRef(this.gonderim);
//				e.setGonderenRef(this.gonderim.getGonderenRef());
				e.setBirimRef(this.gonderim.getBirimRef());
				epostaanlikgonderimlogFilter.createQueryCriterias();
				insertEMailLog(e, iletisimTercih);
			} else { 
				Smsgonderimlog sms = new Smsgonderimlog();
				if(getDBOperator().recordCount(Uye.class.getSimpleName(),"o.rID=" + b.getReferans())>0){
					Uye uye = (Uye) getDBOperator().load(Uye.class.getSimpleName(),"o.rID=" + b.getReferans(), "o.rID").get(0);
							sms.setKisiRef(uye.getKisiRef());
				}
				setLogDurum(this.gonderim, sms);
				timeSetterForSMS(this.gonderim, sms);
				sms.setAciklama(this.gonderim.getAciklama());
				sms.setGonderimRef(gonderim);
//				sms.setGonderenRef(this.gonderim.getGonderenRef());
				sms.setBirimRef(this.gonderim.getBirimRef());
				epostaanlikgonderimlogFilter.createQueryCriterias();
				insertSMSLog(sms, iletisimTercih);
			}
		} catch (DBException e1) {
			logYaz("GonderimController @persistUyeLog="+ e1.getMessage());
			e1.printStackTrace();
		}
	}

	// iletisimlistedetay tablosunda bu gonderim ile ayni master
	// entitye sahip olan Birimin uyelerinin loglarini persist etmek
	// icin
	// True--E.mail.... False--SMS
	public void persistBirimUyeLog(Iletisimlistedetay b, boolean gonderimTip,boolean iletisimTercih) {
		try {
		if (gonderimTip == true) { 
			if (getDBOperator().recordCount(Uye.class.getSimpleName(),"o.birimRef=" + b.getReferans())>0){
				Uye brm = (Uye) getDBOperator().load(Uye.class.getSimpleName(),"o.birimRef=" + b.getReferans(), "o.rID").get(0);
				Epostagonderimlog e = new Epostagonderimlog();
				setLogDurum(this.gonderim, e);
				timeSetterForEMail(this.gonderim, e);
				e.setAciklama(this.gonderim.getAciklama());
				e.setGonderimRef(gonderim);
				e.setKisiRef(brm.getKisiRef());
				e.setGonderimRef(gonderim);
//				e.setGonderenRef(this.gonderim.getGonderenRef());
				e.setBirimRef(this.gonderim.getBirimRef());
				epostaanlikgonderimlogFilter.createQueryCriterias();
				insertEMailLog(e, iletisimTercih);
				}
			}
		else { 
			if (getDBOperator().recordCount(Uye.class.getSimpleName(),"o.birimRef=" + b.getReferans())>0){
				Uye brm = (Uye) getDBOperator().load(Uye.class.getSimpleName(),"o.birimRef=" + b.getReferans(), "o.rID").get(0);
						Smsgonderimlog sms = new Smsgonderimlog();
						setLogDurum(this.gonderim, sms);
						timeSetterForSMS(this.gonderim, sms);
						sms.setAciklama(this.gonderim.getAciklama());
						sms.setGonderimRef(gonderim);
						sms.setKisiRef(brm.getKisiRef());
						sms.setGonderimRef(gonderim);
//						sms.setGonderenRef(this.gonderim.getGonderenRef());
						sms.setBirimRef(this.gonderim.getBirimRef());
						epostaanlikgonderimlogFilter.createQueryCriterias();
						insertSMSLog(sms, iletisimTercih);
					}
			}
		} catch (DBException e) {
			logYaz("GonderimController @persistBirimUyeLog=" + e.getMessage());
			logYaz("Exception @" + getModelName() + "Controller :", e);
		}

	}

	// iletisimlistedetay tablosunda bu gonderim ile ayni master
	// entitye sahip olan Kurumun uyelerinin loglarini persist etmek
	// icin
	// True--E.mail.... False--SMS
	@SuppressWarnings("unchecked")
	public void persistKurumUyeLog(Iletisimlistedetay b, boolean gonderimTip,boolean iletisimTercih) {
		try {
			if (gonderimTip == true) { 
					if(getDBOperator().recordCount(KurumKisi.class.getSimpleName(),"o.kurumRef=" + b.getReferans())>0){
						List<KurumKisi> kurumUye = getDBOperator().load(KurumKisi.class.getSimpleName(),"o.kurumRef=" + b.getReferans(), "o.rID");
						for (KurumKisi krm : kurumUye) {
							Epostagonderimlog e = new Epostagonderimlog();
							setLogDurum(this.gonderim, e);
							timeSetterForEMail(this.gonderim, e);
							e.setAciklama(this.gonderim.getAciklama());
							e.setGonderimRef(gonderim);
							e.setKisiRef(krm.getKisiRef());
							e.setGonderimRef(gonderim);
//							e.setGonderenRef(this.gonderim.getGonderenRef());
							e.setBirimRef(this.gonderim.getBirimRef());
							epostaanlikgonderimlogFilter.createQueryCriterias();
							insertEMailLog(e, iletisimTercih);
						}
					}
			} else { 
				if(getDBOperator().recordCount(KurumKisi.class.getSimpleName(),"o.kurumRef=" + b.getReferans())>0){
					List<KurumKisi> kurumUye = getDBOperator().load(KurumKisi.class.getSimpleName(),"o.kurumRef=" + b.getReferans(), "o.rID");
					for (KurumKisi krm : kurumUye) {
						Smsgonderimlog sms = new Smsgonderimlog();
						setLogDurum(this.gonderim, sms);
						timeSetterForSMS(this.gonderim, sms);
						sms.setAciklama(this.gonderim.getAciklama());
						sms.setGonderimRef(gonderim);
						sms.setKisiRef(krm.getKisiRef());
						sms.setGonderimRef(gonderim);
//						sms.setGonderenRef(this.gonderim.getGonderenRef());
						sms.setBirimRef(this.gonderim.getBirimRef());
						epostaanlikgonderimlogFilter.createQueryCriterias();
						insertSMSLog(sms, iletisimTercih);
					}
				}
			}
		} catch (DBException e) {
			logYaz("GonderimController @persistKurumUyeLog=" + e.getMessage());
			logYaz("Exception @" + getModelName() + "Controller :", e);
		}
	}

	// iletisimlistedetay tablosunda bu gonderim ile ayni master
	// entitye sahip olan Egitimin Egitimkatilimcilarinin loglarini
	// persist
	// etmek icin
	// True--E.mail.... False--SMS
	@SuppressWarnings("unchecked")
	public void persistEgitimKatilimciLog(Iletisimlistedetay b,boolean gonderimTip, boolean iletisimTercih) {
		try {
			if (gonderimTip == true) { 
				if (getDBOperator().recordCount(Egitimkatilimci.class.getSimpleName(),"o.egitimRef=" + b.getReferans())>0){
					List<Egitimkatilimci> egitimKatilimci = getDBOperator().load(Egitimkatilimci.class.getSimpleName(),"o.egitimRef=" + b.getReferans(), "o.rID");
						for (Egitimkatilimci egtmktlmc : egitimKatilimci) {
							Epostagonderimlog e = new Epostagonderimlog();
							setLogDurum(this.gonderim, e);
							timeSetterForEMail(this.gonderim, e);
							e.setAciklama(this.gonderim.getAciklama());
							e.setGonderimRef(gonderim);
							e.setKisiRef(egtmktlmc.getKisiRef());
							e.setGonderimRef(gonderim);
//							e.setGonderenRef(this.gonderim.getGonderenRef());
							e.setBirimRef(this.gonderim.getBirimRef());
							epostaanlikgonderimlogFilter.createQueryCriterias();
							insertEMailLog(e, iletisimTercih);
						}
				}
			} else { 
				if (getDBOperator().recordCount(Egitimkatilimci.class.getSimpleName(),"o.egitimRef=" + b.getReferans())>0){
					List<Egitimkatilimci> egitimKatilimci = getDBOperator().load(Egitimkatilimci.class.getSimpleName(),"o.egitimRef=" + b.getReferans(), "o.rID");
					for (Egitimkatilimci egtmktlmc : egitimKatilimci) {
						Smsgonderimlog sms = new Smsgonderimlog();
						setLogDurum(this.gonderim, sms);
						timeSetterForSMS(this.gonderim, sms);
						sms.setAciklama(this.gonderim.getAciklama());
						sms.setGonderimRef(gonderim);
						sms.setKisiRef(egtmktlmc.getKisiRef());
						sms.setGonderimRef(gonderim);
//						sms.setGonderenRef(this.gonderim.getGonderenRef());
						sms.setBirimRef(this.gonderim.getBirimRef());
						epostaanlikgonderimlogFilter.createQueryCriterias();
						insertSMSLog(sms, iletisimTercih);
					}
				}
			}
		} catch (DBException e) {
			logYaz("GonderimController @persistEgitimKatilimciLog = " + e.getMessage());
			logYaz("Exception @" + getModelName() + "Controller :", e);
		}
				
	}

	// iletisimlistedetay tablosunda bu gonderim ile ayni master
	// entitye sahip olan Etkinlik katilimcilarinin loglarini persist etmek
	// icin
	// True--E.mail.... False--SMS
	@SuppressWarnings("unchecked")
	public void persistEtkinlikKatilimciLog(Iletisimlistedetay b,boolean gonderimTip, boolean iletisimTercih) {
		try {	
			if (gonderimTip == true) { 
				if(getDBOperator().recordCount(Etkinlikkatilimci.class.getSimpleName(),"o.etkinlikRef=" + b.getReferans())>0){
					List<Etkinlikkatilimci> etkinlikKatilimci = getDBOperator().load(Etkinlikkatilimci.class.getSimpleName(),"o.etkinlikRef=" + b.getReferans(), "o.rID");
						for (Etkinlikkatilimci etknlkktlmc : etkinlikKatilimci) {
							Epostagonderimlog e = new Epostagonderimlog();
							setLogDurum(this.gonderim, e);
							timeSetterForEMail(this.gonderim, e);
							e.setAciklama(this.gonderim.getAciklama());
							e.setGonderimRef(gonderim);
							e.setKisiRef(etknlkktlmc.getKisiRef());
							e.setGonderimRef(gonderim);
//							e.setGonderenRef(this.gonderim.getGonderenRef());
							e.setBirimRef(this.gonderim.getBirimRef());
							epostaanlikgonderimlogFilter.createQueryCriterias();
							insertEMailLog(e, iletisimTercih);
						}
					}
			}else { 
				if(getDBOperator().recordCount(Etkinlikkatilimci.class.getSimpleName(),"o.etkinlikRef=" + b.getReferans())>0){
					List<Etkinlikkatilimci> etkinlikKatilimci = getDBOperator().load(Etkinlikkatilimci.class.getSimpleName(),"o.etkinlikRef=" + b.getReferans(), "o.rID");
						for (Etkinlikkatilimci etknlkktlmc : etkinlikKatilimci) {
							Smsgonderimlog sms = new Smsgonderimlog();
							setLogDurum(this.gonderim, sms);
							timeSetterForSMS(this.gonderim, sms);
							sms.setAciklama(this.gonderim.getAciklama());
							sms.setGonderimRef(gonderim);
							sms.setKisiRef(etknlkktlmc.getKisiRef());
							sms.setGonderimRef(gonderim);
//							sms.setGonderenRef(this.gonderim.getGonderenRef());
							sms.setBirimRef(this.gonderim.getBirimRef());
							epostaanlikgonderimlogFilter.createQueryCriterias();
							insertSMSLog(sms, iletisimTercih);
						}
					}
			}
		} catch (DBException e) {
			logYaz("GonderimController @persistEtkinlikKatilimciLog");
			logYaz("Exception @" + getModelName() + "Controller :", e);
		}
	
	}

	// Secilen Egitmenin loglarini persist etmek icin
	// True--E.mail.... False--SMS
	public void persistEgitmenLog(Iletisimlistedetay b, boolean gonderimTip,boolean iletisimTercih) {
		try {
			if (gonderimTip == true) { 
				Epostagonderimlog e = new Epostagonderimlog();
				e.setAciklama(this.gonderim.getAciklama());
				setLogDurum(this.gonderim, e);
				if (getDBOperator().recordCount(Egitimogretmen.class.getSimpleName(),"o.rID=" + b.getReferans())>0){
					Egitimogretmen egitmen = (Egitimogretmen) getDBOperator().load(Egitimogretmen.class.getSimpleName(),"o.rID=" + b.getReferans(), "o.rID").get(0);
						if (egitmen.getRID() == b.getReferans()) {
							e.setKisiRef(egitmen.getEgitmenRef().getKisiRef());
					}
				}
				timeSetterForEMail(this.gonderim, e);
				e.setGonderimRef(gonderim);
//				e.setGonderenRef(this.gonderim.getGonderenRef());
				e.setBirimRef(this.gonderim.getBirimRef());
				epostaanlikgonderimlogFilter.createQueryCriterias();
				insertEMailLog(e, iletisimTercih);
			} else { 
				Smsgonderimlog sms = new Smsgonderimlog();
				if (getDBOperator().recordCount(Egitimogretmen.class.getSimpleName(),"o.rID=" + b.getReferans())>0){
					Egitimogretmen egitmen = (Egitimogretmen) getDBOperator().load(Egitimogretmen.class.getSimpleName(),"o.rID=" + b.getReferans(), "o.rID").get(0);
						if (egitmen.getRID() == b.getReferans()) {
							sms.setKisiRef(egitmen.getEgitmenRef().getKisiRef());
					}
				}
				setLogDurum(this.gonderim, sms);
				timeSetterForSMS(this.gonderim, sms);
				sms.setAciklama(this.gonderim.getAciklama());
				sms.setGonderimRef(gonderim);
//				sms.setGonderenRef(this.gonderim.getGonderenRef());
				sms.setBirimRef(this.gonderim.getBirimRef());
				epostaanlikgonderimlogFilter.createQueryCriterias();
				insertSMSLog(sms, iletisimTercih);
			}
		} catch (DBException e1) {
			logYaz("GonderimController @persistEgitmenLog=" + e1.getMessage());
			e1.printStackTrace();
		}
	}

	// Secilen Isyeri Temsilcisinin loglarini persist etmek icin
	// True--E.mail.... False--SMS
	public void persistIsyeritemsilcileriLog(Iletisimlistedetay b,boolean gonderimTip, boolean iletisimTercih) {
		try {
			if (gonderimTip == true) { 
				Epostagonderimlog e = new Epostagonderimlog();
				if(getDBOperator().recordCount(IsYeriTemsilcileri.class.getSimpleName(),"o.rID=" + b.getReferans())>0){
					IsYeriTemsilcileri isyeritemsilcisi = (IsYeriTemsilcileri) getDBOperator().load(IsYeriTemsilcileri.class.getSimpleName(),"o.rID=" + b.getReferans(), "o.rID").get(0);
					e.setKisiRef(isyeritemsilcisi.getUyeRef().getKisiRef());
				}
				setLogDurum(this.gonderim, e);
				timeSetterForEMail(this.gonderim, e);
				e.setAciklama(this.gonderim.getAciklama());
				e.setGonderimRef(gonderim);
//				e.setGonderenRef(this.gonderim.getGonderenRef());
				e.setBirimRef(this.gonderim.getBirimRef());
				epostaanlikgonderimlogFilter.createQueryCriterias();
				insertEMailLog(e, iletisimTercih);
			} else { 
				Smsgonderimlog sms = new Smsgonderimlog();
				if(getDBOperator().recordCount(IsYeriTemsilcileri.class.getSimpleName(),"o.rID=" + b.getReferans())>0){
					IsYeriTemsilcileri isyeritemsilcisi = (IsYeriTemsilcileri) getDBOperator().load(IsYeriTemsilcileri.class.getSimpleName(),"o.rID=" + b.getReferans(), "o.rID").get(0);
					sms.setKisiRef(isyeritemsilcisi.getUyeRef().getKisiRef());
				}
				setLogDurum(this.gonderim, sms);
				timeSetterForSMS(this.gonderim, sms);
				sms.setGonderimRef(gonderim);
//				sms.setGonderenRef(this.gonderim.getGonderenRef());
				sms.setBirimRef(this.gonderim.getBirimRef());
				sms.setAciklama(this.gonderim.getAciklama());
				epostaanlikgonderimlogFilter.createQueryCriterias();
				insertSMSLog(sms, iletisimTercih);
			}
		} catch (DBException e1) {
			logYaz("GonderimController @persistIsyeritemsilcileriLog=" + e1.getMessage());
			e1.printStackTrace();
		}
	}

	public void insertSMSLog(Smsgonderimlog sms, boolean iletisimTercih) { 
		try {
			if (iletisimTercih == true) {
				getDBOperator().insert(sms);
			}

			else if (iletisimTercih == false) {
				if (sms.getKisiRef().getIletisimtercih() == EvetHayir._EVET) {
					getDBOperator().insert(sms);
				} else {
					// logYaz("Iletisim Tercihi Kesiti");
				}
			}
		} catch (DBException e1) {
			e1.printStackTrace();
		}
	}

	public void insertEMailLog(Epostagonderimlog e, boolean iletisimTercih) { 
		try {
			if (iletisimTercih == true) {
				getDBOperator().insert(e);
			} else if (iletisimTercih == false) {
				if (e.getKisiRef()!=null ){
					if (e.getKisiRef().getIletisimtercih().getCode() == EvetHayir._EVET.getCode()) {
						getDBOperator().insert(e);
					} 
				}
				
			}
		} catch (DBException e1) {
			e1.printStackTrace();
		}
	}

	public void timeSetterForEMail(Gonderim g, Epostagonderimlog e) {
		if (g.getZamanlamali() == EvetHayir._HAYIR) {
			Date d = new Date();
			e.setTarih(d);
		} else {
			e.setTarih(g.getGonderimzamani());
		}
	}

	public void timeSetterForSMS(Gonderim g, Smsgonderimlog sms) {
		if (g.getZamanlamali() == EvetHayir._HAYIR) {
			Date d = new Date();
			sms.setTarih(d);
		} else {
			sms.setTarih(g.getGonderimzamani());
		}
	}

	public void setLogDurum(Gonderim g, BaseEntity b) {
		if (b instanceof Epostagonderimlog) {
			if (g.getDurum() == GonderimDurum._ILETILDI) {
				((Epostagonderimlog) b).setDurum(GonderimDurum._ILETILDI);
			} else {
				((Epostagonderimlog) b).setDurum(GonderimDurum._ILETILMEDI);
			}
		}
		if (b instanceof Smsgonderimlog) {
			if (g.getDurum() == GonderimDurum._ILETILDI) {
				((Smsgonderimlog) b).setDurum(GonderimDurum._ILETILDI);
			} else {
				((Smsgonderimlog) b).setDurum(GonderimDurum._ILETILMEDI);
			}
		}
	}

	public GonderimTuru getEmailTip() {
		return GonderimTuru._EPOSTA;
	}

	public void cancelPost() { 
		if (getGonderim().getGonderimturu() == GonderimTuru._EPOSTA) {
			@SuppressWarnings("unchecked")
			List<Epostagonderimlog> epostaGonderimLog = getDBOperator().load(
					Epostagonderimlog.class.getSimpleName(), "o.gonderimRef="
							+ getGonderim().getRID(), "o.rID");
			for (Epostagonderimlog epostaLog : epostaGonderimLog) {

				super.justDeleteGivenObject(epostaLog, loggable);
			}
			super.justDeleteGivenObject(this.gonderim, loggable);

		} else if (getGonderim().getGonderimturu() == GonderimTuru._SMS) {
			@SuppressWarnings("unchecked")
			List<Smsgonderimlog> smsGonderimLog = getDBOperator().load(
					Smsgonderimlog.class.getSimpleName(), "o.gonderimRef="
							+ getGonderim().getRID(), "o.rID");
			for (Smsgonderimlog smsLog : smsGonderimLog) {

				super.justDeleteGivenObject(smsLog, loggable);
			}
			super.justDeleteGivenObject(this.gonderim, loggable);
		}
		redirect();
	}

	public boolean getDurumKontrolZamanlamali() {
		if (this.gonderim.getZamanlamali() == EvetHayir._EVET) {
			return false;
		}
		return true;
	}

	public void redirect() {
		try {
			Thread.currentThread();
			Thread.sleep(100);
			FacesContext.getCurrentInstance().getExternalContext().redirect("gonderim.xhtml");
		} catch (IOException e) {
			logYaz("GonderimController @redirect=" + e.getMessage());
			logYaz("Exception @" + getModelName() + "Controller :", e);
		} catch (InterruptedException e) {
			logYaz("GonderimController @redirect=" + e.getMessage());
			logYaz("Exception @" + getModelName() + "Controller :", e);
		}
	}

	public boolean getGonderimTuruForRender() {
		if (this.gonderim.getGonderimturu() == GonderimTuru._EPOSTA) {
			return true;
		}
		return false;
	}
	
	public long getSmsParameter(){ 
		Long staticParamForRid = (long) 1;
		Long returnedParameter = (long) 0;
		@SuppressWarnings("unchecked")
		List<SistemParametre> sistemParameters = getDBOperator().load(SistemParametre.class.getSimpleName(), "o.rID="+ staticParamForRid, "o.rID");
		for (SistemParametre sistemParameter : sistemParameters) {
			returnedParameter=Long.parseLong(sistemParameter.getSmsuzunluk());

		}
		// logYaz(returnedParameter*160);
		return returnedParameter*160;
	}
}
// class
