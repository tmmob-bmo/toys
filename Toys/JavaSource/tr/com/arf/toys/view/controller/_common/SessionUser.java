package tr.com.arf.toys.view.controller._common;

import java.util.HashMap;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import tr.com.arf.framework.db._management.DBOperator;
import tr.com.arf.framework.db.model._base.BaseEntity;
import tr.com.arf.framework.utility.exception.data.DBException;
import tr.com.arf.framework.utility.tool.KeyUtil;
import tr.com.arf.framework.view.controller._common.SessionUserBase;
import tr.com.arf.toys.db.enumerated.system.BirimDurum;
import tr.com.arf.toys.db.enumerated.system.BirimYetkiTuru;
import tr.com.arf.toys.db.enumerated.system.PersonelDurum;
import tr.com.arf.toys.db.model.egitim.Egitim;
import tr.com.arf.toys.db.model.etkinlik.Etkinlik;
import tr.com.arf.toys.db.model.kisi.Donem;
import tr.com.arf.toys.db.model.kisi.Kisi;
import tr.com.arf.toys.db.model.kisi.KomisyonDonem;
import tr.com.arf.toys.db.model.kisi.KomisyonDonemUye;
import tr.com.arf.toys.db.model.kisi.KurulDonem;
import tr.com.arf.toys.db.model.kisi.Kurum;
import tr.com.arf.toys.db.model.system.Birim;
import tr.com.arf.toys.db.model.system.BirimHesap;
import tr.com.arf.toys.db.model.system.IslemKullanici;
import tr.com.arf.toys.db.model.system.Kullanici;
import tr.com.arf.toys.db.model.system.KullaniciRole;
import tr.com.arf.toys.db.model.system.Personel;
import tr.com.arf.toys.db.model.system.PersonelBirimGorev;
import tr.com.arf.toys.db.model.system.Sehir;
import tr.com.arf.toys.db.model.system.SistemParametre;
import tr.com.arf.toys.db.model.system.Uyarinot;
import tr.com.arf.toys.db.model.uye.Uye;
import tr.com.arf.toys.db.model.uye.UyeBasvuru;
import tr.com.arf.toys.service.application.ApplicationDescriptor;
import tr.com.arf.toys.service.application.ManagedBeanLocator;
import tr.com.arf.toys.utility.application.ApplicationConstant;
import tr.com.arf.toys.utility.logUtils.LogService;

@ManagedBean(name = "sessionUser")
@SessionScoped
public class SessionUser extends SessionUserBase {
    protected static Logger logger = Logger.getLogger(SessionUser.class);

	private static final long serialVersionUID = 2942589293290657749L;
	private KullaniciRole selectedRole;
	private HashMap<String, IslemKullanici> islemMap;
	private Kullanici kullanici;
	private SistemParametre sistemParametre;
	private boolean kullaniciUyeMi = false;

	private DBOperator dBOperator;
	private String ipAddress;

	@PostConstruct
	public void init() {
		logger.debug("SessionUser init...");
		setSuperUser(false);
		dBOperator = new DBOperator();
	}

	/* Data Operator */
	public final DBOperator getDBOperator() {
		if (dBOperator == null) {
			dBOperator = new DBOperator();
		}
		return dBOperator;
	}

	public SessionUser() {
		super();
		createSehirListesi();
		createBirimListesi();
		@SuppressWarnings("unchecked")
		List<SistemParametre> sisParamListesi = getDBOperator().load(SistemParametre.class.getSimpleName());
		if (sisParamListesi != null && sisParamListesi.size() > 0) {
			setSistemParametre(sisParamListesi.get(0));
		}
	}

	public Personel getPersonelRef() {
		try {
			if (getDBOperator().recordCount(Personel.class.getSimpleName(), "o.kisiRef.rID=" + getKullanici().getKisiRef().getRID() + " AND o.durum=" + PersonelDurum._CALISIYOR.getCode()) > 0) {
				return (Personel) getDBOperator().load(Personel.class.getSimpleName(), "o.kisiRef.rID=" + getKullanici().getKisiRef().getRID() + " AND o.durum=" + PersonelDurum._CALISIYOR.getCode(), "o.rID").get(0);
			}
		} catch (Exception e) {
			return null;
		}
		return null;
	}

	public Uye getUyeRef() {
		try {
			if (getDBOperator().recordCount(Uye.class.getSimpleName(), "o.kisiRef.rID=" + getKullanici().getKisiRef().getRID()) > 0) {
				return (Uye) getDBOperator().load(Uye.class.getSimpleName(), "o.kisiRef.rID=" + getKullanici().getKisiRef().getRID(), "").get(0);
			}
		} catch (Exception e) {
			return null;
		}
		return null;
	}

	// public String gotoSifreDegistir(){
	// SessionUser sessionUser = ManagedBeanLocator.locateSessionUser();
	// sessionUser.clearSessionFilters();
	// ManagedBeanLocator.locateSessionController().setDetailEditType(null);
	// ManagedBeanLocator.locateMenuController().createHomePageLink();
	// return ApplicationDescriptor._DASHBOARD_OUTCOME;
	// }

	public BirimYetkiTuru getBirimYetkiTuru() {
		if (getPersonelRef() == null) {
			return BirimYetkiTuru._SADECEKENDIBIRIMLERI;
		} else {
			if (isSuperUser()) {
				return BirimYetkiTuru._TUMBIRIMLER;
			}
			return getPersonelRef().getBirimRef().getBirimYetkiTuru();
		}
	}

	public String createStaticWhereCondition(String prefixForWhereCon) {
		try {
			if (getBirimYetkiTuru() != BirimYetkiTuru._TUMBIRIMLER) {
				if (getBirimYetkiTuru() == BirimYetkiTuru._KENDIBIRIMIVEALTBIRIMLERI) {
					return prefixForWhereCon + "birimRef.erisimKodu LIKE '" + getPersonelRef().getBirimRef().getErisimKodu() + "%'";
				} else if (getBirimYetkiTuru() == BirimYetkiTuru._SADECEKENDIBIRIMLERI) {
					return prefixForWhereCon + "birimRef.erisimKodu = '" + getPersonelRef().getBirimRef().getErisimKodu() + "'";
				}
			} else {
				return " 1=1 ";
			}
		} catch (Exception e) {
			return " 1=1 ";
		}
		return prefixForWhereCon;
	}

	public KullaniciRole getSelectedRole() {
		return selectedRole;
	}

	public void setSelectedRole(KullaniciRole selectedRole) {
		this.selectedRole = selectedRole;
	}

	public Kullanici getKullanici() {
		if (kullanici != null) {
			return kullanici;
		} else {
			return new Kullanici();
		}
	}

	public void setKullanici(Kullanici kullanici) {
		this.kullanici = kullanici;
	}

	public HashMap<String, IslemKullanici> getIslemMap() {
		return islemMap;
	}

	public void setIslemMap(HashMap<String, IslemKullanici> islemMap) {
		this.islemMap = islemMap;
	}

	public SelectItem[] getThemes() {
		Kullanici kullanici = ManagedBeanLocator.locateSessionUser().getKullanici();
		if (kullanici.getTheme() == null) {
			kullanici.setTheme("blitzer");
			try {
				getDBOperator().update(kullanici);
			} catch (DBException e) {
				LogService.logYaz("Exception @SessionUser :" + e.getMessage());
			}
		}
		SelectItem[] selectItemList = new SelectItem[] { new SelectItem(null, ""), new SelectItem("bootstrap", "Bootstrap (Twitter)"), new SelectItem("afternoon", "Afternoon"), new SelectItem("aristo", "Aristo"),
				new SelectItem("bluesky", "Blue sky"), new SelectItem("cupertino", "Cupertino"), new SelectItem("excite-bike", "Excite-bike"), new SelectItem("humanity", "Humanity"), new SelectItem("hot-sneaks", "Hot-sneaks"),
				new SelectItem("start", "Start"), new SelectItem("cruze", "Cruze"), new SelectItem("mint-choc", "Mint-choc"), new SelectItem("pepper-grinder", "Pepper-grinder"), new SelectItem("south-street", "South street"),
				new SelectItem("sunny", "Sunny"), new SelectItem("afterdark", "After dark"), new SelectItem("blitzer", "Blitzer"), new SelectItem("glass-x", "Glass-x"), new SelectItem("sam", "Sam") };
		return selectItemList;
	}

	public String anasayfa() {
		Kullanici kullanici = ManagedBeanLocator.locateSessionUser().getKullanici();
		kullanici.setTheme(getTheme());
		try {
			getDBOperator().update(kullanici);
			ManagedBeanLocator.locateSessionUser().setMessage(KeyUtil.getMessageValue("kayit.guncellendi"));

		} catch (Exception e) {
			createGenericMessage(KeyUtil.getMessageValue("islem.hata"), FacesMessage.SEVERITY_INFO);
		}
		return ManagedBeanLocator.locateMenuController().homePage();
	}

	public String uyeanasayfa() {
		Kullanici kullanici = ManagedBeanLocator.locateSessionUser().getKullanici();
		kullanici.setTheme(getTheme());
		try {
			getDBOperator().update(kullanici);
			ManagedBeanLocator.locateSessionUser().setMessage(KeyUtil.getMessageValue("kayit.guncellendi"));
		} catch (Exception e) {
			createGenericMessage(KeyUtil.getMessageValue("islem.hata"), FacesMessage.SEVERITY_INFO);
		}
		return ApplicationDescriptor._UYEDASHBOARD_OUTCOME;
	}

	public String getStaticWhereCondition(String modelName) {
		// System.out.println("*************** : getStaticWhereCondition IN SESSIONUSER :"
		// + modelName);
		if (modelName.equalsIgnoreCase(Birim.class.getSimpleName())) {
			return ApplicationConstant._birimStaticWhereCondition + " AND " + createStaticWhereCondition("o.").replace("birimRef.", "");
		} else if (modelName.equalsIgnoreCase(BirimHesap.class.getSimpleName())) {
			return ApplicationConstant._birimHesapStaticWhereCondition + " AND " + createStaticWhereCondition("o.");
		} else if (modelName.equalsIgnoreCase(Kisi.class.getSimpleName())) {
			return ApplicationConstant._kisiStaticWhereCondition;
		} else if (modelName.equalsIgnoreCase(Personel.class.getSimpleName())) {
			return ApplicationConstant._personelStaticWhereCondition + " AND " + createStaticWhereCondition("o.");
		} else if (modelName.equalsIgnoreCase(Uye.class.getSimpleName())) {
			return ApplicationConstant._uyeStaticWhereCondition + " AND " + createStaticWhereCondition("o.");
		} else if (modelName.equalsIgnoreCase(Egitim.class.getSimpleName())) {
			return createStaticWhereCondition("o.");
		} else if (modelName.equalsIgnoreCase(KurulDonem.class.getSimpleName())) {
			return createStaticWhereCondition("o.");
		} else if (modelName.equalsIgnoreCase(KomisyonDonem.class.getSimpleName())) {
			return createStaticWhereCondition("o.");
		} else if (modelName.equalsIgnoreCase(KomisyonDonemUye.class.getSimpleName())) {
			return createStaticWhereCondition("o.komisyonDonemRef.");
		} else if (modelName.equalsIgnoreCase(Etkinlik.class.getSimpleName())) {
			return createStaticWhereCondition("o.");
		} else if (modelName.equalsIgnoreCase(Donem.class.getSimpleName())) {
			return createStaticWhereCondition("o.");
		} else if (modelName.equalsIgnoreCase(PersonelBirimGorev.class.getSimpleName())) {
			return createStaticWhereCondition("o.");
		} else if (modelName.equalsIgnoreCase(UyeBasvuru.class.getSimpleName())) {
			return createStaticWhereCondition("o.");
		} else if (modelName.equalsIgnoreCase(Uyarinot.class.getSimpleName())) {
			return createStaticWhereCondition("o.");
		}
		return "";
	}

	public String[] getAutoCompleteSearchColumns(String modelName) {
		if (modelName.equalsIgnoreCase(Birim.class.getSimpleName())) {
			return ApplicationConstant._birimAutoCompleteSearchColumns;
		} else if (modelName.equalsIgnoreCase(Kurum.class.getSimpleName())) {
			return ApplicationConstant._kurumAutoCompleteSearchColumns;
		} else if (modelName.equalsIgnoreCase(Uye.class.getSimpleName())) {
			return ApplicationConstant._uyeAutoCompleteSearchColumns;
		} else if (modelName.equalsIgnoreCase(Kisi.class.getSimpleName())) {
			return ApplicationConstant._kisiAutoCompleteSearchColumns;
		} else if (modelName.equalsIgnoreCase(Personel.class.getSimpleName())) {
			return ApplicationConstant._personelAutoCompleteSearchColumns;
		}
		return new String[0];
	}

	@Override
	public String getSystemPath() {
        String os = System.getProperty("os.name");
		String filePath = ApplicationConstant._linuxPath;
		if (os.matches("(?i).*windows.*")) {
			filePath = ApplicationConstant._windowsPath;
		}
		return filePath;
	}

	@Override
	public String getGlobalMessagePanelId() {
		return ApplicationConstant._globalMessagePanelId;
	}

	public String getFontPath() {
		String os = System.getProperty("os.name");
		String filePath = ApplicationConstant._linuxFontPath;
		if (os.matches("(?i).*windows.*")) {
			filePath = ApplicationConstant._windowsFontPath;
		}
		return filePath;
	}

	// Sehir listesi session'a set edilir ve hep session'dan cagrilir!!!
	private List<Sehir> sehirListesi;

	public List<Sehir> getSehirListesi() {
		return sehirListesi;
	}

	public void setSehirListesi(List<Sehir> sehirListesi) {
		this.sehirListesi = sehirListesi;
	}

	@SuppressWarnings("unchecked")
	public void createSehirListesi() {
		setSehirListesi(getDBOperator().load(Sehir.class.getSimpleName(), "o.ad ASC"));
	}

	public SelectItem[] sehirItemList;

	public void setSehirItemList(SelectItem[] sehirItemList) {
		this.sehirItemList = sehirItemList;
	}

	@SuppressWarnings("unchecked")
	public SelectItem[] getSehirItemList() {
		List<Sehir> entityList = null;
		if (sehirItemList != null && sehirItemList.length > 0) {
			return sehirItemList;
		}
		if (getSehirListesi() != null) {
			entityList = getSehirListesi();
		} else {
			entityList = getDBOperator().load(Sehir.class.getSimpleName(), "o.ad ASC");
		}
		if (entityList != null) {
			int i = 0;
			SelectItem[] sehirItemList = null;
			sehirItemList = new SelectItem[entityList.size() + 1];
			sehirItemList[0] = new SelectItem(null, "");
			i = 1;
			for (BaseEntity entity : entityList) {
				sehirItemList[i++] = new SelectItem(entity, entity.getUIString());
			}
			setSehirItemList(sehirItemList);
			return sehirItemList;
		}
		return new SelectItem[0];
	}

	public SistemParametre getSistemParametre() {
		return sistemParametre;
	}

	public void setSistemParametre(SistemParametre sistemParametre) {
		this.sistemParametre = sistemParametre;
	}

	public String getLogoPath() {
		if (getSistemParametre() != null) {
			String logoPath = getSistemParametre().getLogodegeri();
			try {
				logoPath = logoPath.substring(logoPath.lastIndexOf("/") + 1);
				return logoPath;
			} catch (Exception e) {
				return "toyslogo2.png";
			}
		}
		return "toyslogo2.png";
	}

	public String getOdaadi() {
		try {
			if (getSistemParametre() != null) {
				return getSistemParametre().getOdaadi();
			}
		} catch (Exception e) {
			LogService.logYaz("Exception @SessionUser :" + e.getMessage());
		}
		return null;
	}

	// Birim listesi session'a set edilir ve hep session'dan cagrilir!!!
	private List<Birim> birimListesi;

	public List<Birim> getBirimListesi() {
		return birimListesi;
	}

	public void setBirimListesi(List<Birim> birimListesi) {
		this.birimListesi = birimListesi;
	}

	@SuppressWarnings("unchecked")
	public void createBirimListesi() {
		setBirimListesi(getDBOperator().load(Birim.class.getSimpleName(), "o.ad ASC"));
	}

	public SelectItem[] birimItemList;

	public void setBirimItemList(SelectItem[] birimItemList) {
		this.birimItemList = birimItemList;
	}

	@SuppressWarnings("unchecked")
	public SelectItem[] getBirimItemList() {
		List<Birim> entityList = null;
		if (birimItemList != null && birimItemList.length > 0) {
			return birimItemList;
		}
		if (getBirimListesi() != null) {
			entityList = getBirimListesi();
		} else {
			entityList = getDBOperator().load(Birim.class.getSimpleName(), "o.durum=" + BirimDurum._ACIK.getCode(), "o.ad ASC");
		}
		if (entityList != null) {
			int i = 0;
			SelectItem[] birimItemList = null;
			birimItemList = new SelectItem[entityList.size() + 1];
			birimItemList[0] = new SelectItem(null, "");
			i = 1;
			for (BaseEntity entity : entityList) {
				birimItemList[i++] = new SelectItem(entity, entity.getUIString());
			}
			setBirimItemList(birimItemList);
			return birimItemList;
		}
		return new SelectItem[0];
	}

	public boolean isKullaniciUyeMi() {
		return kullaniciUyeMi;
	}

	public void setKullaniciUyeMi(boolean kullaniciUyeMi) {
		this.kullaniciUyeMi = kullaniciUyeMi;
	}

	@Override
	public String getIpAddress() {
		if(ipAddress == null){
			setIpAddress(LogService.getClientIpAddr((HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest()));
		}
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

}
