package tr.com.arf.toys.view.controller.form.kisi; 
  
 
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.toys.db.filter.kisi.KisiizinFilter;
import tr.com.arf.toys.db.model.kisi.Kisi;
import tr.com.arf.toys.db.model.kisi.Kisiizin;
import tr.com.arf.toys.service.application.ApplicationDescriptor;
import tr.com.arf.toys.view.controller.form._base.ToysBaseFilteredController;
  
  
@ManagedBean 
@ViewScoped 
public class KisiizinController extends ToysBaseFilteredController {  
  
  private static final long serialVersionUID = 6531152165884980748L; 
  
	private Kisiizin kisiizin;  
	private KisiizinFilter kisiizinFilter = new KisiizinFilter();  
  
	public KisiizinController() { 
		super(Kisiizin.class);  
		setDefaultValues(false);  
		setOrderField("aciklama");  
		setAutoCompleteSearchColumns(new String[]{"aciklama"}); 
		setTable(null); 
		if(getMasterEntity() != null){ 
			setMasterObjectName(ApplicationDescriptor._KISI_MODEL);
			setMasterOutcome(ApplicationDescriptor._KISI_MODEL);
			getKisiizinFilter().setKisiRef(getMasterEntity());
			getKisiizinFilter().createQueryCriterias();   
		} 
		queryAction(); 
	} 
 
	public Kisi getMasterEntity(){
		if(getKisiizinFilter().getKisiRef() != null){
			return getKisiizinFilter().getKisiRef();
		} else {
			return (Kisi) getObjectFromSessionFilter(ApplicationDescriptor._KISI_MODEL);
		}			
	} 
 
	
	
	public Kisiizin getKisiizin() { 
		kisiizin = (Kisiizin) getEntity(); 
		return kisiizin; 
	} 
	
	public void setKisiizin(Kisiizin kisiizin) { 
		this.kisiizin = kisiizin; 
	} 
	
	public KisiizinFilter getKisiizinFilter() { 
		return kisiizinFilter; 
	} 
	 
	public void setKisiizinFilter(KisiizinFilter kisiizinFilter) {  
		this.kisiizinFilter = kisiizinFilter;  
	}  
	 
	@Override  
	public BaseFilter getFilter() {  
		return getKisiizinFilter();  
	}  
	
	
 
	@Override 
	public void resetFilter() {
		super.resetFilterOnly();
		if(getMasterEntity() != null){
			getKisiizinFilter().setKisiRef(getMasterEntity()); 
		}
		queryAction();
	}
 
	@Override	
	public void insert() {	
		super.insert();	
		getKisiizin().setKisiRef(getKisiizinFilter().getKisiRef());	
	}	
	
} // class 
