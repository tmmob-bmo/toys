package tr.com.arf.toys.view.controller.form.kisi; 
  
 
import java.util.Calendar;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.toys.db.filter.kisi.KurulDonemToplantiFilter;
import tr.com.arf.toys.db.model.kisi.KurulDonem;
import tr.com.arf.toys.db.model.kisi.KurulDonemToplanti;
import tr.com.arf.toys.service.application.ApplicationDescriptor;
import tr.com.arf.toys.service.application.ManagedBeanLocator;
import tr.com.arf.toys.view.controller.form._base.ToysBaseFilteredController;
  
  
@ManagedBean 
@ViewScoped 
public class KurulDonemToplantiController extends ToysBaseFilteredController {  
  
  private static final long serialVersionUID = 6531152165884980748L; 
  
	private KurulDonemToplanti kurulDonemToplanti;  
	private KurulDonemToplantiFilter kurulDonemToplantiFilter = new KurulDonemToplantiFilter();  
  
	public KurulDonemToplantiController() { 
		super(KurulDonemToplanti.class);  
		setDefaultValues(false);  
		setOrderField("kurulDonemRef.rID");  
		setLoggable(false);  
		setAutoCompleteSearchColumns(new String[]{"kurulDonemRef"}); 
		setTable(null); 
		if(getMasterEntity() != null){ 
			setMasterObjectName(ApplicationDescriptor._KURULDONEM_MODEL);
			setMasterOutcome(ApplicationDescriptor._KURULDONEM_MODEL);
			getKurulDonemToplantiFilter().setKurulDonemRef(getMasterEntity());
			getKurulDonemToplantiFilter().createQueryCriterias();   
		} 
		queryAction(); 
	} 
 
	public KurulDonem getMasterEntity(){
		if(getKurulDonemToplantiFilter().getKurulDonemRef() != null){
			return getKurulDonemToplantiFilter().getKurulDonemRef();
		} else {
			return (KurulDonem) getObjectFromSessionFilter(ApplicationDescriptor._KURULDONEM_MODEL);
		}			
	} 
 
	
	
	public KurulDonemToplanti getKurulDonemToplanti() { 
		kurulDonemToplanti = (KurulDonemToplanti) getEntity(); 
		return kurulDonemToplanti; 
	} 
	
	public void setKurulDonemToplanti(KurulDonemToplanti kurulDonemToplanti) { 
		this.kurulDonemToplanti = kurulDonemToplanti; 
	} 
	
	public KurulDonemToplantiFilter getKurulDonemToplantiFilter() { 
		return kurulDonemToplantiFilter; 
	} 
	 
	public void setKurulDonemToplantiFilter(KurulDonemToplantiFilter kurulDonemToplantiFilter) {  
		this.kurulDonemToplantiFilter = kurulDonemToplantiFilter;  
	}  
	 
	@Override  
	public BaseFilter getFilter() {  
		return getKurulDonemToplantiFilter();  
	}  
	
 
	@Override	
	public void insert() {	
		super.insert();	
		getKurulDonemToplanti().setKurulDonemRef(getKurulDonemToplantiFilter().getKurulDonemRef());	
		Calendar c = Calendar.getInstance();
		c.set(Calendar.HOUR_OF_DAY, 19);
		c.set(Calendar.MINUTE, 00);
		getKurulDonemToplanti().setTarih(c.getTime());
	}	
	
	public String kurulDonemToplantiTutanak() {	
		if (!setSelected()) {
			return "";
		}	
		getSessionUser().setLegalAccess(1);	
		putObjectToSessionFilter(ApplicationDescriptor._KURULDONEMTOPLANTI_MODEL, getKurulDonemToplanti());
		return ManagedBeanLocator.locateMenuController().gotoPage("menu_KurulDonemToplantiTutanak"); 
	} 
	
	public String kurulDonemToplantiKatilimci() {	
		if (!setSelected()) {
			return "";
		}	
		getSessionUser().setLegalAccess(1);	
		putObjectToSessionFilter(ApplicationDescriptor._KURULDONEMTOPLANTI_MODEL, getKurulDonemToplanti());
		return ManagedBeanLocator.locateMenuController().gotoPage("menu_KurulDonemToplantiKatilimci"); 
	} 
	
} // class 
