package tr.com.arf.toys.view.controller.form.system; 
  
 
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.framework.db.model._base.BaseTreeEntity;
import tr.com.arf.framework.utility.exception.data.DBException;
import tr.com.arf.framework.utility.tool.KeyUtil;
import tr.com.arf.toys.db.filter.system.MenuitemFilter;
import tr.com.arf.toys.db.model.system.Menuitem;
import tr.com.arf.toys.service.application.ApplicationDescriptor;
import tr.com.arf.toys.view.controller.form._base.ToysBaseTreeController;
  
  
@ManagedBean 
@ViewScoped 
public class MenuitemController extends ToysBaseTreeController {  
  
  private static final long serialVersionUID = 6531152165884980748L; 
  
	private Menuitem menuitem;  
	private MenuitemFilter menuitemFilter = new MenuitemFilter();   
	
	public MenuitemController() { 
		super(Menuitem.class);  
		setDefaultValues(false);  
		setOrderField("ad");  
		setAutoCompleteSearchColumns(new String[]{"ad"}); 
		setTable(null); 
		queryAction(); 
		super.expandThis(1L);
	} 
	
	
	public Menuitem getMenuitem() { 
		menuitem = (Menuitem) getEntity(); 
		return menuitem; 
	} 
	
	public void setMenuitem(Menuitem menuitem) { 
		this.menuitem = menuitem; 
	} 
	
	public MenuitemFilter getMenuitemFilter() { 
		return menuitemFilter; 
	} 
	 
	public void setMenuitemFilter(MenuitemFilter menuitemFilter) {  
		this.menuitemFilter = menuitemFilter;  
	}  
	 
	@Override  
	public BaseFilter getFilter() {  
		return getMenuitemFilter();  
	}  
	 
	@Override 
	public synchronized void save() {   
		if(getMenuitem().getUstRef() == null && getMenuitem().getRID() == null){
			createGenericMessage(KeyUtil.getFrameworkLabel("ustkayit.zorunlu"), FacesMessage.SEVERITY_ERROR);
			return;
		} else if(getMenuitem().getUstRef() == null){
			getMenuitem().setUstRef(getPreviousUstRef());
		}
		
		setTable(null); 
		if (getMenuitem().getRID() == null) { 
			try {  
				getMenuitem().setErisimKodu("-"); 
				getDBOperator().insert(getMenuitem()); 
				if(getMenuitem().getUstRef() != null){ 
					getMenuitem().setErisimKodu(getMenuitem().getUstRef().getErisimKodu() + "." + getMenuitem().getRID()); 
				} else {
					getMenuitem().setErisimKodu(getMenuitem().getRID()+ "");
				} 
				getDBOperator().update(getMenuitem());
				createGenericMessage(KeyUtil.getMessageValue("kayit.eklendi"), FacesMessage.SEVERITY_INFO);
			} catch (DBException e) { 
				getMenuitem().setRID(null); 
				createGenericMessage(KeyUtil.getMessageValue("islem.hata"), FacesMessage.SEVERITY_ERROR);
				return;
			} 
		} 
		else { 
			try {  
				String oncekiErisimKodu = getMenuitem().getErisimKodu();
				if(getMenuitem().getUstRef() != null) {
					getMenuitem().setErisimKodu(getMenuitem().getUstRef().getErisimKodu() + "." + getMenuitem().getRID());
				} else {
					getMenuitem().setErisimKodu(getMenuitem().getRID() + "");
				}	
				if(getMenuitem().getValue().hashCode() == getOldValue().hashCode()){
					createGenericMessage(KeyUtil.getMessageValue("islem.degisiklikYok"), FacesMessage.SEVERITY_INFO); 
					return;
				}
				getDBOperator().update(getMenuitem());
				@SuppressWarnings("unchecked")
				List<Menuitem> subList = getDBOperator().load(getModelName(), "o.erisimKodu LIKE '" + oncekiErisimKodu + "%'", "o.erisimKodu");
				if(!isEmpty(subList)){
					for(Menuitem sub : subList){ 
						sub.setErisimKodu(sub.getUstRef().getErisimKodu() + "." + sub.getRID());
						getDBOperator().update(sub);
					}
				}
				createGenericMessage(KeyUtil.getMessageValue("kayit.guncellendi"), FacesMessage.SEVERITY_INFO);
			} catch (DBException e) { 
				createGenericMessage(KeyUtil.getMessageValue("islem.hata"), FacesMessage.SEVERITY_ERROR);
				return; 
			} 
	
		} 
		insert(); 
		queryAction(); 
		cancel();
		return; 
	}
	
	public String tree() {	
		getSessionUser().setLegalAccess(1); 
		return ApplicationDescriptor._MENUITEM_TREE_OUTCOME;	
	}	
	 
	@Override
	public void addToTree(String selectedRID) {  
		if(!isEmpty(selectedRID)){
			setSelectedRID(selectedRID);
			setTable(null);  
			BaseTreeEntity ustKayit = (BaseTreeEntity) getEntity();
			Menuitem cloneableRec = (Menuitem) ustKayit.cloneObject();
			cloneableRec.setRID(null);
			super.insert(); 
			cloneableRec.setUstRef(ustKayit);
			cloneableRec.setUstRef((Menuitem) ustKayit);
			cloneableRec.setAd(""); 
			cloneableRec.setUrl(".do");
			cloneableRec.setKod("menu_");
			setEntity(cloneableRec);   
			if(!getExpandedItemsOid().contains(ustKayit.getRID())){ 
				getExpandedItemsOid().add(ustKayit.getRID());
			}
			return;
		}   
	} 
		
} // class 
