package tr.com.arf.toys.view.controller.form.uye; 
  
 
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.toys.db.filter.uye.UyeaboneFilter;
import tr.com.arf.toys.db.model.uye.Uye;
import tr.com.arf.toys.db.model.uye.Uyeabone;
import tr.com.arf.toys.service.application.ApplicationDescriptor;
import tr.com.arf.toys.view.controller.form._base.ToysBaseFilteredController;
  
  
@ManagedBean 
@ViewScoped 
public class UyeaboneController extends ToysBaseFilteredController {  
  
  private static final long serialVersionUID = 6531152165884980748L; 
  
	private Uyeabone uyeabone;  
	private UyeaboneFilter uyeaboneFilter = new UyeaboneFilter();  
  
	public UyeaboneController() { 
		super(Uyeabone.class);  
		setDefaultValues(false);  
		setOrderField("aciklama");  
		setAutoCompleteSearchColumns(new String[]{"aciklama"}); 
		setTable(null); 
		if(getMasterEntity() != null){ 
			setMasterObjectName(ApplicationDescriptor._UYE_MODEL);
			setMasterOutcome(ApplicationDescriptor._UYE_MODEL);
			getUyeaboneFilter().setUyeRef(getMasterEntity());
			getUyeaboneFilter().createQueryCriterias();   
		} 
		queryAction(); 
	} 
 
	public UyeaboneController(Object object) { 
		super(Uyeabone.class);    
	}
	
	public Uye getMasterEntity(){
		if(getUyeaboneFilter().getUyeRef() != null){
			return getUyeaboneFilter().getUyeRef();
		} else {
			return (Uye) getObjectFromSessionFilter(ApplicationDescriptor._UYE_MODEL);
		}			
	} 
 	
	public Uyeabone getUyeabone() { 
		uyeabone = (Uyeabone) getEntity(); 
		return uyeabone; 
	} 
	
	public void setUyeabone(Uyeabone uyeabone) { 
		this.uyeabone = uyeabone; 
	} 
	
	public UyeaboneFilter getUyeaboneFilter() { 
		return uyeaboneFilter; 
	} 
	 
	public void setUyeaboneFilter(UyeaboneFilter uyeaboneFilter) {  
		this.uyeaboneFilter = uyeaboneFilter;  
	}  
	 
	@Override  
	public BaseFilter getFilter() {  
		return getUyeaboneFilter();  
	}  
	
	
 
	@Override 
	public void resetFilter() {
		super.resetFilterOnly();
		if(getMasterEntity() != null){
			getUyeaboneFilter().setUyeRef(getMasterEntity()); 
		}
		queryAction();
	}
 
	@Override	
	public void insert() {	
		super.insert();	
		getUyeabone().setUyeRef(getUyeaboneFilter().getUyeRef());	
	}	
	
} // class 
