package tr.com.arf.toys.view.controller.form.egitim;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.framework.db.model._base.BaseEntity;
import tr.com.arf.framework.utility.exception.data.DBException;
import tr.com.arf.framework.utility.tool.KeyUtil;
import tr.com.arf.toys.db.enumerated.egitim.Soru;
import tr.com.arf.toys.db.filter.egitim.EgitimdegerlendirmeFilter;
import tr.com.arf.toys.db.model.egitim.Egitimdegerlendirme;
import tr.com.arf.toys.db.model.egitim.Egitimkatilimci;
import tr.com.arf.toys.db.model.kisi.Kisi;
import tr.com.arf.toys.service.application.ApplicationDescriptor;
import tr.com.arf.toys.view.controller.form._base.ToysBaseFilteredController;
 

@ManagedBean
@ViewScoped
public class EgitimdegerlendirmeController extends ToysBaseFilteredController {

	private static final long serialVersionUID = 6531152165884980748L;

	private Egitimdegerlendirme egitimdegerlendirme;
	private Egitimdegerlendirme egitimdegerlendirme1;
	private Egitimdegerlendirme egitimdegerlendirme2;
	private Egitimdegerlendirme egitimdegerlendirme3;
	private Egitimdegerlendirme egitimdegerlendirme4;
	private Egitimdegerlendirme egitimdegerlendirme5;
	private Egitimdegerlendirme egitimdegerlendirme6;
	private Egitimdegerlendirme egitimdegerlendirme7;
	private Egitimdegerlendirme egitimdegerlendirme8;
	private Egitimdegerlendirme egitimdegerlendirme9;
	private Egitimdegerlendirme egitimdegerlendirme10;
	private Egitimdegerlendirme egitimdegerlendirme11;
	private Egitimdegerlendirme egitimdegerlendirme12;
	private Egitimdegerlendirme egitimdegerlendirme13;
	private Egitimdegerlendirme egitimdegerlendirme14;
	private Egitimdegerlendirme egitimdegerlendirme15;
	private Egitimdegerlendirme egitimdegerlendirme16;
	private Egitimdegerlendirme egitimdegerlendirme17;
	private Egitimdegerlendirme egitimdegerlendirme18;
	private Egitimdegerlendirme egitimdegerlendirme19;
	private Boolean evethayir;
	public Boolean getEvethayir() {
		return evethayir;
	}

	public void setEvethayir(Boolean evethayir) {
		this.evethayir = evethayir;
	}

	public EgitimController controller = new EgitimController();

	private EgitimdegerlendirmeFilter egitimdegerlendirmeFilter = new EgitimdegerlendirmeFilter();

	public EgitimdegerlendirmeController() {
		super(Egitimdegerlendirme.class);
		setDefaultValues(false);
		setOrderField("aciklama");
		setLoggable(false);
		setAutoCompleteSearchColumns(new String[] { "aciklama" });
		setTable(null);
		if (getMasterEntity() != null) {
			if (getMasterEntity().getClass()==Egitimkatilimci.class){
				setMasterObjectName(ApplicationDescriptor._EGITIMKATILIMCI_MODEL);
				setMasterOutcome(ApplicationDescriptor._EGITIMKATILIMCI_MODEL);
				getEgitimdegerlendirmeFilter().setEgitimkatilimciRef((Egitimkatilimci)getMasterEntity());
			}else if (getMasterEntity().getClass()==Kisi.class){
				setMasterObjectName(ApplicationDescriptor._KISI_MODEL);
				setMasterOutcome(ApplicationDescriptor._KISI_MODEL);
				getEgitimdegerlendirmeFilter().setKisiRef((Kisi)getMasterEntity());
			}
				
			
		}
		getEgitimdegerlendirmeFilter().createQueryCriterias();
		queryAction();
	}

	public BaseEntity getMasterEntity() {
		if (getEgitimdegerlendirmeFilter().getEgitimkatilimciRef() != null) {
			return getEgitimdegerlendirmeFilter().getEgitimkatilimciRef();
		} else if(getObjectFromSessionFilter(ApplicationDescriptor._KISI_MODEL) != null){
			return (Kisi) getObjectFromSessionFilter(ApplicationDescriptor._KISI_MODEL);
		} else if (getObjectFromSessionFilter(ApplicationDescriptor._EGITIMKATILIMCI_MODEL) != null){
			return (Egitimkatilimci) getObjectFromSessionFilter(ApplicationDescriptor._EGITIMKATILIMCI_MODEL);
		}else {
			return null;
		}
	}

	
	public Egitimdegerlendirme getEgitimdegerlendirme() {
		egitimdegerlendirme = (Egitimdegerlendirme) getEntity();
		return egitimdegerlendirme;
	}

	public void setEgitimdegerlendirme(Egitimdegerlendirme egitimdegerlendirme) {
		this.egitimdegerlendirme = egitimdegerlendirme;
	}

	public Egitimdegerlendirme getEgitimdegerlendirme1() {
		return egitimdegerlendirme1;
	}

	public void setEgitimdegerlendirme1(Egitimdegerlendirme egitimdegerlendirme1) {
		this.egitimdegerlendirme1 = egitimdegerlendirme1;
	}

	public Egitimdegerlendirme getEgitimdegerlendirme2() {
		return egitimdegerlendirme2;
	}

	public void setEgitimdegerlendirme2(Egitimdegerlendirme egitimdegerlendirme2) {
		this.egitimdegerlendirme2 = egitimdegerlendirme2;
	}

	public Egitimdegerlendirme getEgitimdegerlendirme3() {
		return egitimdegerlendirme3;
	}

	public void setEgitimdegerlendirme3(Egitimdegerlendirme egitimdegerlendirme3) {
		this.egitimdegerlendirme3 = egitimdegerlendirme3;
	}

	public Egitimdegerlendirme getEgitimdegerlendirme4() {
		return egitimdegerlendirme4;
	}

	public void setEgitimdegerlendirme4(Egitimdegerlendirme egitimdegerlendirme4) {
		this.egitimdegerlendirme4 = egitimdegerlendirme4;
	}

	public Egitimdegerlendirme getEgitimdegerlendirme5() {
		return egitimdegerlendirme5;
	}

	public void setEgitimdegerlendirme5(Egitimdegerlendirme egitimdegerlendirme5) {
		this.egitimdegerlendirme5 = egitimdegerlendirme5;
	}

	public Egitimdegerlendirme getEgitimdegerlendirme6() {
		return egitimdegerlendirme6;
	}

	public void setEgitimdegerlendirme6(Egitimdegerlendirme egitimdegerlendirme6) {
		this.egitimdegerlendirme6 = egitimdegerlendirme6;
	}

	public Egitimdegerlendirme getEgitimdegerlendirme7() {
		return egitimdegerlendirme7;
	}

	public void setEgitimdegerlendirme7(Egitimdegerlendirme egitimdegerlendirme7) {
		this.egitimdegerlendirme7 = egitimdegerlendirme7;
	}

	public Egitimdegerlendirme getEgitimdegerlendirme8() {
		return egitimdegerlendirme8;
	}

	public void setEgitimdegerlendirme8(Egitimdegerlendirme egitimdegerlendirme8) {
		this.egitimdegerlendirme8 = egitimdegerlendirme8;
	}

	public Egitimdegerlendirme getEgitimdegerlendirme9() {
		return egitimdegerlendirme9;
	}

	public void setEgitimdegerlendirme9(Egitimdegerlendirme egitimdegerlendirme9) {
		this.egitimdegerlendirme9 = egitimdegerlendirme9;
	}

	public Egitimdegerlendirme getEgitimdegerlendirme10() {
		return egitimdegerlendirme10;
	}

	public void setEgitimdegerlendirme10(
			Egitimdegerlendirme egitimdegerlendirme10) {
		this.egitimdegerlendirme10 = egitimdegerlendirme10;
	}

	public Egitimdegerlendirme getEgitimdegerlendirme11() {
		return egitimdegerlendirme11;
	}

	public void setEgitimdegerlendirme11(
			Egitimdegerlendirme egitimdegerlendirme11) {
		this.egitimdegerlendirme11 = egitimdegerlendirme11;
	}

	public Egitimdegerlendirme getEgitimdegerlendirme12() {
		return egitimdegerlendirme12;
	}

	public void setEgitimdegerlendirme12(
			Egitimdegerlendirme egitimdegerlendirme12) {
		this.egitimdegerlendirme12 = egitimdegerlendirme12;
	}

	public Egitimdegerlendirme getEgitimdegerlendirme13() {
		return egitimdegerlendirme13;
	}

	public void setEgitimdegerlendirme13(
			Egitimdegerlendirme egitimdegerlendirme13) {
		this.egitimdegerlendirme13 = egitimdegerlendirme13;
	}

	public Egitimdegerlendirme getEgitimdegerlendirme14() {
		return egitimdegerlendirme14;
	}

	public void setEgitimdegerlendirme14(
			Egitimdegerlendirme egitimdegerlendirme14) {
		this.egitimdegerlendirme14 = egitimdegerlendirme14;
	}

	public Egitimdegerlendirme getEgitimdegerlendirme15() {
		return egitimdegerlendirme15;
	}

	public void setEgitimdegerlendirme15(
			Egitimdegerlendirme egitimdegerlendirme15) {
		this.egitimdegerlendirme15 = egitimdegerlendirme15;
	}

	public Egitimdegerlendirme getEgitimdegerlendirme16() {
		return egitimdegerlendirme16;
	}

	public void setEgitimdegerlendirme16(
			Egitimdegerlendirme egitimdegerlendirme16) {
		this.egitimdegerlendirme16 = egitimdegerlendirme16;
	}

	public Egitimdegerlendirme getEgitimdegerlendirme17() {
		return egitimdegerlendirme17;
	}

	public void setEgitimdegerlendirme17(
			Egitimdegerlendirme egitimdegerlendirme17) {
		this.egitimdegerlendirme17 = egitimdegerlendirme17;
	}

	public Egitimdegerlendirme getEgitimdegerlendirme18() {
		return egitimdegerlendirme18;
	}

	public void setEgitimdegerlendirme18(
			Egitimdegerlendirme egitimdegerlendirme18) {
		this.egitimdegerlendirme18 = egitimdegerlendirme18;
	}

	public Egitimdegerlendirme getEgitimdegerlendirme19() {
		return egitimdegerlendirme19;
	}

	public void setEgitimdegerlendirme19(
			Egitimdegerlendirme egitimdegerlendirme19) {
		this.egitimdegerlendirme19 = egitimdegerlendirme19;
	}

	public EgitimdegerlendirmeFilter getEgitimdegerlendirmeFilter() {
		return egitimdegerlendirmeFilter;
	}

	public void setEgitimdegerlendirmeFilter(
			EgitimdegerlendirmeFilter egitimdegerlendirmeFilter) {
		this.egitimdegerlendirmeFilter = egitimdegerlendirmeFilter;
	}

	public String getSoruadi_TIP1() {
		return Soru._TIP1.toString();
	}

	public String getSoruadi_TIP2() {
		return Soru._TIP2.toString();
	}

	public String getSoruadi_TIP3() {
		return Soru._TIP3.toString();
	}

	public String getSoruadi_TIP4() {
		return Soru._TIP4.toString();
	}

	public String getSoruadi_TIP5() {
		return Soru._TIP5.toString();
	}

	public String getSoruadi_TIP6() {
		return Soru._TIP6.toString();
	}

	public String getSoruadi_TIP7() {
		return Soru._TIP7.toString();
	}

	public String getSoruadi_TIP8() {
		return Soru._TIP8.toString();
	}

	public String getSoruadi_TIP9() {
		return Soru._TIP9.toString();
	}

	public String getSoruadi_TIP10() {
		return Soru._TIP10.toString();
	}

	public String getSoruadi_TIP11() {
		return Soru._TIP11.toString();
	}

	public String getSoruadi_TIP12() {
		return Soru._TIP12.toString();
	}

	public String getSoruadi_TIP13() {
		return Soru._TIP13.toString();
	}

	public String getSoruadi_TIP14() {
		return Soru._TIP14.toString();
	}

	public String getSoruadi_TIP15() {
		return Soru._TIP15.toString();
	}

	public String getSoruadi_TIP16() {
		return Soru._TIP16.toString();
	}

	public String getSoruadi_TIP17() {
		return Soru._TIP17.toString();
	}

	public String getSoruadi_TIP18() {
		return Soru._TIP18.toString();
	}

	public String getSoruadi_TIP19() {
		return Soru._TIP19.toString();
	}

	public String getSoruadi_TIP20() {
		return Soru._TIP20.toString();
	}

	@Override
	public BaseFilter getFilter() {
		return getEgitimdegerlendirmeFilter();
	}

	@Override
	public void resetFilter() {
		super.resetFilterOnly();
		if (getMasterEntity() != null) {
			getEgitimdegerlendirmeFilter().setEgitimkatilimciRef((Egitimkatilimci)getMasterEntity());
		}
		queryAction();
	}

	@Override
	public void insert() {
		egitimdegerlendirmeFilter.createQueryCriterias();
		int cnt = 0; 
		String criter = new String("EGITIMKATILIMCIREF = "
				+ this.getMasterEntity().getRID().toString());

		try {
			cnt = getDBOperator().recordCount("Egitimdegerlendirme", criter);
		} catch (Exception e) {
			logYaz("Exception @" + getModelName() + "Controller :", e);
		}
		if (cnt > 0) {
			createGenericMessage(
					KeyUtil.getMessageValue("egitim.degerlendirmeVar"),
					FacesMessage.SEVERITY_ERROR);
		} else { 
			super.insert();
			this.egitimdegerlendirme1 = new Egitimdegerlendirme();
			this.egitimdegerlendirme2 = new Egitimdegerlendirme();
			this.egitimdegerlendirme3 = new Egitimdegerlendirme();
			this.egitimdegerlendirme4 = new Egitimdegerlendirme();
			this.egitimdegerlendirme5 = new Egitimdegerlendirme();
			this.egitimdegerlendirme6 = new Egitimdegerlendirme();
			this.egitimdegerlendirme7 = new Egitimdegerlendirme();
			this.egitimdegerlendirme8 = new Egitimdegerlendirme();
			this.egitimdegerlendirme9 = new Egitimdegerlendirme();
			this.egitimdegerlendirme10 = new Egitimdegerlendirme();
			this.egitimdegerlendirme11 = new Egitimdegerlendirme();
			this.egitimdegerlendirme12 = new Egitimdegerlendirme();
			this.egitimdegerlendirme13 = new Egitimdegerlendirme();
			this.egitimdegerlendirme14 = new Egitimdegerlendirme();
			this.egitimdegerlendirme15 = new Egitimdegerlendirme();
			this.egitimdegerlendirme16 = new Egitimdegerlendirme();
			this.egitimdegerlendirme17 = new Egitimdegerlendirme();
			this.egitimdegerlendirme18 = new Egitimdegerlendirme();
			this.egitimdegerlendirme19 = new Egitimdegerlendirme();

			getEgitimdegerlendirme().setEgitimkatilimciRef(
					getEgitimdegerlendirmeFilter().getEgitimkatilimciRef());
			getEgitimdegerlendirme1().setEgitimkatilimciRef(
					getEgitimdegerlendirmeFilter().getEgitimkatilimciRef());
			getEgitimdegerlendirme2().setEgitimkatilimciRef(
					getEgitimdegerlendirmeFilter().getEgitimkatilimciRef());
			getEgitimdegerlendirme3().setEgitimkatilimciRef(
					getEgitimdegerlendirmeFilter().getEgitimkatilimciRef());
			getEgitimdegerlendirme4().setEgitimkatilimciRef(
					getEgitimdegerlendirmeFilter().getEgitimkatilimciRef());
			getEgitimdegerlendirme5().setEgitimkatilimciRef(
					getEgitimdegerlendirmeFilter().getEgitimkatilimciRef());
			getEgitimdegerlendirme6().setEgitimkatilimciRef(
					getEgitimdegerlendirmeFilter().getEgitimkatilimciRef());
			getEgitimdegerlendirme7().setEgitimkatilimciRef(
					getEgitimdegerlendirmeFilter().getEgitimkatilimciRef());
			getEgitimdegerlendirme8().setEgitimkatilimciRef(
					getEgitimdegerlendirmeFilter().getEgitimkatilimciRef());
			getEgitimdegerlendirme9().setEgitimkatilimciRef(
					getEgitimdegerlendirmeFilter().getEgitimkatilimciRef());
			getEgitimdegerlendirme10().setEgitimkatilimciRef(
					getEgitimdegerlendirmeFilter().getEgitimkatilimciRef());
			getEgitimdegerlendirme11().setEgitimkatilimciRef(
					getEgitimdegerlendirmeFilter().getEgitimkatilimciRef());
			getEgitimdegerlendirme12().setEgitimkatilimciRef(
					getEgitimdegerlendirmeFilter().getEgitimkatilimciRef());
			getEgitimdegerlendirme13().setEgitimkatilimciRef(
					getEgitimdegerlendirmeFilter().getEgitimkatilimciRef());
			getEgitimdegerlendirme14().setEgitimkatilimciRef(
					getEgitimdegerlendirmeFilter().getEgitimkatilimciRef());
			getEgitimdegerlendirme15().setEgitimkatilimciRef(
					getEgitimdegerlendirmeFilter().getEgitimkatilimciRef());
			getEgitimdegerlendirme16().setEgitimkatilimciRef(
					getEgitimdegerlendirmeFilter().getEgitimkatilimciRef());
			getEgitimdegerlendirme17().setEgitimkatilimciRef(
					getEgitimdegerlendirmeFilter().getEgitimkatilimciRef());
			getEgitimdegerlendirme18().setEgitimkatilimciRef(
					getEgitimdegerlendirmeFilter().getEgitimkatilimciRef());
			getEgitimdegerlendirme19().setEgitimkatilimciRef(
					getEgitimdegerlendirmeFilter().getEgitimkatilimciRef());

			getEgitimdegerlendirme().setSoru(Soru._TIP1);
			getEgitimdegerlendirme1().setSoru(Soru._TIP2);
			getEgitimdegerlendirme2().setSoru(Soru._TIP3);
			getEgitimdegerlendirme3().setSoru(Soru._TIP4);
			getEgitimdegerlendirme4().setSoru(Soru._TIP5);
			getEgitimdegerlendirme5().setSoru(Soru._TIP6);
			getEgitimdegerlendirme6().setSoru(Soru._TIP7);
			getEgitimdegerlendirme7().setSoru(Soru._TIP8);
			getEgitimdegerlendirme8().setSoru(Soru._TIP9);
			getEgitimdegerlendirme9().setSoru(Soru._TIP10);
			getEgitimdegerlendirme10().setSoru(Soru._TIP11);
			getEgitimdegerlendirme11().setSoru(Soru._TIP12);
			getEgitimdegerlendirme12().setSoru(Soru._TIP13);
			getEgitimdegerlendirme13().setSoru(Soru._TIP14);
			getEgitimdegerlendirme14().setSoru(Soru._TIP15);
			getEgitimdegerlendirme15().setSoru(Soru._TIP16);
			getEgitimdegerlendirme16().setSoru(Soru._TIP17);
			getEgitimdegerlendirme17().setSoru(Soru._TIP18);
			getEgitimdegerlendirme18().setSoru(Soru._TIP19);
			getEgitimdegerlendirme19().setSoru(Soru._TIP20);
		}
	}

	@Override
	public void save() { 
		try {
			getDBOperator().insert(egitimdegerlendirme);
		} catch (DBException e) {
			logYaz("Exception @" + getModelName() + "Controller :", e);
		}
		
		try {
			getDBOperator().insert(egitimdegerlendirme1);
		} catch (DBException e) {
			logYaz("Exception @" + getModelName() + "Controller :", e);
		}
		
		try {
			getDBOperator().insert(egitimdegerlendirme2);
		} catch (DBException e) {
			logYaz("Exception @" + getModelName() + "Controller :", e);
		}
		
		try {
			getDBOperator().insert(egitimdegerlendirme3);
		} catch (DBException e) {
			logYaz("Exception @" + getModelName() + "Controller :", e);
		}
		
		try {
			getDBOperator().insert(egitimdegerlendirme4);
		} catch (DBException e) {
			logYaz("Exception @" + getModelName() + "Controller :", e);
		}
		
		try {
			getDBOperator().insert(egitimdegerlendirme5);
		} catch (DBException e) {
			logYaz("Exception @" + getModelName() + "Controller :", e);
		}
		
		try {
			getDBOperator().insert(egitimdegerlendirme6);
		} catch (DBException e) {
			logYaz("Exception @" + getModelName() + "Controller :", e);
		}
		
		try {
			getDBOperator().insert(egitimdegerlendirme7);
		} catch (DBException e) {
			logYaz("Exception @" + getModelName() + "Controller :", e);
		}
		
		try {
			getDBOperator().insert(egitimdegerlendirme8);
		} catch (DBException e) {
			logYaz("Exception @" + getModelName() + "Controller :", e);
		}
		
		try {
			getDBOperator().insert(egitimdegerlendirme9);
		} catch (DBException e) {
			logYaz("Exception @" + getModelName() + "Controller :", e);
		}
		
		try {
			getDBOperator().insert(egitimdegerlendirme10);
		} catch (DBException e) {
			logYaz("Exception @" + getModelName() + "Controller :", e);
		}
		
		try {
			getDBOperator().insert(egitimdegerlendirme11);
		} catch (DBException e) {
			logYaz("Exception @" + getModelName() + "Controller :", e);
		}
		
		try {
			getDBOperator().insert(egitimdegerlendirme12);
		} catch (DBException e) {
			logYaz("Exception @" + getModelName() + "Controller :", e);
		}
		
		try {
			getDBOperator().insert(egitimdegerlendirme13);
		} catch (DBException e) {
			logYaz("Exception @" + getModelName() + "Controller :", e);
		}
		
		try {
			getDBOperator().insert(egitimdegerlendirme14);
		} catch (DBException e) {
			logYaz("Exception @" + getModelName() + "Controller :", e);
		}
		
		try {
			getDBOperator().insert(egitimdegerlendirme15);
		} catch (DBException e) {
			logYaz("Exception @" + getModelName() + "Controller :", e);
		}
		
		try {
			getDBOperator().insert(egitimdegerlendirme16);
		} catch (DBException e) {
			logYaz("Exception @" + getModelName() + "Controller :", e);
		}
		
		try {
			getDBOperator().insert(egitimdegerlendirme17);
		} catch (DBException e) {
			logYaz("Exception @" + getModelName() + "Controller :", e);
		}
		
		try {
			getDBOperator().insert(egitimdegerlendirme18);
		} catch (DBException e) {
			logYaz("Exception @" + getModelName() + "Controller :", e);
		}
		super.justSave(getEgitimdegerlendirme19(), false, "");
		queryAction();
	}

	@Override
	public void update() {
		egitimdegerlendirmeFilter.createQueryCriterias();
		int cnt = 0; 
		String criter = new String("EGITIMKATILIMCIREF = "
				+ this.getMasterEntity().getRID().toString());

		try {
			cnt = getDBOperator().recordCount("Egitimdegerlendirme", criter);
		} catch (Exception e) {
			logYaz("Exception @" + getModelName() + "Controller :", e);
		}
		if (cnt > 0) {
			createGenericMessage(KeyUtil.getMessageValue("egitim.degerlendirmeDegismez"),FacesMessage.SEVERITY_ERROR);
		} else {

			super.update();
		}
	}

} // class 
