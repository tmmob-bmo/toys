package tr.com.arf.toys.view.controller.form.kisi; 
  
 
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.toys.db.filter.kisi.KurulFilter;
import tr.com.arf.toys.db.model.kisi.Kurul;
import tr.com.arf.toys.view.controller.form._base.ToysBaseFilteredController;
  
  
@ManagedBean 
@ViewScoped 
public class KurulController extends ToysBaseFilteredController {  
  
  private static final long serialVersionUID = 6531152165884980748L; 
  
	private Kurul kurul;  
	private KurulFilter kurulFilter = new KurulFilter();  
  
	public KurulController() { 
		super(Kurul.class);  
		setDefaultValues(false);  
		setOrderField("ad");   
		setAutoCompleteSearchColumns(new String[]{"ad"}); 
		setTable(null);  
		queryAction(); 
	} 
	 
	public Kurul getKurul() {
		kurul = (Kurul) getEntity(); 
		return kurul;
	}
	public void setKurul(Kurul kurul) {
		this.kurul = kurul;
	}
	public KurulFilter getKurulFilter() {
		return kurulFilter;
	}
	public void setKurulFilter(KurulFilter kurulFilter) {
		this.kurulFilter = kurulFilter;
	}
	
	@Override  
	public BaseFilter getFilter() {  
		return getKurulFilter();  
	}   
} // class 
