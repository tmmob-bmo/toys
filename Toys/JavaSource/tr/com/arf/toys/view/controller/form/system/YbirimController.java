package tr.com.arf.toys.view.controller.form.system; 
  
 
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.toys.db.filter.system.YbirimFilter;
import tr.com.arf.toys.db.model.system.Birim;
import tr.com.arf.toys.db.model.system.Ybirim;
import tr.com.arf.toys.service.application.ApplicationDescriptor;
import tr.com.arf.toys.service.application.ManagedBeanLocator;
import tr.com.arf.toys.utility.application.ApplicationConstant;
import tr.com.arf.toys.view.controller.form._base.ToysBaseTreeController;
  
  
@ManagedBean 
@ViewScoped 
public class YbirimController extends ToysBaseTreeController {  
  
  private static final long serialVersionUID = 6531152165884980748L; 
  
	private Ybirim birim;   
	private YbirimFilter birimFilter = new YbirimFilter();
	

	public YbirimController() { 
		super(Birim.class);  
		setDefaultValues(false);  
		setOrderField("erisimKodu");
		setStaticWhereCondition(getSessionUser().createStaticWhereCondition("o.").replace("birimRef.", "")); // Birim tablosunda arama yapilacagindan birimRef. siliniyor.
		setAutoCompleteSearchColumns(ApplicationConstant._birimAutoCompleteSearchColumns); 
		setSelectedTabName("birimListTab"); 
		setTable(null);  
		if(checkSessionForPreSelected() != null){ 
			getBirimFilter().setRid(checkSessionForPreSelected().getRID());
			setEntity(checkSessionForPreSelected());
		}
		//getBirimFilter().setDurum(BirimDurum._ACIK);
		queryAction(); 
	} 
	
	public YbirimController(Object object) { 
		super(Ybirim.class);   
		setLoggable(true); 
	}
		
	public Ybirim getBirim() { 
		birim = (Ybirim) getEntity(); 
		return birim; 
	} 
	
	public void setBirim(Ybirim birim) { 
		this.birim = birim; 
	} 
	
	public YbirimFilter getBirimFilter() { 
		return birimFilter; 
	} 
	 
	public void setBirimFilter(YbirimFilter birimFilter) {  
		this.birimFilter = birimFilter;  
	}  
	 
	


	@Override  
	public BaseFilter getFilter() {  
		return getBirimFilter();  
	}  
	
	public String internetadres() {	
		if (!setSelected()) {
			return "";
		}	
		getSessionUser().setLegalAccess(1);	
		removeObjectFromSessionFilter(ApplicationDescriptor._KISI_MODEL);
		putObjectToSessionFilter(ApplicationDescriptor._BIRIM_MODEL, getBirim());
		return ManagedBeanLocator.locateMenuController().gotoPage("menu_Internetadres_Birim"); 
	} 
	
	public String adres() {	
		if (!setSelected()) {
			return "";
		}	
		getSessionUser().setLegalAccess(1);
		removeObjectFromSessionFilter(ApplicationDescriptor._KISI_MODEL);
		putObjectToSessionFilter(ApplicationDescriptor._BIRIM_MODEL, getBirim());
		return ManagedBeanLocator.locateMenuController().gotoPage("menu_Adres_Birim"); 
	} 
	
	public String telefon() {	
		if (!setSelected()) {
			return "";
		}	
		getSessionUser().setLegalAccess(1);	
		removeObjectFromSessionFilter(ApplicationDescriptor._KISI_MODEL);
		putObjectToSessionFilter(ApplicationDescriptor._BIRIM_MODEL, getBirim());
		return ManagedBeanLocator.locateMenuController().gotoPage("menu_Telefon_Birim"); 
	}  
	
	@Override
	public void update(){
		super.update();  
		if(getBirim().getSehirRef() != null){
			super.changeIlce(getBirim().getSehirRef().getRID());
		} 
		
	}
	
	@Override
	public void editFromTree(String selectedRID) {  
		if(!isEmpty(selectedRID)){
			setSelectedRID(selectedRID);
			setTable(null);    
			setOldValue(getEntity().getValue());
			if(getBirim().getSehirRef() != null){
				super.changeIlce(getBirim().getSehirRef().getRID());
			} 
		
			setEditPanelRendered(true);
			getBirim().getbirimtip().toString();
			//setPreviousUstRef(getBirim().getUstRef());		 
			return;
		}   
	} 
	
	public String birimHesap() {	
		if (!setSelected()) {
			return "";
		}	
		getSessionUser().setLegalAccess(1);	
		putObjectToSessionFilter(ApplicationDescriptor._BIRIM_MODEL, getBirim());
		return ManagedBeanLocator.locateMenuController().gotoPage("menu_BirimHesap"); 
	} 
	
	@Override
	public String detailPage(Long selectedRID) {
		if (selectedRID != null && selectedRID > 0) {
			setSelectedRID(selectedRID + "");
		} else if (getSelectedRID() == null) {
			return "";
		}
		ManagedBeanLocator.locateSessionUser().addToSessionFilters(getModelName(),	getEntity());
		return ManagedBeanLocator.locateMenuController().gotoPage("menu_" + getModelName() + "Detay");
	}
	
	@Override
	public void addToTree(String selectedRID) {
		super.addToTree(selectedRID);
		if(getBirim().getSehirRef() != null){
			super.changeIlce(getBirim().getSehirRef().getRID());
		}
		
		setEditPanelRendered(true);
		return;
	}

	
	
	public String birimDonemleri() {	
		if (!setSelected()) {
			return "";
		}	
		getSessionUser().setLegalAccess(1);	
		putObjectToSessionFilter(ApplicationDescriptor._BIRIM_MODEL, getBirim());
		return ManagedBeanLocator.locateMenuController().gotoPage("menu_BirimDonemleri"); 
	} 

	
	
} // class 
