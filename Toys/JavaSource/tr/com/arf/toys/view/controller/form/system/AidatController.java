package tr.com.arf.toys.view.controller.form.system;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.AjaxBehaviorEvent;
import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.primefaces.component.spinner.Spinner;

import tr.com.arf.framework.db.enumerated._common.EvetHayir;
import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.framework.utility.exception.data.DBException;
import tr.com.arf.framework.utility.tool.DateUtil;
import tr.com.arf.framework.utility.tool.KeyUtil;
import tr.com.arf.toys.db.enumerated.uye.UyeAidatDurum;
import tr.com.arf.toys.db.enumerated.uye.UyeDurum;
import tr.com.arf.toys.db.enumerated.uye.UyeKayitDurum;
import tr.com.arf.toys.db.filter.system.AidatFilter;
import tr.com.arf.toys.db.manager.CustomDBOperator;
import tr.com.arf.toys.db.model.system.Aidat;
import tr.com.arf.toys.db.model.uye.Uye;
import tr.com.arf.toys.db.model.uye.Uyeaidat;
import tr.com.arf.toys.db.model.uye.Uyemuafiyet;
import tr.com.arf.toys.service.application.ManagedBeanLocator;
import tr.com.arf.toys.view.controller._common.SessionController;
import tr.com.arf.toys.view.controller.form._base.ToysBaseFilteredController;

@ManagedBean(name = "aidatController")
@ViewScoped
public class AidatController extends ToysBaseFilteredController {

	private static final long serialVersionUID = -6843624017327166220L;

	private Aidat aidat;
	private AidatFilter aidatFilter = new AidatFilter();

	public AidatController() {
		super(Aidat.class);
		setDefaultValues(false);
		setOrderField("yil");
		setAutoCompleteSearchColumns(new String[] { "yil" });
		setTable(null);
		this.aidatFilter.setYil(Calendar.getInstance().get(Calendar.YEAR));
	}

	@Override
	public void init() {
		super.init();
		queryAction();
	}

	public Aidat getAidat() {
		aidat = (Aidat) getEntity();
		return aidat;
	}

	public void setAidat(Aidat aidat) {
		this.aidat = aidat;
	}

	public AidatFilter getAidatFilter() {
		return aidatFilter;
	}

	public void setAidatFilter(AidatFilter aidatFilter) {
		this.aidatFilter = aidatFilter;
	}

	@Override
	public BaseFilter getFilter() {
		return getAidatFilter();
	}

	@Override
	public void insert() {
		super.insert();
		setBorclandirPanelRendered(false);
		getAidat().setYil(Calendar.getInstance().get(Calendar.YEAR));
		getAidat().setBorclandirmaYapildi(EvetHayir._HAYIR);
		getAidat().setKayitucreti(new BigDecimal(0));
		Calendar cal = Calendar.getInstance();
		cal.setTime(new Date());
		cal.set(Calendar.YEAR, getAidat().getYil());
		cal.set(Calendar.MONTH, 0);
		cal.set(Calendar.DAY_OF_MONTH, 1);
		getAidat().setBaslangictarih(cal.getTime());
		cal.set(Calendar.YEAR, getAidat().getYil());
		cal.set(Calendar.MONTH, 11);
		cal.set(Calendar.DAY_OF_MONTH, 31);
		getAidat().setBitistarih(cal.getTime());
	}

	private boolean borclandirPanelRendered;
	private String bulunanUyeMesaj = " ";

	public void aidatBorclandirmaSayfa() {
		if (!setSelected()) {
			return;
		}
		if (getAidat().getYil() < Calendar.getInstance().get(Calendar.YEAR)) {
			createGenericMessage(KeyUtil.getMessageValue("aidat.gecmisHata"), FacesMessage.SEVERITY_ERROR);
			return;
		}
		setBorclandirPanelRendered(true);
		setEditPanelRendered(false);
		String whereCon = " o.uyedurum <> " + UyeDurum._KAPALI.getCode() + " AND o.uyedurum <> " + UyeDurum._PASIFUYE.getCode() + " AND o.uyedurum <> " + UyeDurum._VEFAT.getCode() + " AND o.uyedurum <> " + UyeDurum._EMEKLI.getCode()
				+ " AND o.uyedurum <> " + UyeDurum._ISTIFA.getCode() + " AND o.kisiRef.aktif = " + EvetHayir._EVET.getCode() + " AND o.uyetip = " + getAidat().getUyetip().getCode() + " AND o.kayitdurum = "
				+ UyeKayitDurum._ONAYLANMIS.getCode() + " AND o.rID NOT IN (Select m.uyeRef.rID FROM Uyeaidat m WHERE m.yil = " + getAidat().getYil() + ") ";
		try {
			setBulunanUyeMesaj(getDBOperator().recordCount(Uye.class.getSimpleName(), whereCon) + " üye borçlandırılacak.");
		} catch (DBException e) {
			// TODO Auto-generated catch block
			logYaz("Exception @" + getModelName() + "Controller :", e);
		}
	}

	public boolean isBorclandirPanelRendered() {
		return borclandirPanelRendered;
	}

	public void setBorclandirPanelRendered(boolean borclandirPanelRendered) {
		this.borclandirPanelRendered = borclandirPanelRendered;
	}

	public String getBulunanUyeMesaj() {
		return bulunanUyeMesaj;
	}

	public void setBulunanUyeMesaj(String bulunanUyeMesaj) {
		this.bulunanUyeMesaj = bulunanUyeMesaj;
	}

	private List<Uye> hataliUyeListesi = new ArrayList<Uye>();

	@SuppressWarnings("unchecked")
	public void uyeBorclandir() throws DBException {
		if (!setSelected()) {
			return;
		}
		SessionController sessionController = ManagedBeanLocator.locateSessionController();
		Uyeaidat yeniAidat = new Uyeaidat();
		hataliUyeListesi = new ArrayList<Uye>();
		if (getAidat().getYil() < Calendar.getInstance().get(Calendar.YEAR)) {
			createGenericMessage(KeyUtil.getMessageValue("aidat.gecmisHata"), FacesMessage.SEVERITY_ERROR);
			return;
		}
		EntityManager entityManager = getDBOperator().getEntityManagerFactory().createEntityManager();
		entityManager.getTransaction().begin();
		// Secilen yil icin, aidat tanimlamasi yapilmamis uyelere aidat tanimlaniyor!
		String query = "SELECT OBJECT(o) FROM " + Uye.class.getSimpleName() + " AS o WHERE " + "o.uyedurum <> " + UyeDurum._KAPALI.getCode() + " AND o.uyedurum <> " + UyeDurum._PASIFUYE.getCode() + " AND o.uyedurum <> "
				+ UyeDurum._VEFAT.getCode() + " AND o.uyedurum <> " + UyeDurum._EMEKLI.getCode() + " AND o.uyedurum <> " + UyeDurum._ISTIFA.getCode() + " AND o.kisiRef.aktif = " + EvetHayir._EVET.getCode() + " AND o.uyetip = "
				+ getAidat().getUyetip().getCode() + " AND o.kayitdurum = " + UyeKayitDurum._ONAYLANMIS.getCode() + " AND o.rID NOT IN (Select m.uyeRef.rID FROM Uyeaidat m WHERE m.yil = " + getAidat().getYil() + ") ORDER BY o.rID";
		Query entityQuery = entityManager.createQuery(query);
		List<Uye> aidatiOlmayanUyeListesi = entityQuery.getResultList();
		// Aidat borclandirma yapildi olarak isaretleniyor
		int toplamUyeSayisi = aidatiOlmayanUyeListesi.size();
		System.out.println("Bulunan uye sayisi :" + toplamUyeSayisi);
		int uyeCount = 0;
		BigDecimal aidatMiktar = getAidat().getMiktar();
		for (Uye uye : aidatiOlmayanUyeListesi) {
			Uyemuafiyet uyemuafiyet = new Uyemuafiyet();
			System.out.println("Uye :" + ++uyeCount);
			try {
				// Uyenin muafiyet olusturacak bir durumu varsa, muafiyet giriliyor.
				if (uye.getUyedurum() == UyeDurum._ISSIZ || uye.getUyedurum() == UyeDurum._ASKER || uye.getUyedurum() == UyeDurum._DOGUMIZNI || uye.getUyedurum() == UyeDurum._YURTDISI) {
					uyemuafiyet.setBaslangictarih(new Date());
					uyemuafiyet.setMuafiyettip(sessionController.uyeMuafiyetDurumuBelirle(uye.getUyedurum()));
					uyemuafiyet.setAciklama(sessionController.aciklamaOlustur(uye.getUyedurum()));
					uyemuafiyet.setUyeRef(uye);
					entityManager.persist(uyemuafiyet);
				}

				for (int x = 0; x < 12; x++) {
					yeniAidat = new Uyeaidat();
					yeniAidat.setUyeRef(uye);
					yeniAidat.setMiktar(aidatMiktar);
					yeniAidat.setAy(x + 1);
					yeniAidat.setOdenen(BigDecimal.ZERO);
					yeniAidat.setUyeaidatdurum(UyeAidatDurum._BORCLU);
					yeniAidat.setYil(getAidat().getYil());
					if (uyemuafiyet.getRID() != null) {
						// Eger uyenin muafiyeti varsa, aidat yine de kaydedilir, ancak muafiyet satiri dolu oldugu icin uye borcsuz gorunecektir!
						yeniAidat.setUyemuafiyetRef(uyemuafiyet);
						yeniAidat.setUyeaidatdurum(UyeAidatDurum._MUAF);
					}
					entityManager.persist(yeniAidat);
				}
				// Eski aidat borclari da yeni tutarla guncelleniyor
				if (getDBOperator().recordCount(Uyeaidat.class.getSimpleName(), "o.yil <>" + getAidat().getYil() + " AND o.uyeRef.rID=" + uye.getRID() + " AND o.odenen < o.miktar") > 0) {
					List<Uyeaidat> eskiAidatListesi = getDBOperator().load(Uyeaidat.class.getSimpleName(), "o.yil <>" + getAidat().getYil() + " AND o.uyeRef.rID=" + uye.getRID() + " AND o.odenen < o.miktar", "");
					for (Uyeaidat aidat1 : eskiAidatListesi) {
						aidat1.setMiktar(aidatMiktar);
						entityManager.merge(aidat1);
					}
				}
				getAidat().setBorclandirmaYapildi(EvetHayir._EVET);
				entityManager.merge(getAidat());
			} catch (Exception e) {
				logYaz("HATA :" + e.getMessage());
				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
				hataliUyeListesi.add(uye);
				logYaz("Exception @" + getModelName() + "Controller :", e);
			} // catch
		} // for Uye 
		entityManager.getTransaction().commit();
		createGenericMessage("Borçlandırma İşlemi Başarıyla Gerçekleştirildi!", FacesMessage.SEVERITY_INFO);
		fazlaOdemeleriAktar();
		// logYaz("Borçlandırma İşlemi Başarıyla Gerçekleştirildi!");
	}
	 

	public String getDateTime() {
		return DateUtil.dateToDMYHMS(new Date());
	}

	public void borclandirmaIptal(Aidat aidat) {
		try {
			if (getDBOperator().recordCount(Uyeaidat.class.getSimpleName(), "o.yil = " + aidat.getYil() + " and o.odenen > " + new BigDecimal(0L)) > 0) {
				createGenericMessage(KeyUtil.getMessageValue("aidat.odenmisAidatIptal"), FacesMessage.SEVERITY_ERROR);
				return;
			} else {
				int kayitSayisi = getDBOperator().executeQuery("Delete from Uyeaidat o WHERE o.yil =" + aidat.getYil());
				createGenericMessage(kayitSayisi + " " + KeyUtil.getMessageValue("kayit.silindi"), FacesMessage.SEVERITY_INFO);
			}
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}

	public List<Uye> getHataliUyeListesi() {
		return hataliUyeListesi;
	}

	public void setHataliUyeListesi(List<Uye> hataliUyeListesi) {
		this.hataliUyeListesi = hataliUyeListesi;
	}

	//
	// @Override
	// public void resetFilter(){
	// super.resetFilterOnly();
	// getAidatFilter().setYil(DateUtil.getYear(new Date()));
	// getAidatFilter().createQueryCriterias();
	// queryAction();
	// }

	@Override
	public void save() {
		int yil = getAidat().getYil();
		try {
			if (getDBOperator().recordCount(Aidat.class.getSimpleName(), "o.rID <>" + getAidat().getRID() + " AND o.yil = " + yil + " AND o.uyetip = " + getAidat().getUyetip().getCode()) > 0) {
				createGenericMessage(KeyUtil.getMessageValue("aidat.duplike"), FacesMessage.SEVERITY_WARN);
				return;
			}
			super.justSave();
			getAidatFilter().setYil(yil);
			queryAction();
		} catch (Exception e) {
			logYaz("Exception @" + getModelName() + "Controller :", e);
		}

	}

	@Override
	public void cancel() {
		setBorclandirPanelRendered(false);
		super.cancel();
	}

	@Override
	public void update() {
		if (!setSelected()) {
			return;
		}
		if (getAidat().getYil() < Calendar.getInstance().get(Calendar.YEAR)) {
			createGenericMessage(KeyUtil.getMessageValue("aidat.gecmisGuncellenemez"), FacesMessage.SEVERITY_WARN);
			return;
		}
		if (getAidat().getBorclandirmaYapildi() == EvetHayir._EVET) {
			createGenericMessage(KeyUtil.getMessageValue("aidat.aidatGuncellenemez"), FacesMessage.SEVERITY_WARN);
			return;

		}
		setTable(null);
		if (isLoggable()) {
			setOldValue(getAidat().getValue());
		}
		setAddScreen(false);
		setEditPanelRendered(true);
		setBorclandirPanelRendered(false);
	}

	@Override
	public void delete() {
		if (!setSelected()) {
			return;
		}
		if (getAidat().getBorclandirmaYapildi() == EvetHayir._EVET) {
			createGenericMessage(KeyUtil.getMessageValue("aidat.aidatSilinemez"), FacesMessage.SEVERITY_WARN);
			return;

		} else {
			super.delete();
		}
	}

	public boolean getUyeBorclandirmaYapildimi() {
		if (!setSelected()) {
			// logYaz("Aidat Secilmedi!");
		}
		if (getAidat().getBorclandirmaYapildi() != null) {
			if (getAidat().getBorclandirmaYapildi().getCode() == EvetHayir._EVET.getCode()) {
				return true;
			}
		}
		return false;
	}

	// Yil spinneri icin, value 1900'dan kucukse degeri otomatik olarak current yila set eder.
	public void handleYearChangeForYearSpinner(AjaxBehaviorEvent event) {
		try {
			Spinner spinnerItem = (Spinner) event.getComponent();
			if (Integer.parseInt(spinnerItem.getValue().toString()) < 1900) {
				spinnerItem.setValue(DateUtil.getYear(new Date()));
				getAidatFilter().setYil(DateUtil.getYear(new Date()));
			}
		} catch (Exception e) {
			logYaz("Exception @" + getModelName() + "Controller :", e);
		}
	}

	/* Data Operator */
	private CustomDBOperator customDBOperator; /* Data Operator */

	protected final CustomDBOperator getCustomDBOperator() {
		if (customDBOperator == null) {
			customDBOperator = CustomDBOperator.getInstance();
		}
		return customDBOperator;
	}

	// Uyelerin fazla odemeleri varsa, bunlari borclara aktar.
	public void fazlaOdemeleriAktar() throws DBException {
		if (getCustomDBOperator().fazlaOdemeleriAktar()) {
			createGenericMessage("Fazla ödenen aidatlar diğer aidat borçlarına aktarıldı.", FacesMessage.SEVERITY_INFO);
		} else {
			createGenericMessage("Fazla ödenen aidatlar diğer aidat borçlarına aktarılırken bir hata meydana geldi! Sistem yöneticisiyle iletişime geçiniz!", FacesMessage.SEVERITY_ERROR);
		}
	}
} // class
