package tr.com.arf.toys.view.controller.form.kisi; 
  
 
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.toys.db.filter.kisi.DonemFilter;
import tr.com.arf.toys.db.model.kisi.Anadonem;
import tr.com.arf.toys.db.model.kisi.Donem;
import tr.com.arf.toys.service.application.ApplicationDescriptor;
import tr.com.arf.toys.service.application.ManagedBeanLocator;
import tr.com.arf.toys.view.controller.form._base.ToysBaseFilteredController;
  
  
@ManagedBean 
@ViewScoped 
public class DonemController extends ToysBaseFilteredController {  
  
  private static final long serialVersionUID = 6531152165884980748L; 
  
	private Donem donem;  
	private DonemFilter donemFilter = new DonemFilter();  
  
	public DonemController() { 
		super(Donem.class);  
		setDefaultValues(false);  
		setOrderField("tanim");  
		setAutoCompleteSearchColumns(new String[]{"tanim"}); 
		setTable(null); 
		setStaticWhereCondition(getSessionUser().createStaticWhereCondition("o."));
		if(getMasterEntity() != null){ 
			setMasterObjectName(ApplicationDescriptor._ANADONEM_MODEL);
			setMasterOutcome(ApplicationDescriptor._ANADONEM_MODEL);
			getDonemFilter().setAnadonemRef(getMasterEntity());
			getDonemFilter().createQueryCriterias();   
		} 
		queryAction(); 
	} 
	
	public Anadonem getMasterEntity(){
		if(getDonemFilter().getAnadonemRef() != null){
			return getDonemFilter().getAnadonemRef();
		} else {
			return (Anadonem) getObjectFromSessionFilter(ApplicationDescriptor._ANADONEM_MODEL);
		}			
	} 
	
	public Donem getDonem() {
		donem = (Donem) getEntity(); 
		return donem;
	}

	public void setDonem(Donem donem) {
		this.donem = donem;
	}

	public DonemFilter getDonemFilter() {
		return donemFilter;
	}

	public void setDonemFilter(DonemFilter donemFilter) {
		this.donemFilter = donemFilter;
	}

	@Override  
	public BaseFilter getFilter() {  
		return getDonemFilter();  
	}  
	
	@Override 
	public void resetFilter() {
		super.resetFilterOnly();
		if(getMasterEntity() != null){
			getDonemFilter().setAnadonemRef(getMasterEntity()); 
		}
		queryAction();
	}
 
	@Override	
	public void insert() {	
		super.insert();	
		getDonem().setAnadonemRef(getDonemFilter().getAnadonemRef());	
	}	
	
	public String kurulDonem() {	
		if (!setSelected()) {
			return "";
		}	
		getSessionUser().setLegalAccess(1);	
		putObjectToSessionFilter(ApplicationDescriptor._DONEM_MODEL, getDonem());
		return ManagedBeanLocator.locateMenuController().gotoPage("menu_KurulDonem"); 
	} 
	
	public String komisyonDonem() {	
		if (!setSelected()) {
			return "";
		}	
		getSessionUser().setLegalAccess(1);	
		putObjectToSessionFilter(ApplicationDescriptor._DONEM_MODEL, getDonem());
		return ManagedBeanLocator.locateMenuController().gotoPage("menu_KomisyonDonem"); 
	} 
	
 
} // class 
