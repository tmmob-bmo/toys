package tr.com.arf.toys.view.controller.form.system; 
  
 
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.framework.db.model._base.BaseEntity;
import tr.com.arf.toys.db.filter.system.FakulteFilter;
import tr.com.arf.toys.db.model.system.Fakulte;
import tr.com.arf.toys.db.model.system.Universite;
import tr.com.arf.toys.service.application.ApplicationDescriptor;
import tr.com.arf.toys.service.application.ManagedBeanLocator;
import tr.com.arf.toys.view.controller.form._base.ToysBaseFilteredController;
  
  
@ManagedBean 
@ViewScoped 
public class FakulteController extends ToysBaseFilteredController {  
  
  private static final long serialVersionUID = 6531152165884980748L; 
  
	private Fakulte fakulte;  
	private FakulteFilter fakulteFilter = new FakulteFilter();  
  
	public FakulteController() { 
		super(Fakulte.class);  
		setDefaultValues(false);  
		setOrderField("ad");  
		setAutoCompleteSearchColumns(new String[]{"ad"}); 
		setTable(null); 
		if (getMasterEntity() != null) {
			if (getMasterEntity().getClass() == Universite.class) {
				setMasterObjectName(ApplicationDescriptor._UNIVERSITE_MODEL);
				setMasterOutcome(ApplicationDescriptor._UNIVERSITE_MODEL);
//				getFakulteFilter().setUniversiteRef((Universite) getMasterEntity());
			} else {
				setMasterOutcome(null);
			}
		}
		getFakulteFilter().createQueryCriterias();  
		queryAction(); 
	} 
	
	public BaseEntity getMasterEntity(){
		if(getObjectFromSessionFilter(ApplicationDescriptor._UNIVERSITE_MODEL) != null){
			return (Universite) getObjectFromSessionFilter(ApplicationDescriptor._UNIVERSITE_MODEL);
		}  
		else {
			return null;
		}			
	} 
	public Fakulte getFakulte() {
		fakulte=(Fakulte) getEntity();
		return fakulte;
	}


	public void setFakulte(Fakulte fakulte) {
		this.fakulte = fakulte;
	}


	public FakulteFilter getFakulteFilter() {
		return fakulteFilter;
	}


	public void setFakulteFilter(FakulteFilter fakulteFilter) {
		this.fakulteFilter = fakulteFilter;
	}

	@Override  
	public BaseFilter getFilter() {  
		return getFakulteFilter();  
	}  
	
	public String bolum() {	
		if (!setSelected()) {
			return "";
		}	
		getSessionUser().setLegalAccess(1);	
		putObjectToSessionFilter(ApplicationDescriptor._FAKULTE_MODEL, getFakulte());
		return ManagedBeanLocator.locateMenuController().gotoPage("menu_Bolum"); 
	} 
	
	@Override
	public void insert(){
		super.insert();
//		getFakulte().setUniversiteRef((Universite) getMasterEntity());
	}
	
	
} // class 
