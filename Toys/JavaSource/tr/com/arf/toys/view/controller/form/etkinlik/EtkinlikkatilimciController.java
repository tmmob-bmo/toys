package tr.com.arf.toys.view.controller.form.etkinlik;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.toys.db.filter.etkinlik.EtkinlikkatilimciFilter;
import tr.com.arf.toys.db.model.etkinlik.Etkinlik;
import tr.com.arf.toys.db.model.etkinlik.Etkinlikkatilimci;
import tr.com.arf.toys.service.application.ApplicationDescriptor;
import tr.com.arf.toys.view.controller.form._base.ToysBaseFilteredController;

@ManagedBean
@ViewScoped
public class EtkinlikkatilimciController extends ToysBaseFilteredController {

	private static final long serialVersionUID = 6531152165884980748L;

	private Etkinlikkatilimci etkinlikkatilimci;
	private EtkinlikkatilimciFilter etkinlikkatilimciFilter = new EtkinlikkatilimciFilter();

	public EtkinlikkatilimciController() {
		super(Etkinlikkatilimci.class); 
		setDefaultValues(false);
		setOrderField("etkinlikRef.rID");
		setLoggable(false);
		setAutoCompleteSearchColumns(new String[] { "etkinlikRef" });
		setTable(null);
		if (getMasterEntity() != null) {
			setMasterObjectName(ApplicationDescriptor._ETKINLIK_MODEL);
			setMasterOutcome(ApplicationDescriptor._ETKINLIK_MODEL);
			getEtkinlikkatilimciFilter().setEtkinlikRef(getMasterEntity());
			getEtkinlikkatilimciFilter().createQueryCriterias();
		}
		queryAction();
	}

	public EtkinlikkatilimciController(Object object) { 
		super(Etkinlikkatilimci.class);    
	}
	
	public Etkinlik getMasterEntity() {
		if (getEtkinlikkatilimciFilter().getEtkinlikRef() != null) {
			return getEtkinlikkatilimciFilter().getEtkinlikRef();
		} else {
			return (Etkinlik) getObjectFromSessionFilter(ApplicationDescriptor._ETKINLIK_MODEL);
		}
	}

	public Etkinlikkatilimci getEtkinlikkatilimci() {
		etkinlikkatilimci = (Etkinlikkatilimci) getEntity();
		return etkinlikkatilimci;
	}

	public void setEtkinlikkatilimci(Etkinlikkatilimci etkinlikkatilimci) {
		this.etkinlikkatilimci = etkinlikkatilimci;
	}

	public EtkinlikkatilimciFilter getEtkinlikkatilimciFilter() {
		return etkinlikkatilimciFilter;
	}

	public void setEtkinlikkatilimciFilter(
			EtkinlikkatilimciFilter etkinlikkatilimciFilter) {
		this.etkinlikkatilimciFilter = etkinlikkatilimciFilter;
	}

	@Override
	public BaseFilter getFilter() {
		return getEtkinlikkatilimciFilter();
	}

	@Override
	public void resetFilter() {
		super.resetFilterOnly();
		if (getMasterEntity() != null) {
			getEtkinlikkatilimciFilter().setEtkinlikRef(getMasterEntity());
		}
		queryAction();
	}

	@Override
	public void insert() {
		super.insert();
		getEtkinlikkatilimci().setEtkinlikRef(getEtkinlikkatilimciFilter().getEtkinlikRef());
	}
 
	
} // class 
