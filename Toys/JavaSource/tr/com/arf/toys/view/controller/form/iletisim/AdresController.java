package tr.com.arf.toys.view.controller.form.iletisim; 
  
 
import java.util.Date;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import tr.com.arf.framework.db.enumerated._common.EvetHayir;
import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.framework.db.model._base.BaseEntity;
import tr.com.arf.framework.utility.tool.KeyUtil;
import tr.com.arf.toys.db.enumerated.iletisim.AdresTipi;
import tr.com.arf.toys.db.filter.iletisim.AdresFilter;
import tr.com.arf.toys.db.model.iletisim.Adres;
import tr.com.arf.toys.db.model.kisi.Kisi;
import tr.com.arf.toys.db.model.kisi.Kurum;
import tr.com.arf.toys.db.model.system.Birim;
import tr.com.arf.toys.service.application.ApplicationDescriptor;
import tr.com.arf.toys.view.controller.form._base.ToysBaseFilteredController;
  
  
@ManagedBean 
@ViewScoped 
public class AdresController extends ToysBaseFilteredController {  
  
  private static final long serialVersionUID = 6531152165884980748L; 
  
	private Adres adres;  
	private AdresFilter adresFilter = new AdresFilter();  
  
	public AdresController() { 
		super(Adres.class);  
		setDefaultValues(false);  
		setOrderField("varsayilan");  
		setAutoCompleteSearchColumns(new String[]{"acikAdres"}); 
		setTable(null); 
		if(getMasterEntity() != null){ 
			if(getMasterEntity().getClass() == Kisi.class){
				setMasterObjectName(ApplicationDescriptor._KISI_MODEL);
				setMasterOutcome(ApplicationDescriptor._KISI_MODEL);
				getAdresFilter().setKisiRef((Kisi)getMasterEntity());				 
			} else if(getMasterEntity().getClass() == Kurum.class){
				setMasterObjectName(ApplicationDescriptor._KURUM_MODEL);
				getAdresFilter().setKurumRef((Kurum) getMasterEntity());
				setMasterOutcome(ApplicationDescriptor._KURUM_MODEL);					 
			} else if(getMasterEntity().getClass() == Birim.class){ 
				setMasterObjectName(ApplicationDescriptor._BIRIM_MODEL);
				getAdresFilter().setKurumRef(((Birim) getMasterEntity()).getKurumRef());
				setMasterOutcome(ApplicationDescriptor._BIRIM_MODEL);					 
			}  
			getAdresFilter().createQueryCriterias();  
		}
		setLoggable(true);
		queryAction(); 
	} 
 
	public AdresController(Object object) { 
		super(Adres.class);    
		setLoggable(true); 
	}
	
	public BaseEntity getMasterEntity(){
		if(getObjectFromSessionFilter(ApplicationDescriptor._KISI_MODEL) != null){
			return (Kisi) getObjectFromSessionFilter(ApplicationDescriptor._KISI_MODEL);
		} else if(getObjectFromSessionFilter(ApplicationDescriptor._KURUM_MODEL) != null){
			return (Kurum) getObjectFromSessionFilter(ApplicationDescriptor._KURUM_MODEL);
		} else if(getObjectFromSessionFilter(ApplicationDescriptor._BIRIM_MODEL) != null){
			return (Birim) getObjectFromSessionFilter(ApplicationDescriptor._BIRIM_MODEL);
		}  else {
			return null;
		}			
	}  
	
	public Adres getAdres() { 
		adres = (Adres) getEntity(); 
		return adres; 
	} 
	
	public void setAdres(Adres adres) { 
		this.adres = adres; 
	} 
	
	public AdresFilter getAdresFilter() { 
		return adresFilter; 
	} 
	 
	public void setAdresFilter(AdresFilter adresFilter) {  
		this.adresFilter = adresFilter;  
	}  
	 
	@Override  
	public BaseFilter getFilter() {  
		return getAdresFilter();  
	} 
	
	@Override	
	public void save() { 
		if(this.adres.getAdrestipi() == AdresTipi._YURTDISIADRESI){
			if(this.adres.getUlkeRef() == null || isEmpty(this.adres.getAcikAdres()) || this.adres.getSehirRef() == null){
				createGenericMessage("Yurdışı adresleri için ülke, il ve açık adres alanları zorunludur", FacesMessage.SEVERITY_ERROR);
				return;
			}
		} else if(this.adres.getAdrestipi() != AdresTipi._YURTDISIADRESI){
			if(this.adres.getUlkeRef() == null || isEmpty(this.adres.getAcikAdres()) || this.adres.getSehirRef() == null
					|| this.adres.getIlceRef() == null || isEmpty(this.adres.getMahalle())){
				createGenericMessage("Adres için ülke, il, ilçe, mahalle ve açık adres alanları zorunludur", FacesMessage.SEVERITY_ERROR);
				return;
			} 
		}	
		super.justSave();	
		if(this.adres.getVarsayilan() == EvetHayir._EVET){
			String whereCondition = "o.rID <> " + this.adres.getRID() + " AND o.varsayilan =" + EvetHayir._EVET.getCode();
			if(this.adres.getKurumRef() != null) {
				whereCondition += " AND o.kurumRef.rID = " + this.adres.getKurumRef().getRID();
			} else if(this.adres.getKisiRef() != null){ 
				whereCondition += " AND o.kisiRef.rID = " + this.adres.getKisiRef().getRID();
			}
			
			try {
				if(getDBOperator().recordCount(getModelName(), whereCondition) > 0){
					for(Object obj : getDBOperator().load(getModelName(), whereCondition, "o.rID")){
						Adres adres = (Adres) obj;
						adres.setVarsayilan(EvetHayir._HAYIR);
						getDBOperator().update(adres);
					}					
				}
			} catch (Exception e) { 
				logYaz("Exception @" + getModelName() + "Controller :", e);
			} 
		} else if (this.adres.getVarsayilan() == EvetHayir._HAYIR){
			String whereCondition = "o.rID <> " + this.adres.getRID() + " AND o.varsayilan =" + EvetHayir._HAYIR.getCode();
			if(this.adres.getKurumRef() != null) {
				whereCondition += " AND o.kurumRef.rID = " + this.adres.getKurumRef().getRID();
			} else if(this.adres.getKisiRef() != null){ 
				whereCondition += " AND o.kisiRef.rID = " + this.adres.getKisiRef().getRID();
			}
			String whereCondition1 = "o.rID <> " + this.adres.getRID() + " AND o.varsayilan =" + EvetHayir._EVET.getCode();
			if(this.adres.getKurumRef() != null) {
				whereCondition1 += " AND o.kurumRef.rID = " + this.adres.getKurumRef().getRID();
			} else if(this.adres.getKisiRef() != null){ 
				whereCondition1 += " AND o.kisiRef.rID = " + this.adres.getKisiRef().getRID();
			}
			
			try {
				if(getDBOperator().recordCount(getModelName(), whereCondition) == 0 && getDBOperator().recordCount(getModelName(), whereCondition1) == 0){
					adres.setVarsayilan(EvetHayir._EVET);
					getDBOperator().update(adres);
				}	
				whereCondition = "o.rID <> " + this.adres.getRID() + " AND o.varsayilan =" + EvetHayir._EVET.getCode();
				if(this.adres.getKurumRef() != null) {
					whereCondition += " AND o.kurumRef.rID = " + this.adres.getKurumRef().getRID();
				} else if(this.adres.getKisiRef() != null){ 
					whereCondition += " AND o.kisiRef.rID = " + this.adres.getKisiRef().getRID();
				}
				if(getDBOperator().recordCount(getModelName(), whereCondition) == 0){ 
					createGenericMessage(KeyUtil.getMessageValue("varsayilan.hata"), FacesMessage.SEVERITY_INFO);  
					adres.setVarsayilan(EvetHayir._EVET);
					getDBOperator().update(adres);
				}
				
			}catch (Exception e) { 
				logYaz("Exception @" + getModelName() + "Controller :", e);
			}  
		}
		queryAction();
	}	
	
	@Override
	public void delete() {
		if (!setSelected()) {
			return;
		} 	    
		Long kurumRef = 0L;
		Long kisiRef = 0L; 
		if(getAdres().getKisiRef() != null){
			kisiRef = this.adres.getKisiRef().getRID();
		} else if(getAdres().getKurumRef() != null){
			kurumRef = this.adres.getKurumRef().getRID();
		}  		
		try {
			getDBOperator().delete(super.getEntity()); 
			setSelection(null); 
			setTable(null);
			createGenericMessage(KeyUtil.getMessageValue("kayit.silindi"), FacesMessage.SEVERITY_INFO); 
		} catch (Exception e) {
			createGenericMessage(KeyUtil.getMessageValue("islem.hata") + " Mes : " + e.getMessage(), FacesMessage.SEVERITY_ERROR);  
			return;
		} 
		 
		String whereCondition ="o.varsayilan =" + EvetHayir._EVET.getCode();
			if(kurumRef != 0) {
				whereCondition += " AND o.kurumRef.rID = " + kurumRef;
			} else if(kisiRef != 0){ 
				whereCondition += " AND o.kisiRef.rID = " + kisiRef;
			}
			
			try {
				if(getDBOperator().recordCount(getModelName(), whereCondition) == 0){
					whereCondition = "o.rID <> " + this.adres.getRID() + " AND o.varsayilan =" + EvetHayir._HAYIR.getCode();
					if(kurumRef != 0) {
						whereCondition += " AND o.kurumRef.rID = " + kurumRef;
					} else if(kisiRef != 0){ 
						whereCondition += " AND o.kisiRef.rID = " + kisiRef;
					}
					if(getDBOperator().recordCount(getModelName(), whereCondition)> 0){ 
						Adres adrs = (Adres) getDBOperator().load(getModelName(), whereCondition, "o.rID DESC").get(0);
						if (adrs!=null){
							adrs.setVarsayilan(EvetHayir._EVET);
							getDBOperator().update(adrs);
						}
					}
					
				}	 
			}catch (Exception e) { 
				logYaz("Exception @" + getModelName() + "Controller :", e);
			} 
			queryAction();	 
		}

 
	@Override	
	public void insert() {	
		super.insert();		
		getAdres().setVarsayilan(EvetHayir._HAYIR);	
		getAdres().setBeyanTarihi(new Date());
		if(getAdresFilter().getKisiRef() != null){ 
			getAdres().setKisiRef(getAdresFilter().getKisiRef());
		} else if(getAdresFilter().getKurumRef() != null) {			
			getAdres().setKurumRef(getAdresFilter().getKurumRef()); 
		} 
	}	
	
} // class 
