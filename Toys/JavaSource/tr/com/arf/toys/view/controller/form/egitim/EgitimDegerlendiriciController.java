package tr.com.arf.toys.view.controller.form.egitim; 
 
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.toys.db.filter.egitim.EgitimDegerlendiriciFilter;
import tr.com.arf.toys.db.model.egitim.EgitimDegerlendirici;
import tr.com.arf.toys.db.model.egitim.Egitimtanim;
import tr.com.arf.toys.service.application.ApplicationDescriptor;
import tr.com.arf.toys.view.controller.form._base.ToysBaseFilteredController;
  
  
@ManagedBean 
@ViewScoped 
public class EgitimDegerlendiriciController extends ToysBaseFilteredController {  
  
  private static final long serialVersionUID = 6531152165884980748L; 
  
	private EgitimDegerlendirici egitimDegerlendirici;  
	private EgitimDegerlendiriciFilter egitimDegerlendiriciFilter = new EgitimDegerlendiriciFilter();  
  
	public EgitimDegerlendiriciController() { 
		super(EgitimDegerlendirici.class);  
		setDefaultValues(false);  
		setLoggable(false);  
		setTable(null); 
		if (getMasterEntity() != null) {
			setMasterObjectName(ApplicationDescriptor._EGITIMTANIM_MODEL);
			setMasterOutcome(ApplicationDescriptor._EGITIMTANIM_MODEL);
			getEgitimDegerlendiriciFilter().setEgitimtanimRef(getMasterEntity());
			getEgitimDegerlendiriciFilter().createQueryCriterias();
		}
		queryAction();
	}
	
	public EgitimDegerlendiriciController(Object object) { 
		super(EgitimDegerlendirici.class);  
		setLoggable(true); 
	}
	
	public Egitimtanim getMasterEntity() {
		if (getEgitimDegerlendiriciFilter().getEgitimtanimRef() != null) {
			return getEgitimDegerlendiriciFilter().getEgitimtanimRef();
		} else {
			return (Egitimtanim) getObjectFromSessionFilter(ApplicationDescriptor._EGITIMTANIM_MODEL);
		}
	}
	
	public EgitimDegerlendirici getEgitimDegerlendirici() {
		egitimDegerlendirici=(EgitimDegerlendirici) getEntity();
		return egitimDegerlendirici;
	}

	public void setEgitimDegerlendirici(EgitimDegerlendirici egitimDegerlendirici) {
		this.egitimDegerlendirici = egitimDegerlendirici;
	}

	public EgitimDegerlendiriciFilter getEgitimDegerlendiriciFilter() {
		return egitimDegerlendiriciFilter;
	}

	public void setEgitimDegerlendiriciFilter(
			EgitimDegerlendiriciFilter egitimDegerlendiriciFilter) {
		this.egitimDegerlendiriciFilter = egitimDegerlendiriciFilter;
	}

	@Override  
	public BaseFilter getFilter() {  
		return getEgitimDegerlendiriciFilter(); 
	}  
	
	@Override
		public void save() {
			
			if (getMasterEntity()!=null){
				egitimDegerlendirici.setEgitimtanimRef(getMasterEntity());
			}else {
				createGenericMessage("Eğitim Değerlendirme Bilgileri Boş Geldiğinden Dolayı Kaydetme İşlemini"
						+ " Gerçekleştiremiyoruz!", FacesMessage.SEVERITY_INFO);
				return ;
			}
			super.save();
			queryAction();
		}
	
} // class 
