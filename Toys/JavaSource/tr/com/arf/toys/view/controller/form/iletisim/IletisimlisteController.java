package tr.com.arf.toys.view.controller.form.iletisim;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.toys.db.filter.iletisim.IletisimlisteFilter;
import tr.com.arf.toys.db.model.iletisim.Iletisimliste;
import tr.com.arf.toys.service.application.ApplicationDescriptor;
import tr.com.arf.toys.service.application.ManagedBeanLocator;
import tr.com.arf.toys.view.controller.form._base.ToysBaseTreeController;
import tr.com.arf.toys.view.controller.form.kisi.KisikimlikController;

@ManagedBean
@ViewScoped
public class IletisimlisteController extends ToysBaseTreeController {

	private static final long serialVersionUID = 6531152165884980748L;

	private Iletisimliste iletisimliste;
	private IletisimlisteFilter iletisimlisteFilter = new IletisimlisteFilter();
	public KisikimlikController l = new KisikimlikController();

	public IletisimlisteController() {
		super(Iletisimliste.class);
		setDefaultValues(false);
		setOrderField("ad");
		setLoggable(false);
		setAutoCompleteSearchColumns(new String[] { "ad" });
		setTable(null);
		queryAction();
	}

	public Iletisimliste getIletisimliste() {
		iletisimliste = (Iletisimliste) getEntity();
		return iletisimliste;
	}

	public void setIletisimliste(Iletisimliste iletisimliste) {
		this.iletisimliste = iletisimliste;
	}

	public IletisimlisteFilter getIletisimlisteFilter() {
		return iletisimlisteFilter;
	}

	public void setIletisimlisteFilter(IletisimlisteFilter iletisimlisteFilter) {
		this.iletisimlisteFilter = iletisimlisteFilter;
	}

	@Override
	public BaseFilter getFilter() {
		return getIletisimlisteFilter();
	}

	public String iletisimlistedetay() {
		if (!setSelected()) {
			return "";
		}
		getSessionUser().setLegalAccess(1);
		putObjectToSessionFilter(ApplicationDescriptor._ILETISIMLISTE_MODEL, getIletisimliste());
		return "iletisimlistedetay.do";
	}

	public String iletisimlistegonderimdetay() {
		if (!setSelected()) {
			return "";
		}
		getSessionUser().setLegalAccess(1);
		putObjectToSessionFilter(ApplicationDescriptor._ILETISIMLISTE_MODEL, getIletisimliste());
		return ManagedBeanLocator.locateMenuController().gotoPage("menu_Gonderim");
	}

	public String epostagonder() {
		if (!setSelected()) {
			return "";
		}
		getSessionUser().setLegalAccess(1);
		putObjectToSessionFilter(ApplicationDescriptor._ILETISIMLISTE_MODEL, getIletisimliste());
		return ManagedBeanLocator.locateMenuController().gotoPage("menu_EpostaGonderim");
	}

	public String smsgonder() {
		if (!setSelected()) {
			return "";
		}
		getSessionUser().setLegalAccess(1);
		putObjectToSessionFilter(ApplicationDescriptor._ILETISIMLISTE_MODEL, getIletisimliste());
		return ManagedBeanLocator.locateMenuController().gotoPage("menu_SMSGonderim");
	}

	@Override
	public synchronized void save() {
		if (this.iletisimliste.getUstRef() == null) {
			this.iletisimliste.setUstRef(iletisimliste);

		}
		super.save();
	}

	public String listeuyeleri() {
		if (!setSelected()) {
			return "";
		}
		getSessionUser().setLegalAccess(1);
		putObjectToSessionFilter(ApplicationDescriptor._ILETISIMLISTE_MODEL, getIletisimliste());
		return "listeuye.do";
	}

} // class
