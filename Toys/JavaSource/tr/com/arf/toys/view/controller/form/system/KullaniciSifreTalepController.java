package tr.com.arf.toys.view.controller.form.system;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.validator.ValidatorException;

import org.apache.log4j.Logger;
import org.primefaces.context.RequestContext;

import tr.com.arf.framework.db.enumerated._common.EvetHayir;
import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.framework.utility.exception.data.DBException;
import tr.com.arf.framework.utility.security.ScryptPasswordHashing;
import tr.com.arf.framework.utility.tool.DateUtil;
import tr.com.arf.framework.utility.tool.QueryUtil;
import tr.com.arf.framework.utility.tool.RandomObjectGenerator;
import tr.com.arf.toys.db.filter.system.KullaniciSifreTalepFilter;
import tr.com.arf.toys.db.model.iletisim.Internetadres;
import tr.com.arf.toys.db.model.system.Kullanici;
import tr.com.arf.toys.db.model.system.KullaniciSifreTalep;
import tr.com.arf.toys.db.model.uye.Uye;
import tr.com.arf.toys.service.application.ApplicationDescriptor;
import tr.com.arf.toys.utility.tool.SendEmail;
import tr.com.arf.toys.view.controller.form._base.ToysBaseSecuredController;

@ManagedBean
@ViewScoped
public class KullaniciSifreTalepController extends ToysBaseSecuredController {

	private static final long serialVersionUID = 6531152165884980748L;

    private static final Logger LOGGER = Logger.getLogger(KullaniciSifreTalepController.class);

	private KullaniciSifreTalep kullaniciSifreTalep;
	private KullaniciSifreTalepFilter kullaniciSifreTalepFilter = new KullaniciSifreTalepFilter();

	private String userName;
    private String ePosta;
	private String guvenlikkodu;
    private String yeniSifre;
    private String yeniSifreTekrar;
	private boolean tcKimlikValidator = true;
	private boolean sicilnoValidator = true;

    private int formDurum = 0;

	public KullaniciSifreTalepController() {
		super(KullaniciSifreTalep.class);
		setDefaultValues(false);
		setTable(null);
		queryAction();
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

    public String getePosta() {
        return ePosta;
    }

    public void setePosta(String ePosta) {
        this.ePosta = ePosta;
    }

    public String getGuvenlikkodu() {
		return guvenlikkodu;
	}

	public void setGuvenlikkodu(String guvenlikkodu) {
		this.guvenlikkodu = guvenlikkodu;
	}

    public String getYeniSifre() {
        return yeniSifre;
    }

    public void setYeniSifre(String yeniSifre) {
        this.yeniSifre = yeniSifre;
    }

    public String getYeniSifreTekrar() {
        return yeniSifreTekrar;
    }

    public void setYeniSifreTekrar(String yeniSifreTekrar) {
        this.yeniSifreTekrar = yeniSifreTekrar;
    }

    public int getFormDurum() {
        return formDurum;
    }

    public void setFormDurum(int formDurum) {
        this.formDurum = formDurum;
    }

    @Override
	public BaseFilter getFilter() {
		return kullaniciSifreTalepFilter;
	}

	@SuppressWarnings("unchecked")
	public void guvenlikKoduGonderUye() {
		try {
            int usernameLength = userName.length();
            if (usernameLength == 11) {
				validateTCKimlikNumarasi(userName);
				if (!tcKimlikValidator) {
					RequestContext context = RequestContext.getCurrentInstance();
					context.update("kullaniciSifreTalepForm");
					return;
				}
			} else if (usernameLength <= 6) {
                while (userName.length() < 6) {
                    userName = "0" + userName;
                }
				validateSicilNo(userName);
				if (!sicilnoValidator) {
					createGenericMessage("Sicil Numarası Geçersiz!", FacesMessage.SEVERITY_ERROR);
					RequestContext context = RequestContext.getCurrentInstance();
					context.update("kullaniciSifreTalepForm");
					return;
				}
			}
            List<Kullanici> kullaniciList = getDBOperator().load(ApplicationDescriptor._KULLANICI_MODEL, "o.name='" + userName + "'", "o.name");
			if (kullaniciList.size() == 0) {
                // TC Kimlik No'dan kullanıcı kaydına ulaşamadık
                // Sicil numarası üzerinden arayalım
				if (getDBOperator().recordCount(ApplicationDescriptor._UYE_MODEL, "o.sicilno='" + userName + "'") > 0) {
					Uye uye = (Uye) getDBOperator().load(ApplicationDescriptor._UYE_MODEL, "o.sicilno='" + userName + "'", "").get(0);
					if (getDBOperator().recordCount(ApplicationDescriptor._KULLANICI_MODEL, "o.kisiRef.rID=" + uye.getKisiRef().getRID()) > 0) {
						kullaniciList = getDBOperator().load(ApplicationDescriptor._KULLANICI_MODEL, "o.kisiRef.rID=" + uye.getKisiRef().getRID(), "");
					}
				}
			}
			if (!isEmpty(kullaniciList)) {
				Kullanici kullanici = kullaniciList.get(0);

                List epostalari = getDBOperator().load(Internetadres.class.getSimpleName(),
                        "o.kisiRef.rID=" + kullanici.getKisiRef().getRID(), "");
                for (Object item : epostalari) {
                    Internetadres adres = (Internetadres) item;
                    String netadresmetni = adres.getNetadresmetni();
                    if (ePosta.equals(netadresmetni)) {
                        kullaniciSifreTalep = new KullaniciSifreTalep();
                        kullaniciSifreTalep.setKullaniciRef(kullanici);
                        String password = RandomObjectGenerator.generateRandomPassword(12);
                        kullaniciSifreTalep.setGuvenlikkodu(ScryptPasswordHashing.encrypt(password));
                        SendEmail email = new SendEmail();
                        email.sendEmailToRecipent(ePosta, "BMO Güvenlik Kodu", "Güvenlik Kodu: " + password + "");
                        createGenericMessage("Güvenlik kodu e-posta adresinize gönderildi",
                                FacesMessage.SEVERITY_INFO);
                        ++formDurum;
                        return;
                    }
                }
                // bu noktaya gelmişsek kullanıcının girdiği e-posta adresi sistemde kayıtlı e-posta adresleri
                // arasında yok demektir
                createGenericMessage("Verdiğiniz e-posta adresi sistemimizde kayıtlı adresinizle uyuşmamaktadır."
                                + " Oda birimlerimizle iletişime geçerek yardım alabilirsiniz.",
                        FacesMessage.SEVERITY_INFO);
			}
            else {
                createGenericMessage("Sistemimizde üye kaydınıza ulaşılamadı. Oda birimlerimizle iletişime geçerek yardım alabilirsiniz.", FacesMessage.SEVERITY_INFO);
            }
		} catch (Exception exc) {
			createGenericMessage("İşlem Sırasında Bir Hata Meydana Geldi!" + "Şifre Almak İçin En Yakınınızdaki Oda Birimiyle İletişime Geçmelisiniz!", FacesMessage.SEVERITY_INFO);
            LOGGER.error(exc.toString(), exc);
		}

	}

    public void guvenlikKoduDogrula() {
        if (ScryptPasswordHashing.check(getGuvenlikkodu(), kullaniciSifreTalep.getGuvenlikkodu())) {
            createGenericMessage(
                    "Güvenlik kodu doğrulandı. Yeni şifrenizi belirleyebilirsiniz.",
                    FacesMessage.SEVERITY_INFO);
            ++formDurum;
        }
        else {
            createGenericMessage(
                    "Güvenlik kodu doğrulanamdı. Lütfen e-posta adresinize gönderilen güvenlik kodunu giriniz.",
                    FacesMessage.SEVERITY_INFO);
        }
    }

    public void sifreDegistir() {
        // şifre geçerlilik kontrolü xhtml'deki passwordValidator tarafından sağlanmaktadır
        if (yeniSifre.equals(yeniSifreTekrar)) {
            try {
                Kullanici kullanici = kullaniciSifreTalep.getKullaniciRef();
                kullanici.setPassword(ScryptPasswordHashing.encrypt(yeniSifre));
                getDBOperator().update(kullanici);
                createGenericMessage(
                        "Şifreniz başarılı bir şekilde değiştirildi. Yeni şifrenizle sisteme giriş yapabilirsiniz.",
                        FacesMessage.SEVERITY_INFO);
                ++formDurum;
            } catch (DBException exc) {
                LOGGER.error(exc.toString(), exc);
                createGenericMessage("Sistemde beklenmeyen bir hata oluştu. Litfen daha sonra tekrar deneyiniz.",
                        FacesMessage.SEVERITY_ERROR);
            }

        }
        else {
            createGenericMessage("Şifre aynı değil. Lütfen yukarıdaki 2 alana da aynı şifreyi yazınız.",
                    FacesMessage.SEVERITY_ERROR);
        }
    }

	public void validateTCKimlikNumarasi(Object value) throws ValidatorException {
		String kimlikNo = "";

		if (value != null) {
			kimlikNo = (String) value;
		}
		kimlikNo = kimlikNo.replaceAll(" ", "");

		if (kimlikNo.trim().length() != 11) {
			createGenericMessage("TC Kimlik Numarası Geçersiz!", FacesMessage.SEVERITY_ERROR);
			tcKimlikValidator = false;

		} else if (Integer.parseInt(kimlikNo.charAt(0) + "") == 0) {
			createGenericMessage("TC Kimlik Numarası Geçersiz!", FacesMessage.SEVERITY_ERROR);
			tcKimlikValidator = false;
		} else {

			int[] numaralar = new int[11];
			for (int i = 0; i < 11; i++) {
                try {
                    numaralar[i] = Integer.parseInt(kimlikNo.charAt(i) + "");
                } catch (NumberFormatException e) {
                    createGenericMessage("TC Kimlik Numarası Geçersiz!", FacesMessage.SEVERITY_ERROR);
                    tcKimlikValidator = false;
                    return;
                }
            }

			int ciftlerToplami = 0;
			for (int i = 1; i < 8; i += 2) {
				ciftlerToplami += numaralar[i];
			}

			int teklerToplami = 0;
			for (int i = 0; i <= 8; i += 2) {

				teklerToplami += numaralar[i];

			}

			int t1 = teklerToplami * 3 + ciftlerToplami;
			int c1 = (10 - t1 % 10) % 10;
			int t2 = c1 + ciftlerToplami;
			int t3 = t2 * 3 + teklerToplami;
			int c2 = (10 - t3 % 10) % 10;

			if (c1 != numaralar[9] || c2 != numaralar[10]) {
				createGenericMessage("TC Kimlik Numarası Geçersiz!", FacesMessage.SEVERITY_ERROR);
				tcKimlikValidator = false;
			}
		}
	}

	public void validateSicilNo(Object value) throws ValidatorException {
		String sicilno = "";
		int sicilnochar;
		if (value != null) {
			sicilno = (String) value;
		}
		sicilno = sicilno.replaceAll(" ", "");

		if (sicilno.trim().length() != 6) {
			// createGenericMessage("Sicil Numarası Geçersiz!",
			// FacesMessage.SEVERITY_ERROR);
			sicilnoValidator = false;
		} else {
			if (sicilno.charAt(0) == 'G' || sicilno.charAt(0) == 'Y') {
				for (int i = 1; i < 6; i++) {
					for (int j = 0; j <= 9; j++) {
						sicilnochar = sicilno.charAt(i);
						sicilnochar = sicilnochar - 48;
						if (sicilnochar == j) {
							sicilnoValidator = true;
							break;
						} else {
							sicilnoValidator = false;
						}
					}
					if (!sicilnoValidator) {
						// createGenericMessage("Sicil Numarası Geçersiz!",
						// FacesMessage.SEVERITY_ERROR);
						break;
					}

				}
			} else {
				sicilnoValidator = true;
			}
		}
	}

}
