package tr.com.arf.toys.view.controller.form.etkinlik;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.framework.db.model._base.BaseEntity;
import tr.com.arf.framework.utility.tool.KeyUtil;
import tr.com.arf.toys.db.enumerated.etkinlik.Uyeturu;
import tr.com.arf.toys.db.filter.etkinlik.EtkinlikkuruluyeFilter;
import tr.com.arf.toys.db.model.etkinlik.Etkinlikkurul;
import tr.com.arf.toys.db.model.etkinlik.Etkinlikkuruluye;
import tr.com.arf.toys.service.application.ApplicationDescriptor;
import tr.com.arf.toys.view.controller.form._base.ToysBaseFilteredController;

@ManagedBean
@ViewScoped
public class EtkinlikkuruluyeController extends ToysBaseFilteredController {

	private static final long serialVersionUID = 6531152165884980748L;

	private Etkinlikkuruluye etkinlikkuruluye;
	private EtkinlikkuruluyeFilter etkinlikkuruluyeFilter = new EtkinlikkuruluyeFilter();
	protected int i = 0;

	public EtkinlikkuruluyeController() {
		super(Etkinlikkuruluye.class);
		setDefaultValues(false);
		setOrderField("etkinlikkurulRef.rID");
		setLoggable(false); 
		setAutoCompleteSearchColumns(new String[] { "etkinlikkurulRef" });
		setTable(null);
		if (getMasterEntity() != null) {
			setMasterObjectName(ApplicationDescriptor._ETKINLIKKURUL_MODEL);
			setMasterOutcome(ApplicationDescriptor._ETKINLIKKURUL_MODEL);
			getEtkinlikkuruluyeFilter().setEtkinlikkurulRef(getMasterEntity());
			getEtkinlikkuruluyeFilter().createQueryCriterias();
		}
		queryAction();
	}

	public EtkinlikkuruluyeController(Object object) { 
		super(Etkinlikkuruluye.class);   
	}
	
	public Etkinlikkurul getMasterEntity() {
		if (getEtkinlikkuruluyeFilter().getEtkinlikkurulRef() != null) {
			return getEtkinlikkuruluyeFilter().getEtkinlikkurulRef();
		} else {
			return (Etkinlikkurul) getObjectFromSessionFilter(ApplicationDescriptor._ETKINLIKKURUL_MODEL);
		}
	}

	public Etkinlikkuruluye getEtkinlikkuruluye() {
		etkinlikkuruluye = (Etkinlikkuruluye) getEntity();
		return etkinlikkuruluye;
	}

	public void setEtkinlikkuruluye(Etkinlikkuruluye etkinlikkuruluye) {
		this.etkinlikkuruluye = etkinlikkuruluye;
	}

	public EtkinlikkuruluyeFilter getEtkinlikkuruluyeFilter() {
		return etkinlikkuruluyeFilter;
	}

	public void setEtkinlikkuruluyeFilter(
			EtkinlikkuruluyeFilter etkinlikkuruluyeFilter) {
		this.etkinlikkuruluyeFilter = etkinlikkuruluyeFilter;
	}

	@Override
	public BaseFilter getFilter() {
		return getEtkinlikkuruluyeFilter();
	}

	@Override
	public void resetFilter() {
		super.resetFilterOnly();
		if (getMasterEntity() != null) {
			getEtkinlikkuruluyeFilter().setEtkinlikkurulRef(getMasterEntity());
		}
		queryAction();
	}

	@Override
	public void insert() {
		super.insert();
		getEtkinlikkuruluye().setEtkinlikkurulRef(
				getEtkinlikkuruluyeFilter().getEtkinlikkurulRef());
	}

	@Override
	public void save() {
		boolean flag = true;
		if (((Etkinlikkuruluye) getEntity()).getUyeturu() == Uyeturu._BASKAN) {
			List<BaseEntity> list = getList();

			for (BaseEntity b : list) {
				if (((Etkinlikkuruluye) b).getUyeturu().equals(Uyeturu._BASKAN)) {
					flag = false;
					createGenericMessage(
							KeyUtil.getMessageValue("etkinlikkuruluuye.kuruldayalnizbirbaskanolabilir"),
							FacesMessage.SEVERITY_ERROR);
					break;
				}
			}
			if (flag) {
				super.save();
			}

		} else {
			super.save();
		}
	}
 

} // class 
