package tr.com.arf.toys.view.controller.form.system;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.toys.db.filter.system.KoyFilter;
import tr.com.arf.toys.db.model.system.Ilce;
import tr.com.arf.toys.db.model.system.Koy;
import tr.com.arf.toys.service.application.ApplicationDescriptor;
import tr.com.arf.toys.view.controller.form._base.ToysFilteredControllerNoPaging;

@ManagedBean
@ViewScoped
public class KoyController extends ToysFilteredControllerNoPaging {

	private static final long serialVersionUID = -5971656428587612536L;

	private Koy koy; 
	private KoyFilter koyFilter = new KoyFilter();

	public KoyController() {
		super(Koy.class);
		setDefaultValues(false);
		// setPostAddOutcome(getListOutcome());
		setOrderField("ad");
		if(getMasterEntity() != null){
			setMasterObjectName(ApplicationDescriptor._ILCE_MODEL);
			setMasterOutcome(ApplicationDescriptor._ILCE_OUTCOME);
			getKoyFilter().setIlceRef(getMasterEntity());
			getKoyFilter().createQueryCriterias();
		}
		queryAction();
	}

	public Ilce getMasterEntity() {
		if (getKoyFilter().getIlceRef() != null) {
			return getKoyFilter().getIlceRef();
		} else {
			return (Ilce) getObjectFromSessionFilter(ApplicationDescriptor._ILCE_MODEL);
		}
	}

	public Koy getKoy() {
		koy = (Koy) getEntity();
		return koy;
	}

	public void setKoy(Koy koy) {
		this.koy = koy;
	}

	public KoyFilter getKoyFilter() {
		return koyFilter;
	}

	public void setKoyFilter(KoyFilter koyFilter) {
		this.koyFilter = koyFilter;
	}

	@Override
	public BaseFilter getFilter() {
		return getKoyFilter();
	}   

	@Override
	public void insert() {
		super.insert();
		getKoy().setIlceRef(getKoyFilter().getIlceRef());
	}

} // class 
