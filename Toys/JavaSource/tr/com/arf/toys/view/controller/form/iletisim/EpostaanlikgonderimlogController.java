package tr.com.arf.toys.view.controller.form.iletisim;

import java.util.Date;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.event.AjaxBehaviorEvent;

import tr.com.arf.framework.db.enumerated._common.EvetHayir;
import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.framework.db.model._base.BaseEntity;
import tr.com.arf.framework.utility.tool.KeyUtil;
import tr.com.arf.toys.db.enumerated.iletisim.GonderimDurum;
import tr.com.arf.toys.db.filter.iletisim.EpostaanlikgonderimlogFilter;
import tr.com.arf.toys.db.model.iletisim.Epostaanlikgonderimlog;
import tr.com.arf.toys.db.model.kisi.Kisi;
import tr.com.arf.toys.db.model.uye.Uye;
import tr.com.arf.toys.service.application.ApplicationDescriptor;
import tr.com.arf.toys.utility.tool.SendEmail;
import tr.com.arf.toys.view.controller.form._base.ToysBaseFilteredController;

@ManagedBean
@ViewScoped
public class EpostaanlikgonderimlogController extends ToysBaseFilteredController {

	private static final long serialVersionUID = 6531152165884980748L;

	private Epostaanlikgonderimlog epostaanlikgonderimlog;
	private EpostaanlikgonderimlogFilter epostaanlikgonderimlogFilter = new EpostaanlikgonderimlogFilter();
	public EvetHayir icerikOrEditor;

	public EpostaanlikgonderimlogController() {
		super(Epostaanlikgonderimlog.class);
		setDefaultValues(false);
		setOrderField("aciklama");
		setLoggable(false);
		setAutoCompleteSearchColumns(new String[] { "aciklama" });
		setTable(null);
		if (getMasterEntity() != null) {
			if (getMasterEntity().getClass() == Kisi.class) {
				setMasterObjectName(ApplicationDescriptor._KISI_MODEL);
				getEpostaanlikgonderimlogFilter().setKisiRef((Kisi) getMasterEntity());
				setMasterOutcome(ApplicationDescriptor._KISI_MODEL);
			} else if (getMasterEntity().getClass() == Uye.class) {
				setMasterObjectName(ApplicationDescriptor._UYE_MODEL);
				getEpostaanlikgonderimlogFilter().setKisiRef(((Uye) getMasterEntity()).getKisiRef());
				setMasterOutcome(ApplicationDescriptor._UYE_MODEL);
			}
		}
		getEpostaanlikgonderimlogFilter().createQueryCriterias();
		queryAction();
	}

	public BaseEntity getMasterEntity() {
		if (getObjectFromSessionFilter(ApplicationDescriptor._KISI_MODEL) != null) {
			return (Kisi) getObjectFromSessionFilter(ApplicationDescriptor._KISI_MODEL);
		} else if (getObjectFromSessionFilter(ApplicationDescriptor._UYE_MODEL) != null) {
			return (Uye) getObjectFromSessionFilter(ApplicationDescriptor._UYE_MODEL);
		} else {
			return null;
		}
	}

	@Override
	@PostConstruct
	public void init(){
		super.init();
		if (getMasterEntity() != null) {
			 if (getMasterEntity() instanceof Uye) {
				insert();
			}
		}
	}

	public Epostaanlikgonderimlog getEpostaanlikgonderimlog() {
		epostaanlikgonderimlog = (Epostaanlikgonderimlog) getEntity();
		return epostaanlikgonderimlog;
	}

	public void setEpostaanlikgonderimlog(Epostaanlikgonderimlog epostaanlikgonderimlog) {
		this.epostaanlikgonderimlog = epostaanlikgonderimlog;
	}

	public EpostaanlikgonderimlogFilter getEpostaanlikgonderimlogFilter() {
		return epostaanlikgonderimlogFilter;
	}

	public void setEpostaanlikgonderimlogFilter(EpostaanlikgonderimlogFilter epostaanlikgonderimlogFilter) {
		this.epostaanlikgonderimlogFilter = epostaanlikgonderimlogFilter;
	}

	public EvetHayir getIcerikOrEditor() {
		return icerikOrEditor;
	}

	public void setIcerikOrEditor(EvetHayir icerikOrEditor) {
		this.icerikOrEditor = icerikOrEditor;
	}

	private Boolean onizlemeDialog = false;

	public Boolean getOnizlemeDialog() {
		return onizlemeDialog;
	}

	public void setOnizlemeDialog(Boolean onizlemeDialog) {
		this.onizlemeDialog = onizlemeDialog;
	}

	@Override
	public void setSelectedRID(String selectedRID) {
		super.setSelectedRID(selectedRID);
		setOnizlemeDialog(true);
	}

	public void onizlemedialogkapat() {
		setOnizlemeDialog(false);
	}

	public boolean handleIcerikOrEditor(AjaxBehaviorEvent event1) {
		try {
			HtmlSelectOneMenu menuForIcerikOrEditor = (HtmlSelectOneMenu) event1.getComponent();
			if (menuForIcerikOrEditor.getValue() instanceof EvetHayir) {
				if (getIcerikOrEditor().equals(EvetHayir._EVET)) {
					return true;
				} else {
					return false;
				}

			} else {
				return false;
			}
		} catch (Exception e) {
			logYaz("Exception @" + getModelName() + "Controller :", e);
		}
		return true;
	}

	@Override
	public BaseFilter getFilter() {
		return getEpostaanlikgonderimlogFilter();
	}

	@Override
	public void save() {
		if (getMasterEntity() == null) {
			createGenericMessage(KeyUtil.getMessageValue("eposta.gonderenYok"), FacesMessage.SEVERITY_ERROR);
			return;
		} else {
			if (getMasterEntity() instanceof Uye) {
				this.epostaanlikgonderimlog.setKisiRef(((Uye) getMasterEntity()).getKisiRef());
			} else if (getMasterEntity() instanceof Uye) {
				this.epostaanlikgonderimlog.setKisiRef((Kisi) getMasterEntity());
			}
		}
		Date d = new Date();
		this.epostaanlikgonderimlog.setTarih(d);
		this.epostaanlikgonderimlog.setDurum(GonderimDurum._ILETILDI);
		this.epostaanlikgonderimlog.setGonderenRef(getSessionUser().getKullanici().getKisiRef());
		SendEmail email = new SendEmail();
		if (epostaanlikgonderimlog.getKisiRef().getEposta() != null && !isEmpty(epostaanlikgonderimlog.getKisiRef().getEposta())) {
			if (this.epostaanlikgonderimlog.getIcerikRef() != null) {
				if (email.sendEmailToRecipent(epostaanlikgonderimlog.getKisiRef().getEposta(), "TOYS", this.epostaanlikgonderimlog.getIcerikRef().getIcerik())) {
					createGenericMessage(KeyUtil.getMessageValue("eposta.gonderildi"), FacesMessage.SEVERITY_INFO);
					this.epostaanlikgonderimlog.setDurum(GonderimDurum._ILETILDI);
				} else {
					createGenericMessage(KeyUtil.getMessageValue("eposta.gonderilemedi"), FacesMessage.SEVERITY_ERROR);
					this.epostaanlikgonderimlog.setDurum(GonderimDurum._ILETILMEDI);
				}
			} else if (this.epostaanlikgonderimlog.getAciklama() != null) {
				if (email.sendEmailToRecipent(epostaanlikgonderimlog.getKisiRef().getEposta(), "TOYS", this.epostaanlikgonderimlog.getAciklama())) {
					createGenericMessage(KeyUtil.getMessageValue("eposta.gonderildi"), FacesMessage.SEVERITY_INFO);
					this.epostaanlikgonderimlog.setDurum(GonderimDurum._ILETILDI);
				} else {
					createGenericMessage(KeyUtil.getMessageValue("eposta.gonderilemedi"), FacesMessage.SEVERITY_ERROR);
					this.epostaanlikgonderimlog.setDurum(GonderimDurum._ILETILMEDI);
				}
			}
		}
		super.save();
	}
} // class
