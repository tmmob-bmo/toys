package tr.com.arf.toys.view.controller.form.system; 
  
 
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.model.SelectItem;
import javax.persistence.EntityManager;
import javax.persistence.Query;

import tr.com.arf.framework.db._management.DBOperator;
import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.framework.utility.tool.KeyUtil;
import tr.com.arf.toys.db.filter.system.KullaniciRoleFilter;
import tr.com.arf.toys.db.model.system.IslemKullanici;
import tr.com.arf.toys.db.model.system.IslemRole;
import tr.com.arf.toys.db.model.system.Kullanici;
import tr.com.arf.toys.db.model.system.KullaniciRole;
import tr.com.arf.toys.db.model.system.Role;
import tr.com.arf.toys.service.application.ApplicationDescriptor;
import tr.com.arf.toys.service.application.ManagedBeanLocator;
import tr.com.arf.toys.view.controller.form._base.ToysFilteredControllerNoPaging;
  
  
@ManagedBean 
@ViewScoped 
public class KullaniciRoleController extends ToysFilteredControllerNoPaging {  
  
  private static final long serialVersionUID = 6531152165884980748L; 
  
	private KullaniciRole kullaniciRole;  
	private KullaniciRoleFilter kullaniciRoleFilter = new KullaniciRoleFilter();  
  
	public KullaniciRoleController() { 
		super(KullaniciRole.class);  
		setDefaultValues(false);  
		setOrderField("roleRef.rID");  
		setAutoCompleteSearchColumns(new String[]{"roleRef"}); 
		setTable(null); 
		if(getMasterEntity() != null){
			setMasterObjectName(ApplicationDescriptor._KULLANICI_MODEL);
			setMasterOutcome(ApplicationDescriptor._KULLANICI_MODEL);
			getKullaniciRoleFilter().setKullaniciRef(getMasterEntity());
			getKullaniciRoleFilter().createQueryCriterias();
		}
		queryAction(); 
	} 
	
	public Kullanici getMasterEntity(){ 
		return (Kullanici) getObjectFromSessionFilter(ApplicationDescriptor._KULLANICI_MODEL); 
	}
	
	public KullaniciRole getKullaniciRole() { 
		kullaniciRole = (KullaniciRole) getEntity(); 
		return kullaniciRole; 
	} 
	
	public void setKullaniciRole(KullaniciRole kullaniciRole) { 
		this.kullaniciRole = kullaniciRole; 
	} 
	
	public KullaniciRoleFilter getKullaniciRoleFilter() { 
		return kullaniciRoleFilter; 
	} 
	 
	public void setKullaniciRoleFilter(KullaniciRoleFilter kullaniciRoleFilter) {  
		this.kullaniciRoleFilter = kullaniciRoleFilter;  
	}  
	 
	@Override  
	public BaseFilter getFilter() {  
		return getKullaniciRoleFilter();  
	}  	 
	
	@Override	
	public void insert() {	
		super.insert();	
		getKullaniciRole().setKullaniciRef(getKullaniciRoleFilter().getKullaniciRef());	
	}	
	
	@SuppressWarnings("unchecked")
	@Override 
	public void save() { 
		DBOperator dbOperator = getDBOperator();
		getKullaniciRole().setKullaniciRef(getKullaniciRoleFilter().getKullaniciRef());
		setTable(null);
		setEditPanelRendered(false); 
		if (getKullaniciRole().getRID() == null) {
			try {
				dbOperator.insert(getKullaniciRole());
				List<IslemRole> islemRoleList = dbOperator.load(ApplicationDescriptor._ISLEMROLE_MODEL,
						"o.roleRef.rID=" + getKullaniciRole().getRoleRef().getRID(), "o.rID"); 
				for(IslemRole islemRole : islemRoleList){
					IslemKullanici islemKullanici = new IslemKullanici();
						islemKullanici.setKullaniciRoleRef(getKullaniciRole());
						islemKullanici.setIslemRef(islemRole.getIslemRef());
						islemKullanici.setListPermission(islemRole.getListPermission());  
						islemKullanici.setUpdatePermission(islemRole.getUpdatePermission());
						islemKullanici.setDeletePermission(islemRole.getDeletePermission()); 
						dbOperator.insert(islemKullanici);
				}
				createGenericMessage(KeyUtil.getMessageValue("kayit.eklendi"), FacesMessage.SEVERITY_INFO); 
				queryAction();
			} catch (Exception e) {
				getKullaniciRole().setRID(null); 
				createGenericMessage(KeyUtil.getMessageValue("islem.hata"), FacesMessage.SEVERITY_ERROR);
			} 
		} else {
			try { 
				if(getKullaniciRole().getValue().hashCode() == getOldValue().hashCode()){
					createGenericMessage(KeyUtil.getMessageValue("islem.degisiklikYok"), FacesMessage.SEVERITY_INFO); 
					return;
				}
				dbOperator.update(getKullaniciRole());
				queryAction();
				createGenericMessage(KeyUtil.getMessageValue("kayit.guncellendi"), FacesMessage.SEVERITY_INFO);  
			} catch (Exception e) {
				createGenericMessage(KeyUtil.getMessageValue("islem.hata"), FacesMessage.SEVERITY_ERROR);
			} 
		} 
	} 
	
	public String islemKullanici() {	
		if (!setSelected()) {
			return "";
		}	 
		putObjectToSessionFilter(ApplicationDescriptor._KULLANICIROLE_MODEL, getKullaniciRole());
		return ManagedBeanLocator.locateMenuController().gotoPage("menu_IslemKullanici");   
		 
	} 
	
	@SuppressWarnings("unchecked")
	public SelectItem[] getSelectItemListForRole(){ 
		List<Role> entityList = getDBOperator().load(Role.class.getSimpleName(), " o.rID NOT IN (Select m.roleRef.rID FROM KullaniciRole m " +
				" WHERE m.kullaniciRef.rID =" + getKullaniciRoleFilter().getKullaniciRef().getRID() + ")", "o.name");
		if (entityList != null) {
			SelectItem[] selectItemList = new SelectItem[entityList.size() + 1];
			selectItemList[0] = new SelectItem(null, "");
			int i = 1;
			for (Role entity : entityList) {
				selectItemList[i++] = new SelectItem(entity, entity.getUIString());
			}
			return selectItemList;
		}
		return new SelectItem[0]; 
	}
	
	@Override
	public void delete(){
		if (!setSelected()) {
			return;
		} 	 
		// setDoDelete(true); 
		try {
			try {  
				EntityManager entityManager = getDBOperator().getEntityManagerFactory().createEntityManager();
				entityManager.getTransaction().begin();
				Query query = entityManager
					.createQuery("DELETE FROM " + IslemKullanici.class.getSimpleName() + " o WHERE o.kullaniciRoleRef.rID = :roleRef ");
				 
				query.setParameter("roleRef", getKullaniciRole().getRID());   
				query.executeUpdate();
				entityManager.getTransaction().commit();  
				entityManager.close(); 	 
			} catch (Exception e) {
				logYaz("HATA :" + e.getMessage()); 
			}
			getDBOperator().delete(super.getEntity());
			queryAction();
			setSelection(null); 
			setTable(null);
			createGenericMessage(KeyUtil.getMessageValue("kayit.silindi"), FacesMessage.SEVERITY_INFO);
			return;
		} catch (Exception e) {
			createGenericMessage(KeyUtil.getMessageValue("islem.hata") + " Mes : " + e.getMessage(), FacesMessage.SEVERITY_ERROR);  
			return;
		}  
	}
	
	
} // class 
