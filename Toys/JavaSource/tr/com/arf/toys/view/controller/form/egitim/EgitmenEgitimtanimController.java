package tr.com.arf.toys.view.controller.form.egitim; 
 
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.toys.db.filter.egitim.EgitmenEgitimtanimFilter;
import tr.com.arf.toys.db.model.egitim.Egitimtanim;
import tr.com.arf.toys.db.model.egitim.EgitmenEgitimtanim;
import tr.com.arf.toys.service.application.ApplicationDescriptor;
import tr.com.arf.toys.view.controller.form._base.ToysBaseFilteredController;
  
  
@ManagedBean 
@ViewScoped 
public class EgitmenEgitimtanimController extends ToysBaseFilteredController {  
  
  private static final long serialVersionUID = 6531152165884980748L; 
  
	private EgitmenEgitimtanim egitmenEgitimtanim;  
	private EgitmenEgitimtanimFilter egitmenEgitimtanimFilter = new EgitmenEgitimtanimFilter();  
  
	public EgitmenEgitimtanimController() { 
		super(EgitmenEgitimtanim.class);  
		setDefaultValues(false);  
		setLoggable(false);  
		setTable(null); 
		if (getMasterEntity() != null) {
			setMasterObjectName(ApplicationDescriptor._EGITIMTANIM_MODEL);
			setMasterOutcome(ApplicationDescriptor._EGITIMTANIM_MODEL);
			getEgitmenEgitimtanimFilter().setEgitimtanimRef(getMasterEntity());
			getEgitmenEgitimtanimFilter().createQueryCriterias();
		}
		queryAction();
	}
	
	public EgitmenEgitimtanimController(Object object) { 
		super(EgitmenEgitimtanim.class);  
		setLoggable(true); 
	}
	
	public Egitimtanim getMasterEntity() {
		if (getEgitmenEgitimtanimFilter().getEgitimtanimRef() != null) {
			return getEgitmenEgitimtanimFilter().getEgitimtanimRef();
		} else {
			return (Egitimtanim) getObjectFromSessionFilter(ApplicationDescriptor._EGITIMTANIM_MODEL);
		}
	}

	public EgitmenEgitimtanim getEgitmenEgitimtanim() {
		egitmenEgitimtanim=(EgitmenEgitimtanim) getEntity();
		return egitmenEgitimtanim;
	}

	public void setEgitmenEgitimtanim(EgitmenEgitimtanim egitmenEgitimtanim) {
		this.egitmenEgitimtanim = egitmenEgitimtanim;
	}

	public EgitmenEgitimtanimFilter getEgitmenEgitimtanimFilter() {
		return egitmenEgitimtanimFilter;
	}

	public void setEgitmenEgitimtanimFilter(
			EgitmenEgitimtanimFilter egitmenEgitimtanimFilter) {
		this.egitmenEgitimtanimFilter = egitmenEgitimtanimFilter;
	}

	@Override  
	public BaseFilter getFilter() {  
		return getEgitmenEgitimtanimFilter(); 
	}  
	
	@Override
		public void save() {
			if (getMasterEntity()!=null){
				egitmenEgitimtanim.setEgitimtanimRef(getMasterEntity());
			}else {
				createGenericMessage("Eğitim Bilgileri Boş Geldiğinden Dolayı Kaydetme İşlemini"
						+ " Gerçekleştiremiyoruz!", FacesMessage.SEVERITY_INFO);
				return ;
			}
			super.save();
			queryAction();
		}
	
} // class 
