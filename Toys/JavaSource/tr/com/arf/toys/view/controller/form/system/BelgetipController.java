package tr.com.arf.toys.view.controller.form.system; 
  
 
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.toys.db.filter.system.BelgetipFilter;
import tr.com.arf.toys.db.model.system.Belgetip;
import tr.com.arf.toys.view.controller.form._base.ToysBaseFilteredController;
  
  
@ManagedBean 
@ViewScoped 
public class BelgetipController extends ToysBaseFilteredController {  
  
  private static final long serialVersionUID = 6531152165884980748L; 
  
	private Belgetip belgetip;  
	private BelgetipFilter belgetipFilter = new BelgetipFilter();  
  
	public BelgetipController() { 
		super(Belgetip.class);  
		setDefaultValues(false);  
		setOrderField("kod");  
		setAutoCompleteSearchColumns(new String[]{"kod"}); 
		setTable(null); 
		queryAction(); 
	} 
	
	
	public Belgetip getBelgetip() { 
		belgetip = (Belgetip) getEntity(); 
		return belgetip; 
	} 
	
	public void setBelgetip(Belgetip belgetip) { 
		this.belgetip = belgetip; 
	} 
	
	public BelgetipFilter getBelgetipFilter() { 
		return belgetipFilter; 
	} 
	 
	public void setBelgetipFilter(BelgetipFilter belgetipFilter) {  
		this.belgetipFilter = belgetipFilter;  
	}  
	 
	@Override  
	public BaseFilter getFilter() {  
		return getBelgetipFilter();  
	}  
	
	
 
	
} // class 
