package tr.com.arf.toys.view.controller.form.uye;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.rmi.RemoteException;
import java.text.MessageFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.model.SelectItem;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.primefaces.component.datatable.DataTable;
import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.UploadedFile;

import com.itextpdf.text.DocumentException;

import tr.com.arf.framework.db.enumerated._common.EvetHayir;
import tr.com.arf.framework.db.enumerated._common.IslemTuru;
import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.framework.db.model._base.BaseEntity;
import tr.com.arf.framework.db.model.sistem.Islemlog;
import tr.com.arf.framework.utility.exception.data.DBException;
import tr.com.arf.framework.utility.security.ScryptPasswordHashing;
import tr.com.arf.framework.utility.tool.BigDecimalUtil;
import tr.com.arf.framework.utility.tool.DateUtil;
import tr.com.arf.framework.utility.tool.KeyUtil;
import tr.com.arf.framework.utility.tool.RandomObjectGenerator;
import tr.com.arf.framework.utility.tool.StringUtil;
import tr.com.arf.toys.db.enumerated.egitim.Cevap;
import tr.com.arf.toys.db.enumerated.egitim.Soru;
import tr.com.arf.toys.db.enumerated.finans.BorcDurum;
import tr.com.arf.toys.db.enumerated.finans.OdemeTuru;
import tr.com.arf.toys.db.enumerated.iletisim.AdresTipi;
import tr.com.arf.toys.db.enumerated.iletisim.AdresTuru;
import tr.com.arf.toys.db.enumerated.iletisim.NetAdresTuru;
import tr.com.arf.toys.db.enumerated.iletisim.TelefonTuru;
import tr.com.arf.toys.db.enumerated.system.KpsTuru;
import tr.com.arf.toys.db.enumerated.uye.Formturu;
import tr.com.arf.toys.db.enumerated.uye.OgrenimTip;
import tr.com.arf.toys.db.enumerated.uye.SiparisDurum;
import tr.com.arf.toys.db.enumerated.uye.UyeAidatDurum;
import tr.com.arf.toys.db.enumerated.uye.UyeBelgeDurum;
import tr.com.arf.toys.db.enumerated.uye.UyeDurum;
import tr.com.arf.toys.db.enumerated.uye.UyeKayitDurum;
import tr.com.arf.toys.db.enumerated.uye.UyeTip;
import tr.com.arf.toys.db.enumerated.uye.UyekurumDurum;
import tr.com.arf.toys.db.filter.uye.UyeFilter;
import tr.com.arf.toys.db.model.egitim.Egitimdegerlendirme;
import tr.com.arf.toys.db.model.egitim.Egitimkatilimci;
import tr.com.arf.toys.db.model.etkinlik.Etkinlikkatilimci;
import tr.com.arf.toys.db.model.etkinlik.Etkinlikkuruluye;
import tr.com.arf.toys.db.model.finans.Borc;
import tr.com.arf.toys.db.model.finans.Odeme;
import tr.com.arf.toys.db.model.iletisim.Adres;
import tr.com.arf.toys.db.model.iletisim.Internetadres;
import tr.com.arf.toys.db.model.iletisim.Telefon;
import tr.com.arf.toys.db.model.kisi.Kisi;
import tr.com.arf.toys.db.model.kisi.Kisifotograf;
import tr.com.arf.toys.db.model.kisi.Kisikimlik;
import tr.com.arf.toys.db.model.kisi.Kisiogrenim;
import tr.com.arf.toys.db.model.kisi.KomisyonDonemUye;
import tr.com.arf.toys.db.model.kisi.KurulDonemUye;
import tr.com.arf.toys.db.model.kisi.Kurum;
import tr.com.arf.toys.db.model.system.Aidat;
import tr.com.arf.toys.db.model.system.Ilce;
import tr.com.arf.toys.db.model.system.Kpslog;
import tr.com.arf.toys.db.model.system.Kullanici;
import tr.com.arf.toys.db.model.system.KurumKisi;
import tr.com.arf.toys.db.model.system.Sehir;
import tr.com.arf.toys.db.model.system.SistemParametre;
import tr.com.arf.toys.db.model.system.Ulke;
import tr.com.arf.toys.db.model.uye.Uye;
import tr.com.arf.toys.db.model.uye.UyeSiparis;
import tr.com.arf.toys.db.model.uye.Uyeabone;
import tr.com.arf.toys.db.model.uye.Uyeaidat;
import tr.com.arf.toys.db.model.uye.Uyebelge;
import tr.com.arf.toys.db.model.uye.Uyebilirkisi;
import tr.com.arf.toys.db.model.uye.Uyeceza;
import tr.com.arf.toys.db.model.uye.Uyedurumdegistirme;
import tr.com.arf.toys.db.model.uye.Uyeform;
import tr.com.arf.toys.db.model.uye.Uyemuafiyet;
import tr.com.arf.toys.db.model.uye.Uyeodul;
import tr.com.arf.toys.db.model.uye.Uyesigorta;
import tr.com.arf.toys.db.model.uye.Uyeuzmanlik;
import tr.com.arf.toys.db.nonEntityModel.PojoAdres;
import tr.com.arf.toys.db.nonEntityModel.Siparis;
import tr.com.arf.toys.db.nonEntityModel.UyeAidatMuafiyet;
import tr.com.arf.toys.service.application.ApplicationDescriptor;
import tr.com.arf.toys.service.application.ChangePasswordValidator;
import tr.com.arf.toys.service.application.KPSService;
import tr.com.arf.toys.service.application.ManagedBeanLocator;
import tr.com.arf.toys.service.export.CustomPdfExport;
import tr.com.arf.toys.utility.application.ApplicationConstant;
import tr.com.arf.toys.utility.export.CustomExcelExport;
import tr.com.arf.toys.utility.tool.SendEmail;
import tr.com.arf.toys.utility.tool.ToysXMLReader;
import tr.com.arf.toys.view.controller._common.SessionUser;
import tr.com.arf.toys.view.controller.form._base.ToysBaseFilteredController;
import tr.com.arf.toys.view.controller.form.egitim.EgitimkatilimciController;
import tr.com.arf.toys.view.controller.form.etkinlik.EtkinlikkatilimciController;
import tr.com.arf.toys.view.controller.form.etkinlik.EtkinlikkuruluyeController;
import tr.com.arf.toys.view.controller.form.finans.BorcController;
import tr.com.arf.toys.view.controller.form.finans.OdemeController;
import tr.com.arf.toys.view.controller.form.iletisim.AdresController;
import tr.com.arf.toys.view.controller.form.iletisim.InternetadresController;
import tr.com.arf.toys.view.controller.form.iletisim.TelefonController;
import tr.com.arf.toys.view.controller.form.kisi.KisifotografController;
import tr.com.arf.toys.view.controller.form.kisi.KisikimlikController;
import tr.com.arf.toys.view.controller.form.kisi.KisiogrenimController;
import tr.com.arf.toys.view.controller.form.kisi.KomisyonDonemUyeController;
import tr.com.arf.toys.view.controller.form.kisi.KurulDonemUyeController;
import tr.com.arf.toys.view.controller.form.system.KurumKisiController;

@ManagedBean
@ViewScoped
public class UyeController extends ToysBaseFilteredController {

	/* Log4j Logger */
	protected static Logger logger = Logger.getLogger(UyeController.class);
	protected static Logger epostaLogger = Logger.getLogger("eposta");

	private static final long serialVersionUID = 6531152165884980748L;

	private Uye uye;
	private UyeFilter uyeFilter = new UyeFilter();
	private String teslimAdresSecimi;
	private String teslimAdresi;

	private Uye uyeDetay;
	private String selectedTab;

	private Siparis siparisSonuc;

	private Adres adres = new Adres();
	private Adres adresev = new Adres();
	private Adres adresisyeri = new Adres();
	private Telefon telefon = new Telefon();
	private Telefon uyetelefon = new Telefon();
	private Internetadres uyeinternetadres = new Internetadres();
	private Internetadres internetadres = new Internetadres();
	private Internetadres uyeweb = new Internetadres();
	private Kisikimlik kisikimlik;
	private DataTable table2 = new DataTable();
	private List<BaseEntity> list2;

	private UyebelgeController uyebelgeController = new UyebelgeController(new Uye());
	private UyeuzmanlikController uyeuzmanlikController = new UyeuzmanlikController(new Uye());
	private UyebilirkisiController uyebilirkisiController = new UyebilirkisiController(new Uye());
	private KisiogrenimController kisiogrenimController = new KisiogrenimController(new Uye());
	private UyecezaController uyecezaController = new UyecezaController(new Uye());
	private UyeodulController uyeodulController = new UyeodulController(new Uye());
	private EtkinlikkuruluyeController etkinlikkuruluyeController = new EtkinlikkuruluyeController(new Uye());
	private UyeaboneController uyeaboneController = new UyeaboneController(new Uye());
	private UyesigortaController uyesigortaController = new UyesigortaController(new Uye());
	private UyeaidatController uyeaidatController = new UyeaidatController(new Uye());
	private UyemuafiyetController uyemuafiyetController = new UyemuafiyetController(new Uye());
	private BorcController borcController = new BorcController(new Uye());
	private KurumKisiController kurumKisiController = new KurumKisiController(new Uye());
	private InternetadresController internetadresController = new InternetadresController(new Uye());
	private AdresController adresController = new AdresController(new Uye());
	private TelefonController telefonController = new TelefonController(new Uye());
	private UyedurumdegistirmeController uyedurumdegistirmeController = new UyedurumdegistirmeController(new Uye());
	private OdemeController uyeOdemeController = new OdemeController(new Uye());
	private OdemeController aidatOdemeController = new OdemeController(new Uye());
	private OdemeController aidatodemeisbankasiController = new OdemeController(new Uye());
	private EgitimkatilimciController egitimkatilimciController = new EgitimkatilimciController(new Uye());
	private EtkinlikkatilimciController etkinlikkatilimciController = new EtkinlikkatilimciController(new Uye());
	private KurulDonemUyeController kuruldonemuyeController = new KurulDonemUyeController(new Uye());
	private KomisyonDonemUyeController komisyondonemuyeController = new KomisyonDonemUyeController(new Uye());
	private KisifotografController kisifotografController = new KisifotografController(new Uye());
	private KisikimlikController kisikimlikController = new KisikimlikController(new Uye());
	private UyeformController uyeformController = new UyeformController(new Uye());

	// Detaylar
	private String detailEditType;

	private Uyebelge uyebelge;
	private Uyeform uyeform;
	private Uyeuzmanlik uyeuzmanlik;
	private Uyebilirkisi uyebilirkisi;
	private Kisiogrenim kisiogrenim;
	private Uyeceza uyeceza;
	private Uyeodul uyeodul;
	private Etkinlikkuruluye etkinlikkuruluye;
	private Uyeabone uyeabone;
	private Uyesigorta uyesigorta;
	private Uyeaidat uyeaidat;
	private Uyemuafiyet uyemuafiyet;
	private Borc uyeborc;
	private Egitimkatilimci egitimkatilimci;
	private Etkinlikkatilimci etkinlikkatilimci;
	private KurulDonemUye kuruldonemuye;
	private KomisyonDonemUye komisyondonemuye;
	private Kisifotograf kisifotograf;
	// private Uye ogrenciToUye;
	private KurumKisi kurumKisi;
	private Kurum previousKurumForUyeKurum;

	private Odeme uyeodeme = new Odeme();
	private Odeme aidatodeme = new Odeme();
	private Odeme aidatodemeisbankasi = new Odeme();

	private String oldValueForUyedurumdegistirme;
	private String oldValueForUyebelge;
	private String oldValueForUyeuzmanlik;
	private String oldValueForUyebilirkisi;
	private String oldValueForKisiogrenim;
	private String oldValueForUyeceza;
	private String oldValueForUyeodul;
	private String oldValueForEtkinlikkuruluye;
	private String oldValueForUyeabone;
	private String oldValueForUyesigorta;
	private String oldValueForUyeaidat;
	private String oldValueForUyemuafiyet;
	private String oldValueForUyeborc;
	private String oldValueForOgrenciToUye;
	private String oldValueForKurumKisi;
	private String oldValueForAdres;
	private String oldValueForInternetadres;
	private String oldValueForInternetadresMetni;
	private String oldValueForTelefon;
	private String oldValueForUyeOdeme;
	private String oldValueForAidatOdeme;
	private String oldValueForAidatOdemeIsbankasi;
	private String oldValueForEgitimKatilimci;
	private String oldValueForEtkinlikKatilimci;
	private String oldValueForKurulDonemUye;
	private String oldValueForKomisyonDonemUye;
	private String oldValueForKisiFotograf;
	private SelectItem[] selectAdresListForUyeKurum = new SelectItem[0];

	private SelectItem[] ilItemListForIsyeriAdres;
	private SelectItem[] ilceItemListForIsyeriAdres;
	private int yilFiltreleme = DateUtil.getYear(new Date());
	private Boolean hashlendimi = false;
	private Boolean adresupdate = false;

	public void uye() {
		setSelectedTab("uye");
	}

	public UyeController() {
		super(Uye.class);
		setDefaultValues(false);
		setOrderField("length(o.sicilno) asc, o.sicilno desc");
		setAutoCompleteSearchColumns(new String[] { "kisiRef.adsoyad", "sicilno" });
		setTable(null);
		setLoggable(true);
		this.uyelikDurumuDegistirRendered = false;
		this.ay = 1;
		this.yil = DateUtil.getYear(new Date());
		setStaticWhereCondition(getSessionUser().createStaticWhereCondition("o."));
		if (getMasterEntity() != null && getMasterEntity().getRID() != null) {
			this.uyeDetay = getMasterEntity();
			// Detay sayfasinda sadece secilen kaydin gorunmesi icin
			this.uyeFilter.setSicilnoEquals(this.uyeDetay.getSicilno());
			// Listeye donuldugunda,sicil numarasina gore arama yapilmasi icin
			this.uyeFilter.setSicilno(this.uyeDetay.getSicilno());
			this.selectedTab = "uye";
			createUyeDetay("uye");
		}
		if (getObjectFromSessionFilter("OgrenciOnly") != null) {
			this.uyeFilter.setUyetip(UyeTip._OGRENCI);
		} else if (getObjectFromSessionFilter("YabanciOnly") != null) {
			this.uyeFilter.setUyetip(UyeTip._YABANCILAR);
		}
		if (getObjectFromSessionFilter(getModelName() + "Filter") != null) {
			if (!isEmpty(((UyeFilter) getObjectFromSessionFilter(getModelName() + "Filter")).getSicilno())) {
				getUyeFilter().setSicilnoEquals(((UyeFilter) getObjectFromSessionFilter(getModelName() + "Filter")).getSicilno());
			}
		}
		queryAction();
		setHashlendimi(false);
	}

	@Override
	@PostConstruct
	public void init() {
		super.init();
		if (getObjectFromSessionFilter("lastActiveTab") != null) {
			System.out.println("createUyeDetay...");
			createUyeDetay(getObjectFromSessionFilter("lastActiveTab").toString());
		}
		if (getObjectFromSessionFilter("detailEditType") != null) {
			/* Uye is bankasi aidat odeme sayfasindan donuste, aidat sayfasinin acilmasi icin yapilan ekleme! */
			createUyeDetay((String) getObjectFromSessionFilter("detailEditType"));
			// System.out.println("detailEditType bulundu :" + (String) getObjectFromSessionFilter("detailEditType"));
		}

	}

	public UyeController(Object object) {
		super(Uye.class);
	}

	public void selectedRIDNull() {
		this.selectedRID = null;
	}

	public Uye getMasterEntity() {
		if (getUye() != null && getUye().getRID() != null) {
			return getUye();
		} else if (getObjectFromSessionFilter(getModelName()) != null) {
			return (Uye) getObjectFromSessionFilter(getModelName());
		} else {
			return null;
		}
	}

	public Uye getUye() {
		uye = (Uye) getEntity();
		return uye;
	}

	public void setUye(Uye uye) {
		this.uye = uye;
	}

	public UyeFilter getUyeFilter() {
		return uyeFilter;
	}

	public void setUyeFilter(UyeFilter uyeFilter) {
		this.uyeFilter = uyeFilter;
	}

	@Override
	public BaseFilter getFilter() {
		return getUyeFilter();
	}

	public void createUyeDetay(String tabSelected) {
		setHashlendimi(false);
		setSelectedTab(tabSelected);
		getSessionUser().setLegalAccess(1);
		uyeBorcAidatHesapla(getUyeDetay());
		uyeBorcHesapla(getUyeDetay());
		setDetailEditType(null);
		if (!tabSelected.equalsIgnoreCase("uye")) {
			logYaz("Üye detay paneli açılıyor :" + tabSelected);
		}
		if (tabSelected.equalsIgnoreCase("uyebelge")) {
			uyeBelgeGuncelle(getUyeDetay());
		} else if (tabSelected.equalsIgnoreCase("uyeuzmanlik")) {
			uyeUzmanlikGuncelle(getUyeDetay());
		} else if (tabSelected.equalsIgnoreCase("uyedashboarduzmanlik")) {
			uyeUzmanlikGuncelle(getUyeDetay());
		} else if (tabSelected.equalsIgnoreCase("kisiogrenim")) {
			kisiOgrenimGuncelle(getUyeDetay());
		} else if (tabSelected.equalsIgnoreCase("uyebilirkisi")) {
			uyeBilirkisiGuncelle(getUyeDetay());
		} else if (tabSelected.equalsIgnoreCase("uyeceza")) {
			uyeOdulveCezaGuncelle(getUyeDetay());
		} else if (tabSelected.equalsIgnoreCase("uyeodul")) {
			uyeOdulveCezaGuncelle(getUyeDetay());
		} else if (tabSelected.equalsIgnoreCase("etkinlikkuruluye")) {
			etkinlikKuruluyeGuncelle(getUyeDetay());
		} else if (tabSelected.equalsIgnoreCase("uyeabone")) {
			uyeAboneGuncelle(getUyeDetay());
		} else if (tabSelected.equalsIgnoreCase("uyesigorta")) {
			uyeSigortaGuncelle(getUyeDetay());
		} else if (tabSelected.equalsIgnoreCase("uyeaidat")) {
			uyeAidatGuncelle(getUyeDetay());
		} else if (tabSelected.equalsIgnoreCase("uyemuafiyet")) {
			uyeMuafiyetGuncelle(getUyeDetay());
		} else if (tabSelected.equalsIgnoreCase("uyeborc")) {
			uyeBorcGuncelle(getUyeDetay());
		} else if (tabSelected.equalsIgnoreCase("kurumkisi")) {
			kurumKisiGuncelle(getUyeDetay());
		} else if (tabSelected.equalsIgnoreCase("uyedurumdegistirme")) {
			uyedurumdegistirmeGuncelle(getUyeDetay());
		} else if (tabSelected.equalsIgnoreCase("uyeodeme")) {
			uyeOdemeGuncelle(getUyeDetay().getKisiRef());
		} else if (tabSelected.equalsIgnoreCase("aidatodeme")) {
			aidatOdemeGuncelle(getUyeDetay().getKisiRef());
		} else if (tabSelected.equalsIgnoreCase("adres") || tabSelected.equalsIgnoreCase("telefon") || tabSelected.equalsIgnoreCase("internetadres")) {
			adresListesiGuncelle(getUyeDetay().getKisiRef());
			telefonListesiGuncelle(getUyeDetay().getKisiRef());
			internetadresListesiGuncelle(getUyeDetay().getKisiRef());
		} else if (tabSelected.equalsIgnoreCase("egitimkatilimci")) {
			egitimKatilimciGuncelle(getUyeDetay());
		} else if (tabSelected.equalsIgnoreCase("etkinlikkatilimci")) {
			etkinlikKatilimciGuncelle(getUyeDetay());
		} else if (tabSelected.equalsIgnoreCase("kuruldonemuye")) {
			kurulDonemUyeGuncelle(getUyeDetay());
		} else if (tabSelected.equalsIgnoreCase("komisyondonemuye")) {
			komisyonDonemUyeGuncelle(getUyeDetay());
		} else if (tabSelected.equalsIgnoreCase("aidatodemeisbankasi")) {
			setHashlendimi(false);
			aidatOdemeIsBankasiGuncelle(getUyeDetay().getKisiRef());
		} else if (tabSelected.equalsIgnoreCase("uyeform")) {
			uyeFormGuncelle(getUyeDetay());
		} else if (tabSelected.equalsIgnoreCase("kisikimlik")) {
			kisiKimlikGuncelle(getUyeDetay().getKisiRef());
			if (!this.kisikimlikController.getList().isEmpty()) {
				this.kisikimlik = (Kisikimlik) this.kisikimlikController.getList().get(0);
			} else {
				this.kisikimlik = new Kisikimlik();
			}
		} else if (tabSelected.equalsIgnoreCase("kisiogrenimbilgileri")) {
			kisiOgrenimGuncelle(getUyeDetay());
			kisiOgrenimBilgileriGuncelle(getUyeDetay());
			try {
				if (getDBOperator().recordCount(Kisiogrenim.class.getSimpleName(), "o.kisiRef.rID=" + getUyeDetay().getKisiRef().getRID() + " AND o.ogrenimtip=" + OgrenimTip._LISANS.getCode()) > 0) {
					this.kisiogrenim = (Kisiogrenim) getDBOperator().load(Kisiogrenim.class.getSimpleName(), "o.kisiRef.rID=" + getUyeDetay().getKisiRef().getRID() + " AND o.ogrenimtip=" + OgrenimTip._LISANS.getCode(), "").get(0);
				} else {
					this.kisiogrenim = new Kisiogrenim();
				}
			} catch (DBException e) {
				logYaz("Exception @" + getModelName() + "Controller :", e);
			}
		} else if (tabSelected.equalsIgnoreCase("adresev")) {
			try {
				if (getDBOperator().recordCount(Adres.class.getSimpleName(),
						"o.kisiRef.rID= " + getUyeDetay().getKisiRef().getRID() + " AND o.adresturu=" + AdresTuru._EVADRESI.getCode() + " AND o.adrestipi=" + AdresTipi._ILILCEMERKEZI.getCode()) > 0) {
					this.adresev = (Adres) getDBOperator()
							.load(Adres.class.getSimpleName(), "o.kisiRef.rID= " + getUyeDetay().getKisiRef().getRID() + " AND o.adresturu=" + AdresTuru._EVADRESI.getCode() + " AND o.adrestipi=" + AdresTipi._ILILCEMERKEZI.getCode(), "")
							.get(0);
					if (this.adresev.getSehirRef() != null) {
						changeIlce(this.adresev.getSehirRef().getRID());
					}
				} else if (getDBOperator().recordCount(Adres.class.getSimpleName(),
						"o.kisiRef.rID= " + getUyeDetay().getKisiRef().getRID() + " AND o.adresturu=" + AdresTuru._EVADRESI.getCode() + " AND o.adrestipi=" + AdresTipi._YURTDISIADRESI.getCode()) > 0) {
					this.adresev = (Adres) getDBOperator()
							.load(Adres.class.getSimpleName(), "o.kisiRef.rID= " + getUyeDetay().getKisiRef().getRID() + " AND o.adresturu=" + AdresTuru._EVADRESI.getCode() + " AND o.adrestipi=" + AdresTipi._YURTDISIADRESI.getCode(), "")
							.get(0);
				} else {
					this.adresev = new Adres();
				}
				if (getDBOperator().recordCount(Adres.class.getSimpleName(), "o.kisiRef.rID= " + getUyeDetay().getKisiRef().getRID() + " AND o.adresturu=" + AdresTuru._ISADRESI.getCode()) > 0) {
					this.adresisyeri = (Adres) getDBOperator().load(Adres.class.getSimpleName(), "o.kisiRef.rID= " + getUyeDetay().getKisiRef().getRID() + " AND o.adresturu=" + AdresTuru._ISADRESI.getCode(), "").get(0);
					if (this.adresisyeri.getSehirRef() != null) {
						changeIlceForUyeIsyeri(this.adresisyeri.getSehirRef().getRID());
					}
				} else {
					this.adresisyeri = new Adres();
				}
				if (getDBOperator().recordCount(Telefon.class.getSimpleName(), "o.kisiRef.rID=" + getUyeDetay().getKisiRef().getRID() + " AND o.telefonturu=" + TelefonTuru._GSM.getCode()) > 0) {
					this.uyetelefon = (Telefon) getDBOperator().load(Telefon.class.getSimpleName(), "o.kisiRef.rID=" + getUyeDetay().getKisiRef().getRID() + " AND o.telefonturu=" + TelefonTuru._GSM.getCode(), "").get(0);
				} else {
					this.uyetelefon = new Telefon();
				}
				if (getDBOperator().recordCount(Internetadres.class.getSimpleName(), "o.kisiRef.rID=" + getUyeDetay().getKisiRef().getRID() + " AND o.netadresturu=" + NetAdresTuru._EPOSTA.getCode()) > 0) {
					this.uyeinternetadres = (Internetadres) getDBOperator().load(Internetadres.class.getSimpleName(), "o.kisiRef.rID=" + getUyeDetay().getKisiRef().getRID() + " AND o.netadresturu=" + NetAdresTuru._EPOSTA.getCode(), "")
							.get(0);
				} else {
					this.uyeinternetadres = new Internetadres();
				}
				if (getDBOperator().recordCount(Internetadres.class.getSimpleName(), "o.kisiRef.rID=" + getUyeDetay().getKisiRef().getRID() + " AND o.netadresturu=" + NetAdresTuru._WEBSAYFASI.getCode()) > 0) {
					this.uyeweb = (Internetadres) getDBOperator().load(Internetadres.class.getSimpleName(), "o.kisiRef.rID=" + getUyeDetay().getKisiRef().getRID() + " AND o.netadresturu=" + NetAdresTuru._WEBSAYFASI.getCode(), "").get(0);
				} else {
					this.uyeweb = new Internetadres();
				}
			} catch (DBException e) {
				logYaz("Exception @" + getModelName() + "Controller :", e);
			}
		} else if (tabSelected.equalsIgnoreCase("uyeaidatbilgisi")) {
			if (getUyeDetay() == null) {
				setUyeDetay(getSessionUser().getUyeRef());
			}
			uyeaidatyilSorgula();
			uyeOdemeGuncelle(getUyeDetay().getKisiRef());
			uyeAidatBilgisiniGuncelle(getUyeDetay());
			setHashlendimi(false);
			setEditPanelRendered(false);
			setUyelikDurumuDegistirRendered(false);
		} else if (tabSelected.equalsIgnoreCase("kisifotograf")) {
			this.kisifotografController.getKisifotografFilter().setKisiRef(getUyeDetay().getKisiRef());
			this.kisifotografController.getKisifotografFilter().createQueryCriterias();
			this.kisifotografController.queryAction();
		} else if (tabSelected.equalsIgnoreCase("uyeetkinlik")) {
			egitimKatilimciGuncelle(getUyeDetay());
			komisyonDonemUyeGuncelle(getUyeDetay());
			etkinlikKatilimciGuncelle(getUyeDetay());
			kurulDonemUyeGuncelle(getUyeDetay());
			etkinlikKuruluyeGuncelle(getUyeDetay());
		}

	}

	private String eskisifre;
	private String yenisifre;
	private String yenisifretekrar;

	public void sifredegistir() {
		ChangePasswordValidator passwordValidator = ChangePasswordValidator.buildValidator();
		RequestContext context = RequestContext.getCurrentInstance();
		try {
			if (eskisifre != null && eskisifre.length() >= 6) {
				if (!ScryptPasswordHashing.check(eskisifre, getSessionUser().getKullanici().getPassword())) {
					createCustomMessage("Şifre Yanlış Girildi!", FacesMessage.SEVERITY_INFO, "detailEditForm:eskipassword");
					return;
				}
			} else {
				createCustomMessage("Şifre 6 karakterden kısa olamaz!", FacesMessage.SEVERITY_INFO, "detailEditForm:eskipassword");
				return;
			}
			String validationMessage = "Kolay tahmin edilebilir bir şifre giriniz. Şifreniz en az 6 karakter, bir harf, bir büyük harf ve bir rakam içermelidir!";
			if (yenisifre != null && yenisifre.length() >= 6 && yenisifretekrar != null && yenisifretekrar.length() >= 6) {
				if (passwordValidator.validatePassword(yenisifre) && passwordValidator.validatePassword(yenisifretekrar)) {
					if (yenisifre.equals(yenisifretekrar)) {
						getSessionUser().getKullanici().setPassword(ScryptPasswordHashing.encrypt(yenisifre));
						getDBOperator().update(getSessionUser().getKullanici());
						context.update("dashboardForm");
						updateDialogAndForm(context);
						this.detailEditType = null;
						createGenericMessage("Şifreniz Değiştirildi!", FacesMessage.SEVERITY_INFO);
					} else {
						createCustomMessage("Şifreler Eşleşmedi!", FacesMessage.SEVERITY_INFO, "detailEditForm:yenipassword");
						createCustomMessage("Şifreler Eşleşmedi!", FacesMessage.SEVERITY_INFO, "detailEditForm:yenipasswordtekrar");
					}
				} else {
					createCustomMessage(validationMessage, FacesMessage.SEVERITY_INFO, "detailEditForm:yenipassword");
				}
			} else {
				createCustomMessage(validationMessage, FacesMessage.SEVERITY_INFO, "detailEditForm:yenipassword");
			}
		} catch (Exception e) {
			logYaz("Exception @" + getModelName() + "Controller :", e);
		}
	}

	public void handleKurumChange(AjaxBehaviorEvent event) {
		try {
			HtmlSelectOneMenu menu = (HtmlSelectOneMenu) event.getComponent();
			if (menu.getValue() instanceof Sehir) {
				changeIlceForUyeIsyeri(((Sehir) menu.getValue()).getRID());
			}
		} catch (Exception e) {
			logYaz("Error @handleChange :" + e.getMessage());
			logYaz("Exception @" + getModelName() + "Controller :", e);
		}
	}

	public void handleUyeDurumChange(AjaxBehaviorEvent event) {
		try {
			HtmlSelectOneMenu menu = (HtmlSelectOneMenu) event.getComponent();
			if (menu.getValue() instanceof UyeDurum) {
				UyeDurum durum = (UyeDurum) menu.getValue();
				getUyedurumdegistirme().setYenidurum(durum);
				if (getUyedurumdegistirme().isSurekliMuafiyet()) {
					// Surekli muafiyetlerde bitis tarihi olmaz...
					getUyedurumdegistirme().setBitisTarih(null);
				}
			}
		} catch (Exception e) {
			logYaz("Error @handleDurumTipChange :" + e.getMessage());
			logYaz("Exception @" + getModelName() + "Controller :", e);
		}
	}

	protected void changeIlForUyeIsyeri(Long selectedRecId) {
		if (selectedRecId.longValue() == 0) {
			setIlItemListForIsyeriAdres(new SelectItem[0]);
		} else {
			@SuppressWarnings("unchecked")
			// List<Sehir> entityList =
			// getDBOperator().load(Sehir.class.getSimpleName(),
			// "o.ulkeRef.rID=" + selectedRecId ,"o.ad");
			List<Sehir> entityList = getDBOperator().load(Sehir.class.getSimpleName(), "", "o.ad");
			if (entityList != null) {
				ilItemListForIsyeriAdres = new SelectItem[entityList.size() + 1];
				ilItemListForIsyeriAdres[0] = new SelectItem(null, "");
				int i = 1;
				for (Sehir entity : entityList) {
					ilItemListForIsyeriAdres[i++] = new SelectItem(entity, entity.getUIString());
				}
			} else {
				setIlItemListForIsyeriAdres(new SelectItem[0]);
			}
		}
	}

	protected void changeIlceForUyeIsyeri(Long selectedRecId) {
		if (selectedRecId.longValue() == 0) {
			setIlceItemListForIsyeriAdres(new SelectItem[0]);
		} else {
			@SuppressWarnings("unchecked")
			List<Ilce> entityList = getDBOperator().load(Ilce.class.getSimpleName(), "o.sehirRef.rID=" + selectedRecId, "o.ad");
			if (entityList != null) {
				ilceItemListForIsyeriAdres = new SelectItem[entityList.size() + 1];
				ilceItemListForIsyeriAdres[0] = new SelectItem(null, "");
				int i = 1;
				for (Ilce entity : entityList) {
					ilceItemListForIsyeriAdres[i++] = new SelectItem(entity, entity.getUIString());
				}
			} else {
				setIlceItemListForIsyeriAdres(new SelectItem[0]);
			}
		}
	}

	public void normalUyeYap() throws DBException, ParseException {
		if (getUyeDetay() == null) {
			return;
		} else {
			this.oldValueForOgrenciToUye = getUyeDetay().getValue();
			getUyeDetay().setSicilno(ManagedBeanLocator.locateSessionController().uyeSicilNoOlustur(getUyeDetay(), false));
			getUyeDetay().setUyetip(UyeTip._CALISAN);
			getUyeDetay().setSinif(null);
			getUyeDetay().setOgrenciNo(null);
			super.justSave(getUyeDetay(), isLoggable(), this.oldValueForOgrenciToUye);
			ManagedBeanLocator.locateSessionController().uyeBorclandir(getUyeDetay());
		}
	}

	public void evadresidegistir() throws DBException {
		if (getAdresev().getRID() != null) {
			getAdresev().setVarsayilan(EvetHayir._EVET);
			getDBOperator().update(getAdresev());
		} else {
			getAdresev().setVarsayilan(EvetHayir._EVET);
			getDBOperator().insert(getAdresev());
		}
		createGenericMessage(KeyUtil.getFrameworkLabel("kayit.guncellendi"), FacesMessage.SEVERITY_INFO);
		this.adresController.queryAction();

	}

	public void isyeriadresdegistir() throws DBException {
		if (getAdresisyeri().getRID() != null) {
			getAdresisyeri().setVarsayilan(EvetHayir._EVET);
			getDBOperator().update(getAdresisyeri());
		} else {
			getAdresisyeri().setVarsayilan(EvetHayir._EVET);
			getDBOperator().insert(getIsyeriadres());
		}
		createGenericMessage(KeyUtil.getFrameworkLabel("kayit.guncellendi"), FacesMessage.SEVERITY_INFO);
		this.adresController.queryAction();

	}

	public void iletisimbilgilerinidegistir() throws DBException {
		Internetadres internetadres1 = (Internetadres) getDBOperator().load(Internetadres.class.getSimpleName(), "o.kisiRef.rID=" + getUyeDetay().getKisiRef().getRID() + " AND o.netadresturu=" + NetAdresTuru._EPOSTA.getCode(), "").get(0);
		String eskiMail = internetadres1.getNetadresmetni();
		String yeniMail = this.uyeinternetadres.getNetadresmetni();
		if (getUyetelefon().getRID() != null) {
			getUyetelefon().setVarsayilan(EvetHayir._EVET);
			getDBOperator().update(getUyetelefon());
		} else {
			getUyetelefon().setVarsayilan(EvetHayir._EVET);
			getUyetelefon().setTelefonturu(TelefonTuru._GSM);
			getDBOperator().insert(getUyetelefon());
		}
		if (getUyeinternetadres().getRID() != null) {
			getUyeinternetadres().setVarsayilan(EvetHayir._EVET);
			getDBOperator().update(getUyeinternetadres());
			if (!eskiMail.equals(yeniMail)) {
				if (yeniMail.length() > 0) {
					logYaz("GUNCELLENDI | " + epostaKisiLog() + eskiMail + " | " + yeniMail);
					epostaLogger.info("GUNCELLENDI | " + epostaKisiLog() + eskiMail + " | " + yeniMail);
				} else {
					logYaz("SILINDI | " + epostaKisiLog() + eskiMail);
					epostaLogger.info("SILINDI | " + epostaKisiLog() + eskiMail);
				}
			}
		} else {
			if (!isEmpty(getUyeinternetadres().getNetadresmetni())) {
				getUyeinternetadres().setVarsayilan(EvetHayir._EVET);
				getDBOperator().insert(getUyeinternetadres());
				logYaz("EKLENDI | " + epostaKisiLog() + yeniMail);
				epostaLogger.info("EKLENDI | " + epostaKisiLog() + yeniMail);
			}
		}
		if (getUyeweb().getRID() != null) {
			getUyeweb().setVarsayilan(EvetHayir._EVET);
			getDBOperator().update(getUyeweb());
		} else {
			if (!isEmpty(getUyeweb().getNetadresmetni())) {
				getUyeweb().setNetadresturu(NetAdresTuru._WEBSAYFASI);
				getUyeweb().setVarsayilan(EvetHayir._EVET);
				getUyeweb().setKisiRef(getUyeDetay().getKisiRef());
				getDBOperator().insert(getUyeweb());
			}

		}
		super.justSave(getUyeDetay(), isLoggable(), getOldValue());
		this.adresController.queryAction();

	}

	public void newDetail(String detailType) {
		setHashlendimi(false);
		detailType = detailType.toLowerCase(Locale.ENGLISH);
		this.detailEditType = detailType;
		RequestContext context = RequestContext.getCurrentInstance();
		context.update("dialogUpdateBox");

		if (detailType.equalsIgnoreCase("uye")) {
			EntityManager entityManager = getDBOperator().getEntityManagerFactory().createEntityManager();
			entityManager.detach(getUyeDetay());
			// this.ogrenciToUye = getUyeDetay(); this.ogrenciToUye.setRID(null);
			entityManager.persist(getUyeDetay());
			entityManager.close();
			// this.ogrenciToUye.setRID(null); this.ogrenciToUye.setSicilno(""); this.ogrenciToUye.setUyetip(UyeTip._CALISAN); this.ogrenciToUye.setSinif(null);
			// this.ogrenciToUye.setOgrenciNo(null);
			this.oldValueForOgrenciToUye = "";
		} else if (detailType.equalsIgnoreCase("uyebelge")) {
			this.uyebelge = new Uyebelge();
			this.oldValueForUyebelge = "";
		} else if (detailType.equalsIgnoreCase("uyedurumdegistirme")) {
			this.uyedurumdegistirme = new Uyedurumdegistirme();
			this.uyedurumdegistirme.setUyeRef(getUyeDetay());
			this.uyedurumdegistirme.setOncekidurum(getUyeDetay().getUyedurum());
			this.oldValueForUyebelge = "";
		} else if (detailType.equalsIgnoreCase("uyeform")) {
			this.uyeform = new Uyeform();
		} else if (detailType.equalsIgnoreCase("uyeuzmanlik")) {
			this.uyeuzmanlik = new Uyeuzmanlik();
			this.oldValueForUyeuzmanlik = "";
		} else if (detailType.equalsIgnoreCase("uyedashboarduzmanlik")) {
			this.uyeuzmanlik = new Uyeuzmanlik();
			this.oldValueForUyeuzmanlik = "";
		} else if (detailType.equalsIgnoreCase("uyebilirkisi")) {
			this.uyebilirkisi = new Uyebilirkisi();
			if (getUyeDetay().getOnaytarih() == null || DateUtil.calculateDateDifference(new Date(), getUyeDetay().getOnaytarih()) < ApplicationConstant._bilirkisilikSureSinirlamasi) {
				createGenericMessage("Üyenin bilirkişi olabilmesi için en az 3 yıllık üyelik gerekmektedir!", FacesMessage.SEVERITY_ERROR);
				context.update("globalMessages");
				this.detailEditType = null;
				return;
			}
			if (getUyeDetay().getUyetip() == UyeTip._YABANCILAR) {
				createGenericMessage("Yabancı üyeler bilirkişi olamazlar!", FacesMessage.SEVERITY_ERROR);
				context.update("globalMessages");
				this.detailEditType = null;
				return;
			}
			this.oldValueForUyebilirkisi = "";
		} else if (detailType.equalsIgnoreCase("kisiogrenim")) {
			this.kisiogrenim = new Kisiogrenim();
			Calendar cal = Calendar.getInstance();
			int year = cal.get(Calendar.YEAR);
			this.kisiogrenim.setMezuniyettarihi(year);
			this.oldValueForKisiogrenim = "";
		} else if (detailType.equalsIgnoreCase("uyeceza")) {
			this.uyeceza = new Uyeceza();
			this.oldValueForUyeceza = "";
		} else if (detailType.equalsIgnoreCase("uyeodul")) {
			this.uyeodul = new Uyeodul();
			this.oldValueForUyeodul = "";
		} else if (detailType.equalsIgnoreCase("etkinlikkuruluye")) {
			this.etkinlikkuruluye = new Etkinlikkuruluye();
			this.oldValueForEtkinlikkuruluye = "";
		} else if (detailType.equalsIgnoreCase("uyeabone")) {
			this.uyeabone = new Uyeabone();
			this.oldValueForUyeabone = "";
		} else if (detailType.equalsIgnoreCase("uyesigorta")) {
			this.uyesigorta = new Uyesigorta();
			uyeBorcAidatHesapla(getUyeDetay());
			if (getUyeAidatBorcLongValue() > 0) {
				createGenericMessage("Üyenin aidat borcu olduğu için sigorta girişi yapamazsınız!", FacesMessage.SEVERITY_ERROR);
				context.update("globalMessages");
				this.detailEditType = null;
				return;
			}
			this.oldValueForUyesigorta = "";
		} else if (detailType.equalsIgnoreCase("uyeaidat")) {
			this.uyeaidat = new Uyeaidat();
			this.oldValueForUyeaidat = "";
		} else if (detailType.equalsIgnoreCase("uyemuafiyet")) {
			this.uyemuafiyet = new Uyemuafiyet();
			this.oldValueForUyemuafiyet = "";
		} else if (detailType.equalsIgnoreCase("uyeborc")) {
			this.uyeborc = new Borc();
			this.oldValueForUyeborc = "";
		} else if (detailType.equalsIgnoreCase("kurumkisi")) {
			this.kurumKisi = new KurumKisi();
			try {
				if (getDBOperator().recordCount(KurumKisi.class.getSimpleName(), "o.kisiRef.rID=" + getUyeDetay().getKisiRef().getRID()) > 0) {
					KurumKisi entity = (KurumKisi) getDBOperator().load(KurumKisi.class.getSimpleName(), "o.kisiRef.rID=" + getUyeDetay().getKisiRef().getRID(), "o.rID DESC").get(0);
					setPreviousKurumForUyeKurum(entity.getKurumRef());
				}
			} catch (DBException e) {
				logYaz("Exception @" + getModelName() + "Controller :", e);
			}
			this.oldValueForKurumKisi = "";
		} else if (detailType.equalsIgnoreCase("adres")) {
			setAdresupdate(true);
			this.adres = new Adres();
			this.adres.setVarsayilan(EvetHayir._EVET);
			this.adres.setAdresturu(AdresTuru._EVADRESI);
			this.oldValueForAdres = "";
		} else if (detailType.equalsIgnoreCase("telefon")) {
			this.telefon = new Telefon();
			this.telefon.setVarsayilan(EvetHayir._HAYIR);
			this.oldValueForTelefon = "";
		} else if (detailType.equalsIgnoreCase("internetadres")) {
			this.internetadres = new Internetadres();
			this.internetadres.setVarsayilan(EvetHayir._HAYIR);
			this.oldValueForInternetadres = "";
		} else if (detailType.equalsIgnoreCase("uyeodeme")) {
			this.uyeodeme = new Odeme();
			this.oldValueForUyeOdeme = "";
			this.uyeodeme.setKisiRef(getUyeDetay().getKisiRef());
			Calendar cal = Calendar.getInstance();
			this.uyeodeme.setTarih(cal.getTime());
		} else if (detailType.equalsIgnoreCase("aidatodeme")) {
			this.aidatodeme = new Odeme();
			this.oldValueForAidatOdeme = "";
			this.aidatodeme.setKisiRef(getUyeDetay().getKisiRef());
			Calendar cal = Calendar.getInstance();
			this.aidatodeme.setTarih(cal.getTime());
		} else if (detailType.equalsIgnoreCase("aidatodemeisbankasi")) {
			this.aidatodemeisbankasi = new Odeme();
			this.oldValueForAidatOdemeIsbankasi = "";
			this.aidatodemeisbankasi.setKisiRef(getUyeDetay().getKisiRef());
			Calendar cal = Calendar.getInstance();
			this.aidatodemeisbankasi.setOdemeTuru(OdemeTuru._AIDAT);
			this.aidatodemeisbankasi.setTarih(cal.getTime());

		} else if (detailType.equalsIgnoreCase("egitimkatilimci")) {
			this.egitimkatilimci = new Egitimkatilimci();
			this.oldValueForEgitimKatilimci = "";
		} else if (detailType.equalsIgnoreCase("etkinlikkatilimci")) {
			this.etkinlikkatilimci = new Etkinlikkatilimci();
			this.oldValueForEtkinlikKatilimci = "";
		} else if (detailType.equalsIgnoreCase("kuruldonemuye")) {
			this.kuruldonemuye = new KurulDonemUye();
			this.oldValueForKurulDonemUye = "";
		} else if (detailType.equalsIgnoreCase("komisyondonemuye")) {
			this.komisyondonemuye = new KomisyonDonemUye();
			this.oldValueForKomisyonDonemUye = "";
		} else if (detailType.equalsIgnoreCase("kisifotograf")) {
			this.kisifotograf = new Kisifotograf();
			this.oldValueForKisiFotograf = "";
		} else if (detailType.equalsIgnoreCase("kisiogrenimbilgileri")) {
			this.kisiogrenim = new Kisiogrenim();
			Calendar cal = Calendar.getInstance();
			int year = cal.get(Calendar.YEAR);
			this.kisiogrenim.setMezuniyettarihi(year);
			this.oldValueForKisiogrenim = "";
		} else if (detailType.equalsIgnoreCase("sifredegistir")) {
			this.eskisifre = null;
			this.yenisifre = null;
			this.yenisifretekrar = null;
		} else if (detailType.equalsIgnoreCase("kartbilgikontrol")) {
			kisiKimlikGuncelle(getUyeDetay().getKisiRef());
			if (!this.kisikimlikController.getList().isEmpty()) {
				this.kisikimlik = (Kisikimlik) this.kisikimlikController.getList().get(0);
			} else {
				this.kisikimlik = new Kisikimlik();
			}
			kisiOgrenimGuncelle(getUyeDetay());
			kisiOgrenimBilgileriGuncelle(getUyeDetay());
			try {
				if (getDBOperator().recordCount(Kisiogrenim.class.getSimpleName(), "o.kisiRef.rID=" + getUyeDetay().getKisiRef().getRID() + " AND o.ogrenimtip=" + OgrenimTip._LISANS.getCode()) > 0) {
					this.kisiogrenim = (Kisiogrenim) getDBOperator().load(Kisiogrenim.class.getSimpleName(), "o.kisiRef.rID=" + getUyeDetay().getKisiRef().getRID() + " AND o.ogrenimtip=" + OgrenimTip._LISANS.getCode(), "").get(0);
				} else {
					this.kisiogrenim = new Kisiogrenim();
				}
			} catch (DBException e) {
				logYaz("Exception @" + getModelName() + "Controller :", e);
			}
		}
	}

	public void updateDetail(String detailType, Object entity) {
		detailType = detailType.toLowerCase(Locale.ENGLISH);
		this.detailEditType = detailType;
		RequestContext context = RequestContext.getCurrentInstance();
		context.update("dialogUpdateBox");

		if (detailType.equalsIgnoreCase("uyebelge")) {
			this.uyebelge = (Uyebelge) entity;
			this.oldValueForUyebelge = this.uyebelge.getValue();
		} else if (detailType.equalsIgnoreCase("uyeuzmanlik")) {
			this.uyeuzmanlik = (Uyeuzmanlik) entity;
			this.oldValueForUyeuzmanlik = this.uyeuzmanlik.getValue();
		} else if (detailType.equalsIgnoreCase("uyedashboarduzmanlik")) {
			this.uyeuzmanlik = (Uyeuzmanlik) entity;
			this.oldValueForUyeuzmanlik = this.uyeuzmanlik.getValue();
		} else if (detailType.equalsIgnoreCase("uyebilirkisi")) {
			this.uyebilirkisi = (Uyebilirkisi) entity;
			this.oldValueForUyebilirkisi = this.uyebilirkisi.getValue();
		} else if (detailType.equalsIgnoreCase("kisiogrenim")) {
			this.kisiogrenim = (Kisiogrenim) entity;
			this.oldValueForKisiogrenim = this.kisiogrenim.getValue();
		} else if (detailType.equalsIgnoreCase("uyeceza")) {
			this.uyeceza = (Uyeceza) entity;
			this.oldValueForUyeceza = this.uyeceza.getValue();
		} else if (detailType.equalsIgnoreCase("uyeodul")) {
			this.uyeodul = (Uyeodul) entity;
			this.oldValueForUyeodul = this.uyeodul.getValue();
		} else if (detailType.equalsIgnoreCase("etkinlikkuruluye")) {
			this.etkinlikkuruluye = (Etkinlikkuruluye) entity;
			this.oldValueForEtkinlikkuruluye = this.etkinlikkuruluye.getValue();
		} else if (detailType.equalsIgnoreCase("uyeabone")) {
			this.uyeabone = (Uyeabone) entity;
			this.oldValueForUyeabone = this.uyeabone.getValue();
		} else if (detailType.equalsIgnoreCase("uyesigorta")) {
			this.uyesigorta = (Uyesigorta) entity;
			this.oldValueForUyesigorta = this.uyesigorta.getValue();
		} else if (detailType.equalsIgnoreCase("uyeaidat")) {
			this.uyeaidat = (Uyeaidat) entity;
			this.oldValueForUyeaidat = this.uyeaidat.getValue();
		} else if (detailType.equalsIgnoreCase("uyemuafiyet")) {
			this.uyemuafiyet = (Uyemuafiyet) entity;
			this.oldValueForUyemuafiyet = this.uyemuafiyet.getValue();
		} else if (detailType.equalsIgnoreCase("uyeborc")) {
			this.uyeborc = (Borc) entity;
			this.oldValueForUyeborc = this.uyeborc.getValue();
		} else if (detailType.equalsIgnoreCase("kurumkisi")) {
			this.kurumKisi = (KurumKisi) entity;
			setPreviousKurumForUyeKurum(this.kurumKisi.getKurumRef());
			this.oldValueForKurumKisi = this.kurumKisi.getValue();
		} else if (detailType.equalsIgnoreCase("adres")) {
			this.adres = (Adres) entity;
			if (this.adres.getSehirRef() != null) {
				changeIlce(this.adres.getSehirRef().getRID());
			}
			this.oldValueForAdres = this.adres.getValue();
		} else if (detailType.equalsIgnoreCase("telefon")) {
			this.telefon = (Telefon) entity;
			this.oldValueForTelefon = this.telefon.getValue();
		} else if (detailType.equalsIgnoreCase("internetadres")) {
			this.internetadres = (Internetadres) entity;
			this.oldValueForInternetadresMetni = this.internetadres.getNetadresmetni();
			this.oldValueForInternetadres = this.internetadres.getValue();
		} else if (detailType.equalsIgnoreCase("uyeodeme")) {
			this.uyeodeme = (Odeme) entity;
			this.oldValueForUyeOdeme = this.uyeodeme.getValue();
		} else if (detailType.equalsIgnoreCase("aidatodeme")) {
			this.aidatodeme = (Odeme) entity;
			this.oldValueForAidatOdeme = this.aidatodeme.getValue();
		} else if (detailType.equalsIgnoreCase("aidatodemeisbankasi")) {
			this.aidatodemeisbankasi = (Odeme) entity;
			this.oldValueForAidatOdemeIsbankasi = this.aidatodemeisbankasi.getValue();
		} else if (detailType.equalsIgnoreCase("egitimkatilimci")) {
			this.egitimkatilimci = (Egitimkatilimci) entity;
			this.oldValueForEgitimKatilimci = this.egitimkatilimci.getValue();
		} else if (detailType.equalsIgnoreCase("etkinlikkatilimci")) {
			this.etkinlikkatilimci = (Etkinlikkatilimci) entity;
			this.oldValueForEtkinlikKatilimci = this.etkinlikkatilimci.getValue();
		} else if (detailType.equalsIgnoreCase("kuruldonemuye")) {
			this.kuruldonemuye = (KurulDonemUye) entity;
			this.oldValueForKurulDonemUye = this.kuruldonemuye.getValue();
		} else if (detailType.equalsIgnoreCase("komisyondonemuye")) {
			this.komisyondonemuye = (KomisyonDonemUye) entity;
			this.oldValueForKomisyonDonemUye = this.komisyondonemuye.getValue();
		} else if (detailType.equalsIgnoreCase("kisifotograf")) {
			this.kisifotograf = (Kisifotograf) entity;
		} else if (detailType.equalsIgnoreCase("kisiogrenimbilgileri")) {
			this.kisiogrenim = (Kisiogrenim) entity;
			this.oldValueForKisiogrenim = this.kisiogrenim.getValue();
		}

	}

	public void openDetail(Object entitySent) {
		if (entitySent != null) {
			putObjectToSessionFilter("logEntity", entitySent);
		}
	}

	public void deleteDetail(String detailType, Object entity) throws DBException {
		this.detailEditType = null;
		detailType = detailType.toLowerCase(Locale.ENGLISH);
		if (detailType.equalsIgnoreCase("uyebelge")) {
			this.uyebelge = (Uyebelge) entity;
			deleteFile(uyebelge.getPath());
			super.justDelete(this.uyebelge, this.uyebelgeController.isLoggable());
			this.uyebelgeController.queryAction();
		} else if (detailType.equalsIgnoreCase("uyeform")) {
			this.uyeform = (Uyeform) entity;
			deleteFile(uyeform.getPath());
			super.justDelete(this.uyeform, this.uyeformController.isLoggable());
			this.uyeformController.queryAction();
		} else if (detailType.equalsIgnoreCase("uyeuzmanlik")) {
			this.uyeuzmanlik = (Uyeuzmanlik) entity;
			super.justDelete(this.uyeuzmanlik, this.uyeuzmanlikController.isLoggable());
			this.uyeuzmanlikController.queryAction();
		} else if (detailType.equalsIgnoreCase("uyedashboarduzmanlik")) {
			this.uyeuzmanlik = (Uyeuzmanlik) entity;
			super.justDelete(this.uyeuzmanlik, this.uyeuzmanlikController.isLoggable());
			this.uyeUzmanlikGuncelle(getUyeDetay());
		} else if (detailType.equalsIgnoreCase("kisiogrenim")) {
			this.kisiogrenim = (Kisiogrenim) entity;
			Long kisiRef = 0L;
			if (getKisiogrenim().getKisiRef() != null) {
				kisiRef = this.kisiogrenim.getKisiRef().getRID();
				if (getKisiogrenim().getOgrenimtip() == OgrenimTip._LISANS) {
					createGenericMessage("Lisans öğrenim bilgilerini silemezsiniz ! ", FacesMessage.SEVERITY_INFO);
					return;
				}
			}
			try {
				super.justDelete(this.kisiogrenim, this.kisiogrenimController.isLoggable());
				setSelection(null);
				setTable(null);
			} catch (Exception e) {
				createGenericMessage(KeyUtil.getMessageValue("islem.hata") + " Mes : " + e.getMessage(), FacesMessage.SEVERITY_ERROR);
				return;
			}

			String whereCondition = "o.varsayilan =" + EvetHayir._EVET.getCode();
			if (kisiRef != 0) {
				whereCondition += " AND o.kisiRef.rID = " + kisiRef;
			}
			try {
				if (getDBOperator().recordCount(Kisiogrenim.class.getSimpleName(), whereCondition) == 0) {
					whereCondition = "o.rID <> " + this.kisiogrenim.getRID() + " AND o.varsayilan =" + EvetHayir._HAYIR.getCode();
					if (kisiRef != 0) {
						whereCondition += " AND o.kisiRef.rID = " + kisiRef;
					}
					if (getDBOperator().recordCount(Kisiogrenim.class.getSimpleName(), whereCondition) > 0) {
						Kisiogrenim kisiogrenim1 = (Kisiogrenim) getDBOperator().load(Kisiogrenim.class.getSimpleName(), whereCondition, "o.rID DESC").get(0);
						if (kisiogrenim1 != null) {
							kisiogrenim1.setVarsayilan(EvetHayir._EVET);
							getDBOperator().update(kisiogrenim1);
						}
					}
				}
			} catch (Exception e) {
				logYaz("Exception @" + getModelName() + "Controller :", e);
			}
			this.kisiogrenimController.queryAction();
		} else if (detailType.equalsIgnoreCase("uyebilirkisi")) {
			this.uyebilirkisi = (Uyebilirkisi) entity;
			super.justDelete(this.uyebilirkisi, this.uyebilirkisiController.isLoggable());
			this.uyebilirkisiController.queryAction();
		} else if (detailType.equalsIgnoreCase("uyeceza")) {
			this.uyeceza = (Uyeceza) entity;
			super.justDelete(this.uyeceza, this.uyecezaController.isLoggable());
			this.uyecezaController.queryAction();
		} else if (detailType.equalsIgnoreCase("uyeodul")) {
			this.uyeodul = (Uyeodul) entity;
			super.justDelete(this.uyeodul, this.uyeodulController.isLoggable());
			this.uyeodulController.queryAction();
		} else if (detailType.equalsIgnoreCase("etkinlikkuruluye")) {
			this.etkinlikkuruluye = (Etkinlikkuruluye) entity;
			super.justDelete(this.etkinlikkuruluye, this.etkinlikkuruluyeController.isLoggable());
			this.etkinlikkuruluyeController.queryAction();
		} else if (detailType.equalsIgnoreCase("uyeabone")) {
			this.uyeabone = (Uyeabone) entity;
			super.justDelete(this.uyeabone, this.uyeaboneController.isLoggable());
			this.uyeaboneController.queryAction();
		} else if (detailType.equalsIgnoreCase("uyesigorta")) {
			this.uyesigorta = (Uyesigorta) entity;
			super.justDelete(this.uyesigorta, this.uyesigortaController.isLoggable());
			this.uyesigortaController.queryAction();
		} else if (detailType.equalsIgnoreCase("uyeaidat")) {
			this.uyeaidat = (Uyeaidat) entity;
			super.justDelete(this.uyeaidat, this.uyeaidatController.isLoggable());
			this.uyeaidatController.queryAction();
		} else if (detailType.equalsIgnoreCase("uyemuafiyet")) {
			this.uyemuafiyet = (Uyemuafiyet) entity;
			List<Uyeaidat> aidatListesi = getUyeMuafiyetEtkilenenAidatListesi(this.uyemuafiyet.getRID());
			// Muafiyet silinmeden once aidat bilgileri guncelleniyor...
			if (!isEmpty(aidatListesi)) {
				for (Uyeaidat aidat : aidatListesi) {
					aidat.setUyemuafiyetRef(null);
					if (aidat.getKalan().longValue() == 0) {
						aidat.setUyeaidatdurum(UyeAidatDurum._ODENMIS);
					} else if (aidat.getKalan().longValue() < 0) {
						aidat.setUyeaidatdurum(UyeAidatDurum._ALACAKLI);
					} else if (aidat.getKalan().longValue() > 0) {
						aidat.setUyeaidatdurum(UyeAidatDurum._BORCLU);
					}
					getDBOperator().update(aidat);
				}
			}
			super.justDelete(this.uyemuafiyet, this.uyemuafiyetController.isLoggable());
			uyeBorcAidatHesapla(getUyeDetay());
			this.uyemuafiyetController.queryAction();
		} else if (detailType.equalsIgnoreCase("uyeborc")) {
			this.uyeborc = (Borc) entity;
			super.justDelete(this.uyeborc, this.borcController.isLoggable());
			this.borcController.queryAction();

		} else if (detailType.equalsIgnoreCase("kurumkisi")) {
			this.kurumKisi = (KurumKisi) entity;
			Long kisiRef = 0L;
			if (getKurumKisi().getKisiRef() != null) {
				kisiRef = this.kurumKisi.getKisiRef().getRID();
			}
			try {
				if (getDBOperator().recordCount(KurumKisi.class.getSimpleName(), "o.kurumRef.rID=" + getKurumKisi().getKurumRef().getRID() + " AND o.kisiRef.rID=" + getKurumKisi().getKisiRef().getRID()) > 0) {
					if (getDBOperator().recordCount(Adres.class.getSimpleName(),
							"o.kisiRef.rID=" + getKurumKisi().getKisiRef().getRID() + " AND o.kurumRef.rID=" + getKurumKisi().getKurumRef().getRID() + " AND o.varsayilan= " + EvetHayir._EVET.getCode()) > 0) {
						Adres eskiAdres = (Adres) getDBOperator().load(Adres.class.getSimpleName(),
								"o.kisiRef.rID=" + getKurumKisi().getKisiRef().getRID() + " AND o.kurumRef.rID=" + getKurumKisi().getKurumRef().getRID() + " AND o.varsayilan= " + EvetHayir._EVET.getCode(), "").get(0);
						getDBOperator().delete(eskiAdres);
					}
					super.justDelete(this.kurumKisi, this.kurumKisiController.isLoggable());
				} else {
					createGenericMessage("Varsayılan İşyeri Adresini Silemezsiniz!", FacesMessage.SEVERITY_INFO);
					return;

				}

				setSelection(null);
				setTable(null);
				createGenericMessage(KeyUtil.getMessageValue("kayit.silindi"), FacesMessage.SEVERITY_INFO);
			} catch (Exception e) {
				createGenericMessage(KeyUtil.getMessageValue("islem.hata") + " Mes : " + e.getMessage(), FacesMessage.SEVERITY_ERROR);
				return;
			}
			String whereCondition = "o.varsayilan =" + EvetHayir._EVET.getCode();
			try {
				if (getDBOperator().recordCount(KurumKisi.class.getSimpleName(), whereCondition) == 0) {
					whereCondition = "o.rID <> " + this.kurumKisi.getRID() + " AND o.varsayilan =" + EvetHayir._HAYIR.getCode();
					if (kisiRef != 0) {
						whereCondition += " AND o.kisiRef.rID = " + kisiRef;
					}
					if (getDBOperator().recordCount(KurumKisi.class.getSimpleName(), whereCondition) > 0) {
						KurumKisi kurumkisi1 = (KurumKisi) getDBOperator().load(KurumKisi.class.getSimpleName(), whereCondition, "o.rID DESC").get(0);
						if (kurumkisi1 != null) {
							kurumkisi1.setVarsayilan(EvetHayir._EVET);
							getDBOperator().update(kurumkisi1);
						}
					}
				}
			} catch (Exception e) {
				logYaz("Exception @" + getModelName() + "Controller :", e);
			}
			this.kurumKisiController.queryAction();

		} else if (detailType.equalsIgnoreCase("adres")) {
			this.adres = (Adres) entity;
			if (this.adres.getVarsayilan() == EvetHayir._EVET) {
				try {
					if (getDBOperator().recordCount(Adres.class.getSimpleName(), "o.rID <> " + this.adres.getRID() + " AND o.kisiRef.rID = " + this.adres.getKisiRef().getRID()) > 0) {
						Adres varsayilanYapilacakAdres = (Adres) getDBOperator().load(Adres.class.getSimpleName(), "o.rID <> " + this.adres.getRID() + " AND o.kisiRef.rID = " + this.adres.getKisiRef().getRID(), "o.rID DESC").get(0);
						varsayilanYapilacakAdres.setVarsayilan(EvetHayir._EVET);
						getDBOperator().update(varsayilanYapilacakAdres);
					}
				} catch (Exception e) {
					logYaz("Exception @" + getModelName() + "Controller :", e);
				}
			}
			super.justDelete(this.adres, this.adresController.isLoggable());
			this.adresController.queryAction();
		} else if (detailType.equalsIgnoreCase("telefon")) {
			this.telefon = (Telefon) entity;
			if (this.telefon.getVarsayilan() == EvetHayir._EVET) {
				if (getDBOperator().recordCount(Telefon.class.getSimpleName(), "o.rID <> " + this.telefon.getRID() + " AND o.kisiRef.rID = " + this.telefon.getKisiRef().getRID()) > 0) {
					Telefon varsayilanYapilacakTelefon = (Telefon) getDBOperator().load(Telefon.class.getSimpleName(), "o.rID <> " + this.telefon.getRID() + " AND o.kisiRef.rID = " + this.telefon.getKisiRef().getRID(), "o.rID DESC").get(0);
					varsayilanYapilacakTelefon.setVarsayilan(EvetHayir._EVET);
					getDBOperator().update(varsayilanYapilacakTelefon);
				} else if (getDBOperator().recordCount(Telefon.class.getSimpleName(), "o.kisiRef.rID = " + this.telefon.getKisiRef().getRID()) == 1) {
					createGenericMessage("Varsayılan telefon numarasını silemezsiniz!", FacesMessage.SEVERITY_INFO);
					return;
				}
			}
			super.justDelete(this.telefon, this.telefonController.isLoggable());
			this.telefonController.queryAction();
		} else if (detailType.equalsIgnoreCase("internetadres")) {
			this.internetadres = (Internetadres) entity;
			if (this.internetadres.getVarsayilan() == EvetHayir._EVET) {
				try {
					if (getDBOperator().recordCount(Internetadres.class.getSimpleName(), "o.rID <> " + this.internetadres.getRID() + " AND o.kisiRef.rID = " + this.internetadres.getKisiRef().getRID()) > 0) {
						Internetadres varsayilanYapilacakInternetadres = (Internetadres) getDBOperator()
								.load(Internetadres.class.getSimpleName(), "o.rID <> " + this.internetadres.getRID() + " AND o.kisiRef.rID = " + this.internetadres.getKisiRef().getRID(), "o.rID DESC").get(0);
						varsayilanYapilacakInternetadres.setVarsayilan(EvetHayir._EVET);
						getDBOperator().update(varsayilanYapilacakInternetadres);
					}
				} catch (Exception e) {
					logYaz("Exception @" + getModelName() + "Controller :", e);
				}
			}
			// todo internet adresinin silindigi kisim
			String silinenMail = this.internetadres.getNetadresmetni();
			logYaz("SILINDI | " + silinenMail);
			epostaLogger.info("SILINDI | " + epostaKisiLog() + silinenMail);
			super.justDelete(this.internetadres, this.internetadresController.isLoggable());
			this.internetadresController.queryAction();
		} else if (detailType.equalsIgnoreCase("uyeodeme")) {
			this.uyeodeme = (Odeme) entity;
			super.justDelete(this.uyeodeme, this.uyeOdemeController.isLoggable());
			this.uyeOdemeController.queryAction();
		} else if (detailType.equalsIgnoreCase("aidatodeme")) {
			this.aidatodeme = (Odeme) entity;
			super.justDelete(this.aidatodeme, this.aidatOdemeController.isLoggable());
			this.aidatOdemeController.queryAction();
		} else if (detailType.equalsIgnoreCase("aidatodemeisbankasi")) {
			this.aidatodemeisbankasi = (Odeme) entity;
			super.justDelete(this.aidatodemeisbankasi, this.aidatOdemeController.isLoggable());
			this.aidatodemeisbankasiController.queryAction();
		} else if (detailType.equalsIgnoreCase("kisifotograf")) {
			this.kisifotograf = (Kisifotograf) entity;
			deleteFile(kisifotograf.getPath());
			super.justDelete(this.kisifotograf, this.kisifotografController.isLoggable());
			this.kisifotografController.queryAction();
		} else if (detailType.equalsIgnoreCase("kisiogrenimbilgileri")) {
			this.kisiogrenim = (Kisiogrenim) entity;
			Long kisiRef = 0L;
			if (getKisiogrenim().getKisiRef() != null) {
				kisiRef = this.kisiogrenim.getKisiRef().getRID();
				if (getKisiogrenim().getOgrenimtip() == OgrenimTip._LISANS) {
					createGenericMessage("Lisans öğrenim bilgilerini silemezsiniz ! ", FacesMessage.SEVERITY_INFO);
					return;
				}
			}
			try {
				super.justDelete(this.kisiogrenim, this.kisiogrenimController.isLoggable());
				setSelection(null);
				setTable(null);
				kisiOgrenimGuncelle(getUyeDetay());
			} catch (Exception e) {
				createGenericMessage(KeyUtil.getMessageValue("islem.hata") + " Mes : " + e.getMessage(), FacesMessage.SEVERITY_ERROR);
				return;
			}

			String whereCondition = "o.varsayilan =" + EvetHayir._EVET.getCode();
			if (kisiRef != 0) {
				whereCondition += " AND o.kisiRef.rID = " + kisiRef;
			}
			try {
				if (getDBOperator().recordCount(Kisiogrenim.class.getSimpleName(), whereCondition) == 0) {
					whereCondition = "o.rID <> " + this.kisiogrenim.getRID() + " AND o.varsayilan =" + EvetHayir._HAYIR.getCode();
					if (kisiRef != 0) {
						whereCondition += " AND o.kisiRef.rID = " + kisiRef;
					}
					if (getDBOperator().recordCount(Kisiogrenim.class.getSimpleName(), whereCondition) > 0) {
						Kisiogrenim kisiogrenim1 = (Kisiogrenim) getDBOperator().load(Kisiogrenim.class.getSimpleName(), whereCondition, "o.rID DESC").get(0);
						if (kisiogrenim1 != null) {
							kisiogrenim1.setVarsayilan(EvetHayir._EVET);
							getDBOperator().update(kisiogrenim1);
						}
					}
				}
			} catch (Exception e) {
				logYaz("Exception @" + getModelName() + "Controller :", e);
			}
			this.kisiogrenimController.queryAction();
		}
	}

	private boolean duplicateCheckForKisi(String tableName, String whereCon) {
		try {
			logger.debug(tableName + "o.kisiRef.rID=" + this.uyeDetay.getKisiRef().getRID() + " AND " + whereCon);
			if (getDBOperator().recordCount(tableName, "o.kisiRef.rID=" + this.uyeDetay.getKisiRef().getRID() + " AND " + whereCon) > 0) {
				return false;
			} else {
				return true;
			}
		} catch (DBException e) {
			logYaz("@duplicateCheck", e);
			return false;
		}
	}

	@SuppressWarnings("static-access")
	public void saveDetails() throws Exception {
		RequestContext context = RequestContext.getCurrentInstance();
		logger.debug("saveDetails :" + this.detailEditType);
		if (this.detailEditType.equalsIgnoreCase("uyebelge")) {
			if (this.uyebelge.getBelgetipRef() != null && this.uyebelge.getBasvurutarih() != null) {
				this.uyebelge.setUyeRef(getUyeDetay());
				if (this.uyebelge.getTeslimtarih() != null) {
					this.uyebelge.setUyebelgedurum(UyeBelgeDurum._TESLIMEDILMIS);
				} else if (this.uyebelge.getHazirlanmatarih() != null) {
					this.uyebelge.setUyebelgedurum(UyeBelgeDurum._HAZIRLANMIS);
				}
				super.justSave(this.uyebelge, this.uyebelgeController.isLoggable(), this.oldValueForUyebelge);
				reloadPage();
			}
		} else if (this.detailEditType.equalsIgnoreCase("uyeform")) {
			this.uyeform.setUyeRef(getUyeDetay());
			super.justSave(getUyeform(), this.uyeformController.isLoggable(), "");
			reloadPage();
		} else if (this.detailEditType.equalsIgnoreCase("uyeuzmanlik")) {
			if (duplicateCheckForKisi(Uyeuzmanlik.class.getSimpleName(), "o.uzmanliktipRef.rID =" + this.uyeuzmanlik.getUzmanliktipRef().getRID())) {
				this.uyeuzmanlik.setKisiRef(getUyeDetay().getKisiRef());
				super.justSave(this.uyeuzmanlik, this.uyeuzmanlikController.isLoggable(), this.oldValueForUyeuzmanlik);
				reloadPage();
			} else {
				createCustomMessage("Üye için bu uzmanlık zaten tanımlanmış.", FacesMessage.SEVERITY_ERROR, "detailEditForm:uzmanliktipRefDropDown:uzmanliktipRef");
				return;
			}
		} else if (this.detailEditType.equalsIgnoreCase("uyedashboarduzmanlik")) {
			this.uyeuzmanlik.setKisiRef(getUyeDetay().getKisiRef());
			if (duplicateCheckForKisi(Uyeuzmanlik.class.getSimpleName(), "o.uzmanliktipRef.rID =" + this.uyeuzmanlik.getUzmanliktipRef().getRID())) {
				super.justSave(this.uyeuzmanlik, this.uyeuzmanlikController.isLoggable(), this.oldValueForUyeuzmanlik);
				this.uyeUzmanlikGuncelle(getUyeDetay());
				reloadUyeDashboard();
			} else {
				createCustomMessage("Üye için bu uzmanlık zaten tanımlanmış.", FacesMessage.SEVERITY_ERROR, "detailEditForm:uzmanliktipRefDropDown:uzmanliktipRef");
				return;
			}
		} else if (this.detailEditType.equalsIgnoreCase("uyebilirkisi")) {
			this.uyebilirkisi.setUyeRef(getUyeDetay());
			super.justSave(this.uyebilirkisi, this.uyebilirkisiController.isLoggable(), this.oldValueForUyebilirkisi);
			reloadPage();
		} else if (this.detailEditType.equalsIgnoreCase("kisiogrenim")) {
			this.kisiogrenim.setKisiRef(getUyeDetay().getKisiRef());
			super.justSave(this.kisiogrenim, this.kisiogrenimController.isLoggable(), this.oldValueForKisiogrenim);
			if (this.kisiogrenim.getVarsayilan() == EvetHayir._EVET) {
				String whereCondition = "o.rID <> " + this.kisiogrenim.getRID() + " AND o.varsayilan =" + EvetHayir._EVET.getCode();
				if (this.kisiogrenim.getKisiRef() != null) {
					whereCondition += " AND o.kisiRef.rID = " + this.kisiogrenim.getKisiRef().getRID();
				}
				try {
					if (getDBOperator().recordCount(Kisiogrenim.class.getSimpleName(), whereCondition) > 0) {
						for (Object obj : getDBOperator().load(Kisiogrenim.class.getSimpleName(), whereCondition, "o.rID")) {
							Kisiogrenim kisiogrenim = (Kisiogrenim) obj;
							kisiogrenim.setVarsayilan(EvetHayir._HAYIR);
							getDBOperator().update(kisiogrenim);
						}
					}
				} catch (Exception e) {
					logYaz("Exception @" + getModelName() + "Controller :", e);
				}
			} else if (this.kisiogrenim.getVarsayilan() == EvetHayir._HAYIR) {
				String whereCondition = "o.rID <> " + this.kisiogrenim.getRID() + " AND o.varsayilan =" + EvetHayir._HAYIR.getCode();
				if (this.kisiogrenim.getKisiRef() != null) {
					whereCondition += " AND o.kisiRef.rID = " + this.kisiogrenim.getKisiRef().getRID();
				}
				String whereCondition1 = "o.rID <> " + this.kisiogrenim.getRID() + " AND o.varsayilan =" + EvetHayir._EVET.getCode();
				if (this.kisiogrenim.getKisiRef() != null) {
					whereCondition += " AND o.kisiRef.rID = " + this.kisiogrenim.getKisiRef().getRID();
				}
				try {
					if (getDBOperator().recordCount(Kisiogrenim.class.getSimpleName(), whereCondition) == 0 && getDBOperator().recordCount(Kisiogrenim.class.getSimpleName(), whereCondition1) == 0) {
						kisiogrenim.setVarsayilan(EvetHayir._EVET);
						getDBOperator().update(kisiogrenim);
					}
					whereCondition = "o.rID <> " + this.kisiogrenim.getRID() + " AND o.varsayilan =" + EvetHayir._EVET.getCode();
					if (this.kisiogrenim.getKisiRef() != null) {
						whereCondition += " AND o.kisiRef.rID = " + this.kisiogrenim.getKisiRef().getRID();
					}
					if (getDBOperator().recordCount(Kisiogrenim.class.getSimpleName(), whereCondition) == 0) {
						createGenericMessage(KeyUtil.getMessageValue("varsayilan.hata"), FacesMessage.SEVERITY_INFO);
						kisiogrenim.setVarsayilan(EvetHayir._EVET);
						getDBOperator().update(kisiogrenim);
					}

				} catch (Exception e) {
					logYaz("Exception @" + getModelName() + "Controller :", e);
				}
			}
			reloadPage();
		} else if (this.detailEditType.equalsIgnoreCase("uyeceza")) {
			this.uyeceza.setUyeRef(getUyeDetay());
			super.justSave(this.uyeceza, this.uyecezaController.isLoggable(), this.oldValueForUyeceza);
			reloadPage();
		} else if (this.detailEditType.equalsIgnoreCase("uyeodul")) {
			this.uyeodul.setUyeRef(getUyeDetay());
			super.justSave(this.uyeodul, this.uyeodulController.isLoggable(), this.oldValueForUyeodul);
			reloadPage();
		} else if (this.detailEditType.equalsIgnoreCase("etkinlikkuruluye")) {
			this.etkinlikkuruluye.setUyeRef(getUyeDetay());
			super.justSave(this.etkinlikkuruluye, this.etkinlikkuruluyeController.isLoggable(), this.oldValueForEtkinlikkuruluye);
			reloadPage();
		} else if (this.detailEditType.equalsIgnoreCase("uyeabone")) {
			this.uyeabone.setUyeRef(getUyeDetay());
			super.justSave(this.uyeabone, this.uyeaboneController.isLoggable(), this.oldValueForUyeabone);
			reloadPage();
		} else if (this.detailEditType.equalsIgnoreCase("uyesigorta")) {
			this.uyesigorta.setUyeRef(getUyeDetay());
			super.justSave(this.uyesigorta, this.uyesigortaController.isLoggable(), this.oldValueForUyesigorta);
			reloadPage();
		} else if (this.detailEditType.equalsIgnoreCase("uyeaidat")) {
			this.uyeaidat.setUyeRef(getUyeDetay());
			this.uyeaidat = this.uyeaidatController.uyeAidatDurumGuncelle(this.uyeaidat);
			super.justSave(getUyeaidat(), this.uyeaidatController.isLoggable(), this.oldValueForUyeaidat);
			reloadPage();
		} else if (this.detailEditType.equalsIgnoreCase("uyemuafiyet")) {
			this.uyemuafiyet.setUyeRef(getUyeDetay());
			if (getUyemuafiyet().getBitistarih().before(getUyemuafiyet().getBaslangictarih()) || getUyemuafiyet().getBitistarih().equals(getUyemuafiyet().getBaslangictarih())) {
				createCustomMessage("Başlangıç tarihi bitiş tarihinden önce olmalıdır!", FacesMessage.SEVERITY_ERROR, "detailEditForm:baslangictarihCalendar:baslangictarih");
				return;
			}
			super.justSave(getUyemuafiyet(), this.uyemuafiyetController.isLoggable(), this.oldValueForUyemuafiyet);

			// Muafiyet kaydi yapildiktan sonra aidat bilgileri guncelleniyor...
			List<Uyeaidat> aidatListesi = getUyeMuafiyetEtkilenenAidatListesi();
			if (!isEmpty(aidatListesi)) {
				for (Uyeaidat aidat : aidatListesi) {
					aidat.setUyemuafiyetRef(uyemuafiyet);
					aidat.setUyeaidatdurum(UyeAidatDurum._MUAF);
					getDBOperator().update(aidat);
				}
			}
			reloadPage();
		} else if (this.detailEditType.equalsIgnoreCase("uyeborc")) {
			this.uyeborc.setKisiRef(getUyeDetay().getKisiRef());
			this.uyeborc = this.borcController.borcDurumuGuncelle(this.uyeborc);
			super.justSave(this.uyeborc, this.borcController.isLoggable(), this.oldValueForUyeborc);
			reloadPage();
		} else if (this.detailEditType.equalsIgnoreCase("kurumkisi")) {
			this.kurumKisi.setKisiRef(getUyeDetay().getKisiRef());
			if (this.kurumKisi.getVarsayilan() == EvetHayir._EVET) {
				String whereCondition = "o.rID <> " + this.kurumKisi.getRID() + " AND o.varsayilan =" + EvetHayir._EVET.getCode();
				if (this.kurumKisi.getKisiRef() != null) {
					whereCondition += " AND o.kisiRef.rID = " + this.kurumKisi.getKisiRef().getRID();
				}
				try {
					getDBOperator().executeQuery("UPDATE " + KurumKisi.class.getSimpleName() + " AS o SET o.varsayilan =" + EvetHayir._HAYIR.getCode() + " WHERE " + whereCondition);
				} catch (Exception e) {
					logYaz("Exception @" + getModelName() + "Controller :", e);
				}
			} else if (this.kurumKisi.getVarsayilan() == EvetHayir._HAYIR) {
				String whereCondition = "o.rID <> " + this.kurumKisi.getRID();
				if (this.kurumKisi.getKisiRef() != null) {
					whereCondition += " AND o.kisiRef.rID = " + this.kurumKisi.getKisiRef().getRID() + " AND o.varsayilan=" + EvetHayir._EVET.getCode();
				}
				try {
					if (getDBOperator().recordCount(KurumKisi.class.getSimpleName(), whereCondition) == 0) {
						kurumKisi.setVarsayilan(EvetHayir._EVET);
						getDBOperator().update(kurumKisi);
					}
				} catch (Exception e) {
					logYaz("Exception @" + getModelName() + "Controller :", e);
				}
			}
			this.kurumKisi.setKisiRef(getUyeDetay().getKisiRef());
			if (this.kurumKisi.getKurumRef() == null) {
				this.kurumKisi.setKurumRef(getPreviousKurumForUyeKurum());
			}
			if (this.kurumKisi.getUyekurumDurum().getCode() == UyekurumDurum._CALISIYOR.getCode()) {
				this.kurumKisi.setBitistarih(null);
			}
			super.justSave(this.kurumKisi, this.kurumKisiController.isLoggable(), this.oldValueForKurumKisi);
			KurumKisi kurumKisi = new KurumKisi();
			if (this.kurumKisi.getKisiRef() != null) {
				if (getDBOperator().recordCount(KurumKisi.class.getSimpleName(), "o.kurumRef.rID=" + getKurumKisi().getKurumRef().getRID() + " AND o.kisiRef.rID=" + this.kurumKisi.getKisiRef().getRID()) == 0
						&& this.kurumKisi.getUyekurumDurum().getCode() == UyekurumDurum._CALISIYOR.getCode()) {
					kurumKisi.setKisiRef(this.kurumKisi.getKisiRef());
					kurumKisi.setKurumRef(getKurumKisi().getKurumRef());
					kurumKisi.setGecerli(EvetHayir._EVET);
					getDBOperator().insert(kurumKisi);
				} else if (getDBOperator().recordCount(KurumKisi.class.getSimpleName(), "o.kurumRef.rID=" + getKurumKisi().getKurumRef().getRID() + " AND o.kisiRef.rID=" + this.kurumKisi.getKisiRef().getRID()) > 0) {
					kurumKisi = (KurumKisi) getDBOperator().load(KurumKisi.class.getSimpleName(), "o.kurumRef.rID=" + getKurumKisi().getKurumRef().getRID() + " AND o.kisiRef.rID=" + this.kurumKisi.getKisiRef().getRID(), "").get(0);
					if (this.kurumKisi.getUyekurumDurum().getCode() == UyekurumDurum._AYRILMIS.getCode()) {
						getDBOperator().update(kurumKisi);
					}
				}
			}
			if (getPreviousKurumForUyeKurum() != null && getKurumKisi() != null) {
				if (getPreviousKurumForUyeKurum().getRID() != getKurumKisi().getKurumRef().getRID()) {
					if (getDBOperator().recordCount(Adres.class.getSimpleName(), "o.kisiRef.rID =" + getKurumKisi().getKisiRef().getRID() + " AND o.adresturu=" + AdresTuru._ISADRESI.getCode()) > 0) {
						Adres eskiadres = (Adres) getDBOperator().load(Adres.class.getSimpleName(), "o.kisiRef.rID =" + getKurumKisi().getKisiRef().getRID() + " AND o.adresturu=" + AdresTuru._ISADRESI.getCode(), "").get(0);
						getDBOperator().delete(eskiadres);
					}

					if (getDBOperator().recordCount(Adres.class.getSimpleName(), "o.kurumRef.rID=" + getKurumKisi().getKurumRef().getRID() + " AND o.varsayilan=" + EvetHayir._EVET.getCode()) > 0) {
						Adres kurumAdres = (Adres) getDBOperator().load(Adres.class.getSimpleName(), "o.kurumRef.rID=" + getKurumKisi().getKurumRef().getRID() + " AND o.varsayilan=" + EvetHayir._EVET.getCode(), "").get(0);
						adres = new Adres();
						adres = (Adres) kurumAdres.cloneObject();
						adres.setRID(null);
						adres.setVarsayilan(EvetHayir._EVET);
						adres.setKisiRef(getKurumKisi().getKisiRef());
						getDBOperator().insert(adres);
						if (getDBOperator().recordCount(Adres.class.getSimpleName(),
								"o.kisiRef.rID=" + getKurumKisi().getKisiRef().getRID() + " AND ( o.adresturu=" + AdresTuru._EVADRESI.getCode() + " OR o.adresturu=" + AdresTuru._KPSADRESI.getCode() + " ) ") > 0) {
							@SuppressWarnings("unchecked")
							List<Adres> tumadresleri = getDBOperator().load(Adres.class.getSimpleName(),
									"o.kisiRef.rID=" + getKurumKisi().getKisiRef().getRID() + " AND ( o.adresturu=" + AdresTuru._EVADRESI.getCode() + " OR o.adresturu=" + AdresTuru._KPSADRESI.getCode() + " ) ", "");
							for (Adres entity : tumadresleri) {
								entity.setVarsayilan(EvetHayir._HAYIR);
								getDBOperator().update(entity);
							}
						}
					}
				}
			} else if (getKurumKisi() != null) {
				if (getDBOperator().recordCount(Adres.class.getSimpleName(), "o.kurumRef.rID=" + getKurumKisi().getKurumRef().getRID() + " AND o.varsayilan=" + EvetHayir._EVET.getCode()) > 0) {
					Adres kurumAdres = (Adres) getDBOperator().load(Adres.class.getSimpleName(), "o.kurumRef.rID=" + getKurumKisi().getKurumRef().getRID() + " AND o.varsayilan=" + EvetHayir._EVET.getCode(), "").get(0);
					adres = new Adres();
					adres = (Adres) kurumAdres.cloneObject();
					adres.setRID(null);
					adres.setVarsayilan(EvetHayir._EVET);
					adres.setKisiRef(getKurumKisi().getKisiRef());
					getDBOperator().insert(adres);
					if (getDBOperator().recordCount(Adres.class.getSimpleName(),
							"o.kisiRef.rID=" + getKurumKisi().getKisiRef().getRID() + " AND ( o.adresturu=" + AdresTuru._EVADRESI.getCode() + " OR o.adresturu=" + AdresTuru._KPSADRESI.getCode() + " ) ") > 0) {
						@SuppressWarnings("unchecked")
						List<Adres> tumadresleri = getDBOperator().load(Adres.class.getSimpleName(),
								"o.kisiRef.rID=" + getKurumKisi().getKisiRef().getRID() + " AND ( o.adresturu=" + AdresTuru._EVADRESI.getCode() + " OR o.adresturu=" + AdresTuru._KPSADRESI.getCode() + " ) ", "");
						for (Adres entity : tumadresleri) {
							entity.setVarsayilan(EvetHayir._HAYIR);
							getDBOperator().update(entity);
						}
					}
				}
			}
			// reloadPage();
			if (getSessionUser().isKullaniciUyeMi()) {
				reloadUyeDashboard();
			} else {
				reloadPage();
			}
		} else if (this.detailEditType.equalsIgnoreCase("adres")) {
			if (this.adres.getAdrestipi() == AdresTipi._YURTDISIADRESI) {
				if (this.adres.getYabanciulke() == null && !isEmpty(this.adres.getYabanciadres()) && this.adres.getYabancisehir() == null) {
					createGenericMessage("Yurdışı adresleri için ülke, şehir ve  adres alanları zorunludur.", FacesMessage.SEVERITY_ERROR);
					return;
				}
			} else if (this.adres.getAdrestipi() != AdresTipi._YURTDISIADRESI) {
				if (isEmpty(this.adres.getAcikAdres()) || this.adres.getSehirRef() == null || this.adres.getIlceRef() == null) {
					createGenericMessage("Adres için il, ilçe ve açık adres alanları zorunludur.", FacesMessage.SEVERITY_ERROR);
					return;
				} else if (this.adres.getUlkeRef() == null) {
					this.adres.setUlkeRef((Ulke) getDBOperator().find(Ulke.class.getSimpleName(), "o.rID=9980"));
				}
			}
			if (this.adres.getAdresturu() != AdresTuru._NULL) {
				this.adres.setKisiRef(getUyeDetay().getKisiRef());
				if (this.adres.getAdresturu() == AdresTuru._EVADRESI && this.adres.getRID() == null && getAdresupdate()) {
					if (getDBOperator().recordCount(Adres.class.getSimpleName(), "o.kisiRef.rID=" + getUyeDetay().getKisiRef().getRID() + " AND o.adresturu=" + AdresTuru._EVADRESI.getCode()) > 0) {
						createGenericMessage("Birden fazla ev adresi ekleyemezsiniz!", FacesMessage.SEVERITY_INFO);

						this.detailEditType = null;
						updateDialogAndForm(context);
						return;
					}
				} else if (this.adres.getAdresturu() == AdresTuru._ISADRESI && getAdresupdate()) {
					if (getDBOperator().recordCount(Adres.class.getSimpleName(), "o.kisiRef.rID=" + getUyeDetay().getKisiRef().getRID() + " AND o.adresturu=" + AdresTuru._ISADRESI.getCode()) > 0) {
						createGenericMessage("Birden fazla işyeri adresi ekleyemezsiniz!", FacesMessage.SEVERITY_INFO);
						this.detailEditType = null;
						updateDialogAndForm(context);
						return;
					}
				}
				if (this.adres.getVarsayilan() == EvetHayir._HAYIR) {
					if (getDBOperator().recordCount(Adres.class.getSimpleName(), "o.kisiRef = " + getUyeDetay().getKisiRef().getRID() + " AND o.varsayilan = " + EvetHayir._EVET.getCode()) == 0) {
						this.adres.setVarsayilan(EvetHayir._EVET);
					}
				}
				if (this.adres.getVarsayilan() == EvetHayir._EVET) {
					getDBOperator().executeQuery(
							"UPDATE " + Adres.class.getSimpleName() + " o set o.varsayilan = " + EvetHayir._HAYIR.getCode() + " " + "	WHERE o.kisiRef = " + this.adres.getKisiRef().getRID() + " AND o.rID <> " + this.adres.getRID());
				}
				super.justSave(this.adres, this.adresController.isLoggable(), this.oldValueForAdres);
				reloadPage();
			}
		} else if (this.detailEditType.equalsIgnoreCase("telefon")) {
			if (!isEmpty(this.telefon.getTelefonno()) && this.telefon.getTelefonturu() != TelefonTuru._NULL) {
				this.telefon.setKisiRef(getUyeDetay().getKisiRef());
				if (this.telefon.getTelefonturu() == TelefonTuru._FAKS) {
					this.telefon.setVarsayilan(EvetHayir._HAYIR);
				}
				if (this.telefon.getVarsayilan() == EvetHayir._HAYIR) {
					if (getDBOperator().recordCount(Telefon.class.getSimpleName(), "o.kisiRef = " + getUyeDetay().getKisiRef().getRID() + " AND o.varsayilan = " + EvetHayir._EVET.getCode()) == 0) {
						this.telefon.setVarsayilan(EvetHayir._EVET);
					}
				}
				super.justSave(this.telefon, this.telefonController.isLoggable(), this.oldValueForTelefon);
				if (this.telefon.getVarsayilan() == EvetHayir._EVET) {
					getDBOperator().executeQuery(
							"UPDATE " + Telefon.class.getSimpleName() + " o set o.varsayilan = " + EvetHayir._HAYIR.getCode() + " " + "	WHERE o.kisiRef = " + this.telefon.getKisiRef().getRID() + " AND o.rID <> " + this.telefon.getRID());
				}
				reloadPage();
			}
		} else if (this.detailEditType.equalsIgnoreCase("internetadres")) {
			if (!isEmpty(this.internetadres.getNetadresmetni()) && this.internetadres.getNetadresturu() != NetAdresTuru._NULL) {
				this.internetadres.setKisiRef(getUyeDetay().getKisiRef());
				if (this.internetadres.getNetadresturu() != NetAdresTuru._EPOSTA) {
					this.internetadres.setVarsayilan(EvetHayir._HAYIR);
				}

				String mail = null;
				if (this.internetadres.getNetadresturu() == NetAdresTuru._WEBSAYFASI) {
					if (internetadres.getNetadresmetni() != null) {
						mail = internetadres.getNetadresmetni();
						if (!mail.contains("www.")) {
							createCustomMessage("Geçersiz Web Sayfası !", FacesMessage.SEVERITY_ERROR, "detailEditForm:netadresmetniInputText:netadresmetni");
							return;
						}
					}
				} else {
					if (internetadres.getNetadresmetni() != null) {
						mail = internetadres.getNetadresmetni();
						if (!mail.contains("@")) {
							createCustomMessage("Geçersiz Eposta Adresi !", FacesMessage.SEVERITY_ERROR, "detailEditForm:netadresmetniInputText:netadresmetni");
							return;
						}
					}
				}
				if (this.internetadres.getVarsayilan() == EvetHayir._HAYIR) {
					if (getDBOperator().recordCount(Internetadres.class.getSimpleName(), "o.kisiRef = " + getUyeDetay().getKisiRef().getRID() + " AND o.varsayilan = " + EvetHayir._EVET.getCode()) == 0) {
						this.internetadres.setVarsayilan(EvetHayir._EVET);
					}
				}
				// todo mail eklendigi ve guncellendigi kisim - oldValue atanmamissa eklemedir yoksa guncelleme
				String yeniMail = this.internetadres.getNetadresmetni();
				String eskiMail;
				if (this.oldValueForInternetadresMetni != null) {
					eskiMail = this.oldValueForInternetadresMetni;
					logYaz("GUNCELLENDI | " + epostaKisiLog() + eskiMail + " | " + yeniMail);
					epostaLogger.info("GUNCELLENDI | " + epostaKisiLog() + eskiMail + " | " + yeniMail);
				} else {
					logYaz("EKLENDI | " + epostaKisiLog() + yeniMail);
					epostaLogger.info("EKLENDI | " + epostaKisiLog() + yeniMail);
				}
				super.justSave(this.internetadres, this.internetadresController.isLoggable(), this.oldValueForInternetadres);
				if (this.internetadres.getVarsayilan() == EvetHayir._EVET) {
					getDBOperator().executeQuery("UPDATE " + Internetadres.class.getSimpleName() + " o set o.varsayilan = " + EvetHayir._HAYIR.getCode() + " " + "	WHERE o.kisiRef = " + this.internetadres.getKisiRef().getRID()
							+ " AND o.rID <> " + this.internetadres.getRID());
				}
				reloadPage();
			}
		} else if (this.detailEditType.equalsIgnoreCase("uyeodeme")) {
			this.uyeodeme.setKisiRef(getUyeDetay().getKisiRef());
			this.uyeodeme.setBirimRef(getSessionUser().getPersonelRef().getBirimRef());
			if (this.uyeodeme.getMakbuzno().intValue() == 0) {
				this.uyeodeme.setMakbuzno(null);
			}
			ManagedBeanLocator.locateSessionController().uyeOdeme(this.uyeodeme, null);
			super.justSave(getUyeodeme(), this.uyeOdemeController.isLoggable(), this.oldValueForUyeOdeme);
			reloadPage();
		} else if (this.detailEditType.equalsIgnoreCase("aidatodeme")) {
			this.aidatodeme.setKisiRef(getUyeDetay().getKisiRef());
			this.aidatodeme.setBirimRef(getSessionUser().getPersonelRef().getBirimRef());
			if (this.aidatodeme.getMakbuzno().intValue() == 0) {
				this.aidatodeme.setMakbuzno(null);
			}
			ManagedBeanLocator.locateSessionController().uyeOdeme(this.aidatodeme, null);
			super.justSave(getAidatodeme(), this.aidatOdemeController.isLoggable(), this.oldValueForAidatOdeme);
			this.detailEditType = "uyeaidat";
			reloadPage();
		} else if (this.detailEditType.equalsIgnoreCase("aidatodemeisbankasi")) {
			this.aidatodemeisbankasi.setKisiRef(getUyeDetay().getKisiRef());
			Calendar cal = Calendar.getInstance();
			this.aidatodemeisbankasi.setTarih(cal.getTime());
			ManagedBeanLocator.locateSessionController().uyeOdeme(this.aidatodemeisbankasi, null);
			super.justSave(getAidatodeme(), this.aidatodemeisbankasiController.isLoggable(), this.oldValueForAidatOdemeIsbankasi);
			this.aidatodemeisbankasi = new Odeme();
			this.aidatodemeisbankasiController.queryAction();
			this.uyeaidatController.queryAction();
			uyeAidatYuklemesayfasi("aidatodemeisbankasi");
			// getHash();
			updateDialogAndForm(context);
		} else if (this.detailEditType.equalsIgnoreCase("kisifotograf")) {
			// if(!isEmpty(this.kisifotograf.getPath())){
			if (!isEmpty(this.kisifotograf.getPath())) {
				this.kisifotograf.setVarsayilan(EvetHayir._EVET);
				getDBOperator().executeQuery("UPDATE " + Kisifotograf.class.getSimpleName() + " o set o.varsayilan = " + EvetHayir._HAYIR.getCode() + " " + "	WHERE o.kisiRef.rID = " + getUyeDetay().getKisiRef().getRID());
				this.kisifotograf.setKisiRef(getUyeDetay().getKisiRef());
				super.justSave(this.kisifotograf, this.kisifotografController.isLoggable(), "");
				setDetailEditType(null);
				context.update("uyeForm");
				context.execute("document.getElementById('detailEditForm:cancelDetailsButton').click();");
				reloadPage();
				if (getSessionUser().isKullaniciUyeMi()) {
					reloadUyeDashboard();
				} else {
					reloadPage();
				}
			}
		} else if (this.detailEditType.equalsIgnoreCase("kisiogrenimbilgileri")) {
			this.kisiogrenim.setKisiRef(getUyeDetay().getKisiRef());
			super.justSave(this.kisiogrenim, this.kisiogrenimController.isLoggable(), this.oldValueForKisiogrenim);
			if (this.kisiogrenim.getVarsayilan() == EvetHayir._EVET) {
				String whereCondition = "o.rID <> " + this.kisiogrenim.getRID() + " AND o.varsayilan =" + EvetHayir._EVET.getCode();
				if (this.kisiogrenim.getKisiRef() != null) {
					whereCondition += " AND o.kisiRef.rID = " + this.kisiogrenim.getKisiRef().getRID();
				}
				try {
					if (getDBOperator().recordCount(Kisiogrenim.class.getSimpleName(), whereCondition) > 0) {
						for (Object obj : getDBOperator().load(Kisiogrenim.class.getSimpleName(), whereCondition, "o.rID")) {
							Kisiogrenim kisiogrenim = (Kisiogrenim) obj;
							kisiogrenim.setVarsayilan(EvetHayir._HAYIR);
							getDBOperator().update(kisiogrenim);
						}
					}
				} catch (Exception e) {
					logYaz("Exception @" + getModelName() + "Controller :", e);
				}
			} else if (this.kisiogrenim.getVarsayilan() == EvetHayir._HAYIR) {
				String whereCondition = "o.rID <> " + this.kisiogrenim.getRID() + " AND o.varsayilan =" + EvetHayir._HAYIR.getCode();
				if (this.kisiogrenim.getKisiRef() != null) {
					whereCondition += " AND o.kisiRef.rID = " + this.kisiogrenim.getKisiRef().getRID();
				}
				String whereCondition1 = "o.rID <> " + this.kisiogrenim.getRID() + " AND o.varsayilan =" + EvetHayir._EVET.getCode();
				if (this.kisiogrenim.getKisiRef() != null) {
					whereCondition += " AND o.kisiRef.rID = " + this.kisiogrenim.getKisiRef().getRID();
				}
				try {
					if (getDBOperator().recordCount(Kisiogrenim.class.getSimpleName(), whereCondition) == 0 && getDBOperator().recordCount(Kisiogrenim.class.getSimpleName(), whereCondition1) == 0) {
						kisiogrenim.setVarsayilan(EvetHayir._EVET);
						getDBOperator().update(kisiogrenim);
					}
					whereCondition = "o.rID <> " + this.kisiogrenim.getRID() + " AND o.varsayilan =" + EvetHayir._EVET.getCode();
					if (this.kisiogrenim.getKisiRef() != null) {
						whereCondition += " AND o.kisiRef.rID = " + this.kisiogrenim.getKisiRef().getRID();
					}
					if (getDBOperator().recordCount(Kisiogrenim.class.getSimpleName(), whereCondition) == 0) {
						createGenericMessage(KeyUtil.getMessageValue("varsayilan.hata"), FacesMessage.SEVERITY_INFO);
						kisiogrenim.setVarsayilan(EvetHayir._EVET);
						getDBOperator().update(kisiogrenim);
					}

				} catch (Exception e) {
					logYaz("Exception @" + getModelName() + "Controller :", e);
				}
			}
			this.kisiOgrenimGuncelle(getUyeDetay());
			reloadUyeDashboard();
		}
		context.update("uyePanel");
		this.detailEditType = null;
	}

	private String epostaKisiLog() {
		Kisi kisiDetay = uyeDetay.getKisiRef();
		StringBuilder builder = new StringBuilder();
		builder.append(kisiDetay.getKimlikno());
		builder.append(" | ");
		builder.append(kisiDetay.getAd());
		builder.append(" ");
		builder.append(kisiDetay.getSoyad());
		builder.append(" | ");
		return builder.toString();
	}

	public void saveUyeOdeme() {
		RequestContext context = RequestContext.getCurrentInstance();
		this.uyeodeme.setKisiRef(getUyeDetay().getKisiRef());
		super.justSave(getUyeodeme(), this.uyeOdemeController.isLoggable(), this.oldValueForUyeOdeme);
		this.uyeodeme = new Odeme();
		this.uyeOdemeController.queryAction();
		updateDialogAndForm(context);
		context.update("uyePanel");
		this.detailEditType = null;
	}

	private List<Uyeaidat> getUyeMuafiyetEtkilenenAidatListesi() {
		Calendar cal = Calendar.getInstance();
		cal.setTime(getUyemuafiyet().getBaslangictarih());
		int monthBaslangic = cal.get(Calendar.MONTH) + 1;
		int yearBaslangic = cal.get(Calendar.YEAR);
		// TODO bitiş tarihi null olunca nasıl davranır
		cal.setTime(getUyemuafiyet().getBitistarih());
		int monthBitis = cal.get(Calendar.MONTH) + 1;
		int yearBitis = cal.get(Calendar.YEAR);
		// TODO bu sorgu yanlış
		@SuppressWarnings("unchecked")
		List<Uyeaidat> aidatListesi = getDBOperator().load(Uyeaidat.class.getSimpleName(),
				"o.uyeRef.rID = " + getUyeDetay().getRID() + " " + " AND ((o.ay >=" + monthBaslangic + " AND o.yil >= " + yearBaslangic + ") " + " " + " AND (o.ay <=" + monthBitis + " AND o.yil <= " + yearBitis + "))", "o.rID");
		return aidatListesi;
	}

	private List<Uyeaidat> getUyeMuafiyetEtkilenenAidatListesi(Long muafiyetRefRID) {
		@SuppressWarnings("unchecked")
		List<Uyeaidat> aidatListesi = getDBOperator().load(Uyeaidat.class.getSimpleName(), "o.uyemuafiyetRef.rID = " + muafiyetRefRID, "o.rID");
		return aidatListesi;
	}

	private void updateDialogAndForm(RequestContext context) {
		context.update("dialogUpdateBox");
		context.update("uyeForm");
	}

	private void reloadPage() {
		putObjectToSessionFilter("lastActiveTab", this.detailEditType);
		getSessionUser().setMessage("Kayıt eklendi/güncellendi.");
		redirectPage("/faces/_page/form/uye/uye_Detay.xhtml");
	}

	private void reloadUyeDashboard() {
		putObjectToSessionFilter("lastActiveTab", this.detailEditType);
		putObjectToSessionFilter("detailEditType", this.detailEditType);
		this.detailEditType = null;
		getSessionUser().setMessage("Kayıt eklendi/güncellendi.");
		redirectPage("/faces/_page/_main/uyedashboard.xhtml");
	}

	public SelectItem[] getSelectItemListForUyeuzmanlik() {
		@SuppressWarnings("unchecked")
		List<Uyeuzmanlik> entityList = getDBOperator().load(Uyeuzmanlik.class.getSimpleName(), "o.uyeRef.rID=" + getUyeDetay().getRID(), "o.rID");
		if (entityList != null) {
			SelectItem[] selectItemList = new SelectItem[entityList.size() + 1];
			selectItemList[0] = new SelectItem(null, "");
			int i = 1;
			for (BaseEntity entity : entityList) {
				selectItemList[i++] = new SelectItem(entity, entity.getUIString());
			}
			return selectItemList;
		}
		return new SelectItem[0];
	}

	@Override
	public void update() {
		if (getUyeDetay() == null) {
			return;
		}
		setTable(null);
		setOldValue(getUyeDetay().getValue());
		setAddScreen(false);
		setEditPanelRendered(true);
	}

	@Override
	public void save() {
		if (getUye().getUyetip() == UyeTip._OGRENCI) {
			if (isEmpty(getUye().getOgrenciNo())) {
				createCustomMessage("Öğrenci üyeler için öğrenci numarası ve sınıf bilgisi giriniz!", FacesMessage.SEVERITY_ERROR, "ogrenciNoInputText:ogrenciNo");
				return;
			}
			if (getUye().getSinif() <= 0) {
				createCustomMessage("Öğrenci üyeler için öğrenci numarası ve sınıf bilgisi giriniz!", FacesMessage.SEVERITY_ERROR, "sinifSpinner:sinif");
				return;
			}
		}
		super.justSave();
	}

	public void updateUyeDetay() {
		if (getUyeDetay().getUyetip() == UyeTip._OGRENCI) {
			if (isEmpty(getUyeDetay().getOgrenciNo())) {
				createCustomMessage("Öğrenci üyeler için öğrenci numarası ve sınıf bilgisi giriniz!", FacesMessage.SEVERITY_ERROR, "ogrenciNoInputText:ogrenciNo");
				return;
			}
			if (!isNumber(getUyeDetay().getSinif() + "") || getUyeDetay().getSinif() <= 0) {
				createCustomMessage("Öğrenci üyeler için öğrenci numarası ve sınıf bilgisi giriniz!", FacesMessage.SEVERITY_ERROR, "sinifSpinner:sinif");
				return;
			}
		}
		super.justSave(getUyeDetay(), isLoggable(), getOldValue());
	}

	public void kartTeslim() {
		super.justSave(getUyeDetay(), isLoggable(), getOldValue());
		reloadPage();
	}

	public void hazirFormCikti(String formTuru) throws DocumentException, Exception {
		if (getUyeDetay() == null) {
			return;
		}
		Kullanici sysUser = ManagedBeanLocator.locateSessionUser().getKullanici();
		byte[] arr = CustomPdfExport.uyeForm(getUyeDetay().getSicilno() + formTuru + ".pdf", getUyeDetay().getRID(), sysUser);
		download(arr, getUyeDetay().getSicilno() + formTuru);
	}

	public void download(byte[] pdfData, String fileName) throws IOException {
		// Prepare.
		FacesContext facesContext = FacesContext.getCurrentInstance();
		ExternalContext externalContext = facesContext.getExternalContext();
		HttpServletResponse response = (HttpServletResponse) externalContext.getResponse();

		// Initialize response.
		response.reset();
		response.setContentType("application/pdf");
		response.setHeader("Content-disposition", "attachment; filename=\"" + fileName + ".pdf\"");

		// Write file to response.
		OutputStream output = response.getOutputStream();
		output.write(pdfData);
		output.close();

		// Inform JSF to not take the response in hands.
		facesContext.responseComplete();
	}

	@Override
	public void delete() {
		if (!setSelected()) {
			return;
		}
		Long universiteRef = 0L;
		Long fakulteRef = 0L;
		Long bolumRef = 0L;
		if (getKisiogrenim().getUniversiteRef() != null) {
			universiteRef = this.kisiogrenim.getUniversiteRef().getRID();
		} else if (getKisiogrenim().getFakulteRef() != null) {
			fakulteRef = this.kisiogrenim.getFakulteRef().getRID();
		} else if (getKisiogrenim().getBolumRef() != null) {
			bolumRef = this.kisiogrenim.getBolumRef().getRID();
		}
		try {
			getDBOperator().delete(super.getEntity());
			setSelection(null);
			setTable(null);
			createGenericMessage(KeyUtil.getMessageValue("kayit.silindi"), FacesMessage.SEVERITY_INFO);
		} catch (Exception e) {
			createGenericMessage(KeyUtil.getMessageValue("islem.hata") + " Mes : " + e.getMessage(), FacesMessage.SEVERITY_ERROR);
			return;
		}

		String whereCondition = "o.varsayilan =" + EvetHayir._EVET.getCode();
		if (universiteRef != 0) {
			whereCondition += " AND o.universiteRef.rID = " + universiteRef;
		} else if (fakulteRef != 0) {
			whereCondition += " AND o.fakulteRef.rID = " + fakulteRef;
		} else if (bolumRef != 0) {
			whereCondition += " AND o.bolumRef.rID = " + fakulteRef;
		}

		try {
			if (getDBOperator().recordCount(getModelName(), whereCondition) == 0) {
				whereCondition = "o.rID <> " + this.kisiogrenim.getRID() + " AND o.varsayilan =" + EvetHayir._HAYIR.getCode();
				if (universiteRef != 0) {
					whereCondition += " AND o.universiteRef.rID = " + universiteRef;
				} else if (fakulteRef != 0) {
					whereCondition += " AND o.fakulteRef.rID = " + fakulteRef;
				} else if (bolumRef != 0) {
					whereCondition += " AND o.bolumRef.rID = " + fakulteRef;
				}
				if (getDBOperator().recordCount(getModelName(), whereCondition) > 0) {
					Kisiogrenim kisiogrenim = (Kisiogrenim) getDBOperator().load(getModelName(), whereCondition, "o.rID DESC").get(0);
					if (kisiogrenim != null) {
						kisiogrenim.setVarsayilan(EvetHayir._EVET);
						getDBOperator().update(kisiogrenim);
					}
				}
			}
		} catch (Exception e) {
			logYaz("Exception @" + getModelName() + "Controller :", e);
		}
		queryAction();
	}

	private BigDecimal uyeAidatBorc = new BigDecimal(0L);
	private BigDecimal uyeBorc = new BigDecimal(0L);

	public void uyeBorcHesapla(Uye uye) {
		uyeBorcAidatHesapla(uye);
		borcHesapla(uye);
	}

	private void uyeBorcAidatHesapla(Uye uye) {
		try {
			BigDecimal borc = (BigDecimal) getDBOperator().sum("miktar-odenen", Uyeaidat.class.getSimpleName(), "o.uyeRef.rID=" + uye.getRID() + " AND o.uyemuafiyetRef is null " + "AND ((o.yil = " + DateUtil.getYear(new Date())
					+ " AND o.ay < " + DateUtil.getMonth(new Date()) + ") OR (o.yil < " + DateUtil.getYear(new Date()) + " ))");
			setUyeAidatBorc(BigDecimalUtil.changeToCurrency(borc));
		} catch (Exception e) {
			setUyeAidatBorc(new BigDecimal(0L));
		}
	}

	private void borcHesapla(Uye uye) {
		try {
			BigDecimal borc = (BigDecimal) getDBOperator().sum("miktar-odenen", Borc.class.getSimpleName(), "o.kisiRef.rID=" + uye.getKisiRef().getRID());
			setUyeBorc(BigDecimalUtil.changeToCurrency(borc));
		} catch (Exception e) {
			setUyeBorc(new BigDecimal(0L));
		}
	}

	int doubleclick = 0;

	public int getDoubleclick() {
		return doubleclick;
	}

	public void setDoubleclick(int doubleclick) {
		this.doubleclick = doubleclick;
	}

	@Override
	public void tableRowSelect(SelectEvent event) {
		super.tableRowSelect(event);

		try {
			doubleclick++;
			setDoubleclick(doubleclick);
			uyeBorcHesapla(getUye());
			setEntity((BaseEntity) event.getObject());
			if (getEntity() != null) {
				setSelectedRID(((Uye) getEntity()).getRID().toString());
			}
			// if (doubleclick==2){
			//
			// ManagedBeanLocator.locateSessionUser().addToSessionFilters(getModelName(),
			// getEntity());
			// FacesContext ctx = FacesContext.getCurrentInstance();
			// ExternalContext ectx = ctx.getExternalContext();
			// ectx.redirect("uye_Detay.xhtml");
			// }

		} catch (Exception e) {
			logYaz("Exception @" + getModelName() + "Controller :", e);
		}
	}

	public void tableOgrenciUyeRowSelect(SelectEvent event) {
		super.tableRowSelect(event);
		try {
			setEntity((BaseEntity) event.getObject());
			if (getEntity() != null) {
				setSelectedRID(((Uye) getEntity()).getRID().toString());
			}
		} catch (Exception e) {
			logYaz("Exception @" + getModelName() + "Controller :", e);
		}
	}

	private void uyeBelgeGuncelle(Uye refUye) {
		this.uyebelgeController.setTable(null);
		this.uyebelgeController.getUyebelgeFilter().setUyeRef(refUye);
		this.uyebelgeController.queryAction();
	}

	private void uyeUzmanlikGuncelle(Uye refUye) {
		this.uyeuzmanlikController.setTable(null);
		this.uyeuzmanlikController.getUyeuzmanlikFilter().setKisiRef(refUye.getKisiRef());
		this.uyeuzmanlikController.queryAction();
	}

	private void uyeBilirkisiGuncelle(Uye refUye) {
		this.uyebilirkisiController.setTable(null);
		this.uyebilirkisiController.getUyebilirkisiFilter().setUyeRef(refUye);
		this.uyebilirkisiController.queryAction();
	}

	private void kisiOgrenimGuncelle(Uye refUye) {
		this.kisiogrenimController.setTable(null);
		this.kisiogrenimController.getKisiogrenimFilter().setKisiRef(refUye.getKisiRef());
		this.kisiogrenimController.queryAction();
		if (!isEmpty(this.kisiogrenimController.getList())) {
			for (BaseEntity baseEn : this.kisiogrenimController.getList()) {
				if (((Kisiogrenim) baseEn).getOgrenimtip() == OgrenimTip._LISANS) {
					setKisiogrenim((Kisiogrenim) baseEn);
					return;
				}
			}
		}
	}

	private void kisiOgrenimBilgileriGuncelle(Uye refUye) {
		this.kisiogrenimController.setTable(null);
		this.kisiogrenimController.getKisiogrenimFilter().setKisiRef(refUye.getKisiRef());
		ArrayList<OgrenimTip> ogrenimtiplistesi = new ArrayList<>();
		ogrenimtiplistesi.add(OgrenimTip._YUKSEKLISANS);
		ogrenimtiplistesi.add(OgrenimTip._DOKTORA);
		this.kisiogrenimController.getKisiogrenimFilter().setOgrenimtiplistesi(ogrenimtiplistesi);
		this.kisiogrenimController.queryAction();
	}

	private void uyeOdulveCezaGuncelle(Uye refUye) {
		this.uyeodulController.setTable(null);
		this.uyeodulController.getUyeodulFilter().setUyeRef(refUye);
		this.uyeodulController.queryAction();

		this.uyecezaController.setTable(null);
		this.uyecezaController.getUyecezaFilter().setUyeRef(refUye);
		this.uyecezaController.queryAction();
	}

	private void etkinlikKuruluyeGuncelle(Uye refUye) {
		this.etkinlikkuruluyeController.setTable(null);
		this.etkinlikkuruluyeController.getEtkinlikkuruluyeFilter().setUyeRef(refUye);
		this.etkinlikkuruluyeController.getEtkinlikkuruluyeFilter().createQueryCriterias();
		this.etkinlikkuruluyeController.queryAction();
	}

	private void egitimKatilimciGuncelle(Uye refUye) {
		this.egitimkatilimciController.setTable(null);
		this.egitimkatilimciController.getEgitimkatilimciFilter().setKisiRef(refUye.getKisiRef());
		this.egitimkatilimciController.queryAction();
	}

	private void etkinlikKatilimciGuncelle(Uye refUye) {
		this.etkinlikkatilimciController.setTable(null);
		this.etkinlikkatilimciController.getEtkinlikkatilimciFilter().setKisiRef(refUye.getKisiRef());
		this.etkinlikkatilimciController.queryAction();
	}

	private void kurulDonemUyeGuncelle(Uye refUye) {
		this.kuruldonemuyeController.setTable(null);
		this.kuruldonemuyeController.getKurulDonemUyeFilter().setUyeRef(refUye);
		this.kuruldonemuyeController.queryAction();
	}

	private void komisyonDonemUyeGuncelle(Uye refUye) {
		this.komisyondonemuyeController.setTable(null);
		this.komisyondonemuyeController.getKomisyonDonemUyeFilter().setKisiRef(refUye.getKisiRef());
		this.komisyondonemuyeController.queryAction();
	}

	private void uyeFormGuncelle(Uye refUye) {
		this.uyeformController.setTable(null);
		this.uyeformController.getUyeformFilter().setUyeRef(refUye);
		this.uyeformController.queryAction();
	}

	private void uyeAboneGuncelle(Uye refUye) {
		this.uyeaboneController.setTable(null);
		this.uyeaboneController.getUyeaboneFilter().setUyeRef(refUye);
		this.uyeaboneController.queryAction();
	}

	private void uyeSigortaGuncelle(Uye refUye) {
		this.uyesigortaController.setTable(null);
		this.uyesigortaController.getUyesigortaFilter().setUyeRef(refUye);
		this.uyesigortaController.queryAction();
	}

	private void uyeAidatGuncelle(Uye refUye) {
		this.uyeaidatController.setTable(null);
		this.uyeaidatController.uyeTumAidatDurumGuncelle(refUye.getRID());
		this.uyeaidatController.setOrderField("ay ASC");
		this.uyeaidatController.getUyeaidatFilter().setUyeRef(refUye);
		this.uyeaidatController.getUyeaidatFilter().setYil(DateUtil.getYearOfADate(new Date()));
		this.uyeaidatController.setListSize(12);
		this.uyeaidatController.queryAction();
	}

	private void uyeMuafiyetGuncelle(Uye refUye) {
		this.uyemuafiyetController.setTable(null);
		this.uyemuafiyetController.getUyemuafiyetFilter().setUyeRef(refUye);
		this.uyemuafiyetController.queryAction();
	}

	private void uyeBorcGuncelle(Uye refUye) {
		this.borcController.setTable(null);
		this.borcController.getBorcFilter().setKisiRef(refUye.getKisiRef());
		this.borcController.queryAction();
	}

	private void kurumKisiGuncelle(Uye refUye) {
		this.kurumKisiController.setTable(null);
		this.kurumKisiController.getKurumKisiFilter().setKisiRef(refUye.getKisiRef());
		this.kurumKisiController.queryAction();
	}

	private void uyedurumdegistirmeGuncelle(Uye refUye) {
		this.uyedurumdegistirmeController.setTable(null);
		this.uyedurumdegistirmeController.getUyedurumdegistirmeFilter().setUyeRef(refUye);
		this.uyedurumdegistirmeController.queryAction();
	}

	private void adresListesiGuncelle(Kisi refKisi) {
		this.adresController.setTable(null);
		this.adresController.getAdresFilter().setKisiRef(refKisi);
		this.adresController.getAdresFilter().createQueryCriterias();
		this.adresController.queryAction();
	}

	private void telefonListesiGuncelle(Kisi refKisi) {
		this.telefonController.setTable(null);
		this.telefonController.getTelefonFilter().setKisiRef(refKisi);
		this.telefonController.queryAction();
	}

	private void internetadresListesiGuncelle(Kisi refKisi) {
		this.internetadresController.setTable(null);
		this.internetadresController.getInternetadresFilter().setKisiRef(refKisi);
		this.internetadresController.queryAction();
	}

	private void uyeOdemeGuncelle(Kisi refKisi) {
		this.uyeOdemeController.setTable(null);
		this.uyeOdemeController.getOdemeFilter().setKisiRef(refKisi);
		this.uyeOdemeController.queryAction();
	}

	private void aidatOdemeGuncelle(Kisi refKisi) {
		this.aidatOdemeController.setTable(null);
		this.aidatOdemeController.getOdemeFilter().setKisiRef(refKisi);
		this.aidatOdemeController.queryAction();
	}

	private void aidatOdemeIsBankasiGuncelle(Kisi refKisi) {
		this.aidatOdemeController.setTable(null);
		this.aidatOdemeController.getOdemeFilter().setKisiRef(refKisi);
		this.aidatOdemeController.queryAction();
	}

	private void kisiKimlikGuncelle(Kisi refKisi) {
		this.kisikimlikController.setTable(null);
		this.kisikimlikController.getKisikimlikFilter().setKisiRef(refKisi);
		this.kisikimlikController.queryAction();
	}

	List<UyeAidatMuafiyet> mlist = new ArrayList<UyeAidatMuafiyet>();

	public List<UyeAidatMuafiyet> getMlist() {
		return mlist;
	}

	public void setMlist(List<UyeAidatMuafiyet> mlist) {
		this.mlist = mlist;
	}

	private Boolean borcvarmi = false;

	private BigDecimal kalanToplamBorc = new BigDecimal(0);

	public Boolean getBorcvarmi() {
		return borcvarmi;
	}

	public void setBorcvarmi(Boolean borcvarmi) {
		this.borcvarmi = borcvarmi;
	}

	@SuppressWarnings("unchecked")
	private void uyeAidatBilgisiniGuncelle(Uye refUye) {
		this.setTable(null);
		this.getUyeFilter().setKisiRef(refUye.getKisiRef());
		String query = "SELECT uyeadt.yil, uyeadt.miktar, SUM(uyeadt.odenen), COUNT(uyeadt.rID), COUNT(uyeadt.uyemuafiyetRef.rID)" + " FROM " + ApplicationDescriptor._UYEAIDAT_MODEL + " AS uyeadt " + " WHERE uyeadt.uyeRef.rID="
				+ getUyeDetay().getRID() + " GROUP BY uyeadt.yil,uyeadt.miktar ORDER BY uyeadt.yil ASC";
		List<Object[]> objectList = getDBOperator().loadByQuery(query);
		List<UyeAidatMuafiyet> mlist = new ArrayList<UyeAidatMuafiyet>();
		setKalanToplamBorc(BigDecimal.ZERO);
		for (Object[] objectArray : objectList) {
			int yil = (Integer) objectArray[0];
			BigDecimal aidattutari = (BigDecimal) objectArray[1];
			// todo kalan borc kismini gormek icin odenen degiskenine BigDecimal.ZERO; atamasi yapilabilir
			BigDecimal odenen = (BigDecimal) objectArray[2];
			Long ay = (Long) objectArray[3];
			Long muafolduguaysayisi = (Long) objectArray[4];
			BigDecimal bigay = new BigDecimal(ay);
			/*
			 * logYaz("aidattutari :" + aidattutari.doubleValue()); logYaz("odenen : " + odenen.doubleValue()); logYaz("ay :" + ay); logYaz("muafolduguaysayisi :" + muafolduguaysayisi.doubleValue()); logYaz("bigay :" + bigay.doubleValue());
			 */
			BigDecimal toplamaidat = bigay.multiply(aidattutari);
			BigDecimal bigmuafolduguay = new BigDecimal(muafolduguaysayisi);
			BigDecimal muafolduguaidat = bigmuafolduguay.multiply(aidattutari);
			BigDecimal kalanborc = bigay.multiply(aidattutari).subtract(odenen).subtract(muafolduguaidat);
			BigDecimal odenmeyenaidattoplam = toplamaidat.subtract(odenen.add(muafolduguaidat));
			/*
			 * logYaz( "-------------------------------------------------------------"); logYaz("toplamaidat :" + toplamaidat.doubleValue()); logYaz("bigmuafolduguay : " + bigmuafolduguay.doubleValue()); logYaz("muafolduguaidat :" +
			 * muafolduguaidat.doubleValue()); logYaz("kalanborc :" + kalanborc); logYaz("odenmeyenaidattoplam :" + odenmeyenaidattoplam.doubleValue()); logYaz( "--------------------------------------------------------------" );
			 */
			Long kalanay = 0L;
			if (odenmeyenaidattoplam.longValue() / aidattutari.longValue() != 0L) {
				kalanay = odenmeyenaidattoplam.longValue() / aidattutari.longValue();
				if (odenen.remainder(aidattutari, super.mc).doubleValue() > 0) {
					kalanay++;
				}
			}

			Long odenenay = ay - (kalanay + muafolduguaysayisi);
			UyeAidatMuafiyet uyeaidatmuafiyet = new UyeAidatMuafiyet();
			uyeaidatmuafiyet.setYil(yil);
			uyeaidatmuafiyet.setAidatTutari(aidattutari);
			uyeaidatmuafiyet.setKalanborc(kalanborc);
			uyeaidatmuafiyet.setMuafolduguay(muafolduguaysayisi);
			uyeaidatmuafiyet.setOdenenay(odenenay.intValue());
			uyeaidatmuafiyet.setKalanay(kalanay);
			if (kalanborc.intValue() != 0) {
				borcvarmi = true;
			}
			mlist.add(uyeaidatmuafiyet);
			setKalanToplamBorc(getKalanToplamBorc().add(kalanborc));
			setMlist(mlist);
		}
	}

	public void varsayilanyap(Kisifotograf foto) {
		RequestContext context = RequestContext.getCurrentInstance();
		if (foto != null) {
			try {
				if (getDBOperator().recordCount(Kisifotograf.class.getSimpleName(), "o.rID <> " + foto.getRID() + " AND o.varsayilan =" + EvetHayir._EVET.getCode()) > 0) {
					for (Object obj : getDBOperator().load(Kisifotograf.class.getSimpleName(), "o.rID <> " + foto.getRID() + " AND o.varsayilan =" + EvetHayir._EVET.getCode(), "o.rID")) {
						kisifotograf = (Kisifotograf) obj;
						kisifotograf.setVarsayilan(EvetHayir._HAYIR);
						getDBOperator().update(kisifotograf);

					}
				}
				if (getDBOperator().recordCount(Kisifotograf.class.getSimpleName(), "o.rID = " + foto.getRID() + " AND o.varsayilan =" + EvetHayir._HAYIR.getCode()) > 0) {
					for (Object obj : getDBOperator().load(Kisifotograf.class.getSimpleName(), "o.rID = " + foto.getRID() + " AND o.varsayilan =" + EvetHayir._HAYIR.getCode(), "o.rID")) {
						kisifotograf = (Kisifotograf) obj;
						kisifotograf.setVarsayilan(EvetHayir._EVET);
						getDBOperator().update(kisifotograf);

					}
				}
			} catch (DBException e) {
				logYaz("Exception @" + getModelName() + "Controller :", e);
			}
			kisifotografController.queryAction();
			updateDialogAndForm(context);
			context.update("kisiPanel");
		}

	}

	public boolean varsayilan(Kisifotograf foto) {
		if (foto != null) {
			try {
				if (getDBOperator().recordCount(Kisifotograf.class.getSimpleName(), "o.rID = " + foto.getRID() + " AND o.varsayilan =" + EvetHayir._EVET.getCode()) > 0) {
					return false;
				}
			} catch (DBException e) {
				logYaz("Exception @" + getModelName() + "Controller :", e);
			}
		}
		return true;
	}

	public String getHeightOfEditDialog() {
		if (this.detailEditType == null) {
			return "400";
		} else if (this.detailEditType.equalsIgnoreCase("uyebelge")) {
			return "600";
		} else if (this.detailEditType.equalsIgnoreCase("uye")) {
			return "200";
		} else if (this.detailEditType.equalsIgnoreCase("uyeuzmanlik")) {
			return "400";
		} else if (this.detailEditType.equalsIgnoreCase("uyedashboarduzmanlik")) {
			return "400";
		} else if (this.detailEditType.equalsIgnoreCase("uyebilirkisi")) {
			return "400";
		} else if (this.detailEditType.equalsIgnoreCase("kisiogrenim")) {
			return "600";
		} else if (this.detailEditType.equalsIgnoreCase("uyeceza")) {
			return "400";
		} else if (this.detailEditType.equalsIgnoreCase("uyeodul")) {
			return "400";
		} else if (this.detailEditType.equalsIgnoreCase("etkinlikkuruluye")) {
			return "400";
		} else if (this.detailEditType.equalsIgnoreCase("uyeabone")) {
			return "400";
		} else if (this.detailEditType.equalsIgnoreCase("uyesigorta")) {
			return "500";
		} else if (this.detailEditType.equalsIgnoreCase("uyeaidat")) {
			return "600";
		} else if (this.detailEditType.equalsIgnoreCase("uyemuafiyet")) {
			return "400";
		} else if (this.detailEditType.equalsIgnoreCase("uyeborc")) {
			return "400";
		} else if (this.detailEditType.equalsIgnoreCase("kurumkisi")) {
			return "400";
		} else if (this.detailEditType.equalsIgnoreCase("adres")) {
			return "600";
		} else if (this.detailEditType.equalsIgnoreCase("telefon")) {
			return "300";
		} else if (this.detailEditType.equalsIgnoreCase("internetadres")) {
			return "300";
		} else if (this.detailEditType.equalsIgnoreCase("uyeodeme")) {
			return "500";
		} else if (this.detailEditType.equalsIgnoreCase("aidatodeme")) {
			return "500";
		} else if (this.detailEditType.equalsIgnoreCase("aidatodemeisbankasi")) {
			return "300";
		} else if (this.detailEditType.equalsIgnoreCase("kisifotograf")) {
			return "190";
		} else if (this.detailEditType.equalsIgnoreCase("sifredegistir")) {
			return "250";
		} else if (this.detailEditType.equalsIgnoreCase("kartbilgikontrol")) {
			return "750";
		} else if (this.detailEditType.equalsIgnoreCase("")) {
			return "450";
		} else {
			return "450";
		}
	}

	public void closeDialog() {
		setDetailEditType(null);
	}

	public String wizardPage() {
		getSessionUser().setLegalAccess(1);
		removeObjectFromSessionFilter(ApplicationDescriptor._UYE_MODEL);
		return "uye_Wizard.do";
	}

	@Override
	public String detailPage(Long selectedRID) {
		if (selectedRID != null && selectedRID > 0) {
			setSelectedRID(selectedRID + "");
		} else if (getSelectedRID() == null) {
			return "";
		}
		ManagedBeanLocator.locateSessionUser().addToSessionFilters(getModelName(), getUye());
		return ManagedBeanLocator.locateMenuController().gotoPage("menu_" + getModelName() + "Detay");
	}

	public String detailKisiUyePage(Long selectedRID) {

		Uye uye1 = (Uye) getDBOperator().load(Uye.class.getSimpleName(), "o.kisiRef.rID=" + selectedRID, "o.rID").get(0);
		if (uye1 != null) {
			setSelectedRID(uye1.getRID() + "");
			ManagedBeanLocator.locateSessionUser().addToSessionFilters(getModelName(), getEntity());
			return ManagedBeanLocator.locateMenuController().gotoPage("menu_" + getModelName() + "Detay");
		}
		ManagedBeanLocator.locateSessionUser().addToSessionFilters(getModelName(), getEntity());
		return ManagedBeanLocator.locateMenuController().gotoPage("menu_" + getModelName() + "Detay");
	}

	public String detailUyePage(Long selectedRID) {
		if (selectedRID != null && selectedRID > 0) {
			setSelectedRID(selectedRID + "");
		} else if (getSelectedRID() == null) {
			return "";
		}
		ManagedBeanLocator.locateSessionUser().addToSessionFilters(getModelName(), getEntity());
		return "uyedashboard.do";
	}

	public SelectItem[] getSelectAdresListForUyeKurum() {
		if (getKurumKisi().getKurumRef() != null) {
			@SuppressWarnings("unchecked")
			List<Adres> entityList = getDBOperator().load(Adres.class.getSimpleName(), "o.kurumRef.rID=" + getKurumKisi().getKurumRef().getRID(), "o.varsayilan");
			if (entityList != null) {
				SelectItem[] selectItemList = new SelectItem[entityList.size() + 1];
				selectItemList[0] = new SelectItem(null, "");
				int i = 1;
				for (BaseEntity entity : entityList) {
					selectItemList[i++] = new SelectItem(entity, entity.getUIString());
				}
				setSelectAdresListForUyeKurum(selectItemList);
			} else {
				setSelectAdresListForUyeKurum(new SelectItem[0]);
			}
		} else {
			setSelectAdresListForUyeKurum(new SelectItem[0]);
		}
		return selectAdresListForUyeKurum;
	}

	public void setSelectAdresListForUyeKurum(SelectItem[] selectAdresListForUyeKurum) {
		this.selectAdresListForUyeKurum = selectAdresListForUyeKurum;
	}

	public String getUyeDetayFotograf() {
		if (getUyeDetay() != null && getUyeDetay().getKisiRef() != null) {
			try {
				if (getDBOperator().recordCount(Kisifotograf.class.getSimpleName(), "o.kisiRef.rID=" + getUyeDetay().getKisiRef().getRID()) > 0) {
					Kisifotograf foto = (Kisifotograf) getDBOperator().load(Kisifotograf.class.getSimpleName(), "o.kisiRef.rID=" + getUyeDetay().getKisiRef().getRID(), "o.varsayilan ASC").get(0);
					if (!isEmpty(foto.getPath())) {
						return foto.getPath();
					} else {
						return getNoImagePath();
					}
				} else {
					return getNoImagePath();
				}
			} catch (Exception e) {
				logYaz("Uye fotograf hatasi :" + e.getMessage());
				return "";
			}
		}
		return "";
	}

	public String getUyeDetayIsyeri() {
		if (getUyeDetay() != null) {
			try {
				if (getDBOperator().recordCount(KurumKisi.class.getSimpleName(), "o.kisiRef.rID=" + getUyeDetay().getKisiRef().getRID()) > 0) {
					KurumKisi isyeri = (KurumKisi) getDBOperator().load(KurumKisi.class.getSimpleName(), "o.kisiRef.rID=" + getUyeDetay().getKisiRef().getRID(), "o.rID DESC").get(0);
					return isyeri.getUIString();
				}
			} catch (Exception e) {
				logYaz("Uye isyeri hatasi :" + e.getMessage());
				return "";
			}
		}
		return "";
	}

	public String getUyeDetayOgrenim() {
		if (getUyeDetay() != null) {
			try {
				if (getDBOperator().recordCount(Kisiogrenim.class.getSimpleName(), "o.uyeRef.rID=" + getUyeDetay().getRID()) > 0) {
					Kisiogrenim okul = (Kisiogrenim) getDBOperator().load(Kisiogrenim.class.getSimpleName(), "o.uyeRef.rID=" + getUyeDetay().getRID(), "o.rID DESC").get(0);
					if (okul.getUniversiteRef() != null && okul.getBolumRef() != null) {
						return okul.getUniversiteRef().getAd() + " - " + okul.getBolumRef().getAd();
					} else {
						return "!!!";
					}
				}
			} catch (Exception e) {
				logYaz("Uye ogrenim hatasi :" + e.getMessage());
				return "";
			}
		}
		return "";
	}

	public void uyeKaydiOnayla() throws Exception {
		if (!setSelected()) {
			return;
		}
		if (this.uye.getSicilno() == null || this.uye.getSicilno().equals("") && this.uye.getSicilno().length() == 0) {
			String uyeSicil = ManagedBeanLocator.locateSessionController().uyeSicilNoOlustur(getUye(), true);
			if (!isEmpty(uyeSicil)) {
				this.uye.setSicilno(uyeSicil);
			} else {
				createGenericMessage("Sicil No Verilirken Hata Meydana Geldi!", FacesMessage.SEVERITY_ERROR);
				return;
			}
		}
		EntityManager entityManager = getDBOperator().getEntityManagerFactory().createEntityManager();
		String recordCountQuery = null;
		Query query;
		Long count = 0L;
		try {
			entityManager.getTransaction().begin();
			this.uye.setKayitdurum(UyeKayitDurum._ONAYLANMIS);
			this.uye.setOnaylayanRef(getSessionUser().getPersonelRef());
			this.uye.setOnaytarih(new Date());
			this.uye.setUyedurum(UyeDurum._AKTIFUYE);
			entityManager.merge(this.uye);
			if (this.uye.getUyetip() != UyeTip._OGRENCI) {
				ManagedBeanLocator.locateSessionController().uyeBorclandir(this.uye);
				recordCountQuery = "SELECT COUNT(o.rID) FROM " + Aidat.class.getSimpleName() + " AS o WHERE o.yil=" + DateUtil.getYear(new Date()) + " AND o.uyetip=" + this.uye.getUyetip().getCode();
				query = entityManager.createQuery(recordCountQuery);
				count = (Long) query.getSingleResult();
				if (count > 0) {
					String aidatQuery = "SELECT OBJECT(o) FROM " + Aidat.class.getSimpleName() + " AS o WHERE o.yil=" + DateUtil.getYear(new Date()) + " AND o.uyetip=" + this.uye.getUyetip().getCode();
					query = entityManager.createQuery(aidatQuery);
					Aidat aidat = (Aidat) query.getResultList().get(0);
					Borc borc = new Borc();
					borc.setKisiRef(this.uye.getKisiRef());
					borc.setBorcdurum(BorcDurum._BORCLU);
					borc.setMiktar(aidat.getKayitucreti());
					borc.setOdenen(new BigDecimal(0));
					borc.setTarih(new Date());
					entityManager.persist(borc);
				} else {
					createGenericMessage("Üye kayıt ücreti bulunamadı... İşlem kesildi...", FacesMessage.SEVERITY_ERROR);
					if (entityManager.getTransaction().isActive()) {
						entityManager.getTransaction().rollback();
					}
					return;
				}
			}
			Kullanici kullanici = new Kullanici();
			SendEmail email = new SendEmail();
			// Eposta olusturuluyor.
			String epostaHeader = "TOYS - Üye Kayıt Onayı";
			StringBuilder result = new StringBuilder(ManagedBeanLocator.locateSessionController().getSistemParametre().getUyeBildirimEpostaSonu());

			// Kullanici daha once tanimlanmis mi kontrol ediliyor
			recordCountQuery = "SELECT COUNT(o.rID) FROM " + Kullanici.class.getSimpleName() + " AS o WHERE o.kisiRef.rID=" + this.uye.getKisiRef().getRID();
			query = entityManager.createQuery(recordCountQuery);
			count = (Long) query.getSingleResult();
			if (count == 0) {
				kullanici.setKisiRef(this.uye.getKisiRef());
				String kullaniciadi = "";
				if (this.uye != null) {
					if (this.uye.getUyetip().getCode() == UyeTip._TURKUYRUKLU.getCode() || this.uye.getUyetip().getCode() == UyeTip._YABANCILAR.getCode()) {
						kullaniciadi = this.uye.getSicilno();
					} else {
						kullaniciadi = this.uye.getKisiRef().getKimlikno().toString();
					}
				} else {
					kullaniciadi = this.uye.getKisiRef().getKimlikno().toString();
				}

				kullanici.setName(kullaniciadi);
				String password = RandomObjectGenerator.generateRandomPassword(6);
				kullanici.setPassword(ScryptPasswordHashing.encrypt(password));
				kullanici.setTheme("bootstrap");
				entityManager.persist(kullanici);
				result.append("TC kimlik numaranızla veya sicil numaranızla birlikte aşağıda verilen şifreyi " + "kullanarak sisteme giriş yapabilirsiniz.\nŞifre : {2} ");
				String str = MessageFormat.format(result.toString(), new Object[] { getUye().getKisiRef().getUIStringShort(), getUye().getSicilno(), password });
				// logYaz(str);

				if (getUye().getKisiRef().getKisiInternetadres() != null) {
					email.sendEmailToRecipent(getUye().getKisiRef().getKisiInternetadres().getNetadresmetni(), epostaHeader, str);
				}
			} else {
				result.append("Üye kaydınız onaylandı. Daha önce size verilen kullanıcı bilgileriyle giriş yapabilirsiniz.");
				String str = MessageFormat.format(result.toString(), new Object[] { getUye().getKisiRef().getUIStringShort(), getUye().getSicilno() });
				logYaz(str);
				if (getUye().getKisiRef().getKisiInternetadres() != null) {
					email.sendEmailToRecipent(getUye().getKisiRef().getKisiInternetadres().getNetadresmetni(), epostaHeader, str);
				}
			}
			entityManager.getTransaction().commit();
			entityManager.close();
			createGenericMessage("Üye Onaylandı!", FacesMessage.SEVERITY_INFO);
			queryAction();
		} catch (Exception e) {
			if (entityManager.getTransaction().isActive()) {
				entityManager.getTransaction().rollback();
			}
			logYaz("ERROR @uyeKaydiOnayla of UyeController :" + e.getMessage());
			logYaz("ERROR @uyeKaydiOnayla of UyeController :" + e.getMessage());
			createGenericMessage(KeyUtil.getMessageValue("islem.hata"), FacesMessage.SEVERITY_FATAL);
		} finally {
			if (entityManager.isOpen()) {
				entityManager.close();
			}
		}

	}

	public void fileUploadListenerForBelge(FileUploadEvent event) throws Exception {
		UploadedFile item = event.getFile();
		File creationFile = createFile(item, Uyebelge.class.getSimpleName());
		getUyebelge().setPath(creationFile.getAbsolutePath());
		getUyebelge().setKurumRef(null);
	}

	public void fileUploadListenerForUyeForm(FileUploadEvent event) throws Exception {
		if (getUyeform().getFormturu() == null || getUyeform().getFormturu() == Formturu._NULL) {
			createGenericMessage("Form türü seçiniz!", FacesMessage.SEVERITY_ERROR);
			return;
		}
		UploadedFile item = event.getFile();
		File creationFile = createFile(item, Uyeform.class.getSimpleName());
		getUyeform().setPath(creationFile.getAbsolutePath());
		saveDetails();
	}

	/* GERIYE YONELIK BORCLANDIRMA */
	private boolean uyelikDurumuDegistirRendered;
	private int ay;
	private int yil;

	private Uyedurumdegistirme uyedurumdegistirme;

	public void uyeDurumDegistirmePanelAc() {
		if (getUye() == null) {
			return;
		}
		this.uyedurumdegistirme = new Uyedurumdegistirme();
		this.uyedurumdegistirme.setUyeRef(getUye());
		this.uyedurumdegistirme.setOncekidurum(getUye().getUyedurum());
		// this.uyedurumdegistirme.setTarih(new Date());
		this.uyelikDurumuDegistirRendered = true;
		// LOG ICIN
		setOldValue(getUye().getValue());
		setEditPanelRendered(false);
	}

	@Override
	public void cancel() {
		super.cancel();
		this.uyedurumdegistirme = new Uyedurumdegistirme();
		this.uyelikDurumuDegistirRendered = false;
	}

	@SuppressWarnings("unchecked")
	public void saveDurumDegisiklik() throws DBException {
		RequestContext context = RequestContext.getCurrentInstance();
		logYaz(getUyeDetay().getUIString() + " icin durum degistirme islemi yapiliyor. Eski durum :" + getUyedurumdegistirme().getOncekidurum().getLabel() + ", yeni durum :" + getUyedurumdegistirme().getYenidurum().getLabel());

		if (getUyedurumdegistirme().getYenidurum().getCode() == getUyedurumdegistirme().getOncekidurum().getCode()) {
			createGenericMessage(KeyUtil.getMessageValue("islem.degisiklikYok"), FacesMessage.SEVERITY_INFO);
			return;
		}
		if (getUyedurumdegistirme().getYenidurum() == UyeDurum._ISSIZ || getUyedurumdegistirme().getYenidurum() == UyeDurum._ASKER || getUyedurumdegistirme().getYenidurum() == UyeDurum._DOGUMIZNI
				|| getUyedurumdegistirme().getYenidurum() == UyeDurum._YURTDISI) {
			if (this.uyedurumdegistirme.getTarih().getTime() >= new Date().getTime()) {
				context.execute("alert('Sürekli olmayan muafiyet durumları ancak geçmişe dönük olarak girilebilirler!');");
				return;
			}
			if (this.uyedurumdegistirme.getBitisTarih() == null) {
				context.execute("alert('Sürekli olmayan muafiyet durumları için bitiş tarihi zorunludur!');");
				return;
			}
		}
		UyeDurum durum = this.uyedurumdegistirme.getYenidurum();
		EntityManager entityManager = getDBOperator().getEntityManagerFactory().createEntityManager();
		Query entityQuery = null;
		try {
			entityManager.getTransaction().begin();
			this.uyedurumdegistirme.setKayityapan(getSessionUser().getKullanici());
			entityManager.persist(this.uyedurumdegistirme);
			if (isLoggable()) {
				Islemlog log = logOlustur(this.uyedurumdegistirme, IslemTuru._GUNCELLEME, getOldValue());
				entityManager.persist(log);
			}
			if (durum != UyeDurum._KAPALI && !this.uyedurumdegistirme.isSurekliMuafiyet()) {
				Uyedurumdegistirme ikinciDurumDegistirme = new Uyedurumdegistirme(this.uyedurumdegistirme.getUyeRef(), this.uyedurumdegistirme.getBitisTarih(), getSessionUser().getKullanici(), this.uyedurumdegistirme.getYenidurum(),
						UyeDurum._AKTIFUYE);
				entityManager.persist(ikinciDurumDegistirme);
				if (isLoggable()) {
					Islemlog log = logOlustur(ikinciDurumDegistirme, IslemTuru._GUNCELLEME, getOldValue());
					entityManager.persist(log);
				}
			}
			if (durum == UyeDurum._VEFAT || durum == UyeDurum._ISTIFA || durum == UyeDurum._EMEKLI || durum == UyeDurum._ISSIZ || durum == UyeDurum._ASKER || durum == UyeDurum._DOGUMIZNI || durum == UyeDurum._YURTDISI
					|| durum == UyeDurum._KAPALI) {
				// UYE DURUMUNDA DEGISIKLIK YAPILMISTIR, UYE MUAFIYET KAZANMISTIR
				String whereConditionForAidatListesi = "(o.uyeRef.rID = " + getUyeDetay().getRID() + ")";
				List<Uyeaidat> aidatListesi = new ArrayList<Uyeaidat>();

				if (durum != UyeDurum._KAPALI && this.uyedurumdegistirme.isSurekliMuafiyet()) {
					whereConditionForAidatListesi += " AND ((o.ay >" + DateUtil.getMonth(this.uyedurumdegistirme.getTarih()) + " AND o.yil = " + DateUtil.getYear(this.uyedurumdegistirme.getTarih()) + ") " + " OR (o.yil > "
							+ DateUtil.getYear(this.uyedurumdegistirme.getTarih()) + "))";
					System.out.println("Where con :" + whereConditionForAidatListesi);
					entityQuery = entityManager.createQuery("SELECT OBJECT(o) FROM " + Uyeaidat.class.getSimpleName() + " AS o WHERE " + whereConditionForAidatListesi);
					aidatListesi = entityQuery.getResultList();
				} else if (durum != UyeDurum._KAPALI && !this.uyedurumdegistirme.isSurekliMuafiyet()) {
					System.out.println("Where con :" + whereConditionForAidatListesi);
					entityQuery = entityManager.createQuery("SELECT OBJECT(o) FROM " + Uyeaidat.class.getSimpleName() + " AS o " + " WHERE " + whereConditionForAidatListesi + " AND (o.ay >"
							+ DateUtil.getMonth(this.uyedurumdegistirme.getTarih()) + " AND o.yil = " + DateUtil.getYear(this.uyedurumdegistirme.getTarih()) + ")");
					aidatListesi = entityQuery.getResultList();

					entityQuery = entityManager.createQuery("SELECT OBJECT(o) FROM " + Uyeaidat.class.getSimpleName() + " AS o " + " WHERE " + whereConditionForAidatListesi + " AND (o.ay <"
							+ DateUtil.getMonth(this.uyedurumdegistirme.getBitisTarih()) + " AND o.yil = " + DateUtil.getYear(this.uyedurumdegistirme.getBitisTarih()) + ")");
					aidatListesi.addAll(entityQuery.getResultList());
				}

				if (!isEmpty(aidatListesi)) {
					// System.out.println("aidatListesi size :" + aidatListesi.size());
					Uyemuafiyet uyemuafiyet = new Uyemuafiyet();
					uyemuafiyet.setBaslangictarih(this.uyedurumdegistirme.getTarih());
					if (this.uyedurumdegistirme.getBitisTarih() != null) {
						uyemuafiyet.setBitistarih(this.uyedurumdegistirme.getBitisTarih());
					}
					uyemuafiyet.setMuafiyettip(ManagedBeanLocator.locateSessionController().uyeMuafiyetDurumuBelirle(durum));
					uyemuafiyet.setAciklama(ManagedBeanLocator.locateSessionController().aciklamaOlustur(durum));
					uyemuafiyet.setUyeRef(getUyeDetay());
					entityManager.persist(uyemuafiyet);
					BigDecimal toplamOdemeler = BigDecimal.ZERO;
					for (Uyeaidat aidat : aidatListesi) {
						aidat.setUyemuafiyetRef(uyemuafiyet);
						aidat.setUyeaidatdurum(UyeAidatDurum._MUAF);
						if (aidat.getOdenen().doubleValue() > 0) {
							toplamOdemeler = toplamOdemeler.add(aidat.getOdenen());
						}
						aidat.setOdenen(BigDecimal.ZERO);
						entityManager.merge(aidat);
					}

					// MUAFIYET GIRILEN KULLANICININ ODEMELERI VARSA SONRAKI AIDATLARINA AKTARILIYOR.
					if (toplamOdemeler.doubleValue() > 0) {
						whereConditionForAidatListesi = "(o.uyeRef.rID = " + getUyeDetay().getRID() + ") AND o.uyeaidatdurum = " + UyeAidatDurum._BORCLU.getCode();
						System.out.println("MUAFIYET GIRILEN KULLANICININ ODEMELERI VARSA SONRAKI AIDATLARINA AKTARILIYOR :" + whereConditionForAidatListesi);
						System.out.println("AKTARILACAK ODEME :" + toplamOdemeler.doubleValue());
						entityQuery = entityManager.createQuery("SELECT OBJECT(o) FROM " + Uyeaidat.class.getSimpleName() + " AS o WHERE " + whereConditionForAidatListesi + " ORDER BY o.yil, o.ay ASC");
						List<Uyeaidat> odemelerinTasinacagiAidatListesi = entityQuery.getResultList();
						if (!isEmpty(odemelerinTasinacagiAidatListesi)) {
							for (Uyeaidat aidat : odemelerinTasinacagiAidatListesi) {
								if (toplamOdemeler.doubleValue() > 0) {
									BigDecimal kalanOdeme = aidat.getKalan();
									toplamOdemeler = toplamOdemeler.subtract(kalanOdeme, mc);
									aidat.setOdenen(kalanOdeme);
									entityManager.merge(aidat);
								}
							}
						}
						// HALA FAZLA ODEMELERI VARSA O YILIN SON AYINA AKTARILACAK
						if (toplamOdemeler.doubleValue() > 0) {
							System.out.println("AKTARILACAK ODEME KALAN :" + toplamOdemeler.doubleValue());
							whereConditionForAidatListesi = "(o.uyeRef.rID = " + getUyeDetay().getRID() + ") AND o.uyemuafiyetRef IS NULL";
							entityQuery = entityManager.createQuery("SELECT OBJECT(o) FROM " + Uyeaidat.class.getSimpleName() + " AS o WHERE " + whereConditionForAidatListesi + " ORDER BY o.yil DESC, o.ay DESC");
							odemelerinTasinacagiAidatListesi = entityQuery.getResultList();
							Uyeaidat aidat = odemelerinTasinacagiAidatListesi.get(0);
							aidat.setOdenen(aidat.getOdenen().add(toplamOdemeler));
							entityManager.merge(aidat);
						}
					}
				}
				// select * from oltp.Uyedurumdegistirme order by rid desc limit 5
				//
				// select * from oltp.Uyemuafiyet order by rid desc limit 5
				//
				//
				//
				//
				// select * from oltp.uyeaidat WHERE (uyeRef = 4836) ORDER BY Yil desc, ay asc
				//
				// delete from oltp.Uyedurumdegistirme where rid > 329;
				// update oltp.uyeaidat set odenen = miktar, uyeaidatdurum = 2, uyemuafiyetref = null WHERE RID IN (218183,218182);
				// delete from oltp.uyemuafiyet WHERE uyeRef = 4836;
				// update oltp.uyeaidat set odenen = 0, uyeaidatdurum = 1, uyemuafiyetref = null WHERE RID IN (218188,218189,218190,218191,218192,218193);
				//
				entityQuery = entityManager.createQuery("SELECT OBJECT(o) FROM " + Uyemuafiyet.class.getSimpleName() + " AS o WHERE o.uyeRef.rID=" + this.uyedurumdegistirme.getUyeRef().getRID());
				if (!isEmpty(entityQuery.getResultList()) && entityQuery.getResultList().size() != 1) {
					Uyemuafiyet uyeMuafiyet1 = (Uyemuafiyet) entityQuery.getResultList().get(0);
					Calendar cal = Calendar.getInstance();
					cal.setTime(this.uyedurumdegistirme.getTarih());
					cal.add(Calendar.DAY_OF_WEEK, -1);
					uyeMuafiyet1.setBitistarih(cal.getTime());
					entityManager.merge(uyeMuafiyet1);
				}
			}
			createGenericMessage(KeyUtil.getMessageValue("kayit.guncellendi"), FacesMessage.SEVERITY_INFO);
			entityManager.getTransaction().commit();
			this.uyedurumdegistirmeGuncelle(getUyeDetay());
			setDetailEditType(null);
			context.update("uyeForm");
			context.execute("document.getElementById('detailEditForm:cancelDetailsButton').click();");
			// reloadUyeDashboard();
			reloadPage();
		} catch (Exception e) {
			if (entityManager.getTransaction().isActive()) {
				entityManager.getTransaction().rollback();
			}
			logYaz("Exception @" + getModelName() + "Controller :", e);
		} // catch
	}

	@SuppressWarnings("unchecked")
	public void saveDurumDegisiklikOld() throws DBException {
		RequestContext context = RequestContext.getCurrentInstance();
		logYaz(getUyeDetay().getUIString() + " icin durum degistirme islemi yapiliyor. Eski durum :" + getUyedurumdegistirme().getOncekidurum().getLabel() + ", yeni durum :" + getUyedurumdegistirme().getYenidurum().getLabel());

		if (getUyedurumdegistirme().getYenidurum().getCode() == getUyedurumdegistirme().getOncekidurum().getCode()) {
			createGenericMessage(KeyUtil.getMessageValue("islem.degisiklikYok"), FacesMessage.SEVERITY_INFO);
			return;
		}
		if (getUyedurumdegistirme().getYenidurum() == UyeDurum._ISSIZ || getUyedurumdegistirme().getYenidurum() == UyeDurum._ASKER || getUyedurumdegistirme().getYenidurum() == UyeDurum._DOGUMIZNI
				|| getUyedurumdegistirme().getYenidurum() == UyeDurum._YURTDISI || getUyedurumdegistirme().getYenidurum() == UyeDurum._KAPALI && this.uyedurumdegistirme.getTarih().getTime() >= new Date().getTime()) {
			System.out.println("Sürekli olmayan muafiyet durumları ancak geçmişe dönük olarak girilebilirler...");
			context.execute("alert('Sürekli olmayan muafiyet durumları ancak geçmişe dönük olarak girilebilirler!');");
			return;
		}

		EntityManager entityManager = getDBOperator().getEntityManagerFactory().createEntityManager();
		try {
			entityManager.getTransaction().begin();
			this.uyedurumdegistirme.setKayityapan(getSessionUser().getKullanici());
			getUyeDetay().setUyedurum(this.uyedurumdegistirme.getYenidurum());
			entityManager.merge(getUyeDetay());
			entityManager.persist(this.uyedurumdegistirme);
			if (isLoggable()) {
				Islemlog log = logOlustur(getUyeDetay(), IslemTuru._GUNCELLEME, getOldValue());
				entityManager.persist(log);
			}

			UyeDurum durum = getUyeDetay().getUyedurum();
			if (durum == UyeDurum._VEFAT || durum == UyeDurum._ISTIFA || durum == UyeDurum._EMEKLI || durum == UyeDurum._ISSIZ || durum == UyeDurum._ASKER || durum == UyeDurum._DOGUMIZNI || durum == UyeDurum._YURTDISI
					|| durum == UyeDurum._KAPALI) {
				// UYE DURUMUNDA DEGISIKLIK YAPILMISTIR, UYE MUAFIYET KAZANMISTIR
				String whereConditionForAidatListesi = "(o.uyeRef.rID = " + getUyeDetay().getRID() + ")";
				if (durum != UyeDurum._KAPALI) {
					whereConditionForAidatListesi += " AND ((o.ay >" + DateUtil.getMonth(this.uyedurumdegistirme.getTarih()) + " AND o.yil = " + DateUtil.getYear(this.uyedurumdegistirme.getTarih()) + ") " + " OR (o.yil > "
							+ DateUtil.getYear(this.uyedurumdegistirme.getTarih()) + "))";
				}
				// logYaz("Where con :" + whereConditionForAidatListesi);
				Query entityQuery = entityManager.createQuery("SELECT OBJECT(o) FROM " + Uyeaidat.class.getSimpleName() + " AS o WHERE " + whereConditionForAidatListesi);
				List<Uyeaidat> aidatListesi = entityQuery.getResultList();
				if (!isEmpty(aidatListesi)) {
					Uyemuafiyet uyemuafiyet = new Uyemuafiyet();
					uyemuafiyet.setBaslangictarih(this.uyedurumdegistirme.getTarih());
					if (this.uyedurumdegistirme.getBitisTarih() != null) {
						uyemuafiyet.setBitistarih(this.uyedurumdegistirme.getBitisTarih());
					}
					uyemuafiyet.setMuafiyettip(ManagedBeanLocator.locateSessionController().uyeMuafiyetDurumuBelirle(durum));
					uyemuafiyet.setAciklama(ManagedBeanLocator.locateSessionController().aciklamaOlustur(durum));
					uyemuafiyet.setUyeRef(getUyeDetay());
					entityManager.persist(uyemuafiyet);
					BigDecimal toplamOdemeler = BigDecimal.ZERO;
					for (Uyeaidat aidat : aidatListesi) {
						aidat.setUyemuafiyetRef(uyemuafiyet);
						aidat.setUyeaidatdurum(UyeAidatDurum._MUAF);
						if (aidat.getOdenen().doubleValue() > 0) {
							toplamOdemeler = toplamOdemeler.add(aidat.getOdenen());
						}
						entityManager.merge(aidat);
					}
					// MUAFIYET GIRILEN KULLANICININ ODEMELERI VARSA SONRAKI AIDATLARINA AKTARILIYOR.
					if (toplamOdemeler.doubleValue() > 0) {
						whereConditionForAidatListesi = "(o.uyeRef.rID = " + getUyeDetay().getRID() + ") AND o.miktar > o.odenen AND o.uyemuafiyetRef IS NULL";
						logYaz("MUAFIYET GIRILEN KULLANICININ ODEMELERI VARSA SONRAKI AIDATLARINA AKTARILIYOR :" + whereConditionForAidatListesi);
						entityQuery = entityManager.createQuery("SELECT OBJECT(o) FROM " + Uyeaidat.class.getSimpleName() + " AS o WHERE " + whereConditionForAidatListesi + " ORDER BY o.yil, o.ay ASC");
						List<Uyeaidat> odemelerinTasinacagiAidatListesi = entityQuery.getResultList();
						if (!isEmpty(odemelerinTasinacagiAidatListesi)) {
							for (Uyeaidat aidat : aidatListesi) {
								toplamOdemeler = toplamOdemeler.subtract(aidat.getKalan(), mc);
								aidat.setOdenen(aidat.getKalan());
								entityManager.merge(aidat);
							}
						}
						// HALA FAZLA ODEMELERI VARSA O YILIN SON AYINA AKTARILACAK
						if (toplamOdemeler.doubleValue() > 0) {
							whereConditionForAidatListesi = "(o.uyeRef.rID = " + getUyeDetay().getRID() + ") AND o.uyemuafiyetRef IS NULL";
							entityQuery = entityManager.createQuery("SELECT OBJECT(o) FROM " + Uyeaidat.class.getSimpleName() + " AS o WHERE " + whereConditionForAidatListesi + " ORDER BY o.yil, o.ay DESC");
							odemelerinTasinacagiAidatListesi = entityQuery.getResultList();
							Uyeaidat aidat = aidatListesi.get(0);
							aidat.setOdenen(toplamOdemeler);
							entityManager.merge(aidat);
						}
					}
				}
				entityQuery = entityManager.createQuery("SELECT OBJECT(o) FROM " + Uyemuafiyet.class.getSimpleName() + " AS o WHERE o.uyeRef.rID=" + this.uyedurumdegistirme.getUyeRef().getRID());
				if (!isEmpty(entityQuery.getResultList()) && entityQuery.getResultList().size() != 1) {
					Uyemuafiyet uyeMuafiyet1 = (Uyemuafiyet) entityQuery.getResultList().get(0);
					Calendar cal = Calendar.getInstance();
					cal.setTime(this.uyedurumdegistirme.getTarih());
					cal.add(Calendar.DAY_OF_WEEK, -1);
					uyeMuafiyet1.setBitistarih(cal.getTime());
					entityManager.merge(uyeMuafiyet1);
				}
			} else if (durum == UyeDurum._AKTIFUYE) {
				// UYE DURUMUNDA DEGISIKLIK YAPILMISTIR, UYE AKTIFTIR, UYENIN MUAFIYETLERI VARSA SILINECEK !!!
				String whereCon = "o.uyeRef.rID = " + getUyeDetay().getRID() + " " + " AND (o.ay >" + DateUtil.getMonth(this.uyedurumdegistirme.getTarih()) + " AND o.yil = " + DateUtil.getYear(this.uyedurumdegistirme.getTarih()) + ") "
						+ " OR (o.yil > " + DateUtil.getYear(this.uyedurumdegistirme.getTarih()) + ")";
				Query entityQuery = entityManager.createQuery("SELECT OBJECT(o) FROM " + Uyeaidat.class.getSimpleName() + " AS o WHERE " + whereCon);
				List<Uyeaidat> aidatListesi = entityQuery.getResultList();
				if (!isEmpty(aidatListesi)) {
					for (Uyeaidat aidat : aidatListesi) {
						aidat.setUyemuafiyetRef(null);
						getUyeaidatController();
						aidat = UyeaidatController.uyeAidatDurumGuncelle(aidat);
						entityManager.merge(aidat);
					}
				}

				entityQuery = entityManager.createQuery("SELECT COUNT(o) FROM " + Uyemuafiyet.class.getSimpleName() + " AS o WHERE o.uyeRef.rID=" + this.uyedurumdegistirme.getUyeRef().getRID());
				int count = ((Long) entityQuery.getSingleResult()).intValue();
				if (count > 0) {
					entityQuery = entityManager.createQuery("SELECT OBJECT(o) FROM " + Uyemuafiyet.class.getSimpleName() + " AS o WHERE o.uyeRef.rID=" + this.uyedurumdegistirme.getUyeRef().getRID() + " ORDER BY o.rID  DESC");
					Uyemuafiyet uyeMuafiyet1 = (Uyemuafiyet) entityQuery.getResultList().get(0);
					Calendar cal = Calendar.getInstance();
					cal.setTime(this.uyedurumdegistirme.getTarih());
					cal.add(Calendar.DAY_OF_WEEK, -1);
					uyeMuafiyet1.setBitistarih(cal.getTime());
					entityManager.merge(uyeMuafiyet1);
				}

			} // else if(durum == UyeDurum._AKTIFUYE )
			createGenericMessage(KeyUtil.getMessageValue("kayit.guncellendi"), FacesMessage.SEVERITY_INFO);
			entityManager.getTransaction().commit();
			this.uyedurumdegistirmeGuncelle(getUyeDetay());
			setDetailEditType(null);
			context.update("uyeForm");
			context.execute("document.getElementById('detailEditForm:cancelDetailsButton').click();");
			// reloadUyeDashboard();
			reloadPage();
		} catch (Exception e) {
			if (entityManager.getTransaction().isActive()) {
				entityManager.getTransaction().rollback();
			}
			logYaz("Exception @" + getModelName() + "Controller :", e);
		} // catch
	}

	@Override
	public void resetFilter() {
		super.resetFilterOnly();
		getUyeFilter().setUyedurum(UyeDurum._AKTIFUYE);
		if (getObjectFromSessionFilter("OgrenciOnly") != null) {
			this.uyeFilter.setUyetip(UyeTip._OGRENCI);
		} else if (getObjectFromSessionFilter("YabanciOnly") != null) {
			this.uyeFilter.setUyetip(UyeTip._YABANCILAR);
		}
		queryAction();
	}

	@Override
	public void newFilter() {
		if (getObjectFromSessionFilter("OgrenciOnly") != null) {
			this.uyeFilter.setUyetip(UyeTip._OGRENCI);
		} else if (getObjectFromSessionFilter("YabanciOnly") != null) {
			this.uyeFilter.setUyetip(UyeTip._YABANCILAR);
		}
		putObjectToSessionFilter(getModelName() + "Filter", getUyeFilter());
		super.newFilter();
	}

	public void onRowDblClckSelect(SelectEvent event) {
		setEditPanelRendered(false);
		setEntity((BaseEntity) event.getObject());
		if (getEntity() != null) {
			setSelectedRID(((Uye) getEntity()).getRID().toString());
		}
		ManagedBeanLocator.locateSessionUser().addToSessionFilters(getModelName(), getEntity());
		FacesContext ctx = FacesContext.getCurrentInstance();
		ExternalContext ectx = ctx.getExternalContext();
		try {
			ectx.redirect("uye_Detay.xhtml");
		} catch (IOException e) {
			logYaz("Exception @" + getModelName() + "Controller :", e);
		}

	}

	public void uyeKpsAdresGuncelle() {
		Adres kpsAdresGuncelle = new Adres();
		String kpsAdres = "";
		try {
			if (getDBOperator().recordCount(Adres.class.getSimpleName(), "o.kisiRef.rID=" + getUyeDetay().getKisiRef().getRID() + " AND o.adresturu=" + AdresTuru._KPSADRESI.getCode()) > 0) {
				kpsAdresGuncelle = (Adres) getDBOperator().load(Adres.class.getSimpleName(), "o.kisiRef.rID=" + getUyeDetay().getKisiRef().getRID() + " AND o.adresturu=" + AdresTuru._KPSADRESI.getCode(), "").get(0);
				KPSService kpsService = KPSService.getInstance();
				kpsAdres = kpsService.kisiAdresBilgisiGetirYeni(getUyeDetay().getKisiRef().getKimlikno());
				// PojoAdres kpsPojo = XmlHelper.readXmlStringForAdres(kpsAdres, AdresTuru._KPSADRESI);
				PojoAdres kpsPojo = ToysXMLReader.readXmlStringForAdres(kpsAdres, "YerlesimYeriAdresi");
				if (kpsPojo != null) {
					// logYaz("kpsPojo hata bilgisi kodu :" +
					// kpsPojo.getHataBilgisiKodu());
					if (kpsPojo.getHataBilgisiKodu() == null || kpsPojo.getHataBilgisiKodu().trim().equals("")) {
						kpsAdresGuncelle.setKisiRef(getUyeDetay().getKisiRef());
						kpsAdresGuncelle.setAdresturu(AdresTuru._KPSADRESI);
						kpsAdresGuncelle.setAdrestipi(kpsPojo.getAdrestipi());
						kpsAdresGuncelle.setAcikAdres(kpsPojo.getAcikAdres());
						kpsAdresGuncelle.setAdresNo(kpsPojo.getAdresNo());
						if (!isEmpty(kpsPojo.getBeyanTarihi()) && !kpsPojo.getBeyanTarihi().toString().equalsIgnoreCase("null/null/null")) {
							kpsAdresGuncelle.setBeyanTarihi(DateUtil.createDateObject(kpsPojo.getBeyanTarihi()));
						}
						kpsAdresGuncelle.setBinaAda(kpsPojo.getBinaAda());
						kpsAdresGuncelle.setBinaBlokAdi(kpsPojo.getBinaBlokAdi());
						kpsAdresGuncelle.setBinaKodu(kpsPojo.getBinaKodu());
						kpsAdresGuncelle.setBinaPafta(kpsPojo.getBinaPafta());
						kpsAdresGuncelle.setBinaParsel(kpsPojo.getBinaParsel());
						kpsAdresGuncelle.setBinaPafta(kpsPojo.getBinaPafta());
						kpsAdresGuncelle.setBinaSiteAdi(kpsPojo.getBinaSiteAdi());
						kpsAdresGuncelle.setCsbm(kpsPojo.getCsbm());
						kpsAdresGuncelle.setCsbmKodu(kpsPojo.getCsbmKodu());
						kpsAdresGuncelle.setDisKapiNo(kpsPojo.getDisKapiNo());
						kpsAdresGuncelle.setIcKapiNo(kpsPojo.getIcKapiNo());
						if (!isEmpty(kpsPojo.getIlKodu()) && isNumber(kpsPojo.getIlKodu().trim())) {
							String plakaKodu = kpsPojo.getIlKodu();
							if (plakaKodu.charAt(0) == '0') {
								plakaKodu = plakaKodu.charAt(1) + "";
							}
							String whereCon = " (o.ilKodu = '" + plakaKodu + "'";
							if (plakaKodu.length() == 1) {
								whereCon += " OR o.ilKodu = '0" + plakaKodu + "')";
							} else {
								whereCon += ")";
							}
							if (getDBOperator().recordCount(Sehir.class.getSimpleName(), whereCon) > 0) {
								kpsAdresGuncelle.setSehirRef((Sehir) getDBOperator().find(Sehir.class.getSimpleName(), whereCon));
							}
						}
						if (!isEmpty(kpsPojo.getIlceKodu()) && isNumber(kpsPojo.getIlceKodu().trim())) {
							if (getDBOperator().recordCount(Ilce.class.getSimpleName(), "o.ilcekodu='" + kpsPojo.getIlceKodu() + "'") > 0) {
								kpsAdresGuncelle.setIlceRef((Ilce) getDBOperator().find(Ilce.class.getSimpleName(), "o.ilcekodu='" + kpsPojo.getIlceKodu() + "'"));
							}
						}
						kpsAdresGuncelle.setKpsGuncellemeTarihi(new Date());
						kpsAdresGuncelle.setMahalle(kpsPojo.getMahalle());
						kpsAdresGuncelle.setMahalleKodu(kpsPojo.getMahalleKodu());
						if (kpsPojo.getTasinmaTarihi() != null && !kpsPojo.getTasinmaTarihi().toString().equalsIgnoreCase("null/null/null")) {
							kpsAdresGuncelle.setTasinmaTarihi(DateUtil.createDateObject(kpsPojo.getTasinmaTarihi()));
						}
						if (kpsPojo.getTescilTarihi() != null && !kpsPojo.getTescilTarihi().toString().equalsIgnoreCase("null/null/null")) {
							kpsAdresGuncelle.setTescilTarihi(DateUtil.createDateObject(kpsPojo.getTescilTarihi()));
						}
						kpsAdresGuncelle.setVarsayilan(EvetHayir._HAYIR);
						getDBOperator().update(kpsAdresGuncelle);

					}
				} // if (kpsPojo != null) {
			} else { // if (getDBOperator().recordCount(Adres.class.getSimpleName()) 0 ise
				KPSService kpsService = KPSService.getInstance();
				kpsAdres = kpsService.kisiAdresBilgisiGetirYeni(getUyeDetay().getKisiRef().getKimlikno());
				Kpslog kpslog = new Kpslog();
				kpslog.setKpsturu(KpsTuru._KPSADRES);
				kpslog.setKullaniciRef(getSessionUser().getKullanici());
				kpslog.setKimlikno(getUyeDetay().getKisiRef().getKimlikno());
				kpslog.setTarih(new Date());
				getDBOperator().insert(kpslog);
				PojoAdres kpsPojo = ToysXMLReader.readXmlStringForAdres(kpsAdres, "YerlesimYeriAdresi");

				// logYaz("kpsPojo hata bilgisi kodu :" +
				// kpsPojo.getHataBilgisiKodu());
				if (kpsPojo.getHataBilgisiKodu() == null || kpsPojo.getHataBilgisiKodu().trim().equals("")) {
					kpsAdresGuncelle.setKisiRef(getUyeDetay().getKisiRef());
					kpsAdresGuncelle.setAdresturu(AdresTuru._KPSADRESI);
					kpsAdresGuncelle.setAdrestipi(kpsPojo.getAdrestipi());
					kpsAdresGuncelle.setAcikAdres(kpsPojo.getAcikAdres());
					kpsAdresGuncelle.setAdresNo(kpsPojo.getAdresNo());
					if (!isEmpty(kpsPojo.getBeyanTarihi()) && !kpsPojo.getBeyanTarihi().toString().equalsIgnoreCase("null/null/null")) {
						kpsAdresGuncelle.setBeyanTarihi(DateUtil.createDateObject(kpsPojo.getBeyanTarihi()));
					}
					kpsAdresGuncelle.setBinaAda(kpsPojo.getBinaAda());
					kpsAdresGuncelle.setBinaBlokAdi(kpsPojo.getBinaBlokAdi());
					kpsAdresGuncelle.setBinaKodu(kpsPojo.getBinaKodu());
					kpsAdresGuncelle.setBinaPafta(kpsPojo.getBinaPafta());
					kpsAdresGuncelle.setBinaParsel(kpsPojo.getBinaParsel());
					kpsAdresGuncelle.setBinaPafta(kpsPojo.getBinaPafta());
					kpsAdresGuncelle.setBinaSiteAdi(kpsPojo.getBinaSiteAdi());
					kpsAdresGuncelle.setCsbm(kpsPojo.getCsbm());
					kpsAdresGuncelle.setCsbmKodu(kpsPojo.getCsbmKodu());
					kpsAdresGuncelle.setDisKapiNo(kpsPojo.getDisKapiNo());
					kpsAdresGuncelle.setIcKapiNo(kpsPojo.getIcKapiNo());
					if (!isEmpty(kpsPojo.getIlKodu()) && isNumber(kpsPojo.getIlKodu().trim())) {
						String plakaKodu = kpsPojo.getIlKodu();
						if (plakaKodu.charAt(0) == '0') {
							plakaKodu = plakaKodu.charAt(1) + "";
						}
						String whereCon = " (o.ilKodu = '" + plakaKodu + "'";
						if (plakaKodu.length() == 1) {
							whereCon += " OR o.ilKodu = '0" + plakaKodu + "')";
						} else {
							whereCon += ")";
						}

						if (getDBOperator().recordCount(Sehir.class.getSimpleName(), whereCon) > 0) {
							kpsAdresGuncelle.setSehirRef((Sehir) getDBOperator().find(Sehir.class.getSimpleName(), whereCon));
						}
					}
					if (!isEmpty(kpsPojo.getIlceKodu()) && isNumber(kpsPojo.getIlceKodu().trim())) {
						if (getDBOperator().recordCount(Ilce.class.getSimpleName(), "o.ilcekodu='" + kpsPojo.getIlceKodu() + "'") > 0) {
							kpsAdresGuncelle.setIlceRef((Ilce) getDBOperator().find(Ilce.class.getSimpleName(), "o.ilcekodu='" + kpsPojo.getIlceKodu() + "'"));
						}
					}
					kpsAdresGuncelle.setKpsGuncellemeTarihi(new Date());
					kpsAdresGuncelle.setMahalle(kpsPojo.getMahalle());
					kpsAdresGuncelle.setMahalleKodu(kpsPojo.getMahalleKodu());
					if (kpsPojo.getTasinmaTarihi() != null && !kpsPojo.getTasinmaTarihi().toString().equalsIgnoreCase("null/null/null")) {
						kpsAdresGuncelle.setTasinmaTarihi(DateUtil.createDateObject(kpsPojo.getTasinmaTarihi()));
					}
					if (kpsPojo.getTescilTarihi() != null && !kpsPojo.getTescilTarihi().toString().equalsIgnoreCase("null/null/null")) {
						kpsAdresGuncelle.setTescilTarihi(DateUtil.createDateObject(kpsPojo.getTescilTarihi()));
					}
					kpsAdresGuncelle.setVarsayilan(EvetHayir._EVET);
					getDBOperator().insert(kpsAdresGuncelle);
					Adres evadresi = new Adres();
					evadresi = (Adres) kpsAdresGuncelle.cloneObject();
					evadresi.setRID(null);
					evadresi.setVarsayilan(EvetHayir._HAYIR);
					evadresi.setAdresturu(AdresTuru._EVADRESI);
					getDBOperator().insert(evadresi);
					this.adresController.queryAction();
				}
			}
			createGenericMessage("KPS Adres Bilgileriniz Güncellendi!", FacesMessage.SEVERITY_INFO);
		} catch (DBException e) {
			logYaz("Exception @" + getModelName() + "Controller :", e);
			createGenericMessage("Hata meydana geldi!Bu hata devam ederse lütfen sistem yöneticisi ile irtibata geçiniz!", FacesMessage.SEVERITY_INFO);
		} catch (RemoteException e) {
			logYaz("Exception @" + getModelName() + "Controller :", e);
			createGenericMessage("KPS Adres güncellemede hata meydana geldi!Bu hata devam ederse lütfen sistem yöneticisi ile irtibata geçiniz!", FacesMessage.SEVERITY_INFO);
		} catch (ParseException e) {
			logYaz("Exception @" + getModelName() + "Controller :", e);
			createGenericMessage("KPS Adres güncellemede hata meydana geldi!Bu hata devam ederse lütfen sistem yöneticisi ile irtibata geçiniz!", FacesMessage.SEVERITY_INFO);
		} catch (Exception e) {
			logYaz("Exception @" + getModelName() + "Controller :", e);
			createGenericMessage("KPS Adres güncellemede hata meydana geldi!Bu hata devam ederse lütfen sistem yöneticisi ile irtibata geçiniz!", FacesMessage.SEVERITY_INFO);
		}
		this.adresController.queryAction();
		this.detailEditType = null;
	}

	@SuppressWarnings("unchecked")
	public void sifregonder() {
		List<Kullanici> kullaniciList = new ArrayList<Kullanici>();
		try {
			if (getDBOperator().recordCount(ApplicationDescriptor._KULLANICI_MODEL, "o.kisiRef.rID=" + getUyeDetay().getKisiRef().getRID()) > 0) {
				kullaniciList = getDBOperator().load(ApplicationDescriptor._KULLANICI_MODEL, "o.kisiRef.rID=" + getUyeDetay().getKisiRef().getRID(), "o.name");
			}

			if (kullaniciList != null && kullaniciList.size() > 0) {
				Kullanici kullanici = kullaniciList.get(0);
				String password = RandomObjectGenerator.generateRandomPassword(6);
				kullanici.setPassword(ScryptPasswordHashing.encrypt(password));
				getDBOperator().update(kullanici);
				SendEmail email = new SendEmail();
				if (getDBOperator().recordCount(Internetadres.class.getSimpleName(), "o.kisiRef.rID=" + kullanici.getKisiRef().getRID() + " AND o.varsayilan=" + EvetHayir._EVET.getCode()) > 0) {
					Internetadres varsayilanInternetAdd = (Internetadres) getDBOperator().load(Internetadres.class.getSimpleName(), "o.kisiRef.rID=" + kullanici.getKisiRef().getRID() + " AND o.varsayilan=" + EvetHayir._EVET.getCode(), "")
							.get(0);
					email.sendEmailToRecipent(varsayilanInternetAdd.getNetadresmetni(), "TOYS Şifre Yenileme", "Şifre:" + password + "");
					createGenericMessage("Şifreniz Mail Adresinize Gönderildi! ", FacesMessage.SEVERITY_INFO);
				} else {
					createGenericMessage("Mail Adresiniz Bulunmadığından Dolayı Şifre Alamazsınız!" + "Şifre Almak İçin En Yakınınızdaki Oda Birimiyle İletişime Geçmelisiniz!", FacesMessage.SEVERITY_INFO);
				}
			}
		} catch (DBException e) {
			logYaz("Error @sifregonder of UyeController : " + e.getMessage());
		}
	}

	public String uyeListesineDon() {
		if (getObjectFromSessionFilter("OgrenciOnly") != null) {
			this.uyeFilter.setUyetip(UyeTip._OGRENCI);
			ManagedBeanLocator.locateSessionUser().clearSessionFilters();
			return ManagedBeanLocator.locateMenuController().gotoPage("menu_OgrenciUyeListesi");
		} else if (getObjectFromSessionFilter("YabanciOnly") != null) {
			this.uyeFilter.setUyetip(UyeTip._YABANCILAR);
			ManagedBeanLocator.locateSessionUser().clearSessionFilters();
			return ManagedBeanLocator.locateMenuController().gotoPage("menu_YabanciUyeListesi");
		} else {
			// this.uyeFilter.setSadeceUyeler(true);
			return ManagedBeanLocator.locateMenuController().gotoPage("menu_Uye");
		}
	}

	@Override
	public void fileUploadListener(FileUploadEvent event) throws Exception {
		File creationFile = super.createFile(event.getFile(), Kisifotograf.class.getSimpleName());
		this.kisifotograf.setPath(creationFile.getName());
		saveDetails();
	}

	public void yilSorgula() {
		if (getUyeDetay() == null) {
			return;
		}
		RequestContext context = RequestContext.getCurrentInstance();
		if (getYilFiltreleme() != 0) {
			this.uyeaidatController.getUyeaidatFilter().clearQueryCriterias();
			this.uyeaidatController.setTable(null);
			this.uyeaidatController.getUyeaidatFilter().setUyeRef(getUyeDetay());
			this.uyeaidatController.getUyeaidatFilter().setYil(getYilFiltreleme());
			this.uyeaidatController.getUyeaidatFilter().createQueryCriterias();
			this.uyeaidatController.setListSize(12);
			this.uyeaidatController.queryAction();
			updateDialogAndForm(context);

		}
	}

	Calendar cal = Calendar.getInstance();
	int aidatyili = cal.get(Calendar.YEAR);
	int kacaylikaidatborc = 0;
	private int aidatmiktar = 0;
	private int aidatodenenmiktar = 0;
	private int toplam = 0;

	@SuppressWarnings("unchecked")
	public void uyeaidatyilSorgula() {
		setHashlendimi(false);
		int flag = 0;

		if (getUyeDetay() == null) {
			return;
		}
		RequestContext context = RequestContext.getCurrentInstance();
		if (getYilFiltreleme() != 0) {
			kacaylikaidatborc = 0;
			aidatodenenmiktar = 0;
			aidatmiktar = 0;
			toplam = 0;
			this.uyeaidatController.getUyeaidatFilter().clearQueryCriterias();
			this.uyeaidatController.setTable(null);
			this.uyeaidatController.getUyeaidatFilter().setUyeRef(getUyeDetay());
			this.uyeaidatController.getUyeaidatFilter().setYil(getYilFiltreleme());
			this.uyeaidatController.getUyeaidatFilter().createQueryCriterias();
			this.uyeaidatController.setListSize(12);
			this.uyeaidatController.queryAction();
			try {
				if (getDBOperator().recordCount(Uyeaidat.class.getSimpleName(), "o.uyeRef.rID=" + getUyeDetay().getRID() + " AND o.yil=" + getYilFiltreleme()) > 0) {
					aidatyili = getYilFiltreleme();
					List<Uyeaidat> uyeaidatListesi = getDBOperator().load(Uyeaidat.class.getSimpleName(), "o.uyeRef.rID=" + getUyeDetay().getRID() + " AND o.yil=" + getYilFiltreleme(), "");
					for (Uyeaidat entity : uyeaidatListesi) {
						aidatmiktar = entity.getMiktar().intValue();
						kacaylikaidatborc++;
						toplam += entity.getMiktar().intValue();
						flag = entity.getOdenen().intValue();
						if (flag != 0) {
							kacaylikaidatborc--;
							aidatodenenmiktar += entity.getOdenen().intValue();
						}
					}
				} else {
					createGenericMessage(getYilFiltreleme() + "yılına ait borcunuz bulunmamaktadır!", FacesMessage.SEVERITY_INFO);
					aidatyili = getYilFiltreleme();
					kacaylikaidatborc = 0;
				}
			} catch (DBException e) {
				logYaz("Exception @" + getModelName() + "Controller :", e);
			}
			toplam -= aidatodenenmiktar;
			if (context != null) {
				context.update("dashboardForm");
				updateDialogAndForm(context);
			}
		}
	}

	private BigDecimal miktar = new BigDecimal(0);

	// Yapi kredi ve is bankasi entegrasyon
	public void uyeAidatYuklemesayfasi(String detailType) {
		detailType = detailType.toLowerCase(Locale.ENGLISH);
		this.detailEditType = detailType;
		RequestContext context = RequestContext.getCurrentInstance();

		UyeSiparis siparis = new UyeSiparis();
		siparis.setUyeRef(getUyeDetay());
		try {
			if (detailType.equalsIgnoreCase("aidatodeme")) {
				putObjectToSessionFilter("detaileditType", this.detailEditType);
				context.update("dialogUpdateBox");
				this.aidatodeme = new Odeme();
				this.oldValueForAidatOdeme = "";
				this.aidatodeme.setKisiRef(getUyeDetay().getKisiRef());
				Calendar cal = Calendar.getInstance();
				this.aidatodeme.setTarih(cal.getTime());

				Object maxNum = getDBOperator().recordCountByNativeQuery("SELECT COALESCE(MAX(CAST(SIPARISNO AS BIGINT)),0) FROM oltp." + ApplicationDescriptor._UYESIPARIS_MODEL + "" + " WHERE uyeRef= " + siparis.getUyeRef().getRID());
				BigInteger max = (BigInteger) maxNum;
				siparis.setSiparisno(StringUtil.fillStringLeft(max.add(new BigInteger("1")) + "", "0", 20));
				siparis.setDurum(SiparisDurum._YANITBEKLIYOR);
				siparis.setTutar(new BigDecimal(0));
				setSiparis(siparis);
				// logYaz("Siparis numarasi " + siparis.getSiparisno() + " \n ");
			} else if (detailType.equalsIgnoreCase("aidatodemeisbankasi")) {
				this.aidatodemeisbankasi = new Odeme();
				this.oldValueForAidatOdeme = "";
				this.aidatodemeisbankasi.setKisiRef(getUyeDetay().getKisiRef());
				Calendar cal = Calendar.getInstance();
				this.aidatodemeisbankasi.setTarih(cal.getTime());
				this.aidatodemeisbankasi.setOdemeTuru(OdemeTuru._AIDAT);
				Object maxNum = getDBOperator().recordCountByNativeQuery("SELECT COALESCE(MAX(CAST(SIPARISNO AS BIGINT)),0) FROM oltp." + ApplicationDescriptor._UYESIPARIS_MODEL + "" + " WHERE uyeRef= " + siparis.getUyeRef().getRID());
				BigInteger max = (BigInteger) maxNum;
				siparis.setSiparisno(StringUtil.fillStringLeft(max.add(new BigInteger("1")) + "", "0", 13));
				siparis.setDurum(SiparisDurum._YANITBEKLIYOR);
				// logYaz("Miktar ="
				// +this.aidatodemeisbankasi.getMiktar());
				if (this.aidatodemeisbankasi.getMiktar() != null) {
					siparis.setTutar(this.aidatodemeisbankasi.getMiktar());
				} else {
					siparis.setTutar(new BigDecimal(0));
				}
				setSiparis(siparis);
			}
			getDBOperator().insert(siparis);
			this.siparisSonuc = new Siparis();
			siparisSonuc.setSiparisNo(getSiparis().getSiparisno());
		} catch (Exception e) {
			logYaz("Exception @" + getModelName() + "Controller :", e);
		}
	}

	public void isBankasinaGonder(String detailType) {
		detailType = detailType.toLowerCase(Locale.ENGLISH);
		this.detailEditType = detailType;
		if (getUyeDetay() == null) {
			return;
		}
		UyeSiparis siparis = new UyeSiparis();
		siparis.setUyeRef(getUyeDetay());
		try {
			if (detailType.equalsIgnoreCase("aidatodemeisbankasi")) {
				this.oldValueForAidatOdeme = "";
				this.aidatodemeisbankasi.setKisiRef(getUyeDetay().getKisiRef());
				Calendar cal = Calendar.getInstance();
				this.aidatodemeisbankasi.setTarih(cal.getTime());
				this.aidatodemeisbankasi.setOdemeTuru(OdemeTuru._AIDAT);
				Object maxNum;
				maxNum = getDBOperator().recordCountByNativeQuery("SELECT COALESCE(MAX(CAST(SIPARISNO AS BIGINT)),0) FROM oltp." + ApplicationDescriptor._UYESIPARIS_MODEL + "" + " WHERE uyeRef= " + siparis.getUyeRef().getRID());
				BigInteger max = (BigInteger) maxNum;
				siparis.setSiparisno(StringUtil.fillStringLeft(max.add(new BigInteger("1")) + "", "0", 13));
				setSiparisno(siparis.getSiparisno());
				siparis.setDurum(SiparisDurum._YANITBEKLIYOR);
				// logYaz("Miktar ="
				// +this.aidatodemeisbankasi.getMiktar());
				if (this.aidatodemeisbankasi.getMiktar() != null) {
					siparis.setTutar(this.aidatodemeisbankasi.getMiktar());
					setMiktar(this.aidatodemeisbankasi.getMiktar());
					getDBOperator().insert(siparis);
				} else {
					siparis.setTutar(new BigDecimal(0));
					setMiktar(new BigDecimal(0));
					getDBOperator().insert(siparis);
				}
				setSiparis(siparis);
			}
			this.siparisSonuc = new Siparis();
			siparisSonuc.setSiparisNo(getSiparis().getSiparisno());
			setHashlendimi(true);
		} catch (Exception e) {
			logYaz("Exception @" + getModelName() + "Controller :", e);
		}
		// ManagedBeanLocator.locateSessionUser().addToSessionFilters(getModelName(),getUyeDetay());
		// return "isbankasigondermeformu.do";
	}

	private String siparisno = "";

	public String getSiparisno() {
		return siparisno;
	}

	public void setSiparisno(String siparisno) {
		this.siparisno = siparisno;
	}

	// Banka entegrasyon sonu

	public Adres getIsyeriadres() {
		if (getUyeDetay() == null) {
			return null;
		} else {
			try {
				if (getDBOperator().recordCount(Adres.class.getSimpleName(), "o.kisiRef.rID=" + getUyeDetay().getKisiRef().getRID() + " AND o.adresturu=" + AdresTuru._ISADRESI.getCode()) > 0) {
					Adres adres = (Adres) getDBOperator().load(Adres.class.getSimpleName(), "o.kisiRef.rID=" + getUyeDetay().getKisiRef().getRID() + " AND o.adresturu=" + AdresTuru._ISADRESI.getCode(), "").get(0);
					return adres;
				}
			} catch (DBException e) {
				logYaz("Exception @" + getModelName() + "Controller :", e);
			}
		}
		return null;
	}

	public Telefon getIstelefon() {
		if (getUyeDetay() == null) {
			return null;
		} else {
			try {
				if (getDBOperator().recordCount(Telefon.class.getSimpleName(), "o.kisiRef.rID=" + getUyeDetay().getKisiRef().getRID() + " AND o.telefonturu=" + TelefonTuru._ISTELEFONU.getCode()) > 0) {
					Telefon telefon = (Telefon) getDBOperator().load(Telefon.class.getSimpleName(), "o.kisiRef.rID=" + getUyeDetay().getKisiRef().getRID() + " AND o.telefonturu=" + TelefonTuru._ISTELEFONU.getCode(), "").get(0);
					return telefon;
				}
			} catch (DBException e) {
				logYaz("Exception @" + getModelName() + "Controller :", e);
			}
		}
		return null;
	}

	public Adres getEvadresi() {
		if (getUyeDetay() == null) {
			return null;
		} else {
			try {
				if (getDBOperator().recordCount(Adres.class.getSimpleName(), "o.kisiRef.rID=" + getUyeDetay().getKisiRef().getRID() + " AND o.adresturu=" + AdresTuru._EVADRESI.getCode()) > 0) {
					Adres adres = (Adres) getDBOperator().load(Adres.class.getSimpleName(), "o.kisiRef.rID=" + getUyeDetay().getKisiRef().getRID() + " AND o.adresturu=" + AdresTuru._EVADRESI.getCode(), "").get(0);
					return adres;
				}
			} catch (DBException e) {
				logYaz("Exception @" + getModelName() + "Controller :", e);
			}
		}
		return null;
	}

	public Telefon getEvtelefon() {
		if (getUyeDetay() == null) {
			return null;
		} else {
			try {
				if (getDBOperator().recordCount(Telefon.class.getSimpleName(), "o.kisiRef.rID=" + getUyeDetay().getKisiRef().getRID() + " AND o.telefonturu=" + TelefonTuru._EVTELEFONU.getCode()) > 0) {
					Telefon telefon = (Telefon) getDBOperator().load(Telefon.class.getSimpleName(), "o.kisiRef.rID=" + getUyeDetay().getKisiRef().getRID() + " AND o.telefonturu=" + TelefonTuru._EVTELEFONU.getCode(), "").get(0);
					return telefon;
				}
			} catch (DBException e) {
				logYaz("Exception @" + getModelName() + "Controller :", e);
			}
		}
		return null;
	}

	public String uyedurumdegistirme() {
		if (!setSelected()) {
			return "";
		}
		getSessionUser().setLegalAccess(1);
		putObjectToSessionFilter(ApplicationDescriptor._UYE_MODEL, getUye());
		return ManagedBeanLocator.locateMenuController().gotoPage("menu_Uyedurumdegistirme");
	}

	// todo kimlik bilgilerinin onaylandigi kisim
	public void kimlikBilgileriKontroluOnay() {
		Date today = new Date();
		this.uye.setKartTalepTarihi(today);
		this.uye.setTeslimAdresi(this.teslimAdresi);
		try {
			getDBOperator().update(this.uye);
		} catch (DBException exc) {
			logYaz("KART BILGISI ONAYLANAMADI!", exc);
		}
		logYaz("KART BILGISI ONAYLANDI!");
		this.detailEditType = null;
		redirectPage("/faces/_page/_main/uyedashboard.xhtml");
	}

	/* UYE EKLEME SAYFASI */
	@Override
	public void insert() {
		if (getSessionUser().getPersonelRef() == null || getSessionUser().getPersonelRef().getBirimRef() == null) {
			return;
		}
		super.insert();
		getUye().setIletisimEmail(EvetHayir._HAYIR);
		getUye().setIletisimPosta(EvetHayir._HAYIR);
		getUye().setIletisimSms(EvetHayir._HAYIR);
		getUye().setKayitdurum(UyeKayitDurum._INCELEMEDE);
		getUye().setBirimRef(getSessionUser().getPersonelRef().getBirimRef());
	}

	public String gotoOdeme() {
		Odeme odeme = new Odeme();
		odeme.setKisiRef(this.uyeDetay.getKisiRef());
		ManagedBeanLocator.locateSessionUser().addToSessionFilters(getModelName(), getEntity());
		return ManagedBeanLocator.locateMenuController().gotoPage("menu_Odeme");
	}

	public String gotoUyeDetay(Long selectedRID) {
		if (selectedRID != null && selectedRID > 0) {
			setSelectedRID(selectedRID + "");
		} else if (getSelectedRID() == null) {
			return "";
		}
		ManagedBeanLocator.locateSessionUser().addToSessionFilters(getModelName(), getEntity());
		return ManagedBeanLocator.locateMenuController().gotoPage("menu_" + getModelName() + "Detay");
	}

	public String eposta() {
		getSessionUser().setLegalAccess(1);
		putObjectToSessionFilter(ApplicationDescriptor._UYE_MODEL, getUyeDetay());
		return ManagedBeanLocator.locateMenuController().gotoPage("menu_Kisianlikeposta");
	}

	public String sms() {

		getSessionUser().setLegalAccess(1);
		putObjectToSessionFilter(ApplicationDescriptor._UYE_MODEL, getUyeDetay());
		return ManagedBeanLocator.locateMenuController().gotoPage("menu_Kisianliksms");
	}

	public String getYapiKrediPosUrl() {
		if (getDBOperator().recordCount(SistemParametre.class.getSimpleName()) > 0) {
			SistemParametre param = (SistemParametre) getDBOperator().load(SistemParametre.class.getSimpleName()).get(0);
			if (param.getYapikrediaktif() == EvetHayir._EVET && param.getYapikrediposadres() != null) {
				return param.getYapikrediposadres();
			} else {
				if (ManagedBeanLocator.locateApplicationController().getSistemParametre().getYapikrediaktif() == EvetHayir._EVET) {
					getSessionUser().setMessage("Yapi Kredi pos adresi yok! Hata oluştu!Lütfen ilgili birimi arayıp hatayı bildiriniz!");
					createGenericMessage("Yapi Kredi pos adresi yok! Hata oluştu!Lütfen ilgili birimi arayıp hatayı bildiriniz!", FacesMessage.SEVERITY_INFO);
				}
			}
		} else {
			createGenericMessage("Hata oluştu!Lütfen ilgili birimi arayıp hatayı bildiriniz!", FacesMessage.SEVERITY_INFO);
		}
		return null;
	}

	public Boolean getIsBankasiAktif() {
		if (getDBOperator().recordCount(SistemParametre.class.getSimpleName()) > 0) {
			SistemParametre param = (SistemParametre) getDBOperator().load(SistemParametre.class.getSimpleName()).get(0);
			if (param.getIsbankasiaktif() == EvetHayir._EVET) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}

	}

	public Boolean getGarantiAktif() {
		if (getDBOperator().recordCount(SistemParametre.class.getSimpleName()) > 0) {
			SistemParametre param = (SistemParametre) getDBOperator().load(SistemParametre.class.getSimpleName()).get(0);
			if (param.getGarantiaktif() == EvetHayir._EVET) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}

	}

	public boolean garantiOdemeDugmesiniGoster() {
		String odeyenKullanici = FacesContext.getCurrentInstance().getExternalContext().getInitParameter("odeyenKullanici");
		if (odeyenKullanici == null) {
			logger.debug("odeyenKullanici parametresi yok");
			return true;
		}
		SessionUser sessionUser = getSessionUser();
		if (sessionUser != null) {
			Uye uye = sessionUser.getUyeRef();
			if (uye != null) {
				String name = uye.getSicilno();
				boolean odemeYapabilir = odeyenKullanici.equals(name);
				logger.debug("odemeYapabilir = " + odemeYapabilir);
				return odemeYapabilir;
			}
		}
		logger.debug("kullanici olmadigi icin true donduruyorum");
		return false;
	}

	public Boolean getYapiKrediAktif() {
		if (getDBOperator().recordCount(SistemParametre.class.getSimpleName()) > 0) {
			SistemParametre param = (SistemParametre) getDBOperator().load(SistemParametre.class.getSimpleName()).get(0);
			if (param.getYapikrediaktif() == EvetHayir._EVET) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}

	}

	public OdemeTuru getBorcOdemeTuru() {
		this.uyeodeme.setOdemeTuru(OdemeTuru._BORC);
		return OdemeTuru._BORC;
	}

	public OdemeTuru getAidatOdemeTuru() {
		this.aidatodeme.setOdemeTuru(OdemeTuru._AIDAT);
		return OdemeTuru._AIDAT;
	}

	private List<String> selectedOptions;

	public List<String> getSelectedOptions() {
		return selectedOptions;
	}

	public void setSelectedOptions(List<String> selectedOptions) {
		this.selectedOptions = selectedOptions;
	}

	public Siparis getSiparisSonuc() {
		return siparisSonuc;
	}

	public void setSiparisSonuc(Siparis siparisSonuc) {
		this.siparisSonuc = siparisSonuc;
	}

	private UyeSiparis siparis;

	public UyeSiparis getSiparis() {
		return siparis;
	}

	public void setSiparis(UyeSiparis siparis) {
		this.siparis = siparis;
	}

	private String nowdate = new java.util.Date().toString();

	public String getNowdate() {
		return nowdate;
	}

	public void setNowdate(String nowdate) {
		this.nowdate = nowdate;
	}

	public String getOldValueForKisiFotograf() {
		return oldValueForKisiFotograf;
	}

	public void setOldValueForKisiFotograf(String oldValueForKisiFotograf) {
		this.oldValueForKisiFotograf = oldValueForKisiFotograf;
	}

	public boolean isUyelikDurumuDegistirRendered() {
		return uyelikDurumuDegistirRendered;
	}

	public void setUyelikDurumuDegistirRendered(boolean uyelikDurumuDegistirRendered) {
		this.uyelikDurumuDegistirRendered = uyelikDurumuDegistirRendered;
	}

	public int getAy() {
		return ay;
	}

	public void setAy(int ay) {
		this.ay = ay;
	}

	public int getYil() {
		return yil;
	}

	public void setYil(int yil) {
		this.yil = yil;
	}

	public DataTable getTable2() {
		return table2;
	}

	public void setTable2(DataTable table2) {
		this.table2 = table2;
	}

	public OdemeController getAidatodemeisbankasiController() {
		return aidatodemeisbankasiController;
	}

	public void setAidatodemeisbankasiController(OdemeController aidatodemeisbankasiController) {
		this.aidatodemeisbankasiController = aidatodemeisbankasiController;
	}

	public Odeme getAidatodemeisbankasi() {
		return aidatodemeisbankasi;
	}

	public void setAidatodemeisbankasi(Odeme aidatodemeisbankasi) {
		this.aidatodemeisbankasi = aidatodemeisbankasi;
	}

	public String getOldValueForAidatOdemeIsbankasi() {
		return oldValueForAidatOdemeIsbankasi;
	}

	public void setOldValueForAidatOdemeIsbankasi(String oldValueForAidatOdemeIsbankasi) {
		this.oldValueForAidatOdemeIsbankasi = oldValueForAidatOdemeIsbankasi;
	}

	public Uyedurumdegistirme getUyedurumdegistirme() {
		return uyedurumdegistirme;
	}

	public void setUyedurumdegistirme(Uyedurumdegistirme uyedurumdegistirme) {
		this.uyedurumdegistirme = uyedurumdegistirme;
	}

	public UyedurumdegistirmeController getUyedurumdegistirmeController() {
		return uyedurumdegistirmeController;
	}

	public void setUyedurumdegistirmeController(UyedurumdegistirmeController uyedurumdegistirmeController) {
		this.uyedurumdegistirmeController = uyedurumdegistirmeController;
	}

	public KisifotografController getKisifotografController() {
		return kisifotografController;
	}

	public void setKisifotografController(KisifotografController kisifotografController) {
		this.kisifotografController = kisifotografController;
	}

	public Kisifotograf getKisifotograf() {
		return kisifotograf;
	}

	public void setKisifotograf(Kisifotograf kisifotograf) {
		this.kisifotograf = kisifotograf;
	}

	public Uye getUyeDetay() {
		return uyeDetay;
	}

	public void setUyeDetay(Uye uyeDetay) {
		this.uyeDetay = uyeDetay;
	}

	public String getSelectedTab() {
		return selectedTab;
	}

	public void setSelectedTab(String selectedTab) {
		this.selectedTab = selectedTab;
	}

	public UyebelgeController getUyebelgeController() {
		return uyebelgeController;
	}

	public void setUyebelgeController(UyebelgeController uyebelgeController) {
		this.uyebelgeController = uyebelgeController;
	}

	public UyeuzmanlikController getUyeuzmanlikController() {
		return uyeuzmanlikController;
	}

	public void setUyeuzmanlikController(UyeuzmanlikController uyeuzmanlikController) {
		this.uyeuzmanlikController = uyeuzmanlikController;
	}

	public UyebilirkisiController getUyebilirkisiController() {
		return uyebilirkisiController;
	}

	public void setUyebilirkisiController(UyebilirkisiController uyebilirkisiController) {
		this.uyebilirkisiController = uyebilirkisiController;
	}

	public UyecezaController getUyecezaController() {
		return uyecezaController;
	}

	public void setUyecezaController(UyecezaController uyecezaController) {
		this.uyecezaController = uyecezaController;
	}

	public UyeodulController getUyeodulController() {
		return uyeodulController;
	}

	public void setUyeodulController(UyeodulController uyeodulController) {
		this.uyeodulController = uyeodulController;
	}

	public EtkinlikkuruluyeController getEtkinlikkuruluyeController() {
		return etkinlikkuruluyeController;
	}

	public void setEtkinlikkuruluyeController(EtkinlikkuruluyeController etkinlikkuruluyeController) {
		this.etkinlikkuruluyeController = etkinlikkuruluyeController;
	}

	public UyeaboneController getUyeaboneController() {
		return uyeaboneController;
	}

	public void setUyeaboneController(UyeaboneController uyeaboneController) {
		this.uyeaboneController = uyeaboneController;
	}

	public UyesigortaController getUyesigortaController() {
		return uyesigortaController;
	}

	public void setUyesigortaController(UyesigortaController uyesigortaController) {
		this.uyesigortaController = uyesigortaController;
	}

	public UyeaidatController getUyeaidatController() {
		return uyeaidatController;
	}

	public void setUyeaidatController(UyeaidatController uyeaidatController) {
		this.uyeaidatController = uyeaidatController;
	}

	public UyemuafiyetController getUyemuafiyetController() {
		return uyemuafiyetController;
	}

	public void setUyemuafiyetController(UyemuafiyetController uyemuafiyetController) {
		this.uyemuafiyetController = uyemuafiyetController;
	}

	public BorcController getBorcController() {
		return borcController;
	}

	public void setBorcController(BorcController borcController) {
		this.borcController = borcController;
	}

	public String getDetailEditType() {
		return detailEditType;
	}

	public void setDetailEditType(String detailEditType) {
		this.detailEditType = detailEditType;
	}

	public Uyebelge getUyebelge() {
		return uyebelge;
	}

	public void setUyebelge(Uyebelge uyebelge) {
		this.uyebelge = uyebelge;
	}

	public Uyeform getUyeform() {
		return uyeform;
	}

	public void setUyeform(Uyeform uyeform) {
		this.uyeform = uyeform;
	}

	public Uyeuzmanlik getUyeuzmanlik() {
		return uyeuzmanlik;
	}

	public void setUyeuzmanlik(Uyeuzmanlik uyeuzmanlik) {
		this.uyeuzmanlik = uyeuzmanlik;
	}

	public Uyebilirkisi getUyebilirkisi() {
		return uyebilirkisi;
	}

	public void setUyebilirkisi(Uyebilirkisi uyebilirkisi) {
		this.uyebilirkisi = uyebilirkisi;
	}

	public KisiogrenimController getKisiogrenimController() {
		return kisiogrenimController;
	}

	public void setKisiogrenimController(KisiogrenimController kisiogrenimController) {
		this.kisiogrenimController = kisiogrenimController;
	}

	public Kisiogrenim getKisiogrenim() {
		return kisiogrenim;
	}

	public void setKisiogrenim(Kisiogrenim kisiogrenim) {
		this.kisiogrenim = kisiogrenim;
	}

	public Uyeceza getUyeceza() {
		return uyeceza;
	}

	public void setUyeceza(Uyeceza uyeceza) {
		this.uyeceza = uyeceza;
	}

	public Uyeodul getUyeodul() {
		return uyeodul;
	}

	public void setUyeodul(Uyeodul uyeodul) {
		this.uyeodul = uyeodul;
	}

	public Etkinlikkuruluye getEtkinlikkuruluye() {
		return etkinlikkuruluye;
	}

	public void setEtkinlikkuruluye(Etkinlikkuruluye etkinlikkuruluye) {
		this.etkinlikkuruluye = etkinlikkuruluye;
	}

	public Uyeabone getUyeabone() {
		return uyeabone;
	}

	public void setUyeabone(Uyeabone uyeabone) {
		this.uyeabone = uyeabone;
	}

	public Uyesigorta getUyesigorta() {
		return uyesigorta;
	}

	public void setUyesigorta(Uyesigorta uyesigorta) {
		this.uyesigorta = uyesigorta;
	}

	public Uyeaidat getUyeaidat() {
		return uyeaidat;
	}

	public void setUyeaidat(Uyeaidat uyeaidat) {
		this.uyeaidat = uyeaidat;
	}

	public Uyemuafiyet getUyemuafiyet() {
		return uyemuafiyet;
	}

	public void setUyemuafiyet(Uyemuafiyet uyemuafiyet) {
		this.uyemuafiyet = uyemuafiyet;
	}

	public Borc getUyeborc() {
		return uyeborc;
	}

	public void setUyeborc(Borc uyeborc) {
		this.uyeborc = uyeborc;
	}

	public String getOldValueForUyedurumdegistirme() {
		return oldValueForUyedurumdegistirme;
	}

	public void setOldValueForUyedurumdegistirme(String oldValueForUyedurumdegistirme) {
		this.oldValueForUyedurumdegistirme = oldValueForUyedurumdegistirme;
	}

	public String getOldValueForUyebelge() {
		return oldValueForUyebelge;
	}

	public void setOldValueForUyebelge(String oldValueForUyebelge) {
		this.oldValueForUyebelge = oldValueForUyebelge;
	}

	public String getOldValueForUyeuzmanlik() {
		return oldValueForUyeuzmanlik;
	}

	public void setOldValueForUyeuzmanlik(String oldValueForUyeuzmanlik) {
		this.oldValueForUyeuzmanlik = oldValueForUyeuzmanlik;
	}

	public String getOldValueForUyebilirkisi() {
		return oldValueForUyebilirkisi;
	}

	public void setOldValueForUyebilirkisi(String oldValueForUyebilirkisi) {
		this.oldValueForUyebilirkisi = oldValueForUyebilirkisi;
	}

	public String getOldValueForUyeceza() {
		return oldValueForUyeceza;
	}

	public void setOldValueForUyeceza(String oldValueForUyeceza) {
		this.oldValueForUyeceza = oldValueForUyeceza;
	}

	public String getOldValueForUyeodul() {
		return oldValueForUyeodul;
	}

	public void setOldValueForUyeodul(String oldValueForUyeodul) {
		this.oldValueForUyeodul = oldValueForUyeodul;
	}

	public String getOldValueForEtkinlikkuruluye() {
		return oldValueForEtkinlikkuruluye;
	}

	public void setOldValueForEtkinlikkuruluye(String oldValueForEtkinlikkuruluye) {
		this.oldValueForEtkinlikkuruluye = oldValueForEtkinlikkuruluye;
	}

	public String getOldValueForUyeabone() {
		return oldValueForUyeabone;
	}

	public void setOldValueForUyeabone(String oldValueForUyeabone) {
		this.oldValueForUyeabone = oldValueForUyeabone;
	}

	public String getOldValueForUyesigorta() {
		return oldValueForUyesigorta;
	}

	public void setOldValueForUyesigorta(String oldValueForUyesigorta) {
		this.oldValueForUyesigorta = oldValueForUyesigorta;
	}

	public String getOldValueForUyeaidat() {
		return oldValueForUyeaidat;
	}

	public void setOldValueForUyeaidat(String oldValueForUyeaidat) {
		this.oldValueForUyeaidat = oldValueForUyeaidat;
	}

	public String getOldValueForUyemuafiyet() {
		return oldValueForUyemuafiyet;
	}

	public void setOldValueForUyemuafiyet(String oldValueForUyemuafiyet) {
		this.oldValueForUyemuafiyet = oldValueForUyemuafiyet;
	}

	public String getOldValueForUyeborc() {
		return oldValueForUyeborc;
	}

	public void setOldValueForUyeborc(String oldValueForUyeborc) {
		this.oldValueForUyeborc = oldValueForUyeborc;
	}

	public BigDecimal getUyeBorc() {
		return uyeBorc;
	}

	public void setUyeBorc(BigDecimal uyeBorc) {
		this.uyeBorc = uyeBorc;
	}

	public BigDecimal getUyeAidatBorc() {
		return uyeAidatBorc;
	}

	public Long getUyeAidatBorcLongValue() {
		return uyeAidatBorc.longValue();
	}

	public void setUyeAidatBorc(BigDecimal uyeAidatBorc) {
		this.uyeAidatBorc = uyeAidatBorc;
	}

	public String getOldValueForOgrenciToUye() {
		return oldValueForOgrenciToUye;
	}

	public void setOldValueForOgrenciToUye(String oldValueForOgrenciToUye) {
		this.oldValueForOgrenciToUye = oldValueForOgrenciToUye;
	}

	public Adres getAdres() {
		return adres;
	}

	public void setAdres(Adres adres) {
		this.adres = adres;
	}

	public Telefon getTelefon() {
		return telefon;
	}

	public void setTelefon(Telefon telefon) {
		this.telefon = telefon;
	}

	public Internetadres getInternetadres() {
		return internetadres;
	}

	public void setInternetadres(Internetadres internetadres) {
		this.internetadres = internetadres;
	}

	public InternetadresController getInternetadresController() {
		return internetadresController;
	}

	public void setInternetadresController(InternetadresController internetadresController) {
		this.internetadresController = internetadresController;
	}

	public AdresController getAdresController() {
		return adresController;
	}

	public void setAdresController(AdresController adresController) {
		this.adresController = adresController;
	}

	public TelefonController getTelefonController() {
		return telefonController;
	}

	public void setTelefonController(TelefonController telefonController) {
		this.telefonController = telefonController;
	}

	public String getOldValueForAdres() {
		return oldValueForAdres;
	}

	public void setOldValueForAdres(String oldValueForAdres) {
		this.oldValueForAdres = oldValueForAdres;
	}

	public String getOldValueForInternetadres() {
		return oldValueForInternetadres;
	}

	public void setOldValueForInternetadres(String oldValueForInternetadres) {
		this.oldValueForInternetadres = oldValueForInternetadres;
	}

	public String getOldValueForTelefon() {
		return oldValueForTelefon;
	}

	public void setOldValueForTelefon(String oldValueForTelefon) {
		this.oldValueForTelefon = oldValueForTelefon;
	}

	public OdemeController getUyeOdemeController() {
		return uyeOdemeController;
	}

	public void setUyeOdemeController(OdemeController uyeOdemeController) {
		this.uyeOdemeController = uyeOdemeController;
	}

	public OdemeController getAidatOdemeController() {
		return aidatOdemeController;
	}

	public void setAidatOdemeController(OdemeController aidatOdemeController) {
		this.aidatOdemeController = aidatOdemeController;
	}

	public Odeme getAidatodeme() {
		return aidatodeme;
	}

	public void setAidatodeme(Odeme aidatodeme) {
		this.aidatodeme = aidatodeme;
	}

	public String getOldValueForAidatOdeme() {
		return oldValueForAidatOdeme;
	}

	public void setOldValueForAidatOdeme(String oldValueForAidatOdeme) {
		this.oldValueForAidatOdeme = oldValueForAidatOdeme;
	}

	public Odeme getUyeodeme() {
		return uyeodeme;
	}

	public void setUyeodeme(Odeme uyeodeme) {
		this.uyeodeme = uyeodeme;
	}

	public String getOldValueForUyeOdeme() {
		return oldValueForUyeOdeme;
	}

	public void setOldValueForUyeOdeme(String oldValueForUyeOdeme) {
		this.oldValueForUyeOdeme = oldValueForUyeOdeme;
	}

	public EgitimkatilimciController getEgitimkatilimciController() {
		return egitimkatilimciController;
	}

	public void setEgitimkatilimciController(EgitimkatilimciController egitimkatilimciController) {
		this.egitimkatilimciController = egitimkatilimciController;
	}

	public Egitimkatilimci getEgitimkatilimci() {
		return egitimkatilimci;
	}

	public void setEgitimkatilimci(Egitimkatilimci egitimkatilimci) {
		this.egitimkatilimci = egitimkatilimci;
	}

	public String getOldValueForEgitimKatilimci() {
		return oldValueForEgitimKatilimci;
	}

	public void setOldValueForEgitimKatilimci(String oldValueForEgitimKatilimci) {
		this.oldValueForEgitimKatilimci = oldValueForEgitimKatilimci;
	}

	public EtkinlikkatilimciController getEtkinlikkatilimciController() {
		return etkinlikkatilimciController;
	}

	public void setEtkinlikkatilimciController(EtkinlikkatilimciController etkinlikkatilimciController) {
		this.etkinlikkatilimciController = etkinlikkatilimciController;
	}

	public Etkinlikkatilimci getEtkinlikkatilimci() {
		return etkinlikkatilimci;
	}

	public void setEtkinlikkatilimci(Etkinlikkatilimci etkinlikkatilimci) {
		this.etkinlikkatilimci = etkinlikkatilimci;
	}

	public String getOldValueForEtkinlikKatilimci() {
		return oldValueForEtkinlikKatilimci;
	}

	public void setOldValueForEtkinlikKatilimci(String oldValueForEtkinlikKatilimci) {
		this.oldValueForEtkinlikKatilimci = oldValueForEtkinlikKatilimci;
	}

	public String getOldValueForKomisyonDonemUye() {
		return oldValueForKomisyonDonemUye;
	}

	public void setOldValueForKomisyonDonemUye(String oldValueForKomisyonDonemUye) {
		this.oldValueForKomisyonDonemUye = oldValueForKomisyonDonemUye;
	}

	public Kisikimlik getKisikimlik() {
		return kisikimlik;
	}

	public void setKisikimlik(Kisikimlik kisikimlik) {
		this.kisikimlik = kisikimlik;
	}

	public KisikimlikController getKisikimlikController() {
		return kisikimlikController;
	}

	public void setKisikimlikController(KisikimlikController kisikimlikController) {
		this.kisikimlikController = kisikimlikController;
	}

	public UyeformController getUyeformController() {
		return uyeformController;
	}

	public void setUyeformController(UyeformController uyeformController) {
		this.uyeformController = uyeformController;
	}

	public Kurum getPreviousKurumForUyeKurum() {
		return previousKurumForUyeKurum;
	}

	public void setPreviousKurumForUyeKurum(Kurum previousKurumForUyeKurum) {
		this.previousKurumForUyeKurum = previousKurumForUyeKurum;
	}

	public KurumKisiController getKurumKisiController() {
		return kurumKisiController;
	}

	public void setKurumKisiController(KurumKisiController kurumKisiController) {
		this.kurumKisiController = kurumKisiController;
	}

	public KurumKisi getKurumKisi() {
		return kurumKisi;
	}

	public void setKurumKisi(KurumKisi kurumKisi) {
		this.kurumKisi = kurumKisi;
	}

	public String getOldValueForKurumKisi() {
		return oldValueForKurumKisi;
	}

	public void setOldValueForKurumKisi(String oldValueForKurumKisi) {
		this.oldValueForKurumKisi = oldValueForKurumKisi;
	}

	public KurulDonemUyeController getKuruldonemuyeController() {
		return kuruldonemuyeController;
	}

	public void setKuruldonemuyeController(KurulDonemUyeController kuruldonemuyeController) {
		this.kuruldonemuyeController = kuruldonemuyeController;
	}

	public KomisyonDonemUyeController getKomisyondonemuyeController() {
		return komisyondonemuyeController;
	}

	public void setKomisyondonemuyeController(KomisyonDonemUyeController komisyondonemuyeController) {
		this.komisyondonemuyeController = komisyondonemuyeController;
	}

	public KurulDonemUye getKuruldonemuye() {
		return kuruldonemuye;
	}

	public void setKuruldonemuye(KurulDonemUye kuruldonemuye) {
		this.kuruldonemuye = kuruldonemuye;
	}

	public KomisyonDonemUye getKomisyondonemuye() {
		return komisyondonemuye;
	}

	public void setKomisyondonemuye(KomisyonDonemUye komisyondonemuye) {
		this.komisyondonemuye = komisyondonemuye;
	}

	public String getOldValueForKurulDonemUye() {
		return oldValueForKurulDonemUye;
	}

	public void setOldValueForKurulDonemUye(String oldValueForKurulDonemUye) {
		this.oldValueForKurulDonemUye = oldValueForKurulDonemUye;
	}

	public Adres getAdresev() {
		return adresev;
	}

	public void setAdresev(Adres adresev) {
		this.adresev = adresev;
	}

	public Adres getAdresisyeri() {
		return adresisyeri;
	}

	public void setAdresisyeri(Adres adresisyeri) {
		this.adresisyeri = adresisyeri;
	}

	public SelectItem[] getIlItemListForIsyeriAdres() {
		return ilItemListForIsyeriAdres;
	}

	public void setIlItemListForIsyeriAdres(SelectItem[] ilItemListForIsyeriAdres) {
		this.ilItemListForIsyeriAdres = ilItemListForIsyeriAdres;
	}

	public SelectItem[] getIlceItemListForIsyeriAdres() {
		return ilceItemListForIsyeriAdres;
	}

	public void setIlceItemListForIsyeriAdres(SelectItem[] ilceItemListForIsyeriAdres) {
		this.ilceItemListForIsyeriAdres = ilceItemListForIsyeriAdres;
	}

	public int getYilFiltreleme() {
		return yilFiltreleme;
	}

	public void setYilFiltreleme(int yilFiltreleme) {
		this.yilFiltreleme = yilFiltreleme;
	}

	public Telefon getUyetelefon() {
		return uyetelefon;
	}

	public void setUyetelefon(Telefon uyetelefon) {
		this.uyetelefon = uyetelefon;
	}

	public Internetadres getUyeinternetadres() {
		return uyeinternetadres;
	}

	public void setUyeinternetadres(Internetadres uyeinternetadres) {
		this.uyeinternetadres = uyeinternetadres;
	}

	public Internetadres getUyeweb() {
		return uyeweb;
	}

	public void setUyeweb(Internetadres uyeweb) {
		this.uyeweb = uyeweb;
	}

	public int getKacaylikaidatborc() {
		return kacaylikaidatborc;
	}

	public void setKacaylikaidatborc(int kacaylikaidatborc) {
		this.kacaylikaidatborc = kacaylikaidatborc;
	}

	public int getAidatmiktar() {
		return aidatmiktar;
	}

	public void setAidatmiktar(int aidatmiktar) {
		this.aidatmiktar = aidatmiktar;
	}

	public int getAidatodenenmiktar() {
		return aidatodenenmiktar;
	}

	public void setAidatodenenmiktar(int aidatodenenmiktar) {
		this.aidatodenenmiktar = aidatodenenmiktar;
	}

	public BigDecimal getMiktar() {
		return miktar;
	}

	public void setMiktar(BigDecimal miktar) {
		this.miktar = miktar;
	}

	public int getAidatyili() {
		return aidatyili;
	}

	public void setAidatyili(int aidatyili) {
		this.aidatyili = aidatyili;
	}

	public int getToplam() {
		return toplam;
	}

	public void setToplam(int toplam) {
		this.toplam = toplam;
	}

	public Boolean getHashlendimi() {
		return hashlendimi;
	}

	public void setHashlendimi(Boolean hashlendimi) {
		this.hashlendimi = hashlendimi;
	}

	public String aidatodemeisbankasiSayfasi() {
		return ApplicationDescriptor._ISBANKASI_ODEME_OUTCOME;
	}

	public String aidatodemeGarantiSayfasi() {
		return ApplicationDescriptor._GARANTI_ODEME_OUTCOME;
	}

	public List<BaseEntity> getList2() {
		return list2;
	}

	public void setList2(List<BaseEntity> list2) {
		this.list2 = list2;
	}

	public Boolean getAdresupdate() {
		return adresupdate;
	}

	public void setAdresupdate(Boolean adresupdate) {
		this.adresupdate = adresupdate;
	}

	public String getEskisifre() {
		return eskisifre;
	}

	public void setEskisifre(String eskisifre) {
		this.eskisifre = eskisifre;
	}

	public String getYenisifre() {
		return yenisifre;
	}

	public void setYenisifre(String yenisifre) {
		this.yenisifre = yenisifre;
	}

	public String getYenisifretekrar() {
		return yenisifretekrar;
	}

	public void setYenisifretekrar(String yenisifretekrar) {
		this.yenisifretekrar = yenisifretekrar;
	}

	public String getOldValueForKisiogrenim() {
		return oldValueForKisiogrenim;
	}

	public void setOldValueForKisiogrenim(String oldValueForKisiogrenim) {
		this.oldValueForKisiogrenim = oldValueForKisiogrenim;
	}

	public String uyeform() {
		return super.detaySayfa(ApplicationDescriptor._UYEFORM_OUTCOME);
	}

	public void egitimdegerlendirme() {
		if (getEgitimkatilimciController().getSelectedRID() != null) {
			return;
		}
		egitimdegerlendirmePanelAc = true;
		this.egitimdegerlendirme = new Egitimdegerlendirme();
		this.egitimdegerlendirme1 = new Egitimdegerlendirme();
		this.egitimdegerlendirme2 = new Egitimdegerlendirme();
		this.egitimdegerlendirme3 = new Egitimdegerlendirme();
		this.egitimdegerlendirme4 = new Egitimdegerlendirme();
		this.egitimdegerlendirme5 = new Egitimdegerlendirme();
		this.egitimdegerlendirme6 = new Egitimdegerlendirme();
		this.egitimdegerlendirme7 = new Egitimdegerlendirme();
		this.egitimdegerlendirme8 = new Egitimdegerlendirme();
		this.egitimdegerlendirme9 = new Egitimdegerlendirme();
		this.egitimdegerlendirme10 = new Egitimdegerlendirme();
		this.egitimdegerlendirme11 = new Egitimdegerlendirme();
		this.egitimdegerlendirme12 = new Egitimdegerlendirme();
		this.egitimdegerlendirme13 = new Egitimdegerlendirme();
		this.egitimdegerlendirme14 = new Egitimdegerlendirme();
		this.egitimdegerlendirme15 = new Egitimdegerlendirme();
		this.egitimdegerlendirme16 = new Egitimdegerlendirme();
		this.egitimdegerlendirme17 = new Egitimdegerlendirme();
		this.egitimdegerlendirme18 = new Egitimdegerlendirme();
		this.egitimdegerlendirme19 = new Egitimdegerlendirme();

		this.egitimdegerlendirme.setEgitimRef(getEgitimkatilimciController().getEgitimkatilimci().getEgitimRef());
		this.egitimdegerlendirme1.setEgitimRef(getEgitimkatilimciController().getEgitimkatilimci().getEgitimRef());
		this.egitimdegerlendirme2.setEgitimRef(getEgitimkatilimciController().getEgitimkatilimci().getEgitimRef());
		this.egitimdegerlendirme3.setEgitimRef(getEgitimkatilimciController().getEgitimkatilimci().getEgitimRef());
		this.egitimdegerlendirme4.setEgitimRef(getEgitimkatilimciController().getEgitimkatilimci().getEgitimRef());
		this.egitimdegerlendirme5.setEgitimRef(getEgitimkatilimciController().getEgitimkatilimci().getEgitimRef());
		this.egitimdegerlendirme6.setEgitimRef(getEgitimkatilimciController().getEgitimkatilimci().getEgitimRef());
		this.egitimdegerlendirme7.setEgitimRef(getEgitimkatilimciController().getEgitimkatilimci().getEgitimRef());
		this.egitimdegerlendirme8.setEgitimRef(getEgitimkatilimciController().getEgitimkatilimci().getEgitimRef());
		this.egitimdegerlendirme9.setEgitimRef(getEgitimkatilimciController().getEgitimkatilimci().getEgitimRef());
		this.egitimdegerlendirme10.setEgitimRef(getEgitimkatilimciController().getEgitimkatilimci().getEgitimRef());
		this.egitimdegerlendirme11.setEgitimRef(getEgitimkatilimciController().getEgitimkatilimci().getEgitimRef());
		this.egitimdegerlendirme12.setEgitimRef(getEgitimkatilimciController().getEgitimkatilimci().getEgitimRef());
		this.egitimdegerlendirme13.setEgitimRef(getEgitimkatilimciController().getEgitimkatilimci().getEgitimRef());
		this.egitimdegerlendirme14.setEgitimRef(getEgitimkatilimciController().getEgitimkatilimci().getEgitimRef());
		this.egitimdegerlendirme15.setEgitimRef(getEgitimkatilimciController().getEgitimkatilimci().getEgitimRef());
		this.egitimdegerlendirme16.setEgitimRef(getEgitimkatilimciController().getEgitimkatilimci().getEgitimRef());
		this.egitimdegerlendirme17.setEgitimRef(getEgitimkatilimciController().getEgitimkatilimci().getEgitimRef());
		this.egitimdegerlendirme18.setEgitimRef(getEgitimkatilimciController().getEgitimkatilimci().getEgitimRef());
		this.egitimdegerlendirme19.setEgitimRef(getEgitimkatilimciController().getEgitimkatilimci().getEgitimRef());

		this.egitimdegerlendirme.setEgitimkatilimciRef(getEgitimkatilimciController().getEgitimkatilimci());
		this.egitimdegerlendirme1.setEgitimkatilimciRef(getEgitimkatilimciController().getEgitimkatilimci());
		this.egitimdegerlendirme2.setEgitimkatilimciRef(getEgitimkatilimciController().getEgitimkatilimci());
		this.egitimdegerlendirme3.setEgitimkatilimciRef(getEgitimkatilimciController().getEgitimkatilimci());
		this.egitimdegerlendirme4.setEgitimkatilimciRef(getEgitimkatilimciController().getEgitimkatilimci());
		this.egitimdegerlendirme5.setEgitimkatilimciRef(getEgitimkatilimciController().getEgitimkatilimci());
		this.egitimdegerlendirme6.setEgitimkatilimciRef(getEgitimkatilimciController().getEgitimkatilimci());
		this.egitimdegerlendirme7.setEgitimkatilimciRef(getEgitimkatilimciController().getEgitimkatilimci());
		this.egitimdegerlendirme8.setEgitimkatilimciRef(getEgitimkatilimciController().getEgitimkatilimci());
		this.egitimdegerlendirme9.setEgitimkatilimciRef(getEgitimkatilimciController().getEgitimkatilimci());
		this.egitimdegerlendirme10.setEgitimkatilimciRef(getEgitimkatilimciController().getEgitimkatilimci());
		this.egitimdegerlendirme11.setEgitimkatilimciRef(getEgitimkatilimciController().getEgitimkatilimci());
		this.egitimdegerlendirme12.setEgitimkatilimciRef(getEgitimkatilimciController().getEgitimkatilimci());
		this.egitimdegerlendirme13.setEgitimkatilimciRef(getEgitimkatilimciController().getEgitimkatilimci());
		this.egitimdegerlendirme14.setEgitimkatilimciRef(getEgitimkatilimciController().getEgitimkatilimci());
		this.egitimdegerlendirme15.setEgitimkatilimciRef(getEgitimkatilimciController().getEgitimkatilimci());
		this.egitimdegerlendirme16.setEgitimkatilimciRef(getEgitimkatilimciController().getEgitimkatilimci());
		this.egitimdegerlendirme17.setEgitimkatilimciRef(getEgitimkatilimciController().getEgitimkatilimci());
		this.egitimdegerlendirme18.setEgitimkatilimciRef(getEgitimkatilimciController().getEgitimkatilimci());
		this.egitimdegerlendirme19.setEgitimkatilimciRef(getEgitimkatilimciController().getEgitimkatilimci());

		this.egitimdegerlendirme.setSoru(Soru._TIP1);
		this.egitimdegerlendirme1.setSoru(Soru._TIP2);
		this.egitimdegerlendirme2.setSoru(Soru._TIP3);
		this.egitimdegerlendirme3.setSoru(Soru._TIP4);
		this.egitimdegerlendirme4.setSoru(Soru._TIP5);
		this.egitimdegerlendirme5.setSoru(Soru._TIP6);
		this.egitimdegerlendirme6.setSoru(Soru._TIP7);
		this.egitimdegerlendirme7.setSoru(Soru._TIP8);
		this.egitimdegerlendirme8.setSoru(Soru._TIP9);
		this.egitimdegerlendirme9.setSoru(Soru._TIP10);
		this.egitimdegerlendirme10.setSoru(Soru._TIP11);
		this.egitimdegerlendirme11.setSoru(Soru._TIP12);
		this.egitimdegerlendirme12.setSoru(Soru._TIP13);
		this.egitimdegerlendirme13.setSoru(Soru._TIP14);
		this.egitimdegerlendirme14.setSoru(Soru._TIP15);
		this.egitimdegerlendirme15.setSoru(Soru._TIP16);
		this.egitimdegerlendirme16.setSoru(Soru._TIP17);
		this.egitimdegerlendirme17.setSoru(Soru._TIP18);
		this.egitimdegerlendirme18.setSoru(Soru._TIP19);
		this.egitimdegerlendirme19.setSoru(Soru._TIP20);

		this.egitimdegerlendirme.setCevap(Cevap._NULL);
		this.egitimdegerlendirme1.setCevap(Cevap._NULL);
		this.egitimdegerlendirme2.setCevap(Cevap._NULL);
		this.egitimdegerlendirme3.setCevap(Cevap._NULL);
		this.egitimdegerlendirme4.setCevap(Cevap._NULL);
		this.egitimdegerlendirme5.setCevap(Cevap._NULL);
		this.egitimdegerlendirme6.setCevap(Cevap._NULL);
		this.egitimdegerlendirme7.setCevap(Cevap._NULL);
		this.egitimdegerlendirme8.setCevap(Cevap._NULL);
		this.egitimdegerlendirme9.setCevap(Cevap._NULL);
		this.egitimdegerlendirme10.setCevap(Cevap._NULL);
		this.egitimdegerlendirme11.setCevap(Cevap._NULL);
		this.egitimdegerlendirme12.setCevap(Cevap._NULL);
		this.egitimdegerlendirme13.setCevap(Cevap._NULL);
		this.egitimdegerlendirme14.setCevap(Cevap._NULL);
		this.egitimdegerlendirme15.setCevap(Cevap._NULL);
		this.egitimdegerlendirme16.setCevap(Cevap._NULL);
		this.egitimdegerlendirme17.setCevap(Cevap._NULL);
		this.egitimdegerlendirme18.setCevap(Cevap._NULL);
		this.egitimdegerlendirme19.setCevap(Cevap._NULL);

	}

	private Boolean egitimdegerlendirmePanelAc = false;

	public Boolean getEgitimdegerlendirmePanelAc() {
		return egitimdegerlendirmePanelAc;
	}

	public void setEgitimdegerlendirmePanelAc(Boolean egitimdegerlendirmePanelAc) {
		this.egitimdegerlendirmePanelAc = egitimdegerlendirmePanelAc;
	}

	private Egitimdegerlendirme egitimdegerlendirme = new Egitimdegerlendirme();
	private Egitimdegerlendirme egitimdegerlendirme1 = new Egitimdegerlendirme();
	private Egitimdegerlendirme egitimdegerlendirme2 = new Egitimdegerlendirme();
	private Egitimdegerlendirme egitimdegerlendirme3 = new Egitimdegerlendirme();
	private Egitimdegerlendirme egitimdegerlendirme4 = new Egitimdegerlendirme();
	private Egitimdegerlendirme egitimdegerlendirme5 = new Egitimdegerlendirme();
	private Egitimdegerlendirme egitimdegerlendirme6 = new Egitimdegerlendirme();
	private Egitimdegerlendirme egitimdegerlendirme7 = new Egitimdegerlendirme();
	private Egitimdegerlendirme egitimdegerlendirme8 = new Egitimdegerlendirme();
	private Egitimdegerlendirme egitimdegerlendirme9 = new Egitimdegerlendirme();
	private Egitimdegerlendirme egitimdegerlendirme10 = new Egitimdegerlendirme();
	private Egitimdegerlendirme egitimdegerlendirme11 = new Egitimdegerlendirme();
	private Egitimdegerlendirme egitimdegerlendirme12 = new Egitimdegerlendirme();
	private Egitimdegerlendirme egitimdegerlendirme13 = new Egitimdegerlendirme();
	private Egitimdegerlendirme egitimdegerlendirme14 = new Egitimdegerlendirme();
	private Egitimdegerlendirme egitimdegerlendirme15 = new Egitimdegerlendirme();
	private Egitimdegerlendirme egitimdegerlendirme16 = new Egitimdegerlendirme();
	private Egitimdegerlendirme egitimdegerlendirme17 = new Egitimdegerlendirme();
	private Egitimdegerlendirme egitimdegerlendirme18 = new Egitimdegerlendirme();
	private Egitimdegerlendirme egitimdegerlendirme19 = new Egitimdegerlendirme();
	private Boolean egitimdegerlendirmecevap = false;
	private Boolean egitimdegerlendirmecevap1 = false;
	private Boolean egitimdegerlendirmecevap2 = false;
	private Boolean egitimdegerlendirmecevap3 = false;
	private Boolean egitimdegerlendirmecevap4 = false;
	private Boolean egitimdegerlendirmecevap5 = false;
	private Boolean egitimdegerlendirmecevap6 = false;
	private Boolean egitimdegerlendirmecevap7 = false;
	private Boolean egitimdegerlendirmecevap8 = false;
	private Boolean egitimdegerlendirmecevap9 = false;
	private Boolean egitimdegerlendirmecevap10 = false;
	private Boolean egitimdegerlendirmecevap11 = false;
	private Boolean egitimdegerlendirmecevap12 = false;
	private Boolean egitimdegerlendirmecevap13 = false;
	private Boolean egitimdegerlendirmecevap14 = false;
	private Boolean egitimdegerlendirmecevap15 = false;
	private Boolean egitimdegerlendirmecevap16 = false;
	private Boolean egitimdegerlendirmecevap17 = false;
	private Boolean egitimdegerlendirmecevap18 = false;
	private Boolean egitimdegerlendirmecevap19 = false;

	public void saveEgitimDegerlendirme() {
		RequestContext context = RequestContext.getCurrentInstance();
		try {
			if (this.egitimdegerlendirme.getCevap() == null) {
				this.egitimdegerlendirme.setCevap(Cevap._NULL);
			}
			if (this.egitimdegerlendirme1.getCevap() == null) {
				this.egitimdegerlendirme1.setCevap(Cevap._NULL);
			}
			if (this.egitimdegerlendirme2.getCevap() == null) {
				this.egitimdegerlendirme2.setCevap(Cevap._NULL);
			}
			if (this.egitimdegerlendirme3.getCevap() == null) {
				this.egitimdegerlendirme3.setCevap(Cevap._NULL);
			}
			if (this.egitimdegerlendirme4.getCevap() == null) {
				this.egitimdegerlendirme4.setCevap(Cevap._NULL);
			}
			if (this.egitimdegerlendirme5.getCevap() == null) {
				this.egitimdegerlendirme5.setCevap(Cevap._NULL);
			}
			if (this.egitimdegerlendirme6.getCevap() == null) {
				this.egitimdegerlendirme6.setCevap(Cevap._NULL);
			}
			if (this.egitimdegerlendirme7.getCevap() == null) {
				this.egitimdegerlendirme7.setCevap(Cevap._NULL);
			}
			if (this.egitimdegerlendirme8.getCevap() == null) {
				this.egitimdegerlendirme8.setCevap(Cevap._NULL);
			}
			if (this.egitimdegerlendirme9.getCevap() == null) {
				this.egitimdegerlendirme9.setCevap(Cevap._NULL);
			}
			if (this.egitimdegerlendirme10.getCevap() == null) {
				this.egitimdegerlendirme10.setCevap(Cevap._NULL);
			}
			if (this.egitimdegerlendirme11.getCevap() == null) {
				this.egitimdegerlendirme11.setCevap(Cevap._NULL);
			}
			if (this.egitimdegerlendirme12.getCevap() == null) {
				this.egitimdegerlendirme12.setCevap(Cevap._NULL);
			}
			if (this.egitimdegerlendirme13.getCevap() == null) {
				this.egitimdegerlendirme13.setCevap(Cevap._NULL);
			}
			if (this.egitimdegerlendirme14.getCevap() == null) {
				this.egitimdegerlendirme14.setCevap(Cevap._NULL);
			}
			if (this.egitimdegerlendirme15.getCevap() == null) {
				this.egitimdegerlendirme15.setCevap(Cevap._NULL);
			}
			if (this.egitimdegerlendirme16.getCevap() == null) {
				this.egitimdegerlendirme16.setCevap(Cevap._NULL);
			}
			if (this.egitimdegerlendirme17.getCevap() == null) {
				this.egitimdegerlendirme17.setCevap(Cevap._NULL);
			}
			if (this.egitimdegerlendirme18.getCevap() == null) {
				this.egitimdegerlendirme18.setCevap(Cevap._NULL);
			}
			if (this.egitimdegerlendirme19.getCevap() == null) {
				this.egitimdegerlendirme19.setCevap(Cevap._NULL);
			}

			getDBOperator().insert(this.egitimdegerlendirme);
			getDBOperator().insert(this.egitimdegerlendirme1);
			getDBOperator().insert(this.egitimdegerlendirme2);
			getDBOperator().insert(this.egitimdegerlendirme3);
			getDBOperator().insert(this.egitimdegerlendirme4);
			getDBOperator().insert(this.egitimdegerlendirme5);
			getDBOperator().insert(this.egitimdegerlendirme6);
			getDBOperator().insert(this.egitimdegerlendirme7);
			getDBOperator().insert(this.egitimdegerlendirme8);
			getDBOperator().insert(this.egitimdegerlendirme9);
			getDBOperator().insert(this.egitimdegerlendirme10);
			getDBOperator().insert(this.egitimdegerlendirme11);
			getDBOperator().insert(this.egitimdegerlendirme12);
			getDBOperator().insert(this.egitimdegerlendirme13);
			getDBOperator().insert(this.egitimdegerlendirme14);
			getDBOperator().insert(this.egitimdegerlendirme15);
			getDBOperator().insert(this.egitimdegerlendirme16);
			getDBOperator().insert(this.egitimdegerlendirme17);
			getDBOperator().insert(this.egitimdegerlendirme18);
			getDBOperator().insert(this.egitimdegerlendirme19);
			this.egitimdegerlendirmePanelAc = false;

			context.update("uyedashboardpanel");
			this.detailEditType = null;
			this.egitimkatilimciController.queryAction();
		} catch (DBException e) {
			logYaz("UyeController @saveEgitimDegerlendirme = " + e.getMessage());
			e.printStackTrace();
		}
	}

	public Boolean getEgitimdegerlendirmecevap() {
		return egitimdegerlendirmecevap;
	}

	public void setEgitimdegerlendirmecevap(Boolean egitimdegerlendirmecevap) {
		this.egitimdegerlendirmecevap = egitimdegerlendirmecevap;
	}

	public Boolean getEgitimdegerlendirmecevap1() {
		return egitimdegerlendirmecevap1;
	}

	public void setEgitimdegerlendirmecevap1(Boolean egitimdegerlendirmecevap1) {
		this.egitimdegerlendirmecevap1 = egitimdegerlendirmecevap1;
	}

	public Boolean getEgitimdegerlendirmecevap2() {
		return egitimdegerlendirmecevap2;
	}

	public void setEgitimdegerlendirmecevap2(Boolean egitimdegerlendirmecevap2) {
		this.egitimdegerlendirmecevap2 = egitimdegerlendirmecevap2;
	}

	public Boolean getEgitimdegerlendirmecevap3() {
		return egitimdegerlendirmecevap3;
	}

	public void setEgitimdegerlendirmecevap3(Boolean egitimdegerlendirmecevap3) {
		this.egitimdegerlendirmecevap3 = egitimdegerlendirmecevap3;
	}

	public Boolean getEgitimdegerlendirmecevap4() {
		return egitimdegerlendirmecevap4;
	}

	public void setEgitimdegerlendirmecevap4(Boolean egitimdegerlendirmecevap4) {
		this.egitimdegerlendirmecevap4 = egitimdegerlendirmecevap4;
	}

	public Boolean getEgitimdegerlendirmecevap5() {
		return egitimdegerlendirmecevap5;
	}

	public void setEgitimdegerlendirmecevap5(Boolean egitimdegerlendirmecevap5) {
		this.egitimdegerlendirmecevap5 = egitimdegerlendirmecevap5;
	}

	public Boolean getEgitimdegerlendirmecevap6() {
		return egitimdegerlendirmecevap6;
	}

	public void setEgitimdegerlendirmecevap6(Boolean egitimdegerlendirmecevap6) {
		this.egitimdegerlendirmecevap6 = egitimdegerlendirmecevap6;
	}

	public Boolean getEgitimdegerlendirmecevap7() {
		return egitimdegerlendirmecevap7;
	}

	public void setEgitimdegerlendirmecevap7(Boolean egitimdegerlendirmecevap7) {
		this.egitimdegerlendirmecevap7 = egitimdegerlendirmecevap7;
	}

	public Boolean getEgitimdegerlendirmecevap8() {
		return egitimdegerlendirmecevap8;
	}

	public void setEgitimdegerlendirmecevap8(Boolean egitimdegerlendirmecevap8) {
		this.egitimdegerlendirmecevap8 = egitimdegerlendirmecevap8;
	}

	public Boolean getEgitimdegerlendirmecevap9() {
		return egitimdegerlendirmecevap9;
	}

	public void setEgitimdegerlendirmecevap9(Boolean egitimdegerlendirmecevap9) {
		this.egitimdegerlendirmecevap9 = egitimdegerlendirmecevap9;
	}

	public Boolean getEgitimdegerlendirmecevap10() {
		return egitimdegerlendirmecevap10;
	}

	public void setEgitimdegerlendirmecevap10(Boolean egitimdegerlendirmecevap10) {
		this.egitimdegerlendirmecevap10 = egitimdegerlendirmecevap10;
	}

	public Boolean getEgitimdegerlendirmecevap11() {
		return egitimdegerlendirmecevap11;
	}

	public void setEgitimdegerlendirmecevap11(Boolean egitimdegerlendirmecevap11) {
		this.egitimdegerlendirmecevap11 = egitimdegerlendirmecevap11;
	}

	public Boolean getEgitimdegerlendirmecevap12() {
		return egitimdegerlendirmecevap12;
	}

	public void setEgitimdegerlendirmecevap12(Boolean egitimdegerlendirmecevap12) {
		this.egitimdegerlendirmecevap12 = egitimdegerlendirmecevap12;
	}

	public Boolean getEgitimdegerlendirmecevap13() {
		return egitimdegerlendirmecevap13;
	}

	public void setEgitimdegerlendirmecevap13(Boolean egitimdegerlendirmecevap13) {
		this.egitimdegerlendirmecevap13 = egitimdegerlendirmecevap13;
	}

	public Boolean getEgitimdegerlendirmecevap14() {
		return egitimdegerlendirmecevap14;
	}

	public void setEgitimdegerlendirmecevap14(Boolean egitimdegerlendirmecevap14) {
		this.egitimdegerlendirmecevap14 = egitimdegerlendirmecevap14;
	}

	public Boolean getEgitimdegerlendirmecevap15() {
		return egitimdegerlendirmecevap15;
	}

	public void setEgitimdegerlendirmecevap15(Boolean egitimdegerlendirmecevap15) {
		this.egitimdegerlendirmecevap15 = egitimdegerlendirmecevap15;
	}

	public Boolean getEgitimdegerlendirmecevap16() {
		return egitimdegerlendirmecevap16;
	}

	public void setEgitimdegerlendirmecevap16(Boolean egitimdegerlendirmecevap16) {
		this.egitimdegerlendirmecevap16 = egitimdegerlendirmecevap16;
	}

	public Boolean getEgitimdegerlendirmecevap17() {
		return egitimdegerlendirmecevap17;
	}

	public void setEgitimdegerlendirmecevap17(Boolean egitimdegerlendirmecevap17) {
		this.egitimdegerlendirmecevap17 = egitimdegerlendirmecevap17;
	}

	public Boolean getEgitimdegerlendirmecevap18() {
		return egitimdegerlendirmecevap18;
	}

	public void setEgitimdegerlendirmecevap18(Boolean egitimdegerlendirmecevap18) {
		this.egitimdegerlendirmecevap18 = egitimdegerlendirmecevap18;
	}

	public Boolean getEgitimdegerlendirmecevap19() {
		return egitimdegerlendirmecevap19;
	}

	public void setEgitimdegerlendirmecevap19(Boolean egitimdegerlendirmecevap19) {
		this.egitimdegerlendirmecevap19 = egitimdegerlendirmecevap19;
	}

	public Egitimdegerlendirme getEgitimdegerlendirme() {
		return egitimdegerlendirme;
	}

	public void setEgitimdegerlendirme(Egitimdegerlendirme egitimdegerlendirme) {
		this.egitimdegerlendirme = egitimdegerlendirme;
	}

	public Egitimdegerlendirme getEgitimdegerlendirme1() {
		return egitimdegerlendirme1;
	}

	public void setEgitimdegerlendirme1(Egitimdegerlendirme egitimdegerlendirme1) {
		this.egitimdegerlendirme1 = egitimdegerlendirme1;
	}

	public Egitimdegerlendirme getEgitimdegerlendirme2() {
		return egitimdegerlendirme2;
	}

	public void setEgitimdegerlendirme2(Egitimdegerlendirme egitimdegerlendirme2) {
		this.egitimdegerlendirme2 = egitimdegerlendirme2;
	}

	public Egitimdegerlendirme getEgitimdegerlendirme3() {
		return egitimdegerlendirme3;
	}

	public void setEgitimdegerlendirme3(Egitimdegerlendirme egitimdegerlendirme3) {
		this.egitimdegerlendirme3 = egitimdegerlendirme3;
	}

	public Egitimdegerlendirme getEgitimdegerlendirme4() {
		return egitimdegerlendirme4;
	}

	public void setEgitimdegerlendirme4(Egitimdegerlendirme egitimdegerlendirme4) {
		this.egitimdegerlendirme4 = egitimdegerlendirme4;
	}

	public Egitimdegerlendirme getEgitimdegerlendirme5() {
		return egitimdegerlendirme5;
	}

	public void setEgitimdegerlendirme5(Egitimdegerlendirme egitimdegerlendirme5) {
		this.egitimdegerlendirme5 = egitimdegerlendirme5;
	}

	public Egitimdegerlendirme getEgitimdegerlendirme6() {
		return egitimdegerlendirme6;
	}

	public void setEgitimdegerlendirme6(Egitimdegerlendirme egitimdegerlendirme6) {
		this.egitimdegerlendirme6 = egitimdegerlendirme6;
	}

	public Egitimdegerlendirme getEgitimdegerlendirme7() {
		return egitimdegerlendirme7;
	}

	public void setEgitimdegerlendirme7(Egitimdegerlendirme egitimdegerlendirme7) {
		this.egitimdegerlendirme7 = egitimdegerlendirme7;
	}

	public Egitimdegerlendirme getEgitimdegerlendirme8() {
		return egitimdegerlendirme8;
	}

	public void setEgitimdegerlendirme8(Egitimdegerlendirme egitimdegerlendirme8) {
		this.egitimdegerlendirme8 = egitimdegerlendirme8;
	}

	public Egitimdegerlendirme getEgitimdegerlendirme9() {
		return egitimdegerlendirme9;
	}

	public void setEgitimdegerlendirme9(Egitimdegerlendirme egitimdegerlendirme9) {
		this.egitimdegerlendirme9 = egitimdegerlendirme9;
	}

	public Egitimdegerlendirme getEgitimdegerlendirme10() {
		return egitimdegerlendirme10;
	}

	public void setEgitimdegerlendirme10(Egitimdegerlendirme egitimdegerlendirme10) {
		this.egitimdegerlendirme10 = egitimdegerlendirme10;
	}

	public Egitimdegerlendirme getEgitimdegerlendirme11() {
		return egitimdegerlendirme11;
	}

	public void setEgitimdegerlendirme11(Egitimdegerlendirme egitimdegerlendirme11) {
		this.egitimdegerlendirme11 = egitimdegerlendirme11;
	}

	public Egitimdegerlendirme getEgitimdegerlendirme12() {
		return egitimdegerlendirme12;
	}

	public void setEgitimdegerlendirme12(Egitimdegerlendirme egitimdegerlendirme12) {
		this.egitimdegerlendirme12 = egitimdegerlendirme12;
	}

	public Egitimdegerlendirme getEgitimdegerlendirme13() {
		return egitimdegerlendirme13;
	}

	public void setEgitimdegerlendirme13(Egitimdegerlendirme egitimdegerlendirme13) {
		this.egitimdegerlendirme13 = egitimdegerlendirme13;
	}

	public Egitimdegerlendirme getEgitimdegerlendirme14() {
		return egitimdegerlendirme14;
	}

	public void setEgitimdegerlendirme14(Egitimdegerlendirme egitimdegerlendirme14) {
		this.egitimdegerlendirme14 = egitimdegerlendirme14;
	}

	public Egitimdegerlendirme getEgitimdegerlendirme15() {
		return egitimdegerlendirme15;
	}

	public void setEgitimdegerlendirme15(Egitimdegerlendirme egitimdegerlendirme15) {
		this.egitimdegerlendirme15 = egitimdegerlendirme15;
	}

	public Egitimdegerlendirme getEgitimdegerlendirme16() {
		return egitimdegerlendirme16;
	}

	public void setEgitimdegerlendirme16(Egitimdegerlendirme egitimdegerlendirme16) {
		this.egitimdegerlendirme16 = egitimdegerlendirme16;
	}

	public Egitimdegerlendirme getEgitimdegerlendirme17() {
		return egitimdegerlendirme17;
	}

	public void setEgitimdegerlendirme17(Egitimdegerlendirme egitimdegerlendirme17) {
		this.egitimdegerlendirme17 = egitimdegerlendirme17;
	}

	public Egitimdegerlendirme getEgitimdegerlendirme18() {
		return egitimdegerlendirme18;
	}

	public void setEgitimdegerlendirme18(Egitimdegerlendirme egitimdegerlendirme18) {
		this.egitimdegerlendirme18 = egitimdegerlendirme18;
	}

	public Egitimdegerlendirme getEgitimdegerlendirme19() {
		return egitimdegerlendirme19;
	}

	public void setEgitimdegerlendirme19(Egitimdegerlendirme egitimdegerlendirme19) {
		this.egitimdegerlendirme19 = egitimdegerlendirme19;
	}

	public String getSoruadi_TIP1() {
		return Soru._TIP1.toString();
	}

	public String getSoruadi_TIP2() {
		return Soru._TIP2.toString();
	}

	public String getSoruadi_TIP3() {
		return Soru._TIP3.toString();
	}

	public String getSoruadi_TIP4() {
		return Soru._TIP4.toString();
	}

	public String getSoruadi_TIP5() {
		return Soru._TIP5.toString();
	}

	public String getSoruadi_TIP6() {
		return Soru._TIP6.toString();
	}

	public String getSoruadi_TIP7() {
		return Soru._TIP7.toString();
	}

	public String getSoruadi_TIP8() {
		return Soru._TIP8.toString();
	}

	public String getSoruadi_TIP9() {
		return Soru._TIP9.toString();
	}

	public String getSoruadi_TIP10() {
		return Soru._TIP10.toString();
	}

	public String getSoruadi_TIP11() {
		return Soru._TIP11.toString();
	}

	public String getSoruadi_TIP12() {
		return Soru._TIP12.toString();
	}

	public String getSoruadi_TIP13() {
		return Soru._TIP13.toString();
	}

	public String getSoruadi_TIP14() {
		return Soru._TIP14.toString();
	}

	public String getSoruadi_TIP15() {
		return Soru._TIP15.toString();
	}

	public String getSoruadi_TIP16() {
		return Soru._TIP16.toString();
	}

	public String getSoruadi_TIP17() {
		return Soru._TIP17.toString();
	}

	public String getSoruadi_TIP18() {
		return Soru._TIP18.toString();
	}

	public String getSoruadi_TIP19() {
		return Soru._TIP19.toString();
	}

	public String getSoruadi_TIP20() {
		return Soru._TIP20.toString();
	}

	@SuppressWarnings("unchecked")
	public void exportAll() {
		CustomExcelExport.writeGenericExcelExport(getDBOperator().load(getModelName()));
	}

	public BigDecimal getKalanToplamBorc() {
		return kalanToplamBorc;
	}

	public void setKalanToplamBorc(BigDecimal kalanToplamBorc) {
		this.kalanToplamBorc = kalanToplamBorc;
	}

	public String getOldValueForInternetadresMetni() {
		return oldValueForInternetadresMetni;
	}

	public void setOldValueForInternetadresMetni(String oldValueForInternetadresMetni) {
		this.oldValueForInternetadresMetni = oldValueForInternetadresMetni;
	}

	public String getTeslimAdresSecimi() {
		return teslimAdresSecimi;
	}

	public void setTeslimAdresSecimi(String teslimAdresSecimi) {
		this.teslimAdresSecimi = teslimAdresSecimi;
	}

	public String getTeslimAdresi() {
		return teslimAdresi;
	}

	public void setTeslimAdresi(String teslimAdresi) {
		this.teslimAdresi = teslimAdresi;
	}

	public SelectItem[] getAdresTuruItems() {
		if (getAdres().getRID() != null) {
			return new SelectItem[] { new SelectItem(getAdres().getAdresturu(), getAdres().getAdresturu().getLabel()) };
		}
		if (getUyeDetay() == null) {
			System.out.println("getUyeDetay() is NULL...");
			return ManagedBeanLocator.locateEnumeratedController().getAdresTuruItems();
		} else {
			System.out.println("getUyeDetay() is NOT NULL... Going to database");
			return getDBOperator().getAdresTuruItems(getUyeDetay().getKisiRef());
		}
	}
} // class
