package tr.com.arf.toys.view.controller.form.system;

import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

import tr.com.arf.framework.db._management.DBOperator;
import tr.com.arf.framework.db.enumerated._common.EvetHayir;
import tr.com.arf.framework.db.enumerated._common.IslemTuru;
import tr.com.arf.framework.db.model._base.BaseEntity;
import tr.com.arf.framework.utility.exception.data.DBException;
import tr.com.arf.framework.utility.tool.DateUtil;
import tr.com.arf.framework.utility.tool.KeyUtil;
import tr.com.arf.toys.db.enumerated.iletisim.AdresTuru;
import tr.com.arf.toys.db.enumerated.iletisim.NetAdresTuru;
import tr.com.arf.toys.db.enumerated.iletisim.TelefonTuru;
import tr.com.arf.toys.db.enumerated.kisi.Cinsiyet;
import tr.com.arf.toys.db.enumerated.kisi.KimlikTip;
import tr.com.arf.toys.db.enumerated.kisi.KisiDurum;
import tr.com.arf.toys.db.enumerated.kisi.MedeniHal;
import tr.com.arf.toys.db.enumerated.system.PersonelDurum;
import tr.com.arf.toys.db.enumerated.system.PersonelGorevDurum;
import tr.com.arf.toys.db.model.gorev.Gorev;
import tr.com.arf.toys.db.model.iletisim.Adres;
import tr.com.arf.toys.db.model.iletisim.Internetadres;
import tr.com.arf.toys.db.model.iletisim.Telefon;
import tr.com.arf.toys.db.model.kisi.Kisi;
import tr.com.arf.toys.db.model.kisi.Kisikimlik;
import tr.com.arf.toys.db.model.kisi.Kurum;
import tr.com.arf.toys.db.model.system.Birim;
import tr.com.arf.toys.db.model.system.Ilce;
import tr.com.arf.toys.db.model.system.Personel;
import tr.com.arf.toys.db.model.system.PersonelBirimGorev;
import tr.com.arf.toys.db.model.system.Sehir;
import tr.com.arf.toys.db.nonEntityModel.PojoAdres;
import tr.com.arf.toys.db.nonEntityModel.PojoKisi;
import tr.com.arf.toys.service.application.ApplicationDescriptor;
import tr.com.arf.toys.service.application.KPSService;
import tr.com.arf.toys.service.application.ManagedBeanLocator;
import tr.com.arf.toys.utility.tool.ToysXMLReader;
import tr.com.arf.toys.view.controller.form._base.ToysBaseController;
import tr.com.arf.toys.view.controller.form.kisi.KisiController;
import tr.com.arf.toys.view.controller.form.kisi.KisikimlikController;

@ManagedBean(name = "personelWizardController")
@ViewScoped
public class PersonelWizardController extends ToysBaseController {

	private static final long serialVersionUID = 6531152165884980748L;

	private Personel personel;
	private int wizardStep = 1;
	private Kisi kisi;
	private Kisikimlik kisikimlik;
	private PersonelBirimGorev personelGorev = new PersonelBirimGorev();

	private Adres kpsAdres;
	private Adres kpsDigerAdres;

	private Kurum kurum;
	private Birim birim;
	private String detailEditType;

	private boolean kpsSorguSonucDondu = false;
	private boolean kpsKaydiBulundu = false;
	private boolean kisiVeritabanindaBulundu = false;

	private PojoKisi kisikimlikPojo = new PojoKisi();

	private Telefon telefon1;
	private Telefon telefon2;
	private Telefon telefon3;
	private Telefon telefon4;

	private Internetadres internetAdres1;

	// private static final String PHONE_PATTERN = "0\\d{3}\\d{7}";
	private static final String PHONE_PATTERN = "0\\s[\\(]\\d{3}[\\)]\\s\\d{3}\\s\\d{4}";
	private final Pattern pattern;

	public PersonelWizardController() {
		super(Personel.class);
		setDefaultValues(false);
		setOrderField("sicilno");
		setAutoCompleteSearchColumns(new String[] { "sicilno" });
		pattern = Pattern.compile(PHONE_PATTERN);
		prepareForWizard();
		setTable(null);
		queryAction();
	}

	private void prepareForWizard() {
		this.wizardStep = 1;
		super.insert();

		this.kisi = new Kisi();
		this.kisi.setKimliknotip(KimlikTip._TCKIMLIKNO);
		this.kisi.setAktif(EvetHayir._EVET);
		this.kisikimlik = new Kisikimlik();
		this.personel = new Personel();
		this.personelGorev = new PersonelBirimGorev();
		this.personelGorev.setDurum(PersonelGorevDurum._AKTIF);
		this.birim = new Birim();
		removeObjectFromSessionFilter(ApplicationDescriptor._PERSONEL_MODEL);

		this.kisikimlikPojo = new PojoKisi();

		this.telefon1 = new Telefon();
		telefon1.setTelefonturu(TelefonTuru._EVTELEFONU);
		this.telefon2 = new Telefon();
		telefon2.setTelefonturu(TelefonTuru._ISTELEFONU);
		this.telefon3 = new Telefon();
		telefon3.setTelefonturu(TelefonTuru._GSM);
		this.telefon4 = new Telefon();
		telefon4.setTelefonturu(TelefonTuru._FAKS);
		this.internetAdres1 = new Internetadres();
		internetAdres1.setNetadresturu(NetAdresTuru._EPOSTA);

		this.detailEditType = "";
		this.kpsSorguSonucDondu = false;
		this.kpsKaydiBulundu = false;
		this.kisiVeritabanindaBulundu = false;
	}

	public void validateTelefon() throws DBException {
		if (!isEmpty(this.telefon1.getTelefonno())) {
			Matcher matcher = pattern.matcher(this.telefon1.getTelefonno().toString());
			if (!matcher.matches()) {
				createCustomMessage("Geçersiz Telefon Numarası ! (0 (XXX) XXX XXXX şeklinde giriniz.) ", FacesMessage.SEVERITY_ERROR, "telefonnoInputText1:telefonno1");
				return;
			}
			this.telefon1.setKisiRef(this.kisi);
			this.telefon1.setVarsayilan(EvetHayir._HAYIR);
		}

		if (!isEmpty(this.telefon2.getTelefonno())) {
			Matcher matcher = pattern.matcher(this.telefon2.getTelefonno().toString());
			if (!matcher.matches()) {
				createCustomMessage("Geçersiz Telefon Numarası ! (0 (XXX) XXX XXXX şeklinde giriniz.) ", FacesMessage.SEVERITY_ERROR, "telefonnoInputText2:telefonno2");
				return;
			}
			this.telefon2.setKisiRef(this.kisi);
			this.telefon2.setVarsayilan(EvetHayir._HAYIR);
		}
		if (!isEmpty(this.telefon3.getTelefonno())) {
			Matcher matcher = pattern.matcher(this.telefon3.getTelefonno().toString());
			if (!matcher.matches()) {
				createCustomMessage("Geçersiz Telefon Numarası ! (0 (XXX) XXX XXXX şeklinde giriniz.) ", FacesMessage.SEVERITY_ERROR, "telefonnoInputText3:telefonno3");
				return;
			}
			this.telefon3.setKisiRef(this.kisi);
			this.telefon3.setVarsayilan(EvetHayir._HAYIR);
		}
		if (!isEmpty(this.telefon4.getTelefonno())) {
			Matcher matcher = pattern.matcher(this.telefon4.getTelefonno().toString());
			if (!matcher.matches()) {
				createCustomMessage("Geçersiz Telefon Numarası ! (0 (XXX) XXX XXXX şeklinde giriniz.) ", FacesMessage.SEVERITY_ERROR, "telefonnoInputText4:telefonno4");
				return;
			}
			this.telefon4.setKisiRef(this.kisi);
			this.telefon4.setVarsayilan(EvetHayir._HAYIR);
		}
		finishWizard();
	}

	@SuppressWarnings("unchecked")
	public void finishWizard() throws DBException {
		DBOperator dbOperator = getDBOperator();
		try {
			// Tum Save'ler yapilacak
			this.personel.setKisiRef(getKisi());
			this.personel.setBaslamatarih(new Date());
			this.personel.setDurum(PersonelDurum._CALISIYOR);
			this.personel.setBirimRef(getSessionUser().getPersonelRef().getBirimRef());
			this.personel.setTumUyeSorgulama(EvetHayir._HAYIR);
			dbOperator.insert(this.personel);
			if (getPersonelController().isLoggable()) {
				logKaydet(this.personel, IslemTuru._EKLEME, "");
			}
			if (this.personel != null) {
				getPersonelGorev().setPersonelRef(this.personel);
			}
			dbOperator.insert(getPersonelGorev());
			if (getPersonelBirimGorevController().isLoggable()) {
				logKaydet(this.personelGorev, IslemTuru._EKLEME, "");
			}

			if (!isEmpty(this.telefon1.getTelefonno())) {
				if (getDBOperator().recordCount(Telefon.class.getSimpleName(), "o.kisiRef.rID=" + getKisi().getRID() + " AND o.telefonturu=" + this.telefon1.getTelefonturu().getCode()) > 0) {
					List<Telefon> telefon2Listesi = getDBOperator().load(Telefon.class.getSimpleName(), "o.kisiRef.rID=" + getKisi().getRID() + " AND o.telefonturu=" + this.telefon1.getTelefonturu().getCode(), "");
					for (Telefon entity : telefon2Listesi) {
						if (entity.getTelefonno().equals(this.telefon1.getTelefonno())) {
							entity.setVarsayilan(EvetHayir._EVET);
							getDBOperator().update(entity);
						} else {
							entity.setVarsayilan(EvetHayir._HAYIR);
							getDBOperator().update(entity);
						}
					}
				} else {
					this.telefon1.setKisiRef(getKisi());
					this.telefon1.setVarsayilan(EvetHayir._HAYIR);
					dbOperator.insert(telefon1);
				}
			}
			if (!isEmpty(this.telefon2.getTelefonno())) {
				if (getDBOperator().recordCount(Telefon.class.getSimpleName(), "o.kisiRef.rID=" + getKisi().getRID() + " AND o.telefonturu=" + this.telefon2.getTelefonturu().getCode()) > 0) {
					List<Telefon> telefon2Listesi = getDBOperator().load(Telefon.class.getSimpleName(), "o.kisiRef.rID=" + getKisi().getRID() + " AND o.telefonturu=" + this.telefon2.getTelefonturu().getCode(), "");
					for (Telefon entity : telefon2Listesi) {
						if (entity.getTelefonno().equals(this.telefon2.getTelefonno())) {
							entity.setVarsayilan(EvetHayir._EVET);
							getDBOperator().update(entity);
						} else {
							entity.setVarsayilan(EvetHayir._HAYIR);
							getDBOperator().update(entity);
						}
					}
				} else {
					this.telefon2.setKisiRef(getKisi());
					this.telefon2.setVarsayilan(EvetHayir._HAYIR);
					dbOperator.insert(telefon2);
				}
			}
			if (!isEmpty(this.telefon3.getTelefonno())) {
				if (getDBOperator().recordCount(Telefon.class.getSimpleName(), "o.kisiRef.rID=" + getKisi().getRID() + " AND o.telefonturu=" + this.telefon3.getTelefonturu().getCode()) > 0) {
					List<Telefon> telefon3Listesi = getDBOperator().load(Telefon.class.getSimpleName(), "o.kisiRef.rID=" + getKisi().getRID() + " AND o.telefonturu=" + this.telefon3.getTelefonturu().getCode(), "");
					for (Telefon entity : telefon3Listesi) {
						if (entity.getTelefonno().equals(this.telefon3.getTelefonno())) {
							entity.setVarsayilan(EvetHayir._EVET);
							getDBOperator().update(entity);
						} else {
							entity.setVarsayilan(EvetHayir._HAYIR);
							getDBOperator().update(entity);
						}
					}
				} else {
					this.telefon3.setKisiRef(getKisi());
					this.telefon3.setVarsayilan(EvetHayir._EVET);
					dbOperator.insert(telefon3);
				}
				// Personel sayfasina gidiyor...
				if (this.personel.getRID() != null) {
					getPersonelController().setSelectedRID(this.personel.getRID() + "");
					ManagedBeanLocator.locateSessionUser().addToSessionFilters(Personel.class.getSimpleName(), this.personel);
					FacesContext.getCurrentInstance().getExternalContext().redirect("personel_Detay.xhtml");
				} else {
					FacesContext.getCurrentInstance().getExternalContext().redirect("personel.xhtml");
				}
			}
		} catch (Exception e) {
			logYaz("Error @finishWizard:" + e.getMessage());
			logYaz("Exception @" + getModelName() + "Controller :", e);
			createGenericMessage(KeyUtil.getMessageValue("islem.hata"), FacesMessage.SEVERITY_ERROR);
		}
	}

	public boolean isPersonelBulundu() {
		if (getObjectFromSessionFilter(ApplicationDescriptor._PERSONEL_MODEL) != null) {
			return true;
		} else {
			return false;
		}
	}

	private Boolean zorunlu = false;

	public Boolean getZorunlu() {
		return zorunlu;
	}

	public void setZorunlu(Boolean zorunlu) {
		this.zorunlu = zorunlu;
	}

	public Kisikimlik getKisikimlik(Long kisiRID) {
		if (kisiRID != null) {
			@SuppressWarnings("unchecked")
			List<Kisikimlik> kimlikListesi = getDBOperator().load(ApplicationDescriptor._KISIKIMLIK_MODEL, "o.kisiRef.rID=" + kisiRID, "o.rID");
			if (kimlikListesi != null && kimlikListesi.size() > 0) {
				return kimlikListesi.get(0);
			} else {
				return null;
			}
		} else {
			return null;
		}
	}

	public void kimlikBilgisiSorgula() throws ParseException {
		if (this.kisi.getKimlikno() != null) {
			setKisikimlik(new Kisikimlik());
			this.kpsKaydiBulundu = false;
			this.kpsSorguSonucDondu = false;
			this.kisiVeritabanindaBulundu = false;
			removeObjectFromSessionFilter(ApplicationDescriptor._PERSONEL_MODEL);
			KPSService kpsService = KPSService.getInstance();
			String kpsKisi;
			try {
				if (getDBOperator().recordCount(Personel.class.getSimpleName(), "o.durum between 1 and 2 AND o.kisiRef.kimlikno = '" + this.kisi.getKimlikno() + "'") > 0) {
					createCustomMessage("Bu personel sistemde tanımlı! Detayını görmek için personel listesine tıklayınız!", FacesMessage.SEVERITY_ERROR, "kimliknoInputTextKisi:kimliknoKisi");
					// Personel varsa islem kesilecek
					putObjectToSessionFilter(ApplicationDescriptor._PERSONEL_MODEL, getPersonel());
					getPersonelController()
							.setSelectedRID(((Personel) getDBOperator().load(Personel.class.getSimpleName(), "o.durum between 1 and 2 AND o.kisiRef.kimlikno = '" + this.kisi.getKimlikno() + "'", "o.rID").get(0)).getRID() + "");
					return;
				}
				if (getDBOperator().recordCount(Kisi.class.getSimpleName(), "o.kimlikno = '" + this.kisi.getKimlikno() + "'") > 0) {
					Kisi bulunanKisi = (Kisi) getDBOperator().find(Kisi.class.getSimpleName(), "o.kimlikno='" + this.kisi.getKimlikno() + "'");
					setKisi(bulunanKisi);
					this.zorunlu = true;
					if (getKisikimlik(bulunanKisi.getRID()) != null) {
						setKisikimlik(getKisikimlik(bulunanKisi.getRID()));
						this.kisiVeritabanindaBulundu = true;
					}
					// Kisi varsa islem kesilmeyecek, var olan kisinin bilgileri getirilecek ve 'KPS'den guncelle' butonu eklenecek.
					return;
				}
				kpsKisi = kpsService.kisiKimlikBilgisiGetirYeni(this.kisi.getKimlikno());
				this.kisikimlikPojo = ToysXMLReader.readXmlStringForKimlik(kpsKisi.trim());
				this.kpsSorguSonucDondu = true;
				if (!isEmpty(this.kisikimlikPojo.getHataBilgisiKodu())) {
					createCustomMessage("Girdiğiniz kimlik numarası KPS sisteminde bulunamadı!", FacesMessage.SEVERITY_ERROR, "kimliknoInputTextKisi:kimliknoKisi");
					this.kisikimlik.setKimlikno(this.kisi.getKimlikno());
					return;
				}
				this.kpsKaydiBulundu = true;
				this.zorunlu = true;
				this.kisi.setAd(this.kisikimlikPojo.getAd());
				this.kisi.setSoyad(this.kisikimlikPojo.getSoyad());
				kisiKimlikBilgisiAktar();

				String kpsAdres = kpsService.kisiAdresBilgisiGetirYeni(kisikimlikPojo.getTcKimlikNo());
				// PojoAdres kpsDigerPojo = XmlHelper.readXmlStringForAdres(kpsAdres, AdresTuru._KPSDIGERADRESI);
				// PojoAdres kpsPojo = XmlHelper.readXmlStringForAdres(kpsAdres, AdresTuru._KPSADRESI);
				PojoAdres kpsPojo = ToysXMLReader.readXmlStringForAdres(kpsAdres, "YerlesimYeriAdresi");
				PojoAdres kpsDigerPojo = ToysXMLReader.readXmlStringForAdres(kpsAdres, "DigerAdresBilgileri");
				if (kpsPojo != null && (kpsPojo.getHataBilgisiKodu() == null || kpsPojo.getHataBilgisiKodu().trim().equals(""))) {
					setKpsAdres(kisiAdresAktar(kpsPojo, AdresTuru._KPSADRESI));
				}
				if (kpsDigerPojo != null && (kpsDigerPojo.getHataBilgisiKodu() == null || kpsDigerPojo.getHataBilgisiKodu().trim().equals(""))) {
					setKpsDigerAdres(kisiAdresAktar(kpsDigerPojo, AdresTuru._KPSDIGERADRESI));
				}
			} catch (Exception e) {
				logYaz("Exception @" + getModelName() + "Controller :", e);
			}
		}
	}

	private void kisiKimlikBilgisiAktar() throws Exception {
		if (isNumber(this.kisikimlikPojo.getCinsiyetKod())) {
			this.kisikimlik.setCinsiyet(Cinsiyet.getWithCode(Integer.parseInt(this.kisikimlikPojo.getCinsiyetKod())));
		}
		this.kisikimlik.setKimlikno(this.kisikimlikPojo.getTcKimlikNo());
		this.kisikimlik.setBabaad(this.kisikimlikPojo.getBabaAdi());
		this.kisikimlik.setAnaad(this.kisikimlikPojo.getAnaAdi());
		if (isNumber(this.kisikimlikPojo.getMedeniHalKodu())) {
			this.kisikimlik.setMedenihal(MedeniHal.getWithCode(Integer.parseInt(this.kisikimlikPojo.getMedeniHalKodu())));
		}
		this.kisikimlik.setDogumyer(this.kisikimlikPojo.getDogumYeri());
		if (this.kisikimlikPojo.getDogumTarihi() != null && !this.kisikimlikPojo.getDogumTarihi().toString().equalsIgnoreCase("null/null/null")) {
			this.kisikimlik.setDogumtarih(DateUtil.createDateObject(this.kisikimlikPojo.getDogumTarihi()));
		}
		if (this.kisikimlikPojo.getOlumTarihi() != null && !this.kisikimlikPojo.getOlumTarihi().toString().equalsIgnoreCase("null/null/null")) {
			this.kisikimlik.setOlumtarih(DateUtil.createDateObject(this.kisikimlikPojo.getOlumTarihi()));
		}
		// this.kisikimlik.setBabatckimlikno(this.kisikimlikPojo.getBabaTCKimlikNo());
		// this.kisikimlik.setAnnetckimlikno(this.kisikimlikPojo.getAnneTCKimlikNo());
		//
		// this.kisikimlik.setEstckimlikno(this.kisikimlikPojo.getEsTCKimlikNo());
		if (isNumber(this.kisikimlikPojo.getDurumKod())) {
			this.kisikimlik.setKisidurum(KisiDurum.getWithCode(Integer.parseInt(this.kisikimlikPojo.getDurumKod())));
		}
		String plakakodu = kisikimlikPojo.getNufusaKayitliOlduguIlKodu();
		int ilkodulength = kisikimlikPojo.getNufusaKayitliOlduguIlKodu().length();
		int zero = 0;
		if (ilkodulength == 1) {
			plakakodu = zero + plakakodu;
		}
		if (getDBOperator().recordCount(Sehir.class.getSimpleName(), "o.ilKodu='" + plakakodu + "'") > 0) {
			this.kisikimlik.setSehirRef((Sehir) getDBOperator().find(Sehir.class.getSimpleName(), "o.ilKodu='" + plakakodu + "'"));
			changeIlce(this.kisikimlik.getSehirRef().getRID());
			this.kisikimlik.setIlceRef((Ilce) getDBOperator().find(Ilce.class.getSimpleName(), "o.ilcekodu='" + kisikimlikPojo.getNufusaKayitliOlduguIlceKodu() + "'"));
			this.kisikimlik.setMahalle(kisikimlikPojo.getMahalle());
		}
		this.kisikimlik.setKpsdogrulamatarih(new Date());
		this.kisikimlik.setCiltno(this.kisikimlikPojo.getCiltNo());
		this.kisikimlik.setMahalle(this.kisikimlikPojo.getMahalle());
		// this.kisikimlik.setCiltAciklama(this.kisikimlikPojo.getCiltAciklama());
		this.kisikimlik.setAileno(this.kisikimlikPojo.getAileSiraNo());
		this.kisikimlik.setSirano(this.kisikimlikPojo.getBireySiraNo());
	}

	private Adres kisiAdresAktar(PojoAdres kpsPojo, AdresTuru adresTuru) throws Exception {
		Adres yeniAdres = new Adres();
		yeniAdres.setAdresturu(adresTuru);
		yeniAdres.setAdrestipi(kpsPojo.getAdrestipi());
		yeniAdres.setAcikAdres(kpsPojo.getAcikAdres());
		yeniAdres.setAdresNo(kpsPojo.getAdresNo());
		if (!isEmpty(kpsPojo.getBeyanTarihi()) && !kpsPojo.getBeyanTarihi().toString().equalsIgnoreCase("null/null/null")) {
			yeniAdres.setBeyanTarihi(DateUtil.createDateObject(kpsPojo.getBeyanTarihi()));
		}
		yeniAdres.setBinaAda(kpsPojo.getBinaAda());
		yeniAdres.setBinaBlokAdi(kpsPojo.getBinaBlokAdi());
		yeniAdres.setBinaKodu(kpsPojo.getBinaKodu());
		yeniAdres.setBinaPafta(kpsPojo.getBinaPafta());
		yeniAdres.setBinaParsel(kpsPojo.getBinaParsel());
		yeniAdres.setBinaPafta(kpsPojo.getBinaPafta());
		yeniAdres.setBinaSiteAdi(kpsPojo.getBinaSiteAdi());
		yeniAdres.setCsbm(kpsPojo.getCsbm());
		yeniAdres.setCsbmKodu(kpsPojo.getCsbmKodu());
		yeniAdres.setDisKapiNo(kpsPojo.getDisKapiNo());
		yeniAdres.setIcKapiNo(kpsPojo.getIcKapiNo());
		if (!isEmpty(kpsPojo.getIlKodu()) && isNumber(kpsPojo.getIlKodu().trim())) {
			if (getDBOperator().recordCount(Sehir.class.getSimpleName(), "o.ilKodu='" + kpsPojo.getIlKodu() + "'") > 0) {
				yeniAdres.setSehirRef((Sehir) getDBOperator().find(Sehir.class.getSimpleName(), "o.ilKodu='" + kpsPojo.getIlKodu() + "'"));
			}
		}
		if (!isEmpty(kpsPojo.getIlceKodu()) && isNumber(kpsPojo.getIlceKodu().trim())) {
			if (getDBOperator().recordCount(Ilce.class.getSimpleName(), "o.ilcekodu='" + kpsPojo.getIlceKodu() + "'") > 0) {
				yeniAdres.setIlceRef((Ilce) getDBOperator().find(Ilce.class.getSimpleName(), "o.ilcekodu='" + kpsPojo.getIlceKodu() + "'"));
			}
		}
		yeniAdres.setKpsGuncellemeTarihi(new Date());
		yeniAdres.setMahalle(kpsPojo.getMahalle());
		yeniAdres.setMahalleKodu(kpsPojo.getMahalleKodu());
		if (kpsPojo.getTasinmaTarihi() != null && !kpsPojo.getTasinmaTarihi().toString().equalsIgnoreCase("null/null/null")) {
			yeniAdres.setTasinmaTarihi(DateUtil.createDateObject(kpsPojo.getTasinmaTarihi()));
		}
		if (kpsPojo.getTescilTarihi() != null && !kpsPojo.getTescilTarihi().toString().equalsIgnoreCase("null/null/null")) {
			yeniAdres.setTescilTarihi(DateUtil.createDateObject(kpsPojo.getTescilTarihi()));
		}
		yeniAdres.setVarsayilan(EvetHayir._HAYIR);
		return yeniAdres;
	}

	@Override
	public void wizardForward() {
		if (this.wizardStep == 1) {
			// Kisi ve kisikimlik bilgileri kaydedilecek...
			try {

				if (!kisiVeritabanindaBulundu && getKisi().getRID() == null) {
					getDBOperator().insert(getKisi());
					if (getKisiController().isLoggable()) {
						logKaydet(getKisi(), IslemTuru._EKLEME, "");
					}
					this.kisikimlik.setKisiRef(getKisi());
					getDBOperator().insert(getKisikimlik());
					if (getKisikimlikController().isLoggable()) {
						logKaydet(this.kisikimlik, IslemTuru._EKLEME, "");
					}
					createGenericMessage(KeyUtil.getMessageValue("kisi.kaydedildi"), FacesMessage.SEVERITY_INFO);
				} else {
					if (this.kisikimlikPojo.getTcKimlikNo() != null) {
						this.kisi.setKimlikno(this.kisikimlikPojo.getTcKimlikNo());
					}
				}
				try {
					this.personelGorev = new PersonelBirimGorev();
					this.personelGorev.setBirimRef(getSessionUser().getPersonelRef().getBirimRef());
					this.personelGorev.setDurum(PersonelGorevDurum._AKTIF);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					logYaz("Exception @" + getModelName() + "Controller :", e);
				}
				wizardStep++;
			} catch (DBException e) {
				createGenericMessage(KeyUtil.getMessageValue("kisi.hata"), FacesMessage.SEVERITY_ERROR);
				logYaz("Exception @" + getModelName() + "Controller :", e);
			}
		} else {
			wizardStep++;
		}
	}

	@Override
	public void wizardBackward() {
		wizardStep--;
	}

	@Override
	public int getWizardStep() {
		return wizardStep;
	}

	@Override
	public void setWizardStep(int wizardStep) {
		this.wizardStep = wizardStep;
	}

	public Kisi getKisi() {
		return kisi;
	}

	public void setKisi(Kisi kisi) {
		this.kisi = kisi;
	}

	public Kisikimlik getKisikimlik() {
		return kisikimlik;
	}

	public void setKisikimlik(Kisikimlik kisikimlik) {
		this.kisikimlik = kisikimlik;
	}

	public Telefon getTelefon1() {
		return telefon1;
	}

	public void setTelefon1(Telefon telefon1) {
		this.telefon1 = telefon1;
	}

	public Telefon getTelefon2() {
		return telefon2;
	}

	public void setTelefon2(Telefon telefon2) {
		this.telefon2 = telefon2;
	}

	public Telefon getTelefon3() {
		return telefon3;
	}

	public void setTelefon3(Telefon telefon3) {
		this.telefon3 = telefon3;
	}

	public Telefon getTelefon4() {
		return telefon4;
	}

	public void setTelefon4(Telefon telefon4) {
		this.telefon4 = telefon4;
	}

	public Internetadres getInternetAdres1() {
		return internetAdres1;
	}

	public void setInternetAdres1(Internetadres internetAdres1) {
		this.internetAdres1 = internetAdres1;
	}

	private KisiController kisiController = new KisiController(null);
	private KisikimlikController kisikimlikController = new KisikimlikController(null);
	private PersonelController personelController = new PersonelController(null);
	private PersonelBirimGorevController personelBirimGorevController = new PersonelBirimGorevController(null);

	public PersonelBirimGorevController getPersonelBirimGorevController() {
		return personelBirimGorevController;
	}

	public void setPersonelBirimGorevController(PersonelBirimGorevController personelBirimGorevController) {
		this.personelBirimGorevController = personelBirimGorevController;
	}

	public PersonelController getPersonelController() {
		return personelController;
	}

	public void setPersonelController(PersonelController personelController) {
		this.personelController = personelController;
	}

	public KisiController getKisiController() {
		return kisiController;
	}

	public void setKisiController(KisiController kisiController) {
		this.kisiController = kisiController;
	}

	public KisikimlikController getKisikimlikController() {
		return kisikimlikController;
	}

	public void setKisikimlikController(KisikimlikController kisikimlikController) {
		this.kisikimlikController = kisikimlikController;
	}

	public Kurum getKurum() {
		return kurum;
	}

	public void setKurum(Kurum kurum) {
		this.kurum = kurum;
	}

	public String getDetailEditType() {
		return detailEditType;
	}

	public void setDetailEditType(String detailEditType) {
		this.detailEditType = detailEditType;
	}

	public Birim getBirim() {
		return birim;
	}

	public void setBirim(Birim birim) {
		this.birim = birim;
	}

	public boolean isKpsSorguSonucDondu() {
		return kpsSorguSonucDondu;
	}

	public void setKpsSorguSonucDondu(boolean kpsSorguSonucDondu) {
		this.kpsSorguSonucDondu = kpsSorguSonucDondu;
	}

	public boolean isKpsKaydiBulundu() {
		return kpsKaydiBulundu;
	}

	public void setKpsKaydiBulundu(boolean kpsKaydiBulundu) {
		this.kpsKaydiBulundu = kpsKaydiBulundu;
	}

	public Adres getKpsAdres() {
		return kpsAdres;
	}

	public void setKpsAdres(Adres kpsAdres) {
		this.kpsAdres = kpsAdres;
	}

	public Adres getKpsDigerAdres() {
		return kpsDigerAdres;
	}

	public void setKpsDigerAdres(Adres kpsDigerAdres) {
		this.kpsDigerAdres = kpsDigerAdres;
	}

	public Personel getPersonel() {
		personel = (Personel) getEntity();
		return personel;
	}

	public void setPersonel(Personel personel) {
		this.personel = personel;
	}

	public boolean isKisiVeritabanindaBulundu() {
		return kisiVeritabanindaBulundu;
	}

	public void setKisiVeritabanindaBulundu(boolean kisiVeritabanindaBulundu) {
		this.kisiVeritabanindaBulundu = kisiVeritabanindaBulundu;
	}

	public PersonelBirimGorev getPersonelGorev() {
		return personelGorev;
	}

	public void setPersonelGorev(PersonelBirimGorev personelBirimGorev) {
		this.personelGorev = personelBirimGorev;
	}

	public SelectItem[] getSelectBirimList() {
		@SuppressWarnings("unchecked")
		List<Birim> entityList = getDBOperator().load(Birim.class.getSimpleName(), "", "o.ad");
		if (entityList != null) {
			SelectItem[] selectItemList = new SelectItem[entityList.size() + 1];
			selectItemList[0] = new SelectItem(null, "");
			int i = 1;
			for (BaseEntity entity : entityList) {
				selectItemList[i++] = new SelectItem(entity, entity.getUIString());
			}
			return selectItemList;
		} else {
			return new SelectItem[0];
		}
	}

	public SelectItem[] getSelectPersonelBirimGorevList() {
		@SuppressWarnings("unchecked")
		List<Gorev> entityList = getDBOperator().load(Gorev.class.getSimpleName(), "", "o.ad");
		if (entityList != null) {
			SelectItem[] selectItemList = new SelectItem[entityList.size() + 1];
			selectItemList[0] = new SelectItem(null, "");
			int i = 1;
			for (BaseEntity entity : entityList) {
				selectItemList[i++] = new SelectItem(entity, entity.getUIString());
			}
			return selectItemList;
		} else {
			return new SelectItem[0];
		}
	}

	public String callDetailFromWizard() {
		return getPersonelController().detailPage(null);
	}

} // class
