package tr.com.arf.toys.view.controller.form.iletisim;

import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.event.AjaxBehaviorEvent;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.framework.utility.exception.data.DBException;
import tr.com.arf.toys.db.enumerated.iletisim.ReferansTipi;
import tr.com.arf.toys.db.filter.iletisim.IletisimlistedetayFilter;
import tr.com.arf.toys.db.model.egitim.Egitim;
import tr.com.arf.toys.db.model.egitim.Egitimkatilimci;
import tr.com.arf.toys.db.model.egitim.Egitimogretmen;
import tr.com.arf.toys.db.model.etkinlik.Etkinlik;
import tr.com.arf.toys.db.model.etkinlik.Etkinlikkatilimci;
import tr.com.arf.toys.db.model.iletisim.IletisimListeUye;
import tr.com.arf.toys.db.model.iletisim.Iletisimliste;
import tr.com.arf.toys.db.model.iletisim.Iletisimlistedetay;
import tr.com.arf.toys.db.model.kisi.IsYeriTemsilcileri;
import tr.com.arf.toys.db.model.kisi.Kisi;
import tr.com.arf.toys.db.model.kisi.KomisyonDonemUye;
import tr.com.arf.toys.db.model.kisi.Kurul;
import tr.com.arf.toys.db.model.kisi.KurulDonemUye;
import tr.com.arf.toys.db.model.kisi.Kurum;
import tr.com.arf.toys.db.model.system.Birim;
import tr.com.arf.toys.db.model.system.Komisyon;
import tr.com.arf.toys.db.model.uye.Uye;
import tr.com.arf.toys.db.model.uye.Uyebilirkisi;
import tr.com.arf.toys.service.application.ApplicationDescriptor;
import tr.com.arf.toys.service.application.ManagedBeanLocator;
import tr.com.arf.toys.view.controller.form._base.ToysBaseFilteredController;

@ManagedBean
@ViewScoped
public class IletisimlistedetayController extends ToysBaseFilteredController {

	private static final long serialVersionUID = 6531152165884980748L;

	private Iletisimlistedetay iletisimlistedetay;
	private IletisimlistedetayFilter iletisimlistedetayFilter = new IletisimlistedetayFilter();
	// private final EpostaanlikgonderimlogFilter epostaanlikgonderimlogFilter =
	// new EpostaanlikgonderimlogFilter();
	long kisiTemp = 0;
	long uyeTemp = 0;
	long birimTemp = 0;
	long kurumTemp = 0;
	long kurulTemp = 0;
	long komisyonTemp = 0;
	long egitmenTemp = 0;
	long egitimkatilimciTemp = 0;
	long etkinlikkatilimciTemp = 0;
	long isYeriTemsilcileriTemp = 0;
	long uyebilirkisiTemp = 0;
	private Kurum kurum;
	private Kisi kisi;
	private Uye uye;

	public IletisimlistedetayController() {
		super(Iletisimlistedetay.class);
		setDefaultValues(false);
		setOrderField("Referans");
		setLoggable(false);
		setAutoCompleteSearchColumns(new String[] { "Referans" });
		setTable(null);
		if (getMasterEntity() != null) {
			setMasterObjectName(ApplicationDescriptor._ILETISIMLISTE_MODEL);
			setMasterOutcome(ApplicationDescriptor._ILETISIMLISTE_MODEL);
			getIletisimlistedetayFilter().setIletisimlisteRef(getMasterEntity());
			getIletisimlistedetayFilter().createQueryCriterias();
		}
		queryAction();
	}

	public Iletisimliste getMasterEntity() {
		if (getIletisimlistedetayFilter().getIletisimlisteRef() != null) {
			return getIletisimlistedetayFilter().getIletisimlisteRef();
		} else {
			return (Iletisimliste) getObjectFromSessionFilter(ApplicationDescriptor._ILETISIMLISTE_MODEL);
		}
	}

	public Iletisimlistedetay getIletisimlistedetay() {
		iletisimlistedetay = (Iletisimlistedetay) getEntity();
		return iletisimlistedetay;
	}

	public void setIletisimlistedetay(Iletisimlistedetay iletisimlistedetay) {
		this.iletisimlistedetay = iletisimlistedetay;
	}

	public IletisimlistedetayFilter getIletisimlistedetayFilter() {
		return iletisimlistedetayFilter;
	}

	public void setIletisimlistedetayFilter(IletisimlistedetayFilter iletisimlistedetayFilter) {
		this.iletisimlistedetayFilter = iletisimlistedetayFilter;
	}

	public Kurum getKurum() {
		return kurum;
	}

	public void setKurum(Kurum kurum) {
		this.kurum = kurum;
	}

	public Kisi getKisi() {
		return kisi;
	}

	public void setKisi(Kisi kisi) {
		this.kisi = kisi;
	}

	public Uye getUye() {
		return uye;
	}

	public void setUye(Uye uye) {
		this.uye = uye;
	}

	@Override
	public BaseFilter getFilter() {
		return getIletisimlistedetayFilter();
	}

	@Override
	public void resetFilter() {
		super.resetFilterOnly();
		if (getMasterEntity() != null) {
			getIletisimlistedetayFilter().setIletisimlisteRef(getMasterEntity());
		}
		queryAction();
	}

	@Override
	public void insert() {
		super.insert();
		getIletisimlistedetay().setIletisimlisteRef(getIletisimlistedetayFilter().getIletisimlisteRef());
	}

	@Override
	public void save() {
		// this.iletisimlistedetay.setReferans(getMasterEntity().getRID());
		if (kisiTemp > 0) {
			this.iletisimlistedetay.setReferans(kisiTemp);
		} else if (uyeTemp > 0) {
			this.iletisimlistedetay.setReferans(uyeTemp);
			uyeTemp = 0;
		} else if (birimTemp > 0) {
			this.iletisimlistedetay.setReferans(birimTemp);
			birimTemp = 0;
		} else if (kurumTemp > 0) {
			this.iletisimlistedetay.setReferans(kurumTemp);
			kurumTemp = 0;
		} else if (kurulTemp > 0) {
			this.iletisimlistedetay.setReferans(kurulTemp);
			kurulTemp = 0;
		} else if (komisyonTemp > 0) {
			this.iletisimlistedetay.setReferans(komisyonTemp);
			komisyonTemp = 0;
		} else if (egitmenTemp > 0) {
			this.iletisimlistedetay.setReferans(egitmenTemp);
			egitmenTemp = 0;
		} else if (egitimkatilimciTemp > 0) {
			this.iletisimlistedetay.setReferans(egitimkatilimciTemp);
			egitimkatilimciTemp = 0;
		} else if (etkinlikkatilimciTemp > 0) {
			this.iletisimlistedetay.setReferans(etkinlikkatilimciTemp);
			etkinlikkatilimciTemp = 0;
		} else if (isYeriTemsilcileriTemp > 0) {
			this.iletisimlistedetay.setReferans(isYeriTemsilcileriTemp);
			isYeriTemsilcileriTemp = 0;
		} else if (uyebilirkisiTemp > 0) {
			this.iletisimlistedetay.setReferans(uyebilirkisiTemp);
			uyebilirkisiTemp = 0;
		} else {
			this.iletisimlistedetay.setReferans(0L);
		}
		super.save();
	}

	public boolean handleReferansTipiChange(AjaxBehaviorEvent event1) {
		try {
			HtmlSelectOneMenu menu = (HtmlSelectOneMenu) event1.getComponent();
			if (menu.getValue() instanceof ReferansTipi) {
				if (this.iletisimlistedetay.getReferansTipi() == ReferansTipi._KISI) {
					return true;
				}

			} else {
				return false;
			}
		} catch (Exception e) {
			logYaz("Exception @" + getModelName() + "Controller :", e);
		}
		return true;
	}

	public long handleKisiChange(AjaxBehaviorEvent event2) {
		try {
			if (getKisi() != null) {
				this.iletisimlistedetay.setReferans(getKisi().getRID());
				kisiTemp = getKisi().getRID();
				return kisiTemp;
			} else {
				return 0;
			}
		} catch (Exception e) {
			logYaz("Exception @" + getModelName() + "Controller :", e);
		}
		return 0;
	}

	public long handleUyeChange(AjaxBehaviorEvent event3) {
		try {
			if (getUye() != null) {
				this.iletisimlistedetay.setReferans(getUye().getRID());
				uyeTemp = getUye().getRID();
				return uyeTemp;
			} else {
				return 0;
			}
		} catch (Exception e) {
			logYaz("Exception @" + getModelName() + "Controller :", e);
		}
		return 0;
	}

	public long handleBirimChange(AjaxBehaviorEvent event4) {
		try {
			HtmlSelectOneMenu menuForBirim = (HtmlSelectOneMenu) event4.getComponent();
			if (menuForBirim.getValue() instanceof Birim) {
				this.iletisimlistedetay.setReferans(((Birim) menuForBirim.getValue()).getRID());
				birimTemp = ((Birim) menuForBirim.getValue()).getRID();
				return birimTemp;
			} else {
				return 0;
			}
		} catch (Exception e) {
			logYaz("Exception @" + getModelName() + "Controller :", e);
		}
		return 0;
	}

	public long handleKurumChange(AjaxBehaviorEvent event5) {
		try {
			if (getKurum() != null) {
				this.iletisimlistedetay.setReferans(getKurum().getRID());
				kurumTemp = getKurum().getRID();
				return kurumTemp;
			} else {
				return 0;
			}
		} catch (Exception e) {
			logYaz("Exception @" + getModelName() + "Controller :", e);
		}
		return 0;
	}

	public long handleKurulChange(AjaxBehaviorEvent event6) {
		try {
			HtmlSelectOneMenu menuForKurul = (HtmlSelectOneMenu) event6.getComponent();
			if (menuForKurul.getValue() instanceof Kurul) {
				this.iletisimlistedetay.setReferans(((Kurul) menuForKurul.getValue()).getRID());
				kurulTemp = ((Kurul) menuForKurul.getValue()).getRID();
				return kurulTemp;
			} else {
				return 0;
			}
		} catch (Exception e) {
			logYaz("Exception @" + getModelName() + "Controller :", e);
		}
		return 0;
	}

	public long handleKomisyonChange(AjaxBehaviorEvent event7) {
		try {
			HtmlSelectOneMenu menuForKomisyon = (HtmlSelectOneMenu) event7.getComponent();
			if (menuForKomisyon.getValue() instanceof Komisyon) {
				this.iletisimlistedetay.setReferans(((Komisyon) menuForKomisyon.getValue()).getRID());
				komisyonTemp = ((Komisyon) menuForKomisyon.getValue()).getRID();
				return komisyonTemp;
			} else {
				return 0;
			}
		} catch (Exception e) {
			logYaz("Exception @" + getModelName() + "Controller :", e);
		}
		return 0;
	}

	public long handleEgitmenChange(AjaxBehaviorEvent event8) {
		try {
			HtmlSelectOneMenu menuForEgitmen = (HtmlSelectOneMenu) event8.getComponent();
			if (menuForEgitmen.getValue() instanceof Egitimogretmen) {
				this.iletisimlistedetay.setReferans(((Egitimogretmen) menuForEgitmen.getValue()).getRID());
				egitmenTemp = ((Egitimogretmen) menuForEgitmen.getValue()).getRID();
				return egitmenTemp;
			} else {
				return 0;
			}
		} catch (Exception e) {
			logYaz("Exception @" + getModelName() + "Controller :", e);
		}
		return 0;
	}

	public long handleEgitimChange(AjaxBehaviorEvent event9) {
		try {
			HtmlSelectOneMenu menuForEgitimKatilimci = (HtmlSelectOneMenu) event9.getComponent();
			if (menuForEgitimKatilimci.getValue() instanceof Egitim) {
				this.iletisimlistedetay.setReferans(((Egitim) menuForEgitimKatilimci.getValue()).getRID());
				egitimkatilimciTemp = ((Egitim) menuForEgitimKatilimci.getValue()).getRID();
				return egitimkatilimciTemp;
			} else {
				return 0;
			}
		} catch (Exception e) {
			logYaz("Exception @" + getModelName() + "Controller :", e);
		}
		return 0;
	}

	public long handleEtkinlikChange(AjaxBehaviorEvent event10) {
		try {
			HtmlSelectOneMenu menuForEtkinlik = (HtmlSelectOneMenu) event10.getComponent();
			if (menuForEtkinlik.getValue() instanceof Etkinlik) {
				this.iletisimlistedetay.setReferans(((Etkinlik) menuForEtkinlik.getValue()).getRID());
				etkinlikkatilimciTemp = ((Etkinlik) menuForEtkinlik.getValue()).getRID();
				return etkinlikkatilimciTemp;
			} else {
				return 0;
			}
		} catch (Exception e) {
			logYaz("Exception @" + getModelName() + "Controller :", e);
		}
		return 0;
	}

	public long handleIsYeriTemsilcileriChange(AjaxBehaviorEvent event11) {
		try {
			HtmlSelectOneMenu menuForIsYeriTemsilcileri = (HtmlSelectOneMenu) event11.getComponent();
			if (menuForIsYeriTemsilcileri.getValue() instanceof IsYeriTemsilcileri) {
				this.iletisimlistedetay.setReferans(((IsYeriTemsilcileri) menuForIsYeriTemsilcileri.getValue()).getRID());
				isYeriTemsilcileriTemp = ((IsYeriTemsilcileri) menuForIsYeriTemsilcileri.getValue()).getRID();
				return isYeriTemsilcileriTemp;
			} else {
				return 0;
			}
		} catch (Exception e) {
			logYaz("Exception @" + getModelName() + "Controller :", e);
		}
		return 0;
	}

	public long handleUyebilirkisiChange(AjaxBehaviorEvent event12) {
		try {
			HtmlSelectOneMenu menuForUyebilirkisi = (HtmlSelectOneMenu) event12.getComponent();
			if (menuForUyebilirkisi.getValue() instanceof Uyebilirkisi) {
				this.iletisimlistedetay.setReferans(((Uyebilirkisi) menuForUyebilirkisi.getValue()).getRID());
				uyebilirkisiTemp = ((Uyebilirkisi) menuForUyebilirkisi.getValue()).getRID();
				return uyebilirkisiTemp;
			} else {
				return 0;
			}
		} catch (Exception e) {
			logYaz("Exception @" + getModelName() + "Controller :", e);
		}
		return 0;
	}

	public String getKisiAd() {
		return " G G ";
	}

	public String listeuyeleri() {
		if (!setSelected()) {
			return "";
		}
		getSessionUser().setLegalAccess(1);
		putObjectToSessionFilter(ApplicationDescriptor._ILETISIMLISTEDETAY_MODEL, getIletisimlistedetay());
		return ManagedBeanLocator.locateMenuController().gotoPage("menu_ListeUye");
	}

	@SuppressWarnings({ "unchecked" })
	public List<IletisimListeUye> getListeuye() {
		List<IletisimListeUye> uyeListele = new ArrayList<IletisimListeUye>();
		IletisimListeUye iletisimlisteuye = new IletisimListeUye();
		try {
			if (getMasterEntity() != null) {
				if (getDBOperator().recordCount(ApplicationDescriptor._ILETISIMLISTEDETAY_MODEL, "o.iletisimlisteRef.rID=" + getMasterEntity().getRID()) > 0) {
					List<Iletisimlistedetay> iletisimListesi = getDBOperator().load(ApplicationDescriptor._ILETISIMLISTEDETAY_MODEL,
							"o.iletisimlisteRef.rID=" + getMasterEntity().getRID(), "o.rID");
					for (Iletisimlistedetay entity : iletisimListesi) {
						if (entity.getReferansTipi() == ReferansTipi._KISI) {
							if (getDBOperator().recordCount(ApplicationDescriptor._KISI_MODEL, "o.rID=" + entity.getReferans()) > 0) {
								Kisi kisi = (Kisi) getDBOperator().load(ApplicationDescriptor._KISI_MODEL, "o.rID=" + entity.getReferans(), "o.rID").get(0);
								iletisimlisteuye = new IletisimListeUye();
								iletisimlisteuye.setUyeler(kisi.getUIStringShort());
								uyeListele.add(iletisimlisteuye);
							}
						} else if (entity.getReferansTipi() == ReferansTipi._UYE) {
							if (getDBOperator().recordCount(ApplicationDescriptor._UYE_MODEL, "o.rID=" + entity.getReferans()) > 0) {
								Uye uye = (Uye) getDBOperator().load(ApplicationDescriptor._UYE_MODEL, "o.rID=" + entity.getReferans(), "o.rID").get(0);
								iletisimlisteuye = new IletisimListeUye();
								iletisimlisteuye.setUyeler(uye.getKisiRef().getUIStringShort());
								uyeListele.add(iletisimlisteuye);
							}
						} else if (entity.getReferansTipi() == ReferansTipi._BIRIM) {
							if (getDBOperator().recordCount(ApplicationDescriptor._UYE_MODEL, "o.birimRef.rID=" + entity.getReferans()) > 0) {
								List<Uye> uyeList = getDBOperator().load(ApplicationDescriptor._UYE_MODEL, "o.birimRef.rID=" + entity.getReferans(), "o.rID");
								for (Uye uyeler : uyeList) {
									iletisimlisteuye = new IletisimListeUye();
									iletisimlisteuye.setUyeler(uyeler.getKisiRef().getUIStringShort());
									uyeListele.add(iletisimlisteuye);
								}
							}
						} else if (entity.getReferansTipi() == ReferansTipi._KURUM) {
							if (getDBOperator().recordCount(ApplicationDescriptor._KURUM_MODEL, "o.rID=" + entity.getReferans()) > 0) {
								Kurum kurum = (Kurum) getDBOperator().load(ApplicationDescriptor._KURUM_MODEL, "o.rID=" + entity.getReferans(), "o.rID").get(0);
								iletisimlisteuye = new IletisimListeUye();
								iletisimlisteuye.setUyeler(kurum.getAd());
								uyeListele.add(iletisimlisteuye);
							}
						} else if (entity.getReferansTipi() == ReferansTipi._KURUL) {
							if (getDBOperator().recordCount(ApplicationDescriptor._KURULDONEMUYE_MODEL, "o.kurulDonemRef.rID=" + entity.getReferans()) > 0) {
								List<KurulDonemUye> krluyelist = getDBOperator().load(ApplicationDescriptor._KURULDONEMUYE_MODEL, "o.kurulDonemRef.rID=" + entity.getReferans(),
										"o.rID");
								for (KurulDonemUye kuruluye : krluyelist) {
									iletisimlisteuye = new IletisimListeUye();
									iletisimlisteuye.setUyeler(kuruluye.getUyeRef().getKisiRef().getUIStringShort());
									uyeListele.add(iletisimlisteuye);
								}
							}
						} else if (entity.getReferansTipi() == ReferansTipi._KOMISYON) {
							if (getDBOperator().recordCount(ApplicationDescriptor._KOMISYONDONEMUYE_MODEL, "o.komisyonDonemRef.rID=" + entity.getReferans()) > 0) {
								List<KomisyonDonemUye> kmsynuyelist = getDBOperator().load(ApplicationDescriptor._KOMISYONDONEMUYE_MODEL,
										"o.komisyonDonemRef.rID=" + entity.getReferans(), "o.rID");
								for (KomisyonDonemUye komisyonuye : kmsynuyelist) {
									if (komisyonuye.getKisiRef() != null) {
										iletisimlisteuye = new IletisimListeUye();
										iletisimlisteuye.setUyeler(komisyonuye.getKisiRef().getUIStringShort());
										uyeListele.add(iletisimlisteuye);
									}

								}
							}
						} else if (entity.getReferansTipi() == ReferansTipi._EGITMEN) {
							if (getDBOperator().recordCount(ApplicationDescriptor._EGITIMOGRETMEN_MODEL, "o.egitimRef.rID=" + entity.getReferans()) > 0) {
								List<Egitimogretmen> egtmnList = getDBOperator().load(ApplicationDescriptor._EGITIMOGRETMEN_MODEL, "o.egitimRef.rID=" + entity.getReferans(),
										"o.rID");
								for (Egitimogretmen egitmn : egtmnList) {
									iletisimlisteuye = new IletisimListeUye();
									iletisimlisteuye.setUyeler(egitmn.getEgitmenRef().getKisiRef().getUIStringShort());
									uyeListele.add(iletisimlisteuye);
								}
							}
						} else if (entity.getReferansTipi() == ReferansTipi._EGITIMKATILIMCI) {
							if (getDBOperator().recordCount(ApplicationDescriptor._EGITIMKATILIMCI_MODEL, "o.egitimRef.rID=" + entity.getReferans()) > 0) {
								List<Egitimkatilimci> egtmKatilimciList = getDBOperator().load(ApplicationDescriptor._EGITIMKATILIMCI_MODEL,
										"o.egitimRef.rID=" + entity.getReferans(), "o.rID");
								for (Egitimkatilimci egtm : egtmKatilimciList) {
									iletisimlisteuye = new IletisimListeUye();
									iletisimlisteuye.setUyeler(egtm.getKisiRef().getUIStringShort());
									uyeListele.add(iletisimlisteuye);
								}
							}
						} else if (entity.getReferansTipi() == ReferansTipi._ETKINLIKKATILIMCI) {
							if (getDBOperator().recordCount(ApplicationDescriptor._ETKINLIKKATILIMCI_MODEL, "o.etkinlikRef.rID=" + entity.getReferans()) > 0) {
								List<Etkinlikkatilimci> etknlkList = getDBOperator().load(ApplicationDescriptor._ETKINLIKKATILIMCI_MODEL,
										"o.etkinlikRef.rID=" + entity.getReferans(), "o.rID");
								for (Etkinlikkatilimci etknlk : etknlkList) {
									iletisimlisteuye = new IletisimListeUye();
									iletisimlisteuye.setUyeler(etknlk.getKisiRef().getUIStringShort());
									uyeListele.add(iletisimlisteuye);
								}
							}
						} else if (entity.getReferansTipi() == ReferansTipi._ISYERITEMSILCISI) {
							if (getDBOperator().recordCount(ApplicationDescriptor._ISYERITEMSILCILERI_MODEL, "o.rID=" + entity.getReferans()) > 0) {
								IsYeriTemsilcileri isyrtemsilcisi = (IsYeriTemsilcileri) getDBOperator().load(ApplicationDescriptor._ISYERITEMSILCILERI_MODEL,
										"o.rID=" + entity.getReferans(), "o.rID").get(0);
								if (isyrtemsilcisi.getUyeRef() != null) {
									iletisimlisteuye = new IletisimListeUye();
									iletisimlisteuye.setUyeler(isyrtemsilcisi.getUyeRef().getKisiRef().getUIStringShort());
									uyeListele.add(iletisimlisteuye);
								}
							}
						}
					}
				}
			}
		} catch (DBException e) {
			logYaz("Iletisimlistedetay @listeuye=" + e.getMessage());
			logYaz("Exception @" + getModelName() + "Controller :", e);
		}
		return uyeListele;

	}

} // class
