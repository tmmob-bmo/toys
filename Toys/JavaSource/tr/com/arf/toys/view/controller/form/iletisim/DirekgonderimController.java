package tr.com.arf.toys.view.controller.form.iletisim;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.component.html.HtmlCommandButton;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;
import javax.faces.event.AjaxBehaviorEvent;

import org.primefaces.event.CloseEvent;

import tr.com.arf.framework.db.enumerated._common.EvetHayir;
import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.framework.db.model._base.BaseEntity;
import tr.com.arf.framework.utility.exception.data.DBException;
import tr.com.arf.framework.utility.tool.KeyUtil;
import tr.com.arf.toys.db.enumerated.iletisim.GonderimDurum;
import tr.com.arf.toys.db.enumerated.iletisim.GonderimTuru;
import tr.com.arf.toys.db.enumerated.iletisim.IcerikTuru;
import tr.com.arf.toys.db.enumerated.iletisim.ReferansTipi;
import tr.com.arf.toys.db.enumerated.system.PersonelDurum;
import tr.com.arf.toys.db.filter.iletisim.EpostaanlikgonderimlogFilter;
import tr.com.arf.toys.db.filter.iletisim.GonderimFilter;
import tr.com.arf.toys.db.manager.CustomDBOperator;
import tr.com.arf.toys.db.model.egitim.Egitimkatilimci;
import tr.com.arf.toys.db.model.egitim.Egitimogretmen;
import tr.com.arf.toys.db.model.etkinlik.Etkinlikkatilimci;
import tr.com.arf.toys.db.model.iletisim.Epostagonderimlog;
import tr.com.arf.toys.db.model.iletisim.Gonderim;
import tr.com.arf.toys.db.model.iletisim.Icerik;
import tr.com.arf.toys.db.model.iletisim.Iletisimliste;
import tr.com.arf.toys.db.model.iletisim.Iletisimlistedetay;
import tr.com.arf.toys.db.model.iletisim.Smsgonderimlog;
import tr.com.arf.toys.db.model.kisi.IsYeriTemsilcileri;
import tr.com.arf.toys.db.model.kisi.Kisi;
import tr.com.arf.toys.db.model.system.Birim;
import tr.com.arf.toys.db.model.system.KurumKisi;
import tr.com.arf.toys.db.model.system.Personel;
import tr.com.arf.toys.db.model.system.SistemParametre;
import tr.com.arf.toys.db.model.uye.Uye;
import tr.com.arf.toys.service.application.ApplicationDescriptor;
import tr.com.arf.toys.service.application.ManagedBeanLocator;
import tr.com.arf.toys.utility.tool.SendEmail;
import tr.com.arf.toys.view.controller.form._base.ToysBaseFilteredController;

@ManagedBean
@ViewScoped
public class DirekgonderimController extends ToysBaseFilteredController {

	private static final long serialVersionUID = 6531152165884980748L;

	private Gonderim gonderim;
	public Gonderim gonderim1;
	private GonderimFilter gonderimFilter = new GonderimFilter();
	public EvetHayir icerikOrEditor = EvetHayir._EVET;
	private final EpostaanlikgonderimlogFilter epostaanlikgonderimlogFilter = new EpostaanlikgonderimlogFilter();
	Iletisimliste iletisimlisteRef = new Iletisimliste();
	String sEmailMi; // 1:eposta, 2: sms

	/* Data Operator */
	private CustomDBOperator customDBOperator;
	private SendEmail email = new SendEmail();

	/* Data Operator */
	protected final CustomDBOperator getCustomDBOperator() {
		if (customDBOperator == null) {
			customDBOperator = CustomDBOperator.getInstance();
		}
		return customDBOperator;
	}

	public DirekgonderimController() {
		super(Gonderim.class);
		setDefaultValues(false);
		setOrderField("rID");
		setLoggable(false);
		FacesContext context = FacesContext.getCurrentInstance();
		Map<String, String> queryStringParams = context.getExternalContext().getRequestParameterMap();
		sEmailMi = queryStringParams.get("type");
		setAutoCompleteSearchColumns(new String[] { "rID" });
		setTable(null);
		if (getMasterEntity() != null) {
			setMasterObjectName(ApplicationDescriptor._ILETISIMLISTE_MODEL);
			setMasterOutcome(ApplicationDescriptor._ILETISIMLISTE_MODEL);
			getGonderimFilter().setIletisimlisteRef(getMasterEntity());
			getGonderimFilter().createQueryCriterias();
		}
		queryAction();
		insert();
	}

	public Iletisimliste getMasterEntity() {
		if (getGonderimFilter().getIletisimlisteRef() != null) {
			return getGonderimFilter().getIletisimlisteRef();
		} else {
			return (Iletisimliste) getObjectFromSessionFilter(ApplicationDescriptor._ILETISIMLISTE_MODEL);
		}
	}

	public Gonderim getGonderim() {
		gonderim = (Gonderim) getEntity();
		return gonderim;
	}

	public void setGonderim(Gonderim gonderim) {
		this.gonderim = gonderim;
	}

	public GonderimFilter getGonderimFilter() {
		return gonderimFilter;
	}

	public void setGonderimFilter(GonderimFilter gonderimFilter) {
		this.gonderimFilter = gonderimFilter;
	}

	public EvetHayir getIcerikOrEditor() {
		return icerikOrEditor;
	}

	public void setIcerikOrEditor(EvetHayir icerikOrEditor) {
		this.icerikOrEditor = icerikOrEditor;
	}

	@Override
	public BaseFilter getFilter() {
		return getGonderimFilter();
	}

	public String epostagonderimlog() {
		if (!setSelected()) {
			return "";
		}
		getSessionUser().setLegalAccess(1);
		putObjectToSessionFilter(ApplicationDescriptor._GONDERIM_MODEL, getGonderim());
		return ManagedBeanLocator.locateMenuController().gotoPage("menu_Epostagonderimlog");
	}

	public String smsgonderimlog() {
		if (!setSelected()) {
			return "";
		}
		getSessionUser().setLegalAccess(1);
		putObjectToSessionFilter(ApplicationDescriptor._GONDERIM_MODEL, getGonderim());
		return ManagedBeanLocator.locateMenuController().gotoPage("menu_Smsgonderimlog");
	}

	public boolean handleZamanlamaliChange(AjaxBehaviorEvent event) {
		try {
			HtmlSelectOneMenu menu = (HtmlSelectOneMenu) event.getComponent();
			if (menu.getValue() instanceof EvetHayir) {
				if (this.gonderim.getZamanlamali() == EvetHayir._EVET) {
					return true;
				}

			} else {
				return false;
			}
		} catch (Exception e) {
			logYaz("Exception @" + getModelName() + "Controller :", e);
		}
		return true;
	}

	/*
	 * public void handleIcerikOrEditor(AjaxBehaviorEvent event1) {
	 * RequestContext context = RequestContext.getCurrentInstance(); try {
	 * HtmlSelectOneMenu menuForIcerikOrEditor = (HtmlSelectOneMenu)
	 * event1.getComponent(); if (menuForIcerikOrEditor.getValue() instanceof
	 * EvetHayir) { if (getIcerikOrEditor().equals(EvetHayir._EVET)) {
	 * setIcerikOrEditor(EvetHayir._EVET); } else {
	 * setIcerikOrEditor(EvetHayir._HAYIR); }
	 *
	 * } else { setIcerikOrEditor(EvetHayir._HAYIR); }
	 * context.update("icerikSec"); context.update("editorOutputpanel"); } catch
	 * (Exception e) { logYaz("Exception @" + getModelName() +
	 * "Controller :", e); } }
	 */

	//
	// @SuppressWarnings("unused")
	// public Kisi handleKisi(SelectEvent event2) {
	// try {
	//
	// String deviceSelect = event2.getComponent().getId();
	// if (deviceSelect.equalsIgnoreCase("gonderenRefAutoComplete")) {
	// Kisi kisi=(Kisi) event2.getComponent()
	// }
	// HtmlSelectOneMenu menuForKisi = (HtmlSelectOneMenu)
	// event2.getComponent();
	// if (menuForKisi.getValue() instanceof Kisi) {
	// gonderenRef = (Kisi) menuForKisi.getValue();
	// return gonderenRef;
	// } else {
	// return null;
	// }
	// } catch (Exception e) {
	// logYaz("Exception @" + getModelName() + "Controller :", e);
	// }
	// return null;
	// }

	public Iletisimliste handleIletisimListe(AjaxBehaviorEvent event2) {
		try {
			HtmlSelectOneMenu menuForIletisimListe = (HtmlSelectOneMenu) event2.getComponent();
			if (menuForIletisimListe.getValue() instanceof Iletisimliste) {
				iletisimlisteRef = (Iletisimliste) menuForIletisimListe.getValue();
				return iletisimlisteRef;
			} else {
				return null;
			}
		} catch (Exception e) {
			logYaz("Exception @" + getModelName() + "Controller :", e);
		}
		return null;
	}

	@SuppressWarnings("unused")
	public Iletisimliste handleSave(AjaxBehaviorEvent event2) {
		try {
			HtmlCommandButton button = (HtmlCommandButton) event2.getComponent();
		} catch (Exception e) {
			logYaz("Exception @" + getModelName() + "Controller :", e);
		}
		return null;
	}

	public boolean getSMSKontrol() {
		if (this.gonderim.getGonderimturu() == GonderimTuru._SMS) {
			return false;
		}
		return true;
	}

	public boolean getEMailKontrol() {
		if (this.gonderim.getGonderimturu() == GonderimTuru._EPOSTA) {
			return false;
		}
		return true;
	}

	public void save(String afterSave) {
		if (sEmailMi.equals("1") || sEmailMi.equals("2")) {
			try {
				Birim gonderenBirim = getGonderim().getBirimRef();
				if (getIcerikOrEditor() == EvetHayir._HAYIR && isEmpty(getGonderim().getAciklama())) {
					createGenericMessage("İçerik giriniz!", FacesMessage.SEVERITY_ERROR);
					return;
				}
				setTable(null);
				if (sEmailMi.equals("1")) {
					getGonderim().setGonderimturu(GonderimTuru._EPOSTA);
				} else {
					getGonderim().setGonderimturu(GonderimTuru._SMS);
				}
				if (getMasterEntity() != null) {
					getGonderim().setIletisimlisteRef(getMasterEntity());
				} else {
					getGonderim().setIletisimlisteRef(iletisimlisteRef);
				}
				// getGonderim().setGonderenRef(gonderenKisi);
				getGonderim().setBirimRef(gonderenBirim);
				getGonderim().setDurum(GonderimDurum._BEKLEMEDE);
				if (getGonderim().getZamanlamali() == EvetHayir._HAYIR) {
					Date d = new Date();
					getGonderim().setGonderimzamani(d);
				}
				getDBOperator().insert(getGonderim());
				// createGenericMessage(KeyUtil.getMessageValue("kayit.eklendi"), FacesMessage.SEVERITY_INFO);
			} catch (DBException e) {
				getGonderim().setRID(null);
				createGenericMessage(KeyUtil.getMessageValue("islem.hata"), FacesMessage.SEVERITY_ERROR);
				return;
			}
		} else if (sEmailMi.equals("3")) {
			this.gonderim.setIletisimlisteRef(getGonderimFilter().getIletisimlisteRef());
			if (this.gonderim.getZamanlamali() == EvetHayir._HAYIR) {
				Date d = new Date();
				this.gonderim.setGonderimzamani(d);
			}
			super.justSave();
		}
		// saveRecursive(this.gonderim.getIletisimlisteRef().getRID(),
		// this.gonderim.getGonderimturu(), false);

		if (getGonderim().getGonderimturu() == GonderimTuru._EPOSTA) {
			if (afterSave.equalsIgnoreCase("redirect")) {
				redirectEPosta();
			} else if (afterSave.equalsIgnoreCase("draft")) {
				taslaklaraKaydet();
			}
		} else {
			if (afterSave.equalsIgnoreCase("redirect")) {
				redirectSMS();
			} else if (afterSave.equalsIgnoreCase("draft")) {
				taslaklaraKaydet();
			}
		}
	}

	// //// Kisinin iletisim tercihine bakilmadan gonderim yapmak icin
	public void saveTercihsiz() {
		this.gonderim.setIletisimlisteRef(getGonderimFilter().getIletisimlisteRef());
		if (this.gonderim.getZamanlamali() == EvetHayir._HAYIR) {
			Date d = new Date();
			this.gonderim.setGonderimzamani(d);
		}
		// /////// E-Mail ya da sms atildiktan sonra set edilmeli/////
		this.gonderim.setDurum(GonderimDurum._ILETILDI);
		// ////////////////////////////////////////////////
		save();
		// //E-Mail Logger (Boolean True)
		saveRecursive(this.gonderim.getIletisimlisteRef().getRID(), this.gonderim.getGonderimturu(), true);
	}

	public void saveRecursive(Long altRef, GonderimTuru gonderimTuru, boolean iletisimTercih) {
		@SuppressWarnings("unchecked")
		List<Iletisimlistedetay> iletisimlistedetay = getDBOperator().load(Iletisimlistedetay.class.getSimpleName(), "o.iletisimlisteRef.rID=" + altRef, "o.rID");
		for (Iletisimlistedetay b : iletisimlistedetay) {
			if (b.getReferansTipi() == ReferansTipi._KISI) {
				persistKisiLog(b, gonderimTuru, iletisimTercih);
			} else if (b.getReferansTipi() == ReferansTipi._UYE) {
				persistUyeLog(b, gonderimTuru, iletisimTercih);
			} else if (b.getReferansTipi() == ReferansTipi._BIRIM) {
				persistBirimUyeLog(b, gonderimTuru, iletisimTercih);
			} else if (b.getReferansTipi() == ReferansTipi._KURUM) {
				persistKurumUyeLog(b, gonderimTuru, iletisimTercih);
			} else if (b.getReferansTipi() == ReferansTipi._KURUL) {

			} else if (b.getReferansTipi() == ReferansTipi._KOMISYON) {

			} else if (b.getReferansTipi() == ReferansTipi._EGITMEN) {
				persistEgitmenLog(b, gonderimTuru, iletisimTercih);
			} else if (b.getReferansTipi() == ReferansTipi._EGITIMKATILIMCI) {
				persistEgitimKatilimciLog(b, gonderimTuru, iletisimTercih);
			} else if (b.getReferansTipi() == ReferansTipi._ETKINLIKKATILIMCI) {
				persistEtkinlikKatilimciLog(b, gonderimTuru, iletisimTercih);
			} else if (b.getReferansTipi() == ReferansTipi._ISYERITEMSILCISI) {
				persistIsyeritemsilcileriLog(b, gonderimTuru, iletisimTercih);
			}
		}
	}

	// iletisimlistedetay tablosunda bu gonderim ile ayni master
	// entitye sahip olan Kisilerin loglarini persist etmek icin
	// True--E.mail.... False--SMS
	public void persistKisiLog(Iletisimlistedetay b, GonderimTuru gonderimTuru, boolean iletisimTercih) {
		try {
			if (gonderimTuru == GonderimTuru._EPOSTA) {
				Epostagonderimlog e = new Epostagonderimlog();
				if (getDBOperator().recordCount(Kisi.class.getSimpleName(), "o.rID=" + b.getReferans()) > 0) {
					Kisi kisi = (Kisi) getDBOperator().load(Kisi.class.getSimpleName(), "o.rID=" + b.getReferans(), "o.rID").get(0);
					e.setKisiRef(kisi);
				}
				timeSetterForEMail(this.gonderim, e);
				setLogDurum(this.gonderim, e);
				e.setAciklama(this.gonderim.getAciklama());
				e.setGonderimRef(gonderim);
				// e.setGonderenRef(this.gonderim.getGonderenRef());
				e.setBirimRef(this.gonderim.getBirimRef());
				epostaanlikgonderimlogFilter.createQueryCriterias();
				insertEMailLog(e, iletisimTercih);
			} else {
				Smsgonderimlog sms = new Smsgonderimlog();
				if (getDBOperator().recordCount(Kisi.class.getSimpleName(), "o.rID=" + b.getReferans()) > 0) {
					Kisi kisi = (Kisi) getDBOperator().load(Kisi.class.getSimpleName(), "o.rID=" + b.getReferans(), "o.rID").get(0);
					sms.setKisiRef(kisi);
					// Emailer emailer = new Emailer();
					// emailer.sendEmail("muratsumbul@ymail.com",
					// "anildogan@windowslive.com",
					// "Testing 1-2-3", "blah blah blah");
				}
				setLogDurum(this.gonderim, sms);
				timeSetterForSMS(this.gonderim, sms);
				sms.setAciklama(this.gonderim.getAciklama());
				sms.setGonderimRef(gonderim);
				// sms.setGonderenRef(this.gonderim.getGonderenRef());
				sms.setBirimRef(this.gonderim.getBirimRef());
				epostaanlikgonderimlogFilter.createQueryCriterias();
				insertSMSLog(sms, iletisimTercih);
			}
		} catch (DBException e) {
			logYaz("DirekgonderimController @persistKisiLog" + e.getMessage());
			logYaz("Exception @" + getModelName() + "Controller :", e);
		}
	}

	// iletisimlistedetay tablosunda bu gonderim ile ayni master
	// entitye sahip olan UYELERin loglarini persist etmek icin
	// True--E.mail.... False--SMS
	public void persistUyeLog(Iletisimlistedetay b, GonderimTuru gonderimTuru, boolean iletisimTercih) {
		try {
			if (gonderimTuru == GonderimTuru._EPOSTA) {
				Epostagonderimlog e = new Epostagonderimlog();
				if (getDBOperator().recordCount(Uye.class.getSimpleName(), "o.rID=" + b.getReferans()) > 0) {
					Uye uye = (Uye) getDBOperator().load(Uye.class.getSimpleName(), "o.rID=" + b.getReferans(), "o.rID").get(0);
					e.setKisiRef(uye.getKisiRef());
				}
				setLogDurum(this.gonderim, e);
				timeSetterForEMail(this.gonderim, e);
				e.setAciklama(this.gonderim.getAciklama());
				e.setGonderimRef(this.gonderim);
				// e.setGonderenRef(this.gonderim.getGonderenRef());
				e.setBirimRef(this.gonderim.getBirimRef());
				epostaanlikgonderimlogFilter.createQueryCriterias();
				insertEMailLog(e, iletisimTercih);
			} else {
				Smsgonderimlog sms = new Smsgonderimlog();
				if (getDBOperator().recordCount(Uye.class.getSimpleName(), "o.rID=" + b.getReferans()) > 0) {
					Uye uye = (Uye) getDBOperator().load(Uye.class.getSimpleName(), "o.rID=" + b.getReferans(), "o.rID").get(0);
					sms.setKisiRef(uye.getKisiRef());
				}
				setLogDurum(this.gonderim, sms);
				timeSetterForSMS(this.gonderim, sms);
				sms.setAciklama(this.gonderim.getAciklama());
				sms.setGonderimRef(gonderim);
				// sms.setGonderenRef(this.gonderim.getGonderenRef());
				sms.setBirimRef(this.gonderim.getBirimRef());
				epostaanlikgonderimlogFilter.createQueryCriterias();
				insertSMSLog(sms, iletisimTercih);
			}
		} catch (DBException e) {
			logYaz("Exception @" + getModelName() + "Controller :", e);
		}
	}

	// iletisimlistedetay tablosunda bu gonderim ile ayni master
	// entitye sahip olan Birimin uyelerinin loglarini persist etmek
	// icin
	// True--E.mail.... False--SMS
	@SuppressWarnings("unchecked")
	public void persistBirimUyeLog(Iletisimlistedetay b, GonderimTuru gonderimTuru, boolean iletisimTercih) {
		try {
			if (gonderimTuru == GonderimTuru._EPOSTA) {
				if (getDBOperator().recordCount(Uye.class.getSimpleName(), "o.birimRef=" + b.getReferans()) > 0) {
					List<Uye> birimUye = getDBOperator().load(Uye.class.getSimpleName(), "o.birimRef=" + b.getReferans(), "o.rID");
					for (Uye brm : birimUye) {
						Epostagonderimlog e = new Epostagonderimlog();
						setLogDurum(this.gonderim, e);
						timeSetterForEMail(this.gonderim, e);
						e.setAciklama(this.gonderim.getAciklama());
						e.setGonderimRef(gonderim);
						e.setKisiRef(brm.getKisiRef());
						e.setGonderimRef(gonderim);
						// e.setGonderenRef(this.gonderim.getGonderenRef());
						e.setBirimRef(this.gonderim.getBirimRef());
						epostaanlikgonderimlogFilter.createQueryCriterias();
						insertEMailLog(e, iletisimTercih);
					}
				}
			} else {
				if (getDBOperator().recordCount(Uye.class.getSimpleName(), "o.birimRef=" + b.getReferans()) > 0) {
					List<Uye> birimUye = getDBOperator().load(Uye.class.getSimpleName(), "o.birimRef=" + b.getReferans(), "o.rID");
					for (Uye brm : birimUye) {
						Smsgonderimlog sms = new Smsgonderimlog();
						setLogDurum(this.gonderim, sms);
						timeSetterForSMS(this.gonderim, sms);
						sms.setAciklama(this.gonderim.getAciklama());
						sms.setGonderimRef(gonderim);
						sms.setKisiRef(brm.getKisiRef());
						sms.setGonderimRef(gonderim);
						// sms.setGonderenRef(this.gonderim.getGonderenRef());
						sms.setBirimRef(this.gonderim.getBirimRef());
						epostaanlikgonderimlogFilter.createQueryCriterias();
						insertSMSLog(sms, iletisimTercih);
					}
				}
			}
		} catch (DBException e) {
			logYaz("DirekGonderimController @persistBirimUyeLog =" + e.getMessage());
			logYaz("Exception @" + getModelName() + "Controller :", e);
		}
	}

	// iletisimlistedetay tablosunda bu gonderim ile ayni master
	// entitye sahip olan Kurumun uyelerinin loglarini persist etmek
	// icin
	// True--E.mail.... False--SMS
	@SuppressWarnings("unchecked")
	public void persistKurumUyeLog(Iletisimlistedetay b, GonderimTuru gonderimTuru, boolean iletisimTercih) {
		try {
			if (gonderimTuru == GonderimTuru._EPOSTA) {
				if (getDBOperator().recordCount(KurumKisi.class.getSimpleName(), "o.kurumRef=" + b.getReferans()) > 0) {
					KurumKisi kurumUye = (KurumKisi) getDBOperator().load(KurumKisi.class.getSimpleName(), "o.kurumRef=" + b.getReferans(), "o.rID").get(0);
					Epostagonderimlog e = new Epostagonderimlog();
					setLogDurum(this.gonderim, e);
					timeSetterForEMail(this.gonderim, e);
					e.setAciklama(this.gonderim.getAciklama());
					e.setGonderimRef(gonderim);
					e.setKisiRef(kurumUye.getKisiRef());
					e.setGonderimRef(gonderim);
					// e.setGonderenRef(this.gonderim.getGonderenRef());
					e.setBirimRef(this.gonderim.getBirimRef());
					epostaanlikgonderimlogFilter.createQueryCriterias();
					insertEMailLog(e, iletisimTercih);
				}
			} else {
				if (getDBOperator().recordCount(KurumKisi.class.getSimpleName(), "o.kurumRef=" + b.getReferans()) > 0) {
					List<KurumKisi> kurumUye = getDBOperator().load(KurumKisi.class.getSimpleName(), "o.kurumRef=" + b.getReferans(), "o.rID");
					for (KurumKisi krm : kurumUye) {
						Smsgonderimlog sms = new Smsgonderimlog();
						setLogDurum(this.gonderim, sms);
						timeSetterForSMS(this.gonderim, sms);
						sms.setAciklama(this.gonderim.getAciklama());
						sms.setGonderimRef(gonderim);
						sms.setKisiRef(krm.getKisiRef());
						sms.setGonderimRef(gonderim);
						// sms.setGonderenRef(this.gonderim.getGonderenRef());
						sms.setBirimRef(this.gonderim.getBirimRef());
						epostaanlikgonderimlogFilter.createQueryCriterias();
						insertSMSLog(sms, iletisimTercih);
					}
				}
			}
		} catch (DBException e) {
			logYaz("DirekgonderimController @persistKurumUyeLog= " + e.getMessage());
			logYaz("Exception @" + getModelName() + "Controller :", e);
		}
	}

	// iletisimlistedetay tablosunda bu gonderim ile ayni master
	// entitye sahip olan Egitimin Egitimkatilimcilarinin loglarini
	// persist etmek icin
	// True--E.mail.... False--SMS
	@SuppressWarnings("unchecked")
	public void persistEgitimKatilimciLog(Iletisimlistedetay b, GonderimTuru gonderimTuru, boolean iletisimTercih) {
		try {
			if (gonderimTuru == GonderimTuru._EPOSTA) {
				if (getDBOperator().recordCount(Egitimkatilimci.class.getSimpleName(), "o.egitimRef=" + b.getReferans()) > 0) {
					List<Egitimkatilimci> egitimKatilimci = getDBOperator().load(Egitimkatilimci.class.getSimpleName(), "o.egitimRef=" + b.getReferans(), "o.rID");
					for (Egitimkatilimci egtmktlmc : egitimKatilimci) {
						Epostagonderimlog e = new Epostagonderimlog();
						setLogDurum(this.gonderim, e);
						timeSetterForEMail(this.gonderim, e);
						e.setAciklama(this.gonderim.getAciklama());
						e.setGonderimRef(gonderim);
						e.setKisiRef(egtmktlmc.getKisiRef());
						e.setGonderimRef(gonderim);
						// e.setGonderenRef(this.gonderim.getGonderenRef());
						e.setBirimRef(this.gonderim.getBirimRef());
						epostaanlikgonderimlogFilter.createQueryCriterias();
						insertEMailLog(e, iletisimTercih);
					}
				}
			} else {
				if (getDBOperator().recordCount(Egitimkatilimci.class.getSimpleName(), "o.egitimRef=" + b.getReferans()) > 0) {
					List<Egitimkatilimci> egitimKatilimci = getDBOperator().load(Egitimkatilimci.class.getSimpleName(), "o.egitimRef=" + b.getReferans(), "o.rID");
					for (Egitimkatilimci egtmktlmc : egitimKatilimci) {
						Smsgonderimlog sms = new Smsgonderimlog();
						setLogDurum(this.gonderim, sms);
						timeSetterForSMS(this.gonderim, sms);
						sms.setAciklama(this.gonderim.getAciklama());
						sms.setGonderimRef(gonderim);
						sms.setKisiRef(egtmktlmc.getKisiRef());
						sms.setGonderimRef(gonderim);
						// sms.setGonderenRef(this.gonderim.getGonderenRef());
						sms.setBirimRef(this.gonderim.getBirimRef());
						epostaanlikgonderimlogFilter.createQueryCriterias();
						insertSMSLog(sms, iletisimTercih);
					}
				}
			}
		} catch (DBException e) {
			logYaz("DirekgonderimController @persistEgitimKatilimciLog=" + e.getMessage());
			logYaz("Exception @" + getModelName() + "Controller :", e);
		}
	}

	// iletisimlistedetay tablosunda bu gonderim ile ayni master
	// entitye sahip olan Etkinlik katilimcilarinin loglarini persist etmek icin
	// True--E.mail.... False--SMS
	@SuppressWarnings("unchecked")
	public void persistEtkinlikKatilimciLog(Iletisimlistedetay b, GonderimTuru gonderimTuru, boolean iletisimTercih) {
		try {
			if (gonderimTuru == GonderimTuru._EPOSTA) {
				if (getDBOperator().recordCount(Etkinlikkatilimci.class.getSimpleName(), "o.etkinlikRef=" + b.getReferans()) > 0) {
					List<Etkinlikkatilimci> etkinlikKatilimci = getDBOperator().load(Etkinlikkatilimci.class.getSimpleName(), "o.etkinlikRef=" + b.getReferans(), "o.rID");
					for (Etkinlikkatilimci etknlkktlmc : etkinlikKatilimci) {
						Epostagonderimlog e = new Epostagonderimlog();
						setLogDurum(this.gonderim, e);
						timeSetterForEMail(this.gonderim, e);
						e.setAciklama(this.gonderim.getAciklama());
						e.setGonderimRef(gonderim);
						e.setKisiRef(etknlkktlmc.getKisiRef());
						e.setGonderimRef(gonderim);
						// e.setGonderenRef(this.gonderim.getGonderenRef());
						e.setBirimRef(this.gonderim.getBirimRef());
						epostaanlikgonderimlogFilter.createQueryCriterias();
						insertEMailLog(e, iletisimTercih);
					}
				}
			} else {
				if (getDBOperator().recordCount(Etkinlikkatilimci.class.getSimpleName(), "o.etkinlikRef=" + b.getReferans()) > 0) {
					List<Etkinlikkatilimci> etkinlikKatilimci = getDBOperator().load(Etkinlikkatilimci.class.getSimpleName(), "o.etkinlikRef=" + b.getReferans(), "o.rID");
					for (Etkinlikkatilimci etknlkktlmc : etkinlikKatilimci) {
						Smsgonderimlog sms = new Smsgonderimlog();
						setLogDurum(this.gonderim, sms);
						timeSetterForSMS(this.gonderim, sms);
						sms.setAciklama(this.gonderim.getAciklama());
						sms.setGonderimRef(gonderim);
						sms.setKisiRef(etknlkktlmc.getKisiRef());
						sms.setGonderimRef(gonderim);
						// sms.setGonderenRef(this.gonderim.getGonderenRef());
						sms.setBirimRef(this.gonderim.getBirimRef());
						epostaanlikgonderimlogFilter.createQueryCriterias();
						insertSMSLog(sms, iletisimTercih);
					}
				}
			}
		} catch (DBException e) {
			logYaz("DirekgonderimController @persistEtkinlikkatilimciLog=" + e.getMessage());
			logYaz("Exception @" + getModelName() + "Controller :", e);
		}
	}

	// Secilen Egitmenin loglarini persist etmek icin
	// True--E.mail.... False--SMS
	public void persistEgitmenLog(Iletisimlistedetay b, GonderimTuru gonderimTuru, boolean iletisimTercih) {
		try {
			if (gonderimTuru == GonderimTuru._EPOSTA) {
				Epostagonderimlog e = new Epostagonderimlog();
				e.setAciklama(this.gonderim.getAciklama());
				setLogDurum(this.gonderim, e);
				if (getDBOperator().recordCount(Egitimogretmen.class.getSimpleName(), "o.rID=" + b.getReferans()) > 0) {
					Egitimogretmen egitmen = (Egitimogretmen) getDBOperator().load(Egitimogretmen.class.getSimpleName(), "o.rID=" + b.getReferans(), "o.rID").get(0);
					e.setKisiRef(egitmen.getEgitmenRef().getKisiRef());
				}
				timeSetterForEMail(this.gonderim, e);
				e.setGonderimRef(gonderim);
				// e.setGonderenRef(this.gonderim.getGonderenRef());
				e.setBirimRef(this.gonderim.getBirimRef());
				epostaanlikgonderimlogFilter.createQueryCriterias();
				insertEMailLog(e, iletisimTercih);
			} else {
				Smsgonderimlog sms = new Smsgonderimlog();
				if (getDBOperator().recordCount(Egitimogretmen.class.getSimpleName(), "o.rID=" + b.getReferans()) > 0) {
					Egitimogretmen egitmen = (Egitimogretmen) getDBOperator().load(Egitimogretmen.class.getSimpleName(), "o.rID=" + b.getReferans(), "o.rID").get(0);
					sms.setKisiRef(egitmen.getEgitmenRef().getKisiRef());
				}
				setLogDurum(this.gonderim, sms);
				timeSetterForSMS(this.gonderim, sms);
				sms.setAciklama(this.gonderim.getAciklama());
				sms.setGonderimRef(gonderim);
				// sms.setGonderenRef(this.gonderim.getGonderenRef());
				sms.setBirimRef(this.gonderim.getBirimRef());
				epostaanlikgonderimlogFilter.createQueryCriterias();
				insertSMSLog(sms, iletisimTercih);
			}
		} catch (DBException e) {
			logYaz("DirekgonderimController @persistEgitmenLog" + e.getMessage());
			logYaz("Exception @" + getModelName() + "Controller :", e);
		}
	}

	// Secilen Isyeri Temsilcisinin loglarini persist etmek icin
	// True--E.mail.... False--SMS
	public void persistIsyeritemsilcileriLog(Iletisimlistedetay b, GonderimTuru gonderimTuru, boolean iletisimTercih) {
		try {
			if (gonderimTuru == GonderimTuru._EPOSTA) {
				Epostagonderimlog e = new Epostagonderimlog();
				if (getDBOperator().recordCount(IsYeriTemsilcileri.class.getSimpleName(), "o.rID=" + b.getReferans()) > 0) {
					IsYeriTemsilcileri isyeritemsilcisi = (IsYeriTemsilcileri) getDBOperator().load(IsYeriTemsilcileri.class.getSimpleName(), "o.rID=" + b.getReferans(), "o.rID")
							.get(0);
					e.setKisiRef(isyeritemsilcisi.getUyeRef().getKisiRef());
				}
				setLogDurum(this.gonderim, e);
				timeSetterForEMail(this.gonderim, e);
				e.setAciklama(this.gonderim.getAciklama());
				e.setGonderimRef(gonderim);
				// e.setGonderenRef(this.gonderim.getGonderenRef());
				e.setBirimRef(this.gonderim.getBirimRef());
				epostaanlikgonderimlogFilter.createQueryCriterias();
				insertEMailLog(e, iletisimTercih);
			} else {
				Smsgonderimlog sms = new Smsgonderimlog();
				if (getDBOperator().recordCount(IsYeriTemsilcileri.class.getSimpleName(), "o.rID=" + b.getReferans()) > 0) {
					IsYeriTemsilcileri isyeritemsilcisi = (IsYeriTemsilcileri) getDBOperator().load(IsYeriTemsilcileri.class.getSimpleName(), "o.rID=" + b.getReferans(), "o.rID")
							.get(0);
					sms.setKisiRef(isyeritemsilcisi.getUyeRef().getKisiRef());
				}
				setLogDurum(this.gonderim, sms);
				timeSetterForSMS(this.gonderim, sms);
				sms.setGonderimRef(gonderim);
				sms.setBirimRef(this.gonderim.getBirimRef());
				// sms.setGonderenRef(this.gonderim.getGonderenRef());
				sms.setAciklama(this.gonderim.getAciklama());
				epostaanlikgonderimlogFilter.createQueryCriterias();
				insertSMSLog(sms, iletisimTercih);
			}
		} catch (DBException e1) {
			logYaz("DirekgonderimController @persistIsyeritemsilcileriLog =" + e1.getMessage());
			e1.printStackTrace();
		}
	}

	public void insertSMSLog(Smsgonderimlog sms, boolean iletisimTercih) {
		try {
			if (iletisimTercih == true) {
				getDBOperator().insert(sms);
			}

			else if (iletisimTercih == false) {
				if (sms.getKisiRef().getIletisimtercih() == EvetHayir._EVET) {
					getDBOperator().insert(sms);
				} else {
					// logYaz("Iletisim Tercihi Kesiti");
				}
			}
		} catch (DBException e1) {
			e1.printStackTrace();
		}
	}

	public void insertEMailLog(Epostagonderimlog e, boolean iletisimTercih) {
		try {
			if (iletisimTercih == true) {
				getDBOperator().insert(e);
			} else if (iletisimTercih == false) {
				if (e.getKisiRef() != null) {
					if (e.getKisiRef().getIletisimtercih() != null) {
						if (e.getKisiRef().getIletisimtercih().getCode() == EvetHayir._EVET.getCode()) {
							getDBOperator().insert(e);
						}
					}
				}
			}
		} catch (DBException e1) {
			e1.printStackTrace();
		}
	}

	public void timeSetterForEMail(Gonderim g, Epostagonderimlog e) {
		if (g.getZamanlamali() == EvetHayir._HAYIR) {
			Date d = new Date();
			e.setTarih(d);
		} else {
			e.setTarih(g.getGonderimzamani());
		}
	}

	public void timeSetterForSMS(Gonderim g, Smsgonderimlog sms) {
		if (g.getZamanlamali() == EvetHayir._HAYIR) {
			Date d = new Date();
			sms.setTarih(d);
		} else {
			sms.setTarih(g.getGonderimzamani());
		}
	}

	public void setLogDurum(Gonderim g, BaseEntity b) {
		if (b instanceof Epostagonderimlog) {
			if (g.getDurum() == GonderimDurum._ILETILDI) {
				((Epostagonderimlog) b).setDurum(GonderimDurum._ILETILDI);
			} else {
				((Epostagonderimlog) b).setDurum(GonderimDurum._ILETILMEDI);
			}
		}
		if (b instanceof Smsgonderimlog) {
			if (g.getDurum() == GonderimDurum._ILETILDI) {
				((Smsgonderimlog) b).setDurum(GonderimDurum._ILETILDI);
			} else {
				((Smsgonderimlog) b).setDurum(GonderimDurum._ILETILMEDI);
			}
		}
	}

	public GonderimTuru getEmailTip() {
		return GonderimTuru._EPOSTA;
	}

	public void handleClose(CloseEvent event) {
		try {
			if (sEmailMi.equals("1")) {
				FacesContext.getCurrentInstance().getExternalContext().redirect("epostagonder.xhtml?type=1");
			} else {
				FacesContext.getCurrentInstance().getExternalContext().redirect("smsgonder.xhtml?type=2");
			}

		} catch (IOException e1) {

			e1.printStackTrace();
		}
	}

	public void cancelPost() {
		if (getGonderim().getGonderimturu() == GonderimTuru._EPOSTA) {
			@SuppressWarnings("unchecked")
			List<Epostagonderimlog> epostaGonderimLog = getDBOperator().load(Epostagonderimlog.class.getSimpleName(), "o.gonderimRef=" + getGonderim().getRID(), "o.rID");
			for (Epostagonderimlog epostaLog : epostaGonderimLog) {
				super.justDeleteGivenObject(epostaLog, loggable);
			}
			super.justDeleteGivenObject(this.gonderim, loggable);
			Thread.currentThread();
			try {
				FacesContext.getCurrentInstance().getExternalContext().redirect("epostagonder.xhtml?type=1");
				Thread.sleep(100);
			} catch (InterruptedException e) {
				logYaz("Exception @" + getModelName() + "Controller :", e);
			} catch (IOException e) {
				logYaz("Exception @" + getModelName() + "Controller :", e);
			}
		} else if (getGonderim().getGonderimturu() == GonderimTuru._SMS) {
			@SuppressWarnings("unchecked")
			List<Smsgonderimlog> smsGonderimLog = getDBOperator().load(Smsgonderimlog.class.getSimpleName(), "o.gonderimRef=" + getGonderim().getRID(), "o.rID");
			for (Smsgonderimlog smsLog : smsGonderimLog) {
				super.justDeleteGivenObject(smsLog, loggable);
			}
			super.justDeleteGivenObject(this.gonderim, loggable);
			Thread.currentThread();
			try {
				Thread.sleep(100);
				FacesContext.getCurrentInstance().getExternalContext().redirect("smsgonder.xhtml?type=2");
			} catch (InterruptedException e) {
				logYaz("Exception @" + getModelName() + "Controller :", e);
			} catch (IOException e) {
				logYaz("Exception @" + getModelName() + "Controller :", e);
			}
		}
	}

	public void redirectEPosta() {
		try {
			if (getGonderim() != null) {
				if (getGonderim().getIletisimlisteRef() != null) {
					// Eposta alicilari bulunuyor ve toplu olarak eposta gonderiliyor.
					List<Kisi> kisiListesi = getCustomDBOperator().epostaGonderimGonderimListesiGetir(getGonderim().getIletisimlisteRef());
					if (!isEmpty(kisiListesi)) {
						if (getGonderim().getIcerikRef() != null) {
							email.sendEmailToKisiList(kisiListesi, getGonderim().getKonu(), getGonderim().getIcerikRef().getIcerik(), getGonderim(), getSessionUser().getKullanici().getKisiRef());
						} else {
							email.sendEmailToKisiList(kisiListesi, getGonderim().getKonu(), getGonderim().getAciklama(), getGonderim(), getSessionUser().getKullanici().getKisiRef());
						}
						createGenericMessage("Eposta alıcılara gönderildi.", FacesMessage.SEVERITY_INFO);
					}
				}
			}
			insert();
		} catch (Exception e) {
			logYaz("Exception @" + getModelName() + "Controller :", e);
		}
	}

	public void redirectSMS() {
		try {
			createGenericMessage("Sms gönderimi yapilacak...", FacesMessage.SEVERITY_INFO);
		} catch (Exception e) {
			logYaz("DirekgonderimController @redirectSMS=" + e.getMessage());
			logYaz("Exception @" + getModelName() + "Controller :", e);
		}
	}

	@SuppressWarnings("unchecked")
	public void taslaklaraKaydet() {
		Icerik icerik = new Icerik();
		try {
			icerik.setGonderimturu(getGonderim().getGonderimturu());
			icerik.setIcerik(getGonderim().getAciklama());
			if(getGonderim().getIcerikRef() != null) {
				icerik.setIcerikturu(IcerikTuru._HAZIRICERIK);
			} else {
				icerik.setIcerikturu(IcerikTuru._MANUEL);
			}
			if (getGonderim().getGonderimturu().getCode() == GonderimTuru._EPOSTA.getCode()) {
				getDBOperator().insert(icerik);
				List<BaseEntity> epostaGonderimLog = getDBOperator().load(Epostagonderimlog.class.getSimpleName(), "o.gonderimRef=" + getGonderim().getRID(), "o.rID");
				getDBOperator().recordInOneTransaction(null, null, epostaGonderimLog);
				super.justDeleteGivenObject(this.gonderim, loggable);
				insert();
			} else if (getGonderim().getGonderimturu().getCode() == GonderimTuru._SMS.getCode()) {
				getDBOperator().insert(icerik);
				List<BaseEntity> smsGonderimLog = getDBOperator().load(Smsgonderimlog.class.getSimpleName(), "o.gonderimRef=" + getGonderim().getRID(), "o.rID");
				getDBOperator().recordInOneTransaction(null, null, smsGonderimLog);
				super.justDeleteGivenObject(this.gonderim, loggable);
				insert();
			}
		} catch (DBException e) {
			logYaz("DirekGonderimController @taslaklaraKaydet=" + e.getMessage());
			logYaz("Exception @" + getModelName() + "Controller :", e);
		}
	}

	public long getSmsParameter() {
		Long staticParamForRid = (long) 1;
		Long returnedParameter = (long) 0;
		@SuppressWarnings("unchecked")
		List<SistemParametre> sistemParameters = getDBOperator().load(SistemParametre.class.getSimpleName(), "o.rID=" + staticParamForRid, "o.rID");
		for (SistemParametre sistemParameter : sistemParameters) {
			returnedParameter = Long.parseLong(sistemParameter.getSmsuzunluk());
		}
		// logYaz(returnedParameter*160);
		return returnedParameter * 160;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void insert() {
		List<Personel> personelListesi = getDBOperator().load(Personel.class.getSimpleName(),
				"o.kisiRef.rID = " + getSessionUser().getKullanici().getKisiRef().getRID() + " AND o.durum = " + PersonelDurum._CALISIYOR.getCode(), "o.rID");
		if (!isEmpty(personelListesi)) {
			super.insert();
			getGonderim().setBirimRef(personelListesi.get(0).getBirimRef());
			getGonderim().setZamanlamali(EvetHayir._HAYIR);
		} else {
			return;
		}

	}
}
// class
