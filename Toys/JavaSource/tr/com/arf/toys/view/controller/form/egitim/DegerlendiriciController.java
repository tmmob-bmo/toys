package tr.com.arf.toys.view.controller.form.egitim; 
 
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.component.html.HtmlSelectBooleanCheckbox;
import javax.faces.event.AjaxBehaviorEvent;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.toys.db.filter.egitim.DegerlendiriciFilter;
import tr.com.arf.toys.db.model.egitim.Degerlendirici;
import tr.com.arf.toys.view.controller.form._base.ToysBaseFilteredController;
  
  
@ManagedBean 
@ViewScoped 
public class DegerlendiriciController extends ToysBaseFilteredController {  
  
  private static final long serialVersionUID = 6531152165884980748L; 
  
	private Degerlendirici degerlendirici;  
	private DegerlendiriciFilter degerlendiriciFilter = new DegerlendiriciFilter();  
  
	public DegerlendiriciController() { 
		super(Degerlendirici.class);  
		setDefaultValues(false);  
		setLoggable(false);  
		setTable(null); 
		queryAction();
	}
	
	public Degerlendirici getDegerlendirici() {
		degerlendirici=(Degerlendirici) getEntity();
		return degerlendirici;
	}

	public void setDegerlendirici(Degerlendirici degerlendirici) {
		this.degerlendirici = degerlendirici;
	}

	public DegerlendiriciFilter getDegerlendiriciFilter() {
		return degerlendiriciFilter;
	}

	public void setDegerlendiriciFilter(DegerlendiriciFilter degerlendiriciFilter) {
		this.degerlendiriciFilter = degerlendiriciFilter;
	}

	@Override
	public BaseFilter getFilter() {
		return getDegerlendiriciFilter();
	}
	
	private Boolean kisimi=false;
	private Boolean kisiuye;
	
	public Boolean getKisimi() {
		return kisimi;
	}

	public void setKisimi(Boolean kisimi) {
		this.kisimi = kisimi;
	}
	
	public Boolean getKisiuye() {
		return kisiuye;
	}

	public void setKisiuye(Boolean kisiuye) {
		this.kisiuye = kisiuye;
	}

	public void handleKisiChange(AjaxBehaviorEvent event) {
		try {
			HtmlSelectBooleanCheckbox menu = (HtmlSelectBooleanCheckbox) event.getComponent();
			if ((Boolean)menu.getValue()) {
				kisimi=true;
			}else{
				kisimi=false;
			}
		} catch (Exception e) {
			logYaz("Exception @" + getModelName() + "Controller :", e);
		}
		
	}

	
} // class 
