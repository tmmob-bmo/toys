package tr.com.arf.toys.view.controller._common;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.Serializable;
import java.math.BigDecimal;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.nio.channels.WritableByteChannel;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.log4j.Logger;
import org.primefaces.event.TabChangeEvent;

import tr.com.arf.framework.db._management.DBOperator;
import tr.com.arf.framework.db.enumerated._common.EvetHayir;
import tr.com.arf.framework.db.enumerated._common.IslemTuru;
import tr.com.arf.framework.db.model._base.BaseEntity;
import tr.com.arf.framework.db.model.query.QueryObject;
import tr.com.arf.framework.db.model.sistem.Islemlog;
import tr.com.arf.framework.utility.exception.data.DBException;
import tr.com.arf.framework.utility.tool.BigDecimalUtil;
import tr.com.arf.framework.utility.tool.DateUtil;
import tr.com.arf.framework.utility.tool.KeyUtil;
import tr.com.arf.framework.utility.tool.StringUtil;
import tr.com.arf.framework.view.controller._common.SessionUserBase;
import tr.com.arf.toys.db.enumerated.egitim.EgitimDurum;
import tr.com.arf.toys.db.enumerated.evrak.EvrakDurum;
import tr.com.arf.toys.db.enumerated.evrak.EvrakHareketTuru;
import tr.com.arf.toys.db.enumerated.evrak.HazirEvrakTuru;
import tr.com.arf.toys.db.enumerated.finans.OdemeTuru;
import tr.com.arf.toys.db.enumerated.iletisim.AdresTipi;
import tr.com.arf.toys.db.enumerated.iletisim.AdresTuru;
import tr.com.arf.toys.db.enumerated.iletisim.ReferansTipi;
import tr.com.arf.toys.db.enumerated.kisi.KomisyonUyelikDurumu;
import tr.com.arf.toys.db.enumerated.kisi.KurulTuru;
import tr.com.arf.toys.db.enumerated.kisi.KurulUyelikDurumu;
import tr.com.arf.toys.db.enumerated.uye.OdemeSekli;
import tr.com.arf.toys.db.enumerated.uye.UyeAidatDurum;
import tr.com.arf.toys.db.enumerated.uye.UyeDurum;
import tr.com.arf.toys.db.enumerated.uye.UyeKayitDurum;
import tr.com.arf.toys.db.enumerated.uye.UyeMuafiyetTip;
import tr.com.arf.toys.db.enumerated.uye.UyeTip;
import tr.com.arf.toys.db.enumerated.uye.UyekurumDurum;
import tr.com.arf.toys.db.filter.system.AidatFilter;
import tr.com.arf.toys.db.model.evrak.Evrak;
import tr.com.arf.toys.db.model.evrak.Evrakdolasimbildirim;
import tr.com.arf.toys.db.model.finans.Borc;
import tr.com.arf.toys.db.model.finans.Odeme;
import tr.com.arf.toys.db.model.system.Aidat;
import tr.com.arf.toys.db.model.system.IslemKullanici;
import tr.com.arf.toys.db.model.system.SistemParametre;
import tr.com.arf.toys.db.model.uye.Uye;
import tr.com.arf.toys.db.model.uye.UyeSiparis;
import tr.com.arf.toys.db.model.uye.Uyeaidat;
import tr.com.arf.toys.db.model.uye.Uyeform;
import tr.com.arf.toys.service.application.ApplicationDescriptor;
import tr.com.arf.toys.service.application.ManagedBeanLocator;
import tr.com.arf.toys.utility.application.ApplicationConstant;
import tr.com.arf.toys.utility.logUtils.LogService;

@ManagedBean(name = "sessionController")
@SessionScoped
public class SessionController implements Serializable {
    protected static Logger logger = Logger.getLogger(SessionController.class);

    private static final long serialVersionUID = -6940087545937686362L;

	private final SessionUser sessionUser = ManagedBeanLocator.locateSessionUser();
	private HashMap<String, IslemKullanici> islemMap = sessionUser.getIslemMap();

	@SuppressWarnings("unused")
	private String detailEditType = null;

	private DBOperator dBOperator;

	public SessionController() {
		super();
	}

	@PostConstruct
	public void init() {
		logger.debug("Session Controller init...");
		dBOperator = new DBOperator();
	}

	/* Data Operator */
	public final DBOperator getDBOperator() {
		if (dBOperator == null) {
			dBOperator = new DBOperator();
		}
		return dBOperator;
	}

	public void setDetailEditType(String detailEditType) {
		this.detailEditType = detailEditType;
	}

	public String uyelogout() {
		FacesContext context = FacesContext.getCurrentInstance();
		HttpSession session = (HttpSession) context.getExternalContext().getSession(false);
		if (session != null) {
			session.invalidate();
		}
		return "uyelogin.do";
	}

	public boolean listRenderedFor(String modelNameForListControl) {
		if (sessionUser.isSuperUser()) {
			return true;
		}
		try {
			return islemMap.get(modelNameForListControl).getListPermission() == EvetHayir._EVET;
		} catch (NullPointerException e) {
			return false;
		}
	}

	public boolean updateRecordRenderedFor(String modelNameForListControl) {
		if (sessionUser.isSuperUser()) {
			return true;
		}
		try {
			return islemMap.get(modelNameForListControl).getUpdatePermission() == EvetHayir._EVET;
		} catch (NullPointerException e) {
			return false;
		}
	}

	public boolean deleteRenderedFor(String modelNameForListControl) {
		if (sessionUser.isSuperUser()) {
			return true;
		}
		try {
			return islemMap.get(modelNameForListControl).getDeletePermission() == EvetHayir._EVET;
		} catch (NullPointerException e) {
			return false;
		}
	}

	public HashMap<String, IslemKullanici> getIslemMap() {
		return islemMap;
	}

	public void setIslemMap(HashMap<String, IslemKullanici> islemMap) {
		this.islemMap = islemMap;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/* Generic File download for all entity types */
	public void execute(Object secilenKayit) {
		if (secilenKayit == null || ((BaseEntity) secilenKayit).getPath() == null) {
			createGenericMessage(KeyUtil.getMessageValue("HATA! DOSYA YOK!"), FacesMessage.SEVERITY_ERROR);
			return;
		}
		download(secilenKayit);
	}

	public String download(Object secilenKayit) {
		if (secilenKayit == null) {
			return "";
		}

		final FacesContext facesContext = FacesContext.getCurrentInstance();
		HttpServletResponse response = (HttpServletResponse) facesContext.getExternalContext().getResponse();
		BaseEntity entity = (BaseEntity) secilenKayit;
		String fileName = "";
		try {
			if (getSessionUser().getKullanici().getKisiRef() != null) {
				if (entity.getClass().getSimpleName().equalsIgnoreCase("uyeform")) {
					fileName += getSessionUser().getKullanici().getKisiRef().getUIStringForDownload() + "_" + ((Uyeform) entity).getFormturu().getLabel();
				} else {
					fileName += getSessionUser().getKullanici().getKisiRef().getUIStringForDownload() + "_" + entity.getClass().getSimpleName();
				}
			}
		} catch (Exception e) {
			fileName = RandomStringUtils.randomAlphabetic(12).toUpperCase() + "" + DateUtil.dateToDMYHMS(new Date()).replace("/", "").replace(" ", "").replace(":", "");
		}
		if (entity.getPath() == null || new File(entity.getPath()) == null) {
			createGenericMessage(KeyUtil.getMessageValue("HATA! DOSYA YOK!"), FacesMessage.SEVERITY_ERROR);
			return null;
		}
		writeOutContent(response, new File(entity.getPath()), fileName, entity.getPath());
		facesContext.responseComplete();
		return null;
	}

	void writeOutContent(final HttpServletResponse res, final File content, String theFilename, String path) {
		if (content == null) {
			return;
		}
		FileInputStream fis = null;
		ReadableByteChannel src = null;
		WritableByteChannel wbc = null;
		try {
			String fileType = path.toString().substring(path.toString().length() - 4);
			if (fileType.contains(".")) {
				theFilename = StringUtil.clearTurkishChars(theFilename);
				fileType = fileType.substring(1);
			} else {
				theFilename = StringUtil.clearTurkishChars(theFilename);
			}
			res.setHeader("Pragma", "no-cache");
			res.setDateHeader("Expires", 0);
			if (fileType.equalsIgnoreCase("xls")) {
				res.setContentType("application/vnd.ms-excel");
			} else if (fileType.equalsIgnoreCase("xlsx")) {
				res.setContentType("application/vnd.ms-excel");
			} else if (fileType.equalsIgnoreCase("doc")) {
				res.setContentType("application/msword");
			} else if (fileType.equalsIgnoreCase("docx")) {
				res.setContentType("application/vnd.openxmlformats-officedocument.wordprocessingml.document");
			} else if (fileType.equalsIgnoreCase("pdf")) {
				res.setContentType("application/pdf");
			} else if (fileType.equalsIgnoreCase("ppt")) {
				res.setContentType("application/vnd.ms-powerpoint");
			} else if (fileType.equalsIgnoreCase("png") || fileType.equalsIgnoreCase("jpg") || fileType.equalsIgnoreCase("jpeg") || fileType.equalsIgnoreCase("gif") || fileType.equalsIgnoreCase("tiff")) {
				res.setContentType("image/" + fileType);
			}
			// theFilename = theFilename.substring(0,
			// theFilename.lastIndexOf("."));
			res.setHeader("Content-disposition", "attachment; filename=" + theFilename + "." + fileType);
			fis = new FileInputStream(content);
			src = Channels.newChannel(fis);
			wbc = Channels.newChannel(res.getOutputStream());
			fastChannelCopy(src, wbc);
		} catch (final IOException e) {
			sessionUser.setMessage("HATA :" + e.getMessage());
		} finally {
			if (fis != null) {
				try {
					fis.close();
				} catch (IOException e) {
					logger.error("ERROR @writeOutContent, fis.close() : " + e.getMessage(), e);
				}
			}
			if (src != null) {
				try {
					src.close();
				} catch (IOException e) {
					logger.error("ERROR @writeOutContent, src.close() : " + e.getMessage(), e);
				}
			}
			if (wbc != null) {
				try {
					wbc.close();
				} catch (IOException e) {
					logger.error("ERROR @writeOutContent, wbc.close() : " + e.getMessage(), e);
				}
			}

		}
	}

	void fastChannelCopy(final ReadableByteChannel src, final WritableByteChannel dest) throws IOException {
		final ByteBuffer buffer = ByteBuffer.allocateDirect(16 * 1024);
		try {
			while (src.read(buffer) != -1) {
				buffer.flip();
				dest.write(buffer);
				buffer.compact();
			}
			buffer.flip();
			while (buffer.hasRemaining()) {
				dest.write(buffer);
			}
		} catch (Exception e) {
			logger.error("ERROR @fastChannelCopy : " + e.getMessage(), e);
		} finally {
			if (buffer != null) {
				buffer.clear();
			}
		}
	}

	private int activeIndexNo = 0;

	public int getActiveIndexNo() {
		return activeIndexNo;
	}

	public void setActiveIndexNo(int activeIndexNo) {
		this.activeIndexNo = activeIndexNo;
	}

	public void onTabChange(TabChangeEvent event) {
		String activeTabId = event.getTab().getId();
		// System.out.println("active tab id :" + activeTabId);
		if (activeTabId.equalsIgnoreCase("finansIslemleri")) {
			this.activeIndexNo = 0;
		} else if (activeTabId.equalsIgnoreCase("tanimlamaIslemleri")) {
			this.activeIndexNo = 1;
		} else if (activeTabId.equalsIgnoreCase("referansVeriIslemleri")) {
			this.activeIndexNo = 2;
		} else if (activeTabId.equalsIgnoreCase("raporIslemleri")) {
			this.activeIndexNo = 3;
		} else if (activeTabId.equalsIgnoreCase("raporIslemleri")) {
			this.activeIndexNo = 4;
		} else if (activeTabId.equalsIgnoreCase("kisiKurumYonetimiIslemleri")) {
			this.activeIndexNo = 5;
		} else if (activeTabId.equalsIgnoreCase("odaYonetimiIslemleri")) {
			this.activeIndexNo = 6;
		} else if (activeTabId.equalsIgnoreCase("iletisimIslemleri")) {
			this.activeIndexNo = 7;
		} else if (activeTabId.equalsIgnoreCase("etkinlikIslemleri")) {
			this.activeIndexNo = 8;
		} else if (activeTabId.equalsIgnoreCase("egitimIslemleri")) {
			this.activeIndexNo = 9;
		} else if (activeTabId.equalsIgnoreCase("yayinVeKutuphaneIslemleri")) {
			this.activeIndexNo = 10;
		}

	}

	public void createGenericMessage(String messageValue, javax.faces.application.FacesMessage.Severity messageSeverity) {
		FacesMessage message = new FacesMessage();
		FacesContext context = FacesContext.getCurrentInstance();
		message.setSeverity(messageSeverity);
		message.setDetail(messageValue);
		message.setSummary(messageValue);
		context.addMessage(null, message);
	}

	public SessionUser getSessionUser() {
		return sessionUser;
	}

	public String uyeSicilNoOlustur(Uye eklenecekUye, Boolean gecici) {
		String prefix = "";
		// String whereCon = "o.uyetip = " + eklenecekUye.getUyetip().getCode();
		if (eklenecekUye.getUyetip() == UyeTip._CALISAN) {
			prefix = "";
		} else if (eklenecekUye.getUyetip() == UyeTip._OGRENCI) {
			prefix = "G";
		} else if (eklenecekUye.getUyetip() == UyeTip._YABANCILAR || eklenecekUye.getUyetip() == UyeTip._TURKUYRUKLU) {
			prefix = "Y";
			// whereCon = "(o.uyetip = " + UyeTip._YABANCILAR.getCode() +
			// " OR o.uyetip = " + UyeTip._TURKUYRUKLU.getCode();
		}
		int rec = 0;
		try {
			if (eklenecekUye.getUyetip() == UyeTip._CALISAN) {
				if (getDBOperator().recordCountByNativeQuery("SELECT COALESCE( MAX (SICILNO),'0') FROM oltp." + ApplicationDescriptor._UYE_MODEL + "" + " WHERE  uyetip= " + eklenecekUye.getUyetip().getCode()) != null) {
					Object maxNum = getDBOperator().loadByQuery("SELECT COALESCE( MAX (o.sicilno),'0') FROM " + Uye.class.getSimpleName() + " as o" + " WHERE  o.uyetip= " + eklenecekUye.getUyetip().getCode()).get(0);
					String maxNumber = maxNum.toString();
					maxNumber = StringUtil.cleanAlphaNumericCharacters(maxNumber);
					rec = Integer.parseInt(maxNumber);
				}
			} else {
				if (getDBOperator().recordCountByNativeQuery(
						"SELECT COALESCE( MAX (SICILNO),'0') FROM oltp." + ApplicationDescriptor._UYE_MODEL + "" + " WHERE  uyetip= " + eklenecekUye.getUyetip().getCode() + " AND (UPPER(sicilno) LIKE UPPER('%" + "Y" + "%'))") != null) {
					Object maxNum = getDBOperator().loadByQuery(
							"SELECT COALESCE( MAX (o.sicilno),'0') FROM " + Uye.class.getSimpleName() + " as o" + " WHERE  o.uyetip= " + eklenecekUye.getUyetip().getCode() + " AND (UPPER(o.sicilno) LIKE UPPER('%" + "Y" + "%'))").get(0);
					String maxNumber = maxNum.toString();
					maxNumber = StringUtil.cleanAlphaNumericCharacters(maxNumber);
					rec = Integer.parseInt(maxNumber);
				}
			}
			rec++;
			while (true) {
				int x = 0;
				if (eklenecekUye.getUyetip() != UyeTip._CALISAN) {
					x = getDBOperator().recordCount(Uye.class.getSimpleName(), "o.sicilno = '" + prefix + org.apache.commons.lang3.StringUtils.leftPad(rec + "", 5, "0") + "'");
				} else {
					x = getDBOperator().recordCount(Uye.class.getSimpleName(), "o.sicilno = '" + org.apache.commons.lang3.StringUtils.leftPad(rec + "", 6, "0") + "'");
				}
				if (x == 0) {
					break;
				} else {
					rec++;
				}
			}
			if (eklenecekUye.getUyetip() != UyeTip._CALISAN) {
				return prefix + org.apache.commons.lang3.StringUtils.leftPad(rec + "", 5, "0");
			} else {
				return prefix + org.apache.commons.lang3.StringUtils.leftPad(rec + "", 6, "0");
			}
		} catch (Exception e) {
			logger.error("ERROR @uyeSicilNoOlustur : " + e.getMessage(), e);
			return null;
		}
	}

	@SuppressWarnings("unchecked")
	public void uyeBorclandir(Uye uye) throws DBException, ParseException {
		/* Eger uyelik tarihi bos ise uye bugunden itibaren borclandirilir. Bos degilse, uyelik tarihinden itibaren borclandirilir. */
		Date uyeliktarih = DateUtil.createDateObject("01/08/2012");
		if (uye.getUyeliktarih() != null) {
			uyeliktarih = uye.getUyeliktarih();
		}
		Uyeaidat yeniAidat = new Uyeaidat();
		EntityManager entityManager = getDBOperator().getEntityManagerFactory().createEntityManager();
		AidatFilter aidatFilter = new AidatFilter();
		aidatFilter.setUyetip(uye.getUyetip());
		aidatFilter.setBaslangictarih(uyeliktarih);
		aidatFilter.setBitistarih(uyeliktarih);
		aidatFilter.setYil(DateUtil.getYearOfADate(uyeliktarih));
		// aidatFilter.setYilMax(DateUtil.getYearOfADate(new Date())+2);
		ArrayList<ArrayList<QueryObject>> allCriterias = (ArrayList<ArrayList<QueryObject>>) aidatFilter.createQueryCriterias();
		List<Aidat> aidatListesi = getDBOperator().loadFiltered(Aidat.class.getSimpleName(), allCriterias.get(0), new ArrayList<QueryObject>(), null, "rID");
		try {
			for (Aidat aidat : aidatListesi) {
				entityManager.getTransaction().begin();
				for (int x = DateUtil.getMonthOfADate(uyeliktarih); x <= DateUtil.getMonthOfADate(aidat.getBitistarih()); x++) {
					yeniAidat = new Uyeaidat();
					yeniAidat.setUyeRef(uye);
					yeniAidat.setMiktar(aidat.getMiktar());
					yeniAidat.setAy(x);
					yeniAidat.setOdenen(new BigDecimal(0));
					yeniAidat.setUyeaidatdurum(UyeAidatDurum._BORCLU);
					yeniAidat.setYil(aidat.getYil());
					entityManager.persist(yeniAidat);
				}
				entityManager.getTransaction().commit();
			}// for
			aidatFilter = new AidatFilter();
			aidatFilter.setYil(DateUtil.getYearOfADate(uyeliktarih) + 1);
			aidatFilter.setYilMax(DateUtil.getYearOfADate(uyeliktarih) + 2);
			aidatFilter.setUyetip(uye.getUyetip());
			allCriterias = (ArrayList<ArrayList<QueryObject>>) aidatFilter.createQueryCriterias();
			aidatListesi = getDBOperator().loadFiltered(Aidat.class.getSimpleName(), allCriterias.get(0), new ArrayList<QueryObject>(), null, "rID");
			for (Aidat aidat : aidatListesi) {
				entityManager.getTransaction().begin();
				for (int x = 1; x <= DateUtil.getMonthOfADate(aidat.getBitistarih()); x++) {
					yeniAidat = new Uyeaidat();
					yeniAidat.setUyeRef(uye);
					yeniAidat.setMiktar(aidat.getMiktar());
					yeniAidat.setAy(x);
					yeniAidat.setOdenen(new BigDecimal(0));
					yeniAidat.setUyeaidatdurum(UyeAidatDurum._BORCLU);
					yeniAidat.setYil(aidat.getYil());
					entityManager.persist(yeniAidat);
				}
				entityManager.getTransaction().commit();
			}// for
		} catch (Exception e) {
			if (entityManager.getTransaction().isActive()) {
				entityManager.getTransaction().rollback();
			}
			logger.error("Exception @SessionController : " + e.getMessage(), e);
		} finally {
			if (entityManager.isOpen()) {
				entityManager.close();
			}
		}
	}

	public UyeMuafiyetTip uyeMuafiyetDurumuBelirle(UyeDurum durum) {
		if (durum == UyeDurum._VEFAT) {
			return UyeMuafiyetTip._VEFAT;
		} else if (durum == UyeDurum._EMEKLI) {
			return UyeMuafiyetTip._EMEKLI;
		} else if (durum == UyeDurum._ASKER) {
			return UyeMuafiyetTip._ASKERLIK;
		} else if (durum == UyeDurum._DOGUMIZNI) {
			return UyeMuafiyetTip._DOGUMIZNI;
		} else if (durum == UyeDurum._YURTDISI) {
			return UyeMuafiyetTip._YURTDISI;
		} else if (durum == UyeDurum._ISSIZ) {
			return UyeMuafiyetTip._ISSIZ;
		} else if (durum == UyeDurum._KAPALI) {
			return UyeMuafiyetTip._UYELIKIPTAL;
		} else if (durum == UyeDurum._ISTIFA) {
			return UyeMuafiyetTip._ISTIFA;
		} else {
			return UyeMuafiyetTip._NULL;
		}
	}

	public String aciklamaOlustur(UyeDurum durum) {
		if (durum == UyeDurum._ASKER) {
			return KeyUtil.getMessageValue("muafiyet.asker");
		} else if (durum == UyeDurum._EMEKLI) {
			return KeyUtil.getMessageValue("muafiyet.emekli");
		} else if (durum == UyeDurum._VEFAT) {
			return KeyUtil.getMessageValue("muafiyet.vefat");
		} else if (durum == UyeDurum._YURTDISI) {
			return KeyUtil.getMessageValue("muafiyet.yurtDisi");
		} else if (durum == UyeDurum._DOGUMIZNI) {
			return KeyUtil.getMessageValue("muafiyet.dogumIzni");
		} else if (durum == UyeDurum._KAPALI) {
			return KeyUtil.getMessageValue("muafiyet.dogumIzni");
		} else if (durum == UyeDurum._ISTIFA) {
			return KeyUtil.getMessageValue("muafiyet.istifa");
		} else {
			return "-";
		}
	}

	@SuppressWarnings("unchecked")
	public void uyeOdeme(Odeme odeme, UyeSiparis uyeSiparis) throws DBException {
		EntityManager entityManager = getDBOperator().getEntityManagerFactory().createEntityManager();
		if (odeme.getRID() == null) {
			try {
				boolean uyeBorcKaydiVar = false;
				BigDecimal kalan = odeme.getMiktar();
				if (odeme.getOdemeTuru() == OdemeTuru._AIDAT) {
					List<Uyeaidat> uyeAidatListesi = getDBOperator().load(Uyeaidat.class.getSimpleName(), "o.uyeRef.kisiRef.rID=" + odeme.getKisiRef().getRID() + " AND o.miktar > o.odenen", "o.yil,o.ay");
					if (uyeAidatListesi == null || uyeAidatListesi.size() == 0) {
						if (getDBOperator().recordCount(Uyeaidat.class.getSimpleName(), "o.uyeRef.kisiRef.rID=" + odeme.getKisiRef().getRID()) == 0) {
							createGenericMessage("Üye aidat borcu bulunamadı!", FacesMessage.SEVERITY_ERROR);
							return;
						} else {
							uyeAidatListesi = getDBOperator().load(Uyeaidat.class.getSimpleName(), "o.uyeRef.kisiRef.rID=" + odeme.getKisiRef().getRID(), "o.yil DESC,o.ay DESC");
							Uyeaidat sonAidat = uyeAidatListesi.get(0);
							uyeBorcKaydiVar = true;
							uyeAidatListesi.clear();
							uyeAidatListesi.add(sonAidat);
						}

					}
					entityManager.getTransaction().begin();
					if (uyeAidatListesi.size() >= 1) {
						uyeBorcKaydiVar = true;
						for (int x = 0; x < uyeAidatListesi.size() - 1; x++) {
							if (uyeAidatListesi.get(x).getKalan().longValue() > 0) {
								if (kalan.subtract(uyeAidatListesi.get(x).getKalan()).longValue() > 0) {
									kalan = kalan.subtract(uyeAidatListesi.get(x).getKalan());
									// System.out.println("Kalan :" + kalan);
									uyeAidatListesi.get(x).setOdenen(uyeAidatListesi.get(x).getMiktar());
									entityManager.merge(uyeAidatListesi.get(x));
								} else {
									uyeAidatListesi.get(x).setOdenen(uyeAidatListesi.get(x).getOdenen().add(kalan));
									entityManager.merge(uyeAidatListesi.get(x));
									kalan = new BigDecimal(0);
								}
							}
						}
					}
					if (uyeBorcKaydiVar && kalan.longValue() > 0) {
						Uyeaidat sonAidat = uyeAidatListesi.get(uyeAidatListesi.size() - 1);
						sonAidat.setOdenen(sonAidat.getOdenen().add(kalan));
						entityManager.merge(sonAidat);
					}
				} else if (odeme.getOdemeTuru() == OdemeTuru._BORC) {
					List<Borc> borcListesi = getDBOperator().load(Borc.class.getSimpleName(), "o.kisiRef.rID=" + odeme.getKisiRef().getRID() + " AND o.miktar > o.odenen", "o.tarih");
 					if (borcListesi == null || borcListesi.size() == 0) {
//						System.out.println("Borclist empty....");
						if (getDBOperator().recordCount(Borc.class.getSimpleName(), "o.kisiRef.rID=" + odeme.getKisiRef().getRID()) == 0) {
							createGenericMessage("Üye borcu bulunamadı!", FacesMessage.SEVERITY_ERROR);
							return;
						} else {
							borcListesi = getDBOperator().load(Borc.class.getSimpleName(), "o.kisiRef.rID=" + odeme.getKisiRef().getRID(), "o.tarih DESC");
							Borc sonBorc = borcListesi.get(0);
							borcListesi.clear();
							uyeBorcKaydiVar = true;
							borcListesi.add(sonBorc);
						}
					}
					entityManager.getTransaction().begin();
					if (borcListesi.size() >= 1) {
//						System.out.println("Borclist size...." + borcListesi.size());
						uyeBorcKaydiVar = true;
						for (int x = 0; x < borcListesi.size() - 1; x++) {
							if (borcListesi.get(x).getKalan().longValue() > 0) {
								if (kalan.subtract(borcListesi.get(x).getKalan()).longValue() > 0) {
									kalan = kalan.subtract(borcListesi.get(x).getKalan());
//									System.out.println("Kalan :" + kalan);
									borcListesi.get(x).setOdenen(borcListesi.get(x).getMiktar());
									entityManager.merge(borcListesi.get(x));
								} else {
									borcListesi.get(x).setOdenen(borcListesi.get(x).getOdenen().add(kalan));
									entityManager.merge(borcListesi.get(x));
									kalan = new BigDecimal(0);
								}
							}
						}
					}
//					System.out.println("Borclar guncellendi, kalan :" + kalan);
					if (uyeBorcKaydiVar && kalan.longValue() > 0) {
						Borc sonBorc = borcListesi.get(borcListesi.size() - 1);
						sonBorc.setOdenen(kalan.add(sonBorc.getOdenen()));
						entityManager.merge(sonBorc);
					}

				}
				entityManager.persist(odeme);

				// Iliskili siparis bilgisi gonderilmisse, ilgili siparis guncelleniyor.
				if(uyeSiparis != null && uyeSiparis.getRID() != null){
					entityManager.merge(uyeSiparis);
				}

				entityManager.getTransaction().commit();
				entityManager.close();
				logKaydet(odeme, IslemTuru._EKLEME, null);
			} catch (Exception e) {
				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
				LogService.logYaz("Exception @SessionController : " + e.getMessage(), e);
				createGenericMessage(KeyUtil.getMessageValue("islem.hata"), FacesMessage.SEVERITY_FATAL);
			}
		}
	}

	public String evrakKayitNoOlustur(Evrak evrak) throws DBException {
		int kayitNo = getDBOperator().recordCount(Evrak.class.getSimpleName(), "o.yil = " + evrak.getYil() + " AND o.evrakHareketTuru = " + evrak.getEvrakHareketTuru().getCode());
		kayitNo++;
		String evrakTuru = "GL";
		String prefix = getSessionUser().getPersonelRef().getBirimRef().getKisaad();
		if (evrak.getEvrakHareketTuru() == EvrakHareketTuru._GIDEN) {
			evrakTuru = "GD";
		} else if (evrak.getEvrakHareketTuru() == EvrakHareketTuru._ICYAZISMA) {
			evrakTuru = "ICY";
		}
		String result = evrak.getYil() + "-" + evrakTuru + "-" + StringUtil.fillStringLeft(kayitNo + "", "0", 5);
		while (true) {
			if (getDBOperator().recordCount(Evrak.class.getSimpleName(), "o.kayitno='" + prefix + "-" + result + "'") == 0) {
				break;
			} else {
				kayitNo++;
				result = evrak.getYil() + "-" + evrakTuru + "-" + StringUtil.fillStringLeft(kayitNo + "", "0", 5);
			}
		}
		return prefix + "-" + result;
	}

	public Evrakdolasimbildirim bildirimOlustur(String alici, Evrak evrak, String aciklama) {
		Evrakdolasimbildirim bildirim = new Evrakdolasimbildirim();
		bildirim.setTarih(new Date());
		bildirim.setGonderenRef(getSessionUser().getPersonelRef().getUIString());
		bildirim.setEvrakRef(evrak);
		bildirim.setAliciRef(alici);
		bildirim.setAciklama(aciklama);
		return bildirim;
	}

	public SistemParametre getSistemParametre() {
		if (getDBOperator().recordCount(SistemParametre.class.getSimpleName()) > 0) {
			return (SistemParametre) getDBOperator().load(SistemParametre.class.getSimpleName()).get(0);
		} else {
			return null;
		}
	}

	public BigDecimal getUyeAidatBorc(Long uyeRID) {
		try {
			BigDecimal borc = (BigDecimal) getDBOperator().sum("miktar-odenen", Uyeaidat.class.getSimpleName(), "o.uyeRef.rID=" + uyeRID + " AND o.uyemuafiyetRef is null");
			return BigDecimalUtil.changeToCurrency(borc);
		} catch (Exception e) {
			return BigDecimalUtil.changeToCurrency(new BigDecimal(0L));
		}
	}

	/* ENUM KODLARI */
	public int getOgrenciCode() {
		return UyeTip._OGRENCI.getCode();
	}

	public int getHayirCode() {
		return EvetHayir._HAYIR.getCode();
	}

	public int getPlanlananCode() {
		return EgitimDurum._Planlanan.getCode();
	}

	public int getZamanlamaliCode() {
		return EvetHayir._EVET.getCode();
	}

	public int getEvetCode() {
		return EvetHayir._EVET.getCode();
	}

	public int getUyeKayitDurumOnaylanmisCode() {
		return UyeKayitDurum._ONAYLANMIS.getCode();
	}

	public int getYurtdisiAdresCode() {
		return AdresTipi._YURTDISIADRESI.getCode();
	}

	public int getGelenEvrakCode() {
		return EvrakHareketTuru._GELEN.getCode();
	}

	public int getGidenEvrakCode() {
		return EvrakHareketTuru._GIDEN.getCode();
	}

	public int getIcYazismaCode() {
		return EvrakHareketTuru._ICYAZISMA.getCode();
	}

	public int getEvrakTaslakCode() {
		return EvrakDurum._TASLAK.getCode();
	}

	public int getEvrakOnaylanmisCode() {
		return EvrakDurum._ONAYLANMIS.getCode();
	}

	public int getEvrakTamamlanmisCode() {
		return EvrakDurum._TAMAMLANMIS.getCode();
	}

	public int getEvrakReddedilmisCode() {
		return EvrakDurum._REDDEDILMIS.getCode();
	}

	public int getUyeDurumKapaliCode() {
		return UyeDurum._KAPALI.getCode();
	}

	public int getUyeDurumIstifaCode() {
		return UyeDurum._ISTIFA.getCode();
	}

	public int getUyeTipiYabanciCode() {
		return UyeTip._YABANCILAR.getCode();
	}

	public int getKurulUyelikDurumuIstifaCode() {
		return KurulUyelikDurumu._UYELIKTENISTIFA.getCode();
	}

	public int getKurulUyelikDurumuNullCode() {
		return KurulUyelikDurumu._NULLDEGERI.getCode();
	}

	public int getKurulUyelikDurumuUyelikDusmeCode() {
		return KurulUyelikDurumu._UYELIGINDUSMESI.getCode();
	}

	public int getKomisyonUyelikDurumuIstifaCode() {
		return KomisyonUyelikDurumu._UYELIKTENISTIFA.getCode();
	}

	public int getKomisyonUyelikDurumuUyelikDusmeCode() {
		return KomisyonUyelikDurumu._UYELIGINDUSMESI.getCode();
	}

	public int getUyekurumDurumAyrilmisCode() {
		return UyekurumDurum._AYRILMIS.getCode();
	}

	public EvetHayir getEvetHayirEvetCode() {
		return EvetHayir._EVET;
	}

	public EvetHayir getEvetHayirHayirCode() {
		return EvetHayir._HAYIR;
	}

	public int getKomisyonUyelikDurumuNullCode() {
		return KomisyonUyelikDurumu._NULLDEGERI.getCode();
	}

	public int getKisiCodeForIletisim() {
		return ReferansTipi._KISI.getCode();
	}

	public int getUyecodeForIletisim() {
		return ReferansTipi._UYE.getCode();
	}

	public int getBirimcodeForIletisim() {
		return ReferansTipi._BIRIM.getCode();
	}

	public int getKurumcodeForIletisim() {
		return ReferansTipi._KURUM.getCode();
	}

	public int getKurulcodeForIletisim() {
		return ReferansTipi._KURUL.getCode();
	}

	public int getKomisyoncodeForIletisim() {
		return ReferansTipi._KOMISYON.getCode();
	}

	public int getEgitmencodeForIletisim() {
		return ReferansTipi._EGITMEN.getCode();
	}

	public int getEgitimkatilimcicodeForIletisim() {
		return ReferansTipi._EGITIMKATILIMCI.getCode();
	}

	public int getEtkinlikkatilimcicodeForIletisim() {
		return ReferansTipi._ETKINLIKKATILIMCI.getCode();
	}

	public int getIsyeritemsilcisicodeForIletisim() {
		return ReferansTipi._ISYERITEMSILCISI.getCode();
	}

	public int getKpsAdresTuru() {
		return AdresTuru._KPSADRESI.getCode();
	}

	public int getGenelEvrakTuruCode() {
		return HazirEvrakTuru._GENELEVRAK.getCode();
	}

	public int getIhaleBelgesiCode() {
		return HazirEvrakTuru._IHALEBELGESI.getCode();
	}

	public int getSantiyeSefligiCode() {
		return HazirEvrakTuru._SANTIYESEFLIGIBELGESI.getCode();
	}

	public int getGenelKurulCode() {
		return KurulTuru._GENELKURUL.getCode();
	}

	public int getKoordinasyonKuruluCode() {
		return KurulTuru._KOORDINASYONKURULU.getCode();
	}

	public int getOdemeSekliBankaVezneCode() {
		return OdemeSekli._BANKADAN.getCode();
	}

	public String getKpsKullaniciAdi() {
		try {
			if (FacesContext.getCurrentInstance().getExternalContext().getInitParameter("tmmobKpsKullaniciAdi") != null) {
				return FacesContext.getCurrentInstance().getExternalContext().getInitParameter("tmmobKpsKullaniciAdi");
			} else {
				return ApplicationConstant._kpsKullaniciAdi;
			}
		} catch (Exception e) {
			return ApplicationConstant._kpsKullaniciAdi;
		}
	}

	public String getKpsSifre() {
		try {
			if (FacesContext.getCurrentInstance().getExternalContext().getInitParameter("tmmobKpsSifre") != null) {
				return FacesContext.getCurrentInstance().getExternalContext().getInitParameter("tmmobKpsSifre");
			} else {
				return ApplicationConstant._kpsSifre;
			}
		} catch (Exception e) {
			return ApplicationConstant._kpsSifre;
		}
	}

	public String getKpsEndPoint() {
		try {
			if (ManagedBeanLocator.locateSessionController().getKpsSifre() != null) {
				return FacesContext.getCurrentInstance().getExternalContext().getInitParameter("tmmobKpsEndpoint");
			} else {
				return ApplicationConstant._kpsEndpoint;
			}
		} catch (Exception e) {
			return ApplicationConstant._kpsEndpoint;
		}
	}

	@SuppressWarnings("rawtypes")
	public List loadObjects(String modelName, String whereCon) {
		return loadObjects(modelName, whereCon, "o.rID");
	}

	@SuppressWarnings("rawtypes")
	public List loadObjects(String modelName, String whereCon, String orderField) {
		try {
			return getDBOperator().load(modelName, whereCon, orderField);
		} catch (Exception e) {
			return null;
		}
	}

	@SuppressWarnings("rawtypes")
	public List loadObjects(String query) {
		try {
			return getDBOperator().loadByQuery(query);
		} catch (Exception e) {
			return null;
		}
	}

	public int countObjects(String modelName, String whereCon) {
		try {
			return getDBOperator().recordCount(modelName, whereCon);
		} catch (Exception e) {
			return 0;
		}
	}

	public void saveObject(BaseEntity obj) {
		try {
			getDBOperator().insert(obj);
		} catch (Exception e) {
			System.out.println("ERROR saving object :" + e.getMessage());
		}
	}

	protected void logKaydet(BaseEntity entity, IslemTuru islemTuru, String oldVal) throws DBException {
		// StringUtil st = new StringUtil();
		Islemlog islemLog = new Islemlog();
		islemLog.setIslemtarihi(new Date());
		islemLog.setIslemturu(islemTuru);
		islemLog.setReferansRID(entity.getRID());
		islemLog.setTabloadi(entity.getClass().getSimpleName());
		if (islemTuru != IslemTuru._SILME) {
			islemLog.setEskideger(oldVal);
			islemLog.setYenideger(entity.getValue());
			// islemLog.setYenideger(st.findDifferencesBetweenTwoStrings(oldVal,
			// entity.getValue()));
		} else {
			islemLog.setEskideger(entity.getValue());
			islemLog.setYenideger("");
		}
		Map<String, Object> sessionMap = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
		islemLog.setKullaniciRid(((SessionUserBase) sessionMap.get("sessionUser")).getKullaniciRid());
		islemLog.setKullaniciText(((SessionUserBase) sessionMap.get("sessionUser")).getKullaniciText());
		getDBOperator().insert(islemLog);
		islemLog = new Islemlog();
	}
}
