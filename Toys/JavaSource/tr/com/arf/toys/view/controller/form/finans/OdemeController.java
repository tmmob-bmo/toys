package tr.com.arf.toys.view.controller.form.finans;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletResponse;

import org.primefaces.event.FileUploadEvent;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.framework.utility.exception.data.DBException;
import tr.com.arf.framework.utility.tool.DateUtil;
import tr.com.arf.framework.utility.tool.KeyUtil;
import tr.com.arf.framework.utility.tool.RandomObjectGenerator;
import tr.com.arf.toys.db.enumerated.finans.OdemeTuru;
import tr.com.arf.toys.db.filter.finans.OdemeFilter;
import tr.com.arf.toys.db.manager.CustomDBOperator;
import tr.com.arf.toys.db.model.finans.Odeme;
import tr.com.arf.toys.db.model.system.Kullanici;
import tr.com.arf.toys.db.model.uye.Uye;
import tr.com.arf.toys.service.application.ManagedBeanLocator;
import tr.com.arf.toys.service.export.CustomPdfExport;
import tr.com.arf.toys.utility.tool.ExcelImport;
import tr.com.arf.toys.view.controller.form._base.ToysBaseFilteredController;

import com.itextpdf.text.DocumentException;

@ManagedBean
@ViewScoped
public class OdemeController extends ToysBaseFilteredController {

	private static final long serialVersionUID = 6531152165884980748L;

	private Odeme odeme;
	private OdemeFilter odemeFilter = new OdemeFilter();

	private boolean uploadExcelPanelRendered = false;

	public OdemeController() {
		super(Odeme.class);
		setDefaultValues(false);
		setOrderField("tarih DESC");
		setAutoCompleteSearchColumns(new String[] { "miktar" });
		setTable(null);
		queryAction();
	}

	public OdemeController(Object object) {
		super(Odeme.class);
	}

	public Odeme getOdeme() {
		odeme = (Odeme) getEntity();
		return odeme;
	}

	public void setOdeme(Odeme odeme) {
		this.odeme = odeme;
	}

	public OdemeFilter getOdemeFilter() {
		return odemeFilter;
	}

	public void setOdemeFilter(OdemeFilter odemeFilter) {
		this.odemeFilter = odemeFilter;
	}

	@Override
	public void insert() {
		super.insert();
		getOdeme().setTarih(new Date());
		getOdeme().setBirimRef(getSessionUser().getPersonelRef().getBirimRef());
		setUploadExcelPanelRendered(false);
		setHataMesajlari(null);
	}

	@Override
	public void cancel() {
		super.cancel();
		setUploadExcelPanelRendered(false);
		setHataMesajlari(null);
	}

	@Override
	public BaseFilter getFilter() {
		return getOdemeFilter();
	}

	@Override
	public void save() {
		setTable(null);
		if (getOdeme().getRID() == null) {
			try {
				ManagedBeanLocator.locateSessionController().uyeOdeme(getOdeme(), null);
				createGenericMessage(KeyUtil.getMessageValue("kayit.eklendi"), FacesMessage.SEVERITY_INFO);
			} catch (DBException e) {
				createGenericMessage(KeyUtil.getMessageValue("islem.hata"), FacesMessage.SEVERITY_FATAL);
				e.printStackTrace();
			}
		} else {
			if (getOdeme().getValue().hashCode() == getOldValue().hashCode()) {
				createGenericMessage(KeyUtil.getMessageValue("islem.degisiklikYok"), FacesMessage.SEVERITY_INFO);
				return;
			}

		}
		setEditPanelRendered(false);
		queryAction();
	}

	/********************************************************* RAPORLAMALAR *******************************************************************/
	private Date raporTarih = new Date();

	public Date getRaporTarih() {
		return raporTarih;
	}

	public void setRaporTarih(Date raporTarih) {
		this.raporTarih = raporTarih;
	}

	public void hazirMakbuzCikti(Odeme odeme) throws DocumentException, Exception {
		String formTuru = "makbuz";
		Kullanici sysUser = ManagedBeanLocator.locateSessionUser().getKullanici();
		if (getDBOperator().recordCount(Uye.class.getSimpleName(), "o.kisiRef.rID=" + odeme.getKisiRef().getRID()) > 0) {
			Uye uye = (Uye) getDBOperator().load(Uye.class.getSimpleName(), "o.kisiRef.rID=" + odeme.getKisiRef().getRID(), "").get(0);
			byte[] arr = CustomPdfExport.makbuz(uye.getSicilno() + formTuru + ".pdf", uye, sysUser, odeme);
			download(arr, uye.getSicilno() + uye.getKisiRef().getAd() + "_" + uye.getKisiRef().getSoyad() + "_" + formTuru);
		} else {
			createGenericMessage("Çıktı Alamadıysanız Lütfen Sistem Yöneticisi İle İrtibata Geçiniz!", FacesMessage.SEVERITY_INFO);
		}
		odeme.setMakbuzbasilmatarihi(new Date());
		getDBOperator().update(odeme);
		queryAction();
	}

	public void gunlukAidatTahsilatSorgusu() throws DocumentException, Exception {
		if (getRaporTarih() == null) {
			createGenericMessage("Tarih seçiniz!", FacesMessage.SEVERITY_ERROR);
			return;
		}
		String whereCondition = "o.odemeTuru =" + OdemeTuru._AIDAT.getCode() + " AND TO_CHAR(o.tarih, 'yyyy-mm-dd') = '" + DateUtil.dateToYMD(getRaporTarih()) + "'";
		// logYaz("whereCondition :" + whereCondition);
		@SuppressWarnings("unchecked")
		List<Odeme> odemeListesi = getDBOperator().load(Odeme.class.getSimpleName(), whereCondition, "o.tarih ASC");
		if (!isEmpty(odemeListesi)) {
			byte[] arr = CustomPdfExport.gunlukAidatTahsilatRaporu(odemeListesi);
			download(arr, DateUtil.dateToYMD(getRaporTarih()) + "_" + RandomObjectGenerator.generateRandomString(6, true));
		} else {
			createGenericMessage("Seçilen tarihte ödeme yapılmamış!", FacesMessage.SEVERITY_INFO);
		}

	}

	public void download(byte[] pdfData, String fileName) throws IOException {
		// Prepare.
		FacesContext facesContext = FacesContext.getCurrentInstance();
		ExternalContext externalContext = facesContext.getExternalContext();
		HttpServletResponse response = (HttpServletResponse) externalContext.getResponse();

		// Initialize response.
		response.reset();
		response.setContentType("application/pdf");
		response.setHeader("Content-disposition", "attachment; filename=\"" + fileName + ".pdf\"");

		// Write file to response.
		OutputStream output = response.getOutputStream();
		output.write(pdfData);
		output.close();

		// Inform JSF to not take the response in hands.
		facesContext.responseComplete();
	}

	/* EXCEL UPLOAD */
	private ArrayList<String> hataMesajlari;
	private CustomDBOperator customDBOperator; /* Data Operator */

	public void excelYuklemePaneliAc() {
		insertOnly();
		setHataMesajlari(null);
		setUploadExcelPanelRendered(true);
		setEditPanelRendered(false);
	}

	public boolean isUploadExcelPanelRendered() {
		return uploadExcelPanelRendered;
	}

	public void setUploadExcelPanelRendered(boolean uploadExcelPanelRendered) {
		this.uploadExcelPanelRendered = uploadExcelPanelRendered;
	}

	public ArrayList<String> getHataMesajlari() {
		return hataMesajlari;
	}

	public void setHataMesajlari(ArrayList<String> hataMesajlari) {
		this.hataMesajlari = hataMesajlari;
	}

	@Override
	public void fileUploadListener(FileUploadEvent event) throws Exception {
		super.fileUploadListener(event);
		uploadExcel();
	}

	/* Data Operator */
	protected final CustomDBOperator getCustomDBOperator() {
		if (customDBOperator == null) {
			customDBOperator = CustomDBOperator.getInstance();
		}
		return customDBOperator;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void uploadExcel() {
		if (isEmpty(getOdeme().getPath())) {
			createGenericMessage(KeyUtil.getMessageValue("dosya.yukleyiniz"), FacesMessage.SEVERITY_ERROR);
		} else {
			Map<String, ArrayList> excelImportLists = null;
			try {
				excelImportLists = ExcelImport.odemeDosyasiYukle(getOdeme().getPath());
				// Dosya siliniyor...
				if (getOdeme() != null) {
					if (!isEmpty(getOdeme().getPath())) {
						File file = new File(getOdeme().getPath());
						if (!file.delete()) {
							logYaz(getOdeme().getPath() + " could not be deleted!");
						}
					}
				}
				if (excelImportLists != null && excelImportLists.get("odemeler") != null && excelImportLists.get("hataMesajlari") != null) {
					ArrayList<Odeme> eklenecekOdemeler = excelImportLists.get("odemeler");
					ArrayList<String> hataMesajlari = excelImportLists.get("hataMesajlari");
					try {
						hataMesajlari.addAll(getCustomDBOperator().odemeTopluKaydet(eklenecekOdemeler));
						if (!isEmpty(hataMesajlari)) {
							createGenericMessage(KeyUtil.getMessageValue("dogrusatirlar.eklendi"), FacesMessage.SEVERITY_INFO);
						} else {
							createGenericMessage(KeyUtil.getMessageValue("tumsatirlar.eklendi"), FacesMessage.SEVERITY_INFO);
						}
						queryAction();
					} catch (Exception e) {
						createGenericMessage(KeyUtil.getMessageValue("islem.hata") + " (" + e.getMessage() + ")", FacesMessage.SEVERITY_ERROR);
					}
					setHataMesajlari(hataMesajlari);
				}
			} catch (Exception e1) {
				createGenericMessage(KeyUtil.getMessageValue("islem.hata") + " (" + e1.getMessage() + ")", FacesMessage.SEVERITY_ERROR);
			}
		}
	}

	public void odemeIptal() throws DocumentException, Exception {
		if (!setSelected()) {
			return;
		}
		String hataMesaji = getCustomDBOperator().odemeIptal(getOdeme());
		if (!isEmpty(hataMesaji)) {
			createGenericMessage(hataMesaji, FacesMessage.SEVERITY_ERROR);
		} else {
			createGenericMessage(KeyUtil.getMessageValue("kayit.silindi"), FacesMessage.SEVERITY_INFO);
		}
		queryAction();
	}
} // class
