package tr.com.arf.toys.view.controller.form.uye;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.toys.db.filter.uye.UyedurumdegistirmeFilter;
import tr.com.arf.toys.db.model.uye.Uye;
import tr.com.arf.toys.db.model.uye.Uyedurumdegistirme;
import tr.com.arf.toys.service.application.ApplicationDescriptor;
import tr.com.arf.toys.view.controller.form._base.ToysBaseFilteredController;

@ManagedBean
@ViewScoped
public class UyedurumdegistirmeController extends ToysBaseFilteredController {

	private static final long serialVersionUID = 6531152165884980748L;

	private Uyedurumdegistirme uyedurumdegistirme;
	private UyedurumdegistirmeFilter uyedurumdegistirmeFilter = new UyedurumdegistirmeFilter();

	public UyedurumdegistirmeController() {
		super(Uyedurumdegistirme.class);
		setDefaultValues(false);
		setOrderField("uyeRef.rID");
		setLoggable(false);
		setAutoCompleteSearchColumns(new String[] { "uyeRef" });
		setTable(null);
		if (getMasterEntity() != null) {
			setMasterObjectName(ApplicationDescriptor._UYE_MODEL);
			setMasterOutcome(ApplicationDescriptor._UYE_MODEL);
			getUyedurumdegistirmeFilter().setUyeRef(getMasterEntity());
			getUyedurumdegistirmeFilter().createQueryCriterias();
		}
		queryAction();
	}

	public UyedurumdegistirmeController(Object object) {
		super(Uyedurumdegistirme.class);
	}

	public Uye getMasterEntity() {
		if (getUyedurumdegistirmeFilter().getUyeRef() != null) {
			return getUyedurumdegistirmeFilter().getUyeRef();
		} else {
			return (Uye) getObjectFromSessionFilter(ApplicationDescriptor._UYE_MODEL);
		}
	}

	public Uyedurumdegistirme getUyedurumdegistirme() {
		uyedurumdegistirme = (Uyedurumdegistirme) getEntity();
		return uyedurumdegistirme;
	}

	public void setUyedurumdegistirme(Uyedurumdegistirme uyedurumdegistirme) {
		this.uyedurumdegistirme = uyedurumdegistirme;
	}

	public UyedurumdegistirmeFilter getUyedurumdegistirmeFilter() {
		return uyedurumdegistirmeFilter;
	}

	public void setUyedurumdegistirmeFilter(UyedurumdegistirmeFilter uyedurumdegistirmeFilter) {
		this.uyedurumdegistirmeFilter = uyedurumdegistirmeFilter;
	}

	@Override
	public BaseFilter getFilter() {
		return getUyedurumdegistirmeFilter();
	}

	@Override
	public void insert() {
		super.insert();
		getUyedurumdegistirme().setUyeRef(getUyedurumdegistirmeFilter().getUyeRef());
	}

} // class
