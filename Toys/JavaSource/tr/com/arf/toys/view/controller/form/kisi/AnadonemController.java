package tr.com.arf.toys.view.controller.form.kisi; 
  
 
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.primefaces.model.DualListModel;

import tr.com.arf.framework.db.enumerated._common.EvetHayir;
import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.framework.db.model._base.BaseEntity;
import tr.com.arf.framework.utility.exception.data.DBException;
import tr.com.arf.framework.utility.tool.DateUtil;
import tr.com.arf.framework.utility.tool.KeyUtil;
import tr.com.arf.toys.db.enumerated.system.BirimDurum;
import tr.com.arf.toys.db.filter.kisi.AnadonemFilter;
import tr.com.arf.toys.db.model.kisi.Anadonem;
import tr.com.arf.toys.db.model.kisi.Donem;
import tr.com.arf.toys.db.model.system.Birim;
import tr.com.arf.toys.service.application.ApplicationDescriptor;
import tr.com.arf.toys.service.application.ManagedBeanLocator;
import tr.com.arf.toys.view.controller.form._base.ToysBaseFilteredController;
  
  
@ManagedBean 
@ViewScoped 
public class AnadonemController extends ToysBaseFilteredController {  
  
  private static final long serialVersionUID = 6531152165884980748L; 
  
	private Anadonem anadonem;  
	private AnadonemFilter anadonemFilter = new AnadonemFilter();  
  
	public AnadonemController() { 
		super(Anadonem.class);  
		setDefaultValues(false);  
		setOrderField("tanim");  
		setLoggable(false);  
		setAutoCompleteSearchColumns(new String[]{"tanim"}); 
		setTable(null); 
		queryAction(); 
	} 
	
	
	public Anadonem getAnadonem() { 
		anadonem = (Anadonem) getEntity(); 
		return anadonem; 
	} 
	
	public void setAnadonem(Anadonem anadonem) { 
		this.anadonem = anadonem; 
	} 
	
	public AnadonemFilter getAnadonemFilter() { 
		return anadonemFilter; 
	} 
	 
	public void setAnadonemFilter(AnadonemFilter anadonemFilter) {  
		this.anadonemFilter = anadonemFilter;  
	}  
	 
	@Override  
	public BaseFilter getFilter() {  
		return getAnadonemFilter();  
	}  
		
	public String donem() {	
		if (!setSelected()) {
			return "";
		}	
		getSessionUser().setLegalAccess(1);	
		putObjectToSessionFilter(ApplicationDescriptor._ANADONEM_MODEL, getAnadonem());
		return ManagedBeanLocator.locateMenuController().gotoPage("menu_Donem"); 
	} 
	
	@Override
	public void insert(){
		super.insert();
		super.fillDualListFor(Birim.class.getSimpleName(), "o.durum=" + BirimDurum._ACIK.getCode(), "ad");
	}
	


	@SuppressWarnings("unchecked")
	public void fillTargetDualListFor(String modelName, String queryCriteria, String orderField){		
		List<BaseEntity> source =new ArrayList<BaseEntity>();
		List<Long> birimRID=new ArrayList<Long>();
		List<BaseEntity> target =new ArrayList<BaseEntity>();
		List <Donem> donemListesi =getDBOperator().load(Donem.class.getSimpleName(), "o.anadonemRef.rID=" + getAnadonem().getRID(),"");
		if (!isEmpty(donemListesi)){
			for (Donem donem: donemListesi){
				List <Birim> birimListesi1 = getDBOperator().load(Birim.class.getSimpleName(), "o.rID =" + donem.getBirimRef().getRID(),"");
				if (!isEmpty(birimListesi1)){
					for (Birim birim : birimListesi1){
						target.add(birim);
						birimRID.add(birim.getRID());
					}
				}
			}
		}
		Long rID=0L;
		List<Birim> birimListesi = getDBOperator().load(Birim.class.getSimpleName(), "", "");
		if (!isEmpty(birimListesi)){
			for (Birim birimSource : birimListesi){
				for (Long birimrID : birimRID ){
					rID=birimrID;
					if (birimSource.getRID()== birimrID){
						break;
					}
				}
				if (birimSource.getRID()!= rID){
					source.add(birimSource);
				}
			}
		}
		setDualList(new DualListModel<BaseEntity>(source, target)); 
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public void update(){
		super.update();
		List <Donem> donemListesi =getDBOperator().load(Donem.class.getSimpleName(), "o.anadonemRef.rID=" + getAnadonem().getRID(),"");
		if (!isEmpty(donemListesi)){
			fillTargetDualListFor(Birim.class.getSimpleName(), "" , "ad");
		}
		else 
			super.fillDualListFor(Birim.class.getSimpleName(), "o.durum=" + BirimDurum._ACIK.getCode(), "ad");
		
		
	}
	
	String tanim="";
	
	public String getTanim() {
		return tanim;
	}

	public void setTanim(String tanim) {
		this.tanim = tanim;
	}


	@SuppressWarnings("unchecked")
	@Override
	public void save(){
		if(getTanim()!=null){
			getAnadonem().setTanim(getTanim()+".Dönem");
		}
		super.justSave(); 
		
		
		Donem donem = new Donem();
		if(!isEmpty(getDualList().getTarget())){
			List<Donem> donemListesi  =getDBOperator().load(Donem.class.getSimpleName(),"o.anadonemRef.rID=" + getAnadonem().getRID(), "");
			if (!isEmpty(donemListesi)){
				for(Donem donem1 :donemListesi){
					try {
						getDBOperator().delete(donem1);
					} catch (DBException e) {
						// TODO Auto-generated catch block
						logYaz("Exception @" + getModelName() + "Controller :", e);
					}
				}
				
			}
			List<BaseEntity> birimList = getDualList().getTarget(); 			
			for(BaseEntity b : birimList){
				donem = new Donem();
				donem.setBirimRef( (Birim) b);
				donem.setAnadonemRef(getAnadonem());
				donem.setTanim(getAnadonem().getTanim() + " - " +  ((Birim) b).getAd());
				donem.setBaslangictarih(new Date());
				donem.setBitistarih(DateUtil.addDaysToDate(new Date(), 365));
				try {
					getDBOperator().update(donem);
				} catch (DBException e) {
					logYaz("Exception @" + getModelName() + "Controller :", e);
				}
			}
		}
		if(this.anadonem.getAktif() == EvetHayir._EVET){
			String whereCondition = "o.rID <> " + this.anadonem.getRID() + " AND o.aktif =" + EvetHayir._EVET.getCode();
			try {
					if(getDBOperator().recordCount(Anadonem.class.getSimpleName(), whereCondition) > 0){
						for(Object obj : getDBOperator().load(Anadonem.class.getSimpleName(), whereCondition, "o.rID")){
							Anadonem anadonem1 = (Anadonem) obj;
							anadonem1.setAktif(EvetHayir._HAYIR);
							getDBOperator().update(anadonem1);
						}					
					}
			} catch (Exception e) { 
				logYaz("Exception @" + getModelName() + "Controller :", e);
			} 
		} else if (this.anadonem.getAktif() == EvetHayir._HAYIR){
			String whereCondition = "o.rID <> " + this.anadonem.getRID() + " AND o.aktif =" + EvetHayir._EVET.getCode();
			try {
				if(getDBOperator().recordCount(Anadonem.class.getSimpleName(), whereCondition) == 0){
					anadonem.setAktif(EvetHayir._EVET);
					getDBOperator().update(anadonem);
				}	
				whereCondition = "o.rID <> " + this.anadonem.getRID() + " AND o.aktif =" + EvetHayir._HAYIR.getCode();
//				if(getDBOperator().recordCount(Anadonem.class.getSimpleName(), whereCondition) == 0){ 
//					createGenericMessage(KeyUtil.getMessageValue("aktif.hata"), FacesMessage.SEVERITY_ERROR);  
//					anadonem.setAktif(EvetHayir._EVET);
//					getDBOperator().update(anadonem);
//				}
				
			}catch (Exception e) { 
				logYaz("Exception @" + getModelName() + "Controller :", e);
			}  
		}
		
		queryAction();
	}
	
	@Override
		public void delete() {
			
			try {
				super.delete();
				setSelection(null); 
				setTable(null);
				createGenericMessage(KeyUtil.getMessageValue("kayit.silindi"), FacesMessage.SEVERITY_INFO); 
			} catch (Exception e) {
				createGenericMessage(KeyUtil.getMessageValue("islem.hata") + " Mes : " + e.getMessage(), FacesMessage.SEVERITY_ERROR);  
				return;
			} 
			String whereCondition ="o.aktif =" + EvetHayir._EVET.getCode();
			try {
				if(getDBOperator().recordCount(Anadonem.class.getSimpleName(), whereCondition) == 0){
					whereCondition = "o.rID <> " + this.anadonem.getRID() + " AND o.aktif =" + EvetHayir._HAYIR.getCode();
				
					if(getDBOperator().recordCount(Anadonem.class.getSimpleName(), whereCondition)> 0){
						Anadonem anadonem1 = (Anadonem) getDBOperator().load(Anadonem.class.getSimpleName(), whereCondition, "o.rID DESC").get(0);
						if (anadonem1!=null){
							anadonem1.setAktif(EvetHayir._EVET);
							getDBOperator().update(anadonem1);
						}
					}
				}	 
			}catch (Exception e) { 
				logYaz("Exception @" + getModelName() + "Controller :", e);
			}
			queryAction();
		}
	
} // class 
