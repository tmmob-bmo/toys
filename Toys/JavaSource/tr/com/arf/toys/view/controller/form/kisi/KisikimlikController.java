package tr.com.arf.toys.view.controller.form.kisi; 
  
 
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.toys.db.filter.kisi.KisikimlikFilter;
import tr.com.arf.toys.db.model.kisi.Kisi;
import tr.com.arf.toys.db.model.kisi.Kisikimlik;
import tr.com.arf.toys.service.application.ApplicationDescriptor;
import tr.com.arf.toys.view.controller.form._base.ToysBaseFilteredController;
  
  
@ManagedBean 
@ViewScoped 
public class KisikimlikController extends ToysBaseFilteredController {  
  
  private static final long serialVersionUID = 6531152165884980748L; 
  
	private Kisikimlik kisikimlik;  
	private KisikimlikFilter kisikimlikFilter = new KisikimlikFilter();  
  
	public KisikimlikController() { 
		super(Kisikimlik.class);  
		setDefaultValues(false);  
		setOrderField("dogumtarih");  
		setAutoCompleteSearchColumns(new String[]{"kimlikno"}); 
		setTable(null); 
		setLoggable(true);
		if(getMasterEntity() != null){ 
			setMasterObjectName(ApplicationDescriptor._KISI_MODEL);
			setMasterOutcome(ApplicationDescriptor._KISI_MODEL);
			getKisikimlikFilter().setKisiRef(getMasterEntity());
			getKisikimlikFilter().createQueryCriterias();   
		}  
		queryAction(); 
	} 
 
	public KisikimlikController(Object object) { 
		super(Kisikimlik.class);
		setLoggable(true); 
	}
	
	public Kisi getMasterEntity(){
		if(getKisikimlikFilter().getKisiRef() != null){
			return getKisikimlikFilter().getKisiRef();
		} else {
			return (Kisi) getObjectFromSessionFilter(ApplicationDescriptor._KISI_MODEL);
		}			
	}  
	
	public Kisikimlik getKisikimlik() { 
		kisikimlik = (Kisikimlik) getEntity(); 
		return kisikimlik; 
	} 
	
	public void setKisikimlik(Kisikimlik kisikimlik) { 
		this.kisikimlik = kisikimlik; 
	} 
	
	public KisikimlikFilter getKisikimlikFilter() { 
		return kisikimlikFilter; 
	} 
	 
	public void setKisikimlikFilter(KisikimlikFilter kisikimlikFilter) {  
		this.kisikimlikFilter = kisikimlikFilter;  
	}  
	 
	@Override  
	public BaseFilter getFilter() {  
		return getKisikimlikFilter();  
	}   
 
 
	@Override	
	public void insert() {	
		super.insert();	
		getKisikimlik().setKisiRef(getKisikimlikFilter().getKisiRef());	
	}	
	
} // class 
