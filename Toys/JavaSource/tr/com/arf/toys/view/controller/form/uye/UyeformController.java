package tr.com.arf.toys.view.controller.form.uye;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.toys.db.filter.uye.UyeformFilter;
import tr.com.arf.toys.db.model.uye.Uye;
import tr.com.arf.toys.db.model.uye.Uyeform;
import tr.com.arf.toys.service.application.ApplicationDescriptor;
import tr.com.arf.toys.view.controller.form._base.ToysBaseFilteredController;

@ManagedBean
@ViewScoped
public class UyeformController extends ToysBaseFilteredController {

	private static final long serialVersionUID = 6531152165884980748L;

	private Uyeform uyeform;
	private UyeformFilter uyeformFilter = new UyeformFilter();

	public UyeformController() {
		super(Uyeform.class);
		setDefaultValues(false);
		setOrderField("path");
		setLoggable(true);
		setAutoCompleteSearchColumns(new String[] { "path" });
		setTable(null);
		if (getMasterEntity() != null) {
			setMasterObjectName(ApplicationDescriptor._UYE_MODEL);
			setMasterOutcome(ApplicationDescriptor._UYE_MODEL);
			getUyeformFilter().setUyeRef(getMasterEntity());
			getUyeformFilter().createQueryCriterias();
		}
		queryAction();
	}

	public UyeformController(Object object) {
		super(Uyeform.class);
		setLoggable(true);
	}

	public Uye getMasterEntity() {
		return (Uye) getObjectFromSessionFilter(ApplicationDescriptor._UYE_MODEL);
	}

	public Uyeform getUyeform() {
		uyeform = (Uyeform) getEntity();
		return uyeform;
	}

	public void setUyeform(Uyeform uyeform) {
		this.uyeform = uyeform;
	}

	public UyeformFilter getUyeformFilter() {
		return uyeformFilter;
	}

	public void setUyeformFilter(UyeformFilter uyeformFilter) {
		this.uyeformFilter = uyeformFilter;
	}

	@Override
	public BaseFilter getFilter() {
		return getUyeformFilter();
	}

	@Override
	public void insertOnly() {
		super.insertOnly();
		getUyeform().setUyeRef(getMasterEntity());
	}

	@Override
	public void save() {
		getUyeform().setUyeRef(getMasterEntity());
		super.save();
	}

} // class