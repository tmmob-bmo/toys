package tr.com.arf.toys.view.controller.form.kisi; 

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.toys.db.filter.kisi.KurulDonemUyeDurumFilter;
import tr.com.arf.toys.db.model.kisi.KurulDonem;
import tr.com.arf.toys.db.model.kisi.KurulDonemUyeDurum;
import tr.com.arf.toys.service.application.ApplicationDescriptor;
import tr.com.arf.toys.view.controller.form._base.ToysBaseFilteredController;
  
  
@ManagedBean 
@ViewScoped 
public class KurulDonemUyeDurumController extends ToysBaseFilteredController {  
  
  private static final long serialVersionUID = 6531152165884980748L; 
  
	private KurulDonemUyeDurum kurulDonemUyeDurum;  
	private KurulDonemUyeDurumFilter kurulDonemUyeDurumFilter = new KurulDonemUyeDurumFilter();  
  
	public KurulDonemUyeDurumController() { 
		super(KurulDonemUyeDurum.class);  
		setDefaultValues(false);  
		setTable(null); 
		setOrderField("rID desc");
		if(getMasterEntity() != null){ 
		  if(getMasterEntity().getClass() == KurulDonem.class){
			setMasterObjectName(ApplicationDescriptor._KURULDONEM_MODEL);
			setMasterOutcome(ApplicationDescriptor._KURULDONEM_MODEL);	
			getKurulDonemUyeDurumFilter().setKurulDonemRef(getMasterEntity());
			} 
		  else {
				setMasterOutcome(null);
			}

			getKurulDonemUyeDurumFilter().createQueryCriterias();  
		}
		queryAction(); 
	} 
	
	public KurulDonem getMasterEntity(){
		if(getObjectFromSessionFilter(ApplicationDescriptor._KURULDONEM_MODEL) != null){
			return (KurulDonem) getObjectFromSessionFilter(ApplicationDescriptor._KURULDONEM_MODEL);
		}  
		else {
			return null;
		}			
	} 
	
	public KurulDonemUyeDurum getKurulDonemUyeDurum() {
		kurulDonemUyeDurum=(KurulDonemUyeDurum) getEntity();
		return kurulDonemUyeDurum;
	}

	public void setKurulDonemUyeDurum(KurulDonemUyeDurum kurulDonemUyeDurum) {
		this.kurulDonemUyeDurum = kurulDonemUyeDurum;
	}

	public KurulDonemUyeDurumFilter getKurulDonemUyeDurumFilter() {
		return kurulDonemUyeDurumFilter;
	}

	public void setKurulDonemUyeDurumFilter(
			KurulDonemUyeDurumFilter kurulDonemUyeDurumFilter) {
		this.kurulDonemUyeDurumFilter = kurulDonemUyeDurumFilter;
	}

	@Override  
	public BaseFilter getFilter() {  
		return getKurulDonemUyeDurumFilter();  
	} 
 
} // class 
