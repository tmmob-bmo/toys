package tr.com.arf.toys.view.controller.form.egitim;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.framework.db.model._base.BaseEntity;
import tr.com.arf.framework.utility.exception.data.DBException;
import tr.com.arf.toys.db.enumerated.egitim.EgitimDurum;
import tr.com.arf.toys.db.enumerated.egitim.EgitimUcretTipi;
import tr.com.arf.toys.db.enumerated.egitim.EgitimkatilimciDurum;
import tr.com.arf.toys.db.enumerated.egitim.Katilimbelgesi;
import tr.com.arf.toys.db.enumerated.egitim.Sertifika;
import tr.com.arf.toys.db.enumerated.egitim.SinavbasariDurum;
import tr.com.arf.toys.db.enumerated.finans.BorcDurum;
import tr.com.arf.toys.db.filter.egitim.EgitimkatilimciFilter;
import tr.com.arf.toys.db.model.egitim.Egitim;
import tr.com.arf.toys.db.model.egitim.EgitimUcreti;
import tr.com.arf.toys.db.model.egitim.Egitimkatilimci;
import tr.com.arf.toys.db.model.finans.Borc;
import tr.com.arf.toys.db.model.system.Odemetip;
import tr.com.arf.toys.service.application.ApplicationDescriptor;
import tr.com.arf.toys.service.application.ManagedBeanLocator;
import tr.com.arf.toys.view.controller.form._base.ToysBaseFilteredController;

@ManagedBean
@ViewScoped
public class EgitimkatilimciController extends ToysBaseFilteredController {

	private static final long serialVersionUID = 6531152165884980748L;

	private Egitimkatilimci egitimkatilimci;
	private EgitimkatilimciFilter egitimkatilimciFilter = new EgitimkatilimciFilter();
	

	public EgitimkatilimciController() {
		super(Egitimkatilimci.class);
		setDefaultValues(false);
		setOrderField("kisiRef.rID");
		setLoggable(false);
		setAutoCompleteSearchColumns(new String[] { "kisiRef" });
		setTable(null);
		if (getMasterEntity() != null) {
			setMasterObjectName(ApplicationDescriptor._EGITIM_MODEL);
			setMasterOutcome(ApplicationDescriptor._EGITIM_MODEL);
			getEgitimkatilimciFilter().setEgitimRef(getMasterEntity());
			getEgitimkatilimciFilter().createQueryCriterias();
		}
		queryAction();
	}
	
	public EgitimkatilimciController(Object object) { 
		super(Egitimkatilimci.class);    
	}

	public Egitim getMasterEntity() {
		if (getEgitimkatilimciFilter().getEgitimRef() != null) {
			return getEgitimkatilimciFilter().getEgitimRef();
		} else {
			return (Egitim) getObjectFromSessionFilter(ApplicationDescriptor._EGITIM_MODEL);
		}
	}

	public Egitimkatilimci getEgitimkatilimci() {
		egitimkatilimci = (Egitimkatilimci) getEntity();
		return egitimkatilimci;
	}

	public void setEgitimkatilimci(Egitimkatilimci egitimkatilimci) {
		this.egitimkatilimci = egitimkatilimci;
	}

	public EgitimkatilimciFilter getEgitimkatilimciFilter() {
		return egitimkatilimciFilter;
	}

	public void setEgitimkatilimciFilter(
			EgitimkatilimciFilter egitimkatilimciFilter) {
		this.egitimkatilimciFilter = egitimkatilimciFilter;
	}
	

	public int getKalanKontenjan() throws Exception {
		String criter = new String("EGITIMREF = "
				+ this.getMasterEntity().getRID().toString());
		egitimkatilimciFilter.createQueryCriterias(); 
		int kalanKontenjan=getDBOperator().recordCount("Egitimkatilimci", criter);
		return this.getMasterEntity().getKontenjan() - kalanKontenjan ;
		
	}
	@Override
	public BaseFilter getFilter() {
		return getEgitimkatilimciFilter();
	}

	@Override
	public void resetFilter() {
		super.resetFilterOnly();
		if (getMasterEntity() != null) {
			getEgitimkatilimciFilter().setEgitimRef(getMasterEntity());
		}
		queryAction();
	}

	@Override
	public void insert() {
		super.insert();
		getEgitimkatilimci().setEgitimRef(getEgitimkatilimciFilter().getEgitimRef());
	}

	@SuppressWarnings("unused")
	@Override
	public void save() {
		List<BaseEntity> list = getList();
		int toplamKatilimci = 0;
		for (BaseEntity b : list) {
			toplamKatilimci++;
			b.getRID();
		}
		int toplamKontenjan = this.egitimkatilimci.getEgitimRef().getKontenjan();
		if (toplamKatilimci < toplamKontenjan) {
			int kalanKontenjan=toplamKontenjan-toplamKatilimci;
			try {
				if (getDBOperator().recordCount(ApplicationDescriptor._KISIOGRENIM_MODEL,"o.kisiRef.rID="+ 
						this.egitimkatilimci.getKisiRef().getRID()+" AND o.bolumRef.rID IN (Select m.bolumRef.rID FROM "+
						ApplicationDescriptor._EGITIMLISANS_MODEL+  " As m WHERE m.egitimtanimRef.rID=" + this.egitimkatilimci.getEgitimRef().getEgitimtanimRef().getRID()+ ")")>0){
					if (this.getMasterEntity().getDurum() == EgitimDurum._Planlanan) {
						this.egitimkatilimci.setDurum(EgitimkatilimciDurum._Katilmadi);
						this.egitimkatilimci.setKatilimbelgesi(Katilimbelgesi._Verilmedi);
						this.egitimkatilimci.setSertifika(Sertifika._Verilmedi);
						this.egitimkatilimci.setSinavbasariDurum(SinavbasariDurum._BASARISIZ);
						this.egitimkatilimci.setSinavnotu("0");
						super.save();
						Borc borc=new Borc();
						if (getDBOperator().recordCount(ApplicationDescriptor._EGITIMUCRETI_MODEL,"o.egitimtanimRef.rID=" + 
								this.egitimkatilimci.getEgitimRef().getEgitimtanimRef().getRID()+" AND o.egitimUcretTipi="+ 
								EgitimUcretTipi._SEMINERUCRETI.getCode())>0){
							EgitimUcreti egitimUcreti= (EgitimUcreti) getDBOperator().load(ApplicationDescriptor._EGITIMUCRETI_MODEL,"o.egitimtanimRef.rID=" + 
									this.egitimkatilimci.getEgitimRef().getEgitimtanimRef().getRID()+" AND o.egitimUcretTipi="+ 
									EgitimUcretTipi._SEMINERUCRETI.getCode(),"").get(0);
							borc.setMiktar(egitimUcreti.getUcret());
							borc.setKisiRef(this.egitimkatilimci.getKisiRef());
							borc.setEgitimRef(this.egitimkatilimci.getEgitimRef());
							borc.setOdenen(BigDecimal.valueOf(0));
							borc.setBorcdurum(BorcDurum._BORCLU);
							if (getDBOperator().recordCount(ApplicationDescriptor._ODEMETIP_MODEL,"o.kod='EGTM'")>0){
								Odemetip odemetip = (Odemetip) getDBOperator().load(ApplicationDescriptor._ODEMETIP_MODEL,"o.kod='EGTM'","").get(0);
								borc.setOdemetipRef(odemetip);
							}else {
								borc.setOdemetipRef(null);
							}
							borc.setTarih(new Date());
							getDBOperator().insert(borc);
						}
					}else{
						super.save();
					}
				}else {
					createGenericMessage("Kişinin lisans bilgileri uymadığı için ekleme yapamazsınız!", FacesMessage.SEVERITY_INFO);
				}
			} catch (DBException e1) {
				e1.printStackTrace();
			}
			
		}else{
			try {
				addbuttondisable=true;
				getDBOperator().update(egitimkatilimci);
			} catch (DBException e) {
				e.printStackTrace();
			}
//			createGenericMessageKeyUtil.getMessageValue("egitim.katilimciFazla"),FacesMessage.SEVERITY_ERROR);
		}
	}
	public int getEgitimKatilimciCount(){
		List<BaseEntity> list = this.getList();
		int toplamKatilimci = 0;
		for (BaseEntity b : list) {
			toplamKatilimci++;
			b.getRID();
		}
		return toplamKatilimci;
	}

	public String egitimdegerlendirme() {
		if (!setSelected()) {
			return "";
		}
		getSessionUser().setLegalAccess(1);
		putObjectToSessionFilter(ApplicationDescriptor._EGITIMKATILIMCI_MODEL,	getEgitimkatilimci());
		return ManagedBeanLocator.locateMenuController().gotoPage("menu_Egitimdegerlendirme");
	}
	
	private Boolean egitimNotuPanelAc=false;
	
	public Boolean getEgitimNotuPanelAc() {
		return egitimNotuPanelAc;
	}

	public void setEgitimNotuPanelAc(Boolean egitimNotuPanelAc) {
		this.egitimNotuPanelAc = egitimNotuPanelAc;
	}
	private List<BaseEntity> list2=new ArrayList<BaseEntity>();
	
	public List<BaseEntity> getList2() {
		logYaz("List size  "+list2.size());
		return list2;
	}

	public void setList2(List<BaseEntity> list2) {
		this.list2 = list2;
	}
	private Boolean addbuttondisable=true;
	
	public Boolean getAddbuttondisable() {
		return addbuttondisable;
	}

	public void setAddbuttondisable(Boolean addbuttondisable) {
		this.addbuttondisable = addbuttondisable;
	}

	@SuppressWarnings("unchecked")
	public void katilimNotGuncelle() {
		setEgitimNotuPanelAc(true);		
		try {
			if (getDBOperator().recordCount(ApplicationDescriptor._EGITIMKATILIMCI_MODEL,"o.egitimRef.rID=" +
					((Egitim)getMasterEntity()).getRID())>0){
				List<BaseEntity> itemList = new ArrayList<BaseEntity>();   
				itemList=getDBOperator().load(ApplicationDescriptor._EGITIMKATILIMCI_MODEL,
						"o.egitimRef.rID=" +((Egitim)getMasterEntity()).getRID(),"o.rID");
				setList2(itemList);
				setList(itemList);
				this.queryAction();
			}
		} catch (DBException e) {
			logYaz("EgitimkatilimciController @katilimNotGuncelle = " + e.getMessage());
			e.printStackTrace();
		}
	}
	
	public void notVer(){
		try {
			if (!getList().isEmpty()){
				for (BaseEntity base : getList()){
					getDBOperator().update(base);
				}
			}
		} catch (DBException e) {
			logYaz("EgitimKatilimciController @notVer =" + e.getMessage());
			e.printStackTrace();
		}
		setEgitimNotuPanelAc(false);
		this.queryAction();
		
	}
	
	public void dialogKapat(){
		setEgitimNotuPanelAc(false);
		this.queryAction();
	}
	
	@Override
	public void delete() {
		try {
			if (getDBOperator().recordCount(ApplicationDescriptor._BORC_MODEL,"o.kisiRef.rID= "
				+getEgitimkatilimci().getKisiRef().getRID() + " AND o.egitimRef.rID=" + 
				+getEgitimkatilimci().getEgitimRef().getRID())>0){
				@SuppressWarnings("unchecked")
				List <Borc> borcList=getDBOperator().load(ApplicationDescriptor._BORC_MODEL,"o.kisiRef.rID= "
					+getEgitimkatilimci().getKisiRef().getRID() + " AND o.egitimRef.rID=" + 
					+getEgitimkatilimci().getEgitimRef().getRID(),"");
				for (Borc borc:borcList){
					getDBOperator().delete(borc);
				}
			}
		} catch (DBException e) {
			e.printStackTrace();
		}
		super.delete();
	}
} // class 
