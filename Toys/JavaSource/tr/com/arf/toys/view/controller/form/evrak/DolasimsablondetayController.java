package tr.com.arf.toys.view.controller.form.evrak; 
  
 
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.framework.utility.tool.KeyUtil;
import tr.com.arf.toys.db.filter.evrak.DolasimsablondetayFilter;
import tr.com.arf.toys.db.model.evrak.Dolasimsablon;
import tr.com.arf.toys.db.model.evrak.Dolasimsablondetay;
import tr.com.arf.toys.service.application.ApplicationDescriptor;
import tr.com.arf.toys.view.controller.form._base.ToysBaseFilteredController;
  
  
@ManagedBean 
@ViewScoped 
public class DolasimsablondetayController extends ToysBaseFilteredController {  
  
  private static final long serialVersionUID = 6531152165884980748L; 
  
	private Dolasimsablondetay dolasimsablondetay;  
	private DolasimsablondetayFilter dolasimsablondetayFilter = new DolasimsablondetayFilter();  
  
	public DolasimsablondetayController() { 
		super(Dolasimsablondetay.class);  
		setDefaultValues(false);  
		setOrderField("sirano");  
		setLoggable(false);  
		setAutoCompleteSearchColumns(new String[]{"dolasimsablonRef"}); 
		setTable(null); 
		if(getMasterEntity() != null){ 
			setMasterObjectName(ApplicationDescriptor._DOLASIMSABLON_MODEL);
			setMasterOutcome(ApplicationDescriptor._DOLASIMSABLON_MODEL);
			getDolasimsablondetayFilter().setDolasimsablonRef(getMasterEntity());
			getDolasimsablondetayFilter().createQueryCriterias();   
		} 
		queryAction(); 
	} 
 
	public Dolasimsablon getMasterEntity(){
		if(getDolasimsablondetayFilter().getDolasimsablonRef() != null){
			return getDolasimsablondetayFilter().getDolasimsablonRef();
		} else {
			return (Dolasimsablon) getObjectFromSessionFilter(ApplicationDescriptor._DOLASIMSABLON_MODEL);
		}			
	} 	
	
	public Dolasimsablondetay getDolasimsablondetay() { 
		dolasimsablondetay = (Dolasimsablondetay) getEntity(); 
		return dolasimsablondetay; 
	} 
	
	public void setDolasimsablondetay(Dolasimsablondetay dolasimsablondetay) { 
		this.dolasimsablondetay = dolasimsablondetay; 
	} 
	
	public DolasimsablondetayFilter getDolasimsablondetayFilter() { 
		return dolasimsablondetayFilter; 
	} 
	 
	public void setDolasimsablondetayFilter(DolasimsablondetayFilter dolasimsablondetayFilter) {  
		this.dolasimsablondetayFilter = dolasimsablondetayFilter;  
	}  
	 
	@Override  
	public BaseFilter getFilter() {  
		return getDolasimsablondetayFilter();  
	}  
 
	@Override	
	public void insert() {	
		super.insert();	
		getDolasimsablondetay().setDolasimsablonRef(getDolasimsablondetayFilter().getDolasimsablonRef());	
		try {
			getDolasimsablondetay().setSirano(getDBOperator().recordCount(getModelName(), "o.dolasimsablonRef.rID=" + getMasterEntity().getRID()) + 1);
		} catch (Exception e) {
			getDolasimsablondetay().setSirano(1);
			logYaz("Exception @" + getModelName() + "Controller :", e);
		} 
	}	
	
	@Override 
	public void save() {
		if(getDolasimsablondetay().getBirimRef() == null && getDolasimsablondetay().getPersonelRef() == null){
			createCustomMessage(KeyUtil.getMessageValue("dolasimsablondetay.personelBirimSec"), FacesMessage.SEVERITY_ERROR, "birimRefDropDown:birimRef");
			createCustomMessage(KeyUtil.getMessageValue("dolasimsablondetay.personelBirimSec"), FacesMessage.SEVERITY_ERROR, "personelRefDropDown:personelRef");
			return;
		}
		super.save();
	}
	
	@Override 
	public void delete() {
		int siraNo = getDolasimsablondetay().getSirano();
		super.justDeleteGivenObject(getDolasimsablondetay(), isLoggable());
		try {
			if(getDBOperator().recordCount(getModelName(), "o.dolasimsablonRef.rID=" + getMasterEntity().getRID() + " AND o.sirano >" + siraNo) > 0){
				String query = "UPDATE " + getModelName() + " o SET o.sirano = (o.sirano -1) WHERE o.dolasimsablonRef.rID=" + getMasterEntity().getRID() + " AND sirano >" + siraNo;
				getDBOperator().executeQuery(query);
			}
		} catch (Exception e) {
			logYaz("ERROR @delete of DolasimsablondetayController : " + e.getMessage());
			logYaz("Exception @" + getModelName() + "Controller :", e);
		}
		queryAction();
	}
} // class 
