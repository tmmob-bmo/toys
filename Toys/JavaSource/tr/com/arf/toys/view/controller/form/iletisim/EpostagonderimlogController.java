package tr.com.arf.toys.view.controller.form.iletisim; 
  
 
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.toys.db.filter.iletisim.EpostagonderimlogFilter;
import tr.com.arf.toys.db.model.iletisim.Epostagonderimlog;
import tr.com.arf.toys.db.model.iletisim.Gonderim;
import tr.com.arf.toys.service.application.ApplicationDescriptor;
import tr.com.arf.toys.view.controller.form._base.ToysBaseFilteredController;
  
  
@ManagedBean 
@ViewScoped 
public class EpostagonderimlogController extends ToysBaseFilteredController {  
  
  private static final long serialVersionUID = 6531152165884980748L; 
  
	private Epostagonderimlog epostagonderimlog;  
	private EpostagonderimlogFilter epostagonderimlogFilter = new EpostagonderimlogFilter();  
  
	public EpostagonderimlogController() { 
		super(Epostagonderimlog.class);  
		setDefaultValues(false);  
		setOrderField("aciklama");  
		setLoggable(false);  
		setAutoCompleteSearchColumns(new String[]{"aciklama"}); 
		setTable(null); 
		if(getMasterEntity() != null){ 
			setMasterObjectName(ApplicationDescriptor._GONDERIM_MODEL);
			setMasterOutcome(ApplicationDescriptor._GONDERIM_MODEL);
			getEpostagonderimlogFilter().setGonderimRef(getMasterEntity());
			getEpostagonderimlogFilter().createQueryCriterias();   
		} 
		queryAction(); 
	} 
 
	public Gonderim getMasterEntity(){
		if(getEpostagonderimlogFilter().getGonderimRef() != null){
			return getEpostagonderimlogFilter().getGonderimRef();
		} else {
			return (Gonderim) getObjectFromSessionFilter(ApplicationDescriptor._GONDERIM_MODEL);
		}			
	} 
 
	
	
	public Epostagonderimlog getEpostagonderimlog() { 
		epostagonderimlog = (Epostagonderimlog) getEntity(); 
		return epostagonderimlog; 
	} 
	
	public void setEpostagonderimlog(Epostagonderimlog epostagonderimlog) { 
		this.epostagonderimlog = epostagonderimlog; 
	} 
	
	public EpostagonderimlogFilter getEpostagonderimlogFilter() { 
		return epostagonderimlogFilter; 
	} 
	 
	public void setEpostagonderimlogFilter(EpostagonderimlogFilter epostagonderimlogFilter) {  
		this.epostagonderimlogFilter = epostagonderimlogFilter;  
	}  
	
	private Boolean onizlemeDialog=false;
	
	public Boolean getOnizlemeDialog() {
		return onizlemeDialog;
	}

	public void setOnizlemeDialog(Boolean onizlemeDialog) {
		this.onizlemeDialog = onizlemeDialog;
	}

	@Override
	public void setSelectedRID(String selectedRID) {
		super.setSelectedRID(selectedRID);
		setOnizlemeDialog(true);
	}
	public void onizlemedialogkapat(){
		setOnizlemeDialog(false);
	}
	 
	@Override  
	public BaseFilter getFilter() {  
		return getEpostagonderimlogFilter();  
	}  
	
	@Override 
	public void resetFilter() {
		super.resetFilterOnly();
		if(getMasterEntity() != null){
			getEpostagonderimlogFilter().setGonderimRef(getMasterEntity()); 
		}
		queryAction();
	}
 
	@Override	
	public void insert() {	
		super.insert();	
		getEpostagonderimlog().setGonderimRef(getEpostagonderimlogFilter().getGonderimRef());	
	}	
	
} // class 
