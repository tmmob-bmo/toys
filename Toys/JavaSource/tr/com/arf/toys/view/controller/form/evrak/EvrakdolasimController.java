package tr.com.arf.toys.view.controller.form.evrak; 
  
 
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.toys.db.filter.evrak.EvrakdolasimFilter;
import tr.com.arf.toys.db.model.evrak.Evrak;
import tr.com.arf.toys.db.model.evrak.Evrakdolasim;
import tr.com.arf.toys.service.application.ApplicationDescriptor;
import tr.com.arf.toys.view.controller.form._base.ToysBaseFilteredController;
  
  
@ManagedBean 
@ViewScoped 
public class EvrakdolasimController extends ToysBaseFilteredController {  
  
  private static final long serialVersionUID = 6531152165884980748L; 
  
	private Evrakdolasim evrakdolasim;  
	private EvrakdolasimFilter evrakdolasimFilter = new EvrakdolasimFilter();  
  
	public EvrakdolasimController() { 
		super(Evrakdolasim.class);  
		setDefaultValues(false);  
		setOrderField("rID ASC");  
		setLoggable(false);  
		setAutoCompleteSearchColumns(new String[]{"evrakRef"}); 
		setTable(null); 
		if(getMasterEntity() != null){ 
			setMasterObjectName(ApplicationDescriptor._EVRAK_MODEL);
			setMasterOutcome(ApplicationDescriptor._EVRAK_MODEL);
			getEvrakdolasimFilter().setEvrakRef(getMasterEntity());
			getEvrakdolasimFilter().createQueryCriterias();   
		} 
		queryAction(); 
	} 
 
	public Evrak getMasterEntity(){
		if(getEvrakdolasimFilter().getEvrakRef() != null){
			return getEvrakdolasimFilter().getEvrakRef();
		} else {
			return (Evrak) getObjectFromSessionFilter(ApplicationDescriptor._EVRAK_MODEL);
		}			
	} 
	
	public Evrakdolasim getEvrakdolasim() { 
		evrakdolasim = (Evrakdolasim) getEntity(); 
		return evrakdolasim; 
	} 
	
	public void setEvrakdolasim(Evrakdolasim evrakdolasim) { 
		this.evrakdolasim = evrakdolasim; 
	} 
	
	public EvrakdolasimFilter getEvrakdolasimFilter() { 
		return evrakdolasimFilter; 
	} 
	 
	public void setEvrakdolasimFilter(EvrakdolasimFilter evrakdolasimFilter) {  
		this.evrakdolasimFilter = evrakdolasimFilter;  
	}  
	 
	@Override  
	public BaseFilter getFilter() {  
		return getEvrakdolasimFilter();  
	}  
	 
	@Override	
	public void insert() {	
		super.insert();	
		getEvrakdolasim().setEvrakRef(getEvrakdolasimFilter().getEvrakRef());	
	}
	
} // class 
