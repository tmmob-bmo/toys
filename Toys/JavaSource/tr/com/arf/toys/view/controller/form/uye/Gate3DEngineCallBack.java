package tr.com.arf.toys.view.controller.form.uye;

import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.faces.context.FacesContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import sun.misc.BASE64Encoder;
import tr.com.arf.toys.utility.application.ApplicationConstant;
import tr.com.arf.toys.utility.logUtils.LogService;

/**
 * Servlet implementation class Gate3DEngineCallBack
 */
public class Gate3DEngineCallBack extends HttpServlet {
    protected static Logger logger = Logger.getLogger(Gate3DEngineCallBack.class);

	private static final long serialVersionUID = 1L;
	protected BASE64Encoder B64ENC = new BASE64Encoder();
    private String storeKey;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Gate3DEngineCallBack() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doProcess(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doProcess(request, response);
	}

	private void doProcess(HttpServletRequest request, HttpServletResponse response) {
		logger.debug("Validation : " + this.validateHash(request));
	}

    public void createStoreKey() {
        try {
            if (FacesContext.getCurrentInstance().getExternalContext().getInitParameter("storeKey") != null) {
                setStoreKey(FacesContext.getCurrentInstance().getExternalContext().getInitParameter("storeKey"));
            } else {
                setStoreKey(ApplicationConstant.storeKey);
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            setStoreKey(ApplicationConstant.storeKey);
        }
    }

	@SuppressWarnings("unused")
	public boolean validateHash(HttpServletRequest request) {
		String responseHashparams = request.getParameter("hashparams");
		String responseHashparamsval = request.getParameter("hashparamsval");
		String responseHash = request.getParameter("hash");
		String storekey = getStoreKey();

		// logger.debug("RESPONSE HASHPARAMS :[" + responseHashparams + "]");
		// logger.debug("RESPONSE HASHPARAMSVAL :[" + responseHashparamsval + "]");
		// logger.debug("RESPONSE HASH :[" + responseHash + "]");

		if (responseHashparams != null && !"".equals(responseHashparams)) {
			String digestData = "";
			String paramList[] = responseHashparams.split(":");
			// logger.debug("RESPONSE ParamList:");
			for (String param : paramList) {
				logger.debug("[" + param + "] value : [" + request.getParameter(param) + "]");
				digestData += request.getParameter(param) == null ? "" : request.getParameter(param);
			}
			// logger.debug("CALCULATED digestData = [" + digestData + "]");
			String hashCalculated = macsha1(digestData + storekey);
			// logger.debug("CALCULATED HASH : [" + hashCalculated + "]");

			if (responseHash.equals(hashCalculated)) {
				// logger.debug("!!!!!HASH VALID!!!!!");
				return true;
			} else {
				LogService.logYaz("!!!!!HASH INVALID!!!!! \n RESPONSE HASH : [" + responseHash + "] \\ n CALCULATED HASH : [" + hashCalculated + "]");
			}
		} else {
			LogService.logYaz("!!!!!FATAL ERROR/INTEGRATION ERROR!!!!! \n Response :" + request.getParameter("response") + "\nprocreturncode :" + request.getParameter("procreturncode") + "\nmderrormessage :" + request.getParameter("mderrormessage"));
		}
		return false;

	}

	public String macsha1(String hashdata) {
		String returnText = "";
		MessageDigest sha1 = null;
		try {
			sha1 = MessageDigest.getInstance("SHA-1");
			sha1.update(hashdata.getBytes());
			returnText = B64ENC.encode(sha1.digest());
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return returnText;
	}

    public String getStoreKey() {
        if (storeKey == null || storeKey.trim().length() == 0) {
            createStoreKey();
        }
        return storeKey;
    }

    public void setStoreKey(String storeKey) {
        this.storeKey = storeKey;
    }

}
