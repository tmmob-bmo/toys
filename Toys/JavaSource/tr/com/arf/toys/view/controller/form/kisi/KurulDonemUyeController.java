package tr.com.arf.toys.view.controller.form.kisi;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.model.SelectItem;

import tr.com.arf.framework.db.enumerated._common.EvetHayir;
import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.framework.utility.exception.data.DBException;
import tr.com.arf.toys.db.enumerated.kisi.GorevTuru;
import tr.com.arf.toys.db.enumerated.kisi.KurulTuru;
import tr.com.arf.toys.db.enumerated.kisi.KurulUyelikDurumu;
import tr.com.arf.toys.db.enumerated.kisi.UyeTuru;
import tr.com.arf.toys.db.enumerated.system.BirimTip;
import tr.com.arf.toys.db.enumerated.uye.UyeTip;
import tr.com.arf.toys.db.filter.kisi.KurulDonemUyeFilter;
import tr.com.arf.toys.db.model.kisi.KurulDonem;
import tr.com.arf.toys.db.model.kisi.KurulDonemUye;
import tr.com.arf.toys.db.model.kisi.KurulDonemUyeDurum;
import tr.com.arf.toys.db.model.uye.Uye;
import tr.com.arf.toys.service.application.ApplicationDescriptor;
import tr.com.arf.toys.view.controller.form._base.ToysBaseFilteredController;

@ManagedBean
@ViewScoped
public class KurulDonemUyeController extends ToysBaseFilteredController {

	private static final long serialVersionUID = 6531152165884980748L;

	private KurulDonemUye kurulDonemUye;
	private KurulDonemUyeFilter kurulDonemUyeFilter = new KurulDonemUyeFilter();
	private String kurulDonemId;
	private boolean odakoordinasyonkuruluPanelRendered = false;
	private boolean subekoordinasyonkuruluPanelRendered = false;

	public KurulDonemUyeController() {
		super(KurulDonemUye.class);
		setDefaultValues(false);
		setTable(null);
		if (getMasterEntity() != null) {
			if (getMasterEntity().getClass() == KurulDonem.class) {
				setMasterObjectName(ApplicationDescriptor._KURULDONEM_MODEL);
				setMasterOutcome(ApplicationDescriptor._KURULDONEM_MODEL);
				getKurulDonemUyeFilter().setKurulDonemRef(getMasterEntity());
			} else {
				setMasterOutcome(null);
			}

			getKurulDonemUyeFilter().createQueryCriterias();
		}
		queryAction();
	}

	public KurulDonemUyeController(Object object) {
		super(KurulDonemUye.class);
	}

	public KurulDonem getMasterEntity() {
		if (getObjectFromSessionFilter(ApplicationDescriptor._KURULDONEM_MODEL) != null) {
			return (KurulDonem) getObjectFromSessionFilter(ApplicationDescriptor._KURULDONEM_MODEL);
		} else {
			return null;
		}
	}

	public KurulDonemUye getKurulDonemUye() {
		kurulDonemUye = (KurulDonemUye) getEntity();
		return kurulDonemUye;
	}

	public void setKurulDonemUye(KurulDonemUye kurulDonemUye) {
		this.kurulDonemUye = kurulDonemUye;
	}

	public KurulDonemUyeFilter getKurulDonemUyeFilter() {
		return kurulDonemUyeFilter;
	}

	public void setKurulDonemUyeFilter(KurulDonemUyeFilter kurulDonemUyeFilter) {
		this.kurulDonemUyeFilter = kurulDonemUyeFilter;
	}

	public String getKurulDonemId() {
		return kurulDonemId;
	}

	public void setKurulDonemId(String kurulDonemId) {
		this.kurulDonemId = kurulDonemId;
	}

	public boolean isOdakoordinasyonkuruluPanelRendered() {
		return odakoordinasyonkuruluPanelRendered;
	}

	public void setOdakoordinasyonkuruluPanelRendered(boolean odakoordinasyonkuruluPanelRendered) {
		this.odakoordinasyonkuruluPanelRendered = odakoordinasyonkuruluPanelRendered;
	}

	public boolean isSubekoordinasyonkuruluPanelRendered() {
		return subekoordinasyonkuruluPanelRendered;
	}

	public void setSubekoordinasyonkuruluPanelRendered(boolean subekoordinasyonkuruluPanelRendered) {
		this.subekoordinasyonkuruluPanelRendered = subekoordinasyonkuruluPanelRendered;
	}

	@Override
	public BaseFilter getFilter() {
		return getKurulDonemUyeFilter();
	}

	@Override
	public void insert() {
		super.insert();
		getKurulDonemUye().setKurulDonemRef(getMasterEntity());
		getKurulDonemUye().setKurulUyelikDurumu(KurulUyelikDurumu._NULLDEGERI);
		getKurulDonemUye().setGorevTuru(GorevTuru._UYE);
		UyeTuruItems = new SelectItem[UyeTuru.values().length - 1];
		for (int x = 0; x < UyeTuru.values().length - 1; x++) {
			UyeTuruItems[x] = new SelectItem(UyeTuru.values()[x + 1], UyeTuru.values()[x + 1].getLabel());
		}
		setUyeTuruItems(UyeTuruItems);
		// super.fillDualListFor(Birim.class.getSimpleName(), "o.durum=" +
		// BirimDurum._ACIK.getCode() + " AND o.erisimKodu LIKE '"
		// + ((KurulDonem)
		// getMasterEntity()).getKurulRef().getBirimRef().getErisimKodu() +"%'",
		// "ad");
	}

	@SuppressWarnings("unchecked")
	public SelectItem[] getSelectItemListForTurkUye() {
		List<Uye> entityList = getDBOperator().load(Uye.class.getSimpleName(), "o.uyetip <>" + UyeTip._YABANCILAR.getCode(), "rID");
		if (entityList != null) {
			SelectItem[] selectItemList = new SelectItem[entityList.size() + 1];
			selectItemList[0] = new SelectItem(null, "");
			int i = 1;
			for (Uye entity : entityList) {
				selectItemList[i++] = new SelectItem(entity, entity.getUIString());
			}
			return selectItemList;
		}
		return new SelectItem[0];
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public void save() {
		if (kurulDonemUye.getRID() != null) {
			if (kurulDonemUye.getGorevTuru() == GorevTuru._BASKAN) {

				List<KurulDonemUye> kurulDonemUyeListesi = getDBOperator().load(
						KurulDonemUye.class.getSimpleName(),
						"o.rID<> " + kurulDonemUye.getRID() + " AND o.kurulDonemRef.rID=" + kurulDonemUye.getKurulDonemRef().getRID() + " AND  o.gorevTuru="
								+ GorevTuru._BASKAN.getCode() + " AND o.aktif=" + EvetHayir._EVET.getCode(), "");
				if (!isEmpty(kurulDonemUyeListesi)) {
					createGenericMessage("Birden Fazla Başkan Üye Yapamazsınız ! ", FacesMessage.SEVERITY_ERROR);
					return;
				}
			} else if (kurulDonemUye.getGorevTuru() == GorevTuru._BASKANYRD) {
				List<KurulDonemUye> kurulDonemUyeListesi = getDBOperator().load(
						KurulDonemUye.class.getSimpleName(),
						"o.rID<>" + kurulDonemUye.getRID() + " AND o.kurulDonemRef.rID=" + kurulDonemUye.getKurulDonemRef().getRID() + " AND  o.gorevTuru="
								+ GorevTuru._BASKANYRD.getCode() + " AND o.aktif=" + EvetHayir._EVET.getCode(), "");
				if (!isEmpty(kurulDonemUyeListesi)) {
					createGenericMessage("Birden Fazla Başkan Yardımcısı Üye Yapamazsınız ! ", FacesMessage.SEVERITY_ERROR);
					return;
				}
			}
		} else {
			if (kurulDonemUye.getGorevTuru() == GorevTuru._BASKAN) {
				List<KurulDonemUye> kurulDonemUyeListesi = getDBOperator().load(
						KurulDonemUye.class.getSimpleName(),
						"o.kurulDonemRef.rID=" + kurulDonemUye.getKurulDonemRef().getRID() + " AND  o.gorevTuru=" + GorevTuru._BASKAN.getCode() + " AND o.aktif="
								+ EvetHayir._EVET.getCode(), "");
				if (!isEmpty(kurulDonemUyeListesi)) {
					createGenericMessage("Birden Fazla Başkan Üye Yapamazsınız ! ", FacesMessage.SEVERITY_ERROR);
					return;
				}
			} else if (kurulDonemUye.getGorevTuru() == GorevTuru._BASKANYRD) {
				List<KurulDonemUye> kurulDonemUyeListesi1 = getDBOperator().load(
						KurulDonemUye.class.getSimpleName(),
						"o.kurulDonemRef.rID=" + kurulDonemUye.getKurulDonemRef().getRID() + " AND  o.gorevTuru=" + GorevTuru._BASKANYRD.getCode() + " AND o.aktif="
								+ EvetHayir._EVET.getCode(), "");
				if (!isEmpty(kurulDonemUyeListesi1)) {
					createGenericMessage("Birden Fazla Başkan Yardımcısı Üye Yapamazsınız ! ", FacesMessage.SEVERITY_ERROR);
					return;
				}
			}
		}
		super.justSave();
		if (kurulDonemUye.getKurulUyelikDurumu() == KurulUyelikDurumu._UYELIGINDUSMESI || kurulDonemUye.getKurulUyelikDurumu() == KurulUyelikDurumu._UYELIKTENISTIFA) {
			kurulDonemUye.setAktif(EvetHayir._HAYIR);
		} else {
			kurulDonemUye.setAktif(EvetHayir._EVET);
		}
		try {
			getDBOperator().update(kurulDonemUye);
			if (getKurulDonemUye() != null) {
				KurulDonemUyeDurum kurulUyeDurum = new KurulDonemUyeDurum();
				kurulUyeDurum.setUyeRef(getKurulDonemUye().getUyeRef());
				kurulUyeDurum.setKurulDonemRef(getKurulDonemUye().getKurulDonemRef());
				if (getKurulDonemUye().getDurumtarihi() != null) {
					kurulUyeDurum.setDurumtarihi(getKurulDonemUye().getDurumtarihi());
				}
				if (getKurulDonemUye().getKurulUyelikDurumu() != null) {
					kurulUyeDurum.setKurulUyelikDurumu(getKurulDonemUye().getKurulUyelikDurumu());
				}
				kurulUyeDurum.setGorevTuru(getKurulDonemUye().getGorevTuru());
				kurulUyeDurum.setAktif(getKurulDonemUye().getAktif());
				kurulUyeDurum.setUyeTuru(getKurulDonemUye().getUyeTuru());
				getDBOperator().insert(kurulUyeDurum);
			}
		} catch (DBException e) {
			// TODO Auto-generated catch block
			logYaz("Exception @" + getModelName() + "Controller :", e);
		}

		queryAction();
	}

	private UyeTuru uyeTuru;

	public UyeTuru getUyeTuru() {
		return uyeTuru;
	}

	public void setUyeTuru(UyeTuru uyeTuru) {
		this.uyeTuru = uyeTuru;
	}

	private SelectItem[] UyeTuruItems;

	public SelectItem[] getUyeTuruItems() {
		return UyeTuruItems;
	}

	public void setUyeTuruItems(SelectItem[] uyeTuruItems) {
		UyeTuruItems = uyeTuruItems;
	}

	public void handleChangeForUyeTuru(AjaxBehaviorEvent event) {
		try {
			HtmlSelectOneMenu menu = (HtmlSelectOneMenu) event.getComponent();
			GorevTuru secilenKurulGorev = (GorevTuru) menu.getValue();
			if (secilenKurulGorev != GorevTuru._UYE) {
				UyeTuruItems = new SelectItem[UyeTuru.values().length - 2];
				for (int x = 0; x < UyeTuru.values().length - 2; x++) {
					UyeTuruItems[x] = new SelectItem(UyeTuru.values()[x + 1], UyeTuru.values()[x + 1].getLabel());
				}

			} else {
				UyeTuruItems = new SelectItem[UyeTuru.values().length - 1];
				for (int x = 0; x < UyeTuru.values().length - 1; x++) {
					UyeTuruItems[x] = new SelectItem(UyeTuru.values()[x + 1], UyeTuru.values()[x + 1].getLabel());
				}
			}
			setUyeTuruItems(UyeTuruItems);
		} catch (Exception e) {
			logYaz("Error @handleChange :" + e.getMessage());
		}

	}

	@Override
	public void update() {
		if (getKurulDonemUye().getGorevTuru() != GorevTuru._UYE) {
			UyeTuruItems = new SelectItem[UyeTuru.values().length - 2];
			for (int x = 0; x < UyeTuru.values().length - 2; x++) {
				UyeTuruItems[x] = new SelectItem(UyeTuru.values()[x + 1], UyeTuru.values()[x + 1].getLabel());
			}

		} else {
			UyeTuruItems = new SelectItem[UyeTuru.values().length - 1];
			for (int x = 0; x < UyeTuru.values().length - 1; x++) {
				UyeTuruItems[x] = new SelectItem(UyeTuru.values()[x + 1], UyeTuru.values()[x + 1].getLabel());
			}
		}
		setUyeTuruItems(UyeTuruItems);

		super.update();
	}

	private SelectItem[] selectKurulDonemListesi = new SelectItem[0];

	public SelectItem[] getSelectKurulDonemListesi() {
		return selectKurulDonemListesi;
	}

	public void setSelectKurulDonemListesi(SelectItem[] selectKurulDonemListesi) {
		this.selectKurulDonemListesi = selectKurulDonemListesi;
	}

	@SuppressWarnings("unchecked")
	public SelectItem[] getKurulDonemListesi() {
		try {
			if (getDBOperator().recordCount(KurulDonem.class.getSimpleName(),
					"o.donemRef.anadonemRef.aktif=" + EvetHayir._EVET.getCode() + " " + " AND ( o.kurulRef.kurulturu = " + KurulTuru._YONETIMKURULU.getCode()
							+ " AND ( o.birimRef.birimtip=" + BirimTip._ODA.getCode() + " OR o.birimRef.birimtip= " + BirimTip._SUBE.getCode() + " ) OR o.kurulRef.kurulturu= "
							+ KurulTuru._DENETLEMEKURULU.getCode() + " ) ") > 0) {
				List<KurulDonem> kurulDonemList = getDBOperator().load(KurulDonem.class.getSimpleName(),
						"o.donemRef.anadonemRef.aktif=" + EvetHayir._EVET.getCode() + " " + " AND ( o.kurulRef.kurulturu = " + KurulTuru._YONETIMKURULU.getCode()
								+ " AND ( o.birimRef.birimtip=" + BirimTip._ODA.getCode() + " OR o.birimRef.birimtip= " + BirimTip._SUBE.getCode() + " ) OR o.kurulRef.kurulturu= "
								+ KurulTuru._DENETLEMEKURULU.getCode() + " ) ", "");
				if (!isEmpty(kurulDonemList)) {
					selectKurulDonemListesi = new SelectItem[kurulDonemList.size() + 1];
					selectKurulDonemListesi[0] = new SelectItem(null, "Kayıt Seçiniz");
					int x = 0;
					for (x = 0; x < kurulDonemList.size(); x++) {
						selectKurulDonemListesi[x + 1] = new SelectItem(kurulDonemList.get(x).getRID(), kurulDonemList.get(x).getUIStringShort());
					}
				} else {
					selectKurulDonemListesi = new SelectItem[0];
				}
			}
		} catch (DBException e) {
			// TODO Auto-generated catch block
			logYaz("Exception @" + getModelName() + "Controller :", e);
		}
		return selectKurulDonemListesi;
	}
	
	@SuppressWarnings("unchecked")
	public SelectItem[] getSubeKoordinasyonKurulListesi() {
		try {
			if (getDBOperator().recordCount(KurulDonem.class.getSimpleName(),
					"o.donemRef.anadonemRef.aktif=" + EvetHayir._EVET.getCode() + " " + " AND o.kurulRef.kurulturu = " + KurulTuru._YONETIMKURULU.getCode()
							+ " AND o.birimRef.birimtip= " + BirimTip._SUBE.getCode()) > 0) {
				List<KurulDonem> kurulDonemList = getDBOperator().load(KurulDonem.class.getSimpleName(),
						"o.donemRef.anadonemRef.aktif=" + EvetHayir._EVET.getCode() + " " + " AND o.kurulRef.kurulturu = " + KurulTuru._YONETIMKURULU.getCode()
								+ " AND o.birimRef.birimtip= " + BirimTip._SUBE.getCode(), "");
				if (!isEmpty(kurulDonemList)) {
					selectKurulDonemListesi = new SelectItem[kurulDonemList.size() + 1];
					selectKurulDonemListesi[0] = new SelectItem(null, "Kayıt Seçiniz");
					int x = 0;
					for (x = 0; x < kurulDonemList.size(); x++) {
						selectKurulDonemListesi[x + 1] = new SelectItem(kurulDonemList.get(x).getRID(), kurulDonemList.get(x).getUIString());
					}
				} else {
					selectKurulDonemListesi = new SelectItem[0];
				}
			}
		} catch (DBException e) {
			// TODO Auto-generated catch block
			logYaz("Exception @" + getModelName() + "Controller :", e);
		}
		return selectKurulDonemListesi;
	}
	
	private Object[] odaKurulDonemList=new Object[0];
	
	public Object[] getOdaKurulDonemList() {
		return odaKurulDonemList;
	}

	public void setOdaKurulDonemList(Object[] odaKurulDonemList) {
		this.odaKurulDonemList = odaKurulDonemList;
	}

	@SuppressWarnings("unchecked")
	public void odakoordinasyonkurulu() {
		if (getOdaKurulDonemList()!=null){
			for (int i=0; i<getOdaKurulDonemList().length ; i++){
				List<KurulDonemUye> kurulDonemUyeListesi = getDBOperator().load(KurulDonemUye.class.getSimpleName(),
						"o.kurulDonemRef.rID=" + getOdaKurulDonemList()[i] + " AND o.uyeTuru=" + UyeTuru._ASIL.getCode() + " AND o.aktif=" + EvetHayir._EVET.getCode(), "");
				if (!isEmpty(kurulDonemUyeListesi)) {
					for (KurulDonemUye kurulDnmUye : kurulDonemUyeListesi) {
						try {
							if (getDBOperator().recordCount(KurulDonemUye.class.getSimpleName(),
									"o.uyeRef.rID=" + kurulDnmUye.getUyeRef().getRID() + " AND o.kurulDonemRef.rID=" + getMasterEntity().getRID()) <= 0) {
								kurulDonemUye = new KurulDonemUye();
								kurulDonemUye = kurulDnmUye;
								kurulDonemUye.setRID(null);
								kurulDonemUye.setUyeRef(kurulDnmUye.getUyeRef());
								kurulDonemUye.setKurulDonemRef(getMasterEntity());
								getDBOperator().insert(kurulDonemUye);
							}
						} catch (DBException e) {
							logYaz("Exception @" + getModelName() + "Controller :", e);
						}
					}
				}
			}
		}
		
		odakoordinasyonkuruluPanelKapat();
		queryAction();
	}
	
	private Object[] subeKurulDonemList=new Object[0];

	public Object[] getSubeKurulDonemList() {
		return subeKurulDonemList;
	}

	public void setSubeKurulDonemList(Object[] subeKurulDonemList) {
		this.subeKurulDonemList = subeKurulDonemList;
	}

	@SuppressWarnings("unchecked")
	public void subekoordinasyonkurulu() {
		if (getSubeKurulDonemList()!=null){
			for (int i=0; i<getSubeKurulDonemList().length ; i++){
				List<KurulDonemUye> kurulDonemUyeListesi = getDBOperator().load(KurulDonemUye.class.getSimpleName(),
						"o.kurulDonemRef.rID=" + getSubeKurulDonemList()[i] + " AND o.uyeTuru=" + UyeTuru._ASIL.getCode() + " AND o.aktif=" + EvetHayir._EVET.getCode(), "");
				if (!isEmpty(kurulDonemUyeListesi)) {
					for (KurulDonemUye kurulDnmUye : kurulDonemUyeListesi) {
						try {
							if (getDBOperator().recordCount(KurulDonemUye.class.getSimpleName(),
									"o.uyeRef.rID=" + kurulDnmUye.getUyeRef().getRID() + " AND o.kurulDonemRef.rID=" + getMasterEntity().getRID()) <= 0) {
								kurulDonemUye = new KurulDonemUye();
								kurulDonemUye = kurulDnmUye;
								kurulDonemUye.setRID(null);
								kurulDonemUye.setUyeRef(kurulDnmUye.getUyeRef());
								kurulDonemUye.setKurulDonemRef(getMasterEntity());
								getDBOperator().insert(kurulDonemUye);
							}
						} catch (DBException e) {
							logYaz("Exception @" + getModelName() + "Controller :", e);
						}
					}
				}
			}
		}
		
		subekoordinasyonkuruluPanelKapat();
//		odakoordinasyonkuruluPanelKapat();
		queryAction();
	}

	public void closeAllPanels() {
		setOdakoordinasyonkuruluPanelRendered(false);
		setSubekoordinasyonkuruluPanelRendered(false);
		setEditPanelRendered(false);
	}

	public void subekoordinasyonkuruluPanelAc() {
		closeAllPanels();
		setSubekoordinasyonkuruluPanelRendered(true);
	}

	public void subekoordinasyonkuruluPanelKapat() {
		setSubekoordinasyonkuruluPanelRendered(false);
	}

	public void odakoordinasyonkuruluPanelAc() {
		closeAllPanels();
		setOdakoordinasyonkuruluPanelRendered(true);
	}

	public void odakoordinasyonkuruluPanelKapat() {
		setOdakoordinasyonkuruluPanelRendered(false);
	}

	@Override
	@SuppressWarnings("rawtypes")
	public List completeMethod(String value) {
		return super.completeMethod(value, " o.birimRef.rID= " + getMasterEntity().getBirimRef().getRID() + " AND o.kisiRef.aktif=" + EvetHayir._EVET.getCode() 
				+ " AND o.rID NOT IN (SELECT m.uyeRef.rID FROM " + KurulDonemUye.class.getSimpleName() + " AS m WHERE m.kurulDonemRef.rID = " + getMasterEntity().getRID() + ")");
	}

} // class
