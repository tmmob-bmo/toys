package tr.com.arf.toys.view.controller.form.system; 
  
 
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.toys.db.filter.system.KullaniciraporFilter;
import tr.com.arf.toys.db.model.system.Kullanicirapor;
import tr.com.arf.toys.view.controller.form._base.ToysBaseFilteredController;
  
  
@ManagedBean 
@ViewScoped 
public class KullaniciraporController extends ToysBaseFilteredController {  
  
  private static final long serialVersionUID = 6531152165884980748L; 
  
	private Kullanicirapor kullanicirapor;  
	private KullaniciraporFilter kullaniciraporFilter = new KullaniciraporFilter();  
  
	public KullaniciraporController() { 
		super(Kullanicirapor.class);  
		setDefaultValues(false);  
		setOrderField("raporadi");  
		setLoggable(false);  
		setAutoCompleteSearchColumns(new String[]{"raporadi"}); 
		setTable(null); 
		queryAction(); 
	} 
	
	
	public Kullanicirapor getKullanicirapor() { 
		kullanicirapor = (Kullanicirapor) getEntity(); 
		return kullanicirapor; 
	} 
	
	public void setKullanicirapor(Kullanicirapor kullanicirapor) { 
		this.kullanicirapor = kullanicirapor; 
	} 
	
	public KullaniciraporFilter getKullaniciraporFilter() { 
		return kullaniciraporFilter; 
	} 
	 
	public void setKullaniciraporFilter(KullaniciraporFilter kullaniciraporFilter) {  
		this.kullaniciraporFilter = kullaniciraporFilter;  
	}  
	 
	@Override  
	public BaseFilter getFilter() {  
		return getKullaniciraporFilter();  
	}  
	
	
 
	
} // class 