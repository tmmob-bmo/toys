package tr.com.arf.toys.view.controller.form.uye; 
  
 
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.toys.db.filter.uye.UyeodemeFilter;
import tr.com.arf.toys.db.model.uye.Uyeaidat;
import tr.com.arf.toys.db.model.uye.Uyeodeme;
import tr.com.arf.toys.service.application.ApplicationDescriptor;
import tr.com.arf.toys.view.controller.form._base.ToysBaseFilteredController;
  
  
@ManagedBean 
@ViewScoped 
public class UyeodemeController extends ToysBaseFilteredController {  
  
  private static final long serialVersionUID = 6531152165884980748L; 
  
	private Uyeodeme uyeodeme;  
	private UyeodemeFilter uyeodemeFilter = new UyeodemeFilter();  
  
	public UyeodemeController() { 
		super(Uyeodeme.class);  
		setDefaultValues(false);  
		setOrderField("miktar");  
		setAutoCompleteSearchColumns(new String[]{"miktar"}); 
		setTable(null); 
		if(getMasterEntity() != null){ 
			setMasterObjectName(ApplicationDescriptor._UYEAIDAT_MODEL);
			setMasterOutcome(ApplicationDescriptor._UYEAIDAT_MODEL);
			getUyeodemeFilter().setUyeaidatRef(getMasterEntity());
			getUyeodemeFilter().createQueryCriterias();   
		} 
		queryAction(); 
	} 
 
	public Uyeaidat getMasterEntity(){
		if(getUyeodemeFilter().getUyeaidatRef() != null){
			return getUyeodemeFilter().getUyeaidatRef();
		} else {
			return (Uyeaidat) getObjectFromSessionFilter(ApplicationDescriptor._UYEAIDAT_MODEL);
		}			
	} 
	
	public Uyeodeme getUyeodeme() { 
		uyeodeme = (Uyeodeme) getEntity(); 
		return uyeodeme; 
	} 
	
	public void setUyeodeme(Uyeodeme uyeodeme) { 
		this.uyeodeme = uyeodeme; 
	} 
	
	public UyeodemeFilter getUyeodemeFilter() { 
		return uyeodemeFilter; 
	} 
	 
	public void setUyeodemeFilter(UyeodemeFilter uyeodemeFilter) {  
		this.uyeodemeFilter = uyeodemeFilter;  
	}  
	 
	@Override  
	public BaseFilter getFilter() {  
		return getUyeodemeFilter();  
	}  
	 
	@Override 
	public void resetFilter() {
		super.resetFilterOnly();
		if(getMasterEntity() != null){
			getUyeodemeFilter().setUyeaidatRef(getMasterEntity()); 
		}
		queryAction();
	}
 
	@Override	
	public void insert() {	
		super.insert();	
		getUyeodeme().setUyeaidatRef(getUyeodemeFilter().getUyeaidatRef());		
	}	
	
} // class 
