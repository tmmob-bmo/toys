package tr.com.arf.toys.view.controller.form.system;

import java.util.List;
import java.util.Locale;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.primefaces.context.RequestContext;

import tr.com.arf.framework.db.enumerated._common.EvetHayir;
import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.framework.utility.exception.data.DBException;
import tr.com.arf.framework.utility.security.ScryptPasswordHashing;
import tr.com.arf.framework.utility.tool.RandomObjectGenerator;
import tr.com.arf.toys.db.enumerated.iletisim.AdresTipi;
import tr.com.arf.toys.db.enumerated.iletisim.AdresTuru;
import tr.com.arf.toys.db.enumerated.iletisim.NetAdresTuru;
import tr.com.arf.toys.db.enumerated.iletisim.TelefonTuru;
import tr.com.arf.toys.db.filter.system.YpersonelFilter;
import tr.com.arf.toys.db.model.iletisim.Adres;
import tr.com.arf.toys.db.model.iletisim.Internetadres;
import tr.com.arf.toys.db.model.iletisim.Telefon;
import tr.com.arf.toys.db.model.kisi.Kisi;
import tr.com.arf.toys.db.model.system.Kullanici;
import tr.com.arf.toys.db.model.system.Ulke;
import tr.com.arf.toys.db.model.system.Ypersonel;
import tr.com.arf.toys.db.model.system.Ypersonelbirimproje;
import tr.com.arf.toys.service.application.ApplicationDescriptor;
import tr.com.arf.toys.service.application.ManagedBeanLocator;
import tr.com.arf.toys.utility.application.ApplicationConstant;
import tr.com.arf.toys.utility.tool.SendEmail;
import tr.com.arf.toys.view.controller.form._base.ToysBaseFilteredController;
import tr.com.arf.toys.view.controller.form.iletisim.AdresController;
import tr.com.arf.toys.view.controller.form.iletisim.InternetadresController;
import tr.com.arf.toys.view.controller.form.iletisim.TelefonController;

@ManagedBean
@ViewScoped
public class YpersonelController extends ToysBaseFilteredController {

	private static final long serialVersionUID = -7094010765608112567L;

	private Ypersonel personel;
	private YpersonelFilter personelFilter = new YpersonelFilter();

	private Ypersonel personelDetay;
	private String selectedTab;

	private Ypersonelbirimproje personelBirimProje;

	private String oldValueForPersonelBirimProje;
	private String oldValueForAdres;
	private String oldValueForInternetadres;
	private String oldValueForTelefon;

	private String detailEditType;

	private Adres adres = new Adres();
	private Telefon telefon = new Telefon();
	private Internetadres internetadres = new Internetadres();

	private YpersonelbirimprojeController personelBirimProjeController = new YpersonelbirimprojeController(null);
	private InternetadresController internetadresController = new InternetadresController(null);
	private AdresController adresController = new AdresController(null);
	private TelefonController telefonController = new TelefonController(null);

	protected YpersonelController() {
		super(YpersonelController.class);
		setDefaultValues(false);
		setOrderField("baslamatarih");
		setStaticWhereCondition(ApplicationConstant._personelStaticWhereCondition);
		setAutoCompleteSearchColumns(new String[] { "kisiRef.ad", "kisiRef.soyad" });
		setTable(null);
		setLoggable(true);
		setStaticWhereCondition(getSessionUser().createStaticWhereCondition("o."));
		if (getMasterEntity() != null && getMasterEntity().getRID() != null) {
			this.personelDetay = getMasterEntity();
			this.selectedTab = "personel";
			createPersonelDetay("personel");
		}
		queryAction();

	}

	public YpersonelController(Object object) {
		super(Ypersonel.class);
	}

	public Ypersonel getMasterEntity() {
		if (getPersonel() != null && getPersonel().getRID() != null) {
			return getPersonel();
		} else if (getObjectFromSessionFilter(getModelName()) != null) {
			return (Ypersonel) getObjectFromSessionFilter(getModelName());
		} else {
			return null;
		}
	}

	@Override
	public BaseFilter getFilter() {
		return getPersonelFilter();
	}

	public String personelBirimProje() {
		if (!setSelected()) {
			return "";
		}
		getSessionUser().setLegalAccess(1);
		putObjectToSessionFilter(ApplicationDescriptor._PERSONEL_MODEL, getPersonel());
		return ManagedBeanLocator.locateMenuController().gotoPage("menu_PersonelBirimProje");
	}

	public void personel() {
		setSelectedTab("personel");
	}

	public void createPersonelDetay(String tabSelected) {

		setSelectedTab(tabSelected);
		getSessionUser().setLegalAccess(1);
		if (tabSelected.equalsIgnoreCase("adres") || tabSelected.equalsIgnoreCase("telefon") || tabSelected.equalsIgnoreCase("internetadres")) {
			adresListesiGuncelle(getPersonelDetay().getKisiRef());
			telefonListesiGuncelle(getPersonelDetay().getKisiRef());
			internetadresListesiGuncelle(getPersonelDetay().getKisiRef());
		} else if (tabSelected.equalsIgnoreCase("personelBirimProje")) {
			personelBirimProjeGuncelle(getPersonelDetay());
		}
	}

	private void adresListesiGuncelle(Kisi refKisi) {
		this.adresController.setTable(null);
		this.adresController.getAdresFilter().setKisiRef(refKisi);
		this.adresController.getAdresFilter().createQueryCriterias();
		this.adresController.queryAction();
	}

	private void telefonListesiGuncelle(Kisi refKisi) {
		this.telefonController.setTable(null);
		this.telefonController.getTelefonFilter().setKisiRef(refKisi);
		this.telefonController.getTelefonFilter().createQueryCriterias();
		this.telefonController.queryAction();
	}

	private void internetadresListesiGuncelle(Kisi refKisi) {
		this.internetadresController.setTable(null);
		this.internetadresController.getInternetadresFilter().setKisiRef(refKisi);
		this.internetadresController.getInternetadresFilter().createQueryCriterias();
		this.internetadresController.queryAction();
	}

	private void personelBirimProjeGuncelle(Ypersonel ypersonel) {
		this.personelBirimProjeController.setTable(null);
		this.personelBirimProjeController.getPersonelBirimProjeFilter().setPersonelRef(ypersonel);
		this.personelBirimProjeController.getPersonelBirimProjeFilter().createQueryCriterias();
	}

	@SuppressWarnings("unused")
	private void YpersonelbirimprojeGuncelle(Ypersonel refPersonel) {
		this.personelBirimProjeController.setTable(null);
		this.personelBirimProjeController.getPersonelBirimProjeFilter().setPersonelRef(refPersonel);
		this.personelBirimProjeController.getPersonelBirimProjeFilter().createQueryCriterias();
	}

	public void saveDetails() throws Exception {
		RequestContext context = RequestContext.getCurrentInstance();
		if (this.detailEditType.equalsIgnoreCase("adres")) {
			if (this.adres.getAdrestipi() == AdresTipi._YURTDISIADRESI) {
				if (this.adres.getUlkeRef() == null || isEmpty(this.adres.getAcikAdres()) || this.adres.getSehirRef() == null) {
					createGenericMessage("Yurdışı adresleri için ülke, il ve açık adres alanları zorunludur", FacesMessage.SEVERITY_ERROR);
					return;
				}
			} else if (this.adres.getAdrestipi() != AdresTipi._YURTDISIADRESI) {
				if (isEmpty(this.adres.getAcikAdres()) || this.adres.getSehirRef() == null || this.adres.getIlceRef() == null) {
					createGenericMessage("Adres için ülke, il, ilçe, mahalle ve açık adres alanları zorunludur", FacesMessage.SEVERITY_ERROR);
					return;
				}
				if (this.adres.getUlkeRef() == null) {
					this.adres.setUlkeRef((Ulke) getDBOperator().find(Ulke.class.getSimpleName(), "o.ulkekod='9980'"));
				}
			}
			if (this.adres.getAdresturu() != AdresTuru._NULL) {
				this.adres.setKisiRef(getPersonelDetay().getKisiRef());
				if (this.adres.getVarsayilan() == EvetHayir._HAYIR) {
					if (getDBOperator().recordCount(Adres.class.getSimpleName(),
							"o.kisiRef = " + getPersonelDetay().getKisiRef().getRID() + " AND o.varsayilan = " + EvetHayir._EVET.getCode()) == 0) {
						this.adres.setVarsayilan(EvetHayir._EVET);
					}
				}
				super.justSave(this.adres, this.adresController.isLoggable(), this.oldValueForAdres);
				if (this.adres.getVarsayilan() == EvetHayir._EVET) {
					getDBOperator().executeQuery(
							"UPDATE " + Adres.class.getSimpleName() + " o set o.varsayilan = " + EvetHayir._HAYIR.getCode() + " " + "	WHERE o.kisiRef = "
									+ this.adres.getKisiRef().getRID() + " AND o.rID <> " + this.adres.getRID());
				}
				this.adres = new Adres();
				this.adresController.queryAction();
				updateDialogAndForm(context);
			}
		} else if (this.detailEditType.equalsIgnoreCase("telefon")) {
			if (!isEmpty(this.telefon.getTelefonno()) && this.telefon.getTelefonturu() != TelefonTuru._NULL) {
				this.telefon.setKisiRef(getPersonelDetay().getKisiRef());
				if (this.telefon.getTelefonturu() == TelefonTuru._FAKS) {
					this.telefon.setVarsayilan(EvetHayir._HAYIR);
				}
				if (this.telefon.getVarsayilan() == EvetHayir._HAYIR) {
					if (getDBOperator().recordCount(Telefon.class.getSimpleName(),
							"o.kisiRef = " + getPersonelDetay().getKisiRef().getRID() + " AND o.varsayilan = " + EvetHayir._EVET.getCode()) == 0) {
						this.telefon.setVarsayilan(EvetHayir._EVET);
					}
				}
				super.justSave(this.telefon, this.telefonController.isLoggable(), this.oldValueForTelefon);
				if (this.telefon.getVarsayilan() == EvetHayir._EVET) {
					getDBOperator().executeQuery(
							"UPDATE " + Telefon.class.getSimpleName() + " o set o.varsayilan = " + EvetHayir._HAYIR.getCode() + " " + "	WHERE o.kisiRef = "
									+ this.telefon.getKisiRef().getRID() + " AND o.rID <> " + this.telefon.getRID());
				}
				this.telefon = new Telefon();
				this.telefonController.queryAction();
				updateDialogAndForm(context);
			}
		} else if (this.detailEditType.equalsIgnoreCase("internetadres")) {
			if (!isEmpty(this.internetadres.getNetadresmetni()) && this.internetadres.getNetadresturu() != NetAdresTuru._NULL) {
				this.internetadres.setKisiRef(getPersonelDetay().getKisiRef());
				if (this.internetadres.getNetadresturu() != NetAdresTuru._EPOSTA) {
					this.internetadres.setVarsayilan(EvetHayir._HAYIR);
				}
				String mail = null;
				if (this.internetadres.getNetadresturu() == NetAdresTuru._WEBSAYFASI) {
					if (internetadres.getNetadresmetni() != null) {
						mail = internetadres.getNetadresmetni();
						if (mail.contains("www.") == false) {
							createCustomMessage("Geçersiz Web Sayfası !", FacesMessage.SEVERITY_ERROR, "detailEditForm:netadresmetniInputText:netadresmetni");
							return;
						}
					}
				} else {
					if (internetadres.getNetadresmetni() != null) {
						mail = internetadres.getNetadresmetni();
						if (mail.contains("@") == false) {
							createCustomMessage("Geçersiz Eposta Adresi !", FacesMessage.SEVERITY_ERROR, "detailEditForm:netadresmetniInputText:netadresmetni");
							return;
						}
					}
				}

				if (this.internetadres.getVarsayilan() == EvetHayir._HAYIR) {
					if (getDBOperator().recordCount(Internetadres.class.getSimpleName(),
							"o.kisiRef = " + getPersonelDetay().getKisiRef().getRID() + " AND o.varsayilan = " + EvetHayir._EVET.getCode()) == 0) {
						this.internetadres.setVarsayilan(EvetHayir._EVET);
					}
				}
				super.justSave(this.internetadres, this.internetadresController.isLoggable(), this.oldValueForInternetadres);
				if (this.internetadres.getVarsayilan() == EvetHayir._EVET) {
					getDBOperator().executeQuery(
							"UPDATE " + Internetadres.class.getSimpleName() + " o set o.varsayilan = " + EvetHayir._HAYIR.getCode() + " " + "	WHERE o.kisiRef = "
									+ this.internetadres.getKisiRef().getRID() + " AND o.rID <> " + this.internetadres.getRID());
				}
				this.internetadres = new Internetadres();
				this.internetadresController.queryAction();
				updateDialogAndForm(context);
			}
		} else if (this.detailEditType.equalsIgnoreCase("personelBirimProje")) {
			this.personelBirimProje.setPersonelRef(getPersonelDetay());
			super.justSave(this.personelBirimProje, this.personelBirimProjeController.isLoggable(), this.oldValueForPersonelBirimProje);
			this.personelBirimProje = new Ypersonelbirimproje();
			this.personelBirimProjeController.queryAction();
			updateDialogAndForm(context);
		}

		context.update("personelPanel");
		this.detailEditType = null;
		this.personelBirimProjeController.queryAction();
	}

	private void updateDialogAndForm(RequestContext context) {
		context.update("dialogUpdateBox");
		context.update("personelForm");
	}

	public void newDetail(String detailType) {
		detailType = detailType.toLowerCase(Locale.ENGLISH);
		this.detailEditType = detailType;
		RequestContext context = RequestContext.getCurrentInstance();
		context.update("dialogUpdateBox");
		if (detailType.equalsIgnoreCase("adres")) {
			this.adres = new Adres();
			this.adres.setVarsayilan(EvetHayir._EVET);
			this.adres.setAdresturu(AdresTuru._EVADRESI);
			this.oldValueForAdres = "";
		} else if (detailType.equalsIgnoreCase("telefon")) {
			this.telefon = new Telefon();
			this.telefon.setVarsayilan(EvetHayir._HAYIR);
			this.oldValueForTelefon = "";
		} else if (detailType.equalsIgnoreCase("internetadres")) {
			this.internetadres = new Internetadres();
			this.internetadres.setVarsayilan(EvetHayir._HAYIR);
			this.oldValueForInternetadres = "";
		} else if (detailType.equalsIgnoreCase("personelBirimProje")) {
			this.personelBirimProje = new Ypersonelbirimproje();
			this.oldValueForPersonelBirimProje = "";
		}
	}

	public void updateDetail(String detailType, Object entity) {
		detailType = detailType.toLowerCase(Locale.ENGLISH);
		this.detailEditType = detailType;
		RequestContext context = RequestContext.getCurrentInstance();
		context.update("dialogUpdateBox");

		if (detailType.equalsIgnoreCase("adres")) {
			this.adres = (Adres) entity;
			if (this.adres.getUlkeRef() != null) {
				changeIl(this.adres.getUlkeRef().getRID());
			}
			if (this.adres.getSehirRef() != null) {
				changeIlce(this.adres.getSehirRef().getRID());
			}
			this.oldValueForAdres = this.adres.getValue();
		} else if (detailType.equalsIgnoreCase("telefon")) {
			this.telefon = (Telefon) entity;
			this.oldValueForTelefon = this.telefon.getValue();
		} else if (detailType.equalsIgnoreCase("internetadres")) {
			this.internetadres = (Internetadres) entity;
			this.oldValueForInternetadres = this.internetadres.getValue();
		} else if (detailType.equalsIgnoreCase("personelBirimProje")) {
			this.personelBirimProje = (Ypersonelbirimproje) entity;
			this.oldValueForPersonelBirimProje = this.personelBirimProje.getValue();
		}
	}

	public void openDetail(Object entitySent) {
		if (entitySent != null) {
			putObjectToSessionFilter("logEntity", entitySent);
		}
	}

	public String getHeightOfEditDialog() {
		if (this.detailEditType == null) {
			return "400";
		} else if (this.detailEditType.equalsIgnoreCase("adres")) {
			return "600";
		} else if (this.detailEditType.equalsIgnoreCase("telefon")) {
			return "300";
		} else if (this.detailEditType.equalsIgnoreCase("internetadres")) {
			return "300";
		} else if (this.detailEditType.equalsIgnoreCase("personelBirimProje")) {
			return "400";
		} else if (this.detailEditType.equalsIgnoreCase("personel")) {
			return "200";
		} else {
			return "400";
		}
	}

	public void deleteDetail(String detailType, Object entity) throws Exception {
		this.detailEditType = null;
		detailType = detailType.toLowerCase(Locale.ENGLISH);

		if (detailType.equalsIgnoreCase("adres")) {
			this.adres = (Adres) entity;
			if (this.adres.getVarsayilan() == EvetHayir._EVET) {
				if (getDBOperator().recordCount(Adres.class.getSimpleName(), "o.rID <> " + this.adres.getRID() + " AND o.kisiRef.rID = " + this.adres.getKisiRef().getRID()) > 0) {
					Adres varsayilanYapilacakAdres = (Adres) getDBOperator().load(Adres.class.getSimpleName(),
							"o.rID <> " + this.adres.getRID() + " AND o.kisiRef.rID = " + this.adres.getKisiRef().getRID(), "o.rID DESC").get(0);
					varsayilanYapilacakAdres.setVarsayilan(EvetHayir._EVET);
					getDBOperator().update(varsayilanYapilacakAdres);
				}
			}
			super.justDelete(this.adres, this.adresController.isLoggable());
			this.adresController.queryAction();
		} else if (detailType.equalsIgnoreCase("telefon")) {
			this.telefon = (Telefon) entity;
			if (this.telefon.getVarsayilan() == EvetHayir._EVET) {
				if (getDBOperator().recordCount(Telefon.class.getSimpleName(), "o.rID <> " + this.telefon.getRID() + " AND o.kisiRef.rID = " + this.telefon.getKisiRef().getRID()) > 0) {
					Telefon varsayilanYapilacakTelefon = (Telefon) getDBOperator().load(Telefon.class.getSimpleName(),
							"o.rID <> " + this.telefon.getRID() + " AND o.kisiRef.rID = " + this.telefon.getKisiRef().getRID(), "o.rID DESC").get(0);
					varsayilanYapilacakTelefon.setVarsayilan(EvetHayir._EVET);
					getDBOperator().update(varsayilanYapilacakTelefon);
				}
			}
			super.justDelete(this.telefon, this.telefonController.isLoggable());
			this.telefonController.queryAction();
		} else if (detailType.equalsIgnoreCase("internetadres")) {
			this.internetadres = (Internetadres) entity;
			if (this.internetadres.getVarsayilan() == EvetHayir._EVET) {
				if (getDBOperator().recordCount(Internetadres.class.getSimpleName(),
						"o.rID <> " + this.internetadres.getRID() + " AND o.kisiRef.rID = " + this.internetadres.getKisiRef().getRID()) > 0) {
					Internetadres varsayilanYapilacakInternetadres = (Internetadres) getDBOperator().load(Internetadres.class.getSimpleName(),
							"o.rID <> " + this.internetadres.getRID() + " AND o.kisiRef.rID = " + this.internetadres.getKisiRef().getRID(), "o.rID DESC").get(0);
					varsayilanYapilacakInternetadres.setVarsayilan(EvetHayir._EVET);
					getDBOperator().update(varsayilanYapilacakInternetadres);
				}
			}
			super.justDelete(this.internetadres, this.internetadresController.isLoggable());
			this.internetadresController.queryAction();
		}

		else if (detailType.equalsIgnoreCase("personelBirimProje")) {
			this.personelBirimProje = (Ypersonelbirimproje) entity;
			super.justDelete(this.personelBirimProje, this.personelBirimProjeController.isLoggable());
			this.personelBirimProjeController.queryAction();
		}
	}

	@Override
	public String detailPage(Long selectedRID) {
		if (selectedRID != null && selectedRID > 0) {
			setSelectedRID(selectedRID + "");
		} else if (getSelectedRID() == null) {
			return "";
		}
		ManagedBeanLocator.locateSessionUser().addToSessionFilters(getModelName(), getEntity());
		return ManagedBeanLocator.locateMenuController().gotoPage("menu_" + getModelName() + "Detay");
	}

	public Ypersonel getPersonel() {
		personel = (Ypersonel) getEntity();
		return personel;
	}

	public void setPersonel(Ypersonel personel) {
		this.personel = personel;
	}

	public YpersonelFilter getPersonelFilter() {
		return personelFilter;
	}

	public void setPersonelFilter(YpersonelFilter personelFilter) {
		this.personelFilter = personelFilter;
	}

	public Ypersonel getPersonelDetay() {
		return personelDetay;
	}

	public void setPersonelDetay(Ypersonel personelDetay) {
		this.personelDetay = personelDetay;
	}

	public String getSelectedTab() {
		return selectedTab;
	}

	public void setSelectedTab(String selectedTab) {
		this.selectedTab = selectedTab;
	}

	public String getDetailEditType() {
		return detailEditType;
	}

	public void setDetailEditType(String detailEditType) {
		this.detailEditType = detailEditType;
	}

	public Ypersonelbirimproje getPersonelBirimProje() {
		return personelBirimProje;
	}

	public void setPersonelBirimGorev(Ypersonelbirimproje personelBirimProje) {
		this.personelBirimProje = personelBirimProje;
	}

	public String getOldValueForPersonelBirimProje() {
		return oldValueForPersonelBirimProje;
	}

	public void setOldValueForPersonelBirimProje(String OldValueForPersonelBirimProje) {
		this.oldValueForPersonelBirimProje = OldValueForPersonelBirimProje;
	}

	public Adres getAdres() {
		return adres;
	}

	public void setAdres(Adres adres) {
		this.adres = adres;
	}

	public Telefon getTelefon() {
		return telefon;
	}

	public void setTelefon(Telefon telefon) {
		this.telefon = telefon;
	}

	public Internetadres getInternetadres() {
		return internetadres;
	}

	public void setInternetadres(Internetadres internetadres) {
		this.internetadres = internetadres;
	}

	public YpersonelbirimprojeController getPersonelBirimGorevController() {
		return personelBirimProjeController;
	}

	public void setPersonelBirimGorevController(YpersonelbirimprojeController personelBirimProjeController) {
		this.personelBirimProjeController = personelBirimProjeController;
	}

	public InternetadresController getInternetadresController() {
		return internetadresController;
	}

	public void setInternetadresController(InternetadresController internetadresController) {
		this.internetadresController = internetadresController;
	}

	public AdresController getAdresController() {
		return adresController;
	}

	public void setAdresController(AdresController adresController) {
		this.adresController = adresController;
	}

	public TelefonController getTelefonController() {
		return telefonController;
	}

	public void setTelefonController(TelefonController telefonController) {
		this.telefonController = telefonController;
	}

	public String getOldValueForAdres() {
		return oldValueForAdres;
	}

	public void setOldValueForAdres(String oldValueForAdres) {
		this.oldValueForAdres = oldValueForAdres;
	}

	public String getOldValueForInternetadres() {
		return oldValueForInternetadres;
	}

	public void setOldValueForInternetadres(String oldValueForInternetadres) {
		this.oldValueForInternetadres = oldValueForInternetadres;
	}

	public String getOldValueForTelefon() {
		return oldValueForTelefon;
	}

	public void setOldValueForTelefon(String oldValueForTelefon) {
		this.oldValueForTelefon = oldValueForTelefon;
	}

	public void kullanicitanimla() {
		try {
			if (getDBOperator().recordCount(Kullanici.class.getSimpleName(), "o.kisiRef.rID=" + getPersonel().getKisiRef().getRID()) <= 0) {
				Kullanici kullanici = new Kullanici();
				kullanici.setKisiRef(getPersonel().getKisiRef());
				String kullaniciadi = getPersonel().getKisiRef().getAd().toLowerCase() + "." + getPersonel().getKisiRef().getSoyad().toLowerCase();
				int kullaniciadiuzunluk = kullaniciadi.length();
				String randomnumericdeger = null;
				if (getDBOperator().recordCount(Kullanici.class.getSimpleName(), "") > 0) {
					@SuppressWarnings("unchecked")
					List<Kullanici> kullaniciList = getDBOperator().load(Kullanici.class.getSimpleName(), "", "");
					for (Kullanici entity : kullaniciList) {
						if (entity.getName().equals(kullaniciadi)) {
							randomnumericdeger = "." + RandomObjectGenerator.generateRandomPassword(6);
							if (kullaniciadi.length() > kullaniciadiuzunluk) {
								kullaniciadi.substring(0, 7);
								kullaniciadi += randomnumericdeger;
							} else {
								kullaniciadi += randomnumericdeger;
							}
						}
					}
				}

				kullanici.setName(kullaniciadi);
				String password = RandomObjectGenerator.generateRandomPassword(6);
				kullanici.setPassword(ScryptPasswordHashing.encrypt(password));
				kullanici.setTheme("bootstrap");
				getDBOperator().insert(kullanici);
				SendEmail email = new SendEmail();
				if (getDBOperator().recordCount(Internetadres.class.getSimpleName(),
						"o.kisiRef.rID=" + getPersonel().getKisiRef().getRID() + " AND o.varsayilan=" + EvetHayir._EVET.getCode()) > 0) {
					Internetadres varsayilanInternetAdd = (Internetadres) getDBOperator().load(Internetadres.class.getSimpleName(),
							"o.kisiRef.rID=" + getPersonel().getKisiRef().getRID() + " AND o.varsayilan=" + EvetHayir._EVET.getCode(), "").get(0);
					email.sendEmailToRecipent(varsayilanInternetAdd.getNetadresmetni(), "TOYS", "Kullanıcı Adı :" + kullanici.getName() + "\nŞifre :" + password + "");
					createGenericMessage("Kişinin Mail Adresine Kullanıcı Adı ve Şifre Gönderildi! ", FacesMessage.SEVERITY_INFO);
				}
			} else {
				createGenericMessage("Bu kişi kullanıcı olarak tanımlı olduğundan tekrardan kullanıcı olarak tanımlayamazsınız!", FacesMessage.SEVERITY_INFO);
			}

		} catch (DBException e) {
			// TODO Auto-generated catch block
			logYaz("Exception @" + getModelName() + "Controller :", e);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logYaz("Exception @" + getModelName() + "Controller :", e);
		}

	}

} // class

