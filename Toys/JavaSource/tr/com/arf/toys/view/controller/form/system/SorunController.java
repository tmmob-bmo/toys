package tr.com.arf.toys.view.controller.form.system; 
  
 
import java.util.Date;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.framework.utility.exception.data.DBException;
import tr.com.arf.toys.db.enumerated.system.OnemDerece;
import tr.com.arf.toys.db.filter.system.SorunFilter;
import tr.com.arf.toys.db.model.system.Sorun;
import tr.com.arf.toys.service.application.ApplicationDescriptor;
import tr.com.arf.toys.view.controller.form._base.ToysBaseFilteredController;
  
@ManagedBean 
@ViewScoped 
public class SorunController extends ToysBaseFilteredController {  
  
	private static final long serialVersionUID = 570658022388304008L;
	private Sorun sorun;  
	private SorunFilter sorunFilter = new SorunFilter();  
	private String baslik; 
	private String aciklama; 
	private OnemDerece onem;
  
	public SorunController() { 
		super(Sorun.class);  
		setDefaultValues(false);  
		setTable(null); 
		queryAction(); 
	} 
	
	
	public Sorun getSorun() { 
		sorun = (Sorun) getEntity(); 
		return sorun; 
	} 
	
	public void setSorun(Sorun sorun) { 
		this.sorun = sorun; 
	} 
	
	public SorunFilter getSorunFilter() { 
		return sorunFilter; 
	} 
	 
	public void setSorunFilter(SorunFilter sorunFilter) {  
		this.sorunFilter = sorunFilter;  
	}  
	
	public String getBaslik() {
		return baslik;
	}

	public void setBaslik(String baslik) {
		this.baslik = baslik;
	}

	public String getAciklama() {
		return aciklama;
	}

	public void setAciklama(String aciklama) {
		this.aciklama = aciklama;
	}

	public OnemDerece getOnem() {
		return onem;
	}

	public void setOnem(OnemDerece onem) {
		this.onem = onem;
	}


	@Override  
	public BaseFilter getFilter() {  
		return getSorunFilter();  
	}   
	
	@Override	
	public void insert() {	
		super.insert();		
		getSorun().setKullaniciRef(getSessionUser().getKullanici());		
		getSorun().setTarih(new Date());
	}	
	
	public String saveAndReturnHomePage(){
		try {
			Sorun sorunBildir=new Sorun();
			sorunBildir.setAciklama(getAciklama());
			sorunBildir.setBaslik(getBaslik());
			sorunBildir.setOnem(getOnem());
			sorunBildir.setKullaniciRef(getSessionUser().getKullanici());
			sorunBildir.setTarih(new Date());
			getDBOperator().insert(sorunBildir);
			createGenericMessage("Sorun Bildirimleriniz Sistemimize Kaydedildi! ", FacesMessage.SEVERITY_INFO);
		} catch (DBException e) {
			// TODO Auto-generated catch block
			logYaz("Exception @" + getModelName() + "Controller :", e);
		}
		return ApplicationDescriptor._DASHBOARD_OUTCOME; 
	}
	
	public String homePage(){
		return ApplicationDescriptor._DASHBOARD_OUTCOME; 
	}
	
} // class 
