package tr.com.arf.toys.view.controller.form.system; 
  
 
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.toys.db.filter.system.IlceFilter;
import tr.com.arf.toys.db.model.system.Ilce;
import tr.com.arf.toys.db.model.system.Sehir;
import tr.com.arf.toys.service.application.ApplicationDescriptor;
import tr.com.arf.toys.service.application.ManagedBeanLocator;
import tr.com.arf.toys.view.controller.form._base.ToysFilteredControllerNoPaging;
  
  
@ManagedBean 
@ViewScoped
public class IlceController extends ToysFilteredControllerNoPaging {  
   
	private static final long serialVersionUID = -4536989616189413483L;
	
	private Ilce ilce;   
	private IlceFilter ilceFilter = new IlceFilter();  
  
	public IlceController() { 
		super(Ilce.class);   
		setDefaultValues(false);   
		// setPostAddOutcome(getListOutcome());  
		setOrderField("ad");   
		if(getMasterEntity() != null){
			setMasterObjectName(ApplicationDescriptor._SEHIR_MODEL);
			setMasterOutcome(ApplicationDescriptor._SEHIR_MODEL);
			getIlceFilter().setSehirRef(getMasterEntity());
			getIlceFilter().createQueryCriterias();
		}
		queryAction();
	} 
	
	public Sehir getMasterEntity(){
		if(getIlceFilter().getSehirRef() != null){
			return getIlceFilter().getSehirRef();
		} else {
			return (Sehir) getObjectFromSessionFilter(ApplicationDescriptor._SEHIR_MODEL);			
		}			
	} 
	
	public Ilce getIlce() { 
		ilce = (Ilce) getEntity(); 
		return ilce; 
	} 
	
	public void setIlce(Ilce ilce) { 
		this.ilce = ilce; 
	} 
	
	public IlceFilter getIlceFilter() { 
		return ilceFilter; 
	} 
	 
	public void setIlceFilter(IlceFilter ilceFilter) {  
		this.ilceFilter = ilceFilter;  
	}  
	 
	@Override  
	public BaseFilter getFilter() {  
		return getIlceFilter();  
	}   
	  
	@Override	
	public void insert() {	
		super.insert();	
		getIlce().setSehirRef(getIlceFilter().getSehirRef());	 
	}	
	
	public String koy() {	
		if (!setSelected()) {
			return "";
		}	 
		putObjectToSessionFilter(ApplicationDescriptor._ILCE_MODEL, getIlce());
		return ManagedBeanLocator.locateMenuController().gotoPage("menu_Koy");
	} 
	
} // class 
