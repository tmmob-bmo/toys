package tr.com.arf.toys.view.controller._common;

import static org.quartz.CronScheduleBuilder.cronSchedule;
import static org.quartz.TriggerBuilder.newTrigger;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;

import org.apache.log4j.Logger;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.impl.StdSchedulerFactory;

import tr.com.arf.framework.db._management.DBOperator;
import tr.com.arf.framework.utility.application.ApplicationVariables;
import tr.com.arf.toys.db.model.system.Menuitem;
import tr.com.arf.toys.db.model.system.SistemParametre;
import tr.com.arf.toys.service.timer.TimedJobService;
import tr.com.arf.toys.utility.application.ApplicationConstant;
import tr.com.arf.toys.utility.logUtils.LogService;
import tr.com.arf.toys.utility.tool.EmailConfigObject;

@ManagedBean(eager = true)
@ApplicationScoped
public final class ApplicationController {
	protected static Logger logger = Logger.getLogger(ApplicationController.class);

	/* String Resource */
	public String labelResource = "tr.com.arf.toys.view.resource.label";

	private HashMap<String, Object> menuItems = new HashMap<String, Object>();

	private DBOperator dBOperator;

	private EmailConfigObject eMailConfig;

	public ApplicationController() {
		@SuppressWarnings("unused")
		ApplicationVariables av = new ApplicationVariables("TOYS");
		createMenuItemList();
		checkIfAtTest();
	}

	@PostConstruct
	public void init() {
		startScheduler();
		dBOperator = new DBOperator();
		// KPS servislerinin testi icin eklendi.
		System.out.println("-------------------------- ApplicationController initialized..");
	}

	@PreDestroy
	public void destroy() {
		LogService.logYaz("-------------------------- ALERT !!! ApplicationController destroy begin..", "ApplicationController");
		stopScheduler();
	}

	public static void startScheduler() {
		try {
			Scheduler scheduler = StdSchedulerFactory.getDefaultScheduler();
			JobKey jobKeyA = new JobKey("jobA", "group1");
			JobDetail cronJob = JobBuilder.newJob(TimedJobService.class).withIdentity(jobKeyA).build();
			// Job her gun saat 9:30'da calisacak sekilde programlaniyor.
			Trigger trigger;
			trigger = newTrigger().withIdentity("trigger1", "group1").startNow().withSchedule(cronSchedule("0 30 09am * * ?")).build();
			scheduler.start();
			scheduler.scheduleJob(cronJob, trigger);
			// .withSchedule(simpleSchedule().withIntervalInSeconds(5).repeatForever())
		} catch (Exception se) {
			LogService.logYaz("Exception @ApplicationController :" + se.getMessage(), "ApplicationController", se);
		}
	}

	public static void stopScheduler() {
		try {
			Scheduler scheduler = StdSchedulerFactory.getDefaultScheduler();
			if (scheduler.isStarted()) {
				scheduler.shutdown(true);
				System.out.println("-------------------------- ACTIVE SCHEDULER STOPPED .... ");
			}

		} catch (SchedulerException se) {
			LogService.logYaz("Exception @ApplicationController :" + se.getMessage(), "ApplicationController", se);
		}
	}

	@SuppressWarnings("unchecked")
	public void createMenuItemList() {
		DBOperator dbOperator = new DBOperator();
		List<Menuitem> menuItemList = dbOperator.load(Menuitem.class.getSimpleName());
		if (menuItemList != null && menuItemList.size() > 0) {
			for (Menuitem menuItem : menuItemList) {
				menuItems.put(menuItem.getErisimKodu(), menuItem);
			}
		}
	}

	public String getLabelResource() {
		return labelResource;
	}

	public HashMap<String, Object> getMenuItems() {
		return menuItems;
	}

	public void setMenuItems(HashMap<String, Object> menuItems) {
		this.menuItems = menuItems;
	}

	public String getAlphaRegex() {
		return "/^[a-zA-ZıİöÖğĞşŞüÜçÇ ]+$/";
	}

	public String getAlphaRegexSpaceAllowed() {
		return "/^[a-zA-ZıİöÖğĞşŞüÜçÇ ]+$/";
	}

	public String getAlphaNumericRegex() {
		return "/^[a-zA-ZıİöÖğĞşŞüÜçÇ0-9]+$/";
	}

	public String getAlphaNumericRegexSpaceAllowed() {
		return "/^[a-zA-ZıİöÖğĞşŞüÜçÇ0-9 ]+$/";
	}

	public final DBOperator getDBOperator() {
		if (dBOperator == null) {
			dBOperator = new DBOperator();
		}
		return dBOperator;
	}

	private SistemParametre sistemParametre;

	public void setSistemParametre(SistemParametre sistemParametre) {
		this.sistemParametre = sistemParametre;
	}

	public SistemParametre getSistemParametre() {
		if (sistemParametre == null) {
			createSistemParametre();
		}
		return sistemParametre;
	}

	public void createSistemParametre() {
		if (getDBOperator().recordCount(SistemParametre.class.getSimpleName()) > 0) {
			setSistemParametre((SistemParametre) getDBOperator().load(SistemParametre.class.getSimpleName()).get(0));
		}
	}

	/* Eposta parametreleri - Tomcat Context'inden alinacak. */

	private String emailHostName;
	private String emailFrom;
	private String emailPass;
	private String emailPort;

	public String getEmailHostName() {
		if (emailHostName == null || emailHostName.trim().length() == 0) {
			createEmailHostName();
		}
		return emailHostName;
	}

	public void setEmailHostName(String emailHostName) {
		this.emailHostName = emailHostName;
	}

	public String getEmailFrom() {
		if (emailFrom == null || emailFrom.trim().length() == 0) {
			createEmailFrom();
		}
		return emailFrom;
	}

	public void setEmailFrom(String emailFrom) {
		this.emailFrom = emailFrom;
	}

	public String getEmailPass() {
		if (emailPass == null || emailPass.trim().length() == 0) {
			createEmailPass();
		}
		return emailPass;
	}

	public void setEmailPass(String emailPass) {
		this.emailPass = emailPass;
	}

	public String getEmailPort() {
		if (emailPort == null || emailPort.trim().length() == 0) {
			createEmailPort();
		}
		return emailPort;
	}

	public void setEmailPort(String emailPort) {
		this.emailPort = emailPort;
	}

	public void createEmailHostName() {
		try {
			if (FacesContext.getCurrentInstance().getExternalContext().getInitParameter("emailHost") != null) {
				setEmailHostName(FacesContext.getCurrentInstance().getExternalContext().getInitParameter("emailHost"));
			} else {
				setEmailHostName(ApplicationConstant._emailHost);
			}
		} catch (Exception e) {
			setEmailHostName(ApplicationConstant._emailHost);
		}
	}

	public void createEmailFrom() {
		try {
			if (FacesContext.getCurrentInstance().getExternalContext().getInitParameter("emailFrom") != null) {
				setEmailFrom(FacesContext.getCurrentInstance().getExternalContext().getInitParameter("emailFrom"));
			} else {
				setEmailFrom(ApplicationConstant._emailFrom);
			}
		} catch (Exception e) {
			LogService.logYaz(e.getMessage(), e);
			setEmailFrom(ApplicationConstant._emailFrom);
		}
	}

	public void createEmailPass() {
		try {
			if (FacesContext.getCurrentInstance().getExternalContext().getInitParameter("emailPass") != null) {
				setEmailPass(FacesContext.getCurrentInstance().getExternalContext().getInitParameter("emailPass"));
			} else {
				setEmailPass(ApplicationConstant._emailPass);
			}
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			setEmailPass(ApplicationConstant._emailPass);
		}
	}

	public void createEmailPort() {
		try {
			if (FacesContext.getCurrentInstance().getExternalContext().getInitParameter("emailPort") != null) {
				setEmailPort(FacesContext.getCurrentInstance().getExternalContext().getInitParameter("emailPort"));
			} else {
				setEmailPort(ApplicationConstant._emailPort);
			}
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			setEmailPort(ApplicationConstant._emailPort);
		}
	}

	public EmailConfigObject geteMailConfig() {
		if (eMailConfig == null || eMailConfig.getProp() == null || eMailConfig.getEmailFrom() == null) {
			logger.debug("Creating EmailConfigObject....");
			// System.out.println(" getEmailHostName() : " +
			// getEmailHostName());
			// System.out.println(" getEmailFrom() : " + getEmailFrom());
			// System.out.println(" getEmailPass() : " + getEmailPass());
			// System.out.println(" getEmailPort() : " + getEmailPort());
			this.eMailConfig = new EmailConfigObject(getEmailHostName(), getEmailFrom(), getEmailPass(), getEmailPort());
		}
		logger.debug("Email config got :" + eMailConfig.getEmailFrom());
		return eMailConfig;
	}

	public void seteMailConfig(EmailConfigObject eMailConfig) {
		this.eMailConfig = eMailConfig;
	}

	/* GARANTI URL PARAMETRELERI */
	private String terminalID;
	private String merchantID;
	private String storeKey;
	private String password;

	private String garantiBankasiOKURL;
	private String garantiBankasiFAILURL;
	private String terminalUserID;
	private String garantiMode;
	private String strHostAddress;

	public String getTerminalID() {
		if (terminalID == null || terminalID.trim().length() == 0) {
			createTerminalID();
		}
		return terminalID;
	}

	public void setTerminalID(String terminalID) {
		this.terminalID = terminalID;
	}

	public String getMerchantID() {
		if (merchantID == null || merchantID.trim().length() == 0) {
			createMerchantID();
		}
		return merchantID;
	}

	public void setMerchantID(String merchantID) {
		this.merchantID = merchantID;
	}

	public String getStoreKey() {
		if (storeKey == null || storeKey.trim().length() == 0) {
			createStoreKey();
		}
		return storeKey;
	}

	public void setStoreKey(String storeKey) {
		this.storeKey = storeKey;
	}

	public String getPassword() {
		if (password == null || password.trim().length() == 0) {
			createPassword();
		}
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getGarantiBankasiOKURL() {
		if (garantiBankasiOKURL == null || garantiBankasiOKURL.trim().length() == 0) {
			createGarantiBankasiOKURL();
		}
		return garantiBankasiOKURL;
	}

	public void setGarantiBankasiOKURL(String garantiBankasiOKURL) {
		this.garantiBankasiOKURL = garantiBankasiOKURL;
	}

	public String getGarantiBankasiFAILURL() {
		if (garantiBankasiFAILURL == null || garantiBankasiFAILURL.trim().length() == 0) {
			createGarantiBankasiFAILURL();
		}
		return garantiBankasiFAILURL;
	}

	public void setGarantiBankasiFAILURL(String garantiBankasiFAILURL) {
		this.garantiBankasiFAILURL = garantiBankasiFAILURL;
	}

	public String getStrHostAddress() {
		if (strHostAddress == null || strHostAddress.trim().length() == 0) {
			createStrHostAddress();
		}
		return strHostAddress;
	}

	public void setStrHostAddress(String strHostAddress) {
		this.strHostAddress = strHostAddress;
	}

	public String getTerminalUserID() {
		if (terminalUserID == null || terminalUserID.trim().length() == 0) {
			createTerminalUserID();
		}
		return terminalUserID;
	}

	public void setTerminalUserID(String terminalUserID) {
		this.terminalUserID = terminalUserID;
	}

	public String getGarantiMode() {
		if (garantiMode == null || garantiMode.trim().length() == 0) {
			createGarantiMode();
		}
		return garantiMode;
	}

	public void setGarantiMode(String garantiMode) {
		this.garantiMode = garantiMode;
	}

	public void createTerminalID() {
		try {
			if (FacesContext.getCurrentInstance().getExternalContext().getInitParameter("terminalID") != null) {
				setTerminalID(FacesContext.getCurrentInstance().getExternalContext().getInitParameter("terminalID"));
			} else {
				setTerminalID(ApplicationConstant.terminalID);
			}
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			setTerminalID(ApplicationConstant.terminalID);
		}
	}

	public void createMerchantID() {
		try {
			if (FacesContext.getCurrentInstance().getExternalContext().getInitParameter("merchantID") != null) {
				setMerchantID(FacesContext.getCurrentInstance().getExternalContext().getInitParameter("merchantID"));
			} else {
				setMerchantID(ApplicationConstant.merchantID);
			}
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			setMerchantID(ApplicationConstant.merchantID);
		}
	}

	public void createStoreKey() {
		try {
			if (FacesContext.getCurrentInstance().getExternalContext().getInitParameter("storeKey") != null) {
				setStoreKey(FacesContext.getCurrentInstance().getExternalContext().getInitParameter("storeKey"));
			} else {
				setStoreKey(ApplicationConstant.storeKey);
			}
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			setStoreKey(ApplicationConstant.storeKey);
		}
	}

	public void createPassword() {
		try {
			if (FacesContext.getCurrentInstance().getExternalContext().getInitParameter("password") != null) {
				setPassword(FacesContext.getCurrentInstance().getExternalContext().getInitParameter("password"));
			} else {
				setPassword(ApplicationConstant.password);
			}
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			setPassword(ApplicationConstant.password);
		}
	}

	public void createGarantiBankasiOKURL() {
		try {
			if (FacesContext.getCurrentInstance().getExternalContext().getInitParameter("garantiBankasiOKURL") != null) {
				System.out.println("garantiBankasiOKURL found in context.");
				setGarantiBankasiOKURL(FacesContext.getCurrentInstance().getExternalContext().getInitParameter("garantiBankasiOKURL"));
			} else {
				System.out.println("garantiBankasiOKURL NOT found in context !!!!");
				setGarantiBankasiOKURL(ApplicationConstant._garantiBankasiOKURL);
			}
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			setGarantiBankasiOKURL(ApplicationConstant._garantiBankasiOKURL);
		}
	}

	public void createGarantiBankasiFAILURL() {
		try {
			if (FacesContext.getCurrentInstance().getExternalContext().getInitParameter("garantiBankasiFAILURL") != null) {
				setGarantiBankasiFAILURL(FacesContext.getCurrentInstance().getExternalContext().getInitParameter("garantiBankasiFAILURL"));
			} else {
				setGarantiBankasiFAILURL(ApplicationConstant._garantiBankasiFAILURL);
			}
		} catch (Exception exc) {
			logger.error(exc.getMessage(), exc);
			setGarantiBankasiFAILURL(ApplicationConstant._garantiBankasiFAILURL);
		}
	}

	public void createStrHostAddress() {
		try {
			if (FacesContext.getCurrentInstance().getExternalContext().getInitParameter("garantiProvizyonAdresi") != null) {
				setStrHostAddress(FacesContext.getCurrentInstance().getExternalContext().getInitParameter("garantiProvizyonAdresi"));
			} else {
				setStrHostAddress(ApplicationConstant._garantiProvizyonAdresi);
			}
		} catch (Exception exc) {
			logger.error(exc.getMessage(), exc);
			setStrHostAddress(ApplicationConstant._garantiProvizyonAdresi);
		}
	}

	public void createTerminalUserID() {
		try {
			if (FacesContext.getCurrentInstance().getExternalContext().getInitParameter("terminalUserID") != null) {
				setTerminalUserID(FacesContext.getCurrentInstance().getExternalContext().getInitParameter("terminalUserID"));
			} else {
				setTerminalUserID(ApplicationConstant.terminalUserID);
			}
		} catch (Exception exc) {
			logger.error(exc.getMessage(), exc);
			setTerminalUserID(ApplicationConstant.terminalUserID);
		}
	}

	public void createGarantiMode() {
		try {
			if (FacesContext.getCurrentInstance().getExternalContext().getInitParameter("garantiMode") != null) {
				setGarantiMode(FacesContext.getCurrentInstance().getExternalContext().getInitParameter("garantiMode"));
			} else {
				setGarantiMode(ApplicationConstant.garantiMode);
			}
		} catch (Exception exc) {
			logger.error(exc.getMessage(), exc);
			setGarantiMode(ApplicationConstant.garantiMode);
		}
	}

	/* Uygulamanin ARF'deki PC'lerde calisip calismadigini belirler. True ise ARF'de calisiyordur. */
	public boolean atTest = false;

	public boolean isAtTest() {
		return atTest;
	}

	public void setAtTest(boolean atTest) {
		this.atTest = atTest;
	}
	
	private void checkIfAtTest() {
		InetAddress ip;
		try {
			ip = InetAddress.getLocalHost();
			String serverName = ip.getHostName();
			if (serverName.equalsIgnoreCase("Ufuk-Lenovo") || serverName.equalsIgnoreCase("PALLADIUM") || serverName.equalsIgnoreCase("Ufuk-MSI")) {
				setAtTest(true);
			} else {
				setAtTest(false);
			}
		} catch (UnknownHostException e) {
			e.printStackTrace();
			setAtTest(false);
		}
	}
}
