package tr.com.arf.toys.view.controller.form.uye;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.toys.db.filter.uye.UyeuzmanlikFilter;
import tr.com.arf.toys.db.model.kisi.Kisi;
import tr.com.arf.toys.db.model.uye.Uyeuzmanlik;
import tr.com.arf.toys.service.application.ApplicationDescriptor;
import tr.com.arf.toys.view.controller.form._base.ToysBaseFilteredController;

@ManagedBean
@ViewScoped
public class UyeuzmanlikController extends ToysBaseFilteredController {

	private static final long serialVersionUID = 6531152165884980748L;

	private Uyeuzmanlik uyeuzmanlik;
	private UyeuzmanlikFilter uyeuzmanlikFilter = new UyeuzmanlikFilter();

	public UyeuzmanlikController() {
		super(Uyeuzmanlik.class);
		setDefaultValues(false);
		setOrderField("aciklama");
		setAutoCompleteSearchColumns(new String[] { "aciklama" });
		setTable(null);
		if (getMasterEntity() != null) {
			setMasterObjectName(ApplicationDescriptor._KISI_MODEL);
			setMasterOutcome(ApplicationDescriptor._KISI_MODEL);
			getUyeuzmanlikFilter().setKisiRef(getMasterEntity());
			getUyeuzmanlikFilter().createQueryCriterias();
		}
		queryAction();
	}

	public UyeuzmanlikController(Object object) {
		super(Uyeuzmanlik.class);
	}

	public Kisi getMasterEntity() {
		if (getUyeuzmanlikFilter().getKisiRef() != null) {
			return getUyeuzmanlikFilter().getKisiRef();
		} else {
			return (Kisi) getObjectFromSessionFilter(ApplicationDescriptor._KISI_MODEL);
		}
	}

	public Uyeuzmanlik getUyeuzmanlik() {
		uyeuzmanlik = (Uyeuzmanlik) getEntity();
		return uyeuzmanlik;
	}

	public void setUyeuzmanlik(Uyeuzmanlik uyeuzmanlik) {
		this.uyeuzmanlik = uyeuzmanlik;
	}

	public UyeuzmanlikFilter getUyeuzmanlikFilter() {
		return uyeuzmanlikFilter;
	}

	public void setUyeuzmanlikFilter(UyeuzmanlikFilter uyeuzmanlikFilter) {
		this.uyeuzmanlikFilter = uyeuzmanlikFilter;
	}

	@Override
	public BaseFilter getFilter() {
		return getUyeuzmanlikFilter();
	}

	@Override
	public void insert() {
		super.insert();
		getUyeuzmanlik().setKisiRef(getUyeuzmanlikFilter().getKisiRef());
	}
} // class
