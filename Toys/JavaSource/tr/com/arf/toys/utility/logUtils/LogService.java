package tr.com.arf.toys.utility.logUtils;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import tr.com.arf.framework.utility.tool.DateUtil;
import tr.com.arf.toys.service.application.ManagedBeanLocator;
import tr.com.arf.toys.view.controller._common.SessionUser;

public class LogService {

	protected static Logger logger = Logger.getLogger("logService");

	public static void logYaz(String logMessage) {
		System.out.println(logMessage);
		logger.error(logMessage);
		try {
			if (ManagedBeanLocator.locateSessionUser() != null && ManagedBeanLocator.locateSessionUser().getKullanici() != null && ManagedBeanLocator.locateSessionUser().getKullanici().getRID() != null) {
				SessionUser sessionUser = ManagedBeanLocator.locateSessionUser();
				logger.error("Kullanıcı Adı : " + sessionUser.getKullaniciText());
				logger.error("Kullanıcı RID : " + sessionUser.getKullaniciRid());
				logger.error("Kullanıcı IP : " + sessionUser.getIpAddress());
			}
		} catch(Exception e){
			logger.error("Can not reach SessionUser");
			System.out.println("Can not reach SessionUser...");
		}
		logger.error("Tarih : " + DateUtil.dateToDMYHMS(new Date()));
	}

	public static void logYaz(String logMessage, Exception e) {
		logYaz(logMessage + " : " + e.getMessage());
	}

	public static void logYaz(String logMessage, String className) {
		logYaz(logMessage);
		logger.error("Class : " + className);
	}

	public static void logYaz(String logMessage, String className, Exception e) {
		logYaz(logMessage + " : " + e.getMessage(), className);
        logger.error(e.toString(), e);
	}

	public static String getClientIpAddr(HttpServletRequest request) {
		String ip = request.getHeader("X-Forwarded-For");
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("Proxy-Client-IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("WL-Proxy-Client-IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("HTTP_X_FORWARDED_FOR");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("HTTP_X_FORWARDED");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("HTTP_X_CLUSTER_CLIENT_IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("HTTP_CLIENT_IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("HTTP_FORWARDED_FOR");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("HTTP_FORWARDED");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("HTTP_VIA");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("REMOTE_ADDR");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getRemoteAddr();
		}
		return ip;
	}
}
