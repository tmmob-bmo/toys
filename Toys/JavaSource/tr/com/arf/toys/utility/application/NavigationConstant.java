package tr.com.arf.toys.utility.application;

public final class NavigationConstant {
	
	private static final String _BASE = "/_page/_main/main";
	private static final String _DASHBOARD_COMMON = "/_page/_dashboard/dashboard_Common";
	private static final String _UYEBASE = "/_page/_main/uyemain";
	
	public static String basePage() {
		return _BASE;
	}
	
	public static String uyebasePage(){
		return _UYEBASE;
	}

	public static String getDashboardCommon() {
		return _DASHBOARD_COMMON;
	}

}
