package tr.com.arf.toys.utility.application;

import tr.com.arf.framework.db.enumerated._common.EvetHayir;
import tr.com.arf.framework.utility.application.ApplicationConstantBase;
import tr.com.arf.toys.db.enumerated.evrak.EvrakDurum;
import tr.com.arf.toys.db.enumerated.system.BirimDurum;
import tr.com.arf.toys.db.enumerated.system.PersonelDurum;
import tr.com.arf.toys.db.enumerated.uye.UyeDurum;

public class ApplicationConstant extends ApplicationConstantBase {

	private static final long serialVersionUID = 4885389018684066374L;

	/* WEB SERVIS */
	public static final String _kpsKullaniciAdi = "demo";
	public static final String _kpsSifre = "2o14!Bmo?";
	public static final String _kpsEndpoint = "http://kps.emo.org.tr/TmmobKpsService.asmx";

	public static final int _MAX_RECORD_COUNT = 200;
	public static final int _MAX_NOFILTER_RECORD_COUNT = 200;
	public static final int _LIST_SIZE = 10;
	public static final String _EDIT_SUFFIX = "_Edit";

	public static final String _insideTextSearch = "inside";
	public static final String _beginsWithTextSearch = "beginsWith";
	public static final String _equalsTextSearch = "equals";

	public static final String _emailHost = "smtp.gmail.com";
	public static final String _emailFrom = "bmotoys@gmail.com";
	public static final String _emailPass = "toys123456";
	public static final String _emailPort = "587";

	/* GARANTI CONN PARAMETERS */
	public static final String terminalID = "30691298"; // 3D ICIN CALISAN KOD
//	public static final String terminalID = "30691300";


	public static final String merchantID = "7000679";
	public static final String storeKey = "12345678";
	public static final String password = "123qweASD";
	public static final String terminalUserID = "9814652";
	public static final String garantiMode = "TEST";


	public static final String _linuxNoImagePath = "Nobody.jpg";
	public static final String _windowsNoImagePath = "Nobody.jpg";

	public static final String _clientId = "700300000";
	public static final String _isBankasiOKURL = "http://localhost:8081/Toys/faces/_page/form/uye/uyeIsBankasiHesap_Sonuc.xhtml";
	public static final String _isBankasiFAILURL = "http://localhost:8081/Toys/faces/_page/form/uye/uyeIsBankasiHesap_Fail.xhtml";

	public static final String _garantiBankasiOKURL = "http://localhost:8082/Toys/faces/_page/form/uye/uyeGarantiHesap_Sonuc.xhtml";
	public static final String _garantiBankasiFAILURL = "http://localhost:8082/Toys/faces/_page/form/uye/uyeGarantiHesap_Fail.xhtml";
	public static final String _garantiProvizyonAdresi = "https://sanalposprov.garanti.com.tr/VPServlet";


	public static final String _successMessage = "success";

	public static final int _userDefined = 0;
	public static final int _equals = 1;
	public static final int _beginsWith = 2;
	public static final int _inside = 3;
	public static final int _smallerThan = 4;
	public static final int _largerThan = 5;
	public static final int _equalsAndLargerThan = 6;
	public static final int _equalsAndSmallerThan = 7;
	public static final int _notEquals = 8;

	public static final String _compIdParamName = "javax.faces.source";

	public static final int _bilirkisilikSureSinirlamasi = 1095;

	public static final String _windowsPath = "e:/Upload/Toys/";
	public static final String _linuxPath = "/opt/toys/";

	public static final String _globalMessagePanelId = "globalMessages";

	public static final String _windowsFontPath = "c:/temp/arialuni.ttf";
	public static final String _linuxFontPath = "/opt/toys/fonts/arialuni.ttf";

	public static final String _birimStaticWhereCondition = "o.durum = " + BirimDurum._ACIK.getCode();
	public static final String _kisiStaticWhereCondition = "o.aktif = " + EvetHayir._EVET.getCode();
	public static final String _birimHesapStaticWhereCondition = "o.gecerli = " + EvetHayir._EVET.getCode();
	public static final String _personelStaticWhereCondition = "o.durum <> " + PersonelDurum._AYRILMIS.getCode();
	public static final String _uyeStaticWhereCondition = "o.uyedurum NOT IN (" + UyeDurum._ISTIFA.getCode() + "," + UyeDurum._VEFAT.getCode() + "," + UyeDurum._KAPALI.getCode()
			+ "," + UyeDurum._PASIFUYE.getCode() + ")";
	public static final String _evrakStaticWhereCondition = "o.durum = " + EvrakDurum._ONAYLANMIS.getCode();

	public static final String[] _birimAutoCompleteSearchColumns = new String[] { "ad", "kisaad" };
	public static final String[] _kurumAutoCompleteSearchColumns = new String[] { "ad" };
	public static final String[] _uyeAutoCompleteSearchColumns = new String[] { "kisiRef.ad||' '||kisiRef.soyad", "sicilno" };
	public static final String[] _kisiAutoCompleteSearchColumns = new String[] { "ad||' '||soyad" };
	public static final String[] _personelAutoCompleteSearchColumns = new String[] { "kisiRef.ad", "kisiRef.soyad" };

	public static final String _uyelikIptalTalepCevapEvrakMetin = "<p><b>Sayın $UYEADSOYAD$</b></p>"
			+ "<p>$TARIH$ tarihinde üyelikten ayrılma talebi ile ilgili başvurunuz tarafımıza ulaşmıştır. </p>"
			+ "<p>$ODAADI$ Ana Yönetmeliği’nde üyelikten ayrılma koşul ve yükümlülükler Madde 14’ de şu şekilde ifade edilmektedir;</p>"
			+ "<p>1. Herhangi bir nedenle mesleki etkinliğini sürdürmeyen</p>"
			+ "<p>2. Askerlik görevi hariç silahlı kuvvetler mensubu olan</p>"
			+ "<p>3. Kamu kurum ve kuruluşlarında asli ve sürekli görevde çalışırken</p>"
			+ "<p>üyelikten ayrılmak isteyen üyeler;</p>"
			+ "<p>a. Bu durumu Oda Yönetim Kuruluna yazılı olarak bildirmek</p>"
			+ "<p>b. Gerektiğinde belgelemek</p>"
			+ "<p>c. Oda kimlik belgesini geri vermek</p>"
			+ "<p>d. O tarihe kadar olan üyelik ödentilerinin tamamını ödemek  ( Ödeme makbuzu-dekontu ile üyelik işlemleri sonlandırılacaktır.)</p>"
			+ "<p>koşuluyla Odadan ayrılabilirler.</p>"
			+ "<p>Başvurunuz Yönetim Kurulumuzun 1/14 numaralı toplantısında görüşülmüş ve üyelikten ayrılma başvurunuzda ifade ettiğiniz ... sağladığına dair belge eki .... bulunduğundan ve ... maddesinin gereği yerine getirildiği takdirde talebinizin ..., karar verilmiştir."
			+ "<p>Saygılarımızla,</p>" + "<p></p>" + "<p></p>" + "<p></p>" + "<p><b>$PERSONEL$</b></p>" + "<p>Yönetim Kurulu a.</p>" + "<p>Yönetim Kurulu Yazman Üyesi</p>";

	public static final String _bilirkisilikTalepCevapEvrakMetin = "<p>İlgi yazı ile istemiş olduğunuz Bilirkişilik Hizmeti işi, Türk Mühendis ve Mimar Odaları Birliği $YIL$ Yılı Bilirkişilik - Eksperlik - Hakemlik ve Teknik Müşavirlik Yönetmeliği’nin 7.b.2 Maddesine göre Bilirkişilik hizmet bedeli; Arazide ve iş sahalarında düzenlenen raporlarda arazide geçen her gün için kişi başına $BILIRKISILIKHIZMETBEDELI$ eklenmek üzere, her rapor için kişi başına en az $BILIRKISILIKRAPORBEDELI$’dur.</p>"
			+ "<p>Tespiti yapan kişinin yol, iaşe ve ibate bedeli hizmeti isteyen kişi ya da kuruluşa ait olup, söz konusu iş bir bilgisayar mühendisi tarafından yapılacaktır.</p>"
			+ "<p>Hizmet bedeli $BILIRKISILIKTOPLAMHIZMETBEDELI$’un Odamız kasasına veya $BANKAADI$ $BANKASUBEADI$ Şubesi $HESAPNO$ nolu $HESAPADI$ hesabımıza (IBAN: $IBANNO$) yatırdığınız takdirde, görevlendirme yapılarak teknik rapor hazırlanacak ve tarafınıza sunulacaktır.</p>"
			+ "<p>Bilgilerinize gereği için rica ederiz.</p>"
			+ "<p>Saygılarımızla,</p>"
			+ "<p></p>"
			+ "<p></p>"
			+ "<p></p>"
			+ "<p><b>$PERSONEL$</b></p>"
			+ "<p>Yönetim Kurulu a.</p>" + "<p>Yönetim Kurulu Yazman Üyesi</p>";

	public static final String _bilirkisilikGorevlendirmeMetin = "<p><b>Sayın $UYEADSOYAD$</b></p>"
			+ "<p>.... ait ilgi yazı ile talep edilen Bilirkişilik Hizmeti işi için Yönetim Kurulumuzca Bilirkişi olarak görevlendirilmiş olup; hizmet bedeli TMMOB yönetmeliğine uygun olarak mahsup edilecektir. Yol, iaşe ve ibate bedeli hizmeti isteyen kişi ya da kuruluşa ait olacaktır.</p>"
			+ "<p>Gereği için bilgilerinize sunarız.</p>" + "<p>Saygılarımızla,</p>" + "<p></p>" + "<p></p>" + "<p></p>" + "<p><b>$PERSONEL$</b></p>" + "<p>Yönetim Kurulu a.</p>"
			+ "<p>Yönetim Kurulu Yazman Üyesi</p>";

	public static final String _emeklilikBildirimMetin = "<p><b>Sayın $UYEADSOYAD$</b></p>"
			+ "<p>../../.... tarihli üyelikten ayrılma talebi ile ilgili başvurunuz tarafımıza ulaşmıştır. </p>"
			+ "<p>$ODAADI$ Ana Yönetmeliği’nde üyelikten ayrılma koşul ve yükümlülükleri Madde 14’ de şu şekilde ifade edilmektedir;</p>"
			+ "<p>1.	Herhangi bir nedenle mesleki etkinliğini sürdürmeyen</p>"
			+ "<p>2.	Askerlik görevi hariç silahlı kuvvetler mensubu olan</p>"
			+ "<p>3.	Kamu kurum ve kuruluşlarında asli ve sürekli görevde çalışırken </p>"
			+ "<p>koşuluyla Odadan ayrılabilirler.</p>"
			+ "<p>Bunun dışında kalan işsiz, emekli, yurtdışında olan üyelerin ise oda üyelikleri sürer. BMO 1. Olağan Genel Kurulu’nda alınan karar gereği bu üyelere aidat mahsup edilmez. Buna göre bu koşulları barındıran üyelerin bunu ilgili kurumlardan aldıkları belgelerle ibraz etmeleri gerekmektedir.</p>"
			+ "<p>Başvurunuz Yönetim Kurulumuzun 1/8 numaralı toplantısında görüşülmüş ve 1. 2. Ve 3. Koşulları sağlamadığı için üyelikten ayrılma başvurunuzun reddine, işsiz, emekli, yurtdışında olduğunu gösteren belge ekinin tarafımıza gönderilmesinin bildirimine karar verilmiştir.</p>"
			+ "<p>Saygılarımızla,</p>" + "<p></p>" + "<p></p>" + "<p></p>" + "<p><b>$PERSONEL$</b></p>" + "<p>Yönetim Kurulu a.</p>" + "<p>Yönetim Kurulu Yazman Üyesi</p>";

	public static final String _yabanciUyeBildirimYenileme = "<p><b>Sayın $UYEADSOYAD$</b></p>"
			+ "<p>$ADRES$</p>"
			+ "<p><b>$SEHIR$</b></p>"
			+ "<p></p>"
			+ "<p>TMMOB ilgi yazısı ile ...’nde . (..) yıl süreyle çalışmak üzere çalışma izni aldığınız Odamıza iletilmiştir. Yazımızın elinize geçtiği tarihten itibaren en geç bir ay içinde aşağıdaki evrakları Odamıza göndererek izninizin başladığı tarih itibari ile Oda kaydınızı yaptırmanız gerekmektedir.  Bununla birlikte aldığınız ücrette değişiklik olması, işyerinizin değişmesi, işten ayrılmanız veya sigortasız çalıştırılmanız durumlarında Odamıza bilgi vermeniz ve aşağıda belirtilen evrakları 6 ayda bir düzenli olarak Odamıza iletmeniz gerekmektedir. Gerekli evrakları Odamıza göndermediğiniz ve üyelik kaydınızı yaptırmadığınız takdirde almış olduğunuz çalışma izninin iptali için gerekli işlemler başlatılacaktır.</p>"
			+ "<p></p>"
			+ "<p>Gereği için bilgilerinize rica ederiz.</p>"
			+ "<p></p>"
			+ "<p><b>$PERSONEL$</b></p>"
			+ "<p>Yönetim Kurulu a.</p>"
			+ "<p>Yönetim Kurulu Yazman Üyesi</p>"
			+ "<p></p>"
			+ "<span style=\"font-size:9px;\"><p><b>Kayıt İçin Gerekli Evraklar:</b></p>"
			+ "<p>1.	Aylık SSK bildirgesi ve dekont</p>"
			+ "<p>2.	Aylık muhtasar beyanname ve dekont</p>"
			+ "<p>3.	Aylık ücret bordrosu (Odamızca Belirlenen Asgari Aylık Net Ücret $AYLIKNETASGARIUCRET$ TL’dir. $AYLIKNETASGARIUCRET$ TL altı aylık net ücret alan yabancı uyruklu şahsın/şahısların Odamıza Geçici Üyelik Kayıtları yapılamamaktadır)</p>"
			+ "<p>4.	Çalışma ve Sosyal Güvenlik Bakanlığı’ndan almış olduğunuz çalışma izninizin bir örneği.</p>"
			+ "<p>5.	Odamızdan tarafından size iletilen, tarafınızca doldurulmuş Üyelik Kayıt Formu ve 2 fotoğraf.</p>"
			+ "<p>6.	$YABANCIUYEKAYITUCRETI$ TL kayıt ücreti ve çalışma izni aldığınız tarihten itibaren her ay için aidat ücreti (Aylık $YABANCIUYEAIDATBEDELI$ TL) banka ödeme dekontları.</p>"
			+ "<p></p>" + "<b><p>Hesap Bilgileri	:	$HESAPADI$</p>" + "<p>$BANKAADI$ $BANKASUBEADI$/p>" + "<p>IBAN: $IBANNO$</p></b></span>";

	public static final String _bilirkisilikBildirim = "<p><b>İLGİLİ MAKAMA</b></p>"
			+ "<p></p>"
			+ "<p>Aşağıda kimlik dökümü yapılan <b>Bilgisayar Mühendisi</b> Sayın $UYEADSOYAD$ disiplin yönünden meslekten ve sanat icrasından geçici olarak yasaklı olmayıp, üyelik yükümlülüklerini düzenli olarak yerine getirmektedir.</p>"
			+ "<p>Saygılarımızla</p>" + "<p></p>" + "<p><b>$PERSONEL$</b></p>" + "<p>Yönetim Kurulu a.</p>" + "<p>Yönetim Kurulu Yazman Üyesi</p>" + "<p></p>"
			+ "<p><b>TC KİMLİK NO</b> 		: $UYETCKIMLIKNO$</p>" + "<p><b>BABA ADI</b> 		: $UYEBABAAD$</p>"
			+ "<p><b>DOĞUM YERİ - TARİHİ</b> 	: $UYEDOGUMYERI$ – :$UYEDOGUMTARIHI$</p>" + "<p><b>MEZUN OLDUĞU OKUL</b>	: $UYEMEZUNIYETOKUL$</p>"
			+ "<p><b>MEZUNİYET TARİHİ</b> 	: $UYEMEZUNIYETTARIH$</p>" + "<p><b>ODA KAYIT TARİHİ</b> 	: $UYEKAYITTARIH$</p>" + "<p><b>ODA SİCİL NO</b> 		: $UYESICILNO$</p>"
			+ "<p></p>" + "<p>* Üyemizin “Bilirkişilik Yetkilendirme Belgesi”  bulunmamaktadır.</p>" + "<p></p>"
			+ "<p>NOT: * Üzerinde tahrifat yapılan ve fotokopi belgeler geçersizdir.</p>";

	public static final String _ihaleBelgesi = "<p><b>$KURUMADI$</b></p>" + "<p><b>$SEHIR$</b></p>" + "<p></p>" + "<p><strong>ÜYELİK BELGESİ</strong></p>"
			+ "<p><b>TC KİMLİK NO</b> 		: $UYETCKIMLIKNO$</p>" + "<p><b>ADI SOYADI ADI</b> 		: $UYEADSOYAD$</p>" + "<p><b>BABA ADI</b> 		: $UYEBABAAD$</p>"
			+ "<p><b>DOĞUM YERİ - TARİHİ</b> 	: $UYEDOGUMYERI$ – :$UYEDOGUMTARIHI$</p>" + "<p><b>MEZUN OLDUĞU OKUL</b>	: $UYEMEZUNIYETOKUL$</p>"
			+ "<p><b>MEZUNİYET TARİHİ</b> 	: $UYEMEZUNIYETTARIH$</p>" + "<p><b>ODA KAYIT TARİHİ</b> 	: $UYEKAYITTARIH$</p>" + "<p><b>ODA SİCİL NO</b> 		: $UYESICILNO$</p>"
			+ "<p></p>"
			+ "<p>Yukarıda açık kimliği yazılı olan $UYEADSOYAD$ Odamızın $UYESICILNO$ sicil numarasıyla kayıtlı üyesidir. Bu belge ilgilinin isteği üzerine verilmiştir.</p>"
			+ "<p>Saygılarımızla</p>" + "<p></p>" + "<p></p>" + "<p></p>" + "<p><b>$PERSONEL$</b></p>" + "<p>Yönetim Kurulu a.</p>" + "<p>Yönetim Kurulu Yazman Üyesi</p>";

	public static final String _odaUyelikBelgesi = "<p></p>" + "<p><strong>İLGİLİ MAKAMA</strong></p>" + "<p></p>" + "<p></p>" + "<p><b>TC KİMLİK NO</b> 		: $UYETCKIMLIKNO$</p>"
			+ "<p><b>ADI SOYADI ADI</b> 		: $UYEADSOYAD$</p>" + "<p><b>BABA ADI</b> 		: $UYEBABAAD$</p>"
			+ "<p><b>DOĞUM YERİ - TARİHİ</b> 	: $UYEDOGUMYERI$ – :$UYEDOGUMTARIHI$</p>" + "<p><b>MEZUN OLDUĞU OKUL</b>	: $UYEMEZUNIYETOKUL$</p>"
			+ "<p><b>MEZUNİYET TARİHİ</b> 	: $UYEMEZUNIYETTARIH$</p>" + "<p><b>ODA KAYIT TARİHİ</b> 	: $UYEKAYITTARIH$</p>" + "<p><b>ODA SİCİL NO</b> 		: $UYESICILNO$</p>"
			+ "<p></p>"
			+ "<p>Yukarıda açık kimliği yazılı olan $UYEADSOYAD$ Odamızın $UYESICILNO$ sicil numarasıyla kayıtlı üyesidir. Bu belge ilgilinin isteği üzerine verilmiştir.</p>"
			+ "<p>Saygılarımızla</p>" + "<p></p>" + "<p></p>" + "<p></p>" + "<p><b>$PERSONEL$</b></p>" + "<p>Yönetim Kurulu a.</p>" + "<p>Yönetim Kurulu Yazman Üyesi</p>";

	public static final String _odaUyelikBelgesiAdaletKomisyonu = "<p></p>" + "<p><strong>ADLİ YARGI İLK DERECE MAHKEMESİ ADALET KOMİSYONU BAŞKANLIĞINA</strong></p>"
			+ "<p><strong>BİLİRKİŞİLİK BAŞVURUSU İÇİN</strong></p>" + "<p></p>" + "<p><strong>ÜYELİK BELGESİ</strong></p>" + "<p><b>TC KİMLİK NO</b> 		: $UYETCKIMLIKNO$</p>"
			+ "<p><b>ADI SOYADI ADI</b> 		: $UYEADSOYAD$</p>" + "<p><b>BABA ADI</b> 		: $UYEBABAAD$</p>"
			+ "<p><b>DOĞUM YERİ - TARİHİ</b> 	: $UYEDOGUMYERI$ – :$UYEDOGUMTARIHI$</p>" + "<p><b>MEZUN OLDUĞU OKUL</b>	: $UYEMEZUNIYETOKUL$</p>"
			+ "<p><b>MEZUNİYET TARİHİ</b> 	: $UYEMEZUNIYETTARIH$</p>" + "<p><b>ODA KAYIT TARİHİ</b> 	: $UYEKAYITTARIH$</p>" + "<p><b>ODA SİCİL NO</b> 		: $UYESICILNO$</p>"
			+ "<p></p>"
			+ "<p>Yukarıda açık kimliği yazılı olan $UYEADSOYAD$ Odamızın $UYESICILNO$ sicil numarasıyla kayıtlı üyesidir. Bu belge ilgilinin isteği üzerine verilmiştir.</p>"
			+ "<p>Saygılarımızla</p>" + "<p></p>" + "<p></p>" + "<p></p>" + "<p><b>$PERSONEL$</b></p>" + "<p>Yönetim Kurulu a.</p>" + "<p>Yönetim Kurulu Yazman Üyesi</p>";

	public static final String _santiyeSefligiBelgesi = "<p><b>$KURUMADI$</b></p>"
			+ "<p><b>$SEHIR$</b></p>"
			+ "<p></p>"
			+ "<p><strong>ÜYELİK BELGESİ</strong></p>"
			+ "<p><b>TC KİMLİK NO</b> 		: $UYETCKIMLIKNO$</p>"
			+ "<p><b>ADI SOYADI ADI</b> 		: $UYEADSOYAD$</p>"
			+ "<p><b>BABA ADI</b> 		: $UYEBABAAD$</p>"
			+ "<p><b>DOĞUM YERİ - TARİHİ</b> 	: $UYEDOGUMYERI$ – :$UYEDOGUMTARIHI$</p>"
			+ "<p><b>MEZUN OLDUĞU OKUL</b>	: $UYEMEZUNIYETOKUL$</p>"
			+ "<p><b>MEZUNİYET TARİHİ</b> 	: $UYEMEZUNIYETTARIH$</p>"
			+ "<p><b>ODA KAYIT TARİHİ</b> 	: $UYEKAYITTARIH$</p>"
			+ "<p><b>ODA SİCİL NO</b> 		: $UYESICILNO$</p>"
			+ "<p></p>"
			+ "<p>Yukarıda açık kimliği yazılı olan $UYEADSOYAD$ Odamızın $UYESICILNO$ sicil numarasıyla kayıtlı üyesidir. Şantiye şefliği yapmasında sakınca yoktur. Üye ekteki taahhütnamenin şartlarını yerine getirmekle yükümlüdür.</p>"
			+ "<p>Saygılarımızla</p>" + "<p></p>" + "<p></p>" + "<p></p>" + "<p><b>$PERSONEL$</b></p>" + "<p>Yönetim Kurulu a.</p>" + "<p>Yönetim Kurulu Yazman Üyesi</p>";

	public static final String _kurumaGidenGenelEvrak = "<p>$KURUMADI$</p>" + "<p>$SEHIR$</p>";

}// class
