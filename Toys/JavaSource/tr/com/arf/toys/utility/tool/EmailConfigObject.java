package tr.com.arf.toys.utility.tool;

import java.util.Properties;

public class EmailConfigObject {

	private Properties prop;
	private String emailHost;
	private String emailFrom;
	private String emailPass;
	private String emailPort;

	public EmailConfigObject(String hostName, String emailFrom, String emailPass, String emailPort) {
		this.emailHost = hostName;
		this.emailFrom = emailFrom;
		this.emailPass = emailPass;
		this.emailPort = emailPort;

		prop = System.getProperties();
		prop.put("mail.smtp.starttls.enable", "true"); // added this line
		prop.put("mail.smtp.host", emailHost);
		prop.put("mail.smtp.user", emailFrom);
		prop.put("mail.smtp.password", emailPass);
		prop.put("mail.smtp.port", emailPort);
		prop.put("mail.smtp.auth", "true");
		prop.put("mail.debug", "true");
		prop.put("mail.smtp.debug", "true");
		prop.put("mail.smtp.starttls.enable", "true");
		prop.put("mail.transport.protocol", "smtp");
	}

	public Properties getProp() {
		return prop;
	}

	public void setProp(Properties prop) {
		this.prop = prop;
	}

	public String getEmailHost() {
		return emailHost;
	}

	public void setEmailHost(String emailHost) {
		this.emailHost = emailHost;
	}

	public String getEmailFrom() {
		return emailFrom;
	}

	public void setEmailFrom(String emailFrom) {
		this.emailFrom = emailFrom;
	}

	public String getEmailPass() {
		return emailPass;
	}

	public void setEmailPass(String emailPass) {
		this.emailPass = emailPass;
	}

	public String getEmailPort() {
		return emailPort;
	}

	public void setEmailPort(String emailPort) {
		this.emailPort = emailPort;
	}

}
