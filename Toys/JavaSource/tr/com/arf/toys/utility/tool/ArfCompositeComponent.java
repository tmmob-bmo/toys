package tr.com.arf.toys.utility.tool;
 
import javax.el.MethodExpression;
import javax.faces.component.FacesComponent;
import javax.faces.component.UINamingContainer;
import javax.faces.context.FacesContext;
import javax.faces.event.AjaxBehaviorEvent;

import org.primefaces.event.SelectEvent;

@FacesComponent(value = "arfCompositeComponent")
public class ArfCompositeComponent extends UINamingContainer {

	public void ajaxEventListener(AjaxBehaviorEvent event) {   
		if(event != null){
			FacesContext context = FacesContext.getCurrentInstance();
			MethodExpression ajaxEventListener = (MethodExpression) getAttributes().get("ajaxEventListener");		
			ajaxEventListener.invoke(context.getELContext(), new Object[] { event });
		}
	} 
	
	public void handleChanges(SelectEvent event) {
		FacesContext context = FacesContext.getCurrentInstance();
		MethodExpression handleChanges = (MethodExpression) getAttributes().get("handleChanges");  
		handleChanges.invoke(context.getELContext(), new Object[]{event}); 
	} 

}