/*
 * Copyright 2009-2012 Prime Teknoloji.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package tr.com.arf.toys.utility.tool;

import java.io.IOException;
import java.util.List;

import javax.el.MethodExpression;
import javax.faces.component.UIColumn;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFTable;
import org.apache.poi.xwpf.usermodel.XWPFTableCell;
import org.apache.poi.xwpf.usermodel.XWPFTableRow;
import org.primefaces.component.datatable.DataTable;
import org.primefaces.component.export.Exporter;
import org.primefaces.util.Constants;

public class WordExporter extends Exporter {

    @Override
	public void export(FacesContext context, DataTable table, String filename, boolean pageOnly, boolean selectionOnly, int[] excludeColumns, String encodingType, MethodExpression preProcessor, MethodExpression postProcessor) throws IOException {    	
    	XWPFDocument doc = new XWPFDocument();
    	XWPFTable table1 =doc.createTable();
    	List<UIColumn> columns = getColumnsToExport(table, excludeColumns);
        String rowIndexVar = table.getRowIndexVar();
    	if(preProcessor != null) {
    		preProcessor.invoke(context.getELContext(), new Object[]{doc});
    	}

    	addFacetColumns(table1, columns, ColumnType.HEADER, 0);
		exportAll(context, table, columns, table1);
        if(hasColumnFooter(columns)) {
            addFacetColumns(table1, columns, ColumnType.FOOTER,table1.getRowBandSize()+1);
        }
    	
    	table.setRowIndex(-1);
        
        if(rowIndexVar != null) {
            context.getExternalContext().getRequestMap().remove(rowIndexVar);
        }
    	
    	if(postProcessor != null) {
    		postProcessor.invoke(context.getELContext(), new Object[]{doc});
    	}
    	
    	writeWordToResponse((HttpServletResponse)context.getExternalContext().getResponse(), doc, filename);
	}
	
 

    
    protected void exportAll(FacesContext context, DataTable table, List<UIColumn> columns, XWPFTable table1) {
        String rowIndexVar = table.getRowIndexVar();
        int numberOfColumns = columns.size();
        
        int first = table.getFirst();
    	int rowCount = table.getRowCount();
        int rows = table.getRows();
        boolean lazy = table.isLazy();
        int sheetRowIndex = 1;
        XWPFTableRow row ;
        if(lazy) {
            for(int i = 0; i < rowCount; i++) {
                if(i % rows == 0) {
                    table.setFirst(i);
                    table.loadLazyData();
                }

                table.setRowIndex(i);
                if(!table.isRowAvailable()) {
					break;
				}

                if(rowIndexVar != null) {
                    context.getExternalContext().getRequestMap().put(rowIndexVar, i);
                }

                row = table1.getRow(sheetRowIndex++);

                for(int j = 0; j < numberOfColumns; j++) {
                    addColumnValue(row, columns.get(j).getChildren(), j);
                }
                row=table1.createRow();
                
            }
     
            //restore
            table.setFirst(first);
            table.loadLazyData();
        } 
        else {
            for(int i = 0; i < rowCount; i++) {
                table.setRowIndex(i);
                if(!table.isRowAvailable()) {
					break;
				}

                if(rowIndexVar != null) {
                    context.getExternalContext().getRequestMap().put(rowIndexVar, i);
                }
                
                row =table1.createRow();
                row=table1.getRow(sheetRowIndex++);;
                
                
                for(int j = 0; j < numberOfColumns; j++) {
                    addColumnValue(row, columns.get(j).getChildren(), j);
                }
                
            }
            
            //restore
            table.setFirst(first);
        }
    }
    
	protected void addFacetColumns(XWPFTable table, List<UIColumn> columns, ColumnType columnType, int rowIndex) {
//        Row rowHeader = sheet.createRow(rowIndex);
		
		XWPFTableRow rowHeader=table.getRow(rowIndex);
        for(int i = 0; i < columns.size(); i++) {            
            addColumnValue(rowHeader, columns.get(i).getFacet(columnType.facet()), i);
        }
    }
	
	
    @SuppressWarnings("unused")
	protected void addColumnValue(XWPFTableRow row, UIComponent component, int index) {
    	XWPFTableCell cell=row.createCell();
        String value = component == null ? "" : exportValue(FacesContext.getCurrentInstance(), component);
        row.getCell(index).setText(value);
    }
    
    protected void addColumnValue(XWPFTableRow row, List<UIComponent> components, int index) {
        StringBuilder builder = new StringBuilder();
        for(UIComponent component : components) {
        	if(component.isRendered()) {
                String value = exportValue(FacesContext.getCurrentInstance(), component);
                
                if(value != null) {
					builder.append(value);
				}
            }
		}  
        row.getCell(index).setText(builder.toString());
    }
    
    protected void writeWordToResponse(HttpServletResponse response, XWPFDocument generatedWord, String filename) throws IOException {
        response.setContentType("application/vnd.ms-word");
        response.setHeader("Expires", "0");
        response.setHeader("Cache-Control","must-revalidate, post-check=0, pre-check=0");
        response.setHeader("Pragma", "public");
        response.setHeader("Content-disposition", "attachment;filename="+ filename + ".doc");
        response.addCookie(new Cookie(Constants.DOWNLOAD_COOKIE, "true"));
        generatedWord.write(response.getOutputStream());
    }
}