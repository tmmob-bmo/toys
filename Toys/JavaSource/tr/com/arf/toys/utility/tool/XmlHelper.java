package tr.com.arf.toys.utility.tool;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.log4j.Logger;
import org.w3c.dom.CharacterData;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import tr.com.arf.framework.db._management.DBOperator;
import tr.com.arf.framework.db.enumerated._common.EvetHayir;
import tr.com.arf.framework.utility.tool.DateUtil;
import tr.com.arf.toys.db.enumerated.iletisim.AdresTipi;
import tr.com.arf.toys.db.enumerated.iletisim.AdresTuru;
import tr.com.arf.toys.db.model.iletisim.Adres;
import tr.com.arf.toys.db.model.system.Ilce;
import tr.com.arf.toys.db.model.system.Sehir;
import tr.com.arf.toys.db.nonEntityModel.PojoAdres;
import tr.com.arf.toys.db.nonEntityModel.PojoKisi;
import tr.com.arf.toys.utility.logUtils.LogService;

public class XmlHelper {

	protected static Logger logger = Logger.getLogger(XmlHelper.class);

	public static Adres kisiAdresAktar(String kpsAdres, AdresTuru adresTuru, EvetHayir varsayilan) throws Exception {
		if (adresTuru == AdresTuru._KPSADRESI) {
			PojoAdres kpsPojo = XmlHelper.readXmlStringForAdres(kpsAdres, AdresTuru._KPSADRESI);
			if (kpsPojo != null) {
				String hataBilgisiKodu = kpsPojo.getHataBilgisiKodu();
				if (hataBilgisiKodu == null || hataBilgisiKodu.trim().equals("")) {
					return kisiAdresAktar(kpsPojo, adresTuru, varsayilan);
				}
			}
		} else if (adresTuru == AdresTuru._KPSDIGERADRESI) {
			PojoAdres kpsPojo = XmlHelper.readXmlStringForAdres(kpsAdres, AdresTuru._KPSDIGERADRESI);
			if (kpsPojo != null) {
				String hataBilgisiKodu = kpsPojo.getHataBilgisiKodu();
				if (hataBilgisiKodu == null || hataBilgisiKodu.trim().equals("")) {
					return kisiAdresAktar(kpsPojo, adresTuru, varsayilan);
				}
			}
		}
		return null;
	}

	private static Adres kisiAdresAktar(PojoAdres kpsPojo, AdresTuru adresTuru, EvetHayir varsayilan) throws Exception {
		DBOperator dbOperator = DBOperator.getInstance();
		Adres yeniAdres = new Adres();
		yeniAdres.setAdresturu(adresTuru);
		yeniAdres.setAdrestipi(kpsPojo.getAdrestipi());
		yeniAdres.setAcikAdres(kpsPojo.getAcikAdres());
		yeniAdres.setAdresNo(kpsPojo.getAdresNo());
		if (!isEmpty(kpsPojo.getBeyanTarihi()) && !kpsPojo.getBeyanTarihi().equalsIgnoreCase("null/null/null")) {
			yeniAdres.setBeyanTarihi(DateUtil.createDateObject(kpsPojo.getBeyanTarihi()));
		}
		yeniAdres.setBinaAda(kpsPojo.getBinaAda());
		yeniAdres.setBinaBlokAdi(kpsPojo.getBinaBlokAdi());
		yeniAdres.setBinaKodu(kpsPojo.getBinaKodu());
		yeniAdres.setBinaPafta(kpsPojo.getBinaPafta());
		yeniAdres.setBinaParsel(kpsPojo.getBinaParsel());
		yeniAdres.setBinaPafta(kpsPojo.getBinaPafta());
		yeniAdres.setBinaSiteAdi(kpsPojo.getBinaSiteAdi());
		yeniAdres.setCsbm(kpsPojo.getCsbm());
		yeniAdres.setCsbmKodu(kpsPojo.getCsbmKodu());
		yeniAdres.setDisKapiNo(kpsPojo.getDisKapiNo());
		yeniAdres.setIcKapiNo(kpsPojo.getIcKapiNo());
		if (!isEmpty(kpsPojo.getIlKodu()) && isNumber(kpsPojo.getIlKodu().trim())) {
			String ilKodu = kpsPojo.getIlKodu();
			if (dbOperator.recordCount(Sehir.class.getSimpleName(), "o.ilKodu='" + ilKodu + "'") > 0) {
				yeniAdres.setSehirRef((Sehir) dbOperator.find(Sehir.class.getSimpleName(), "o.ilKodu='" + ilKodu + "'"));
			} else {
				if (ilKodu.substring(0, 1).equals("0")) {
					ilKodu = ilKodu.substring(1);
					if (dbOperator.recordCount(Sehir.class.getSimpleName(), "o.ilKodu='" + ilKodu + "'") > 0) {
						yeniAdres.setSehirRef((Sehir) dbOperator.find(Sehir.class.getSimpleName(), "o.ilKodu='" + ilKodu + "'"));
					}
				}
			}
		}
		if (!isEmpty(kpsPojo.getIlceKodu()) && isNumber(kpsPojo.getIlceKodu().trim())) {
			String ilceKodu = kpsPojo.getIlceKodu();
			if (dbOperator.recordCount(Ilce.class.getSimpleName(), "o.ilcekodu='" + kpsPojo.getIlceKodu() + "'") > 0) {
				yeniAdres.setIlceRef((Ilce) dbOperator.find(Ilce.class.getSimpleName(), "o.ilcekodu='" + kpsPojo.getIlceKodu() + "'"));
			} else {
				if (ilceKodu.substring(0, 1).equals("0")) {
					ilceKodu = ilceKodu.substring(1);
					if (dbOperator.recordCount(Ilce.class.getSimpleName(), "o.ilcekodu='" + kpsPojo.getIlceKodu() + "'") > 0) {
						yeniAdres.setIlceRef((Ilce) dbOperator.find(Ilce.class.getSimpleName(), "o.ilcekodu='" + kpsPojo.getIlceKodu() + "'"));
					}
				}
			}
		}
		yeniAdres.setKpsGuncellemeTarihi(new Date());
		yeniAdres.setMahalle(kpsPojo.getMahalle());
		yeniAdres.setMahalleKodu(kpsPojo.getMahalleKodu());
		if (kpsPojo.getTasinmaTarihi() != null && !kpsPojo.getTasinmaTarihi().equalsIgnoreCase("null/null/null")) {
			yeniAdres.setTasinmaTarihi(DateUtil.createDateObject(kpsPojo.getTasinmaTarihi()));
		}
		if (kpsPojo.getTescilTarihi() != null && !kpsPojo.getTescilTarihi().equalsIgnoreCase("null/null/null")) {
			yeniAdres.setTescilTarihi(DateUtil.createDateObject(kpsPojo.getTescilTarihi()));
		}
		yeniAdres.setVarsayilan(varsayilan);
		return yeniAdres;
	}

	public static PojoAdres readXmlStringForAdres(String xmlRecords, AdresTuru adresTuru) throws Exception {
		if (adresTuru == AdresTuru._KPSADRESI) {
			if (!xmlRecords.contains("<YerlesimYeriAdresi")) {
				return null;
			}
			xmlRecords = xmlRecords.substring(xmlRecords.indexOf("<YerlesimYeriAdresi"), xmlRecords.indexOf("</YerlesimYeriAdresi>") + 21);
		} else if (adresTuru == AdresTuru._KPSDIGERADRESI) {
			if (!xmlRecords.contains("<KisiAdresBilgisi")) {
				return null;
			}
			xmlRecords = xmlRecords.substring(xmlRecords.indexOf("<KisiAdresBilgisi"), xmlRecords.indexOf("</KisiAdresBilgisi>") + 19);
		} else {
			logger.error("KPS'den parse edilemeyen adres geldi.");
			return null;
		}

		PojoAdres adres = new PojoAdres();
		try {
			DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
			InputSource is = new InputSource();
			is.setCharacterStream(new StringReader(xmlRecords));

			Document doc = db.parse(is);
			doc.getDocumentElement().normalize();
			Map<String, Object> domValues = new HashMap<String, Object>();
			if (doc.hasChildNodes()) {
				domValues = printNote(doc.getChildNodes(), new HashMap<String, Object>());
			}
			Map<String, Object> domValuesNew = new HashMap<String, Object>();
			for (Map.Entry<String, Object> entry : domValues.entrySet()) {
				if (entry.getKey().endsWith("_BinaAda")) {
					domValuesNew.put("_BinaAda", entry.getValue());
				} else if (entry.getKey().endsWith("_BinaBlokAdi")) {
					domValuesNew.put("_BinaBlokAdi", entry.getValue());
				} else if (entry.getKey().endsWith("_BinaKodu")) {
					domValuesNew.put("_BinaKodu", entry.getValue());
				} else if (entry.getKey().endsWith("_BinaPafta")) {
					domValuesNew.put("_BinaPafta", entry.getValue());
				} else if (entry.getKey().endsWith("_BinaParsel")) {
					domValuesNew.put("_BinaParsel", entry.getValue());
				} else if (entry.getKey().endsWith("_BinaSiteAdi")) {
					domValuesNew.put("_BinaSiteAdi", entry.getValue());
				} else if (entry.getKey().endsWith("_Csbm")) {
					domValuesNew.put("_Csbm", entry.getValue());
				} else if (entry.getKey().endsWith("_CsbmKodu")) {
					domValuesNew.put("_CsbmKodu", entry.getValue());
				} else if (entry.getKey().endsWith("_DisKapiNo")) {
					domValuesNew.put("_DisKapiNo", entry.getValue());
				} else if (entry.getKey().endsWith("_IcKapiNo")) {
					domValuesNew.put("_IcKapiNo", entry.getValue());
				} else if (entry.getKey().endsWith("_IlKodu")) {
					domValuesNew.put("_IlKodu", entry.getValue());
				} else if (entry.getKey().endsWith("_IlceKodu")) {
					domValuesNew.put("_IlceKodu", entry.getValue());
				} else if (entry.getKey().endsWith("_Mahalle")) {
					domValuesNew.put("_Mahalle", entry.getValue());
				} else if (entry.getKey().endsWith("_MahalleKodu")) {
					domValuesNew.put("_MahalleKodu", entry.getValue());
				} else if (entry.getKey().endsWith("_TasinmaTarihi_Gun")) {
					domValuesNew.put("_TasinmaTarihi_Gun", entry.getValue());
				} else if (entry.getKey().endsWith("_TasinmaTarihi_Ay")) {
					domValuesNew.put("_TasinmaTarihi_Ay", entry.getValue());
				} else if (entry.getKey().endsWith("_TasinmaTarihi_Yil")) {
					domValuesNew.put("_TasinmaTarihi_Yil", entry.getValue());
				} else if (entry.getKey().endsWith("_TescilTarihi_Gun")) {
					domValuesNew.put("_TescilTarihi_Gun", entry.getValue());
				} else if (entry.getKey().endsWith("_TescilTarihi_Ay")) {
					domValuesNew.put("_TescilTarihi_Ay", entry.getValue());
				} else if (entry.getKey().endsWith("_TescilTarihi_Yil")) {
					domValuesNew.put("_TescilTarihi_Yil", entry.getValue());
				} else {
					domValuesNew.put(entry.getKey(), entry.getValue());
				}
			}

			if (domValuesNew.get("KisiAdresBilgisi_HataBilgisi_Kod") != null || domValuesNew.get("HataBilgisi") != null) {
				// System.out.println("hata bilgisi kodu var." + domValuesNew.get("KisiAdresBilgisi_HataBilgisi_Kod"));
				adres.setHataBilgisiKodu(domValuesNew.get("KisiAdresBilgisi_HataBilgisi_Kod").toString());
			} else {
				String prefix = "KisiAdresBilgisi";
				if (adresTuru == AdresTuru._KPSADRESI) {
					prefix = "YerlesimYeriAdresi";
				}
				Object adresTipiKod = domValuesNew.get(prefix + "_AdresTip_Kod");
				if (adresTipiKod != null) {
					adres.setAdrestipi(AdresTipi.getWithCode(Integer.parseInt(adresTipiKod.toString())));
				} else {
					adres.setAdrestipi(null);
				}
				Object acikAdres = domValuesNew.get(prefix + "_AcikAdres");
				if (acikAdres != null) {
					adres.setAcikAdres(acikAdres.toString());
				} else {
					adres.setAcikAdres(null);
				}
				Object adresNo = domValuesNew.get(prefix + "_AdresNo");
				if (adresNo != null) {
					adres.setAdresNo(adresNo.toString());
				} else {
					adres.setAdresNo(null);
				}
				Object beyanTarihiGun = domValuesNew.get(prefix + "_BeyanTarihi_Gun");
				if (beyanTarihiGun != null) {
					adres.setBeyanTarihiGun(beyanTarihiGun.toString());
				} else {
					adres.setBeyanTarihiGun(null);
				}
				Object beyanTarihiAy = domValuesNew.get(prefix + "_BeyanTarihi_Ay");
				if (beyanTarihiAy != null) {
					adres.setBeyanTarihiAy(beyanTarihiAy.toString());
				} else {
					adres.setBeyanTarihiAy(null);
				}
				Object beyanTarihiYil = domValuesNew.get(prefix + "_BeyanTarihi_Yil");
				if (beyanTarihiYil != null) {
					adres.setBeyanTarihiYil(beyanTarihiYil.toString());
				} else {
					adres.setBeyanTarihiYil(null);
				}
				if (domValuesNew.get("_BinaAda") != null) {
					adres.setBinaAda(domValuesNew.get("_BinaAda").toString());
				}
				if (domValuesNew.get("_BinaBlokAdi") != null) {
					adres.setBinaBlokAdi(domValuesNew.get("_BinaBlokAdi").toString());
				}
				if (domValuesNew.get("_BinaKodu") != null) {
					adres.setBinaKodu(domValuesNew.get("_BinaKodu").toString());
				}
				if (domValuesNew.get("_BinaPafta") != null) {
					adres.setBinaPafta(domValuesNew.get("_BinaPafta").toString());
				}
				if (domValuesNew.get("_BinaParsel") != null) {
					adres.setBinaParsel(domValuesNew.get("_BinaParsel").toString());
				}
				if (domValuesNew.get("_BinaSiteAdi") != null) {
					adres.setBinaSiteAdi(domValuesNew.get("_BinaSiteAdi").toString());
				}
				if (domValuesNew.get("_Csbm") != null) {
					adres.setCsbm(domValuesNew.get("_Csbm").toString());
				}
				if (domValuesNew.get("_CsbmKodu") != null) {
					adres.setCsbmKodu(domValuesNew.get("_CsbmKodu").toString());
				}
				if (domValuesNew.get("_DisKapiNo") != null) {
					adres.setDisKapiNo(domValuesNew.get("_DisKapiNo").toString());
				}
				if (domValuesNew.get("_IcKapiNo") != null) {
					adres.setIcKapiNo(domValuesNew.get("_IcKapiNo").toString());
				}
				if (domValuesNew.get("_IlKodu") != null) {
					adres.setIlKodu(domValuesNew.get("_IlKodu").toString());
				}
				if (domValuesNew.get("_IlceKodu") != null) {
					adres.setIlceKodu(domValuesNew.get("_IlceKodu").toString());
				}
				if (domValuesNew.get("_Mahalle") != null) {
					adres.setMahalle(domValuesNew.get("_Mahalle").toString());
				}
				if (domValuesNew.get("_MahalleKodu") != null) {
					adres.setMahalleKodu(domValuesNew.get("_MahalleKodu").toString());
				}
				if (domValuesNew.get("_TasinmaTarihi_Gun") != null) {
					adres.setTasinmaTarihiGun(domValuesNew.get("_TasinmaTarihi_Gun").toString());
				}
				if (domValuesNew.get("_TasinmaTarihi_Ay") != null) {
					adres.setTasinmaTarihiAy(domValuesNew.get("_TasinmaTarihi_Ay").toString());
				}
				if (domValuesNew.get("_TasinmaTarihi_Yil") != null) {
					adres.setTasinmaTarihiYil(domValuesNew.get("_TasinmaTarihi_Yil").toString());
				}
				if (domValuesNew.get("_TescilTarihi_Gun") != null) {
					adres.setTescilTarihiGun(domValuesNew.get("_TescilTarihi_Gun").toString());
				}
				if (domValuesNew.get("_TescilTarihi_Ay") != null) {
					adres.setTescilTarihiAy(domValuesNew.get("_TescilTarihi_Ay").toString());
				}
				if (domValuesNew.get("_TescilTarihi_Yil") != null) {
					adres.setTescilTarihiYil(domValuesNew.get("_TescilTarihi_Yil").toString());
				}

			}
			return adres;
		} catch (Exception exc) {
			logger.error("ERROR :" + exc.getMessage(), exc);
		}
		return adres;
	}

	public static PojoKisi readXmlStringOld(String xmlRecords) throws Exception {
		PojoKisi kisi = new PojoKisi();
		try {
			xmlRecords = xmlRecords.substring(xmlRecords.indexOf("<?xml"));
			DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
			InputSource is = new InputSource();
			is.setCharacterStream(new StringReader(xmlRecords));

			Document doc = db.parse(is);
			doc.getDocumentElement().normalize();
			Map<String, Object> domValues = new HashMap<String, Object>();
			if (doc.hasChildNodes()) {
				domValues = printNote(doc.getChildNodes(), new HashMap<String, Object>());
			}
			if (domValues.get("HataBilgisi_Kod") != null || domValues.get("HataBilgisi") != null) {
				// System.out.println("hata bilgisi kodu var.");
				kisi.setHataBilgisiKodu(domValues.get("HataBilgisi_Kod").toString());
			} else {
				if (domValues.get("Kod") != null && domValues.get("Kod").toString().equals("9") || domValues.get("TemelBilgisi_Ad") == null || domValues.get("TemelBilgisi_Soyad") == null) {
					// Servis cagirma yetkisi yok, kullanici adi veya sifre hatali.
					kisi.setHataBilgisiKodu("9");
				} else {
					kisi.setAd(domValues.get("TemelBilgisi_Ad").toString());
					kisi.setSoyad(domValues.get("TemelBilgisi_Soyad").toString());

					kisi.setCinsiyetKod(domValues.get("TemelBilgisi_Cinsiyet_Kod").toString());
					kisi.setCinsiyetAciklama(domValues.get("TemelBilgisi_Cinsiyet_Aciklama").toString());
					kisi.setTcKimlikNo(Long.parseLong(domValues.get("TCKimlikNo").toString()));
					kisi.setBabaAdi(domValues.get("TemelBilgisi_BabaAd").toString());
					kisi.setAnaAdi(domValues.get("TemelBilgisi_AnneAd").toString());
					kisi.setMedeniHalKodu(domValues.get("DurumBilgisi_MedeniHal_Kod").toString());
					kisi.setMedeniHalAciklamasi(domValues.get("DurumBilgisi_MedeniHal_Aciklama").toString());
					kisi.setDogumYeri(domValues.get("TemelBilgisi_DogumYer").toString());
					kisi.setOlumTarihiGun(domValues.get("DurumBilgisi_OlumTarih_Gun").toString());
					kisi.setOlumTarihiAy(domValues.get("DurumBilgisi_OlumTarih_Ay").toString());
					kisi.setOlumTarihiYil(domValues.get("DurumBilgisi_OlumTarih_Yil").toString());

					kisi.setDogumTarihiGun(domValues.get("TemelBilgisi_DogumTarih_Gun").toString());
					kisi.setDogumTarihiAy(domValues.get("TemelBilgisi_DogumTarih_Ay").toString());
					kisi.setDogumTarihiYil(domValues.get("TemelBilgisi_DogumTarih_Yil").toString());

					if (!Objects.equals(domValues.get("BabaTCKimlikNo").toString(), "")) {
						kisi.setBabaTCKimlikNo(Long.parseLong(domValues.get("BabaTCKimlikNo").toString()));
					}
					if (!Objects.equals(domValues.get("AnneTCKimlikNo").toString(), "")) {
						kisi.setAnneTCKimlikNo(Long.parseLong(domValues.get("AnneTCKimlikNo").toString()));
					}
					if (!Objects.equals(domValues.get("EsTCKimlikNo").toString(), "")) {
						kisi.setEsTCKimlikNo(Long.parseLong(domValues.get("EsTCKimlikNo").toString()));
					}
					kisi.setDurumKod(domValues.get("DurumBilgisi_Durum_Kod").toString());
					kisi.setDurumAciklama(domValues.get("DurumBilgisi_Durum_Aciklama").toString());
					kisi.setNufusaKayitliOlduguIlceKodu(domValues.get("KayitYeriBilgisi_Ilce_Kod").toString());
					kisi.setNufusaKayitliOlduguIlceAdi(domValues.get("KayitYeriBilgisi_Ilce_Aciklama").toString());
					kisi.setNufusaKayitliOlduguIlKodu(domValues.get("KayitYeriBilgisi_Il_Kod").toString());
					kisi.setNufusaKayitliOlduguIlAdi(domValues.get("KayitYeriBilgisi_Il_Aciklama").toString());
					kisi.setCiltNo(domValues.get("KayitYeriBilgisi_Cilt_Kod").toString());
					kisi.setCiltAciklama(domValues.get("KayitYeriBilgisi_Cilt_Aciklama").toString());
					kisi.setMahalle(domValues.get("KayitYeriBilgisi_Cilt_Aciklama").toString());
					kisi.setAileSiraNo(domValues.get("KayitYeriBilgisi_AileSiraNo").toString());
					kisi.setBireySiraNo(domValues.get("KayitYeriBilgisi_BireySiraNo").toString());
				}
			}
			return kisi;
		} catch (NullPointerException ne) {
			LogService.logYaz("NullPointerException @readXmlString of XMLHELPER : " + ne.getMessage(), "XmlHelper", ne);
			System.out.println(ne.getMessage());
			return null;
		} catch (Exception e) {
			LogService.logYaz("Exception @readXmlString of XMLHELPER : " + e.getMessage(), "XmlHelper", e);
			System.out.println(e.getMessage());
		}
		return kisi;
	}

	public static String getCharacterDataFromElement(Element e) {
		Node child = e.getFirstChild();
		if (child instanceof CharacterData) {
			CharacterData cd = (CharacterData) child;
			return cd.getData();
		}
		return "";
	}

	public static void readGarantiXMLResponse(String xmlRecords) throws Exception {
		try {
			DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
			InputSource is = new InputSource();
			is.setCharacterStream(new StringReader(xmlRecords));

			Document doc = db.parse(is);
			doc.getDocumentElement().normalize();
			Map<String, Object> domValues = new HashMap<String, Object>();
			if (doc.hasChildNodes()) {
				domValues = printNote(doc.getChildNodes(), new HashMap<String, Object>());
			}

			for (Entry<String, Object> entry : domValues.entrySet()) {
				System.out.println(entry.getKey().replace("GVPSResponse_", "") + " : " + entry.getValue());
			}

			if (domValues.get("GVPSResponse_OrderID") != null) {
				System.out.println("OrderID :" + domValues.get("GVPSResponse_OrderID"));
			}
		} catch (NullPointerException ne) {
			LogService.logYaz("NullPointerException @readXmlString of XMLHELPER : " + ne.getMessage(), "XmlHelper", ne);
			System.out.println(ne.getMessage());
		} catch (Exception e) {
			LogService.logYaz("Exception @readXmlString of XMLHELPER : " + e.getMessage(), "XmlHelper", e);
			System.out.println(e.getMessage());
		}
	}

	private static String getParentNodeName(Node node) {
		StringBuilder result = new StringBuilder("");
		ArrayList<Node> hieararchialNodeList = new ArrayList<Node>();
		while (true) {
			if (node.getParentNode() == null || node.getParentNode().getNodeType() != Node.ELEMENT_NODE) {
				break;
			} else {
				try {
					hieararchialNodeList.add(node.getParentNode());
					node = node.getParentNode();
				} catch (Exception e) {
					break;
				}
			}
		}
		Collections.reverse(hieararchialNodeList);
		for (Node n : hieararchialNodeList) {
			if (!n.getNodeName().equalsIgnoreCase("KisiBilgisi")) {
				result.append(n.getNodeName()).append("_");
			}
		}
		return result.toString();
	}

	private static Map<String, Object> printNote(NodeList nodeList, Map<String, Object> resultMap) {
		for (int count = 0; count < nodeList.getLength(); count++) {
			Node tempNode = nodeList.item(count);
			if (tempNode.getNodeType() == Node.ELEMENT_NODE) {
				if (!tempNode.getNodeName().equalsIgnoreCase("ExtensionData")) {
					if (tempNode.getParentNode() != null) {
						resultMap.put(getParentNodeName(tempNode) + tempNode.getNodeName(), tempNode.getTextContent());
					} else {
						resultMap.put(tempNode.getNodeName(), tempNode.getTextContent());
					}
				}
				if (tempNode.hasChildNodes()) {
					printNote(tempNode.getChildNodes(), resultMap);
				}

			}

		}
		return resultMap;

	}

	private static boolean isEmpty(String str) {
		return str == null || str.trim().length() == 0;
	}

	private static boolean isNumber(String s) {
		if (s == null || s.trim().length() == 0) {
			return false;
		}
		try {
			Integer.parseInt(s);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

}
