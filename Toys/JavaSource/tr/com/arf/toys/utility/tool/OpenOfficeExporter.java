/*
 * Copyright 2009-2012 Prime Teknoloji.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package tr.com.arf.toys.utility.tool;

import java.io.IOException;
import java.util.List;

import javax.el.MethodExpression;
import javax.faces.component.UIColumn;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;

import org.odftoolkit.odfdom.doc.OdfTextDocument;
import org.odftoolkit.odfdom.doc.table.OdfTable;
import org.odftoolkit.odfdom.doc.table.OdfTableCell;
import org.odftoolkit.odfdom.doc.table.OdfTableRow;
import org.primefaces.component.datatable.DataTable;
import org.primefaces.component.export.Exporter;
import org.primefaces.util.Constants;



public class OpenOfficeExporter extends Exporter {

    @Override
	public void export(FacesContext context, DataTable table, String filename, boolean pageOnly, boolean selectionOnly, int[] excludeColumns, String encodingType, MethodExpression preProcessor, MethodExpression postProcessor) throws IOException {    	
    
    	OdfTextDocument odt=null;
		try {
				odt = OdfTextDocument.newTextDocument();
				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		OdfTable table1=OdfTable.newTable(odt);
    	List<UIColumn> columns = getColumnsToExport(table, excludeColumns);
        String rowIndexVar = table.getRowIndexVar();
    	if(preProcessor != null) {
    		preProcessor.invoke(context.getELContext(), new Object[]{odt});
    	}
    	addFacetColumns(table1, columns, ColumnType.HEADER, 0);
        exportAll(context, table, columns, table1);
    	
    	if(hasColumnFooter(columns)) {
            addFacetColumns(table1, columns, ColumnType.FOOTER,table1.getRowCount()+1);
        }
    	table.setRowIndex(-1);
    	
        if(rowIndexVar != null) {
            context.getExternalContext().getRequestMap().remove(rowIndexVar);
        }
    	
    	if(postProcessor != null) {
    		postProcessor.invoke(context.getELContext(), new Object[]{odt});
    	}
    	
    	writeOpenOfficeToResponse((HttpServletResponse)context.getExternalContext().getResponse(), odt, filename);
	}

    
    protected void exportAll(FacesContext context, DataTable table, List<UIColumn> columns, OdfTable table1) {
        String rowIndexVar = table.getRowIndexVar();
        int numberOfColumns = columns.size();
        int first = table.getFirst();
    	int rowCount = table.getRowCount();
        int rows = table.getRows();
        boolean lazy = table.isLazy();
        int sheetRowIndex = 1;
        
        OdfTableRow row;
        if(lazy) {
            for(int i = 0; i < rowCount; i++) {
                if(i % rows == 0) {
                    table.setFirst(i);
                    table.loadLazyData();
                }
                table.setRowIndex(i);
                if(!table.isRowAvailable()) {
					break;
				}
                if(rowIndexVar != null) {
                    context.getExternalContext().getRequestMap().put(rowIndexVar, i);
                }
                row = table1.getRowByIndex(sheetRowIndex++);
                for(int j = 0; j < numberOfColumns; j++) {
                    addColumnValue(row, columns.get(j).getChildren(), j);
                }
                row=table1.appendRow();
            }
            table.setFirst(first);
            table.loadLazyData();
        } 
        else {
            for(int i = 0; i < rowCount; i++) {
                table.setRowIndex(i);
                if(!table.isRowAvailable()) {
					break;
				}

                if(rowIndexVar != null) {
                    context.getExternalContext().getRequestMap().put(rowIndexVar, i);
                }
                row =table1.appendRow();
                row=table1.getRowByIndex(sheetRowIndex++);
                
                for(int j = 0; j < numberOfColumns; j++) {
                    addColumnValue(row, columns.get(j).getChildren(), j);
                }
            }
            
            table.setFirst(first);
        }
    }
    
	protected void addFacetColumns(OdfTable table, List<UIColumn> columns, ColumnType columnType, int rowIndex) {
		OdfTableRow rowHeader=table.getRowByIndex(rowIndex);
        for(int i = 0; i < columns.size(); i++) {            
            addColumnValue(rowHeader, columns.get(i).getFacet(columnType.facet()), i);
        }
    }
	
	
    @SuppressWarnings("unused")
	protected void addColumnValue(OdfTableRow row, UIComponent component, int index) {
    	OdfTableCell cell=row.getCellByIndex(index);
        String value = component == null ? "" : exportValue(FacesContext.getCurrentInstance(), component);
        row.getCellByIndex(index).setDisplayText(value);
    }
    
    protected void addColumnValue(OdfTableRow row, List<UIComponent> components, int index) {
        StringBuilder builder = new StringBuilder();
        for(UIComponent component : components) {
        	if(component.isRendered()) {
                String value = exportValue(FacesContext.getCurrentInstance(), component);
                if(value != null) {
					builder.append(value);
				}
            }
		}  
        row.getCellByIndex(index).setDisplayText(builder.toString());
    }
    
    protected void writeOpenOfficeToResponse(HttpServletResponse response, OdfTextDocument generatedOpenOffice, String filename) throws IOException {
        response.setContentType("application/vnd.openoffice");
        response.setHeader("Expires", "0");
        response.setHeader("Cache-Control","must-revalidate, post-check=0, pre-check=0");
        response.setHeader("Pragma", "public");
        response.setHeader("Content-disposition", "attachment;filename="+ filename + ".odt");
        response.addCookie(new Cookie(Constants.DOWNLOAD_COOKIE, "true"));
        try {
			generatedOpenOffice.save(response.getOutputStream());
        } catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    
}