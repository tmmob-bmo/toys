package tr.com.arf.toys.utility.tool;

import java.util.ArrayList;

import org.apache.log4j.Logger;
import org.tempuri.Tmmob_x0020_KPS_x0020_ServiceV20SoapProxy;

import tr.com.arf.framework.db._management.DBOperator;
import tr.com.arf.framework.utility.tool.StringUtil;
import tr.com.arf.toys.db.model.system.Ilce;
import tr.com.arf.toys.db.model.system.Sehir;
import tr.com.arf.toys.db.model.system.Ulke;
import tr.com.arf.toys.service.application.KPSService;
import tr.com.arf.toys.service.application.ManagedBeanLocator;

public class KPSSehirIlceUpdater {
    protected static Logger logger = Logger.getLogger(KPSSehirIlceUpdater.class);


	private static DBOperator getDBOperator(){
		return ManagedBeanLocator.locateSessionController().getDBOperator();
	}
	
	
	public static void sehirGuncelle(Ulke ulke) throws Exception{
        KPSService kpsService = KPSService.getInstance();
        String xml = kpsService.ilBilgisiGetir();
        // todo böyle bir kodlama olamaz!!!
        xml = xml.replace("﻿<?xml version=\"1.0\" encoding=\"utf-8\"?><IlSonucu xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\"><ExtensionData />", "");
		xml = xml.replace("</IlSonucu>", "");
		xml = xml.replace("<ExtensionData />", "");
		xml = xml.replace("<SorguSonucu>", "");
		xml = xml.replace("</SorguSonucu>", "");
		xml = xml.replace("<Il><Ad>", "<IlAdi>");
		xml = xml.replace("</Ad><Kod>", "_");
		xml = xml.replace("</Kod></Il>", "</IlAdi>");
		xml = xml.trim();	 
		
		ArrayList<String> iller = new ArrayList<String>();
		String il = "";
		while(xml.length() > 7){
			xml = xml.substring(7);
			il = xml.substring(0, xml.indexOf("</IlAdi>"));
			xml = xml.substring(8 + il.length());
			iller.add(il);
		} 
		 
		String ilAdi = "";
		String plakaKodu = "";
		for(String s : iller){
			ilAdi = s.substring(0, s.indexOf("_"));
			plakaKodu =s.substring(s.indexOf("_") + 1);
			String whereCon = "upper(o.ad) = upper('"+ ilAdi +"') " + "AND (o.ilKodu = '" + plakaKodu + "'";
			if(plakaKodu.length() == 1){
				whereCon += " OR o.ilKodu = '0" + plakaKodu + "')";
				plakaKodu = StringUtil.fillStringLeft(s.substring(s.indexOf("_") + 1), "0", 2);
				
			} else {
				whereCon += ")";
			}
			if(getDBOperator().recordCount(Sehir.class.getSimpleName(), whereCon) == 0){
                try {
                    Sehir sehir = new Sehir();
                    sehir.setAd(ilAdi);
                    sehir.setIlKodu(plakaKodu);
//				sehir.setUlkeRef(ulke);
                    getDBOperator().insert(sehir);
                    ilceGuncelle(sehir);
                } catch (Exception exc) {
                    logger.error(exc.toString(), exc);
                }
            } else {
                try {
                    Sehir sehir = (Sehir) getDBOperator().find(Sehir.class.getSimpleName(), "upper(o.ad) = upper('"+ ilAdi +"') " + " AND o.ilKodu = '" + plakaKodu + "'");
                    ilceGuncelle(sehir);
                } catch (Exception exc) {
                    logger.error(exc.toString(), exc);
                }
            }
		} 
	}
	
	public static void ilceGuncelle(Sehir sehir) throws Exception{
		Tmmob_x0020_KPS_x0020_ServiceV20SoapProxy kpsServis = new Tmmob_x0020_KPS_x0020_ServiceV20SoapProxy(); 
		String xml = kpsServis.ileBagliIlceBilgisiGetir(Integer.parseInt(sehir.getIlKodu()), ManagedBeanLocator.locateSessionController().getKpsKullaniciAdi(), ManagedBeanLocator.locateSessionController().getKpsSifre()); 
		xml = xml.replace("﻿<?xml version=\"1.0\" encoding=\"utf-8\"?><IlceSonucu xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\">", "");
		xml = xml.replace("</IlceSonucu>", "");
		xml = xml.replace("<ExtensionData />", "");
		xml = xml.replace("<SorguSonucu>", "");
		xml = xml.replace("</SorguSonucu>", "");
		xml = xml.replace("<Ilce><Ad>", "<IlceAdi>");
		xml = xml.replace("</Ad><IlKod>", "_");
		xml = xml.replace("</IlKod><Kod>", "_"); 
		xml = xml.replace("</Kod></Ilce>", "</IlceAdi>");		
		xml = xml.trim();	 
		
		ArrayList<String> ilceler = new ArrayList<String>();
		String ilce = "";
		while(xml.length() > 10){
			xml = xml.substring(10);
			ilce = xml.substring(0, xml.indexOf("</IlceAdi>"));
			xml = xml.substring(9 + ilce.length());
			ilceler.add(ilce);
		} 
		String ilceAdi = "";
		String ilceKodu = "";
		for(String s : ilceler){
			ilceAdi = s.substring(0, s.indexOf("_"));
			ilceKodu = s.substring(s.lastIndexOf("_") + 1);
			// System.out.println(ilceAdi + " / " + ilceKodu);
			if(getDBOperator().recordCount(Ilce.class.getSimpleName(), "upper(o.ad) = upper('"+ ilceAdi +"') AND o.sehirRef.rID = " + sehir.getRID() + " AND o.ilcekodu = " + Long.parseLong(ilceKodu)) == 0){
				Ilce ilceObj = new Ilce();
				ilceObj.setAd(ilceAdi);
				ilceObj.setSehirRef(sehir);
				ilceObj.setIlcekodu(Long.parseLong(ilceKodu));
				getDBOperator().insert(ilceObj);
			} else {
				Ilce ilceObj = (Ilce) getDBOperator().find(Ilce.class.getSimpleName(), "upper(o.ad) = upper('"+ ilceAdi +"') AND o.sehirRef.rID = " + sehir.getRID()); 
				ilceObj.setAd(ilceAdi);
				ilceObj.setSehirRef(sehir);
				ilceObj.setIlcekodu(Long.parseLong(ilceKodu));
				getDBOperator().update(ilceObj);
			}
		} 
	}
}
