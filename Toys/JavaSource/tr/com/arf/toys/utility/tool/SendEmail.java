package tr.com.arf.toys.utility.tool;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.ejb.Asynchronous;
import javax.mail.Message;
import javax.mail.Message.RecipientType;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.apache.log4j.Logger;

import tr.com.arf.framework.db._management.DBOperator;
import tr.com.arf.framework.db.enumerated._common.EvetHayir;
import tr.com.arf.framework.utility.exception.data.DBException;
import tr.com.arf.framework.utility.tool.DateUtil;
import tr.com.arf.toys.db.enumerated.iletisim.GonderimDurum;
import tr.com.arf.toys.db.model.iletisim.Epostagonderimlog;
import tr.com.arf.toys.db.model.iletisim.Gonderim;
import tr.com.arf.toys.db.model.kisi.Kisi;
import tr.com.arf.toys.service.application.ManagedBeanLocator;
import tr.com.arf.toys.utility.logUtils.LogService;
import tr.com.arf.toys.view.controller._common.ApplicationController;

public class SendEmail {

	/* Log4j Logger */
	protected static Logger logger = Logger.getLogger("sendEmailLogger");

	public String createMessageText(String messageValue) {
		StringBuilder messageBody = new StringBuilder("");
		String divStyle = "text-align:center; width:80%; margin:auto; border:1px solid gray; background-color:#EDEDEB; padding:20px; font-family:Calibri; -webkit-border-radius: 15px; -moz-border-radius: 15px; border-radius: 15px; ";
		messageBody.append("<div style=\"" + divStyle + "\">");
		messageBody.append(messageValue).append("<br/>");
		messageBody.append("<b>TOYS (c) " + DateUtil.getYear(new Date()) + "</b></div>");
		return messageBody.toString();
	}

	@Asynchronous
	public boolean sendEmailToRecipent(String recipient, String subject, String messageText) {
		return sendEmailToRecipentList(new String[] { recipient }, subject, messageText);
	}

	@Asynchronous
	public boolean sendEmailToRecipentList(ArrayList<String> recipientList, String subject, String messageText) {
		return sendEmailToRecipentList(recipientList.toArray(new String[recipientList.size()]), subject, messageText);
	}

	@Asynchronous
	public boolean sendEmailToRecipentList(String[] recipients, String subject, String messageText) {
		try {
            ApplicationController applicationController = ManagedBeanLocator.locateApplicationController();
            EmailConfigObject emailConfig = applicationController.geteMailConfig();
            logger.debug("email config = " + emailConfig);
            Properties props = emailConfig.getProp();
            logger.debug("email props = " + props);
//			String hostname = ManagedBeanLocator.locateApplicationController().getHostname();
//			if (hostname.contains("Ufuk")) {
//				subject = subject + " - TEST İÇİN GÖNDERİLMİŞTİR!";
//				messageText = messageText + "<br/><br/>DİKKAT! Bu eposta test sunucularımızdan gönderilmiştir, epostada yazılanlar test ortamında yapılan işlemler olup, çalışan sistemi yansıtmamaktadır."
//						+ "<br/>İşlemlerinizin son durumunu öğrenmek için canlı sisteme bağlanabilirsiniz.";
//			}

			Session session = Session.getDefaultInstance(props, null);
			MimeMessage message = new MimeMessage(session);
			message.setFrom(new InternetAddress(emailConfig.getEmailFrom()));
			InternetAddress[] toAddress = new InternetAddress[recipients.length];
			for (int i = 0; i < recipients.length; i++) {
				toAddress[i] = new InternetAddress(recipients[i]);
			}
			// toAddress[toAddress.length - 1] = new InternetAddress(ManagedBeanLocator.locateApplicationController().getSistemParametre().getMailadresi());
			for (int i = 0; i < toAddress.length; i++) {
				message.addRecipient(Message.RecipientType.BCC, toAddress[i]);
			}
			message.setSubject(subject, "utf-8");
			message.setText(createMessageText(messageText), "utf-8", "html");
			Transport transport = session.getTransport("smtp");
			transport.connect(emailConfig.getEmailHost(), emailConfig.getEmailFrom(), emailConfig.getEmailPass());
			transport.sendMessage(message, message.getAllRecipients());
			transport.close();
			logger.info("Mail sent successfully...");
			return true;
		} catch (Exception e) {
			LogService.logYaz("Error occured sending Multiple email :" + e.getMessage() + " --- Recipients :" + Arrays.toString(recipients), "SendEmail", e);
			return false;
		}
	}

	@Asynchronous
	public void sendEmailToKisiList(List<Kisi> recipientListesi, String subject, String messageText, Gonderim gonderim, Kisi gonderenRef) throws MessagingException, DBException {
		Epostagonderimlog eLog;
		DBOperator dbOperator = new DBOperator();
		// System.out.println("SENDING EMAIL 0............");
		EmailConfigObject emailConfig = ManagedBeanLocator.locateApplicationController().geteMailConfig();
		Properties props = emailConfig.getProp();
//		String hostname = ManagedBeanLocator.locateApplicationController().getHostname();
//		// System.out.println("SENDING EMAIL 1............");
//		if (hostname.contains("Ufuk")) {
//			subject = subject + " - TEST İÇİN GÖNDERİLMİŞTİR!";
//			messageText = messageText + "<br/><br/>DİKKAT! Bu eposta test sunucularımızdan gönderilmiştir, epostada yazılanlar test ortamında yapılan işlemler olup, çalışan sistemi yansıtmamaktadır."
//					+ "<br/>İşlemlerinizin son durumunu öğrenmek için canlı sisteme bağlanabilirsiniz.";
//		}
		// System.out.println("SENDING EMAIL 2............");
		Session session = Session.getDefaultInstance(props, null);
		session.setDebug(true);
		MimeMessage message = new MimeMessage(session);
		Transport transport = session.getTransport("smtp");
		transport.connect(emailConfig.getEmailHost(), emailConfig.getEmailFrom(), emailConfig.getEmailPass());
		// System.out.println("SENDING EMAIL 3............");
		// Gonderim zamani set ediliyor.
		Date tarih = new Date();
		if (gonderim.getZamanlamali() == EvetHayir._EVET) {
			tarih = gonderim.getGonderimzamani();
		}
		// System.out.println("SENDING EMAIL 4............");
		for (int i = 0; i < recipientListesi.size(); i++) {
			try {
				// System.out.println("SENDING EMAIL 5." + i + "............");
				message = new MimeMessage(session);
				message.setFrom(new InternetAddress(emailConfig.getEmailFrom()));
				/* Mesaj alicilari olusturuluyor. */
				message.setFrom(new InternetAddress(emailConfig.getEmailFrom()));
				message.setSubject(subject, "utf-8");
				message.setText(createMessageText(messageText), "utf-8", "html");
				message.addRecipient(Message.RecipientType.BCC, new InternetAddress(recipientListesi.get(i).getEposta()));
				if (gonderim.getZamanlamali() == EvetHayir._EVET) {
					eLog = new Epostagonderimlog(gonderim, recipientListesi.get(i), tarih, gonderim.getBirimRef(), GonderimDurum._BEKLEMEDE, gonderim.getAciklama(), gonderenRef);
				} else {
					transport.sendMessage(message, message.getRecipients(RecipientType.BCC));
					eLog = new Epostagonderimlog(gonderim, recipientListesi.get(i), tarih, gonderim.getBirimRef(), GonderimDurum._ILETILDI, gonderim.getAciklama(), gonderenRef);
				}
				logger.info("Mail sent successfully...");
				dbOperator.insert(eLog);
			} catch (Exception e) {
				LogService.logYaz("Mail can not be sent to " + recipientListesi.get(i).getEposta() +" : " + e.getMessage(), "SendEmail");
				eLog = new Epostagonderimlog(gonderim, recipientListesi.get(i), tarih, gonderim.getBirimRef(), GonderimDurum._ILETILMEDI, gonderim.getAciklama(), gonderenRef);
				dbOperator.insert(eLog);
			}
		}
		transport.close();
	}
}
