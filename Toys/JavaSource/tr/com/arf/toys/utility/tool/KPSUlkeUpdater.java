package tr.com.arf.toys.utility.tool;

import java.util.ArrayList;

import org.tempuri.Tmmob_x0020_KPS_x0020_ServiceV20SoapProxy;

import tr.com.arf.framework.db._management.DBOperator;
import tr.com.arf.toys.db.model.system.Ulke;
import tr.com.arf.toys.service.application.ManagedBeanLocator;

public class KPSUlkeUpdater {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}
	
	private static DBOperator getDBOperator(){
		return ManagedBeanLocator.locateSessionController().getDBOperator();
	} 
	
	public static void ulkeGuncelle() throws Exception{
		Tmmob_x0020_KPS_x0020_ServiceV20SoapProxy kpsServis = new Tmmob_x0020_KPS_x0020_ServiceV20SoapProxy(); 
		String xml = kpsServis.ulkeBilgisiGetir(ManagedBeanLocator.locateSessionController().getKpsKullaniciAdi(), ManagedBeanLocator.locateSessionController().getKpsSifre()); 
		xml = xml.replace("﻿<?xml version=\"1.0\" encoding=\"utf-8\"?><ParametreSonucu xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\"><ExtensionData />", "");
		xml = xml.replace("</ParametreSonucu>", "");
		xml = xml.replace("<ExtensionData />", "");
		xml = xml.replace("<SorguSonucu>", "");
		xml = xml.replace("</SorguSonucu>", "");
		xml = xml.replace("<Parametre><Aciklama>", "<UlkeAdi>");
		xml = xml.replace("</Aciklama><Kod>", "_");
		xml = xml.replace("</Kod></Parametre>", "</UlkeAdi>");
		xml = xml.trim();	 
		
		ArrayList<String> ulkeler = new ArrayList<String>();
		String ulke = "";
		while(xml.length() > 9){
			xml = xml.substring(9);
			ulke = xml.substring(0, xml.indexOf("</UlkeAdi>"));
			xml = xml.substring(10 + ulke.length());
			ulkeler.add(ulke);
		}  
		String ulkeadi = "";
		String ulkekod = "";
		for(String s : ulkeler){
			ulkeadi = s.substring(0, s.indexOf("_"));
			ulkekod =s.substring(s.indexOf("_") + 1);
			String whereCon = "upper(o.ad) = upper('"+ ulkeadi +"') " + "AND o.ulkekod = '" + ulkekod + "'";
			
			if(getDBOperator().recordCount(Ulke.class.getSimpleName(), whereCon) == 0){
				Ulke ulke1 = new Ulke();
				ulke1.setAd(ulkeadi);
				ulke1.setUlkekod(ulkekod);
//				sehir.setUlkeRef(ulke);
				getDBOperator().insert(ulke1);
			} else {
				Ulke ulke1 = (Ulke) getDBOperator().find(Ulke.class.getSimpleName(), "upper(o.ad) = upper('"+ ulkeadi +"') " + " AND o.ulkekod = '" + ulkekod + "'"); 
				ulke1.setAd(ulkeadi);
				ulke1.setUlkekod(ulkekod);
				getDBOperator().update(ulke1);
			}
		} 
	}
	
}
