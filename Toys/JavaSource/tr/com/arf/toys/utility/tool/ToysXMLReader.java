package tr.com.arf.toys.utility.tool;

import java.io.StringReader;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.w3c.dom.CharacterData;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import tr.com.arf.toys.db.enumerated.iletisim.AdresTipi;
import tr.com.arf.toys.db.nonEntityModel.PojoAdres;
import tr.com.arf.toys.db.nonEntityModel.PojoKisi;
import tr.com.arf.toys.utility.logUtils.LogService;

public class ToysXMLReader {

	protected static Logger logger = Logger.getLogger(ToysXMLReader.class);

	// https://www.freeformatter.com/xml-formatter.html#ad-output

	private static String cleanXMLFirst(String xmlRecords) {
		xmlRecords = xmlRecords.replace("&lt;", "<");
		xmlRecords = xmlRecords.replace("&gt;", ">");
		xmlRecords = xmlRecords.trim().replaceFirst("^([\\W]+)<", "<");
		int index2 = StringUtils.ordinalIndexOf(xmlRecords, "<?xml", 2);
		System.out.println("index2 2.occur :" + index2);
		if (index2 > 0) {
			String result = xmlRecords.substring(0, index2);
			result += xmlRecords.substring(index2 + 38);
			return result;
		} else {
			return xmlRecords;
		}

	}

	public static PojoKisi readXmlStringForKimlik(String xmlRecords) throws Exception {
		PojoKisi kisi = new PojoKisi();
		xmlRecords = cleanXMLFirst(xmlRecords);
		try {
			DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
			InputSource is = new InputSource();
			is.setCharacterStream(new StringReader(xmlRecords));
			System.out.println("xmlRecords SON...");
			System.out.println(xmlRecords);
			Document doc = db.parse(is);
			NodeList nodes = doc.getElementsByTagName("TCVatandasiKisiKutukleri");
			for (int i = 0; i < nodes.getLength(); i++) {
				Element element = (Element) nodes.item(i);
				kisi.setTcKimlikNo(Long.parseLong(findTagValue(element, "TCKimlikNo")));
				// kisi.setDogumYeriKodu(findTagValue(element, "DogumYerKod"));
				// System.out.println("Dogum tarihleri...");
				kisi.setDogumTarihiGun(findTagValue(findChildElement(element, "TemelBilgisi", "DogumTarih"), "Gun"));
				kisi.setDogumTarihiAy(findTagValue(findChildElement(element, "TemelBilgisi", "DogumTarih"), "Ay"));
				kisi.setDogumTarihiYil(findTagValue(findChildElement(element, "TemelBilgisi", "DogumTarih"), "Yil"));

				kisi.setAileSiraNo(findChildValue(element, "KayitYeriBilgisi", "AileSiraNo"));
				kisi.setBireySiraNo(findChildValue(element, "KayitYeriBilgisi", "BireySiraNo"));
				kisi.setCiltAciklama(findChildValue(element, "Cilt", "Aciklama"));
				kisi.setMahalle(findChildValue(element, "Cilt", "Aciklama"));
				kisi.setCiltNo(findChildValue(element, "Cilt", "Kod"));

				kisi.setNufusaKayitliOlduguIlAdi(findChildValue(element, "Il", "Aciklama"));
				kisi.setNufusaKayitliOlduguIlKodu(findChildValue(element, "Il", "Kod"));
				kisi.setNufusaKayitliOlduguIlceAdi(findChildValue(element, "Ilce", "Aciklama"));
				kisi.setNufusaKayitliOlduguIlceKodu(findChildValue(element, "Ilce", "Kod"));

				kisi.setAd(findChildValue(element, "TemelBilgisi", "Ad"));
				kisi.setSoyad(findChildValue(element, "TemelBilgisi", "Soyad"));
				kisi.setAnaAdi(findChildValue(element, "TemelBilgisi", "AnneAd"));
				kisi.setBabaAdi(findChildValue(element, "TemelBilgisi", "BabaAd"));
				kisi.setDogumYeri(findChildValue(element, "TemelBilgisi", "DogumYer"));
				kisi.setCinsiyetAciklama(findChildValue(element, "Cinsiyet", "Aciklama"));
				kisi.setCinsiyetKod(findChildValue(element, "Cinsiyet", "Kod"));

				kisi.setMedeniHalKodu(findChildValue(element, "MedeniHal", "Kod"));
				kisi.setMedeniHalAciklamasi(findChildValue(element, "MedeniHal", "Aciklama"));

				kisi.setOlumTarihiGun(findChildValue(element, "OlumTarih", "Gun"));
				kisi.setOlumTarihiAy(findChildValue(element, "OlumTarih", "Ay"));
				kisi.setOlumTarihiYil(findChildValue(element, "OlumTarih", "Yil"));

				if (!isEmpty(findChildValue(element, "KisiBilgisi", "EsTCKimlikNo")) && isNumber(findChildValue(element, "KisiBilgisi", "EsTCKimlikNo"))) {
					kisi.setEsTCKimlikNo(Long.parseLong(findChildValue(element, "KisiBilgisi", "EsTCKimlikNo")));
				}
				if (!isEmpty(findChildValue(element, "KisiBilgisi", "BabaTCKimlikNo")) && isNumber(findChildValue(element, "KisiBilgisi", "BabaTCKimlikNo"))) {
					kisi.setBabaTCKimlikNo(Long.parseLong(findChildValue(element, "KisiBilgisi", "BabaTCKimlikNo")));
				}
				if (!isEmpty(findChildValue(element, "KisiBilgisi", "AnneTCKimlikNo")) && isNumber(findChildValue(element, "KisiBilgisi", "AnneTCKimlikNo"))) {
					kisi.setAnneTCKimlikNo(Long.parseLong(findChildValue(element, "KisiBilgisi", "AnneTCKimlikNo")));
				}
			}
			// System.out.println(kisi.getValue());
		} catch (NullPointerException ne) {
			LogService.logYaz("NullPointerException @readXmlString of ToysXMLReader : " + ne.getMessage(), "ToysXMLReader", ne);
			System.out.println(ne.getMessage());
		} catch (Exception e) {
			LogService.logYaz("Exception @readXmlString of ToysXMLReader : " + e.getMessage(), "ToysXMLReader", e);
			System.out.println(e.getMessage());
		}
		return kisi;
	}

	public static PojoAdres readXmlStringForAdres(String xmlRecords, String adresTuru) throws Exception {
		PojoAdres adres = new PojoAdres();
		try {
			xmlRecords = cleanXMLFirst(xmlRecords);
			System.out.println(xmlRecords);
			DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
			InputSource is = new InputSource();
			is.setCharacterStream(new StringReader(xmlRecords));

			Document doc = db.parse(is);
			NodeList nodes = doc.getElementsByTagName(adresTuru);
			for (int i = 0; i < nodes.getLength(); i++) {
				Element element = (Element) nodes.item(i);
				// System.out.println("AdresNo NE : " + findTagValue(element, "AdresNo"));
				if (findTagValue(element, "AdresNo") == null || findTagValue(element, "AdresNo").equalsIgnoreCase("nil") || findTagValue(element, "AdresNo").trim().length() == 0) {
					// System.out.println("ADRES NO null...");
					adres.setHataBilgisiKodu("HATA_01");
					return adres;
				}
				adres.setAdresNo(findTagValue(element, "AdresNo"));
				adres.setAcikAdres(findTagValue(element, "AcikAdres"));
				adres.setBeyanTarihiGun(findChildValue(element, "BeyanTarihi", "Gun"));
				adres.setBeyanTarihiAy(findChildValue(element, "BeyanTarihi", "Ay"));
				adres.setBeyanTarihiYil(findChildValue(element, "BeyanTarihi", "Yil"));

				adres.setAdrestipi(AdresTipi.getWithCode(Integer.parseInt(findChildValue(element, "AdresTip", "Kod"))));
				adres.setBinaAda(findTagValue(element, "BinaAda"));
				adres.setBinaKodu(findTagValue(element, "BinaKodu"));
				adres.setBinaParsel(findTagValue(element, "BinaParsel"));
				adres.setBinaSiteAdi(findTagValue(element, "BinaSiteAdi"));

				adres.setCsbm(findTagValue(element, "Csbm"));
				adres.setCsbmKodu(findTagValue(element, "CsbmKodu"));
				adres.setDisKapiNo(findTagValue(element, "DisKapiNo"));

				adres.setIcKapiNo(findTagValue(element, "IcKapiNo"));
				adres.setIlKodu(findTagValue(element, "IlKodu"));
				adres.setIlceKodu(findTagValue(element, "IlceKodu"));
				adres.setMahalle(findTagValue(element, "Mahalle"));
				adres.setMahalleKodu(findTagValue(element, "MahalleKodu"));

				adres.setTasinmaTarihiGun(findChildValue(element, "TasinmaTarihi", "Gun"));
				adres.setTasinmaTarihiAy(findChildValue(element, "TasinmaTarihi", "Ay"));
				adres.setTasinmaTarihiYil(findChildValue(element, "TasinmaTarihi", "Yil"));

				adres.setTescilTarihiGun(findChildValue(element, "TescilTarihi", "Gun"));
				adres.setTescilTarihiAy(findChildValue(element, "TescilTarihi", "Ay"));
				adres.setTescilTarihiYil(findChildValue(element, "TescilTarihi", "Yil"));

				adres.setBinaBlokAdi(findTagValue(element, "DisKapiNo"));
			}
			// System.out.println(adres.getValue());
		} catch (NullPointerException ne) {
			// LogService.logYaz("NullPointerException @readXmlString of XMLHELPER : " + ne.getMessage(), "XmlHelper", ne);
			System.out.println(ne.getMessage());
		} catch (Exception e) {
			// LogService.logYaz("Exception @readXmlString of XMLHELPER : " + e.getMessage(), "XmlHelper", e);
			System.out.println(e.getMessage());
		}
		return adres;
	}

	public static String findTagValue(Element element, String tagName) {
		NodeList title = element.getElementsByTagName(tagName);
		Element line = (Element) title.item(0);
		line = (Element) title.item(0);
		return getCharacterDataFromElement(line);
	}

	public static String findChildValue(Element element, String headerTagName, String tagName) {
		NodeList nodeList = element.getElementsByTagName(headerTagName);
		String result = "";
		for (int temp = 0; temp < nodeList.getLength(); temp++) {
			Node nNode = nodeList.item(temp);
			if (nNode.getNodeType() == Node.ELEMENT_NODE) {
				Element eElement = (Element) nNode;
				result = eElement.getElementsByTagName(tagName).item(0).getTextContent();
			}
		}
		return result;
	}

	public static Element findChildElement(Element element, String headerTagName, String tagName) {
		NodeList nodeList = element.getElementsByTagName(headerTagName);
		Element result = null;
		for (int temp = 0; temp < nodeList.getLength(); temp++) {
			Node nNode = nodeList.item(temp);
			if (nNode.getNodeType() == Node.ELEMENT_NODE) {
				Element eElement = (Element) nNode;
				return eElement;
			}
		}
		return result;
	}

	public static String getCharacterDataFromElement(Element e) {
		Node child = e.getFirstChild();
		if (child instanceof CharacterData) {
			CharacterData cd = (CharacterData) child;
			return cd.getData();
		}
		return "";
	}

	private static boolean isEmpty(String str) {
		return str == null || str.trim().length() == 0;
	}

	private static boolean isNumber(String s) {
		if (s == null || s.trim().length() == 0) {
			return false;
		}
		try {
			Integer.parseInt(s);
			return true;
		} catch (Exception e) {
			return false;
		}
	}
}
