package tr.com.arf.toys.utility.tool;

import java.io.FileInputStream;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import tr.com.arf.framework.db.model.query.QueryObject;
import tr.com.arf.framework.utility.tool.DateUtil;
import tr.com.arf.framework.utility.tool.KeyUtil;
import tr.com.arf.toys.db.enumerated.finans.OdemeTuru;
import tr.com.arf.toys.db.enumerated.uye.OdemeSekli;
import tr.com.arf.toys.db.model.finans.Odeme;
import tr.com.arf.toys.db.model.system.Birim;
import tr.com.arf.toys.db.model.uye.Uye;
import tr.com.arf.toys.service.application.ManagedBeanLocator;

public class ExcelImport {

	@SuppressWarnings({ "rawtypes" })
	public static Map<String, ArrayList> odemeDosyasiYukle(String path) throws Exception {
		InputStream myxls = new FileInputStream(path);
		XSSFWorkbook wb = new XSSFWorkbook(myxls);
		XSSFSheet sheetPOI = wb.getSheetAt(0);
		myxls.close();
		int j = 0;
		int i = 0;
		Map<String, ArrayList> result = new HashMap<String, ArrayList>();
		Odeme odeme = new Odeme();
		ArrayList<String> hataMesajlari = new ArrayList<String>();
		ArrayList<Odeme> data = new ArrayList<Odeme>();
		Iterator rows = sheetPOI.rowIterator();
		while (rows.hasNext()) {
			odeme = new Odeme();
			try {
				XSSFRow row = (XSSFRow) rows.next();
				Iterator cells = row.cellIterator();
				odeme.setRID(null);
				i = 0;
				String val = null;
				String validationResult = null;
				if (j > 0) {
					while (cells.hasNext() && i < 16) {
						XSSFCell cellPOI = (XSSFCell) cells.next();
						cellPOI.setCellType(Cell.CELL_TYPE_STRING);
						val = cellPOI.toString();

						// System.out.println("Cell value at line " + j + ", at column " + i + " is : " + val);
						if (i == 0) { // sicil no
							List uyeListesi = uyeSicildenKisiBul(val);
							if (!isEmpty(uyeListesi)) {
								odeme.setKisiRef(((Uye) uyeListesi.get(0)).getKisiRef());
							}
						} else if (i == 1) { // ad soyad
							// DO NOTHING
						} else if (i == 2) { // birim
							List birimListesi = birimAdindanBirimBul(val);
							if (!isEmpty(birimListesi)) {
								odeme.setBirimRef((Birim) birimListesi.get(0));
							}
						} else if (i == 3) { // miktar
							try {
								odeme.setMiktar(new BigDecimal(val));
							} catch (Exception e) {
								odeme.setMiktar(BigDecimal.ZERO);
							}
						} else if (i == 4) {// tarih
							odeme.setTarih(DateUtil.createDateObject(val));
						} else if (i == 5) {// odeme turu
							odeme.setOdemeTuru(OdemeTuru.getWithLabel(val));
						} else if (i == 6) { // odeme sekli
							System.out.println("");
							odeme.setOdemeSekli(OdemeSekli.getWithLabel(val));
						} else if (i == 7) { // dekont no
							odeme.setDekontNo(val);
						} else if (i == 8) { // makbuz no
							if (!isEmpty(val)) {
								odeme.setMakbuzno(new BigDecimal(val));
							}
						} else if (i == 9) { // makbuz basilma tarihi
							if (!isEmpty(val)) {
								odeme.setMakbuzbasilmatarihi(DateUtil.createDateObject(val));
							}
						}
						i++;
					}
					validationResult = validateExcelRecord(odeme);
					if (isEmpty(validationResult)) {
						data.add(odeme);
					} else {
						hataMesajlari.add(j + 1 + ". " + KeyUtil.getLabelValue("common.satir") + " " + KeyUtil.getLabelValue("common.excelUploadError") + " " + validationResult);
					}
				} // if j > 0
			} catch (Exception e) {
				hataMesajlari.add(j + 1 + ". " + KeyUtil.getLabelValue("common.satir") + " " + (i + 1) + ". " + KeyUtil.getLabelValue("common.kolon") + " "
						+ KeyUtil.getLabelValue("common.excelUploadError") + " " + e.getMessage());
			}

			j++;
		} // while (rows.hasNext())
		result.put("odemeler", data);
		result.put("hataMesajlari", hataMesajlari);
		return result;
	}// method

	private static String validateExcelRecord(Odeme odeme) {
		if (odeme.getKisiRef() == null) {
			return KeyUtil.getLabelValue("eksik.alan") + " : " + KeyUtil.getLabelValue("odeme.kisiRef");
		} else if (odeme.getBirimRef() == null) {
			return KeyUtil.getLabelValue("eksik.alan") + " : " + KeyUtil.getLabelValue("odeme.birimRef");
		} else if (odeme.getMiktar() == null || odeme.getMiktar().longValue() == 0) {
			return KeyUtil.getLabelValue("eksik.alan") + " : " + KeyUtil.getLabelValue("odeme.miktar");
		} else if (odeme.getOdemeTuru() == null) {
			return KeyUtil.getLabelValue("eksik.alan") + " : " + KeyUtil.getLabelValue("odeme.odemeTuru");
		} else {
			return "";
		}
	}

	public static boolean isNumber(String str) {
		try {
			new Long(str);
			return true;

		} catch (Exception e) {
			return false;
		}
	}

	public static boolean isDouble(String str) {
		try {
			new Double(str);
			return true;

		} catch (Exception e) {
			return false;
		}
	}

	public static boolean isBigDecimal(String str) {
		try {
			new BigDecimal(str);
			return true;

		} catch (Exception e) {
			return false;
		}
	}

	@SuppressWarnings("rawtypes")
	private static List uyeSicildenKisiBul(String cellValue) {
		return ManagedBeanLocator.locateSessionController().loadObjects(Uye.class.getSimpleName(), "o.sicilno ='" + cellValue + "'");
	}

	@SuppressWarnings("rawtypes")
	private static List birimAdindanBirimBul(String cellValue) {
		String whereCondition = "(LOWER(o.ad) = LOWER('" + cellValue + "') OR UPPER(o.ad) = UPPER('" + cellValue + "'))";
		return ManagedBeanLocator.locateSessionController().loadObjects(Birim.class.getSimpleName(), whereCondition);
	}

	@SuppressWarnings("rawtypes")
	private static boolean isEmpty(Collection col) {
		return col == null || col.size() == 0;
	}

	@SuppressWarnings("unused")
	private static boolean isEmpty(ArrayList<ArrayList<QueryObject>> col) {
		if (col == null || col.size() == 0) {
			return true;
		} else {
			for (int x = 0; x < col.size(); x++) {
				if (!isEmpty(col.get(x))) {
					return false;
				}
			}
			return true;
		}
	}

	@SuppressWarnings("unused")
	private static boolean isEmpty(Object[] col) {
		return col == null || col.length == 0;
	}

	private static boolean isEmpty(String str) {
		return str == null || str.trim().length() == 0;
	}
}
