package tr.com.arf.toys.utility.export;

import java.io.IOException;
import java.text.MessageFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

import javax.faces.context.FacesContext;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.DataFormat;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.PrintSetup;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;

import tr.com.arf.framework.db.model._base.BaseEntity;
import tr.com.arf.framework.utility.tool.KeyUtil;
import tr.com.arf.framework.utility.tool.StringUtil;

public class CustomExcelExport {

	private static void writeExcelToResponse(HttpServletResponse response, HSSFWorkbook generatedExcel, String filename) throws IOException {
		response.setContentType("application/vnd.ms-excel");
		response.setHeader("Content-disposition", "attachment; filename=" + filename + ".xls");

		try {
			ServletOutputStream out = response.getOutputStream();
			generatedExcel.write(out);
			out.flush();
			out.close();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		FacesContext faces = FacesContext.getCurrentInstance();
		faces.responseComplete();
	}

	protected static ClassLoader getCurrentClassLoader(Object defaultObject) {
		ClassLoader loader = Thread.currentThread().getContextClassLoader();
		if (loader == null) {
			loader = defaultObject.getClass().getClassLoader();
		}
		return loader;
	}

	public static String getMessageResourceString(String bundleName, String key, Object params[], Locale locale) {

		String text = null;

		ResourceBundle bundle = ResourceBundle.getBundle(bundleName, locale, getCurrentClassLoader(params));

		try {
			text = bundle.getString(key);
		} catch (MissingResourceException e) {
			text = "?? key " + key + " not found ??";
		}

		if (params != null) {
			MessageFormat mf = new MessageFormat(text, locale);
			text = mf.format(params, new StringBuffer(), null).toString();
		}

		return text;
	}

	private static Map<String, CellStyle> createStyles(Workbook wb) {
		Map<String, CellStyle> styles = new HashMap<String, CellStyle>();
		DataFormat df = wb.createDataFormat();

		CellStyle style;
		Font headerFont = wb.createFont();
		headerFont.setBoldweight(Font.BOLDWEIGHT_BOLD);
		headerFont.setFontHeightInPoints((short) 11);
		headerFont.setColor(IndexedColors.BLACK.getIndex());
		style = createBorderedStyle(wb);
		style.setAlignment(CellStyle.ALIGN_CENTER);
		style.setFillForegroundColor(IndexedColors.LIGHT_CORNFLOWER_BLUE.getIndex());
		style.setFillPattern(CellStyle.SOLID_FOREGROUND);
		style.setFont(headerFont);
		styles.put("header", style);

		style = createBorderedStyle(wb);
		style.setAlignment(CellStyle.ALIGN_CENTER);
		style.setFont(headerFont);
		styles.put("headerNoFill", style);

		style = createBorderedStyle(wb);
		style.setAlignment(CellStyle.ALIGN_CENTER);
		style.setFillForegroundColor(IndexedColors.LIGHT_CORNFLOWER_BLUE.getIndex());
		style.setFillPattern(CellStyle.SOLID_FOREGROUND);
		style.setFont(headerFont);
		style.setDataFormat(df.getFormat("d-mmm"));
		styles.put("header_date", style);

		Font font1 = wb.createFont();
		font1.setBoldweight(Font.BOLDWEIGHT_BOLD);
		style = createBorderedStyle(wb);
		style.setAlignment(CellStyle.ALIGN_LEFT);
		style.setFont(font1);
		styles.put("cell_b", style);

		style = createBorderedStyle(wb);
		style.setAlignment(CellStyle.ALIGN_CENTER);
		style.setFont(font1);
		styles.put("cell_b_centered", style);

		style = createBorderedStyle(wb);
		style.setAlignment(CellStyle.ALIGN_RIGHT);
		style.setFont(font1);
		style.setDataFormat(df.getFormat("d-mmm"));
		styles.put("cell_b_date", style);

		style = createBorderedStyle(wb);
		style.setAlignment(CellStyle.ALIGN_CENTER);
		style.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
		style.setFont(font1);
		style.setFillForegroundColor(IndexedColors.WHITE.getIndex());
		style.setFillPattern(CellStyle.SOLID_FOREGROUND);
		style.setDataFormat(df.getFormat("dd/mm/yyyy"));
		Font font0 = wb.createFont();
		font0.setBoldweight(Font.BOLDWEIGHT_NORMAL);
		font0.setFontHeightInPoints((short) 10);
		font0.setColor(IndexedColors.BLACK.getIndex());
		style.setFont(font0);
		styles.put("cell_g", style);

		Font font2 = wb.createFont();
		font2.setColor(IndexedColors.BLUE.getIndex());
		font2.setBoldweight(Font.BOLDWEIGHT_BOLD);
		style = createBorderedStyle(wb);
		style.setAlignment(CellStyle.ALIGN_LEFT);
		style.setFont(font2);
		styles.put("cell_bb", style);

		style = createBorderedStyle(wb);
		style.setAlignment(CellStyle.ALIGN_RIGHT);
		style.setFont(font1);
		style.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
		style.setFillPattern(CellStyle.SOLID_FOREGROUND);
		style.setDataFormat(df.getFormat("d-mmm"));
		styles.put("cell_bg", style);

		Font font3 = wb.createFont();
		font3.setFontHeightInPoints((short) 14);
		font3.setColor(IndexedColors.DARK_BLUE.getIndex());
		font3.setBoldweight(Font.BOLDWEIGHT_BOLD);
		style = createBorderedStyle(wb);
		style.setAlignment(CellStyle.ALIGN_LEFT);
		style.setFont(font3);
		style.setWrapText(true);
		styles.put("cell_h", style);

		style = createBorderedStyle(wb);
		style.setAlignment(CellStyle.ALIGN_LEFT);
		style.setWrapText(true);
		styles.put("cell_normal", style);

		style = createBorderedStyle(wb);
		style.setAlignment(CellStyle.ALIGN_CENTER);
		style.setWrapText(true);
		styles.put("cell_normal_centered", style);

		style = createBorderedStyle(wb);
		style.setAlignment(CellStyle.ALIGN_RIGHT);
		style.setWrapText(true);
		style.setDataFormat(df.getFormat("d-mmm"));
		styles.put("cell_normal_date", style);

		style = createBorderedStyle(wb);
		style.setAlignment(CellStyle.ALIGN_LEFT);
		style.setIndention((short) 1);
		style.setWrapText(true);
		styles.put("cell_indented", style);

		style = createBorderedStyle(wb);
		style.setFillForegroundColor(IndexedColors.BLUE.getIndex());
		style.setFillPattern(CellStyle.SOLID_FOREGROUND);
		styles.put("cell_blue", style);

		return styles;
	}

	private static CellStyle createBorderedStyle(Workbook wb) {
		CellStyle style = wb.createCellStyle();
		style.setBorderRight(CellStyle.BORDER_THIN);
		style.setRightBorderColor(IndexedColors.BLACK.getIndex());
		style.setBorderBottom(CellStyle.BORDER_THIN);
		style.setBottomBorderColor(IndexedColors.BLACK.getIndex());
		style.setBorderLeft(CellStyle.BORDER_THIN);
		style.setLeftBorderColor(IndexedColors.BLACK.getIndex());
		style.setBorderTop(CellStyle.BORDER_THIN);
		style.setTopBorderColor(IndexedColors.BLACK.getIndex());
		return style;
	}

	public static void writeGenericExcelExport(List<BaseEntity> exportList) {
		HSSFWorkbook workbook = new HSSFWorkbook();

		// Styles loaded
		Map<String, CellStyle> styles = createStyles(workbook);

		// Create two sheets in the excel document and name it First Sheet and
		// Second Sheet.
		HSSFSheet firstSheet = workbook.createSheet("FirstSheet");
		// HSSFSheet secondSheet = workbook.createSheet("SECOND SHEET");

		firstSheet.setFitToPage(true);
		firstSheet.setHorizontallyCenter(true);

		// LANDSCAPE VS PORTRAIT
		PrintSetup printSetup = firstSheet.getPrintSetup();
		printSetup.setLandscape(false);

		// BASLIK
		HSSFRow row1 = firstSheet.createRow(0);
		row1.setHeight((short) 1000);
		HSSFCell cell = row1.createCell(0);
		cell.setCellStyle(styles.get("headerNoFill"));
		String keyValue = StringUtil.makeFirstCharLowerCase(exportList.get(0).getClass().getSimpleName());
		cell.setCellValue(new HSSFRichTextString(KeyUtil.getLabelValue(keyValue + "." + keyValue) + KeyUtil.getLabelValue("common.list")));
		BaseEntity baseValue = exportList.get(0);
		System.out.println(exportList.size() + " RECORD TO BE EXPORTED...");
		String stringValue = baseValue.getValue();
		String[] columnValues = stringValue.split("<br />");
		int columnSize = columnValues.length;

		// KOLON BASLIKLARI
		firstSheet.addMergedRegion(new CellRangeAddress(0, 0, 0, columnSize - 1));
		row1 = firstSheet.createRow(1);
		for (int x = 0; x < columnSize; x++) {
			cell = row1.createCell(x);
			cell.setCellStyle(styles.get("headerNoFill"));
			cell.setCellValue(new HSSFRichTextString(columnValues[x].substring(0, columnValues[x].indexOf(" : "))));
		}
		System.out.println("HEADERS CREATED FOR " + columnSize + " COLUMNS...");
		int x = 2;
		for (BaseEntity ent : exportList) {
			HSSFRow rowA = firstSheet.createRow(x);
			stringValue = ent.getValue();
			columnValues = stringValue.split("<br />");
			System.out.println("EXPORTING... " + (x - 1));
			for (int a = 0; a < columnSize; a++) {
				cell = rowA.createCell(a);
				cell.setCellStyle(styles.get("cell_g"));
				cell.setCellValue(new HSSFRichTextString(columnValues[a].substring(columnValues[a].indexOf(" : ") + 3, columnValues[a].length())));
			}
			x++;
		}
		System.out.println("VALUES CREATED...");
		FacesContext.getCurrentInstance().responseComplete();
		try {
			writeExcelToResponse((HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse(), workbook, baseValue.getClass().getSimpleName().toLowerCase(Locale.ENGLISH));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			System.out.println("SAYFA YAZILAMADI...");
			e.printStackTrace();
		}
	}// writeUmkeMalzemeExcelRapor

}
