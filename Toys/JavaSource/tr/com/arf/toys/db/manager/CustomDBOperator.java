package tr.com.arf.toys.db.manager;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.MathContext;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.ejb.Singleton;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;

import tr.com.arf.framework.db._management.PersistenceManager;
import tr.com.arf.framework.db.enumerated._common.IslemTuru;
import tr.com.arf.framework.db.model._base.BaseEntity;
import tr.com.arf.framework.db.model.sistem.Islemlog;
import tr.com.arf.framework.utility.exception.data.DBException;
import tr.com.arf.framework.view.controller._common.SessionUserBase;
import tr.com.arf.toys.db.enumerated.finans.OdemeTuru;
import tr.com.arf.toys.db.enumerated.iletisim.ReferansTipi;
import tr.com.arf.toys.db.enumerated.uye.UyeAidatDurum;
import tr.com.arf.toys.db.model.egitim.Egitimkatilimci;
import tr.com.arf.toys.db.model.egitim.Egitimogretmen;
import tr.com.arf.toys.db.model.etkinlik.Etkinlikkatilimci;
import tr.com.arf.toys.db.model.finans.Borc;
import tr.com.arf.toys.db.model.finans.Odeme;
import tr.com.arf.toys.db.model.iletisim.Iletisimliste;
import tr.com.arf.toys.db.model.iletisim.Iletisimlistedetay;
import tr.com.arf.toys.db.model.kisi.IsYeriTemsilcileri;
import tr.com.arf.toys.db.model.kisi.Kisi;
import tr.com.arf.toys.db.model.kisi.KomisyonDonemUye;
import tr.com.arf.toys.db.model.kisi.KurulDonemUye;
import tr.com.arf.toys.db.model.system.KurumKisi;
import tr.com.arf.toys.db.model.uye.Uye;
import tr.com.arf.toys.db.model.uye.Uyeaidat;
import tr.com.arf.toys.utility.logUtils.LogService;

@SuppressWarnings("rawtypes")
@Singleton
public final class CustomDBOperator implements Serializable {

	private static final long serialVersionUID = -725648633548176494L;

	private static final CustomDBOperator singleton = new CustomDBOperator();

	private EntityManagerFactory entityManagerFactory = null;

	private static final MathContext mc = MathContext.DECIMAL32;

	public static CustomDBOperator getInstance() {
		return singleton;
	}

	public EntityManagerFactory getEntityManagerFactory() {
		if (entityManagerFactory == null) {
			entityManagerFactory = PersistenceManager.getInstance().getEntityManagerFactory();
		}
		return entityManagerFactory;
	}

	protected void logKaydet(BaseEntity entity, IslemTuru islemTuru, String oldVal) throws DBException {
		EntityManager entityManager = getEntityManagerFactory().createEntityManager();
		Islemlog islemLog = new Islemlog();
		islemLog.setIslemtarihi(new Date());
		islemLog.setIslemturu(islemTuru);
		islemLog.setReferansRID(entity.getRID());
		islemLog.setTabloadi(entity.getClass().getSimpleName());
		if (islemTuru != IslemTuru._SILME) {
			islemLog.setEskideger(oldVal);
			islemLog.setYenideger(entity.getValue());
		} else {
			islemLog.setEskideger(entity.getValue());
			islemLog.setYenideger("");
		}
		Map<String, Object> sessionMap = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
		islemLog.setKullaniciRid(((SessionUserBase) sessionMap.get("sessionUser")).getKullaniciRid());
		islemLog.setKullaniciText(((SessionUserBase) sessionMap.get("sessionUser")).getKullaniciText());
		try {
			entityManager.getTransaction().begin();
			entityManager.persist(islemLog);
			entityManager.getTransaction().commit();
		} catch (Exception e) {
			if (entityManager.getTransaction().isActive()) {
				entityManager.getTransaction().rollback();
			}
			e.printStackTrace();
			throw new DBException(e, this.getClass().getName(), "Begin Transaction");
		} finally {
			entityManager.close();
		}
	}

	public EntityManager beginTransaction() throws DBException {
		EntityManager entityManager = getEntityManagerFactory().createEntityManager();
		try {
			entityManager.getTransaction().begin();
		} catch (Exception e) {
			if (entityManager.getTransaction().isActive()) {
				entityManager.getTransaction().rollback();
			}
			e.printStackTrace();
			throw new DBException(e, this.getClass().getName(), "Begin Transaction");
		}
		return entityManager;
	}

	public void commitTransaction(EntityManager entityManager) throws DBException {
		try {
			entityManager.getTransaction().commit();
		} catch (Exception e) {
			if (entityManager.getTransaction().isActive()) {
				entityManager.getTransaction().rollback();
			}
			e.printStackTrace();
			throw new DBException(e, this.getClass().getName(), "Commit Transaction");
		} finally {
			entityManager.close();
		}
	}

	public void rollBackTransaction(EntityManager entityManager) throws DBException {
		try {
			entityManager.getTransaction().rollback();
		} catch (Exception e) {
			e.printStackTrace();
			throw new DBException(e, this.getClass().getName(), "Rollback Transaction");
		} finally {
			entityManager.close();
		}
	}

	/* Data Load Methods */
	public List loadByQuery(String entity, String whereCondition) {
		try {
			EntityManager entityManager = getEntityManagerFactory().createEntityManager();
			String query = "SELECT OBJECT(o) FROM " + entity + " AS o";
			if (!isEmpty(whereCondition)) {
				query += " WHERE " + whereCondition;
			}
			Query entityQuery = entityManager.createQuery(query);
			List dataList = entityQuery.getResultList();
			entityManager.close();
			return dataList;
		} catch (Exception e) {
			System.out.println("ERROR : " + e.getMessage());
			System.out.println("ENTITY : " + entity);
			System.out.println("WHERECONDITION : " + whereCondition);
			return null;
		}
	}

	public List loadByQuery(String query) {
		try {
			EntityManager entityManager = getEntityManagerFactory().createEntityManager();
			Query entityQuery = entityManager.createQuery(query);
			List dataList = entityQuery.getResultList();
			entityManager.close();
			return dataList;
		} catch (Exception e) {
			System.out.println("ERROR : " + e.getMessage());
			System.out.println("QUERY : " + query);
			return null;
		}
	}

	public int recordCount(String entity, String queryCriteria) throws DBException {
		EntityManager entityManager = getEntityManagerFactory().createEntityManager();
		String query = "SELECT COUNT(o) FROM " + entity + " AS o";
		if (!isEmpty(queryCriteria)) {
			query += " WHERE " + queryCriteria;
		}
		int count = 0;
		try {
			Query entityQuery = entityManager.createQuery(query);
			count = ((Long) entityQuery.getSingleResult()).intValue();
		} catch (Exception e) {
			System.out.println("ERROR @recordCount :" + e.getMessage());
			e.printStackTrace();
			try {
				throw new DBException(e, this.getClass().getName(), "Update", "Kayıt: " + entity.toString());
			} catch (DBException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		} finally {
			entityManager.close();
		}
		return count;
	}

	@SuppressWarnings("unchecked")
	public String getMaxSicilNo() {
		EntityManager entityManager = getEntityManagerFactory().createEntityManager();
		String query = "SELECT max(o.kimlikno) FROM " + Kisi.class.getSimpleName() + " AS o WHERE o.kimlikno > 99900000000 ";
		try {
			Query entityQuery = entityManager.createQuery(query);
			List<Object> countList = entityQuery.getResultList();
			if (countList != null && countList.size() > 0 && countList.get(0) != null) {
				return (Long) countList.get(0) + 1 + "";
			} else {
				return "99900000001";
			}
		} catch (Exception e) {
			System.out.println("ERROR @recordCount :" + e.getMessage());
			e.printStackTrace();
		} finally {
			entityManager.close();
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	public List<Kisi> epostaGonderimGonderimListesiGetir(Iletisimliste iletisimListe) {
		EntityManager entityManager = getEntityManagerFactory().createEntityManager();
		List<Kisi> kisilistesi = null;
		String whereCondition = "Select m.Referans from " + Iletisimlistedetay.class.getSimpleName() + " m WHERE m.iletisimlisteRef.rID=" + iletisimListe.getRID() + " AND m.referansTipi=";

		try {
			/* Kisi listesi */
			kisilistesi = loadByQuery("SELECT OBJECT(o) FROM " + Kisi.class.getSimpleName() + " AS o WHERE o.rID IN (" + whereCondition + ReferansTipi._KISI.getCode() + " ) ");

			/* Uye listesi */
			kisilistesi.addAll(loadByQuery(
					"SELECT OBJECT(n) FROM " + Kisi.class.getSimpleName() + " AS n WHERE n.rID IN (SELECT o.kisiRef.rID FROM " + Uye.class.getSimpleName() + " AS o WHERE o.rID IN (" + whereCondition + ReferansTipi._UYE.getCode() + " )) "));

			/* Birimden uye listesi */
			kisilistesi.addAll(loadByQuery("SELECT OBJECT(n) FROM " + Kisi.class.getSimpleName() + " AS n WHERE n.rID IN (SELECT o.kisiRef.rID FROM " + Uye.class.getSimpleName() + " AS o WHERE o.birimRef.rID IN  (" + whereCondition
					+ ReferansTipi._BIRIM.getCode() + " )) "));

			/* Kurul uye listesi */
			kisilistesi.addAll(loadByQuery("SELECT OBJECT(n) FROM " + Kisi.class.getSimpleName() + " AS n WHERE n.rID IN (SELECT o.uyeRef.kisiRef.rID FROM " + KurulDonemUye.class.getSimpleName()
					+ " AS o WHERE o.kurulDonemRef.kurulRef.rID IN  (" + whereCondition + ReferansTipi._KURUL.getCode() + " )) "));

			/* Komisyon Üyeleri */
			kisilistesi.addAll(loadByQuery("SELECT OBJECT(n) FROM " + Kisi.class.getSimpleName() + " AS n WHERE n.rID IN (SELECT o.uyeRef.kisiRef.rID FROM " + KomisyonDonemUye.class.getSimpleName()
					+ " AS o WHERE o.komisyonDonemRef.komisyonRef.rID IN  (" + whereCondition + ReferansTipi._KOMISYON.getCode() + " )) "));

			/* Egitmenler */
			kisilistesi.addAll(loadByQuery("SELECT OBJECT(n) FROM " + Kisi.class.getSimpleName() + " AS n WHERE n.rID IN (SELECT o.egitmenRef.kisiRef.rID FROM " + Egitimogretmen.class.getSimpleName() + " AS o WHERE o.egitimRef.rID IN  ("
					+ whereCondition + ReferansTipi._EGITMEN.getCode() + " )) "));

			/* Egitim Katilimcilari */
			kisilistesi.addAll(loadByQuery("SELECT OBJECT(n) FROM " + Kisi.class.getSimpleName() + " AS n WHERE n.rID IN (SELECT o.kisiRef.rID FROM " + Egitimkatilimci.class.getSimpleName() + " AS o WHERE o.egitimRef.rID IN  ("
					+ whereCondition + ReferansTipi._EGITIMKATILIMCI.getCode() + " )) "));

			/* Etkinlik Katilimcilari */
			kisilistesi.addAll(loadByQuery("SELECT OBJECT(n) FROM " + Kisi.class.getSimpleName() + " AS n WHERE n.rID IN (SELECT o.kisiRef.rID FROM " + Etkinlikkatilimci.class.getSimpleName() + " AS o WHERE o.etkinlikRef.rID IN  ("
					+ whereCondition + ReferansTipi._ETKINLIKKATILIMCI.getCode() + " )) "));

			/* Isyeri Temsilcileri */
			kisilistesi.addAll(loadByQuery("SELECT OBJECT(n) FROM " + Kisi.class.getSimpleName() + " AS n WHERE n.rID IN (SELECT o.uyeRef.kisiRef.rID FROM " + IsYeriTemsilcileri.class.getSimpleName() + " AS o WHERE o.kurumRef.rID IN  ("
					+ whereCondition + ReferansTipi._ISYERITEMSILCISI.getCode() + " )) "));

			/* Kurumlar listesi */
			kisilistesi.addAll(loadByQuery("SELECT OBJECT(n) FROM " + Kisi.class.getSimpleName() + " AS n WHERE n.rID IN (SELECT o.kisiRef.rID FROM " + KurumKisi.class.getSimpleName() + " AS o WHERE o.kurumRef.rID IN  (" + whereCondition
					+ ReferansTipi._ISYERITEMSILCISI.getCode() + " )) "));

		} catch (Exception e) {
			e.printStackTrace();
			try {
				throw new DBException(e, this.getClass().getName(), "Load", "Kayıt: ");
			} catch (DBException e1) {
				e1.printStackTrace();
			}
		} finally {
			entityManager.close();
		}

		return kisilistesi;
	}

	@SuppressWarnings("unchecked")
	public ArrayList<String> odemeTopluKaydet(List<Odeme> odemeListesi) throws DBException {
		ArrayList<String> hataMesajlari = new ArrayList<String>();
		EntityManager entityManager = getEntityManagerFactory().createEntityManager();
		List<Uyeaidat> uyeAidatListesi = null;
		try {
			for (Odeme odeme : odemeListesi) {
				entityManager.getTransaction().begin();
				BigDecimal kalan = odeme.getMiktar();
				uyeAidatListesi = loadByQuery(Uyeaidat.class.getSimpleName(), "o.uyeRef.kisiRef.rID=" + odeme.getKisiRef().getRID() + " AND o.miktar > o.odenen ORDER BY o.yil,o.ay ASC");
				if (!isEmpty(uyeAidatListesi)) {
					if (uyeAidatListesi.size() >= 1) {
						for (int x = 0; x < uyeAidatListesi.size() - 1; x++) {
							if (uyeAidatListesi.get(x).getKalan().longValue() > 0) {
								if (kalan.subtract(uyeAidatListesi.get(x).getKalan()).longValue() > 0) {
									kalan = kalan.subtract(uyeAidatListesi.get(x).getKalan());
									uyeAidatListesi.get(x).setOdenen(uyeAidatListesi.get(x).getMiktar());
									entityManager.merge(uyeAidatListesi.get(x));
								} else {
									uyeAidatListesi.get(x).setOdenen(uyeAidatListesi.get(x).getOdenen().add(kalan));
									entityManager.merge(uyeAidatListesi.get(x));
									kalan = BigDecimal.ZERO;
								}
							}
						}
					}
					if (kalan.longValue() > 0) {
						Uyeaidat sonAidat = uyeAidatListesi.get(uyeAidatListesi.size() - 1);
						sonAidat.setOdenen(sonAidat.getOdenen().add(kalan));
						entityManager.merge(sonAidat);
					}
					entityManager.persist(odeme);
				} else {
					if (recordCount(Uyeaidat.class.getSimpleName(), "o.uyeRef.kisiRef.rID=" + odeme.getKisiRef().getRID()) > 0) {
						Uyeaidat sonAidat = (Uyeaidat) loadByQuery(Uyeaidat.class.getSimpleName(), "o.uyeRef.kisiRef.rID=" + odeme.getKisiRef().getRID() + " ORDER BY o.rID DESC").get(0);
						sonAidat.setOdenen(sonAidat.getOdenen().add(odeme.getMiktar()));
						entityManager.merge(sonAidat);
						entityManager.persist(odeme);
					} else {
						hataMesajlari.add("Üyeye ait aidat kaydı bulunamadı! Bu üyeye ait aidat ödemesi giremezsiniz!");
					}
				}
				entityManager.getTransaction().commit();
			}
		} catch (Exception e) {
			if (entityManager.getTransaction().isActive()) {
				entityManager.getTransaction().rollback();
			}
			hataMesajlari.add("Ödeme kayıtları yapılırken beklenmedik bir hata meydana geldi. Sistem yöneticisiyle iletişime geçiniz!");
			LogService.logYaz("ERROR @odemeTopluKaydet of CustomDBOperator :" + e.getMessage());
		} finally {
			entityManager.close();
		}
		return hataMesajlari;
	}

	@SuppressWarnings("unchecked")
	public boolean fazlaOdemeleriAktar() throws DBException {
		EntityManager entityManager = getEntityManagerFactory().createEntityManager();
		List<Uyeaidat> uyeAidatListesi = null;
		try {
			entityManager.getTransaction().begin();
			uyeAidatListesi = loadByQuery(Uyeaidat.class.getSimpleName(), "o.miktar < o.odenen");
			if (!isEmpty(uyeAidatListesi)) {
				for (Uyeaidat aidat : uyeAidatListesi) {
					if (recordCount(Uyeaidat.class.getSimpleName(), "o.uyeRef.rID=" + aidat.getUyeRef().getRID() + " AND o.miktar > o.odenen AND o.uyemuafiyetRef IS NULL") > 0) {
						BigDecimal fazlaOdeme = aidat.getOdenen().subtract(aidat.getMiktar(), MathContext.DECIMAL32);
						aidat.setOdenen(aidat.getMiktar());
						aidat.setUyeaidatdurum(UyeAidatDurum._ODENMIS);// Alacakli durumu kaldiriliyor...
						// System.out.println("TOPLAM FAZLA ODEME :" + fazlaOdeme);
						List<Uyeaidat> eksikOdenenAidatlar = loadByQuery(Uyeaidat.class.getSimpleName(), "o.uyeRef.rID=" + aidat.getUyeRef().getRID() + " AND o.miktar > o.odenen  AND o.uyemuafiyetRef IS NULL ORDER BY o.yil,o.ay ASC");
						for (Uyeaidat eksikAidat : eksikOdenenAidatlar) {
							BigDecimal kalanOdeme = eksikAidat.getMiktar().subtract(eksikAidat.getOdenen(), MathContext.DECIMAL32);
							// System.out.println("eksikAidat : " + eksikAidat.getUIString());
							// System.out.println("FAZLA ODEME ve KALAN ODEME :" + fazlaOdeme + " - " + kalanOdeme);
							if (fazlaOdeme.doubleValue() == 0) {
								// System.out.println("FAZLA ODEME BITTI BREAK...");
								break;
							}
							if (fazlaOdeme.doubleValue() >= kalanOdeme.doubleValue()) {
								// System.out.println("FAZLA ODEME TUM KALANI ODUYOR...");
								fazlaOdeme = fazlaOdeme.subtract(kalanOdeme, MathContext.DECIMAL32);
								eksikAidat.setOdenen(eksikAidat.getMiktar());
								eksikAidat.setUyeaidatdurum(UyeAidatDurum._ODENMIS);
								entityManager.merge(eksikAidat);
							} else {
								// System.out.println("FAZLA ODEME TUM KALANI ODEMIYOR, SADECE BIR KISMINI ODEYEBILIR...");
								eksikAidat.setOdenen(eksikAidat.getOdenen().add(fazlaOdeme, MathContext.DECIMAL32));
								fazlaOdeme = BigDecimal.ZERO;
								eksikAidat.setUyeaidatdurum(UyeAidatDurum._BORCLU);
								entityManager.merge(eksikAidat);
							}
						}
						entityManager.merge(aidat);
					}
				}
			}
			entityManager.getTransaction().commit();
			return true;
		} catch (Exception e) {
			if (entityManager.getTransaction().isActive()) {
				entityManager.getTransaction().rollback();
			}
			LogService.logYaz("ERROR @fazlaOdemeleriAktar of CustomDBOperator :" + e.getMessage());
			return false;
		} finally {
			entityManager.close();
		}
	}

	@SuppressWarnings("unchecked")
	public String odemeIptal(Odeme odemeIptal) throws DBException {
		String hataMesaji = null;
		EntityManager entityManager = getEntityManagerFactory().createEntityManager();
		BigDecimal kalan = odemeIptal.getMiktar();

		try {
			entityManager.getTransaction().begin();
			if (odemeIptal.getOdemeTuru() == OdemeTuru._BORC) {
				List<Borc> uyeBorcListesi = loadByQuery(Borc.class.getSimpleName(), "o.kisiRef.rID=" + odemeIptal.getKisiRef().getRID() + " AND o.odenen > 0  ORDER BY o.tarih DESC");
				if (!isEmpty(uyeBorcListesi)) {
					for (Borc borc : uyeBorcListesi) {
						if (kalan.doubleValue() > 0) {
							if (kalan.doubleValue() >= borc.getOdenen().doubleValue()) {
								// Kalan miktar aidat icin odenenden fazla
								kalan = kalan.subtract(borc.getOdenen(), mc);
								borc.setOdenen(BigDecimal.ZERO);
							} else {
								// Kalan miktar aidat icin odenenden az veya esit
								borc.setOdenen(borc.getOdenen().subtract(kalan, mc));
								kalan = BigDecimal.ZERO;
							}
							borc.borcDurumGuncelle();
							entityManager.merge(borc);
						} else {
							break;
						}
					}
					if (kalan.doubleValue() == 0) {
						logKaydet(odemeIptal, IslemTuru._SILME, odemeIptal.getValue());
						entityManager.remove(entityManager.merge(odemeIptal));
					} else {
						logKaydet(odemeIptal, IslemTuru._GUNCELLEME, "");
						hataMesaji = "Üyenin aidat ödemeleri, ödeme tutarından az olduğu için kısmen iptal yapıldı.";
						odemeIptal.setMiktar(kalan);
						entityManager.merge(odemeIptal);
					}
				} else {
					hataMesaji = "Üyenin borç ödemesi bulunamadı.";
					return hataMesaji;
				}
			} else {
				List<Uyeaidat> uyeAidatListesi = loadByQuery(Uyeaidat.class.getSimpleName(), "o.uyeRef.kisiRef.rID=" + odemeIptal.getKisiRef().getRID() + " AND o.odenen > 0  ORDER BY o.yil DESC,o.ay DESC");
				if (!isEmpty(uyeAidatListesi)) {
					for (Uyeaidat aidat : uyeAidatListesi) {
						if (kalan.doubleValue() > 0) {
							if (kalan.doubleValue() >= aidat.getOdenen().doubleValue()) {
								// Kalan miktar aidat icin odenenden fazla
								kalan = kalan.subtract(aidat.getOdenen(), mc);
								aidat.setOdenen(BigDecimal.ZERO);
							} else {
								// Kalan miktar aidat icin odenenden az veya esit
								aidat.setOdenen(aidat.getOdenen().subtract(kalan, mc));
								kalan = BigDecimal.ZERO;
							}
							aidat.uyeAidatDurumGuncelle();
							entityManager.merge(aidat);
						} else {
							break;
						}
					}
					if (kalan.doubleValue() == 0) {
						logKaydet(odemeIptal, IslemTuru._SILME, odemeIptal.getValue());
						entityManager.remove(entityManager.merge(odemeIptal));
					} else {
						logKaydet(odemeIptal, IslemTuru._GUNCELLEME, "");
						hataMesaji = "Üyenin aidat ödemeleri, ödeme tutarından az olduğu için kısmen iptal yapıldı.";
						odemeIptal.setMiktar(kalan);
						entityManager.merge(odemeIptal);
					}
				} else {
					hataMesaji = "Üyenin aidat ödemesi bulunamadı.";
					return hataMesaji;
				}
			}
			entityManager.getTransaction().commit();
		} catch (Exception e) {
			if (entityManager.getTransaction().isActive()) {
				entityManager.getTransaction().rollback();
			}
			hataMesaji = "İşlem sırasında beklenmedik bir hata meydana geldi.";
			LogService.logYaz("ERROR @odemeIptal of CustomDBOperator ", e);
		} finally {
			entityManager.close();
		}
		return hataMesaji;
	}

	private static boolean isEmpty(Collection col) {
		return col == null || col.size() == 0;
	}

	@SuppressWarnings("unused")
	private static boolean isEmpty(Object[] col) {
		return col == null || col.length == 0;
	}

	private boolean isEmpty(String str) {
		return str == null || str.trim().length() == 0;
	}

}
