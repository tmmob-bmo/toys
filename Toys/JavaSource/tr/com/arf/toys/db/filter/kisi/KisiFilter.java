package tr.com.arf.toys.db.filter.kisi; 
 
import java.util.ArrayList;
import java.util.List;

import tr.com.arf.framework.db.enumerated._common.EvetHayir;
import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.framework.db.model.query.QueryObject;
import tr.com.arf.toys.db.enumerated.kisi.KimlikTip;
import tr.com.arf.toys.db.model.system.Sehir;
import tr.com.arf.toys.utility.application.ApplicationConstant;
 
 
public class KisiFilter extends BaseFilter { 
	
private static final long serialVersionUID = 1L; 
		
	private Long kimlikno; 
	private Long kimliknoEquals;
	private String ad; 
	private String soyad; 
	private KimlikTip kimliktip; 
	private Sehir sehirRef;
	private EvetHayir iletisimtercih;
	
	@Override 
	public void init() { 
		this.kimlikno = null; 	
		this.ad = null; 	
		this.soyad = null; 	
		this.kimliktip = KimlikTip._NULL;	
		this.sehirRef = null;
		this.iletisimtercih = EvetHayir._NULL;
	} 
	 
	@Override 
	public List<ArrayList<QueryObject>> createQueryCriterias(){ 
		ArrayList<ArrayList<QueryObject>> criteriaList = new ArrayList<ArrayList<QueryObject>>(); 
		ArrayList<QueryObject> kriterler = new ArrayList<QueryObject>();	 
		ArrayList<QueryObject> opsiyonelKriterler = new ArrayList<QueryObject>(); 
		try { 
			if(validateQueryObject(getKimlikno(), 0, false)){
				kriterler.add(new QueryObject(" o.kimlikno||'' LIKE '" + getKimlikno() + "%'")); 
			} 
			if(validateQueryObject(getAd(), null, false)){  
				kriterler.add(new QueryObject("ad", ApplicationConstant._beginsWith, getAd())); 
			} 			
			if(validateQueryObject(getSoyad(), null, false)){ 
				kriterler.add(new QueryObject("soyad", ApplicationConstant._beginsWith, getSoyad())); 
			} 
			if(validateQueryObject(getKimliktip(), null, true)){ 
				kriterler.add(new QueryObject("kimliktip", ApplicationConstant._equals, getKimliktip())); 
			} 
			if(validateQueryObject(getSehirRef(), null, false)){ 
				kriterler.add(new QueryObject(getSehirRef().getRID() + " IN (Select m.sehirRef.rID From Adres m WHERE m.kisiRef.rID = o.rID)", ApplicationConstant._userDefined, getSehirRef())); 
			} 
			if(validateQueryObject(getIletisimtercih(), null, true)){ 
				kriterler.add(new QueryObject("iletisimtercih", ApplicationConstant._equals, getIletisimtercih())); 
			} 
			
		} catch(Exception e){ 
			System.out.println("Error @createQueryCriterias method, Class : KisiFilter :" + e.getMessage()); 
		}  
		criteriaList.add(kriterler); 
		criteriaList.add(opsiyonelKriterler); 
		return criteriaList; 
	} 

	public Long getKimlikno() {
		return kimlikno;
	}

	public void setKimlikno(Long kimlikno) {
		this.kimlikno = kimlikno;
	}

	public String getAd() { 
		return this.ad; 
	} 
	 
	public void setAd(String ad) { 
		this.ad = ad; 
	} 
  
	public String getSoyad() { 
		return this.soyad; 
	} 
	 
	public void setSoyad(String soyad) { 
		this.soyad = soyad; 
	} 
  
	public KimlikTip getKimliktip() { 
		return this.kimliktip; 
	} 
	 
	public void setKimliktip(KimlikTip kimliktip) { 
		this.kimliktip = kimliktip; 
	}

	public Long getKimliknoEquals() {
		return kimliknoEquals;
	}

	public void setKimliknoEquals(Long kimliknoEquals) {
		this.kimliknoEquals = kimliknoEquals;
	}

	public Sehir getSehirRef() {
		return sehirRef;
	}

	public void setSehirRef(Sehir sehirRef) {
		this.sehirRef = sehirRef;
	}

	public EvetHayir getIletisimtercih() {
		return iletisimtercih;
	}

	public void setIletisimtercih(EvetHayir iletisimtercih) {
		this.iletisimtercih = iletisimtercih;
	}
	
} 
