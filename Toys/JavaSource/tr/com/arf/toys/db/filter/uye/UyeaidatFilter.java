package tr.com.arf.toys.db.filter.uye; 
 
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.framework.db.model.query.QueryObject;
import tr.com.arf.toys.db.enumerated.uye.UyeAidatDurum;
import tr.com.arf.toys.db.model.uye.Uye;
import tr.com.arf.toys.db.model.uye.Uyemuafiyet;
import tr.com.arf.toys.utility.application.ApplicationConstant;
 
 
public class UyeaidatFilter extends BaseFilter { 
	
private static final long serialVersionUID = 1L; 
	
	private Uye uyeRef; 
	
	private int yil; 
	private int ay; 
	private BigDecimal miktar; 
	private BigDecimal odenen; 
	private UyeAidatDurum uyeaidatdurum; 
	private Uyemuafiyet uyemuafiyetRef; 
	
	@Override 
	public void init() { 
		this.uyeRef = null; 	
		this.yil = 0; 	
		this.ay = 0; 	
		this.miktar = null; 	
		this.odenen = null; 	
		this.uyeaidatdurum = UyeAidatDurum._NULL;
		this.uyemuafiyetRef = null;
	} 
	 
	@Override 
	public List<ArrayList<QueryObject>> createQueryCriterias(){ 
		ArrayList<ArrayList<QueryObject>> criteriaList = new ArrayList<ArrayList<QueryObject>>(); 
		ArrayList<QueryObject> kriterler = new ArrayList<QueryObject>();	 
		ArrayList<QueryObject> opsiyonelKriterler = new ArrayList<QueryObject>(); 
		try { 	 
			if(validateQueryObject(getUyeRef(), null, false)){ 
				kriterler.add(new QueryObject("uyeRef", ApplicationConstant._equals, getUyeRef())); 
			} 
			if(validateQueryObject(getYil(), 0, false)){ 
				kriterler.add(new QueryObject("yil", ApplicationConstant._equals, getYil())); 
			} 
			if(validateQueryObject(getAy(), 0, false)){ 
				kriterler.add(new QueryObject("ay", ApplicationConstant._equals, getAy())); 
			} 
			if(validateQueryObject(getMiktar(), 0, false)){
				kriterler.add(new QueryObject("miktar", ApplicationConstant._equals, getMiktar())); 	
			}
			if(validateQueryObject(getOdenen(), 0, false)){
				kriterler.add(new QueryObject("odenen", ApplicationConstant._equals, getOdenen())); 	
			}
			if(validateQueryObject(getUyeaidatdurum(), null, true)){ 
				kriterler.add(new QueryObject("uyeaidatdurum", ApplicationConstant._equals, getUyeaidatdurum())); 
			} 
			if(validateQueryObject(getUyemuafiyetRef(), null, false)){ 
				kriterler.add(new QueryObject("uyemuafiyetRef", ApplicationConstant._equals, getUyemuafiyetRef())); 
			} 
		} catch(Exception e){ 
			System.out.println("Error @createQueryCriterias method, Class : UyeaidatFilter :" + e.getMessage()); 
		}  
		criteriaList.add(kriterler); 
		criteriaList.add(opsiyonelKriterler); 
		return criteriaList; 
	} 
	 
	public Uye getUyeRef() { 
		return this.uyeRef; 
	} 
	 
	public void setUyeRef(Uye uyeRef) { 
		this.uyeRef = uyeRef; 
	} 
  
	public int getYil() { 
		return this.yil; 
	} 
	 
	public void setYil(int yil) { 
		this.yil = yil; 
	} 
  
	public int getAy() { 
		return this.ay; 
	} 
	 
	public void setAy(int ay) { 
		this.ay = ay; 
	} 
  
	public BigDecimal getMiktar() { 
		return this.miktar; 
	} 
	 
	public void setMiktar(BigDecimal miktar) { 
		this.miktar = miktar; 
	} 
  
	public BigDecimal getOdenen() { 
		return this.odenen; 
	} 
	 
	public void setOdenen(BigDecimal odenen) { 
		this.odenen = odenen; 
	} 
  
	public UyeAidatDurum getUyeaidatdurum() { 
		return this.uyeaidatdurum; 
	} 
	 
	public void setUyeaidatdurum(UyeAidatDurum uyeaidatdurum) { 
		this.uyeaidatdurum = uyeaidatdurum; 
	}

	public Uyemuafiyet getUyemuafiyetRef() {
		return uyemuafiyetRef;
	}

	public void setUyemuafiyetRef(Uyemuafiyet uyemuafiyetRef) {
		this.uyemuafiyetRef = uyemuafiyetRef;
	} 
	
} 
