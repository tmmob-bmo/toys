package tr.com.arf.toys.db.filter.evrak; 
 
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.framework.db.model.query.QueryObject;
import tr.com.arf.toys.db.model.evrak.Evrak;
import tr.com.arf.toys.db.model.system.Birim;
import tr.com.arf.toys.db.model.system.Personel;
import tr.com.arf.toys.utility.application.ApplicationConstant;
 
 
public class EvrakdolasimFilter extends BaseFilter { 
	
private static final long serialVersionUID = 1L; 
	
	private Evrak evrakRef; 
	
	private Long rID; 
	private Birim birimRef; 
	private Personel personelRef; 
	private java.util.Date gelistarih; 
	private java.util.Date gelistarihMax; 
	private java.util.Date gidistarih; 
	private java.util.Date gidistarihMax; 
	
	@Override 
	public void init() { 
		this.rID = null; 	
		this.evrakRef = null; 	
		this.birimRef = null; 	
		this.personelRef = null; 	
		this.gelistarih = null; 	
		this.gelistarihMax = null; 	
		this.gidistarih = null; 	
		this.gidistarihMax = null; 	
	} 
	 
	@Override 
	public List<ArrayList<QueryObject>> createQueryCriterias(){ 
		ArrayList<ArrayList<QueryObject>> criteriaList = new ArrayList<ArrayList<QueryObject>>(); 
		ArrayList<QueryObject> kriterler = new ArrayList<QueryObject>();	 
		ArrayList<QueryObject> opsiyonelKriterler = new ArrayList<QueryObject>(); 
		try { 	 
			if(validateQueryObject(getRID(), null, false)){ 
				kriterler.add(new QueryObject("rID", ApplicationConstant._equals, getRID())); 
			} 
			if(validateQueryObject(getEvrakRef(), null, false)){ 
				kriterler.add(new QueryObject("evrakRef", ApplicationConstant._equals, getEvrakRef())); 
			} 
			if(validateQueryObject(getBirimRef(), null, false)){ 
				kriterler.add(new QueryObject("birimRef", ApplicationConstant._equals, getBirimRef())); 
			} 
			if(validateQueryObject(getPersonelRef(), null, false)){ 
				kriterler.add(new QueryObject("personelRef", ApplicationConstant._equals, getPersonelRef())); 
			} 
			if(validateQueryObject(getGelistarih(), null, false)  && validateQueryObject(getGelistarihMax(), null, false)){
				kriterler.add(new QueryObject("gelistarih", ApplicationConstant._equalsAndLargerThan, getGelistarih())); 
				kriterler.add(new QueryObject("gelistarih", ApplicationConstant._equalsAndSmallerThan, getGelistarihMax())); 
			} else if(validateQueryObject(getGelistarih(), null, false)){ 
				kriterler.add(new QueryObject("gelistarih", ApplicationConstant._equalsAndLargerThan, getGelistarih()));
			}
			if(validateQueryObject(getGidistarih(), null, false)  && validateQueryObject(getGidistarihMax(), null, false)){
				kriterler.add(new QueryObject("gidistarih", ApplicationConstant._equalsAndLargerThan, getGidistarih())); 
				kriterler.add(new QueryObject("gidistarih", ApplicationConstant._equalsAndSmallerThan, getGidistarihMax())); 
			} else if(validateQueryObject(getGidistarih(), null, false)){ 
				kriterler.add(new QueryObject("gidistarih", ApplicationConstant._equalsAndLargerThan, getGidistarih()));
			}
		} catch(Exception e){ 
			System.out.println("Error @createQueryCriterias method, Class : EvrakdolasimFilter :" + e.getMessage()); 
		}  
		criteriaList.add(kriterler); 
		criteriaList.add(opsiyonelKriterler); 
		return criteriaList; 
	} 
	 
	public Long getRID() { 
		return this.rID; 
	} 
	 
	public void setRID(Long rID) { 
		this.rID = rID; 
	} 
  
	public Evrak getEvrakRef() { 
		return this.evrakRef; 
	} 
	 
	public void setEvrakRef(Evrak evrakRef) { 
		this.evrakRef = evrakRef; 
	} 
  
	public Birim getBirimRef() { 
		return this.birimRef; 
	} 
	 
	public void setBirimRef(Birim birimRef) { 
		this.birimRef = birimRef; 
	} 
  
	public Personel getPersonelRef() { 
		return this.personelRef; 
	} 
	 
	public void setPersonelRef(Personel personelRef) { 
		this.personelRef = personelRef; 
	} 
  
	public Date getGelistarih() { 
		return this.gelistarih; 
	} 
	 
	public void setGelistarih(Date gelistarih) { 
		this.gelistarih = gelistarih; 
	} 
  
	public Date getGelistarihMax() { 
		return this.gelistarihMax; 
	} 
	 
	public void setGelistarihMax(Date gelistarihMax) { 
		this.gelistarihMax = gelistarihMax; 
	} 
  
	public Date getGidistarih() { 
		return this.gidistarih; 
	} 
	 
	public void setGidistarih(Date gidistarih) { 
		this.gidistarih = gidistarih; 
	} 
  
	public Date getGidistarihMax() { 
		return this.gidistarihMax; 
	} 
	 
	public void setGidistarihMax(Date gidistarihMax) { 
		this.gidistarihMax = gidistarihMax; 
	} 
  
	
} 
