package tr.com.arf.toys.db.filter.etkinlik; 
 
import java.util.ArrayList;
import java.util.List;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.framework.db.model.query.QueryObject;
import tr.com.arf.toys.db.enumerated.etkinlik.KatilimciDurum;
import tr.com.arf.toys.db.model.etkinlik.Etkinlik;
import tr.com.arf.toys.db.model.kisi.Kisi;
import tr.com.arf.toys.utility.application.ApplicationConstant;
 
 
public class EtkinlikkatilimciFilter extends BaseFilter { 
	
private static final long serialVersionUID = 1L; 
	
	private Etkinlik etkinlikRef; 
	
	private Long rID; 
	private Kisi kisiRef; 
	private KatilimciDurum durum; 
	
	private String ad;
	private String soyad;
	
	@Override 
	public void init() { 
		this.rID = null; 	
		this.etkinlikRef = null; 	
		this.kisiRef = null; 	
		this.durum = KatilimciDurum._NULL;	
		this.ad = null;
		this.soyad = null;
	} 
	 
	@Override 
	public List<ArrayList<QueryObject>> createQueryCriterias(){ 
		ArrayList<ArrayList<QueryObject>> criteriaList = new ArrayList<ArrayList<QueryObject>>(); 
		ArrayList<QueryObject> kriterler = new ArrayList<QueryObject>();	 
		ArrayList<QueryObject> opsiyonelKriterler = new ArrayList<QueryObject>(); 
		try { 	 
			if(validateQueryObject(getRID(), null, false)){ 
				kriterler.add(new QueryObject("rID", ApplicationConstant._equals, getRID())); 
			} 
			if(validateQueryObject(getEtkinlikRef(), null, false)){ 
				kriterler.add(new QueryObject("etkinlikRef", ApplicationConstant._equals, getEtkinlikRef())); 
			} 
			if(validateQueryObject(getKisiRef(), null, false)){ 
				kriterler.add(new QueryObject("kisiRef", ApplicationConstant._equals, getKisiRef())); 
			} 
			if(validateQueryObject(getDurum(), null, true)){ 
				kriterler.add(new QueryObject("durum", ApplicationConstant._equals, getDurum())); 
			} 
			if(validateQueryObject(getAd(), null, false)){ 
				kriterler.add(new QueryObject("kisiRef.ad", ApplicationConstant._beginsWith, getAd())); 
			}
			if(validateQueryObject(getSoyad(), null, false)){ 
				kriterler.add(new QueryObject("kisiRef.soyad", ApplicationConstant._beginsWith, getSoyad())); 
			} 
		} catch(Exception e){ 
			System.out.println("Error @createQueryCriterias method, Class : EtkinlikkatilimciFilter :" + e.getMessage()); 
		}  
		criteriaList.add(kriterler); 
		criteriaList.add(opsiyonelKriterler); 
		return criteriaList; 
	} 
	 
	public Long getRID() { 
		return this.rID; 
	} 
	 
	public void setRID(Long rID) { 
		this.rID = rID; 
	} 
  
	public Etkinlik getEtkinlikRef() { 
		return this.etkinlikRef; 
	} 
	 
	public void setEtkinlikRef(Etkinlik etkinlikRef) { 
		this.etkinlikRef = etkinlikRef; 
	} 
  
	public Kisi getKisiRef() { 
		return this.kisiRef; 
	} 
	 
	public void setKisiRef(Kisi kisiRef) { 
		this.kisiRef = kisiRef; 
	} 
  
	public KatilimciDurum getDurum() { 
		return this.durum; 
	} 
	 
	public void setDurum(KatilimciDurum durum) { 
		this.durum = durum; 
	} 
  
	public String getAd() {
		return ad;
	}

	public void setAd(String ad) {
		this.ad = ad;
	}

	public String getSoyad() {
		return soyad;
	}

	public void setSoyad(String soyad) {
		this.soyad = soyad;
	} 
	
} 
