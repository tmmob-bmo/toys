package tr.com.arf.toys.db.filter.system; 
 
import java.util.ArrayList;
import java.util.List;

import tr.com.arf.framework.db.enumerated._common.EvetHayir;
import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.framework.db.model.query.QueryObject;
import tr.com.arf.toys.db.model.kisi.Kurum;
import tr.com.arf.toys.db.model.system.Banka;
import tr.com.arf.toys.utility.application.ApplicationConstant;
 
 
public class KurumHesapFilter extends BaseFilter { 
	
private static final long serialVersionUID = 1L; 
	
	private Banka bankaRef;
	private Long hesapno;
	private Long subekodu;
	private Long ibannumarasi;
	private Kurum kurumRef;
	private EvetHayir gecerli; 
	
	@Override 
	public void init() { 
		this.bankaRef=null;
		this.hesapno=null;
		this.ibannumarasi=null;
		this.subekodu=null;
		this.kurumRef=null;
		this.gecerli = EvetHayir._NULL;	
		
	} 
	 
	@Override 
	public List<ArrayList<QueryObject>> createQueryCriterias(){ 
		ArrayList<ArrayList<QueryObject>> criteriaList = new ArrayList<ArrayList<QueryObject>>(); 
		ArrayList<QueryObject> kriterler = new ArrayList<QueryObject>();	 
		ArrayList<QueryObject> opsiyonelKriterler = new ArrayList<QueryObject>(); 
		try { 	 
			if(validateQueryObject(getBankaRef(), null, false)){ 
				kriterler.add(new QueryObject("bankaRef", ApplicationConstant._equals, getBankaRef())); 
			}
			if(validateQueryObject(getHesapno(), null, false)){ 
				kriterler.add(new QueryObject("hesapno", ApplicationConstant._beginsWith, getHesapno())); 
			} 
			if(validateQueryObject(getSubekodu(), null, false)){ 
				kriterler.add(new QueryObject("subekodu", ApplicationConstant._beginsWith, getSubekodu())); 
			} 
			if(validateQueryObject(getIbannumarasi(), null, false)){ 
				kriterler.add(new QueryObject("ibannumarasi", ApplicationConstant._beginsWith, getIbannumarasi())); 
			} 
			if(validateQueryObject(getKurumRef(), null, false)){ 
				kriterler.add(new QueryObject("kurumRef", ApplicationConstant._equals, getKurumRef())); 
			}
			if(validateQueryObject(getGecerli(), null, true)){ 
				kriterler.add(new QueryObject("gecerli", ApplicationConstant._equals, getGecerli())); 
			} 
			
		} catch(Exception e){ 
			System.out.println("Error @createQueryCriterias method, Class : KurumHesapFilter :" + e.getMessage()); 
		}  
		criteriaList.add(kriterler); 
		criteriaList.add(opsiyonelKriterler); 
		return criteriaList; 
	}


	public Long getHesapno() {
		return hesapno;
	}

	public void setHesapno(Long hesapno) {
		this.hesapno = hesapno;
	}

	public Long getSubekodu() {
		return subekodu;
	}

	public void setSubekodu(Long subekodu) {
		this.subekodu = subekodu;
	}

	public Long getIbannumarasi() {
		return ibannumarasi;
	}

	public void setIbannumarasi(Long ibannumarasi) {
		this.ibannumarasi = ibannumarasi;
	}

	public Kurum getKurumRef() {
		return kurumRef;
	}

	public void setKurumRef(Kurum kurumRef) {
		this.kurumRef = kurumRef;
	}

	public EvetHayir getGecerli() {
		return gecerli;
	}

	public void setGecerli(EvetHayir gecerli) {
		this.gecerli = gecerli;
	}

	public Banka getBankaRef() {
		return bankaRef;
	}

	public void setBankaRef(Banka bankaRef) {
		this.bankaRef = bankaRef;
	}

	
	
} 
