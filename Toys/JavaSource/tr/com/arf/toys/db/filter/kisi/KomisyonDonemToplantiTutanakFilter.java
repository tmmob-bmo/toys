package tr.com.arf.toys.db.filter.kisi; 
 
import java.util.ArrayList;
import java.util.List;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.framework.db.model.query.QueryObject;
import tr.com.arf.toys.db.model.kisi.KomisyonDonemToplanti;
import tr.com.arf.toys.db.model.system.Personel;
import tr.com.arf.toys.utility.application.ApplicationConstant;
 
 
public class KomisyonDonemToplantiTutanakFilter extends BaseFilter { 
	
private static final long serialVersionUID = 1L; 
	
	private KomisyonDonemToplanti komisyonDonemToplantiRef; 
	
	private Long rID; 
	private Personel yukleyenRef; 
	private String path; 
	
	@Override 
	public void init() { 
		this.rID = null; 	
		this.komisyonDonemToplantiRef = null; 	
		this.yukleyenRef = null; 	
		this.path = null; 	
	} 
	 
	@Override 
	public List<ArrayList<QueryObject>> createQueryCriterias(){ 
		ArrayList<ArrayList<QueryObject>> criteriaList = new ArrayList<ArrayList<QueryObject>>(); 
		ArrayList<QueryObject> kriterler = new ArrayList<QueryObject>();	 
		ArrayList<QueryObject> opsiyonelKriterler = new ArrayList<QueryObject>(); 
		try { 	 
			if(validateQueryObject(getRID(), null, false)){ 
				kriterler.add(new QueryObject("rID", ApplicationConstant._equals, getRID())); 
			} 
			if(validateQueryObject(getKomisyonDonemToplantiRef(), null, false)){ 
				kriterler.add(new QueryObject("komisyonDonemToplantiRef", ApplicationConstant._equals, getKomisyonDonemToplantiRef())); 
			} 
			if(validateQueryObject(getYukleyenRef(), null, false)){ 
				kriterler.add(new QueryObject("yukleyenRef", ApplicationConstant._equals, getYukleyenRef())); 
			} 
			if(validateQueryObject(getPath(), null, false)){ 
				kriterler.add(new QueryObject("path", ApplicationConstant._beginsWith, getPath(), false)); 
			} 
		} catch(Exception e){ 
			System.out.println("Error @createQueryCriterias method, Class : KomisyonDonemToplantiTutanakFilter :" + e.getMessage()); 
		}  
		criteriaList.add(kriterler); 
		criteriaList.add(opsiyonelKriterler); 
		return criteriaList; 
	} 
	 
	public Long getRID() { 
		return this.rID; 
	} 
	 
	public void setRID(Long rID) { 
		this.rID = rID; 
	} 
  
	public KomisyonDonemToplanti getKomisyonDonemToplantiRef() { 
		return this.komisyonDonemToplantiRef; 
	} 
	 
	public void setKomisyonDonemToplantiRef(KomisyonDonemToplanti komisyonDonemToplantiRef) { 
		this.komisyonDonemToplantiRef = komisyonDonemToplantiRef; 
	} 
  
	public Personel getYukleyenRef() { 
		return this.yukleyenRef; 
	} 
	 
	public void setYukleyenRef(Personel yukleyenRef) { 
		this.yukleyenRef = yukleyenRef; 
	} 
  
	public String getPath() { 
		return this.path; 
	} 
	 
	public void setPath(String path) { 
		this.path = path; 
	} 
  
	
} 
