package tr.com.arf.toys.db.filter.iletisim;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.framework.db.model.query.QueryObject;
import tr.com.arf.toys.db.enumerated.iletisim.GonderimDurum;
import tr.com.arf.toys.db.model.iletisim.Gonderim;
import tr.com.arf.toys.db.model.kisi.Kisi;
import tr.com.arf.toys.db.model.system.Birim;
import tr.com.arf.toys.utility.application.ApplicationConstant;

public class EpostagonderimlogFilter extends BaseFilter {

	private static final long serialVersionUID = 1L;

	private Gonderim gonderimRef;

	private Long rID;
	private Kisi kisiRef;
	private java.util.Date tarih;
	private java.util.Date tarihMax;
	private GonderimDurum durum;
	private String aciklama;
	private Birim birimRef;

	private Kisi gonderenRef;
	private String gonderenAd;
	private String gonderenSoyad;

	@Override
	public void init() {
		this.rID = null;
		this.gonderimRef = null;
		this.kisiRef = null;
		this.tarih = null;
		this.tarihMax = null;
		this.birimRef = null;
		this.durum = GonderimDurum._NULL;
		this.aciklama = null;
		this.gonderenRef = null;
		this.gonderenAd = null;
		this.gonderenSoyad = null;
	}

	@Override
	public List<ArrayList<QueryObject>> createQueryCriterias() {
		ArrayList<ArrayList<QueryObject>> criteriaList = new ArrayList<ArrayList<QueryObject>>();
		ArrayList<QueryObject> kriterler = new ArrayList<QueryObject>();
		ArrayList<QueryObject> opsiyonelKriterler = new ArrayList<QueryObject>();
		try {
			if (validateQueryObject(getRID(), null, false)) {
				kriterler.add(new QueryObject("rID", ApplicationConstant._equals, getRID()));
			}
			if (validateQueryObject(getGonderimRef(), null, false)) {
				kriterler.add(new QueryObject("gonderimRef", ApplicationConstant._equals, getGonderimRef()));
			}
			if (validateQueryObject(getKisiRef(), null, false)) {
				kriterler.add(new QueryObject("kisiRef", ApplicationConstant._equals, getKisiRef()));
			}
			if (validateQueryObject(getTarih(), null, false) && validateQueryObject(getTarihMax(), null, false)) {
				kriterler.add(new QueryObject("tarih", ApplicationConstant._equalsAndLargerThan, getTarih()));
				kriterler.add(new QueryObject("tarih", ApplicationConstant._equalsAndSmallerThan, getTarihMax()));
			} else if (validateQueryObject(getTarih(), null, false)) {
				kriterler.add(new QueryObject("tarih", ApplicationConstant._equalsAndLargerThan, getTarih()));
			}
			if (validateQueryObject(getBirimRef(), null, false)) {
				kriterler.add(new QueryObject("birimRef", ApplicationConstant._equals, getBirimRef()));
			}
			if (validateQueryObject(getDurum(), null, true)) {
				kriterler.add(new QueryObject("durum", ApplicationConstant._equals, getDurum()));
			}
			if (validateQueryObject(getGonderenRef(), null, false)) {
				kriterler.add(new QueryObject("gonderenRef", ApplicationConstant._equals, getGonderenRef()));
			}
			if (validateQueryObject(getGonderenAd(), null, false)) {
				kriterler.add(new QueryObject("gonderenRef.ad", ApplicationConstant._beginsWith, getGonderenAd()));
			}
			if (validateQueryObject(getGonderenSoyad(), null, false)) {
				kriterler.add(new QueryObject("gonderenRef.soyad", ApplicationConstant._beginsWith, getGonderenSoyad()));
			}
			if (validateQueryObject(getAciklama(), null, false)) {
				kriterler.add(new QueryObject("aciklama", ApplicationConstant._beginsWith, getAciklama(), false));
			}
		} catch (Exception e) {
			System.out.println("Error @createQueryCriterias method, Class : EpostagonderimlogFilter :" + e.getMessage());
		}
		criteriaList.add(kriterler);
		criteriaList.add(opsiyonelKriterler);
		return criteriaList;
	}

	public Long getRID() {
		return this.rID;
	}

	public void setRID(Long rID) {
		this.rID = rID;
	}

	public Gonderim getGonderimRef() {
		return this.gonderimRef;
	}

	public void setGonderimRef(Gonderim gonderimRef) {
		this.gonderimRef = gonderimRef;
	}

	public Kisi getKisiRef() {
		return this.kisiRef;
	}

	public void setKisiRef(Kisi kisiRef) {
		this.kisiRef = kisiRef;
	}

	public Date getTarih() {
		return this.tarih;
	}

	public void setTarih(Date tarih) {
		this.tarih = tarih;
	}

	public Date getTarihMax() {
		return this.tarihMax;
	}

	public void setTarihMax(Date tarihMax) {
		this.tarihMax = tarihMax;
	}

	public GonderimDurum getDurum() {
		return this.durum;
	}

	public void setDurum(GonderimDurum durum) {
		this.durum = durum;
	}

	public String getAciklama() {
		return this.aciklama;
	}

	public void setAciklama(String aciklama) {
		this.aciklama = aciklama;
	}

	public Birim getBirimRef() {
		return birimRef;
	}

	public void setBirimRef(Birim birimRef) {
		this.birimRef = birimRef;
	}

	public Kisi getGonderenRef() {
		return gonderenRef;
	}

	public void setGonderenRef(Kisi gonderenRef) {
		this.gonderenRef = gonderenRef;
	}

	public String getGonderenAd() {
		return gonderenAd;
	}

	public void setGonderenAd(String gonderenAd) {
		this.gonderenAd = gonderenAd;
	}

	public String getGonderenSoyad() {
		return gonderenSoyad;
	}

	public void setGonderenSoyad(String gonderenSoyad) {
		this.gonderenSoyad = gonderenSoyad;
	}

}
