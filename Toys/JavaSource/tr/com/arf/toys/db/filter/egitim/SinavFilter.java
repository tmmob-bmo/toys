package tr.com.arf.toys.db.filter.egitim; 
 
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import tr.com.arf.framework.db.enumerated._common.EvetHayir;
import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.framework.db.model.query.QueryObject;
import tr.com.arf.toys.db.enumerated.egitim.SinavDurum;
import tr.com.arf.toys.db.enumerated.egitim.SinavTuru;
import tr.com.arf.toys.db.model.egitim.Egitim;
import tr.com.arf.toys.db.model.system.Ilce;
import tr.com.arf.toys.db.model.system.Sehir;
import tr.com.arf.toys.utility.application.ApplicationConstant;
 
 
public class SinavFilter extends BaseFilter { 
	
private static final long serialVersionUID = 1L; 
	
	private Long rID; 
	private String ad;
	private String aciklama;
	private Egitim egitimRef;
	private BigDecimal kontenjan;
	private EvetHayir yayinda;
	private Sehir sehirRef;
	private Ilce ilceRef;
	private java.util.Date baslangictarih;
	private java.util.Date baslangictarihMax;
	private java.util.Date bitistarih;
	private java.util.Date bitistarihMax;
	private String pbkkod;
	private SinavTuru sinavturu;
	private SinavDurum sinavdurum;
	
	
	
	@Override 
	public void init() { 
		this.rID = null; 	
		this.aciklama=null;
		this.ad=null;
		this.egitimRef=null;
		this.kontenjan=null;
		this.yayinda=EvetHayir._NULL;
		this.sehirRef=null;
		this.ilceRef=null;
		this.baslangictarih=null;
		this.baslangictarihMax=null;
		this.bitistarih=null;
		this.baslangictarihMax=null;
		this.pbkkod=null;
		this.sinavturu=SinavTuru._NULL;
		this.sinavdurum=SinavDurum._NULL;
	} 
	 
	@Override 
	public List<ArrayList<QueryObject>> createQueryCriterias(){ 
		ArrayList<ArrayList<QueryObject>> criteriaList = new ArrayList<ArrayList<QueryObject>>(); 
		ArrayList<QueryObject> kriterler = new ArrayList<QueryObject>();	 
		ArrayList<QueryObject> opsiyonelKriterler = new ArrayList<QueryObject>(); 
		try { 	 
			if(validateQueryObject(getRID(), null, false)){ 
				kriterler.add(new QueryObject("rID", ApplicationConstant._equals, getRID())); 
			} 
			if(validateQueryObject(getEgitimRef(), null, false)){ 
				kriterler.add(new QueryObject("egitimRef", ApplicationConstant._equals, getEgitimRef())); 
			} 
			if(validateQueryObject(getSehirRef(), null, false)){ 
				kriterler.add(new QueryObject("sehirRef", ApplicationConstant._equals, getSehirRef())); 
			} 
			if(validateQueryObject(getIlceRef(), null, false)){ 
				kriterler.add(new QueryObject("ilceRef", ApplicationConstant._equals, getIlceRef())); 
			} 
			if(validateQueryObject(getAciklama(), null, false)){ 
				kriterler.add(new QueryObject("aciklama", ApplicationConstant._inside, getAciklama())); 
			} 
			if(validateQueryObject(getAd(), null, false)){ 
				kriterler.add(new QueryObject("ad", ApplicationConstant._inside, getAd())); 
			}
			if(validateQueryObject(getKontenjan(), null, false)){ 
				kriterler.add(new QueryObject("kontenjan", ApplicationConstant._inside, getKontenjan())); 
			}
			if(validateQueryObject(getPbkkod(), null, false)){ 
				kriterler.add(new QueryObject("pbkkod", ApplicationConstant._inside, getPbkkod())); 
			}
			if(validateQueryObject(getYayinda(), null, false)){ 
				kriterler.add(new QueryObject("yayinda", ApplicationConstant._equals, getYayinda())); 
			}
			if(validateQueryObject(getBaslangictarih(), null, false)  && validateQueryObject(getBaslangictarihMax(), null, false)){
				kriterler.add(new QueryObject("baslangictarih", ApplicationConstant._equalsAndLargerThan, getBaslangictarih())); 
				kriterler.add(new QueryObject("baslangictarih", ApplicationConstant._equalsAndSmallerThan, getBaslangictarihMax())); 
			} else if(validateQueryObject(getBaslangictarih(), null, false)){ 
				kriterler.add(new QueryObject("baslangictarih", ApplicationConstant._equalsAndLargerThan, getBaslangictarih()));
			}
			if(validateQueryObject(getBitistarih(), null, false)  && validateQueryObject(getBitistarihMax(), null, false)){
				kriterler.add(new QueryObject("bitistarih", ApplicationConstant._equalsAndLargerThan, getBitistarih())); 
				kriterler.add(new QueryObject("bitistarih", ApplicationConstant._equalsAndSmallerThan, getBitistarihMax())); 
			} else if(validateQueryObject(getBitistarih(), null, false)){ 
				kriterler.add(new QueryObject("bitistarih", ApplicationConstant._equalsAndLargerThan, getBitistarih()));
			}
			if(validateQueryObject(getSinavturu(), null, false)){ 
				kriterler.add(new QueryObject("sinavturu", ApplicationConstant._equals, getSinavturu())); 
			}
			if(validateQueryObject(getSinavdurum(), null, false)){ 
				kriterler.add(new QueryObject("sinavdurum", ApplicationConstant._equals, getSinavdurum())); 
			}
		} catch(Exception e){ 
			System.out.println("Error @createQueryCriterias method, Class : EgitimlisansFilter :" + e.getMessage()); 
		}  
		criteriaList.add(kriterler); 
		criteriaList.add(opsiyonelKriterler); 
		return criteriaList; 
	} 
	 
	public Long getRID() { 
		return this.rID; 
	} 
	 
	public void setRID(Long rID) { 
		this.rID = rID; 
	}

	public String getAd() {
		return ad;
	}

	public void setAd(String ad) {
		this.ad = ad;
	}

	public String getAciklama() {
		return aciklama;
	}

	public void setAciklama(String aciklama) {
		this.aciklama = aciklama;
	}

	public Egitim getEgitimRef() {
		return egitimRef;
	}

	public void setEgitimRef(Egitim egitimRef) {
		this.egitimRef = egitimRef;
	}

	public BigDecimal getKontenjan() {
		return kontenjan;
	}

	public void setKontenjan(BigDecimal kontenjan) {
		this.kontenjan = kontenjan;
	}

	public EvetHayir getYayinda() {
		return yayinda;
	}

	public void setYayinda(EvetHayir yayinda) {
		this.yayinda = yayinda;
	}

	public Sehir getSehirRef() {
		return sehirRef;
	}

	public void setSehirRef(Sehir sehirRef) {
		this.sehirRef = sehirRef;
	}

	public Ilce getIlceRef() {
		return ilceRef;
	}

	public void setIlceRef(Ilce ilceRef) {
		this.ilceRef = ilceRef;
	}

	public String getPbkkod() {
		return pbkkod;
	}

	public void setPbkkod(String pbkkod) {
		this.pbkkod = pbkkod;
	}

	public java.util.Date getBaslangictarih() {
		return baslangictarih;
	}

	public void setBaslangictarih(java.util.Date baslangictarih) {
		this.baslangictarih = baslangictarih;
	}

	public java.util.Date getBaslangictarihMax() {
		return baslangictarihMax;
	}

	public void setBaslangictarihMax(java.util.Date baslangictarihMax) {
		this.baslangictarihMax = baslangictarihMax;
	}

	public java.util.Date getBitistarih() {
		return bitistarih;
	}

	public void setBitistarih(java.util.Date bitistarih) {
		this.bitistarih = bitistarih;
	}

	public java.util.Date getBitistarihMax() {
		return bitistarihMax;
	}

	public void setBitistarihMax(java.util.Date bitistarihMax) {
		this.bitistarihMax = bitistarihMax;
	}

	public SinavTuru getSinavturu() {
		return sinavturu;
	}

	public void setSinavturu(SinavTuru sinavturu) {
		this.sinavturu = sinavturu;
	}

	public SinavDurum getSinavdurum() {
		return sinavdurum;
	}

	public void setSinavdurum(SinavDurum sinavdurum) {
		this.sinavdurum = sinavdurum;
	}
	

} 
