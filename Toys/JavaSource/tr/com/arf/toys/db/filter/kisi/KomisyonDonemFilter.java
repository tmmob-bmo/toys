package tr.com.arf.toys.db.filter.kisi; 
 
import java.util.ArrayList;
import java.util.List;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.framework.db.model.query.QueryObject;
import tr.com.arf.toys.db.enumerated.system.BirimYetkiTuru;
import tr.com.arf.toys.db.model.kisi.Donem;
import tr.com.arf.toys.db.model.system.Birim;
import tr.com.arf.toys.db.model.system.Komisyon;
import tr.com.arf.toys.service.application.ManagedBeanLocator;
import tr.com.arf.toys.utility.application.ApplicationConstant;
import tr.com.arf.toys.view.controller._common.SessionUser;
 
 
public class KomisyonDonemFilter extends BaseFilter { 
	
private static final long serialVersionUID = 1L; 
	
	private Komisyon komisyonRef; 
	private Donem donemRef;
	private String aciklama;
	private Birim birimRef;
	
	@Override 
	public void init() { 
		this.komisyonRef=null;
		this.donemRef=null;
		this.aciklama=null;
		this.birimRef=null;
	} 
	 
	@Override 
	public List<ArrayList<QueryObject>> createQueryCriterias(){ 
		ArrayList<ArrayList<QueryObject>> criteriaList = new ArrayList<ArrayList<QueryObject>>(); 
		ArrayList<QueryObject> kriterler = new ArrayList<QueryObject>();	 
		ArrayList<QueryObject> opsiyonelKriterler = new ArrayList<QueryObject>(); 
		try { 	 
			if(validateQueryObject(getKomisyonRef(), null, false)){ 
				kriterler.add(new QueryObject("komisyonRef", ApplicationConstant._equals, getKomisyonRef())); 
			} 
			if(validateQueryObject(getDonemRef(), null, false)){ 
				kriterler.add(new QueryObject("donemRef", ApplicationConstant._equals, getDonemRef())); 
			} 
			if(validateQueryObject(getAciklama(), null, false)){ 
				kriterler.add(new QueryObject("aciklama", ApplicationConstant._equals, getAciklama())); 
			} 
			if(validateQueryObject(getBirimRef(), null, false)){ 
				kriterler.add(new QueryObject("birimRef", ApplicationConstant._equals, getBirimRef())); 
			} 
			// Kullanicinin birimine gore yetkilendirme yapiliyor.
			SessionUser sessionUser = ManagedBeanLocator.locateSessionUser();
			if(sessionUser.getPersonelRef() != null && sessionUser.getPersonelRef().getBirimRef() != null){
				if(sessionUser.getBirimYetkiTuru() != BirimYetkiTuru._TUMBIRIMLER){
					if(sessionUser.getBirimYetkiTuru() == BirimYetkiTuru._KENDIBIRIMIVEALTBIRIMLERI){ 
						kriterler.add(new QueryObject("birimRef.erisimKodu", ApplicationConstant._beginsWith, sessionUser.getPersonelRef().getBirimRef().getErisimKodu())); 
					} else if(sessionUser.getBirimYetkiTuru() == BirimYetkiTuru._SADECEKENDIBIRIMLERI){ 
						kriterler.add(new QueryObject("birimRef", ApplicationConstant._equals, sessionUser.getPersonelRef().getBirimRef())); 
					}
				}
			}
		} catch(Exception e){ 
			System.out.println("Error @createQueryCriterias method, Class : KomisyonDonemFilter :" + e.getMessage()); 
		}  
		criteriaList.add(kriterler); 
		criteriaList.add(opsiyonelKriterler); 
		return criteriaList; 
	}


	public Komisyon getKomisyonRef() {
		return komisyonRef;
	}

	public void setKomisyonRef(Komisyon komisyonRef) {
		this.komisyonRef = komisyonRef;
	}

	public Donem getDonemRef() {
		return donemRef;
	}

	public void setDonemRef(Donem donemRef) {
		this.donemRef = donemRef;
	}

	public String getAciklama() {
		return aciklama;
	}

	public void setAciklama(String aciklama) {
		this.aciklama = aciklama;
	}

	public Birim getBirimRef() {
		return birimRef;
	}

	public void setBirimRef(Birim birimRef) {
		this.birimRef = birimRef;
	}

	 
} 
