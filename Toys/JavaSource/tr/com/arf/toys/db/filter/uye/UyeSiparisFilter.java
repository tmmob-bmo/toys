package tr.com.arf.toys.db.filter.uye;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.framework.db.model.query.QueryObject;
import tr.com.arf.toys.db.enumerated.uye.SiparisDurum;
import tr.com.arf.toys.db.model.uye.Uye;
import tr.com.arf.toys.utility.application.ApplicationConstant;


public class UyeSiparisFilter extends BaseFilter {

private static final long serialVersionUID = 1L;

	private Long rID;
	private Uye uyeRef;
	private String siparisno;
	private String tutar;
	private SiparisDurum durum;
	private String garantisecuritycode;
	private String mdstatus;
	private java.util.Date tarih;
	private java.util.Date tarihMax;

	@Override
	public void init() {
		this.rID = null;
		this.uyeRef = null;
		this.siparisno = null;
		this.tutar = null;
		this.durum = SiparisDurum._NULL;
		this.garantisecuritycode = null;
		this.mdstatus = null;
		this.tarih = null;
		this.tarihMax = null;
	}

	@Override
	public List<ArrayList<QueryObject>> createQueryCriterias(){
		ArrayList<ArrayList<QueryObject>> criteriaList = new ArrayList<ArrayList<QueryObject>>();
		ArrayList<QueryObject> kriterler = new ArrayList<QueryObject>();
		ArrayList<QueryObject> opsiyonelKriterler = new ArrayList<QueryObject>();
		try {
			if(validateQueryObject(getRID(), null, false)){
				kriterler.add(new QueryObject("rID", ApplicationConstant._equals, getRID()));
			}
			if(validateQueryObject(getUyeRef(), null, false)){
				kriterler.add(new QueryObject("uyeRef", ApplicationConstant._equals, getUyeRef()));
			}
			if(validateQueryObject(getSiparisno(), null, false)){
				kriterler.add(new QueryObject("siparisno", ApplicationConstant._beginsWith, getSiparisno(), false));
			}
			if(validateQueryObject(getTutar(), null, false)){
				kriterler.add(new QueryObject("tutar", ApplicationConstant._beginsWith, getTutar(), false));
			}
			if(validateQueryObject(getDurum(), null, true)){
				kriterler.add(new QueryObject("durum", ApplicationConstant._equals, getDurum()));
			}
			if(validateQueryObject(getGarantisecuritycode(), null, false)){
				kriterler.add(new QueryObject("garantisecuritycode", ApplicationConstant._beginsWith, getGarantisecuritycode(), false));
			}
			if(validateQueryObject(getMdstatus(), null, false)){
				kriterler.add(new QueryObject("mdstatus", ApplicationConstant._beginsWith, getMdstatus(), false));
			}
			if(validateQueryObject(getTarih(), null, false)  && validateQueryObject(getTarihMax(), null, false)){
				kriterler.add(new QueryObject("tarih", ApplicationConstant._equalsAndLargerThan, getTarih()));
				kriterler.add(new QueryObject("tarih", ApplicationConstant._equalsAndSmallerThan, getTarihMax()));
			} else if(validateQueryObject(getTarih(), null, false)){
				kriterler.add(new QueryObject("tarih", ApplicationConstant._equalsAndLargerThan, getTarih()));
			}
		} catch(Exception e){
			System.out.println("Error @createQueryCriterias method, Class : UyesiparisFilter :" + e.getMessage());
		}
		criteriaList.add(kriterler);
		criteriaList.add(opsiyonelKriterler);
		return criteriaList;
	}

	public Long getRID() {
		return this.rID;
	}

	public void setRID(Long rID) {
		this.rID = rID;
	}

	public Uye getUyeRef() {
		return this.uyeRef;
	}

	public void setUyeRef(Uye uyeRef) {
		this.uyeRef = uyeRef;
	}

	public String getSiparisno() {
		return this.siparisno;
	}

	public void setSiparisno(String siparisno) {
		this.siparisno = siparisno;
	}

	public String getTutar() {
		return this.tutar;
	}

	public void setTutar(String tutar) {
		this.tutar = tutar;
	}

	public SiparisDurum getDurum() {
		return this.durum;
	}

	public void setDurum(SiparisDurum durum) {
		this.durum = durum;
	}

	public String getGarantisecuritycode() {
		return this.garantisecuritycode;
	}

	public void setGarantisecuritycode(String garantisecuritycode) {
		this.garantisecuritycode = garantisecuritycode;
	}

	public String getMdstatus() {
		return this.mdstatus;
	}

	public void setMdstatus(String mdstatus) {
		this.mdstatus = mdstatus;
	}

	public Date getTarih() {
		return this.tarih;
	}

	public void setTarih(Date tarih) {
		this.tarih = tarih;
	}

	public Date getTarihMax() {
		return this.tarihMax;
	}

	public void setTarihMax(Date tarihMax) {
		this.tarihMax = tarihMax;
	}


}
