package tr.com.arf.toys.db.filter.system;

import java.util.ArrayList;
import java.util.List;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.framework.db.model.query.QueryObject;
import tr.com.arf.toys.utility.application.ApplicationConstant;

public class InsanFilter extends BaseFilter{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String ad;
	private String soyad;
	private char cinsiyet;
	private int tckn;
	
	public void init() { 
		this.ad = null; 	
		this.soyad = null; 	
		this.cinsiyet = 0;
		this.tckn = 0;
	} 
	
	
	public List<ArrayList<QueryObject>> createQueryCriterias(){ 
		ArrayList<ArrayList<QueryObject>> criteriaList = new ArrayList<ArrayList<QueryObject>>(); 
		ArrayList<QueryObject> kriterler = new ArrayList<QueryObject>();	 
		ArrayList<QueryObject> opsiyonelKriterler = new ArrayList<QueryObject>(); 
		try { 	 
			if(validateQueryObject(getAd(), null, false)){ 
				kriterler.add(new QueryObject("ad", ApplicationConstant._beginsWith, getAd())); 
			} 
			if(validateQueryObject(getSoyad(), null, false)){ 
				kriterler.add(new QueryObject("soyad", ApplicationConstant._beginsWith, getSoyad())); 
			} 
			if(validateQueryObject(getKimlikno(), null, false)){ 
				kriterler.add(new QueryObject("tckn", ApplicationConstant._beginsWith, getKimlikno())); 
			} 
			if(validateQueryObject(getCinsiyet(), null, false)){ 
				kriterler.add(new QueryObject("tckn", ApplicationConstant._beginsWith, getCinsiyet())); 
			} 
		} catch(Exception e){ 
			System.out.println("Error @createQueryCriterias method, Class : InsanFilter :" + e.getMessage()); 
		}  
		criteriaList.add(kriterler); 
		criteriaList.add(opsiyonelKriterler); 
		return criteriaList; 
	} 
	
	
	public String getAd(){
		return this.ad;	
	}

	
	public void setAd(String ad){
		this.ad=ad;
	}
	
	public String getSoyad(){
		return this.soyad;	
	}
	
	public void setSoyad(String soyad){
		this.soyad=soyad;
	}
	
	public int getKimlikno(){
		return this.tckn;	
	}

	
	public void setKimlikno(int tckn){
		this.tckn=tckn;
	}
	
	public int getCinsiyet(){
		return this.cinsiyet;	
	}

	
	public void setCinsiyet(char cinsiyet){
		this.cinsiyet=cinsiyet;
	}

}
