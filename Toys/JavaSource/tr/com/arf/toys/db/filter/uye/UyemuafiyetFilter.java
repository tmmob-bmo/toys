package tr.com.arf.toys.db.filter.uye; 
 
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.framework.db.model.query.QueryObject;
import tr.com.arf.toys.db.enumerated.uye.UyeMuafiyetTip;
import tr.com.arf.toys.db.model.evrak.Evrak;
import tr.com.arf.toys.db.model.uye.Uye;
import tr.com.arf.toys.utility.application.ApplicationConstant;
 
 
public class UyemuafiyetFilter extends BaseFilter { 
	
private static final long serialVersionUID = 1L; 
	
	private Long rID; 	
	private Uye uyeRef; 
	private UyeMuafiyetTip muafiyettip; 
	private Evrak evrakRef; 
	private String aciklama; 
	private java.util.Date baslangictarih; 
	private java.util.Date baslangictarihMax; 
	private java.util.Date bitistarih; 
	private java.util.Date bitistarihMax; 
	
	@Override 
	public void init() { 
		this.uyeRef = null; 	
		this.rID = null; 	
		this.muafiyettip = UyeMuafiyetTip._NULL;	
		this.evrakRef = null; 	
		this.aciklama = null; 
		this.baslangictarih = null; 	
		this.baslangictarihMax = null; 	
		this.bitistarih = null; 	
		this.bitistarihMax = null; 	
	} 
	 
	@Override 
	public List<ArrayList<QueryObject>> createQueryCriterias(){ 
		ArrayList<ArrayList<QueryObject>> criteriaList = new ArrayList<ArrayList<QueryObject>>(); 
		ArrayList<QueryObject> kriterler = new ArrayList<QueryObject>();	 
		ArrayList<QueryObject> opsiyonelKriterler = new ArrayList<QueryObject>(); 
		try { 	 
			if(validateQueryObject(getUyeRef(), null, false)){ 
				kriterler.add(new QueryObject("uyeRef", ApplicationConstant._equals, getUyeRef())); 
			} 
			if(validateQueryObject(getrID(), 0, false)){  
				kriterler.add(new QueryObject("rID", ApplicationConstant._equals, getrID())); 
			} 
			if(validateQueryObject(getMuafiyettip(), null, true)){ 
				kriterler.add(new QueryObject("muafiyettip", ApplicationConstant._equals, getMuafiyettip())); 
			} 
			if(validateQueryObject(getEvrakRef(), null, false)){ 
				kriterler.add(new QueryObject("evrakRef", ApplicationConstant._equals, getEvrakRef())); 
			} 
			if(validateQueryObject(getAciklama(), null, false)){ 
				kriterler.add(new QueryObject("aciklama", ApplicationConstant._beginsWith, getAciklama())); 
			} 
			if(validateQueryObject(getBaslangictarih(), null, false)  && validateQueryObject(getBaslangictarihMax(), null, false)){
				kriterler.add(new QueryObject("baslangictarih", ApplicationConstant._equalsAndLargerThan, getBaslangictarih())); 
				kriterler.add(new QueryObject("baslangictarih", ApplicationConstant._equalsAndSmallerThan, getBaslangictarihMax())); 
			} else if(validateQueryObject(getBaslangictarih(), null, false)){ 
				kriterler.add(new QueryObject("baslangictarih", ApplicationConstant._equalsAndLargerThan, getBaslangictarih()));
			}
			if(validateQueryObject(getBitistarih(), null, false)  && validateQueryObject(getBitistarihMax(), null, false)){
				kriterler.add(new QueryObject("bitistarih", ApplicationConstant._equalsAndLargerThan, getBitistarih())); 
				kriterler.add(new QueryObject("bitistarih", ApplicationConstant._equalsAndSmallerThan, getBitistarihMax())); 
			} else if(validateQueryObject(getBitistarih(), null, false)){ 
				kriterler.add(new QueryObject("bitistarih", ApplicationConstant._equalsAndLargerThan, getBitistarih()));
			}
		} catch(Exception e){ 
			System.out.println("Error @createQueryCriterias method, Class : UyemuafiyetFilter :" + e.getMessage()); 
		}  
		criteriaList.add(kriterler); 
		criteriaList.add(opsiyonelKriterler); 
		return criteriaList; 
	} 
	 
	public Uye getUyeRef() { 
		return this.uyeRef; 
	} 
	 
	public void setUyeRef(Uye uyeRef) { 
		this.uyeRef = uyeRef; 
	}  
	 
	public Long getrID() {
		return rID;
	}

	public void setrID(Long rID) {
		this.rID = rID;
	} 
  
	public UyeMuafiyetTip getMuafiyettip() { 
		return this.muafiyettip; 
	} 
	 
	public void setMuafiyettip(UyeMuafiyetTip muafiyettip) { 
		this.muafiyettip = muafiyettip; 
	} 
  
	public Evrak getEvrakRef() { 
		return this.evrakRef; 
	} 
	 
	public void setEvrakRef(Evrak evrakRef) { 
		this.evrakRef = evrakRef; 
	} 
  
	public String getAciklama() { 
		return this.aciklama; 
	} 
	 
	public void setAciklama(String aciklama) { 
		this.aciklama = aciklama; 
	} 
  
	public Date getBaslangictarih() { 
		return this.baslangictarih; 
	} 
	 
	public void setBaslangictarih(Date baslangictarih) { 
		this.baslangictarih = baslangictarih; 
	} 
  
	public Date getBaslangictarihMax() { 
		return this.baslangictarihMax; 
	} 
	 
	public void setBaslangictarihMax(Date baslangictarihMax) { 
		this.baslangictarihMax = baslangictarihMax; 
	} 
  
	public Date getBitistarih() { 
		return this.bitistarih; 
	} 
	 
	public void setBitistarih(Date bitistarih) { 
		this.bitistarih = bitistarih; 
	} 
  
	public Date getBitistarihMax() { 
		return this.bitistarihMax; 
	} 
	 
	public void setBitistarihMax(Date bitistarihMax) { 
		this.bitistarihMax = bitistarihMax; 
	} 
	
} 
