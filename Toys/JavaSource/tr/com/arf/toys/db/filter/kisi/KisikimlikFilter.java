package tr.com.arf.toys.db.filter.kisi; 
 
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.framework.db.model.query.QueryObject;
import tr.com.arf.toys.db.enumerated.kisi.Cinsiyet;
import tr.com.arf.toys.db.enumerated.kisi.KisiDurum;
import tr.com.arf.toys.db.model.kisi.Kisi;
import tr.com.arf.toys.db.model.system.Ilce;
import tr.com.arf.toys.db.model.system.Sehir;
import tr.com.arf.toys.utility.application.ApplicationConstant;
 
 
public class KisikimlikFilter extends BaseFilter { 
	
private static final long serialVersionUID = 1L; 
	
	private Kisi kisiRef; 
	private Long kimlikno; 
	private String babaad; 
	private String anaad; 
	private Ilce dogumsehirRef; 
	private String dogumyer; 
	private java.util.Date dogumtarih; 
	private java.util.Date dogumtarihMax; 
	private Cinsiyet cinsiyet;   
	private Sehir sehirRef; 
	private Ilce ilceRef;  
	private String ciltno; 
	private String aileno; 
	private String sirano;   
	private java.util.Date olumtarih; 
	private java.util.Date olumtarihMax; 
	private KisiDurum kisidurum;
	private String mahalle ;
	private java.util.Date kpsdogrulamatarih; 
	private java.util.Date kpsdogrulamatarihMax; 
	
	@Override 
	public void init() { 
		this.kisiRef = null; 	
		this.kimlikno = null; 	
		this.babaad = null; 	
		this.anaad = null; 	 	
		this.dogumsehirRef = null; 	
		this.dogumyer = null; 	
		this.dogumtarih = null; 	
		this.dogumtarihMax = null; 	
		this.cinsiyet = Cinsiyet._NULL;	 
		this.sehirRef = null; 	
		this.sehirRef = null; 	 
		this.ciltno = null; 	
		this.aileno = null; 	
		this.sirano = null; 	 
		this.olumtarih=null;
		this.olumtarihMax=null;
		this.kisidurum=KisiDurum._NULL;
		this.mahalle=null;
		this.kpsdogrulamatarih=null;
		this.kpsdogrulamatarihMax=null;
	} 
	 
	@Override 
	public List<ArrayList<QueryObject>> createQueryCriterias(){ 
		ArrayList<ArrayList<QueryObject>> criteriaList = new ArrayList<ArrayList<QueryObject>>(); 
		ArrayList<QueryObject> kriterler = new ArrayList<QueryObject>();	 
		ArrayList<QueryObject> opsiyonelKriterler = new ArrayList<QueryObject>(); 
		try { 	 
			if(validateQueryObject(getKisiRef(), null, false)){ 
				kriterler.add(new QueryObject("kisiRef", ApplicationConstant._equals, getKisiRef())); 
			} 
			if(validateQueryObject(getKimlikno(), null, false)){ 
				kriterler.add(new QueryObject("kimlikno", ApplicationConstant._beginsWith, getKimlikno())); 
			} 
			if(validateQueryObject(getBabaad(), null, false)){ 
				kriterler.add(new QueryObject("babaad", ApplicationConstant._beginsWith, getBabaad())); 
			} 
			if(validateQueryObject(getAnaad(), null, false)){ 
				kriterler.add(new QueryObject("anaad", ApplicationConstant._beginsWith, getAnaad())); 
			} 
			if(validateQueryObject(getDogumsehirRef(), null, false)){ 
				kriterler.add(new QueryObject("dogumsehirRef", ApplicationConstant._equals, getDogumsehirRef())); 
			} 
			if(validateQueryObject(getDogumyer(), null, false)){ 
				kriterler.add(new QueryObject("dogumyer", ApplicationConstant._beginsWith, getDogumyer())); 
			} 
			if(validateQueryObject(getDogumtarih(), null, false)  && validateQueryObject(getDogumtarihMax(), null, false)){
				kriterler.add(new QueryObject("dogumtarih", ApplicationConstant._equalsAndLargerThan, getDogumtarih())); 
				kriterler.add(new QueryObject("dogumtarih", ApplicationConstant._equalsAndSmallerThan, getDogumtarihMax())); 
			} else if(validateQueryObject(getDogumtarih(), null, false)){ 
				kriterler.add(new QueryObject("dogumtarih", ApplicationConstant._equalsAndLargerThan, getDogumtarih()));
			}
			if(validateQueryObject(getCinsiyet(), null, true)){ 
				kriterler.add(new QueryObject("cinsiyet", ApplicationConstant._equals, getCinsiyet())); 
			} 			
			if(validateQueryObject(getSehirRef(), null, false)){ 
				kriterler.add(new QueryObject("sehirRef", ApplicationConstant._equals, getSehirRef())); 
			} 
			if(validateQueryObject(getIlceRef(), null, false)){ 
				kriterler.add(new QueryObject("sehirRef", ApplicationConstant._equals, getIlceRef())); 
			} 
			if(validateQueryObject(getCiltno(), null, false)){ 
				kriterler.add(new QueryObject("ciltno", ApplicationConstant._beginsWith, getCiltno())); 
			} 
			if(validateQueryObject(getAileno(), null, false)){ 
				kriterler.add(new QueryObject("aileno", ApplicationConstant._beginsWith, getAileno())); 
			} 
			if(validateQueryObject(getSirano(), null, false)){ 
				kriterler.add(new QueryObject("sirano", ApplicationConstant._beginsWith, getSirano())); 
			} 			
			if(validateQueryObject(getOlumtarih(), null, false)  && validateQueryObject(getOlumtarihMax(), null, false)){
				kriterler.add(new QueryObject("olumtarih", ApplicationConstant._equalsAndLargerThan, getOlumtarih())); 
				kriterler.add(new QueryObject("olumtarih", ApplicationConstant._equalsAndSmallerThan, getOlumtarihMax())); 
			} else if(validateQueryObject(getOlumtarih(), null, false)){ 
				kriterler.add(new QueryObject("olumtarih", ApplicationConstant._equalsAndLargerThan, getOlumtarih()));
			}
			if(validateQueryObject(getDogumtarih(), null, false)  && validateQueryObject(getDogumtarihMax(), null, false)){
				kriterler.add(new QueryObject("dogumtarih", ApplicationConstant._equalsAndLargerThan, getDogumtarih())); 
				kriterler.add(new QueryObject("dogumtarih", ApplicationConstant._equalsAndSmallerThan, getDogumtarihMax())); 
			} else if(validateQueryObject(getDogumtarih(), null, false)){ 
				kriterler.add(new QueryObject("dogumtarih", ApplicationConstant._equalsAndLargerThan, getDogumtarih()));
			}
			if(validateQueryObject(getKisidurum(), null, true)){ 
				kriterler.add(new QueryObject("kisidurum", ApplicationConstant._equals, getKisidurum())); 
			}
			if(validateQueryObject(getMahalle(), null, true)){ 
				kriterler.add(new QueryObject("mahalle", ApplicationConstant._equals, getMahalle())); 
			}
			if(validateQueryObject(getKpsdogrulamatarih(), null, false)  && validateQueryObject(getKpsdogrulamatarihMax(), null, false)){
				kriterler.add(new QueryObject("kpsdogrulamatarih", ApplicationConstant._equalsAndLargerThan, getKpsdogrulamatarih())); 
				kriterler.add(new QueryObject("kpsdogrulamatarih", ApplicationConstant._equalsAndSmallerThan, getKpsdogrulamatarihMax())); 
			} else if(validateQueryObject(getKpsdogrulamatarih(), null, false)){ 
				kriterler.add(new QueryObject("kpsdogrulamatarih", ApplicationConstant._equalsAndLargerThan, getKpsdogrulamatarih()));
			}
		} catch(Exception e){ 
			System.out.println("Error @createQueryCriterias method, Class : KisikimlikFilter :" + e.getMessage()); 
		}  
		criteriaList.add(kriterler); 
		criteriaList.add(opsiyonelKriterler); 
		return criteriaList; 
	} 
	 
	public Kisi getKisiRef() { 
		return this.kisiRef; 
	} 
	 
	public void setKisiRef(Kisi kisiRef) { 
		this.kisiRef = kisiRef; 
	} 
  
	public Long getKimlikno() {
		return kimlikno;
	}

	public void setKimlikno(Long kimlikno) {
		this.kimlikno = kimlikno;
	}

	public String getBabaad() { 
		return this.babaad; 
	} 
	 
	public void setBabaad(String babaad) { 
		this.babaad = babaad; 
	} 
  
	public String getAnaad() { 
		return this.anaad; 
	} 
	 
	public void setAnaad(String anaad) { 
		this.anaad = anaad; 
	} 
	
	public Ilce getDogumsehirRef() { 
		return this.dogumsehirRef; 
	} 
	 
	public void setDogumsehirRef(Ilce dogumsehirRef) { 
		this.dogumsehirRef = dogumsehirRef; 
	} 
  
	public String getDogumyer() { 
		return this.dogumyer; 
	} 
	 
	public void setDogumyer(String dogumyer) { 
		this.dogumyer = dogumyer; 
	} 
  
	public Date getDogumtarih() { 
		return this.dogumtarih; 
	} 
	 
	public void setDogumtarih(Date dogumtarih) { 
		this.dogumtarih = dogumtarih; 
	} 
  
	public Date getDogumtarihMax() { 
		return this.dogumtarihMax; 
	} 
	 
	public void setDogumtarihMax(Date dogumtarihMax) { 
		this.dogumtarihMax = dogumtarihMax; 
	} 
  
	public Cinsiyet getCinsiyet() { 
		return this.cinsiyet; 
	} 
	 
	public void setCinsiyet(Cinsiyet cinsiyet) { 
		this.cinsiyet = cinsiyet; 
	} 
  
	public Sehir getSehirRef() { 
		return this.sehirRef; 
	} 
	 
	public void setSehirRef(Sehir sehirRef) { 
		this.sehirRef = sehirRef; 
	} 
  
	public Ilce getIlceRef() {
		return ilceRef;
	}

	public void setIlceRef(Ilce ilceRef) {
		this.ilceRef = ilceRef;
	}

	public String getCiltno() { 
		return this.ciltno; 
	} 
	 
	public void setCiltno(String ciltno) { 
		this.ciltno = ciltno; 
	} 
  
	public String getAileno() { 
		return this.aileno; 
	} 
	 
	public void setAileno(String aileno) { 
		this.aileno = aileno; 
	} 
  
	public String getSirano() { 
		return this.sirano; 
	} 
	 
	public void setSirano(String sirano) { 
		this.sirano = sirano; 
	} 
  
	public java.util.Date getOlumtarih() {
		return olumtarih;
	}

	public void setOlumtarih(java.util.Date olumtarih) {
		this.olumtarih = olumtarih;
	}

	public java.util.Date getOlumtarihMax() {
		return olumtarihMax;
	}

	public void setOlumtarihMax(java.util.Date olumtarihMax) {
		this.olumtarihMax = olumtarihMax;
	}

	public KisiDurum getKisidurum() {
		return kisidurum;
	}

	public void setKisidurum(KisiDurum kisidurum) {
		this.kisidurum = kisidurum;
	}

	public String getMahalle() {
		return mahalle;
	}

	public void setMahalle(String mahalle) {
		this.mahalle = mahalle;
	}

	public java.util.Date getKpsdogrulamatarih() {
		return kpsdogrulamatarih;
	}

	public void setKpsdogrulamatarih(java.util.Date kpsdogrulamatarih) {
		this.kpsdogrulamatarih = kpsdogrulamatarih;
	}

	public java.util.Date getKpsdogrulamatarihMax() {
		return kpsdogrulamatarihMax;
	}

	public void setKpsdogrulamatarihMax(java.util.Date kpsdogrulamatarihMax) {
		this.kpsdogrulamatarihMax = kpsdogrulamatarihMax;
	} 
	
	
	
	
} 
