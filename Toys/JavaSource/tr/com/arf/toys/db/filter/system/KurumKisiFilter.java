package tr.com.arf.toys.db.filter.system; 
 
import java.util.ArrayList;
import java.util.List;

import tr.com.arf.framework.db.enumerated._common.EvetHayir;
import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.framework.db.model.query.QueryObject;
import tr.com.arf.toys.db.enumerated.uye.UyekurumDurum;
import tr.com.arf.toys.db.model.kisi.Kisi;
import tr.com.arf.toys.db.model.kisi.Kurum;
import tr.com.arf.toys.utility.application.ApplicationConstant;
 
 
public class KurumKisiFilter extends BaseFilter { 
	
private static final long serialVersionUID = 1L; 
	
	private Kisi kisiRef;
	private Kurum kurumRef;
	private EvetHayir gecerli;
	private String gorev; 
	private java.util.Date baslangictarih; 
	private java.util.Date baslangictarihMax; 
	private java.util.Date bitistarih; 
	private java.util.Date bitistarihMax; 
	private UyekurumDurum uyekurumDurum; 
	private EvetHayir varsayilan; 
	
	private String ad;
	private String soyad;
	
	
	@Override 
	public void init() { 
		this.kisiRef=null;
		this.kurumRef=null;
		this.gecerli = EvetHayir._NULL;	
		this.soyad = null;
		this.ad=null;
		this.gorev = null; 	
		this.baslangictarih = null; 	
		this.baslangictarihMax = null; 	
		this.bitistarih = null; 	
		this.bitistarihMax = null; 	
		this.uyekurumDurum = UyekurumDurum._NULL;	
		this.varsayilan=EvetHayir._NULL;
	} 
	 
	@Override 
	public List<ArrayList<QueryObject>> createQueryCriterias(){ 
		ArrayList<ArrayList<QueryObject>> criteriaList = new ArrayList<ArrayList<QueryObject>>(); 
		ArrayList<QueryObject> kriterler = new ArrayList<QueryObject>();	 
		ArrayList<QueryObject> opsiyonelKriterler = new ArrayList<QueryObject>(); 
		try { 	 
			if(validateQueryObject(getKurumRef(), null, false)){ 
				kriterler.add(new QueryObject("kurumRef", ApplicationConstant._equals, getKurumRef())); 
			}
			if(validateQueryObject(getKisiRef(), null, false)){ 
				kriterler.add(new QueryObject("kisiRef", ApplicationConstant._equals, getKisiRef())); 
			}
			if(validateQueryObject(getAd(), null, false)){ 
				kriterler.add(new QueryObject("kisiRef.ad", ApplicationConstant._beginsWith, getAd())); 
			}
			if(validateQueryObject(getSoyad(), null, false)){ 
				kriterler.add(new QueryObject("kisiRef.soyad", ApplicationConstant._beginsWith, getSoyad())); 
			}		
			if(validateQueryObject(getGecerli(), null, true)){ 
				kriterler.add(new QueryObject("gecerli", ApplicationConstant._equals, getGecerli())); 
			} 
			if(validateQueryObject(getGorev(), null, false)){ 
				kriterler.add(new QueryObject("gorev", ApplicationConstant._beginsWith, getGorev(), false)); 
			} 
			if(validateQueryObject(getBaslangictarih(), null, false)  && validateQueryObject(getBaslangictarihMax(), null, false)){
				kriterler.add(new QueryObject("baslangictarih", ApplicationConstant._equalsAndLargerThan, getBaslangictarih())); 
				kriterler.add(new QueryObject("baslangictarih", ApplicationConstant._equalsAndSmallerThan, getBaslangictarihMax())); 
			} else if(validateQueryObject(getBaslangictarih(), null, false)){ 
				kriterler.add(new QueryObject("baslangictarih", ApplicationConstant._equalsAndLargerThan, getBaslangictarih()));
			}
			if(validateQueryObject(getBitistarih(), null, false)  && validateQueryObject(getBitistarihMax(), null, false)){
				kriterler.add(new QueryObject("bitistarih", ApplicationConstant._equalsAndLargerThan, getBitistarih())); 
				kriterler.add(new QueryObject("bitistarih", ApplicationConstant._equalsAndSmallerThan, getBitistarihMax())); 
			} else if(validateQueryObject(getBitistarih(), null, false)){ 
				kriterler.add(new QueryObject("bitistarih", ApplicationConstant._equalsAndLargerThan, getBitistarih()));
			}
			if(validateQueryObject(getUyekurumDurum(), null, true)){ 
				kriterler.add(new QueryObject("uyekurumDurum", ApplicationConstant._equals, getUyekurumDurum())); 
			} 
			if(validateQueryObject(getVarsayilan(), null, true)){ 
				kriterler.add(new QueryObject("varsayilan", ApplicationConstant._equals, getVarsayilan())); 
			}
			
		} catch(Exception e){ 
			System.out.println("Error @createQueryCriterias method, Class : KurumKisiFilter :" + e.getMessage()); 
		}  
		criteriaList.add(kriterler); 
		criteriaList.add(opsiyonelKriterler); 
		return criteriaList; 
	}


	public Kisi getKisiRef() {
		return kisiRef;
	}

	public void setKisiRef(Kisi kisiRef) {
		this.kisiRef = kisiRef;
	}

	public Kurum getKurumRef() {
		return kurumRef;
	}

	public void setKurumRef(Kurum kurumRef) {
		this.kurumRef = kurumRef;
	}

	public EvetHayir getGecerli() {
		return gecerli;
	}

	public void setGecerli(EvetHayir gecerli) {
		this.gecerli = gecerli;
	}

	public String getAd() {
		return ad;
	}

	public void setAd(String ad) {
		this.ad = ad;
	}

	public String getSoyad() {
		return soyad;
	}

	public void setSoyad(String soyad) {
		this.soyad = soyad;
	}

	public String getGorev() {
		return gorev;
	}

	public void setGorev(String gorev) {
		this.gorev = gorev;
	}

	public java.util.Date getBaslangictarih() {
		return baslangictarih;
	}

	public void setBaslangictarih(java.util.Date baslangictarih) {
		this.baslangictarih = baslangictarih;
	}

	public java.util.Date getBaslangictarihMax() {
		return baslangictarihMax;
	}

	public void setBaslangictarihMax(java.util.Date baslangictarihMax) {
		this.baslangictarihMax = baslangictarihMax;
	}

	public java.util.Date getBitistarih() {
		return bitistarih;
	}

	public void setBitistarih(java.util.Date bitistarih) {
		this.bitistarih = bitistarih;
	}

	public java.util.Date getBitistarihMax() {
		return bitistarihMax;
	}

	public void setBitistarihMax(java.util.Date bitistarihMax) {
		this.bitistarihMax = bitistarihMax;
	}

	public UyekurumDurum getUyekurumDurum() {
		return uyekurumDurum;
	}

	public void setUyekurumDurum(UyekurumDurum uyekurumDurum) {
		this.uyekurumDurum = uyekurumDurum;
	}

	public EvetHayir getVarsayilan() {
		return varsayilan;
	}

	public void setVarsayilan(EvetHayir varsayilan) {
		this.varsayilan = varsayilan;
	}
} 
