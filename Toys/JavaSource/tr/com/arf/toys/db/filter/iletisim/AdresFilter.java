package tr.com.arf.toys.db.filter.iletisim; 
 
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import tr.com.arf.framework.db.enumerated._common.EvetHayir;
import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.framework.db.model.query.QueryObject;
import tr.com.arf.toys.db.enumerated.iletisim.AdresTipi;
import tr.com.arf.toys.db.enumerated.iletisim.AdresTuru;
import tr.com.arf.toys.db.model.kisi.Kisi;
import tr.com.arf.toys.db.model.kisi.Kurum;
import tr.com.arf.toys.db.model.system.Ilce;
import tr.com.arf.toys.db.model.system.Sehir;
import tr.com.arf.toys.db.model.system.Ulke;
import tr.com.arf.toys.utility.application.ApplicationConstant;
 
 
public class AdresFilter extends BaseFilter { 
	
private static final long serialVersionUID = 1L; 
	
	private AdresTuru adresturu; 
	private Kisi kisiRef; 
	private EvetHayir varsayilan; 
	private Kurum kurumRef; 
	private AdresTipi adrestipi; 
	private java.util.Date kpsGuncellemeTarihi; 
	private java.util.Date kpsGuncellemeTarihiMax; 
	private String acikAdres; 
	private String adresNo; 
	private String binaBlokAdi; 
	private String binaKodu; 
	private String binaSiteAdi; 
	private String csbm; 
	private String csbmKodu; 
	private String disKapiNo; 
	private String icKapiNo; 
	private String mahalle; 
	private String mahalleKodu; 
	private Ulke ulkeRef;
	private Sehir sehirRef; 
	private Ilce ilceRef; 
	private java.util.Date beyanTarihi; 
	private java.util.Date beyanTarihiMax; 
	private String binaAda; 
	private String binaPafta; 
	private String binaParsel; 
	private java.util.Date tasinmaTarihi; 
	private java.util.Date tasinmaTarihiMax; 
	private java.util.Date tescilTarihi; 
	private java.util.Date tescilTarihiMax; 
	private String bucak; 
	private String bucakkod; 
	private String koy; 
	private String koykod; 
	private String yabanciadres; 
	private String yabancisehir; 
	private String yabanciulke; 
	
	
	@Override 
	public void init() { 
		this.adresturu = AdresTuru._NULL;	
		this.kisiRef = null; 	
		this.varsayilan = EvetHayir._NULL;	
		this.kurumRef = null; 	
		this.adrestipi = AdresTipi._NULL;	
		this.kpsGuncellemeTarihi = null; 	
		this.kpsGuncellemeTarihiMax = null; 	
		this.acikAdres = null; 	
		this.adresNo = null; 	
		this.binaBlokAdi = null; 	
		this.binaKodu = null; 	
		this.binaSiteAdi = null; 	
		this.csbm = null; 	
		this.csbmKodu = null; 	
		this.disKapiNo = null; 	
		this.icKapiNo = null; 	
		this.mahalle = null; 	
		this.mahalleKodu = null; 	
		this.sehirRef = null; 	
		this.ilceRef = null; 	
		this.beyanTarihi = null; 	
		this.beyanTarihiMax = null; 	
		this.binaAda = null; 	
		this.binaPafta = null; 	
		this.binaParsel = null; 	
		this.tasinmaTarihi = null; 	
		this.tasinmaTarihiMax = null; 	
		this.tescilTarihi = null; 	
		this.tescilTarihiMax = null; 
		this.bucak=null;
		this.bucakkod=null;
		this.koy=null;
		this.koykod=null;
		this.yabanciadres=null;
		this.yabancisehir=null;
		this.yabanciulke=null;
	} 
	 
	@Override 
	public List<ArrayList<QueryObject>> createQueryCriterias(){ 
		ArrayList<ArrayList<QueryObject>> criteriaList = new ArrayList<ArrayList<QueryObject>>(); 
		ArrayList<QueryObject> kriterler = new ArrayList<QueryObject>();	 
		ArrayList<QueryObject> opsiyonelKriterler = new ArrayList<QueryObject>(); 
		try { 	 
			if(validateQueryObject(getAdresturu(), null, true)){ 
				kriterler.add(new QueryObject("adresturu", ApplicationConstant._equals, getAdresturu())); 
			} 
			if(validateQueryObject(getKisiRef(), null, false)){ 
				kriterler.add(new QueryObject("kisiRef", ApplicationConstant._equals, getKisiRef())); 
			} 
			if(validateQueryObject(getVarsayilan(), null, true)){ 
				kriterler.add(new QueryObject("varsayilan", ApplicationConstant._equals, getVarsayilan())); 
			} 
			if(validateQueryObject(getKurumRef(), null, false)){ 
				kriterler.add(new QueryObject("kurumRef", ApplicationConstant._equals, getKurumRef())); 
			} 
			if(validateQueryObject(getAdrestipi(), null, true)){ 
				kriterler.add(new QueryObject("adrestipi", ApplicationConstant._equals, getAdrestipi())); 
			} 
			if(validateQueryObject(getKpsGuncellemeTarihi(), null, false)  && validateQueryObject(getKpsGuncellemeTarihiMax(), null, false)){
				kriterler.add(new QueryObject("kpsGuncellemeTarihi", ApplicationConstant._equalsAndLargerThan, getKpsGuncellemeTarihi())); 
				kriterler.add(new QueryObject("kpsGuncellemeTarihi", ApplicationConstant._equalsAndSmallerThan, getKpsGuncellemeTarihiMax())); 
			} else if(validateQueryObject(getKpsGuncellemeTarihi(), null, false)){ 
				kriterler.add(new QueryObject("kpsGuncellemeTarihi", ApplicationConstant._equalsAndLargerThan, getKpsGuncellemeTarihi()));
			}
			if(validateQueryObject(getAcikAdres(), null, false)){ 
				kriterler.add(new QueryObject("acikAdres", ApplicationConstant._beginsWith, getAcikAdres(), false)); 
			} 
			if(validateQueryObject(getAdresNo(), null, false)){ 
				kriterler.add(new QueryObject("adresNo", ApplicationConstant._beginsWith, getAdresNo(), false)); 
			} 
			if(validateQueryObject(getBinaBlokAdi(), null, false)){ 
				kriterler.add(new QueryObject("binaBlokAdi", ApplicationConstant._beginsWith, getBinaBlokAdi(), false)); 
			} 
			if(validateQueryObject(getBinaKodu(), null, false)){ 
				kriterler.add(new QueryObject("binaKodu", ApplicationConstant._beginsWith, getBinaKodu(), false)); 
			} 
			if(validateQueryObject(getBinasiteadi(), null, false)){ 
				kriterler.add(new QueryObject("binaSiteAdi", ApplicationConstant._beginsWith, getBinasiteadi(), false)); 
			} 
			if(validateQueryObject(getCsbm(), null, false)){ 
				kriterler.add(new QueryObject("csbm", ApplicationConstant._beginsWith, getCsbm(), false)); 
			} 
			if(validateQueryObject(getCsbmKodu(), null, false)){ 
				kriterler.add(new QueryObject("csbmKodu", ApplicationConstant._beginsWith, getCsbmKodu(), false)); 
			} 
			if(validateQueryObject(getDisKapiNo(), null, false)){ 
				kriterler.add(new QueryObject("disKapiNo", ApplicationConstant._beginsWith, getDisKapiNo(), false)); 
			} 
			if(validateQueryObject(getIcKapiNo(), null, false)){ 
				kriterler.add(new QueryObject("icKapiNo", ApplicationConstant._beginsWith, getIcKapiNo(), false)); 
			} 
			if(validateQueryObject(getMahalle(), null, false)){ 
				kriterler.add(new QueryObject("mahalle", ApplicationConstant._beginsWith, getMahalle(), false)); 
			} 
			if(validateQueryObject(getMahalleKodu(), null, false)){ 
				kriterler.add(new QueryObject("mahalleKodu", ApplicationConstant._beginsWith, getMahalleKodu(), false)); 
			} 
			if(validateQueryObject(getUlkeRef(), null, false)){ 
				kriterler.add(new QueryObject("ulkeRef", ApplicationConstant._equals, getUlkeRef())); 
			} 
			if(validateQueryObject(getSehirRef(), null, false)){ 
				kriterler.add(new QueryObject("sehirRef", ApplicationConstant._equals, getSehirRef())); 
			} 
			if(validateQueryObject(getIlceRef(), null, false)){ 
				kriterler.add(new QueryObject("ilceRef", ApplicationConstant._equals, getIlceRef())); 
			} 
			if(validateQueryObject(getBeyanTarihi(), null, false)  && validateQueryObject(getBeyanTarihiMax(), null, false)){
				kriterler.add(new QueryObject("beyanTarihi", ApplicationConstant._equalsAndLargerThan, getBeyanTarihi())); 
				kriterler.add(new QueryObject("beyanTarihi", ApplicationConstant._equalsAndSmallerThan, getBeyanTarihiMax())); 
			} else if(validateQueryObject(getBeyanTarihi(), null, false)){ 
				kriterler.add(new QueryObject("beyanTarihi", ApplicationConstant._equalsAndLargerThan, getBeyanTarihi()));
			}
			if(validateQueryObject(getBinaAda(), null, false)){ 
				kriterler.add(new QueryObject("binaAda", ApplicationConstant._beginsWith, getBinaAda(), false)); 
			} 
			if(validateQueryObject(getBinaPafta(), null, false)){ 
				kriterler.add(new QueryObject("binaPafta", ApplicationConstant._beginsWith, getBinaPafta(), false)); 
			} 
			if(validateQueryObject(getBinaParsel(), null, false)){ 
				kriterler.add(new QueryObject("binaParsel", ApplicationConstant._beginsWith, getBinaParsel(), false)); 
			} 
			if(validateQueryObject(getTasinmaTarihi(), null, false)  && validateQueryObject(getTasinmaTarihiMax(), null, false)){
				kriterler.add(new QueryObject("tasinmaTarihi", ApplicationConstant._equalsAndLargerThan, getTasinmaTarihi())); 
				kriterler.add(new QueryObject("tasinmaTarihi", ApplicationConstant._equalsAndSmallerThan, getTasinmaTarihiMax())); 
			} else if(validateQueryObject(getTasinmaTarihi(), null, false)){ 
				kriterler.add(new QueryObject("tasinmaTarihi", ApplicationConstant._equalsAndLargerThan, getTasinmaTarihi()));
			}
			if(validateQueryObject(getTescilTarihi(), null, false)  && validateQueryObject(getTescilTarihiMax(), null, false)){
				kriterler.add(new QueryObject("tescilTarihi", ApplicationConstant._equalsAndLargerThan, getTescilTarihi())); 
				kriterler.add(new QueryObject("tescilTarihi", ApplicationConstant._equalsAndSmallerThan, getTescilTarihiMax())); 
			} else if(validateQueryObject(getTescilTarihi(), null, false)){ 
				kriterler.add(new QueryObject("tescilTarihi", ApplicationConstant._equalsAndLargerThan, getTescilTarihi()));
			}
			if(validateQueryObject(getBucak(), null, false)){ 
				kriterler.add(new QueryObject("bucak", ApplicationConstant._beginsWith, getBucak(), false)); 
			}
			if(validateQueryObject(getBucakkod(), null, false)){ 
				kriterler.add(new QueryObject("bucakkod", ApplicationConstant._beginsWith, getBucakkod(), false)); 
			}
			if(validateQueryObject(getKoy(), null, false)){ 
				kriterler.add(new QueryObject("koy", ApplicationConstant._beginsWith, getKoy(), false)); 
			}
			if(validateQueryObject(getKoykod(), null, false)){ 
				kriterler.add(new QueryObject("koykod", ApplicationConstant._beginsWith, getKoykod(), false)); 
			}
			if(validateQueryObject(getYabanciadres(), null, false)){ 
				kriterler.add(new QueryObject("yabanciadres", ApplicationConstant._beginsWith, getYabanciadres(), false)); 
			}
			if(validateQueryObject(getYabancisehir(), null, false)){ 
				kriterler.add(new QueryObject("yabancisehir", ApplicationConstant._beginsWith, getYabancisehir(), false)); 
			}
			if(validateQueryObject(getYabanciulke(), null, false)){ 
				kriterler.add(new QueryObject("yabanciulke", ApplicationConstant._beginsWith, getYabanciulke(), false)); 
			}
		} catch(Exception e){ 
			System.out.println("Error @createQueryCriterias method, Class : AdresFilter :" + e.getMessage()); 
		}  
		criteriaList.add(kriterler); 
		criteriaList.add(opsiyonelKriterler); 
		return criteriaList; 
	} 
	 
	public AdresTuru getAdresturu() { 
		return this.adresturu; 
	} 
	 
	public void setAdresturu(AdresTuru adresturu) { 
		this.adresturu = adresturu; 
	} 
  
	public Kisi getKisiRef() { 
		return this.kisiRef; 
	} 
	 
	public void setKisiRef(Kisi kisiRef) { 
		this.kisiRef = kisiRef; 
	} 
  
	public EvetHayir getVarsayilan() { 
		return this.varsayilan; 
	} 
	 
	public void setVarsayilan(EvetHayir varsayilan) { 
		this.varsayilan = varsayilan; 
	} 
  
	public Kurum getKurumRef() { 
		return this.kurumRef; 
	} 
	 
	public void setKurumRef(Kurum kurumRef) { 
		this.kurumRef = kurumRef; 
	} 
  
	public AdresTipi getAdrestipi() { 
		return this.adrestipi; 
	} 
	 
	public void setAdrestipi(AdresTipi adrestipi) { 
		this.adrestipi = adrestipi; 
	} 
  
	public Date getKpsGuncellemeTarihi() { 
		return this.kpsGuncellemeTarihi; 
	} 
	 
	public void setKpsGuncellemeTarihi(Date kpsGuncellemeTarihi) { 
		this.kpsGuncellemeTarihi = kpsGuncellemeTarihi; 
	} 
  
	public Date getKpsGuncellemeTarihiMax() { 
		return this.kpsGuncellemeTarihiMax; 
	} 
	 
	public void setKpsGuncellemeTarihiMax(Date kpsGuncellemeTarihiMax) { 
		this.kpsGuncellemeTarihiMax = kpsGuncellemeTarihiMax; 
	} 
  
	public String getAcikAdres() { 
		return this.acikAdres; 
	} 
	 
	public void setAcikAdres(String acikAdres) { 
		this.acikAdres = acikAdres; 
	} 
  
	public String getAdresNo() { 
		return this.adresNo; 
	} 
	 
	public void setAdresNo(String adresNo) { 
		this.adresNo = adresNo; 
	} 
  
	public String getBinaBlokAdi() { 
		return this.binaBlokAdi; 
	} 
	 
	public void setBinaBlokAdi(String binaBlokAdi) { 
		this.binaBlokAdi = binaBlokAdi; 
	} 
  
	public String getBinaKodu() { 
		return this.binaKodu; 
	} 
	 
	public void setBinaKodu(String binaKodu) { 
		this.binaKodu = binaKodu; 
	} 
  
	public String getBinasiteadi() { 
		return this.binaSiteAdi; 
	} 
	 
	public void setBinasiteadi(String binaSiteAdi) { 
		this.binaSiteAdi = binaSiteAdi; 
	} 
  
	public String getCsbm() { 
		return this.csbm; 
	} 
	 
	public void setCsbm(String csbm) { 
		this.csbm = csbm; 
	} 
  
	public String getCsbmKodu() { 
		return this.csbmKodu; 
	} 
	 
	public void setCsbmKodu(String csbmKodu) { 
		this.csbmKodu = csbmKodu; 
	} 
  
	public String getDisKapiNo() { 
		return this.disKapiNo; 
	} 
	 
	public void setDisKapiNo(String disKapiNo) { 
		this.disKapiNo = disKapiNo; 
	} 
  
	public String getIcKapiNo() { 
		return this.icKapiNo; 
	} 
	 
	public void setIcKapiNo(String icKapiNo) { 
		this.icKapiNo = icKapiNo; 
	} 
  
	public String getMahalle() { 
		return this.mahalle; 
	} 
	 
	public void setMahalle(String mahalle) { 
		this.mahalle = mahalle; 
	} 
  
	public String getMahalleKodu() { 
		return this.mahalleKodu; 
	} 
	 
	public void setMahalleKodu(String mahalleKodu) { 
		this.mahalleKodu = mahalleKodu; 
	}   
	public Ulke getUlkeRef() {
		return ulkeRef;
	}

	public void setUlkeRef(Ulke ulkeRef) {
		this.ulkeRef = ulkeRef;
	}

	public Sehir getSehirRef() { 
		return this.sehirRef; 
	} 
	 
	public void setSehirRef(Sehir sehirRef) { 
		this.sehirRef = sehirRef; 
	} 
  
	public Ilce getIlceRef() { 
		return this.ilceRef; 
	} 
	 
	public void setIlceRef(Ilce ilceRef) { 
		this.ilceRef = ilceRef; 
	} 
  
	public Date getBeyanTarihi() { 
		return this.beyanTarihi; 
	} 
	 
	public void setBeyanTarihi(Date beyanTarihi) { 
		this.beyanTarihi = beyanTarihi; 
	} 
  
	public Date getBeyanTarihiMax() { 
		return this.beyanTarihiMax; 
	} 
	 
	public void setBeyanTarihiMax(Date beyanTarihiMax) { 
		this.beyanTarihiMax = beyanTarihiMax; 
	} 
  
	public String getBinaAda() { 
		return this.binaAda; 
	} 
	 
	public void setBinaAda(String binaAda) { 
		this.binaAda = binaAda; 
	} 
  
	public String getBinaPafta() { 
		return this.binaPafta; 
	} 
	 
	public void setBinaPafta(String binaPafta) { 
		this.binaPafta = binaPafta; 
	} 
  
	public String getBinaParsel() { 
		return this.binaParsel; 
	} 
	 
	public void setBinaParsel(String binaParsel) { 
		this.binaParsel = binaParsel; 
	} 
  
	public Date getTasinmaTarihi() { 
		return this.tasinmaTarihi; 
	} 
	 
	public void setTasinmaTarihi(Date tasinmaTarihi) { 
		this.tasinmaTarihi = tasinmaTarihi; 
	} 
  
	public Date getTasinmaTarihiMax() { 
		return this.tasinmaTarihiMax; 
	} 
	 
	public void setTasinmaTarihiMax(Date tasinmaTarihiMax) { 
		this.tasinmaTarihiMax = tasinmaTarihiMax; 
	} 
  
	public Date getTescilTarihi() { 
		return this.tescilTarihi; 
	} 
	 
	public void setTescilTarihi(Date tescilTarihi) { 
		this.tescilTarihi = tescilTarihi; 
	} 
  
	public Date getTescilTarihiMax() { 
		return this.tescilTarihiMax; 
	} 
	 
	public void setTescilTarihiMax(Date tescilTarihiMax) { 
		this.tescilTarihiMax = tescilTarihiMax; 
	}

	public String getBinaSiteAdi() {
		return binaSiteAdi;
	}

	public void setBinaSiteAdi(String binaSiteAdi) {
		this.binaSiteAdi = binaSiteAdi;
	}

	public String getBucak() {
		return bucak;
	}

	public void setBucak(String bucak) {
		this.bucak = bucak;
	}

	public String getBucakkod() {
		return bucakkod;
	}

	public void setBucakkod(String bucakkod) {
		this.bucakkod = bucakkod;
	}

	public String getKoy() {
		return koy;
	}

	public void setKoy(String koy) {
		this.koy = koy;
	}

	public String getKoykod() {
		return koykod;
	}

	public void setKoykod(String koykod) {
		this.koykod = koykod;
	}

	public String getYabanciadres() {
		return yabanciadres;
	}

	public void setYabanciadres(String yabanciadres) {
		this.yabanciadres = yabanciadres;
	}

	public String getYabancisehir() {
		return yabancisehir;
	}

	public void setYabancisehir(String yabancisehir) {
		this.yabancisehir = yabancisehir;
	}

	public String getYabanciulke() {
		return yabanciulke;
	}

	public void setYabanciulke(String yabanciulke) {
		this.yabanciulke = yabanciulke;
	} 
	
	
} 
