package tr.com.arf.toys.db.filter.uye; 
 
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.framework.db.model.query.QueryObject;
import tr.com.arf.toys.db.enumerated.uye.UyeBelgeDurum;
import tr.com.arf.toys.db.model.evrak.Evrak;
import tr.com.arf.toys.db.model.kisi.Kurum;
import tr.com.arf.toys.db.model.system.Belgetip;
import tr.com.arf.toys.db.model.uye.Uye;
import tr.com.arf.toys.utility.application.ApplicationConstant;
 
 
public class UyebelgeFilter extends BaseFilter { 
	
private static final long serialVersionUID = 1L; 
	
	private Uye uyeRef; 
	
	private Kurum kurumRef; 
	private Belgetip belgetipRef; 
	private java.util.Date tarih; 
	private java.util.Date tarihMax; 
	private java.util.Date basvurutarih; 
	private java.util.Date basvurutarihMax; 
	private java.util.Date hazirlanmatarih; 
	private java.util.Date hazirlanmatarihMax; 
	private Evrak evrakRef; 
	private java.util.Date teslimtarih; 
	private java.util.Date teslimtarihMax; 
	private UyeBelgeDurum uyebelgedurum; 
	private String aciklama; 
	
	@Override 
	public void init() { 
		this.uyeRef = null; 	
		this.kurumRef = null; 	
		this.belgetipRef = null; 	
		this.tarih = null; 	
		this.tarihMax = null; 	
		this.basvurutarih = null; 	
		this.basvurutarihMax = null; 	
		this.hazirlanmatarih = null; 	
		this.hazirlanmatarihMax = null; 	
		this.evrakRef = null; 	
		this.teslimtarih = null; 	
		this.teslimtarihMax = null; 	
		this.uyebelgedurum = UyeBelgeDurum._NULL;	
		this.aciklama = null; 	
	} 
	 
	@Override 
	public List<ArrayList<QueryObject>> createQueryCriterias(){ 
		ArrayList<ArrayList<QueryObject>> criteriaList = new ArrayList<ArrayList<QueryObject>>(); 
		ArrayList<QueryObject> kriterler = new ArrayList<QueryObject>();	 
		ArrayList<QueryObject> opsiyonelKriterler = new ArrayList<QueryObject>(); 
		try { 	 
			if(validateQueryObject(getUyeRef(), null, false)){ 
				kriterler.add(new QueryObject("uyeRef", ApplicationConstant._equals, getUyeRef())); 
			} 
			if(validateQueryObject(getKurumRef(), null, false)){ 
				kriterler.add(new QueryObject("kurumRef", ApplicationConstant._equals, getKurumRef())); 
			} 
			if(validateQueryObject(getBelgetipRef(), null, false)){ 
				kriterler.add(new QueryObject("belgetipRef", ApplicationConstant._equals, getBelgetipRef())); 
			} 
			if(validateQueryObject(getTarih(), null, false)  && validateQueryObject(getTarihMax(), null, false)){
				kriterler.add(new QueryObject("tarih", ApplicationConstant._equalsAndLargerThan, getTarih())); 
				kriterler.add(new QueryObject("tarih", ApplicationConstant._equalsAndSmallerThan, getTarihMax())); 
			} else if(validateQueryObject(getTarih(), null, false)){ 
				kriterler.add(new QueryObject("tarih", ApplicationConstant._equalsAndLargerThan, getTarih()));
			}
			if(validateQueryObject(getBasvurutarih(), null, false)  && validateQueryObject(getBasvurutarihMax(), null, false)){
				kriterler.add(new QueryObject("basvurutarih", ApplicationConstant._equalsAndLargerThan, getBasvurutarih())); 
				kriterler.add(new QueryObject("basvurutarih", ApplicationConstant._equalsAndSmallerThan, getBasvurutarihMax())); 
			} else if(validateQueryObject(getBasvurutarih(), null, false)){ 
				kriterler.add(new QueryObject("basvurutarih", ApplicationConstant._equalsAndLargerThan, getBasvurutarih()));
			}
			if(validateQueryObject(getHazirlanmatarih(), null, false)  && validateQueryObject(getHazirlanmatarihMax(), null, false)){
				kriterler.add(new QueryObject("hazirlanmatarih", ApplicationConstant._equalsAndLargerThan, getHazirlanmatarih())); 
				kriterler.add(new QueryObject("hazirlanmatarih", ApplicationConstant._equalsAndSmallerThan, getHazirlanmatarihMax())); 
			} else if(validateQueryObject(getHazirlanmatarih(), null, false)){ 
				kriterler.add(new QueryObject("hazirlanmatarih", ApplicationConstant._equalsAndLargerThan, getHazirlanmatarih()));
			}
			if(validateQueryObject(getEvrakRef(), null, false)){ 
				kriterler.add(new QueryObject("evrakRef", ApplicationConstant._equals, getEvrakRef())); 
			} 
			if(validateQueryObject(getTeslimtarih(), null, false)  && validateQueryObject(getTeslimtarihMax(), null, false)){
				kriterler.add(new QueryObject("teslimtarih", ApplicationConstant._equalsAndLargerThan, getTeslimtarih())); 
				kriterler.add(new QueryObject("teslimtarih", ApplicationConstant._equalsAndSmallerThan, getTeslimtarihMax())); 
			} else if(validateQueryObject(getTeslimtarih(), null, false)){ 
				kriterler.add(new QueryObject("teslimtarih", ApplicationConstant._equalsAndLargerThan, getTeslimtarih()));
			}
			if(validateQueryObject(getUyebelgedurum(), null, true)){ 
				kriterler.add(new QueryObject("uyebelgedurum", ApplicationConstant._equals, getUyebelgedurum())); 
			} 
			if(validateQueryObject(getAciklama(), null, false)){ 
				kriterler.add(new QueryObject("aciklama", ApplicationConstant._beginsWith, getAciklama())); 
			} 
		} catch(Exception e){ 
			System.out.println("Error @createQueryCriterias method, Class : UyebelgeFilter :" + e.getMessage()); 
		}  
		criteriaList.add(kriterler); 
		criteriaList.add(opsiyonelKriterler); 
		return criteriaList; 
	} 
	 
	public Uye getUyeRef() { 
		return this.uyeRef; 
	} 
	 
	public void setUyeRef(Uye uyeRef) { 
		this.uyeRef = uyeRef; 
	} 
  
	public Kurum getKurumRef() { 
		return this.kurumRef; 
	} 
	 
	public void setKurumRef(Kurum kurumRef) { 
		this.kurumRef = kurumRef; 
	} 
  
	public Belgetip getBelgetipRef() { 
		return this.belgetipRef; 
	} 
	 
	public void setBelgetipRef(Belgetip belgetipRef) { 
		this.belgetipRef = belgetipRef; 
	} 
  
	public Date getTarih() { 
		return this.tarih; 
	} 
	 
	public void setTarih(Date tarih) { 
		this.tarih = tarih; 
	} 
  
	public Date getTarihMax() { 
		return this.tarihMax; 
	} 
	 
	public void setTarihMax(Date tarihMax) { 
		this.tarihMax = tarihMax; 
	} 
  
	public Date getBasvurutarih() { 
		return this.basvurutarih; 
	} 
	 
	public void setBasvurutarih(Date basvurutarih) { 
		this.basvurutarih = basvurutarih; 
	} 
  
	public Date getBasvurutarihMax() { 
		return this.basvurutarihMax; 
	} 
	 
	public void setBasvurutarihMax(Date basvurutarihMax) { 
		this.basvurutarihMax = basvurutarihMax; 
	} 
  
	public Date getHazirlanmatarih() { 
		return this.hazirlanmatarih; 
	} 
	 
	public void setHazirlanmatarih(Date hazirlanmatarih) { 
		this.hazirlanmatarih = hazirlanmatarih; 
	} 
  
	public Date getHazirlanmatarihMax() { 
		return this.hazirlanmatarihMax; 
	} 
	 
	public void setHazirlanmatarihMax(Date hazirlanmatarihMax) { 
		this.hazirlanmatarihMax = hazirlanmatarihMax; 
	} 
  
	public Evrak getEvrakRef() { 
		return this.evrakRef; 
	} 
	 
	public void setEvrakRef(Evrak evrakRef) { 
		this.evrakRef = evrakRef; 
	} 
  
	public Date getTeslimtarih() { 
		return this.teslimtarih; 
	} 
	 
	public void setTeslimtarih(Date teslimtarih) { 
		this.teslimtarih = teslimtarih; 
	} 
  
	public Date getTeslimtarihMax() { 
		return this.teslimtarihMax; 
	} 
	 
	public void setTeslimtarihMax(Date teslimtarihMax) { 
		this.teslimtarihMax = teslimtarihMax; 
	} 
  
	public UyeBelgeDurum getUyebelgedurum() { 
		return this.uyebelgedurum; 
	} 
	 
	public void setUyebelgedurum(UyeBelgeDurum uyebelgedurum) { 
		this.uyebelgedurum = uyebelgedurum; 
	} 
  
	public String getAciklama() { 
		return this.aciklama; 
	} 
	 
	public void setAciklama(String aciklama) { 
		this.aciklama = aciklama; 
	} 
  
	
} 
