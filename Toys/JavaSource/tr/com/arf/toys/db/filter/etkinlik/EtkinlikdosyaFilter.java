package tr.com.arf.toys.db.filter.etkinlik; 
 
import java.util.ArrayList;
import java.util.List;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.framework.db.model.query.QueryObject;
import tr.com.arf.toys.db.enumerated.etkinlik.Dosyaturu;
import tr.com.arf.toys.db.model.etkinlik.Etkinlik;
import tr.com.arf.toys.utility.application.ApplicationConstant;
 
 
public class EtkinlikdosyaFilter extends BaseFilter { 
	
private static final long serialVersionUID = 1L; 
	
	private Etkinlik etkinlikRef; 
	
	private Long rID; 
	private Dosyaturu dosyaturu; 
	private String ad; 
	private String path; 
	
	@Override 
	public void init() { 
		this.rID = null; 	
		this.etkinlikRef = null; 	
		this.dosyaturu = Dosyaturu._NULL;	
		this.ad = null; 	
		this.path = null; 	
	} 
	 
	@Override 
	public List<ArrayList<QueryObject>> createQueryCriterias(){ 
		ArrayList<ArrayList<QueryObject>> criteriaList = new ArrayList<ArrayList<QueryObject>>(); 
		ArrayList<QueryObject> kriterler = new ArrayList<QueryObject>();	 
		ArrayList<QueryObject> opsiyonelKriterler = new ArrayList<QueryObject>(); 
		try { 	 
			if(validateQueryObject(getRID(), null, false)){ 
				kriterler.add(new QueryObject("rID", ApplicationConstant._equals, getRID())); 
			} 
			if(validateQueryObject(getEtkinlikRef(), null, false)){ 
				kriterler.add(new QueryObject("etkinlikRef", ApplicationConstant._equals, getEtkinlikRef())); 
			} 
			if(validateQueryObject(getDosyaturu(), null, true)){ 
				kriterler.add(new QueryObject("dosyaturu", ApplicationConstant._equals, getDosyaturu())); 
			} 
			if(validateQueryObject(getAd(), null, false)){ 
				kriterler.add(new QueryObject("ad", ApplicationConstant._beginsWith, getAd(), false)); 
			} 
			if(validateQueryObject(getPath(), null, false)){ 
				kriterler.add(new QueryObject("path", ApplicationConstant._beginsWith, getPath(), false)); 
			} 
		} catch(Exception e){ 
			System.out.println("Error @createQueryCriterias method, Class : EtkinlikdosyaFilter :" + e.getMessage()); 
		}  
		criteriaList.add(kriterler); 
		criteriaList.add(opsiyonelKriterler); 
		return criteriaList; 
	} 
	 
	public Long getRID() { 
		return this.rID; 
	} 
	 
	public void setRID(Long rID) { 
		this.rID = rID; 
	} 
  
	public Etkinlik getEtkinlikRef() { 
		return this.etkinlikRef; 
	} 
	 
	public void setEtkinlikRef(Etkinlik etkinlikRef) { 
		this.etkinlikRef = etkinlikRef; 
	} 
  
	public Dosyaturu getDosyaturu() { 
		return this.dosyaturu; 
	} 
	 
	public void setDosyaturu(Dosyaturu dosyaturu) { 
		this.dosyaturu = dosyaturu; 
	} 
  
	public String getAd() { 
		return this.ad; 
	} 
	 
	public void setAd(String ad) { 
		this.ad = ad; 
	} 
  
	public String getPath() { 
		return this.path; 
	} 
	 
	public void setPath(String path) { 
		this.path = path; 
	} 
  
	
} 
