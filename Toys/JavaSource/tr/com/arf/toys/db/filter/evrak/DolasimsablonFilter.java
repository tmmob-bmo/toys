package tr.com.arf.toys.db.filter.evrak; 
 
import java.util.ArrayList;
import java.util.List;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.framework.db.model.query.QueryObject;
import tr.com.arf.toys.db.enumerated.evrak.EvrakHareketTuru;
import tr.com.arf.toys.db.model.system.Birim;
import tr.com.arf.toys.utility.application.ApplicationConstant;
 
 
public class DolasimsablonFilter extends BaseFilter { 
	
private static final long serialVersionUID = 1L; 
	
	private Long rID; 
	private EvrakHareketTuru evrakHareketTuru; 
	private String ad; 
	
	private Birim birimRef; 
	
	@Override 
	public void init() { 
		this.rID = null; 	
		this.birimRef = null; 
		this.evrakHareketTuru = EvrakHareketTuru._NULL; 
		this.ad = null; 	
	} 
	 
	@Override 
	public List<ArrayList<QueryObject>> createQueryCriterias(){ 
		ArrayList<ArrayList<QueryObject>> criteriaList = new ArrayList<ArrayList<QueryObject>>(); 
		ArrayList<QueryObject> kriterler = new ArrayList<QueryObject>();	 
		ArrayList<QueryObject> opsiyonelKriterler = new ArrayList<QueryObject>(); 
		try { 	 
			if(validateQueryObject(getRID(), null, false)){ 
				kriterler.add(new QueryObject("rID", ApplicationConstant._equals, getRID())); 
			} 
			if(validateQueryObject(getEvrakHareketTuru(), null, true)){ 
				kriterler.add(new QueryObject("evrakHareketTuru", ApplicationConstant._equals, getEvrakHareketTuru())); 
			} 
			if(validateQueryObject(getBirimRef(), null, false)){;
				kriterler.add(new QueryObject("birimRef", ApplicationConstant._equals, getBirimRef()));
			}  
			if(validateQueryObject(getAd(), null, false)){ 
				kriterler.add(new QueryObject("ad", ApplicationConstant._beginsWith, getAd(), false)); 
			} 
		} catch(Exception e){ 
			System.out.println("Error @createQueryCriterias method, Class : DolasimsablonFilter :" + e.getMessage()); 
		}  
		criteriaList.add(kriterler); 
		criteriaList.add(opsiyonelKriterler); 
		return criteriaList; 
	} 
	 
	public Long getRID() { 
		return this.rID; 
	} 
	 
	public void setRID(Long rID) { 
		this.rID = rID; 
	} 
  
	public EvrakHareketTuru getEvrakHareketTuru() { 
		return this.evrakHareketTuru; 
	} 
	 
	public void setEvrakHareketTuru(EvrakHareketTuru evrakHareketTuru) { 
		this.evrakHareketTuru = evrakHareketTuru; 
	} 
  
	public String getAd() { 
		return this.ad; 
	} 
	 
	public void setAd(String ad) { 
		this.ad = ad; 
	} 
	
	public Birim getBirimRef() {
		return birimRef;
	}

	public void setBirimRef(Birim birimRef) {
		this.birimRef = birimRef;
	}
	
} 
