package tr.com.arf.toys.db.filter.system; 
 
import java.util.ArrayList;
import java.util.List;

import tr.com.arf.framework.db.enumerated._common.EvetHayir;
import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.framework.db.model.query.QueryObject;
import tr.com.arf.toys.db.model.system.Kullanici;
import tr.com.arf.toys.db.model.system.Role;
import tr.com.arf.toys.utility.application.ApplicationConstant;
 
 
public class KullaniciRoleFilter extends BaseFilter { 
	
private static final long serialVersionUID = 1L; 
	
	private Kullanici kullaniciRef; 
	private Role roleref; 
	private EvetHayir active; 
	
	@Override 
	public void init() { 
		this.kullaniciRef = null; 	
		this.roleref = null; 	
		this.active = EvetHayir._NULL;	
	} 
	 
	@Override 
	public List<ArrayList<QueryObject>> createQueryCriterias(){ 
		ArrayList<ArrayList<QueryObject>> criteriaList = new ArrayList<ArrayList<QueryObject>>(); 
		ArrayList<QueryObject> kriterler = new ArrayList<QueryObject>();	 
		ArrayList<QueryObject> opsiyonelKriterler = new ArrayList<QueryObject>(); 
		try { 	 
			if(validateQueryObject(getKullaniciRef(), 0, false)){
				kriterler.add(new QueryObject("kullaniciRef", ApplicationConstant._equals, getKullaniciRef())); 	
			}
			if(validateQueryObject(getRoleref(), null, false)){ 
				kriterler.add(new QueryObject("roleref", ApplicationConstant._equals, getRoleref())); 
			} 
			if(validateQueryObject(getActive(), null, true)){ 
				kriterler.add(new QueryObject("active", ApplicationConstant._equals, getActive())); 
			} 
		} catch(Exception e){ 
			System.out.println("Error @createQueryCriterias method, Class : kullaniciroleFilter :" + e.getMessage()); 
		}  
		criteriaList.add(kriterler); 
		criteriaList.add(opsiyonelKriterler); 
		return criteriaList; 
	} 
	 
	public Kullanici getKullaniciRef() { 
		return this.kullaniciRef; 
	} 
	 
	public void setKullaniciRef(Kullanici kullaniciRef) { 
		this.kullaniciRef = kullaniciRef; 
	} 
  
	public Role getRoleref() { 
		return this.roleref; 
	} 
	 
	public void setRoleref(Role roleref) { 
		this.roleref = roleref; 
	} 
  
	public EvetHayir getActive() { 
		return this.active; 
	} 
	 
	public void setActive(EvetHayir active) { 
		this.active = active; 
	} 
  
	
} 
