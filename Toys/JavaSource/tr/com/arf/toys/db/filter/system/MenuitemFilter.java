package tr.com.arf.toys.db.filter.system; 
 
import java.util.ArrayList;
import java.util.List;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.framework.db.model.query.QueryObject;
import tr.com.arf.toys.db.model.system.Menuitem;
import tr.com.arf.toys.utility.application.ApplicationConstant;
 
 
public class MenuitemFilter extends BaseFilter { 
	
private static final long serialVersionUID = 1L; 
	
	private Menuitem ustRef; 
	private String ad; 
	private String erisimKodu; 
	private String tablo; 
	
	@Override 
	public void init() { 
		this.ustRef = null; 	
		this.ad = null; 	
		this.erisimKodu = null;
		this.tablo = null; 
	} 
	 
	@Override 
	public List<ArrayList<QueryObject>> createQueryCriterias(){ 
		ArrayList<ArrayList<QueryObject>> criteriaList = new ArrayList<ArrayList<QueryObject>>(); 
		ArrayList<QueryObject> kriterler = new ArrayList<QueryObject>();	 
		ArrayList<QueryObject> opsiyonelKriterler = new ArrayList<QueryObject>(); 
		try { 	 
			if(validateQueryObject(getUstRef(), null, false)){ 
				kriterler.add(new QueryObject("ustRef", ApplicationConstant._equals, getUstRef())); 
			} 
			if(validateQueryObject(getAd(), null, false)){ 
				kriterler.add(new QueryObject("ad", ApplicationConstant._beginsWith, getAd())); 
			} 
			if(validateQueryObject(getErisimKodu(), null, false)){ 
				kriterler.add(new QueryObject("erisimKodu", ApplicationConstant._beginsWith, getErisimKodu())); 
			} 
			if(validateQueryObject(getTablo(), null, false)){ 
				kriterler.add(new QueryObject("tablo", ApplicationConstant._beginsWith, getTablo(), false)); 
			}
		} catch(Exception e){ 
			System.out.println("Error @createQueryCriterias method, Class : MenuitemFilter :" + e.getMessage()); 
		}  
		criteriaList.add(kriterler); 
		criteriaList.add(opsiyonelKriterler); 
		return criteriaList; 
	} 
	 
	public Menuitem getUstRef() { 
		return this.ustRef; 
	} 
	 
	public void setUstRef(Menuitem ustRef) { 
		this.ustRef = ustRef; 
	} 
  
	public String getAd() { 
		return this.ad; 
	} 
	 
	public void setAd(String ad) { 
		this.ad = ad; 
	} 
  
	public String getErisimKodu() { 
		return this.erisimKodu; 
	} 
	 
	public void setErisimKodu(String erisimKodu) { 
		this.erisimKodu = erisimKodu; 
	} 
	
	public String getTablo() {
		return tablo;
	}

	public void setTablo(String tablo) {
		this.tablo = tablo;
	}
  
	
} 
