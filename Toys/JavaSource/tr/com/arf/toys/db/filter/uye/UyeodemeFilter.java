package tr.com.arf.toys.db.filter.uye; 
 
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.framework.db.model.query.QueryObject;
import tr.com.arf.toys.db.enumerated.uye.OdemeTip;
import tr.com.arf.toys.db.model.uye.Uyeaidat;
import tr.com.arf.toys.db.model.uye.Uyemuafiyet;
import tr.com.arf.toys.utility.application.ApplicationConstant;
 
 
public class UyeodemeFilter extends BaseFilter { 
	
private static final long serialVersionUID = 1L; 
	
	private Uyeaidat uyeaidatRef; 
	
	private java.util.Date tarih; 
	private java.util.Date tarihMax; 
	private BigDecimal miktar; 
	private OdemeTip odemetip; 
	private Uyemuafiyet muafiyetRef; 
	
	@Override 
	public void init() { 
		this.uyeaidatRef = null; 	
		this.tarih = null; 	
		this.tarihMax = null; 	
		this.miktar = null; 	
		this.odemetip = OdemeTip._NULL;	
		this.muafiyetRef = null; 	
	} 
	 
	@Override 
	public List<ArrayList<QueryObject>> createQueryCriterias(){ 
		ArrayList<ArrayList<QueryObject>> criteriaList = new ArrayList<ArrayList<QueryObject>>(); 
		ArrayList<QueryObject> kriterler = new ArrayList<QueryObject>();	 
		ArrayList<QueryObject> opsiyonelKriterler = new ArrayList<QueryObject>(); 
		try { 	 
			if(validateQueryObject(getUyeaidatRef(), null, false)){ 
				kriterler.add(new QueryObject("uyeaidatRef", ApplicationConstant._equals, getUyeaidatRef())); 
			} 
			if(validateQueryObject(getTarih(), null, false)  && validateQueryObject(getTarihMax(), null, false)){
				kriterler.add(new QueryObject("tarih", ApplicationConstant._equalsAndLargerThan, getTarih())); 
				kriterler.add(new QueryObject("tarih", ApplicationConstant._equalsAndSmallerThan, getTarihMax())); 
			} else if(validateQueryObject(getTarih(), null, false)){ 
				kriterler.add(new QueryObject("tarih", ApplicationConstant._equalsAndLargerThan, getTarih()));
			}
			if(validateQueryObject(getMiktar(), 0, false)){ 
				kriterler.add(new QueryObject("miktar", ApplicationConstant._equals, getMiktar())); 	
			}
			if(validateQueryObject(getOdemetip(), null, true)){ 
				kriterler.add(new QueryObject("odemetip", ApplicationConstant._equals, getOdemetip())); 
			} 
			if(validateQueryObject(getMuafiyetRef(), null, false)){ 
				kriterler.add(new QueryObject("muafiyetRef", ApplicationConstant._equals, getMuafiyetRef())); 
			} 
		} catch(Exception e){ 
			System.out.println("Error @createQueryCriterias method, Class : UyeodemeFilter :" + e.getMessage()); 
		}  
		criteriaList.add(kriterler); 
		criteriaList.add(opsiyonelKriterler); 
		return criteriaList; 
	} 
	 
	public Uyeaidat getUyeaidatRef() { 
		return this.uyeaidatRef; 
	} 
	 
	public void setUyeaidatRef(Uyeaidat uyeaidatRef) { 
		this.uyeaidatRef = uyeaidatRef; 
	} 
  
	public Date getTarih() { 
		return this.tarih; 
	} 
	 
	public void setTarih(Date tarih) { 
		this.tarih = tarih; 
	} 
  
	public Date getTarihMax() { 
		return this.tarihMax; 
	} 
	 
	public void setTarihMax(Date tarihMax) { 
		this.tarihMax = tarihMax; 
	} 
  
	public BigDecimal getMiktar() { 
		return this.miktar; 
	} 
	 
	public void setMiktar(BigDecimal miktar) { 
		this.miktar = miktar; 
	} 
  
	public OdemeTip getOdemetip() { 
		return this.odemetip; 
	} 
	 
	public void setOdemetip(OdemeTip odemetip) { 
		this.odemetip = odemetip; 
	} 
  
	public Uyemuafiyet getMuafiyetRef() { 
		return this.muafiyetRef; 
	} 
	 
	public void setMuafiyetRef(Uyemuafiyet muafiyetRef) { 
		this.muafiyetRef = muafiyetRef; 
	} 
  
	
} 
