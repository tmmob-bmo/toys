package tr.com.arf.toys.db.filter.evrak;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.framework.db.model.query.QueryObject;
import tr.com.arf.framework.utility.tool.DateUtil;
import tr.com.arf.toys.db.enumerated.evrak.AciliyetTuru;
import tr.com.arf.toys.db.enumerated.evrak.EvrakDurum;
import tr.com.arf.toys.db.enumerated.evrak.EvrakHareketTuru;
import tr.com.arf.toys.db.enumerated.evrak.EvrakTuru;
import tr.com.arf.toys.db.enumerated.evrak.GizlilikDerecesi;
import tr.com.arf.toys.db.enumerated.evrak.HazirEvrakTuru;
import tr.com.arf.toys.db.enumerated.evrak.TeslimTuru;
import tr.com.arf.toys.db.model.evrak.Dolasimsablon;
import tr.com.arf.toys.db.model.evrak.Dolasimsablondetay;
import tr.com.arf.toys.db.model.evrak.Dosyakodu;
import tr.com.arf.toys.db.model.kisi.Kisi;
import tr.com.arf.toys.db.model.kisi.Kurum;
import tr.com.arf.toys.db.model.system.Birim;
import tr.com.arf.toys.db.model.system.Personel;
import tr.com.arf.toys.db.model.uye.Uye;
import tr.com.arf.toys.service.application.ManagedBeanLocator;
import tr.com.arf.toys.utility.application.ApplicationConstant;

public class EvrakFilter extends BaseFilter {

	private static final long serialVersionUID = 1L;

	private Long rID;
	private int yil;
	private java.util.Date kayittarihi;
	private java.util.Date kayittarihiMax;
	private String kayitno;
	private EvrakHareketTuru evrakHareketTuru;
	private Kurum gonderenKurumRef;
	private Kisi gonderenkisiRef;
	private Dosyakodu dosyaKoduRef;
	private String konusu;
	private java.util.Date evraktarihi;
	private java.util.Date evraktarihiMax;
	private String evrakno;
	private EvrakTuru evrakturu;
	private TeslimTuru teslimturu;
	private GizlilikDerecesi gizlilikderecesi;
	private AciliyetTuru aciliyetturu;
	private EvrakDurum durum;
	private Personel hazirlayanPersonelRef;
	private String takdimmakami;
	private String anahtarkelime;
	private Kurum gittigiKurumRef;
	private Birim gittigiBirimRef;
	private Dolasimsablon sablonRef;
	private Dolasimsablondetay sonDolasimSablonDetayRef;

	private String gonderenad;
	private String gonderensoyad;

	private HazirEvrakTuru hazirEvrakTuru;
	private Uye gittigiUyeRef;

	@Override
	public void init() {
		this.rID = null;
		this.yil = DateUtil.getYear(new Date());
		this.kayittarihi = null;
		this.kayittarihiMax = null;
		this.kayitno = null;
		this.gonderenKurumRef = null;
		this.gonderenkisiRef = null;
		this.dosyaKoduRef = null;
		this.konusu = null;
		this.evraktarihi = null;
		this.evraktarihiMax = null;
		this.evrakno = null;
		this.evrakturu = EvrakTuru._NULL;
		this.teslimturu = TeslimTuru._NULL;
		this.gizlilikderecesi = GizlilikDerecesi._NULL;
		this.aciliyetturu = AciliyetTuru._NULL;
		this.durum = EvrakDurum._ONAYLANMIS;
		this.hazirlayanPersonelRef = null;
		this.takdimmakami = null;
		this.anahtarkelime = null;
		this.evrakHareketTuru = EvrakHareketTuru._NULL;
		this.gonderenad = null;
		this.gonderensoyad = null;
		this.hazirEvrakTuru = HazirEvrakTuru._NULL;
		this.gittigiUyeRef = null;
	}

	@Override
	public List<ArrayList<QueryObject>> createQueryCriterias() {
		ArrayList<ArrayList<QueryObject>> criteriaList = new ArrayList<ArrayList<QueryObject>>();
		ArrayList<QueryObject> kriterler = new ArrayList<QueryObject>();
		ArrayList<QueryObject> opsiyonelKriterler = new ArrayList<QueryObject>();
		try {
			if (validateQueryObject(getrID(), 0, false)) {
				kriterler.add(new QueryObject("rID", ApplicationConstant._equals, getrID()));
			}
			if (validateQueryObject(getYil(), 0, false)) {
				kriterler.add(new QueryObject("yil", ApplicationConstant._equals, getYil()));
			}
			if (validateQueryObject(getKayittarihi(), null, false) && validateQueryObject(getKayittarihiMax(), null, false)) {
				kriterler.add(new QueryObject("kayittarihi", ApplicationConstant._equalsAndLargerThan, getKayittarihi()));
				kriterler.add(new QueryObject("kayittarihi", ApplicationConstant._equalsAndSmallerThan, getKayittarihiMax()));
			} else if (validateQueryObject(getKayittarihi(), null, false)) {
				kriterler.add(new QueryObject("kayittarihi", ApplicationConstant._equalsAndLargerThan, getKayittarihi()));
			}
			if (validateQueryObject(getEvrakHareketTuru(), null, true)) {
				kriterler.add(new QueryObject("evrakHareketTuru", ApplicationConstant._equals, getEvrakHareketTuru()));
			}
			if (validateQueryObject(getKayitno(), null, false)) {
				kriterler.add(new QueryObject("kayitno", ApplicationConstant._beginsWith, getKayitno(), false));
			}
			if (validateQueryObject(getGonderenKurumRef(), null, false)) {
				kriterler.add(new QueryObject("gonderenKurumRef", ApplicationConstant._equals, getGonderenKurumRef()));
			}
			if (validateQueryObject(getGonderenkisiRef(), null, false)) {
				kriterler.add(new QueryObject("gonderenkisiRef", ApplicationConstant._equals, getGonderenkisiRef()));
			}
			if (validateQueryObject(getGonderenad(), null, false)) {
				kriterler.add(new QueryObject("gonderenkisiRef.ad", ApplicationConstant._beginsWith, getGonderenad()));
			}
			if (validateQueryObject(getGonderensoyad(), null, false)) {
				kriterler.add(new QueryObject("gonderenkisiRef.soyad", ApplicationConstant._beginsWith, getGonderensoyad()));
			}
			if (validateQueryObject(getDosyaKoduRef(), null, false)) {
				kriterler.add(new QueryObject("dosyaKoduRef", ApplicationConstant._equals, getDosyaKoduRef()));
			}
			if (validateQueryObject(getKonusu(), null, false)) {
				kriterler.add(new QueryObject("konusu", ApplicationConstant._beginsWith, getKonusu(), false));
			}
			if (validateQueryObject(getEvraktarihi(), null, false) && validateQueryObject(getEvraktarihiMax(), null, false)) {
				kriterler.add(new QueryObject("evraktarihi", ApplicationConstant._equalsAndLargerThan, getEvraktarihi()));
				kriterler.add(new QueryObject("evraktarihi", ApplicationConstant._equalsAndSmallerThan, getEvraktarihiMax()));
			} else if (validateQueryObject(getEvraktarihi(), null, false)) {
				kriterler.add(new QueryObject("evraktarihi", ApplicationConstant._equalsAndLargerThan, getEvraktarihi()));
			}
			if (validateQueryObject(getEvrakno(), null, false)) {
				kriterler.add(new QueryObject("evrakno", ApplicationConstant._beginsWith, getEvrakno(), false));
			}
			if (validateQueryObject(getEvrakturu(), null, true)) {
				kriterler.add(new QueryObject("evrakturu", ApplicationConstant._equals, getEvrakturu()));
			}
			if (validateQueryObject(getTeslimturu(), null, true)) {
				kriterler.add(new QueryObject("teslimturu", ApplicationConstant._equals, getTeslimturu()));
			}
			if (validateQueryObject(getGizlilikderecesi(), null, true)) {
				kriterler.add(new QueryObject("gizlilikderecesi", ApplicationConstant._equals, getGizlilikderecesi()));
			}
			if (validateQueryObject(getAciliyetturu(), null, true)) {
				kriterler.add(new QueryObject("aciliyetturu", ApplicationConstant._equals, getAciliyetturu()));
			}
			if (validateQueryObject(getDurum(), null, true)) {
				kriterler.add(new QueryObject("durum", ApplicationConstant._equals, getDurum()));
			}
			if (validateQueryObject(getHazirlayanPersonelRef(), null, false)) {
				kriterler.add(new QueryObject("hazirlayanPersonelRef", ApplicationConstant._equals, getHazirlayanPersonelRef()));
			}
			if (validateQueryObject(getTakdimmakami(), null, false)) {
				kriterler.add(new QueryObject("takdimmakami", ApplicationConstant._beginsWith, getTakdimmakami(), false));
			}
			if (validateQueryObject(getAnahtarkelime(), null, false)) {
				kriterler.add(new QueryObject("anahtarkelime", ApplicationConstant._beginsWith, getAnahtarkelime(), false));
			}
			if (validateQueryObject(getGittigiBirimRef(), null, false)) {
				kriterler.add(new QueryObject("gittigiBirimRef", ApplicationConstant._equals, getGittigiBirimRef()));
			}
			if (validateQueryObject(getGittigiKurumRef(), null, false)) {
				kriterler.add(new QueryObject("gittigiKurumRef", ApplicationConstant._equals, getGittigiKurumRef()));
			}
			if (validateQueryObject(getSablonRef(), null, false)) {
				kriterler.add(new QueryObject("sablonRef", ApplicationConstant._equals, getSablonRef()));
			}
			if (validateQueryObject(getSablonRef(), null, false)) {
				kriterler.add(new QueryObject("sablonRef", ApplicationConstant._equals, getSablonRef()));
			}
			if (validateQueryObject(getSonDolasimSablonDetayRef(), null, false)) {
				kriterler.add(new QueryObject("sonDolasimSablonDetayRef", ApplicationConstant._equals, getSonDolasimSablonDetayRef()));
			}
			if (validateQueryObject(getHazirEvrakTuru(), null, true)) {
				kriterler.add(new QueryObject("hazirEvrakTuru", ApplicationConstant._equals, getHazirEvrakTuru()));
			}
			if (validateQueryObject(getGittigiUyeRef(), null, false)) {
				kriterler.add(new QueryObject("gittigiUyeRef", ApplicationConstant._equals, getGittigiUyeRef()));
			}

			// Personel sisteme kendi girdigi evraklari gorebilir.
			// Personel dolasim ile kendisine gonderilen evraklari gorebilir.
			// Personel dolasim ile calistigi birime gonderilen evraklari gorebilir.
			Personel aktifPersonel = ManagedBeanLocator.locateSessionUser().getPersonelRef();
			if (aktifPersonel != null) {
				String whereCon = "( (o.sistemeTanimlayan.rID = " + aktifPersonel.getRID() + ") OR " + "	( (Select m.personelRef.rID From Evrakdolasim m WHERE m.evrakRef.rID = o.rID AND m.gidistarih IS NULL) = " + aktifPersonel.getRID();
				if (aktifPersonel.getBirimRef() != null) {
					whereCon += " OR (Select n.birimRef.rID From Evrakdolasim n WHERE n.evrakRef.rID = o.rID AND n.gidistarih IS NULL) = " + aktifPersonel.getBirimRef().getRID() + "))";
				} else {
					whereCon += ") ) ";
				}
				kriterler.add(new QueryObject(whereCon, ApplicationConstant._userDefined, aktifPersonel));
			}
		} catch (Exception e) {
			System.out.println("Error @createQueryCriterias method, Class : EvrakFilter :" + e.getMessage());
		}
		criteriaList.add(kriterler);
		criteriaList.add(opsiyonelKriterler);
		return criteriaList;
	}

	public Long getrID() {
		return rID;
	}

	public void setrID(Long rID) {
		this.rID = rID;
	}

	public int getYil() {
		return this.yil;
	}

	public void setYil(int yil) {
		this.yil = yil;
	}

	public Date getKayittarihi() {
		return this.kayittarihi;
	}

	public void setKayittarihi(Date kayittarihi) {
		this.kayittarihi = kayittarihi;
	}

	public Date getKayittarihiMax() {
		return this.kayittarihiMax;
	}

	public void setKayittarihiMax(Date kayittarihiMax) {
		this.kayittarihiMax = kayittarihiMax;
	}

	public String getKayitno() {
		return this.kayitno;
	}

	public void setKayitno(String kayitno) {
		this.kayitno = kayitno;
	}

	public EvrakHareketTuru getEvrakHareketTuru() {
		return evrakHareketTuru;
	}

	public void setEvrakHareketTuru(EvrakHareketTuru evrakHareketTuru) {
		this.evrakHareketTuru = evrakHareketTuru;
	}

	public Kurum getGonderenKurumRef() {
		return this.gonderenKurumRef;
	}

	public void setGonderenKurumRef(Kurum gonderenKurumRef) {
		this.gonderenKurumRef = gonderenKurumRef;
	}

	public Kisi getGonderenkisiRef() {
		return this.gonderenkisiRef;
	}

	public void setGonderenkisiRef(Kisi gonderenkisiRef) {
		this.gonderenkisiRef = gonderenkisiRef;
	}

	public Dosyakodu getDosyaKoduRef() {
		return this.dosyaKoduRef;
	}

	public void setDosyaKoduRef(Dosyakodu dosyaKoduRef) {
		this.dosyaKoduRef = dosyaKoduRef;
	}

	public String getKonusu() {
		return this.konusu;
	}

	public void setKonusu(String konusu) {
		this.konusu = konusu;
	}

	public Date getEvraktarihi() {
		return this.evraktarihi;
	}

	public void setEvraktarihi(Date evraktarihi) {
		this.evraktarihi = evraktarihi;
	}

	public Date getEvraktarihiMax() {
		return this.evraktarihiMax;
	}

	public void setEvraktarihiMax(Date evraktarihiMax) {
		this.evraktarihiMax = evraktarihiMax;
	}

	public String getEvrakno() {
		return this.evrakno;
	}

	public void setEvrakno(String evrakno) {
		this.evrakno = evrakno;
	}

	public EvrakTuru getEvrakturu() {
		return this.evrakturu;
	}

	public void setEvrakturu(EvrakTuru evrakturu) {
		this.evrakturu = evrakturu;
	}

	public TeslimTuru getTeslimturu() {
		return this.teslimturu;
	}

	public void setTeslimturu(TeslimTuru teslimturu) {
		this.teslimturu = teslimturu;
	}

	public GizlilikDerecesi getGizlilikderecesi() {
		return this.gizlilikderecesi;
	}

	public void setGizlilikderecesi(GizlilikDerecesi gizlilikderecesi) {
		this.gizlilikderecesi = gizlilikderecesi;
	}

	public AciliyetTuru getAciliyetturu() {
		return this.aciliyetturu;
	}

	public void setAciliyetturu(AciliyetTuru aciliyetturu) {
		this.aciliyetturu = aciliyetturu;
	}

	public EvrakDurum getDurum() {
		return this.durum;
	}

	public void setDurum(EvrakDurum durum) {
		this.durum = durum;
	}

	public Personel getHazirlayanPersonelRef() {
		return this.hazirlayanPersonelRef;
	}

	public void setHazirlayanPersonelRef(Personel hazirlayanPersonelRef) {
		this.hazirlayanPersonelRef = hazirlayanPersonelRef;
	}

	public String getTakdimmakami() {
		return this.takdimmakami;
	}

	public void setTakdimmakami(String takdimmakami) {
		this.takdimmakami = takdimmakami;
	}

	public String getAnahtarkelime() {
		return this.anahtarkelime;
	}

	public void setAnahtarkelime(String anahtarkelime) {
		this.anahtarkelime = anahtarkelime;
	}

	public Kurum getGittigiKurumRef() {
		return gittigiKurumRef;
	}

	public void setGittigiKurumRef(Kurum gittigiKurumRef) {
		this.gittigiKurumRef = gittigiKurumRef;
	}

	public Birim getGittigiBirimRef() {
		return gittigiBirimRef;
	}

	public void setGittigiBirimRef(Birim gittigiBirimRef) {
		this.gittigiBirimRef = gittigiBirimRef;
	}

	public Dolasimsablon getSablonRef() {
		return sablonRef;
	}

	public void setSablonRef(Dolasimsablon sablonRef) {
		this.sablonRef = sablonRef;
	}

	public Dolasimsablondetay getSonDolasimSablonDetayRef() {
		return sonDolasimSablonDetayRef;
	}

	public void setSonDolasimSablonDetayRef(Dolasimsablondetay sonDolasimSablonDetayRef) {
		this.sonDolasimSablonDetayRef = sonDolasimSablonDetayRef;
	}

	public String getGonderenad() {
		return gonderenad;
	}

	public void setGonderenad(String gonderenad) {
		this.gonderenad = gonderenad;
	}

	public String getGonderensoyad() {
		return gonderensoyad;
	}

	public void setGonderensoyad(String gonderensoyad) {
		this.gonderensoyad = gonderensoyad;
	}

	public HazirEvrakTuru getHazirEvrakTuru() {
		return hazirEvrakTuru;
	}

	public void setHazirEvrakTuru(HazirEvrakTuru hazirEvrakTuru) {
		this.hazirEvrakTuru = hazirEvrakTuru;
	}

	public Uye getGittigiUyeRef() {
		return gittigiUyeRef;
	}

	public void setGittigiUyeRef(Uye gittigiUyeRef) {
		this.gittigiUyeRef = gittigiUyeRef;
	}
}
