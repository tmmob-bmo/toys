package tr.com.arf.toys.db.filter.system; 
 
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.framework.db.model.query.QueryObject;
import tr.com.arf.toys.utility.application.ApplicationConstant;
 
 
public class OdemetipFilter extends BaseFilter { 
	
private static final long serialVersionUID = 1L; 
	
	private String kod; 
	private String ad; 
	private BigDecimal miktar; 
	private java.util.Date baslangictarih; 
	private java.util.Date baslangictarihMax; 
	private java.util.Date bitistarih; 
	private java.util.Date bitistarihMax; 
	
	@Override 
	public void init() { 
		this.kod = null; 	
		this.ad = null; 	
		this.miktar = null; 	
		this.baslangictarih = null; 	
		this.baslangictarihMax = null; 	
		this.bitistarih = null; 	
		this.bitistarihMax = null; 	
	} 
	 
	@Override 
	public List<ArrayList<QueryObject>> createQueryCriterias(){ 
		ArrayList<ArrayList<QueryObject>> criteriaList = new ArrayList<ArrayList<QueryObject>>(); 
		ArrayList<QueryObject> kriterler = new ArrayList<QueryObject>();	 
		ArrayList<QueryObject> opsiyonelKriterler = new ArrayList<QueryObject>(); 
		try { 	 
			if(validateQueryObject(getKod(), null, false)){ 
				kriterler.add(new QueryObject("kod", ApplicationConstant._beginsWith, getKod())); 
			} 
			if(validateQueryObject(getAd(), null, false)){ 
				kriterler.add(new QueryObject("ad", ApplicationConstant._beginsWith, getAd())); 
			} 
			if(validateQueryObject(getMiktar(), 0, false)){
				kriterler.add(new QueryObject("miktar", ApplicationConstant._equals, getMiktar())); 	
			}
			if(validateQueryObject(getBaslangictarih(), null, false)  && validateQueryObject(getBaslangictarihMax(), null, false)){
				kriterler.add(new QueryObject("baslangictarih", ApplicationConstant._equalsAndLargerThan, getBaslangictarih())); 
				kriterler.add(new QueryObject("baslangictarih", ApplicationConstant._equalsAndSmallerThan, getBaslangictarihMax())); 
			} else if(validateQueryObject(getBaslangictarih(), null, false)){ 
				kriterler.add(new QueryObject("baslangictarih", ApplicationConstant._equalsAndLargerThan, getBaslangictarih()));
			}
			if(validateQueryObject(getBitistarih(), null, false)  && validateQueryObject(getBitistarihMax(), null, false)){
				kriterler.add(new QueryObject("bitistarih", ApplicationConstant._equalsAndLargerThan, getBitistarih())); 
				kriterler.add(new QueryObject("bitistarih", ApplicationConstant._equalsAndSmallerThan, getBitistarihMax())); 
			} else if(validateQueryObject(getBitistarih(), null, false)){ 
				kriterler.add(new QueryObject("bitistarih", ApplicationConstant._equalsAndLargerThan, getBitistarih()));
			}
		} catch(Exception e){ 
			System.out.println("Error @createQueryCriterias method, Class : OdemetipFilter :" + e.getMessage()); 
		}  
		criteriaList.add(kriterler); 
		criteriaList.add(opsiyonelKriterler); 
		return criteriaList; 
	} 
	 
	public String getKod() { 
		return this.kod; 
	} 
	 
	public void setKod(String kod) { 
		this.kod = kod; 
	} 
  
	public String getAd() { 
		return this.ad; 
	} 
	 
	public void setAd(String ad) { 
		this.ad = ad; 
	} 
  
	public BigDecimal getMiktar() { 
		return this.miktar; 
	} 
	 
	public void setMiktar(BigDecimal miktar) { 
		this.miktar = miktar; 
	} 
  
	public Date getBaslangictarih() { 
		return this.baslangictarih; 
	} 
	 
	public void setBaslangictarih(Date baslangictarih) { 
		this.baslangictarih = baslangictarih; 
	} 
  
	public Date getBaslangictarihMax() { 
		return this.baslangictarihMax; 
	} 
	 
	public void setBaslangictarihMax(Date baslangictarihMax) { 
		this.baslangictarihMax = baslangictarihMax; 
	} 
  
	public Date getBitistarih() { 
		return this.bitistarih; 
	} 
	 
	public void setBitistarih(Date bitistarih) { 
		this.bitistarih = bitistarih; 
	} 
  
	public Date getBitistarihMax() { 
		return this.bitistarihMax; 
	} 
	 
	public void setBitistarihMax(Date bitistarihMax) { 
		this.bitistarihMax = bitistarihMax; 
	} 
  
	
} 
