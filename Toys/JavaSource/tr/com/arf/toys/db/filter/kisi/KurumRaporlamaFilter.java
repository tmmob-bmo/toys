package tr.com.arf.toys.db.filter.kisi; 
 

import tr.com.arf.toys.db.model.kisi.IsYeriTemsilcileri;
import tr.com.arf.toys.db.model.kisi.Kisi;
import tr.com.arf.toys.db.model.system.Ilce;
import tr.com.arf.toys.db.model.system.Kullanicirapor;
import tr.com.arf.toys.db.model.system.KurumTuru;
import tr.com.arf.toys.db.model.system.Sehir;
 
 
public class KurumRaporlamaFilter { 

	private Sehir il;
	private Ilce ilce;
	private KurumTuru kurumturu;
	private String kurumadi;
	
	private Boolean telefonYok;
	private Boolean epostaYok;
	private Kisi sorumlu ; 
	private IsYeriTemsilcileri isyeritemsilcileri;
	private Kullanicirapor kullanicirapor;
	
	
	
	public KurumRaporlamaFilter(){
		 super();
		 init();
	 }
	 
	public void init() {  
		this.telefonYok=false;
		this.epostaYok=false;
		this.kurumadi=null;
		this.il=null;
		this.ilce=null;
		this.sorumlu=null;
		this.isyeritemsilcileri=null;
		this.kurumturu=null;
		this.kullanicirapor=null;
	}

	public Sehir getIl() {
		return il;
	}

	public void setIl(Sehir il) {
		this.il = il;
	}

	public Ilce getIlce() {
		return ilce;
	}

	public void setIlce(Ilce ilce) {
		this.ilce = ilce;
	}

	public KurumTuru getKurumturu() {
		return kurumturu;
	}

	public void setKurumturu(KurumTuru kurumturu) {
		this.kurumturu = kurumturu;
	}

	public String getKurumadi() {
		return kurumadi;
	}

	public void setKurumadi(String kurumadi) {
		this.kurumadi = kurumadi;
	}

	public Boolean getTelefonYok() {
		return telefonYok;
	}

	public void setTelefonYok(Boolean telefonYok) {
		this.telefonYok = telefonYok;
	}

	public Boolean getEpostaYok() {
		return epostaYok;
	}

	public void setEpostaYok(Boolean epostaYok) {
		this.epostaYok = epostaYok;
	}

	public Kisi getSorumlu() {
		return sorumlu;
	}

	public void setSorumlu(Kisi sorumlu) {
		this.sorumlu = sorumlu;
	}

	public IsYeriTemsilcileri getIsyeritemsilcileri() {
		return isyeritemsilcileri;
	}

	public void setIsyeritemsilcileri(IsYeriTemsilcileri isyeritemsilcileri) {
		this.isyeritemsilcileri = isyeritemsilcileri;
	}

	public Kullanicirapor getKullanicirapor() {
		return kullanicirapor;
	}

	public void setKullanicirapor(Kullanicirapor kullanicirapor) {
		this.kullanicirapor = kullanicirapor;
	}
	
} 
