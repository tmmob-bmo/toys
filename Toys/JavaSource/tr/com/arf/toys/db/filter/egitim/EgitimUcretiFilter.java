package tr.com.arf.toys.db.filter.egitim; 
 
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.framework.db.model.query.QueryObject;
import tr.com.arf.toys.db.enumerated.egitim.EgitimUcretTipi;
import tr.com.arf.toys.db.model.egitim.Egitimtanim;
import tr.com.arf.toys.utility.application.ApplicationConstant;
 
 
public class EgitimUcretiFilter extends BaseFilter { 
	
private static final long serialVersionUID = 1L; 
	
	private Long rID; 
	private Egitimtanim egitimtanimRef; 
	private BigDecimal ucret; 
	private int yil; 
	private EgitimUcretTipi egitimUcretTipi;
	
	@Override 
	public void init() { 
		this.rID = null; 	
		this.egitimtanimRef=null;
		this.ucret=null;
		this.yil=0;
		this.egitimUcretTipi=EgitimUcretTipi._NULL;
	} 
	 
	@Override 
	public List<ArrayList<QueryObject>> createQueryCriterias(){ 
		ArrayList<ArrayList<QueryObject>> criteriaList = new ArrayList<ArrayList<QueryObject>>(); 
		ArrayList<QueryObject> kriterler = new ArrayList<QueryObject>();	 
		ArrayList<QueryObject> opsiyonelKriterler = new ArrayList<QueryObject>(); 
		try { 	 
			if(validateQueryObject(getRID(), null, false)){ 
				kriterler.add(new QueryObject("rID", ApplicationConstant._equals, getRID())); 
			} 
			if(validateQueryObject(getEgitimtanimRef(), null, false)){ 
				kriterler.add(new QueryObject("egitimtanimRef", ApplicationConstant._equals, getEgitimtanimRef())); 
			} 
			if(validateQueryObject(getUcret(), null, false)){ 
				kriterler.add(new QueryObject("ucret", ApplicationConstant._equals, getUcret())); 
			} 
			if(validateQueryObject(getYil(), null, false)){ 
				kriterler.add(new QueryObject("yil", ApplicationConstant._equals, getYil())); 
			}
			if(validateQueryObject(getEgitimUcretTipi(), null, false)){ 
				kriterler.add(new QueryObject("egitimUcretTipi", ApplicationConstant._equals, getEgitimUcretTipi())); 
			}
		} catch(Exception e){ 
			System.out.println("Error @createQueryCriterias method, Class : EgitimUcretiFilter :" + e.getMessage()); 
		}  
		criteriaList.add(kriterler); 
		criteriaList.add(opsiyonelKriterler); 
		return criteriaList; 
	} 
	 
	public Long getRID() { 
		return this.rID; 
	} 
	 
	public void setRID(Long rID) { 
		this.rID = rID; 
	}

	public Egitimtanim getEgitimtanimRef() {
		return egitimtanimRef;
	}

	public void setEgitimtanimRef(Egitimtanim egitimtanimRef) {
		this.egitimtanimRef = egitimtanimRef;
	}

	public BigDecimal getUcret() {
		return ucret;
	}

	public void setUcret(BigDecimal ucret) {
		this.ucret = ucret;
	}

	public int getYil() {
		return yil;
	}

	public void setYil(int yil) {
		this.yil = yil;
	}

	public EgitimUcretTipi getEgitimUcretTipi() {
		return egitimUcretTipi;
	}

	public void setEgitimUcretTipi(EgitimUcretTipi egitimUcretTipi) {
		this.egitimUcretTipi = egitimUcretTipi;
	}

} 
