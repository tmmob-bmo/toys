package tr.com.arf.toys.db.filter.system; 
 
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.framework.db.model.query.QueryObject;
import tr.com.arf.toys.db.enumerated.system.BirimYetkiTuru;
import tr.com.arf.toys.db.enumerated.system.UyariDurum;
import tr.com.arf.toys.db.enumerated.system.UyariTipi;
import tr.com.arf.toys.db.model.kisi.Kisi;
import tr.com.arf.toys.db.model.system.Birim;
import tr.com.arf.toys.service.application.ManagedBeanLocator;
import tr.com.arf.toys.utility.application.ApplicationConstant;
import tr.com.arf.toys.view.controller._common.SessionUser;
 
 
public class UyarinotFilter extends BaseFilter { 
	
private static final long serialVersionUID = 1L; 
	
	private Kisi kullaniciRef; 
	private Birim birimRef; 
	private UyariTipi uyaritip; 
	private java.util.Date tarih; 
	private java.util.Date tarihMax; 
	private java.util.Date gecerliliktarih; 
	private java.util.Date gecerliliktarihMax; 
	private String uyarimetin; 
	private UyariDurum uyariDurum; 
	
	@Override 
	public void init() { 
		this.kullaniciRef = null; 	
		this.birimRef = null; 	
		this.uyaritip = UyariTipi._NULL;	
		this.tarih = null; 	
		this.tarihMax = null; 	
		this.gecerliliktarih = null; 	
		this.gecerliliktarihMax = null; 	
		this.uyarimetin = null; 	
		this.uyariDurum = UyariDurum._NULL;	
	} 
	 
	@Override 
	public List<ArrayList<QueryObject>> createQueryCriterias(){ 
		ArrayList<ArrayList<QueryObject>> criteriaList = new ArrayList<ArrayList<QueryObject>>(); 
		ArrayList<QueryObject> kriterler = new ArrayList<QueryObject>();	 
		ArrayList<QueryObject> opsiyonelKriterler = new ArrayList<QueryObject>(); 
		try { 	 
			if(validateQueryObject(getKullaniciRef(), null, false)){ 
				kriterler.add(new QueryObject("kullaniciRef", ApplicationConstant._equals, getKullaniciRef())); 
			} 
			if(validateQueryObject(getBirimRef(), null, false)){ 
				kriterler.add(new QueryObject("birimRef", ApplicationConstant._equals, getBirimRef())); 
			} 
			if(validateQueryObject(getUyaritip(), null, true)){ 
				kriterler.add(new QueryObject("uyaritip", ApplicationConstant._equals, getUyaritip())); 
			} 
			if(validateQueryObject(getTarih(), null, false)  && validateQueryObject(getTarihMax(), null, false)){
				kriterler.add(new QueryObject("tarih", ApplicationConstant._equalsAndLargerThan, getTarih())); 
				kriterler.add(new QueryObject("tarih", ApplicationConstant._equalsAndSmallerThan, getTarihMax())); 
			} else if(validateQueryObject(getTarih(), null, false)){ 
				kriterler.add(new QueryObject("tarih", ApplicationConstant._equalsAndLargerThan, getTarih()));
			}
			if(validateQueryObject(getGecerliliktarih(), null, false)  && validateQueryObject(getGecerliliktarihMax(), null, false)){
				kriterler.add(new QueryObject("gecerliliktarih", ApplicationConstant._equalsAndLargerThan, getGecerliliktarih())); 
				kriterler.add(new QueryObject("gecerliliktarih", ApplicationConstant._equalsAndSmallerThan, getGecerliliktarihMax())); 
			} else if(validateQueryObject(getGecerliliktarih(), null, false)){ 
				kriterler.add(new QueryObject("gecerliliktarih", ApplicationConstant._equalsAndLargerThan, getGecerliliktarih()));
			}
			if(validateQueryObject(getUyarimetin(), null, false)){ 
				kriterler.add(new QueryObject("uyarimetin", ApplicationConstant._beginsWith, getUyarimetin())); 
			} 
			if(validateQueryObject(getUyariDurum(), null, true)){ 
				kriterler.add(new QueryObject("uyariDurum", ApplicationConstant._equals, getUyariDurum())); 
			} 
			
			// Kullanicinin birimine gore yetkilendirme yapiliyor.
			SessionUser sessionUser = ManagedBeanLocator.locateSessionUser();
			if(sessionUser.getPersonelRef() != null && sessionUser.getPersonelRef().getBirimRef() != null){
				if(sessionUser.getBirimYetkiTuru() != BirimYetkiTuru._TUMBIRIMLER){
					if(sessionUser.getBirimYetkiTuru() == BirimYetkiTuru._KENDIBIRIMIVEALTBIRIMLERI){ 
						kriterler.add(new QueryObject("birimRef.erisimKodu", ApplicationConstant._beginsWith, sessionUser.getPersonelRef().getBirimRef().getErisimKodu())); 
					} else if(sessionUser.getBirimYetkiTuru() == BirimYetkiTuru._SADECEKENDIBIRIMLERI){ 
						kriterler.add(new QueryObject("birimRef", ApplicationConstant._equals, sessionUser.getPersonelRef().getBirimRef())); 
					}
				}
			}
		} catch(Exception e){ 
			System.out.println("Error @createQueryCriterias method, Class : UyarinotFilter :" + e.getMessage()); 
		}  
		criteriaList.add(kriterler); 
		criteriaList.add(opsiyonelKriterler); 
		return criteriaList; 
	} 
	 
	public Kisi getKullaniciRef() { 
		return this.kullaniciRef; 
	} 
	 
	public void setKullaniciRef(Kisi kullaniciRef) { 
		this.kullaniciRef = kullaniciRef; 
	} 
  
	public Birim getBirimRef() { 
		return this.birimRef; 
	} 
	 
	public void setBirimRef(Birim birimRef) { 
		this.birimRef = birimRef; 
	} 
  
	public UyariTipi getUyaritip() { 
		return this.uyaritip; 
	} 
	 
	public void setUyaritip(UyariTipi uyaritip) { 
		this.uyaritip = uyaritip; 
	} 
  
	public Date getTarih() { 
		return this.tarih; 
	} 
	 
	public void setTarih(Date tarih) { 
		this.tarih = tarih; 
	} 
  
	public Date getTarihMax() { 
		return this.tarihMax; 
	} 
	 
	public void setTarihMax(Date tarihMax) { 
		this.tarihMax = tarihMax; 
	} 
  
	public Date getGecerliliktarih() { 
		return this.gecerliliktarih; 
	} 
	 
	public void setGecerliliktarih(Date gecerliliktarih) { 
		this.gecerliliktarih = gecerliliktarih; 
	} 
  
	public Date getGecerliliktarihMax() { 
		return this.gecerliliktarihMax; 
	} 
	 
	public void setGecerliliktarihMax(Date gecerliliktarihMax) { 
		this.gecerliliktarihMax = gecerliliktarihMax; 
	} 
  
	public String getUyarimetin() { 
		return this.uyarimetin; 
	} 
	 
	public void setUyarimetin(String uyarimetin) { 
		this.uyarimetin = uyarimetin; 
	} 
  
	public UyariDurum getUyariDurum() { 
		return this.uyariDurum; 
	} 
	 
	public void setUyariDurum(UyariDurum uyariDurum) { 
		this.uyariDurum = uyariDurum; 
	} 
  
	
} 
