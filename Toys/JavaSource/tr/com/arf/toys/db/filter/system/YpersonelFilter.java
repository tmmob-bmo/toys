package tr.com.arf.toys.db.filter.system;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.framework.db.model.query.QueryObject;
import tr.com.arf.toys.db.enumerated.system.PersonelDurum;
import tr.com.arf.toys.db.model.kisi.Kisi;
import tr.com.arf.toys.db.model.system.Ybirim;
import tr.com.arf.toys.utility.application.ApplicationConstant;


	
	public class YpersonelFilter extends BaseFilter { 
	 
		/**
		 * 
		 */
		private static final long serialVersionUID = -8625214350098256902L;
		
		private java.util.Date baslamatarih; 
		private java.util.Date bitistarih; 
		private PersonelDurum durum; 
		private Kisi kisiRef; 
		private Ybirim birimRef; 
		private String adsoyad;  
		
		private String ad;
		private String soyad;
		private java.util.Date baslamatarihMax; 
		private java.util.Date bitistarihMax; 
		
		@Override
		public List<ArrayList<QueryObject>> createQueryCriterias() {
			ArrayList<ArrayList<QueryObject>> criteriaList = new ArrayList<ArrayList<QueryObject>>();
			ArrayList<QueryObject> kriterler = new ArrayList<QueryObject>();
			ArrayList<QueryObject> opsiyonelKriterler=  new ArrayList<QueryObject>();                      
					try { 	 		
						if(validateQueryObject(getBaslamatarih(), null, false)  && validateQueryObject(getBaslamatarihMax(), null, false)){
							kriterler.add(new QueryObject("baslamatarih", ApplicationConstant._equalsAndLargerThan, getBaslamatarih())); 
							kriterler.add(new QueryObject("baslamatarih", ApplicationConstant._equalsAndSmallerThan, getBaslamatarihMax())); 
						} else if(validateQueryObject(getBaslamatarih(), null, false)){ 
							kriterler.add(new QueryObject("baslamatarih", ApplicationConstant._equalsAndLargerThan, getBaslamatarih()));
						}
						if(validateQueryObject(getBitistarih(), null, false)  && validateQueryObject(getBitistarihMax(), null, false)){
							kriterler.add(new QueryObject("bitistarih", ApplicationConstant._equalsAndLargerThan, getBitistarih())); 
							kriterler.add(new QueryObject("bitistarih", ApplicationConstant._equalsAndSmallerThan, getBitistarihMax())); 
						} else if(validateQueryObject(getBitistarih(), null, false)){ 
							kriterler.add(new QueryObject("bitistarih", ApplicationConstant._equalsAndLargerThan, getBitistarih()));
						}
						if(validateQueryObject(getDurum(), null, true)){ 
							kriterler.add(new QueryObject("durum", ApplicationConstant._equals, getDurum())); 
						} 
						if(validateQueryObject(getKisiRef(), null, false)){ 
							kriterler.add(new QueryObject("kisiRef", ApplicationConstant._equals, getKisiRef())); 
						} 
						if(validateQueryObject(getAd(), null, false)){ 
							kriterler.add(new QueryObject("kisiRef.ad", ApplicationConstant._beginsWith, getAd())); 
						}
						if(validateQueryObject(getSoyad(), null, false)){ 
							kriterler.add(new QueryObject("kisiRef.soyad", ApplicationConstant._beginsWith, getSoyad())); 
						} 
						if(validateQueryObject(getAdsoyad(), null, false)){ 
							opsiyonelKriterler.add(new QueryObject("kisiRef.ad", ApplicationConstant._inside, getAdsoyad().trim(), false)); 
							opsiyonelKriterler.add(new QueryObject("kisiRef.soyad", ApplicationConstant._inside, getAdsoyad().trim(), false));
						} 
						if(validateQueryObject(getBirimRef(), null, false)){ 
							kriterler.add(new QueryObject("birimRef", ApplicationConstant._equals, getBirimRef())); }
					}
			 catch(Exception e){ 
				System.out.println("Error @createQueryCriterias method, Class : ypersonelFilter :" + e.getMessage()); 
			}  
			criteriaList.add(kriterler); 
			criteriaList.add(opsiyonelKriterler); 
			return criteriaList; 
		}
	 
		public java.util.Date getBaslamatarih() { 
			return baslamatarih; 
		} 
		 
		public String getAd() {
			return ad;
		}

		public void setAd(String ad) {
			this.ad = ad;
		}

		public String getSoyad() {
			return soyad;
		}

		public void setSoyad(String soyad) {
			this.soyad = soyad;
		} 
		
		public void setBaslamatarih(java.util.Date baslamatarih) { 
			this.baslamatarih = baslamatarih; 
		} 
	 
	 
		public PersonelDurum getDurum() { 
			return this.durum;  
		}  
	  
		public void setDurum(PersonelDurum durum) {  
			this.durum = durum;  
		}   
	 
		public Kisi getKisiRef() { 
			return kisiRef; 
		} 
		 
		public void setKisiRef(Kisi kisiRef) { 
			this.kisiRef = kisiRef; 
		}

		public Ybirim getBirimRef() {
			return birimRef;
		}

		public void setBirimRef(Ybirim birimRef) {
			this.birimRef = birimRef;
		}
		public Date getBaslamatarihMax() { 
			return this.baslamatarihMax; 
		} 
		 
		public void setBaslamatarihMax(Date baslamatarihMax) { 
			this.baslamatarihMax = baslamatarihMax; 
		} 
	  
		public Date getBitistarih() { 
			return this.bitistarih; 
		} 
		 
		public void setBitistarih(Date bitistarih) { 
			this.bitistarih = bitistarih; 
		} 
	  
		public Date getBitistarihMax() { 
			return this.bitistarihMax; 
		} 
		 
		public void setBitistarihMax(Date bitistarihMax) { 
			this.bitistarihMax = bitistarihMax; 
		} 
		public String getAdsoyad() {
			return adsoyad;
		}

		public void setAdsoyad(String adsoyad) {
			this.adsoyad = adsoyad;
		}
		
		@Override
		public void init() {
			this.baslamatarih = null; 
			this.bitistarih = null; 
			this.durum = PersonelDurum._NULL; 
			this.kisiRef = null; 
			this.birimRef = null;
			this.adsoyad = null;
			this.ad = null;
			this.soyad = null;
		}  
		 
		
		 

}