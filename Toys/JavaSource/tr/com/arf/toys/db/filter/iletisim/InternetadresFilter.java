package tr.com.arf.toys.db.filter.iletisim; 
 
import java.util.ArrayList;
import java.util.List;

import tr.com.arf.framework.db.enumerated._common.EvetHayir;
import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.framework.db.model.query.QueryObject;
import tr.com.arf.toys.db.enumerated.iletisim.NetAdresTuru;
import tr.com.arf.toys.db.model.kisi.Kisi;
import tr.com.arf.toys.db.model.kisi.Kurum;
import tr.com.arf.toys.utility.application.ApplicationConstant;
 
 
public class InternetadresFilter extends BaseFilter { 
	
private static final long serialVersionUID = 1L; 
	
	private Kisi kisiRef; 
	
	private Kurum kurumRef; 
	private String netadresmetni; 
	private NetAdresTuru netadresturu; 
	private EvetHayir varsayilan;
	

	@Override 
	public void init() { 
		this.kisiRef = null; 	
		this.kurumRef = null; 	
		this.netadresmetni = null; 	
		this.netadresturu = NetAdresTuru._NULL;	
		this.varsayilan=null;
	} 
	 
	@Override 
	public List<ArrayList<QueryObject>> createQueryCriterias(){ 
		ArrayList<ArrayList<QueryObject>> criteriaList = new ArrayList<ArrayList<QueryObject>>(); 
		ArrayList<QueryObject> kriterler = new ArrayList<QueryObject>();	 
		ArrayList<QueryObject> opsiyonelKriterler = new ArrayList<QueryObject>(); 
		try { 	 
			if(validateQueryObject(getKisiRef(), null, false)){ 
				kriterler.add(new QueryObject("kisiRef", ApplicationConstant._equals, getKisiRef())); 
			} 
			if(validateQueryObject(getKurumRef(), null, false)){ 
				kriterler.add(new QueryObject("kurumRef", ApplicationConstant._equals, getKurumRef())); 
			} 
			if(validateQueryObject(getNetadresmetni(), null, false)){ 
				kriterler.add(new QueryObject("netadresmetni", ApplicationConstant._beginsWith, getNetadresmetni())); 
			} 
			if(validateQueryObject(getNetadresturu(), null, true)){ 
				kriterler.add(new QueryObject("netadresturu", ApplicationConstant._equals, getNetadresturu())); 
			} 
			if(validateQueryObject(getVarsayilan(), null, true)){ 
				kriterler.add(new QueryObject("varsayilan", ApplicationConstant._equals, getVarsayilan())); 
			} 
		} catch(Exception e){ 
			System.out.println("Error @createQueryCriterias method, Class : InternetadresFilter :" + e.getMessage()); 
		}  
		criteriaList.add(kriterler); 
		criteriaList.add(opsiyonelKriterler); 
		return criteriaList; 
	} 
	 
	public Kisi getKisiRef() { 
		return this.kisiRef; 
	} 
	 
	public void setKisiRef(Kisi kisiRef) { 
		this.kisiRef = kisiRef; 
	} 
  
	public Kurum getKurumRef() { 
		return this.kurumRef; 
	} 
	 
	public void setKurumRef(Kurum kurumRef) { 
		this.kurumRef = kurumRef; 
	} 
  
	public String getNetadresmetni() { 
		return this.netadresmetni; 
	} 
	 
	public void setNetadresmetni(String netadresmetni) { 
		this.netadresmetni = netadresmetni; 
	} 
  
	public NetAdresTuru getNetadresturu() { 
		return this.netadresturu; 
	} 
	 
	public void setNetadresturu(NetAdresTuru netadresturu) { 
		this.netadresturu = netadresturu; 
	} 
  
	public EvetHayir getVarsayilan() {
		return varsayilan;
	}

	public void setVarsayilan(EvetHayir varsayilan) {
		this.varsayilan = varsayilan;
	}
} 
