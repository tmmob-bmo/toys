package tr.com.arf.toys.db.filter.system;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import tr.com.arf.framework.db.enumerated._common.EvetHayir;
import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.framework.db.model.query.QueryObject;
import tr.com.arf.toys.db.enumerated.system.BirimYetkiTuru;
import tr.com.arf.toys.db.enumerated.system.PersonelDurum;
import tr.com.arf.toys.db.model.kisi.Kisi;
import tr.com.arf.toys.db.model.system.Birim;
import tr.com.arf.toys.service.application.ManagedBeanLocator;
import tr.com.arf.toys.utility.application.ApplicationConstant;
import tr.com.arf.toys.view.controller._common.SessionUser;

public class PersonelFilter extends BaseFilter {

	private static final long serialVersionUID = 1L;

	private java.util.Date baslamatarih;
	private java.util.Date baslamatarihMax;
	private java.util.Date bitistarih;
	private java.util.Date bitistarihMax;
	private PersonelDurum durum;
	private Kisi kisiRef;
	private Birim birimRef;
	private String adsoyad;

	private String ad;
	private String soyad;
	private EvetHayir tumUyeSorgulama;

	@Override
	public void init() {
		this.baslamatarih = null;
		this.baslamatarihMax = null;
		this.bitistarih = null;
		this.bitistarihMax = null;
		this.durum = PersonelDurum._NULL;
		this.kisiRef = null;
		this.birimRef = null;
		this.adsoyad = null;
		this.ad = null;
		this.soyad = null;
		this.tumUyeSorgulama = EvetHayir._NULL;
	}

	@Override
	public List<ArrayList<QueryObject>> createQueryCriterias() {
		ArrayList<ArrayList<QueryObject>> criteriaList = new ArrayList<ArrayList<QueryObject>>();
		ArrayList<QueryObject> kriterler = new ArrayList<QueryObject>();
		ArrayList<QueryObject> opsiyonelKriterler = new ArrayList<QueryObject>();
		try {
			if (validateQueryObject(getBaslamatarih(), null, false) && validateQueryObject(getBaslamatarihMax(), null, false)) {
				kriterler.add(new QueryObject("baslamatarih", ApplicationConstant._equalsAndLargerThan, getBaslamatarih()));
				kriterler.add(new QueryObject("baslamatarih", ApplicationConstant._equalsAndSmallerThan, getBaslamatarihMax()));
			} else if (validateQueryObject(getBaslamatarih(), null, false)) {
				kriterler.add(new QueryObject("baslamatarih", ApplicationConstant._equalsAndLargerThan, getBaslamatarih()));
			}
			if (validateQueryObject(getBitistarih(), null, false) && validateQueryObject(getBitistarihMax(), null, false)) {
				kriterler.add(new QueryObject("bitistarih", ApplicationConstant._equalsAndLargerThan, getBitistarih()));
				kriterler.add(new QueryObject("bitistarih", ApplicationConstant._equalsAndSmallerThan, getBitistarihMax()));
			} else if (validateQueryObject(getBitistarih(), null, false)) {
				kriterler.add(new QueryObject("bitistarih", ApplicationConstant._equalsAndLargerThan, getBitistarih()));
			}
			if (validateQueryObject(getDurum(), null, true)) {
				kriterler.add(new QueryObject("durum", ApplicationConstant._equals, getDurum()));
			}
			if (validateQueryObject(getKisiRef(), null, false)) {
				kriterler.add(new QueryObject("kisiRef", ApplicationConstant._equals, getKisiRef()));
			}
			if (validateQueryObject(getAd(), null, false)) {
				kriterler.add(new QueryObject("kisiRef.ad", ApplicationConstant._beginsWith, getAd()));
			}
			if (validateQueryObject(getSoyad(), null, false)) {
				kriterler.add(new QueryObject("kisiRef.soyad", ApplicationConstant._beginsWith, getSoyad()));
			}
			if (validateQueryObject(getAdsoyad(), null, false)) {
				opsiyonelKriterler.add(new QueryObject("kisiRef.ad", ApplicationConstant._inside, getAdsoyad().trim(), false));
				opsiyonelKriterler.add(new QueryObject("kisiRef.soyad", ApplicationConstant._inside, getAdsoyad().trim(), false));
			}
			if (validateQueryObject(getBirimRef(), null, false)) {
				kriterler.add(new QueryObject("birimRef", ApplicationConstant._equals, getBirimRef()));
			}
			if (validateQueryObject(getTumUyeSorgulama(), null, true)) {
				kriterler.add(new QueryObject("tumUyeSorgulama", ApplicationConstant._equals, getTumUyeSorgulama()));
			}
			// Kullanicinin birimine gore yetkilendirme yapiliyor.
			SessionUser sessionUser = ManagedBeanLocator.locateSessionUser();
			if (sessionUser.getPersonelRef() != null && sessionUser.getPersonelRef().getBirimRef() != null) {
				if (sessionUser.getBirimYetkiTuru() != BirimYetkiTuru._TUMBIRIMLER && sessionUser.getPersonelRef().getTumUyeSorgulama() != EvetHayir._EVET) {
					if (sessionUser.getBirimYetkiTuru() == BirimYetkiTuru._KENDIBIRIMIVEALTBIRIMLERI) {
						kriterler.add(new QueryObject("birimRef.erisimKodu", ApplicationConstant._beginsWith, sessionUser.getPersonelRef().getBirimRef().getErisimKodu()));
					} else if (sessionUser.getBirimYetkiTuru() == BirimYetkiTuru._SADECEKENDIBIRIMLERI) {
						kriterler.add(new QueryObject("birimRef", ApplicationConstant._equals, sessionUser.getPersonelRef().getBirimRef()));
					}
				}
			}
		} catch (Exception e) {
			System.out.println("Error @createQueryCriterias method, Class : PersonelFilter :" + e.getMessage());
		}
		criteriaList.add(kriterler);
		criteriaList.add(opsiyonelKriterler);
		return criteriaList;
	}

	public String getAd() {
		return ad;
	}

	public void setAd(String ad) {
		this.ad = ad;
	}

	public String getSoyad() {
		return soyad;
	}

	public void setSoyad(String soyad) {
		this.soyad = soyad;
	}

	public Date getBaslamatarih() {
		return this.baslamatarih;
	}

	public void setBaslamatarih(Date baslamatarih) {
		this.baslamatarih = baslamatarih;
	}

	public Date getBaslamatarihMax() {
		return this.baslamatarihMax;
	}

	public void setBaslamatarihMax(Date baslamatarihMax) {
		this.baslamatarihMax = baslamatarihMax;
	}

	public Date getBitistarih() {
		return this.bitistarih;
	}

	public void setBitistarih(Date bitistarih) {
		this.bitistarih = bitistarih;
	}

	public Date getBitistarihMax() {
		return this.bitistarihMax;
	}

	public void setBitistarihMax(Date bitistarihMax) {
		this.bitistarihMax = bitistarihMax;
	}

	public PersonelDurum getDurum() {
		return this.durum;
	}

	public void setDurum(PersonelDurum durum) {
		this.durum = durum;
	}

	public Kisi getKisiRef() {
		return this.kisiRef;
	}

	public void setKisiRef(Kisi kisiRef) {
		this.kisiRef = kisiRef;
	}

	public Birim getBirimRef() {
		return birimRef;
	}

	public void setBirimRef(Birim birimRef) {
		this.birimRef = birimRef;
	}

	public String getAdsoyad() {
		return adsoyad;
	}

	public void setAdsoyad(String adsoyad) {
		this.adsoyad = adsoyad;
	}

	public EvetHayir getTumUyeSorgulama() {
		return tumUyeSorgulama;
	}

	public void setTumUyeSorgulama(EvetHayir tumUyeSorgulama) {
		this.tumUyeSorgulama = tumUyeSorgulama;
	}
}
