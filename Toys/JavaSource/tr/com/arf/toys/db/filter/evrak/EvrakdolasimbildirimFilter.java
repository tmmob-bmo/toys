package tr.com.arf.toys.db.filter.evrak; 
 
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.framework.db.model.query.QueryObject;
import tr.com.arf.toys.db.model.evrak.Evrak;
import tr.com.arf.toys.db.model.system.Personel;
import tr.com.arf.toys.utility.application.ApplicationConstant;
 
 
public class EvrakdolasimbildirimFilter extends BaseFilter { 
	
private static final long serialVersionUID = 1L; 
	
	private Evrak evrakRef; 
	
	private Long rID; 
	private Personel gonderenRef; 
	private Personel aliciRef; 
	private java.util.Date tarih; 
	private java.util.Date tarihMax; 
	private String aciklama; 
	
	@Override 
	public void init() { 
		this.rID = null; 	
		this.evrakRef = null; 	
		this.gonderenRef = null; 	
		this.aliciRef = null; 	
		this.tarih = null; 	
		this.tarihMax = null; 	
		this.aciklama = null; 	
	} 
	 
	@Override 
	public List<ArrayList<QueryObject>> createQueryCriterias(){ 
		ArrayList<ArrayList<QueryObject>> criteriaList = new ArrayList<ArrayList<QueryObject>>(); 
		ArrayList<QueryObject> kriterler = new ArrayList<QueryObject>();	 
		ArrayList<QueryObject> opsiyonelKriterler = new ArrayList<QueryObject>(); 
		try { 	 
			if(validateQueryObject(getRID(), null, false)){ 
				kriterler.add(new QueryObject("rID", ApplicationConstant._equals, getRID())); 
			} 
			if(validateQueryObject(getEvrakRef(), null, false)){ 
				kriterler.add(new QueryObject("evrakRef", ApplicationConstant._equals, getEvrakRef())); 
			} 
			if(validateQueryObject(getGonderenRef(), null, false)){ 
				kriterler.add(new QueryObject("gonderenRef", ApplicationConstant._equals, getGonderenRef())); 
			} 
			if(validateQueryObject(getAliciRef(), null, false)){ 
				kriterler.add(new QueryObject("aliciRef", ApplicationConstant._equals, getAliciRef())); 
			} 
			if(validateQueryObject(getTarih(), null, false)  && validateQueryObject(getTarihMax(), null, false)){
				kriterler.add(new QueryObject("tarih", ApplicationConstant._equalsAndLargerThan, getTarih())); 
				kriterler.add(new QueryObject("tarih", ApplicationConstant._equalsAndSmallerThan, getTarihMax())); 
			} else if(validateQueryObject(getTarih(), null, false)){ 
				kriterler.add(new QueryObject("tarih", ApplicationConstant._equalsAndLargerThan, getTarih()));
			}
			if(validateQueryObject(getAciklama(), null, false)){ 
				kriterler.add(new QueryObject("aciklama", ApplicationConstant._beginsWith, getAciklama(), false)); 
			} 
		} catch(Exception e){ 
			System.out.println("Error @createQueryCriterias method, Class : EvrakdolasimbildirimFilter :" + e.getMessage()); 
		}  
		criteriaList.add(kriterler); 
		criteriaList.add(opsiyonelKriterler); 
		return criteriaList; 
	} 
	 
	public Long getRID() { 
		return this.rID; 
	} 
	 
	public void setRID(Long rID) { 
		this.rID = rID; 
	} 
  
	public Evrak getEvrakRef() { 
		return this.evrakRef; 
	} 
	 
	public void setEvrakRef(Evrak evrakRef) { 
		this.evrakRef = evrakRef; 
	} 
  
	public Personel getGonderenRef() { 
		return this.gonderenRef; 
	} 
	 
	public void setGonderenRef(Personel gonderenRef) { 
		this.gonderenRef = gonderenRef; 
	} 
  
	public Personel getAliciRef() { 
		return this.aliciRef; 
	} 
	 
	public void setAliciRef(Personel aliciRef) { 
		this.aliciRef = aliciRef; 
	} 
  
	public Date getTarih() { 
		return this.tarih; 
	} 
	 
	public void setTarih(Date tarih) { 
		this.tarih = tarih; 
	} 
  
	public Date getTarihMax() { 
		return this.tarihMax; 
	} 
	 
	public void setTarihMax(Date tarihMax) { 
		this.tarihMax = tarihMax; 
	} 
  
	public String getAciklama() { 
		return this.aciklama; 
	} 
	 
	public void setAciklama(String aciklama) { 
		this.aciklama = aciklama; 
	} 
  
	
} 
