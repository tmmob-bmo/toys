package tr.com.arf.toys.db.filter.evrak; 
 
import java.util.ArrayList;
import java.util.List;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.framework.db.model.query.QueryObject;
import tr.com.arf.toys.db.model.evrak.Evrak;
import tr.com.arf.toys.utility.application.ApplicationConstant;
 
 
public class EvrakekFilter extends BaseFilter { 
	
private static final long serialVersionUID = 1L; 
	
	private Evrak evrakRef; 
	
	private Long rID; 
	private String path; 
	private String tanim; 
	
	@Override 
	public void init() { 
		this.rID = null; 	
		this.evrakRef = null; 	
		this.path = null; 	
		this.tanim = null; 	
	} 
	 
	@Override 
	public List<ArrayList<QueryObject>> createQueryCriterias(){ 
		ArrayList<ArrayList<QueryObject>> criteriaList = new ArrayList<ArrayList<QueryObject>>(); 
		ArrayList<QueryObject> kriterler = new ArrayList<QueryObject>();	 
		ArrayList<QueryObject> opsiyonelKriterler = new ArrayList<QueryObject>(); 
		try { 	 
			if(validateQueryObject(getRID(), null, false)){ 
				kriterler.add(new QueryObject("rID", ApplicationConstant._equals, getRID())); 
			} 
			if(validateQueryObject(getEvrakRef(), null, false)){ 
				kriterler.add(new QueryObject("evrakRef", ApplicationConstant._equals, getEvrakRef())); 
			} 
			if(validateQueryObject(getPath(), null, false)){ 
				kriterler.add(new QueryObject("path", ApplicationConstant._beginsWith, getPath(), false)); 
			} 
			if(validateQueryObject(getTanim(), null, false)){ 
				kriterler.add(new QueryObject("tanim", ApplicationConstant._beginsWith, getTanim(), false)); 
			} 
		} catch(Exception e){ 
			System.out.println("Error @createQueryCriterias method, Class : EvrakekFilter :" + e.getMessage()); 
		}  
		criteriaList.add(kriterler); 
		criteriaList.add(opsiyonelKriterler); 
		return criteriaList; 
	} 
	 
	public Long getRID() { 
		return this.rID; 
	} 
	 
	public void setRID(Long rID) { 
		this.rID = rID; 
	} 
  
	public Evrak getEvrakRef() { 
		return this.evrakRef; 
	} 
	 
	public void setEvrakRef(Evrak evrakRef) { 
		this.evrakRef = evrakRef; 
	} 
  
	public String getPath() { 
		return this.path; 
	} 
	 
	public void setPath(String path) { 
		this.path = path; 
	} 
  
	public String getTanim() { 
		return this.tanim; 
	} 
	 
	public void setTanim(String tanim) { 
		this.tanim = tanim; 
	} 
  
	
} 
