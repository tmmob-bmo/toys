package tr.com.arf.toys.db.filter.system; 
 
import java.util.ArrayList;
import java.util.List;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.framework.db.model.query.QueryObject;
import tr.com.arf.toys.utility.application.ApplicationConstant;
 
 
public class BankaFilter extends BaseFilter { 
	
private static final long serialVersionUID = 1L; 
	
	private String ad; 
	
	@Override 
	public void init() { 
		this.ad=null;
	} 
	 
	@Override 
	public List<ArrayList<QueryObject>> createQueryCriterias(){ 
		ArrayList<ArrayList<QueryObject>> criteriaList = new ArrayList<ArrayList<QueryObject>>(); 
		ArrayList<QueryObject> kriterler = new ArrayList<QueryObject>();	 
		ArrayList<QueryObject> opsiyonelKriterler = new ArrayList<QueryObject>(); 
		try { 	 
			if(validateQueryObject(getAd(), null, false)){ 
				kriterler.add(new QueryObject("ad", ApplicationConstant._beginsWith, getAd())); 
			} 			
		} catch(Exception e){ 
			System.out.println("Error @createQueryCriterias method, Class : BankaFilter :" + e.getMessage()); 
		}  
		criteriaList.add(kriterler); 
		criteriaList.add(opsiyonelKriterler); 
		return criteriaList; 
	}

	public String getAd() {
		return ad;
	}

	public void setAd(String ad) {
		this.ad = ad;
	} 
	 

} 
