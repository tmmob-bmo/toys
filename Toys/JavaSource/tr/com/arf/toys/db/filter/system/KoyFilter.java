package tr.com.arf.toys.db.filter.system; 
 
import java.util.ArrayList;
import java.util.List;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.framework.db.model.query.QueryObject;
import tr.com.arf.toys.db.enumerated.system.IdariYapiTip;
import tr.com.arf.toys.db.model.system.Ilce;
import tr.com.arf.toys.utility.application.ApplicationConstant;
 
 
public class KoyFilter extends BaseFilter { 
	
private static final long serialVersionUID = 1L; 
	
	private Ilce ilceRef; 
	private IdariYapiTip idariyapitipi;  
	private String name; 
	
	@Override 
	public void init() { 
		this.ilceRef = null; 	
		this.idariyapitipi = IdariYapiTip._NULL;	 
		this.name = null; 	
	} 
	 
	@Override 
	public List<ArrayList<QueryObject>> createQueryCriterias(){ 
		ArrayList<ArrayList<QueryObject>> criteriaList = new ArrayList<ArrayList<QueryObject>>(); 
		ArrayList<QueryObject> kriterler = new ArrayList<QueryObject>();	 
		ArrayList<QueryObject> opsiyonelKriterler = new ArrayList<QueryObject>(); 
		try { 	 
			if(validateQueryObject(getIlceRef(), null, false)){ 
				kriterler.add(new QueryObject("ilceRef", ApplicationConstant._equals, getIlceRef())); 
			} 
			if(validateQueryObject(getIdariyapitipi(), null, true)){ 
				kriterler.add(new QueryObject("idariyapitipi", ApplicationConstant._equals, getIdariyapitipi())); 
			}  
			if(validateQueryObject(getName(), null, false)){ 
				kriterler.add(new QueryObject("name", ApplicationConstant._beginsWith, getName())); 
			} 
		} catch(Exception e){ 
			System.out.println("Error @createQueryCriterias method, Class : KoyFilter :" + e.getMessage()); 
		}  
		criteriaList.add(kriterler); 
		criteriaList.add(opsiyonelKriterler); 
		return criteriaList; 
	} 
	 
	public Ilce getIlceRef() { 
		return this.ilceRef; 
	} 
	 
	public void setIlceRef(Ilce ilceRef) { 
		this.ilceRef = ilceRef; 
	} 
  
	public IdariYapiTip getIdariyapitipi() { 
		return this.idariyapitipi; 
	} 
	 
	public void setIdariyapitipi(IdariYapiTip idariyapitipi) { 
		this.idariyapitipi = idariyapitipi; 
	}  
  
	public String getName() { 
		return this.name; 
	} 
	 
	public void setName(String name) { 
		this.name = name; 
	} 
  
	
} 
