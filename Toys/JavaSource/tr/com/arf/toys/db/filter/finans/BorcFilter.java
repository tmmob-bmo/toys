package tr.com.arf.toys.db.filter.finans; 
 
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.framework.db.model.query.QueryObject;
import tr.com.arf.toys.db.enumerated.finans.BorcDurum;
import tr.com.arf.toys.db.model.egitim.Egitim;
import tr.com.arf.toys.db.model.kisi.Kisi;
import tr.com.arf.toys.db.model.system.Birim;
import tr.com.arf.toys.db.model.system.Odemetip;
import tr.com.arf.toys.utility.application.ApplicationConstant;
 
 
public class BorcFilter extends BaseFilter { 
	
private static final long serialVersionUID = 1L; 
	
	private Kisi kisiRef; 
	private Birim birimRef; 
	private Odemetip odemetipRef; 
	private java.util.Date tarih; 
	private java.util.Date tarihMax; 
	private BigDecimal miktar; 
	private BigDecimal odenen; 
	private BorcDurum borcdurum; 
	private String ad;
	private String soyad;
	private Egitim egitimRef;
	
	@Override 
	public void init() { 
		this.kisiRef = null; 	
		this.birimRef = null; 	
		this.odemetipRef = null; 	
		this.tarih = null; 	
		this.tarihMax = null; 	
		this.miktar = null; 	
		this.odenen = null; 	
		this.borcdurum = BorcDurum._NULL;	
		this.ad = null;
		this.soyad = null;
		this.egitimRef=null;
	} 
	 
	@Override 
	public List<ArrayList<QueryObject>> createQueryCriterias(){ 
		ArrayList<ArrayList<QueryObject>> criteriaList = new ArrayList<ArrayList<QueryObject>>(); 
		ArrayList<QueryObject> kriterler = new ArrayList<QueryObject>();	 
		ArrayList<QueryObject> opsiyonelKriterler = new ArrayList<QueryObject>(); 
		try { 	 
			if(validateQueryObject(getKisiRef(), null, false)){ 
				kriterler.add(new QueryObject("kisiRef", ApplicationConstant._equals, getKisiRef())); 
			} 
			if(validateQueryObject(getAd(), null, false)){ 
				kriterler.add(new QueryObject("kisiRef.ad", ApplicationConstant._beginsWith, getAd())); 
			}
			if(validateQueryObject(getSoyad(), null, false)){ 
				kriterler.add(new QueryObject("kisiRef.soyad", ApplicationConstant._beginsWith, getSoyad())); 
			} 
			if(validateQueryObject(getBirimRef(), null, false)){ 
				kriterler.add(new QueryObject("birimRef", ApplicationConstant._equals, getBirimRef())); 
			} 
			if(validateQueryObject(getOdemetipRef(), null, false)){ 
				kriterler.add(new QueryObject("odemetipRef", ApplicationConstant._equals, getOdemetipRef())); 
			} 
			if(validateQueryObject(getTarih(), null, false)  && validateQueryObject(getTarihMax(), null, false)){
				kriterler.add(new QueryObject("tarih", ApplicationConstant._equalsAndLargerThan, getTarih())); 
				kriterler.add(new QueryObject("tarih", ApplicationConstant._equalsAndSmallerThan, getTarihMax())); 
			} else if(validateQueryObject(getTarih(), null, false)){ 
				kriterler.add(new QueryObject("tarih", ApplicationConstant._equalsAndLargerThan, getTarih()));
			}
			if(validateQueryObject(getMiktar(), 0, false)){
				kriterler.add(new QueryObject("miktar", ApplicationConstant._equals, getMiktar())); 	
			}
			
			if(validateQueryObject(getOdenen(), 0, false)){
				kriterler.add(new QueryObject("odenen", ApplicationConstant._equals, getOdenen())); 	
			}
			if(validateQueryObject(getBorcdurum(), null, true)){ 
				kriterler.add(new QueryObject("borcdurum", ApplicationConstant._equals, getBorcdurum())); 
			} 
			if(validateQueryObject(getEgitimRef(), null, false)){ 
				kriterler.add(new QueryObject("egitimRef", ApplicationConstant._equals, getEgitimRef())); 
			} 
		} catch(Exception e){ 
			System.out.println("Error @createQueryCriterias method, Class : BorcFilter :" + e.getMessage()); 
		}  
		criteriaList.add(kriterler); 
		criteriaList.add(opsiyonelKriterler); 
		return criteriaList; 
	} 
	 
	public String getAd() {
		return ad;
	}

	public void setAd(String ad) {
		this.ad = ad;
	}

	public String getSoyad() {
		return soyad;
	}

	public void setSoyad(String soyad) {
		this.soyad = soyad;
	}

	public Kisi getKisiRef() { 
		return this.kisiRef; 
	} 
	 
	public void setKisiRef(Kisi kisiRef) { 
		this.kisiRef = kisiRef; 
	} 
  
	public Birim getBirimRef() { 
		return this.birimRef; 
	} 
	 
	public void setBirimRef(Birim birimRef) { 
		this.birimRef = birimRef; 
	} 
  
	public Odemetip getOdemetipRef() { 
		return this.odemetipRef; 
	} 
	 
	public void setOdemetipRef(Odemetip odemetipRef) { 
		this.odemetipRef = odemetipRef; 
	} 
  
	public Date getTarih() { 
		return this.tarih; 
	} 
	 
	public void setTarih(Date tarih) { 
		this.tarih = tarih; 
	} 
  
	public Date getTarihMax() { 
		return this.tarihMax; 
	} 
	 
	public void setTarihMax(Date tarihMax) { 
		this.tarihMax = tarihMax; 
	} 
  
	public BigDecimal getMiktar() { 
		return this.miktar; 
	} 
	 
	public void setMiktar(BigDecimal miktar) { 
		this.miktar = miktar; 
	} 
  
	public BigDecimal getOdenen() { 
		return this.odenen; 
	} 
	 
	public void setOdenen(BigDecimal odenen) { 
		this.odenen = odenen; 
	} 
  
	public BorcDurum getBorcdurum() { 
		return this.borcdurum; 
	} 
	 
	public void setBorcdurum(BorcDurum borcdurum) { 
		this.borcdurum = borcdurum; 
	}

	public Egitim getEgitimRef() {
		return egitimRef;
	}

	public void setEgitimRef(Egitim egitimRef) {
		this.egitimRef = egitimRef;
	} 
} 
