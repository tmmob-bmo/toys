package tr.com.arf.toys.db.filter.kisi; 
 
import java.util.ArrayList;
import java.util.List;

import tr.com.arf.framework.db.enumerated._common.EvetHayir;
import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.framework.db.model.query.QueryObject;
import tr.com.arf.toys.db.model.kisi.Kurum;
import tr.com.arf.toys.db.model.uye.Uye;
import tr.com.arf.toys.utility.application.ApplicationConstant;
 
 
public class IsYeriTemsilcileriFilter extends BaseFilter { 
	
private static final long serialVersionUID = 1L; 
	
	private Kurum kurumRef; 
	private Uye uyeRef;
	private EvetHayir aktif; 
	
	@Override 
	public void init() { 
		this.kurumRef=null;
		this.uyeRef=null;
		this.aktif=EvetHayir._NULL;
	} 
	 
	@Override 
	public List<ArrayList<QueryObject>> createQueryCriterias(){ 
		ArrayList<ArrayList<QueryObject>> criteriaList = new ArrayList<ArrayList<QueryObject>>(); 
		ArrayList<QueryObject> kriterler = new ArrayList<QueryObject>();	 
		ArrayList<QueryObject> opsiyonelKriterler = new ArrayList<QueryObject>(); 
		try { 	 
			if(validateQueryObject(getKurumRef(), null, false)){ 
				kriterler.add(new QueryObject("kurumRef", ApplicationConstant._equals, getKurumRef())); 
			} 
			if(validateQueryObject(getUyeRef(), null, false)){ 
				kriterler.add(new QueryObject("uyeRef", ApplicationConstant._equals, getUyeRef())); 
			} 
			if(validateQueryObject(getAktif(), null, true)){ 
				kriterler.add(new QueryObject("aktif", ApplicationConstant._equals, getAktif())); 
			} 
		} catch(Exception e){ 
			System.out.println("Error @createQueryCriterias method, Class : IsYeriTemsilcileriFilter :" + e.getMessage()); 
		}  
		criteriaList.add(kriterler); 
		criteriaList.add(opsiyonelKriterler); 
		return criteriaList; 
	}

	public Kurum getKurumRef() {
		return kurumRef;
	}

	public void setKurumRef(Kurum kurumRef) {
		this.kurumRef = kurumRef;
	}

	public Uye getUyeRef() {
		return uyeRef;
	}

	public void setUyeRef(Uye uyeRef) {
		this.uyeRef = uyeRef;
	}

	public EvetHayir getAktif() {
		return aktif;
	}

	public void setAktif(EvetHayir aktif) {
		this.aktif = aktif;
	}

	 
} 
