package tr.com.arf.toys.db.filter.system; 
 
import java.util.ArrayList;
import java.util.List;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.framework.db.model.query.QueryObject;
import tr.com.arf.toys.db.enumerated.system.UniversiteDurum;
import tr.com.arf.toys.db.model.system.Sehir;
import tr.com.arf.toys.db.model.system.Ulke;
import tr.com.arf.toys.utility.application.ApplicationConstant;
 
 
public class UniversiteFilter extends BaseFilter { 
	
private static final long serialVersionUID = 1L; 
	
	private String ad;
	private String webadresi;
	private UniversiteDurum durum;
	private Ulke ulkeRef;
	private Sehir ilRef;
	
	@Override 
	public void init() { 
		this.ad=null;
		this.webadresi=null;
		this.durum=UniversiteDurum._NULL;
		this.ulkeRef=null;
		this.ilRef=null;
	} 
	 
	@Override 
	public List<ArrayList<QueryObject>> createQueryCriterias(){ 
		ArrayList<ArrayList<QueryObject>> criteriaList = new ArrayList<ArrayList<QueryObject>>(); 
		ArrayList<QueryObject> kriterler = new ArrayList<QueryObject>();	 
		ArrayList<QueryObject> opsiyonelKriterler = new ArrayList<QueryObject>(); 
		try { 	 
			
			if(validateQueryObject(getAd(), null, false)){ 
				kriterler.add(new QueryObject("ad", ApplicationConstant._beginsWith, getAd())); 
			} 
			
			if(validateQueryObject(getWebadresi(), null, false)){ 
				kriterler.add(new QueryObject("webadresi", ApplicationConstant._beginsWith, getWebadresi())); 
			} 
			
			if(validateQueryObject(getDurum(), null, true)){ 
				kriterler.add(new QueryObject("durum", ApplicationConstant._equals, getDurum())); 
			} 
			
			if(validateQueryObject(getUlkeRef(), null, false)){ 
				kriterler.add(new QueryObject("ulkeRef", ApplicationConstant._equals, getUlkeRef())); 
			}
			
			if(validateQueryObject(getIlRef(), null, false)){ 
				kriterler.add(new QueryObject("ilRef", ApplicationConstant._equals, getIlRef())); 
			}
			
		} catch(Exception e){ 
			System.out.println("Error @createQueryCriterias method, Class : UniversiteFilter :" + e.getMessage()); 
		}  
		criteriaList.add(kriterler); 
		criteriaList.add(opsiyonelKriterler); 
		return criteriaList; 
	}

	public String getAd() {
		return ad;
	}

	public void setAd(String ad) {
		this.ad = ad;
	}

	public String getWebadresi() {
		return webadresi;
	}

	public void setWebadresi(String webadresi) {
		this.webadresi = webadresi;
	}

	public UniversiteDurum getDurum() {
		return durum;
	}

	public void setDurum(UniversiteDurum durum) {
		this.durum = durum;
	}

	public Ulke getUlkeRef() {
		return ulkeRef;
	}

	public void setUlkeRef(Ulke ulkeRef) {
		this.ulkeRef = ulkeRef;
	}

	public Sehir getIlRef() {
		return ilRef;
	}

	public void setIlRef(Sehir ilRef) {
		this.ilRef = ilRef;
	} 
	
	
} 
