package tr.com.arf.toys.db.filter.kisi; 
 
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.framework.db.model.query.QueryObject;
import tr.com.arf.toys.db.enumerated.kisi.KisiIzinDurum;
import tr.com.arf.toys.db.model.evrak.Evrak;
import tr.com.arf.toys.db.model.kisi.Kisi;
import tr.com.arf.toys.utility.application.ApplicationConstant;
 
 
public class KisiizinFilter extends BaseFilter { 
	
private static final long serialVersionUID = 1L; 
	
	private Kisi kisiRef; 
	
	private Evrak evrakRef; 
	private java.util.Date baslangictarih; 
	private java.util.Date baslangictarihMax; 
	private java.util.Date bitistarih; 
	private java.util.Date bitistarihMax; 
	private KisiIzinDurum kisiizindurum; 
	private String aciklama; 
	
	@Override 
	public void init() { 
		this.kisiRef = null; 	
		this.evrakRef = null; 	
		this.baslangictarih = null; 	
		this.baslangictarihMax = null; 	
		this.bitistarih = null; 	
		this.bitistarihMax = null; 	
		this.kisiizindurum = KisiIzinDurum._NULL;	
		this.aciklama = null; 	
	} 
	 
	@Override 
	public List<ArrayList<QueryObject>> createQueryCriterias(){ 
		ArrayList<ArrayList<QueryObject>> criteriaList = new ArrayList<ArrayList<QueryObject>>(); 
		ArrayList<QueryObject> kriterler = new ArrayList<QueryObject>();	 
		ArrayList<QueryObject> opsiyonelKriterler = new ArrayList<QueryObject>(); 
		try { 	 
			if(validateQueryObject(getKisiRef(), null, false)){ 
				kriterler.add(new QueryObject("kisiRef", ApplicationConstant._equals, getKisiRef())); 
			} 
			if(validateQueryObject(getEvrakRef(), null, false)){ 
				kriterler.add(new QueryObject("evrakRef", ApplicationConstant._equals, getEvrakRef())); 
			} 
			if(validateQueryObject(getBaslangictarih(), null, false)  && validateQueryObject(getBaslangictarihMax(), null, false)){
				kriterler.add(new QueryObject("baslangictarih", ApplicationConstant._equalsAndLargerThan, getBaslangictarih())); 
				kriterler.add(new QueryObject("baslangictarih", ApplicationConstant._equalsAndSmallerThan, getBaslangictarihMax())); 
			} else if(validateQueryObject(getBaslangictarih(), null, false)){ 
				kriterler.add(new QueryObject("baslangictarih", ApplicationConstant._equalsAndLargerThan, getBaslangictarih()));
			}
			if(validateQueryObject(getBitistarih(), null, false)  && validateQueryObject(getBitistarihMax(), null, false)){
				kriterler.add(new QueryObject("bitistarih", ApplicationConstant._equalsAndLargerThan, getBitistarih())); 
				kriterler.add(new QueryObject("bitistarih", ApplicationConstant._equalsAndSmallerThan, getBitistarihMax())); 
			} else if(validateQueryObject(getBitistarih(), null, false)){ 
				kriterler.add(new QueryObject("bitistarih", ApplicationConstant._equalsAndLargerThan, getBitistarih()));
			}
			if(validateQueryObject(getKisiizindurum(), null, true)){ 
				kriterler.add(new QueryObject("kisiizindurum", ApplicationConstant._equals, getKisiizindurum())); 
			} 
			if(validateQueryObject(getAciklama(), null, false)){ 
				kriterler.add(new QueryObject("aciklama", ApplicationConstant._beginsWith, getAciklama())); 
			} 
		} catch(Exception e){ 
			System.out.println("Error @createQueryCriterias method, Class : KisiizinFilter :" + e.getMessage()); 
		}  
		criteriaList.add(kriterler); 
		criteriaList.add(opsiyonelKriterler); 
		return criteriaList; 
	} 
	 
	public Kisi getKisiRef() { 
		return this.kisiRef; 
	} 
	 
	public void setKisiRef(Kisi kisiRef) { 
		this.kisiRef = kisiRef; 
	} 
  
	public Evrak getEvrakRef() { 
		return this.evrakRef; 
	} 
	 
	public void setEvrakRef(Evrak evrakRef) { 
		this.evrakRef = evrakRef; 
	} 
  
	public Date getBaslangictarih() { 
		return this.baslangictarih; 
	} 
	 
	public void setBaslangictarih(Date baslangictarih) { 
		this.baslangictarih = baslangictarih; 
	} 
  
	public Date getBaslangictarihMax() { 
		return this.baslangictarihMax; 
	} 
	 
	public void setBaslangictarihMax(Date baslangictarihMax) { 
		this.baslangictarihMax = baslangictarihMax; 
	} 
  
	public Date getBitistarih() { 
		return this.bitistarih; 
	} 
	 
	public void setBitistarih(Date bitistarih) { 
		this.bitistarih = bitistarih; 
	} 
  
	public Date getBitistarihMax() { 
		return this.bitistarihMax; 
	} 
	 
	public void setBitistarihMax(Date bitistarihMax) { 
		this.bitistarihMax = bitistarihMax; 
	} 
  
	public KisiIzinDurum getKisiizindurum() { 
		return this.kisiizindurum; 
	} 
	 
	public void setKisiizindurum(KisiIzinDurum kisiizindurum) { 
		this.kisiizindurum = kisiizindurum; 
	} 
  
	public String getAciklama() { 
		return this.aciklama; 
	} 
	 
	public void setAciklama(String aciklama) { 
		this.aciklama = aciklama; 
	} 
  
	
} 
