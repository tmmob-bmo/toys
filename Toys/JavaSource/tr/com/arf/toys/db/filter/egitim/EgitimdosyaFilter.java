package tr.com.arf.toys.db.filter.egitim; 
 
import java.util.ArrayList;
import java.util.List;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.framework.db.model.query.QueryObject;
import tr.com.arf.toys.db.model.egitim.Egitim;
import tr.com.arf.toys.db.model.egitim.Egitimtanim;
import tr.com.arf.toys.utility.application.ApplicationConstant;
 
 
public class EgitimdosyaFilter extends BaseFilter { 
	
private static final long serialVersionUID = 1L; 
	
	private Egitimtanim egitimtanimRef;
	private Egitim egitimRef;
	private Long rID; 
	private String ad; 
	private String path; 
	
	@Override 
	public void init() { 
		this.rID = null; 	
		this.ad = null; 	
		this.path = null; 
		this.egitimtanimRef=null;
		this.egitimRef=null;
	} 
	 
	@Override 
	public List<ArrayList<QueryObject>> createQueryCriterias(){ 
		ArrayList<ArrayList<QueryObject>> criteriaList = new ArrayList<ArrayList<QueryObject>>(); 
		ArrayList<QueryObject> kriterler = new ArrayList<QueryObject>();	 
		ArrayList<QueryObject> opsiyonelKriterler = new ArrayList<QueryObject>(); 
		try { 	 
			if(validateQueryObject(getRID(), null, false)){ 
				kriterler.add(new QueryObject("rID", ApplicationConstant._equals, getRID())); 
			} 
			if(validateQueryObject(getEgitimtanimRef(), null, false)){ 
				kriterler.add(new QueryObject("egitimtanimRef", ApplicationConstant._equals, getEgitimtanimRef())); 
			}
			if(validateQueryObject(getEgitimRef(), null, false)){ 
				kriterler.add(new QueryObject("egitimRef", ApplicationConstant._equals, getEgitimRef())); 
			}
			if(validateQueryObject(getAd(), null, false)){ 
				kriterler.add(new QueryObject("ad", ApplicationConstant._beginsWith, getAd(), false)); 
			} 
			if(validateQueryObject(getPath(), null, false)){ 
				kriterler.add(new QueryObject("path", ApplicationConstant._beginsWith, getPath(), false)); 
			} 
		} catch(Exception e){ 
			System.out.println("Error @createQueryCriterias method, Class : EgitimdosyaFilter :" + e.getMessage()); 
		}  
		criteriaList.add(kriterler); 
		criteriaList.add(opsiyonelKriterler); 
		return criteriaList; 
	} 
	 
	public Long getRID() { 
		return this.rID; 
	} 
	 
	public void setRID(Long rID) { 
		this.rID = rID; 
	} 
  
	public String getAd() { 
		return this.ad; 
	} 
	 
	public void setAd(String ad) { 
		this.ad = ad; 
	} 
  
	public String getPath() { 
		return this.path; 
	} 
	 
	public void setPath(String path) { 
		this.path = path; 
	}

	public Egitimtanim getEgitimtanimRef() {
		return egitimtanimRef;
	}

	public void setEgitimtanimRef(Egitimtanim egitimtanimRef) {
		this.egitimtanimRef = egitimtanimRef;
	}

	public Egitim getEgitimRef() {
		return egitimRef;
	}

	public void setEgitimRef(Egitim egitimRef) {
		this.egitimRef = egitimRef;
	} 
} 
