package tr.com.arf.toys.db.filter.egitim; 
 
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.framework.db.model.query.QueryObject;
import tr.com.arf.toys.db.enumerated.egitim.SonucTuru;
import tr.com.arf.toys.db.enumerated.kisi.KisiTuru;
import tr.com.arf.toys.db.model.egitim.Sinav;
import tr.com.arf.toys.db.model.kisi.Kisi;
import tr.com.arf.toys.utility.application.ApplicationConstant;
 
 
public class SinavKatilimciFilter extends BaseFilter { 
	
private static final long serialVersionUID = 1L; 
	
	private Long rID; 
	private Kisi kisiRef; 
	private Sinav sinavRef;
	private String aciklama;
	private KisiTuru kisituru;
	private SonucTuru sonuc;
	private BigDecimal teorikpuan; 
	private BigDecimal toplampuan; 
	private String projeuygulama; 
	
	@Override 
	public void init() { 
		this.rID = null; 	
		this.kisiRef=null;
		this.sinavRef=null;
		this.aciklama=null;
		this.kisituru=KisiTuru._NULL;
		this.projeuygulama=null;
		this.sinavRef=null;
		this.sonuc=SonucTuru._NULL;
		this.teorikpuan=null;
		this.toplampuan=null;
	} 
	 
	@Override 
	public List<ArrayList<QueryObject>> createQueryCriterias(){ 
		ArrayList<ArrayList<QueryObject>> criteriaList = new ArrayList<ArrayList<QueryObject>>(); 
		ArrayList<QueryObject> kriterler = new ArrayList<QueryObject>();	 
		ArrayList<QueryObject> opsiyonelKriterler = new ArrayList<QueryObject>(); 
		try { 	 
			if(validateQueryObject(getRID(), null, false)){ 
				kriterler.add(new QueryObject("rID", ApplicationConstant._equals, getRID())); 
			} 
			if(validateQueryObject(getKisiRef(), null, false)){ 
				kriterler.add(new QueryObject("kisiRef", ApplicationConstant._equals, getKisiRef())); 
			} 
			if(validateQueryObject(getSinavRef(), null, false)){ 
				kriterler.add(new QueryObject("sinavRef", ApplicationConstant._equals, getSinavRef())); 
			} 
			if(validateQueryObject(getAciklama(), null, false)){ 
				kriterler.add(new QueryObject("aciklama", ApplicationConstant._inside, getAciklama())); 
			} 
			if(validateQueryObject(getKisituru(), null, false)){ 
				kriterler.add(new QueryObject("kisituru", ApplicationConstant._equals, getKisituru())); 
			}
			if(validateQueryObject(getProjeuygulama(), null, false)){ 
				kriterler.add(new QueryObject("projeuygulama", ApplicationConstant._equals, getProjeuygulama())); 
			}
			if(validateQueryObject(getSonuc(), null, false)){ 
				kriterler.add(new QueryObject("sonuc", ApplicationConstant._equals, getSonuc())); 
			}
			if(validateQueryObject(getTeorikpuan(), null, false)){ 
				kriterler.add(new QueryObject("teorikpuan", ApplicationConstant._inside, getTeorikpuan())); 
			}
			if(validateQueryObject(getToplampuan(), null, false)){ 
				kriterler.add(new QueryObject("toplampuan", ApplicationConstant._equals, getToplampuan())); 
			}
		} catch(Exception e){ 
			System.out.println("Error @createQueryCriterias method, Class : SinavKatilimciFilter :" + e.getMessage()); 
		}  
		criteriaList.add(kriterler); 
		criteriaList.add(opsiyonelKriterler); 
		return criteriaList; 
	} 
	 
	public Long getRID() { 
		return this.rID; 
	} 
	 
	public void setRID(Long rID) { 
		this.rID = rID; 
	}

	public Kisi getKisiRef() {
		return kisiRef;
	}

	public void setKisiRef(Kisi kisiRef) {
		this.kisiRef = kisiRef;
	}

	public Sinav getSinavRef() {
		return sinavRef;
	}

	public void setSinavRef(Sinav sinavRef) {
		this.sinavRef = sinavRef;
	}

	public String getAciklama() {
		return aciklama;
	}

	public void setAciklama(String aciklama) {
		this.aciklama = aciklama;
	}

	public KisiTuru getKisituru() {
		return kisituru;
	}

	public void setKisituru(KisiTuru kisituru) {
		this.kisituru = kisituru;
	}

	public SonucTuru getSonuc() {
		return sonuc;
	}

	public void setSonuc(SonucTuru sonuc) {
		this.sonuc = sonuc;
	}

	public BigDecimal getTeorikpuan() {
		return teorikpuan;
	}

	public void setTeorikpuan(BigDecimal teorikpuan) {
		this.teorikpuan = teorikpuan;
	}

	public BigDecimal getToplampuan() {
		return toplampuan;
	}

	public void setToplampuan(BigDecimal toplampuan) {
		this.toplampuan = toplampuan;
	}

	public String getProjeuygulama() {
		return projeuygulama;
	}

	public void setProjeuygulama(String projeuygulama) {
		this.projeuygulama = projeuygulama;
	}


} 
