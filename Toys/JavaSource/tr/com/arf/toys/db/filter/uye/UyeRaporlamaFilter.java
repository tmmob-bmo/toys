package tr.com.arf.toys.db.filter.uye; 
 
import java.math.BigDecimal;
import java.util.ArrayList;

import tr.com.arf.framework.db.enumerated._common.EvetHayir;
import tr.com.arf.framework.db.enumerated._common.VarYokTumu;
import tr.com.arf.toys.db.enumerated.kisi.Cinsiyet;
import tr.com.arf.toys.db.enumerated.uye.OgrenimTip;
import tr.com.arf.toys.db.enumerated.uye.SigortaTip;
import tr.com.arf.toys.db.enumerated.uye.UyeDurum;
import tr.com.arf.toys.db.enumerated.uye.UyeKayitDurum;
import tr.com.arf.toys.db.enumerated.uye.UyeMuafiyetTip;
import tr.com.arf.toys.db.enumerated.uye.UyeRaporlamaAboneDurum;
import tr.com.arf.toys.db.enumerated.uye.UyeTip;
import tr.com.arf.toys.db.model.kisi.Kurum;
import tr.com.arf.toys.db.model.system.Birim;
import tr.com.arf.toys.db.model.system.Cezatip;
import tr.com.arf.toys.db.model.system.Kullanicirapor;
import tr.com.arf.toys.db.model.system.Odultip;
import tr.com.arf.toys.db.model.system.Sehir;
import tr.com.arf.toys.db.model.system.Uzmanliktip;
 
 
public class UyeRaporlamaFilter { 
	 	
	private UyeTip uyeTip;
	private UyeDurum uyeDurum;
	private UyeKayitDurum uyeKayitDurum;
	private java.util.Date uyeliktarih; 
	private java.util.Date uyeliktarihMax; 
	private java.util.Date dogumtarih; 
	private java.util.Date dogumtarihMax; 
	private java.util.Date ayrilmatarih; 
	private java.util.Date ayrilmatarihMax; 
	private java.util.Date onaytarih; 
	private java.util.Date onaytarihMax; 
	private EvetHayir ogrenciDurum;
	private UyeRaporlamaAboneDurum abonelikDurum;
	
	private BigDecimal uyeAidatBorcMin;
	private BigDecimal uyeAidatBorcMax;
	private Boolean uyeAidatBorcYok;
	private Boolean telefonYok;
	private Boolean epostaYok;
	
	private Uzmanliktip[] uyeUzmanlikList;
	private VarYokTumu uyeUzmanlikVarYok;
	
	private Cezatip[] uyeCezaList;
	private Object[] universiteList;
	private Object[] bolumList;
	private VarYokTumu uyeCezaVarYok;
	
	private Birim secilenBirim; 
	private Kurum secilenKurum;
	private ArrayList<Kurum> kurumListesi;
	
	private UyeMuafiyetTip[] uyeMuafiyetList;
	private VarYokTumu uyeMuafiyetVarYok;	
	
	private Odultip[] uyeOdulList;
	private VarYokTumu uyeOdulVarYok;	
	
	private OgrenimTip[] kisiOgrenimList;
	private VarYokTumu kisiOgrenimVarYok;	
	
	private SigortaTip[] uyeSigortaList;
	private VarYokTumu uyeSigortaVarYok;
	private Cinsiyet cinsiyet;
	private Sehir yasadigiIl;
	private Sehir calistigiIl;
	private OgrenimTip ogrenimtip;
	private Integer mezuniyettarihi; 
	
	private Boolean sicilnoyok;
	private Boolean adyok; 
	private Boolean soyadyok;
	private Boolean birimyok; 
	private Boolean uyeliktarihiyok;
	private Boolean aidatborcuyok;
	private Boolean uyedurumyok;
	private Boolean uyetipiyok;
	private Boolean isyeriyok;
	private Boolean evadresiyok;
	private Boolean yasadigiilyok;
	private Boolean isadresiyok;
	private Boolean calistigiilyok;
	private Boolean gsmyok;
	private Boolean postayok;
	private Boolean mouniversiteyok;
	private Boolean mobolumyok;
	private Boolean mezuniyetyiliyok;
	private Boolean ogrenimtipiyok;
	private Boolean cinsiyetyok;
	private Boolean dogumtarihiyok;
	
	private Kullanicirapor kullanicirapor;
	private String kurumadi;
	
	public UyeRaporlamaFilter(){
		 super();
		 init();
	 }
	 
	public void init() {  
		this.uyeliktarih = null; 	
		this.uyeliktarihMax = null; 	
		this.ayrilmatarih = null; 	
		this.ayrilmatarihMax = null; 
//		this.uyeTipList = new UyeTip[0];
		this.uyeTip=UyeTip._NULL;
		this.uyeDurum=UyeDurum._NULL;
		this.uyeKayitDurum=UyeKayitDurum._NULL;
		this.uyeAidatBorcYok = false;
		this.uyeAidatBorcMin = new BigDecimal(0);
		this.uyeAidatBorcMax = new BigDecimal(0);
		this.uyeUzmanlikList = new Uzmanliktip[0];
		this.uyeUzmanlikVarYok = VarYokTumu._TUMU;
		this.ogrenciDurum = EvetHayir._NULL;
		this.uyeCezaList = new Cezatip[0];
		this.uyeCezaVarYok = VarYokTumu._TUMU;
		this.kurumListesi = new  ArrayList<Kurum>();
		this.secilenKurum = null;
		this.uyeMuafiyetList = new UyeMuafiyetTip[0];
		this.uyeMuafiyetVarYok = VarYokTumu._TUMU;
		this.uyeOdulList = new Odultip[0];
		this.uyeOdulVarYok = VarYokTumu._TUMU;
		this.kisiOgrenimList = new OgrenimTip[0];
		this.kisiOgrenimVarYok = VarYokTumu._TUMU; 
		this.uyeSigortaList = new SigortaTip[0];
		this.uyeSigortaVarYok = VarYokTumu._TUMU;
		this.secilenBirim=null;
		this.cinsiyet=Cinsiyet._NULL;
		this.dogumtarih=null;
		this.dogumtarihMax=null;
		this.yasadigiIl=null;
		this.calistigiIl=null;
		this.telefonYok=false;
		this.epostaYok=false;
		this.ogrenimtip=OgrenimTip._NULL;
		this.mezuniyettarihi=0;
		this.universiteList=new Object[0];
		this.bolumList=new Object[0];
		this.sicilnoyok=false;
		this.adyok=false;
		this.soyadyok=false;
		this.birimyok=false;
		this.uyeliktarihiyok=false;
		this.aidatborcuyok=false;
		this.uyedurumyok=false;
		this.uyetipiyok=false;
		this.isadresiyok=false;
		this.isyeriyok=false;
		this.evadresiyok=false;
		this.yasadigiilyok=false;
		this.calistigiilyok=false;
		this.gsmyok=false;
		this.postayok=false;
		this.mobolumyok=false;
		this.mouniversiteyok=false;
		this.mezuniyetyiliyok=false;
		this.ogrenimtipiyok=false;
		this.cinsiyetyok=false;
		this.dogumtarihiyok=false;
		this.kullanicirapor=null;
		this.kurumadi=null;
	}
	
	public java.util.Date getUyeliktarih() {
		return uyeliktarih;
	}

	public void setUyeliktarih(java.util.Date uyeliktarih) {
		this.uyeliktarih = uyeliktarih;
	}

	public java.util.Date getUyeliktarihMax() {
		return uyeliktarihMax;
	}

	public void setUyeliktarihMax(java.util.Date uyeliktarihMax) {
		this.uyeliktarihMax = uyeliktarihMax;
	}

	public java.util.Date getAyrilmatarih() {
		return ayrilmatarih;
	}

	public void setAyrilmatarih(java.util.Date ayrilmatarih) {
		this.ayrilmatarih = ayrilmatarih;
	}

	public java.util.Date getAyrilmatarihMax() {
		return ayrilmatarihMax;
	}

	public void setAyrilmatarihMax(java.util.Date ayrilmatarihMax) {
		this.ayrilmatarihMax = ayrilmatarihMax;
	} 
	 
	public UyeTip getUyeTip() {
		return uyeTip;
	}

	public void setUyeTip(UyeTip uyeTip) {
		this.uyeTip = uyeTip;
	}

	public UyeDurum getUyeDurum() {
		return uyeDurum;
	}

	public void setUyeDurum(UyeDurum uyeDurum) {
		this.uyeDurum = uyeDurum;
	}

	public UyeKayitDurum getUyeKayitDurum() {
		return uyeKayitDurum;
	}

	public void setUyeKayitDurum(UyeKayitDurum uyeKayitDurum) {
		this.uyeKayitDurum = uyeKayitDurum;
	}

	public EvetHayir getOgrenciDurum() {
		return ogrenciDurum;
	}

	public void setOgrenciDurum(EvetHayir ogrenciDurum) {
		this.ogrenciDurum = ogrenciDurum;
	}

	public java.util.Date getOnaytarih() {
		return onaytarih;
	}

	public void setOnaytarih(java.util.Date onaytarih) {
		this.onaytarih = onaytarih;
	}

	public java.util.Date getOnaytarihMax() {
		return onaytarihMax;
	}

	public void setOnaytarihMax(java.util.Date onaytarihMax) {
		this.onaytarihMax = onaytarihMax;
	}

	public UyeRaporlamaAboneDurum getAbonelikDurum() {
		return abonelikDurum;
	}

	public void setAbonelikDurum(UyeRaporlamaAboneDurum abonelikDurum) {
		this.abonelikDurum = abonelikDurum;
	}

	public BigDecimal getUyeAidatBorcMin() {
		return uyeAidatBorcMin;
	}

	public void setUyeAidatBorcMin(BigDecimal uyeAidatBorcMin) {
		this.uyeAidatBorcMin = uyeAidatBorcMin;
	}

	public BigDecimal getUyeAidatBorcMax() {
		return uyeAidatBorcMax;
	}

	public void setUyeAidatBorcMax(BigDecimal uyeAidatBorcMax) {
		this.uyeAidatBorcMax = uyeAidatBorcMax;
	}

	public VarYokTumu getUyeUzmanlikVarYok() {
		return uyeUzmanlikVarYok;
	}

	public void setUyeUzmanlikVarYok(VarYokTumu uyeUzmanlikVarYok) {
		this.uyeUzmanlikVarYok = uyeUzmanlikVarYok;
	}

	public Uzmanliktip[] getUyeUzmanlikList() {
		return uyeUzmanlikList;
	}

	public void setUyeUzmanlikList(Uzmanliktip[] uyeUzmanlikList) {
		this.uyeUzmanlikList = uyeUzmanlikList;
	}

	public Cezatip[] getUyeCezaList() {
		return uyeCezaList;
	}

	public void setUyeCezaList(Cezatip[] uyeCezaList) {
		this.uyeCezaList = uyeCezaList;
	}

	public VarYokTumu getUyeCezaVarYok() {
		return uyeCezaVarYok;
	}

	public void setUyeCezaVarYok(VarYokTumu uyeCezaVarYok) {
		this.uyeCezaVarYok = uyeCezaVarYok;
	}

	public Kurum getSecilenKurum() {
		return secilenKurum;
	}

	public void setSecilenKurum(Kurum secilenKurum) {
		this.secilenKurum = secilenKurum;
	}

	public ArrayList<Kurum> getKurumListesi() {
		return kurumListesi;
	}

	public void setKurumListesi(ArrayList<Kurum> kurumListesi) {
		this.kurumListesi = kurumListesi;
	}

	public UyeMuafiyetTip[] getUyeMuafiyetList() {
		return uyeMuafiyetList;
	}

	public void setUyeMuafiyetList(UyeMuafiyetTip[] uyeMuafiyetList) {
		this.uyeMuafiyetList = uyeMuafiyetList;
	}

	public VarYokTumu getUyeMuafiyetVarYok() {
		return uyeMuafiyetVarYok;
	}

	public void setUyeMuafiyetVarYok(VarYokTumu uyeMuafiyetVarYok) {
		this.uyeMuafiyetVarYok = uyeMuafiyetVarYok;
	}

	public Odultip[] getUyeOdulList() {
		return uyeOdulList;
	}

	public void setUyeOdulList(Odultip[] uyeOdulList) {
		this.uyeOdulList = uyeOdulList;
	}

	public VarYokTumu getUyeOdulVarYok() {
		return uyeOdulVarYok;
	}

	public void setUyeOdulVarYok(VarYokTumu uyeOdulVarYok) {
		this.uyeOdulVarYok = uyeOdulVarYok;
	}

	public VarYokTumu getKisiOgrenimVarYok() {
		return kisiOgrenimVarYok;
	}

	public void setKisiOgrenimVarYok(VarYokTumu kisiOgrenimVarYok) {
		this.kisiOgrenimVarYok = kisiOgrenimVarYok;
	}

	public SigortaTip[] getUyeSigortaList() {
		return uyeSigortaList;
	}

	public void setUyeSigortaList(SigortaTip[] uyeSigortaList) {
		this.uyeSigortaList = uyeSigortaList;
	}

	public VarYokTumu getUyeSigortaVarYok() {
		return uyeSigortaVarYok;
	}

	public void setUyeSigortaVarYok(VarYokTumu uyeSigortaVarYok) {
		this.uyeSigortaVarYok = uyeSigortaVarYok;
	}

	public Boolean getUyeAidatBorcYok() {
		return uyeAidatBorcYok;
	}

	public void setUyeAidatBorcYok(Boolean uyeAidatBorcYok) {
		this.uyeAidatBorcYok = uyeAidatBorcYok;
	}

	public Birim getSecilenBirim() {
		return secilenBirim;
	}

	public void setSecilenBirim(Birim secilenBirim) {
		this.secilenBirim = secilenBirim;
	}

	public Cinsiyet getCinsiyet() {
		return cinsiyet;
	}

	public void setCinsiyet(Cinsiyet cinsiyet) {
		this.cinsiyet = cinsiyet;
	}

	public java.util.Date getDogumtarih() {
		return dogumtarih;
	}

	public void setDogumtarih(java.util.Date dogumtarih) {
		this.dogumtarih = dogumtarih;
	}

	public java.util.Date getDogumtarihMax() {
		return dogumtarihMax;
	}

	public void setDogumtarihMax(java.util.Date dogumtarihMax) {
		this.dogumtarihMax = dogumtarihMax;
	}

	public Sehir getYasadigiIl() {
		return yasadigiIl;
	}

	public void setYasadigiIl(Sehir yasadigiIl) {
		this.yasadigiIl = yasadigiIl;
	}

	public Sehir getCalistigiIl() {
		return calistigiIl;
	}

	public void setCalistigiIl(Sehir calistigiIl) {
		this.calistigiIl = calistigiIl;
	}

	public Boolean getTelefonYok() {
		return telefonYok;
	}

	public void setTelefonYok(Boolean telefonYok) {
		this.telefonYok = telefonYok;
	}

	public Boolean getEpostaYok() {
		return epostaYok;
	}

	public void setEpostaYok(Boolean epostaYok) {
		this.epostaYok = epostaYok;
	}

	public OgrenimTip getOgrenimtip() {
		return ogrenimtip;
	}

	public void setOgrenimtip(OgrenimTip ogrenimtip) {
		this.ogrenimtip = ogrenimtip;
	}

	public Integer getMezuniyettarihi() {
		return mezuniyettarihi;
	}

	public void setMezuniyettarihi(Integer mezuniyettarihi) {
		this.mezuniyettarihi = mezuniyettarihi;
	}

	public Object[] getUniversiteList() {
		return universiteList;
	}

	public void setUniversiteList(Object[] universiteList) {
		this.universiteList = universiteList;
	}

	public Object[] getBolumList() {
		return bolumList;
	}

	public void setBolumList(Object[] bolumList) {
		this.bolumList = bolumList;
	}

	public Boolean getSicilnoyok() {
		return sicilnoyok;
	}

	public void setSicilnoyok(Boolean sicilnoyok) {
		this.sicilnoyok = sicilnoyok;
	}

	public Boolean getAdyok() {
		return adyok;
	}

	public void setAdyok(Boolean adyok) {
		this.adyok = adyok;
	}

	public Boolean getSoyadyok() {
		return soyadyok;
	}

	public void setSoyadyok(Boolean soyadyok) {
		this.soyadyok = soyadyok;
	}

	public Boolean getBirimyok() {
		return birimyok;
	}

	public void setBirimyok(Boolean birimyok) {
		this.birimyok = birimyok;
	}

	public Boolean getUyeliktarihiyok() {
		return uyeliktarihiyok;
	}

	public void setUyeliktarihiyok(Boolean uyeliktarihiyok) {
		this.uyeliktarihiyok = uyeliktarihiyok;
	}

	public Boolean getAidatborcuyok() {
		return aidatborcuyok;
	}

	public void setAidatborcuyok(Boolean aidatborcuyok) {
		this.aidatborcuyok = aidatborcuyok;
	}

	public Boolean getUyedurumyok() {
		return uyedurumyok;
	}

	public void setUyedurumyok(Boolean uyedurumyok) {
		this.uyedurumyok = uyedurumyok;
	}

	public Boolean getUyetipiyok() {
		return uyetipiyok;
	}

	public void setUyetipiyok(Boolean uyetipiyok) {
		this.uyetipiyok = uyetipiyok;
	}

	public Boolean getIsyeriyok() {
		return isyeriyok;
	}

	public void setIsyeriyok(Boolean isyeriyok) {
		this.isyeriyok = isyeriyok;
	}

	public Boolean getEvadresiyok() {
		return evadresiyok;
	}

	public void setEvadresiyok(Boolean evadresiyok) {
		this.evadresiyok = evadresiyok;
	}

	public Boolean getYasadigiilyok() {
		return yasadigiilyok;
	}

	public void setYasadigiilyok(Boolean yasadigiilyok) {
		this.yasadigiilyok = yasadigiilyok;
	}

	public Boolean getIsadresiyok() {
		return isadresiyok;
	}

	public void setIsadresiyok(Boolean isadresiyok) {
		this.isadresiyok = isadresiyok;
	}

	public Boolean getCalistigiilyok() {
		return calistigiilyok;
	}

	public void setCalistigiilyok(Boolean calistigiilyok) {
		this.calistigiilyok = calistigiilyok;
	}

	public Boolean getGsmyok() {
		return gsmyok;
	}

	public void setGsmyok(Boolean gsmyok) {
		this.gsmyok = gsmyok;
	}

	public Boolean getPostayok() {
		return postayok;
	}

	public void setPostayok(Boolean postayok) {
		this.postayok = postayok;
	}

	public Boolean getMouniversiteyok() {
		return mouniversiteyok;
	}

	public void setMouniversiteyok(Boolean mouniversiteyok) {
		this.mouniversiteyok = mouniversiteyok;
	}

	public Boolean getMobolumyok() {
		return mobolumyok;
	}

	public void setMobolumyok(Boolean mobolumyok) {
		this.mobolumyok = mobolumyok;
	}

	public Boolean getMezuniyetyiliyok() {
		return mezuniyetyiliyok;
	}

	public void setMezuniyetyiliyok(Boolean mezuniyetyiliyok) {
		this.mezuniyetyiliyok = mezuniyetyiliyok;
	}

	public Boolean getOgrenimtipiyok() {
		return ogrenimtipiyok;
	}

	public void setOgrenimtipiyok(Boolean ogrenimtipiyok) {
		this.ogrenimtipiyok = ogrenimtipiyok;
	}

	public Boolean getCinsiyetyok() {
		return cinsiyetyok;
	}

	public void setCinsiyetyok(Boolean cinsiyetyok) {
		this.cinsiyetyok = cinsiyetyok;
	}

	public Boolean getDogumtarihiyok() {
		return dogumtarihiyok;
	}

	public void setDogumtarihiyok(Boolean dogumtarihiyok) {
		this.dogumtarihiyok = dogumtarihiyok;
	}

	public Kullanicirapor getKullanicirapor() {
		return kullanicirapor;
	}

	public void setKullanicirapor(Kullanicirapor kullanicirapor) {
		this.kullanicirapor = kullanicirapor;
	}

	public String getKurumadi() {
		return kurumadi;
	}

	public void setKurumadi(String kurumadi) {
		this.kurumadi = kurumadi;
	}

	public OgrenimTip[] getKisiOgrenimList() {
		return kisiOgrenimList;
	}

	public void setKisiOgrenimList(OgrenimTip[] kisiOgrenimList) {
		this.kisiOgrenimList = kisiOgrenimList;
	}
	
	
} 
