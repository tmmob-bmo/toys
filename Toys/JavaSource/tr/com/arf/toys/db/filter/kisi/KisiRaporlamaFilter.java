package tr.com.arf.toys.db.filter.kisi; 
 

import tr.com.arf.toys.db.enumerated.kisi.Cinsiyet;
import tr.com.arf.toys.db.model.system.Kullanicirapor;
import tr.com.arf.toys.db.model.system.Sehir;
 
 
public class KisiRaporlamaFilter { 

	private java.util.Date dogumtarih; 
	private java.util.Date dogumtarihMax; 

	private Cinsiyet cinsiyet;
	private Sehir yasadigiIl;
	private Sehir calistigiIl;
	
	private Boolean telefonYok;
	private Boolean epostaYok;
	private Boolean adyok; 
	private Boolean soyadyok;
	private Boolean isyeriyok;
	private Boolean evadresiyok;
	private Boolean yasadigiilyok;
	private Boolean isadresiyok;
	private Boolean calistigiilyok;
	private Boolean gsmyok;
	private Boolean postayok;
	private Boolean cinsiyetyok;
	private Boolean dogumtarihiyok;
	
	private Kullanicirapor kullanicirapor;
	private String kurumadi;
	
	public KisiRaporlamaFilter(){
		 super();
		 init();
	 }
	 
	public void init() {  
		this.cinsiyet=Cinsiyet._NULL;
		this.dogumtarih=null;
		this.dogumtarihMax=null;
		this.yasadigiIl=null;
		this.calistigiIl=null;
		this.telefonYok=false;
		this.epostaYok=false;
		this.adyok=false;
		this.soyadyok=false;
		this.isadresiyok=false;
		this.isyeriyok=false;
		this.evadresiyok=false;
		this.yasadigiilyok=false;
		this.calistigiilyok=false;
		this.gsmyok=false;
		this.postayok=false;
		this.cinsiyetyok=false;
		this.dogumtarihiyok=false;
		this.kullanicirapor=null;
		this.kurumadi=null;
	}
	

	public Cinsiyet getCinsiyet() {
		return cinsiyet;
	}

	public void setCinsiyet(Cinsiyet cinsiyet) {
		this.cinsiyet = cinsiyet;
	}

	public java.util.Date getDogumtarih() {
		return dogumtarih;
	}

	public void setDogumtarih(java.util.Date dogumtarih) {
		this.dogumtarih = dogumtarih;
	}

	public java.util.Date getDogumtarihMax() {
		return dogumtarihMax;
	}

	public void setDogumtarihMax(java.util.Date dogumtarihMax) {
		this.dogumtarihMax = dogumtarihMax;
	}

	public Sehir getYasadigiIl() {
		return yasadigiIl;
	}

	public void setYasadigiIl(Sehir yasadigiIl) {
		this.yasadigiIl = yasadigiIl;
	}

	public Sehir getCalistigiIl() {
		return calistigiIl;
	}

	public void setCalistigiIl(Sehir calistigiIl) {
		this.calistigiIl = calistigiIl;
	}

	public Boolean getTelefonYok() {
		return telefonYok;
	}

	public void setTelefonYok(Boolean telefonYok) {
		this.telefonYok = telefonYok;
	}

	public Boolean getEpostaYok() {
		return epostaYok;
	}

	public void setEpostaYok(Boolean epostaYok) {
		this.epostaYok = epostaYok;
	}


	public Boolean getAdyok() {
		return adyok;
	}

	public void setAdyok(Boolean adyok) {
		this.adyok = adyok;
	}

	public Boolean getSoyadyok() {
		return soyadyok;
	}

	public void setSoyadyok(Boolean soyadyok) {
		this.soyadyok = soyadyok;
	}


	public Boolean getIsyeriyok() {
		return isyeriyok;
	}

	public void setIsyeriyok(Boolean isyeriyok) {
		this.isyeriyok = isyeriyok;
	}

	public Boolean getEvadresiyok() {
		return evadresiyok;
	}

	public void setEvadresiyok(Boolean evadresiyok) {
		this.evadresiyok = evadresiyok;
	}

	public Boolean getYasadigiilyok() {
		return yasadigiilyok;
	}

	public void setYasadigiilyok(Boolean yasadigiilyok) {
		this.yasadigiilyok = yasadigiilyok;
	}

	public Boolean getIsadresiyok() {
		return isadresiyok;
	}

	public void setIsadresiyok(Boolean isadresiyok) {
		this.isadresiyok = isadresiyok;
	}

	public Boolean getCalistigiilyok() {
		return calistigiilyok;
	}

	public void setCalistigiilyok(Boolean calistigiilyok) {
		this.calistigiilyok = calistigiilyok;
	}

	public Boolean getGsmyok() {
		return gsmyok;
	}

	public void setGsmyok(Boolean gsmyok) {
		this.gsmyok = gsmyok;
	}

	public Boolean getPostayok() {
		return postayok;
	}

	public void setPostayok(Boolean postayok) {
		this.postayok = postayok;
	}

	public Boolean getCinsiyetyok() {
		return cinsiyetyok;
	}

	public void setCinsiyetyok(Boolean cinsiyetyok) {
		this.cinsiyetyok = cinsiyetyok;
	}

	public Boolean getDogumtarihiyok() {
		return dogumtarihiyok;
	}

	public void setDogumtarihiyok(Boolean dogumtarihiyok) {
		this.dogumtarihiyok = dogumtarihiyok;
	}

	public Kullanicirapor getKullanicirapor() {
		return kullanicirapor;
	}

	public void setKullanicirapor(Kullanicirapor kullanicirapor) {
		this.kullanicirapor = kullanicirapor;
	}

	public String getKurumadi() {
		return kurumadi;
	}

	public void setKurumadi(String kurumadi) {
		this.kurumadi = kurumadi;
	}
	
} 
