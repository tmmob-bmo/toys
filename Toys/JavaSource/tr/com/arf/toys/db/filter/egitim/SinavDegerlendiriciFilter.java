package tr.com.arf.toys.db.filter.egitim; 
 
import java.util.ArrayList;
import java.util.List;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.framework.db.model.query.QueryObject;
import tr.com.arf.toys.db.model.egitim.Degerlendirici;
import tr.com.arf.toys.db.model.egitim.Sinav;
import tr.com.arf.toys.utility.application.ApplicationConstant;
 
 
public class SinavDegerlendiriciFilter extends BaseFilter { 
	
private static final long serialVersionUID = 1L; 
	
	private Long rID; 
	private Degerlendirici degerlendiricRef; 
	private Sinav sinavRef;
	private String aciklama;
	
	@Override 
	public void init() { 
		this.rID = null; 	
		this.degerlendiricRef=null;
		this.sinavRef=null;
		this.aciklama=null;
	} 
	 
	@Override 
	public List<ArrayList<QueryObject>> createQueryCriterias(){ 
		ArrayList<ArrayList<QueryObject>> criteriaList = new ArrayList<ArrayList<QueryObject>>(); 
		ArrayList<QueryObject> kriterler = new ArrayList<QueryObject>();	 
		ArrayList<QueryObject> opsiyonelKriterler = new ArrayList<QueryObject>(); 
		try { 	 
			if(validateQueryObject(getRID(), null, false)){ 
				kriterler.add(new QueryObject("rID", ApplicationConstant._equals, getRID())); 
			} 
			if(validateQueryObject(getDegerlendiricRef(), null, false)){ 
				kriterler.add(new QueryObject("degerlendiriciRef", ApplicationConstant._equals, getDegerlendiricRef())); 
			} 
			if(validateQueryObject(getSinavRef(), null, false)){ 
				kriterler.add(new QueryObject("sinavRef", ApplicationConstant._equals, getSinavRef())); 
			} 
			if(validateQueryObject(getAciklama(), null, false)){ 
				kriterler.add(new QueryObject("aciklama", ApplicationConstant._equals, getAciklama())); 
			} 
		} catch(Exception e){ 
			System.out.println("Error @createQueryCriterias method, Class : SinavDegerlendiriciFilter :" + e.getMessage()); 
		}  
		criteriaList.add(kriterler); 
		criteriaList.add(opsiyonelKriterler); 
		return criteriaList; 
	} 
	 
	public Long getRID() { 
		return this.rID; 
	} 
	 
	public void setRID(Long rID) { 
		this.rID = rID; 
	}

	public Degerlendirici getDegerlendiricRef() {
		return degerlendiricRef;
	}

	public void setDegerlendiricRef(Degerlendirici degerlendiricRef) {
		this.degerlendiricRef = degerlendiricRef;
	}

	public Sinav getSinavRef() {
		return sinavRef;
	}

	public void setSinavRef(Sinav sinavRef) {
		this.sinavRef = sinavRef;
	}

	public String getAciklama() {
		return aciklama;
	}

	public void setAciklama(String aciklama) {
		this.aciklama = aciklama;
	}

} 
