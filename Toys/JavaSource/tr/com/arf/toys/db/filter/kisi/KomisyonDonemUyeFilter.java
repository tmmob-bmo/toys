package tr.com.arf.toys.db.filter.kisi; 
 
import java.util.ArrayList;
import java.util.List;

import tr.com.arf.framework.db.enumerated._common.EvetHayir;
import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.framework.db.model.query.QueryObject;
import tr.com.arf.toys.db.enumerated.kisi.KomisyonUyelikDurumu;
import tr.com.arf.toys.db.model.kisi.Kisi;
import tr.com.arf.toys.db.model.kisi.KomisyonDonem;
import tr.com.arf.toys.db.model.uye.Uye;
import tr.com.arf.toys.utility.application.ApplicationConstant;
 
 
public class KomisyonDonemUyeFilter extends BaseFilter { 
	
private static final long serialVersionUID = 1L; 
	
	private KomisyonDonem komisyonDonemRef; 
	private Uye uyeRef;
	private EvetHayir durum;
	private Kisi kisiRef;
	private KomisyonUyelikDurumu komisyonUyelikDurumu;
	private java.util.Date durumTarihi;
	private java.util.Date durumTarihiMax;
	
	@Override 
	public void init() { 
		this.komisyonDonemRef = null;
		this.uyeRef = null;
		this.durum=EvetHayir._NULL;
		this.kisiRef = null;
		this.komisyonUyelikDurumu=KomisyonUyelikDurumu._NULL;
		this.durumTarihi=null;
		this.durumTarihiMax=null;
	} 
	 
	@Override 
	public List<ArrayList<QueryObject>> createQueryCriterias(){ 
		ArrayList<ArrayList<QueryObject>> criteriaList = new ArrayList<ArrayList<QueryObject>>(); 
		ArrayList<QueryObject> kriterler = new ArrayList<QueryObject>();	 
		ArrayList<QueryObject> opsiyonelKriterler = new ArrayList<QueryObject>(); 
		try { 	 
			if(validateQueryObject(getKomisyonDonemRef(), null, false)){ 
				kriterler.add(new QueryObject("komisyonDonemRef", ApplicationConstant._equals, getKomisyonDonemRef())); 
			} 
			if(validateQueryObject(getUyeRef(), null, false)){ 
				kriterler.add(new QueryObject("uyeRef", ApplicationConstant._equals, getUyeRef())); 
			}
			if(validateQueryObject(getKisiRef(), null, false)){ 
				kriterler.add(new QueryObject("kisiRef", ApplicationConstant._equals, getKisiRef())); 
			}
			if(validateQueryObject(getDurum(), null, true)){ 
				kriterler.add(new QueryObject("durum", ApplicationConstant._equals, getDurum())); 
			}
			if(validateQueryObject(getKomisyonUyelikDurumu(), null, false)){ 
				kriterler.add(new QueryObject("komisyonUyelikDurumu", ApplicationConstant._equals, getKomisyonUyelikDurumu())); 
			}
			if(validateQueryObject(getDurumTarihi(), null, false)  && validateQueryObject(getDurumTarihiMax(), null, false)){
				kriterler.add(new QueryObject("durumtarihi", ApplicationConstant._equalsAndLargerThan, getDurumTarihi())); 
				kriterler.add(new QueryObject("durumtarihi", ApplicationConstant._equalsAndSmallerThan, getDurumTarihiMax())); 
			} else if(validateQueryObject(getDurumTarihi(), null, false)){ 
				kriterler.add(new QueryObject("durumtarihi", ApplicationConstant._equalsAndLargerThan, getDurumTarihi()));
			}
		} catch(Exception e){ 
			System.out.println("Error @createQueryCriterias method, Class : KomisyonDonemUyeFilter :" + e.getMessage()); 
		}  
		criteriaList.add(kriterler); 
		criteriaList.add(opsiyonelKriterler); 
		return criteriaList; 
	}

	public KomisyonDonem getKomisyonDonemRef() {
		return komisyonDonemRef;
	}

	public void setKomisyonDonemRef(KomisyonDonem komisyonDonemRef) {
		this.komisyonDonemRef = komisyonDonemRef;
	}

	public Uye getUyeRef() {
		return uyeRef;
	}

	public void setUyeRef(Uye uyeRef) {
		this.uyeRef = uyeRef;
	}

	public EvetHayir getDurum() {
		return durum;
	}

	public void setDurum(EvetHayir durum) {
		this.durum = durum;
	}

	public Kisi getKisiRef() {
		return kisiRef;
	}

	public void setKisiRef(Kisi kisiRef) {
		this.kisiRef = kisiRef;
	}

	public KomisyonUyelikDurumu getKomisyonUyelikDurumu() {
		return komisyonUyelikDurumu;
	}

	public void setKomisyonUyelikDurumu(KomisyonUyelikDurumu komisyonUyelikDurumu) {
		this.komisyonUyelikDurumu = komisyonUyelikDurumu;
	}

	public java.util.Date getDurumTarihi() {
		return durumTarihi;
	}

	public void setDurumTarihi(java.util.Date durumTarihi) {
		this.durumTarihi = durumTarihi;
	}

	public java.util.Date getDurumTarihiMax() {
		return durumTarihiMax;
	}

	public void setDurumTarihiMax(java.util.Date durumTarihiMax) {
		this.durumTarihiMax = durumTarihiMax;
	}
	
} 
