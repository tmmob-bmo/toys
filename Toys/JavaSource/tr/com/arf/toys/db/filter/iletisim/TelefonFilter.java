package tr.com.arf.toys.db.filter.iletisim; 
 
import java.util.ArrayList;
import java.util.List;

import tr.com.arf.framework.db.enumerated._common.EvetHayir;
import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.framework.db.model.query.QueryObject;
import tr.com.arf.toys.db.enumerated.iletisim.TelefonTuru;
import tr.com.arf.toys.db.model.kisi.Kisi;
import tr.com.arf.toys.db.model.kisi.Kurum;
import tr.com.arf.toys.utility.application.ApplicationConstant;
 
 
public class TelefonFilter extends BaseFilter { 
	
private static final long serialVersionUID = 1L; 
	
	private Kisi kisiRef; 
	
	private Kurum kurumRef; 
	private String telefonno; 
	private TelefonTuru telefonturu;
	private EvetHayir varsayilan;
	

	@Override 
	public void init() { 
		this.kisiRef = null; 	
		this.kurumRef = null; 	
		this.telefonno = null; 	
		this.telefonturu = TelefonTuru._NULL;	
		this.varsayilan=null;
	} 
	 
	@Override 
	public List<ArrayList<QueryObject>> createQueryCriterias(){ 
		ArrayList<ArrayList<QueryObject>> criteriaList = new ArrayList<ArrayList<QueryObject>>(); 
		ArrayList<QueryObject> kriterler = new ArrayList<QueryObject>();	 
		ArrayList<QueryObject> opsiyonelKriterler = new ArrayList<QueryObject>(); 
		try { 	 
			if(validateQueryObject(getKisiRef(), null, false)){ 
				kriterler.add(new QueryObject("kisiRef", ApplicationConstant._equals, getKisiRef())); 
			} 
			if(validateQueryObject(getKurumRef(), null, false)){ 
				kriterler.add(new QueryObject("kurumRef", ApplicationConstant._equals, getKurumRef())); 
			} 
			if(validateQueryObject(getTelefonno(), null, false)){ 
				kriterler.add(new QueryObject("telefonno", ApplicationConstant._beginsWith, getTelefonno())); 
			} 
			if(validateQueryObject(getTelefonturu(), null, true)){ 
				kriterler.add(new QueryObject("telefonturu", ApplicationConstant._equals, getTelefonturu())); 
			} 
			if(validateQueryObject(getVarsayilan(), null, true)){ 
				kriterler.add(new QueryObject("varsayilan", ApplicationConstant._equals, getVarsayilan())); 
			} 
		} catch(Exception e){ 
			System.out.println("Error @createQueryCriterias method, Class : TelefonFilter :" + e.getMessage()); 
		}  
		criteriaList.add(kriterler); 
		criteriaList.add(opsiyonelKriterler); 
		return criteriaList; 
	} 
	 
	public Kisi getKisiRef() { 
		return this.kisiRef; 
	} 
	 
	public void setKisiRef(Kisi kisiRef) { 
		this.kisiRef = kisiRef; 
	} 
  
	public Kurum getKurumRef() { 
		return this.kurumRef; 
	} 
	 
	public void setKurumRef(Kurum kurumRef) { 
		this.kurumRef = kurumRef; 
	} 
  
	public String getTelefonno() { 
		return this.telefonno; 
	} 
	 
	public void setTelefonno(String telefonno) { 
		this.telefonno = telefonno; 
	} 
  
	public TelefonTuru getTelefonturu() { 
		return this.telefonturu; 
	} 
	 
	public void setTelefonturu(TelefonTuru telefonturu) { 
		this.telefonturu = telefonturu; 
	} 
  
	public EvetHayir getVarsayilan() {
		return this.varsayilan;
	}

	public void setVarsayilan(EvetHayir varsayilan) {
		this.varsayilan = varsayilan;
	}
	
} 
