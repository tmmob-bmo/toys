package tr.com.arf.toys.db.filter.egitim; 
 
import java.util.ArrayList;
import java.util.List;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.framework.db.model.query.QueryObject;
import tr.com.arf.toys.db.model.egitim.Egitim;
import tr.com.arf.toys.utility.application.ApplicationConstant;
 
 
public class EgitimGunleriFilter extends BaseFilter { 
	
private static final long serialVersionUID = 1L; 
	
	private Long rID; 
	private Egitim egitimRef; 
	private java.util.Date tarih; 
	private java.util.Date tarihMax; 
	
	@Override 
	public void init() { 
		this.rID = null; 	
		this.egitimRef=null;
		this.tarih = null; 	
		this.tarihMax = null; 	
	} 
	 
	@Override 
	public List<ArrayList<QueryObject>> createQueryCriterias(){ 
		ArrayList<ArrayList<QueryObject>> criteriaList = new ArrayList<ArrayList<QueryObject>>(); 
		ArrayList<QueryObject> kriterler = new ArrayList<QueryObject>();	 
		ArrayList<QueryObject> opsiyonelKriterler = new ArrayList<QueryObject>(); 
		try { 	 
			if(validateQueryObject(getRID(), null, false)){ 
				kriterler.add(new QueryObject("rID", ApplicationConstant._equals, getRID())); 
			} 
			if(validateQueryObject(getEgitimRef(), null, false)){ 
				kriterler.add(new QueryObject("egitimRef", ApplicationConstant._equals, getEgitimRef())); 
			} 
			if(validateQueryObject(getTarih(), null, false)  && validateQueryObject(getTarihMax(), null, false)){
				kriterler.add(new QueryObject("tarih", ApplicationConstant._equalsAndLargerThan, getTarih())); 
				kriterler.add(new QueryObject("tarih", ApplicationConstant._equalsAndSmallerThan, getTarihMax())); 
			} else if(validateQueryObject(getTarih(), null, false)){ 
				kriterler.add(new QueryObject("tarih", ApplicationConstant._equalsAndLargerThan, getTarih()));
			}
		} catch(Exception e){ 
			System.out.println("Error @createQueryCriterias method, Class : EgitimGunleriFilter :" + e.getMessage()); 
		}  
		criteriaList.add(kriterler); 
		criteriaList.add(opsiyonelKriterler); 
		return criteriaList; 
	} 
	 
	public Long getRID() { 
		return this.rID; 
	} 
	 
	public void setRID(Long rID) { 
		this.rID = rID; 
	}

	public Egitim getEgitimRef() {
		return egitimRef;
	}

	public void setEgitimRef(Egitim egitimRef) {
		this.egitimRef = egitimRef;
	}

	public java.util.Date getTarih() {
		return tarih;
	}

	public void setTarih(java.util.Date tarih) {
		this.tarih = tarih;
	}

	public java.util.Date getTarihMax() {
		return tarihMax;
	}

	public void setTarihMax(java.util.Date tarihMax) {
		this.tarihMax = tarihMax;
	}

} 
