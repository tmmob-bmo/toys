package tr.com.arf.toys.db.filter.kisi; 
 
import java.util.ArrayList;
import java.util.List;

import tr.com.arf.framework.db.enumerated._common.EvetHayir;
import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.framework.db.model.query.QueryObject;
import tr.com.arf.toys.db.model.kisi.KomisyonDonemToplanti;
import tr.com.arf.toys.db.model.kisi.KomisyonDonemUye;
import tr.com.arf.toys.utility.application.ApplicationConstant;
 
 
public class KomisyonDonemToplantiKatilimciFilter extends BaseFilter { 
	
private static final long serialVersionUID = 1L; 
	
	private KomisyonDonemUye komisyonDonemUyeRef;
	private EvetHayir katilim;
	private KomisyonDonemToplanti komisyonDonemToplantiRef;
	
	@Override 
	public void init() { 
		this.komisyonDonemUyeRef=null;
		this.katilim=EvetHayir._NULL;
		this.komisyonDonemToplantiRef=null;
	} 
	 
	@Override 
	public List<ArrayList<QueryObject>> createQueryCriterias(){ 
		ArrayList<ArrayList<QueryObject>> criteriaList = new ArrayList<ArrayList<QueryObject>>(); 
		ArrayList<QueryObject> kriterler = new ArrayList<QueryObject>();	 
		ArrayList<QueryObject> opsiyonelKriterler = new ArrayList<QueryObject>(); 
		try { 	 
			if(validateQueryObject(getKomisyonDonemUyeRef(), null, false)){ 
				kriterler.add(new QueryObject("komisyonDonemUyeRef", ApplicationConstant._equals, getKomisyonDonemUyeRef())); 
			} 
			if(validateQueryObject(getKomisyonDonemToplantiRef(), null, false)){ 
				kriterler.add(new QueryObject("komisyonDonemToplantiRef", ApplicationConstant._equals, getKomisyonDonemToplantiRef())); 
			} 
			
			if(validateQueryObject(getKatilim(), null, false)){ 
				kriterler.add(new QueryObject("katilim", ApplicationConstant._equals, getKatilim())); 
			}
			
		} catch(Exception e){ 
			System.out.println("Error @createQueryCriterias method, Class : KomisyonDonemToplantiKatilimciFilter :" + e.getMessage()); 
		}  
		criteriaList.add(kriterler); 
		criteriaList.add(opsiyonelKriterler); 
		return criteriaList; 
	}

	

	public KomisyonDonemUye getKomisyonDonemUyeRef() {
		return komisyonDonemUyeRef;
	}

	public void setKomisyonDonemUyeRef(KomisyonDonemUye komisyonDonemUyeRef) {
		this.komisyonDonemUyeRef = komisyonDonemUyeRef;
	}

	public KomisyonDonemToplanti getKomisyonDonemToplantiRef() {
		return komisyonDonemToplantiRef;
	}

	public void setKomisyonDonemToplantiRef(
			KomisyonDonemToplanti komisyonDonemToplantiRef) {
		this.komisyonDonemToplantiRef = komisyonDonemToplantiRef;
	}

	public EvetHayir getKatilim() {
		return katilim;
	}

	public void setKatilim(EvetHayir katilim) {
		this.katilim = katilim;
	}

} 
