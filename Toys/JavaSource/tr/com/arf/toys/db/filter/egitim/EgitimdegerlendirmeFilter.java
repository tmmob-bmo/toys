package tr.com.arf.toys.db.filter.egitim; 
 
import java.util.ArrayList;
import java.util.List;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.framework.db.model.query.QueryObject;
import tr.com.arf.toys.db.enumerated.egitim.Cevap;
import tr.com.arf.toys.db.enumerated.egitim.Soru;
import tr.com.arf.toys.db.model.egitim.Egitim;
import tr.com.arf.toys.db.model.egitim.Egitimkatilimci;
import tr.com.arf.toys.db.model.kisi.Kisi;
import tr.com.arf.toys.utility.application.ApplicationConstant;
 
 
public class EgitimdegerlendirmeFilter extends BaseFilter { 
	
private static final long serialVersionUID = 1L; 
	
	private Egitimkatilimci egitimkatilimciRef; 
	private Kisi kisiRef;
	private Long rID; 
	private Soru soru; 
	private Cevap cevap; 
	private String aciklama; 
	private Egitim egitimRef;
	
	@Override 
	public void init() { 
		this.rID = null; 	
		this.egitimkatilimciRef = null; 	
		this.soru = Soru._NULL;	
		this.cevap = Cevap._NULL;	
		this.aciklama = null; 	
		this.egitimRef=null;
		this.kisiRef=null;
	} 
	 
	@Override 
	public List<ArrayList<QueryObject>> createQueryCriterias(){ 
		ArrayList<ArrayList<QueryObject>> criteriaList = new ArrayList<ArrayList<QueryObject>>(); 
		ArrayList<QueryObject> kriterler = new ArrayList<QueryObject>();	 
		ArrayList<QueryObject> opsiyonelKriterler = new ArrayList<QueryObject>(); 
		try { 	 
			if(validateQueryObject(getRID(), null, false)){ 
				kriterler.add(new QueryObject("rID", ApplicationConstant._equals, getRID())); 
			} 
			if(validateQueryObject(getEgitimkatilimciRef(), null, false)){ 
				kriterler.add(new QueryObject("egitimkatilimciRef", ApplicationConstant._equals, getEgitimkatilimciRef())); 
			} 
			if(validateQueryObject(getKisiRef(), null, false)){ 
				kriterler.add(new QueryObject("egitimkatilimciRef.kisiRef", ApplicationConstant._equals, getKisiRef())); 
			} 
			if(validateQueryObject(getSoru(), null, true)){ 
				kriterler.add(new QueryObject("soru", ApplicationConstant._equals, getSoru())); 
			} 
			if(validateQueryObject(getCevap(), null, true)){ 
				kriterler.add(new QueryObject("cevap", ApplicationConstant._equals, getCevap())); 
			} 
			if(validateQueryObject(getAciklama(), null, false)){ 
				kriterler.add(new QueryObject("aciklama", ApplicationConstant._beginsWith, getAciklama(), false)); 
			}
			if(validateQueryObject(getEgitimRef(), null, false)){ 
				kriterler.add(new QueryObject("egitimRef", ApplicationConstant._equals, getEgitimRef())); 
			} 
		} catch(Exception e){ 
			System.out.println("Error @createQueryCriterias method, Class : EgitimdegerlendirmeFilter :" + e.getMessage()); 
		}  
		criteriaList.add(kriterler); 
		criteriaList.add(opsiyonelKriterler); 
		return criteriaList; 
	} 
	 
	public Long getRID() { 
		return this.rID; 
	} 
	 
	public void setRID(Long rID) { 
		this.rID = rID; 
	} 
  
	public Egitimkatilimci getEgitimkatilimciRef() { 
		return this.egitimkatilimciRef; 
	} 
	 
	public void setEgitimkatilimciRef(Egitimkatilimci egitimkatilimciRef) { 
		this.egitimkatilimciRef = egitimkatilimciRef; 
	} 
  
	public Soru getSoru() { 
		return this.soru; 
	} 
	 
	public void setSoru(Soru soru) { 
		this.soru = soru; 
	} 
  
	public Cevap getCevap() { 
		return this.cevap; 
	} 
	 
	public void setCevap(Cevap cevap) { 
		this.cevap = cevap; 
	} 
  
	public String getAciklama() { 
		return this.aciklama; 
	} 
	 
	public void setAciklama(String aciklama) { 
		this.aciklama = aciklama; 
	}

	public Egitim getEgitimRef() {
		return egitimRef;
	}

	public void setEgitimRef(Egitim egitimRef) {
		this.egitimRef = egitimRef;
	}

	public Kisi getKisiRef() {
		return kisiRef;
	}

	public void setKisiRef(Kisi kisiRef) {
		this.kisiRef = kisiRef;
	} 
} 
