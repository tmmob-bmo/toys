package tr.com.arf.toys.db.filter.system;

import java.util.ArrayList;
import java.util.List;

import tr.com.arf.framework.db.enumerated._common.EvetHayir;
import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.framework.db.model.query.QueryObject;
import tr.com.arf.toys.utility.application.ApplicationConstant;

public class SistemParametreFilter extends BaseFilter {

	private static final long serialVersionUID = 1L;

	private String listeboyutu;
	private String smsuzunluk;
	private String odadetayi;
	private String odaadi;
	private String odawebadresi;
	private String logodegeri;
	private EvetHayir kpszorla;
	private String mailadresi;
	private String sistemmail;
	private String yapikrediposadres;
	private String isbankasiposadres;
	@Override
	public void init() {
		this.listeboyutu = null;
		this.smsuzunluk = null;
		this.odaadi=null;
		this.odadetayi=null;
		this.odawebadresi=null;
		this.logodegeri=null;
		this.kpszorla=EvetHayir._NULL;
		this.mailadresi=null;
		this.sistemmail=null;
		this.yapikrediposadres=null;
		this.isbankasiposadres=null;
	}

	@Override
	public List<ArrayList<QueryObject>> createQueryCriterias() {
		ArrayList<ArrayList<QueryObject>> criteriaList = new ArrayList<ArrayList<QueryObject>>();
		ArrayList<QueryObject> kriterler = new ArrayList<QueryObject>();
		ArrayList<QueryObject> opsiyonelKriterler = new ArrayList<QueryObject>();
		try {
			if (validateQueryObject(getListeboyutu(), null, false)) {
				kriterler.add(new QueryObject("listeboyutu",ApplicationConstant._beginsWith, getListeboyutu()));
			}
			if (validateQueryObject(getSmsuzunluk(), null, false)) {
				kriterler.add(new QueryObject("smsuzunluk",	ApplicationConstant._beginsWith, getSmsuzunluk()));
			}
			if (validateQueryObject(getOdaadi(), null, false)) {
				kriterler.add(new QueryObject("odaadi",	ApplicationConstant._beginsWith, getOdaadi()));
			}
			if (validateQueryObject(getOdadetayi(), null, false)) {
				kriterler.add(new QueryObject("odadetayi",	ApplicationConstant._beginsWith, getOdadetayi()));
			}
			if (validateQueryObject(getOdawebadresi(), null, false)) {
				kriterler.add(new QueryObject("odawebadresi",	ApplicationConstant._beginsWith, getOdawebadresi()));
			}
			if (validateQueryObject(getLogodegeri(), null, false)) {
				kriterler.add(new QueryObject("logodegeri",	ApplicationConstant._beginsWith, getLogodegeri()));
			}
			if (validateQueryObject(getKpszorla(), null, false)) {
				kriterler.add(new QueryObject("kpszorla",	ApplicationConstant._beginsWith, getKpszorla()));
			}
			if (validateQueryObject(getMailadresi(), null, false)) {
				kriterler.add(new QueryObject("mailadresi",	ApplicationConstant._beginsWith, getMailadresi()));
			}
			if (validateQueryObject(getSistemmail(), null, false)) {
				kriterler.add(new QueryObject("sistemmail",	ApplicationConstant._beginsWith, getSistemmail()));
			}
			if (validateQueryObject(getYapikrediposadres(), null, false)) {
				kriterler.add(new QueryObject("yapikrediposadres",	ApplicationConstant._beginsWith, getYapikrediposadres()));
			}
			if (validateQueryObject(getIsbankasiposadres(), null, false)) {
				kriterler.add(new QueryObject("isbankasiposadres",	ApplicationConstant._beginsWith, getIsbankasiposadres()));
			}
		} catch (Exception e) {
			System.out.println("Error @createQueryCriterias method, Class : SistemParametreFilter :"+ e.getMessage());
		}
		criteriaList.add(kriterler);
		criteriaList.add(opsiyonelKriterler);
		return criteriaList;
	}


	public String getListeboyutu() {
		return listeboyutu;
	}

	public void setListeboyutu(String listeboyutu) {
		this.listeboyutu = listeboyutu;
	}

	public String getSmsuzunluk() {
		return smsuzunluk;
	}

	public void setSmsuzunluk(String smsuzunluk) {
		this.smsuzunluk = smsuzunluk;
	}

	public String getOdadetayi() {
		return odadetayi;
	}

	public void setOdadetayi(String odadetayi) {
		this.odadetayi = odadetayi;
	}

	public String getOdaadi() {
		return odaadi;
	}

	public void setOdaadi(String odaadi) {
		this.odaadi = odaadi;
	}

	public String getOdawebadresi() {
		return odawebadresi;
	}

	public void setOdawebadresi(String odawebadresi) {
		this.odawebadresi = odawebadresi;
	}

	public String getLogodegeri() {
		return logodegeri;
	}

	public void setLogodegeri(String logodegeri) {
		this.logodegeri = logodegeri;
	}

	public EvetHayir getKpszorla() {
		return kpszorla;
	}

	public void setKpszorla(EvetHayir kpszorla) {
		this.kpszorla = kpszorla;
	}

	public String getMailadresi() {
		return mailadresi;
	}

	public void setMailadresi(String mailadresi) {
		this.mailadresi = mailadresi;
	}

	public String getSistemmail() {
		return sistemmail;
	}

	public void setSistemmail(String sistemmail) {
		this.sistemmail = sistemmail;
	}

	public String getYapikrediposadres() {
		return yapikrediposadres;
	}

	public void setYapikrediposadres(String yapikrediposadres) {
		this.yapikrediposadres = yapikrediposadres;
	}

	public String getIsbankasiposadres() {
		return isbankasiposadres;
	}

	public void setIsbankasiposadres(String isbankasiposadres) {
		this.isbankasiposadres = isbankasiposadres;
	}
}
