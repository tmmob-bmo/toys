package tr.com.arf.toys.db.filter.egitim; 
 
import java.util.ArrayList;
import java.util.List;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.framework.db.model.query.QueryObject;
import tr.com.arf.toys.db.enumerated.egitim.EtkinDurum;
import tr.com.arf.toys.db.model.kisi.Kisi;
import tr.com.arf.toys.db.model.system.Birim;
import tr.com.arf.toys.utility.application.ApplicationConstant;
 
 
public class EgitmenFilter extends BaseFilter { 
	
private static final long serialVersionUID = 1L; 
	
	private Birim birimRef;
	private Kisi kisiRef; 
	private String aciklama;
	private EtkinDurum durum;
	
	@Override 
	public void init() { 
		this.birimRef=null;
		this.kisiRef=null;
		this.aciklama=null;
		this.durum=EtkinDurum._NULL;
	} 
	 
	@Override 
	public List<ArrayList<QueryObject>> createQueryCriterias(){ 
		ArrayList<ArrayList<QueryObject>> criteriaList = new ArrayList<ArrayList<QueryObject>>(); 
		ArrayList<QueryObject> kriterler = new ArrayList<QueryObject>();	 
		ArrayList<QueryObject> opsiyonelKriterler = new ArrayList<QueryObject>(); 
		try { 	 
			if(validateQueryObject(getBirimRef(), null, false)){ 
				kriterler.add(new QueryObject("birimRef", ApplicationConstant._equals, getBirimRef())); 
			} 
			if(validateQueryObject(getKisiRef(), null, false)){ 
				kriterler.add(new QueryObject("kisiRef", ApplicationConstant._equals, getKisiRef())); 
			} 
			if(validateQueryObject(getAciklama(), null, false)){ 
				kriterler.add(new QueryObject("aciklama", ApplicationConstant._inside, getAciklama())); 
			} 
			if(validateQueryObject(getDurum(), null, false)){ 
				kriterler.add(new QueryObject("durum", ApplicationConstant._equals, getDurum())); 
			}
		} catch(Exception e){ 
			System.out.println("Error @createQueryCriterias method, Class : EgitmenFilter :" + e.getMessage()); 
		}  
		criteriaList.add(kriterler); 
		criteriaList.add(opsiyonelKriterler); 
		return criteriaList; 
	}

	

	public Birim getBirimRef() {
		return birimRef;
	}

	public void setBirimRef(Birim birimRef) {
		this.birimRef = birimRef;
	}

	public String getAciklama() {
		return aciklama;
	}

	public void setAciklama(String aciklama) {
		this.aciklama = aciklama;
	}

	public Kisi getKisiRef() {
		return kisiRef;
	}

	public void setKisiRef(Kisi kisiRef) {
		this.kisiRef = kisiRef;
	}

	public EtkinDurum getDurum() {
		return durum;
	}

	public void setDurum(EtkinDurum durum) {
		this.durum = durum;
	} 
	
} 
