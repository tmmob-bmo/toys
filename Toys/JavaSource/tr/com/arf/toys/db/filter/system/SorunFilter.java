package tr.com.arf.toys.db.filter.system; 
 
import java.util.ArrayList;
import java.util.List;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.framework.db.model.query.QueryObject;
import tr.com.arf.toys.db.enumerated.system.OnemDerece;
import tr.com.arf.toys.db.model.system.Kullanici;
import tr.com.arf.toys.utility.application.ApplicationConstant;
 
 
public class SorunFilter extends BaseFilter { 
	
private static final long serialVersionUID = 1L; 
	
	
    private Kullanici kullaniciRef; 
	private OnemDerece onem; 
	private String baslik;
	private String aciklama;
	private java.util.Date tarih; 
	private java.util.Date tarihMax; 
	
	@Override 
	public void init() { 
		this.kullaniciRef=null;
		this.onem=OnemDerece._NULL;
		this.baslik=null;
		this.aciklama=null;
		this.tarih=null;
		this.tarihMax=null;
	} 
	 
	@Override 
	public List<ArrayList<QueryObject>> createQueryCriterias(){ 
		ArrayList<ArrayList<QueryObject>> criteriaList = new ArrayList<ArrayList<QueryObject>>(); 
		ArrayList<QueryObject> kriterler = new ArrayList<QueryObject>();	 
		ArrayList<QueryObject> opsiyonelKriterler = new ArrayList<QueryObject>(); 
		try { 	 
			if(validateQueryObject(getKullaniciRef(), null, false)){ 
				kriterler.add(new QueryObject("kullaniciRef", ApplicationConstant._equals, getKullaniciRef())); 
			} 
			if(validateQueryObject(getTarih(), null, false)  && validateQueryObject(getTarihMax(), null, false)){
				kriterler.add(new QueryObject("tarih", ApplicationConstant._equalsAndLargerThan, getTarih())); 
				kriterler.add(new QueryObject("tarih", ApplicationConstant._equalsAndSmallerThan, getTarihMax())); 
			} else if(validateQueryObject(getTarih(), null, false)){ 
				kriterler.add(new QueryObject("tarih", ApplicationConstant._equalsAndLargerThan, getTarih()));
			}
			if(validateQueryObject(getBaslik(), null, false)){ 
				kriterler.add(new QueryObject("baslik", ApplicationConstant._beginsWith, getBaslik())); 
			} 
			if(validateQueryObject(getAciklama(), null, false)){ 
				kriterler.add(new QueryObject("aciklama", ApplicationConstant._beginsWith, getAciklama())); 
			} 
			if(validateQueryObject(getOnem(), null, true)){ 
				kriterler.add(new QueryObject("onem", ApplicationConstant._equals, getOnem())); 
			} 
		} catch(Exception e){ 
			System.out.println("Error @createQueryCriterias method, Class : SorunFilter :" + e.getMessage()); 
		}  
		criteriaList.add(kriterler); 
		criteriaList.add(opsiyonelKriterler); 
		return criteriaList; 
	}


	public Kullanici getKullaniciRef() {
		return kullaniciRef;
	}

	public void setKullaniciRef(Kullanici kullaniciRef) {
		this.kullaniciRef = kullaniciRef;
	}

	public OnemDerece getOnem() {
		return onem;
	}

	public void setOnem(OnemDerece onem) {
		this.onem = onem;
	}

	public String getBaslik() {
		return baslik;
	}

	public void setBaslik(String baslik) {
		this.baslik = baslik;
	}

	public String getAciklama() {
		return aciklama;
	}

	public void setAciklama(String aciklama) {
		this.aciklama = aciklama;
	}

	public java.util.Date getTarih() {
		return tarih;
	}

	public void setTarih(java.util.Date tarih) {
		this.tarih = tarih;
	}
	 
	public java.util.Date getTarihMax() {
		return tarihMax;
	}
	
	public void setTarihMax(java.util.Date tarihMax) {
		this.tarihMax = tarihMax;
	}
} 
