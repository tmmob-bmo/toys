package tr.com.arf.toys.db.filter.system;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.framework.db.model.query.QueryObject;
import tr.com.arf.toys.db.model.system.Kullanici;
import tr.com.arf.toys.utility.application.ApplicationConstant;

public class KullaniciSifreTalepFilter extends BaseFilter {

	private static final long serialVersionUID = 1L;

	private Kullanici kulllaniciRef;
	private String guvenlikkodu;
	private String ad;
	private String soyad;
	private java.util.Date tarih;
	private java.util.Date tarihMax;

	@Override
	public void init() {
		this.kulllaniciRef = null;
		this.guvenlikkodu = null;
		this.ad = null;
		this.soyad = null;
		this.tarih = null;
		this.tarihMax = null;
	}

	@Override
	public List<ArrayList<QueryObject>> createQueryCriterias() {
		ArrayList<ArrayList<QueryObject>> criteriaList = new ArrayList<ArrayList<QueryObject>>();
		ArrayList<QueryObject> kriterler = new ArrayList<QueryObject>();
		ArrayList<QueryObject> opsiyonelKriterler = new ArrayList<QueryObject>();
		try {
			if (validateQueryObject(getKulllaniciRef(), null, false)) {
				kriterler.add(new QueryObject("kullaniciRef", ApplicationConstant._equals, getKulllaniciRef()));
			}
			if (validateQueryObject(getAd(), null, false)) {
				kriterler.add(new QueryObject("kullaniciRef.kisiRef.ad", ApplicationConstant._beginsWith, getAd()));
			}
			if (validateQueryObject(getSoyad(), null, false)) {
				kriterler.add(new QueryObject("kullaniciRef.kisiRef.soyad", ApplicationConstant._beginsWith, getSoyad()));
			}
			if (validateQueryObject(getGuvenlikkodu(), null, false)) {
				kriterler.add(new QueryObject("guvenlikkodu", ApplicationConstant._beginsWith, getGuvenlikkodu()));
			}
			if (validateQueryObject(getTarih(), null, false) && validateQueryObject(getTarihMax(), null, false)) {
				kriterler.add(new QueryObject("tarih", ApplicationConstant._equalsAndLargerThan, getTarih()));
				kriterler.add(new QueryObject("tarih", ApplicationConstant._equalsAndSmallerThan, getTarihMax()));
			} else if (validateQueryObject(getTarih(), null, false)) {
				kriterler.add(new QueryObject("tarih", ApplicationConstant._equalsAndLargerThan, getTarih()));
			}
		} catch (Exception e) {
			System.out.println("Error @createQueryCriterias method, Class : kullaniciSifreTalepFilter :" + e.getMessage());
		}
		criteriaList.add(kriterler);
		criteriaList.add(opsiyonelKriterler);
		return criteriaList;
	}

	public String getAd() {
		return ad;
	}

	public void setAd(String ad) {
		this.ad = ad;
	}

	public String getSoyad() {
		return soyad;
	}

	public void setSoyad(String soyad) {
		this.soyad = soyad;
	}

	public Kullanici getKulllaniciRef() {
		return kulllaniciRef;
	}

	public void setKulllaniciRef(Kullanici kulllaniciRef) {
		this.kulllaniciRef = kulllaniciRef;
	}

	public String getGuvenlikkodu() {
		return guvenlikkodu;
	}

	public void setGuvenlikkodu(String guvenlikkodu) {
		this.guvenlikkodu = guvenlikkodu;
	}

	public Date getTarih() {
		return tarih;
	}

	public void setTarih(Date tarih) {
		this.tarih = tarih;
	}

	public Date getTarihMax() {
		return tarihMax;
	}

	public void setTarihMax(Date tarihMax) {
		this.tarihMax = tarihMax;
	}

}
