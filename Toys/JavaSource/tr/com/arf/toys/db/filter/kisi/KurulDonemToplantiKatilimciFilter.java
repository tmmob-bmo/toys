package tr.com.arf.toys.db.filter.kisi; 
 
import java.util.ArrayList;
import java.util.List;

import tr.com.arf.framework.db.enumerated._common.EvetHayir;
import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.framework.db.model.query.QueryObject;
import tr.com.arf.toys.db.model.kisi.KurulDonemToplanti;
import tr.com.arf.toys.db.model.kisi.KurulDonemUye;
import tr.com.arf.toys.utility.application.ApplicationConstant;
 
 
public class KurulDonemToplantiKatilimciFilter extends BaseFilter { 
	
private static final long serialVersionUID = 1L; 
	
	private KurulDonemUye kurulDonemUyeRef;
	private EvetHayir katilim;
	private KurulDonemToplanti kurulDonemToplantiRef;
	
	@Override 
	public void init() { 
		this.kurulDonemUyeRef=null;
		this.katilim=EvetHayir._NULL;
		this.kurulDonemToplantiRef=null;
	} 
	 
	@Override 
	public List<ArrayList<QueryObject>> createQueryCriterias(){ 
		ArrayList<ArrayList<QueryObject>> criteriaList = new ArrayList<ArrayList<QueryObject>>(); 
		ArrayList<QueryObject> kriterler = new ArrayList<QueryObject>();	 
		ArrayList<QueryObject> opsiyonelKriterler = new ArrayList<QueryObject>(); 
		try { 	 
			if(validateQueryObject(getKurulDonemUyeRef(), null, false)){ 
				kriterler.add(new QueryObject("kurulDonemUyeRef", ApplicationConstant._equals, getKurulDonemUyeRef())); 
			} 
			
			if(validateQueryObject(getKatilim(), null, false)){ 
				kriterler.add(new QueryObject("katilim", ApplicationConstant._equals, getKatilim())); 
			}
			
		} catch(Exception e){ 
			System.out.println("Error @createQueryCriterias method, Class : KurulDonemToplantiKatilimciFilter :" + e.getMessage()); 
		}  
		criteriaList.add(kriterler); 
		criteriaList.add(opsiyonelKriterler); 
		return criteriaList; 
	}

	public KurulDonemUye getKurulDonemUyeRef() {
		return kurulDonemUyeRef;
	}

	public void setKurulDonemUyeRef(KurulDonemUye kurulDonemUyeRef) {
		this.kurulDonemUyeRef = kurulDonemUyeRef;
	}

	public EvetHayir getKatilim() {
		return katilim;
	}

	public void setKatilim(EvetHayir katilim) {
		this.katilim = katilim;
	}

	public KurulDonemToplanti getKurulDonemToplantiRef() {
		return kurulDonemToplantiRef;
	}

	public void setKurulDonemToplantiRef(KurulDonemToplanti kurulDonemToplantiRef) {
		this.kurulDonemToplantiRef = kurulDonemToplantiRef;
	}

} 
