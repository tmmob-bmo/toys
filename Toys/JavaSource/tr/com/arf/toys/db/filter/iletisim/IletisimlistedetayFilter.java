package tr.com.arf.toys.db.filter.iletisim; 
 
import java.util.ArrayList;
import java.util.List;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.framework.db.model.query.QueryObject;
import tr.com.arf.toys.db.enumerated.iletisim.ReferansTipi;
import tr.com.arf.toys.db.model.iletisim.Iletisimliste;
import tr.com.arf.toys.utility.application.ApplicationConstant;
 
 
public class IletisimlistedetayFilter extends BaseFilter { 
	
private static final long serialVersionUID = 1L; 
	
	private Iletisimliste iletisimlisteRef; 
	private Long rID; 
	private ReferansTipi referansTipi; 
	private Long Referans; 
	
	@Override 
	public void init() { 
		this.rID = null; 	
		this.referansTipi = ReferansTipi._NULL;	
		this.Referans = null; 	
		this.iletisimlisteRef = null; 	
	} 
	 
	@Override 
	public List<ArrayList<QueryObject>> createQueryCriterias(){ 
		ArrayList<ArrayList<QueryObject>> criteriaList = new ArrayList<ArrayList<QueryObject>>(); 
		ArrayList<QueryObject> kriterler = new ArrayList<QueryObject>();	 
		ArrayList<QueryObject> opsiyonelKriterler = new ArrayList<QueryObject>(); 
		try { 	 
			if(validateQueryObject(getRID(), null, false)){ 
				kriterler.add(new QueryObject("rID", ApplicationConstant._equals, getRID())); 
			} 
			if(validateQueryObject(getReferansTipi(), null, true)){ 
				kriterler.add(new QueryObject("ReferansTipi", ApplicationConstant._equals, getReferansTipi())); 
			} 
			if(validateQueryObject(getReferans(), null, false)){ 
				kriterler.add(new QueryObject("Referans", ApplicationConstant._equals, getReferans())); 
			} 
			if(validateQueryObject(getIletisimlisteRef(), null, false)){ 
				kriterler.add(new QueryObject("iletisimlisteRef", ApplicationConstant._equals, getIletisimlisteRef())); 
			} 
		} catch(Exception e){ 
			System.out.println("Error @createQueryCriterias method, Class : IletisimlistedetayFilter :" + e.getMessage()); 
		}  
		criteriaList.add(kriterler); 
		criteriaList.add(opsiyonelKriterler); 
		return criteriaList; 
	} 
	 
	public Long getRID() { 
		return this.rID; 
	} 
	 
	public void setRID(Long rID) { 
		this.rID = rID; 
	}     
  
	public ReferansTipi getReferansTipi() {
		return referansTipi;
	}

	public void setReferansTipi(ReferansTipi referansTipi) {
		this.referansTipi = referansTipi;
	}

	public Long getReferans() { 
		return this.Referans; 
	} 
	 
	public void setReferans(Long Referans) { 
		this.Referans = Referans; 
	} 
  
	public Iletisimliste getIletisimlisteRef() { 
		return this.iletisimlisteRef; 
	} 
	 
	public void setIletisimlisteRef(Iletisimliste iletisimlisteRef) { 
		this.iletisimlisteRef = iletisimlisteRef; 
	} 
  
	
} 
