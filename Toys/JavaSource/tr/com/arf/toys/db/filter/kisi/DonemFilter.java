package tr.com.arf.toys.db.filter.kisi; 
 
import java.util.ArrayList;
import java.util.List;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.framework.db.model.query.QueryObject;
import tr.com.arf.toys.db.enumerated.system.BirimYetkiTuru;
import tr.com.arf.toys.db.model.kisi.Anadonem;
import tr.com.arf.toys.db.model.system.Birim;
import tr.com.arf.toys.service.application.ManagedBeanLocator;
import tr.com.arf.toys.utility.application.ApplicationConstant;
import tr.com.arf.toys.view.controller._common.SessionUser;
 
 
public class DonemFilter extends BaseFilter { 
	
private static final long serialVersionUID = 1L; 
	
	private Anadonem anadonemRef;
	private Birim birimRef; 

	private String tanim;
	private java.util.Date baslangictarih; 
	private java.util.Date baslangictarihMax; 
	private java.util.Date bitistarih; 
	private java.util.Date bitistarihMax; 
	
	@Override 
	public void init() { 
		this.tanim=null;
		this.baslangictarih = null; 	
		this.baslangictarihMax = null; 	
		this.bitistarih = null; 	
		this.bitistarihMax = null; 	
	} 
	 
	@Override 
	public List<ArrayList<QueryObject>> createQueryCriterias(){ 
		ArrayList<ArrayList<QueryObject>> criteriaList = new ArrayList<ArrayList<QueryObject>>(); 
		ArrayList<QueryObject> kriterler = new ArrayList<QueryObject>();	 
		ArrayList<QueryObject> opsiyonelKriterler = new ArrayList<QueryObject>(); 
		try { 
			if(validateQueryObject(getAnadonemRef(), null, false)){ 
				kriterler.add(new QueryObject("anadonemRef", ApplicationConstant._equals, getAnadonemRef())); 
			} 
			if(validateQueryObject(getBirimRef(), null, false)){ 
				kriterler.add(new QueryObject("birimRef", ApplicationConstant._equals, getBirimRef())); 
			} 
			if(validateQueryObject(getBaslangictarih(), null, false)  && validateQueryObject(getBaslangictarihMax(), null, false)){
				kriterler.add(new QueryObject("baslangictarih", ApplicationConstant._equalsAndLargerThan, getBaslangictarih())); 
				kriterler.add(new QueryObject("baslangictarih", ApplicationConstant._equalsAndSmallerThan, getBaslangictarihMax())); 
			} else if(validateQueryObject(getBaslangictarih(), null, false)){ 
				kriterler.add(new QueryObject("baslangictarih", ApplicationConstant._equalsAndLargerThan, getBaslangictarih()));
			}
			if(validateQueryObject(getBitistarih(), null, false)  && validateQueryObject(getBitistarihMax(), null, false)){
				kriterler.add(new QueryObject("bitistarih", ApplicationConstant._equalsAndLargerThan, getBitistarih())); 
				kriterler.add(new QueryObject("bitistarih", ApplicationConstant._equalsAndSmallerThan, getBitistarihMax())); 
			} else if(validateQueryObject(getBitistarih(), null, false)){ 
				kriterler.add(new QueryObject("bitistarih", ApplicationConstant._equalsAndLargerThan, getBitistarih()));
			}
			if(validateQueryObject(getTanim(), null, false)){ 
				kriterler.add(new QueryObject("tanim", ApplicationConstant._beginsWith, getTanim())); 
			} 
			// Kullanicinin birimine gore yetkilendirme yapiliyor.
			SessionUser sessionUser = ManagedBeanLocator.locateSessionUser();
			if(sessionUser.getPersonelRef() != null && sessionUser.getPersonelRef().getBirimRef() != null){
				if(sessionUser.getBirimYetkiTuru() != BirimYetkiTuru._TUMBIRIMLER){
					if(sessionUser.getBirimYetkiTuru() == BirimYetkiTuru._KENDIBIRIMIVEALTBIRIMLERI){ 
						kriterler.add(new QueryObject("birimRef.erisimKodu", ApplicationConstant._beginsWith, sessionUser.getPersonelRef().getBirimRef().getErisimKodu())); 
					} else if(sessionUser.getBirimYetkiTuru() == BirimYetkiTuru._SADECEKENDIBIRIMLERI){ 
						kriterler.add(new QueryObject("birimRef", ApplicationConstant._equals, sessionUser.getPersonelRef().getBirimRef())); 
					}
				}
			} 
		} catch(Exception e){ 
			System.out.println("Error @createQueryCriterias method, Class : TanimFilter :" + e.getMessage()); 
		}  
		criteriaList.add(kriterler); 
		criteriaList.add(opsiyonelKriterler); 
		return criteriaList; 
	}

	public Anadonem getAnadonemRef() { 
		return this.anadonemRef; 
	} 
	 
	public void setAnadonemRef(Anadonem anadonemRef) { 
		this.anadonemRef = anadonemRef; 
	}
	
	public Birim getBirimRef() {
		return birimRef;
	}

	public void setBirimRef(Birim birimRef) {
		this.birimRef = birimRef;
	}

	public java.util.Date getBaslangictarih() {
		return baslangictarih;
	}

	public void setBaslangictarih(java.util.Date baslangictarih) {
		this.baslangictarih = baslangictarih;
	}

	public java.util.Date getBaslangictarihMax() {
		return baslangictarihMax;
	}

	public void setBaslangictarihMax(java.util.Date baslangictarihMax) {
		this.baslangictarihMax = baslangictarihMax;
	}

	public java.util.Date getBitistarih() {
		return bitistarih;
	}

	public void setBitistarih(java.util.Date bitistarih) {
		this.bitistarih = bitistarih;
	}

	public java.util.Date getBitistarihMax() {
		return bitistarihMax;
	}

	public void setBitistarihMax(java.util.Date bitistarihMax) {
		this.bitistarihMax = bitistarihMax;
	}

	public String getTanim() {
		return tanim;
	}

	public void setTanim(String tanim) {
		this.tanim = tanim;
	}
	 
} 
