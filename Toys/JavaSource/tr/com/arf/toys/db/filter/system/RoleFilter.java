package tr.com.arf.toys.db.filter.system; 
 
import java.util.ArrayList;
import java.util.List;

import tr.com.arf.framework.db.enumerated._common.EvetHayir;
import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.framework.db.model.query.QueryObject;
import tr.com.arf.toys.utility.application.ApplicationConstant;
 
 
public class RoleFilter extends BaseFilter { 
	
private static final long serialVersionUID = 1L; 
	
	private String name; 
	private EvetHayir active; 
	
	@Override 
	public void init() { 
		this.name = null; 	
		this.active = EvetHayir._NULL;	
	} 
	 
	@Override 
	public List<ArrayList<QueryObject>> createQueryCriterias(){ 
		ArrayList<ArrayList<QueryObject>> criteriaList = new ArrayList<ArrayList<QueryObject>>(); 
		ArrayList<QueryObject> kriterler = new ArrayList<QueryObject>();	 
		ArrayList<QueryObject> opsiyonelKriterler = new ArrayList<QueryObject>(); 
		try { 	 
			if(validateQueryObject(getName(), null, false)){ 
				kriterler.add(new QueryObject("name", ApplicationConstant._beginsWith, getName())); 
			} 
			if(validateQueryObject(getActive(), null, true)){ 
				kriterler.add(new QueryObject("active", ApplicationConstant._equals, getActive())); 
			} 
		} catch(Exception e){ 
			System.out.println("Error @createQueryCriterias method, Class : RoleFilter :" + e.getMessage()); 
		}  
		criteriaList.add(kriterler); 
		criteriaList.add(opsiyonelKriterler); 
		return criteriaList; 
	} 
	 
	public String getName() { 
		return this.name; 
	} 
	 
	public void setName(String name) { 
		this.name = name; 
	} 
  
	public EvetHayir getActive() { 
		return this.active; 
	} 
	 
	public void setActive(EvetHayir active) { 
		this.active = active; 
	} 
  
	
} 
