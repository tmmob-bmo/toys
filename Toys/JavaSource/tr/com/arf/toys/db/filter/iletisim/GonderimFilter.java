package tr.com.arf.toys.db.filter.iletisim; 
 
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import tr.com.arf.framework.db.enumerated._common.EvetHayir;
import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.framework.db.model.query.QueryObject;
import tr.com.arf.toys.db.enumerated.iletisim.GonderimDurum;
import tr.com.arf.toys.db.enumerated.iletisim.GonderimTuru;
import tr.com.arf.toys.db.model.iletisim.Icerik;
import tr.com.arf.toys.db.model.iletisim.Iletisimliste;
import tr.com.arf.toys.db.model.system.Birim;
import tr.com.arf.toys.utility.application.ApplicationConstant;
 
 
public class GonderimFilter extends BaseFilter { 
	
private static final long serialVersionUID = 1L; 
	
	private Long rID; 
	private Iletisimliste iletisimlisteRef; 
	private Icerik icerikRef; 
	private GonderimTuru gonderimturu; 
	private java.util.Date gonderimzamani; 
	private java.util.Date gonderimzamaniMax; 
	private EvetHayir zamanlamali; 
	private GonderimDurum durum; 
	private Birim birimRef;
	private String aciklama;
	private String konu;
	
	@Override 
	public void init() { 
		this.rID = null; 	
		this.iletisimlisteRef = null; 	
		this.icerikRef = null; 	
		this.gonderimturu = GonderimTuru._NULL;	
		this.gonderimzamani = null; 	
		this.gonderimzamaniMax = null; 	
		this.zamanlamali = EvetHayir._NULL;	
		this.durum = GonderimDurum._NULL;	
		this.aciklama = null;
		this.birimRef=null;
		this.konu=null;
	} 
	 
	@Override 
	public List<ArrayList<QueryObject>> createQueryCriterias(){ 
		ArrayList<ArrayList<QueryObject>> criteriaList = new ArrayList<ArrayList<QueryObject>>(); 
		ArrayList<QueryObject> kriterler = new ArrayList<QueryObject>();	 
		ArrayList<QueryObject> opsiyonelKriterler = new ArrayList<QueryObject>(); 
		try { 	 
			if(validateQueryObject(getRID(), null, false)){ 
				kriterler.add(new QueryObject("rID", ApplicationConstant._equals, getRID())); 
			} 
			if(validateQueryObject(getIletisimlisteRef(), null, false)){ 
				kriterler.add(new QueryObject("iletisimlisteRef", ApplicationConstant._equals, getIletisimlisteRef())); 
			} 
			if(validateQueryObject(getIcerikRef(), null, false)){ 
				kriterler.add(new QueryObject("icerikRef", ApplicationConstant._equals, getIcerikRef())); 
			} 
			if(validateQueryObject(getGonderimturu(), null, true)){ 
				kriterler.add(new QueryObject("gonderimturu", ApplicationConstant._equals, getGonderimturu())); 
			} 
			if(validateQueryObject(getGonderimzamani(), null, false)  && validateQueryObject(getGonderimzamaniMax(), null, false)){
				kriterler.add(new QueryObject("gonderimzamani", ApplicationConstant._equalsAndLargerThan, getGonderimzamani())); 
				kriterler.add(new QueryObject("gonderimzamani", ApplicationConstant._equalsAndSmallerThan, getGonderimzamaniMax())); 
			} else if(validateQueryObject(getGonderimzamani(), null, false)){ 
				kriterler.add(new QueryObject("gonderimzamani", ApplicationConstant._equalsAndLargerThan, getGonderimzamani()));
			}
			if(validateQueryObject(getZamanlamali(), null, true)){ 
				kriterler.add(new QueryObject("zamanlamali", ApplicationConstant._equals, getZamanlamali())); 
			} 
			if(validateQueryObject(getDurum(), null, true)){ 
				kriterler.add(new QueryObject("durum", ApplicationConstant._equals, getDurum())); 
			} 
			if(validateQueryObject(getAciklama(), null, false)){ 
				kriterler.add(new QueryObject("aciklama", ApplicationConstant._beginsWith, getAciklama(), false)); 
			} 
			if(validateQueryObject(getKonu(), null, false)){ 
				kriterler.add(new QueryObject("konu", ApplicationConstant._beginsWith, getKonu(), false)); 
			}
		} catch(Exception e){ 
			System.out.println("Error @createQueryCriterias method, Class : GonderimFilter :" + e.getMessage()); 
		}  
		criteriaList.add(kriterler); 
		criteriaList.add(opsiyonelKriterler); 
		return criteriaList; 
	} 
	 
	public Long getRID() { 
		return this.rID; 
	} 
	 
	public void setRID(Long rID) { 
		this.rID = rID; 
	} 
  
	public Iletisimliste getIletisimlisteRef() { 
		return this.iletisimlisteRef; 
	} 
	 
	public void setIletisimlisteRef(Iletisimliste iletisimlisteRef) { 
		this.iletisimlisteRef = iletisimlisteRef; 
	} 
  
	public Icerik getIcerikRef() { 
		return this.icerikRef; 
	} 
	 
	public void setIcerikRef(Icerik icerikRef) { 
		this.icerikRef = icerikRef; 
	} 
  
	public GonderimTuru getGonderimturu() { 
		return this.gonderimturu; 
	} 
	 
	public void setGonderimturu(GonderimTuru gonderimturu) { 
		this.gonderimturu = gonderimturu; 
	} 
  
	public Date getGonderimzamani() { 
		return this.gonderimzamani; 
	} 
	 
	public void setGonderimzamani(Date gonderimzamani) { 
		this.gonderimzamani = gonderimzamani; 
	} 
  
	public Date getGonderimzamaniMax() { 
		return this.gonderimzamaniMax; 
	} 
	 
	public void setGonderimzamaniMax(Date gonderimzamaniMax) { 
		this.gonderimzamaniMax = gonderimzamaniMax; 
	} 
  
	public EvetHayir getZamanlamali() { 
		return this.zamanlamali; 
	} 
	 
	public void setZamanlamali(EvetHayir zamanlamali) { 
		this.zamanlamali = zamanlamali; 
	} 
  
	public GonderimDurum getDurum() { 
		return this.durum; 
	} 
	 
	public void setDurum(GonderimDurum durum) { 
		this.durum = durum; 
	} 
  
	public String getAciklama() { 
		return this.aciklama; 
	} 
	 
	public void setAciklama(String aciklama) { 
		this.aciklama = aciklama; 
	}

	public Birim getBirimRef() {
		return birimRef;
	}

	public void setBirimRef(Birim birimRef) {
		this.birimRef = birimRef;
	}

	public String getKonu() {
		return konu;
	}

	public void setKonu(String konu) {
		this.konu = konu;
	}
} 
