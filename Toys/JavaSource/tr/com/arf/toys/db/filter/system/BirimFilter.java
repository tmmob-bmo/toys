package tr.com.arf.toys.db.filter.system; 
 
import java.util.ArrayList;
import java.util.List;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.framework.db.model.query.QueryObject;
import tr.com.arf.toys.db.enumerated.system.BirimDurum;
import tr.com.arf.toys.db.enumerated.system.BirimTip;
import tr.com.arf.toys.db.enumerated.system.BirimYetkiTuru;
import tr.com.arf.toys.db.model.kisi.Kurum;
import tr.com.arf.toys.db.model.system.Birim;
import tr.com.arf.toys.db.model.system.Ilce;
import tr.com.arf.toys.db.model.system.Sehir;
import tr.com.arf.toys.service.application.ManagedBeanLocator;
import tr.com.arf.toys.utility.application.ApplicationConstant;
import tr.com.arf.toys.view.controller._common.SessionUser;
 
 
public class BirimFilter extends BaseFilter { 
	
private static final long serialVersionUID = 1L; 
	
	private Birim ustRef; 
	private Kurum kurumRef; 
	private String ad; 
	private String kisaad; 
	private BirimDurum durum; 
	private String erisimKodu; 
	private Sehir sehirRef; 
	private Ilce ilceRef; 
	private BirimTip birimtip; 
	private Long rid;
	
	
	@Override 
	public void init() { 
		this.ustRef = null; 	
		this.ad = null; 	
		this.kisaad = null; 	
		this.durum = BirimDurum._NULL;	
		this.erisimKodu = null; 	
		this.sehirRef = null; 	
		this.ilceRef = null; 	
		this.kurumRef = null;
		this.birimtip = BirimTip._NULL;	
	} 
	 
	@Override 
	public List<ArrayList<QueryObject>> createQueryCriterias(){ 
		ArrayList<ArrayList<QueryObject>> criteriaList = new ArrayList<ArrayList<QueryObject>>(); 
		ArrayList<QueryObject> kriterler = new ArrayList<QueryObject>();	 
		ArrayList<QueryObject> opsiyonelKriterler = new ArrayList<QueryObject>(); 
		try { 
			if(validateQueryObject(getRid(), 0, false)){ 
				kriterler.add(new QueryObject("rID", ApplicationConstant._equals, getRid())); 
			} else {
				if(validateQueryObject(getUstRef(), null, false)){ 
					kriterler.add(new QueryObject("ustRef", ApplicationConstant._equals, getUstRef())); 
				} 
				if(validateQueryObject(getAd(), null, false)){ 
					kriterler.add(new QueryObject("ad", ApplicationConstant._beginsWith, getAd(), false)); 
				} 
				if(validateQueryObject(getKisaad(), null, false)){ 
					kriterler.add(new QueryObject("kisaad", ApplicationConstant._beginsWith, getKisaad(), false)); 
				} 
				if(validateQueryObject(getDurum(), null, true)){ 
					kriterler.add(new QueryObject("durum", ApplicationConstant._equals, getDurum())); 
				} 
				if(validateQueryObject(getErisimKodu(), null, false)){ 
					kriterler.add(new QueryObject("erisimKodu", ApplicationConstant._beginsWith, getErisimKodu(), true)); 
				} 
				if(validateQueryObject(getKurumRef(), null, false)){ 
					kriterler.add(new QueryObject("kurumRef", ApplicationConstant._equals, getKurumRef())); 
				} 
				if(validateQueryObject(getSehirRef(), null, false)){ 
					kriterler.add(new QueryObject("sehirRef", ApplicationConstant._equals, getSehirRef())); 
				} 
				if(validateQueryObject(getIlceRef(), null, false)){ 
					kriterler.add(new QueryObject("ilceRef", ApplicationConstant._equals, getIlceRef())); 
				} 
				if(validateQueryObject(getBirimtip(), null, true)){ 
					kriterler.add(new QueryObject("birimtip", ApplicationConstant._equals, getBirimtip())); 
				} 
				
				// Kullanicinin birimine gore yetkilendirme yapiliyor.
				SessionUser sessionUser = ManagedBeanLocator.locateSessionUser();
				if(sessionUser.getPersonelRef() != null && sessionUser.getPersonelRef().getBirimRef() != null){
					if(sessionUser.getBirimYetkiTuru() != BirimYetkiTuru._TUMBIRIMLER){
						if(sessionUser.getBirimYetkiTuru() == BirimYetkiTuru._KENDIBIRIMIVEALTBIRIMLERI){ 
							kriterler.add(new QueryObject("erisimKodu", ApplicationConstant._beginsWith, sessionUser.getPersonelRef().getBirimRef().getErisimKodu())); 
						} else if(sessionUser.getBirimYetkiTuru() == BirimYetkiTuru._SADECEKENDIBIRIMLERI){ 
							kriterler.add(new QueryObject("rID", ApplicationConstant._equals, sessionUser.getPersonelRef().getBirimRef().getRID())); 
						}
					}
				}
			}
		} catch(Exception e){ 
			System.out.println("Error @createQueryCriterias method, Class : BirimFilter :" + e.getMessage()); 
		}  
		criteriaList.add(kriterler); 
		criteriaList.add(opsiyonelKriterler); 
		return criteriaList; 
	} 
	 
	public Birim getUstRef() { 
		return this.ustRef; 
	} 
	 
	public void setUstRef(Birim ustRef) { 
		this.ustRef = ustRef; 
	} 
  
	public String getAd() { 
		return this.ad; 
	} 
	 
	public void setAd(String ad) { 
		this.ad = ad; 
	} 
  
	public String getKisaad() { 
		return this.kisaad; 
	} 
	 
	public void setKisaad(String kisaad) { 
		this.kisaad = kisaad; 
	} 
  
	public BirimDurum getDurum() { 
		return this.durum; 
	} 
	 
	public void setDurum(BirimDurum durum) { 
		this.durum = durum; 
	} 
  
	public String getErisimKodu() { 
		return this.erisimKodu; 
	} 
	 
	public void setErisimKodu(String erisimKodu) { 
		this.erisimKodu = erisimKodu; 
	} 
  
	public Sehir getSehirRef() { 
		return this.sehirRef; 
	} 
	 
	public void setSehirRef(Sehir sehirRef) { 
		this.sehirRef = sehirRef; 
	} 
  
	public Ilce getIlceRef() { 
		return this.ilceRef; 
	} 
	 
	public void setIlceRef(Ilce ilceRef) { 
		this.ilceRef = ilceRef; 
	} 
  
	public BirimTip getBirimtip() { 
		return this.birimtip; 
	} 
	 
	public void setBirimtip(BirimTip birimtip) { 
		this.birimtip = birimtip; 
	}

	public Kurum getKurumRef() {
		return kurumRef;
	}

	public void setKurumRef(Kurum kurumRef) {
		this.kurumRef = kurumRef;
	}

	public Long getRid() {
		return rid;
	}

	public void setRid(Long rid) {
		this.rid = rid;
	}

	
} 
