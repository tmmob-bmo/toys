package tr.com.arf.toys.db.filter.system; 
 
import java.util.ArrayList;
import java.util.List;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.framework.db.model.query.QueryObject;
import tr.com.arf.toys.db.model.kisi.Kisi;
import tr.com.arf.toys.utility.application.ApplicationConstant;
 
 
public class KullaniciFilter extends BaseFilter { 
	
private static final long serialVersionUID = 1L; 
	
	private Kisi kisiRef; 
	private String name; 
	private String password; 
	private String ad;
	private String soyad;
	
	@Override 
	public void init() { 
		this.kisiRef = null; 	
		this.name = null; 	
		this.password = null; 	
		this.ad = null;
		this.soyad = null;
	} 
	 
	@Override 
	public List<ArrayList<QueryObject>> createQueryCriterias(){ 
		ArrayList<ArrayList<QueryObject>> criteriaList = new ArrayList<ArrayList<QueryObject>>(); 
		ArrayList<QueryObject> kriterler = new ArrayList<QueryObject>();	 
		ArrayList<QueryObject> opsiyonelKriterler = new ArrayList<QueryObject>(); 
		try { 	 
			if(validateQueryObject(getKisiRef(), null, false)){ 
				kriterler.add(new QueryObject("kisiRef", ApplicationConstant._equals, getKisiRef())); 
			} 
			if(validateQueryObject(getAd(), null, false)){ 
				kriterler.add(new QueryObject("kisiRef.ad", ApplicationConstant._inside, getAd().trim(),false)); 
			}
			if(validateQueryObject(getSoyad(), null, false)){ 
				kriterler.add(new QueryObject("kisiRef.soyad", ApplicationConstant._inside, getSoyad().trim(),false)); 
			} 
			if(validateQueryObject(getName(), null, false)){ 
				kriterler.add(new QueryObject("name", ApplicationConstant._beginsWith, getName())); 
			} 
			if(validateQueryObject(getPassword(), null, false)){ 
				kriterler.add(new QueryObject("password", ApplicationConstant._beginsWith, getPassword())); 
			} 
		} catch(Exception e){ 
			System.out.println("Error @createQueryCriterias method, Class : kullaniciFilter :" + e.getMessage()); 
		}  
		criteriaList.add(kriterler); 
		criteriaList.add(opsiyonelKriterler); 
		return criteriaList; 
	} 
	
	public String getAd() {
		return ad;
	}

	public void setAd(String ad) {
		this.ad = ad;
	}

	public String getSoyad() {
		return soyad;
	}

	public void setSoyad(String soyad) {
		this.soyad = soyad;
	} 
	 
	public Kisi getKisiRef() { 
		return this.kisiRef; 
	} 
	 
	public void setKisiRef(Kisi kisiRef) { 
		this.kisiRef = kisiRef; 
	} 
  
	public String getName() { 
		return this.name; 
	} 
	 
	public void setName(String name) { 
		this.name = name; 
	} 
  
	public String getPassword() { 
		return this.password; 
	} 
	 
	public void setPassword(String password) { 
		this.password = password; 
	} 
  
	
} 
