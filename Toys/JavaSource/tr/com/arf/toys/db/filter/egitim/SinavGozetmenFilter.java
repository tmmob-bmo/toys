package tr.com.arf.toys.db.filter.egitim; 
 
import java.util.ArrayList;
import java.util.List;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.framework.db.model.query.QueryObject;
import tr.com.arf.toys.db.model.egitim.Gozetmen;
import tr.com.arf.toys.db.model.egitim.Sinav;
import tr.com.arf.toys.utility.application.ApplicationConstant;
 
 
public class SinavGozetmenFilter extends BaseFilter { 
	
private static final long serialVersionUID = 1L; 
	
	private Long rID; 
	private Gozetmen gozetmenRef;
	private Sinav sinavRef;
	private String aciklama;
	
	@Override 
	public void init() { 
		this.rID = null; 	
		this.aciklama=null;
		this.gozetmenRef=null;
		this.sinavRef=null;
	} 
	 
	@Override 
	public List<ArrayList<QueryObject>> createQueryCriterias(){ 
		ArrayList<ArrayList<QueryObject>> criteriaList = new ArrayList<ArrayList<QueryObject>>(); 
		ArrayList<QueryObject> kriterler = new ArrayList<QueryObject>();	 
		ArrayList<QueryObject> opsiyonelKriterler = new ArrayList<QueryObject>(); 
		try { 	 
			if(validateQueryObject(getRID(), null, false)){ 
				kriterler.add(new QueryObject("rID", ApplicationConstant._equals, getRID())); 
			} 
			if(validateQueryObject(getSinavRef(), null, false)){ 
				kriterler.add(new QueryObject("sinavRef", ApplicationConstant._equals, getSinavRef())); 
			} 
			if(validateQueryObject(getGozetmenRef(), null, false)){ 
				kriterler.add(new QueryObject("gozetmenRef", ApplicationConstant._equals, getGozetmenRef())); 
			} 
			if(validateQueryObject(getAciklama(), null, false)){ 
				kriterler.add(new QueryObject("aciklama", ApplicationConstant._inside, getAciklama())); 
			} 
		} catch(Exception e){ 
			System.out.println("Error @createQueryCriterias method, Class : GozetmenFilter :" + e.getMessage()); 
		}  
		criteriaList.add(kriterler); 
		criteriaList.add(opsiyonelKriterler); 
		return criteriaList; 
	} 
	 
	public Long getRID() { 
		return this.rID; 
	} 
	 
	public void setRID(Long rID) { 
		this.rID = rID; 
	}

	public String getAciklama() {
		return aciklama;
	}

	public void setAciklama(String aciklama) {
		this.aciklama = aciklama;
	}

	public Gozetmen getGozetmenRef() {
		return gozetmenRef;
	}

	public void setGozetmenRef(Gozetmen gozetmenRef) {
		this.gozetmenRef = gozetmenRef;
	}

	public Sinav getSinavRef() {
		return sinavRef;
	}

	public void setSinavRef(Sinav sinavRef) {
		this.sinavRef = sinavRef;
	}

} 
