package tr.com.arf.toys.db.filter.uye; 
 
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.framework.db.model.query.QueryObject;
import tr.com.arf.toys.db.enumerated.uye.SigortaTip;
import tr.com.arf.toys.db.enumerated.uye.UyeSigortaDurum;
import tr.com.arf.toys.db.model.evrak.Evrak;
import tr.com.arf.toys.db.model.uye.Uye;
import tr.com.arf.toys.utility.application.ApplicationConstant;
 
 
public class UyesigortaFilter extends BaseFilter { 
	
private static final long serialVersionUID = 1L; 
	
	private Uye uyeRef; 
	private SigortaTip sigortatip; 
	private String sigortasirket; 
	private String policeno; 
	private java.util.Date baslangictarih; 
	private java.util.Date baslangictarihMax; 
	private java.util.Date bitistarih; 
	private java.util.Date bitistarihMax; 
	private UyeSigortaDurum uyesigortadurum; 
	private Evrak evrakRef; 
	private String aciklama; 
	
	@Override 
	public void init() { 
		this.uyeRef = null; 	
		this.sigortatip = SigortaTip._NULL;	
		this.sigortasirket = null; 	
		this.policeno = null; 	
		this.baslangictarih = null; 	
		this.baslangictarihMax = null; 	
		this.bitistarih = null; 	
		this.bitistarihMax = null; 	
		this.uyesigortadurum = UyeSigortaDurum._NULL;	
		this.evrakRef = null; 	
		this.aciklama = null; 	
	} 
	 
	@Override 
	public List<ArrayList<QueryObject>> createQueryCriterias(){ 
		ArrayList<ArrayList<QueryObject>> criteriaList = new ArrayList<ArrayList<QueryObject>>(); 
		ArrayList<QueryObject> kriterler = new ArrayList<QueryObject>();	 
		ArrayList<QueryObject> opsiyonelKriterler = new ArrayList<QueryObject>(); 
		try { 	 
			if(validateQueryObject(getUyeRef(), null, false)){ 
				kriterler.add(new QueryObject("uyeRef", ApplicationConstant._equals, getUyeRef())); 
			} 
			if(validateQueryObject(getSigortatip(), null, true)){ 
				kriterler.add(new QueryObject("sigortatip", ApplicationConstant._equals, getSigortatip())); 
			} 
			if(validateQueryObject(getSigortasirket(), null, false)){ 
				kriterler.add(new QueryObject("sigortasirket", ApplicationConstant._beginsWith, getSigortasirket())); 
			} 
			if(validateQueryObject(getPoliceno(), null, false)){ 
				kriterler.add(new QueryObject("policeno", ApplicationConstant._beginsWith, getPoliceno())); 
			} 
			if(validateQueryObject(getBaslangictarih(), null, false)  && validateQueryObject(getBaslangictarihMax(), null, false)){
				kriterler.add(new QueryObject("baslangictarih", ApplicationConstant._equalsAndLargerThan, getBaslangictarih())); 
				kriterler.add(new QueryObject("baslangictarih", ApplicationConstant._equalsAndSmallerThan, getBaslangictarihMax())); 
			} else if(validateQueryObject(getBaslangictarih(), null, false)){ 
				kriterler.add(new QueryObject("baslangictarih", ApplicationConstant._equalsAndLargerThan, getBaslangictarih()));
			}
			if(validateQueryObject(getBitistarih(), null, false)  && validateQueryObject(getBitistarihMax(), null, false)){
				kriterler.add(new QueryObject("bitistarih", ApplicationConstant._equalsAndLargerThan, getBitistarih())); 
				kriterler.add(new QueryObject("bitistarih", ApplicationConstant._equalsAndSmallerThan, getBitistarihMax())); 
			} else if(validateQueryObject(getBitistarih(), null, false)){ 
				kriterler.add(new QueryObject("bitistarih", ApplicationConstant._equalsAndLargerThan, getBitistarih()));
			}
			if(validateQueryObject(getUyesigortadurum(), null, true)){ 
				kriterler.add(new QueryObject("uyesigortadurum", ApplicationConstant._equals, getUyesigortadurum())); 
			} 
			if(validateQueryObject(getEvrakRef(), null, false)){ 
				kriterler.add(new QueryObject("evrakRef", ApplicationConstant._equals, getEvrakRef())); 
			} 
			if(validateQueryObject(getAciklama(), null, false)){ 
				kriterler.add(new QueryObject("aciklama", ApplicationConstant._beginsWith, getAciklama())); 
			} 
		} catch(Exception e){ 
			System.out.println("Error @createQueryCriterias method, Class : UyesigortaFilter :" + e.getMessage()); 
		}  
		criteriaList.add(kriterler); 
		criteriaList.add(opsiyonelKriterler); 
		return criteriaList; 
	} 
	 
	public Uye getUyeRef() { 
		return this.uyeRef; 
	} 
	 
	public void setUyeRef(Uye uyeRef) { 
		this.uyeRef = uyeRef; 
	} 
  
	public SigortaTip getSigortatip() { 
		return this.sigortatip; 
	} 
	 
	public void setSigortatip(SigortaTip sigortatip) { 
		this.sigortatip = sigortatip; 
	} 
  
	public String getSigortasirket() { 
		return this.sigortasirket; 
	} 
	 
	public void setSigortasirket(String sigortasirket) { 
		this.sigortasirket = sigortasirket; 
	} 
  
	public String getPoliceno() { 
		return this.policeno; 
	} 
	 
	public void setPoliceno(String policeno) { 
		this.policeno = policeno; 
	} 
  
	public Date getBaslangictarih() { 
		return this.baslangictarih; 
	} 
	 
	public void setBaslangictarih(Date baslangictarih) { 
		this.baslangictarih = baslangictarih; 
	} 
  
	public Date getBaslangictarihMax() { 
		return this.baslangictarihMax; 
	} 
	 
	public void setBaslangictarihMax(Date baslangictarihMax) { 
		this.baslangictarihMax = baslangictarihMax; 
	} 
  
	public Date getBitistarih() { 
		return this.bitistarih; 
	} 
	 
	public void setBitistarih(Date bitistarih) { 
		this.bitistarih = bitistarih; 
	} 
  
	public Date getBitistarihMax() { 
		return this.bitistarihMax; 
	} 
	 
	public void setBitistarihMax(Date bitistarihMax) { 
		this.bitistarihMax = bitistarihMax; 
	} 
  
	public UyeSigortaDurum getUyesigortadurum() { 
		return this.uyesigortadurum; 
	} 
	 
	public void setUyesigortadurum(UyeSigortaDurum uyesigortadurum) { 
		this.uyesigortadurum = uyesigortadurum; 
	} 
  
	public Evrak getEvrakRef() { 
		return this.evrakRef; 
	} 
	 
	public void setEvrakRef(Evrak evrakRef) { 
		this.evrakRef = evrakRef; 
	} 
  
	public String getAciklama() { 
		return this.aciklama; 
	} 
	 
	public void setAciklama(String aciklama) { 
		this.aciklama = aciklama; 
	} 
  
	
} 
