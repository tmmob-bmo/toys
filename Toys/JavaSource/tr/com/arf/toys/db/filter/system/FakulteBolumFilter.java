package tr.com.arf.toys.db.filter.system; 
 
import java.util.ArrayList;
import java.util.List;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.framework.db.model.query.QueryObject;
import tr.com.arf.toys.db.model.system.Bolum;
import tr.com.arf.toys.db.model.system.Fakulte;
import tr.com.arf.toys.utility.application.ApplicationConstant;
 
 
public class FakulteBolumFilter extends BaseFilter { 
	
private static final long serialVersionUID = 1L; 
	
	private Fakulte fakulteRef;
	private Bolum bolumRef;
	
	@Override 
	public void init() { 
		this.bolumRef=null;
		this.fakulteRef=null;
	} 
	 
	@Override 
	public List<ArrayList<QueryObject>> createQueryCriterias(){ 
		ArrayList<ArrayList<QueryObject>> criteriaList = new ArrayList<ArrayList<QueryObject>>(); 
		ArrayList<QueryObject> kriterler = new ArrayList<QueryObject>();	 
		ArrayList<QueryObject> opsiyonelKriterler = new ArrayList<QueryObject>(); 
		try { 	 
			
			if(validateQueryObject(getFakulteRef(), null, false)){ 
				kriterler.add(new QueryObject("fakulteRef", ApplicationConstant._equals, getFakulteRef())); 
			}
			if(validateQueryObject(getBolumRef(), null, false)){ 
				kriterler.add(new QueryObject("bolumRef", ApplicationConstant._equals, getBolumRef())); 
			}
			
		} catch(Exception e){ 
			System.out.println("Error @createQueryCriterias method, Class : FakulteFilter :" + e.getMessage()); 
		}  
		criteriaList.add(kriterler); 
		criteriaList.add(opsiyonelKriterler); 
		return criteriaList; 
	}

	public Fakulte getFakulteRef() {
		return fakulteRef;
	}

	public void setFakulteRef(Fakulte fakulteRef) {
		this.fakulteRef = fakulteRef;
	}

	public Bolum getBolumRef() {
		return bolumRef;
	}

	public void setBolumRef(Bolum bolumRef) {
		this.bolumRef = bolumRef;
	}
} 
