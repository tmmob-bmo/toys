package tr.com.arf.toys.db.filter.finans;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.framework.db.model.query.QueryObject;
import tr.com.arf.toys.db.enumerated.uye.OdemeSekli;
import tr.com.arf.toys.db.model.kisi.Kisi;
import tr.com.arf.toys.db.model.system.Birim;
import tr.com.arf.toys.utility.application.ApplicationConstant;

public class OdemeFilter extends BaseFilter {

	private static final long serialVersionUID = 1L;

	private Kisi kisiRef;
	private double miktar;
	private java.util.Date tarih;
	private java.util.Date tarihMax;
	private String ad;
	private String soyad;
	private java.util.Date makbuzbasilmatarihi;
	private java.util.Date makbuzbasilmatarihiMax;
	private Birim birimRef;
	private BigDecimal makbuzno;
	private OdemeSekli odemeSekli;
	private String dekontNo;
	private String kimlikno;

	@Override
	public void init() {
		this.kisiRef = null;
		this.miktar = 0;
		this.tarih = null;
		this.tarihMax = null;
		this.ad = null;
		this.soyad = null;
		this.makbuzbasilmatarihi = null;
		this.makbuzbasilmatarihiMax = null;
		this.birimRef = null;
		this.makbuzno = null;
		this.odemeSekli = OdemeSekli._BANKADAN;
		this.dekontNo = null;
		this.kimlikno = null;
	}

	@Override
	public List<ArrayList<QueryObject>> createQueryCriterias() {
		ArrayList<ArrayList<QueryObject>> criteriaList = new ArrayList<ArrayList<QueryObject>>();
		ArrayList<QueryObject> kriterler = new ArrayList<QueryObject>();
		ArrayList<QueryObject> opsiyonelKriterler = new ArrayList<QueryObject>();
		try {
			if (validateQueryObject(getKisiRef(), null, false)) {
				kriterler.add(new QueryObject("kisiRef", ApplicationConstant._equals, getKisiRef()));
			}
			if (validateQueryObject(getAd(), null, false)) {
				kriterler.add(new QueryObject("kisiRef.ad", ApplicationConstant._beginsWith, getAd()));
			}
			if (validateQueryObject(getSoyad(), null, false)) {
				kriterler.add(new QueryObject("kisiRef.soyad", ApplicationConstant._beginsWith, getSoyad()));
			}
			if (validateQueryObject(getMiktar(), 0, false)) {
				kriterler.add(new QueryObject("miktar", ApplicationConstant._equals, getMiktar()));
			}
			if (validateQueryObject(getTarih(), null, false) && validateQueryObject(getTarihMax(), null, false)) {
				kriterler.add(new QueryObject("tarih", ApplicationConstant._equalsAndLargerThan, getTarih()));
				kriterler.add(new QueryObject("tarih", ApplicationConstant._equalsAndSmallerThan, getTarihMax()));
			} else if (validateQueryObject(getTarih(), null, false)) {
				kriterler.add(new QueryObject("tarih", ApplicationConstant._equalsAndLargerThan, getTarih()));
			}
			if (validateQueryObject(getMakbuzbasilmatarihi(), null, false) && validateQueryObject(getMakbuzbasilmatarihiMax(), null, false)) {
				kriterler.add(new QueryObject("makbuzbasilmatarihi", ApplicationConstant._equalsAndLargerThan, getMakbuzbasilmatarihi()));
				kriterler.add(new QueryObject("makbuzbasilmatarihi", ApplicationConstant._equalsAndSmallerThan, getMakbuzbasilmatarihiMax()));
			} else if (validateQueryObject(getMakbuzbasilmatarihi(), null, false)) {
				kriterler.add(new QueryObject("makbuzbasilmatarihi", ApplicationConstant._equalsAndLargerThan, getMakbuzbasilmatarihi()));
			}
			if (validateQueryObject(getBirimRef(), null, false)) {
				kriterler.add(new QueryObject("birimRef", ApplicationConstant._equals, getBirimRef()));
			}
			if (validateQueryObject(getMakbuzno(), 0, false)) {
				kriterler.add(new QueryObject("makbuzno", ApplicationConstant._equals, getMakbuzno()));
			}
			if (validateQueryObject(getOdemeSekli(), null, true)) {
				kriterler.add(new QueryObject("odemeSekli", ApplicationConstant._equals, getOdemeSekli()));
			}
			if (validateQueryObject(getDekontNo(), null, false)) {
				kriterler.add(new QueryObject("dekontNo", ApplicationConstant._beginsWith, getDekontNo(), false));
			}
			if (validateQueryObject(getKimlikno(), null, false)) { 
				kriterler.add(new QueryObject(" o.kisiRef.kimlikno||'' LIKE '" + getKimlikno() + "%'"));
			}
		} catch (Exception e) {
			System.out.println("Error @createQueryCriterias method, Class : OdemeFilter :" + e.getMessage());
		}
		criteriaList.add(kriterler);
		criteriaList.add(opsiyonelKriterler);
		return criteriaList;
	}

	public String getAd() {
		return ad;
	}

	public void setAd(String ad) {
		this.ad = ad;
	}

	public String getSoyad() {
		return soyad;
	}

	public void setSoyad(String soyad) {
		this.soyad = soyad;
	}

	public Kisi getKisiRef() {
		return this.kisiRef;
	}

	public void setKisiRef(Kisi kisiRef) {
		this.kisiRef = kisiRef;
	}

	public double getMiktar() {
		return this.miktar;
	}

	public void setMiktar(double miktar) {
		this.miktar = miktar;
	}

	public Date getTarih() {
		return this.tarih;
	}

	public void setTarih(Date tarih) {
		this.tarih = tarih;
	}

	public Date getTarihMax() {
		return this.tarihMax;
	}

	public void setTarihMax(Date tarihMax) {
		this.tarihMax = tarihMax;
	}

	public java.util.Date getMakbuzbasilmatarihi() {
		return makbuzbasilmatarihi;
	}

	public void setMakbuzbasilmatarihi(java.util.Date makbuzbasilmatarihi) {
		this.makbuzbasilmatarihi = makbuzbasilmatarihi;
	}

	public java.util.Date getMakbuzbasilmatarihiMax() {
		return makbuzbasilmatarihiMax;
	}

	public void setMakbuzbasilmatarihiMax(java.util.Date makbuzbasilmatarihiMax) {
		this.makbuzbasilmatarihiMax = makbuzbasilmatarihiMax;
	}

	public Birim getBirimRef() {
		return birimRef;
	}

	public void setBirimRef(Birim birimRef) {
		this.birimRef = birimRef;
	}

	public BigDecimal getMakbuzno() {
		return makbuzno;
	}

	public void setMakbuzno(BigDecimal makbuzno) {
		this.makbuzno = makbuzno;
	}

	public OdemeSekli getOdemeSekli() {
		return odemeSekli;
	}

	public void setOdemeSekli(OdemeSekli odemeSekli) {
		this.odemeSekli = odemeSekli;
	}

	public String getDekontNo() {
		return dekontNo;
	}

	public void setDekontNo(String dekontNo) {
		this.dekontNo = dekontNo;
	}

	public String getKimlikno() {
		return kimlikno;
	}

	public void setKimlikno(String kimlikno) {
		this.kimlikno = kimlikno;
	}
}
