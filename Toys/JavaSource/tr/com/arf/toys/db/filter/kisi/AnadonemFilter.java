package tr.com.arf.toys.db.filter.kisi; 
 
import java.util.ArrayList;
import java.util.List;

import tr.com.arf.framework.db.enumerated._common.EvetHayir;
import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.framework.db.model.query.QueryObject;
import tr.com.arf.toys.utility.application.ApplicationConstant;
 
 
public class AnadonemFilter extends BaseFilter { 
	
private static final long serialVersionUID = 1L; 
	
	private Long rID; 
	private String tanim; 
	private EvetHayir aktif;
	
	@Override 
	public void init() { 
		this.rID = null; 	
		this.tanim = null; 	
		this.aktif=null;
	} 
	 
	@Override 
	public List<ArrayList<QueryObject>> createQueryCriterias(){ 
		ArrayList<ArrayList<QueryObject>> criteriaList = new ArrayList<ArrayList<QueryObject>>(); 
		ArrayList<QueryObject> kriterler = new ArrayList<QueryObject>();	 
		ArrayList<QueryObject> opsiyonelKriterler = new ArrayList<QueryObject>(); 
		try { 	 
			if(validateQueryObject(getRID(), null, false)){ 
				kriterler.add(new QueryObject("rID", ApplicationConstant._equals, getRID())); 
			} 
			if(validateQueryObject(getTanim(), null, false)){ 
				kriterler.add(new QueryObject("tanim", ApplicationConstant._beginsWith, getTanim(), false)); 
			} 
			if(validateQueryObject(getAktif(), null, false)){ 
				kriterler.add(new QueryObject("aktif", ApplicationConstant._beginsWith, getAktif(), false)); 
			} 
		} catch(Exception e){ 
			System.out.println("Error @createQueryCriterias method, Class : AnadonemFilter :" + e.getMessage()); 
		}  
		criteriaList.add(kriterler); 
		criteriaList.add(opsiyonelKriterler); 
		return criteriaList; 
	} 
	 
	public Long getRID() { 
		return this.rID; 
	} 
	 
	public void setRID(Long rID) { 
		this.rID = rID; 
	} 
  
	public String getTanim() { 
		return this.tanim; 
	} 
	 
	public void setTanim(String tanim) { 
		this.tanim = tanim; 
	}

	public EvetHayir getAktif() {
		return aktif;
	}

	public void setAktif(EvetHayir aktif) {
		this.aktif = aktif;
	} 
	
  
	
} 
