package tr.com.arf.toys.db.filter.kisi; 
 
import java.util.ArrayList;
import java.util.List;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.framework.db.model.query.QueryObject;
import tr.com.arf.toys.db.enumerated.kisi.KisiUyrukTip;
import tr.com.arf.toys.db.model.kisi.Kisi;
import tr.com.arf.toys.db.model.system.Ulke;
import tr.com.arf.toys.utility.application.ApplicationConstant;
 
 
public class KisiuyrukFilter extends BaseFilter { 
	
private static final long serialVersionUID = 1L; 
	
	private Kisi kisiRef; 
	
	private Ulke ulkeRef; 
	private KisiUyrukTip uyruktip; 
	
	@Override 
	public void init() { 
		this.kisiRef = null; 	
		this.ulkeRef = null; 	
		this.uyruktip = KisiUyrukTip._NULL; 	
	} 
	 
	@Override 
	public List<ArrayList<QueryObject>> createQueryCriterias(){ 
		ArrayList<ArrayList<QueryObject>> criteriaList = new ArrayList<ArrayList<QueryObject>>(); 
		ArrayList<QueryObject> kriterler = new ArrayList<QueryObject>();	 
		ArrayList<QueryObject> opsiyonelKriterler = new ArrayList<QueryObject>(); 
		try { 	 
			if(validateQueryObject(getKisiRef(), null, false)){ 
				kriterler.add(new QueryObject("kisiRef", ApplicationConstant._equals, getKisiRef())); 
			} 
			if(validateQueryObject(getUlkeRef(), null, false)){ 
				kriterler.add(new QueryObject("ulkeRef", ApplicationConstant._equals, getUlkeRef())); 
			} 
			if(validateQueryObject(getUyruktip(), null, false)){
				kriterler.add(new QueryObject("uyruktip", ApplicationConstant._equals, getUyruktip())); 	
			}
		} catch(Exception e){ 
			System.out.println("Error @createQueryCriterias method, Class : KisiuyrukFilter :" + e.getMessage()); 
		}  
		criteriaList.add(kriterler); 
		criteriaList.add(opsiyonelKriterler); 
		return criteriaList; 
	} 
	 
	public Kisi getKisiRef() { 
		return this.kisiRef; 
	} 
	 
	public void setKisiRef(Kisi kisiRef) { 
		this.kisiRef = kisiRef; 
	} 
  
	public Ulke getUlkeRef() { 
		return this.ulkeRef; 
	} 
	 
	public void setUlkeRef(Ulke ulkeRef) { 
		this.ulkeRef = ulkeRef; 
	} 
  
	public KisiUyrukTip getUyruktip() { 
		return this.uyruktip; 
	} 
	 
	public void setUyruktip(KisiUyrukTip uyruktip) { 
		this.uyruktip = uyruktip; 
	} 
  
	
} 
