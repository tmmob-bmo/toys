package tr.com.arf.toys.db.filter.egitim; 
 
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import tr.com.arf.framework.db.enumerated._common.EvetHayir;
import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.framework.db.model.query.QueryObject;
import tr.com.arf.toys.db.enumerated.egitim.EgitimDurum;
import tr.com.arf.toys.db.enumerated.system.BirimYetkiTuru;
import tr.com.arf.toys.db.model.egitim.Egitimtanim;
import tr.com.arf.toys.db.model.system.Birim;
import tr.com.arf.toys.service.application.ManagedBeanLocator;
import tr.com.arf.toys.utility.application.ApplicationConstant;
import tr.com.arf.toys.view.controller._common.SessionUser;
 
 
public class EgitimFilter extends BaseFilter { 
	
private static final long serialVersionUID = 1L; 
	
	private Egitimtanim egitimtanimRef; 
	
	private Long rID; 
	private String ad; 
	private Birim birimRef; 
	private String yer; 
	private int kontenjan; 
	private EgitimDurum durum; 
	private BigDecimal sure;
	private java.util.Date baslangictarih; 
	private java.util.Date bitistarih;
	private EvetHayir yayinda;
	private String egitimkodu;
	
	@Override 
	public void init() { 
		this.rID = null; 	
		this.egitimtanimRef = null; 	
		this.ad = null; 	
		this.birimRef = null; 	
		this.yer = null; 	
		this.kontenjan = 0; 	
		this.durum = EgitimDurum._NULL;	
		this.sure = null;
		this.baslangictarih = null; 		
		this.bitistarih = null;
		this.yayinda=EvetHayir._NULL;
		this.egitimkodu=null;
	} 
	 
	@Override 
	public List<ArrayList<QueryObject>> createQueryCriterias(){ 
		ArrayList<ArrayList<QueryObject>> criteriaList = new ArrayList<ArrayList<QueryObject>>(); 
		ArrayList<QueryObject> kriterler = new ArrayList<QueryObject>();	 
		ArrayList<QueryObject> opsiyonelKriterler = new ArrayList<QueryObject>(); 
		try { 	 
			if(validateQueryObject(getRID(), null, false)){ 
				kriterler.add(new QueryObject("rID", ApplicationConstant._equals, getRID())); 
			} 
			if(validateQueryObject(getEgitimtanimRef(), null, false)){ 
				kriterler.add(new QueryObject("egitimtanimRef", ApplicationConstant._equals, getEgitimtanimRef())); 
			} 
			if(validateQueryObject(getAd(), null, false)){ 
				kriterler.add(new QueryObject("ad", ApplicationConstant._beginsWith, getAd(), false)); 
			} 
			if(validateQueryObject(getBirimRef(), null, false)){ 
				kriterler.add(new QueryObject("birimRef", ApplicationConstant._equals, getBirimRef())); 
			} 
			if(validateQueryObject(getYer(), null, false)){ 
				kriterler.add(new QueryObject("yer", ApplicationConstant._beginsWith, getYer(), false)); 
			} 
			if(validateQueryObject(getKontenjan(), 0, false)){
				kriterler.add(new QueryObject("kontenjan", ApplicationConstant._equals, getKontenjan())); 	
			}
			if(validateQueryObject(getDurum(), null, true)){ 
				kriterler.add(new QueryObject("durum", ApplicationConstant._equals, getDurum())); 
			} 
			if(validateQueryObject(getYayinda(), null, true)){ 
				kriterler.add(new QueryObject("yayinda", ApplicationConstant._equals, getYayinda())); 
			} 
			if(validateQueryObject(getSure(), null, true)){
				kriterler.add(new QueryObject("egitimtanimRef.sure", ApplicationConstant._equals, getSure())); 	
			}
			if(validateQueryObject(getBaslangictarih(), null, false)){ 
				kriterler.add(new QueryObject("baslangictarih", ApplicationConstant._equalsAndLargerThan, getBaslangictarih()));
			}
			if(validateQueryObject(getBitistarih(), null, false)){ 
				kriterler.add(new QueryObject("bitistarih", ApplicationConstant._equalsAndLargerThan, getBitistarih()));
			}	
			if(validateQueryObject(getEgitimkodu(), null, false)){ 
				kriterler.add(new QueryObject("egitimkodu", ApplicationConstant._beginsWith, getEgitimkodu(), false)); 
			} 
			// Kullanicinin birimine gore yetkilendirme yapiliyor.
			SessionUser sessionUser = ManagedBeanLocator.locateSessionUser();
			if(sessionUser.getPersonelRef() != null && sessionUser.getPersonelRef().getBirimRef() != null){
				if(sessionUser.getBirimYetkiTuru() != BirimYetkiTuru._TUMBIRIMLER){
					if(sessionUser.getBirimYetkiTuru() == BirimYetkiTuru._KENDIBIRIMIVEALTBIRIMLERI){ 
						kriterler.add(new QueryObject("birimRef.erisimKodu", ApplicationConstant._beginsWith, sessionUser.getPersonelRef().getBirimRef().getErisimKodu())); 
					} else if(sessionUser.getBirimYetkiTuru() == BirimYetkiTuru._SADECEKENDIBIRIMLERI){ 
						kriterler.add(new QueryObject("birimRef", ApplicationConstant._equals, sessionUser.getPersonelRef().getBirimRef())); 
					}
				}
			}
		} catch(Exception e){ 
			System.out.println("Error @createQueryCriterias method, Class : EgitimFilter :" + e.getMessage()); 
		}  
		criteriaList.add(kriterler); 
		criteriaList.add(opsiyonelKriterler); 
		return criteriaList; 
	} 
	 
	public Long getRID() { 
		return this.rID; 
	} 
	 
	public void setRID(Long rID) { 
		this.rID = rID; 
	} 
  
	public Egitimtanim getEgitimtanimRef() { 
		return this.egitimtanimRef; 
	} 
	 
	public void setEgitimtanimRef(Egitimtanim egitimtanimRef) { 
		this.egitimtanimRef = egitimtanimRef; 
	} 
  
	public String getAd() { 
		return this.ad; 
	} 
	 
	public void setAd(String ad) { 
		this.ad = ad; 
	} 
  
	public Birim getBirimRef() { 
		return this.birimRef; 
	} 
	 
	public void setBirimRef(Birim birimRef) { 
		this.birimRef = birimRef; 
	} 
  
	public String getYer() { 
		return this.yer; 
	} 
	 
	public void setYer(String yer) { 
		this.yer = yer; 
	} 
  
	public int getKontenjan() { 
		return this.kontenjan; 
	} 
	 
	public void setKontenjan(int kontenjan) { 
		this.kontenjan = kontenjan; 
	} 
  
	public EgitimDurum getDurum() { 
		return this.durum; 
	} 
	 
	public void setDurum(EgitimDurum durum) { 
		this.durum = durum; 
	}
	public BigDecimal getSure() { 
		return this.sure; 
	} 
	 
	public void setSure(BigDecimal sure) { 
		this.sure = sure; 
	}
	public Date getBaslangictarih() { 
		return this.baslangictarih; 
	} 
	 
	public void setBaslangictarih(Date baslangictarih) { 
		this.baslangictarih = baslangictarih; 
	} 
  
	public Date getBitistarih() { 
		return this.bitistarih; 
	} 
	 
	public void setBitistarih(Date bitistarih) { 
		this.bitistarih = bitistarih; 
	}

	public EvetHayir getYayinda() {
		return yayinda;
	}

	public void setYayinda(EvetHayir yayinda) {
		this.yayinda = yayinda;
	}

	public String getEgitimkodu() {
		return egitimkodu;
	}

	public void setEgitimkodu(String egitimkodu) {
		this.egitimkodu = egitimkodu;
	} 
	
} 
