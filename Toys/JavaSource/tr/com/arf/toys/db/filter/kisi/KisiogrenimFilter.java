package tr.com.arf.toys.db.filter.kisi; 
 
import java.util.ArrayList;
import java.util.List;

import tr.com.arf.framework.db.enumerated._common.EvetHayir;
import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.framework.db.model.query.QueryObject;
import tr.com.arf.toys.db.enumerated.uye.OgrenimTip;
import tr.com.arf.toys.db.model.kisi.Kisi;
import tr.com.arf.toys.db.model.system.Bolum;
import tr.com.arf.toys.db.model.system.Fakulte;
import tr.com.arf.toys.db.model.system.Universite;
import tr.com.arf.toys.utility.application.ApplicationConstant;
 
 
public class KisiogrenimFilter extends BaseFilter { 
	
private static final long serialVersionUID = 1L; 
	
	private Kisi kisiRef; 
	private Universite universiteRef; 
	private Fakulte fakulteRef; 
	private Bolum bolumRef; 
	private String aciklama; 
	private String lisansunvan;
	private String diplomano;
	private EvetHayir denklikdurum;
	private EvetHayir varsayilan;
	private int mezuniyettarihi;
	private OgrenimTip ogrenimtip; 
	private ArrayList<OgrenimTip> ogrenimtiplistesi; 
	public int listsize;
	
	
	
	@Override 
	public void init() { 
		this.kisiRef = null; 	
		this.ogrenimtip = OgrenimTip._NULL; 	
		this.universiteRef = null; 	 
		this.aciklama = null; 	
		this.bolumRef=null;
		this.denklikdurum=EvetHayir._NULL;
		this.diplomano=null;
		this.lisansunvan=null;
		this.varsayilan=EvetHayir._NULL;
		this.fakulteRef=null;
		this.mezuniyettarihi=0;
		this.ogrenimtiplistesi=null;
		this.listsize=0;
	} 
	 
	@Override 
	public List<ArrayList<QueryObject>> createQueryCriterias(){ 
		ArrayList<ArrayList<QueryObject>> criteriaList = new ArrayList<ArrayList<QueryObject>>(); 
		ArrayList<QueryObject> kriterler = new ArrayList<QueryObject>();	 
		ArrayList<QueryObject> opsiyonelKriterler = new ArrayList<QueryObject>(); 
		String wherecond="";
		String orCond="";
		int sayac=0;
		try { 	 
			if(validateQueryObject(getKisiRef(), null, false)){ 
				kriterler.add(new QueryObject("kisiRef", ApplicationConstant._equals, getKisiRef())); 
			} 
			if(validateQueryObject(getOgrenimtip(),null, false)){
				kriterler.add(new QueryObject("ogrenimtip", ApplicationConstant._equals, getOgrenimtip())); 	
			}
			if(validateQueryObject(getUniversiteRef(), null, false)){ 
				kriterler.add(new QueryObject("universiteRef", ApplicationConstant._equals, getUniversiteRef())); 
			}
			if(validateQueryObject(getFakulteRef(), null, false)){ 
				kriterler.add(new QueryObject("fakulteRef", ApplicationConstant._equals, getFakulteRef())); 
			}			
			if(validateQueryObject(getBolumRef(), null, false)){ 
				kriterler.add(new QueryObject("bolumRef", ApplicationConstant._equals, getBolumRef())); 
			} 
			if(validateQueryObject(getAciklama(), null, false)){ 
				kriterler.add(new QueryObject("aciklama", ApplicationConstant._beginsWith, getAciklama())); 
			} 			
			if(validateQueryObject(getLisansunvan(), null, false)){ 
				kriterler.add(new QueryObject("lisansunvan", ApplicationConstant._beginsWith, getLisansunvan())); 
			} 
			if(validateQueryObject(getDiplomano(), null, false)){ 
				kriterler.add(new QueryObject("diplomano", ApplicationConstant._beginsWith, getDiplomano())); 
			} 
			if(validateQueryObject(getDenklikdurum(), null, true)){ 
				kriterler.add(new QueryObject("denklikdurum", ApplicationConstant._equals, getDenklikdurum())); 
			} 
			if (getOgrenimtiplistesi()!=null){
				if (!getOgrenimtiplistesi().isEmpty() && getOgrenimtiplistesi().size()!=0){
					listsize=getOgrenimtiplistesi().size();
					if (listsize>1){
						orCond=" OR ";
					} 
					for (OgrenimTip ogrenimTp : getOgrenimtiplistesi()){
						 if(validateQueryObject(ogrenimTp, null, false)){ 
							 if (listsize==1){
								 if (ogrenimTp.getCode()==OgrenimTip._DOKTORA.getCode()){
									 wherecond=wherecond+" o.ogrenimtip =" + OgrenimTip._DOKTORA.getCode();
						         } 
								else if (ogrenimTp.getCode()==OgrenimTip._YUKSEKLISANS.getCode()){
					                wherecond=wherecond+" o.ogrenimtip =" + OgrenimTip._YUKSEKLISANS.getCode();
								}  
							 }
							 else if (listsize>1){
								 sayac++;
									 if (sayac==1){
										 if (ogrenimTp.getCode()==OgrenimTip._DOKTORA.getCode() ){
											 wherecond=wherecond+" ( o.ogrenimtip =" + OgrenimTip._DOKTORA.getCode() + orCond;
										 } else if (ogrenimTp.getCode()==OgrenimTip._YUKSEKLISANS.getCode()){
								              wherecond=wherecond+" ( o.ogrenimtip =" + OgrenimTip._YUKSEKLISANS.getCode() + orCond;
										 }  
									 }else if (sayac==listsize){
										 if (ogrenimTp.getCode()==OgrenimTip._DOKTORA.getCode() ){
											 wherecond=wherecond+" o.ogrenimtip =" + OgrenimTip._DOKTORA.getCode()+ " ) ";
											 kriterler.add(new QueryObject(wherecond)); 
											 } else if (ogrenimTp.getCode()==OgrenimTip._YUKSEKLISANS.getCode()){
												 wherecond=wherecond+" o.ogrenimtip =" + OgrenimTip._YUKSEKLISANS.getCode() + " ) ";
												 kriterler.add(new QueryObject(wherecond)); 
											 }
									 } else {
										 if (ogrenimTp.getCode()==OgrenimTip._DOKTORA.getCode() ){
											 wherecond=wherecond+" o.ogrenimtip =" + OgrenimTip._DOKTORA.getCode()+ orCond;
											 } else if (ogrenimTp.getCode()==OgrenimTip._YUKSEKLISANS.getCode()){
									             wherecond= wherecond+ " o.ogrenimtip =" + OgrenimTip._YUKSEKLISANS.getCode() +orCond;
											 }
									 }
							 }
						}
					}
				}
			}
			if(validateQueryObject(getDenklikdurum(), null, true)){ 
				kriterler.add(new QueryObject("denklikdurum", ApplicationConstant._equals, getDenklikdurum())); 
			} 
		} catch(Exception e){ 
			System.out.println("Error @createQueryCriterias method, Class : KisiogrenimFilter :" + e.getMessage()); 
		}  
		criteriaList.add(kriterler); 
		criteriaList.add(opsiyonelKriterler); 
		return criteriaList; 
	} 
	 
	public Kisi getKisiRef() {
		return kisiRef;
	}

	public void setKisiRef(Kisi kisiRef) {
		this.kisiRef = kisiRef;
	}

	public OgrenimTip getOgrenimtip() {
		return ogrenimtip;
	}

	public void setOgrenimtip(OgrenimTip ogrenimtip) {
		this.ogrenimtip = ogrenimtip;
	}

	public Universite getUniversiteRef() {
		return universiteRef;
	}

	public void setUniversiteRef(Universite universiteRef) {
		this.universiteRef = universiteRef;
	} 
  
	public String getAciklama() { 
		return this.aciklama; 
	} 
	 
	public void setAciklama(String aciklama) { 
		this.aciklama = aciklama; 
	}

	public Fakulte getFakulteRef() {
		return fakulteRef;
	}

	public void setFakulteRef(Fakulte fakulteRef) {
		this.fakulteRef = fakulteRef;
	}

	public Bolum getBolumRef() {
		return bolumRef;
	}

	public void setBolumRef(Bolum bolumRef) {
		this.bolumRef = bolumRef;
	}

	public String getLisansunvan() {
		return lisansunvan;
	}

	public void setLisansunvan(String lisansunvan) {
		this.lisansunvan = lisansunvan;
	}

	public String getDiplomano() {
		return diplomano;
	}

	public void setDiplomano(String diplomano) {
		this.diplomano = diplomano;
	} 

	public EvetHayir getDenklikdurum() {
		return denklikdurum;
	}

	public void setDenklikdurum(EvetHayir denklikdurum) {
		this.denklikdurum = denklikdurum;
	}

	public EvetHayir getVarsayilan() {
		return varsayilan;
	}

	public void setVarsayilan(EvetHayir varsayilan) {
		this.varsayilan = varsayilan;
	}

	public int getMezuniyettarihi() {
		return mezuniyettarihi;
	}

	public void setMezuniyettarihi(int mezuniyettarihi) {
		this.mezuniyettarihi = mezuniyettarihi;
	}

	public ArrayList<OgrenimTip> getOgrenimtiplistesi() {
		return ogrenimtiplistesi;
	}

	public void setOgrenimtiplistesi(ArrayList<OgrenimTip> ogrenimtiplistesi) {
		this.ogrenimtiplistesi = ogrenimtiplistesi;
	}   
	
} 
