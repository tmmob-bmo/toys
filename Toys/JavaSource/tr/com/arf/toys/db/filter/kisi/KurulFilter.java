package tr.com.arf.toys.db.filter.kisi; 
 
import java.util.ArrayList;
import java.util.List;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.framework.db.model.query.QueryObject;
import tr.com.arf.toys.db.enumerated.kisi.KurulTuru;
import tr.com.arf.toys.utility.application.ApplicationConstant;
 
 
public class KurulFilter extends BaseFilter { 
	
private static final long serialVersionUID = 1L; 
	
	private String ad;
	private KurulTuru kurulturu;
	
	@Override 
	public void init() { 
		this.ad=null;
		this.kurulturu=KurulTuru._NULL;
	} 
	 
	@Override 
	public List<ArrayList<QueryObject>> createQueryCriterias(){ 
		ArrayList<ArrayList<QueryObject>> criteriaList = new ArrayList<ArrayList<QueryObject>>(); 
		ArrayList<QueryObject> kriterler = new ArrayList<QueryObject>();	 
		ArrayList<QueryObject> opsiyonelKriterler = new ArrayList<QueryObject>(); 
		try { 	 
			if(validateQueryObject(getAd(), null, false)){ 
				kriterler.add(new QueryObject("ad", ApplicationConstant._beginsWith, getAd())); 
			} 
			if(validateQueryObject(getKurulturu(), null, true)){ 
				kriterler.add(new QueryObject("kurulturu", ApplicationConstant._equals, getKurulturu())); 
			} 
		} catch(Exception e){ 
			System.out.println("Error @createQueryCriterias method, Class : KurulFilter :" + e.getMessage()); 
		}  
		criteriaList.add(kriterler); 
		criteriaList.add(opsiyonelKriterler); 
		return criteriaList; 
	}


	public String getAd() {
		return ad;
	}

	public void setAd(String ad) {
		this.ad = ad;
	}

	public KurulTuru getKurulturu() {
		return kurulturu;
	}

	public void setKurulturu(KurulTuru kurulturu) {
		this.kurulturu = kurulturu;
	} 
	
	 
} 
