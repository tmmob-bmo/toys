package tr.com.arf.toys.db.filter.etkinlik; 
 
import java.util.ArrayList;
import java.util.List;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.framework.db.model.query.QueryObject;
import tr.com.arf.toys.db.enumerated.etkinlik.Uyeturu;
import tr.com.arf.toys.db.model.etkinlik.Etkinlikkurul;
import tr.com.arf.toys.db.model.uye.Uye;
import tr.com.arf.toys.utility.application.ApplicationConstant;
 
 
public class EtkinlikkuruluyeFilter extends BaseFilter { 
	
private static final long serialVersionUID = 1L; 
	
	private Etkinlikkurul etkinlikkurulRef; 
	
	private Long rID; 
	private Uyeturu uyeturu; 
	private Uye uyeRef; 
	
	private String ad;
	private String soyad;
	
	@Override 
	public void init() { 
		this.rID = null; 	
		this.etkinlikkurulRef = null; 	
		this.uyeturu = Uyeturu._NULL;	
		this.uyeRef = null; 
		this.ad = null;
		this.soyad = null;
	} 
	 
	@Override 
	public List<ArrayList<QueryObject>> createQueryCriterias(){ 
		ArrayList<ArrayList<QueryObject>> criteriaList = new ArrayList<ArrayList<QueryObject>>(); 
		ArrayList<QueryObject> kriterler = new ArrayList<QueryObject>();	 
		ArrayList<QueryObject> opsiyonelKriterler = new ArrayList<QueryObject>(); 
		try { 	 
			if(validateQueryObject(getRID(), null, false)){ 
				kriterler.add(new QueryObject("rID", ApplicationConstant._equals, getRID())); 
			} 
			if(validateQueryObject(getEtkinlikkurulRef(), null, false)){ 
				kriterler.add(new QueryObject("etkinlikkurulRef", ApplicationConstant._equals, getEtkinlikkurulRef())); 
			} 
			if(validateQueryObject(getUyeturu(), null, true)){ 
				kriterler.add(new QueryObject("uyeturu", ApplicationConstant._equals, getUyeturu())); 
			} 
			if(validateQueryObject(getUyeRef(), null, false)){ 
				kriterler.add(new QueryObject("uyeRef", ApplicationConstant._equals, getUyeRef())); 
			} 
			if(validateQueryObject(getAd(), null, false)){ 
				kriterler.add(new QueryObject("uyeRef.kisiRef.ad", ApplicationConstant._beginsWith, getAd())); 
			}
			if(validateQueryObject(getSoyad(), null, false)){ 
				kriterler.add(new QueryObject("uyeRef.kisiRef.soyad", ApplicationConstant._beginsWith, getSoyad())); 
			} 
		} catch(Exception e){ 
			System.out.println("Error @createQueryCriterias method, Class : EtkinlikkuruluyeFilter :" + e.getMessage()); 
		}  
		criteriaList.add(kriterler); 
		criteriaList.add(opsiyonelKriterler); 
		return criteriaList; 
	} 
	 
	public Long getRID() { 
		return this.rID; 
	} 
	 
	public void setRID(Long rID) { 
		this.rID = rID; 
	} 
  
	public Etkinlikkurul getEtkinlikkurulRef() { 
		return this.etkinlikkurulRef; 
	} 
	 
	public void setEtkinlikkurulRef(Etkinlikkurul etkinlikkurulRef) { 
		this.etkinlikkurulRef = etkinlikkurulRef; 
	} 
  
	public Uyeturu getUyeturu() { 
		return this.uyeturu; 
	} 
	 
	public void setUyeturu(Uyeturu uyeturu) { 
		this.uyeturu = uyeturu; 
	} 
  
	public Uye getUyeRef() { 
		return this.uyeRef; 
	} 
	 
	public void setUyeRef(Uye uyeRef) { 
		this.uyeRef = uyeRef; 
	}

	public String getAd() {
		return ad;
	}

	public void setAd(String ad) {
		this.ad = ad;
	}

	public String getSoyad() {
		return soyad;
	}

	public void setSoyad(String soyad) {
		this.soyad = soyad;
	} 
	
} 
