package tr.com.arf.toys.db.filter.kisi; 
 
import java.util.ArrayList;
import java.util.List;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.framework.db.model.query.QueryObject;
import tr.com.arf.toys.db.model.kisi.KurulDonem;
import tr.com.arf.toys.db.model.uye.Uye;
import tr.com.arf.toys.utility.application.ApplicationConstant;
 
 
public class KurulDonemDelegeFilter extends BaseFilter { 
	
private static final long serialVersionUID = 1L; 
	
	private KurulDonem kurulDonemRef; 
	
	private Long rID; 
	private Uye uyeRef; 
	
	private String ad;
	private String soyad;
	
	@Override 
	public void init() { 
		this.rID = null; 	
		this.kurulDonemRef = null; 	
		this.uyeRef = null;
		this.ad = null;
		this.soyad = null;
	} 
	 
	@Override 
	public List<ArrayList<QueryObject>> createQueryCriterias(){ 
		ArrayList<ArrayList<QueryObject>> criteriaList = new ArrayList<ArrayList<QueryObject>>(); 
		ArrayList<QueryObject> kriterler = new ArrayList<QueryObject>();	 
		ArrayList<QueryObject> opsiyonelKriterler = new ArrayList<QueryObject>(); 
		try { 	 
			if(validateQueryObject(getRID(), null, false)){ 
				kriterler.add(new QueryObject("rID", ApplicationConstant._equals, getRID())); 
			} 
			if(validateQueryObject(getKurulDonemRef(), null, false)){ 
				kriterler.add(new QueryObject("kurulDonemRef", ApplicationConstant._equals, getKurulDonemRef())); 
			} 
			if(validateQueryObject(getUyeRef(), null, false)){ 
				kriterler.add(new QueryObject("uyeRef", ApplicationConstant._equals, getUyeRef())); 
			} 
			if(validateQueryObject(getAd(), null, false)){ 
				kriterler.add(new QueryObject("uyeRef.kisiRef.ad", ApplicationConstant._beginsWith, getAd())); 
			}
			if(validateQueryObject(getSoyad(), null, false)){ 
				kriterler.add(new QueryObject("uyeRef.kisiRef.soyad", ApplicationConstant._beginsWith, getSoyad())); 
			} 
		} catch(Exception e){ 
			System.out.println("Error @createQueryCriterias method, Class : KurulDonemDelegeFilter :" + e.getMessage()); 
		}  
		criteriaList.add(kriterler); 
		criteriaList.add(opsiyonelKriterler); 
		return criteriaList; 
	} 
	 
	public Long getRID() { 
		return this.rID; 
	} 
	 
	public void setRID(Long rID) { 
		this.rID = rID; 
	} 
  
	public KurulDonem getKurulDonemRef() { 
		return this.kurulDonemRef; 
	} 
	 
	public void setKurulDonemRef(KurulDonem kurulDonemRef) { 
		this.kurulDonemRef = kurulDonemRef; 
	} 
  
	public Uye getUyeRef() { 
		return this.uyeRef; 
	} 
	 
	public void setUyeRef(Uye uyeRef) { 
		this.uyeRef = uyeRef; 
	}

	public String getAd() {
		return ad;
	}

	public void setAd(String ad) {
		this.ad = ad;
	}

	public String getSoyad() {
		return soyad;
	}

	public void setSoyad(String soyad) {
		this.soyad = soyad;
	}
	
} 
