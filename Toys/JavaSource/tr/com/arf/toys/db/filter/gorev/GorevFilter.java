package tr.com.arf.toys.db.filter.gorev; 
 
import java.util.ArrayList;
import java.util.List;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.framework.db.model.query.QueryObject;
import tr.com.arf.toys.db.model.gorev.Gorev;
import tr.com.arf.toys.utility.application.ApplicationConstant;
 
 
public class GorevFilter extends BaseFilter { 
	
private static final long serialVersionUID = 1L; 
	
	private Gorev ustRef; 
	private String ad; 
	private String aciklama; 
	
	@Override 
	public void init() { 
		this.ustRef = null; 	
		this.ad = null; 	
		this.aciklama = null; 	
	} 
	 
	@Override 
	public List<ArrayList<QueryObject>> createQueryCriterias(){ 
		ArrayList<ArrayList<QueryObject>> criteriaList = new ArrayList<ArrayList<QueryObject>>(); 
		ArrayList<QueryObject> kriterler = new ArrayList<QueryObject>();	 
		ArrayList<QueryObject> opsiyonelKriterler = new ArrayList<QueryObject>(); 
		try { 	 
			if(validateQueryObject(getUstRef(), null, false)){ 
				kriterler.add(new QueryObject("ustRef", ApplicationConstant._equals, getUstRef())); 
			} 
			if(validateQueryObject(getAd(), null, false)){ 
				kriterler.add(new QueryObject("ad", ApplicationConstant._beginsWith, getAd())); 
			} 
			if(validateQueryObject(getAciklama(), null, false)){ 
				kriterler.add(new QueryObject("aciklama", ApplicationConstant._beginsWith, getAciklama())); 
			} 
		} catch(Exception e){ 
			System.out.println("Error @createQueryCriterias method, Class : GorevFilter :" + e.getMessage()); 
		}  
		criteriaList.add(kriterler); 
		criteriaList.add(opsiyonelKriterler); 
		return criteriaList; 
	} 
	 
	public Gorev getUstRef() { 
		return this.ustRef; 
	} 
	 
	public void setUstRef(Gorev ustRef) { 
		this.ustRef = ustRef; 
	} 
  
	public String getAd() { 
		return this.ad; 
	} 
	 
	public void setAd(String ad) { 
		this.ad = ad; 
	} 
  
	public String getAciklama() { 
		return this.aciklama; 
	} 
	 
	public void setAciklama(String aciklama) { 
		this.aciklama = aciklama; 
	} 
  
	
} 
