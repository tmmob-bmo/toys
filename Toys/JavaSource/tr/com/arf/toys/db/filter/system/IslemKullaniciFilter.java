package tr.com.arf.toys.db.filter.system; 
 
import java.util.ArrayList;
import java.util.List;

import tr.com.arf.framework.db.enumerated._common.EvetHayir;
import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.framework.db.model.query.QueryObject;
import tr.com.arf.toys.db.model.system.Islem;
import tr.com.arf.toys.db.model.system.KullaniciRole;
import tr.com.arf.toys.utility.application.ApplicationConstant;
 
 
public class IslemKullaniciFilter extends BaseFilter { 
	
private static final long serialVersionUID = 1L; 
	
	private Islem islemRef; 
	private KullaniciRole kullaniciRoleRef; 
	private EvetHayir updatePermission; 
	private EvetHayir deletePermission; 
	private EvetHayir listPermission; 
	private String name; 
	
	@Override 
	public void init() { 
		this.islemRef = null; 	
		this.kullaniciRoleRef = null; 	
		this.updatePermission = EvetHayir._NULL;	
		this.deletePermission = EvetHayir._NULL;	
		this.listPermission = EvetHayir._NULL;
		this.name = null;
	} 
	 
	@Override 
	public List<ArrayList<QueryObject>> createQueryCriterias(){ 
		ArrayList<ArrayList<QueryObject>> criteriaList = new ArrayList<ArrayList<QueryObject>>(); 
		ArrayList<QueryObject> kriterler = new ArrayList<QueryObject>();	 
		ArrayList<QueryObject> opsiyonelKriterler = new ArrayList<QueryObject>(); 
		try { 	 
			if(validateQueryObject(getIslemRef(), null, false)){ 
				kriterler.add(new QueryObject("islemRef", ApplicationConstant._equals, getIslemRef())); 
			} 
			if(validateQueryObject(getKullaniciRoleRef(), null, false)){ 
				kriterler.add(new QueryObject("kullaniciRoleRef", ApplicationConstant._equals, getKullaniciRoleRef())); 
			} 
			if(validateQueryObject(getUpdatePermission(), null, true)){ 
				kriterler.add(new QueryObject("updatePermission", ApplicationConstant._equals, getUpdatePermission())); 
			} 
			if(validateQueryObject(getDeletePermission(), null, true)){ 
				kriterler.add(new QueryObject("deletePermission", ApplicationConstant._equals, getDeletePermission())); 
			} 
			if(validateQueryObject(getListPermission(), null, true)){ 
				kriterler.add(new QueryObject("listPermission", ApplicationConstant._equals, getListPermission())); 
			} 
			if(validateQueryObject(getName(), null, false)){ 
				kriterler.add(new QueryObject("islemRef.name", ApplicationConstant._beginsWith, getName())); 
			} 
		} catch(Exception e){ 
			System.out.println("Error @createQueryCriterias method, Class : islemkullaniciFilter :" + e.getMessage()); 
		}  
		criteriaList.add(kriterler); 
		criteriaList.add(opsiyonelKriterler); 
		return criteriaList; 
	} 
	 
	public Islem getIslemRef() { 
		return this.islemRef; 
	} 
	 
	public void setIslemRef(Islem islemRef) { 
		this.islemRef = islemRef; 
	} 
  
	public KullaniciRole getKullaniciRoleRef() { 
		return this.kullaniciRoleRef; 
	} 
	 
	public void setKullaniciRoleRef(KullaniciRole kullaniciRoleRef) { 
		this.kullaniciRoleRef = kullaniciRoleRef; 
	} 
  
	public EvetHayir getUpdatePermission() { 
		return this.updatePermission; 
	} 
	 
	public void setUpdatePermission(EvetHayir updatePermission) { 
		this.updatePermission = updatePermission; 
	} 
  
	public EvetHayir getDeletePermission() { 
		return this.deletePermission; 
	} 
	 
	public void setDeletePermission(EvetHayir deletePermission) { 
		this.deletePermission = deletePermission; 
	} 
  
	public EvetHayir getListPermission() { 
		return this.listPermission; 
	} 
	 
	public void setListPermission(EvetHayir listPermission) { 
		this.listPermission = listPermission; 
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	} 
  
	
} 
