package tr.com.arf.toys.db.filter.etkinlik; 
 
import java.util.ArrayList;
import java.util.List;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.framework.db.model.query.QueryObject;
import tr.com.arf.toys.db.enumerated.etkinlik.Kurulturu;
import tr.com.arf.toys.db.model.etkinlik.Etkinlik;
import tr.com.arf.toys.utility.application.ApplicationConstant;
 
 
public class EtkinlikkurulFilter extends BaseFilter { 
	
private static final long serialVersionUID = 1L; 
	
	private Etkinlik etkinlikRef; 
	
	private Long rID; 
	private Kurulturu kurulturu; 
	
	@Override 
	public void init() { 
		this.rID = null; 	
		this.etkinlikRef = null; 	
		this.kurulturu = Kurulturu._NULL;	
	} 
	 
	@Override 
	public List<ArrayList<QueryObject>> createQueryCriterias(){ 
		ArrayList<ArrayList<QueryObject>> criteriaList = new ArrayList<ArrayList<QueryObject>>(); 
		ArrayList<QueryObject> kriterler = new ArrayList<QueryObject>();	 
		ArrayList<QueryObject> opsiyonelKriterler = new ArrayList<QueryObject>(); 
		try { 	 
			if(validateQueryObject(getRID(), null, false)){ 
				kriterler.add(new QueryObject("rID", ApplicationConstant._equals, getRID())); 
			} 
			if(validateQueryObject(getEtkinlikRef(), null, false)){ 
				kriterler.add(new QueryObject("etkinlikRef", ApplicationConstant._equals, getEtkinlikRef())); 
			} 
			if(validateQueryObject(getKurulturu(), null, true)){ 
				kriterler.add(new QueryObject("kurulturu", ApplicationConstant._equals, getKurulturu())); 
			} 
		} catch(Exception e){ 
			System.out.println("Error @createQueryCriterias method, Class : EtkinlikkurulFilter :" + e.getMessage()); 
		}  
		criteriaList.add(kriterler); 
		criteriaList.add(opsiyonelKriterler); 
		return criteriaList; 
	} 
	 
	public Long getRID() { 
		return this.rID; 
	} 
	 
	public void setRID(Long rID) { 
		this.rID = rID; 
	} 
  
	public Etkinlik getEtkinlikRef() { 
		return this.etkinlikRef; 
	} 
	 
	public void setEtkinlikRef(Etkinlik etkinlikRef) { 
		this.etkinlikRef = etkinlikRef; 
	} 
  
	public Kurulturu getKurulturu() { 
		return this.kurulturu; 
	} 
	 
	public void setKurulturu(Kurulturu kurulturu) { 
		this.kurulturu = kurulturu; 
	} 
  
	
} 
