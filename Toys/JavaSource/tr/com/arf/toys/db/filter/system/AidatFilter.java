package tr.com.arf.toys.db.filter.system;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import tr.com.arf.framework.db.enumerated._common.EvetHayir;
import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.framework.db.model.query.QueryObject;
import tr.com.arf.toys.db.enumerated.uye.UyeTip;
import tr.com.arf.toys.utility.application.ApplicationConstant;


public class AidatFilter extends BaseFilter {

private static final long serialVersionUID = 1L;

	private int yil;
	private int yilMax;
	private java.util.Date baslangictarih;
	private java.util.Date baslangictarihMax;
	private java.util.Date bitistarih;
	private java.util.Date bitistarihMax;
	private UyeTip uyetip;
	private BigDecimal miktar;
	private EvetHayir borclandirmaYapildi;
	private BigDecimal kayitucreti;

	@Override
	public void init() {
		this.yil = 0;
		this.yilMax=0;
		this.baslangictarih = null;
		this.baslangictarihMax = null;
		this.bitistarih = null;
		this.bitistarihMax = null;
		this.uyetip = UyeTip._NULL;
		this.miktar = null;
		this.borclandirmaYapildi=EvetHayir._NULL;
		this.kayitucreti=null;
	}

	@Override
	public List<ArrayList<QueryObject>> createQueryCriterias(){
		ArrayList<ArrayList<QueryObject>> criteriaList = new ArrayList<ArrayList<QueryObject>>();
		ArrayList<QueryObject> kriterler = new ArrayList<QueryObject>();
		ArrayList<QueryObject> opsiyonelKriterler = new ArrayList<QueryObject>();
		try {
			if(validateQueryObject(getYil(), 1900, false)){
				kriterler.add(new QueryObject("yil", ApplicationConstant._equals, getYil()));
			}
			if(validateQueryObject(getBaslangictarih(), null, false)  && validateQueryObject(getBaslangictarihMax(), null, false)){
				kriterler.add(new QueryObject("baslangictarih", ApplicationConstant._equalsAndLargerThan, getBaslangictarih()));
				kriterler.add(new QueryObject("baslangictarih", ApplicationConstant._equalsAndSmallerThan, getBaslangictarihMax()));
			} else if(validateQueryObject(getBaslangictarih(), null, false)){
				kriterler.add(new QueryObject("baslangictarih", ApplicationConstant._equalsAndSmallerThan, getBaslangictarih()));
			}
			if(validateQueryObject(getBitistarih(), null, false)  && validateQueryObject(getBitistarihMax(), null, false)){
				kriterler.add(new QueryObject("bitistarih", ApplicationConstant._equalsAndLargerThan, getBitistarih()));
				kriterler.add(new QueryObject("bitistarih", ApplicationConstant._equalsAndSmallerThan, getBitistarihMax()));
			} else if(validateQueryObject(getBitistarih(), null, false)){
				kriterler.add(new QueryObject("bitistarih", ApplicationConstant._equalsAndLargerThan, getBitistarih()));
			}
			if(validateQueryObject(getUyetip(), null, true)){
				kriterler.add(new QueryObject("uyetip", ApplicationConstant._equals, getUyetip()));
			}
			if(validateQueryObject(getMiktar(), 0, false)){
				kriterler.add(new QueryObject("miktar", ApplicationConstant._equals, getMiktar()));
			}
			if(validateQueryObject(getKayitucreti(), 0, false)){
				kriterler.add(new QueryObject("kayitucreti", ApplicationConstant._equals, getKayitucreti()));
			}
			if(validateQueryObject(getBorclandirmaYapildi(), null, true)){
				kriterler.add(new QueryObject("borclandirmaYapildi", ApplicationConstant._equals, getBorclandirmaYapildi()));
			}
		} catch(Exception e){
			System.out.println("Error @createQueryCriterias method, Class : AidatFilter :" + e.getMessage());
		}
		criteriaList.add(kriterler);
		criteriaList.add(opsiyonelKriterler);
		return criteriaList;
	}

	public int getYil() {
		return this.yil;
	}

	public void setYil(int yil) {
		this.yil = yil;
	}

	public Date getBaslangictarih() {
		return this.baslangictarih;
	}

	public void setBaslangictarih(Date baslangictarih) {
		this.baslangictarih = baslangictarih;
	}

	public Date getBaslangictarihMax() {
		return this.baslangictarihMax;
	}

	public void setBaslangictarihMax(Date baslangictarihMax) {
		this.baslangictarihMax = baslangictarihMax;
	}

	public Date getBitistarih() {
		return this.bitistarih;
	}

	public void setBitistarih(Date bitistarih) {
		this.bitistarih = bitistarih;
	}

	public Date getBitistarihMax() {
		return this.bitistarihMax;
	}

	public void setBitistarihMax(Date bitistarihMax) {
		this.bitistarihMax = bitistarihMax;
	}

	public UyeTip getUyetip() {
		return this.uyetip;
	}

	public void setUyetip(UyeTip uyetip) {
		this.uyetip = uyetip;
	}

	public BigDecimal getMiktar() {
		return this.miktar;
	}

	public void setMiktar(BigDecimal miktar) {
		this.miktar = miktar;
	}

	public EvetHayir getBorclandirmaYapildi() {
		return borclandirmaYapildi;
	}

	public void setBorclandirmaYapildi(EvetHayir borclandirmaYapildi) {
		this.borclandirmaYapildi = borclandirmaYapildi;
	}

	public int getYilMax() {
		return yilMax;
	}

	public void setYilMax(int yilMax) {
		this.yilMax = yilMax;
	}

	public BigDecimal getKayitucreti() {
		return kayitucreti;
	}

	public void setKayitucreti(BigDecimal kayitucreti) {
		this.kayitucreti = kayitucreti;
	}
}
