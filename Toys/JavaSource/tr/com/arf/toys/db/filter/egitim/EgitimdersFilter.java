package tr.com.arf.toys.db.filter.egitim; 
 
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.framework.db.model.query.QueryObject;
import tr.com.arf.toys.db.model.egitim.Egitim;
import tr.com.arf.toys.db.model.egitim.Egitimogretmen;
import tr.com.arf.toys.utility.application.ApplicationConstant;
 
 
public class EgitimdersFilter extends BaseFilter { 
	
private static final long serialVersionUID = 1L; 
	
	private Egitim egitimRef; 
	
	private Long rID; 
	private Egitimogretmen egitimogretmenRef; 
	private java.util.Date zaman; 
	private java.util.Date zamanMax; 
	private String aciklama; 
	private BigDecimal sure;
	
	@Override 
	public void init() { 
		this.rID = null; 	
		this.egitimRef = null; 	
		this.egitimogretmenRef = null; 	
		this.zaman = null; 	
		this.zamanMax = null; 	
		this.aciklama = null; 
		this.sure = null;
	} 
	 
	@Override 
	public List<ArrayList<QueryObject>> createQueryCriterias(){ 
		ArrayList<ArrayList<QueryObject>> criteriaList = new ArrayList<ArrayList<QueryObject>>(); 
		ArrayList<QueryObject> kriterler = new ArrayList<QueryObject>();	 
		ArrayList<QueryObject> opsiyonelKriterler = new ArrayList<QueryObject>(); 
		try { 	 
			if(validateQueryObject(getRID(), null, false)){ 
				kriterler.add(new QueryObject("rID", ApplicationConstant._equals, getRID())); 
			} 
			if(validateQueryObject(getEgitimRef(), null, false)){ 
				kriterler.add(new QueryObject("egitimRef", ApplicationConstant._equals, getEgitimRef())); 
			} 
			if(validateQueryObject(getEgitimogretmenRef(), null, false)){ 
				kriterler.add(new QueryObject("egitimogretmenRef", ApplicationConstant._equals, getEgitimogretmenRef())); 
			} 
			if(validateQueryObject(getZaman(), null, false)  && validateQueryObject(getZamanMax(), null, false)){
				kriterler.add(new QueryObject("zaman", ApplicationConstant._equalsAndLargerThan, getZaman())); 
				kriterler.add(new QueryObject("zaman", ApplicationConstant._equalsAndSmallerThan, getZamanMax())); 
			} else if(validateQueryObject(getZaman(), null, false)){ 
				kriterler.add(new QueryObject("zaman", ApplicationConstant._equalsAndLargerThan, getZaman()));
			}
			if(validateQueryObject(getAciklama(), null, false)){ 
				kriterler.add(new QueryObject("aciklama", ApplicationConstant._beginsWith, getAciklama(), false)); 
			} 
			if(validateQueryObject(getSure(), null, false)){
				kriterler.add(new QueryObject("sure", ApplicationConstant._equals, getSure())); 	
			}
		} catch(Exception e){ 
			System.out.println("Error @createQueryCriterias method, Class : EgitimdersFilter :" + e.getMessage()); 
		}  
		criteriaList.add(kriterler); 
		criteriaList.add(opsiyonelKriterler); 
		return criteriaList; 
	} 
	 
	public Long getRID() { 
		return this.rID; 
	} 
	 
	public void setRID(Long rID) { 
		this.rID = rID; 
	} 
  
	public Egitim getEgitimRef() { 
		return this.egitimRef; 
	} 
	 
	public void setEgitimRef(Egitim egitimRef) { 
		this.egitimRef = egitimRef; 
	} 
  
	public Egitimogretmen getEgitimogretmenRef() { 
		return this.egitimogretmenRef; 
	} 
	 
	public void setEgitimogretmenRef(Egitimogretmen egitimogretmenRef) { 
		this.egitimogretmenRef = egitimogretmenRef; 
	} 
  
	public Date getZaman() { 
		return this.zaman; 
	} 
	 
	public void setZaman(Date zaman) { 
		this.zaman = zaman; 
	} 
  
	public Date getZamanMax() { 
		return this.zamanMax; 
	} 
	 
	public void setZamanMax(Date zamanMax) { 
		this.zamanMax = zamanMax; 
	} 
  
	public String getAciklama() { 
		return this.aciklama; 
	} 
	 
	public void setAciklama(String aciklama) { 
		this.aciklama = aciklama; 
	} 
	public BigDecimal getSure() { 
		return this.sure; 
	} 
	 
	public void setSure(BigDecimal sure) { 
		this.sure = sure; 
	} 
  
	
} 
