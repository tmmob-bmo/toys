package tr.com.arf.toys.db.filter.system; 
 
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.framework.db.model.query.QueryObject;
import tr.com.arf.toys.db.model.system.Kullanici;
import tr.com.arf.toys.service.application.ManagedBeanLocator;
import tr.com.arf.toys.utility.application.ApplicationConstant;
 
 
public class KullaniciraporFilter extends BaseFilter { 
	
private static final long serialVersionUID = 1L; 
	
	private Long rID; 
	private Kullanici kullaniciRef; 
	private String raporadi; 
	private String wherecondition; 
	private java.util.Date tarih; 
	private java.util.Date tarihMax; 
	private String raporTuru; 
	
	@Override 
	public void init() { 
		this.rID = null; 	
		this.kullaniciRef = null; 	
		this.raporadi = null; 	
		this.wherecondition = null; 	
		this.tarih = null; 	
		this.tarihMax = null; 
		this.raporTuru = null; 
	} 
	 
	@Override 
	public List<ArrayList<QueryObject>> createQueryCriterias(){ 
		ArrayList<ArrayList<QueryObject>> criteriaList = new ArrayList<ArrayList<QueryObject>>(); 
		ArrayList<QueryObject> kriterler = new ArrayList<QueryObject>();	 
		ArrayList<QueryObject> opsiyonelKriterler = new ArrayList<QueryObject>(); 
		try { 
			
			// Kisisel raporlarda her kullanici sadece kendi raporunu gorebilir !!!
			kriterler.add(new QueryObject("kullaniciRef.rID", ApplicationConstant._equals, ManagedBeanLocator.locateSessionUser().getKullanici().getRID()));
			
			if(validateQueryObject(getRID(), null, false)){ 
				kriterler.add(new QueryObject("rID", ApplicationConstant._equals, getRID())); 
			} 			 
			if(validateQueryObject(getRaporadi(), null, false)){ 
				kriterler.add(new QueryObject("raporadi", ApplicationConstant._beginsWith, getRaporadi(), false)); 
			} 
			if(validateQueryObject(getWherecondition(), null, false)){ 
				kriterler.add(new QueryObject("wherecondition", ApplicationConstant._beginsWith, getWherecondition(), false)); 
			} 
			if(validateQueryObject(getRaporTuru(), null, false)){  
				kriterler.add(new QueryObject("raporTuru", ApplicationConstant._equals, getRaporTuru(), false)); 
			}
			if(validateQueryObject(getTarih(), null, false)  && validateQueryObject(getTarihMax(), null, false)){
				kriterler.add(new QueryObject("tarih", ApplicationConstant._equalsAndLargerThan, getTarih())); 
				kriterler.add(new QueryObject("tarih", ApplicationConstant._equalsAndSmallerThan, getTarihMax())); 
			} else if(validateQueryObject(getTarih(), null, false)){ 
				kriterler.add(new QueryObject("tarih", ApplicationConstant._equalsAndLargerThan, getTarih()));
			}
		} catch(Exception e){ 
			System.out.println("Error @createQueryCriterias method, Class : KullaniciraporFilter :" + e.getMessage()); 
		}  
		criteriaList.add(kriterler); 
		criteriaList.add(opsiyonelKriterler); 
		return criteriaList; 
	} 
	 
	public Long getRID() { 
		return this.rID; 
	} 
	 
	public void setRID(Long rID) { 
		this.rID = rID; 
	} 
  
	public Kullanici getKullaniciRef() { 
		return this.kullaniciRef; 
	} 
	 
	public void setKullaniciRef(Kullanici kullaniciRef) { 
		this.kullaniciRef = kullaniciRef; 
	} 
  
	public String getRaporadi() { 
		return this.raporadi; 
	} 
	 
	public void setRaporadi(String raporadi) { 
		this.raporadi = raporadi; 
	} 
  
	public String getWherecondition() { 
		return this.wherecondition; 
	} 
	 
	public void setWherecondition(String wherecondition) { 
		this.wherecondition = wherecondition; 
	} 
  
	public Date getTarih() { 
		return this.tarih; 
	} 
	 
	public void setTarih(Date tarih) { 
		this.tarih = tarih; 
	} 
  
	public Date getTarihMax() { 
		return this.tarihMax; 
	} 
	 
	public void setTarihMax(Date tarihMax) { 
		this.tarihMax = tarihMax; 
	} 
	
	public String getRaporTuru() {
		return raporTuru;
	}

	public void setRaporTuru(String raporTuru) {
		this.raporTuru = raporTuru;
	}
  
	
} 
