package tr.com.arf.toys.db.filter.system; 
 
import java.util.ArrayList;
import java.util.List;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.framework.db.model.query.QueryObject;
import tr.com.arf.toys.utility.application.ApplicationConstant;
 
 
public class DuyuruFilter extends BaseFilter { 
	
private static final long serialVersionUID = 1L; 
	
	private String duyurubasligi; 
	private String duyurumetni; 
	private java.util.Date baslangictarih; 
	private java.util.Date baslangictarihMax; 
	private java.util.Date bitistarih; 
	private java.util.Date bitistarihMax;
	
	@Override 
	public void init() { 
		this.duyurubasligi = null; 	
		this.duyurumetni = null; 	
		this.baslangictarih=null;
		this.baslangictarihMax=null;
		this.bitistarih=null;
		this.bitistarihMax=null;
	} 
	 
	@Override 
	public List<ArrayList<QueryObject>> createQueryCriterias(){ 
		ArrayList<ArrayList<QueryObject>> criteriaList = new ArrayList<ArrayList<QueryObject>>(); 
		ArrayList<QueryObject> kriterler = new ArrayList<QueryObject>();	 
		ArrayList<QueryObject> opsiyonelKriterler = new ArrayList<QueryObject>(); 
		try { 	 
			if(validateQueryObject(getDuyurubasligi(), null, false)){ 
				kriterler.add(new QueryObject("duyurubasligi", ApplicationConstant._beginsWith, getDuyurubasligi())); 
			} 
			if(validateQueryObject(getDuyurumetni(), null, false)){ 
				kriterler.add(new QueryObject("duyurumetni", ApplicationConstant._beginsWith, getDuyurumetni())); 
			} 
			if(validateQueryObject(getBaslangictarih(), null, false)  && validateQueryObject(getBaslangictarihMax(), null, false)){
				kriterler.add(new QueryObject("baslangictarih", ApplicationConstant._equalsAndLargerThan, getBaslangictarih())); 
				kriterler.add(new QueryObject("baslangictarih", ApplicationConstant._equalsAndSmallerThan, getBaslangictarihMax())); 
			} else if(validateQueryObject(getBaslangictarih(), null, false)){ 
				kriterler.add(new QueryObject("baslangictarih", ApplicationConstant._equalsAndLargerThan, getBaslangictarih()));
			}
			if(validateQueryObject(getBitistarih(), null, false)  && validateQueryObject(getBitistarihMax(), null, false)){
				kriterler.add(new QueryObject("bitistarih", ApplicationConstant._equalsAndLargerThan, getBitistarih())); 
				kriterler.add(new QueryObject("bitistarih", ApplicationConstant._equalsAndSmallerThan, getBitistarihMax())); 
			} else if(validateQueryObject(getBitistarih(), null, false)){ 
				kriterler.add(new QueryObject("bitistarih", ApplicationConstant._equalsAndLargerThan, getBitistarih()));
			}

		} catch(Exception e){ 
			System.out.println("Error @createQueryCriterias method, Class : DurumFilter :" + e.getMessage()); 
		}  
		criteriaList.add(kriterler); 
		criteriaList.add(opsiyonelKriterler); 
		return criteriaList; 
	}

	public String getDuyurubasligi() {
		return duyurubasligi;
	}

	public void setDuyurubasligi(String duyurubasligi) {
		this.duyurubasligi = duyurubasligi;
	}

	public String getDuyurumetni() {
		return duyurumetni;
	}

	public void setDuyurumetni(String duyurumetni) {
		this.duyurumetni = duyurumetni;
	}

	public java.util.Date getBaslangictarih() {
		return baslangictarih;
	}

	public void setBaslangictarih(java.util.Date baslangictarih) {
		this.baslangictarih = baslangictarih;
	}

	public java.util.Date getBaslangictarihMax() {
		return baslangictarihMax;
	}

	public void setBaslangictarihMax(java.util.Date baslangictarihMax) {
		this.baslangictarihMax = baslangictarihMax;
	}

	public java.util.Date getBitistarih() {
		return bitistarih;
	}

	public void setBitistarih(java.util.Date bitistarih) {
		this.bitistarih = bitistarih;
	}

	public java.util.Date getBitistarihMax() {
		return bitistarihMax;
	}

	public void setBitistarihMax(java.util.Date bitistarihMax) {
		this.bitistarihMax = bitistarihMax;
	} 
	 
	
} 
