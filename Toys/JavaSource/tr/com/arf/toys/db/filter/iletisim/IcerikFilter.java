package tr.com.arf.toys.db.filter.iletisim; 
 
import java.util.ArrayList;
import java.util.List;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.framework.db.model.query.QueryObject;
import tr.com.arf.toys.db.enumerated.iletisim.GonderimTuru;
import tr.com.arf.toys.db.enumerated.iletisim.IcerikTuru;
import tr.com.arf.toys.utility.application.ApplicationConstant;
 
 
public class IcerikFilter extends BaseFilter { 
	
private static final long serialVersionUID = 1L; 
	
	private Long rID; 
	private String icerik; 
	private GonderimTuru gonderimturu; 
	private IcerikTuru icerikturu; 
	
	@Override 
	public void init() { 
		this.rID = null; 	
		this.icerik = null; 	
		this.gonderimturu = GonderimTuru._NULL;	
		this.icerikturu = IcerikTuru._NULL;	
	} 
	 
	@Override 
	public List<ArrayList<QueryObject>> createQueryCriterias(){ 
		ArrayList<ArrayList<QueryObject>> criteriaList = new ArrayList<ArrayList<QueryObject>>(); 
		ArrayList<QueryObject> kriterler = new ArrayList<QueryObject>();	 
		ArrayList<QueryObject> opsiyonelKriterler = new ArrayList<QueryObject>(); 
		try { 	 
			if(validateQueryObject(getRID(), null, false)){ 
				kriterler.add(new QueryObject("rID", ApplicationConstant._equals, getRID())); 
			} 
			if(validateQueryObject(getIcerik(), null, false)){ 
				kriterler.add(new QueryObject("icerik", ApplicationConstant._beginsWith, getIcerik(), false)); 
			} 
			if(validateQueryObject(getGonderimturu(), null, true)){ 
				kriterler.add(new QueryObject("gonderimturu", ApplicationConstant._equals, getGonderimturu())); 
			} 
			if(validateQueryObject(getIcerikturu(), null, true)){ 
				kriterler.add(new QueryObject("icerikturu", ApplicationConstant._equals, getIcerikturu())); 
			} 
		} catch(Exception e){ 
			System.out.println("Error @createQueryCriterias method, Class : IcerikFilter :" + e.getMessage()); 
		}  
		criteriaList.add(kriterler); 
		criteriaList.add(opsiyonelKriterler); 
		return criteriaList; 
	} 
	 
	public Long getRID() { 
		return this.rID; 
	} 
	 
	public void setRID(Long rID) { 
		this.rID = rID; 
	} 
  
	public String getIcerik() { 
		return this.icerik; 
	} 
	 
	public void setIcerik(String icerik) { 
		this.icerik = icerik; 
	} 
  
	public GonderimTuru getGonderimturu() { 
		return this.gonderimturu; 
	} 
	 
	public void setGonderimturu(GonderimTuru gonderimturu) { 
		this.gonderimturu = gonderimturu; 
	} 
  
	public IcerikTuru getIcerikturu() { 
		return this.icerikturu; 
	} 
	 
	public void setIcerikturu(IcerikTuru icerikturu) { 
		this.icerikturu = icerikturu; 
	} 
  
	
} 
