package tr.com.arf.toys.db.filter.system; 
 
import java.util.ArrayList;
import java.util.List;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.framework.db.model.query.QueryObject;
import tr.com.arf.toys.utility.application.ApplicationConstant;
 
 
public class UlkeFilter extends BaseFilter { 
	
private static final long serialVersionUID = 1L; 
	
	private String ad; 
	private String ulkekod;
	
	@Override 
	public void init() { 
		this.ad = null; 
		this.ulkekod=null;
	} 
	 
	@Override 
	public List<ArrayList<QueryObject>> createQueryCriterias(){ 
		ArrayList<ArrayList<QueryObject>> criteriaList = new ArrayList<ArrayList<QueryObject>>(); 
		ArrayList<QueryObject> kriterler = new ArrayList<QueryObject>();	 
		ArrayList<QueryObject> opsiyonelKriterler = new ArrayList<QueryObject>(); 
		try { 	 
			if(validateQueryObject(getAd(), null, false)){ 
				kriterler.add(new QueryObject("ad", ApplicationConstant._beginsWith, getAd().trim(),false)); 
			}
			if(validateQueryObject(getUlkekod(), null, false)){ 
				kriterler.add(new QueryObject("ulkekod", ApplicationConstant._beginsWith, getUlkekod(),false)); 
			}
		} catch(Exception e){ 
			System.out.println("Error @createQueryCriterias method, Class : UlkeFilter :" + e.getMessage()); 
		}  
		criteriaList.add(kriterler); 
		criteriaList.add(opsiyonelKriterler); 
		return criteriaList; 
	} 
	 
	public String getAd() { 
		return this.ad; 
	} 
	 
	public void setAd(String ad) { 
		this.ad = ad; 
	}

	public String getUlkekod() {
		return ulkekod;
	}

	public void setUlkekod(String ulkekod) {
		this.ulkekod = ulkekod;
	}

} 
