package tr.com.arf.toys.db.filter.egitim; 
 
import java.util.ArrayList;
import java.util.List;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.framework.db.model.query.QueryObject;
import tr.com.arf.toys.db.model.egitim.Egitimtanim;
import tr.com.arf.toys.db.model.egitim.Egitmen;
import tr.com.arf.toys.utility.application.ApplicationConstant;
 
 
public class EgitmenEgitimtanimFilter extends BaseFilter { 
	
private static final long serialVersionUID = 1L; 
	
	private Long rID; 
	private Egitimtanim egitimtanimRef; 
	private Egitmen egitmenRef  ; 
	
	@Override 
	public void init() { 
		this.rID = null; 	
		this.egitimtanimRef=null;
		this.egitmenRef=null;
	} 
	 
	@Override 
	public List<ArrayList<QueryObject>> createQueryCriterias(){ 
		ArrayList<ArrayList<QueryObject>> criteriaList = new ArrayList<ArrayList<QueryObject>>(); 
		ArrayList<QueryObject> kriterler = new ArrayList<QueryObject>();	 
		ArrayList<QueryObject> opsiyonelKriterler = new ArrayList<QueryObject>(); 
		try { 	 
			if(validateQueryObject(getRID(), null, false)){ 
				kriterler.add(new QueryObject("rID", ApplicationConstant._equals, getRID())); 
			} 
			if(validateQueryObject(getEgitimtanimRef(), null, false)){ 
				kriterler.add(new QueryObject("egitimtanimRef", ApplicationConstant._equals, getEgitimtanimRef())); 
			} 
			if(validateQueryObject(getEgitmenRef(), null, false)){ 
				kriterler.add(new QueryObject("egitmenRef", ApplicationConstant._equals, getEgitmenRef())); 
			} 
		} catch(Exception e){ 
			System.out.println("Error @createQueryCriterias method, Class : EgitmenEgitimtanimFilter :" + e.getMessage()); 
		}  
		criteriaList.add(kriterler); 
		criteriaList.add(opsiyonelKriterler); 
		return criteriaList; 
	} 
	 
	public Long getRID() { 
		return this.rID; 
	} 
	 
	public void setRID(Long rID) { 
		this.rID = rID; 
	}

	public Egitimtanim getEgitimtanimRef() {
		return egitimtanimRef;
	}

	public void setEgitimtanimRef(Egitimtanim egitimtanimRef) {
		this.egitimtanimRef = egitimtanimRef;
	}

	public Egitmen getEgitmenRef() {
		return egitmenRef;
	}

	public void setEgitmenRef(Egitmen egitmenRef) {
		this.egitmenRef = egitmenRef;
	}

} 
