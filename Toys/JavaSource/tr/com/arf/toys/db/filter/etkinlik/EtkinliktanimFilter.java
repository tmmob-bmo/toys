package tr.com.arf.toys.db.filter.etkinlik; 
 
import java.util.ArrayList;
import java.util.List;

import tr.com.arf.framework.db.enumerated._common.EvetHayir;
import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.framework.db.model.query.QueryObject;
import tr.com.arf.toys.utility.application.ApplicationConstant;
 
 
public class EtkinliktanimFilter extends BaseFilter { 
	
private static final long serialVersionUID = 1L; 
	
	private Long rID; 
	private String ad; 
	private EvetHayir periyodik; 
	private String aciklama; 
	private String anahtarkelimeler;
	
	@Override 
	public void init() { 
		this.rID = null; 	
		this.ad = null; 	
		this.periyodik = EvetHayir._NULL;	
		this.aciklama = null; 	
		this.anahtarkelimeler = "";
	} 
	 
	@Override 
	public List<ArrayList<QueryObject>> createQueryCriterias(){ 
		ArrayList<ArrayList<QueryObject>> criteriaList = new ArrayList<ArrayList<QueryObject>>(); 
		ArrayList<QueryObject> kriterler = new ArrayList<QueryObject>();	 
		ArrayList<QueryObject> opsiyonelKriterler = new ArrayList<QueryObject>(); 
		try { 	 
			if(validateQueryObject(getRID(), null, false)){ 
				kriterler.add(new QueryObject("rID", ApplicationConstant._equals, getRID())); 
			} 
			if(validateQueryObject(getAd(), null, false)){ 
				kriterler.add(new QueryObject("ad", ApplicationConstant._beginsWith, getAd(), false)); 
			} 
			if(validateQueryObject(getPeriyodik(), null, true)){ 
				kriterler.add(new QueryObject("periyodik", ApplicationConstant._equals, getPeriyodik())); 
			} 
			if(validateQueryObject(getAciklama(), null, false)){ 
				kriterler.add(new QueryObject("aciklama", ApplicationConstant._beginsWith, getAciklama(), false)); 
			}
			if(validateQueryObject(getAnahtarkelimeler(), null, false)){ 
				kriterler.add(new QueryObject("anahtarkelimeler", ApplicationConstant._beginsWith, getAnahtarkelimeler(), false)); 
			}
		} catch(Exception e){ 
			System.out.println("Error @createQueryCriterias method, Class : EtkinliktanimFilter :" + e.getMessage()); 
		}  
		criteriaList.add(kriterler); 
		criteriaList.add(opsiyonelKriterler); 
		return criteriaList; 
	} 
	 
	public Long getRID() { 
		return this.rID; 
	} 
	 
	public void setRID(Long rID) { 
		this.rID = rID; 
	} 
  
	public String getAd() { 
		return this.ad; 
	} 
	 
	public void setAd(String ad) { 
		this.ad = ad; 
	} 
  
	public EvetHayir getPeriyodik() { 
		return this.periyodik; 
	} 
	 
	public void setPeriyodik(EvetHayir periyodik) { 
		this.periyodik = periyodik; 
	} 
  
	public String getAciklama() { 
		return this.aciklama; 
	} 
	 
	public void setAciklama(String aciklama) { 
		this.aciklama = aciklama; 
	}

	public String getAnahtarkelimeler() {
		return anahtarkelimeler;
	}

	public void setAnahtarkelimeler(String anahtarkelimeler) {
		this.anahtarkelimeler = anahtarkelimeler;
	} 
  
	
} 
