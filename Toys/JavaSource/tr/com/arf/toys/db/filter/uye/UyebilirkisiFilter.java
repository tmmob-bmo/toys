package tr.com.arf.toys.db.filter.uye; 
 
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.framework.db.model.query.QueryObject;
import tr.com.arf.toys.db.enumerated.uye.UyeBilirkisiDurum;
import tr.com.arf.toys.db.model.evrak.Evrak;
import tr.com.arf.toys.db.model.uye.Uye;
import tr.com.arf.toys.db.model.uye.Uyeuzmanlik;
import tr.com.arf.toys.utility.application.ApplicationConstant;
 
 
public class UyebilirkisiFilter extends BaseFilter { 
	
private static final long serialVersionUID = 1L; 
	
	private Uye uyeRef; 
	
	private Uyeuzmanlik uyeuzmanlikRef; 
	private Evrak evrakRef; 
	private java.util.Date tarih; 
	private java.util.Date tarihMax; 
	private java.util.Date bitistarih; 
	private java.util.Date bitistarihMax; 
	private UyeBilirkisiDurum uyebilirkisidurum; 
	private String aciklama; 
	
	@Override 
	public void init() { 
		this.uyeRef = null; 	
		this.uyeuzmanlikRef = null; 	
		this.evrakRef = null; 	
		this.tarih = null; 	
		this.tarihMax = null; 	
		this.bitistarih = null; 	
		this.bitistarihMax = null; 	
		this.uyebilirkisidurum = UyeBilirkisiDurum._NULL;	
		this.aciklama = null; 	
	} 
	 
	@Override 
	public List<ArrayList<QueryObject>> createQueryCriterias(){ 
		ArrayList<ArrayList<QueryObject>> criteriaList = new ArrayList<ArrayList<QueryObject>>(); 
		ArrayList<QueryObject> kriterler = new ArrayList<QueryObject>();	 
		ArrayList<QueryObject> opsiyonelKriterler = new ArrayList<QueryObject>(); 
		try { 	 
			if(validateQueryObject(getUyeRef(), null, false)){ 
				kriterler.add(new QueryObject("uyeRef", ApplicationConstant._equals, getUyeRef())); 
			} 
			if(validateQueryObject(getUyeuzmanlikRef(), null, false)){ 
				kriterler.add(new QueryObject("uyeuzmanlikRef", ApplicationConstant._equals, getUyeuzmanlikRef())); 
			} 
			if(validateQueryObject(getEvrakRef(), null, false)){ 
				kriterler.add(new QueryObject("evrakRef", ApplicationConstant._equals, getEvrakRef())); 
			} 
			if(validateQueryObject(getTarih(), null, false)  && validateQueryObject(getTarihMax(), null, false)){
				kriterler.add(new QueryObject("tarih", ApplicationConstant._equalsAndLargerThan, getTarih())); 
				kriterler.add(new QueryObject("tarih", ApplicationConstant._equalsAndSmallerThan, getTarihMax())); 
			} else if(validateQueryObject(getTarih(), null, false)){ 
				kriterler.add(new QueryObject("tarih", ApplicationConstant._equalsAndLargerThan, getTarih()));
			}
			if(validateQueryObject(getBitistarih(), null, false)  && validateQueryObject(getBitistarihMax(), null, false)){
				kriterler.add(new QueryObject("bitistarih", ApplicationConstant._equalsAndLargerThan, getBitistarih())); 
				kriterler.add(new QueryObject("bitistarih", ApplicationConstant._equalsAndSmallerThan, getBitistarihMax())); 
			} else if(validateQueryObject(getBitistarih(), null, false)){ 
				kriterler.add(new QueryObject("bitistarih", ApplicationConstant._equalsAndLargerThan, getBitistarih()));
			}
			if(validateQueryObject(getUyebilirkisidurum(), null, true)){ 
				kriterler.add(new QueryObject("uyebilirkisidurum", ApplicationConstant._equals, getUyebilirkisidurum())); 
			} 
			if(validateQueryObject(getAciklama(), null, false)){ 
				kriterler.add(new QueryObject("aciklama", ApplicationConstant._beginsWith, getAciklama())); 
			} 
		} catch(Exception e){ 
			System.out.println("Error @createQueryCriterias method, Class : UyebilirkisiFilter :" + e.getMessage()); 
		}  
		criteriaList.add(kriterler); 
		criteriaList.add(opsiyonelKriterler); 
		return criteriaList; 
	} 
	 
	public Uye getUyeRef() { 
		return this.uyeRef; 
	} 
	 
	public void setUyeRef(Uye uyeRef) { 
		this.uyeRef = uyeRef; 
	} 
  
	public Uyeuzmanlik getUyeuzmanlikRef() { 
		return this.uyeuzmanlikRef; 
	} 
	 
	public void setUyeuzmanlikRef(Uyeuzmanlik uyeuzmanlikRef) { 
		this.uyeuzmanlikRef = uyeuzmanlikRef; 
	} 
  
	public Evrak getEvrakRef() { 
		return this.evrakRef; 
	} 
	 
	public void setEvrakRef(Evrak evrakRef) { 
		this.evrakRef = evrakRef; 
	} 
  
	public Date getTarih() { 
		return this.tarih; 
	} 
	 
	public void setTarih(Date tarih) { 
		this.tarih = tarih; 
	} 
  
	public Date getTarihMax() { 
		return this.tarihMax; 
	} 
	 
	public void setTarihMax(Date tarihMax) { 
		this.tarihMax = tarihMax; 
	} 
  
	public Date getBitistarih() { 
		return this.bitistarih; 
	} 
	 
	public void setBitistarih(Date bitistarih) { 
		this.bitistarih = bitistarih; 
	} 
  
	public Date getBitistarihMax() { 
		return this.bitistarihMax; 
	} 
	 
	public void setBitistarihMax(Date bitistarihMax) { 
		this.bitistarihMax = bitistarihMax; 
	} 
  
	public UyeBilirkisiDurum getUyebilirkisidurum() { 
		return this.uyebilirkisidurum; 
	} 
	 
	public void setUyebilirkisidurum(UyeBilirkisiDurum uyebilirkisidurum) { 
		this.uyebilirkisidurum = uyebilirkisidurum; 
	} 
  
	public String getAciklama() { 
		return this.aciklama; 
	} 
	 
	public void setAciklama(String aciklama) { 
		this.aciklama = aciklama; 
	} 
  
	
} 
