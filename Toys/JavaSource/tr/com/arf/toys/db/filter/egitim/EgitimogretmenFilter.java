package tr.com.arf.toys.db.filter.egitim; 
 
import java.util.ArrayList;
import java.util.List;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.framework.db.model.query.QueryObject;
import tr.com.arf.toys.db.model.egitim.Egitim;
import tr.com.arf.toys.db.model.egitim.Egitmen;
import tr.com.arf.toys.utility.application.ApplicationConstant;
 
 
public class EgitimogretmenFilter extends BaseFilter { 
	
private static final long serialVersionUID = 1L; 
	
	private Egitim egitimRef; 
	
	private Long rID; 
	private Egitmen egitmenRef; 
	private String aciklama; 
	
	private String ad;
	private String soyad;
	
	@Override 
	public void init() { 
		this.rID = null; 	
		this.egitimRef = null; 	
		this.egitmenRef = null; 	
		this.aciklama = null; 	
		this.ad = null;
		this.soyad = null;
	} 
	 
	@Override 
	public List<ArrayList<QueryObject>> createQueryCriterias(){ 
		ArrayList<ArrayList<QueryObject>> criteriaList = new ArrayList<ArrayList<QueryObject>>(); 
		ArrayList<QueryObject> kriterler = new ArrayList<QueryObject>();	 
		ArrayList<QueryObject> opsiyonelKriterler = new ArrayList<QueryObject>(); 
		try { 	 
			if(validateQueryObject(getRID(), null, false)){ 
				kriterler.add(new QueryObject("rID", ApplicationConstant._equals, getRID())); 
			} 
			if(validateQueryObject(getEgitimRef(), null, false)){ 
				kriterler.add(new QueryObject("egitimRef", ApplicationConstant._equals, getEgitimRef())); 
			} 
			if(validateQueryObject(getEgitmenRef(), null, false)){ 
				kriterler.add(new QueryObject("egitmenRef", ApplicationConstant._equals, getEgitmenRef())); 
			} 
			if(validateQueryObject(getAciklama(), null, false)){ 
				kriterler.add(new QueryObject("aciklama", ApplicationConstant._beginsWith, getAciklama(), false)); 
			} 			
			if(validateQueryObject(getAd(), null, false)){ 
				kriterler.add(new QueryObject("egitmenRef.kisiRef.ad", ApplicationConstant._beginsWith, getAd())); 
			}
			if(validateQueryObject(getSoyad(), null, false)){ 
				kriterler.add(new QueryObject("egitmenRef.kisiRef.soyad", ApplicationConstant._beginsWith, getSoyad())); 
			} 
		} catch(Exception e){ 
			System.out.println("Error @createQueryCriterias method, Class : EgitimogretmenFilter :" + e.getMessage()); 
		}  
		criteriaList.add(kriterler); 
		criteriaList.add(opsiyonelKriterler); 
		return criteriaList; 
	} 
	 
	public Long getRID() { 
		return this.rID; 
	} 
	 
	public void setRID(Long rID) { 
		this.rID = rID; 
	} 
  
	public Egitim getEgitimRef() { 
		return this.egitimRef; 
	} 
	 
	public void setEgitimRef(Egitim egitimRef) { 
		this.egitimRef = egitimRef; 
	} 
  
	public Egitmen getEgitmenRef() {
		return egitmenRef;
	}

	public void setEgitmenRef(Egitmen egitmenRef) {
		this.egitmenRef = egitmenRef;
	}

	public String getAciklama() { 
		return this.aciklama; 
	} 
	 
	public void setAciklama(String aciklama) { 
		this.aciklama = aciklama; 
	} 
	public String getAd() {
		return ad;
	}

	public void setAd(String ad) {
		this.ad = ad;
	}

	public String getSoyad() {
		return soyad;
	}

	public void setSoyad(String soyad) {
		this.soyad = soyad;
	} 
	
} 
