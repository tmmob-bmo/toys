package tr.com.arf.toys.db.filter.evrak; 
 
import java.util.ArrayList;
import java.util.List;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.framework.db.model.query.QueryObject;
import tr.com.arf.toys.db.model.evrak.Dolasimsablon;
import tr.com.arf.toys.db.model.system.Birim;
import tr.com.arf.toys.db.model.system.Personel;
import tr.com.arf.toys.utility.application.ApplicationConstant;
 
 
public class DolasimsablondetayFilter extends BaseFilter { 
	
private static final long serialVersionUID = 1L; 
	
	private Dolasimsablon dolasimsablonRef; 
	
	private Long rID; 
	private int sirano; 
	private Birim birimRef; 
	private Personel personelRef; 
	
	@Override 
	public void init() { 
		this.rID = null; 	
		this.dolasimsablonRef = null; 	
		this.sirano = 0; 	
		this.birimRef = null; 	
		this.personelRef = null; 	
	} 
	 
	@Override 
	public List<ArrayList<QueryObject>> createQueryCriterias(){ 
		ArrayList<ArrayList<QueryObject>> criteriaList = new ArrayList<ArrayList<QueryObject>>(); 
		ArrayList<QueryObject> kriterler = new ArrayList<QueryObject>();	 
		ArrayList<QueryObject> opsiyonelKriterler = new ArrayList<QueryObject>(); 
		try { 	 
			if(validateQueryObject(getRID(), null, false)){ 
				kriterler.add(new QueryObject("rID", ApplicationConstant._equals, getRID())); 
			} 
			if(validateQueryObject(getDolasimsablonRef(), null, false)){ 
				kriterler.add(new QueryObject("dolasimsablonRef", ApplicationConstant._equals, getDolasimsablonRef())); 
			} 
			if(validateQueryObject(getSirano(), 0, false)){
				kriterler.add(new QueryObject("sirano", ApplicationConstant._equals, getSirano())); 	
			}
			if(validateQueryObject(getBirimRef(), null, false)){ 
				kriterler.add(new QueryObject("birimRef", ApplicationConstant._equals, getBirimRef())); 
			} 
			if(validateQueryObject(getPersonelRef(), null, false)){ 
				kriterler.add(new QueryObject("personelRef", ApplicationConstant._equals, getPersonelRef())); 
			} 
		} catch(Exception e){ 
			System.out.println("Error @createQueryCriterias method, Class : DolasimsablondetayFilter :" + e.getMessage()); 
		}  
		criteriaList.add(kriterler); 
		criteriaList.add(opsiyonelKriterler); 
		return criteriaList; 
	} 
	 
	public Long getRID() { 
		return this.rID; 
	} 
	 
	public void setRID(Long rID) { 
		this.rID = rID; 
	} 
  
	public Dolasimsablon getDolasimsablonRef() { 
		return this.dolasimsablonRef; 
	} 
	 
	public void setDolasimsablonRef(Dolasimsablon dolasimsablonRef) { 
		this.dolasimsablonRef = dolasimsablonRef; 
	} 
  
	public int getSirano() { 
		return this.sirano; 
	} 
	 
	public void setSirano(int sirano) { 
		this.sirano = sirano; 
	} 
  
	public Birim getBirimRef() { 
		return this.birimRef; 
	} 
	 
	public void setBirimRef(Birim birimRef) { 
		this.birimRef = birimRef; 
	} 
  
	public Personel getPersonelRef() { 
		return this.personelRef; 
	} 
	 
	public void setPersonelRef(Personel personelRef) { 
		this.personelRef = personelRef; 
	} 
  
	
} 
