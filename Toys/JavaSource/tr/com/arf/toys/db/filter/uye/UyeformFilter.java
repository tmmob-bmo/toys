package tr.com.arf.toys.db.filter.uye; 
 
import java.util.ArrayList;
import java.util.List;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.framework.db.model.query.QueryObject;
import tr.com.arf.toys.db.enumerated.uye.Formturu;
import tr.com.arf.toys.db.model.uye.Uye;
import tr.com.arf.toys.utility.application.ApplicationConstant;
 
 
public class UyeformFilter extends BaseFilter { 
	
private static final long serialVersionUID = 1L; 
	
	private Long rID; 
	private Uye uyeRef; 
	private Formturu formturu; 
	private String path; 
	
	@Override 
	public void init() { 
		this.rID = null; 	
		this.uyeRef = null; 	
		this.formturu = Formturu._NULL;	
		this.path = null; 	
	} 
	 
	@Override 
	public List<ArrayList<QueryObject>> createQueryCriterias(){ 
		ArrayList<ArrayList<QueryObject>> criteriaList = new ArrayList<ArrayList<QueryObject>>(); 
		ArrayList<QueryObject> kriterler = new ArrayList<QueryObject>();	 
		ArrayList<QueryObject> opsiyonelKriterler = new ArrayList<QueryObject>(); 
		try { 	 
			if(validateQueryObject(getRID(), null, false)){ 
				kriterler.add(new QueryObject("rID", ApplicationConstant._equals, getRID())); 
			} 
			if(validateQueryObject(getUyeRef(), null, false)){ 
				kriterler.add(new QueryObject("uyeRef", ApplicationConstant._equals, getUyeRef())); 
			} 
			if(validateQueryObject(getFormturu(), null, true)){ 
				kriterler.add(new QueryObject("formturu", ApplicationConstant._equals, getFormturu())); 
			} 
			if(validateQueryObject(getPath(), null, false)){ 
				kriterler.add(new QueryObject("path", ApplicationConstant._beginsWith, getPath(), false)); 
			} 
		} catch(Exception e){ 
			System.out.println("Error @createQueryCriterias method, Class : UyeformFilter :" + e.getMessage()); 
		}  
		criteriaList.add(kriterler); 
		criteriaList.add(opsiyonelKriterler); 
		return criteriaList; 
	} 
	 
	public Long getRID() { 
		return this.rID; 
	} 
	 
	public void setRID(Long rID) { 
		this.rID = rID; 
	} 
  
	public Uye getUyeRef() { 
		return this.uyeRef; 
	} 
	 
	public void setUyeRef(Uye uyeRef) { 
		this.uyeRef = uyeRef; 
	} 
  
	public Formturu getFormturu() { 
		return this.formturu; 
	} 
	 
	public void setFormturu(Formturu formturu) { 
		this.formturu = formturu; 
	} 
  
	public String getPath() { 
		return this.path; 
	} 
	 
	public void setPath(String path) { 
		this.path = path; 
	} 
  
	
} 
