package tr.com.arf.toys.db.filter.system;

import java.util.ArrayList;
import java.util.List;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.framework.db.model.query.QueryObject;
import tr.com.arf.toys.db.enumerated.system.YprojeDurum;
import tr.com.arf.toys.db.model.kisi.Kurum;
import tr.com.arf.toys.utility.application.ApplicationConstant;


public class YprojeFilter extends BaseFilter {
	
	
	
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -7902856073986886399L;
	private String ad; 
	private String aciklama; 
	private Kurum kurumRef; 
	private YprojeDurum projeDurum; 
 
	@Override
	public List<ArrayList<QueryObject>> createQueryCriterias() {
		ArrayList<ArrayList<QueryObject>> criteriaList = new ArrayList<ArrayList<QueryObject>>();
		ArrayList<QueryObject> kriterler = new ArrayList<QueryObject>();
		ArrayList<QueryObject> opsiyonelkriterler = new ArrayList<QueryObject>();
		
		try{
			if(validateQueryObject(getAd(), null, false)){
				kriterler.add(new QueryObject("ad", ApplicationConstant._beginsWith, getAd()));
			}
			if(validateQueryObject(getAciklama(), null, false)){
				kriterler.add(new QueryObject("aciklama", ApplicationConstant._beginsWith, getAciklama()));
			}
			if(validateQueryObject(getKurumRef(), null, false)){
				kriterler.add(new QueryObject("kurumRef", ApplicationConstant._beginsWith, getKurumRef()));
			}
			if(validateQueryObject(getyprojeDurum(), null, true)){
				kriterler.add(new QueryObject("yyprojeDurum", ApplicationConstant._beginsWith, getyprojeDurum()));
			}
			
			
		}catch(Exception e){
			System.out.println("Error @createQueryCriterias method, Class : yprojeFilter :" + e.getMessage());
			
		}
		criteriaList.add(kriterler);
		criteriaList.add(opsiyonelkriterler);
		return criteriaList;
		
	}
	
	
	public String getAd() { 
		return this.ad; 
	} 
	 
	public void setAd(String ad) { 
		this.ad = ad; 
	}  
 
	public String getAciklama() { 
		return this.aciklama; 
	} 
	 
	public void setAciklama(String aciklama) { 
		this.aciklama = aciklama; 
	} 	
	
	public Kurum getKurumRef() { 
		return this.kurumRef; 
	} 
	 
	public void setKurumRef(Kurum kurumRef) { 
		this.kurumRef = kurumRef; 
	} 	
	
	public YprojeDurum getyprojeDurum() { 
		return this.projeDurum; 
	} 
	 
	public void setyyprojeDurum(YprojeDurum projeDurum) { 
		this.projeDurum = projeDurum; 
	} 	

	@Override
	public void init() {
		this.kurumRef = null; 
		this.ad = ""; 
		this.aciklama = ""; 
		this.projeDurum = null; 
		
	}  
	 


}
