package tr.com.arf.toys.db.filter.kisi; 
 
import java.util.ArrayList;
import java.util.List;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.framework.db.model.query.QueryObject;
import tr.com.arf.toys.db.enumerated.kisi.GorevTuru;
import tr.com.arf.toys.utility.application.ApplicationConstant;
 
 
public class KurulGorevFilter extends BaseFilter { 
	
private static final long serialVersionUID = 1L; 
	
	private String tanim;
	private GorevTuru gorevTuru;
	
	@Override 
	public void init() { 
		this.tanim=null;
		this.gorevTuru=GorevTuru._NULL;
	} 
	 
	@Override 
	public List<ArrayList<QueryObject>> createQueryCriterias(){ 
		ArrayList<ArrayList<QueryObject>> criteriaList = new ArrayList<ArrayList<QueryObject>>(); 
		ArrayList<QueryObject> kriterler = new ArrayList<QueryObject>();	 
		ArrayList<QueryObject> opsiyonelKriterler = new ArrayList<QueryObject>(); 
		try { 	 
			if(validateQueryObject(getTanim(), null, false)){ 
				kriterler.add(new QueryObject("tanim", ApplicationConstant._beginsWith, getTanim())); 
			} 
			if(validateQueryObject(getGorevTuru(), null, true)){ 
				kriterler.add(new QueryObject("gorevturu", ApplicationConstant._equals, getGorevTuru())); 
			} 
		
		} catch(Exception e){ 
			System.out.println("Error @createQueryCriterias method, Class : KurulGorevFilter :" + e.getMessage()); 
		}  
		criteriaList.add(kriterler); 
		criteriaList.add(opsiyonelKriterler); 
		return criteriaList; 
	}

	public String getTanim() {
		return tanim;
	}

	public void setTanim(String tanim) {
		this.tanim = tanim;
	}

	public GorevTuru getGorevTuru() {
		return gorevTuru;
	}

	public void setGorevTuru(GorevTuru gorevTuru) {
		this.gorevTuru = gorevTuru;
	}
	
	 
} 
