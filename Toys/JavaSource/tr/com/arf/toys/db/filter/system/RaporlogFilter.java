package tr.com.arf.toys.db.filter.system; 
 
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.framework.db.model.query.QueryObject;
import tr.com.arf.toys.db.enumerated.system.RaporTuru;
import tr.com.arf.toys.db.model.system.Kullanici;
import tr.com.arf.toys.utility.application.ApplicationConstant;
 
 
public class RaporlogFilter extends BaseFilter { 
	
private static final long serialVersionUID = 1L; 
	
	private Long rID; 
	private Kullanici kullaniciRef; 
	private java.util.Date tarih; 
	private java.util.Date tarihMax; 
	private RaporTuru raporturu; 
	
	@Override 
	public void init() { 
		this.rID = null; 	
		this.kullaniciRef = null; 	
		this.tarih = null; 	
		this.tarihMax = null; 	
		this.raporturu = RaporTuru._NULL;	
	} 
	 
	@Override 
	public List<ArrayList<QueryObject>> createQueryCriterias(){ 
		ArrayList<ArrayList<QueryObject>> criteriaList = new ArrayList<ArrayList<QueryObject>>(); 
		ArrayList<QueryObject> kriterler = new ArrayList<QueryObject>();	 
		ArrayList<QueryObject> opsiyonelKriterler = new ArrayList<QueryObject>(); 
		try { 	 
			if(validateQueryObject(getRID(), null, false)){ 
				kriterler.add(new QueryObject("rID", ApplicationConstant._equals, getRID())); 
			} 
			if(validateQueryObject(getKullaniciRef(), null, false)){ 
				kriterler.add(new QueryObject("kullaniciRef", ApplicationConstant._equals, getKullaniciRef())); 
			} 
			if(validateQueryObject(getTarih(), null, false)  && validateQueryObject(getTarihMax(), null, false)){
				kriterler.add(new QueryObject("tarih", ApplicationConstant._equalsAndLargerThan, getTarih())); 
				kriterler.add(new QueryObject("tarih", ApplicationConstant._equalsAndSmallerThan, getTarihMax())); 
			} else if(validateQueryObject(getTarih(), null, false)){ 
				kriterler.add(new QueryObject("tarih", ApplicationConstant._equalsAndLargerThan, getTarih()));
			}
			if(validateQueryObject(getRaporturu(), null, true)){ 
				kriterler.add(new QueryObject("raporturu", ApplicationConstant._equals, getRaporturu())); 
			} 
		} catch(Exception e){ 
			System.out.println("Error @createQueryCriterias method, Class : RaporlogFilter :" + e.getMessage()); 
		}  
		criteriaList.add(kriterler); 
		criteriaList.add(opsiyonelKriterler); 
		return criteriaList; 
	} 
	 
	public Long getRID() { 
		return this.rID; 
	} 
	 
	public void setRID(Long rID) { 
		this.rID = rID; 
	} 
  
	public Kullanici getKullaniciRef() { 
		return this.kullaniciRef; 
	} 
	 
	public void setKullaniciRef(Kullanici kullaniciRef) { 
		this.kullaniciRef = kullaniciRef; 
	} 
  
	public Date getTarih() { 
		return this.tarih; 
	} 
	 
	public void setTarih(Date tarih) { 
		this.tarih = tarih; 
	} 
  
	public Date getTarihMax() { 
		return this.tarihMax; 
	} 
	 
	public void setTarihMax(Date tarihMax) { 
		this.tarihMax = tarihMax; 
	} 
  
	public RaporTuru getRaporturu() { 
		return this.raporturu; 
	} 
	 
	public void setRaporturu(RaporTuru raporturu) { 
		this.raporturu = raporturu; 
	} 
  
	
} 
