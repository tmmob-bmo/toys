package tr.com.arf.toys.db.filter.system; 
 
import java.util.ArrayList;
import java.util.List;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.framework.db.model.query.QueryObject;
import tr.com.arf.toys.utility.application.ApplicationConstant;
 
 
public class KomisyonFilter extends BaseFilter { 
	
private static final long serialVersionUID = 1L; 
	
	private String ad; 
	private String kisaad; 
	private String erisimKodu; 
	
	@Override 
	public void init() { 
		this.ad = null; 	
		this.kisaad = null; 	
		this.erisimKodu = null; 	
	} 
	 
	@Override 
	public List<ArrayList<QueryObject>> createQueryCriterias(){ 
		ArrayList<ArrayList<QueryObject>> criteriaList = new ArrayList<ArrayList<QueryObject>>(); 
		ArrayList<QueryObject> kriterler = new ArrayList<QueryObject>();	 
		ArrayList<QueryObject> opsiyonelKriterler = new ArrayList<QueryObject>(); 
		try { 	 
			if(validateQueryObject(getAd(), null, false)){ 
				kriterler.add(new QueryObject("ad", ApplicationConstant._beginsWith, getAd(), false)); 
			} 
			if(validateQueryObject(getKisaad(), null, false)){ 
				kriterler.add(new QueryObject("kisaad", ApplicationConstant._beginsWith, getKisaad(), false)); 
			} 
			if(validateQueryObject(getErisimKodu(), null, false)){ 
				kriterler.add(new QueryObject("erisimKodu", ApplicationConstant._beginsWith, getErisimKodu(), false)); 
			} 
		} catch(Exception e){ 
			System.out.println("Error @createQueryCriterias method, Class : KomisyonFilter :" + e.getMessage()); 
		}  
		criteriaList.add(kriterler); 
		criteriaList.add(opsiyonelKriterler); 
		return criteriaList; 
	} 
	 
	public String getAd() { 
		return this.ad; 
	} 
	 
	public void setAd(String ad) { 
		this.ad = ad; 
	} 
  
	public String getKisaad() { 
		return this.kisaad; 
	} 
	 
	public void setKisaad(String kisaad) { 
		this.kisaad = kisaad; 
	} 
  
	public String getErisimKodu() { 
		return this.erisimKodu; 
	} 
	 
	public void setErisimKodu(String erisimKodu) { 
		this.erisimKodu = erisimKodu; 
	} 
	
} 
