package tr.com.arf.toys.db.filter.finans; 
 
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.framework.db.model.query.QueryObject;
import tr.com.arf.toys.db.enumerated.uye.OdemeTip;
import tr.com.arf.toys.db.model.finans.Borc;
import tr.com.arf.toys.utility.application.ApplicationConstant;
 
 
public class BorcodemeFilter extends BaseFilter { 
	
private static final long serialVersionUID = 1L; 
	
	private Borc borcRef; 
	
	private java.util.Date tarih; 
	private java.util.Date tarihMax; 
	private BigDecimal miktar; 
	private OdemeTip odemetip; 
	
	@Override 
	public void init() { 
		this.borcRef = null; 	
		this.tarih = null; 	
		this.tarihMax = null; 	
		this.miktar = null; 	
		this.odemetip = OdemeTip._NULL;	
	} 
	 
	@Override 
	public List<ArrayList<QueryObject>> createQueryCriterias(){ 
		ArrayList<ArrayList<QueryObject>> criteriaList = new ArrayList<ArrayList<QueryObject>>(); 
		ArrayList<QueryObject> kriterler = new ArrayList<QueryObject>();	 
		ArrayList<QueryObject> opsiyonelKriterler = new ArrayList<QueryObject>(); 
		try { 	 
			if(validateQueryObject(getBorcRef(), null, false)){ 
				kriterler.add(new QueryObject("borcRef", ApplicationConstant._equals, getBorcRef())); 
			} 
			if(validateQueryObject(getTarih(), null, false)  && validateQueryObject(getTarihMax(), null, false)){
				kriterler.add(new QueryObject("tarih", ApplicationConstant._equalsAndLargerThan, getTarih())); 
				kriterler.add(new QueryObject("tarih", ApplicationConstant._equalsAndSmallerThan, getTarihMax())); 
			} else if(validateQueryObject(getTarih(), null, false)){ 
				kriterler.add(new QueryObject("tarih", ApplicationConstant._equalsAndLargerThan, getTarih()));
			}
			if(validateQueryObject(getMiktar(), 0, false)){
				kriterler.add(new QueryObject("miktar", ApplicationConstant._equals, getMiktar())); 	
			}
			if(validateQueryObject(getOdemetip(), null, true)){ 
				kriterler.add(new QueryObject("odemetip", ApplicationConstant._equals, getOdemetip())); 
			} 
		} catch(Exception e){ 
			System.out.println("Error @createQueryCriterias method, Class : BorcodemeFilter :" + e.getMessage()); 
		}  
		criteriaList.add(kriterler); 
		criteriaList.add(opsiyonelKriterler); 
		return criteriaList; 
	} 
	 
	public Borc getBorcRef() { 
		return this.borcRef; 
	} 
	 
	public void setBorcRef(Borc borcRef) { 
		this.borcRef = borcRef; 
	} 
  
	public Date getTarih() { 
		return this.tarih; 
	} 
	 
	public void setTarih(Date tarih) { 
		this.tarih = tarih; 
	} 
  
	public Date getTarihMax() { 
		return this.tarihMax; 
	} 
	 
	public void setTarihMax(Date tarihMax) { 
		this.tarihMax = tarihMax; 
	} 
  
	public BigDecimal getMiktar() { 
		return this.miktar; 
	} 
	 
	public void setMiktar(BigDecimal miktar) { 
		this.miktar = miktar; 
	} 
  
	public OdemeTip getOdemetip() { 
		return this.odemetip; 
	} 
	 
	public void setOdemetip(OdemeTip odemetip) { 
		this.odemetip = odemetip; 
	} 
  
	
} 
