package tr.com.arf.toys.db.filter.system;

import java.util.ArrayList;
import java.util.List;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.framework.db.model.query.QueryObject;
import tr.com.arf.toys.db.enumerated.system.BirimTip;
import tr.com.arf.toys.db.model.system.Ilce;
import tr.com.arf.toys.db.model.system.Sehir;
import tr.com.arf.toys.db.model.system.Ybirim;
import tr.com.arf.toys.utility.application.ApplicationConstant;



public class YbirimFilter extends BaseFilter{
	
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -102205955017286673L;
	
	private Ybirim ustRef;  
	private BirimTip birimTip;
	private Ilce ilceRef;
	private Sehir sehirRef;
	private Long rid;

	
	
	@Override
	public List<ArrayList<QueryObject>> createQueryCriterias() {
		ArrayList<ArrayList<QueryObject>> criteriaList = new ArrayList<ArrayList<QueryObject>>(); 
		ArrayList<QueryObject> kriterler = new ArrayList<QueryObject>();	 
		ArrayList<QueryObject> opsiyonelKriterler = new ArrayList<QueryObject>(); 
		try { 
			if(validateQueryObject(getRid(), 0, false)){ 
				kriterler.add(new QueryObject("rID", ApplicationConstant._equals, getRid())); 
			} else {
				if(validateQueryObject(getUstRef(), null, false)){ 
					kriterler.add(new QueryObject("ustRef", ApplicationConstant._equals, getUstRef())); 
				} 
				
				
				if(validateQueryObject(getSehirRef(), null, false)){ 
					kriterler.add(new QueryObject("sehirRef", ApplicationConstant._equals, getSehirRef())); 
				} 
				if(validateQueryObject(getIlceRef(), null, false)){ 
					kriterler.add(new QueryObject("ilceRef", ApplicationConstant._equals, getIlceRef())); 
				} 
				if(validateQueryObject(getybirimtip(), null, true)){ 
					kriterler.add(new QueryObject("ybirimtip", ApplicationConstant._equals, getybirimtip())); 
				} 
				
				// Kullanicinin ybirimine gore yetkilendirme yapiliyor.
			}
		} catch(Exception e){ 
			System.out.println("Error @createQueryCriterias method, Class : ybirimFilter :" + e.getMessage()); 
		}  
		criteriaList.add(kriterler); 
		criteriaList.add(opsiyonelKriterler); 
		return criteriaList; 
	}

	
	public Ybirim getUstRef() { 
		return ustRef; 
	}  

	
	
	public void setUstRef(Ybirim ustRef) {
		this.ustRef = ustRef;
	}  
  
	
	public Sehir getSehirRef() { 
		return sehirRef; 
	} 
	 
	public void setSehirRef(Sehir sehirRef) { 
		this.sehirRef = sehirRef; 
	} 
 
	public Ilce getIlceRef() { 
		return ilceRef; 
	} 
	 
	public void setIlceRef(Ilce ilceRef) { 
		this.ilceRef = ilceRef; 
	} 
 
	public BirimTip getybirimtip() { 
		return this.birimTip;  
	}  
  
	public void setybirimtip(BirimTip birimtip) {  
		this.birimTip = birimtip;  
	} 
	 
	public Long getRid() {
		return rid;
	}

	public void setRid(Long rid) {
		this.rid = rid;
	}
 

	


	@Override
	public void init() {
		this.ustRef = null; 
		this.sehirRef = null; 
		this.ilceRef = null; 
		this.birimTip = BirimTip._NULL;
		
	}
	 
}
