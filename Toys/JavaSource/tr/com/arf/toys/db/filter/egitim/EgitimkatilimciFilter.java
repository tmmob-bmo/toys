package tr.com.arf.toys.db.filter.egitim; 
 
import java.util.ArrayList;
import java.util.List;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.framework.db.model.query.QueryObject;
import tr.com.arf.toys.db.enumerated.egitim.EgitimkatilimciDurum;
import tr.com.arf.toys.db.enumerated.egitim.KatilimTuru;
import tr.com.arf.toys.db.enumerated.egitim.Katilimbelgesi;
import tr.com.arf.toys.db.enumerated.egitim.Sertifika;
import tr.com.arf.toys.db.enumerated.egitim.SinavbasariDurum;
import tr.com.arf.toys.db.model.egitim.Egitim;
import tr.com.arf.toys.db.model.kisi.Kisi;
import tr.com.arf.toys.utility.application.ApplicationConstant;
 
 
public class EgitimkatilimciFilter extends BaseFilter { 
	
private static final long serialVersionUID = 1L; 
	
	private Egitim egitimRef; 
	
	private Long rID; 
	private Kisi kisiRef; 
	private EgitimkatilimciDurum durum; 
	private Katilimbelgesi katilimbelgesi; 
	private Sertifika sertifika; 
	private String sinavnotu; 
	private SinavbasariDurum sinavbasariDurum; 
	private String aciklama; 
	private KatilimTuru katilimturu;
	
	private String ad;
	private String soyad;
	
	@Override 
	public void init() { 
		this.rID = null; 	
		this.egitimRef = null; 	
		this.kisiRef = null; 	
		this.durum = EgitimkatilimciDurum._NULL;	
		this.katilimbelgesi = Katilimbelgesi._NULL;	
		this.sertifika = Sertifika._NULL;	
		this.sinavnotu = null; 	
		this.sinavbasariDurum = SinavbasariDurum._NULL;	
		this.aciklama = null; 	
		this.ad = null;
		this.soyad = null;
		this.katilimturu=null;
	} 
	 
	@Override 
	public List<ArrayList<QueryObject>> createQueryCriterias(){ 
		ArrayList<ArrayList<QueryObject>> criteriaList = new ArrayList<ArrayList<QueryObject>>(); 
		ArrayList<QueryObject> kriterler = new ArrayList<QueryObject>();	 
		ArrayList<QueryObject> opsiyonelKriterler = new ArrayList<QueryObject>(); 
		try { 	 
			if(validateQueryObject(getRID(), null, false)){ 
				kriterler.add(new QueryObject("rID", ApplicationConstant._equals, getRID())); 
			} 
			if(validateQueryObject(getEgitimRef(), null, false)){ 
				kriterler.add(new QueryObject("egitimRef", ApplicationConstant._equals, getEgitimRef())); 
			} 
			if(validateQueryObject(getKisiRef(), null, false)){ 
				kriterler.add(new QueryObject("kisiRef", ApplicationConstant._equals, getKisiRef())); 
			} 
			if(validateQueryObject(getDurum(), null, true)){ 
				kriterler.add(new QueryObject("durum", ApplicationConstant._equals, getDurum())); 
			} 
			if(validateQueryObject(getKatilimbelgesi(), null, true)){ 
				kriterler.add(new QueryObject("katilimbelgesi", ApplicationConstant._equals, getKatilimbelgesi())); 
			} 
			if(validateQueryObject(getSertifika(), null, true)){ 
				kriterler.add(new QueryObject("sertifika", ApplicationConstant._equals, getSertifika())); 
			} 
			if(validateQueryObject(getSinavnotu(), null, false)){ 
				kriterler.add(new QueryObject("sinavnotu", ApplicationConstant._beginsWith, getSinavnotu(), false)); 
			} 
			if(validateQueryObject(getSinavbasariDurum(), null, true)){ 
				kriterler.add(new QueryObject("sinavbasariDurum", ApplicationConstant._equals, getSinavbasariDurum())); 
			} 
			if(validateQueryObject(getKatilimturu(), null, true)){ 
				kriterler.add(new QueryObject("katilimturu", ApplicationConstant._equals, getKatilimturu())); 
			} 
			if(validateQueryObject(getAciklama(), null, false)){ 
				kriterler.add(new QueryObject("aciklama", ApplicationConstant._beginsWith, getAciklama(), false)); 
			} 
			if(validateQueryObject(getAd(), null, false)){ 
				kriterler.add(new QueryObject("kisiRef.ad", ApplicationConstant._beginsWith, getAd())); 
			}
			if(validateQueryObject(getSoyad(), null, false)){ 
				kriterler.add(new QueryObject("kisiRef.soyad", ApplicationConstant._beginsWith, getSoyad())); 
			} 
		} catch(Exception e){ 
			System.out.println("Error @createQueryCriterias method, Class : EgitimkatilimciFilter :" + e.getMessage()); 
		}  
		criteriaList.add(kriterler); 
		criteriaList.add(opsiyonelKriterler); 
		return criteriaList; 
	} 	 
	
	public String getAd() {
		return ad;
	}

	public void setAd(String ad) {
		this.ad = ad;
	}

	public String getSoyad() {
		return soyad;
	}

	public void setSoyad(String soyad) {
		this.soyad = soyad;
	}

	public Long getRID() { 
		return this.rID; 
	} 
	 
	public void setRID(Long rID) { 
		this.rID = rID; 
	} 
  
	public Egitim getEgitimRef() { 
		return this.egitimRef; 
	} 
	 
	public void setEgitimRef(Egitim egitimRef) { 
		this.egitimRef = egitimRef; 
	} 
  
	public Kisi getKisiRef() { 
		return this.kisiRef; 
	} 
	 
	public void setKisiRef(Kisi kisiRef) { 
		this.kisiRef = kisiRef; 
	} 
  
	public EgitimkatilimciDurum getDurum() { 
		return this.durum; 
	} 
	 
	public void setDurum(EgitimkatilimciDurum durum) { 
		this.durum = durum; 
	} 
  
	public Katilimbelgesi getKatilimbelgesi() { 
		return this.katilimbelgesi; 
	} 
	 
	public void setKatilimbelgesi(Katilimbelgesi katilimbelgesi) { 
		this.katilimbelgesi = katilimbelgesi; 
	} 
  
	public Sertifika getSertifika() { 
		return this.sertifika; 
	} 
	 
	public void setSertifika(Sertifika sertifika) { 
		this.sertifika = sertifika; 
	} 
  
	public String getSinavnotu() { 
		return this.sinavnotu; 
	} 
	 
	public void setSinavnotu(String sinavnotu) { 
		this.sinavnotu = sinavnotu; 
	} 
  
	public SinavbasariDurum getSinavbasariDurum() { 
		return this.sinavbasariDurum; 
	} 
	 
	public void setSinavbasariDurum(SinavbasariDurum sinavbasariDurum) { 
		this.sinavbasariDurum = sinavbasariDurum; 
	} 
  
	public String getAciklama() { 
		return this.aciklama; 
	} 
	 
	public void setAciklama(String aciklama) { 
		this.aciklama = aciklama; 
	}

	public KatilimTuru getKatilimturu() {
		return katilimturu;
	}

	public void setKatilimturu(KatilimTuru katilimturu) {
		this.katilimturu = katilimturu;
	} 
} 
