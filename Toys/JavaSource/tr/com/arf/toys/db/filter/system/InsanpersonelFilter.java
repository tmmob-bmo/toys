package tr.com.arf.toys.db.filter.system;



import java.util.ArrayList;
import java.util.List;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.framework.db.model.query.QueryObject;
import tr.com.arf.toys.db.model.system.Insan;
import tr.com.arf.toys.utility.application.ApplicationConstant;

public class InsanpersonelFilter extends BaseFilter{
	
     /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Insan insanref;
	private Long sicilno;
	private String adsoyad;
	
	private String ad;
	private String soyad;
	 

		

		public Insan getInsanref() {
			return insanref;
		}

		public void setInsanref(Insan insanref) {
			this.insanref = insanref;
		}

		public Long getSicilno() {
			return sicilno;
		}

		public void setSicilno(Long sicilno) {
			this.sicilno = sicilno;
		}

		public String getAdsoyad() {
			return adsoyad;
		}

		public void setAdsoyad(String adsoyad) {
			this.adsoyad = adsoyad;
		}
		
		public String getAd() {
			return ad;
		}

		public void setAd(String ad) {
			this.ad = ad;
		}
		
		public String getSoyad() {
			return soyad;
		}

		public void setSoyad(String soyad) {
			this.soyad = soyad;
		}
	 
	 
	@Override
	public List<ArrayList<QueryObject>> createQueryCriterias() {
		ArrayList<ArrayList<QueryObject>> criteriaList = new ArrayList<ArrayList<QueryObject>>(); 
		ArrayList<QueryObject> kriterler = new ArrayList<QueryObject>();	 
		ArrayList<QueryObject> opsiyonelKriterler = new ArrayList<QueryObject>(); 
		try { 	 
			if(validateQueryObject(getSicilno(), null, false)){ 
				kriterler.add(new QueryObject("ad", ApplicationConstant._equals, getSicilno())); 
			} 
			if(validateQueryObject(getInsanref(), null, false)){ 
				kriterler.add(new QueryObject("soyad", ApplicationConstant._equals, getInsanref())); 
			} 
			if(validateQueryObject(getAd(), null, false)){ 
				kriterler.add(new QueryObject("insanRef.ad", ApplicationConstant._beginsWith, getAd())); 
			}
			if(validateQueryObject(getSoyad(), null, false)){ 
				kriterler.add(new QueryObject("insanRef.soyad", ApplicationConstant._beginsWith, getSoyad())); 
			} 
			if(validateQueryObject(getAdsoyad(), null, false)){ 
				opsiyonelKriterler.add(new QueryObject("insanRef.ad", ApplicationConstant._inside, getAdsoyad().trim(), false)); 
				opsiyonelKriterler.add(new QueryObject("insanRef.soyad", ApplicationConstant._inside, getAdsoyad().trim(), false));
			} 
			
		} catch(Exception e){ 
			System.out.println("Error @createQueryCriterias method, Class : InsanpersonelFilter :" + e.getMessage()); 
		}  
		criteriaList.add(kriterler); 
		criteriaList.add(opsiyonelKriterler); 
		return criteriaList; 
	}
	@Override
	public void init() {
		this.insanref=null;
		this.sicilno=null;
		this.adsoyad = null;
		this.ad = null;
		this.soyad = null;
		
	}
	
	

}
