package tr.com.arf.toys.db.filter.kisi; 
 
import java.util.ArrayList;
import java.util.List;

import tr.com.arf.framework.db.enumerated._common.EvetHayir;
import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.framework.db.model.query.QueryObject;
import tr.com.arf.toys.db.enumerated.kisi.GorevTuru;
import tr.com.arf.toys.db.enumerated.kisi.KurulTuru;
import tr.com.arf.toys.db.enumerated.kisi.KurulUyelikDurumu;
import tr.com.arf.toys.db.enumerated.kisi.UyeTuru;
import tr.com.arf.toys.db.model.kisi.Donem;
import tr.com.arf.toys.db.model.kisi.KurulDonem;
import tr.com.arf.toys.db.model.kisi.KurulGorev;
import tr.com.arf.toys.db.model.uye.Uye;
import tr.com.arf.toys.utility.application.ApplicationConstant;
 
 
public class KurulDonemUyeFilter extends BaseFilter { 
	
private static final long serialVersionUID = 1L; 
	
	private KurulDonem kurulDonemRef;
	private Uye uyeRef;
	private KurulGorev kurulGorevRef;

	private Donem donemRef;
	private Boolean delegeFilter;

	private GorevTuru gorevTuru;
	private UyeTuru uyeTuru;
	private EvetHayir aktif;
	private KurulUyelikDurumu kurulUyelikDurumu;
	private java.util.Date durumTarihi;
	private java.util.Date durumTarihiMax;
	
	@Override 
	public void init() { 
		this.kurulDonemRef=null;
		this.uyeRef=null;
		this.kurulGorevRef=null;
		this.delegeFilter = false;
		this.gorevTuru = GorevTuru._NULL;
		this.uyeTuru = UyeTuru._NULL;
		this.aktif = EvetHayir._EVET;	
		this.kurulUyelikDurumu=KurulUyelikDurumu._NULL;
		this.durumTarihi=null;
		this.durumTarihiMax=null;
	} 
	 
	@Override 
	public List<ArrayList<QueryObject>> createQueryCriterias(){ 
		ArrayList<ArrayList<QueryObject>> criteriaList = new ArrayList<ArrayList<QueryObject>>(); 
		ArrayList<QueryObject> kriterler = new ArrayList<QueryObject>();	 
		ArrayList<QueryObject> opsiyonelKriterler = new ArrayList<QueryObject>(); 
		try { 	 
			if(validateQueryObject(getKurulDonemRef(), null, false)){ 
				kriterler.add(new QueryObject("kurulDonemRef", ApplicationConstant._equals, getKurulDonemRef())); 
			} 
			if(validateQueryObject(getDonemRef(), null, false)){ 
				kriterler.add(new QueryObject("kurulDonemRef.donemRef.rID", ApplicationConstant._equals, getDonemRef().getRID())); 
			}
			if(validateQueryObject(getUyeRef(), null, false)){ 
				kriterler.add(new QueryObject("uyeRef", ApplicationConstant._equals, getUyeRef())); 
			}
			if(validateQueryObject(getKurulGorevRef(), null, false)){ 
				kriterler.add(new QueryObject("kurulGorevRef", ApplicationConstant._equals, getKurulGorevRef())); 
			}
			
			if(validateQueryObject(getGorevTuru(), null, false)){ 
				kriterler.add(new QueryObject("gorevTuru", ApplicationConstant._equals, getGorevTuru())); 
			}
			if(validateQueryObject(getUyeTuru(), null, false)){ 
				kriterler.add(new QueryObject("uyeTuru", ApplicationConstant._equals, getUyeTuru())); 
			}
			if(validateQueryObject(getAktif(), null, false)){ 
				kriterler.add(new QueryObject("aktif", ApplicationConstant._equals, getAktif())); 
			}
			if(validateQueryObject(getKurulUyelikDurumu(), null, false)){ 
				kriterler.add(new QueryObject("kurulUyelikDurumu", ApplicationConstant._equals, getKurulUyelikDurumu())); 
			}
			if(validateQueryObject(getDurumTarihi(), null, false)  && validateQueryObject(getDurumTarihiMax(), null, false)){
				kriterler.add(new QueryObject("durumtarihi", ApplicationConstant._equalsAndLargerThan, getDurumTarihi())); 
				kriterler.add(new QueryObject("durumtarihi", ApplicationConstant._equalsAndSmallerThan, getDurumTarihiMax())); 
			} else if(validateQueryObject(getDurumTarihi(), null, false)){ 
				kriterler.add(new QueryObject("durumtarihi", ApplicationConstant._equalsAndLargerThan, getDurumTarihi()));
			}
			
			if(delegeFilter != null){
				if(delegeFilter){
					kriterler.add(new QueryObject("kurulDonemRef.kurulRef.kurulturu", ApplicationConstant._equals, KurulTuru._GENELKURUL)); 
				}
			}
		} catch(Exception e){ 
			System.out.println("Error @createQueryCriterias method, Class : KurulDonemUyeFilter :" + e.getMessage()); 
		}  
		criteriaList.add(kriterler); 
		criteriaList.add(opsiyonelKriterler); 
		return criteriaList; 
	}

	public KurulDonem getKurulDonemRef() {
		return kurulDonemRef;
	}

	public void setKurulDonemRef(KurulDonem kurulDonemRef) {
		this.kurulDonemRef = kurulDonemRef;
	}

	public Uye getUyeRef() {
		return uyeRef;
	}

	public void setUyeRef(Uye uyeRef) {
		this.uyeRef = uyeRef;
	}

	public KurulGorev getKurulGorevRef() {
		return kurulGorevRef;
	}

	public void setKurulGorevRef(KurulGorev kurulGorevRef) {
		this.kurulGorevRef = kurulGorevRef;
	}

	public Donem getDonemRef() {
		return donemRef;
	}

	public void setDonemRef(Donem donemRef) {
		this.donemRef = donemRef;
	}

	public Boolean getDelegeFilter() {
		return delegeFilter;
	}

	public void setDelegeFilter(Boolean delegeFilter) {
		this.delegeFilter = delegeFilter;
	}

	public GorevTuru getGorevTuru() {
		return gorevTuru;
	}

	public void setGorevTuru(GorevTuru gorevTuru) {
		this.gorevTuru = gorevTuru;
	}

	public UyeTuru getUyeTuru() {
		return uyeTuru;
	}

	public void setUyeTuru(UyeTuru uyeTuru) {
		this.uyeTuru = uyeTuru;
	}

	public EvetHayir getAktif() {
		return aktif;
	}

	public void setAktif(EvetHayir aktif) {
		this.aktif = aktif;
	}

	public KurulUyelikDurumu getKurulUyelikDurumu() {
		return kurulUyelikDurumu;
	}

	public void setKurulUyelikDurumu(KurulUyelikDurumu kurulUyelikDurumu) {
		this.kurulUyelikDurumu = kurulUyelikDurumu;
	}

	public java.util.Date getDurumTarihi() {
		return durumTarihi;
	}

	public void setDurumTarihi(java.util.Date durumTarihi) {
		this.durumTarihi = durumTarihi;
	}

	public java.util.Date getDurumTarihiMax() {
		return durumTarihiMax;
	}

	public void setDurumTarihiMax(java.util.Date durumTarihiMax) {
		this.durumTarihiMax = durumTarihiMax;
	}
	
	
 
} 
