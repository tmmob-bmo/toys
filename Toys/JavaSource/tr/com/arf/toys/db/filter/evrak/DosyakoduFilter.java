package tr.com.arf.toys.db.filter.evrak; 
 
import java.util.ArrayList;
import java.util.List;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.framework.db.model.query.QueryObject;
import tr.com.arf.toys.db.model.evrak.Dosyakodu;
import tr.com.arf.toys.utility.application.ApplicationConstant;
 
 
public class DosyakoduFilter extends BaseFilter { 
	
private static final long serialVersionUID = 1L; 
	
	private Dosyakodu ustRef; 
	private String kod; 
	private String ad; 
	private String erisimKodu; 
	
	@Override 
	public void init() { 
		this.ustRef = null; 	
		this.kod = null; 	
		this.ad = null; 	
		this.erisimKodu = null; 	
	} 
	 
	@Override 
	public List<ArrayList<QueryObject>> createQueryCriterias(){ 
		ArrayList<ArrayList<QueryObject>> criteriaList = new ArrayList<ArrayList<QueryObject>>(); 
		ArrayList<QueryObject> kriterler = new ArrayList<QueryObject>();	 
		ArrayList<QueryObject> opsiyonelKriterler = new ArrayList<QueryObject>(); 
		try { 	 
			if(validateQueryObject(getUstRef(), null, false)){ 
				kriterler.add(new QueryObject("ustRef", ApplicationConstant._equals, getUstRef())); 
			} 
			if(validateQueryObject(getKod(), null, false)){ 
				kriterler.add(new QueryObject("kod", ApplicationConstant._beginsWith, getKod(), false)); 
			} 
			if(validateQueryObject(getAd(), null, false)){ 
				kriterler.add(new QueryObject("ad", ApplicationConstant._beginsWith, getAd(), false)); 
			} 
			if(validateQueryObject(getErisimKodu(), null, false)){ 
				kriterler.add(new QueryObject("erisimKodu", ApplicationConstant._beginsWith, getErisimKodu(), false)); 
			} 
		} catch(Exception e){ 
			System.out.println("Error @createQueryCriterias method, Class : DosyakoduFilter :" + e.getMessage()); 
		}  
		criteriaList.add(kriterler); 
		criteriaList.add(opsiyonelKriterler); 
		return criteriaList; 
	} 
	 
	public Dosyakodu getUstRef() { 
		return this.ustRef; 
	} 
	 
	public void setUstRef(Dosyakodu ustRef) { 
		this.ustRef = ustRef; 
	} 
  
	public String getKod() { 
		return this.kod; 
	} 
	 
	public void setKod(String kod) { 
		this.kod = kod; 
	} 
  
	public String getAd() { 
		return this.ad; 
	} 
	 
	public void setAd(String ad) { 
		this.ad = ad; 
	} 
  
	public String getErisimKodu() { 
		return this.erisimKodu; 
	} 
	 
	public void setErisimKodu(String erisimKodu) { 
		this.erisimKodu = erisimKodu; 
	} 
  
	
} 
