package tr.com.arf.toys.db.filter.system; 
 
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import tr.com.arf.framework.db.enumerated._common.IslemTuru;
import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.framework.db.model.query.QueryObject;
import tr.com.arf.toys.utility.application.ApplicationConstant;
 
 
public class IslemlogFilter extends BaseFilter { 
	
private static final long serialVersionUID = 1L; 
	
	private IslemTuru islemturu; 
	private java.util.Date islemtarihi; 
	private java.util.Date islemtarihiMax; 
	private String tabloadi; 
	private Long referansRID;  
	
	@Override 
	public void init() { 
		this.islemturu = IslemTuru._NULL;	
		this.islemtarihi = null; 	
		this.islemtarihiMax = null; 	
		this.tabloadi = null; 	
		this.referansRID = null; 	 
	} 
	 
	@Override 
	public List<ArrayList<QueryObject>> createQueryCriterias(){ 
		ArrayList<ArrayList<QueryObject>> criteriaList = new ArrayList<ArrayList<QueryObject>>(); 
		ArrayList<QueryObject> kriterler = new ArrayList<QueryObject>();	 
		ArrayList<QueryObject> opsiyonelKriterler = new ArrayList<QueryObject>(); 
		try { 	 
			if(validateQueryObject(getIslemturu(), null, true)){ 
				kriterler.add(new QueryObject("islemturu", ApplicationConstant._equals, getIslemturu())); 
			}  
			if(validateQueryObject(getIslemtarihi(), null, false)  && validateQueryObject(getIslemtarihiMax(), null, false)){
				kriterler.add(new QueryObject("islemtarihi", ApplicationConstant._equalsAndLargerThan, getIslemtarihi())); 
				kriterler.add(new QueryObject("islemtarihi", ApplicationConstant._equalsAndSmallerThan, getIslemtarihiMax())); 
			} else if(validateQueryObject(getIslemtarihi(), null, false)){ 
				kriterler.add(new QueryObject("islemtarihi", ApplicationConstant._equalsAndLargerThan, getIslemtarihi()));
			} 
			if(validateQueryObject(getTabloadi(), null, false)){ 
				kriterler.add(new QueryObject("tabloadi", ApplicationConstant._equals, getTabloadi(), false)); 
			}  
			if(validateQueryObject(getReferansRID(), 0, false)){ 
				kriterler.add(new QueryObject("referansRID", ApplicationConstant._equals, getReferansRID())); 
			}  
		} catch(Exception e){ 
			System.out.println("Error @createQueryCriterias method, Class : IslemlogFilter :" + e.getMessage()); 
		}  
		criteriaList.add(kriterler); 
		criteriaList.add(opsiyonelKriterler); 
		return criteriaList; 
	} 
	 
	public IslemTuru getIslemturu() { 
		return this.islemturu; 
	} 
	 
	public void setIslemturu(IslemTuru islemturu) { 
		this.islemturu = islemturu; 
	} 
  
	public Date getIslemtarihi() { 
		return this.islemtarihi; 
	} 
	 
	public void setIslemtarihi(Date islemtarihi) { 
		this.islemtarihi = islemtarihi; 
	} 
  
	public Date getIslemtarihiMax() { 
		return this.islemtarihiMax; 
	} 
	 
	public void setIslemtarihiMax(Date islemtarihiMax) { 
		this.islemtarihiMax = islemtarihiMax; 
	} 
  
	public String getTabloadi() { 
		return this.tabloadi; 
	} 
	 
	public void setTabloadi(String tabloadi) { 
		this.tabloadi = tabloadi; 
	} 
  
	public Long getReferansRID() { 
		return this.referansRID; 
	} 
	 
	public void setReferansRID(Long referansRID) { 
		this.referansRID = referansRID; 
	}  
  
	
} 
