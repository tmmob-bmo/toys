package tr.com.arf.toys.db.filter.kisi; 
 
import java.util.ArrayList;
import java.util.List;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.framework.db.model.query.QueryObject;
import tr.com.arf.toys.db.model.kisi.Kisi;
import tr.com.arf.toys.db.model.system.Ilce;
import tr.com.arf.toys.db.model.system.KurumTuru;
import tr.com.arf.toys.db.model.system.Sehir;
import tr.com.arf.toys.utility.application.ApplicationConstant;
 
 
public class KurumFilter extends BaseFilter { 
	
private static final long serialVersionUID = 1L; 
	
	private String ad; 
	private String vergidairesi; 
	private String vergino; 
	private Kisi sorumluRef; 
	private KurumTuru  kurumTuruRef;
	private String aciklama; 
	private Sehir sehirRef ;
	private Ilce ilceRef;
	
	@Override 
	public void init() { 
		this.ad = null; 	
		this.kurumTuruRef=null;
		this.vergidairesi = null; 	
		this.vergino = null; 	
		this.sorumluRef = null; 	
		this.aciklama = null; 
		this.sehirRef=null;
		this.ilceRef=null;
	} 
	 
	@Override 
	public List<ArrayList<QueryObject>> createQueryCriterias(){ 
		ArrayList<ArrayList<QueryObject>> criteriaList = new ArrayList<ArrayList<QueryObject>>(); 
		ArrayList<QueryObject> kriterler = new ArrayList<QueryObject>();	 
		ArrayList<QueryObject> opsiyonelKriterler = new ArrayList<QueryObject>(); 
		try { 	 
			if(validateQueryObject(getAd(), null, false)){ 
				kriterler.add(new QueryObject("ad", ApplicationConstant._beginsWith, getAd().trim(),false)); 
			} 
			if(validateQueryObject(getVergidairesi(), null, false)){ 
				kriterler.add(new QueryObject("vergidairesi", ApplicationConstant._beginsWith, getVergidairesi().trim(),false)); 
			} 
			if(validateQueryObject(getVergino(), null, false)){ 
				kriterler.add(new QueryObject("vergino", ApplicationConstant._beginsWith, getVergino())); 
			} 
			if(validateQueryObject(getSorumluRef(), null, false)){ 
				kriterler.add(new QueryObject("sorumluRef", ApplicationConstant._equals, getSorumluRef())); 
			} 
			if(validateQueryObject(getAciklama(), null, false)){ 
				kriterler.add(new QueryObject("aciklama", ApplicationConstant._beginsWith, getAciklama())); 
			} 
			if(validateQueryObject(getKurumTuruRef(), null, false)){ 
				kriterler.add(new QueryObject("kurumTuruRef", ApplicationConstant._equals, getKurumTuruRef())); 
			}
			if(validateQueryObject(getSehirRef(), null, false)){ 
				kriterler.add(new QueryObject("sehirRef", ApplicationConstant._equals, getSehirRef())); 
			} 
			if(validateQueryObject(getIlceRef(), null, false)){ 
				kriterler.add(new QueryObject("ilceRef", ApplicationConstant._equals, getIlceRef())); 
			} 
		} catch(Exception e){ 
			System.out.println("Error @createQueryCriterias method, Class : KurumFilter :" + e.getMessage()); 
		}  
		criteriaList.add(kriterler); 
		criteriaList.add(opsiyonelKriterler); 
		return criteriaList; 
	} 
	 
	public String getAd() { 
		return this.ad; 
	} 
	 
	public void setAd(String ad) { 
		this.ad = ad; 
	} 
  
	public String getVergidairesi() { 
		return this.vergidairesi; 
	} 
	 
	public void setVergidairesi(String vergidairesi) { 
		this.vergidairesi = vergidairesi; 
	} 
  
	public String getVergino() { 
		return this.vergino; 
	} 
	 
	public void setVergino(String vergino) { 
		this.vergino = vergino; 
	} 
  
	public Kisi getSorumluRef() { 
		return this.sorumluRef; 
	} 
	 
	public void setSorumluRef(Kisi sorumluRef) { 
		this.sorumluRef = sorumluRef; 
	} 
	
	public KurumTuru getKurumTuruRef() {
		return kurumTuruRef;
	}

	public void setKurumTuruRef(KurumTuru kurumTuruRef) {
		this.kurumTuruRef = kurumTuruRef;
	}

	public String getAciklama() { 
		return this.aciklama; 
	} 
	 
	public void setAciklama(String aciklama) { 
		this.aciklama = aciklama; 
	}

	public Sehir getSehirRef() {
		return sehirRef;
	}

	public void setSehirRef(Sehir sehirRef) {
		this.sehirRef = sehirRef;
	}

	public Ilce getIlceRef() {
		return ilceRef;
	}

	public void setIlceRef(Ilce ilceRef) {
		this.ilceRef = ilceRef;
	} 
	
  
	
} 
