package tr.com.arf.toys.db.filter.system; 
 
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.framework.db.model.query.QueryObject;
import tr.com.arf.toys.db.enumerated.system.DemirbasDurum;
import tr.com.arf.toys.db.enumerated.system.DemirbasTuru;
import tr.com.arf.toys.db.model.system.Birim;
import tr.com.arf.toys.utility.application.ApplicationConstant;
 
 
public class DemirbasFilter extends BaseFilter { 
	
private static final long serialVersionUID = 1L; 
	
	private String cinsi; 
	private String kararno; 
	private Birim birimRef;
	private java.util.Date baslangictarih; 
	private java.util.Date baslangictarihMax; 
	private Integer adedi;
	private BigDecimal fiyati;
	private String aciklama;
	private String kullanildigiyer;
	private DemirbasDurum demirbasdurum;
	private DemirbasTuru demirbasturu;
	
	@Override 
	public void init() { 
		this.adedi=null;
		this.birimRef=null;
		this.cinsi=null;
		this.kararno=null;
		this.baslangictarih=null;
		this.baslangictarihMax=null;
		this.fiyati=null;
		this.aciklama=null;
		this.kullanildigiyer=null;
		this.demirbasdurum=DemirbasDurum._NULL;
		this.demirbasturu=DemirbasTuru._NULL;
	} 
	 
	@Override 
	public List<ArrayList<QueryObject>> createQueryCriterias(){ 
		ArrayList<ArrayList<QueryObject>> criteriaList = new ArrayList<ArrayList<QueryObject>>(); 
		ArrayList<QueryObject> kriterler = new ArrayList<QueryObject>();	 
		ArrayList<QueryObject> opsiyonelKriterler = new ArrayList<QueryObject>(); 
		try { 	 
			if(validateQueryObject(getCinsi(), null, false)){ 
				kriterler.add(new QueryObject("cinsi", ApplicationConstant._beginsWith, getCinsi())); 
			}
			if(validateQueryObject(getKararno(), null, false)){ 
				kriterler.add(new QueryObject("kararno", ApplicationConstant._beginsWith, getKararno())); 
			}
			if(validateQueryObject(getBaslangictarih(), null, false)  && validateQueryObject(getBaslangictarihMax(), null, false)){
				kriterler.add(new QueryObject("baslangictarih", ApplicationConstant._equalsAndLargerThan, getBaslangictarih())); 
				kriterler.add(new QueryObject("baslangictarih", ApplicationConstant._equalsAndSmallerThan, getBaslangictarihMax())); 
			} else if(validateQueryObject(getBaslangictarih(), null, false)){ 
				kriterler.add(new QueryObject("baslangictarih", ApplicationConstant._equalsAndLargerThan, getBaslangictarih()));
			}
			if(validateQueryObject(getAdedi(), null, false)){ 
				kriterler.add(new QueryObject("adedi", ApplicationConstant._beginsWith, getAdedi())); 
			} 
			if(validateQueryObject(getFiyati(), 0, false)){
				kriterler.add(new QueryObject("fiyati", ApplicationConstant._equals, getFiyati())); 	
			}
			if(validateQueryObject(getBirimRef(), null, false)){ 
				kriterler.add(new QueryObject("birimRef", ApplicationConstant._equals, getBirimRef())); 
			}
			if(validateQueryObject(getDemirbasturu(), null, true)){ 
				kriterler.add(new QueryObject("demirbasturu", ApplicationConstant._equals, getDemirbasturu())); 
			} 
			if(validateQueryObject(getDemirbasdurum(), null, true)){ 
				kriterler.add(new QueryObject("demirbasdurum", ApplicationConstant._equals, getDemirbasdurum())); 
			} 
			if(validateQueryObject(getAciklama(), null, false)){ 
				kriterler.add(new QueryObject("aciklama", ApplicationConstant._beginsWith, getAciklama())); 
			}
			if(validateQueryObject(getKullanildigiyer(), null, false)){ 
				kriterler.add(new QueryObject("kullanildigiyer", ApplicationConstant._beginsWith, getKullanildigiyer())); 
			}
			
		} catch(Exception e){ 
			System.out.println("Error @createQueryCriterias method, Class : DemirbasFilter :" + e.getMessage()); 
		}  
		criteriaList.add(kriterler); 
		criteriaList.add(opsiyonelKriterler); 
		return criteriaList; 
	}

	public String getCinsi() {
		return cinsi;
	}

	public void setCinsi(String cinsi) {
		this.cinsi = cinsi;
	}

	public String getKararno() {
		return kararno;
	}

	public void setKararno(String kararno) {
		this.kararno = kararno;
	}

	public Birim getBirimRef() {
		return birimRef;
	}

	public void setBirimRef(Birim birimRef) {
		this.birimRef = birimRef;
	}

	public java.util.Date getBaslangictarih() {
		return baslangictarih;
	}

	public void setBaslangictarih(java.util.Date baslangictarih) {
		this.baslangictarih = baslangictarih;
	}

	public java.util.Date getBaslangictarihMax() {
		return baslangictarihMax;
	}

	public void setBaslangictarihMax(java.util.Date baslangictarihMax) {
		this.baslangictarihMax = baslangictarihMax;
	}

	public Integer getAdedi() {
		return adedi;
	}

	public void setAdedi(Integer adedi) {
		this.adedi = adedi;
	}

	public BigDecimal getFiyati() {
		return fiyati;
	}

	public void setFiyati(BigDecimal fiyati) {
		this.fiyati = fiyati;
	}

	public String getAciklama() {
		return aciklama;
	}

	public void setAciklama(String aciklama) {
		this.aciklama = aciklama;
	}

	public String getKullanildigiyer() {
		return kullanildigiyer;
	}

	public void setKullanildigiyer(String kullanildigiyer) {
		this.kullanildigiyer = kullanildigiyer;
	}

	public DemirbasDurum getDemirbasdurum() {
		return demirbasdurum;
	}

	public void setDemirbasdurum(DemirbasDurum demirbasdurum) {
		this.demirbasdurum = demirbasdurum;
	}

	public DemirbasTuru getDemirbasturu() {
		return demirbasturu;
	}

	public void setDemirbasturu(DemirbasTuru demirbasturu) {
		this.demirbasturu = demirbasturu;
	}
} 
