package tr.com.arf.toys.db.filter.uye; 
 
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.jsoup.helper.StringUtil;

import tr.com.arf.framework.db.enumerated._common.EvetHayir;
import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.framework.db.model.query.QueryObject;
import tr.com.arf.toys.db.enumerated.system.BirimYetkiTuru;
import tr.com.arf.toys.db.enumerated.uye.UyeDurum;
import tr.com.arf.toys.db.enumerated.uye.UyeKayitDurum;
import tr.com.arf.toys.db.enumerated.uye.UyeTip;
import tr.com.arf.toys.db.model.kisi.Kisi;
import tr.com.arf.toys.db.model.system.Birim;
import tr.com.arf.toys.db.model.system.Personel;
import tr.com.arf.toys.service.application.ManagedBeanLocator;
import tr.com.arf.toys.utility.application.ApplicationConstant;
import tr.com.arf.toys.view.controller._common.SessionUser;
 
 
public class UyeFilter extends BaseFilter { 
	
private static final long serialVersionUID = 1L; 
	
	private Long rID; 
	private Kisi kisiRef; 
	private String sicilno; 
	private UyeTip uyetip; 
	private Birim birimRef; 
	private java.util.Date uyeliktarih; 
	private java.util.Date uyeliktarihMax; 
	private java.util.Date ayrilmatarih; 
	private java.util.Date ayrilmatarihMax; 
	private UyeDurum uyedurum; 
	private UyeKayitDurum kayitdurum; 
	private Personel onaylayanRef; 
	private java.util.Date onaytarih; 
	private java.util.Date onaytarihMax; 
	private String sicilnoEquals;
	private String kimlikno;	
	
	private String ad;
	private String soyad;
	
	private boolean sadeceUyeler;
	
	@Override 
	public void init() { 
		this.rID = null; 	
		this.kisiRef = null; 	
		this.sicilno = null; 	
		this.uyetip = UyeTip._NULL;	
		this.birimRef = null; 	
		this.uyeliktarih = null; 	
		this.uyeliktarihMax = null; 	
		this.ayrilmatarih = null; 	
		this.ayrilmatarihMax = null; 	
		this.uyedurum = UyeDurum._NULL;	
		this.kayitdurum = UyeKayitDurum._NULL;	
		this.onaylayanRef = null; 	
		this.onaytarih = null; 	
		this.onaytarihMax = null; 	
		this.sicilnoEquals = null;
		this.ad = null;
		this.soyad = null;
		this.kimlikno = null;
		this.sadeceUyeler = true;
	} 
	 
	@Override 
	public List<ArrayList<QueryObject>> createQueryCriterias(){ 
		ArrayList<ArrayList<QueryObject>> criteriaList = new ArrayList<ArrayList<QueryObject>>(); 
		ArrayList<QueryObject> kriterler = new ArrayList<QueryObject>();	 
		ArrayList<QueryObject> opsiyonelKriterler = new ArrayList<QueryObject>(); 
		try { 
			if(validateQueryObject(getRID(), null, false)){ 
				kriterler.add(new QueryObject("rID", ApplicationConstant._equals, getRID())); 
			} 
			if(validateQueryObject(getKisiRef(), null, false)){ 
				kriterler.add(new QueryObject("kisiRef", ApplicationConstant._equals, getKisiRef())); 
			}
			if(validateQueryObject(getAd(), null, false)){ 
				kriterler.add(new QueryObject("kisiRef.ad", ApplicationConstant._beginsWith, getAd())); 
			}
			if(validateQueryObject(getSoyad(), null, false)){ 
				kriterler.add(new QueryObject("kisiRef.soyad", ApplicationConstant._beginsWith, getSoyad())); 
			}			
			if(!isEmpty(getKimlikno()) && StringUtil.isNumeric(getKimlikno())){  
				kriterler.add(new QueryObject("kisiRef.kimlikno", ApplicationConstant._equals, Long.parseLong(getKimlikno()))); 
			}
			if(validateQueryObject(getSicilnoEquals(), null, false)){   
				kriterler.add(new QueryObject("sicilno", ApplicationConstant._equals, getSicilnoEquals())); 
			} else if(validateQueryObject(getSicilno(), null, false)){ 
				kriterler.add(new QueryObject("sicilno", ApplicationConstant._inside, getSicilno())); 
			} 
			if(validateQueryObject(getUyetip(), null, true)){ 
				kriterler.add(new QueryObject("uyetip", ApplicationConstant._equals, getUyetip())); 
			} 
			if(isSadeceUyeler()){ 
				kriterler.add(new QueryObject(" (o.uyetip =" + UyeTip._CALISAN.getCode() + " OR o.uyetip=" + UyeTip._TURKUYRUKLU.getCode() + ") ")); 
			} 
			if(validateQueryObject(getBirimRef(), null, false)){ 
				kriterler.add(new QueryObject("birimRef", ApplicationConstant._equals, getBirimRef())); 
			} 
			if(validateQueryObject(getUyeliktarih(), null, false)  && validateQueryObject(getUyeliktarihMax(), null, false)){
				kriterler.add(new QueryObject("uyeliktarih", ApplicationConstant._equalsAndLargerThan, getUyeliktarih())); 
				kriterler.add(new QueryObject("uyeliktarih", ApplicationConstant._equalsAndSmallerThan, getUyeliktarihMax())); 
			} else if(validateQueryObject(getUyeliktarih(), null, false)){ 
				kriterler.add(new QueryObject("uyeliktarih", ApplicationConstant._equalsAndLargerThan, getUyeliktarih()));
			}
			if(validateQueryObject(getAyrilmatarih(), null, false)  && validateQueryObject(getAyrilmatarihMax(), null, false)){
				kriterler.add(new QueryObject("ayrilmatarih", ApplicationConstant._equalsAndLargerThan, getAyrilmatarih())); 
				kriterler.add(new QueryObject("ayrilmatarih", ApplicationConstant._equalsAndSmallerThan, getAyrilmatarihMax())); 
			} else if(validateQueryObject(getAyrilmatarih(), null, false)){ 
				kriterler.add(new QueryObject("ayrilmatarih", ApplicationConstant._equalsAndLargerThan, getAyrilmatarih()));
			}
			if(validateQueryObject(getUyedurum(), null, true)){ 
				kriterler.add(new QueryObject("uyedurum", ApplicationConstant._equals, getUyedurum())); 
			} 
			if(validateQueryObject(getKayitdurum(), null, true)){ 
				kriterler.add(new QueryObject("kayitdurum", ApplicationConstant._equals, getKayitdurum())); 
			} 
			if(validateQueryObject(getOnaylayanRef(), null, false)){ 
				kriterler.add(new QueryObject("onaylayanRef", ApplicationConstant._equals, getOnaylayanRef())); 
			} 
			if(validateQueryObject(getOnaytarih(), null, false)  && validateQueryObject(getOnaytarihMax(), null, false)){
				kriterler.add(new QueryObject("onaytarih", ApplicationConstant._equalsAndLargerThan, getOnaytarih())); 
				kriterler.add(new QueryObject("onaytarih", ApplicationConstant._equalsAndSmallerThan, getOnaytarihMax())); 
			} else if(validateQueryObject(getOnaytarih(), null, false)){ 
				kriterler.add(new QueryObject("onaytarih", ApplicationConstant._equalsAndLargerThan, getOnaytarih()));
			} 
			// Kullanicinin birimine gore yetkilendirme yapiliyor.
			SessionUser sessionUser = ManagedBeanLocator.locateSessionUser();
			if(sessionUser.getPersonelRef() != null && sessionUser.getPersonelRef().getBirimRef() != null){
				if(sessionUser.getBirimYetkiTuru() != BirimYetkiTuru._TUMBIRIMLER  && sessionUser.getPersonelRef().getTumUyeSorgulama() != EvetHayir._EVET){
					if(sessionUser.getBirimYetkiTuru() == BirimYetkiTuru._KENDIBIRIMIVEALTBIRIMLERI){ 
						kriterler.add(new QueryObject("birimRef.erisimKodu", ApplicationConstant._beginsWith, sessionUser.getPersonelRef().getBirimRef().getErisimKodu())); 
					} else if(sessionUser.getBirimYetkiTuru() == BirimYetkiTuru._SADECEKENDIBIRIMLERI){ 
						kriterler.add(new QueryObject("birimRef", ApplicationConstant._equals, sessionUser.getPersonelRef().getBirimRef())); 
					}
				}
			}
		} catch(Exception e){ 
			System.out.println("Error @createQueryCriterias method, Class : UyeFilter :" + e.getMessage()); 
		}  
		criteriaList.add(kriterler); 
		criteriaList.add(opsiyonelKriterler); 
		return criteriaList; 
	} 
	
	public Long getRID() { 
		return this.rID; 
	} 
	 
	public void setRID(Long rID) { 
		this.rID = rID; 
	} 
	 
	public Kisi getKisiRef() { 
		return this.kisiRef; 
	} 
	 
	public void setKisiRef(Kisi kisiRef) { 
		this.kisiRef = kisiRef; 
	} 
  
	public String getSicilno() { 
		return this.sicilno; 
	} 
	 
	public void setSicilno(String sicilno) { 
		this.sicilno = sicilno; 
	} 
  
	public UyeTip getUyetip() { 
		return this.uyetip; 
	} 
	 
	public void setUyetip(UyeTip uyetip) { 
		this.uyetip = uyetip; 
	} 
  
	public Birim getBirimRef() { 
		return this.birimRef; 
	} 
	 
	public void setBirimRef(Birim birimRef) { 
		this.birimRef = birimRef; 
	} 
  
	public Date getUyeliktarih() { 
		return this.uyeliktarih; 
	} 
	 
	public void setUyeliktarih(Date uyeliktarih) { 
		this.uyeliktarih = uyeliktarih; 
	} 
  
	public Date getUyeliktarihMax() { 
		return this.uyeliktarihMax; 
	} 
	 
	public void setUyeliktarihMax(Date uyeliktarihMax) { 
		this.uyeliktarihMax = uyeliktarihMax; 
	} 
  
	public Date getAyrilmatarih() { 
		return this.ayrilmatarih; 
	} 
	 
	public void setAyrilmatarih(Date ayrilmatarih) { 
		this.ayrilmatarih = ayrilmatarih; 
	} 
  
	public Date getAyrilmatarihMax() { 
		return this.ayrilmatarihMax; 
	} 
	 
	public void setAyrilmatarihMax(Date ayrilmatarihMax) { 
		this.ayrilmatarihMax = ayrilmatarihMax; 
	} 
  
	public UyeDurum getUyedurum() { 
		return this.uyedurum; 
	} 
	 
	public void setUyedurum(UyeDurum uyedurum) { 
		this.uyedurum = uyedurum; 
	} 
  
	public UyeKayitDurum getKayitdurum() { 
		return this.kayitdurum; 
	} 
	 
	public void setKayitdurum(UyeKayitDurum kayitdurum) { 
		this.kayitdurum = kayitdurum; 
	} 
  
	public Personel getOnaylayanRef() { 
		return this.onaylayanRef; 
	} 
	 
	public void setOnaylayanRef(Personel onaylayanRef) { 
		this.onaylayanRef = onaylayanRef; 
	} 
  
	public Date getOnaytarih() { 
		return this.onaytarih; 
	} 
	 
	public void setOnaytarih(Date onaytarih) { 
		this.onaytarih = onaytarih; 
	} 
  
	public Date getOnaytarihMax() { 
		return this.onaytarihMax; 
	} 
	 
	public void setOnaytarihMax(Date onaytarihMax) { 
		this.onaytarihMax = onaytarihMax; 
	}

	public String getSicilnoEquals() {
		return sicilnoEquals;
	}

	public void setSicilnoEquals(String sicilnoEquals) {
		this.sicilnoEquals = sicilnoEquals;
	}

	public String getKimlikno() {
		return kimlikno;
	}

	public void setKimlikno(String kimlikno) {
		this.kimlikno = kimlikno;
	}

	public String getAd() {
		return ad;
	}

	public void setAd(String ad) {
		this.ad = ad;
	}

	public String getSoyad() {
		return soyad;
	}

	public void setSoyad(String soyad) {
		this.soyad = soyad;
	}

	public boolean isSadeceUyeler() {
		return sadeceUyeler;
	}

	public void setSadeceUyeler(boolean sadeceUyeler) {
		this.sadeceUyeler = sadeceUyeler;
	} 
	
} 
