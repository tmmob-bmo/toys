package tr.com.arf.toys.db.filter.system; 
 
import java.util.ArrayList;
import java.util.List;

import tr.com.arf.framework.db.enumerated._common.EvetHayir;
import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.framework.db.model.query.QueryObject;
import tr.com.arf.toys.db.enumerated.system.BirimYetkiTuru;
import tr.com.arf.toys.db.model.system.Birim;
import tr.com.arf.toys.service.application.ManagedBeanLocator;
import tr.com.arf.toys.utility.application.ApplicationConstant;
import tr.com.arf.toys.view.controller._common.SessionUser;
 
 
public class BirimHesapFilter extends BaseFilter { 
	
private static final long serialVersionUID = 1L; 
	
	private String bankaadi;
	private Long hesapno;
	private Long subekodu;
	private Long ibannumarasi;
	private Birim birimRef;
	private EvetHayir gecerli; 
	
	@Override 
	public void init() { 
		this.bankaadi=null;
		this.hesapno=null;
		this.ibannumarasi=null;
		this.subekodu=null;
		this.birimRef=null;
		this.gecerli = EvetHayir._NULL;	
		
	} 
	 
	@Override 
	public List<ArrayList<QueryObject>> createQueryCriterias(){ 
		ArrayList<ArrayList<QueryObject>> criteriaList = new ArrayList<ArrayList<QueryObject>>(); 
		ArrayList<QueryObject> kriterler = new ArrayList<QueryObject>();	 
		ArrayList<QueryObject> opsiyonelKriterler = new ArrayList<QueryObject>(); 
		try { 	 
			if(validateQueryObject(getBankaadi(), null, false)){ 
				kriterler.add(new QueryObject("bankaadi", ApplicationConstant._beginsWith, getBankaadi())); 
			} 
			if(validateQueryObject(getHesapno(), null, false)){ 
				kriterler.add(new QueryObject("hesapno", ApplicationConstant._beginsWith, getHesapno())); 
			} 
			if(validateQueryObject(getSubekodu(), null, false)){ 
				kriterler.add(new QueryObject("subekodu", ApplicationConstant._beginsWith, getSubekodu())); 
			} 
			if(validateQueryObject(getIbannumarasi(), null, false)){ 
				kriterler.add(new QueryObject("ibannumarasi", ApplicationConstant._beginsWith, getIbannumarasi())); 
			} 
			if(validateQueryObject(getBirimRef(), null, false)){ 
				kriterler.add(new QueryObject("birimRef", ApplicationConstant._equals, getBirimRef())); 
			}
			if(validateQueryObject(getGecerli(), null, true)){ 
				kriterler.add(new QueryObject("gecerli", ApplicationConstant._equals, getGecerli())); 
			} 
			// Kullanicinin birimine gore yetkilendirme yapiliyor.
			SessionUser sessionUser = ManagedBeanLocator.locateSessionUser();
			if(sessionUser.getPersonelRef() != null && sessionUser.getPersonelRef().getBirimRef() != null){
				if(sessionUser.getBirimYetkiTuru() != BirimYetkiTuru._TUMBIRIMLER){
					if(sessionUser.getBirimYetkiTuru() == BirimYetkiTuru._KENDIBIRIMIVEALTBIRIMLERI){ 
						kriterler.add(new QueryObject("birimRef.erisimKodu", ApplicationConstant._beginsWith, sessionUser.getPersonelRef().getBirimRef().getErisimKodu())); 
					} else if(sessionUser.getBirimYetkiTuru() == BirimYetkiTuru._SADECEKENDIBIRIMLERI){ 
						kriterler.add(new QueryObject("birimRef", ApplicationConstant._equals, sessionUser.getPersonelRef().getBirimRef())); 
					}
				}
			}
			
		} catch(Exception e){ 
			System.out.println("Error @createQueryCriterias method, Class : BirimHesapFilter :" + e.getMessage()); 
		}  
		criteriaList.add(kriterler); 
		criteriaList.add(opsiyonelKriterler); 
		return criteriaList; 
	}
	
	public String getBankaadi() {
		return bankaadi;
	}
	
	public void setBankaadi(String bankaadi) {
		this.bankaadi = bankaadi;
	}
	
	public Long getHesapno() {
		return hesapno;
	}
	
	public void setHesapno(Long hesapno) {
		this.hesapno = hesapno;
	}
	
	public Long getSubekodu() {
		return subekodu;
	}
	
	public void setSubekodu(Long subekodu) {
		this.subekodu = subekodu;
	}
	
	public Long getIbannumarasi() {
		return ibannumarasi;
	}
	
	public void setIbannumarasi(Long ibannumarasi) {
		this.ibannumarasi = ibannumarasi;
	}
	
	public Birim getBirimRef() {
		return birimRef;
	}
	
	public void setBirimRef(Birim birimRef) {
		this.birimRef = birimRef;
	}
	
	public EvetHayir getGecerli() {
		return gecerli;
	}
	
	public void setGecerli(EvetHayir gecerli) {
		this.gecerli = gecerli;
	}

}
