package tr.com.arf.toys.db.filter.system;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.framework.db.model.query.QueryObject;
import tr.com.arf.toys.db.enumerated.system.YpersonelProjeDurum;
import tr.com.arf.toys.db.model.system.Ybirim;
import tr.com.arf.toys.db.model.system.Ypersonel;
import tr.com.arf.toys.db.model.system.Yproje;
import tr.com.arf.toys.utility.application.ApplicationConstant;



public class YpersonelbirimprojeFilter extends BaseFilter{

	 
	/**
	 * 
	 */
	private static final long serialVersionUID = -5492049099297798626L;
	
	private Ypersonel personelRef; 
	private Yproje projeRef; 
	private Ybirim birimRef; 
	private java.util.Date bitistarih; 
	private String aciklama; 
	private YpersonelProjeDurum personelProjeDurum; 
	private java.util.Date baslamatarihMax; 
	private java.util.Date bitistarihMax;
	private Date baslangictarih; 
 
	@Override
	public List<ArrayList<QueryObject>> createQueryCriterias() {
		ArrayList<ArrayList<QueryObject>> criteriaList = new ArrayList<ArrayList<QueryObject>>();
		ArrayList<QueryObject> kriterler = new ArrayList<QueryObject>();
		ArrayList<QueryObject> opsiyonelKriterler=  new ArrayList<QueryObject>();                      
				try { 	 		
					if(validateQueryObject(getBaslangictarih(), null, false)  && validateQueryObject(getBaslamatarihMax(), null, false)){
						kriterler.add(new QueryObject("baslamatarih", ApplicationConstant._equalsAndLargerThan, getBaslangictarih())); 
						kriterler.add(new QueryObject("baslamatarih", ApplicationConstant._equalsAndSmallerThan, getBaslamatarihMax())); 
					} else if(validateQueryObject(getBaslangictarih(), null, false)){ 
						kriterler.add(new QueryObject("baslamatarih", ApplicationConstant._equalsAndLargerThan, getBaslangictarih()));
					}
					if(validateQueryObject(getBitistarih(), null, false)  && validateQueryObject(getBitistarihMax(), null, false)){
						kriterler.add(new QueryObject("bitistarih", ApplicationConstant._equalsAndLargerThan, getBitistarih())); 
						kriterler.add(new QueryObject("bitistarih", ApplicationConstant._equalsAndSmallerThan, getBitistarihMax())); 
					} else if(validateQueryObject(getBitistarih(), null, false)){ 
						kriterler.add(new QueryObject("bitistarih", ApplicationConstant._equalsAndLargerThan, getBitistarih()));
					}
					if(validateQueryObject(getBirimRef(), null, false)){ 
						kriterler.add(new QueryObject("birimRef", ApplicationConstant._equals, getBirimRef())); }
					if(validateQueryObject(getAciklama(), null, false)){ 
						kriterler.add(new QueryObject("aciklama", ApplicationConstant._equals, getAciklama())); }
					if(validateQueryObject(getpersonelProjeDurum(), null, false)){ 
						kriterler.add(new QueryObject("personelProjeDurum", ApplicationConstant._equals, getpersonelProjeDurum())); }
					if(validateQueryObject(getprojeRef(), null, false)){ 
						kriterler.add(new QueryObject("projeRef", ApplicationConstant._equals, getprojeRef()));}
					
				}
		 catch(Exception e){ 
			System.out.println("Error @createQueryCriterias method, Class : PersonelFilter :" + e.getMessage()); 
		}  
		criteriaList.add(kriterler); 
		criteriaList.add(opsiyonelKriterler); 
		return criteriaList; 
	}

	
	
	public Ypersonel getPersonelRef() { 
		return personelRef; 
	} 
	 
	public void setPersonelRef(Ypersonel personelRef) { 
		this.personelRef = personelRef; 
	} 
 
	public Yproje getprojeRef() { 
		return projeRef; 
	} 
	 
	public void setprojeRef(Yproje projeRef) { 
		this.projeRef = projeRef; 
	} 
 
	public Ybirim getBirimRef() { 
		return birimRef; 
	} 
	 
	public void setBirimRef(Ybirim birimRef) { 
		this.birimRef = birimRef; 
	} 
 
	public java.util.Date getBaslangictarih() { 
		return baslangictarih; 
	} 
	 
	public void setBaslangictarih(java.util.Date baslangictarih) { 
		this.baslangictarih = baslangictarih; 
	} 
 
	public java.util.Date getBitistarih() { 
		return bitistarih; 
	} 
	 
	public void setBitistarih(java.util.Date bitistarih) { 
		this.bitistarih = bitistarih; 
	} 
 
	public String getAciklama() { 
		return this.aciklama; 
	} 
	 
	public void setAciklama(String aciklama) { 
		this.aciklama = aciklama; 
	} 
 
	public YpersonelProjeDurum getpersonelProjeDurum() { 
		return this.personelProjeDurum;  
	}  
  
	public void setDurum(YpersonelProjeDurum personelProjeDurum) {  
		this.personelProjeDurum = personelProjeDurum;  
	}   
 
	public Date getBitistarihMax() { 
		return this.bitistarihMax; 
	} 
	 
	public void setBitistarihMax(Date bitistarihMax) { 
		this.bitistarihMax = bitistarihMax; 
	} 
	
	public Date getBaslamatarihMax() { 
		return this.baslamatarihMax; 
	} 
	 
	public void setBaslamatarihMax(Date baslamatarihMax) { 
		this.baslamatarihMax = baslamatarihMax; 
	} 
	
	@Override
	public void init() { 
		this.personelRef = null; 
		this.projeRef = null; 
		this.birimRef = null; 
		this.baslangictarih = null; 
		this.bitistarih = null; 
		this.aciklama = ""; 
		this.personelProjeDurum = YpersonelProjeDurum._NULL; 
		
	} 
	 
	
}
