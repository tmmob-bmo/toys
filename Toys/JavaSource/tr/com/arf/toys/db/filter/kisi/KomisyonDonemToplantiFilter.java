package tr.com.arf.toys.db.filter.kisi; 
 
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.framework.db.model.query.QueryObject;
import tr.com.arf.toys.db.enumerated.kisi.ToplantiTuru;
import tr.com.arf.toys.db.model.kisi.KomisyonDonem;
import tr.com.arf.toys.utility.application.ApplicationConstant;
 
 
public class KomisyonDonemToplantiFilter extends BaseFilter { 
	
private static final long serialVersionUID = 1L; 
	
	private KomisyonDonem komisyonDonemRef; 
	
	private Long rID; 
	private java.util.Date tarih; 
	private java.util.Date tarihMax; 
	private ToplantiTuru toplantituru; 
	private String toplantiYeri;
	
	@Override 
	public void init() { 
		this.rID = null; 	
		this.komisyonDonemRef = null; 	
		this.tarih = null; 	
		this.tarihMax = null; 	
		this.toplantituru = ToplantiTuru._NULL;	
		this.toplantiYeri=null;
	} 
	 
	@Override 
	public List<ArrayList<QueryObject>> createQueryCriterias(){ 
		ArrayList<ArrayList<QueryObject>> criteriaList = new ArrayList<ArrayList<QueryObject>>(); 
		ArrayList<QueryObject> kriterler = new ArrayList<QueryObject>();	 
		ArrayList<QueryObject> opsiyonelKriterler = new ArrayList<QueryObject>(); 
		try { 	 
			if(validateQueryObject(getRID(), null, false)){ 
				kriterler.add(new QueryObject("rID", ApplicationConstant._equals, getRID())); 
			} 
			if(validateQueryObject(getKomisyonDonemRef(), null, false)){ 
				kriterler.add(new QueryObject("komisyonDonemRef", ApplicationConstant._equals, getKomisyonDonemRef())); 
			} 
			if(validateQueryObject(getTarih(), null, false)  && validateQueryObject(getTarihMax(), null, false)){
				kriterler.add(new QueryObject("tarih", ApplicationConstant._equalsAndLargerThan, getTarih())); 
				kriterler.add(new QueryObject("tarih", ApplicationConstant._equalsAndSmallerThan, getTarihMax())); 
			} else if(validateQueryObject(getTarih(), null, false)){ 
				kriterler.add(new QueryObject("tarih", ApplicationConstant._equalsAndLargerThan, getTarih()));
			}
			if(validateQueryObject(getToplantituru(), null, true)){ 
				kriterler.add(new QueryObject("toplantituru", ApplicationConstant._equals, getToplantituru())); 
			} 
			if(validateQueryObject(getToplantiYeri(), null, true)){ 
				kriterler.add(new QueryObject("toplantiYeri", ApplicationConstant._equals, getToplantiYeri())); 
			}
		} catch(Exception e){ 
			System.out.println("Error @createQueryCriterias method, Class : KomisyonDonemToplantiFilter :" + e.getMessage()); 
		}  
		criteriaList.add(kriterler); 
		criteriaList.add(opsiyonelKriterler); 
		return criteriaList; 
	} 
	 
	public Long getRID() { 
		return this.rID; 
	} 
	 
	public void setRID(Long rID) { 
		this.rID = rID; 
	} 
  
	public KomisyonDonem getKomisyonDonemRef() { 
		return this.komisyonDonemRef; 
	} 
	 
	public void setKomisyonDonemRef(KomisyonDonem komisyonDonemRef) { 
		this.komisyonDonemRef = komisyonDonemRef; 
	} 
  
	public Date getTarih() { 
		return this.tarih; 
	} 
	 
	public void setTarih(Date tarih) { 
		this.tarih = tarih; 
	} 
  
	public Date getTarihMax() { 
		return this.tarihMax; 
	} 
	 
	public void setTarihMax(Date tarihMax) { 
		this.tarihMax = tarihMax; 
	} 
  
	public ToplantiTuru getToplantituru() { 
		return this.toplantituru; 
	} 
	 
	public void setToplantituru(ToplantiTuru toplantituru) { 
		this.toplantituru = toplantituru; 
	}

	public String getToplantiYeri() {
		return toplantiYeri;
	}

	public void setToplantiYeri(String toplantiYeri) {
		this.toplantiYeri = toplantiYeri;
	} 
	
  
	
} 
