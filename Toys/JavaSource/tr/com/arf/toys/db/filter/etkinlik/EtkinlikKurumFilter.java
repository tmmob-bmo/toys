package tr.com.arf.toys.db.filter.etkinlik; 
 
import java.util.ArrayList;
import java.util.List;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.framework.db.model.query.QueryObject;
import tr.com.arf.toys.db.enumerated.etkinlik.EtkinlikKurumTuru;
import tr.com.arf.toys.db.model.etkinlik.Etkinlik;
import tr.com.arf.toys.db.model.kisi.Kurum;
import tr.com.arf.toys.utility.application.ApplicationConstant;
 
 
public class EtkinlikKurumFilter extends BaseFilter { 
	
private static final long serialVersionUID = 1L; 
	
	private Etkinlik etkinlikRef; 
	
	private Long rID; 
	private Kurum kurumRef; 
	private EtkinlikKurumTuru turu; 
	
	@Override 
	public void init() { 
		this.rID = null; 	
		this.etkinlikRef = null; 	
		this.kurumRef = null; 	
		this.turu = EtkinlikKurumTuru._NULL;	
	} 
	 
	@Override 
	public List<ArrayList<QueryObject>> createQueryCriterias(){ 
		ArrayList<ArrayList<QueryObject>> criteriaList = new ArrayList<ArrayList<QueryObject>>(); 
		ArrayList<QueryObject> kriterler = new ArrayList<QueryObject>();	 
		ArrayList<QueryObject> opsiyonelKriterler = new ArrayList<QueryObject>(); 
		try { 	 
			if(validateQueryObject(getRID(), null, false)){ 
				kriterler.add(new QueryObject("rID", ApplicationConstant._equals, getRID())); 
			} 
			if(validateQueryObject(getEtkinlikRef(), null, false)){ 
				kriterler.add(new QueryObject("etkinlikRef", ApplicationConstant._equals, getEtkinlikRef())); 
			} 
			if(validateQueryObject(getKurumRef(), null, false)){ 
				kriterler.add(new QueryObject("kurumRef", ApplicationConstant._equals, getKurumRef())); 
			} 
			if(validateQueryObject(getTuru(), null, true)){ 
				kriterler.add(new QueryObject("turu", ApplicationConstant._equals, getTuru())); 
			} 
		} catch(Exception e){ 
			System.out.println("Error @createQueryCriterias method, Class : EtkinlikKurumFilter :" + e.getMessage()); 
		}  
		criteriaList.add(kriterler); 
		criteriaList.add(opsiyonelKriterler); 
		return criteriaList; 
	} 
	 
	public Long getRID() { 
		return this.rID; 
	} 
	 
	public void setRID(Long rID) { 
		this.rID = rID; 
	} 
  
	public Etkinlik getEtkinlikRef() { 
		return this.etkinlikRef; 
	} 
	 
	public void setEtkinlikRef(Etkinlik etkinlikRef) { 
		this.etkinlikRef = etkinlikRef; 
	} 
  
	public Kurum getKurumRef() { 
		return this.kurumRef; 
	} 
	 
	public void setKurumRef(Kurum kurumRef) { 
		this.kurumRef = kurumRef; 
	} 
  
	public EtkinlikKurumTuru getTuru() { 
		return this.turu; 
	} 
	 
	public void setTuru(EtkinlikKurumTuru turu) { 
		this.turu = turu; 
	} 
  
	
} 
