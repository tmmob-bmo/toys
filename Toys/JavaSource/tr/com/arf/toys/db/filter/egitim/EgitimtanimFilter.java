package tr.com.arf.toys.db.filter.egitim; 
 
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.framework.db.model.query.QueryObject;
import tr.com.arf.toys.db.enumerated.egitim.EgitimDurum;
import tr.com.arf.toys.db.enumerated.egitim.Egitimturu;
import tr.com.arf.toys.db.model.egitim.Sinav;
import tr.com.arf.toys.utility.application.ApplicationConstant;
 
 
public class EgitimtanimFilter extends BaseFilter { 
	
private static final long serialVersionUID = 1L; 
	
	private Long rID; 
	private String ad; 
	private Egitimturu egitimturu; 
	private String aciklama; 
	private String egitimtanimkodu; 
	private EgitimDurum durum;
	private BigDecimal sure;
	private Sinav sinavRef;
	
	@Override 
	public void init() { 
		this.rID = null; 	
		this.ad = null; 	
		this.egitimturu = Egitimturu._NULL;	
		this.aciklama = null; 	
		this.egitimtanimkodu=null;
		this.durum=EgitimDurum._NULL;
		this.sure=null;
		this.sinavRef=null;
	} 
	 
	@Override 
	public List<ArrayList<QueryObject>> createQueryCriterias(){ 
		ArrayList<ArrayList<QueryObject>> criteriaList = new ArrayList<ArrayList<QueryObject>>(); 
		ArrayList<QueryObject> kriterler = new ArrayList<QueryObject>();	 
		ArrayList<QueryObject> opsiyonelKriterler = new ArrayList<QueryObject>(); 
		try { 	 
			if(validateQueryObject(getRID(), null, false)){ 
				kriterler.add(new QueryObject("rID", ApplicationConstant._equals, getRID())); 
			} 
			if(validateQueryObject(getAd(), null, false)){ 
				kriterler.add(new QueryObject("ad", ApplicationConstant._beginsWith, getAd(), false)); 
			} 
			if(validateQueryObject(getEgitimturu(), null, true)){ 
				kriterler.add(new QueryObject("egitimturu", ApplicationConstant._equals, getEgitimturu())); 
			} 
			if(validateQueryObject(getAciklama(), null, false)){ 
				kriterler.add(new QueryObject("aciklama", ApplicationConstant._beginsWith, getAciklama(), false)); 
			} 
			if(validateQueryObject(getEgitimtanimkodu(), null, false)){ 
				kriterler.add(new QueryObject("egitimtanimkodu", ApplicationConstant._beginsWith, getEgitimtanimkodu(), false)); 
			} 
			if(validateQueryObject(getEgitimturu(), null, true)){ 
				kriterler.add(new QueryObject("egitimturu", ApplicationConstant._equals, getEgitimturu())); 
			} 
			if(validateQueryObject(getSure(), null, false)){
				kriterler.add(new QueryObject("sure", ApplicationConstant._equals, getSure())); 	
			}
			if(validateQueryObject(getSinavRef(), null, true)){ 
				kriterler.add(new QueryObject("sinavRef", ApplicationConstant._equals, getSinavRef())); 
			}
		} catch(Exception e){ 
			System.out.println("Error @createQueryCriterias method, Class : EgitimtanimFilter :" + e.getMessage()); 
		}  
		criteriaList.add(kriterler); 
		criteriaList.add(opsiyonelKriterler); 
		return criteriaList; 
	} 
	 
	public Long getRID() { 
		return this.rID; 
	} 
	 
	public void setRID(Long rID) { 
		this.rID = rID; 
	} 
  
	public String getAd() { 
		return this.ad; 
	} 
	 
	public void setAd(String ad) { 
		this.ad = ad; 
	} 
  
	public Egitimturu getEgitimturu() { 
		return this.egitimturu; 
	} 
	 
	public void setEgitimturu(Egitimturu egitimturu) { 
		this.egitimturu = egitimturu; 
	} 
  
	public String getAciklama() { 
		return this.aciklama; 
	} 
	 
	public void setAciklama(String aciklama) { 
		this.aciklama = aciklama; 
	}

	public String getEgitimtanimkodu() {
		return egitimtanimkodu;
	}

	public void setEgitimtanimkodu(String egitimtanimkodu) {
		this.egitimtanimkodu = egitimtanimkodu;
	}

	public EgitimDurum getDurum() {
		return durum;
	}

	public void setDurum(EgitimDurum durum) {
		this.durum = durum;
	}

	public BigDecimal getSure() {
		return sure;
	}

	public void setSure(BigDecimal sure) {
		this.sure = sure;
	}

	public Sinav getSinavRef() {
		return sinavRef;
	}

	public void setSinavRef(Sinav sinavRef) {
		this.sinavRef = sinavRef;
	}
	
} 
