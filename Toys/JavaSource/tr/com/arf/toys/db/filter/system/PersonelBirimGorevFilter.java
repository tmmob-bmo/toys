package tr.com.arf.toys.db.filter.system; 
 
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.framework.db.model.query.QueryObject;
import tr.com.arf.toys.db.enumerated.system.BirimYetkiTuru;
import tr.com.arf.toys.db.enumerated.system.PersonelGorevDurum;
import tr.com.arf.toys.db.model.gorev.Gorev;
import tr.com.arf.toys.db.model.system.Birim;
import tr.com.arf.toys.db.model.system.Personel;
import tr.com.arf.toys.service.application.ManagedBeanLocator;
import tr.com.arf.toys.utility.application.ApplicationConstant;
import tr.com.arf.toys.view.controller._common.SessionUser;
 
 
public class PersonelBirimGorevFilter extends BaseFilter { 
	
private static final long serialVersionUID = 1L; 
	
	private Personel personelRef; 
	
	private Gorev gorevRef; 
	private Birim birimRef; 
	private java.util.Date baslangictarih; 
	private java.util.Date baslangictarihMax; 
	private java.util.Date bitistarih; 
	private java.util.Date bitistarihMax; 
	private String aciklama; 
	private PersonelGorevDurum durum; 
	
	@Override 
	public void init() { 
		this.personelRef = null; 	
		this.gorevRef = null; 	
		this.birimRef = null; 	
		this.baslangictarih = null; 	
		this.baslangictarihMax = null; 	
		this.bitistarih = null; 	
		this.bitistarihMax = null; 	
		this.aciklama = null; 	
		this.durum = PersonelGorevDurum._NULL;	
	} 
	 
	@Override 
	public List<ArrayList<QueryObject>> createQueryCriterias(){ 
		ArrayList<ArrayList<QueryObject>> criteriaList = new ArrayList<ArrayList<QueryObject>>(); 
		ArrayList<QueryObject> kriterler = new ArrayList<QueryObject>();	 
		ArrayList<QueryObject> opsiyonelKriterler = new ArrayList<QueryObject>(); 
		try { 	 
			if(validateQueryObject(getPersonelRef(), null, false)){ 
				kriterler.add(new QueryObject("personelRef", ApplicationConstant._equals, getPersonelRef())); 
			} 
			if(validateQueryObject(getGorevRef(), null, false)){ 
				kriterler.add(new QueryObject("gorevRef", ApplicationConstant._equals, getGorevRef())); 
			} 
			if(validateQueryObject(getBirimRef(), null, false)){ 
				kriterler.add(new QueryObject("birimRef", ApplicationConstant._equals, getBirimRef())); 
			} 
			if(validateQueryObject(getBaslangictarih(), null, false)  && validateQueryObject(getBaslangictarihMax(), null, false)){
				kriterler.add(new QueryObject("baslangictarih", ApplicationConstant._equalsAndLargerThan, getBaslangictarih())); 
				kriterler.add(new QueryObject("baslangictarih", ApplicationConstant._equalsAndSmallerThan, getBaslangictarihMax())); 
			} else if(validateQueryObject(getBaslangictarih(), null, false)){ 
				kriterler.add(new QueryObject("baslangictarih", ApplicationConstant._equalsAndLargerThan, getBaslangictarih()));
			}
			if(validateQueryObject(getBitistarih(), null, false)  && validateQueryObject(getBitistarihMax(), null, false)){
				kriterler.add(new QueryObject("bitistarih", ApplicationConstant._equalsAndLargerThan, getBitistarih())); 
				kriterler.add(new QueryObject("bitistarih", ApplicationConstant._equalsAndSmallerThan, getBitistarihMax())); 
			} else if(validateQueryObject(getBitistarih(), null, false)){ 
				kriterler.add(new QueryObject("bitistarih", ApplicationConstant._equalsAndLargerThan, getBitistarih()));
			}
			if(validateQueryObject(getAciklama(), null, false)){ 
				kriterler.add(new QueryObject("aciklama", ApplicationConstant._beginsWith, getAciklama())); 
			} 
			if(validateQueryObject(getDurum(), null, true)){ 
				kriterler.add(new QueryObject("durum", ApplicationConstant._equals, getDurum())); 
			}
			// Kullanicinin birimine gore yetkilendirme yapiliyor.
			SessionUser sessionUser = ManagedBeanLocator.locateSessionUser();
			if(sessionUser.getPersonelRef() != null && sessionUser.getPersonelRef().getBirimRef() != null){
				if(sessionUser.getBirimYetkiTuru() != BirimYetkiTuru._TUMBIRIMLER){
					if(sessionUser.getBirimYetkiTuru() == BirimYetkiTuru._KENDIBIRIMIVEALTBIRIMLERI){ 
						kriterler.add(new QueryObject("birimRef.erisimKodu", ApplicationConstant._beginsWith, sessionUser.getPersonelRef().getBirimRef().getErisimKodu())); 
					} else if(sessionUser.getBirimYetkiTuru() == BirimYetkiTuru._SADECEKENDIBIRIMLERI){ 
						kriterler.add(new QueryObject("birimRef", ApplicationConstant._equals, sessionUser.getPersonelRef().getBirimRef())); 
					}
				}
			}
		} catch(Exception e){ 
			System.out.println("Error @createQueryCriterias method, Class : PersonelBirimGorevFilter :" + e.getMessage()); 
		}  
		criteriaList.add(kriterler); 
		criteriaList.add(opsiyonelKriterler); 
		return criteriaList; 
	} 
	 
	public Personel getPersonelRef() { 
		return this.personelRef; 
	} 
	 
	public void setPersonelRef(Personel personelRef) { 
		this.personelRef = personelRef; 
	} 
  
	public Gorev getGorevRef() { 
		return this.gorevRef; 
	} 
	 
	public void setGorevRef(Gorev gorevRef) { 
		this.gorevRef = gorevRef; 
	} 
  
	public Birim getBirimRef() { 
		return this.birimRef; 
	} 
	 
	public void setBirimRef(Birim birimRef) { 
		this.birimRef = birimRef; 
	} 
  
	public Date getBaslangictarih() { 
		return this.baslangictarih; 
	} 
	 
	public void setBaslangictarih(Date baslangictarih) { 
		this.baslangictarih = baslangictarih; 
	} 
  
	public Date getBaslangictarihMax() { 
		return this.baslangictarihMax; 
	} 
	 
	public void setBaslangictarihMax(Date baslangictarihMax) { 
		this.baslangictarihMax = baslangictarihMax; 
	} 
  
	public Date getBitistarih() { 
		return this.bitistarih; 
	} 
	 
	public void setBitistarih(Date bitistarih) { 
		this.bitistarih = bitistarih; 
	} 
  
	public Date getBitistarihMax() { 
		return this.bitistarihMax; 
	} 
	 
	public void setBitistarihMax(Date bitistarihMax) { 
		this.bitistarihMax = bitistarihMax; 
	} 
  
	public String getAciklama() { 
		return this.aciklama; 
	} 
	 
	public void setAciklama(String aciklama) { 
		this.aciklama = aciklama; 
	} 
  
	public PersonelGorevDurum getDurum() { 
		return this.durum; 
	} 
	 
	public void setDurum(PersonelGorevDurum durum) { 
		this.durum = durum; 
	} 
  
	
} 
