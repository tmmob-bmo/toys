package tr.com.arf.toys.db.filter.iletisim; 
 
import java.util.ArrayList;
import java.util.List;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.framework.db.model.query.QueryObject;
import tr.com.arf.toys.db.enumerated.iletisim.ListeGizlilikTuru;
import tr.com.arf.toys.db.model.iletisim.Iletisimliste;
import tr.com.arf.toys.db.model.system.Birim;
import tr.com.arf.toys.utility.application.ApplicationConstant;
 
 
public class IletisimlisteFilter extends BaseFilter { 
	
private static final long serialVersionUID = 1L; 
	
	private Long rID; 
	private String ad; 
	private Iletisimliste ustRef; 
	private String erisimKodu; 
	private Birim birimRef; 
	private ListeGizlilikTuru listegizlilikturu; 
	private String aciklama; 
	
	@Override 
	public void init() { 
		this.rID = null; 	
		this.ad = null; 	
		this.ustRef = null; 	
		this.erisimKodu = null; 	
		this.birimRef = null; 	
		this.listegizlilikturu = ListeGizlilikTuru._NULL;	
		this.aciklama = null; 	
	} 
	 
	@Override 
	public List<ArrayList<QueryObject>> createQueryCriterias(){ 
		ArrayList<ArrayList<QueryObject>> criteriaList = new ArrayList<ArrayList<QueryObject>>(); 
		ArrayList<QueryObject> kriterler = new ArrayList<QueryObject>();	 
		ArrayList<QueryObject> opsiyonelKriterler = new ArrayList<QueryObject>(); 
		try { 	 
			if(validateQueryObject(getRID(), null, false)){ 
				kriterler.add(new QueryObject("rID", ApplicationConstant._equals, getRID())); 
			} 
			if(validateQueryObject(getAd(), null, false)){ 
				kriterler.add(new QueryObject("ad", ApplicationConstant._beginsWith, getAd(), false)); 
			} 
			if(validateQueryObject(getUstRef(), null, false)){ 
				kriterler.add(new QueryObject("ustRef", ApplicationConstant._equals, getUstRef(), false)); 
			} 
			if(validateQueryObject(getErisimKodu(), null, false)){ 
				kriterler.add(new QueryObject("erisimKodu", ApplicationConstant._beginsWith, getErisimKodu(), false)); 
			} 
			if(validateQueryObject(getBirimRef(), null, false)){ 
				kriterler.add(new QueryObject("birimRef", ApplicationConstant._equals, getBirimRef())); 
			} 
			if(validateQueryObject(getListegizlilikturu(), null, true)){ 
				kriterler.add(new QueryObject("listegizlilikturu", ApplicationConstant._equals, getListegizlilikturu())); 
			} 
			if(validateQueryObject(getAciklama(), null, false)){ 
				kriterler.add(new QueryObject("aciklama", ApplicationConstant._beginsWith, getAciklama(), false)); 
			} 
		} catch(Exception e){ 
			System.out.println("Error @createQueryCriterias method, Class : IletisimlisteFilter :" + e.getMessage()); 
		}  
		criteriaList.add(kriterler); 
		criteriaList.add(opsiyonelKriterler); 
		return criteriaList; 
	} 
	 
	public Long getRID() { 
		return this.rID; 
	} 
	 
	public void setRID(Long rID) { 
		this.rID = rID; 
	} 
  
	public String getAd() { 
		return this.ad; 
	} 
	 
	public void setAd(String ad) { 
		this.ad = ad; 
	} 
  
	public Iletisimliste getUstRef() { 
		return this.ustRef; 
	} 
	 
	public void setUstRef(Iletisimliste ustRef) { 
		this.ustRef = ustRef; 
	} 
  
	public String getErisimKodu() { 
		return this.erisimKodu; 
	} 
	 
	public void setErisimKodu(String erisimKodu) { 
		this.erisimKodu = erisimKodu; 
	} 
  
	public Birim getBirimRef() { 
		return this.birimRef; 
	} 
	 
	public void setBirimRef(Birim birimRef) { 
		this.birimRef = birimRef; 
	} 
  
	public ListeGizlilikTuru getListegizlilikturu() { 
		return this.listegizlilikturu; 
	} 
	 
	public void setListegizlilikturu(ListeGizlilikTuru listegizlilikturu) { 
		this.listegizlilikturu = listegizlilikturu; 
	} 
  
	public String getAciklama() { 
		return this.aciklama; 
	} 
	 
	public void setAciklama(String aciklama) { 
		this.aciklama = aciklama; 
	} 
  
	
} 
