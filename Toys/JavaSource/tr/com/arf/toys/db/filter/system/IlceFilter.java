package tr.com.arf.toys.db.filter.system; 
 
import java.util.ArrayList;
import java.util.List;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.framework.db.model.query.QueryObject;
import tr.com.arf.toys.db.model.system.Sehir;
import tr.com.arf.toys.utility.application.ApplicationConstant;
 
 
public class IlceFilter extends BaseFilter { 
	
private static final long serialVersionUID = 1L; 
	
	private Sehir sehirRef; 
	private String ad; 
	private Long subekodu;
	
	@Override 
	public void init() { 
		this.sehirRef = null; 		
		this.ad = null; 
		this.subekodu=null;
	} 
	 
	@Override 
	public List<ArrayList<QueryObject>> createQueryCriterias(){ 
		ArrayList<ArrayList<QueryObject>> criteriaList = new ArrayList<ArrayList<QueryObject>>(); 
		ArrayList<QueryObject> kriterler = new ArrayList<QueryObject>();	 
		ArrayList<QueryObject> opsiyonelKriterler = new ArrayList<QueryObject>(); 
		try { 	 
			if(validateQueryObject(getSehirRef(), null, false)){ 
				kriterler.add(new QueryObject("sehirRef", ApplicationConstant._equals, getSehirRef())); 
			} 
			if(validateQueryObject(getAd(), null, false)){ 
				kriterler.add(new QueryObject("ad", ApplicationConstant._beginsWith, getAd())); 
			} 
			if(validateQueryObject(getSubekodu(), null, false)){ 
				kriterler.add(new QueryObject("subekodu", ApplicationConstant._beginsWith, getSubekodu())); 
			} 
		} catch(Exception e){ 
			System.out.println("Error @createQueryCriterias method, Class : IlceFilter :" + e.getMessage()); 
		}  
		criteriaList.add(kriterler); 
		criteriaList.add(opsiyonelKriterler); 
		return criteriaList; 
	} 
	 
	public Sehir getSehirRef() { 
		return this.sehirRef; 
	} 
	 
	public void setSehirRef(Sehir sehirRef) { 
		this.sehirRef = sehirRef; 
	} 
  
	public String getAd() { 
		return this.ad; 
	} 
	 
	public void setAd(String ad) { 
		this.ad = ad; 
	}

	public Long getSubekodu() {
		return subekodu;
	}

	public void setSubekodu(Long subekodu) {
		this.subekodu = subekodu;
	} 
	
  
	
} 
