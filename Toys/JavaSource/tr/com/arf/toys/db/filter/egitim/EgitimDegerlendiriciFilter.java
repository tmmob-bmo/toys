package tr.com.arf.toys.db.filter.egitim; 
 
import java.util.ArrayList;
import java.util.List;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.framework.db.model.query.QueryObject;
import tr.com.arf.toys.db.model.egitim.Degerlendirici;
import tr.com.arf.toys.db.model.egitim.Egitimtanim;
import tr.com.arf.toys.utility.application.ApplicationConstant;
 
 
public class EgitimDegerlendiriciFilter extends BaseFilter { 
	
private static final long serialVersionUID = 1L; 
	
	private Long rID; 
	private Egitimtanim egitimtanimRef; 
	private Degerlendirici degerlendiriciRef  ; 
	
	@Override 
	public void init() { 
		this.rID = null; 	
		this.egitimtanimRef=null;
		this.degerlendiriciRef=null;
	} 
	 
	@Override 
	public List<ArrayList<QueryObject>> createQueryCriterias(){ 
		ArrayList<ArrayList<QueryObject>> criteriaList = new ArrayList<ArrayList<QueryObject>>(); 
		ArrayList<QueryObject> kriterler = new ArrayList<QueryObject>();	 
		ArrayList<QueryObject> opsiyonelKriterler = new ArrayList<QueryObject>(); 
		try { 	 
			if(validateQueryObject(getRID(), null, false)){ 
				kriterler.add(new QueryObject("rID", ApplicationConstant._equals, getRID())); 
			} 
			if(validateQueryObject(getEgitimtanimRef(), null, false)){ 
				kriterler.add(new QueryObject("egitimtanimRef", ApplicationConstant._equals, getEgitimtanimRef())); 
			} 
			if(validateQueryObject(getDegerlendiriciRef(), null, false)){ 
				kriterler.add(new QueryObject("degerlendiriciRef", ApplicationConstant._equals, getDegerlendiriciRef())); 
			} 
		} catch(Exception e){ 
			System.out.println("Error @createQueryCriterias method, Class : EgitimDegerlendiriciFilter :" + e.getMessage()); 
		}  
		criteriaList.add(kriterler); 
		criteriaList.add(opsiyonelKriterler); 
		return criteriaList; 
	} 
	 
	public Long getRID() { 
		return this.rID; 
	} 
	 
	public void setRID(Long rID) { 
		this.rID = rID; 
	}

	public Egitimtanim getEgitimtanimRef() {
		return egitimtanimRef;
	}

	public void setEgitimtanimRef(Egitimtanim egitimtanimRef) {
		this.egitimtanimRef = egitimtanimRef;
	}

	public Degerlendirici getDegerlendiriciRef() {
		return degerlendiriciRef;
	}

	public void setDegerlendiriciRef(Degerlendirici degerlendiriciRef) {
		this.degerlendiriciRef = degerlendiriciRef;
	}

} 
