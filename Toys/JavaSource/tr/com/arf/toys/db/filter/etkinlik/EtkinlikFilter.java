package tr.com.arf.toys.db.filter.etkinlik; 
 
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.framework.db.model.query.QueryObject;
import tr.com.arf.toys.db.enumerated.etkinlik.EtkinlikDurum;
import tr.com.arf.toys.db.enumerated.system.BirimYetkiTuru;
import tr.com.arf.toys.db.model.etkinlik.Etkinliktanim;
import tr.com.arf.toys.db.model.kisi.Donem;
import tr.com.arf.toys.db.model.kisi.Kisi;
import tr.com.arf.toys.db.model.system.Birim;
import tr.com.arf.toys.db.model.system.Ilce;
import tr.com.arf.toys.db.model.system.Sehir;
import tr.com.arf.toys.service.application.ManagedBeanLocator;
import tr.com.arf.toys.utility.application.ApplicationConstant;
import tr.com.arf.toys.view.controller._common.SessionUser;
 
 
public class EtkinlikFilter extends BaseFilter { 
	
private static final long serialVersionUID = 1L; 
	
	private Long rID; 
	private Etkinliktanim etkinliktanimRef; 
	private Birim birimRef;
	private String ad; 
	private java.util.Date baslamatarih; 
	private java.util.Date baslamatarihMax; 
	private Sehir sehirRef; 
	private Ilce ilceRef; 
	private String yer; 
	private String program; 
	private String webadresi; 
	private String epostaadresi; 
	private String telefonno; 
	private Kisi sorumlukisiRef; 
	private EtkinlikDurum durum; 
	private Donem donemRef; 
	
	private String sorumluad;
	private String sorumlusoyad;
	
	@Override 
	public void init() { 
		this.rID = null; 	
		this.etkinliktanimRef = null; 	
		this.ad = null; 	
		this.baslamatarih = null; 	
		this.baslamatarihMax = null; 	
		this.sehirRef = null; 	
		this.ilceRef = null; 	
		this.yer = null; 	
		this.webadresi = null; 	
		this.epostaadresi = null; 	
		this.telefonno = null; 	
		this.sorumlukisiRef = null; 	
		this.durum = EtkinlikDurum._NULL;	
		this.birimRef = null;
		this.donemRef = null;
		this.program = null; 
		this.sorumluad = null;
		this.sorumlusoyad = null;
	} 
	 
	@Override 
	public List<ArrayList<QueryObject>> createQueryCriterias(){ 
		ArrayList<ArrayList<QueryObject>> criteriaList = new ArrayList<ArrayList<QueryObject>>(); 
		ArrayList<QueryObject> kriterler = new ArrayList<QueryObject>();	 
		ArrayList<QueryObject> opsiyonelKriterler = new ArrayList<QueryObject>(); 
		try { 	 
			if(validateQueryObject(getRID(), null, false)){ 
				kriterler.add(new QueryObject("rID", ApplicationConstant._equals, getRID())); 
			} 
			if(validateQueryObject(getEtkinliktanimRef(), null, false)){ 
				kriterler.add(new QueryObject("etkinliktanimRef", ApplicationConstant._equals, getEtkinliktanimRef())); 
			} 
			if(validateQueryObject(getAd(), null, false)){ 
				kriterler.add(new QueryObject("ad", ApplicationConstant._beginsWith, getAd(), false)); 
			} 
			if(validateQueryObject(getBaslamatarih(), null, false)  && validateQueryObject(getbaslamatarihMax(), null, false)){
				kriterler.add(new QueryObject("baslamatarih", ApplicationConstant._equalsAndLargerThan, getBaslamatarih())); 
				kriterler.add(new QueryObject("baslamatarih", ApplicationConstant._equalsAndSmallerThan, getbaslamatarihMax())); 
			} else if(validateQueryObject(getBaslamatarih(), null, false)){ 
				kriterler.add(new QueryObject("baslamatarih", ApplicationConstant._equalsAndLargerThan, getBaslamatarih()));
			}
			if(validateQueryObject(getSehirRef(), null, false)){ 
				kriterler.add(new QueryObject("sehirRef", ApplicationConstant._equals, getSehirRef())); 
			} 
			if(validateQueryObject(getBirimRef(), null, false)){ 
				kriterler.add(new QueryObject("birimRef", ApplicationConstant._equals, getBirimRef())); 
			} 
			if(validateQueryObject(getDonemRef(), null, false)){ 
				kriterler.add(new QueryObject("donemRef", ApplicationConstant._equals, getDonemRef())); 
			} 
			if(validateQueryObject(getIlceRef(), null, false)){ 
				kriterler.add(new QueryObject("ilceRef", ApplicationConstant._equals, getIlceRef())); 
			} 
			if(validateQueryObject(getYer(), null, false)){ 
				kriterler.add(new QueryObject("yer", ApplicationConstant._beginsWith, getYer(), false)); 
			} 
			if(validateQueryObject(getProgram(), null, false)){ 
				kriterler.add(new QueryObject("program", ApplicationConstant._beginsWith, getProgram(), false)); 
			} 
			if(validateQueryObject(getWebadresi(), null, false)){ 
				kriterler.add(new QueryObject("webadresi", ApplicationConstant._beginsWith, getWebadresi(), false)); 
			} 
			if(validateQueryObject(getEpostaadresi(), null, false)){ 
				kriterler.add(new QueryObject("epostaadresi", ApplicationConstant._beginsWith, getEpostaadresi(), false)); 
			} 
			if(validateQueryObject(getTelefonno(), null, false)){ 
				kriterler.add(new QueryObject("telefonno", ApplicationConstant._beginsWith, getTelefonno(), false)); 
			} 
			if(validateQueryObject(getSorumlukisiRef(), null, false)){ 
				kriterler.add(new QueryObject("sorumlukisiRef", ApplicationConstant._equals, getSorumlukisiRef())); 
			} 
			if(validateQueryObject(getSorumluad(), null, false)){ 
				kriterler.add(new QueryObject("sorumlukisiRef.ad", ApplicationConstant._beginsWith, getSorumluad())); 
			}
			if(validateQueryObject(getSorumlusoyad(), null, false)){ 
				kriterler.add(new QueryObject("sorumlukisiRef.soyad", ApplicationConstant._beginsWith, getSorumlusoyad())); 
			} 
			if(validateQueryObject(getDurum(), null, true)){ 
				kriterler.add(new QueryObject("durum", ApplicationConstant._equals, getDurum())); 
			} 
			// Kullanicinin birimine gore yetkilendirme yapiliyor.
			SessionUser sessionUser = ManagedBeanLocator.locateSessionUser();
			if(sessionUser.getPersonelRef() != null && sessionUser.getPersonelRef().getBirimRef() != null){
				if(sessionUser.getBirimYetkiTuru() != BirimYetkiTuru._TUMBIRIMLER){
					if(sessionUser.getBirimYetkiTuru() == BirimYetkiTuru._KENDIBIRIMIVEALTBIRIMLERI){ 
						kriterler.add(new QueryObject("birimRef.erisimKodu", ApplicationConstant._beginsWith, sessionUser.getPersonelRef().getBirimRef().getErisimKodu())); 
					} else if(sessionUser.getBirimYetkiTuru() == BirimYetkiTuru._SADECEKENDIBIRIMLERI){ 
						kriterler.add(new QueryObject("birimRef", ApplicationConstant._equals, sessionUser.getPersonelRef().getBirimRef())); 
					}
				}
			}
		} catch(Exception e){ 
			System.out.println("Error @createQueryCriterias method, Class : EtkinlikFilter :" + e.getMessage()); 
		}  
		criteriaList.add(kriterler); 
		criteriaList.add(opsiyonelKriterler); 
		return criteriaList; 
	} 
	 
	public Long getRID() { 
		return this.rID; 
	} 
	 
	public void setRID(Long rID) { 
		this.rID = rID; 
	} 
  
	public Etkinliktanim getEtkinliktanimRef() { 
		return this.etkinliktanimRef; 
	} 
	 
	public void setEtkinliktanimRef(Etkinliktanim etkinliktanimRef) { 
		this.etkinliktanimRef = etkinliktanimRef; 
	} 
  
	public String getAd() { 
		return this.ad; 
	} 
	 
	public void setAd(String ad) { 
		this.ad = ad; 
	} 
  
	public Date getBaslamatarih() { 
		return this.baslamatarih; 
	} 
	 
	public void setBaslamatarih(Date baslamatarih) { 
		this.baslamatarih = baslamatarih; 
	} 
  
	public Date getbaslamatarihMax() { 
		return this.baslamatarihMax; 
	} 
	 
	public void setbaslamatarihMax(Date baslamatarihMax) { 
		this.baslamatarihMax = baslamatarihMax; 
	} 
  
	public Sehir getSehirRef() { 
		return this.sehirRef; 
	} 
	 
	public void setSehirRef(Sehir sehirRef) { 
		this.sehirRef = sehirRef; 
	} 
  
	public Birim getBirimRef() { 
		return this.birimRef; 
	} 
	
	public void setBirimRef(Birim birimRef) { 
		this.birimRef = birimRef; 
	}
	
	public Donem getDonemRef() { 
		return this.donemRef; 
	} 
	
	public void setDonemRef(Donem donemRef) { 
		this.donemRef = donemRef; 
	}
	
	public Ilce getIlceRef() { 
		return this.ilceRef; 
	} 
	 
	public void setIlceRef(Ilce ilceRef) { 
		this.ilceRef = ilceRef; 
	} 
  
	public String getYer() { 
		return this.yer; 
	} 
	 
	public void setYer(String yer) { 
		this.yer = yer; 
	} 
  
	public String getProgram() { 
		return this.program; 
	} 
	
	public void setProgram(String program) { 
		this.program = program; 
	} 

	public String getWebadresi() { 
		return this.webadresi; 
	} 
	 
	public void setWebadresi(String webadresi) { 
		this.webadresi = webadresi; 
	} 
  
	public String getEpostaadresi() { 
		return this.epostaadresi; 
	} 
	 
	public void setEpostaadresi(String epostaadresi) { 
		this.epostaadresi = epostaadresi; 
	} 
  
	public String getTelefonno() { 
		return this.telefonno; 
	} 
	 
	public void setTelefonno(String telefonno) { 
		this.telefonno = telefonno; 
	} 
  
	public Kisi getSorumlukisiRef() { 
		return this.sorumlukisiRef; 
	} 
	 
	public void setSorumlukisiRef(Kisi sorumlukisiRef) { 
		this.sorumlukisiRef = sorumlukisiRef; 
	} 
  
	public EtkinlikDurum getDurum() { 
		return this.durum; 
	} 
	 
	public void setDurum(EtkinlikDurum durum) { 
		this.durum = durum; 
	}

	public String getSorumluad() {
		return sorumluad;
	}

	public void setSorumluad(String sorumluad) {
		this.sorumluad = sorumluad;
	}

	public String getSorumlusoyad() {
		return sorumlusoyad;
	}

	public void setSorumlusoyad(String sorumlusoyad) {
		this.sorumlusoyad = sorumlusoyad;
	} 
  
	 
} 
