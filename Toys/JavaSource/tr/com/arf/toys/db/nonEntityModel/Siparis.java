package tr.com.arf.toys.db.nonEntityModel;

public class Siparis {
	
	private String siparisNo;
	private String xid;
	private String returnmessage;		
	private String returncode; // 0 : Onaylanmadı 1 : Onaylandı 2 : Daha önceden onaylanmış
	private String authcode;
	private String ykbrefno;
	private String amount;
	private String currency;
	private String errmsg;
	private String errcode;
	private String response;
	private String hostrefnum;
	private String transid;
	private String clientip;
	private String maskedpan;
	private String islemtarihi;
	private String rnd;
	private String hashparams;
	private String hashparamsval;
	private String hash;
	private String mdstatus;
	private String merchantid;
	private String txstatus;
	private String ireqcode;
	private String ireqdetail;
	private String vendorcode;
	private String paressyntaxok;
	private String paresverified;
	private String eci;
	private String cavv;
	private String cavvalgorthm;
	private String md;
	private String version;
	private String sid;
	private String mderrormsg;
	private String procreturncode;
		
	public String getSiparisNo() {
		return siparisNo;
	}
	public void setSiparisNo(String siparisNo) {
		this.siparisNo = siparisNo;
	}
	public String getXid() {
		return xid;
	}
	public void setXid(String xid) {
		this.xid = xid;
	}
	public String getReturnmessage() {
		return returnmessage;
	}
	public void setReturnmessage(String returnmessage) {
		this.returnmessage = returnmessage;
	}
	public String getReturncode() {
		return returncode;
	}
	public void setReturncode(String returncode) {
		this.returncode = returncode;
	}
	public String getAuthcode() {
		return authcode;
	}
	public void setAuthcode(String authcode) {
		this.authcode = authcode;
	}
	public String getYkbrefno() {
		return ykbrefno;
	}
	public void setYkbrefno(String ykbrefno) {
		this.ykbrefno = ykbrefno;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public String getErrmsg() {
		return errmsg;
	}
	public void setErrmsg(String errmsg) {
		this.errmsg = errmsg;
	}
	public String getErrcode() {
		return errcode;
	}
	public void setErrcode(String errcode) {
		this.errcode = errcode;
	}
	public String getResponse() {
		return response;
	}
	public void setResponse(String response) {
		this.response = response;
	}
	public String getHostrefnum() {
		return hostrefnum;
	}
	public void setHostrefnum(String hostrefnum) {
		this.hostrefnum = hostrefnum;
	}
	public String getTransid() {
		return transid;
	}
	public void setTransid(String transid) {
		this.transid = transid;
	}
	public String getClientip() {
		return clientip;
	}
	public void setClientip(String clientip) {
		this.clientip = clientip;
	}
	public String getMaskedpan() {
		return maskedpan;
	}
	public void setMaskedpan(String maskedpan) {
		this.maskedpan = maskedpan;
	}
	public String getIslemtarihi() {
		return islemtarihi;
	}
	public void setIslemtarihi(String islemtarihi) {
		this.islemtarihi = islemtarihi;
	}
	public String getRnd() {
		return rnd;
	}
	public void setRnd(String rnd) {
		this.rnd = rnd;
	}
	public String getHashparams() {
		return hashparams;
	}
	public void setHashparams(String hashparams) {
		this.hashparams = hashparams;
	}
	public String getHashparamsval() {
		return hashparamsval;
	}
	public void setHashparamsval(String hashparamsval) {
		this.hashparamsval = hashparamsval;
	}
	public String getHash() {
		return hash;
	}
	public void setHash(String hash) {
		this.hash = hash;
	}
	public String getMdstatus() {
		return mdstatus;
	}
	public void setMdstatus(String mdstatus) {
		this.mdstatus = mdstatus;
	}
	public String getMerchantid() {
		return merchantid;
	}
	public void setMerchantid(String merchantid) {
		this.merchantid = merchantid;
	}
	public String getTxstatus() {
		return txstatus;
	}
	public void setTxstatus(String txstatus) {
		this.txstatus = txstatus;
	}
	public String getIreqcode() {
		return ireqcode;
	}
	public void setIreqcode(String ireqcode) {
		this.ireqcode = ireqcode;
	}
	public String getIreqdetail() {
		return ireqdetail;
	}
	public void setIreqdetail(String ireqdetail) {
		this.ireqdetail = ireqdetail;
	}
	public String getVendorcode() {
		return vendorcode;
	}
	public void setVendorcode(String vendorcode) {
		this.vendorcode = vendorcode;
	}
	public String getParessyntaxok() {
		return paressyntaxok;
	}
	public void setParessyntaxok(String paressyntaxok) {
		this.paressyntaxok = paressyntaxok;
	}
	public String getParesverified() {
		return paresverified;
	}
	public void setParesverified(String paresverified) {
		this.paresverified = paresverified;
	}
	public String getEci() {
		return eci;
	}
	public void setEci(String eci) {
		this.eci = eci;
	}
	public String getCavv() {
		return cavv;
	}
	public void setCavv(String cavv) {
		this.cavv = cavv;
	}
	
	public String getCavvalgorthm() {
		return cavvalgorthm;
	}
	public void setCavvalgorthm(String cavvalgorthm) {
		this.cavvalgorthm = cavvalgorthm;
	}
	public String getMd() {
		return md;
	}
	public void setMd(String md) {
		this.md = md;
	}
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	public String getSid() {
		return sid;
	}
	public void setSid(String sid) {
		this.sid = sid;
	}
	public String getMderrormsg() {
		return mderrormsg;
	}
	public void setMderrormsg(String mderrormsg) {
		this.mderrormsg = mderrormsg;
	}
	public String getProcreturncode() {
		return procreturncode;
	}
	public void setProcreturncode(String procreturncode) {
		this.procreturncode = procreturncode;
	} 
	
}
