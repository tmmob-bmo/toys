package tr.com.arf.toys.db.nonEntityModel;

import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.util.Date;

import tr.com.arf.framework.db.enumerated._base.BaseEnumerated;
import tr.com.arf.framework.db.model._base.BaseEntity;
import tr.com.arf.framework.utility.tool.DateUtil;
import tr.com.arf.framework.utility.tool.KeyUtil;
import tr.com.arf.framework.utility.tool.StringUtil;
import tr.com.arf.toys.db.enumerated.iletisim.AdresTipi;

public class PojoAdres{
	 
	
	private String hataBilgisiKodu; // KisiAdresBilgisi_HataBilgisi_Kod
									// YerlesimYeriAdresi_HataBilgisi_Kod
		
	private AdresTipi adrestipi;	// YerlesimYeriAdresi_AdresTip_Kod
									// KisiAdresBilgisi_AdresTip_Kod
	
	private Date kpsGuncellemeTarihi; 
	
	private String acikAdres;	// KisiAdresBilgisi_AcikAdres
								// YerlesimYeriAdresi_AcikAdres

	private String adresNo;	// KisiAdresBilgisi_AdresNo
							// YerlesimYeriAdresi_AdresNo
	
	private String beyanTarihiGun; 	// KisiAdresBilgisi_BeyanTarihi_Gun
									// YerlesimYeriAdresi_BeyanTarihi_Gun
	
	private String beyanTarihiAy; 	// KisiAdresBilgisi_BeyanTarihi_Ay
									// YerlesimYeriAdresi_BeyanTarihi_Ay
	
	private String beyanTarihiYil; 	// KisiAdresBilgisi_BeyanTarihi_Yil
									// YerlesimYeriAdresi_BeyanTarihi_Yil
	
	// private String beyandaBulunanKimlikNo; // YerlesimYeriAdresi_BeyandaBulunanKimlikNo
	
	
	/* ASAGIDAKI ALANLAR "ENDSWITH" ILE CEKILECEK !!! 	*/
	private String binaAda; 		// YerlesimYeriAdresi_IlIlceMerkezAdresi_BinaAda
	
	private String binaBlokAdi; 	// YerlesimYeriAdresi_IlIlceMerkezAdresi_BinaBlokAdi

	private String binaKodu; 		// YerlesimYeriAdresi_IlIlceMerkezAdresi_BinaKodu
	
	private String binaPafta; 		// YerlesimYeriAdresi_IlIlceMerkezAdresi_BinaPafta
	
	private String binaParsel; 		// YerlesimYeriAdresi_IlIlceMerkezAdresi_BinaParsel
	
	private String binaSiteAdi; 	// YerlesimYeriAdresi_IlIlceMerkezAdresi_BinaSiteAdi

	private String csbm;			// YerlesimYeriAdresi_IlIlceMerkezAdresi_Csbm

	private String csbmKodu; 		// YerlesimYeriAdresi_IlIlceMerkezAdresi_CsbmKodu

	private String disKapiNo; 		// YerlesimYeriAdresi_IlIlceMerkezAdresi_DisKapiNo

	private String icKapiNo;		// YerlesimYeriAdresi_IlIlceMerkezAdresi_IcKapiNo
 
	private String ilKodu;			// YerlesimYeriAdresi_IlIlceMerkezAdresi_IlKodu

	private String ilceKodu;		// YerlesimYeriAdresi_IlIlceMerkezAdresi_IlceKodu

	private String mahalle;			// YerlesimYeriAdresi_IlIlceMerkezAdresi_Mahalle

	private String mahalleKodu;		// YerlesimYeriAdresi_IlIlceMerkezAdresi_MahalleKodu

	private String tasinmaTarihiGun; 	// KisiAdresBilgisi_TasinmaTarihi_Gun
										// YerlesimYeriAdresi_TasinmaTarihi_Gun
	
	private String tasinmaTarihiAy; 	// KisiAdresBilgisi_TasinmaTarihi_Ay
										// YerlesimYeriAdresi_TasinmaTarihi_Ay
	
	private String tasinmaTarihiYil; 	// KisiAdresBilgisi_TasinmaTarihi_Yil
										// YerlesimYeriAdresi_TasinmaTarihi_Yil
	
	private String tescilTarihiGun; 	// KisiAdresBilgisi_TescilTarihi_Gun
										// YerlesimYeriAdresi_TescilTarihi_Gun

	private String tescilTarihiAy; 		// KisiAdresBilgisi_TescilTarihi_Ay
										// YerlesimYeriAdresi_TescilTarihi_Ay
	
	private String tescilTarihiYil; 	// KisiAdresBilgisi_TescilTarihi_Yil
										// YerlesimYeriAdresi_TescilTarihi_Yil
	
	public PojoAdres() {

	}

	public String getHataBilgisiKodu() {
		return hataBilgisiKodu;
	}

	public void setHataBilgisiKodu(String hataBilgisiKodu) {
		this.hataBilgisiKodu = hataBilgisiKodu;
	}

	public AdresTipi getAdrestipi() {
		return adrestipi;
	}

	public void setAdrestipi(AdresTipi adrestipi) {
		this.adrestipi = adrestipi;
	}

	public Date getKpsGuncellemeTarihi() {
		return kpsGuncellemeTarihi;
	}

	public void setKpsGuncellemeTarihi(Date kpsGuncellemeTarihi) {
		this.kpsGuncellemeTarihi = kpsGuncellemeTarihi;
	} 

	public String getAcikAdres() {
		return acikAdres;
	}

	public void setAcikAdres(String acikAdres) {
		this.acikAdres = acikAdres;
	}

	public String getAdresNo() {
		return adresNo;
	}

	public void setAdresNo(String adresNo) {
		this.adresNo = adresNo;
	}

	public String getBeyanTarihiGun() {
		return beyanTarihiGun;
	}

	public void setBeyanTarihiGun(String beyanTarihiGun) {
		this.beyanTarihiGun = beyanTarihiGun;
	}

	public String getBeyanTarihiAy() {
		return beyanTarihiAy;
	}

	public void setBeyanTarihiAy(String beyanTarihiAy) {
		this.beyanTarihiAy = beyanTarihiAy;
	}

	public String getBeyanTarihiYil() {
		return beyanTarihiYil;
	}

	public void setBeyanTarihiYil(String beyanTarihiYil) {
		this.beyanTarihiYil = beyanTarihiYil;
	} 

	public String getBinaAda() {
		return binaAda;
	}

	public void setBinaAda(String binaAda) {
		this.binaAda = binaAda;
	}

	public String getBinaBlokAdi() {
		return binaBlokAdi;
	}

	public void setBinaBlokAdi(String binaBlokAdi) {
		this.binaBlokAdi = binaBlokAdi;
	}

	public String getBinaKodu() {
		return binaKodu;
	}

	public void setBinaKodu(String binaKodu) {
		this.binaKodu = binaKodu;
	}

	public String getBinaPafta() {
		return binaPafta;
	}

	public void setBinaPafta(String binaPafta) {
		this.binaPafta = binaPafta;
	}

	public String getBinaParsel() {
		return binaParsel;
	}

	public void setBinaParsel(String binaParsel) {
		this.binaParsel = binaParsel;
	}

	public String getBinaSiteAdi() {
		return binaSiteAdi;
	}

	public void setBinaSiteAdi(String binaSiteAdi) {
		this.binaSiteAdi = binaSiteAdi;
	}

	public String getCsbm() {
		return csbm;
	}

	public void setCsbm(String csbm) {
		this.csbm = csbm;
	}

	public String getCsbmKodu() {
		return csbmKodu;
	}

	public void setCsbmKodu(String csbmKodu) {
		this.csbmKodu = csbmKodu;
	}

	public String getDisKapiNo() {
		return disKapiNo;
	}

	public void setDisKapiNo(String disKapiNo) {
		this.disKapiNo = disKapiNo;
	}

	public String getIcKapiNo() {
		return icKapiNo;
	}

	public void setIcKapiNo(String icKapiNo) {
		this.icKapiNo = icKapiNo;
	}

	public String getIlKodu() {
		return formatDateColumns(ilKodu);
	}

	public void setIlKodu(String ilKodu) {
		this.ilKodu = ilKodu;
	}

	public String getIlceKodu() {
		return ilceKodu;
	}

	public void setIlceKodu(String ilceKodu) {
		this.ilceKodu = ilceKodu;
	}

	public String getMahalle() {
		return mahalle;
	}

	public void setMahalle(String mahalle) {
		this.mahalle = mahalle;
	}

	public String getMahalleKodu() {
		return mahalleKodu;
	}

	public void setMahalleKodu(String mahalleKodu) {
		this.mahalleKodu = mahalleKodu;
	}

	public String getTasinmaTarihiGun() {
		return tasinmaTarihiGun;
	}

	public void setTasinmaTarihiGun(String tasinmaTarihiGun) {
		this.tasinmaTarihiGun = tasinmaTarihiGun;
	}

	public String getTasinmaTarihiAy() {
		return tasinmaTarihiAy;
	}

	public void setTasinmaTarihiAy(String tasinmaTarihiAy) {
		this.tasinmaTarihiAy = tasinmaTarihiAy;
	}

	public String getTasinmaTarihiYil() {
		return tasinmaTarihiYil;
	}

	public void setTasinmaTarihiYil(String tasinmaTarihiYil) {
		this.tasinmaTarihiYil = tasinmaTarihiYil;
	}
	
	public String getTescilTarihiGun() {
		return tescilTarihiGun;
	}

	public void setTescilTarihiGun(String tescilTarihiGun) {
		this.tescilTarihiGun = tescilTarihiGun;
	}
	
	public String getTescilTarihiAy() {
		return tescilTarihiAy;
	}

	public void setTescilTarihiAy(String tescilTarihiAy) {
		this.tescilTarihiAy = tescilTarihiAy;
	}

	public String getTescilTarihiYil() {
		return tescilTarihiYil;
	}

	public void setTescilTarihiYil(String tescilTarihiYil) {
		this.tescilTarihiYil = tescilTarihiYil;
	}

	public String getTasinmaTarihi() {
		return formatDateColumns(getTasinmaTarihiGun()) + "/" + formatDateColumns(getTasinmaTarihiAy()) + "/" + getTasinmaTarihiYil();
	}
	
	public String getTescilTarihi() {
		return formatDateColumns(getTescilTarihiGun()) + "/" + formatDateColumns(getTescilTarihiAy()) + "/" + getTescilTarihiYil();
	}
	
	public String getBeyanTarihi() {
		return formatDateColumns(getBeyanTarihiGun()) + "/" + formatDateColumns(getBeyanTarihiAy()) + "/" + getBeyanTarihiYil();
	}
	
	public String formatDateColumns(String s){
		if(s == null){
			return "";
		} if(s.length() == 1) {
			return "0" + s;
		} else {
			return s;
		}
	}

	public String getValue() {
		StringBuilder value = new StringBuilder("");
		String className = StringUtil.makeFirstCharLowerCase(this.getClass().getSimpleName());
		String val = "";
		for (Field field : this.getClass().getDeclaredFields()) {
			field.setAccessible(true);
			val = "";
			if (!field.getName().equalsIgnoreCase("RID") && !field.getName().contains("serialVersion")) {
				Class<?> fieldType = field.getType();
				try {
					if(field.get(this) != null){
						if (fieldType.equals(String.class)) {
							val = (String) field.get(this);
						} else if (Date.class.isAssignableFrom(fieldType)) {
							val = DateUtil.dateToDMYHMS((Date) field.get(this));
						} else if (BigDecimal.class.isAssignableFrom(fieldType)) {
							val = ((BigDecimal) field.get(this)).toString();
						} else if (Number.class.isAssignableFrom(fieldType)) {
							val = ((Number) field.get(this)).toString();
						} else if (fieldType.equals(Integer.TYPE)) {
							val = ((Integer) field.get(this)).toString();
						} else if (fieldType.equals(Long.TYPE) && !field.getName().contains("serialVersion")) {
							val = ((Long) field.get(this)).toString();
						} else if (fieldType.equals(Double.TYPE)) {
							val = ((Double) field.get(this)).toString();
						} else if (Enum.class.isAssignableFrom(fieldType)) {
							val = ((BaseEnumerated) field.get(this)).getLabel();
						} else if (BaseEntity.class.isAssignableFrom(fieldType)) {
							if (field.get(this) != null) {
								val = ((BaseEntity) field.get(this)).getUIString();
							}
						} else if (!field.getName().contains("serialVersion")) {
							val = field.get(this).toString();
						}
					}
					value.append(field.getName() + ":" + val + ", \n");
				} catch (Exception e) {
					System.out.println("ERROR @getValue :" + field.getName());
					System.out.println("ERROR :" + e.getMessage());
					value.append(KeyUtil.getLabelValue(className + "." + field.getName()) + " : ?, ");
				}

			}
		}
		return value.toString().replace("\\", "//").replace("'", "`");
	}
	
	
	 
}