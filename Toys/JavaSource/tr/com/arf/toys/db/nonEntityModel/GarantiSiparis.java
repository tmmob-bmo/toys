package tr.com.arf.toys.db.nonEntityModel;

public class GarantiSiparis {

	private String siparisNo;
	private String terminalID; // TerminalID, başına 0 konularak 9 digit’e tamamlanmalıdır. HashData’da kullanılan TerminalID’in başına ise 0 konulmamalıdır.
	private String password;
	private String number;

	// “.” ve/veya “,” karakterleri Replace edilmelidir. Örn;175,95 olan tutar 17595 olarak gönderilmelidir.Son 2 digit kuruş olarak algılanır.
	// Örn;17595 olarak gönderdi iniz tutar aslında 175,95 olarak algılanır.
	private String amount;
	private String hashData;
	// 1. Security Data: Password + TerminalID
	// 2. Hash Data: OrderID + TerminalID + Number + Amount + SecurityData



	public String getSiparisNo() {
		return siparisNo;
	}

	public void setSiparisNo(String siparisNo) {
		this.siparisNo = siparisNo;
	}

	public String getTerminalID() {
		return terminalID;
	}

	public void setTerminalID(String terminalID) {
		this.terminalID = terminalID;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getHashData() {
		return hashData;
	}

	public void setHashData(String hashData) {
		this.hashData = hashData;
	}
}
