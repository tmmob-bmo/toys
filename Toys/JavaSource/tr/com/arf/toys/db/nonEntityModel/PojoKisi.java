package tr.com.arf.toys.db.nonEntityModel;

import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.util.Date;

import tr.com.arf.framework.db.enumerated._base.BaseEnumerated;
import tr.com.arf.framework.db.model._base.BaseEntity;
import tr.com.arf.framework.utility.tool.DateUtil;
import tr.com.arf.framework.utility.tool.KeyUtil;
import tr.com.arf.framework.utility.tool.StringUtil;

public class PojoKisi {

	private String ad; // TemelBilgisi_Ad
	private String soyad; // TemelBilgisi_Soyad

	private String cinsiyetKod; // TemelBilgisi_Cinsiyet_Kod
	private String cinsiyetAciklama; // TemelBilgisi_Cinsiyet_Aciklama = Erkek

	private Long tcKimlikNo; // TCKimlikNo = 
	private String babaAdi; // TemelBilgisi_BabaAd =  E....
	private String anaAdi; // TemelBilgisi_AnneAd = T..

	private String medeniHalKodu; // DurumBilgisi_MedeniHal_Kod = 2
	private String medeniHalAciklamasi; // DurumBilgisi_MedeniHal_Aciklama = Evli

	private String dogumYeri; // TemelBilgisi_DogumYer = SAFRANBOLU

	private String dogumTarihiGun; // TemelBilgisi_DogumTarih_Gun = 17
	private String dogumTarihiAy; // TemelBilgisi_DogumTarih_Ay = 3
	private String dogumTarihiYil; // TemelBilgisi_DogumTarih_Yil = 1980

	private String olumTarihiGun; // DurumBilgisi_OlumTarih_Gun =
	private String olumTarihiAy; // DurumBilgisi_OlumTarih_Ay =
	private String olumTarihiYil; // DurumBilgisi_OlumTarih_Yil =

	private Long babaTCKimlikNo; // BabaTCKimlikNo
	private Long anneTCKimlikNo; // AnneTCKimlikNo
	private Long esTCKimlikNo; // EsTCKimlikNo

	private String durumKod; // DurumBilgisi_Durum_Kod = 1
	private String durumAciklama; // DurumBilgisi_Durum_Aciklama = Aç?k

	private String nufusaKayitliOlduguIlceKodu; // KayitYeriBilgisi_Ilce_Kod = 1587
	private String nufusaKayitliOlduguIlceAdi; // KayitYeriBilgisi_Ilce_Aciklama = Safranbolu

	private String nufusaKayitliOlduguIlKodu; // KayitYeriBilgisi_Il_Kod = 78
	private String nufusaKayitliOlduguIlAdi; // KayitYeriBilgisi_Il_Aciklama = Karabük

	private String ciltNo; // KayitYeriBilgisi_Cilt_Kod = XX
	private String ciltAciklama; // KayitYeriBilgisi_Cilt_Aciklama = XXXX

	private String aileSiraNo; // KayitYeriBilgisi_AileSiraNo = XX
	private String bireySiraNo; // KayitYeriBilgisi_BireySiraNo = XX

	private String hataBilgisiKodu;
	private String mahalle;

	public PojoKisi() {

	}

	public String getDogumTarihi() {
		return getDogumTarihiGun() + "/" + getDogumTarihiAy() + "/" + getDogumTarihiYil();
	}

	public String getOlumTarihi() {
		return getOlumTarihiGun() + "/" + getOlumTarihiAy() + "/" + getOlumTarihiYil();
	}

	public String getAd() {
		return ad;
	}

	public void setAd(String ad) {
		this.ad = ad;
	}

	public String getSoyad() {
		return soyad;
	}

	public void setSoyad(String soyad) {
		this.soyad = soyad;
	}

	public String getCinsiyetKod() {
		return cinsiyetKod;
	}

	public void setCinsiyetKod(String cinsiyetKod) {
		this.cinsiyetKod = cinsiyetKod;
	}

	public String getCinsiyetAciklama() {
		return cinsiyetAciklama;
	}

	public void setCinsiyetAciklama(String cinsiyetAciklama) {
		this.cinsiyetAciklama = cinsiyetAciklama;
	}

	public Long getTcKimlikNo() {
		return tcKimlikNo;
	}

	public void setTcKimlikNo(Long tcKimlikNo) {
		this.tcKimlikNo = tcKimlikNo;
	}

	public String getBabaAdi() {
		return babaAdi;
	}

	public void setBabaAdi(String babaAdi) {
		this.babaAdi = babaAdi;
	}

	public String getAnaAdi() {
		return anaAdi;
	}

	public void setAnaAdi(String anaAdi) {
		this.anaAdi = anaAdi;
	}

	public String getMedeniHalKodu() {
		return medeniHalKodu;
	}

	public void setMedeniHalKodu(String medeniHalKodu) {
		this.medeniHalKodu = medeniHalKodu;
	}

	public String getMedeniHalAciklamasi() {
		return medeniHalAciklamasi;
	}

	public void setMedeniHalAciklamasi(String medeniHalAciklamasi) {
		this.medeniHalAciklamasi = medeniHalAciklamasi;
	}

	public String getDogumYeri() {
		return dogumYeri;
	}

	public void setDogumYeri(String dogumYeri) {
		this.dogumYeri = dogumYeri;
	}

	public String getOlumTarihiGun() {
		return olumTarihiGun;
	}

	public void setOlumTarihiGun(String olumTarihiGun) {
		this.olumTarihiGun = olumTarihiGun;
	}

	public String getOlumTarihiAy() {
		return olumTarihiAy;
	}

	public void setOlumTarihiAy(String olumTarihiAy) {
		this.olumTarihiAy = olumTarihiAy;
	}

	public String getOlumTarihiYil() {
		return olumTarihiYil;
	}

	public void setOlumTarihiYil(String olumTarihiYil) {
		this.olumTarihiYil = olumTarihiYil;
	}

	public String getDogumTarihiGun() {
		return dogumTarihiGun;
	}

	public void setDogumTarihiGun(String dogumTarihiGun) {
		this.dogumTarihiGun = dogumTarihiGun;
	}

	public String getDogumTarihiAy() {
		return dogumTarihiAy;
	}

	public void setDogumTarihiAy(String dogumTarihiAy) {
		this.dogumTarihiAy = dogumTarihiAy;
	}

	public String getDogumTarihiYil() {
		return dogumTarihiYil;
	}

	public void setDogumTarihiYil(String dogumTarihiYil) {
		this.dogumTarihiYil = dogumTarihiYil;
	}

	public Long getBabaTCKimlikNo() {
		return babaTCKimlikNo;
	}

	public void setBabaTCKimlikNo(Long babaTCKimlikNo) {
		this.babaTCKimlikNo = babaTCKimlikNo;
	}

	public Long getAnneTCKimlikNo() {
		return anneTCKimlikNo;
	}

	public void setAnneTCKimlikNo(Long anneTCKimlikNo) {
		this.anneTCKimlikNo = anneTCKimlikNo;
	}

	public Long getEsTCKimlikNo() {
		return esTCKimlikNo;
	}

	public void setEsTCKimlikNo(Long esTCKimlikNo) {
		this.esTCKimlikNo = esTCKimlikNo;
	}

	public String getDurumKod() {
		return durumKod;
	}

	public void setDurumKod(String durumKod) {
		this.durumKod = durumKod;
	}

	public String getDurumAciklama() {
		return durumAciklama;
	}

	public void setDurumAciklama(String durumAciklama) {
		this.durumAciklama = durumAciklama;
	}

	public String getNufusaKayitliOlduguIlceKodu() {
		return nufusaKayitliOlduguIlceKodu;
	}

	public void setNufusaKayitliOlduguIlceKodu(String nufusaKayitliOlduguIlceKodu) {
		this.nufusaKayitliOlduguIlceKodu = nufusaKayitliOlduguIlceKodu;
	}

	public String getNufusaKayitliOlduguIlceAdi() {
		return nufusaKayitliOlduguIlceAdi;
	}

	public void setNufusaKayitliOlduguIlceAdi(String nufusaKayitliOlduguIlceAdi) {
		this.nufusaKayitliOlduguIlceAdi = nufusaKayitliOlduguIlceAdi;
	}

	public String getNufusaKayitliOlduguIlKodu() {
		return nufusaKayitliOlduguIlKodu;
	}

	public void setNufusaKayitliOlduguIlKodu(String nufusaKayitliOlduguIlKodu) {
		this.nufusaKayitliOlduguIlKodu = nufusaKayitliOlduguIlKodu;
	}

	public String getNufusaKayitliOlduguIlAdi() {
		return nufusaKayitliOlduguIlAdi;
	}

	public void setNufusaKayitliOlduguIlAdi(String nufusaKayitliOlduguIlAdi) {
		this.nufusaKayitliOlduguIlAdi = nufusaKayitliOlduguIlAdi;
	}

	public String getCiltNo() {
		return ciltNo;
	}

	public void setCiltNo(String ciltNo) {
		this.ciltNo = ciltNo;
	}

	public String getCiltAciklama() {
		return ciltAciklama;
	}

	public void setCiltAciklama(String ciltAciklama) {
		this.ciltAciklama = ciltAciklama;
	}

	public String getAileSiraNo() {
		return aileSiraNo;
	}

	public void setAileSiraNo(String aileSiraNo) {
		this.aileSiraNo = aileSiraNo;
	}

	public String getBireySiraNo() {
		return bireySiraNo;
	}

	public void setBireySiraNo(String bireySiraNo) {
		this.bireySiraNo = bireySiraNo;
	}

	public String getHataBilgisiKodu() {
		return hataBilgisiKodu;
	}

	public void setHataBilgisiKodu(String hataBilgisiKodu) {
		this.hataBilgisiKodu = hataBilgisiKodu;
	}

	public String getMahalle() {
		return mahalle;
	}

	public void setMahalle(String mahalle) {
		this.mahalle = mahalle;
	}

	public void printOutValues() {

		System.out.println("TC Kimlik No :" + getTcKimlikNo());
		// System.out.println("DogumYeriKodu :" + getDogumYeriKodu());
		System.out.println("DogumTarihi :" + getDogumTarihiGun() + "/" + getDogumTarihiAy() + "/" + getDogumTarihiYil());
		System.out.println("AileSiraNo :" + getAileSiraNo());
		System.out.println("BireySiraNo :" + getBireySiraNo());
		System.out.println("Cilt Kod :" + getCiltNo());
		System.out.println("Cilt Aciklamasi :" + getCiltAciklama());
		System.out.println("NufusaKayitliOlduguIlAdi :" + getNufusaKayitliOlduguIlAdi());
		System.out.println("NufusaKayitliOlduguIlKodu :" + getNufusaKayitliOlduguIlKodu());
		System.out.println("NufusaKayitliOlduguIlceAdi :" + getNufusaKayitliOlduguIlceAdi());
		System.out.println("NufusaKayitliOlduguIlceKodu :" + getNufusaKayitliOlduguIlceKodu());
		System.out.println("Ad :" + getAd());
		System.out.println("Soyad :" + getSoyad());
		System.out.println("AnaAdi :" + getAnaAdi());
		System.out.println("BabaAd :" + getBabaAdi());
		System.out.println("DogumYer :" + getDogumYeri());
		System.out.println("Cinsiyet Kod :" + getCinsiyetKod());
		System.out.println("Cinsiyet Aciklamasi :" + getCinsiyetAciklama());
		System.out.println("OlumTarihi :" + getOlumTarihiGun() + "/" + getOlumTarihiAy() + "/" + getOlumTarihiYil());
		System.out.println("MedeniHal Kod :" + getMedeniHalKodu());
		System.out.println("MedeniHal Aciklamasi :" + getMedeniHalAciklamasi());

		System.out.println("EsTCKimlikNo :" + getEsTCKimlikNo());
		System.out.println("BabaTCKimlikNo :" + getBabaTCKimlikNo());
		System.out.println("AnneTCKimlikNo :" + getAnneTCKimlikNo());
	}
	
	public String getValue() {
		StringBuilder value = new StringBuilder("");
		String className = StringUtil.makeFirstCharLowerCase(this.getClass().getSimpleName());
		String val = "";
		for (Field field : this.getClass().getDeclaredFields()) {
			field.setAccessible(true);
			val = "";
			if (!field.getName().equalsIgnoreCase("RID") && !field.getName().contains("serialVersion")) {
				Class<?> fieldType = field.getType();
				try {
					if(field.get(this) != null){
						if (fieldType.equals(String.class)) {
							val = (String) field.get(this);
						} else if (Date.class.isAssignableFrom(fieldType)) {
							val = DateUtil.dateToDMYHMS((Date) field.get(this));
						} else if (BigDecimal.class.isAssignableFrom(fieldType)) {
							val = ((BigDecimal) field.get(this)).toString();
						} else if (Number.class.isAssignableFrom(fieldType)) {
							val = ((Number) field.get(this)).toString();
						} else if (fieldType.equals(Integer.TYPE)) {
							val = ((Integer) field.get(this)).toString();
						} else if (fieldType.equals(Long.TYPE) && !field.getName().contains("serialVersion")) {
							val = ((Long) field.get(this)).toString();
						} else if (fieldType.equals(Double.TYPE)) {
							val = ((Double) field.get(this)).toString();
						} else if (Enum.class.isAssignableFrom(fieldType)) {
							val = ((BaseEnumerated) field.get(this)).getLabel();
						} else if (BaseEntity.class.isAssignableFrom(fieldType)) {
							if (field.get(this) != null) {
								val = ((BaseEntity) field.get(this)).getUIString();
							}
						} else if (!field.getName().contains("serialVersion")) {
							val = field.get(this).toString();
						}
					}
					value.append(field.getName() + ":" + val + ", \n");
				} catch (Exception e) {
					System.out.println("ERROR @getValue :" + field.getName());
					System.out.println("ERROR :" + e.getMessage());
					value.append(KeyUtil.getLabelValue(className + "." + field.getName()) + " : ?, ");
				}

			}
		}
		return value.toString().replace("\\", "//").replace("'", "`");
	}
}