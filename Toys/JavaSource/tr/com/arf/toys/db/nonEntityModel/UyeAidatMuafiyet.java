package tr.com.arf.toys.db.nonEntityModel;

import java.math.BigDecimal;

public class UyeAidatMuafiyet {
	
	 private int yil;
     private BigDecimal aidatTutari;
     private BigDecimal kalanborc ;
     private int odenenay;
     private Long muafolduguay;
     private Long kalanay;
     
	public int getYil() {
		return yil;
	}
	public void setYil(int yil) {
		this.yil = yil;
	}
	public BigDecimal getAidatTutari() {
		return aidatTutari;
	}
	public void setAidatTutari(BigDecimal aidatTutari) {
		this.aidatTutari = aidatTutari;
	}
	public BigDecimal getKalanborc() {
		return kalanborc;
	}
	public void setKalanborc(BigDecimal kalanborc) {
		this.kalanborc = kalanborc;
	}
	public int getOdenenay() {
		return odenenay;
	}
	public void setOdenenay(int odenenay) {
		this.odenenay = odenenay;
	}
	public Long getKalanay() {
		return kalanay;
	}
	public void setKalanay(Long kalanay) {
		this.kalanay = kalanay;
	}
	public Long getMuafolduguay() {
		return muafolduguay;
	}
	public void setMuafolduguay(Long muafolduguay) {
		this.muafolduguay = muafolduguay;
	}
}
