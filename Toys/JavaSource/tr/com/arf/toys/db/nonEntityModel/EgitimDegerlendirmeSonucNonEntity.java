package tr.com.arf.toys.db.nonEntityModel; 
 
import java.io.Serializable;

import tr.com.arf.toys.db.enumerated.egitim.Soru;

 
public class EgitimDegerlendirmeSonucNonEntity implements Serializable{ 
 
	private static final long serialVersionUID = 1L; 
	
	private Soru soru;
	private int evetsayisi;
	private int hayirsayisi; 
	private int bossayisi;
	
	public EgitimDegerlendirmeSonucNonEntity() { 
		super(); 
	}

	public EgitimDegerlendirmeSonucNonEntity(Soru soru, int evetsayisi,
			int hayirsayisi, int bossayisi) {
		super();
		this.soru = soru;
		this.evetsayisi = evetsayisi;
		this.hayirsayisi = hayirsayisi;
		this.bossayisi = bossayisi;
	}

	public Soru getSoru() {
		return soru;
	}

	public void setSoru(Soru soru) {
		this.soru = soru;
	}

	public int getEvetsayisi() {
		return evetsayisi;
	}

	public void setEvetsayisi(int evetsayisi) {
		this.evetsayisi = evetsayisi;
	}

	public int getHayirsayisi() {
		return hayirsayisi;
	}

	public void setHayirsayisi(int hayirsayisi) {
		this.hayirsayisi = hayirsayisi;
	}

	public int getBossayisi() {
		return bossayisi;
	}

	public void setBossayisi(int bossayisi) {
		this.bossayisi = bossayisi;
	}

}	 
