package tr.com.arf.toys.db.model.system; 
 
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import tr.com.arf.framework.utility.tool.BigDecimalUtil;
import tr.com.arf.framework.utility.tool.KeyUtil;
import tr.com.arf.toys.db.model.base.ToysBaseEntity;
 
 
@Entity 
@Table(name = "ODEMETIP") 
public class Odemetip extends ToysBaseEntity { 
 
	private static final long serialVersionUID = 1L; 
 
	@Id 
	@GeneratedValue(strategy = GenerationType.IDENTITY) 
	@Column(updatable = false, unique = true, nullable = false, name = "RID") 
	private Long rID; 
 
	@Column(name = "KOD") 
	private String kod; 
 
	@Column(name = "AD") 
	private String ad; 
 
	@Column(name = "MIKTAR") 
	private BigDecimal miktar; 
 
	@Temporal(TemporalType.DATE) 
	@Column(name = "BASLANGICTARIH") 
	private java.util.Date baslangictarih; 
 
	@Temporal(TemporalType.DATE) 
	@Column(name = "BITISTARIH") 
	private java.util.Date bitistarih; 
 
 
	public Odemetip() { 
		super(); 
	} 
 
	@Override
	public Long getRID() { 
		return this.rID;  
	}  
	  
	@Override
	public void setRID(Long rID) {  
		this.rID = rID;  
	}   
 
	public String getKod() { 
		return this.kod; 
	} 
	 
	public void setKod(String kod) { 
		this.kod = kod; 
	} 
 
	public String getAd() { 
		return this.ad; 
	} 
	 
	public void setAd(String ad) { 
		this.ad = ad; 
	} 
 
	public BigDecimal getMiktar() { 
		return BigDecimalUtil.changeToCurrency(this.miktar); 
	} 
	 
	public void setMiktar(BigDecimal miktar) { 
		this.miktar = BigDecimalUtil.changeToCurrency(miktar); 
	} 
 
	public java.util.Date getBaslangictarih() { 
		return baslangictarih; 
	} 
	 
	public void setBaslangictarih(java.util.Date baslangictarih) { 
		this.baslangictarih = baslangictarih; 
	} 
 
	public java.util.Date getBitistarih() { 
		return bitistarih; 
	} 
	 
	public void setBitistarih(java.util.Date bitistarih) { 
		this.bitistarih = bitistarih; 
	} 
 
 
	@Override 
	public String toString() { 
		return "tr.com.arf.toys.db.model.system.Odemetip[id=" + this.getRID()  + "]"; 
	} 
	@Override 
	public String getUIString() { 
		return getKod() + ""; 
	} 
	 
	@Override 
	public void initValues(boolean defaultValues) { 
		if (defaultValues) { 
				// TODO Initialize with default values 
		} else { 
			this.rID = null; 			this.kod = ""; 			this.ad = ""; 			this.miktar = null; 			this.baslangictarih = null; 			this.bitistarih = null; 		}  
	}  
	 
	@Override 
	public String getValue() {  
		StringBuilder value = new StringBuilder(""); 
			if(getKod() != null) {
				value.append(KeyUtil.getLabelValue("odemetip.kod") + " : " + getKod() + "<br />");
			} 
			if(getAd() != null) {
				value.append(KeyUtil.getLabelValue("odemetip.ad") + " : " + getAd() + "<br />");
			} 
			if(getMiktar() != null) {
				value.append(KeyUtil.getLabelValue("odemetip.miktar") + " : " + getMiktar() + "<br />");
			} 
			if(getBaslangictarih() != null) {
				value.append(KeyUtil.getLabelValue("odemetip.baslangictarih") + " : " + getBaslangictarih() + "<br />");
			} 
			if(getBitistarih() != null) {
				value.append(KeyUtil.getLabelValue("odemetip.bitistarih") + " : " + getBitistarih() + "");
			} 
		return value.toString(); 
	} 
	 
}	 
