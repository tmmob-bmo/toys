package tr.com.arf.toys.db.model.system; 
 
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import tr.com.arf.framework.utility.tool.KeyUtil;
import tr.com.arf.toys.db.enumerated.system.DemirbasDurum;
import tr.com.arf.toys.db.enumerated.system.DemirbasTuru;
import tr.com.arf.toys.db.model.base.ToysBaseEntity;
 
 
@Entity 
@Table(name = "DEMIRBAS") 
public class Demirbas extends ToysBaseEntity { 
 
	private static final long serialVersionUID = 1L; 
 
	@Id 
	@GeneratedValue(strategy = GenerationType.IDENTITY) 
	@Column(updatable = false, unique = true, nullable = false, name = "RID") 
	private Long rID; 
 
	@Column(name = "CINSI") 
	private String cinsi; 
	
	@ManyToOne 
	@JoinColumn(name = "BIRIMREF") 
	private Birim birimRef;  
	
	@Column(name = "ADEDI") 
	private Integer adedi; 
	
	@Column(name = "KARARNO") 
	private String kararno; 
	
	@Temporal(TemporalType.DATE) 
	@Column(name = "BASLANGICTARIH") 
	private java.util.Date baslangictarih; 
	
	@Column(name = "FIYATI") 
	private BigDecimal fiyati; 
	
	@Column(name = "KULLANILDIGIYER") 
	private String kullanildigiyer; 
	
	@Column(name = "ACIKLAMA") 
	private String aciklama ; 
	
	@Enumerated(EnumType.ORDINAL) 
	@Column(name = "DEMIRBASDURUM") 
	private DemirbasDurum demirbasdurum; 
	
	@Enumerated(EnumType.ORDINAL) 
	@Column(name = "DEMIRBASTURU") 
	private DemirbasTuru demirbasturu; 
	
	public Demirbas() { 
		super(); 
	} 
 
	@Override
	public Long getRID() { 
		return this.rID;  
	}  
	  
	@Override
	public void setRID(Long rID) {  
		this.rID = rID;  
	}  

	public String getCinsi() {
		return cinsi;
	}

	public void setCinsi(String cinsi) {
		this.cinsi = cinsi;
	}

	public Birim getBirimRef() {
		return birimRef;
	}

	public void setBirimRef(Birim birimRef) {
		this.birimRef = birimRef;
	}

	public Integer getAdedi() {
		return adedi;
	}

	public void setAdedi(Integer adedi) {
		this.adedi = adedi;
	}

	public String getKararno() {
		return kararno;
	}

	public void setKararno(String kararno) {
		this.kararno = kararno;
	}

	public java.util.Date getBaslangictarih() {
		return baslangictarih;
	}

	public void setBaslangictarih(java.util.Date baslangictarih) {
		this.baslangictarih = baslangictarih;
	}

	public BigDecimal getFiyati() {
		return fiyati;
	}

	public void setFiyati(BigDecimal fiyati) {
		this.fiyati = fiyati;
	}
	
	public String getKullanildigiyer() {
		return kullanildigiyer;
	}

	public void setKullanildigiyer(String kullanildigiyer) {
		this.kullanildigiyer = kullanildigiyer;
	}

	public String getAciklama() {
		return aciklama;
	}

	public void setAciklama(String aciklama) {
		this.aciklama = aciklama;
	}

	public DemirbasDurum getDemirbasdurum() {
		return demirbasdurum;
	}

	public void setDemirbasdurum(DemirbasDurum demirbasdurum) {
		this.demirbasdurum = demirbasdurum;
	}

	public DemirbasTuru getDemirbasturu() {
		return demirbasturu;
	}

	public void setDemirbasturu(DemirbasTuru demirbasturu) {
		this.demirbasturu = demirbasturu;
	}

	@Override 
	public String toString() { 
		return "tr.com.arf.toys.db.model.system.Demirbas[id=" + this.getRID()  + "]"; 
	} 
	@Override 
	public String getUIString() { 
		return getCinsi()+""; 
	} 
	 
	@Override 
	public void initValues(boolean defaultValues) { 
		if (defaultValues) { 
				// TODO Initialize with default values 
		} else { 
			this.rID = null; 			this.birimRef=null;
			this.baslangictarih=null;
			this.kararno=null;
			this.adedi=null;
			this.cinsi=null;
			this.fiyati=null;
			this.demirbasdurum=DemirbasDurum._NULL;
			this.demirbasturu=DemirbasTuru._NULL;
			this.aciklama=null;
			this.kullanildigiyer=null;		}  
	}  
	 
	@Override 
	public String getValue() {  
		StringBuilder value = new StringBuilder(""); 
			if(getCinsi() != null) {
				value.append(KeyUtil.getLabelValue("demirbas.cinsi") + " : " + getCinsi() + "<br />");
			}
			if(getBirimRef() != null) {
				value.append(KeyUtil.getLabelValue("demirbas.birimRef") + " : " + getBirimRef().getUIString() + "");
			}
			if(getBaslangictarih() != null) {
				value.append(KeyUtil.getLabelValue("demirbas.baslangictarih") + " : " + getBaslangictarih() + "<br />");
			}
			if(getKararno() != null) {
				value.append(KeyUtil.getLabelValue("demirbas.kararno") + " : " + getKararno() + "<br />");
			}
			if(getAdedi() != null) {
				value.append(KeyUtil.getLabelValue("demirbas.adedi") + " : " + getAdedi() + "<br />");
			}
			if(getFiyati() != null) {
				value.append(KeyUtil.getLabelValue("demirbas.fiyati") + " : " + getFiyati() + "");
			} 
			if(getDemirbasdurum() != null) {
				value.append(KeyUtil.getLabelValue("demirbas.demirbasdurum") + " : " + getDemirbasdurum() + "");
			} 
			if(getDemirbasturu() != null) {
				value.append(KeyUtil.getLabelValue("demirbas.demirbasturu") + " : " + getDemirbasturu() + "");
			}
			if(getAciklama() != null) {
				value.append(KeyUtil.getLabelValue("demirbas.aciklama") + " : " + getAciklama() + "<br />");
			}
			if(getKullanildigiyer() != null) {
				value.append(KeyUtil.getLabelValue("demirbas.kullanildigiyer") + " : " + getKullanildigiyer() + "<br />");
			}
		return value.toString(); 
	} 
	 
}	 
