package tr.com.arf.toys.db.model.system;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import tr.com.arf.framework.utility.tool.KeyUtil;
import tr.com.arf.toys.db.model.base.ToysBaseEntity;


@Entity 
@Table(name = "INSAN")
public class Insan extends ToysBaseEntity {
	
	


	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(updatable=false,unique=true,nullable=false,name="RID")
	private Long rID;
	
	@Column(name="AD")
	private String ad;
	
	@Column(name="SOYAD")
	private String soyad;
	
	@Column(name="KIMLIKNO")
	private int tckn;
	
	@Column(name="CINSIYET")
	private char cinsiyet;
	
	public Insan(){
		
		super();
	}
	
	
	public Long getRID(){
		return this.rID;	
	}

	
	public void setRID(Long rID){
		this.rID=rID;
	}
	
	public String getAd(){
		return this.ad;	
	}

	
	public void setAd(String ad){
		this.ad=ad;
	}
	
	public String getSoyad(){
		return this.soyad;	
	}
	
	public void setSoyad(String soyad){
		this.soyad=soyad;
	}
	
	public int getKimlikno(){
		return this.tckn;	
	}

	
	public void setKimlikno(int tckn){
		this.tckn=tckn;
	}
	
	public int getCinsiyet(){
		return this.cinsiyet;	
	}

	
	public void setCinsiyet(char cinsiyet){
		this.cinsiyet=cinsiyet;
	}

	public String toString() { 
		return "tr.com.arf.toys.db.model.system.Insan[id=" + this.getRID()  + "]"; 
	} 
	
	
	
	@Override
	public String getUIString() {
		return getAd() + "";
	}


	@Override
	public void initValues(boolean defaultValues) {
		if(defaultValues){
		}
		else{
			this.rID=null;
			this.ad="";
			this.soyad=" ";
			this.tckn=0;
			this.cinsiyet=0;
			
		}
			
	}
	
	public String getValue() {  
		StringBuilder value = new StringBuilder(""); 
			if(getAd() != null) {
				value.append(KeyUtil.getLabelValue("insan.ad") + " : " + getAd() + "<br />");
			} 
			if(getSoyad() != null) {
				value.append(KeyUtil.getLabelValue("insan.soyad") + " : " + getSoyad() + "<br />");
			} 
			if(getKimlikno() != 0) {
				value.append(KeyUtil.getLabelValue("insan.tckn") + " : " + getKimlikno() + "");
			} 
			if(getCinsiyet() != 0) {
				value.append(KeyUtil.getLabelValue("insan.cinsiyet") + " : " + getCinsiyet() + "");
			} 
		return value.toString(); 
	} 
	
	
	
}
