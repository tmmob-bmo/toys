package tr.com.arf.toys.db.model.system; 
 
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tr.com.arf.framework.db.model._base.BaseTreeEntity;
import tr.com.arf.framework.utility.tool.KeyUtil;
import tr.com.arf.toys.db.model.base.ToysBaseTreeEntity;
 
 
@Entity 
@Table(name = "MENUITEM") 
public class Menuitem extends ToysBaseTreeEntity { 
 
	private static final long serialVersionUID = 1L; 
 
	@Id 
	@GeneratedValue(strategy = GenerationType.IDENTITY) 
	@Column(updatable = false, unique = true, nullable = false, name = "RID") 
	private Long rID; 
 
	@ManyToOne 
	@JoinColumn(name = "USTREF") 
	private Menuitem ustRef; 
 
	@Column(name = "AD") 
	private String ad; 
 
	@Column(name = "ERISIMKODU") 
	private String erisimKodu; 
	
	@Column(name = "URL") 
	private String url; 
	
	@Column(name = "KOD") 
	private String kod; 
	 
	@Column(name = "TABLO")
	private String tablo; 
	
	public Menuitem() { 
		super(); 
	} 
 
	@Override
	public Long getRID() { 
		return this.rID;  
	}  
	  
	@Override
	public void setRID(Long rID) {  
		this.rID = rID;  
	}   
 
	@Override
	public Menuitem getUstRef() { 
		return ustRef; 
	} 
	 
	@Override
	public void setUstRef(BaseTreeEntity ustRef) {
		this.ustRef = (Menuitem) ustRef;
	} 
 
	public void setUstRef(Menuitem ustRef) { 
		this.ustRef = ustRef;
	} 

	@Override
	public String getAd() { 
		return this.ad; 
	} 
	 
	@Override
	public void setAd(String ad) { 
		this.ad = ad; 
	} 
 
	@Override
	public String getErisimKodu() { 
		return this.erisimKodu; 
	} 
	 
	@Override
	public void setErisimKodu(String erisimKodu) { 
		this.erisimKodu = erisimKodu; 
	} 
     
	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getKod() {
		return kod;
	}
	
	public String getEntityName() {
		return kod.replace("menu_", "");
	}

	public void setKod(String kod) {
		this.kod = kod;
	}

	public String getTablo() {
		return tablo;
	}

	public void setTablo(String tablo) {
		this.tablo = tablo;
	}
	
	@Override 
	public String toString() { 
		return "tr.com.arf.toys.db.model.system.Menuitem[id=" + this.getRID()  + "]"; 
	} 
	
	@Override 
	public String getUIString() { 
		String result = getAd() + ""; 
		if (getUstRef() != null) { 
			result += " / " + getUstRef().getAd() + "" ; 
		} 
		return result; 
	} 
	 
	@Override 
	public void initValues(boolean defaultValues) { 
		if (defaultValues) { 
				// TODO Initialize with default values 
		} else { 
			this.rID = null; 			this.ustRef = null; 			this.ad = ""; 			this.erisimKodu = ""; 
			this.url = "/_page/";
			this.kod = ""; 
			this.tablo = null; 		}  
	}  
	 
	@Override 
	public String getValue() {  
		StringBuilder value = new StringBuilder(""); 
			if(getUstRef() != null) {
				value.append(KeyUtil.getLabelValue("menuitem.ustRef") + " : " + getUstRef().getUIString() + "<br />");
			} 
			if(getAd() != null) {
				value.append(KeyUtil.getLabelValue("menuitem.ad") + " : " + getAd() + "<br />");
			} 
			if(getUrl() != null) {
				value.append(KeyUtil.getLabelValue("menuitem.url") + " : " + getUrl() + "<br />");
			} 
			if(getKod() != null) {
				value.append(KeyUtil.getLabelValue("menuitem.kod") + " : " + getKod() + "<br />");
			} 
			if(getTablo() != null) {
				value.append(KeyUtil.getLabelValue("menuitem.tablo") + " : " + getTablo() + "<br />");
			} 
			if(getErisimKodu() != null) {
				value.append(KeyUtil.getLabelValue("menuitem.erisimKodu") + " : " + getErisimKodu() + "");
			} 
		return value.toString(); 
	} 
	 
}	 
