package tr.com.arf.toys.db.model.iletisim; 
 
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import tr.com.arf.framework.db.enumerated._common.EvetHayir;
import tr.com.arf.framework.utility.tool.DateUtil;
import tr.com.arf.framework.utility.tool.KeyUtil;
import tr.com.arf.toys.db.enumerated.iletisim.GonderimDurum;
import tr.com.arf.toys.db.enumerated.iletisim.GonderimTuru;
import tr.com.arf.toys.db.model.base.ToysBaseEntity;
import tr.com.arf.toys.db.model.system.Birim;
 
@Entity 
@Table(name = "GONDERIM") 
public class Gonderim extends ToysBaseEntity { 
 
	private static final long serialVersionUID = 1L; 
 
	@Id 
	@GeneratedValue(strategy = GenerationType.IDENTITY) 
	@Column(updatable = false, unique = true, nullable = false, name = "RID") 
	private Long rID; 
 
	@ManyToOne 
	@JoinColumn(name = "ILETISIMLISTEREF") 
	private Iletisimliste iletisimlisteRef; 
 
	@ManyToOne 
	@JoinColumn(name = "ICERIKREF") 
	private Icerik icerikRef; 
 
	@Enumerated(EnumType.ORDINAL) 
	@Column(name = "GONDERIMTURU") 
	private GonderimTuru gonderimturu; 
 
	@Temporal(TemporalType.DATE) 
	@Column(name = "GONDERIMZAMANI") 
	private java.util.Date gonderimzamani; 
 
	@Enumerated(EnumType.ORDINAL) 
	@Column(name = "ZAMANLAMALI") 
	private EvetHayir zamanlamali; 
 
	@Enumerated(EnumType.ORDINAL) 
	@Column(name = "DURUM") 
	private GonderimDurum durum; 
 
	@ManyToOne 
	@JoinColumn(name = "BIRIMREF") 
	private Birim birimRef; 
	
	@Column(name = "ACIKLAMA") 
	private String aciklama;
	
	@Column(name = "KONU") 
	private String konu;
 
 
	public Gonderim() { 
		super(); 
	} 
 
	@Override
	public Long getRID() { 
		return this.rID;  
	}  
	  
	@Override
	public void setRID(Long rID) {  
		this.rID = rID;  
	}   
	 
	public Iletisimliste getIletisimlisteRef() { 
		return iletisimlisteRef; 
	} 
	 
	public void setIletisimlisteRef(Iletisimliste iletisimlisteRef) { 
		this.iletisimlisteRef = iletisimlisteRef; 
	}
	public Icerik getIcerikRef() { 
		return icerikRef; 
	} 
	 
	public void setIcerikRef(Icerik icerikRef) { 
		this.icerikRef = icerikRef; 
	} 
 
	public GonderimTuru getGonderimturu() { 
		return this.gonderimturu;  
	}  
  
	public void setGonderimturu(GonderimTuru gonderimturu) {  
		this.gonderimturu = gonderimturu;  
	}   
 
	public java.util.Date getGonderimzamani() { 
		return gonderimzamani; 
	} 
	 
	public void setGonderimzamani(java.util.Date gonderimzamani) { 
		this.gonderimzamani = gonderimzamani; 
	} 
 
	public EvetHayir getZamanlamali() { 
		return this.zamanlamali;  
	}  
  
	public void setZamanlamali(EvetHayir zamanlamali) {  
		this.zamanlamali = zamanlamali;  
	}   
 
	public GonderimDurum getDurum() { 
		return this.durum;  
	}  
  
	public void setDurum(GonderimDurum durum) {  
		this.durum = durum;  
	}   
 
	public String getAciklama() { 
		return this.aciklama; 
	} 
	 
	public void setAciklama(String aciklama) { 
		this.aciklama = aciklama; 
	}
 
	public Birim getBirimRef() {
		return birimRef;
	}

	public void setBirimRef(Birim birimRef) {
		this.birimRef = birimRef;
	}

	public String getKonu() {
		return konu;
	}

	public void setKonu(String konu) {
		this.konu = konu;
	}

	@Override 
	public String toString() { 
		return "tr.com.arf.toys.db.model.iletisim.Gonderim[id=" + this.getRID()  + "]"; 
	} 
	@Override 
	public String getUIString() { 
		return getIletisimlisteRef() + ""; 
	} 
	 
	@Override 
	public void initValues(boolean defaultValues) { 
			this.rID = null; 			this.iletisimlisteRef = null; 			this.icerikRef = null; 			this.gonderimturu = GonderimTuru._NULL; 			this.gonderimzamani = null; 			this.zamanlamali = EvetHayir._NULL; 			this.durum = GonderimDurum._NULL; 			this.aciklama = ""; 
			this.birimRef=null;
			this.konu=null;	}  
	 
	@Override 
	public String getValue() {  
		StringBuilder value = new StringBuilder("Veri : <br />"); 
			if(getIletisimlisteRef() != null) {
				value.append(KeyUtil.getLabelValue("gonderim.iletisimlisteRef") + " : " + getIletisimlisteRef().getUIString() + "<br />");
			} 
			if(getIcerikRef() != null) {
				value.append(KeyUtil.getLabelValue("gonderim.icerikRef") + " : " + getIcerikRef().getUIString() + "<br />");
			} 
			if(getGonderimturu() != null) {
				value.append(KeyUtil.getLabelValue("gonderim.gonderimturu") + " : " + getGonderimturu().getLabel() + "<br />");
			} 
			if(getGonderimzamani() != null) {
				value.append(KeyUtil.getLabelValue("gonderim.gonderimzamani") + " : " + DateUtil.dateToDMYHMS(getGonderimzamani()) + "<br />");
			} 
			if(getZamanlamali() != null) {
				value.append(KeyUtil.getLabelValue("gonderim.zamanlamali") + " : " + getZamanlamali().getLabel() + "<br />");
			} 
			if(getDurum() != null) {
				value.append(KeyUtil.getLabelValue("gonderim.durum") + " : " + getDurum().getLabel() + "<br />");
			} 
			if(getBirimRef() != null) {
				value.append(KeyUtil.getLabelValue("gonderim.birimRef") + " : " + getBirimRef().getUIString() + "");
			} 
			if(getAciklama() != null) {
				value.append(KeyUtil.getLabelValue("gonderim.aciklama") + " : " + getAciklama() + "");
			}
			if(getKonu() != null) {
				value.append(KeyUtil.getLabelValue("gonderim.konu") + " : " + getKonu() + "");
			}
		return value.toString(); 
	} 
	 
}	 
