package tr.com.arf.toys.db.model.evrak; 
 
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tr.com.arf.framework.utility.tool.KeyUtil;
import tr.com.arf.toys.db.enumerated.evrak.EvrakHareketTuru;
import tr.com.arf.toys.db.model.base.ToysBaseEntity;
import tr.com.arf.toys.db.model.system.Birim;
 
@Entity 
@Table(name = "DOLASIMSABLON") 
public class Dolasimsablon extends ToysBaseEntity { 
 
	private static final long serialVersionUID = 1L; 
 
	@Id 
	@GeneratedValue(strategy = GenerationType.IDENTITY) 
	@Column(updatable = false, unique = true, nullable = false, name = "RID") 
	private Long rID; 
 
	@Enumerated(EnumType.ORDINAL) 
	@Column(name = "EVRAKHAREKETTURU") 
	private EvrakHareketTuru evrakHareketTuru; 
  
	@ManyToOne 
	@JoinColumn(name = "BIRIMREF") 
	private Birim birimRef; 
	

	@Column(name = "AD") 
	private String ad; 
 
 
	public Dolasimsablon() { 
		super(); 
	} 
 
	@Override
	public Long getRID() { 
		return this.rID;  
	}  
	  
	@Override
	public void setRID(Long rID) {  
		this.rID = rID;  
	}    
 
	public EvrakHareketTuru getEvrakHareketTuru() {
		return evrakHareketTuru;
	}

	public void setEvrakHareketTuru(EvrakHareketTuru evrakHareketTuru) {
		this.evrakHareketTuru = evrakHareketTuru;
	}
	
	public String getAd() { 
		return this.ad; 
	} 
	 
	public void setAd(String ad) { 
		this.ad = ad; 
	}  
	
	public Birim getBirimRef() { 
		return birimRef; 
	} 
	 
	public void setBirimRef(Birim birimRef) { 
		this.birimRef = birimRef; 
	} 
 
	@Override 
	public String toString() { 
		return "tr.com.arf.toys.db.model.evrak.Dolasimsablon[id=" + this.getRID()  + "]"; 
	} 
	
	@Override 
	public String getUIString() { 
		return getAd() + ""; 
	} 
	 
	@Override 
	public void initValues(boolean defaultValues) { 
		this.rID = null; 		this.evrakHareketTuru = EvrakHareketTuru._NULL; 		this.ad = ""; 
		this.birimRef = null; 	}  
	 
	@Override 
	public String getValue() {  
		StringBuilder value = new StringBuilder("Veri : <br />"); 
			if(getEvrakHareketTuru() != null) {
				value.append(KeyUtil.getLabelValue("dolasimsablon.evrakHareketTuru") + " : " + getEvrakHareketTuru().getLabel() + "<br />");
			} 
			if(getAd() != null) {
				value.append(KeyUtil.getLabelValue("dolasimsablon.ad") + " : " + getAd() + "");
			} 
			if(getBirimRef() != null) {
				value.append(KeyUtil.getLabelValue("dolasimsablondetay.birimRef") + " : " + getBirimRef().getUIString() + "<br />");
			} 
			if(getEvrakHareketTuru() != null) {
				value.append(KeyUtil.getLabelValue("evrak.evrakHareketTuru") + " : " + getEvrakHareketTuru().getLabel() + "<br />");
			}  
		return value.toString(); 
	} 
	 
}	 
