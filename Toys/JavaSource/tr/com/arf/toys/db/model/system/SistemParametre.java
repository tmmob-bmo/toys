package tr.com.arf.toys.db.model.system;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import tr.com.arf.framework.db.enumerated._common.EvetHayir;
import tr.com.arf.framework.utility.tool.KeyUtil;
import tr.com.arf.toys.db.model.base.ToysBaseEntity;

@Entity
@Table(name = "SISTEMPARAMETRE")
public class SistemParametre extends ToysBaseEntity {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(updatable = false, unique = true, nullable = false, name = "RID")
	private Long rID;

	@Column(name = "LISTEBOYUTU")
	private String listeboyutu;

	@Column(name = "SMSUZUNLUK")
	private String smsuzunluk;

	@Column(name = "ODAADI")
	private String odaadi;

	@Column(name = "ODADETAYI")
	private String odadetayi;

	@Column(name = "ODAWEBADRESI")
	private String odawebadresi;

	@Column(name = "LOGODEGERI")
	private String logodegeri;

	@Enumerated(EnumType.ORDINAL)
	@Column(name = "KPSZORLA")
	private EvetHayir kpszorla;

	@Column(name = "MAILADRESI")
	private String mailadresi;

	@Column(name = "ASGARIAYLIKNETUCRET")
	private String asgariAylikNetUcret;

	@Column(name = "YABANCIUYEKAYITUCRETI")
	private String yabanciUyeKayitUcreti;

	@Column(name = "YABANCIUYEAIDATUCRETI")
	private String yabanciUyeAidatUcreti;

	@Column(name = "HESAPADI")
	private String hesapAdi;

	@Column(name = "BANKAADI")
	private String bankaAdi;

	@Column(name = "SUBEADI")
	private String subeAdi;

	@Column(name = "IBANNO")
	private String ibanNo;

	@Column(name = "BLRKSLKHIZMETBEDELI")
	private String bilirkisiHizmetBedeli;

	@Column(name = "BLRKSLKRAPORBEDELI")
	private String bilirkisiRaporBedeli;

	@Column(name = "HESAPNO")
	private String hesapNo;

	@Column(name = "BLRKSLKTPLMHZMBEDELI")
	private String bilirkisilikToplamHizmetBedeli;

	@Column(name = "SISTEMMAIL")
	private String sistemmail;

	@Column(name = "YAPIKREDIPOSADRES")
	private String yapikrediposadres;

	@Column(name = "ISBANKASIPOSADRES")
	private String isbankasiposadres;

	@Column(name = "GARANTIPOSADRES")
	private String garantiposadres;

	@Enumerated(EnumType.ORDINAL)
	@Column(name = "GARANTIAKTIF")
	private EvetHayir garantiaktif;

	@Enumerated(EnumType.ORDINAL)
	@Column(name = "ISBANKASIAKTIF")
	private EvetHayir isbankasiaktif;

	@Enumerated(EnumType.ORDINAL)
	@Column(name = "YAPIKREDIAKTIF")
	private EvetHayir yapikrediaktif;

	@Column(name = "UYEBILDIRIMEPOSTASONU")
	private String uyeBildirimEpostaSonu;

	@Column(name = "SISTEMERISIMADRESI")
	private String sistemErisimAdresi;


	public SistemParametre() {
		super();
	}

	@Override
	public Long getRID() {
		return this.rID;
	}

	@Override
	public void setRID(Long rID) {
		this.rID = rID;
	}

	public String getListeboyutu() {
		return listeboyutu;
	}

	public void setListeboyutu(String listeboyutu) {
		this.listeboyutu = listeboyutu;
	}

	public String getSmsuzunluk() {
		return smsuzunluk;
	}

	public void setSmsuzunluk(String smsuzunluk) {
		this.smsuzunluk = smsuzunluk;
	}

	public String getOdaadi() {
		return odaadi;
	}

	public void setOdaadi(String odaadi) {
		this.odaadi = odaadi;
	}

	public String getOdadetayi() {
		return odadetayi;
	}

	public void setOdadetayi(String odadetayi) {
		this.odadetayi = odadetayi;
	}

	public String getOdawebadresi() {
		return odawebadresi;
	}

	public void setOdawebadresi(String odawebadresi) {
		this.odawebadresi = odawebadresi;
	}

	public String getLogodegeri() {
		return logodegeri;
	}

	public void setLogodegeri(String logodegeri) {
		this.logodegeri = logodegeri;
	}

	public EvetHayir getKpszorla() {
		return kpszorla;
	}

	public void setKpszorla(EvetHayir kpszorla) {
		this.kpszorla = kpszorla;
	}

	public String getMailadresi() {
		return mailadresi;
	}

	public void setMailadresi(String mailadresi) {
		this.mailadresi = mailadresi;
	}

	public String getAsgariAylikNetUcret() {
		return asgariAylikNetUcret;
	}

	public void setAsgariAylikNetUcret(String asgariAylikNetUcret) {
		this.asgariAylikNetUcret = asgariAylikNetUcret;
	}

	public String getYabanciUyeKayitUcreti() {
		return yabanciUyeKayitUcreti;
	}

	public void setYabanciUyeKayitUcreti(String yabanciUyeKayitUcreti) {
		this.yabanciUyeKayitUcreti = yabanciUyeKayitUcreti;
	}

	public String getYabanciUyeAidatUcreti() {
		return yabanciUyeAidatUcreti;
	}

	public void setYabanciUyeAidatUcreti(String yabanciUyeAidatUcreti) {
		this.yabanciUyeAidatUcreti = yabanciUyeAidatUcreti;
	}

	public String getHesapAdi() {
		return hesapAdi;
	}

	public void setHesapAdi(String hesapAdi) {
		this.hesapAdi = hesapAdi;
	}

	public String getBankaAdi() {
		return bankaAdi;
	}

	public void setBankaAdi(String bankaAdi) {
		this.bankaAdi = bankaAdi;
	}

	public String getSubeAdi() {
		return subeAdi;
	}

	public void setSubeAdi(String subeAdi) {
		this.subeAdi = subeAdi;
	}

	public String getIbanNo() {
		return ibanNo;
	}

	public void setIbanNo(String ibanNo) {
		this.ibanNo = ibanNo;
	}

	public String getBilirkisiHizmetBedeli() {
		return bilirkisiHizmetBedeli;
	}

	public void setBilirkisiHizmetBedeli(String bilirkisiHizmetBedeli) {
		this.bilirkisiHizmetBedeli = bilirkisiHizmetBedeli;
	}

	public String getBilirkisiRaporBedeli() {
		return bilirkisiRaporBedeli;
	}

	public void setBilirkisiRaporBedeli(String bilirkisiRaporBedeli) {
		this.bilirkisiRaporBedeli = bilirkisiRaporBedeli;
	}

	public String getHesapNo() {
		return hesapNo;
	}

	public void setHesapNo(String hesapNo) {
		this.hesapNo = hesapNo;
	}

	public String getBilirkisilikToplamHizmetBedeli() {
		return bilirkisilikToplamHizmetBedeli;
	}

	public void setBilirkisilikToplamHizmetBedeli(String bilirkisilikToplamHizmetBedeli) {
		this.bilirkisilikToplamHizmetBedeli = bilirkisilikToplamHizmetBedeli;
	}

	public String getSistemmail() {
		return sistemmail;
	}

	public void setSistemmail(String sistemmail) {
		this.sistemmail = sistemmail;
	}

	public String getYapikrediposadres() {
		return yapikrediposadres;
	}

	public void setYapikrediposadres(String yapikrediposadres) {
		this.yapikrediposadres = yapikrediposadres;
	}

	public String getIsbankasiposadres() {
		return isbankasiposadres;
	}

	public void setIsbankasiposadres(String isbankasiposadres) {
		this.isbankasiposadres = isbankasiposadres;
	}

	public EvetHayir getIsbankasiaktif() {
		return isbankasiaktif;
	}

	public void setIsbankasiaktif(EvetHayir isbankasiaktif) {
		this.isbankasiaktif = isbankasiaktif;
	}

	public String getGarantiposadres() {
		return garantiposadres;
	}

	public void setGarantiposadres(String garantiposadres) {
		this.garantiposadres = garantiposadres;
	}

	public EvetHayir getGarantiaktif() {
		return garantiaktif;
	}

	public void setGarantiaktif(EvetHayir garantiaktif) {
		this.garantiaktif = garantiaktif;
	}

	public EvetHayir getYapikrediaktif() {
		return yapikrediaktif;
	}

	public void setYapikrediaktif(EvetHayir yapikrediaktif) {
		this.yapikrediaktif = yapikrediaktif;
	}

	public String getUyeBildirimEpostaSonu() {
		return uyeBildirimEpostaSonu;
	}

	public void setUyeBildirimEpostaSonu(String uyeBildirimEpostaSonu) {
		this.uyeBildirimEpostaSonu = uyeBildirimEpostaSonu;
	}

	public String getSistemErisimAdresi() {
		return sistemErisimAdresi;
	}

	public void setSistemErisimAdresi(String sistemErisimAdresi) {
		this.sistemErisimAdresi = sistemErisimAdresi;
	}

	@Override
	public String getUIString() {
		return "";
	}

	@Override
	public void initValues(boolean defaultValues) {
		if (defaultValues) {
			// TODO Initialize with default values
		} else {
			this.rID = null;
			this.listeboyutu = null;
			this.smsuzunluk = null;
			this.odaadi = null;
			this.odadetayi = null;
			this.odawebadresi = null;
			this.logodegeri = null;
			this.kpszorla = EvetHayir._NULL;
			this.mailadresi = null;
			this.asgariAylikNetUcret = null;
			this.yabanciUyeKayitUcreti = null;
			this.yabanciUyeAidatUcreti = null;
			this.hesapAdi = null;
			this.bankaAdi = null;
			this.subeAdi = null;
			this.ibanNo = null;
			this.bilirkisiHizmetBedeli = null;
			this.bilirkisiRaporBedeli = null;
			this.hesapNo = null;
			this.bilirkisilikToplamHizmetBedeli = null;
			this.sistemmail = null;
			this.yapikrediposadres = null;
			this.isbankasiposadres = null;
			this.garantiposadres = null;
			this.garantiaktif = EvetHayir._NULL;
			this.isbankasiaktif = EvetHayir._NULL;
			this.yapikrediaktif = EvetHayir._NULL;
			this.uyeBildirimEpostaSonu = null;
			this.sistemErisimAdresi = "http://localhost:8082";
		}
	}

	@Override
	public String getValue() {
		StringBuilder value = new StringBuilder("");
		if (getListeboyutu() != null) {
			value.append(KeyUtil.getLabelValue("sistemParametre.listeboyutu") + " : " + getListeboyutu() + "<br />");
		}
		if (getSmsuzunluk() != null) {
			value.append(KeyUtil.getLabelValue("sistemParametre.smsuzunluk") + " : " + getSmsuzunluk() + "<br />");
		}
		if (getOdaadi() != null) {
			value.append(KeyUtil.getLabelValue("sistemParametre.odaadi") + " : " + getOdaadi() + "<br />");
		}
		if (getOdadetayi() != null) {
			value.append(KeyUtil.getLabelValue("sistemParametre.odadetayi") + " : " + getOdadetayi() + "<br />");
		}
		if (getOdawebadresi() != null) {
			value.append(KeyUtil.getLabelValue("sistemParametre.odawebadresi") + " : " + getOdawebadresi() + "<br />");
		}
		if (getLogodegeri() != null) {
			value.append(KeyUtil.getLabelValue("sistemParametre.logodegeri") + " : " + getLogodegeri() + "<br />");
		}
		if (getKpszorla() != null) {
			value.append(KeyUtil.getLabelValue("sistemParametre.kpszorla") + " : " + getKpszorla().getLabel() + "<br />");
		}
		if (getMailadresi() != null) {
			value.append(KeyUtil.getLabelValue("sistemParametre.mailadresi") + " : " + getMailadresi() + "");
		}
		if (getAsgariAylikNetUcret() != null) {
			value.append(KeyUtil.getLabelValue("sistemParametre.asgariAylikNetUcret") + " : " + getAsgariAylikNetUcret() + "<br />");
		}
		if (getYabanciUyeKayitUcreti() != null) {
			value.append(KeyUtil.getLabelValue("sistemParametre.yabanciUyeKayitUcreti") + " : " + getYabanciUyeKayitUcreti() + "<br />");
		}
		if (getYabanciUyeAidatUcreti() != null) {
			value.append(KeyUtil.getLabelValue("sistemParametre.yabanciUyeAidatUcreti") + " : " + getYabanciUyeAidatUcreti() + "<br />");
		}
		if (getHesapAdi() != null) {
			value.append(KeyUtil.getLabelValue("sistemParametre.hesapAdi") + " : " + getHesapAdi() + "<br />");
		}
		if (getBankaAdi() != null) {
			value.append(KeyUtil.getLabelValue("sistemParametre.bankaAdi") + " : " + getBankaAdi() + "<br />");
		}
		if (getSubeAdi() != null) {
			value.append(KeyUtil.getLabelValue("sistemParametre.subeAdi") + " : " + getSubeAdi() + "<br />");
		}
		if (getIbanNo() != null) {
			value.append(KeyUtil.getLabelValue("sistemParametre.ibanNo") + " : " + getIbanNo() + "<br />");
		}
		if (getBilirkisiHizmetBedeli() != null) {
			value.append(KeyUtil.getLabelValue("sistemParametre.bilirkisiHizmetBedeli") + " : " + getBilirkisiHizmetBedeli() + "<br />");
		}
		if (getBilirkisiRaporBedeli() != null) {
			value.append(KeyUtil.getLabelValue("sistemParametre.bilirkisiRaporBedeli") + " : " + getBilirkisiRaporBedeli() + "<br />");
		}
		if (getHesapNo() != null) {
			value.append(KeyUtil.getLabelValue("sistemParametre.hesapNo") + " : " + getHesapNo() + "<br />");
		}
		if (getBilirkisilikToplamHizmetBedeli() != null) {
			value.append(KeyUtil.getLabelValue("sistemParametre.bilirkisilikToplamHizmetBedeli") + " : " + getBilirkisilikToplamHizmetBedeli() + "<br />");
		}
		if (getSistemmail() != null) {
			value.append(KeyUtil.getLabelValue("sistemParametre.sistemmail") + " : " + getSistemmail() + "");
		}
		if (getIsbankasiposadres() != null) {
			value.append(KeyUtil.getLabelValue("sistemParametre.isbankasiposadres") + " : " + getIsbankasiposadres() + "");
		}
		if (getYapikrediposadres() != null) {
			value.append(KeyUtil.getLabelValue("sistemParametre.yapikrediposadres") + " : " + getYapikrediposadres() + "");
		}
		if (getGarantiposadres() != null) {
			value.append(KeyUtil.getLabelValue("sistemParametre.garantiposadres") + " : " + getGarantiposadres() + "");
		}
		if (getGarantiaktif() != null) {
			value.append(KeyUtil.getLabelValue("sistemParametre.garantiaktif") + " : " + getGarantiaktif().getLabel() + "<br />");
		}
		if (getYapikrediaktif() != null) {
			value.append(KeyUtil.getLabelValue("sistemParametre.yapikrediaktif") + " : " + getYapikrediaktif().getLabel() + "<br />");
		}
		if (getIsbankasiaktif() != null) {
			value.append(KeyUtil.getLabelValue("sistemParametre.isbankasiaktif") + " : " + getIsbankasiaktif().getLabel() + "<br />");
		}
		if (getUyeBildirimEpostaSonu() != null) {
			value.append(KeyUtil.getLabelValue("sistemparametre.uyeBildirimEpostaSonu") + " : " + getUyeBildirimEpostaSonu() + "");
		}
		if (getSistemErisimAdresi() != null) {
			value.append(KeyUtil.getLabelValue("sistemparametre.sistemErisimAdresi") + " : " + getSistemErisimAdresi() + "");
		}

		return value.toString();
	}

}
