package tr.com.arf.toys.db.model.kisi; 
 
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import tr.com.arf.framework.db.enumerated._common.EvetHayir;
import tr.com.arf.framework.utility.tool.KeyUtil;
import tr.com.arf.toys.db.enumerated.kisi.KomisyonGorev;
import tr.com.arf.toys.db.enumerated.kisi.KomisyonUyelikDurumu;
import tr.com.arf.toys.db.enumerated.kisi.UyeTuru;
import tr.com.arf.toys.db.model.base.ToysBaseEntity;
import tr.com.arf.toys.db.model.uye.Uye;
 
@Entity 
@Table(name = "KOMISYONDONEMUYEDURUM") 
public class KomisyonDonemUyeDurum extends ToysBaseEntity { 
 
	private static final long serialVersionUID = 1L; 
 
	@Id 
	@GeneratedValue(strategy = GenerationType.IDENTITY) 
	@Column(updatable = false, unique = true, nullable = false, name = "RID") 
	private Long rID; 
 
	@ManyToOne 
	@JoinColumn(name = "KOMISYONDONEMREF") 
	private KomisyonDonem komisyonDonemRef;
	
	@ManyToOne 
	@JoinColumn(name = "UYEREF") 
	private Uye uyeRef;
	
	@Enumerated(EnumType.ORDINAL)
	@Column(name = "DURUM") 
	private EvetHayir durum; 
	
	@Enumerated(EnumType.ORDINAL)
	@Column(name = "KOMISYONGOREV") 
	private KomisyonGorev komisyonGorev; 
	
	@Enumerated(EnumType.ORDINAL)
	@Column(name = "UYETURU") 
	private UyeTuru uyeTuru; 
	
	@ManyToOne 
	@JoinColumn(name = "KISIREF") 
	private Kisi kisiRef;

	@Enumerated(EnumType.ORDINAL)
	@Column(name = "KOMISYONUYELIKDURUMU") 
	private KomisyonUyelikDurumu komisyonUyelikDurumu;
	
	@Temporal(TemporalType.DATE) 
	@Column(name = "DURUMTARIHI") 
	private java.util.Date durumtarihi; 
	
	public KomisyonDonemUyeDurum() { 
		super(); 
	} 
 
	@Override
	public Long getRID() { 
		return this.rID;  
	}  
	  
	@Override
	public void setRID(Long rID) {  
		this.rID = rID;  
	}   
	
	public Uye getUyeRef() {
		return uyeRef;
	}

	public void setUyeRef(Uye uyeRef) {
		this.uyeRef = uyeRef;
	}

	public KomisyonDonem getKomisyonDonemRef() {
		return komisyonDonemRef;
	}

	public void setKomisyonDonemRef(KomisyonDonem komisyonDonemRef) {
		this.komisyonDonemRef = komisyonDonemRef;
	}
	
	public EvetHayir getDurum() {
		return durum;
	}

	public void setDurum(EvetHayir durum) {
		this.durum = durum;
	}	

	public KomisyonGorev getKomisyonGorev() {
		return komisyonGorev;
	}

	public void setKomisyonGorev(KomisyonGorev komisyonGorev) {
		this.komisyonGorev = komisyonGorev;
	}	

	public UyeTuru getUyeTuru() {
		return uyeTuru;
	}

	public void setUyeTuru(UyeTuru uyeTuru) {
		this.uyeTuru = uyeTuru;
	}
	
	public Kisi getKisiRef() {
		return kisiRef;
	}

	public void setKisiRef(Kisi kisiRef) {
		this.kisiRef = kisiRef;
	}
	
	public KomisyonUyelikDurumu getKomisyonUyelikDurumu() {
		return komisyonUyelikDurumu;
	}

	public void setKomisyonUyelikDurumu(KomisyonUyelikDurumu komisyonUyelikDurumu) {
		this.komisyonUyelikDurumu = komisyonUyelikDurumu;
	}

	public java.util.Date getDurumtarihi() {
		return durumtarihi;
	}

	public void setDurumtarihi(java.util.Date durumtarihi) {
		this.durumtarihi = durumtarihi;
	}

	public String getKomisyonDonemUyeText(){
		if(getUyeRef() != null){
			return getUyeRef().getUIString();
		} else {
			return getKisiRef().getUIStringShort() + " (" + KeyUtil.getLabelValue("komisyonDonemUyeDurum.uyeDegil") + ")";
		}
	}

	@Override 
	public String toString() { 
		return "tr.com.arf.toys.db.model.kisi.KomisyonDonemUyeDurum[id=" + this.getRID()  + "]"; 
	} 
	
	
	@Override 
	public String getUIString() { 
		return komisyonDonemRef.getKomisyonRef().getAd()+ " - " + getKomisyonDonemUyeText(); 
	} 
	 
	@Override 
	public void initValues(boolean defaultValues) { 
		if (defaultValues) { 
				// TODO Initialize with default values 
		} else { 
			this.rID = null; 			this.durum=EvetHayir._NULL;
			this.uyeRef=null;
			this.komisyonDonemRef=null;
			this.komisyonGorev = KomisyonGorev._NULL;
			this.uyeTuru = UyeTuru._NULL;
			this.kisiRef = null;
			this.durumtarihi=null;
			this.komisyonUyelikDurumu=KomisyonUyelikDurumu._NULL;		}  
	}  
	 
	@Override 
	public String getValue() {  
		StringBuilder value = new StringBuilder("Veri : <br />"); 
			if(getKomisyonDonemRef() != null) {
				value.append(KeyUtil.getLabelValue("komisyonDonemUyeDurum.komisyonDonemRef") + " : " + getKomisyonDonemRef().getUIString() + "<br />");
			} 
			if(getUyeRef() != null) {
				value.append(KeyUtil.getLabelValue("komisyonDonemUyeDurum.uyeRef") + " : " + getUyeRef().getUIString() + "<br />");
			} 
			if(getKisiRef() != null) {
				value.append(KeyUtil.getLabelValue("komisyonDonemUyeDurum.kisiRef") + " : " + getKisiRef().getUIString() + "<br />");
			} 
			if(getKomisyonGorev() != null) {
				value.append(KeyUtil.getLabelValue("komisyonDonemUyeDurum.komisyonGorev") + " : " + getKomisyonGorev().getLabel() + "");
			} 
			if(getUyeTuru() != null) {
				value.append(KeyUtil.getLabelValue("komisyonDonemUyeDurum.uyeTuru") + " : " + getUyeTuru().getLabel() + "");
			} 
			if(getDurum() != null) {
				value.append(KeyUtil.getLabelValue("komisyonDonemUyeDurum.durum") + " : " + getDurum().getLabel() + "");
			}
			if(getKomisyonUyelikDurumu() != null) {
				value.append(KeyUtil.getLabelValue("komisyonDonemUyeDurum.komisyonUyelikDurumu") + " : " + getKomisyonUyelikDurumu().getLabel() + "");
			} 
			if(getDurumtarihi() != null) {
				value.append(KeyUtil.getLabelValue("komisyonDonemUyeDurum.durumtarihi") + " : " + getDurumtarihi() + "");
			}
		return value.toString(); 
	} 
	 
}	