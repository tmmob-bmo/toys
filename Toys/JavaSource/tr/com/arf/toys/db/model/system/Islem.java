package tr.com.arf.toys.db.model.system; 
 
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import tr.com.arf.framework.db.enumerated._common.EvetHayir;
import tr.com.arf.framework.utility.tool.KeyUtil;
import tr.com.arf.toys.db.model.base.ToysBaseEntity;
 
@Entity 
@Table(name = "ISLEM") 
public class Islem extends ToysBaseEntity { 
 
	private static final long serialVersionUID = 1L; 
  
	@Id 
	@GeneratedValue(strategy = GenerationType.IDENTITY) 
	@Column(updatable = false, unique = true, nullable = false, name = "RID") 
	private Long rID; 
 
	@Column(name = "NAME") 
	private String name; 
 
	@Enumerated(EnumType.ORDINAL)
	@Column(name = "ACTIVE") 
	private EvetHayir active; 
 
 
	public Islem() { 
		super(); 
	} 
 
	@Override
	public Long getRID() { 
		return this.rID;  
	}  
	  
	@Override
	public void setRID(Long rID) {  
		this.rID = rID;  
	}   
 
	public String getName() { 
		return name; 
	} 
	 
	public void setName(String name) { 
		this.name = letterCase(name); 
	} 
   
	public EvetHayir getActive() {
		return active;
	}

	public void setActive(EvetHayir active) {
		this.active = active;
	}

	@Override 
	public String toString() { 
		return "tr.com.arf.toys.db.model.system.Islem[id=" + this.getRID() 
				+ "]"; 
	} 
	
	@Override 
	public String getUIString() { 
		return getName().toString(); 
	} 
	 
	@Override 
	public void initValues(boolean defaultValues) { 
		if (defaultValues) { 
				// TODO Initialize with default values 
		} else { 
			this.rID = null; 			this.name = ""; 			this.active = EvetHayir._NULL; 		}  
	}  
	 
	@Override 
	public String getValue() {  
		StringBuilder value = new StringBuilder(""); 
		if(getName() != null) 
			value.append(KeyUtil.getLabelValue("islem.name") + " : " + getName() + "<br />"); 
		if(getActive() != null) 
			value.append(KeyUtil.getLabelValue("islem.active") + " : " + getActive() + ""); 
		return value.toString(); 
	} 
	 
	 
}	 
