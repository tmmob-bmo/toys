package tr.com.arf.toys.db.model.system;
 
import static javax.persistence.GenerationType.IDENTITY;

import java.lang.reflect.Field;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import tr.com.arf.framework.db.enumerated._common.EvetHayir;
import tr.com.arf.toys.db.model.base.ToysBaseEntity;
 
@Entity 
@Table(name = "ROLE") 
public class Role extends ToysBaseEntity { 
 
	private static final long serialVersionUID = 1L; 
 
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(updatable = false, unique = true, nullable = false, name = "RID")
	private Long rID; 
 
	@Column(name = "NAME") 
	private String name;  
	
	@Enumerated(EnumType.ORDINAL)
	@Column(name = "ACTIVE") 
	private EvetHayir active; 
	
	public Role() { 
		super(); 
	} 
 
	@Override
	public Long getRID() { 
		return this.rID;  
	}  
	  
	@Override
	public void setRID(Long rID) {  
		this.rID = rID;  
	}   
 
	public String getName() { 
		return name; 
	} 
	 
	public void setName(String name) { 
		this.name = letterCase(name); 
	} 
	
	public EvetHayir getActive() { 
		return this.active; 
	} 
	 
	public void setActive(EvetHayir active) { 
		this.active = active; 
	}  
    
	@Override 
	public String toString() { 
		return "tr.com.arf.toys.db.model.system.Role[id=" + this.getRID() 
				+ "]"; 
	} 
	@Override 
	public String getUIString() { 
		return getName().toString(); 
	} 
	
	@SuppressWarnings("unchecked")
	public List<IslemRole> getIslemRoleList() {
		if(getRID() != null){  
			try {
				if(getDBOperator().recordCount(this.getClass().getSimpleName(), "o.roleRef.rID=" + getRID()) > 0){
					return getDBOperator().load(this.getClass().getSimpleName(), "o.roleRef.rID=" + getRID(), "o.rID");
				} else {
					return null;
				}
			} catch(Exception e){
				e.printStackTrace();
				return null;
			} 
		} else {
			return null;
		}
	}
	 
	@Override 
	public void initValues(boolean defaultValues) { 
		if (defaultValues) { 
				// TODO Initialize with default values 
		} else { 
			this.rID = null; 			this.name = ""; 			this.active = EvetHayir._NULL;		}  
	} 
	 
	@Override 
	public String getValue() {  
		StringBuilder value = new StringBuilder(""); 
		Field[] fields = this.getClass().getDeclaredFields(); 
		for (Field field : fields) {  
			if(!field.getName().equalsIgnoreCase("serialVersionUID")){ 
				value.append(field.getName() + " : "); 
				try {  
					value.append(field.get(this) + "<br />"); 
				} catch (IllegalArgumentException e) {  
					e.printStackTrace(); 
				} catch (IllegalAccessException e) {  
					e.printStackTrace(); 
				} 
			} 
		} 
		return value.toString(); 
	} 
	 
	 
}	 
