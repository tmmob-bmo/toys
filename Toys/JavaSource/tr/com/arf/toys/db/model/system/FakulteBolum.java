package tr.com.arf.toys.db.model.system; 
 
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tr.com.arf.framework.utility.tool.KeyUtil;
import tr.com.arf.toys.db.model.base.ToysBaseEntity;
 
 
@Entity 
@Table(name = "FAKULTEBOLUM") 
public class FakulteBolum extends ToysBaseEntity { 
 
	private static final long serialVersionUID = 1L; 
 
	@Id 
	@GeneratedValue(strategy = GenerationType.IDENTITY) 
	@Column(updatable = false, unique = true, nullable = false, name = "RID") 
	private Long rID; 
 
	@ManyToOne 
	@JoinColumn(name = "FAKULTEREF") 
	private Fakulte fakulteRef;
	
	@ManyToOne 
	@JoinColumn(name = "BOLUMREF") 
	private Bolum bolumRef; 
	
	
	public FakulteBolum() { 
		super(); 
	} 
 
	@Override
	public Long getRID() { 
		return this.rID;  
	}  
	  
	@Override
	public void setRID(Long rID) {  
		this.rID = rID;  
	} 

	public Fakulte getFakulteRef() {
		return fakulteRef;
	}

	public void setFakulteRef(Fakulte fakulteRef) {
		this.fakulteRef = fakulteRef;
	}

	public Bolum getBolumRef() {
		return bolumRef;
	}

	public void setBolumRef(Bolum bolumRef) {
		this.bolumRef = bolumRef;
	}

	@Override 
	public String toString() { 
		return "tr.com.arf.toys.db.model.system.FakulteBolum[id=" + this.getRID()  + "]"; 
	} 
	@Override 
	public String getUIString() { 
		return getBolumRef().getAd();
	} 
	 
	@Override 
	public void initValues(boolean defaultValues) { 
		if (defaultValues) { 
				// TODO Initialize with default values 
		} else { 
			this.rID = null; 			this.bolumRef = null; 
			this.fakulteRef=null;		}  
	}  
	 
	@Override 
	public String getValue() {  
		StringBuilder value = new StringBuilder(""); 
			if(getFakulteRef() != null) 
				value.append(KeyUtil.getLabelValue("fakultebolum.fakulteRef") + " : " + getFakulteRef().getUIString() + "<br />"); 
			if(getBolumRef() != null) 
				value.append(KeyUtil.getLabelValue("fakultebolum.bolumRef") + " : " + getBolumRef().getUIString() + "<br />"); 
		return value.toString(); 
	} 
	 
}	 
