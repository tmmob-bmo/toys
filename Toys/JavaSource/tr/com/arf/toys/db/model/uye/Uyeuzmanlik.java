package tr.com.arf.toys.db.model.uye; 
 
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tr.com.arf.framework.utility.tool.KeyUtil;
import tr.com.arf.toys.db.model.base.ToysBaseEntity;
import tr.com.arf.toys.db.model.kisi.Kisi;
import tr.com.arf.toys.db.model.system.Uzmanliktip;
 
 
@Entity 
@Table(name = "UYEUZMANLIK") 
public class Uyeuzmanlik extends ToysBaseEntity { 
 
	private static final long serialVersionUID = 1L; 
 
	@Id 
	@GeneratedValue(strategy = GenerationType.IDENTITY) 
	@Column(updatable = false, unique = true, nullable = false, name = "RID") 
	private Long rID; 
 
	@ManyToOne 
	@JoinColumn(name = "KISIREF") 
	private Kisi kisiRef; 
 
	@ManyToOne 
	@JoinColumn(name = "UZMANLIKTIPREF") 
	private Uzmanliktip uzmanliktipRef; 
 
	@Column(name = "ACIKLAMA") 
	private String aciklama; 
 
 
	public Uyeuzmanlik() { 
		super(); 
	} 
 
	@Override
	public Long getRID() { 
		return this.rID;  
	}  
	  
	@Override
	public void setRID(Long rID) {  
		this.rID = rID;  
	}   
 
	public Kisi getKisiRef() {
		return kisiRef;
	}

	public void setKisiRef(Kisi kisiRef) {
		this.kisiRef = kisiRef;
	}

	public Uzmanliktip getUzmanliktipRef() { 
		return uzmanliktipRef; 
	} 
	 
	public void setUzmanliktipRef(Uzmanliktip uzmanliktipRef) { 
		this.uzmanliktipRef = uzmanliktipRef; 
	} 
 
	public String getAciklama() { 
		return this.aciklama; 
	} 
	 
	public void setAciklama(String aciklama) { 
		this.aciklama = aciklama; 
	} 
 
 
	@Override 
	public String toString() { 
		return "tr.com.arf.toys.db.model.uye.Uyeuzmanlik[id=" + this.getRID()  + "]"; 
	} 
	@Override 
	public String getUIString() { 
		return KeyUtil.getLabelValue("uyeuzmanlik.uyeRef") + " : " + getKisiRef().getUIStringShort() + ", " + KeyUtil.getLabelValue("uyeuzmanlik.uzmanliktipRef") + " : " +  getUzmanliktipRef().getAd(); 
	} 
	 
	@Override 
	public void initValues(boolean defaultValues) { 
		if (defaultValues) { 
				// TODO Initialize with default values 
		} else { 
			this.rID = null; 			this.kisiRef = null; 			this.uzmanliktipRef = null; 			this.aciklama = ""; 		}  
	}  
	 
	@Override 
	public String getValue() {  
		StringBuilder value = new StringBuilder(""); 
			if(getKisiRef() != null) 
				value.append(KeyUtil.getLabelValue("uyeuzmanlik.kisiRef") + " : " + getKisiRef().getUIStringShort() + "<br />"); 
			if(getUzmanliktipRef() != null) 
				value.append(KeyUtil.getLabelValue("uyeuzmanlik.uzmanliktipRef") + " : " + getUzmanliktipRef().getUIString() + "<br />"); 
			if(getAciklama() != null) 
				value.append(KeyUtil.getLabelValue("uyeuzmanlik.aciklama") + " : " + getAciklama() + ""); 
		return value.toString(); 
	} 
	 
}	 
