package tr.com.arf.toys.db.model.egitim; 
 
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tr.com.arf.framework.utility.tool.KeyUtil;
import tr.com.arf.toys.db.enumerated.egitim.EgitimDurum;
import tr.com.arf.toys.db.enumerated.egitim.Egitimturu;
import tr.com.arf.toys.db.model.base.ToysBaseEntity;
 
@Entity 
@Table(name = "EGITIMTANIM") 
public class Egitimtanim extends ToysBaseEntity { 
 
	private static final long serialVersionUID = 1L; 
 
	@Id 
	@GeneratedValue(strategy = GenerationType.IDENTITY) 
	@Column(updatable = false, unique = true, nullable = false, name = "RID") 
	private Long rID; 
 
	@Column(name = "AD") 
	private String ad; 
 
	@Enumerated(EnumType.ORDINAL) 
	@Column(name = "EGITIMTURU") 
	private Egitimturu egitimturu; 
 
	@Column(name = "ACIKLAMA") 
	private String aciklama; 
 
	@Column(name = "EGITIMTANIMKODU") 
	private String egitimtanimkodu; 
	
	@Enumerated(EnumType.ORDINAL) 
	@Column(name = "DURUM") 
	private EgitimDurum durum; 
	
	@Column(name = "SURE") 
	private BigDecimal sure;
	
	@ManyToOne 
	@JoinColumn(name = "SINAVREF") 
	private Sinav sinavRef; 
 
	public Egitimtanim() { 
		super(); 
	} 
 
	public Long getRID() { 
		return this.rID;  
	}  
	  
	public void setRID(Long rID) {  
		this.rID = rID;  
	}   
 
	public String getAd() { 
		return this.ad; 
	} 
	 
	public void setAd(String ad) { 
		this.ad = ad; 
	} 
 
	public Egitimturu getEgitimturu() { 
		return this.egitimturu;  
	}  
  
	public void setEgitimturu(Egitimturu egitimturu) {  
		this.egitimturu = egitimturu;  
	}   
 
	public String getAciklama() { 
		return this.aciklama; 
	} 
	 
	public void setAciklama(String aciklama) { 
		this.aciklama = aciklama; 
	} 
 
	public String getEgitimtanimkodu() {
		return egitimtanimkodu;
	}

	public void setEgitimtanimkodu(String egitimtanimkodu) {
		this.egitimtanimkodu = egitimtanimkodu;
	}

	public EgitimDurum getDurum() {
		return durum;
	}

	public void setDurum(EgitimDurum durum) {
		this.durum = durum;
	}

	public BigDecimal getSure() {
		return sure;
	}

	public void setSure(BigDecimal sure) {
		this.sure = sure;
	}

	public Sinav getSinavRef() {
		return sinavRef;
	}

	public void setSinavRef(Sinav sinavRef) {
		this.sinavRef = sinavRef;
	}

	@Override 
	public String toString() { 
		return "tr.com.arf.toys.db.model.egitim.Egitimtanim[id=" + this.getRID()  + "]"; 
	} 
	@Override 
	public String getUIString() { 
		return getAd() + ""; 
	} 
	 
	@Override 
	public void initValues(boolean defaultValues) { 
			this.rID = null; 
			this.ad = ""; 
			this.egitimturu = Egitimturu._NULL; 
			this.aciklama = "";
			this.egitimtanimkodu="";
			this.durum=EgitimDurum._NULL;
			this.sure=null;
			this.sinavRef=null;
	}  
	 
	@Override 
	public String getValue() {  
		StringBuilder value = new StringBuilder("Veri : <br />"); 
			if(getAd() != null) 
				value.append(KeyUtil.getLabelValue("egitimtanim.ad") + " : " + getAd() + "<br />"); 
			if(getSinavRef() != null) 
				value.append(KeyUtil.getLabelValue("egitimtanim.sinavRef") + " : " + getSinavRef().getUIString() + "<br />"); 
			if(getEgitimturu() != null) 
				value.append(KeyUtil.getLabelValue("egitimtanim.egitimturu") + " : " + getEgitimturu().getLabel() + "<br />"); 
			if(getAciklama() != null) 
				value.append(KeyUtil.getLabelValue("egitimtanim.aciklama") + " : " + getAciklama() + ""); 
			if(getEgitimtanimkodu() != null) 
				value.append(KeyUtil.getLabelValue("egitimtanim.egitimtanimkodu") + " : " + getEgitimtanimkodu() + ""); 
			if(getDurum() != null) 
				value.append(KeyUtil.getLabelValue("egitimtanim.egitimturu") + " : " + getDurum().getLabel() + "<br />"); 
			if(getSure() != null) 
				value.append(KeyUtil.getLabelValue("egitimtanim.sure") + " : " + getSure() + "<br />"); 
		
		return value.toString(); 
	} 
	 
}	 
