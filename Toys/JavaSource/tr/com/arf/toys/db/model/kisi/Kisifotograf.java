package tr.com.arf.toys.db.model.kisi; 
 
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tr.com.arf.framework.db.enumerated._common.EvetHayir;
import tr.com.arf.framework.utility.tool.KeyUtil;
import tr.com.arf.toys.db.model.base.ToysBaseEntity;
 
 
@Entity 
@Table(name = "KISIFOTOGRAF") 
public class Kisifotograf extends ToysBaseEntity { 
 
	private static final long serialVersionUID = 1L; 
 
	@Id 
	@GeneratedValue(strategy = GenerationType.IDENTITY) 
	@Column(updatable = false, unique = true, nullable = false, name = "RID") 
	private Long rID; 
 
	@ManyToOne 
	@JoinColumn(name = "KISIREF") 
	private Kisi kisiRef; 
 
	@Column(name = "PATH") 
	private String path; 
 
	@Enumerated(EnumType.ORDINAL)
	@Column(name = "VARSAYILAN") 
	private EvetHayir varsayilan;
 
	public Kisifotograf() { 
		super(); 
	} 
 
	@Override
	public Long getRID() { 
		return this.rID;  
	}  
	  
	@Override
	public void setRID(Long rID) {  
		this.rID = rID;  
	}   
 
	public Kisi getKisiRef() { 
		return kisiRef; 
	} 
	 
	public void setKisiRef(Kisi kisiRef) { 
		this.kisiRef = kisiRef; 
	} 
 
	@Override
	public String getPath() { 
		return this.path; 
	} 
	 
	@Override
	public void setPath(String path) { 
		this.path = path; 
	} 
 
	public EvetHayir getVarsayilan() {
		return varsayilan;
	}

	public void setVarsayilan(EvetHayir varsayilan) {
		this.varsayilan = varsayilan;
	}

	@Override 
	public String toString() { 
		return "tr.com.arf.toys.db.model.kisi.Kisifotograf[id=" + this.getRID()  + "]"; 
	} 
	
	@Override 
	public String getUIString() { 
		return getPath() + ""; 
	} 
	 
	@Override 
	public void initValues(boolean defaultValues) { 
		if (defaultValues) { 
				// TODO Initialize with default values 
		} else { 
			this.rID = null; 			this.kisiRef = null; 			this.path = ""; 
			this.varsayilan=EvetHayir._NULL;		}  
	}  
	 
	@Override 
	public String getValue() {  
		StringBuilder value = new StringBuilder(""); 
			if(getKisiRef() != null) {
				value.append(KeyUtil.getLabelValue("kisifotograf.kisiRef") + " : " + getKisiRef().getUIString() + "<br />");
			} 
			if(getPath() != null) {
				value.append(KeyUtil.getLabelValue("kisifotograf.path") + " : " + getPath() + "");
			} 
			if(getVarsayilan() != null) {
				value.append(KeyUtil.getLabelValue("kisifotograf.varsayilan") + " : " + getVarsayilan() + "<br />");
			} 
		return value.toString(); 
	} 
	 
}	 
