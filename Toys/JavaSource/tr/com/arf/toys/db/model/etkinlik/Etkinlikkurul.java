package tr.com.arf.toys.db.model.etkinlik; 
 
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tr.com.arf.framework.utility.tool.KeyUtil;
import tr.com.arf.toys.db.enumerated.etkinlik.Kurulturu;
import tr.com.arf.toys.db.model.base.ToysBaseEntity;
 
@Entity 
@Table(name = "ETKINLIKKURUL") 
public class Etkinlikkurul extends ToysBaseEntity { 
 
	private static final long serialVersionUID = 1L; 
 
	@Id 
	@GeneratedValue(strategy = GenerationType.IDENTITY) 
	@Column(updatable = false, unique = true, nullable = false, name = "RID") 
	private Long rID; 
 
	@ManyToOne 
	@JoinColumn(name = "ETKINLIKREF") 
	private Etkinlik etkinlikRef; 
 
	@Enumerated(EnumType.ORDINAL) 
	@Column(name = "KURULTURU") 
	private Kurulturu kurulturu; 
 
 
	public Etkinlikkurul() { 
		super(); 
	} 
 
	public Long getRID() { 
		return this.rID;  
	}  
	  
	public void setRID(Long rID) {  
		this.rID = rID;  
	}   
 
	public Etkinlik getEtkinlikRef() { 
		return etkinlikRef; 
	} 
	 
	public void setEtkinlikRef(Etkinlik etkinlikRef) { 
		this.etkinlikRef = etkinlikRef; 
	} 
 
	public Kurulturu getKurulturu() { 
		return this.kurulturu;  
	}  
  
	public void setKurulturu(Kurulturu kurulturu) {  
		this.kurulturu = kurulturu;  
	}   
 
 
	@Override 
	public String toString() { 
		return "tr.com.arf.toys.db.model.etkinlik.Etkinlikkurul[id=" + this.getRID()  + "]"; 
	} 
	@Override 
	public String getUIString() { 
		return getEtkinlikRef() + ""; 
	} 
	 
	@Override 
	public void initValues(boolean defaultValues) { 
			this.rID = null; 			this.etkinlikRef = null; 			this.kurulturu = Kurulturu._NULL; 	}  
	 
	@Override 
	public String getValue() {  
		StringBuilder value = new StringBuilder("Veri : <br />"); 
			if(getEtkinlikRef() != null) 
				value.append(KeyUtil.getLabelValue("etkinlikkurul.etkinlikRef") + " : " + getEtkinlikRef().getUIString() + "<br />"); 
			if(getKurulturu() != null) 
				value.append(KeyUtil.getLabelValue("etkinlikkurul.kurulturu") + " : " + getKurulturu().getLabel() + ""); 
		return value.toString(); 
	} 
	 
}	 
