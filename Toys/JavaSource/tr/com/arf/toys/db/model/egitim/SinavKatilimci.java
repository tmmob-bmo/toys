package tr.com.arf.toys.db.model.egitim; 
 
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tr.com.arf.framework.utility.tool.KeyUtil;
import tr.com.arf.toys.db.enumerated.egitim.SonucTuru;
import tr.com.arf.toys.db.enumerated.kisi.KisiTuru;
import tr.com.arf.toys.db.model.base.ToysBaseEntity;
import tr.com.arf.toys.db.model.kisi.Kisi;
 
@Entity 
@Table(name = "SINAVKATILIMCI") 
public class SinavKatilimci extends ToysBaseEntity { 
 
	private static final long serialVersionUID = 1L; 
 
	@Id 
	@GeneratedValue(strategy = GenerationType.IDENTITY) 
	@Column(updatable = false, unique = true, nullable = false, name = "RID") 
	private Long rID; 
 
	@ManyToOne 
	@JoinColumn(name = "KISIREF") 
	private Kisi kisiRef; 
	
	@ManyToOne 
	@JoinColumn(name = "SINAVREF") 
	private Sinav sinavRef; 
	
	@Column(name = "ACIKLAMA") 
	private String aciklama; 
	
	@Enumerated(EnumType.ORDINAL) 
	@Column(name = "KISITURU") 
	private KisiTuru kisituru; 
	
	@Enumerated(EnumType.ORDINAL) 
	@Column(name = "SONUC") 
	private SonucTuru sonuc; 
	
	@Column(name = "TEORIKPUAN") 
	private BigDecimal teorikpuan; 
	
	@Column(name = "TOPLAMPUAN") 
	private BigDecimal toplampuan; 
	
	@Column(name = "PROJEUYGULAMA") 
	private String projeuygulama; 
 
	public SinavKatilimci() { 
		super(); 
	} 
 
	@Override
	public Long getRID() { 
		return this.rID;  
	}  
	  
	@Override
	public void setRID(Long rID) {  
		this.rID = rID;  
	}   
 
	public Sinav getSinavRef() {
		return sinavRef;
	}

	public void setSinavRef(Sinav sinavRef) {
		this.sinavRef = sinavRef;
	}

	public String getAciklama() {
		return aciklama;
	}

	public void setAciklama(String aciklama) {
		this.aciklama = aciklama;
	}

	public Kisi getKisiRef() {
		return kisiRef;
	}

	public void setKisiRef(Kisi kisiRef) {
		this.kisiRef = kisiRef;
	}

	public KisiTuru getKisituru() {
		return kisituru;
	}

	public void setKisituru(KisiTuru kisituru) {
		this.kisituru = kisituru;
	}

	public SonucTuru getSonuc() {
		return sonuc;
	}

	public void setSonuc(SonucTuru sonuc) {
		this.sonuc = sonuc;
	}

	public BigDecimal getTeorikpuan() {
		return teorikpuan;
	}

	public void setTeorikpuan(BigDecimal teorikpuan) {
		this.teorikpuan = teorikpuan;
	}

	public BigDecimal getToplampuan() {
		return toplampuan;
	}

	public void setToplampuan(BigDecimal toplampuan) {
		this.toplampuan = toplampuan;
	}

	public String getProjeuygulama() {
		return projeuygulama;
	}

	public void setProjeuygulama(String projeuygulama) {
		this.projeuygulama = projeuygulama;
	}

	@Override 
	public String toString() { 
		return "tr.com.arf.toys.db.model.egitim.SinavKatilimci[id=" + this.getRID()  + "]"; 
	} 
	@Override 
	public String getUIString() { 
		if (getKisiRef()!=null){
			return getKisiRef().getUIStringShort() + ""; 
		}else{
			return null;
		}
	} 
	 
	@Override 
	public void initValues(boolean defaultValues) { 
			this.rID = null; 			this.aciklama=null;
			this.kisiRef=null;
			this.kisituru=KisiTuru._NULL;
			this.projeuygulama=null;
			this.sinavRef=null;
			this.teorikpuan=null;
			this.sonuc=SonucTuru._NULL;
			this.toplampuan=null;
	}  
	 
	@Override 
	public String getValue() {  
		StringBuilder value = new StringBuilder("Veri : <br />"); 
			if(getKisiRef() != null) {
				value.append(KeyUtil.getLabelValue("sinavKatilimci.kisiRef") + " : " + getKisiRef().getUIString() + "<br />");
			} 
			if(getSinavRef() != null) {
				value.append(KeyUtil.getLabelValue("sinavKatilimci.sinavRef") + " : " + getSinavRef().getUIString() + "<br />");
			} 
			if(getAciklama() != null) {
				value.append(KeyUtil.getLabelValue("sinavKatilimci.aciklama") + " : " + getAciklama() + "<br />");
			} 
			if(getKisituru() != null) {
				value.append(KeyUtil.getLabelValue("sinavKatilimci.kisituru") + " : " + getKisituru().getLabel() + "<br />");
			} 
			if(getProjeuygulama() != null) {
				value.append(KeyUtil.getLabelValue("sinavKatilimci.projeuygulama") + " : " + getProjeuygulama() + "<br />");
			} 
			if(getTeorikpuan() != null) {
				value.append(KeyUtil.getLabelValue("sinavKatilimci.teorikpuan") + " : " + getTeorikpuan() + "<br />");
			}
			if(getToplampuan() != null) {
				value.append(KeyUtil.getLabelValue("sinavKatilimci.toplampuan") + " : " + getToplampuan() + "<br />");
			}
			if(getSonuc() != null) {
				value.append(KeyUtil.getLabelValue("sinavKatilimci.sonuc") + " : " + getSonuc() + "<br />");
			}
			
			
		return value.toString(); 
	} 
	 
}	 
