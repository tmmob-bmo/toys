package tr.com.arf.toys.db.model.finans;

import java.math.BigDecimal;
import java.math.MathContext;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import tr.com.arf.framework.utility.tool.BigDecimalUtil;
import tr.com.arf.framework.utility.tool.KeyUtil;
import tr.com.arf.toys.db.enumerated.finans.BorcDurum;
import tr.com.arf.toys.db.model.base.ToysBaseEntity;
import tr.com.arf.toys.db.model.egitim.Egitim;
import tr.com.arf.toys.db.model.kisi.Kisi;
import tr.com.arf.toys.db.model.system.Birim;
import tr.com.arf.toys.db.model.system.Odemetip;

@Entity
@Table(name = "BORC")
public class Borc extends ToysBaseEntity {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(updatable = false, unique = true, nullable = false, name = "RID")
	private Long rID;

	@ManyToOne
	@JoinColumn(name = "KISIREF")
	private Kisi kisiRef;

	@ManyToOne
	@JoinColumn(name = "BIRIMREF")
	private Birim birimRef;

	@ManyToOne
	@JoinColumn(name = "ODEMETIPREF")
	private Odemetip odemetipRef;

	@Temporal(TemporalType.DATE)
	@Column(name = "TARIH")
	private java.util.Date tarih;

	@Column(name = "MIKTAR")
	private BigDecimal miktar;

	@Column(name = "ODENEN")
	private BigDecimal odenen;

	@Enumerated(EnumType.ORDINAL)
	@Column(name = "BORCDURUM")
	private BorcDurum borcdurum;

	@ManyToOne
	@JoinColumn(name = "EGITIMREF")
	private Egitim egitimRef;

	public Borc() {
		super();
	}

	@Override
	public Long getRID() {
		return this.rID;
	}

	@Override
	public void setRID(Long rID) {
		this.rID = rID;
	}

	public Kisi getKisiRef() {
		return kisiRef;
	}

	public void setKisiRef(Kisi kisiRef) {
		this.kisiRef = kisiRef;
	}

	public Birim getBirimRef() {
		return birimRef;
	}

	public void setBirimRef(Birim birimRef) {
		this.birimRef = birimRef;
	}

	public Odemetip getOdemetipRef() {
		return odemetipRef;
	}

	public void setOdemetipRef(Odemetip odemetipRef) {
		this.odemetipRef = odemetipRef;
	}

	public java.util.Date getTarih() {
		return tarih;
	}

	public void setTarih(java.util.Date tarih) {
		this.tarih = tarih;
	}

	public BigDecimal getMiktar() {
		return BigDecimalUtil.changeToCurrency(this.miktar);
	}

	public void setMiktar(BigDecimal miktar) {
		this.miktar = BigDecimalUtil.changeToCurrency(miktar);
	}

	public BigDecimal getOdenen() {
		return BigDecimalUtil.changeToCurrency(this.odenen);
	}

	public void setOdenen(BigDecimal odenen) {
		this.odenen = BigDecimalUtil.changeToCurrency(odenen);
		borcDurumGuncelle();
	}

	public BigDecimal getKalan() {
		MathContext mc = new MathContext(0);
		mc = MathContext.DECIMAL32;
		return BigDecimalUtil.changeToCurrency(this.miktar.subtract(this.odenen, mc));
	}

	public BorcDurum getBorcdurum() {
		return this.borcdurum;
	}

	public void setBorcdurum(BorcDurum borcdurum) {
		this.borcdurum = borcdurum;
	}

	public Egitim getEgitimRef() {
		return egitimRef;
	}

	public void setEgitimRef(Egitim egitimRef) {
		this.egitimRef = egitimRef;
	}

	public void borcDurumGuncelle() {
		if (getKalan().longValue() < 0) {
			setBorcdurum(BorcDurum._ALACAKLI);
		} else if (getKalan().longValue() == 0) {
			setBorcdurum(BorcDurum._ODENMIS);
		} else if (getKalan().longValue() > 0) {
			setBorcdurum(BorcDurum._BORCLU);
		}
	}

	@Override
	public String toString() {
		return "tr.com.arf.toys.db.model.finans.Borc[id=" + this.getRID() + "]";
	}

	@Override
	public String getUIString() {
		return getMiktar() + "";
	}

	@Override
	public void initValues(boolean defaultValues) {
		if (defaultValues) {
			// TODO Initialize with default values
		} else {
			this.rID = null;
			this.kisiRef = null;
			this.birimRef = null;
			this.odemetipRef = null;
			this.tarih = null;
			this.miktar = null;
			this.odenen = null;
			this.borcdurum = BorcDurum._NULL;
			this.egitimRef = null;
		}
	}

	@Override
	public String getValue() {
		StringBuilder value = new StringBuilder("");
		if (getKisiRef() != null) {
			value.append(KeyUtil.getLabelValue("borc.kisiRef") + " : " + getKisiRef().getUIString() + "<br />");
		}
		if (getBirimRef() != null) {
			value.append(KeyUtil.getLabelValue("borc.birimRef") + " : " + getBirimRef().getUIString() + "<br />");
		}
		if (getOdemetipRef() != null) {
			value.append(KeyUtil.getLabelValue("borc.odemetipRef") + " : " + getOdemetipRef().getUIString() + "<br />");
		}
		if (getTarih() != null) {
			value.append(KeyUtil.getLabelValue("borc.tarih") + " : " + getTarih() + "<br />");
		}
		if (getMiktar() != null) {
			value.append(KeyUtil.getLabelValue("borc.miktar") + " : " + getMiktar() + "<br />");
		}
		if (getOdenen() != null) {
			value.append(KeyUtil.getLabelValue("borc.odenen") + " : " + getOdenen() + "<br />");
		}
		if (getBorcdurum() != null) {
			value.append(KeyUtil.getLabelValue("borc.borcdurum") + " : " + getBorcdurum() + "");
		}
		if (getEgitimRef() != null) {
			value.append(KeyUtil.getLabelValue("borc.egitimRef") + " : " + getEgitimRef().getUIString() + "");
		}

		return value.toString();
	}

}
