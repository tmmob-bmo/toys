package tr.com.arf.toys.db.model.system;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tr.com.arf.framework.db.model._base.BaseEntity;
import tr.com.arf.framework.utility.tool.KeyUtil;
import tr.com.arf.toys.db.enumerated.system.YbirimTip;


@Entity
@Table(name = "YBIRIM")
public class Ybirim extends BaseEntity{
	
	private static final long serialVersionUID = 1387312795487207583L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(updatable = false, unique = true, nullable = false, name = "RID")
	private Long rID;
	
	@Column(name = "USTREF")
	private Ybirim ustRef;  //???
	
	@Enumerated(EnumType.ORDINAL)
	@Column(name = "BIRIMTIP")
	private YbirimTip birimTip;
	
	@ManyToOne 
	@JoinColumn(name = "ILCE")
	private Ilce ilceRef;
	
	@ManyToOne 
	@JoinColumn(name = "SEHIR")
	private Sehir sehirRef;

	
	@Override
	public Long getRID() { 
		return this.rID;  
	}  
	  
	@Override
	public void setRID(Long rID) {  
		this.rID = rID;  
	}   
 
	
	public Ybirim getUstRef() { 
		return ustRef; 
	}  

	
	
	public void setUstRef(Ybirim ustRef) {
		this.ustRef = ustRef;
	}  
  
	
	public Sehir getSehirRef() { 
		return sehirRef; 
	} 
	 
	public void setSehirRef(Sehir sehirRef) { 
		this.sehirRef = sehirRef; 
	} 
 
	public Ilce getIlceRef() { 
		return ilceRef; 
	} 
	 
	public void setIlceRef(Ilce ilceRef) { 
		this.ilceRef = ilceRef; 
	} 
 
	public YbirimTip getbirimtip() { 
		return this.birimTip;  
	}  
  
	public void setbirimtip(YbirimTip birimtip) {  
		this.birimTip = birimtip;  
	} 
	 
	
 
	@Override 
	public String toString() { 
		if(getbirimtip() != null){
			return birimTip.getLabel();
		}
		else {
			return "";
		// return "tr.com.arf.toys.db.model.system.ybirim[id=" + this.getRID()  + "]"; 
		}
	} 
	
	@Override 
	public String getUIString() { 
		String result = getbirimtip().getLabel() + "";  
		return result; 
	}
	
	
	 
	@Override 
	public void initValues(boolean defaultValues) { 
		if (defaultValues) { 
				// TODO Initialize with default values 
		} else { 
			this.rID = null; 
			this.ustRef = null; 
			this.sehirRef = null; 
			this.ilceRef = null; 
			this.birimTip = YbirimTip._NULL;
			 
		}  
	}  
	 
	@Override 
	public String getValue() {  
		StringBuilder value = new StringBuilder(""); 
			if(getUstRef() != null) {
				value.append(KeyUtil.getLabelValue("ybirim.ustRef") + " : " + getUstRef().getUIString() + "<br />");
			} 
			
			
			
			if(getSehirRef() != null) {
				value.append(KeyUtil.getLabelValue("ybirim.sehirRef") + " : " + getSehirRef().getUIString() + "<br />");
			} 
			if(getIlceRef() != null) {
				value.append(KeyUtil.getLabelValue("ybirim.ilceRef") + " : " + getIlceRef().getUIString() + "<br />");
			} 
			if(getbirimtip() != null) {
				value.append(KeyUtil.getLabelValue("ybirim.ybirimtip") + " : " + getbirimtip() + "");
			} 
		return value.toString(); 
	}
	 
}
