package tr.com.arf.toys.db.model.iletisim;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import tr.com.arf.framework.utility.tool.DateUtil;
import tr.com.arf.framework.utility.tool.KeyUtil;
import tr.com.arf.toys.db.enumerated.iletisim.GonderimDurum;
import tr.com.arf.toys.db.model.base.ToysBaseEntity;
import tr.com.arf.toys.db.model.kisi.Kisi;
import tr.com.arf.toys.db.model.system.Birim;

@Entity
@Table(name = "SMSGONDERIMLOG")
public class Smsgonderimlog extends ToysBaseEntity {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(updatable = false, unique = true, nullable = false, name = "RID")
	private Long rID;

	@ManyToOne
	@JoinColumn(name = "GONDERIMREF")
	private Gonderim gonderimRef;

	@ManyToOne
	@JoinColumn(name = "KISIREF")
	private Kisi kisiRef;

	@Temporal(TemporalType.DATE)
	@Column(name = "TARIH")
	private java.util.Date tarih;

	@ManyToOne
	@JoinColumn(name = "BIRIMREF")
	private Birim birimRef;

	@Enumerated(EnumType.ORDINAL)
	@Column(name = "DURUM")
	private GonderimDurum durum;

	@Column(name = "ICERIK")
	private String icerik;

	@ManyToOne
	@JoinColumn(name = "GONDERENREF")
	private Kisi gonderenRef;

	@Column(name = "ACIKLAMA")
	private String aciklama;

	public Smsgonderimlog() {
		super();
	}

	@Override
	public Long getRID() {
		return this.rID;
	}

	@Override
	public void setRID(Long rID) {
		this.rID = rID;
	}

	public Gonderim getGonderimRef() {
		return gonderimRef;
	}

	public void setGonderimRef(Gonderim gonderimRef) {
		this.gonderimRef = gonderimRef;
	}

	public Kisi getKisiRef() {
		return kisiRef;
	}

	public void setKisiRef(Kisi kisiRef) {
		this.kisiRef = kisiRef;
	}

	public java.util.Date getTarih() {
		return tarih;
	}

	public void setTarih(java.util.Date tarih) {
		this.tarih = tarih;
	}

	public Birim getBirimRef() {
		return birimRef;
	}

	public void setBirimRef(Birim birimRef) {
		this.birimRef = birimRef;
	}

	public GonderimDurum getDurum() {
		return this.durum;
	}

	public void setDurum(GonderimDurum durum) {
		this.durum = durum;
	}

	public String getIcerik() {
		return this.icerik;
	}

	public void setIcerik(String icerik) {
		this.icerik = icerik;
	}

	public Kisi getGonderenRef() {
		return gonderenRef;
	}

	public void setGonderenRef(Kisi gonderenRef) {
		this.gonderenRef = gonderenRef;
	}

	public String getAciklama() {
		return this.aciklama;
	}

	public void setAciklama(String aciklama) {
		this.aciklama = aciklama;
	}

	@Override
	public String toString() {
		return "tr.com.arf.toys.db.model.iletisim.Smsgonderimlog[id=" + this.getRID() + "]";
	}

	@Override
	public String getUIString() {
		return getIcerik() + "";
	}

	@Override
	public void initValues(boolean defaultValues) {
		this.rID = null;
		this.gonderimRef = null;
		this.kisiRef = null;
		this.tarih = null;
		this.birimRef = null;
		this.durum = GonderimDurum._NULL;
		this.icerik = "";
		this.aciklama = "";
		this.gonderenRef = null;
	}

	@Override
	public String getValue() {
		StringBuilder value = new StringBuilder("Veri : <br />");
		if (getGonderimRef() != null) {
			value.append(KeyUtil.getLabelValue("smsgonderimlog.gonderimRef") + " : " + getGonderimRef().getUIString() + "<br />");
		}
		if (getKisiRef() != null) {
			value.append(KeyUtil.getLabelValue("smsgonderimlog.kisiRef") + " : " + getKisiRef().getUIString() + "<br />");
		}
		if (getTarih() != null) {
			value.append(KeyUtil.getLabelValue("smsgonderimlog.tarih") + " : " + DateUtil.dateToDMYHMS(getTarih()) + "<br />");
		}
		if (getBirimRef() != null) {
			value.append(KeyUtil.getLabelValue("smsgonderimlog.birimRef") + " : " + getBirimRef().getUIString() + "<br />");
		}
		if (getDurum() != null) {
			value.append(KeyUtil.getLabelValue("smsgonderimlog.durum") + " : " + getDurum().getLabel() + "<br />");
		}
		if (getIcerik() != null) {
			value.append(KeyUtil.getLabelValue("smsgonderimlog.icerik") + " : " + getIcerik() + "<br />");
		}
		if (getGonderenRef() != null) {
			value.append(KeyUtil.getLabelValue("smsgonderimlog.gonderenRef") + " : " + getGonderenRef().getUIString() + "<br />");
		}
		if (getAciklama() != null) {
			value.append(KeyUtil.getLabelValue("smsgonderimlog.aciklama") + " : " + getAciklama() + "");
		}
		return value.toString();
	}

}
