package tr.com.arf.toys.db.model.uye;

import java.math.BigDecimal;
import java.math.MathContext;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tr.com.arf.framework.utility.tool.BigDecimalUtil;
import tr.com.arf.framework.utility.tool.KeyUtil;
import tr.com.arf.toys.db.enumerated.uye.UyeAidatDurum;
import tr.com.arf.toys.db.model.base.ToysBaseEntity;

@Entity
@Table(name = "UYEAIDAT")
public class Uyeaidat extends ToysBaseEntity {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(updatable = false, unique = true, nullable = false, name = "RID")
	private Long rID;

	@ManyToOne
	@JoinColumn(name = "UYEREF")
	private Uye uyeRef;

	@Column(name = "YIL")
	private int yil;

	@Column(name = "AY")
	private int ay;

	@Column(name = "MIKTAR")
	private BigDecimal miktar;

	@Column(name = "ODENEN")
	private BigDecimal odenen;

	@Enumerated(EnumType.ORDINAL)
	@Column(name = "UYEAIDATDURUM")
	private UyeAidatDurum uyeaidatdurum;

	@ManyToOne
	@JoinColumn(name = "UYEMUAFIYETREF")
	private Uyemuafiyet uyemuafiyetRef;

	public Uyeaidat() {
		super();
	}

	@Override
	public Long getRID() {
		return this.rID;
	}

	@Override
	public void setRID(Long rID) {
		this.rID = rID;
	}

	public Uye getUyeRef() {
		return uyeRef;
	}

	public void setUyeRef(Uye uyeRef) {
		this.uyeRef = uyeRef;
	}

	public int getYil() {
		return this.yil;
	}

	public void setYil(int yil) {
		this.yil = yil;
	}

	public int getAy() {
		return this.ay;
	}

	public void setAy(int ay) {
		this.ay = ay;
	}

	public UyeAidatDurum getUyeaidatdurum() {
		return this.uyeaidatdurum;
	}

	public void setUyeaidatdurum(UyeAidatDurum uyeaidatdurum) {
		this.uyeaidatdurum = uyeaidatdurum;
	}

	public Uyemuafiyet getUyemuafiyetRef() {
		return uyemuafiyetRef;
	}

	public void setUyemuafiyetRef(Uyemuafiyet uyemuafiyetRef) {
		this.uyemuafiyetRef = uyemuafiyetRef;
	}

	public BigDecimal getMiktar() {
		return BigDecimalUtil.changeToCurrency(this.miktar);
	}

	public void setMiktar(BigDecimal miktar) {
		this.miktar = BigDecimalUtil.changeToCurrency(miktar);
	}

	public BigDecimal getOdenen() {
		return BigDecimalUtil.changeToCurrency(this.odenen);
	}

	public void setOdenen(BigDecimal odenen) {
		this.odenen = BigDecimalUtil.changeToCurrency(odenen);
		uyeAidatDurumGuncelle();
	}

	// @Transient
	// public int getAidatYili() {
	// try {
	// if(getDBOperator().recordCount() > 0){
	// return (Adres) getDBOperator().load(Adres.class.getSimpleName(), "o.kisiRef.rID=" + getKisiRef().getRID() +
	// " AND o.adresturu=" + AdresTuru._EVADRESI.getCode() , "o.varsayilan ASC, o.beyanTarihi DESC").get(0);
	// } else {
	// return null;
	// }
	// } catch(Exception e){
	// return null;
	// }
	// }

	public BigDecimal getKalan() {
		if (getUyemuafiyetRef() == null) {
			MathContext mc = new MathContext(0);
			mc = MathContext.DECIMAL32;
			return BigDecimalUtil.changeToCurrency(this.miktar.subtract(this.odenen, mc));
		} else {
			return BigDecimalUtil.changeToCurrency(new BigDecimal(0L));
		}
	}

	public void uyeAidatDurumGuncelle() {
		if (getUyemuafiyetRef() != null) {
			setUyeaidatdurum(UyeAidatDurum._MUAF);
		} else if (getKalan().longValue() < 0) {
			setUyeaidatdurum(UyeAidatDurum._ALACAKLI);
		} else if (getKalan().longValue() == 0) {
			setUyeaidatdurum(UyeAidatDurum._ODENMIS);
		} else if (getKalan().longValue() > 0) {
			setUyeaidatdurum(UyeAidatDurum._BORCLU);
		}
	}

	@Override
	public String toString() {
		return "tr.com.arf.toys.db.model.uye.Uyeaidat[id=" + this.getRID() + "]";
	}

	@Override
	public String getUIString() {
		return getYil() + "-" + getAy() + " Kalan :" + getKalan();
	}

	@Override
	public void initValues(boolean defaultValues) {
		if (defaultValues) {
			// TODO Initialize with default values
		} else {
			this.rID = null;
			this.uyeRef = null;
			this.yil = 0;
			this.ay = 0;
			this.miktar = null;
			this.odenen = null;
			this.uyeaidatdurum = UyeAidatDurum._NULL;
			this.uyemuafiyetRef = null;
		}
	}

	@Override
	public String getValue() {
		StringBuilder value = new StringBuilder("");
		if (getUyeRef() != null) {
			value.append(KeyUtil.getLabelValue("uyeaidat.uyeRef") + " : " + getUyeRef().getUIString() + "<br />");
		}
		if (getYil() != 0) {
			value.append(KeyUtil.getLabelValue("uyeaidat.yil") + " : " + getYil() + "<br />");
		}
		if (getAy() != 0) {
			value.append(KeyUtil.getLabelValue("uyeaidat.ay") + " : " + getAy() + "<br />");
		}
		if (getMiktar() != null) {
			value.append(KeyUtil.getLabelValue("uyeaidat.miktar") + " : " + getMiktar() + "<br />");
		}
		if (getOdenen() != null) {
			value.append(KeyUtil.getLabelValue("uyeaidat.odenen") + " : " + getOdenen() + "<br />");
		}
		if (getUyeaidatdurum() != null) {
			value.append(KeyUtil.getLabelValue("uyeaidat.uyeaidatdurum") + " : " + getUyeaidatdurum() + "");
		}
		if (getUyemuafiyetRef() != null) {
			value.append(KeyUtil.getLabelValue("uyeaidat.uyemuafiyetRef") + " : " + getUyemuafiyetRef().getUIString() + "");
		}
		return value.toString();
	}

}
