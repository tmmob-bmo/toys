package tr.com.arf.toys.db.model.etkinlik;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import tr.com.arf.framework.db.enumerated._common.EvetHayir;
import tr.com.arf.framework.utility.tool.KeyUtil;
import tr.com.arf.toys.db.model.base.ToysBaseEntity;

@Entity
@Table(name = "ETKINLIKTANIM")
public class Etkinliktanim extends ToysBaseEntity {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(updatable = false, unique = true, nullable = false, name = "RID")
	private Long rID;

	@Column(name = "AD")
	private String ad;

	@Enumerated(EnumType.ORDINAL)
	@Column(name = "PERIYODIK")
	private EvetHayir periyodik;

	@Column(name = "ACIKLAMA")
	private String aciklama;

	@Column(name = "ANAHTARKELIMELER")
	private String anahtarkelimeler;

	public Etkinliktanim() {
		super();
	}

	@Override
	public Long getRID() {
		return this.rID;
	}

	@Override
	public void setRID(Long rID) {
		this.rID = rID;
	}

	public String getAd() {
		return this.ad;
	}

	public void setAd(String ad) {
		this.ad = ad;
	}

	public EvetHayir getPeriyodik() {
		return this.periyodik;
	}

	public void setPeriyodik(EvetHayir periyodik) {
		this.periyodik = periyodik;
	}

	public String getAciklama() {
		return this.aciklama;
	}

	public void setAciklama(String aciklama) {
		this.aciklama = aciklama;
	}

	public String getAnahtarkelimeler() {
		return anahtarkelimeler;
	}

	public void setAnahtarkelimeler(String anahtarkelimeler) {
		this.anahtarkelimeler = anahtarkelimeler;
	}

	@Override
	public String toString() {
		return "tr.com.arf.toys.db.model.etkinlik.Etkinliktanim[id="
				+ this.getRID() + "]";
	}

	@Override
	public String getUIString() {
		return getAd() + "";
	}

	@Override
	public void initValues(boolean defaultValues) {
		this.rID = null;
		this.ad = "";
		this.periyodik = EvetHayir._NULL;
		this.aciklama = "";
		this.anahtarkelimeler = "";
	}

	@Override
	public String getValue() {
		StringBuilder value = new StringBuilder("Veri : <br />");
		if (getAd() != null) {
			value.append(KeyUtil.getLabelValue("etkinliktanim.ad") + " : "
					+ getAd() + "<br />");
		}
		if (getPeriyodik() != null) {
			value.append(KeyUtil.getLabelValue("etkinliktanim.periyodik")
					+ " : " + getPeriyodik().getLabel() + "<br />");
		}
		if (getAciklama() != null) {
			value.append(KeyUtil.getLabelValue("etkinliktanim.aciklama")
					+ " : " + getAciklama() + "");
		}
		if (getAnahtarkelimeler() != null) {
			value.append(KeyUtil.getLabelValue("etkinliktanim.anahtarkelimeler")
					+ " : " + getAnahtarkelimeler() + "");
		}
		return value.toString();
	}

	public boolean getGoster() {
		if (periyodik == EvetHayir._NULL) {
			return false;
		}
		if (periyodik == EvetHayir._EVET) {
			return false;
		}
		return true;
	}
}
