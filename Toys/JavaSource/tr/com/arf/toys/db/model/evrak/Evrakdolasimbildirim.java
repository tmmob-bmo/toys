package tr.com.arf.toys.db.model.evrak; 
 
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import tr.com.arf.framework.utility.tool.DateUtil;
import tr.com.arf.framework.utility.tool.KeyUtil;
import tr.com.arf.toys.db.model.base.ToysBaseEntity;
 
 
@Entity 
@Table(name = "EVRAKDOLASIMBILDIRIM") 
public class Evrakdolasimbildirim extends ToysBaseEntity { 
 
	private static final long serialVersionUID = 1L; 
 
	@Id 
	@GeneratedValue(strategy = GenerationType.IDENTITY) 
	@Column(updatable = false, unique = true, nullable = false, name = "RID") 
	private Long rID; 
 
	@ManyToOne 
	@JoinColumn(name = "EVRAKREF") 
	private Evrak evrakRef; 
  
	@Column(name = "GONDERENREF") 
	private String gonderenRef; 
  
	@Column(name = "ALICIREF") 
	private String aliciRef; 
 
	@Temporal(TemporalType.DATE) 
	@Column(name = "TARIH") 
	private java.util.Date tarih; 
 
	@Column(name = "ACIKLAMA") 
	private String aciklama; 
 
 
	public Evrakdolasimbildirim() { 
		super(); 
	} 
 
	@Override
	public Long getRID() { 
		return this.rID;  
	}  
	  
	@Override
	public void setRID(Long rID) {  
		this.rID = rID;  
	}   
 
	public Evrak getEvrakRef() { 
		return evrakRef; 
	} 
	 
	public void setEvrakRef(Evrak evrakRef) { 
		this.evrakRef = evrakRef; 
	}  
	public String getGonderenRef() {
		return gonderenRef;
	}

	public void setGonderenRef(String gonderenRef) {
		this.gonderenRef = gonderenRef;
	}

	public String getAliciRef() {
		return aliciRef;
	}

	public void setAliciRef(String aliciRef) {
		this.aliciRef = aliciRef;
	}

	public java.util.Date getTarih() { 
		return tarih; 
	} 
	 
	public void setTarih(java.util.Date tarih) { 
		this.tarih = tarih; 
	} 
 
	public String getAciklama() { 
		return this.aciklama; 
	} 
	 
	public void setAciklama(String aciklama) { 
		this.aciklama = aciklama; 
	} 
 
 
	@Override 
	public String toString() { 
		return "tr.com.arf.toys.db.model.evrak.Evrakdolasimbildirim[id=" + this.getRID()  + "]"; 
	} 
	@Override 
	public String getUIString() { 
		return getAciklama() + ""; 
	} 
	 
	@Override 
	public void initValues(boolean defaultValues) { 
			this.rID = null; 			this.evrakRef = null; 			this.gonderenRef = null; 			this.aliciRef = null; 			this.tarih = null; 			this.aciklama = ""; 	}  
	 
	@Override 
	public String getValue() {  
		StringBuilder value = new StringBuilder("Veri : <br />"); 
			if(getEvrakRef() != null) {
				value.append(KeyUtil.getLabelValue("evrakdolasimbildirim.evrakRef") + " : " + getEvrakRef().getUIString() + "<br />");
			} 
			if(getGonderenRef() != null) {
				value.append(KeyUtil.getLabelValue("evrakdolasimbildirim.gonderenRef") + " : " + getGonderenRef() + "<br />");
			} 
			if(getAliciRef() != null) {
				value.append(KeyUtil.getLabelValue("evrakdolasimbildirim.aliciRef") + " : " + getAliciRef() + "<br />");
			} 
			if(getTarih() != null) {
				value.append(KeyUtil.getLabelValue("evrakdolasimbildirim.tarih") + " : " + DateUtil.dateToDMYHMS(getTarih()) + "<br />");
			} 
			if(getAciklama() != null) {
				value.append(KeyUtil.getLabelValue("evrakdolasimbildirim.aciklama") + " : " + getAciklama() + "");
			} 
		return value.toString(); 
	} 
	 
}	 
