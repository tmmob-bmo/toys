package tr.com.arf.toys.db.model.kisi; 
 
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import tr.com.arf.framework.utility.tool.DateUtil;
import tr.com.arf.framework.utility.tool.KeyUtil;
import tr.com.arf.toys.db.enumerated.kisi.ToplantiTuru;
import tr.com.arf.toys.db.model.base.ToysBaseEntity;
 
@Entity 
@Table(name = "KOMISYONDONEMTOPLANTI") 
public class KomisyonDonemToplanti extends ToysBaseEntity { 
 
	private static final long serialVersionUID = 1L; 
 
	@Id 
	@GeneratedValue(strategy = GenerationType.IDENTITY) 
	@Column(updatable = false, unique = true, nullable = false, name = "RID") 
	private Long rID; 
 
	@ManyToOne 
	@JoinColumn(name = "KOMISYONDONEMREF") 
	private KomisyonDonem komisyonDonemRef; 
 
	@Temporal(TemporalType.TIMESTAMP) 
	@Column(name = "TARIH") 
	private java.util.Date tarih; 
 
	@Enumerated(EnumType.ORDINAL) 
	@Column(name = "TOPLANTITURU") 
	private ToplantiTuru toplantituru; 
	
	@Column(name = "TOPLANTIYERI") 
	private String toplantiYeri; 
 
 
	public KomisyonDonemToplanti() { 
		super(); 
	} 
 
	@Override
	public Long getRID() { 
		return this.rID;  
	}  
	  
	@Override
	public void setRID(Long rID) {  
		this.rID = rID;  
	}   
 
	public KomisyonDonem getKomisyonDonemRef() { 
		return komisyonDonemRef; 
	} 
	 
	public void setKomisyonDonemRef(KomisyonDonem komisyonDonemRef) { 
		this.komisyonDonemRef = komisyonDonemRef; 
	} 
 
	public java.util.Date getTarih() { 
		return tarih; 
	} 
	 
	public void setTarih(java.util.Date tarih) { 
		this.tarih = tarih; 
	} 
 
	public ToplantiTuru getToplantituru() { 
		return this.toplantituru;  
	}  
  
	public void setToplantituru(ToplantiTuru toplantituru) {  
		this.toplantituru = toplantituru;  
	} 
 
	public String getToplantiYeri() {
		return toplantiYeri;
	}

	public void setToplantiYeri(String toplantiYeri) {
		this.toplantiYeri = toplantiYeri;
	}

	@Override 
	public String toString() { 
		return getKomisyonDonemRef().getUIString() + " - " + DateUtil.dateToDMY(getTarih());
	} 
	@Override 
	public String getUIString() { 
		return getKomisyonDonemRef().getUIString() + " - " + DateUtil.dateToDMY(getTarih());
	} 
	 
	@Override 
	public void initValues(boolean defaultValues) { 
			this.rID = null; 			this.komisyonDonemRef = null; 			this.tarih = null; 			this.toplantituru = ToplantiTuru._NULL; 
			this.toplantiYeri=null;	}  
	 
	@Override 
	public String getValue() {  
		StringBuilder value = new StringBuilder("Veri : <br />"); 
			if(getKomisyonDonemRef() != null) {
				value.append(KeyUtil.getLabelValue("komisyonDonemToplanti.komisyonDonemRef") + " : " + getKomisyonDonemRef().getUIString() + "<br />");
			} 
			if(getTarih() != null) {
				value.append(KeyUtil.getLabelValue("komisyonDonemToplanti.tarih") + " : " + DateUtil.dateToDMYHMS(getTarih()) + "<br />");
			} 
			if(getToplantituru() != null) {
				value.append(KeyUtil.getLabelValue("komisyonDonemToplanti.toplantituru") + " : " + getToplantituru().getLabel() + "");
			} 
			if(getToplantiYeri() != null) {
				value.append(KeyUtil.getLabelValue("komisyonDonemToplanti.toplantiYeri") + " : " + getToplantiYeri() + "");
			} 
		return value.toString(); 
	} 
	 
}	 
