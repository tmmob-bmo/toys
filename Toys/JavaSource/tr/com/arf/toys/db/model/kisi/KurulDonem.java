package tr.com.arf.toys.db.model.kisi; 
 
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tr.com.arf.framework.utility.tool.KeyUtil;
import tr.com.arf.toys.db.model.base.ToysBaseEntity;
import tr.com.arf.toys.db.model.system.Birim;
 
@Entity 
@Table(name = "KURULDONEM") 
public class KurulDonem extends ToysBaseEntity { 
 
	private static final long serialVersionUID = 1L; 
 
	@Id 
	@GeneratedValue(strategy = GenerationType.IDENTITY) 
	@Column(updatable = false, unique = true, nullable = false, name = "RID") 
	private Long rID; 
 
	@ManyToOne 
	@JoinColumn(name = "KURULREF") 
	private Kurul kurulRef;
	
	@ManyToOne 
	@JoinColumn(name = "BIRIMREF") 
	private Birim birimRef;
	
	@ManyToOne 
	@JoinColumn(name = "DONEMREF") 
	private Donem donemRef;
	
	@Column(name = "ACIKLAMA") 
	private String aciklama; 
	
	public KurulDonem() { 
		super(); 
	} 
 
	@Override
	public Long getRID() { 
		return this.rID;  
	}  
	  
	@Override
	public void setRID(Long rID) {  
		this.rID = rID;  
	}   
	
	public Kurul getKurulRef() {
		return kurulRef;
	}

	public void setKurulRef(Kurul kurulRef) {
		this.kurulRef = kurulRef;
	}

	public Donem getDonemRef() {
		return donemRef;
	}

	public void setDonemRef(Donem donemRef) {
		this.donemRef = donemRef;
	}

	public String getAciklama() {
		return aciklama;
	}

	public void setAciklama(String aciklama) {
		this.aciklama = aciklama;
	}

	public Birim getBirimRef() {
		return birimRef;
	}

	public void setBirimRef(Birim birimRef) {
		this.birimRef = birimRef;
	}

	@Override 
	public String toString() { 
		return "tr.com.arf.toys.db.model.kisi.KurulDonem[id=" + this.getRID()  + "]"; 
	} 
	@Override 
	public String getUIString() { 
		return getDonemRef().getTanim() + ", " + getKurulRef().getUIString(); 
	} 
	
	public String getUIStringShort() { 
		return getKurulRef().getUIString(); 
	}
	 
	@Override 
	public void initValues(boolean defaultValues) { 
		if (defaultValues) { 
				// TODO Initialize with default values 
		} else { 
			this.rID = null; 			this.kurulRef=null;
			this.donemRef=null;
			this.aciklama=null;
			this.birimRef=null;		}  
	}  
	 
	@Override 
	public String getValue() {  
		StringBuilder value = new StringBuilder(""); 
			if(getKurulRef() != null) {
				value.append(KeyUtil.getLabelValue("kurulDonem.kurulRef") + " : " + getKurulRef().getUIString() + "<br />");
			} 
			if(getDonemRef() != null) {
				value.append(KeyUtil.getLabelValue("kurulDonem.donemRef") + " : " + getDonemRef().getUIString() + "<br />");
			} 
			if(getAciklama() != null) {
				value.append(KeyUtil.getLabelValue("kurulDonem.aciklama") + " : " + getAciklama()+ "<br />");
			}
			if(getBirimRef() != null) {
				value.append(KeyUtil.getLabelValue("kurulDonem.birimRef") + " : " + getBirimRef().getUIString() + "<br />");
			} 
		return value.toString(); 
	} 
	 
}	 
