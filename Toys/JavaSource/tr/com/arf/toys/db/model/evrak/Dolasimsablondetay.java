package tr.com.arf.toys.db.model.evrak; 
 
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tr.com.arf.framework.utility.tool.KeyUtil;
import tr.com.arf.toys.db.model.base.ToysBaseEntity;
import tr.com.arf.toys.db.model.system.Birim;
import tr.com.arf.toys.db.model.system.Personel;
 
 
@Entity 
@Table(name = "DOLASIMSABLONDETAY") 
public class Dolasimsablondetay extends ToysBaseEntity { 
 
	private static final long serialVersionUID = 1L; 
 
	@Id 
	@GeneratedValue(strategy = GenerationType.IDENTITY) 
	@Column(updatable = false, unique = true, nullable = false, name = "RID") 
	private Long rID; 
 
	@ManyToOne 
	@JoinColumn(name = "DOLASIMSABLONREF") 
	private Dolasimsablon dolasimsablonRef; 
 
	@Column(name = "SIRANO") 
	private int sirano; 
 
	@ManyToOne 
	@JoinColumn(name = "BIRIMREF") 
	private Birim birimRef; 
 
	@ManyToOne 
	@JoinColumn(name = "PERSONELREF") 
	private Personel personelRef; 
 
 
	public Dolasimsablondetay() { 
		super(); 
	} 
 
	@Override
	public Long getRID() { 
		return this.rID;  
	}  
	  
	@Override
	public void setRID(Long rID) {  
		this.rID = rID;  
	}   
 
	public Dolasimsablon getDolasimsablonRef() { 
		return dolasimsablonRef; 
	} 
	 
	public void setDolasimsablonRef(Dolasimsablon dolasimsablonRef) { 
		this.dolasimsablonRef = dolasimsablonRef; 
	} 
 
	public int getSirano() { 
		return this.sirano; 
	} 
	 
	public void setSirano(int sirano) { 
		this.sirano = sirano; 
	} 
 
	public Birim getBirimRef() { 
		return birimRef; 
	} 
	 
	public void setBirimRef(Birim birimRef) { 
		this.birimRef = birimRef; 
	} 
 
	public Personel getPersonelRef() { 
		return personelRef; 
	} 
	 
	public void setPersonelRef(Personel personelRef) { 
		this.personelRef = personelRef; 
	} 
 
 
	@Override 
	public String toString() { 
		return "tr.com.arf.toys.db.model.evrak.Dolasimsablondetay[id=" + this.getRID()  + "]"; 
	} 
	
	@Override 
	public String getUIString() { 
		return getDolasimsablonRef() + ""; 
	} 
	
	public String getBirimPersonelRefUIString(){
		if(getBirimRef() != null){
			return "<b>" + KeyUtil.getLabelValue("dolasimsablondetay.birimRef") + " : </b>" + getBirimRef().getAd(); 
		} else if(getPersonelRef() != null){
			return "<b>" + KeyUtil.getLabelValue("dolasimsablondetay.personelRef") + " : </b>" + getPersonelRef().getKisiRef().getUIStringShort(); 
		} else {
			return "-";
		}
	}
	 
	@Override 
	public void initValues(boolean defaultValues) { 
			this.rID = null; 			this.dolasimsablonRef = null; 			this.sirano = 0; 			this.birimRef = null; 			this.personelRef = null; 	}  
	 
	@Override 
	public String getValue() {  
		StringBuilder value = new StringBuilder("Veri : <br />"); 
			if(getDolasimsablonRef() != null) {
				value.append(KeyUtil.getLabelValue("dolasimsablondetay.dolasimsablonRef") + " : " + getDolasimsablonRef().getUIString() + "<br />");
			} 
			if(getSirano() != 0) {
				value.append(KeyUtil.getLabelValue("dolasimsablondetay.sirano") + " : " + getSirano() + "<br />");
			} 
			if(getBirimRef() != null) {
				value.append(KeyUtil.getLabelValue("dolasimsablondetay.birimRef") + " : " + getBirimRef().getUIString() + "<br />");
			} 
			if(getPersonelRef() != null) {
				value.append(KeyUtil.getLabelValue("dolasimsablondetay.personelRef") + " : " + getPersonelRef().getUIString() + "");
			} 
		return value.toString(); 
	} 
	 
}	 
