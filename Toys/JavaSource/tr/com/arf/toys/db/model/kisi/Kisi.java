package tr.com.arf.toys.db.model.kisi;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import tr.com.arf.framework.db._management.DBOperator;
import tr.com.arf.framework.db.enumerated._common.EvetHayir;
import tr.com.arf.framework.utility.tool.DateUtil;
import tr.com.arf.framework.utility.tool.KeyUtil;
import tr.com.arf.toys.db.enumerated.iletisim.AdresTuru;
import tr.com.arf.toys.db.enumerated.iletisim.NetAdresTuru;
import tr.com.arf.toys.db.enumerated.kisi.KimlikTip;
import tr.com.arf.toys.db.enumerated.uye.OgrenimTip;
import tr.com.arf.toys.db.model.base.ToysBaseEntity;
import tr.com.arf.toys.db.model.iletisim.Adres;
import tr.com.arf.toys.db.model.iletisim.Internetadres;
import tr.com.arf.toys.db.model.iletisim.Telefon;
import tr.com.arf.toys.db.model.system.KurumKisi;
import tr.com.arf.toys.db.model.uye.Uye;
import tr.com.arf.toys.service.application.ManagedBeanLocator;

@Entity
@Table(name = "KISI")
public class Kisi extends ToysBaseEntity {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(updatable = false, unique = true, nullable = false, name = "RID")
	private Long rID;

	@Column(name = "KIMLIKNO")
	private Long kimlikno;

	@Column(name = "AD")
	private String ad;

	@Column(name = "SOYAD")
	private String soyad;

	@Enumerated(EnumType.ORDINAL)
	@Column(name = "KIMLIKNOTIP")
	private KimlikTip kimliktip;

	@Enumerated(EnumType.ORDINAL)
	@Column(name = "AKTIF")
	private EvetHayir aktif;

	@Enumerated(EnumType.ORDINAL)
	@Column(name = "ILETISIMTERCIH")
	private EvetHayir iletisimtercih;

	@Transient
	private boolean eskiKayitMi;

	@Transient
	private boolean yabanciMi;

	public Kisi() {
		super();
		this.kimliktip = KimlikTip._TCKIMLIKNO;
	}

	@Override
	public Long getRID() {
		return this.rID;
	}

	@Override
	public void setRID(Long rID) {
		this.rID = rID;
	}

	public Long getKimlikno() {
		return kimlikno;
	}

	public void setKimlikno(Long kimlikno) {
		this.kimlikno = kimlikno;
	}

	public String getAd() {
		return this.ad;
	}

	public void setAd(String ad) {
		this.ad = ad;
	}

	public String getSoyad() {
		return this.soyad;
	}

	public void setSoyad(String soyad) {
		this.soyad = soyad;
	}

	public KimlikTip getKimliktip() {
		return this.kimliktip;
	}

	public void setKimliknotip(KimlikTip kimliktip) {
		this.kimliktip = kimliktip;
	}

	public EvetHayir getAktif() {
		return aktif;
	}

	public void setAktif(EvetHayir aktif) {
		this.aktif = aktif;
	}

	public EvetHayir getIletisimtercih() {
		return iletisimtercih;
	}

	public void setIletisimtercih(EvetHayir iletisimtercih) {
		this.iletisimtercih = iletisimtercih;
	}

	@Transient
	public Adres getKisiAdres() {
		if (getRID() == null) {
			return null;
		} else {
			try {
				if (getDBOperator().recordCount(Adres.class.getSimpleName(), "o.kisiRef.rID=" + getRID()) > 0) {
					Adres adres = (Adres) getDBOperator().load(Adres.class.getSimpleName(), "o.kisiRef.rID=" + getRID(), "o.varsayilan ASC").get(0);
					return adres;
				}
			} catch (Exception e) {
				e.printStackTrace();
				return null;
			}
		}
		return null;
	}

	@Transient
	public Adres getKisiEvAdres() {
		if (getRID() == null) {
			return null;
		} else {
			try {
				if (getDBOperator().recordCount(Adres.class.getSimpleName(), "o.kisiRef.rID=" + getRID() + " AND o.adresturu=" + AdresTuru._EVADRESI.getCode()) > 0) {
					Adres adres = (Adres) getDBOperator().load(Adres.class.getSimpleName(), "o.kisiRef.rID=" + getRID() + " AND o.adresturu=" + AdresTuru._EVADRESI.getCode(),
							"o.varsayilan ASC").get(0);
					return adres;
				}
			} catch (Exception e) {
				e.printStackTrace();
				return null;
			}
		}
		return null;
	}

	@Transient
	public Internetadres getKisiInternetadres() {
		if (getRID() == null) {
			return null;
		} else {
			try {
				if (getDBOperator().recordCount(Internetadres.class.getSimpleName(), "o.kisiRef.rID=" + getRID()) > 0) {
					Internetadres internetAdres = (Internetadres) getDBOperator().load(Internetadres.class.getSimpleName(),
							"o.netadresturu = " + NetAdresTuru._EPOSTA.getCode() + " AND o.kisiRef.rID=" + getRID(), "o.varsayilan ASC").get(0);
					return internetAdres;
				}
			} catch (Exception e) {
				e.printStackTrace();
				return null;
			}
		}
		return null;
	}

	@Transient
	public Telefon getKisiTelefon() {
		if (getRID() == null) {
			return null;
		} else {
			try {
				if (getDBOperator().recordCount(Telefon.class.getSimpleName(), "o.kisiRef.rID=" + getRID()) > 0) {
					Telefon telefon = (Telefon) getDBOperator().load(Telefon.class.getSimpleName(), "o.kisiRef.rID=" + getRID(), "o.varsayilan ASC").get(0);
					return telefon;
				}
			} catch (Exception e) {
				e.printStackTrace();
				return null;
			}
		}
		return null;
	}

	@Transient
	public KurumKisi getKisiIsyeri() {
		if (getRID() == null) {
			return null;
		} else {
			try {
				if (getDBOperator().recordCount(KurumKisi.class.getSimpleName(), "o.kisiRef.rID=" + getRID()) > 0) {
					KurumKisi kurumKisi = (KurumKisi) getDBOperator().load(KurumKisi.class.getSimpleName(), "o.kisiRef.rID=" + getRID(), "o.varsayilan ASC").get(0);
					return kurumKisi;
				}
			} catch (Exception e) {
				e.printStackTrace();
				return null;
			}
		}
		return null;
	}

	@Transient
	public String getKisiUyeyseSicilNo() {
		if (getRID() == null) {
			return null;
		} else {
			try {
				if (getDBOperator().recordCount(Uye.class.getSimpleName(), "o.kisiRef.rID=" + getRID()) > 0) {
					Uye uye = (Uye) getDBOperator().load(Uye.class.getSimpleName(), "o.kisiRef.rID=" + getRID(), "").get(0);
					if (uye.getSicilno() != null) {
						return uye.getSicilno();
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
				return null;
			}
		}
		return null;
	}

	@Transient
	public Kisiogrenim getKisiogrenim() {
		if (getRID() == null) {
			return null;
		} else {
			try {
				if (getDBOperator().recordCount(Kisiogrenim.class.getSimpleName(), "o.kisiRef.rID=" + getRID() + " AND o.ogrenimtip=" + OgrenimTip._LISANS.getCode()) > 0) {
					Kisiogrenim kisiogrenim = (Kisiogrenim) getDBOperator().load(Kisiogrenim.class.getSimpleName(),
							"o.kisiRef.rID = " + getRID() + " AND o.ogrenimtip=" + OgrenimTip._LISANS.getCode(), "o.rID ASC").get(0);
					return kisiogrenim;
				}
			} catch (Exception e) {
				e.printStackTrace();
				return null;
			}
		}
		return null;
	}

	@Transient
	public Kisikimlik getKisikimlik() {
		try {
            DBOperator operator = DBOperator.getInstance();
            if (operator.recordCount(Kisikimlik.class.getSimpleName(), "o.kisiRef.rID=" + getRID()) > 0) {
				return (Kisikimlik) operator.load(Kisikimlik.class.getSimpleName(), "o.kisiRef.rID=" + getRID(), "").get(0);
			} else {
				return null;
			}
		} catch (Exception e) {
			return null;
		}
	}

	public String getDogumtarihText() {
		if (getKisikimlik() == null) {
			return "-";
		} else if (getKisikimlik().getDogumtarih() == null) {
			return "-";
		} else {
			return DateUtil.dateToDMY(getKisikimlik().getDogumtarih());
		}

	}

	public boolean isEskiKayitMi() {
		return eskiKayitMi;
	}

	public void setEskiKayitMi(boolean eskiKayitMi) {
		this.eskiKayitMi = eskiKayitMi;
	}

	public boolean isYabanciMi() {
		return yabanciMi;
	}

	public void setYabanciMi(boolean yabanciMi) {
		this.yabanciMi = yabanciMi;
	}

	public boolean isEskiKayitMiForPersisted() {
		if(getKimlikno().toString().substring(0,3).equalsIgnoreCase("999")) {
			return true;
		} else {
			return false;
		}
	}

	public boolean isYabanciMiForPersisted() {
		if(getKimliktip() == KimlikTip._PASAPORT) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public String toString() {
		return getUIString();
	}

	@Override
	public String getUIString() {
		if (getRID() != null) {
			return getAd() + " " + getSoyad() + " (" + getKimlikno() + ")";
		} else {
			return "";
		}
	}

	@SuppressWarnings("unchecked")
	public String getUIStringForDownload() {
		if (getRID() != null) {
			String result = "";
			List<Uye> uyeListesi = ManagedBeanLocator.locateSessionController().loadObjects(Uye.class.getSimpleName(), "o.kisiRef.rID = " + getRID());
			if (uyeListesi != null && uyeListesi.size() > 0) {
				result = uyeListesi.get(0).getSicilno() + "_" + getUIStringShort();
			} else {
				result = getUIStringShort();
			}
			return result;
		} else {
			return "";
		}
	}

	public String getUIStringShort() {
		return getAd() + " " + getSoyad();
	}

	@Override
	public void initValues(boolean defaultValues) {
		if (defaultValues) {
			// TODO Initialize with default values
			this.kimliktip = KimlikTip._TCKIMLIKNO;
		} else {
			this.rID = null;
			this.kimlikno = null;
			this.ad = "";
			this.soyad = "";
			this.kimliktip = KimlikTip._TCKIMLIKNO;
			this.aktif = EvetHayir._NULL;
			this.iletisimtercih = EvetHayir._NULL;
			this.eskiKayitMi = false;
		}
	}

	@Override
	public String getValue() {
		StringBuilder value = new StringBuilder("");
		if (getKimlikno() != null) {
			value.append(KeyUtil.getLabelValue("kisi.kimlikno") + " : " + getKimlikno() + "<br />");
		}
		if (getAd() != null) {
			value.append(KeyUtil.getLabelValue("kisi.ad") + " : " + getAd() + "<br />");
		}
		if (getSoyad() != null) {
			value.append(KeyUtil.getLabelValue("kisi.soyad") + " : " + getSoyad() + "<br />");
		}
		if (getKimliktip() != null) {
			value.append(KeyUtil.getLabelValue("kisi.kimliknotip") + " : " + getKimliktip() + "");
		}
		if (getAktif() != null) {
			value.append(KeyUtil.getLabelValue("kisi.aktif") + " : " + getAktif().getLabel() + "");
		}
		if (getIletisimtercih() != null) {
			value.append(KeyUtil.getLabelValue("kisi.iletisimtercih") + " : " + getIletisimtercih().getLabel() + "<br />");
		}
		return value.toString();
	}

	@Transient
	public String getIl() {
		try {
			if (getDBOperator().recordCount(Adres.class.getSimpleName(), "o.kisiRef=" + getRID() + " AND o.varsayilan=" + EvetHayir._EVET.getCode()) > 0) {
				Adres adres = (Adres) getDBOperator().load(Adres.class.getSimpleName(), "o.kisiRef=" + getRID() + " AND o.varsayilan=" + EvetHayir._EVET.getCode(), "").get(0);
				if (adres.getSehirRef() != null) {
					return adres.getSehirRef().getAd();
				}
			}
			return "";
		} catch (Exception e) {
			e.printStackTrace();
			return "";
		}
	}

	@Transient
	private String adresilce = "";

	public String getAdresilce() {
		return adresilce;
	}

	public void setAdresilce(String adresilce) {
		this.adresilce = adresilce;
	}

	public void setKimliktip(KimlikTip kimliktip) {
		this.kimliktip = kimliktip;
	}

	@Transient
	public String getAdresililce() {
		try {
			if (getRID() != null) {
				if (getDBOperator().recordCount(Adres.class.getSimpleName(), "o.kisiRef=" + getRID() + " AND o.varsayilan=" + EvetHayir._EVET.getCode()) > 0) {
					Adres adres = (Adres) getDBOperator().load(Adres.class.getSimpleName(), "o.kisiRef=" + getRID() + " AND o.varsayilan=" + EvetHayir._EVET.getCode(), "").get(0);
					if (adres.getIlceRef() != null) {
						setAdresilce(adres.getIlceRef().getAd());
					}
					if (adres.getSehirRef() != null) {
						return adres.getSehirRef().getAd();
					}
				}
			}
			return "";
		} catch (Exception e) {
			e.printStackTrace();
			return "";
		}
	}

	@Transient
	public String getIsYeriBilgisi() {
		try {
			if (getDBOperator().recordCount(KurumKisi.class.getSimpleName(), "o.kisiRef=" + getRID()) > 0) {
				KurumKisi kurumKisi = (KurumKisi) getDBOperator().load(KurumKisi.class.getSimpleName(), "o.kisiRef=" + getRID(), "").get(0);
				if (kurumKisi.getKurumRef() != null) {
					return kurumKisi.getKurumRef().getAd();
				}
			}
			return "";
		} catch (Exception e) {
			e.printStackTrace();
			return "";
		}
	}

	@Transient
	public String getTelefon() {
		try {
			if (getDBOperator().recordCount(Telefon.class.getSimpleName(), "o.kisiRef=" + getRID() + " AND o.varsayilan=" + EvetHayir._EVET.getCode()) > 0) {
				Telefon telefon = (Telefon) getDBOperator().load(Telefon.class.getSimpleName(), "o.kisiRef=" + getRID() + " AND o.varsayilan=" + EvetHayir._EVET.getCode(), "")
						.get(0);
				return telefon.getTelefonno();
			} else {
				return "";
			}
		} catch (Exception e) {
			e.printStackTrace();
			return "";
		}
	}

	@Transient
	public String getEposta() {
		try {
			if (getDBOperator().recordCount(Internetadres.class.getSimpleName(), "o.kisiRef=" + getRID() + " AND o.varsayilan=" + EvetHayir._EVET.getCode()) > 0) {
				Internetadres internetAdres = (Internetadres) getDBOperator().load(Internetadres.class.getSimpleName(),
						"o.kisiRef=" + getRID() + " AND o.varsayilan=" + EvetHayir._EVET.getCode(), "").get(0);
				return internetAdres.getNetadresmetni();
			} else {
				return "";
			}
		} catch (Exception e) {
			e.printStackTrace();
			return "";
		}
	}

}
