package tr.com.arf.toys.db.model.egitim; 
 

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tr.com.arf.framework.utility.tool.KeyUtil;
import tr.com.arf.toys.db.model.base.ToysBaseEntity;
 
@Entity 
@Table(name = "SINAVGOZETMEN") 
public class SinavGozetmen extends ToysBaseEntity { 
 
	private static final long serialVersionUID = 1L; 
 
	@Id 
	@GeneratedValue(strategy = GenerationType.IDENTITY) 
	@Column(updatable = false, unique = true, nullable = false, name = "RID") 
	private Long rID; 
 
	@ManyToOne 
	@JoinColumn(name = "GOZETMENREF") 
	private Gozetmen gozetmenRef; 
	
	@ManyToOne 
	@JoinColumn(name = "SINAVREF") 
	private Sinav sinavRef; 
	
	@Column(name = "ACIKLAMA") 
	private String aciklama; 
	
	public SinavGozetmen() { 
		super(); 
	} 
 
	@Override
	public Long getRID() { 
		return this.rID;  
	}  
	  
	@Override
	public void setRID(Long rID) {  
		this.rID = rID;  
	}   

	public String getAciklama() {
		return aciklama;
	}

	public void setAciklama(String aciklama) {
		this.aciklama = aciklama;
	}

	public Gozetmen getGozetmenRef() {
		return gozetmenRef;
	}

	public void setGozetmenRef(Gozetmen gozetmenRef) {
		this.gozetmenRef = gozetmenRef;
	}

	public Sinav getSinavRef() {
		return sinavRef;
	}

	public void setSinavRef(Sinav sinavRef) {
		this.sinavRef = sinavRef;
	}

	@Override 
	public String toString() { 
		return "tr.com.arf.toys.db.model.egitim.SinavGozetmen[id=" + this.getRID()  + "]"; 
	} 
	@Override 
	public String getUIString() { 
		if (getGozetmenRef()!=null){
			return getGozetmenRef().getUIString();
		}
		return null;
		
	} 
	 
	@Override 
	public void initValues(boolean defaultValues) { 
			this.rID = null; 			this.aciklama=null;
			this.gozetmenRef=null;
			this.sinavRef=null;	}  
	 
	@Override 
	public String getValue() {  
		StringBuilder value = new StringBuilder("Veri : <br />"); 
			if(getGozetmenRef() != null) {
				value.append(KeyUtil.getLabelValue("sinavGozetmen.gozetmenRef") + " : " + getGozetmenRef().getUIString() + "<br />");
			} 
			if(getSinavRef() != null) {
				value.append(KeyUtil.getLabelValue("sinavGozetmen.sinavRef") + " : " + getSinavRef().getUIString() + "<br />");
			} 
			if(getAciklama() != null) {
				value.append(KeyUtil.getLabelValue("sinavGozetmen.aciklama") + " : " + getAciklama() + "<br />");
			} 
		return value.toString(); 
	} 
	 
}	 
