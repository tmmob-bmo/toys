package tr.com.arf.toys.db.model.system; 
 
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import tr.com.arf.framework.utility.tool.DateUtil;
import tr.com.arf.framework.utility.tool.KeyUtil;
import tr.com.arf.toys.db.enumerated.system.PersonelGorevDurum;
import tr.com.arf.toys.db.model.base.ToysBaseEntity;
import tr.com.arf.toys.db.model.gorev.Gorev;
 
@Entity 
@Table(name = "PERSONELBIRIMGOREV") 
public class PersonelBirimGorev extends ToysBaseEntity { 
 
	private static final long serialVersionUID = 1L; 
 
	@Id 
	@GeneratedValue(strategy = GenerationType.IDENTITY) 
	@Column(updatable = false, unique = true, nullable = false, name = "RID") 
	private Long rID; 
 
	@ManyToOne 
	@JoinColumn(name = "PERSONELREF") 
	private Personel personelRef; 
 
	@ManyToOne 
	@JoinColumn(name = "GOREVREF") 
	private Gorev gorevRef; 
 
	@ManyToOne 
	@JoinColumn(name = "BIRIMREF") 
	private Birim birimRef; 
 
	@Temporal(TemporalType.DATE) 
	@Column(name = "BASLANGICTARIH") 
	private java.util.Date baslangictarih; 
 
	@Temporal(TemporalType.DATE) 
	@Column(name = "BITISTARIH") 
	private java.util.Date bitistarih; 
 
	@Column(name = "ACIKLAMA") 
	private String aciklama; 
 
	@Enumerated(EnumType.ORDINAL) 
	@Column(name = "DURUM") 
	private PersonelGorevDurum durum; 
 
 
	public PersonelBirimGorev() { 
		super(); 
	} 
 
	@Override
	public Long getRID() { 
		return this.rID;  
	}  
	  
	@Override
	public void setRID(Long rID) {  
		this.rID = rID;  
	}   
 
	public Personel getPersonelRef() { 
		return personelRef; 
	} 
	 
	public void setPersonelRef(Personel personelRef) { 
		this.personelRef = personelRef; 
	} 
 
	public Gorev getGorevRef() { 
		return gorevRef; 
	} 
	 
	public void setGorevRef(Gorev gorevRef) { 
		this.gorevRef = gorevRef; 
	} 
 
	public Birim getBirimRef() { 
		return birimRef; 
	} 
	 
	public void setBirimRef(Birim birimRef) { 
		this.birimRef = birimRef; 
	} 
 
	public java.util.Date getBaslangictarih() { 
		return baslangictarih; 
	} 
	 
	public void setBaslangictarih(java.util.Date baslangictarih) { 
		this.baslangictarih = baslangictarih; 
	} 
 
	public java.util.Date getBitistarih() { 
		return bitistarih; 
	} 
	 
	public void setBitistarih(java.util.Date bitistarih) { 
		this.bitistarih = bitistarih; 
	} 
 
	public String getAciklama() { 
		return this.aciklama; 
	} 
	 
	public void setAciklama(String aciklama) { 
		this.aciklama = aciklama; 
	} 
 
	public PersonelGorevDurum getDurum() { 
		return this.durum;  
	}  
  
	public void setDurum(PersonelGorevDurum durum) {  
		this.durum = durum;  
	}   
 
 
	@Override 
	public String toString() { 
		return "tr.com.arf.toys.db.model.system.PersonelBirimGorev[id=" + this.getRID()  + "]"; 
	} 
	@Override 
	public String getUIString() { 
		return getAciklama() + ""; 
	} 
	 
	@Override 
	public void initValues(boolean defaultValues) { 
		if (defaultValues) { 
				// TODO Initialize with default values 
		} else { 
			this.rID = null; 			this.personelRef = null; 			this.gorevRef = null; 			this.birimRef = null; 			this.baslangictarih = null; 			this.bitistarih = null; 			this.aciklama = ""; 			this.durum = PersonelGorevDurum._NULL; 		}  
	}  
	 
	@Override 
	public String getValue() {  
		StringBuilder value = new StringBuilder(""); 
			if(getPersonelRef() != null) {
				value.append(KeyUtil.getLabelValue("personelBirimGorev.personelRef") + " : " + getPersonelRef().getUIString() + "<br />");
			} 
			if(getGorevRef() != null) {
				value.append(KeyUtil.getLabelValue("personelBirimGorev.gorevRef") + " : " + getGorevRef().getUIString() + "<br />");
			} 
			if(getBirimRef() != null) {
				value.append(KeyUtil.getLabelValue("personelBirimGorev.birimRef") + " : " + getBirimRef().getUIString() + "<br />");
			} 
			if(getBaslangictarih() != null) {
				value.append(KeyUtil.getLabelValue("personelBirimGorev.baslangictarih") + " : " + DateUtil.dateToDMYHMS(getBaslangictarih()) + "<br />");
			} 
			if(getBitistarih() != null) {
				value.append(KeyUtil.getLabelValue("personelBirimGorev.bitistarih") + " : " + DateUtil.dateToDMYHMS(getBitistarih()) + "<br />");
			} 
			if(getAciklama() != null) {
				value.append(KeyUtil.getLabelValue("personelBirimGorev.aciklama") + " : " + getAciklama() + "<br />");
			} 
			if(getDurum() != null) {
				value.append(KeyUtil.getLabelValue("personelBirimGorev.durum") + " : " + getDurum() + "");
			} 
		return value.toString(); 
	} 
	 
}	 
