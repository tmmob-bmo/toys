package tr.com.arf.toys.db.model.egitim; 
 
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tr.com.arf.framework.utility.tool.KeyUtil;
import tr.com.arf.toys.db.enumerated.egitim.Cevap;
import tr.com.arf.toys.db.enumerated.egitim.Soru;
import tr.com.arf.toys.db.model.base.ToysBaseEntity;
 
@Entity 
@Table(name = "EGITIMDEGERLENDIRME") 
public class Egitimdegerlendirme extends ToysBaseEntity { 
 
	private static final long serialVersionUID = 1L; 
 
	@Id 
	@GeneratedValue(strategy = GenerationType.IDENTITY) 
	@Column(updatable = false, unique = true, nullable = false, name = "RID") 
	private Long rID; 
 
	@ManyToOne 
	@JoinColumn(name = "EGITIMKATILIMCIREF") 
	private Egitimkatilimci egitimkatilimciRef; 
 
	@Enumerated(EnumType.ORDINAL) 
	@Column(name = "SORU") 
	private Soru soru; 
 
	@Enumerated(EnumType.ORDINAL) 
	@Column(name = "CEVAP") 
	private Cevap cevap; 
 
	@Column(name = "ACIKLAMA") 
	private String aciklama; 
	
	@ManyToOne 
	@JoinColumn(name = "EGITIMREF") 
	private Egitim egitimRef; 
 
 
	public Egitimdegerlendirme() { 
		super(); 
	} 
 
	@Override
	public Long getRID() { 
		return this.rID;  
	}  
	  
	@Override
	public void setRID(Long rID) {  
		this.rID = rID;  
	}   
 
	public Egitimkatilimci getEgitimkatilimciRef() { 
		return egitimkatilimciRef; 
	} 
	 
	public void setEgitimkatilimciRef(Egitimkatilimci egitimkatilimciRef) { 
		this.egitimkatilimciRef = egitimkatilimciRef; 
	} 
 
	public Soru getSoru() { 
		return this.soru;  
	}  
  
	public void setSoru(Soru soru) {  
		this.soru = soru;  
	}   
 
	public Cevap getCevap() { 
		return this.cevap;  
	}  
  
	public void setCevap(Cevap cevap) {  
		this.cevap = cevap;  
	}   
 
	public String getAciklama() { 
		return this.aciklama; 
	} 
	 
	public void setAciklama(String aciklama) { 
		this.aciklama = aciklama; 
	} 
 
	public Egitim getEgitimRef() {
		return egitimRef;
	}

	public void setEgitimRef(Egitim egitimRef) {
		this.egitimRef = egitimRef;
	}

	@Override 
	public String toString() { 
		return "tr.com.arf.toys.db.model.egitim.Egitimdegerlendirme[id=" + this.getRID()  + "]"; 
	} 
	@Override 
	public String getUIString() { 
		return getAciklama() + ""; 
	} 
	 
	@Override 
	public void initValues(boolean defaultValues) { 
			this.rID = null; 
			this.egitimkatilimciRef = null; 
			this.soru = Soru._NULL; 
			this.cevap = Cevap._NULL; 
			this.aciklama = ""; 
			this.egitimRef=null;
	}  
	 
	@Override 
	public String getValue() {  
		StringBuilder value = new StringBuilder("Veri : <br />"); 
			if(getEgitimkatilimciRef() != null) {
				value.append(KeyUtil.getLabelValue("egitimdegerlendirme.egitimkatilimciRef") + " : " + getEgitimkatilimciRef().getUIString() + "<br />");
			} 
			if(getEgitimRef() != null) {
				value.append(KeyUtil.getLabelValue("egitimdegerlendirme.egitimRef") + " : " + getEgitimRef().getUIString() + "<br />");
			} 
			if(getSoru() != null) {
				value.append(KeyUtil.getLabelValue("egitimdegerlendirme.soru") + " : " + getSoru().getLabel() + "<br />");
			} 
			if(getCevap() != null) {
				value.append(KeyUtil.getLabelValue("egitimdegerlendirme.cevap") + " : " + getCevap().getLabel() + "<br />");
			} 
			if(getAciklama() != null) {
				value.append(KeyUtil.getLabelValue("egitimdegerlendirme.aciklama") + " : " + getAciklama() + "");
			} 
		return value.toString(); 
	} 
	 
}	 
