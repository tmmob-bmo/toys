package tr.com.arf.toys.db.model.etkinlik; 
 
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tr.com.arf.framework.utility.tool.KeyUtil;
import tr.com.arf.toys.db.enumerated.etkinlik.EtkinlikKurumTuru;
import tr.com.arf.toys.db.model.base.ToysBaseEntity;
import tr.com.arf.toys.db.model.kisi.Kurum;
 
@Entity 
@Table(name = "ETKINLIKKURUM") 
public class EtkinlikKurum extends ToysBaseEntity { 
 
	private static final long serialVersionUID = 1L; 
 
	@Id 
	@GeneratedValue(strategy = GenerationType.IDENTITY) 
	@Column(updatable = false, unique = true, nullable = false, name = "RID") 
	private Long rID; 
 
	@ManyToOne 
	@JoinColumn(name = "ETKINLIKREF") 
	private Etkinlik etkinlikRef; 
 
	@ManyToOne 
	@JoinColumn(name = "KURUMREF") 
	private Kurum kurumRef; 
 
	@Enumerated(EnumType.ORDINAL) 
	@Column(name = "TURU") 
	private EtkinlikKurumTuru turu; 
 
 
	public EtkinlikKurum() { 
		super(); 
	} 
 
	public Long getRID() { 
		return this.rID;  
	}  
	  
	public void setRID(Long rID) {  
		this.rID = rID;  
	}   
 
	public Etkinlik getEtkinlikRef() { 
		return etkinlikRef; 
	} 
	 
	public void setEtkinlikRef(Etkinlik etkinlikRef) { 
		this.etkinlikRef = etkinlikRef; 
	} 
 
	public Kurum getKurumRef() { 
		return kurumRef; 
	} 
	 
	public void setKurumRef(Kurum kurumRef) { 
		this.kurumRef = kurumRef; 
	} 
 
	public EtkinlikKurumTuru getTuru() { 
		return this.turu;  
	}  
  
	public void setTuru(EtkinlikKurumTuru turu) {  
		this.turu = turu;  
	}   
 
 
	@Override 
	public String toString() { 
		return "tr.com.arf.toys.db.model.etkinlik.EtkinlikKurum[id=" + this.getRID()  + "]"; 
	} 
	@Override 
	public String getUIString() { 
		return getEtkinlikRef() + ""; 
	} 
	 
	@Override 
	public void initValues(boolean defaultValues) { 
			this.rID = null; 			this.etkinlikRef = null; 			this.kurumRef = null; 			this.turu = EtkinlikKurumTuru._NULL; 	}  
	 
	@Override 
	public String getValue() {  
		StringBuilder value = new StringBuilder("Veri : <br />"); 
			if(getEtkinlikRef() != null) 
				value.append(KeyUtil.getLabelValue("etkinlikkurum.etkinlikRef") + " : " + getEtkinlikRef().getUIString() + "<br />"); 
			if(getKurumRef() != null) 
				value.append(KeyUtil.getLabelValue("etkinlikkurum.kurumRef") + " : " + getKurumRef().getUIString() + "<br />"); 
			if(getTuru() != null) 
				value.append(KeyUtil.getLabelValue("etkinlikkurum.turu") + " : " + getTuru().getLabel() + ""); 
		return value.toString(); 
	} 
	 
}	 
