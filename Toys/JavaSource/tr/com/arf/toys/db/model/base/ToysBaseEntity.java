package tr.com.arf.toys.db.model.base;

import tr.com.arf.framework.db._management.DBOperator;
import tr.com.arf.framework.db.model._base.BaseEntity;
import tr.com.arf.toys.service.application.ManagedBeanLocator;

public abstract class ToysBaseEntity extends BaseEntity {
	  
	private static final long serialVersionUID = -7998107723055755674L;

	public DBOperator getDBOperator(){
		return ManagedBeanLocator.locateSessionController().getDBOperator();
	}
}
