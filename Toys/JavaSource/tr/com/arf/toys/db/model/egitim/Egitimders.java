package tr.com.arf.toys.db.model.egitim; 
 
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import tr.com.arf.framework.utility.tool.DateUtil;
import tr.com.arf.framework.utility.tool.KeyUtil;
import tr.com.arf.toys.db.model.base.ToysBaseEntity;
 
 
@Entity 
@Table(name = "EGITIMDERS") 
public class Egitimders extends ToysBaseEntity { 
 
	private static final long serialVersionUID = 1L; 
 
	@Id 
	@GeneratedValue(strategy = GenerationType.IDENTITY) 
	@Column(updatable = false, unique = true, nullable = false, name = "RID") 
	private Long rID; 
 
	@ManyToOne 
	@JoinColumn(name = "EGITIMREF") 
	private Egitim egitimRef; 
 
	@ManyToOne 
	@JoinColumn(name = "EGITIMOGRETMENREF") 
	private Egitimogretmen egitimogretmenRef; 
 
	@Temporal(TemporalType.TIMESTAMP) 
	@Column(name = "ZAMAN") 
	private java.util.Date zaman; 
 
	@Column(name = "ACIKLAMA") 
	private String aciklama; 
	
	@Column(name = "SURE") 
	private BigDecimal sure; 
 
 
	public Egitimders() { 
		super(); 
	} 
 
	@Override
	public Long getRID() { 
		return this.rID;  
	}  
	  
	@Override
	public void setRID(Long rID) {  
		this.rID = rID;  
	}   
 
	public Egitim getEgitimRef() { 
		return egitimRef; 
	} 
	 
	public void setEgitimRef(Egitim egitimRef) { 
		this.egitimRef = egitimRef; 
	} 
 
	public Egitimogretmen getEgitimogretmenRef() { 
		return egitimogretmenRef; 
	} 
	 
	public void setEgitimogretmenRef(Egitimogretmen egitimogretmenRef) { 
		this.egitimogretmenRef = egitimogretmenRef; 
	} 
 
	public java.util.Date getZaman() { 
		return zaman; 
	} 
	 
	public void setZaman(java.util.Date zaman) { 
		this.zaman = zaman; 
	} 
 
	public String getAciklama() { 
		return this.aciklama; 
	} 
	 
	public void setAciklama(String aciklama) { 
		this.aciklama = aciklama; 
	} 
	
	public BigDecimal getSure() { 
		return this.sure; 
	} 
	 
	public void setSure(BigDecimal sure) { 
		this.sure = sure; 
	}
 
 
	@Override 
	public String toString() { 
		return "tr.com.arf.toys.db.model.egitim.Egitimders[id=" + this.getRID()  + "]"; 
	} 
	@Override 
	public String getUIString() { 
		return getAciklama() + ""; 
	} 
	 
	@Override 
	public void initValues(boolean defaultValues) { 
			this.rID = null; 			this.egitimRef = null; 			this.egitimogretmenRef = null; 			this.zaman = null; 			this.aciklama = ""; 
			this.sure= null;	}  
	 
	@Override 
	public String getValue() {  
		StringBuilder value = new StringBuilder("Veri : <br />"); 
			if(getEgitimRef() != null) {
				value.append(KeyUtil.getLabelValue("egitimders.egitimRef") + " : " + getEgitimRef().getUIString() + "<br />");
			} 
			if(getEgitimogretmenRef() != null) {
				value.append(KeyUtil.getLabelValue("egitimders.egitimogretmenRef") + " : " + getEgitimogretmenRef().getUIString() + "<br />");
			} 
			if(getZaman() != null) {
				value.append(KeyUtil.getLabelValue("egitimders.zaman") + " : " + DateUtil.dateToDMYHMS(getZaman()) + "<br />");
			} 
			if(getAciklama() != null) {
				value.append(KeyUtil.getLabelValue("egitimders.aciklama") + " : " + getAciklama() + "<br />");
			} 
			if(getSure() != null) {
				value.append(KeyUtil.getLabelValue("egitimders.sure") + " : " + getSure() + "");
			} 
		return value.toString(); 
	} 
	 
}	 
