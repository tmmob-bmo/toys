package tr.com.arf.toys.db.model.iletisim; 
 
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tr.com.arf.framework.utility.exception.data.DBException;
import tr.com.arf.framework.utility.tool.KeyUtil;
import tr.com.arf.toys.db.enumerated.iletisim.ReferansTipi;
import tr.com.arf.toys.db.model.base.ToysBaseEntity;
import tr.com.arf.toys.db.model.egitim.Egitim;
import tr.com.arf.toys.db.model.egitim.Egitimogretmen;
import tr.com.arf.toys.db.model.etkinlik.Etkinlik;
import tr.com.arf.toys.db.model.kisi.IsYeriTemsilcileri;
import tr.com.arf.toys.db.model.kisi.Kisi;
import tr.com.arf.toys.db.model.kisi.KomisyonDonem;
import tr.com.arf.toys.db.model.kisi.KurulDonem;
import tr.com.arf.toys.db.model.kisi.Kurum;
import tr.com.arf.toys.db.model.system.Birim;
import tr.com.arf.toys.db.model.uye.Uye;
 
@Entity 
@Table(name = "ILETISIMLISTEDETAY") 
public class Iletisimlistedetay extends ToysBaseEntity { 
 
	private static final long serialVersionUID = 1L; 
 
	@Id 
	@GeneratedValue(strategy = GenerationType.IDENTITY) 
	@Column(updatable = false, unique = true, nullable = false, name = "RID") 
	private Long rID; 
 
	@Enumerated(EnumType.ORDINAL) 
	@Column(name = "REFERANSTIPI") 
	private ReferansTipi referansTipi; 
 
	@Column(name = "REFERANS") 
	private Long Referans; 
 
	@ManyToOne 
	@JoinColumn(name = "ILETISIMLISTEREF") 
	private Iletisimliste iletisimlisteRef; 
 
 
	public Iletisimlistedetay() { 
		super(); 
	} 
 
	@Override
	public Long getRID() { 
		return this.rID;  
	}  
	  
	@Override
	public void setRID(Long rID) {  
		this.rID = rID;  
	}   
  
	public ReferansTipi getReferansTipi() {
		return referansTipi;
	}

	public void setReferansTipi(ReferansTipi referansTipi) {
		this.referansTipi = referansTipi;
	}

	public Long getReferans() { 
		return this.Referans; 
	} 
	 
	public void setReferans(Long Referans) { 
		this.Referans = Referans; 
	} 
 
	public Iletisimliste getIletisimlisteRef() { 
		return iletisimlisteRef; 
	} 
	 
	public void setIletisimlisteRef(Iletisimliste iletisimlisteRef) { 
		this.iletisimlisteRef = iletisimlisteRef; 
	} 
	
	public String getIletisimListesi(){
		if (getRID()==null){
			return null ;
		}else {
			try {
				if (getReferansTipi().getCode()==ReferansTipi._KISI.getCode()){
					if (getDBOperator().recordCount(Kisi.class.getSimpleName(),"o.rID=" + getReferans())>0){
						Kisi kisi=(Kisi) getDBOperator().load(Kisi.class.getSimpleName(),"o.rID=" + getReferans(),"o.rID").get(0);
						return kisi.getUIStringShort();
					}
				} else if (getReferansTipi().getCode()==ReferansTipi._UYE.getCode()){
					if (getDBOperator().recordCount(Uye.class.getSimpleName(),"o.rID=" + getReferans())>0){
						Uye uye=(Uye) getDBOperator().load(Uye.class.getSimpleName(),"o.rID=" + getReferans(),"o.rID").get(0);
						return uye.getKisiRef().getUIStringShort();
					}
				} else if (getReferansTipi().getCode()==ReferansTipi._BIRIM.getCode()){
					if (getDBOperator().recordCount(Birim.class.getSimpleName(),"o.rID=" + getReferans())>0){
						Birim birim=(Birim) getDBOperator().load(Birim.class.getSimpleName(),"o.rID=" + getReferans(),"o.rID").get(0);
						return birim.getUIString();
					}
				} else if (getReferansTipi().getCode()==ReferansTipi._KURUM.getCode()){
					if (getDBOperator().recordCount(Kurum.class.getSimpleName(),"o.rID=" + getReferans())>0){
						Kurum kurum=(Kurum) getDBOperator().load(Kurum.class.getSimpleName(),"o.rID=" + getReferans(),"o.rID").get(0);
						return kurum.getUIString();
					}
				} else if (getReferansTipi().getCode()==ReferansTipi._KURUL.getCode()){
						if (getDBOperator().recordCount(KurulDonem.class.getSimpleName(),"o.rID=" + getReferans())>0){
							KurulDonem kurul=(KurulDonem) getDBOperator().load(KurulDonem.class.getSimpleName(),"o.rID=" + getReferans(),"o.rID").get(0);
							return kurul.getUIString();
						}
				} else if (getReferansTipi().getCode()==ReferansTipi._KOMISYON.getCode()){
					if (getDBOperator().recordCount(KomisyonDonem.class.getSimpleName(),"o.rID=" + getReferans())>0){
						KomisyonDonem komisyon=(KomisyonDonem) getDBOperator().load(KomisyonDonem.class.getSimpleName(),"o.rID=" + getReferans(),"o.rID").get(0);
						return komisyon.getUIString();
					}
				} else if (getReferansTipi().getCode()==ReferansTipi._EGITMEN.getCode()){
					if (getDBOperator().recordCount(Egitimogretmen.class.getSimpleName(),"o.rID=" + getReferans())>0){
						Egitimogretmen egitmen=(Egitimogretmen) getDBOperator().load(Egitimogretmen.class.getSimpleName(),"o.rID=" + getReferans(),"o.rID").get(0);
						return egitmen.getUIStringShort();
					}
				} else if (getReferansTipi().getCode()==ReferansTipi._EGITIMKATILIMCI.getCode()){
					if (getDBOperator().recordCount(Egitim.class.getSimpleName(),"o.rID=" + getReferans())>0){
						Egitim egitim=(Egitim) getDBOperator().load(Egitim.class.getSimpleName(),"o.rID=" + getReferans(),"o.rID").get(0);
						return egitim.getUIString();
					}
				} else if (getReferansTipi().getCode()==ReferansTipi._ETKINLIKKATILIMCI.getCode()){
					if (getDBOperator().recordCount(Etkinlik.class.getSimpleName(),"o.rID=" + getReferans())>0){
						Etkinlik etkinlik=(Etkinlik) getDBOperator().load(Etkinlik.class.getSimpleName(),"o.rID=" + getReferans(),"o.rID").get(0);
						return etkinlik.getUIString();
					}
				} else if (getReferansTipi().getCode()==ReferansTipi._ISYERITEMSILCISI.getCode()){
					if (getDBOperator().recordCount(IsYeriTemsilcileri.class.getSimpleName(),"o.rID=" + getReferans())>0){
						IsYeriTemsilcileri isyeritemsilcileri=(IsYeriTemsilcileri) getDBOperator().load(IsYeriTemsilcileri.class.getSimpleName(),"o.rID=" + getReferans(),"o.rID").get(0);
						return isyeritemsilcileri.getUIString();
					}
				}
			} catch (DBException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return null;
	}
 
 
	@Override 
	public String toString() { 
		return "tr.com.arf.toys.db.model.iletisim.Iletisimlistedetay[id=" + this.getRID()  + "]"; 
	} 
	@Override 
	public String getUIString() { 
		return getReferans() + ""; 
	} 
	 
	@Override 
	public void initValues(boolean defaultValues) { 
			this.rID = null; 			this.referansTipi = ReferansTipi._NULL; 			this.Referans = null; 			this.iletisimlisteRef = null; 	}  
	 
	@Override 
	public String getValue() {  
		StringBuilder value = new StringBuilder("Veri : <br />"); 
			if(getReferansTipi() != null) {
				value.append(KeyUtil.getLabelValue("iletisimlistedetay.referansTipi") + " : " + getReferansTipi().getLabel() + "<br />");
			} 
			if(getReferans() != 0) {
				value.append(KeyUtil.getLabelValue("iletisimlistedetay.Referans") + " : " + getReferans() + "<br />");
			} 
			if(getIletisimlisteRef() != null) {
				value.append(KeyUtil.getLabelValue("iletisimlistedetay.iletisimlisteRef") + " : " + getIletisimlisteRef().getUIString() + "");
			} 
		return value.toString(); 
	} 
	 
}	 
