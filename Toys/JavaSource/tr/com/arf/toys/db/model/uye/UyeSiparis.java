package tr.com.arf.toys.db.model.uye;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.*;

import tr.com.arf.framework.utility.tool.KeyUtil;
import tr.com.arf.toys.db.enumerated.uye.SiparisDurum;
import tr.com.arf.toys.db.model.base.ToysBaseEntity;

@Entity
@Table(name = "UYESIPARIS")
public class UyeSiparis extends ToysBaseEntity {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(updatable = false, unique = true, nullable = false, name = "RID")
	private Long rID;

	@ManyToOne
	@JoinColumn(name = "UYEREF")
	private Uye uyeRef;

	@Column(name = "SIPARISNO")
	private String siparisno;

	@Column(name = "TUTAR")
	private BigDecimal tutar;

	@Enumerated(EnumType.ORDINAL)
	@Column(name = "DURUM")
	private SiparisDurum durum;

	@Column(name = "GARANTISECURITYCODE")
	private String garantiSecurityCode;

    @Column(name =  "MDSTATUS")
    private String mdstatus;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "TARIH")
    private Date tarih;

	public UyeSiparis() {
		super();
	}

	@Override
	public Long getRID() {
		return this.rID;
	}

	@Override
	public void setRID(Long rID) {
		this.rID = rID;
	}

	public Uye getUyeRef() {
		return uyeRef;
	}

	public void setUyeRef(Uye uyeRef) {
		this.uyeRef = uyeRef;
	}

	public String getSiparisno() {
		return this.siparisno;
	}

	public void setSiparisno(String siparisno) {
		this.siparisno = siparisno;
	}

	public BigDecimal getTutar() {
		return this.tutar;
	}

	public void setTutar(BigDecimal tutar) {
		this.tutar = tutar;
	}

	public String getTutarGaranti() {
		return getTutar().toString().replace(".", "");
	}

	public SiparisDurum getDurum() {
		return this.durum;
	}

	public void setDurum(SiparisDurum durum) {
		this.durum = durum;
	}

	public String getGarantiSecurityCode() {
		return garantiSecurityCode;
	}

	public void setGarantiSecurityCode(String garantiSecurityCode) {
		this.garantiSecurityCode = garantiSecurityCode;
	}

    public String getMdstatus() {
        return mdstatus;
    }

    public void setMdstatus(String mdstatus) {
        this.mdstatus = mdstatus;
    }

    public Date getTarih() {
        return tarih;
    }

    public void setTarih(Date tarih) {
        this.tarih = tarih;
    }

    public String getMdstatusText() {
        if (mdstatus != null) {
            switch (mdstatus) {
                case "0":
                    return "Başarısız - 3-D Secure imzası geçersiz.";
                case "1":
                    return "Başarılı - Tam Doğrulama";
                case "2":
                    return "Başarılı - Kart Sahibi veya bankası sisteme kayıtlı değil";
                case "3":
                    return "Başarılı - Kartın bankası sisteme kayıtlı değil";
                case "4":
                    return "Başarılı - Doğrulama denemesi, kart sahibi sisteme daha sonra kayıt olmayı seçmis";
                case "5":
                    return "Başarısız - Doğrulama yapılamıyor.";
                case "6":
                    return "Başarısız - 3-D Secure Hatası";
                case "7":
                    return "Başarısız - Sistem Hatası";
                case "8":
                    return "Başarısız - Bilinmeyen Kart No";
            }
        }
        return "---";
    }

    @Override
	public String toString() {
		return "tr.com.arf.toys.db.model.uye.UyeSiparis[id=" + this.getRID()  + "]";
	}

	@Override
	public String getUIString() {
		return getSiparisno() + "";
	}

	@Override
	public void initValues(boolean defaultValues) {
			this.rID = null;
			this.uyeRef = null;
			this.siparisno = "";
			this.tutar = null;
			this.durum = SiparisDurum._NULL;
			this.garantiSecurityCode = null;
			this.mdstatus = null;
			this.tarih = null;
	}

	@Override
	public String getValue() {
		StringBuilder value = new StringBuilder("Veri : <br />");
			if(getUyeRef()!= null) {
				value.append(KeyUtil.getLabelValue("uyesiparis.uyeRef") + " : " + getUyeRef().getUIString() + "<br />");
			}
			if(getSiparisno() != null) {
				value.append(KeyUtil.getLabelValue("uyesiparis.siparisno") + " : " + getSiparisno() + "<br />");
			}
			if(getTutar() != null) {
				value.append(KeyUtil.getLabelValue("uyesiparis.tutar") + " : " + getTutar() + "<br />");
			}
			if(getDurum() != null) {
				value.append(KeyUtil.getLabelValue("uyesiparis.durum") + " : " + getDurum().getLabel() + "");
			}
			if(getGarantiSecurityCode() != null) {
				value.append(KeyUtil.getLabelValue("uyesiparis.garantiSecurityCode") + " : " + getGarantiSecurityCode() + "<br />");
			}
			if(getMdstatus() != null) {
				value.append(KeyUtil.getLabelValue("uyesiparis.mdstatus") + " : " + getMdstatus() + "<br />");
			}
			if(getTarih() != null) {
				value.append(KeyUtil.getLabelValue("uyesiparis.tarih") + " : " + getTarih() + "<br />");
			}
		return value.toString();
	}

}
