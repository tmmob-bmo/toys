package tr.com.arf.toys.db.model.egitim; 
 
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tr.com.arf.framework.utility.tool.KeyUtil;
import tr.com.arf.toys.db.model.base.ToysBaseEntity;
 
 
@Entity 
@Table(name = "EGITIMDOSYA") 
public class Egitimdosya extends ToysBaseEntity { 
 
	private static final long serialVersionUID = 1L; 
 
	@Id 
	@GeneratedValue(strategy = GenerationType.IDENTITY) 
	@Column(updatable = false, unique = true, nullable = false, name = "RID") 
	private Long rID; 
 
	@ManyToOne 
	@JoinColumn(name = "EGITIMTANIMREF") 
	private Egitimtanim egitimtanimRef; 
 
	@Column(name = "AD") 
	private String ad; 
 
	@Column(name = "PATH") 
	private String path; 
 
 
	public Egitimdosya() { 
		super(); 
	} 
 
	public Long getRID() { 
		return this.rID;  
	}  
	  
	public void setRID(Long rID) {  
		this.rID = rID;  
	}   
 
	public Egitimtanim getEgitimtanimRef() {
		return egitimtanimRef;
	}

	public void setEgitimtanimRef(Egitimtanim egitimtanimRef) {
		this.egitimtanimRef = egitimtanimRef;
	}

	public String getAd() { 
		return this.ad; 
	} 
	 
	public void setAd(String ad) { 
		this.ad = ad; 
	} 
 
	public String getPath() { 
		return this.path; 
	} 
	 
	public void setPath(String path) { 
		this.path = path; 
	} 
	
	@Override 
	public String toString() { 
		return "tr.com.arf.toys.db.model.egitim.Egitimdosya[id=" + this.getRID()  + "]"; 
	} 
	@Override 
	public String getUIString() { 
		return getAd() + ""; 
	} 
	 
	@Override 
	public void initValues(boolean defaultValues) { 
			this.rID = null; 
			this.egitimtanimRef = null; 
			this.ad = ""; 
			this.path = ""; 
	}  
	 
	@Override 
	public String getValue() {  
		StringBuilder value = new StringBuilder("Veri : <br />"); 
			if(getEgitimtanimRef() != null) 
				value.append(KeyUtil.getLabelValue("egitimdosya.egitimtanimRef") + " : " + getEgitimtanimRef().getUIString() + "<br />"); 
			if(getAd() != null) 
				value.append(KeyUtil.getLabelValue("egitimdosya.ad") + " : " + getAd() + "<br />"); 
			if(getPath() != null) 
				value.append(KeyUtil.getLabelValue("egitimdosya.path") + " : " + getPath() + ""); 
		return value.toString(); 
	} 
	 
}	 
