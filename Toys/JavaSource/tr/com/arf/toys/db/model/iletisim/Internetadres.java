package tr.com.arf.toys.db.model.iletisim; 
 
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tr.com.arf.framework.db.enumerated._common.EvetHayir;
import tr.com.arf.framework.utility.tool.KeyUtil;
import tr.com.arf.toys.db.enumerated.iletisim.NetAdresTuru;
import tr.com.arf.toys.db.model.base.ToysBaseEntity;
import tr.com.arf.toys.db.model.kisi.Kisi;
import tr.com.arf.toys.db.model.kisi.Kurum;
 
@Entity 
@Table(name = "INTERNETADRES") 
public class Internetadres extends ToysBaseEntity { 
 
	private static final long serialVersionUID = 1L; 
 
	@Id 
	@GeneratedValue(strategy = GenerationType.IDENTITY) 
	@Column(updatable = false, unique = true, nullable = false, name = "RID") 
	private Long rID; 
 
	@ManyToOne  
	@JoinColumn(name = "KISIREF") 
	private Kisi kisiRef; 
 
	@ManyToOne 
	@JoinColumn(name = "KURUMREF") 
	private Kurum kurumRef; 
 
	@Column(name = "NETADRESMETNI") 
	private String netadresmetni; 
 
	@Enumerated(EnumType.ORDINAL) 
	@Column(name = "NETADRESTURU") 
	private NetAdresTuru netadresturu; 
 
	@Enumerated(EnumType.ORDINAL)
	@Column(name = "VARSAYILAN") 
	private EvetHayir varsayilan;
 
	
	public Internetadres() { 
		super(); 
		this.varsayilan = EvetHayir._HAYIR;
	} 
 
	@Override
	public Long getRID() { 
		return this.rID;  
	}  
	  
	@Override
	public void setRID(Long rID) {  
		this.rID = rID;  
	}   
 
	public Kisi getKisiRef() { 
		return kisiRef; 
	} 
	 
	public void setKisiRef(Kisi kisiRef) { 
		this.kisiRef = kisiRef; 
	} 
 
	public Kurum getKurumRef() { 
		return kurumRef; 
	} 
	 
	public void setKurumRef(Kurum kurumRef) { 
		this.kurumRef = kurumRef; 
	} 
 
	public String getNetadresmetni() { 
		return this.netadresmetni; 
	} 
	 
	public void setNetadresmetni(String netadresmetni) { 
		this.netadresmetni = netadresmetni; 
	} 
 
	public NetAdresTuru getNetadresturu() { 
		return this.netadresturu;  
	}  
  
	public void setNetadresturu(NetAdresTuru netadresturu) {  
		this.netadresturu = netadresturu;  
	}   
 
    public EvetHayir getVarsayilan() {
		return varsayilan;
	}

	public void setVarsayilan(EvetHayir varsayilan) {
		this.varsayilan = varsayilan;
	}
	
	@Override 
	public String toString() { 
		return "tr.com.arf.toys.db.model.iletisim.Internetadres[id=" + this.getRID()  + "]"; 
	} 
	@Override 
	public String getUIString() { 
		return getNetadresmetni() + ""; 
	} 
	 
	@Override 
	public void initValues(boolean defaultValues) { 
		if (defaultValues) { 
				// TODO Initialize with default values 
		} else { 
			this.rID = null; 			this.kisiRef = null; 			this.kurumRef = null; 			this.netadresmetni = ""; 			this.netadresturu = NetAdresTuru._NULL; 
			this.varsayilan = EvetHayir._HAYIR;		}  
	}  
	 
	@Override 
	public String getValue() {  
		StringBuilder value = new StringBuilder(""); 
			if(getKisiRef() != null) {
				value.append(KeyUtil.getLabelValue("internetadres.kisiRef") + " : " + getKisiRef().getUIString() + "<br />");
			} 
			if(getKurumRef() != null) {
				value.append(KeyUtil.getLabelValue("internetadres.kurumRef") + " : " + getKurumRef().getUIString() + "<br />");
			} 
			if(getNetadresmetni() != null) {
				value.append(KeyUtil.getLabelValue("internetadres.netadresmetni") + " : " + getNetadresmetni() + "<br />");
			} 
			if(getVarsayilan() != null) {
				value.append(KeyUtil.getLabelValue("internetadres.varsayilan") + " : " + getVarsayilan() + "<br />");
			} 
			if(getNetadresturu() != null) {
				value.append(KeyUtil.getLabelValue("internetadres.netadresturu") + " : " + getNetadresturu() + "");
			} 
		return value.toString(); 
	} 
	 
}	 
