package tr.com.arf.toys.db.model.evrak;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import tr.com.arf.framework.utility.tool.DateUtil;
import tr.com.arf.framework.utility.tool.KeyUtil;
import tr.com.arf.toys.db.enumerated.evrak.AciliyetTuru;
import tr.com.arf.toys.db.enumerated.evrak.EvrakDurum;
import tr.com.arf.toys.db.enumerated.evrak.EvrakHareketTuru;
import tr.com.arf.toys.db.enumerated.evrak.EvrakTuru;
import tr.com.arf.toys.db.enumerated.evrak.GizlilikDerecesi;
import tr.com.arf.toys.db.enumerated.evrak.HazirEvrakTuru;
import tr.com.arf.toys.db.enumerated.evrak.TeslimTuru;
import tr.com.arf.toys.db.model.base.ToysBaseEntity;
import tr.com.arf.toys.db.model.kisi.Kisi;
import tr.com.arf.toys.db.model.kisi.Kurum;
import tr.com.arf.toys.db.model.system.Birim;
import tr.com.arf.toys.db.model.system.Personel;
import tr.com.arf.toys.db.model.uye.Uye;

@Entity
@Table(name = "EVRAK")
public class Evrak extends ToysBaseEntity {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(updatable = false, unique = true, nullable = false, name = "RID")
	private Long rID;

	@Column(name = "YIL")
	private int yil;

	@Enumerated(EnumType.ORDINAL)
	@Column(name = "EVRAKHAREKETTURU")
	private EvrakHareketTuru evrakHareketTuru;

	@Enumerated(EnumType.ORDINAL)
	@Column(name = "HAZIREVRAKTURU")
	private HazirEvrakTuru hazirEvrakTuru;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "KAYITTARIHI")
	private java.util.Date kayittarihi;

	@Column(name = "KAYITNO")
	private String kayitno;

	@ManyToOne
	@JoinColumn(name = "GONDERENKURUMREF")
	private Kurum gonderenKurumRef;

	@ManyToOne
	@JoinColumn(name = "GONDERENKISIREF")
	private Kisi gonderenkisiRef;

	@ManyToOne
	@JoinColumn(name = "DOSYAKODUREF")
	private Dosyakodu dosyaKoduRef;

	@Transient
	private Dosyakodu dosyaKoduRefPrev;

	@ManyToOne
	@JoinColumn(name = "SABLONREF")
	private Dolasimsablon sablonRef;

	@Column(name = "KONUSU")
	private String konusu;

	@Temporal(TemporalType.DATE)
	@Column(name = "EVRAKTARIHI")
	private java.util.Date evraktarihi;

	@Column(name = "EVRAKNO")
	private String evrakno;

	@Enumerated(EnumType.ORDINAL)
	@Column(name = "EVRAKTURU")
	private EvrakTuru evrakturu;

	@Enumerated(EnumType.ORDINAL)
	@Column(name = "TESLIMTURU")
	private TeslimTuru teslimturu;

	@Enumerated(EnumType.ORDINAL)
	@Column(name = "GIZLILIKDERECESI")
	private GizlilikDerecesi gizlilikderecesi;

	@Enumerated(EnumType.ORDINAL)
	@Column(name = "ACILIYETTURU")
	private AciliyetTuru aciliyetturu;

	@Enumerated(EnumType.ORDINAL)
	@Column(name = "DURUM")
	private EvrakDurum durum;

	@ManyToOne
	@JoinColumn(name = "HAZIRLAYANPERSONELREF")
	private Personel hazirlayanPersonelRef;

	@ManyToOne
	@JoinColumn(name = "SONDOLASIMSABLONDETAYREF")
	private Dolasimsablondetay sonDolasimSablonDetayRef;

	@Column(name = "TAKDIMMAKAMI")
	private String takdimmakami;

	@Column(name = "ANAHTARKELIME")
	private String anahtarkelime;

	@Column(name = "PATH")
	private String path;

	@Column(name = "ICERIK")
	private String icerik;

	@ManyToOne
	@JoinColumn(name = "GITTIGIKURUMREF")
	private Kurum gittigiKurumRef;

	@Transient
	private Kurum gittigiKurumRefPrev;

	@ManyToOne
	@JoinColumn(name = "GITTIGIBIRIMREF")
	private Birim gittigiBirimRef;

	@ManyToOne
	@JoinColumn(name = "GITTIGIUYEREF")
	private Uye gittigiUyeRef;

	@Transient
	private Uye gittigiUyeRefPrev;

	@ManyToOne
	@JoinColumn(name = "SISTEMETANIMLAYAN")
	private Personel sistemeTanimlayan;

	@Column(name = "ILGI")
	private String ilgi;

	public Evrak() {
		super();
	}

	@Override
	public Long getRID() {
		return this.rID;
	}

	@Override
	public void setRID(Long rID) {
		this.rID = rID;
	}

	public int getYil() {
		return this.yil;
	}

	public void setYil(int yil) {
		this.yil = yil;
	}

	public HazirEvrakTuru getHazirEvrakTuru() {
		return hazirEvrakTuru;
	}

	public void setHazirEvrakTuru(HazirEvrakTuru hazirEvrakTuru) {
		this.hazirEvrakTuru = hazirEvrakTuru;
	}

	public EvrakHareketTuru getEvrakHareketTuru() {
		return evrakHareketTuru;
	}

	public void setEvrakHareketTuru(EvrakHareketTuru evrakHareketTuru) {
		this.evrakHareketTuru = evrakHareketTuru;
	}

	public java.util.Date getKayittarihi() {
		return kayittarihi;
	}

	public void setKayittarihi(java.util.Date kayittarihi) {
		this.kayittarihi = kayittarihi;
	}

	public String getKayitno() {
		return this.kayitno;
	}

	public void setKayitno(String kayitno) {
		this.kayitno = kayitno;
	}

	public Kurum getGonderenKurumRef() {
		return gonderenKurumRef;
	}

	public void setGonderenKurumRef(Kurum gonderenKurumRef) {
		this.gonderenKurumRef = gonderenKurumRef;
	}

	public Kisi getGonderenkisiRef() {
		return gonderenkisiRef;
	}

	public void setGonderenkisiRef(Kisi gonderenkisiRef) {
		this.gonderenkisiRef = gonderenkisiRef;
	}

	public Dosyakodu getDosyaKoduRef() {
		return dosyaKoduRef;
	}

	public void setDosyaKoduRef(Dosyakodu dosyaKoduRef) {
		this.dosyaKoduRef = dosyaKoduRef;
	}

	public Dosyakodu getDosyaKoduRefPrev() {
		return dosyaKoduRefPrev;
	}

	public void setDosyaKoduRefPrev(Dosyakodu dosyaKoduRefPrev) {
		this.dosyaKoduRefPrev = dosyaKoduRefPrev;
	}

	public Dolasimsablon getSablonRef() {
		return sablonRef;
	}

	public Kurum getGittigiKurumRefPrev() {
		return gittigiKurumRefPrev;
	}

	public void setGittigiKurumRefPrev(Kurum gittigiKurumRefPrev) {
		this.gittigiKurumRefPrev = gittigiKurumRefPrev;
	}

	public Kurum getGittigiKurumRef() {
		return gittigiKurumRef;
	}

	public void setGittigiKurumRef(Kurum gittigiKurumRef) {
		this.gittigiKurumRef = gittigiKurumRef;
	}

	public Birim getGittigiBirimRef() {
		return gittigiBirimRef;
	}

	public void setGittigiBirimRef(Birim gittigiBirimRef) {
		this.gittigiBirimRef = gittigiBirimRef;
	}

	public Uye getGittigiUyeRef() {
		return gittigiUyeRef;
	}

	public void setGittigiUyeRef(Uye gittigiUyeRef) {
		this.gittigiUyeRef = gittigiUyeRef;
	}

	public void setSablonRef(Dolasimsablon sablonRef) {
		this.sablonRef = sablonRef;
	}

	public String getKonusu() {
		return this.konusu;
	}

	public void setKonusu(String konusu) {
		this.konusu = konusu;
	}

	public java.util.Date getEvraktarihi() {
		return evraktarihi;
	}

	public void setEvraktarihi(java.util.Date evraktarihi) {
		this.evraktarihi = evraktarihi;
	}

	public String getEvrakno() {
		return this.evrakno;
	}

	public void setEvrakno(String evrakno) {
		this.evrakno = evrakno;
	}

	public EvrakTuru getEvrakturu() {
		return this.evrakturu;
	}

	public void setEvrakturu(EvrakTuru evrakturu) {
		this.evrakturu = evrakturu;
	}

	public TeslimTuru getTeslimturu() {
		return this.teslimturu;
	}

	public void setTeslimturu(TeslimTuru teslimturu) {
		this.teslimturu = teslimturu;
	}

	public GizlilikDerecesi getGizlilikderecesi() {
		return this.gizlilikderecesi;
	}

	public void setGizlilikderecesi(GizlilikDerecesi gizlilikderecesi) {
		this.gizlilikderecesi = gizlilikderecesi;
	}

	public AciliyetTuru getAciliyetturu() {
		return this.aciliyetturu;
	}

	public void setAciliyetturu(AciliyetTuru aciliyetturu) {
		this.aciliyetturu = aciliyetturu;
	}

	public EvrakDurum getDurum() {
		return this.durum;
	}

	public void setDurum(EvrakDurum durum) {
		this.durum = durum;
	}

	public Personel getHazirlayanPersonelRef() {
		return hazirlayanPersonelRef;
	}

	public void setHazirlayanPersonelRef(Personel hazirlayanPersonelRef) {
		this.hazirlayanPersonelRef = hazirlayanPersonelRef;
	}

	public Dolasimsablondetay getSonDolasimSablonDetayRef() {
		return sonDolasimSablonDetayRef;
	}

	public void setSonDolasimSablonDetayRef(
			Dolasimsablondetay sonDolasimSablonDetayRef) {
		this.sonDolasimSablonDetayRef = sonDolasimSablonDetayRef;
	}

	public String getTakdimmakami() {
		return this.takdimmakami;
	}

	public void setTakdimmakami(String takdimmakami) {
		this.takdimmakami = takdimmakami;
	}

	public String getAnahtarkelime() {
		return this.anahtarkelime;
	}

	public void setAnahtarkelime(String anahtarkelime) {
		this.anahtarkelime = anahtarkelime;
	}

	public String getIcerik() {
		return icerik;
	}

	public void setIcerik(String icerik) {
		this.icerik = icerik;
	}

	@Override
	public String getPath() {
		return this.path;
	}

	@Override
	public void setPath(String path) {
		this.path = path;
	}

	public Personel getSistemeTanimlayan() {
		return sistemeTanimlayan;
	}

	public void setSistemeTanimlayan(Personel sistemeTanimlayan) {
		this.sistemeTanimlayan = sistemeTanimlayan;
	}

	public Uye getGittigiUyeRefPrev() {
		return gittigiUyeRefPrev;
	}

	public void setGittigiUyeRefPrev(Uye gittigiUyeRefPrev) {
		this.gittigiUyeRefPrev = gittigiUyeRefPrev;
	}

	public String getIlgi() {
		return ilgi;
	}

	public void setIlgi(String ilgi) {
		this.ilgi = ilgi;
	}

	@Override
	public String toString() {
		return getUIString();
	}

	@Override
	public String getUIString() {
		return getKayitno() + " - " + getDurum().getLabel();
	}

	public String getUIStringList() {
		return getKayitno() + " - " + getDurum().getLabelForEvrakList();
	}

	public String getGonderenText() {
		if(getGonderenkisiRef() != null || getGonderenKurumRef() != null) {
			if(getGonderenkisiRef() != null){
				return getGonderenkisiRef().getUIStringShort();
			} else {
				return getGonderenKurumRef().getUIString();
			}
		} else {
			return "-";
		}
	}

	public String getGittigiText(){
		if(getGittigiBirimRef() != null || getGittigiKurumRef() != null || getGittigiUyeRef() != null) {
			if(getGittigiKurumRef() != null){
				return getGittigiKurumRef().getUIString();
			} else if(getGittigiUyeRef() != null){
				return getGittigiUyeRef().getUIString();
			}else {
				return getGittigiBirimRef().getUIString();
			}
		} else {
			return "-";
		}
	}


	@Override
	public void initValues(boolean defaultValues) {
		if (defaultValues) {
				// TODO Initialize with default values
		} else {
			this.rID = null;
			this.yil = 0;
			this.kayittarihi = null;
			this.kayitno = "";
			this.gonderenKurumRef = null;
			this.gonderenkisiRef = null;
			this.dosyaKoduRef = null;
			this.konusu = "";
			this.evraktarihi = null;
			this.evrakno = "";
			this.evrakturu = EvrakTuru._NULL;
			this.teslimturu = TeslimTuru._NULL;
			this.gizlilikderecesi = GizlilikDerecesi._NULL;
			this.aciliyetturu = AciliyetTuru._NULL;
			this.durum = EvrakDurum._NULL;
			this.hazirlayanPersonelRef = null;
			this.takdimmakami = "";
			this.anahtarkelime = "";
			this.path = null;
			this.icerik = null;
			this.sistemeTanimlayan = null;
			this.gittigiBirimRef = null;
			this.gittigiKurumRef = null;
			this.sablonRef = null;
			this.sonDolasimSablonDetayRef = null;
			this.hazirEvrakTuru = HazirEvrakTuru._NULL;
			this.gittigiUyeRef = null;
			this.ilgi = null;
			this.dosyaKoduRefPrev = null;
		}
	}

	@Override
	public String getValue() {
		StringBuilder value = new StringBuilder("Veri : <br />");
			if(getYil() != 0) {
				value.append(KeyUtil.getLabelValue("evrak.yil") + " : " + getYil() + "<br />");
			}
			if(getEvrakHareketTuru() != null) {
				value.append(KeyUtil.getLabelValue("evrak.evrakHareketTuru") + " : " + getEvrakHareketTuru().getLabel() + "<br />");
			}
			if(getKayittarihi() != null) {
				value.append(KeyUtil.getLabelValue("evrak.kayittarihi") + " : " + DateUtil.dateToDMYHMS(getKayittarihi()) + "<br />");
			}
			if(getKayitno() != null) {
				value.append(KeyUtil.getLabelValue("evrak.kayitno") + " : " + getKayitno() + "<br />");
			}
			if(getGonderenKurumRef() != null) {
				value.append(KeyUtil.getLabelValue("evrak.gonderenKurumRef") + " : " + getGonderenKurumRef().getUIString() + "<br />");
			}
			if(getGonderenkisiRef() != null) {
				value.append(KeyUtil.getLabelValue("evrak.gonderenkisiRef") + " : " + getGonderenkisiRef().getUIString() + "<br />");
			}
			if(getDosyaKoduRef() != null) {
				value.append(KeyUtil.getLabelValue("evrak.dosyaKoduRef") + " : " + getDosyaKoduRef().getUIString() + "<br />");
			}
			if(getKonusu() != null) {
				value.append(KeyUtil.getLabelValue("evrak.konusu") + " : " + getKonusu() + "<br />");
			}
			if(getEvraktarihi() != null) {
				value.append(KeyUtil.getLabelValue("evrak.evraktarihi") + " : " + DateUtil.dateToDMYHMS(getEvraktarihi()) + "<br />");
			}
			if(getEvrakno() != null) {
				value.append(KeyUtil.getLabelValue("evrak.evrakno") + " : " + getEvrakno() + "<br />");
			}
			if(getEvrakturu() != null) {
				value.append(KeyUtil.getLabelValue("evrak.evrakturu") + " : " + getEvrakturu() + "<br />");
			}
			if(getTeslimturu() != null) {
				value.append(KeyUtil.getLabelValue("evrak.teslimturu") + " : " + getTeslimturu() + "<br />");
			}
			if(getGizlilikderecesi() != null) {
				value.append(KeyUtil.getLabelValue("evrak.gizlilikderecesi") + " : " + getGizlilikderecesi() + "<br />");
			}
			if(getAciliyetturu() != null) {
				value.append(KeyUtil.getLabelValue("evrak.aciliyetturu") + " : " + getAciliyetturu() + "<br />");
			}
			if(getDurum() != null) {
				value.append(KeyUtil.getLabelValue("evrak.durum") + " : " + getDurum() + "<br />");
			}
			if(getHazirlayanPersonelRef() != null) {
				value.append(KeyUtil.getLabelValue("evrak.hazirlayanPersonelRef") + " : " + getHazirlayanPersonelRef().getUIString() + "<br />");
			}
			if(getTakdimmakami() != null) {
				value.append(KeyUtil.getLabelValue("evrak.takdimmakami") + " : " + getTakdimmakami() + "<br />");
			}
			if(getIcerik() != null) {
				value.append(KeyUtil.getLabelValue("evrak.icerik") + " : " + getIcerik() + "<br />");
			}
			if(getAnahtarkelime() != null) {
				value.append(KeyUtil.getLabelValue("evrak.anahtarkelime") + " : " + getAnahtarkelime() + "");
			}
			if(getPath() != null) {
				value.append(KeyUtil.getLabelValue("evrak.path") + " : " + getPath() + "");
			}
			if(getSistemeTanimlayan() != null) {
				value.append(KeyUtil.getLabelValue("evrak.sistemeTanimlayan") + " : " + getSistemeTanimlayan().getUIString() + "");
			}
			if(getGittigiBirimRef() != null) {
				value.append(KeyUtil.getLabelValue("evrak.gittigiBirimRef") + " : " + getGittigiBirimRef().getUIString() + "");
			}
			if(getGittigiKurumRef() != null) {
				value.append(KeyUtil.getLabelValue("evrak.gittigiKurumRef") + " : " + getGittigiKurumRef().getUIString() + "");
			}
			if(getSablonRef() != null) {
				value.append(KeyUtil.getLabelValue("evrak.sablonRef") + " : " + getSablonRef().getUIString() + "");
			}
			if(getSonDolasimSablonDetayRef() != null) {
				value.append(KeyUtil.getLabelValue("evrak.sonDolasimSablonDetayRef") + " : " + getSonDolasimSablonDetayRef().getUIString() + "");
			}
			if(getHazirEvrakTuru() != null) {
				value.append(KeyUtil.getLabelValue("evrak.hazirEvrakTuru") + " : " + getHazirEvrakTuru().getLabel() + "<br />");
			}
			if(getGittigiUyeRef() != null) {
				value.append(KeyUtil.getLabelValue("evrak.gittigiUyeRef") + " : " + getGittigiUyeRef().getUIString() + "<br />");
			}
			if(getIlgi() != null) {
				value.append(KeyUtil.getLabelValue("evrak.ilgi") + " : " + getIlgi() + "<br />");
			}
		return value.toString();
	}

}
