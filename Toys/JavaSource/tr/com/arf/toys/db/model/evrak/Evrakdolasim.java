package tr.com.arf.toys.db.model.evrak; 
 
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import tr.com.arf.framework.utility.tool.DateUtil;
import tr.com.arf.framework.utility.tool.KeyUtil;
import tr.com.arf.toys.db.model.base.ToysBaseEntity;
import tr.com.arf.toys.db.model.system.Birim;
import tr.com.arf.toys.db.model.system.Personel;
 
 
@Entity 
@Table(name = "EVRAKDOLASIM") 
public class Evrakdolasim extends ToysBaseEntity { 
 
	private static final long serialVersionUID = 1L; 
 
	@Id 
	@GeneratedValue(strategy = GenerationType.IDENTITY) 
	@Column(updatable = false, unique = true, nullable = false, name = "RID") 
	private Long rID; 
 
	@ManyToOne 
	@JoinColumn(name = "EVRAKREF") 
	private Evrak evrakRef; 
 
	@ManyToOne 
	@JoinColumn(name = "BIRIMREF") 
	private Birim birimRef; 
 
	@ManyToOne 
	@JoinColumn(name = "PERSONELREF") 
	private Personel personelRef; 
 
	@Temporal(TemporalType.DATE) 
	@Column(name = "GELISTARIH") 
	private java.util.Date gelistarih; 
 
	@Temporal(TemporalType.DATE) 
	@Column(name = "GIDISTARIH") 
	private java.util.Date gidistarih; 
 
	@ManyToOne 
	@JoinColumn(name = "DOLASIMSABLONDETAYREF") 
	private Dolasimsablondetay dolasimSablonDetayRef;
 
	public Evrakdolasim() { 
		super(); 
	} 
 
	@Override
	public Long getRID() { 
		return this.rID;  
	}  
	  
	@Override
	public void setRID(Long rID) {  
		this.rID = rID;  
	}   
 
	public Evrak getEvrakRef() { 
		return evrakRef; 
	} 
	 
	public void setEvrakRef(Evrak evrakRef) { 
		this.evrakRef = evrakRef; 
	} 
 
	public Birim getBirimRef() { 
		return birimRef; 
	} 
	 
	public void setBirimRef(Birim birimRef) { 
		this.birimRef = birimRef; 
	} 
 
	public Personel getPersonelRef() { 
		return personelRef; 
	} 
	 
	public void setPersonelRef(Personel personelRef) { 
		this.personelRef = personelRef; 
	} 
 
	public java.util.Date getGelistarih() { 
		return gelistarih; 
	} 
	 
	public void setGelistarih(java.util.Date gelistarih) { 
		this.gelistarih = gelistarih; 
	} 
 
	public java.util.Date getGidistarih() { 
		return gidistarih; 
	} 
	 
	public void setGidistarih(java.util.Date gidistarih) { 
		this.gidistarih = gidistarih; 
	} 
 
	public Dolasimsablondetay getDolasimSablonDetayRef() {
		return dolasimSablonDetayRef;
	}

	public void setDolasimSablonDetayRef(Dolasimsablondetay dolasimSablonDetayRef) {
		this.dolasimSablonDetayRef = dolasimSablonDetayRef;
	}

	@Override 
	public String toString() { 
		return getUIString(); 
	}
	
	@Override 
	public String getUIString() {
		String result = getEvrakRef().getUIString();
		if(getBirimRef() != null) {
			result += " - " + getBirimRef().getAd();
		} else {
			result += " - " + getPersonelRef().getKisiRef().getUIStringShort();
		}
		return result; 
	} 
	
	public String getAliciText() { 
		if(getBirimRef() != null) {
			return getBirimRef().getAd();
		} else {
			return getPersonelRef().getKisiRef().getUIStringShort();
		} 
	}  
	
	@Override 
	public void initValues(boolean defaultValues) { 
			this.rID = null; 			this.evrakRef = null; 			this.birimRef = null; 			this.personelRef = null; 			this.gelistarih = null; 			this.gidistarih = null; 
			this.dolasimSablonDetayRef = null;	}  
	 
	@Override 
	public String getValue() {  
		StringBuilder value = new StringBuilder("Veri : <br />"); 
			if(getEvrakRef() != null) {
				value.append(KeyUtil.getLabelValue("evrakdolasim.evrakRef") + " : " + getEvrakRef().getUIString() + "<br />");
			} 
			if(getBirimRef() != null) {
				value.append(KeyUtil.getLabelValue("evrakdolasim.birimRef") + " : " + getBirimRef().getUIString() + "<br />");
			} 
			if(getPersonelRef() != null) {
				value.append(KeyUtil.getLabelValue("evrakdolasim.personelRef") + " : " + getPersonelRef().getUIString() + "<br />");
			} 
			if(getGelistarih() != null) {
				value.append(KeyUtil.getLabelValue("evrakdolasim.gelistarih") + " : " + DateUtil.dateToDMYHMS(getGelistarih()) + "<br />");
			} 
			if(getGidistarih() != null) {
				value.append(KeyUtil.getLabelValue("evrakdolasim.gidistarih") + " : " + DateUtil.dateToDMYHMS(getGidistarih()) + "");
			} 
			if(getDolasimSablonDetayRef() != null) {
				value.append("Sablon detay ref : " + getDolasimSablonDetayRef().getUIString() + "");
			} 
		return value.toString(); 
	} 
	 
}	 
