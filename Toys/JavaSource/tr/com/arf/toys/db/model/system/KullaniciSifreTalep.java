package tr.com.arf.toys.db.model.system;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import tr.com.arf.framework.utility.tool.DateUtil;
import tr.com.arf.framework.utility.tool.KeyUtil;
import tr.com.arf.toys.db.model.base.ToysBaseEntity;

@Entity
@Table(name = "KULLANICISIFRETALEP")
public class KullaniciSifreTalep extends ToysBaseEntity {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(updatable = false, unique = true, nullable = false, name = "RID")
	private Long rID;

	@ManyToOne
	@JoinColumn(name = "KULLANICIREF")
	private Kullanici kullaniciRef;

	@Column(name = "GUVENLIKKODU")
	private String guvenlikkodu;

	@Temporal(TemporalType.DATE)
	@Column(name = "tarih")
	private java.util.Date tarih;

	public KullaniciSifreTalep() {
		super();
		setTarih(new Date());
	}

	@Override
	public Long getRID() {
		return this.rID;
	}

	@Override
	public void setRID(Long rID) {
		this.rID = rID;
	}

	public Kullanici getKullaniciRef() {
		return kullaniciRef;
	}

	public void setKullaniciRef(Kullanici kullaniciRef) {
		this.kullaniciRef = kullaniciRef;
	}

	public String getGuvenlikkodu() {
		return guvenlikkodu;
	}

	public void setGuvenlikkodu(String guvenlikkodu) {
		this.guvenlikkodu = guvenlikkodu;
	}

	public Date getTarih() {
		return tarih;
	}

	public void setTarih(Date tarih) {
		this.tarih = tarih;
	}

	@Override
	public String toString() {
		return getUIString();
	}

	@Override
	public String getUIString() {
		if (getKullaniciRef().getKisiRef() != null) {
			return getKullaniciRef().getKisiRef().getAd() + "";
		} else {
			return "";
		}
	}

	@Override
	public void initValues(boolean defaultValues) {
		if (defaultValues) {
			// TODO Initialize with default values
		} else {
			this.rID = null;
			this.kullaniciRef = null;
			this.guvenlikkodu = "";
			this.tarih = null;
		}
	}

	@Override
	public String getValue() {
		StringBuilder value = new StringBuilder("");
		if (getKullaniciRef() != null) {
			value.append(KeyUtil.getLabelValue("kullaniciSifreTalep.kullaniciRef") + " : " + getKullaniciRef().getUIString() + "");
		}
		if (getGuvenlikkodu() != null) {
			value.append(KeyUtil.getLabelValue("kullaniciSifreTalep.guvenlikkodu") + " : " + getGuvenlikkodu() + "");
		}
		if (getTarih() != null) {
			value.append(KeyUtil.getLabelValue("kullaniciSifreTalep.tarih") + " : " + DateUtil.dateToDMY(getTarih()) + "");
		}
		return value.toString();
	}

}
