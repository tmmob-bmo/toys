package tr.com.arf.toys.db.model.system; 
 
import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import tr.com.arf.framework.db.enumerated._common.EvetHayir;
import tr.com.arf.framework.utility.tool.BigDecimalUtil;
import tr.com.arf.framework.utility.tool.KeyUtil;
import tr.com.arf.toys.db.enumerated.uye.UyeTip;
import tr.com.arf.toys.db.model.base.ToysBaseEntity;
 
@Entity 
@Table(name = "AIDAT") 
public class Aidat extends ToysBaseEntity { 
 
	private static final long serialVersionUID = 1L; 
 
	@Id 
	@GeneratedValue(strategy = GenerationType.IDENTITY) 
	@Column(updatable = false, unique = true, nullable = false, name = "RID") 
	private Long rID; 
 
	@Column(name = "YIL") 
	private int yil; 
 
	@Temporal(TemporalType.DATE) 
	@Column(name = "BASLANGICTARIH") 
	private java.util.Date baslangictarih; 
 
	@Temporal(TemporalType.DATE) 
	@Column(name = "BITISTARIH") 
	private java.util.Date bitistarih; 
 
	@Enumerated(EnumType.ORDINAL) 
	@Column(name = "UYETIP") 
	private UyeTip uyetip; 
	
	@Enumerated(EnumType.ORDINAL) 
	@Column(name = "BORCLANDIRMAYAPILDI") 
	private EvetHayir borclandirmaYapildi; 	
 
	@Column(name = "MIKTAR") 
	private BigDecimal miktar; 
	
	@Column(name = "KAYITUCRETI") 
	private BigDecimal kayitucreti; 
 
 
	public Aidat() { 
		super(); 
	} 
 
	@Override
	public Long getRID() { 
		return this.rID;  
	}  
	  
	@Override
	public void setRID(Long rID) {  
		this.rID = rID;  
	}   
 
	public int getYil() { 
		return this.yil; 
	} 
	 
	public void setYil(int yil) { 
		this.yil = yil; 
	} 
 
	public java.util.Date getBaslangictarih() { 
		return baslangictarih; 
	} 
	 
	public void setBaslangictarih(java.util.Date baslangictarih) { 
		this.baslangictarih = baslangictarih; 
	} 
 
	public java.util.Date getBitistarih() { 
		return bitistarih; 
	} 
	 
	public void setBitistarih(java.util.Date bitistarih) { 
		this.bitistarih = bitistarih; 
	} 
 
	public UyeTip getUyetip() { 
		return this.uyetip;  
	}  
  
	public void setUyetip(UyeTip uyetip) {  
		this.uyetip = uyetip;  
	}   
 	
	public EvetHayir getBorclandirmaYapildi() {
		return borclandirmaYapildi;
	}

	public void setBorclandirmaYapildi(EvetHayir borclandirmaYapildi) {
		this.borclandirmaYapildi = borclandirmaYapildi;
	}

	public BigDecimal getMiktar() { 
		this.miktar = BigDecimalUtil.changeToCurrency(this.miktar); 
		return this.miktar; 
	} 
	 
	public void setMiktar(BigDecimal miktar) { 
		this.miktar = BigDecimalUtil.changeToCurrency(miktar); 
	}  
 
	public BigDecimal getKayitucreti() {
		this.kayitucreti = BigDecimalUtil.changeToCurrency(this.kayitucreti); 
		return this.kayitucreti; 
	}

	public void setKayitucreti(BigDecimal kayitucreti) {
		this.kayitucreti = kayitucreti;
	}

	@Override 
	public String toString() { 
		return "tr.com.arf.toys.db.model.system.Aidat[id=" + this.getRID()  + "]"; 
	} 
	
	@Override 
	public String getUIString() { 
		return getYil() + ""; 
	} 
	 
	@Override 
	public void initValues(boolean defaultValues) { 
		if (defaultValues) { 
				// TODO Initialize with default values 
		} else { 
			this.rID = null; 			this.yil = 0; 			this.baslangictarih = null; 			this.bitistarih = null; 			this.uyetip = UyeTip._NULL; 			this.miktar = null; 
			this.borclandirmaYapildi = EvetHayir._NULL;
			this.kayitucreti=null;		}  
	}  
	 
	@Override 
	public String getValue() {  
		StringBuilder value = new StringBuilder(""); 
			if(getYil() != 0) {
				value.append(KeyUtil.getLabelValue("aidat.yil") + " : " + getYil() + "<br />");
			} 
			if(getBaslangictarih() != null) {
				value.append(KeyUtil.getLabelValue("aidat.baslangictarih") + " : " + getBaslangictarih() + "<br />");
			} 
			if(getBitistarih() != null) {
				value.append(KeyUtil.getLabelValue("aidat.bitistarih") + " : " + getBitistarih() + "<br />");
			} 
			if(getUyetip() != null) {
				value.append(KeyUtil.getLabelValue("aidat.uyetip") + " : " + getUyetip() + "<br />");
			} 
			if(getMiktar() != null) {
				value.append(KeyUtil.getLabelValue("aidat.miktar") + " : " + getMiktar() + "");
			} 
			if(getKayitucreti() != null) {
				value.append(KeyUtil.getLabelValue("aidat.kayitucreti") + " : " + getKayitucreti() + "");
			} 
		return value.toString(); 
	} 
	
	public java.util.Date  getBaslangictarih1(){
		int yil=getYil();
		Calendar cal = Calendar.getInstance();
		cal.setTime(new Date());
		cal.set(Calendar.YEAR, yil);
		cal.set(Calendar.MONTH, 0);
		cal.set(Calendar.DAY_OF_MONTH, 1);
		setBaslangictarih(cal.getTime());
		return getBaslangictarih();
	}

	public java.util.Date  getBitistarih1(){
		int yil=getYil();
		Calendar cal = Calendar.getInstance();
		cal.setTime(new Date());
		cal.set(Calendar.YEAR, yil);
		cal.set(Calendar.MONTH, 11);
		cal.set(Calendar.DAY_OF_MONTH, 31);
		setBitistarih(cal.getTime());
		return getBitistarih();
	}
	 
}	 
