package tr.com.arf.toys.db.model.system; 
 
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import tr.com.arf.framework.utility.tool.DateUtil;
import tr.com.arf.framework.utility.tool.KeyUtil;
import tr.com.arf.toys.db.model.base.ToysBaseEntity;
 
 
@Entity 
@Table(name = "KULLANICIRAPOR") 
public class Kullanicirapor extends ToysBaseEntity { 
 
	private static final long serialVersionUID = 1L; 
 
	@Id 
	@GeneratedValue(strategy = GenerationType.IDENTITY) 
	@Column(updatable = false, unique = true, nullable = false, name = "RID") 
	private Long rID; 
 
	@ManyToOne 
	@JoinColumn(name = "KULLANICIREF") 
	private Kullanici kullaniciRef; 
 
	@Column(name = "RAPORADI") 
	private String raporadi; 
 
	@Column(name = "WHERECONDITION") 
	private String wherecondition; 
 
	@Temporal(TemporalType.DATE) 
	@Column(name = "TARIH") 
	private java.util.Date tarih; 
	
	@Column(name = "RAPORTURU")
	private String raporTuru;  
 
	public Kullanicirapor() { 
		super(); 
	} 
 
	@Override
	public Long getRID() { 
		return this.rID;  
	}  
	  
	@Override
	public void setRID(Long rID) {  
		this.rID = rID;  
	}   
 
	public Kullanici getKullaniciRef() { 
		return kullaniciRef; 
	} 
	 
	public void setKullaniciRef(Kullanici kullaniciRef) { 
		this.kullaniciRef = kullaniciRef; 
	} 
 
	public String getRaporadi() { 
		return this.raporadi; 
	} 
	 
	public void setRaporadi(String raporadi) { 
		this.raporadi = raporadi; 
	} 
 
	public String getWherecondition() { 
		return this.wherecondition; 
	} 
	 
	public void setWherecondition(String wherecondition) { 
		this.wherecondition = wherecondition; 
	} 
 
	public java.util.Date getTarih() { 
		return tarih; 
	} 
	 
	public void setTarih(java.util.Date tarih) { 
		this.tarih = tarih; 
	} 
	
	public String getRaporTuru() {
		return raporTuru;
	}

	public void setRaporTuru(String raporTuru) {
		this.raporTuru = raporTuru;
	}
 
	@Override 
	public String toString() { 
		return "tr.com.arf.toys.db.model.system.Kullanicirapor[id=" + this.getRID()  + "]"; 
	} 
	
	@Override 
	public String getUIString() { 
		return getRaporadi() + ""; 
	} 
	 
	@Override 
	public void initValues(boolean defaultValues) { 
			this.rID = null; 			this.kullaniciRef = null; 			this.raporadi = ""; 			this.wherecondition = ""; 			this.tarih = null; 
			this.raporTuru = null;	}  
	 
	@Override 
	public String getValue() {  
		StringBuilder value = new StringBuilder("Veri : <br />"); 
			if(getKullaniciRef() != null) {
				value.append(KeyUtil.getLabelValue("kullanicirapor.kullaniciRef") + " : " + getKullaniciRef().getUIString() + "<br />");
			} 
			if(getRaporadi() != null) {
				value.append(KeyUtil.getLabelValue("kullanicirapor.raporadi") + " : " + getRaporadi() + "<br />");
			} 
			if(getWherecondition() != null) {
				value.append(KeyUtil.getLabelValue("kullanicirapor.wherecondition") + " : " + getWherecondition() + "<br />");
			} 
			if(getTarih() != null) {
				value.append(KeyUtil.getLabelValue("kullanicirapor.tarih") + " : " + DateUtil.dateToDMYHMS(getTarih()) + "");
			} 
			if(getRaporTuru() != null) {
				value.append(KeyUtil.getLabelValue("kullanicirapor.raporTuru") + " : " + getRaporTuru() + "<br />");
			} 
		return value.toString(); 
	} 
	 
}	 
