package tr.com.arf.toys.db.model.system; 
 
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import tr.com.arf.framework.utility.tool.KeyUtil;
import tr.com.arf.toys.db.enumerated.system.OnemDerece;
import tr.com.arf.toys.db.model.base.ToysBaseEntity;
 
@Entity 
@Table(name = "SORUN") 
public class Sorun extends ToysBaseEntity { 
 
	private static final long serialVersionUID = 1L; 
 
	@Id 
	@GeneratedValue(strategy = GenerationType.IDENTITY) 
	@Column(updatable = false, unique = true, nullable = false, name = "RID") 
	private Long rID; 
 
	@Temporal(TemporalType.DATE) 
	@Column(name = "TARIH") 
	private java.util.Date tarih; 
 
	@Enumerated(EnumType.ORDINAL) 
	@Column(name = "ONEM") 
	private OnemDerece onem; 
 
	@ManyToOne 
	@JoinColumn(name = "KULLANICIREF") 
	private Kullanici kullaniciRef; 
	
	@Column(name = "BASLIK") 
	private String baslik; 

	@Column(name = "ACIKLAMA") 
	private String aciklama; 
 
	public Sorun() { 
		super(); 
	} 
 
	@Override
	public Long getRID() { 
		return this.rID;  
	}  
	  
	@Override
	public void setRID(Long rID) {  
		this.rID = rID;  
	}   
 
	public java.util.Date getTarih() { 
		return tarih; 
	} 
	 
	public void setTarih(java.util.Date tarih) { 
		this.tarih = tarih; 
	} 
 
	public OnemDerece getOnem() { 
		return this.onem;  
	}  
  
	public void setOnem(OnemDerece onem) {  
		this.onem = onem;  
	}   
 
	public Kullanici getKullaniciRef() { 
		return kullaniciRef; 
	} 
	 
	public void setKullaniciRef(Kullanici kullaniciRef) { 
		this.kullaniciRef = kullaniciRef; 
	}

	public String getBaslik() {
		return this.baslik;
	}

	public void setBaslik(String baslik) {
		this.baslik = baslik;
	}

	public String getAciklama() {
		return this.aciklama;
	}

	public void setAciklama(String aciklama) {
		this.aciklama = aciklama;
	}


	@Override 
	public String toString() { 
		return "tr.com.arf.toys.db.model.system.Sorun[id=" + this.getRID()  + "]"; 
	} 
	
	
	@Override 
	public void initValues(boolean defaultValues) { 
		if (defaultValues) { 
				// TODO Initialize with default values 
		} else { 
			this.rID = null; 
			this.tarih = null; 
			this.onem = OnemDerece._NULL; 
			this.kullaniciRef = null; 
		    this.baslik=null;
		    this.aciklama=null;
		}  
	}  
	 
	@Override 
	public String getValue() {  
		StringBuilder value = new StringBuilder(""); 
		
	    	if(getTarih() != null) {
				value.append(KeyUtil.getLabelValue("sorunbildir.tarih") + " : " + getTarih() + "<br />");
			} 
    		if(getOnem() != null) {
				value.append(KeyUtil.getLabelValue("sorunbildir.onem") + " : " + getOnem() + "<br />");
			} 
    		if(getBaslik() != null) {
				value.append(KeyUtil.getLabelValue("sorunbildir.baslik") + " : " + getBaslik() + "<br />");
			} 
    		if(getAciklama() != null) {
				value.append(KeyUtil.getLabelValue("sorunbildir.aciklama") + " : " + getAciklama() + "<br />");
			} 
			if(getKullaniciRef() != null) {
				value.append(KeyUtil.getLabelValue("sorunbildir.kullaniciRef") + " : " + getKullaniciRef() + "");
			} 
			
		return value.toString(); 
	}

	@Override
	public String getUIString() {
		// TODO Auto-generated method stub
		return null;
	} 
	 
}	 
