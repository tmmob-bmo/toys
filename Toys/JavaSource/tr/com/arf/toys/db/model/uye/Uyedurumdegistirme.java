package tr.com.arf.toys.db.model.uye;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import tr.com.arf.framework.utility.tool.DateUtil;
import tr.com.arf.framework.utility.tool.KeyUtil;
import tr.com.arf.toys.db.enumerated.uye.UyeDurum;
import tr.com.arf.toys.db.model.base.ToysBaseEntity;
import tr.com.arf.toys.db.model.system.Kullanici;

@Entity
@Table(name = "UYEDURUMDEGISTIRME")
public class Uyedurumdegistirme extends ToysBaseEntity {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(updatable = false, unique = true, nullable = false, name = "RID")
	private Long rID;

	@ManyToOne
	@JoinColumn(name = "UYEREF")
	private Uye uyeRef;

	@Temporal(TemporalType.DATE)
	@Column(name = "TARIH")
	private java.util.Date tarih;

	@Transient
	private java.util.Date bitisTarih;

	@ManyToOne
	@JoinColumn(name = "KAYITYAPAN")
	private Kullanici kayityapan;

	@Enumerated(EnumType.ORDINAL)
	@Column(name = "ONCEKIDURUM")
	private UyeDurum oncekidurum;

	@Enumerated(EnumType.ORDINAL)
	@Column(name = "YENIDURUM")
	private UyeDurum yenidurum;

	public Uyedurumdegistirme() {
		super();
	}

	public Uyedurumdegistirme(Uye uyeRef, java.util.Date tarih, Kullanici kayityapan, UyeDurum oncekidurum, UyeDurum yenidurum) {
		super();
		this.uyeRef = uyeRef;
		this.tarih = tarih;
		this.kayityapan = kayityapan;
		this.oncekidurum = oncekidurum;
		this.yenidurum = yenidurum;
	}

	@Override
	public Long getRID() {
		return this.rID;
	}

	@Override
	public void setRID(Long rID) {
		this.rID = rID;
	}

	public Uye getUyeRef() {
		return uyeRef;
	}

	public void setUyeRef(Uye uyeRef) {
		this.uyeRef = uyeRef;
	}

	public java.util.Date getTarih() {
		return tarih;
	}

	public void setTarih(java.util.Date tarih) {
		this.tarih = tarih;
	}

	public java.util.Date getBitisTarih() {
		return bitisTarih;
	}

	public void setBitisTarih(java.util.Date bitisTarih) {
		this.bitisTarih = bitisTarih;
	}

	public Kullanici getKayityapan() {
		return kayityapan;
	}

	public void setKayityapan(Kullanici kayityapan) {
		this.kayityapan = kayityapan;
	}

	public UyeDurum getOncekidurum() {
		return this.oncekidurum;
	}

	public void setOncekidurum(UyeDurum oncekidurum) {
		this.oncekidurum = oncekidurum;
	}

	public UyeDurum getYenidurum() {
		return this.yenidurum;
	}

	public void setYenidurum(UyeDurum yenidurum) {
		this.yenidurum = yenidurum;
	}

	@Override
	public String toString() {
		return "tr.com.arf.toys.db.model.uye.Uyedurumdegistirme[id=" + this.getRID() + "]";
	}

	@Override
	public String getUIString() {
		return getUyeRef() + "";
	}

	@Override
	public void initValues(boolean defaultValues) {
		if (defaultValues) {
			// TODO Initialize with default values
		} else {
			this.rID = null;
			this.uyeRef = null;
			this.tarih = null;
			this.kayityapan = null;
			this.oncekidurum = UyeDurum._NULL;
			this.yenidurum = UyeDurum._NULL;
		}
	}

	public boolean isSurekliMuafiyet() {
		if (getYenidurum() == null) {
			return false;
		}
		if (getYenidurum() == UyeDurum._VEFAT || getYenidurum() == UyeDurum._ISTIFA || getYenidurum() == UyeDurum._EMEKLI) {
			// Surekli muafiyetlerde bitis tarihi olmaz...
			return true;
		}
		return false;
	}

	@Override
	public String getValue() {
		StringBuilder value = new StringBuilder("Veri : <br />");
		if (getUyeRef() != null) {
			value.append(KeyUtil.getLabelValue("uyedurumdegistirme.uyeRef") + " : " + getUyeRef().getUIString() + "<br />");
		}
		if (getTarih() != null) {
			value.append(KeyUtil.getLabelValue("uyedurumdegistirme.tarih") + " : " + DateUtil.dateToDMYHMS(getTarih()) + "<br />");
		}
		if (getKayityapan() != null) {
			value.append(KeyUtil.getLabelValue("uyedurumdegistirme.kayityapan") + " : " + getKayityapan().getUIString() + "<br />");
		}
		if (getOncekidurum() != null) {
			value.append(KeyUtil.getLabelValue("uyedurumdegistirme.oncekidurum") + " : " + getOncekidurum() + "<br />");
		}
		if (getYenidurum() != null) {
			value.append(KeyUtil.getLabelValue("uyedurumdegistirme.yenidurum") + " : " + getYenidurum() + "");
		}
		return value.toString();
	}

}
