package tr.com.arf.toys.db.model.etkinlik; 
 
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tr.com.arf.framework.utility.tool.KeyUtil;
import tr.com.arf.toys.db.enumerated.etkinlik.Dosyaturu;
import tr.com.arf.toys.db.model.base.ToysBaseEntity;
 
@Entity 
@Table(name = "ETKINLIKDOSYA") 
public class Etkinlikdosya extends ToysBaseEntity { 
 
	private static final long serialVersionUID = 1L; 
 
	@Id 
	@GeneratedValue(strategy = GenerationType.IDENTITY) 
	@Column(updatable = false, unique = true, nullable = false, name = "RID") 
	private Long rID; 
 
	@ManyToOne 
	@JoinColumn(name = "ETKINLIKREF") 
	private Etkinlik etkinlikRef; 
 
	@Enumerated(EnumType.ORDINAL) 
	@Column(name = "DOSYATURU") 
	private Dosyaturu dosyaturu; 
 
	@Column(name = "AD") 
	private String ad; 
 
	@Column(name = "PATH") 
	private String path; 
 
 
	public Etkinlikdosya() { 
		super(); 
	} 
 
	public Long getRID() { 
		return this.rID;  
	}  
	  
	public void setRID(Long rID) {  
		this.rID = rID;  
	}   
 
	public Etkinlik getEtkinlikRef() { 
		return etkinlikRef; 
	} 
	 
	public void setEtkinlikRef(Etkinlik etkinlikRef) { 
		this.etkinlikRef = etkinlikRef; 
	} 
 
	public Dosyaturu getDosyaturu() { 
		return this.dosyaturu;  
	}  
  
	public void setDosyaturu(Dosyaturu dosyaturu) {  
		this.dosyaturu = dosyaturu;  
	}   
 
	public String getAd() { 
		return this.ad; 
	} 
	 
	public void setAd(String ad) { 
		this.ad = ad; 
	} 
 
	public String getPath() { 
		return this.path; 
	} 
	 
	public void setPath(String path) { 
		this.path = path; 
	} 
 
 
	@Override 
	public String toString() { 
		return "tr.com.arf.toys.db.model.etkinlik.Etkinlikdosya[id=" + this.getRID()  + "]"; 
	} 
	@Override 
	public String getUIString() { 
		return getAd() + ""; 
	} 
	 
	@Override 
	public void initValues(boolean defaultValues) { 
			this.rID = null; 			this.etkinlikRef = null; 			this.dosyaturu = Dosyaturu._NULL; 			this.ad = ""; 			this.path = ""; 	}  
	 
	@Override 
	public String getValue() {  
		StringBuilder value = new StringBuilder("Veri : <br />"); 
			if(getEtkinlikRef() != null) 
				value.append(KeyUtil.getLabelValue("etkinlikdosya.etkinlikRef") + " : " + getEtkinlikRef().getUIString() + "<br />"); 
			if(getDosyaturu() != null) 
				value.append(KeyUtil.getLabelValue("etkinlikdosya.dosyaturu") + " : " + getDosyaturu().getLabel() + "<br />"); 
			if(getAd() != null) 
				value.append(KeyUtil.getLabelValue("etkinlikdosya.ad") + " : " + getAd() + "<br />"); 
			if(getPath() != null) 
				value.append(KeyUtil.getLabelValue("etkinlikdosya.path") + " : " + getPath() + ""); 
		return value.toString(); 
	} 
	 
}	 
