package tr.com.arf.toys.db.model.kisi; 
 
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tr.com.arf.framework.db.enumerated._common.EvetHayir;
import tr.com.arf.framework.utility.tool.KeyUtil;
import tr.com.arf.toys.db.enumerated.uye.OgrenimTip;
import tr.com.arf.toys.db.model.base.ToysBaseEntity;
import tr.com.arf.toys.db.model.system.Bolum;
import tr.com.arf.toys.db.model.system.Fakulte;
import tr.com.arf.toys.db.model.system.Universite;
 
 
@Entity 
@Table(name = "KISIOGRENIM") 
public class Kisiogrenim extends ToysBaseEntity { 
 
	private static final long serialVersionUID = 1L; 
 
	@Id 
	@GeneratedValue(strategy = GenerationType.IDENTITY) 
	@Column(updatable = false, unique = true, nullable = false, name = "RID") 
	private Long rID; 
 
	@ManyToOne 
	@JoinColumn(name = "KISIREF") 
	private Kisi kisiRef; 
 
	@Column(name = "OGRENIMTIP") 
	private OgrenimTip ogrenimtip; 
 
	@ManyToOne 
	@JoinColumn(name = "UNIVERSITEREF") 
	private Universite universiteRef;  
 
	@Column(name = "ACIKLAMA") 
	private String aciklama; 
	
	@ManyToOne 
	@JoinColumn(name = "FAKULTEREF") 
	private Fakulte fakulteRef; 
	
	@ManyToOne 
	@JoinColumn(name = "BOLUMREF") 
	private Bolum bolumRef; 
	
	@Column(name = "LISANSUNVAN") 
	private String lisansunvan; 
	
	@Column(name = "DIPLOMANO") 
	private String diplomano;
	
	@Enumerated(EnumType.ORDINAL)  
	@Column(name = "DENKLIKDURUM") 
	private EvetHayir denklikdurum; 
	
	@Enumerated(EnumType.ORDINAL)
	@Column(name = "VARSAYILAN") 
	private EvetHayir varsayilan; 
	
	@Column(name = "MEZUNIYETTARIHI") 
	private Integer mezuniyettarihi; 
	
	public Kisiogrenim() { 
		super(); 
	} 
 
	@Override
	public Long getRID() { 
		return this.rID;  
	}  
	  
	@Override
	public void setRID(Long rID) {  
		this.rID = rID;  
	}   
 
	public Kisi getKisiRef() {
		return kisiRef;
	}

	public void setKisiRef(Kisi kisiRef) {
		this.kisiRef = kisiRef;
	}

	public OgrenimTip getOgrenimtip() { 
		return this.ogrenimtip; 
	} 
	 
	public void setOgrenimtip(OgrenimTip ogrenimtip) { 
		this.ogrenimtip = ogrenimtip; 
	} 
 
	public Universite getUniversiteRef() {
		return universiteRef;
	}

	public void setUniversiteRef(Universite universiteRef) {
		this.universiteRef = universiteRef;
	} 
 
	public String getAciklama() { 
		return this.aciklama; 
	} 
	 
	public void setAciklama(String aciklama) { 
		this.aciklama = aciklama; 
	} 

	public Fakulte getFakulteRef() {
		return fakulteRef;
	}

	public void setFakulteRef(Fakulte fakulteRef) {
		this.fakulteRef = fakulteRef;
	}

	public Bolum getBolumRef() {
		return bolumRef;
	}

	public void setBolumRef(Bolum bolumRef) {
		this.bolumRef = bolumRef;
	}

	public String getLisansunvan() {
		return lisansunvan;
	}

	public void setLisansunvan(String lisansunvan) {
		this.lisansunvan = lisansunvan;
	}

	public String getDiplomano() {
		return diplomano;
	}

	public void setDiplomano(String diplomano) {
		this.diplomano = diplomano;
	} 

	public EvetHayir getDenklikdurum() {
		return denklikdurum;
	}

	public void setDenklikdurum(EvetHayir denklikdurum) {
		this.denklikdurum = denklikdurum;
	}

	public EvetHayir getVarsayilan() {
		return varsayilan;
	}

	public void setVarsayilan(EvetHayir varsayilan) {
		this.varsayilan = varsayilan;
	} 	

	public Integer getMezuniyettarihi() {
		return mezuniyettarihi;
	}

	public void setMezuniyettarihi(Integer mezuniyettarihi) {
		this.mezuniyettarihi = mezuniyettarihi;
	}

	@Override 
	public String toString() { 
		return "tr.com.arf.toys.db.model.kisi.Kisiogrenim[id=" + this.getRID()  + "]"; 
	} 
	
	@Override 
	public String getUIString() { 
		if (getUniversiteRef()!=null && getOgrenimtip()!=null){
			return getOgrenimtip().getLabel() + " : " + getUniversiteRef().getAd(); 
		}else 
			return "";
		
	} 
	 
	@Override 
	public void initValues(boolean defaultValues) { 
		if (defaultValues) { 
		} else { 
			this.rID = null; 			this.kisiRef = null; 			this.ogrenimtip = OgrenimTip._NULL; 			this.universiteRef = null;  
			this.aciklama = ""; 
			this.bolumRef=null;
			this.denklikdurum=EvetHayir._NULL;
			this.diplomano="";
			this.lisansunvan="";
			this.fakulteRef=null;
			this.varsayilan=EvetHayir._NULL;
			this.mezuniyettarihi = null;		}  
	}  
	 
	@Override 
	public String getValue() {  
		StringBuilder value = new StringBuilder(""); 
			if(getKisiRef() != null) {
				value.append(KeyUtil.getLabelValue("kisiogrenim.kisiRef") + " : " + getKisiRef().getUIString() + "<br />");
			} 
			if(getOgrenimtip() != null) {
				value.append(KeyUtil.getLabelValue("kisiogrenim.ogrenimtip") + " : " + getOgrenimtip().getLabel() + "<br />");
			} 
			if(getUniversiteRef() != null) {
				value.append(KeyUtil.getLabelValue("kisiogrenim.universiteRef") + " : " + getUniversiteRef().getUIString() + "<br />");
			} 
			if(getFakulteRef() != null) {
				value.append(KeyUtil.getLabelValue("kisiogrenim.fakulteRef") + " : " + getFakulteRef().getUIString() + "<br />");
			}  
			if(getAciklama() != null) {
				value.append(KeyUtil.getLabelValue("kisiogrenim.aciklama") + " : " + getAciklama() + "");
			} 
			if(getBolumRef() != null) {
				value.append(KeyUtil.getLabelValue("kisiogrenim.bolumRef") + " : " + getBolumRef() + "");
			} 
			if(getLisansunvan() != null) {
				value.append(KeyUtil.getLabelValue("kisiogrenim.lisansunvan") + " : " + getLisansunvan() + "");
			} 
			if(getDiplomano() != null) {
				value.append(KeyUtil.getLabelValue("kisiogrenim.diplomano") + " : " + getDiplomano() + "");
			} 
			if(getDenklikdurum() != null) {
				value.append(KeyUtil.getLabelValue("kisiogrenim.denklikdurum") + " : " + getDenklikdurum().getLabel() + "");
			} 
			if(getVarsayilan() != null) {
				value.append(KeyUtil.getLabelValue("kisiogrenim.varsayilan") + " : " + getVarsayilan() + "");
			} 
			if(getMezuniyettarihi() != null) {
				value.append(KeyUtil.getLabelValue("kisiogrenim.mezuniyettarihi") + " : " + getMezuniyettarihi() + "<br />");
			}
		return value.toString(); 
	} 
	 
}	 
