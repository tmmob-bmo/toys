package tr.com.arf.toys.db.model.uye;

import tr.com.arf.framework.db._management.DBOperator;
import tr.com.arf.framework.db.enumerated._common.EvetHayir;
import tr.com.arf.framework.utility.tool.DateUtil;
import tr.com.arf.framework.utility.tool.KeyUtil;
import tr.com.arf.toys.db.enumerated.iletisim.AdresTuru;
import tr.com.arf.toys.db.enumerated.uye.*;
import tr.com.arf.toys.db.model.base.ToysBaseEntity;
import tr.com.arf.toys.db.model.iletisim.Adres;
import tr.com.arf.toys.db.model.kisi.Kisi;
import tr.com.arf.toys.db.model.kisi.Kisikimlik;
import tr.com.arf.toys.db.model.kisi.Kisiogrenim;
import tr.com.arf.toys.db.model.kisi.Kurum;
import tr.com.arf.toys.db.model.system.Birim;
import tr.com.arf.toys.db.model.system.KurumKisi;
import tr.com.arf.toys.db.model.system.Personel;
import tr.com.arf.toys.service.application.ManagedBeanLocator;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Table(name = "UYE")
public class Uye extends ToysBaseEntity {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(updatable = false, unique = true, nullable = false, name = "RID")
	private Long rID;

	@ManyToOne
	@JoinColumn(name = "KISIREF")
	private Kisi kisiRef;

	@Column(name = "SICILNO")
	private String sicilno;

	@Enumerated(EnumType.ORDINAL)
	@Column(name = "UYETIP")
	private UyeTip uyetip;

	@ManyToOne
	@JoinColumn(name = "BIRIMREF")
	private Birim birimRef;

	@Column(name = "ILETISIMSMS")
	private EvetHayir iletisimSms;

	@Column(name = "ILETISIMEMAIL")
	private EvetHayir iletisimEmail;

	@Column(name = "ILETISIMPOSTA")
	private EvetHayir iletisimPosta;

	@Temporal(TemporalType.DATE)
	@Column(name = "UYELIKTARIH")
	private java.util.Date uyeliktarih;

	@Temporal(TemporalType.DATE)
	@Column(name = "AYRILMATARIH")
	private java.util.Date ayrilmatarih;

	@Enumerated(EnumType.ORDINAL)
	@Column(name = "UYEDURUM")
	private UyeDurum uyedurum;

	@Enumerated(EnumType.ORDINAL)
	@Column(name = "KAYITDURUM")
	private UyeKayitDurum kayitdurum;

	@ManyToOne
	@JoinColumn(name = "ONAYLAYANREF")
	private Personel onaylayanRef;

	@Temporal(TemporalType.DATE)
	@Column(name = "ONAYTARIH")
	private java.util.Date onaytarih;

	@ManyToOne
	@JoinColumn(name = "OGRENCIUYEREF")
	private Uye ogrenciUyeRef;

	@Column(name = "OGRENCINO")
	private String ogrenciNo;

	@Column(name = "SINIF")
	private Integer sinif;

    @Temporal(TemporalType.DATE)
    @Column(name = "KART_TALEP_TARIHI")
    private java.util.Date kartTalepTarihi;

    @Temporal(TemporalType.DATE)
    @Column(name = "KART_TESLIM_TARIHI")
    private java.util.Date kartTeslimTarihi;

	@Column(name = "TESLIM_ADRESI")
	private String teslimAdresi;

	@Transient
	private boolean epostaIletisimi;

	@Transient
	private boolean telefonIletisimi;

	@Transient
	private boolean postaIletisimi;

	public boolean isEpostaIletisimi() {
		if (iletisimEmail==EvetHayir._EVET) {
			epostaIletisimi=true;
		} else {
			epostaIletisimi=false;
		}
		return epostaIletisimi;
	}

	public void setEpostaIletisimi(boolean epostaIletisimi) {
		if(epostaIletisimi){
			setIletisimEmail(EvetHayir._EVET);
		} else {
			setIletisimEmail(EvetHayir._HAYIR);
		}
		this.epostaIletisimi = epostaIletisimi;
	}

	public boolean isTelefonIletisimi() {
		if (iletisimSms==EvetHayir._EVET) {
			telefonIletisimi=true;
		} else {
			telefonIletisimi=false;
		}
		return telefonIletisimi;
	}

	public void setTelefonIletisimi(boolean telefonIletisimi) {
		if(telefonIletisimi){
			setIletisimSms(EvetHayir._EVET);
		} else {
			setIletisimSms(EvetHayir._HAYIR);
		}
		this.telefonIletisimi = telefonIletisimi;
	}

	public boolean isPostaIletisimi() {
		if (iletisimPosta==EvetHayir._EVET) {
			postaIletisimi=true;
		} else {
			postaIletisimi=false;
		}
		return postaIletisimi;
	}

	public void setPostaIletisimi(boolean postaIletisimi) {
		if(postaIletisimi){
			setIletisimPosta(EvetHayir._EVET);
		} else {
			setIletisimPosta(EvetHayir._HAYIR);
		}
		this.postaIletisimi = postaIletisimi;
	}

	public Uye() {
		super();
	}

	@Override
	public Long getRID() {
		return this.rID;
	}

	@Override
	public void setRID(Long rID) {
		this.rID = rID;
	}

	public Kisi getKisiRef() {
		return kisiRef;
	}

	public void setKisiRef(Kisi kisiRef) {
		this.kisiRef = kisiRef;
	}

	public String getSicilno() {
		return this.sicilno;
	}

	public void setSicilno(String sicilno) {
		this.sicilno = sicilno;
	}

	public UyeTip getUyetip() {
		return this.uyetip;
	}

	public void setUyetip(UyeTip uyetip) {
		this.uyetip = uyetip;
	}

	public Birim getBirimRef() {
		return birimRef;
	}

	public void setBirimRef(Birim birimRef) {
		this.birimRef = birimRef;
	}

	public EvetHayir getIletisimSms() {
		return iletisimSms;
	}

	public void setIletisimSms(EvetHayir iletisimSms) {
		this.iletisimSms = iletisimSms;
	}

	public EvetHayir getIletisimEmail() {
		return iletisimEmail;
	}

	public void setIletisimEmail(EvetHayir iletisimEmail) {
		this.iletisimEmail = iletisimEmail;
	}

	public EvetHayir getIletisimPosta() {
		return iletisimPosta;
	}

	public void setIletisimPosta(EvetHayir iletisimPosta) {
		this.iletisimPosta = iletisimPosta;
	}

	public java.util.Date getUyeliktarih() {
		return uyeliktarih;
	}

	public void setUyeliktarih(java.util.Date uyeliktarih) {
		this.uyeliktarih = uyeliktarih;
	}

	public java.util.Date getAyrilmatarih() {
		return ayrilmatarih;
	}

	public void setAyrilmatarih(java.util.Date ayrilmatarih) {
		this.ayrilmatarih = ayrilmatarih;
	}

	public UyeDurum getUyedurum() {
		return this.uyedurum;
	}

	public void setUyedurum(UyeDurum uyedurum) {
		this.uyedurum = uyedurum;
	}

	public UyeKayitDurum getKayitdurum() {
		return this.kayitdurum;
	}

	public void setKayitdurum(UyeKayitDurum kayitdurum) {
		this.kayitdurum = kayitdurum;
	}

	public Personel getOnaylayanRef() {
		return onaylayanRef;
	}

	public void setOnaylayanRef(Personel onaylayanRef) {
		this.onaylayanRef = onaylayanRef;
	}

	public java.util.Date getOnaytarih() {
		return onaytarih;
	}

	public void setOnaytarih(java.util.Date onaytarih) {
		this.onaytarih = onaytarih;
	}

	public Uye getOgrenciUyeRef() {
		return ogrenciUyeRef;
	}

	public void setOgrenciUyeRef(Uye ogrenciUyeRef) {
		this.ogrenciUyeRef = ogrenciUyeRef;
	}

	public String getOgrenciNo() {
		return ogrenciNo;
	}

	public void setOgrenciNo(String ogrenciNo) {
		this.ogrenciNo = ogrenciNo;
	}

	public Integer getSinif() {
		return sinif;
	}

	public void setSinif(Integer sinif) {
		this.sinif = sinif;
	}

    public Date getKartTalepTarihi() {
        return kartTalepTarihi;
    }

    public void setKartTalepTarihi(Date kartTalepTarihi) {
        this.kartTalepTarihi = kartTalepTarihi;
    }

	@Transient
	public BigDecimal getUyeAidatBorc() {
		return ManagedBeanLocator.locateSessionController().getUyeAidatBorc(getRID());
	}

	@Transient
	public Adres getVarsayilanAdres() {
		try {
			if(getDBOperator().recordCount(Adres.class.getSimpleName(), "o.kisiRef.rID=" + getKisiRef().getRID()) > 0){
				return (Adres) getDBOperator().load(Adres.class.getSimpleName(), "o.kisiRef.rID=" + getKisiRef().getRID(), "o.varsayilan ASC, o.beyanTarihi DESC").get(0);
			} else {
				return null;
			}
		} catch(Exception e){
			return null;
		}
	}

	@Transient
	public Adres getEvAdres() {
		try {
			if(getDBOperator().recordCount(Adres.class.getSimpleName(), "o.kisiRef.rID=" + getKisiRef().getRID() + " AND o.adresturu=" + AdresTuru._EVADRESI.getCode()) > 0){
				return (Adres) getDBOperator().load(Adres.class.getSimpleName(), "o.kisiRef.rID=" + getKisiRef().getRID() +
						" AND o.adresturu=" + AdresTuru._EVADRESI.getCode() , "o.varsayilan ASC, o.beyanTarihi DESC").get(0);
			} else {
				return null;
			}
		} catch(Exception e){
			return null;
		}
	}


	@Transient
	public Kurum getKurum() {
		try {
			if(getDBOperator().recordCount(KurumKisi.class.getSimpleName(), "o.kisiRef.rID=" + getKisiRef().getRID() + " AND o.uyekurumDurum=" +UyekurumDurum._CALISIYOR.getCode() + " AND o.varsayilan=" + EvetHayir._EVET.getCode()) > 0){
				KurumKisi kurumkisi =(KurumKisi) getDBOperator().load(KurumKisi.class.getSimpleName(), "o.kisiRef.rID=" + getKisiRef().getRID()+" AND o.uyekurumDurum="
						+UyekurumDurum._CALISIYOR.getCode()+ " AND o.varsayilan=" + EvetHayir._EVET.getCode(),"").get(0);
				return kurumkisi.getKurumRef();
			} else {
				return null;
			}
		} catch(Exception e){
			return null;
		}
	}

	public String getOnaylanmamisSicilNo() {
		if (getSicilno()!=null && getKayitdurum().getCode()== UyeKayitDurum._ONAYLANMIS.getCode()){
			return "<span style=\"color:black;\">" + getSicilno() + "</span>";
		}else if (getSicilno()!=null && getKayitdurum().getCode()!= UyeKayitDurum._ONAYLANMIS.getCode()) {
			return "<span style=\"color:red;\">" + getSicilno()  + "</span>";
		}else {
			return null;
		}
	}

	public String getOnaylanmamisUye() {
		if (getKisiRef()!=null){
			if (getSicilno()!=null && getKayitdurum().getCode()== UyeKayitDurum._ONAYLANMIS.getCode()){
				return "<span style=\"color:black;\">" + getKisiRef().getUIStringShort() + "</span>";
			}else if (getSicilno()==null && getKayitdurum().getCode()!= UyeKayitDurum._ONAYLANMIS.getCode()) {
				return "<span style=\"color:red;\">" + getKisiRef().getUIStringShort() + "</span>";
			} else if (getSicilno()!=null && getKayitdurum().getCode()!= UyeKayitDurum._ONAYLANMIS.getCode()) {
				return "<span style=\"color:red;\">" + getKisiRef().getUIStringShort() + "</span>";
			}
		}
		return getKisiRef().getUIString();


	}

	@Transient
	public KurumKisi getUyekurum() {
		try {
			if(getDBOperator().recordCount(KurumKisi.class.getSimpleName(), "o.kisiRef.rID=" + getKisiRef().getRID()) > 0){
				KurumKisi uyeKurum =(KurumKisi) getDBOperator().load(KurumKisi.class.getSimpleName(), "o.kisiRef.rID=" + getKisiRef().getRID(),"").get(0);
				return uyeKurum;
			}
		} catch(Exception e){

		}
		return null;

	}

	@Transient
	public Kisiogrenim getUyeogrenim() {
		try {
			if(getDBOperator().recordCount(Kisiogrenim.class.getSimpleName(), "o.uyeRef.kisiRef.rID=" + getKisiRef().getRID()) > 0){
				return (Kisiogrenim) getDBOperator().load(Kisiogrenim.class.getSimpleName(), "o.uyeRef.kisiRef.rID=" + getKisiRef().getRID(),"").get(0);
			} else {
				return null;
			}
		} catch(Exception e){
			return null;
		}
	}

	@Transient
	public Kisiogrenim getUyeogrenimlisans() {
		try {
            DBOperator operator = DBOperator.getInstance();
            if(operator.recordCount(Kisiogrenim.class.getSimpleName(), "o.kisiRef.rID=" + getKisiRef().getRID() + " AND o.ogrenimtip=" + OgrenimTip._LISANS.getCode()) > 0){
				return (Kisiogrenim) operator.load(Kisiogrenim.class.getSimpleName(), "o.kisiRef.rID=" + getKisiRef().getRID() +
                        " AND o.ogrenimtip=" + OgrenimTip._LISANS.getCode(), "").get(0);
			} else {
				return null;
			}
		} catch(Exception e){
			return null;
		}
	}

	@Transient
	public Kisikimlik getKisikimlik() {
		try {
			if(getDBOperator().recordCount(Kisikimlik.class.getSimpleName(), "o.kisiRef.rID=" + getKisiRef().getRID()) > 0){
				return (Kisikimlik) getDBOperator().load(Kisikimlik.class.getSimpleName(), "o.kisiRef.rID=" + getKisiRef().getRID(),"").get(0);
			} else {
				return null;
			}
		} catch(Exception e){
			return null;
		}
	}


	@Override
	public String toString() {
		return getUIString();
	}

	public String getIletisimTercih() {
		StringBuffer result = new StringBuffer();
		result.append("<b>").append(KeyUtil.getLabelValue("uye.iletisimEmail")).append(" : </b>").append(getIletisimEmail().getLabel()).append("<br/>");
		result.append("<b>").append(KeyUtil.getLabelValue("uye.iletisimSms")).append(" : </b>").append(getIletisimSms().getLabel()).append("<br/>");
		result.append("<b>").append(KeyUtil.getLabelValue("uye.iletisimPosta")).append(" : </b>").append(getIletisimPosta().getLabel());
		return result.toString();
	}

	@Override
	public String getUIString() {
		if (getSicilno()!=null){
			return getKisiRef().getUIStringShort()+ ", " + "(" + getSicilno() + ")";
		}
		else {
			return	getKisiRef().getUIStringShort();
		}
	}

	@Override
	public void initValues(boolean defaultValues) {
		if (defaultValues) {
				// TODO Initialize with default values
		} else {
			this.rID = null;
			this.kisiRef = null;
			this.sicilno = "";
			this.uyetip = UyeTip._NULL;
			this.birimRef = null;
			this.iletisimEmail = EvetHayir._NULL;
			this.iletisimPosta = EvetHayir._NULL;
			this.iletisimSms = EvetHayir._NULL;
			this.uyeliktarih = null;
			this.ayrilmatarih = null;
			this.uyedurum = UyeDurum._NULL;
			this.kayitdurum = UyeKayitDurum._NULL;
			this.onaylayanRef = null;
			this.onaytarih = null;
			this.ogrenciUyeRef = null;
			this.sinif = null;
			this.ogrenciNo = "";
            this.kartTalepTarihi = null;
            this.kartTeslimTarihi = null;
			this.teslimAdresi = "";
		}
	}

	@Override
	public String getValue() {
		StringBuilder value = new StringBuilder("");
//			if(getKisiRef() != null) {
				value.append(KeyUtil.getLabelValue("uye.kisiRef") + " : " + getKisiRef() + "<br />");
//			}
//			if(getSicilno() != null) {
				value.append(KeyUtil.getLabelValue("uye.sicilno") + " : " + getSicilno() + "<br />");
//			}
//			if(getUyetip() != null) {
				value.append(KeyUtil.getLabelValue("uye.uyetip") + " : " + getUyetip() + "<br />");
//			}
//			if(getBirimRef() != null) {
				value.append(KeyUtil.getLabelValue("uye.birimRef") + " : " + getBirimRef() + "<br />");
//			}
//			if(getUyeliktarih() != null) {
				value.append(KeyUtil.getLabelValue("uye.uyeliktarih") + " : " + DateUtil.dateToDMY(getUyeliktarih()) + "<br />");
//			}
//			if(getAyrilmatarih() != null) {
				value.append(KeyUtil.getLabelValue("uye.ayrilmatarih") + " : " + DateUtil.dateToDMY(getAyrilmatarih()) + "<br />");
//			}
//			if(getUyedurum() != null) {
				value.append(KeyUtil.getLabelValue("uye.uyedurum") + " : " + getUyedurum() + "<br />");
//			}
//			if(getKayitdurum() != null) {
				value.append(KeyUtil.getLabelValue("uye.kayitdurum") + " : " + getKayitdurum() + "<br />");
//			}
//			if(getOnaylayanRef() != null) {
				value.append(KeyUtil.getLabelValue("uye.onaylayanRef") + " : " + getOnaylayanRef() + "<br />");
//			}
//			if(getOnaytarih() != null) {
				value.append(KeyUtil.getLabelValue("uye.onaytarih") + " : " + DateUtil.dateToDMY(getOnaytarih()) + "<br />");
//			}
//			if(getSinif() != null) {
				value.append(KeyUtil.getLabelValue("uye.sinif") + " : " + getSinif() + "<br />");
//			}
//			if(getOgrenciNo() != null) {
				value.append(KeyUtil.getLabelValue("uye.ogrenciNo") + " : " + getOgrenciNo() + "<br />");
//			}
//			if(getOgrenciUyeRef() != null) {
				value.append(KeyUtil.getLabelValue("uye.ogrenciUyeRef") + " : " + getOgrenciUyeRef() + "<br />");
//			}
//			if(getIletisimEmail() != null){
				value.append(KeyUtil.getLabelValue("uye.iletisimEmail") + " : " + getIletisimEmail().getLabel() + "<br />");
//			}
//			if(getIletisimPosta() != null){
				value.append(KeyUtil.getLabelValue("uye.iletisimPosta") + " : " + getIletisimPosta().getLabel() + "<br />");
//			}
//			if(getIletisimSms() != null){
				value.append(KeyUtil.getLabelValue("uye.iletisimSms") + " : " + getIletisimSms().getLabel() + "<br />");
//			}
		return value.toString();
	}

    public Date getKartTeslimTarihi() {
        return kartTeslimTarihi;
    }

    public void setKartTeslimTarihi(Date kartTeslimTarihi) {
        this.kartTeslimTarihi = kartTeslimTarihi;
    }

	public String getTeslimAdresi() {
		return teslimAdresi;
	}

	public void setTeslimAdresi(String teslimAdresi) {
		this.teslimAdresi = teslimAdresi;
	}
}
