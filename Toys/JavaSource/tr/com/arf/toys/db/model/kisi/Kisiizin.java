package tr.com.arf.toys.db.model.kisi; 
 
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import tr.com.arf.framework.utility.tool.KeyUtil;
import tr.com.arf.toys.db.enumerated.kisi.KisiIzinDurum;
import tr.com.arf.toys.db.model.base.ToysBaseEntity;
import tr.com.arf.toys.db.model.evrak.Evrak;
 
@Entity 
@Table(name = "KISIIZIN") 
public class Kisiizin extends ToysBaseEntity { 
 
	private static final long serialVersionUID = 1L; 
 
	@Id 
	@GeneratedValue(strategy = GenerationType.IDENTITY) 
	@Column(updatable = false, unique = true, nullable = false, name = "RID") 
	private Long rID; 
 
	@ManyToOne 
	@JoinColumn(name = "KISIREF") 
	private Kisi kisiRef; 
 
	@ManyToOne 
	@JoinColumn(name = "EVRAKREF") 
	private Evrak evrakRef; 
 
	@Temporal(TemporalType.DATE) 
	@Column(name = "BASLANGICTARIH") 
	private java.util.Date baslangictarih; 
 
	@Temporal(TemporalType.DATE) 
	@Column(name = "BITISTARIH") 
	private java.util.Date bitistarih; 
 
	@Enumerated(EnumType.ORDINAL) 
	@Column(name = "KISIIZINDURUM") 
	private KisiIzinDurum kisiizindurum; 
 
	@Column(name = "ACIKLAMA") 
	private String aciklama; 
 
 
	public Kisiizin() { 
		super(); 
	} 
 
	@Override
	public Long getRID() { 
		return this.rID;  
	}  
	  
	@Override
	public void setRID(Long rID) {  
		this.rID = rID;  
	}   
 
	public Kisi getKisiRef() { 
		return kisiRef; 
	} 
	 
	public void setKisiRef(Kisi kisiRef) { 
		this.kisiRef = kisiRef; 
	} 
 
	public Evrak getEvrakRef() { 
		return evrakRef; 
	} 
	 
	public void setEvrakRef(Evrak evrakRef) { 
		this.evrakRef = evrakRef; 
	} 
 
	public java.util.Date getBaslangictarih() { 
		return baslangictarih; 
	} 
	 
	public void setBaslangictarih(java.util.Date baslangictarih) { 
		this.baslangictarih = baslangictarih; 
	} 
 
	public java.util.Date getBitistarih() { 
		return bitistarih; 
	} 
	 
	public void setBitistarih(java.util.Date bitistarih) { 
		this.bitistarih = bitistarih; 
	} 
 
	public KisiIzinDurum getKisiizindurum() { 
		return this.kisiizindurum;  
	}  
  
	public void setKisiizindurum(KisiIzinDurum kisiizindurum) {  
		this.kisiizindurum = kisiizindurum;  
	}   
 
	public String getAciklama() { 
		return this.aciklama; 
	} 
	 
	public void setAciklama(String aciklama) { 
		this.aciklama = aciklama; 
	} 
 
 
	@Override 
	public String toString() { 
		return "tr.com.arf.toys.db.model.kisi.Kisiizin[id=" + this.getRID()  + "]"; 
	} 
	@Override 
	public String getUIString() { 
		return getAciklama() + ""; 
	} 
	 
	@Override 
	public void initValues(boolean defaultValues) { 
		if (defaultValues) { 
				// TODO Initialize with default values 
		} else { 
			this.rID = null; 			this.kisiRef = null; 			this.evrakRef = null; 			this.baslangictarih = null; 			this.bitistarih = null; 			this.kisiizindurum = KisiIzinDurum._NULL; 			this.aciklama = ""; 		}  
	}  
	 
	@Override 
	public String getValue() {  
		StringBuilder value = new StringBuilder(""); 
			if(getKisiRef() != null) 
				value.append(KeyUtil.getLabelValue("kisiizin.kisiRef") + " : " + getKisiRef().getUIString() + "<br />"); 
			if(getEvrakRef() != null) 
				value.append(KeyUtil.getLabelValue("kisiizin.evrakRef") + " : " + getEvrakRef().getUIString() + "<br />"); 
			if(getBaslangictarih() != null) 
				value.append(KeyUtil.getLabelValue("kisiizin.baslangictarih") + " : " + getBaslangictarih() + "<br />"); 
			if(getBitistarih() != null) 
				value.append(KeyUtil.getLabelValue("kisiizin.bitistarih") + " : " + getBitistarih() + "<br />"); 
			if(getKisiizindurum() != null) 
				value.append(KeyUtil.getLabelValue("kisiizin.kisiizindurum") + " : " + getKisiizindurum() + "<br />"); 
			if(getAciklama() != null) 
				value.append(KeyUtil.getLabelValue("kisiizin.aciklama") + " : " + getAciklama() + ""); 
		return value.toString(); 
	} 
	 
}	 
