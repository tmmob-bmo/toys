package tr.com.arf.toys.db.model.uye;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tr.com.arf.framework.db.model._base.BaseEntity;
import tr.com.arf.framework.utility.tool.KeyUtil;
import tr.com.arf.toys.db.enumerated.uye.Formturu;

@Entity
@Table(name = "UYEFORM")
public class Uyeform extends BaseEntity {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(updatable = false, unique = true, nullable = false, name = "RID")
	private Long rID;

	@ManyToOne
	@JoinColumn(name = "UYEREF")
	private Uye uyeRef;

	@Enumerated(EnumType.ORDINAL)
	@Column(name = "FORMTURU")
	private Formturu formturu;

	@Column(name = "PATH")
	private String path;

	public Uyeform() {
		super();
	}

	@Override
	public Long getRID() {
		return this.rID;
	}

	@Override
	public void setRID(Long rID) {
		this.rID = rID;
	}

	public Uye getUyeRef() {
		return uyeRef;
	}

	public void setUyeRef(Uye uyeRef) {
		this.uyeRef = uyeRef;
	}

	public Formturu getFormturu() {
		return this.formturu;
	}

	public void setFormturu(Formturu formturu) {
		this.formturu = formturu;
	}

	@Override
	public String getPath() {
		return this.path;
	}

	@Override
	public void setPath(String path) {
		this.path = path;
	}

	@Override
	public String toString() {
		return getUIString();
	}

	@Override
	public String getUIString() {
		return getPath() + "";
	}

	@Override
	public void initValues(boolean defaultValues) {
		this.rID = null;
		this.uyeRef = null;
		this.formturu = Formturu._NULL;
		this.path = "";
	}

	@Override
	public String getValue() {
		StringBuilder value = new StringBuilder("Veri : <br />");
		if (getUyeRef() != null) {
			value.append(KeyUtil.getLabelValue("uyeform.uyeRef") + " : " + getUyeRef().getUIString() + "<br />");
		}
		if (getFormturu() != null) {
			value.append(KeyUtil.getLabelValue("uyeform.formturu") + " : " + getFormturu().getLabel() + "<br />");
		}
		if (getPath() != null) {
			value.append(KeyUtil.getLabelValue("uyeform.path") + " : " + getPath() + "");
		}
		return value.toString();
	}

}
