package tr.com.arf.toys.db.model.system; 
 
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tr.com.arf.framework.db.enumerated._common.EvetHayir;
import tr.com.arf.framework.utility.tool.KeyUtil;
import tr.com.arf.toys.db.model.base.ToysBaseEntity;
import tr.com.arf.toys.db.model.kisi.Kurum;
 
 
@Entity 
@Table(name = "KURUMHESAP") 
public class KurumHesap extends ToysBaseEntity { 
 
	private static final long serialVersionUID = 1L; 
 
	@Id 
	@GeneratedValue(strategy = GenerationType.IDENTITY) 
	@Column(updatable = false, unique = true, nullable = false, name = "RID") 
	private Long rID; 
	
	@ManyToOne 
	@JoinColumn(name = "BANKAREF") 
	private Banka bankaRef; 
	
	@Column(name = "HESAPNO") 
	private Long hesapno; 
	
	@Column(name = "SUBEKODU") 
	private Long subekodu; 
	
	@Column(name = "IBANNUMARASI") 
	private Long ibannumarasi; 
	
	@ManyToOne 
	@JoinColumn(name = "KURUMREF") 
	private Kurum kurumRef; 
	
	@Enumerated(EnumType.ORDINAL)
	@Column(name = "GECERLI") 
	private EvetHayir gecerli; 
 
 
	public KurumHesap() { 
		super(); 
	} 
 
	@Override
	public Long getRID() { 
		return this.rID;  
	}  
	  
	@Override
	public void setRID(Long rID) {  
		this.rID = rID;  
	}   
 
	public Long getHesapno() {
		return hesapno;
	}

	public void setHesapno(Long hesapno) {
		this.hesapno = hesapno;
	}

	public Long getSubekodu() {
		return subekodu;
	}

	public void setSubekodu(Long subekodu) {
		this.subekodu = subekodu;
	}

	public Long getIbannumarasi() {
		return ibannumarasi;
	}

	public void setIbannumarasi(Long ibannumarasi) {
		this.ibannumarasi = ibannumarasi;
	}

	public Kurum getKurumRef() {
		return kurumRef;
	}

	public void setKurumRef(Kurum kurumRef) {
		this.kurumRef = kurumRef;
	}
	
	public EvetHayir getGecerli() {
		return gecerli;
	}

	public void setGecerli(EvetHayir gecerli) {
		this.gecerli = gecerli;
	}

	public Banka getBankaRef() {
		return bankaRef;
	}

	public void setBankaRef(Banka bankaRef) {
		this.bankaRef = bankaRef;
	}

	@Override 
	public String toString() { 
		return "tr.com.arf.toys.db.model.system.KurumHesap[id=" + this.getRID()  + "]"; 
	} 
	@Override 
	public String getUIString() { 
		return getBankaRef().getAd() + ""; 
	} 
	 
	@Override 
	public void initValues(boolean defaultValues) { 
		if (defaultValues) { 
				// TODO Initialize with default values 
		} else { 
			this.rID = null; 			this.bankaRef=null;
			this.hesapno=null;
			this.ibannumarasi=null;
			this.subekodu=null;
			this.kurumRef=null;
			this.gecerli = EvetHayir._NULL;
		}  
	}  
	 
	@Override 
	public String getValue() {  
		StringBuilder value = new StringBuilder(""); 
			if(getBankaRef() != null) 
				value.append(KeyUtil.getLabelValue("kurumhesap.bankaRef") + " : " + getBankaRef() + "<br />"); 
			if(getHesapno() != null) 
				value.append(KeyUtil.getLabelValue("kurumhesap.hesapno") + " : " + getHesapno() + "<br />"); 
			if(getSubekodu() != null) 
				value.append(KeyUtil.getLabelValue("kurumhesap.subekodu") + " : " + getSubekodu() + "<br />"); 
			if(getIbannumarasi() != null) 
				value.append(KeyUtil.getLabelValue("kurumhesap.ibannumarasi") + " : " + getIbannumarasi() + "<br />"); 
			if(getKurumRef() != null) 
				value.append(KeyUtil.getLabelValue("kurumhesap.kurumRef") + " : " + getKurumRef().getUIString() + "<br />"); 
			if(getGecerli() != null) 
				value.append(KeyUtil.getLabelValue("kurumhesap.gecerli") + " : " + getGecerli() + ""); 
			
		return value.toString(); 
	} 
	 
}	 
