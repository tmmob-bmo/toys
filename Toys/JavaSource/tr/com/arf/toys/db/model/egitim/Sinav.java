package tr.com.arf.toys.db.model.egitim; 
 
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import tr.com.arf.framework.db.enumerated._common.EvetHayir;
import tr.com.arf.framework.utility.tool.DateUtil;
import tr.com.arf.framework.utility.tool.KeyUtil;
import tr.com.arf.toys.db.enumerated.egitim.SinavDurum;
import tr.com.arf.toys.db.enumerated.egitim.SinavTuru;
import tr.com.arf.toys.db.model.base.ToysBaseEntity;
import tr.com.arf.toys.db.model.system.Ilce;
import tr.com.arf.toys.db.model.system.Sehir;
 
@Entity 
@Table(name = "SINAV") 
public class Sinav extends ToysBaseEntity { 
 
	private static final long serialVersionUID = 1L; 
 
	@Id 
	@GeneratedValue(strategy = GenerationType.IDENTITY) 
	@Column(updatable = false, unique = true, nullable = false, name = "RID") 
	private Long rID; 
 
	@Column(name = "AD") 
	private String ad; 
	
	@ManyToOne 
	@JoinColumn(name = "EGITIMREF") 
	private Egitim egitimRef; 
	
	@ManyToOne 
	@JoinColumn(name = "SEHIRREF") 
	private Sehir sehirRef; 
	
	@ManyToOne 
	@JoinColumn(name = "ILCEREF") 
	private Ilce ilceRef; 
	
	@Column(name = "ACIKLAMA") 
	private String aciklama; 
	
	@Column(name = "PBKKOD") 
	private String pbkkod; 
	
	@Column(name = "KONTENJAN") 
	private BigDecimal kontenjan; 
	
	@Temporal(TemporalType.TIMESTAMP) 
	@Column(name = "BASLANGICTARIH") 
	private java.util.Date baslangictarih;
	
	@Temporal(TemporalType.TIMESTAMP) 
	@Column(name = "BITISTARIH") 
	private java.util.Date bitistarih;

	@Column(name = "YAYINDA") 
	private EvetHayir yayinda;
	
	@Enumerated(EnumType.ORDINAL) 
	@Column(name = "SINAVTURU") 
	private SinavTuru sinavturu; 
	
	@Enumerated(EnumType.ORDINAL) 
	@Column(name = "SINAVDURUM") 
	private SinavDurum sinavdurum; 
 
	
	public Sinav() { 
		super(); 
	} 
 
	@Override
	public Long getRID() { 
		return this.rID;  
	}  
	  
	@Override
	public void setRID(Long rID) {  
		this.rID = rID;  
	}   
 
	public Egitim getEgitimRef() {
		return egitimRef;
	}

	public void setEgitimRef(Egitim egitimRef) {
		this.egitimRef = egitimRef;
	}

	public Sehir getSehirRef() {
		return sehirRef;
	}

	public void setSehirRef(Sehir sehirRef) {
		this.sehirRef = sehirRef;
	}

	public Ilce getIlceRef() {
		return ilceRef;
	}

	public void setIlceRef(Ilce ilceRef) {
		this.ilceRef = ilceRef;
	}

	public String getPbkkod() {
		return pbkkod;
	}

	public void setPbkkod(String pbkkod) {
		this.pbkkod = pbkkod;
	}

	public BigDecimal getKontenjan() {
		return kontenjan;
	}

	public void setKontenjan(BigDecimal kontenjan) {
		this.kontenjan = kontenjan;
	}

	public java.util.Date getBaslangictarih() {
		return baslangictarih;
	}

	public void setBaslangictarih(java.util.Date baslangictarih) {
		this.baslangictarih = baslangictarih;
	}

	public java.util.Date getBitistarih() {
		return bitistarih;
	}

	public void setBitistarih(java.util.Date bitistarih) {
		this.bitistarih = bitistarih;
	}

	public EvetHayir getYayinda() {
		return yayinda;
	}

	public void setYayinda(EvetHayir yayinda) {
		this.yayinda = yayinda;
	}

	public String getAciklama() {
		return aciklama;
	}

	public void setAciklama(String aciklama) {
		this.aciklama = aciklama;
	}

	public String getAd() {
		return ad;
	}

	public void setAd(String ad) {
		this.ad = ad;
	}
	
	public SinavTuru getSinavturu() {
		return sinavturu;
	}

	public void setSinavturu(SinavTuru sinavturu) {
		this.sinavturu = sinavturu;
	}

	public SinavDurum getSinavdurum() {
		return sinavdurum;
	}

	public void setSinavdurum(SinavDurum sinavdurum) {
		this.sinavdurum = sinavdurum;
	}

	@Override 
	public String toString() { 
		return "tr.com.arf.toys.db.model.egitim.Sinav[id=" + this.getRID()  + "]"; 
	} 
	@Override 
	public String getUIString() { 
		return getAd();
	} 
	 
	@Override 
	public void initValues(boolean defaultValues) { 
			this.rID = null; 			this.aciklama=null;
			this.baslangictarih=null;
			this.bitistarih=null;
			this.egitimRef=null;
			this.ilceRef=null;
			this.kontenjan=null;
			this.pbkkod=null;
			this.sehirRef=null;
			this.yayinda=EvetHayir._NULL;
			this.ad=null;
			this.sinavturu=SinavTuru._NULL;
			this.sinavdurum=SinavDurum._NULL;	}  
	 
	@Override 
	public String getValue() {  
		StringBuilder value = new StringBuilder("Veri : <br />"); 
			if(getIlceRef() != null) {
				value.append(KeyUtil.getLabelValue("sinav.ilceRef") + " : " + getIlceRef().getUIString() + "<br />");
			} 
			if(getSehirRef() != null) {
				value.append(KeyUtil.getLabelValue("sinav.sehirRef") + " : " + getSehirRef().getUIString() + "<br />");
			} 
			if(getEgitimRef() != null) {
				value.append(KeyUtil.getLabelValue("sinav.egitimRef") + " : " + getEgitimRef().getUIString() + "<br />");
			} 
			if(getAciklama() != null) {
				value.append(KeyUtil.getLabelValue("sinav.aciklama") + " : " + getAciklama() + "<br />");
			} 
			if(getKontenjan() != null) {
				value.append(KeyUtil.getLabelValue("sinav.kontenjan") + " : " + getKontenjan() + "<br />");
			} 
			if(getYayinda() != null) {
				value.append(KeyUtil.getLabelValue("sinav.yayinda") + " : " + getYayinda().getLabel() + "<br />");
			} 
			if(getPbkkod()!= null) {
				value.append(KeyUtil.getLabelValue("sinav.pbkkod") + " : " + getPbkkod() + "<br />");
			} 
			if(getBaslangictarih() != null) {
				value.append(KeyUtil.getLabelValue("sinav.baslangictarih") + " : " + DateUtil.dateToDMYHMS(getBaslangictarih()) + "<br />");
			}
			if(getBitistarih() != null) {
				value.append(KeyUtil.getLabelValue("sinav.bitistarih") + " : " + DateUtil.dateToDMYHMS(getBitistarih()) + "<br />");
			}
			if(getAd()!= null) {
				value.append(KeyUtil.getLabelValue("sinav.ad") + " : " + getAd() + "<br />");
			} 
			if(getSinavturu() != null) {
				value.append(KeyUtil.getLabelValue("sinav.sinavturu") + " : " + getSinavturu().getLabel() + "<br />");
			} 
			if(getSinavdurum() != null) {
				value.append(KeyUtil.getLabelValue("sinav.sinavdurum") + " : " + getSinavdurum().getLabel() + "<br />");
			} 
			
		return value.toString(); 
	} 
	 
}	 
