package tr.com.arf.toys.db.model.kisi; 
 
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tr.com.arf.framework.db.enumerated._common.EvetHayir;
import tr.com.arf.framework.utility.tool.KeyUtil;
import tr.com.arf.toys.db.model.base.ToysBaseEntity;
import tr.com.arf.toys.db.model.uye.Uye;
 
@Entity 
@Table(name = "ISYERITEMSILCILERI") 
public class IsYeriTemsilcileri extends ToysBaseEntity { 
 
	private static final long serialVersionUID = 1L; 
 
	@Id 
	@GeneratedValue(strategy = GenerationType.IDENTITY) 
	@Column(updatable = false, unique = true, nullable = false, name = "RID") 
	private Long rID; 
 
	@ManyToOne 
	@JoinColumn(name = "KURUMREF") 
	private Kurum kurumRef;
	
	@ManyToOne 
	@JoinColumn(name = "UYEREF") 
	private Uye uyeRef;
	
	@Enumerated(EnumType.ORDINAL)
	@Column(name = "AKTIF") 
	private EvetHayir aktif; 
	
	public IsYeriTemsilcileri() { 
		super(); 
	} 
 
	@Override
	public Long getRID() { 
		return this.rID;  
	}  
	
	@Override
	public void setRID(Long rID) {  
		this.rID = rID;  
	} 
	  
	public Kurum getKurumRef() {
		return kurumRef;
	}

	public void setKurumRef(Kurum kurumRef) {
		this.kurumRef = kurumRef;
	}

	public Uye getUyeRef() {
		return uyeRef;
	}

	public void setUyeRef(Uye uyeRef) {
		this.uyeRef = uyeRef;
	}

	public EvetHayir getAktif() {
		return aktif;
	}

	public void setAktif(EvetHayir aktif) {
		this.aktif = aktif;
	}

	@Override 
	public String toString() { 
		return "tr.com.arf.toys.db.model.kisi.IsYeriTemsilciligi[id=" + this.getRID()  + "]"; 
	} 
	
	@Override 
	public String getUIString() { 
		return "( " + getKurumRef().getAd() + " )  " + getUyeRef().getKisiRef().getAd() + " " + getUyeRef().getKisiRef().getSoyad(); 
	} 
	 
	@Override 
	public void initValues(boolean defaultValues) { 
		if (defaultValues) { 
				// TODO Initialize with default values 
		} else { 
			this.rID = null; 			this.kurumRef=null;
			this.uyeRef=null;
			this.aktif=EvetHayir._NULL;		}  
	}  
	 
	@Override 
	public String getValue() {  
		StringBuilder value = new StringBuilder(""); 
			if(getKurumRef() != null) {
				value.append(KeyUtil.getLabelValue("isyeritemsilcileri.kurumRef") + " : " + getKurumRef().getUIString() + "<br />");
			} 
			if(getUyeRef() != null) {
				value.append(KeyUtil.getLabelValue("isyeritemsilcileri.uyeRef") + " : " + getUyeRef().getUIString() + "<br />");
			}
			if(getAktif() != null) {
				value.append(KeyUtil.getLabelValue("isyeritemsilcileri.aktif") + " : " + getAktif() + "");
			} 
		return value.toString(); 
	}
	 
}	 
