package tr.com.arf.toys.db.model.system; 
 
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import tr.com.arf.framework.utility.tool.DateUtil;
import tr.com.arf.framework.utility.tool.KeyUtil;
import tr.com.arf.toys.db.enumerated.system.RaporTuru;
import tr.com.arf.toys.db.model.base.ToysBaseEntity;
 
@Entity 
@Table(name = "RAPORLOG") 
public class Raporlog extends ToysBaseEntity { 
 
	private static final long serialVersionUID = 1L; 
 
	@Id 
	@GeneratedValue(strategy = GenerationType.IDENTITY) 
	@Column(updatable = false, unique = true, nullable = false, name = "RID") 
	private Long rID; 
 
	@ManyToOne 
	@JoinColumn(name = "KULLANICIREF") 
	private Kullanici kullaniciRef; 
 
	@Temporal(TemporalType.DATE) 
	@Column(name = "TARIH") 
	private java.util.Date tarih; 
 
	@Enumerated(EnumType.ORDINAL) 
	@Column(name = "RAPORTURU") 
	private RaporTuru raporturu; 
 
 
	public Raporlog() { 
		super(); 
	} 
	
	public Raporlog(RaporTuru raporturu, Kullanici kullaniciRef) {
		super();
		this.raporturu = raporturu;
		this.kullaniciRef = kullaniciRef;
		this.tarih = new Date();
	} 
 
	@Override
	public Long getRID() { 
		return this.rID;  
	}  
	  
	@Override
	public void setRID(Long rID) {  
		this.rID = rID;  
	}   
 
	public Kullanici getKullaniciRef() { 
		return kullaniciRef; 
	} 
	 
	public void setKullaniciRef(Kullanici kullaniciRef) { 
		this.kullaniciRef = kullaniciRef; 
	} 
 
	public java.util.Date getTarih() { 
		return tarih; 
	} 
	 
	public void setTarih(java.util.Date tarih) { 
		this.tarih = tarih; 
	} 
 
	public RaporTuru getRaporturu() { 
		return this.raporturu;  
	}  
  
	public void setRaporturu(RaporTuru raporturu) {  
		this.raporturu = raporturu;  
	}   
 
 
	@Override 
	public String toString() { 
		return "tr.com.arf.toys.db.model.system.Raporlog[id=" + this.getRID()  + "]"; 
	} 
	@Override 
	public String getUIString() { 
		return getKullaniciRef() + ""; 
	} 
	 
	@Override 
	public void initValues(boolean defaultValues) { 
			this.rID = null; 			this.kullaniciRef = null; 			this.tarih = null; 			this.raporturu = RaporTuru._NULL; 	}  
	 
	@Override 
	public String getValue() {  
		StringBuilder value = new StringBuilder("Veri : <br />"); 
			if(getKullaniciRef() != null) {
				value.append(KeyUtil.getLabelValue("raporlog.kullaniciRef") + " : " + getKullaniciRef().getUIString() + "<br />");
			} 
			if(getTarih() != null) {
				value.append(KeyUtil.getLabelValue("raporlog.tarih") + " : " + DateUtil.dateToDMYHMS(getTarih()) + "<br />");
			} 
			if(getRaporturu() != null) {
				value.append(KeyUtil.getLabelValue("raporlog.raporturu") + " : " + getRaporturu().getLabel() + "");
			} 
		return value.toString(); 
	} 
	 
}	 
