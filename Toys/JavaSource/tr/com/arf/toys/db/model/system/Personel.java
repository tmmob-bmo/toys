package tr.com.arf.toys.db.model.system;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import tr.com.arf.framework.db.enumerated._common.EvetHayir;
import tr.com.arf.framework.utility.tool.KeyUtil;
import tr.com.arf.toys.db.enumerated.system.PersonelDurum;
import tr.com.arf.toys.db.model.base.ToysBaseEntity;
import tr.com.arf.toys.db.model.kisi.Kisi;

@Entity
@Table(name = "PERSONEL")
public class Personel extends ToysBaseEntity {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(updatable = false, unique = true, nullable = false, name = "RID")
	private Long rID;

	@Temporal(TemporalType.DATE)
	@Column(name = "BASLAMATARIH")
	private java.util.Date baslamatarih;

	@Temporal(TemporalType.DATE)
	@Column(name = "BITISTARIH")
	private java.util.Date bitistarih;

	@Enumerated(EnumType.ORDINAL)
	@Column(name = "DURUM")
	private PersonelDurum durum;

	@ManyToOne
	@JoinColumn(name = "KISIREF")
	private Kisi kisiRef;

	@ManyToOne
	@JoinColumn(name = "BIRIMREF")
	private Birim birimRef;

	@Enumerated(EnumType.ORDINAL)
	@Column(name = "tumUyeSorgulama")
	private EvetHayir tumUyeSorgulama;

	public Personel() {
		super();
		this.tumUyeSorgulama = EvetHayir._HAYIR;
	}

	@Override
	public Long getRID() {
		return this.rID;
	}

	@Override
	public void setRID(Long rID) {
		this.rID = rID;
	}

	public java.util.Date getBaslamatarih() {
		return baslamatarih;
	}

	public void setBaslamatarih(java.util.Date baslamatarih) {
		this.baslamatarih = baslamatarih;
	}

	public java.util.Date getBitistarih() {
		return bitistarih;
	}

	public void setBitistarih(java.util.Date bitistarih) {
		this.bitistarih = bitistarih;
	}

	public PersonelDurum getDurum() {
		return this.durum;
	}

	public void setDurum(PersonelDurum durum) {
		this.durum = durum;
	}

	public Kisi getKisiRef() {
		return kisiRef;
	}

	public void setKisiRef(Kisi kisiRef) {
		this.kisiRef = kisiRef;
	}

	public Birim getBirimRef() {
		return birimRef;
	}

	public void setBirimRef(Birim birimRef) {
		this.birimRef = birimRef;
	}

	public EvetHayir getTumUyeSorgulama() {
		return tumUyeSorgulama;
	}

	public void setTumUyeSorgulama(EvetHayir tumUyeSorgulama) {
		this.tumUyeSorgulama = tumUyeSorgulama;
	}

	@Override
	public String getUIString() {
		if (getKisiRef() == null) {
			return "";
		}
		return getKisiRef().getUIStringShort();
	}

	public String getUIStringShort() {
		if (getKisiRef() == null) {
			return "";
		}
		return getKisiRef().getUIString();
	}

	public String getUIStringShortest() {
		if (getKisiRef() == null) {
			return "";
		}
		return getKisiRef().getUIStringShort();
	}

	@Override
	public void initValues(boolean defaultValues) {
		if (defaultValues) {
			// TODO Initialize with default values
		} else {
			this.rID = null;
			this.baslamatarih = null;
			this.bitistarih = null;
			this.durum = PersonelDurum._NULL;
			this.kisiRef = null;
			this.birimRef = null;
			this.tumUyeSorgulama = EvetHayir._HAYIR;
		}
	}

	@Override
	public String getValue() {
		StringBuilder value = new StringBuilder("");
		if (getBaslamatarih() != null) {
			value.append(KeyUtil.getLabelValue("personel.baslamatarih") + " : " + getBaslamatarih() + "<br />");
		}
		if (getBitistarih() != null) {
			value.append(KeyUtil.getLabelValue("personel.bitistarih") + " : " + getBitistarih() + "<br />");
		}
		if (getDurum() != null) {
			value.append(KeyUtil.getLabelValue("personel.durum") + " : " + getDurum() + "<br />");
		}
		if (getKisiRef() != null) {
			value.append(KeyUtil.getLabelValue("personel.kisiRef") + " : " + getKisiRef().getUIString() + "");
		}
		if (getBirimRef() != null) {
			value.append(KeyUtil.getLabelValue("personel.birimRef") + " : " + getBirimRef().getUIString() + "");
		}
		if (getTumUyeSorgulama() != null) {
			value.append(KeyUtil.getLabelValue("personel.tumUyeSorgulama") + " : " + getTumUyeSorgulama().getLabel() + "<br />");
		}
		return value.toString();
	}

}
