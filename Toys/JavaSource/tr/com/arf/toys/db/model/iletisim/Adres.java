package tr.com.arf.toys.db.model.iletisim;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import tr.com.arf.framework.db.enumerated._common.EvetHayir;
import tr.com.arf.framework.utility.tool.DateUtil;
import tr.com.arf.framework.utility.tool.KeyUtil;
import tr.com.arf.toys.db.enumerated.iletisim.AdresTipi;
import tr.com.arf.toys.db.enumerated.iletisim.AdresTuru;
import tr.com.arf.toys.db.model.base.ToysBaseEntity;
import tr.com.arf.toys.db.model.kisi.Kisi;
import tr.com.arf.toys.db.model.kisi.Kurum;
import tr.com.arf.toys.db.model.system.Ilce;
import tr.com.arf.toys.db.model.system.Sehir;
import tr.com.arf.toys.db.model.system.Ulke;


@Entity
@Table(name = "ADRES")
public class Adres extends ToysBaseEntity {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(updatable = false, unique = true, nullable = false, name = "RID")
	private Long rID;

	@ManyToOne
	@JoinColumn(name = "KISIREF")
	private Kisi kisiRef;

	@ManyToOne
	@JoinColumn(name = "KURUMREF")
	private Kurum kurumRef;

	@Enumerated(EnumType.ORDINAL)
	@Column(name = "VARSAYILAN")
	private EvetHayir varsayilan;

	@Column(name = "ADRESTURU")
	private AdresTuru adresturu;

	@Enumerated(EnumType.ORDINAL)
	@Column(name = "ADRESTIPI")
	private AdresTipi adrestipi;

	@Temporal(TemporalType.DATE)
	@Column(name = "KPSGUNCELLEMETARIHI")
	private Date kpsGuncellemeTarihi;

	@Column(name = "ACIKADRES")
	private String acikAdres;

	@Column(name = "ADRESNO")
	private String adresNo;

	@Column(name = "BINAADA")
	private String binaAda;

	@Column(name = "BINAPAFTA")
	private String binaPafta;

	@Column(name = "BINAPARSEL")
	private String binaParsel;

	@Temporal(TemporalType.DATE)
	@Column(name = "TASINMATARIHI")
	private Date tasinmaTarihi;

	@Temporal(TemporalType.DATE)
	@Column(name = "BEYANTARIHI")
	private Date beyanTarihi;

	@Temporal(TemporalType.DATE)
	@Column(name = "TESCILTARIHI")
	private Date tescilTarihi;

	@Column(name = "BINABLOKADI")
	private String binaBlokAdi;

	@Column(name = "BINAKODU")
	private String binaKodu;

	@Column(name = "BINASITEADI")
	private String binaSiteAdi;

	@Column(name = "CSBM")
	private String csbm;

	@Column(name = "CSBMKODU")
	private String csbmKodu;

	@Column(name = "DISKAPINO")
	private String disKapiNo;

	@Column(name = "ICKAPINO")
	private String icKapiNo;

	@ManyToOne
	@JoinColumn(name = "ULKEREF")
	private Ulke ulkeRef;

	@ManyToOne
	@JoinColumn(name = "SEHIRREF")
	private Sehir sehirRef;

	@ManyToOne
	@JoinColumn(name = "ILCEREF")
	private Ilce ilceRef;

	@Column(name = "MAHALLE")
	private String mahalle;

	@Column(name = "MAHALLEKODU")
	private String mahalleKodu;

	@Column(name = "KOY")
	private String koy;

	@Column(name = "KOYKOD")
	private String koykod;

	@Column(name = "BUCAK")
	private String bucak;

	@Column(name = "BUCAKKOD")
	private String bucakkod;

	@Column(name = "YABANCIULKE")
	private String yabanciulke;

	@Column(name = "YABANCISEHIR")
	private String yabancisehir;

	@Column(name = "YABANCIADRES")
	private String yabanciadres;

	public Adres() {
		super();
		this.varsayilan = EvetHayir._HAYIR;
	}

	@Override
	public Long getRID() {
		return this.rID;
	}

	@Override
	public void setRID(Long rID) {
		this.rID = rID;
	}

	public Kisi getKisiRef() {
		return kisiRef;
	}

	public void setKisiRef(Kisi kisiRef) {
		this.kisiRef = kisiRef;
	}

	public Kurum getKurumRef() {
		return kurumRef;
	}

	public void setKurumRef(Kurum kurumRef) {
		this.kurumRef = kurumRef;
	}

	public AdresTuru getAdresturu() {
		return this.adresturu;
	}

	public void setAdresturu(AdresTuru adresturu) {
		this.adresturu = adresturu;
	}

	public EvetHayir getVarsayilan() {
		return varsayilan;
	}

	public void setVarsayilan(EvetHayir varsayilan) {
		this.varsayilan = varsayilan;
	}

	public AdresTipi getAdrestipi() {
		return adrestipi;
	}

	public void setAdrestipi(AdresTipi adrestipi) {
		this.adrestipi = adrestipi;
	}

	public Date getKpsGuncellemeTarihi() {
		return kpsGuncellemeTarihi;
	}

	public void setKpsGuncellemeTarihi(Date kpsGuncellemeTarihi) {
		this.kpsGuncellemeTarihi = kpsGuncellemeTarihi;
	}

	public String getAcikAdres() {
		return acikAdres;
	}

	public void setAcikAdres(String acikAdres) {
		this.acikAdres = acikAdres;
	}

	public String getAdresNo() {
		return adresNo;
	}

	public void setAdresNo(String adresNo) {
		this.adresNo = adresNo;
	}

	public String getBinaBlokAdi() {
		return binaBlokAdi;
	}

	public void setBinaBlokAdi(String binaBlokAdi) {
		this.binaBlokAdi = binaBlokAdi;
	}

	public String getBinaKodu() {
		return binaKodu;
	}

	public void setBinaKodu(String binaKodu) {
		this.binaKodu = binaKodu;
	}

	public String getBinaSiteAdi() {
		return binaSiteAdi;
	}

	public void setBinaSiteAdi(String binaSiteAdi) {
		this.binaSiteAdi = binaSiteAdi;
	}

	public String getCsbm() {
		return csbm;
	}

	public void setCsbm(String csbm) {
		this.csbm = csbm;
	}

	public String getCsbmKodu() {
		return csbmKodu;
	}

	public void setCsbmKodu(String csbmKodu) {
		this.csbmKodu = csbmKodu;
	}

	public String getDisKapiNo() {
		return disKapiNo;
	}

	public void setDisKapiNo(String disKapiNo) {
		this.disKapiNo = disKapiNo;
	}

	public String getIcKapiNo() {
		return icKapiNo;
	}

	public void setIcKapiNo(String icKapiNo) {
		this.icKapiNo = icKapiNo;
	}

	public Ulke getUlkeRef() {
		return ulkeRef;
	}

	public void setUlkeRef(Ulke ulkeRef) {
		this.ulkeRef = ulkeRef;
	}

	public Sehir getSehirRef() {
		return sehirRef;
	}

	public void setSehirRef(Sehir sehirRef) {
		this.sehirRef = sehirRef;
	}

	public Ilce getIlceRef() {
		return ilceRef;
	}

	public void setIlceRef(Ilce ilceRef) {
		this.ilceRef = ilceRef;
	}

	public String getMahalle() {
		return mahalle;
	}

	public void setMahalle(String mahalle) {
		this.mahalle = mahalle;
	}

	public String getMahalleKodu() {
		return mahalleKodu;
	}

	public void setMahalleKodu(String mahalleKodu) {
		this.mahalleKodu = mahalleKodu;
	}

	public String getBinaAda() {
		return binaAda;
	}

	public void setBinaAda(String binaAda) {
		this.binaAda = binaAda;
	}

	public String getBinaPafta() {
		return binaPafta;
	}

	public void setBinaPafta(String binaPafta) {
		this.binaPafta = binaPafta;
	}

	public String getBinaParsel() {
		return binaParsel;
	}

	public void setBinaParsel(String binaParsel) {
		this.binaParsel = binaParsel;
	}

	public Date getTasinmaTarihi() {
		return tasinmaTarihi;
	}

	public void setTasinmaTarihi(Date tasinmaTarihi) {
		this.tasinmaTarihi = tasinmaTarihi;
	}

	public Date getBeyanTarihi() {
		return beyanTarihi;
	}

	public void setBeyanTarihi(Date beyanTarihi) {
		this.beyanTarihi = beyanTarihi;
	}

	public Date getTescilTarihi() {
		return tescilTarihi;
	}

	public void setTescilTarihi(Date tescilTarihi) {
		this.tescilTarihi = tescilTarihi;
	}

	public String getKoy() {
		return koy;
	}

	public void setKoy(String koy) {
		this.koy = koy;
	}

	public String getKoykod() {
		return koykod;
	}

	public void setKoykod(String koykod) {
		this.koykod = koykod;
	}

	public String getBucak() {
		return bucak;
	}

	public void setBucak(String bucak) {
		this.bucak = bucak;
	}

	public String getBucakkod() {
		return bucakkod;
	}

	public void setBucakkod(String bucakkod) {
		this.bucakkod = bucakkod;
	}

	public String getYabanciulke() {
		return yabanciulke;
	}

	public void setYabanciulke(String yabanciulke) {
		this.yabanciulke = yabanciulke;
	}

	public String getYabancisehir() {
		return yabancisehir;
	}

	public void setYabancisehir(String yabancisehir) {
		this.yabancisehir = yabancisehir;
	}

	public String getYabanciadres() {
		return yabanciadres;
	}

	public void setYabanciadres(String yabanciadres) {
		this.yabanciadres = yabanciadres;
	}

	@Override
	public String toString() {
		return "tr.com.arf.toys.db.model.iletisim.Adres[id=" + this.getRID()  + "]";
	}
	@Override
	public String getUIString() {
		if (getAdrestipi()!=null){
		return getAdrestipi().getLabel() + "";
		}
		return "";
	}

	@Override
	public void initValues(boolean defaultValues) {
		if (defaultValues) {
				// TODO Initialize with default values
		} else {
			this.rID = null;			this.kisiRef = null;			this.kurumRef = null;			this.adresturu = AdresTuru._NULL;
			this.varsayilan = EvetHayir._HAYIR;
			this.adrestipi = AdresTipi._NULL;
			this.kpsGuncellemeTarihi = null;
			this.acikAdres = null;
			this.adresNo = null;
			this.binaBlokAdi = null;
			this.binaKodu = null;
			this.binaSiteAdi = null;
			this.csbm = null;
			this.csbmKodu = null;
			this.disKapiNo = null;
			this.icKapiNo = null;
			this.sehirRef = null;
			this.ilceRef = null;
			this.mahalle = null;
			this.mahalleKodu = null;
			this.tescilTarihi = null;
			this.beyanTarihi = null;
			this.tasinmaTarihi = null;
			this.binaAda = null;
			this.binaPafta = null;
			this.binaParsel = null;
			this.ulkeRef = null;
			this.bucakkod=null;
			this.bucak=null;
			this.koy=null;
			this.koykod=null;
			this.yabanciadres=null;
			this.yabancisehir=null;
			this.yabanciulke=null;
		}
	}

	@Override
	public String getValue() {
		StringBuilder value = new StringBuilder("");
			if(getKisiRef() != null) {
				value.append(KeyUtil.getLabelValue("adres.kisiRef") + " : " + getKisiRef().getUIString() + "<br />");
			}
			if(getKurumRef() != null) {
				value.append(KeyUtil.getLabelValue("adres.kurumRef") + " : " + getKurumRef().getUIString() + "<br />");
			}
			if(getVarsayilan() != null) {
				value.append(KeyUtil.getLabelValue("adres.varsayilan") + " : " + getVarsayilan() + "<br />");
			}
			if(getAdresturu() != null) {
				value.append(KeyUtil.getLabelValue("adres.adresturu") + " : " + getAdresturu().getLabel() + "");
			}
			if(getAdrestipi() != null) {
				value.append(KeyUtil.getLabelValue("adres.adrestipi") + " : " + getAdrestipi().getLabel() + "");
			}
			if(getKpsGuncellemeTarihi() != null) {
				value.append(KeyUtil.getLabelValue("adres.kpsGuncellemeTarihi") + " : " + DateUtil.dateToDMY(getKpsGuncellemeTarihi()));
			}
			if(getAcikAdres() != null) {
				value.append(KeyUtil.getLabelValue("adres.acikAdres") + " : " + getAcikAdres());
			}
			if(getAdresNo() != null) {
				value.append(KeyUtil.getLabelValue("adres.adresNo") + " : " + getAdresNo());
			}
			if(getBinaBlokAdi() != null) {
				value.append(KeyUtil.getLabelValue("adres.binaBlokAdi") + " : " + getBinaBlokAdi());
			}
			if(getBinaKodu() != null) {
				value.append(KeyUtil.getLabelValue("adres.binaKodu") + " : " + getBinaKodu());
			}
			if(getBinaSiteAdi() != null) {
				value.append(KeyUtil.getLabelValue("adres.binaSiteAdi") + " : " + getBinaSiteAdi());
			}
			if(getCsbm() != null) {
				value.append(KeyUtil.getLabelValue("adres.csbm") + " : " + getCsbm());
			}
			if(getCsbmKodu() != null) {
				value.append(KeyUtil.getLabelValue("adres.csbmKodu") + " : " + getCsbmKodu());
			}
			if(getDisKapiNo() != null) {
				value.append(KeyUtil.getLabelValue("adres.disKapiNo") + " : " + getDisKapiNo());
			}
			if(getIcKapiNo() != null) {
				value.append(KeyUtil.getLabelValue("adres.icKapiNo") + " : " + getIcKapiNo());
			}
			if(getSehirRef() != null) {
				value.append(KeyUtil.getLabelValue("adres.sehirRef") + " : " + getSehirRef().getAd());
			}
			if(getIlceRef() != null) {
				value.append(KeyUtil.getLabelValue("adres.ilceRef") + " : " + getIlceRef().getAd());
			}
			if(getMahalle() != null) {
				value.append(KeyUtil.getLabelValue("adres.mahalle") + " : " + getMahalle());
			}
			if(getMahalleKodu() != null) {
				value.append(KeyUtil.getLabelValue("adres.mahalleKodu") + " : " + getMahalleKodu());
			}
			if(getBucak() != null) {
				value.append(KeyUtil.getLabelValue("adres.bucak") + " : " + getBucak());
			}
			if(getBucakkod() != null) {
				value.append(KeyUtil.getLabelValue("adres.bucakkod") + " : " + getBucakkod());
			}
			if(getKoy() != null) {
				value.append(KeyUtil.getLabelValue("adres.koy") + " : " + getKoy());
			}
			if(getKoykod() != null) {
				value.append(KeyUtil.getLabelValue("adres.koykod") + " : " + getKoykod());
			}
			if(getYabanciadres() != null) {
				value.append(KeyUtil.getLabelValue("adres.yabanciadres") + " : " + getYabanciadres());
			}
			if(getYabancisehir() != null) {
				value.append(KeyUtil.getLabelValue("adres.yabancisehir") + " : " + getYabancisehir());
			}
			if(getYabanciulke() != null) {
				value.append(KeyUtil.getLabelValue("adres.yabanciulke") + " : " + getYabanciulke());
			}

		return value.toString();
	}

}
