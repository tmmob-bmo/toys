package tr.com.arf.toys.db.model.system;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import tr.com.arf.framework.utility.tool.KeyUtil;
import tr.com.arf.toys.db.model.base.ToysBaseEntity;

@Entity
@Table(name = "INSANPERSONEL")
public class Insanpersonel extends ToysBaseEntity {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(updatable=false,unique=true,nullable=false,name="RID")
	private Long rID;

	@Column(name="INSANREF")
	private Insan insanref;
	
	@Column(name="SICILNO")
	private Long sicilno;
	
	@Override
	public Long getRID() {
		return rID;
	}
	@Override
	public void setRID(Long rid) {
		this.rID = rid;
	}

	public Insan getInsanref() {
		return insanref;
	}

	public void setInsanref(Insan insanref) {
		this.insanref = insanref;
	}

	public Long getSicilno() {
		return sicilno;
	}

	public void setSicilno(Long sicilno) {
		this.sicilno = sicilno;
	}

	

	@Override
	public void initValues(boolean defaultValues) {
		if(defaultValues){
		}else{
			this.rID=null;
			this.insanref=null;
			this.sicilno=null;
			
		}
		
	}
	
	public String toString() { 
		return "tr.com.arf.toys.db.model.system.Insanpersonel[id=" + this.getRID()  + "]"; 
	} 
	
	
	
	@Override
	public String getUIString() {
		return getInsanref().getUIString();
	}

	
	public String getValue() {  
		StringBuilder value = new StringBuilder(""); 
			if(getSicilno() != null) {
				value.append(KeyUtil.getLabelValue("insanpersonel.sicilno") + " : " + getSicilno() + "<br />");
			} 
			if(getInsanref() != null) {
				value.append(KeyUtil.getLabelValue("insanpersonel.insanref") + " : " + getInsanref().getUIString() + "<br />");
			} 
			
		return value.toString(); 
	} 
	
	

}
