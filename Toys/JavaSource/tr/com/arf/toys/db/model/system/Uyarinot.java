package tr.com.arf.toys.db.model.system; 
 
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import tr.com.arf.framework.utility.tool.KeyUtil;
import tr.com.arf.toys.db.enumerated.system.UyariDurum;
import tr.com.arf.toys.db.enumerated.system.UyariTipi;
import tr.com.arf.toys.db.model.base.ToysBaseEntity;
import tr.com.arf.toys.db.model.kisi.Kisi;
 
@Entity 
@Table(name = "UYARINOT") 
public class Uyarinot extends ToysBaseEntity { 
 
	private static final long serialVersionUID = 1L; 
 
	@Id 
	@GeneratedValue(strategy = GenerationType.IDENTITY) 
	@Column(updatable = false, unique = true, nullable = false, name = "RID") 
	private Long rID; 
 
	@ManyToOne 
	@JoinColumn(name = "KULLANICIREF") 
	private Kisi kullaniciRef; 
 
	@ManyToOne 
	@JoinColumn(name = "BIRIMREF") 
	private Birim birimRef; 
 
	@Enumerated(EnumType.ORDINAL) 
	@Column(name = "UYARITIP") 
	private UyariTipi uyaritip; 
 
	@Temporal(TemporalType.DATE) 
	@Column(name = "TARIH") 
	private java.util.Date tarih; 
 
	@Temporal(TemporalType.DATE) 
	@Column(name = "GECERLILIKTARIH") 
	private java.util.Date gecerliliktarih; 
 
	@Column(name = "UYARIMETIN") 
	private String uyarimetin; 
 
	@Enumerated(EnumType.ORDINAL) 
	@Column(name = "UYARIDURUM") 
	private UyariDurum uyariDurum; 
 
 
	public Uyarinot() { 
		super(); 
	} 
 
	@Override
	public Long getRID() { 
		return this.rID;  
	}  
	  
	@Override
	public void setRID(Long rID) {  
		this.rID = rID;  
	}   
 
	public Kisi getKullaniciRef() { 
		return kullaniciRef; 
	} 
	 
	public void setKullaniciRef(Kisi kullaniciRef) { 
		this.kullaniciRef = kullaniciRef; 
	} 
 
	public Birim getBirimRef() { 
		return birimRef; 
	} 
	 
	public void setBirimRef(Birim birimRef) { 
		this.birimRef = birimRef; 
	} 
 
	public UyariTipi getUyaritip() { 
		return this.uyaritip;  
	}  
  
	public void setUyaritip(UyariTipi uyaritip) {  
		this.uyaritip = uyaritip;  
	}   
 
	public java.util.Date getTarih() { 
		return tarih; 
	} 
	 
	public void setTarih(java.util.Date tarih) { 
		this.tarih = tarih; 
	} 
 
	public java.util.Date getGecerliliktarih() { 
		return gecerliliktarih; 
	} 
	 
	public void setGecerliliktarih(java.util.Date gecerliliktarih) { 
		this.gecerliliktarih = gecerliliktarih; 
	} 
 
	public String getUyarimetin() { 
		return this.uyarimetin; 
	} 
	 
	public void setUyarimetin(String uyarimetin) { 
		this.uyarimetin = uyarimetin; 
	} 
 
	public UyariDurum getUyariDurum() { 
		return this.uyariDurum;  
	}  
  
	public void setUyariDurum(UyariDurum uyariDurum) {  
		this.uyariDurum = uyariDurum;  
	}   
 
 
	@Override 
	public String toString() { 
		return "tr.com.arf.toys.db.model.system.Uyarinot[id=" + this.getRID()  + "]"; 
	} 
	@Override 
	public String getUIString() { 
		return getUyarimetin() + ""; 
	} 
	 
	@Override 
	public void initValues(boolean defaultValues) { 
		if (defaultValues) { 
				// TODO Initialize with default values 
		} else { 
			this.rID = null; 			this.kullaniciRef = null; 			this.birimRef = null; 			this.uyaritip = UyariTipi._NULL; 			this.tarih = null; 			this.gecerliliktarih = null; 			this.uyarimetin = ""; 			this.uyariDurum = UyariDurum._NULL; 		}  
	}  
	 
	@Override 
	public String getValue() {  
		StringBuilder value = new StringBuilder(""); 
			if(getKullaniciRef() != null) 
				value.append(KeyUtil.getLabelValue("uyarinot.kullaniciRef") + " : " + getKullaniciRef().getUIString() + "<br />"); 
			if(getBirimRef() != null) 
				value.append(KeyUtil.getLabelValue("uyarinot.birimRef") + " : " + getBirimRef().getUIString() + "<br />"); 
			if(getUyaritip() != null) 
				value.append(KeyUtil.getLabelValue("uyarinot.uyaritip") + " : " + getUyaritip() + "<br />"); 
			if(getTarih() != null) 
				value.append(KeyUtil.getLabelValue("uyarinot.tarih") + " : " + getTarih() + "<br />"); 
			if(getGecerliliktarih() != null) 
				value.append(KeyUtil.getLabelValue("uyarinot.gecerliliktarih") + " : " + getGecerliliktarih() + "<br />"); 
			if(getUyarimetin() != null) 
				value.append(KeyUtil.getLabelValue("uyarinot.uyarimetin") + " : " + getUyarimetin() + "<br />"); 
			if(getUyariDurum() != null) 
				value.append(KeyUtil.getLabelValue("uyarinot.uyariDurum") + " : " + getUyariDurum() + ""); 
		return value.toString(); 
	} 
	 
}	 
