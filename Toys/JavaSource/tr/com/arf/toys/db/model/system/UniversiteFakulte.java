package tr.com.arf.toys.db.model.system; 
 
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tr.com.arf.framework.utility.tool.KeyUtil;
import tr.com.arf.toys.db.model.base.ToysBaseEntity;
 
 
@Entity 
@Table(name = "UNIVERSITEFAKULTE") 
public class UniversiteFakulte extends ToysBaseEntity { 
 
	private static final long serialVersionUID = 1L; 
 
	@Id 
	@GeneratedValue(strategy = GenerationType.IDENTITY) 
	@Column(updatable = false, unique = true, nullable = false, name = "RID") 
	private Long rID; 
 
	@ManyToOne
	@JoinColumn(name = "UNIVERSITEREF")
	private Universite universiteRef;

	@ManyToOne
	@JoinColumn(name = "FAKULTEREF")
	private Fakulte fakulteRef;

	public UniversiteFakulte() { 
		super(); 
	} 
 
	@Override
	public Long getRID() { 
		return this.rID;  
	}  
	  
	@Override
	public void setRID(Long rID) {  
		this.rID = rID;  
	} 

	public Fakulte getFakulteRef() {
		return fakulteRef;
	}

	public void setFakulteRef(Fakulte fakulteRef) {
		this.fakulteRef = fakulteRef;
	}

	public Universite getUniversiteRef() {
		return universiteRef;
	}

	public void setUniversiteRef(Universite universiteRef) {
		this.universiteRef = universiteRef;
	}

	@Override 
	public String toString() { 
		return "tr.com.arf.toys.db.model.system.UniversiteFakulte[id=" + this.getRID()  + "]"; 
	} 
	@Override 
	public String getUIString() { 
		return getUniversiteRef().getAd() + " " +getFakulteRef().getAd();
	} 
	 
	@Override 
	public void initValues(boolean defaultValues) { 
		if (defaultValues) { 
				// TODO Initialize with default values 
		} else { 
			this.rID = null; 			this.universiteRef = null; 
			this.fakulteRef = null; 		}  
	}  
	 
	@Override 
	public String getValue() {  
		StringBuilder value = new StringBuilder(""); 
		if(getUniversiteRef() != null) {
			value.append(KeyUtil.getLabelValue("universiteFakulte.universiteRef") + " : " + getUniversiteRef().getUIString() + "<br />");
		} 
		if(getFakulteRef() != null) {
			value.append(KeyUtil.getLabelValue("universiteFakulte.fakulteRef") + " : " + getFakulteRef().getUIString() + "<br />");
		} 
		return value.toString(); 
	} 
	 
}	 
