package tr.com.arf.toys.db.model.gorev; 
 
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tr.com.arf.framework.db.model._base.BaseTreeEntity;
import tr.com.arf.framework.utility.tool.KeyUtil;
import tr.com.arf.toys.db.model.base.ToysBaseTreeEntity;
 
 
@Entity 
@Table(name = "GOREV") 
public class Gorev extends ToysBaseTreeEntity { 
 
	private static final long serialVersionUID = 1L; 
 
	@Id 
	@GeneratedValue(strategy = GenerationType.IDENTITY) 
	@Column(updatable = false, unique = true, nullable = false, name = "RID") 
	private Long rID; 
 
	@ManyToOne 
	@JoinColumn(name = "USTREF") 
	private Gorev ustRef; 
 
	@Column(name = "AD") 
	private String ad; 
 
	@Column(name = "ACIKLAMA") 
	private String aciklama; 
 
	@Column(name = "ERISIMKODU") 
	private String erisimKodu;  
	 
	
	public Gorev() { 
		super(); 
	} 
 
	@Override
	public Long getRID() { 
		return this.rID;  
	}  
	  
	@Override
	public void setRID(Long rID) {  
		this.rID = rID;  
	}   
 
	@Override
	public Gorev getUstRef() { 
		return ustRef; 
	} 
	 
	@Override
	public void setUstRef(BaseTreeEntity ustRef) { 
		this.ustRef = (Gorev) ustRef; 
	}
	
	public void setUstRef(Gorev ustRef) { 
		this.ustRef = ustRef;
	} 
 
	@Override
	public String getAd() { 
		return this.ad; 
	} 
	 
	@Override
	public void setAd(String ad) { 
		this.ad = ad; 
	}  
 
	public String getAciklama() { 
		return this.aciklama; 
	} 
	 
	public void setAciklama(String aciklama) { 
		this.aciklama = aciklama; 
	} 	
 
	@Override
	public String getErisimKodu() {
		return erisimKodu;
	}

	@Override
	public void setErisimKodu(String erisimKodu) {
		this.erisimKodu = erisimKodu;
	} 
 
	@Override 
	public String toString() { 
		return getAd();
	} 
	
	@Override 
	public String getUIString() { 
		String result = getAd() + ""; 
		if (getUstRef() != null) { 
			result += " / " + getUstRef().getAd() + "" ; 
		} 
		return result; 
	} 
	 
	@Override 
	public void initValues(boolean defaultValues) { 
		if (defaultValues) { 
				// TODO Initialize with default values 
		} else { 
			this.rID = null; 			this.ustRef = null; 			this.ad = ""; 			this.aciklama = ""; 
			this.erisimKodu = ""; 		}  
	}  
	 
	@Override 
	public String getValue() {  
		StringBuilder value = new StringBuilder(""); 
			if(getUstRef() != null) {
				value.append(KeyUtil.getLabelValue("gorev.ustRef") + " : " + getUstRef().getUIString() + "<br />");
			} 
			if(getAd() != null) {
				value.append(KeyUtil.getLabelValue("gorev.ad") + " : " + getAd() + "<br />");
			} 
			if(getAciklama() != null) {
				value.append(KeyUtil.getLabelValue("gorev.aciklama") + " : " + getAciklama() + "");
			} 
		return value.toString(); 
	} 
	 
}	 
