package tr.com.arf.toys.db.model.kisi; 
 
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import tr.com.arf.framework.db.enumerated._common.EvetHayir;
import tr.com.arf.framework.utility.tool.KeyUtil;
import tr.com.arf.toys.db.model.base.ToysBaseEntity;
 
@Entity 
@Table(name = "KURULDONEMTOPLANTIKATILIMCI") 
public class KurulDonemToplantiKatilimci extends ToysBaseEntity { 
 
	private static final long serialVersionUID = 1L; 
 
	@Id 
	@GeneratedValue(strategy = GenerationType.IDENTITY) 
	@Column(updatable = false, unique = true, nullable = false, name = "RID") 
	private Long rID; 
 
	@ManyToOne 
	@JoinColumn(name = "KURULDONEMUYEREF") 
	private KurulDonemUye kurulDonemUyeRef;

	
	@Enumerated(EnumType.ORDINAL)
	@Column(name = "KATILIM") 
	private EvetHayir katilim; 
	
	@ManyToOne 
	@JoinColumn(name = "KURULDONEMTOPLANTIREF") 
	private KurulDonemToplanti kurulDonemToplantiRef; 

	@Transient
	private boolean katilimi=false;
	

	public boolean isKatilimi() {
		if (katilim==EvetHayir._EVET)
			katilimi=true;
		else 
			katilimi=false;
		return katilimi;
	}

	public void setKatilimi(boolean katilimi) {
		if(katilimi){
			setKatilim(EvetHayir._EVET);
		} else {
			setKatilim(EvetHayir._HAYIR);
		}
		this.katilimi = katilimi;
	}
	
	public KurulDonemToplantiKatilimci() { 
		super(); 
	} 
 
	@Override
	public Long getRID() { 
		return this.rID;  
	}  
	  
	@Override
	public void setRID(Long rID) {  
		this.rID = rID;  
	}   
	
	public KurulDonemUye getKurulDonemUyeRef() {
		return kurulDonemUyeRef;
	}

	public void setKurulDonemUyeRef(KurulDonemUye kurulDonemUyeRef) {
		this.kurulDonemUyeRef = kurulDonemUyeRef;
	}

	public EvetHayir getKatilim() {
		return katilim;
	}

	public void setKatilim(EvetHayir katilim) {
		this.katilim = katilim;
	}

	public KurulDonemToplanti getKurulDonemToplantiRef() {
		return kurulDonemToplantiRef;
	}

	public void setKurulDonemToplantiRef(KurulDonemToplanti kurulDonemToplantiRef) {
		this.kurulDonemToplantiRef = kurulDonemToplantiRef;
	}

	@Override 
	public String toString() { 
		return "tr.com.arf.toys.db.model.kisi.KurulDonemToplantiKatilimci[id=" + this.getRID()  + "]"; 
	} 
	@Override 
	public String getUIString() { 
		return ""; 
	} 
	 
	@Override 
	public void initValues(boolean defaultValues) { 
		if (defaultValues) { 
				// TODO Initialize with default values 
		} else { 
			this.rID = null; 			this.kurulDonemUyeRef=null;
			this.katilim=EvetHayir._NULL;
			this.kurulDonemToplantiRef=null;
		}  
	}  
	 
	@Override 
	public String getValue() {  
		StringBuilder value = new StringBuilder("Veri : <br />"); 
			if(getKurulDonemUyeRef() != null) {
				value.append(KeyUtil.getLabelValue("kurulDonemToplantiKatilimci.kurulDonemUyeRef") + " : " + getKurulDonemUyeRef().getUIString() + "<br />");
			} 
			if(getKatilim() != null) {
				value.append(KeyUtil.getLabelValue("kurulDonemToplantiKatilimci.katilim") + " : " + getKatilim().getLabel() + "");
			} 
			if(getKurulDonemToplantiRef() != null) {
				value.append(KeyUtil.getLabelValue("kurulDonemToplantiKatilimci.kurulDonemToplantiRef") + " : " + getKurulDonemToplantiRef().getUIString() + "<br />");
			}
		
		return value.toString(); 
	} 
	 
}	