package tr.com.arf.toys.db.model.finans; 
 
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import tr.com.arf.framework.utility.tool.BigDecimalUtil;
import tr.com.arf.framework.utility.tool.KeyUtil;
import tr.com.arf.toys.db.enumerated.uye.OdemeTip;
import tr.com.arf.toys.db.model.base.ToysBaseEntity;
 
@Entity 
@Table(name = "BORCODEME") 
public class Borcodeme extends ToysBaseEntity { 
 
	private static final long serialVersionUID = 1L; 
 
	@Id 
	@GeneratedValue(strategy = GenerationType.IDENTITY) 
	@Column(updatable = false, unique = true, nullable = false, name = "RID") 
	private Long rID; 
 
	@ManyToOne 
	@JoinColumn(name = "BORCREF") 
	private Borc borcRef; 
 
	@Temporal(TemporalType.DATE) 
	@Column(name = "TARIH") 
	private java.util.Date tarih; 
 
	@Column(name = "MIKTAR") 
	private BigDecimal miktar; 
 
	@Enumerated(EnumType.ORDINAL) 
	@Column(name = "ODEMETIP") 
	private OdemeTip odemetip; 
	
	@ManyToOne 
	@JoinColumn(name = "ODEMEREF") 
	private Odeme odemeRef; 
 
 
	public Borcodeme() { 
		super(); 
	} 
 
	@Override
	public Long getRID() { 
		return this.rID;  
	}  
	  
	@Override
	public void setRID(Long rID) {  
		this.rID = rID;  
	}   
 
	public Borc getBorcRef() { 
		return borcRef; 
	} 
	 
	public void setBorcRef(Borc borcRef) { 
		this.borcRef = borcRef; 
	} 
 
	public java.util.Date getTarih() { 
		return tarih; 
	} 
	 
	public void setTarih(java.util.Date tarih) { 
		this.tarih = tarih; 
	} 
 
	public BigDecimal getMiktar() { 
		return BigDecimalUtil.changeToCurrency(this.miktar); 
	} 
	 
	public void setMiktar(BigDecimal miktar) { 
		this.miktar = BigDecimalUtil.changeToCurrency(miktar); 
	} 
 
	public OdemeTip getOdemetip() { 
		return this.odemetip;  
	}  
  
	public void setOdemetip(OdemeTip odemetip) {  
		this.odemetip = odemetip;  
	}    
 
	public Odeme getOdemeRef() {
		return odemeRef;
	}

	public void setOdemeRef(Odeme odemeRef) {
		this.odemeRef = odemeRef;
	}

	@Override 
	public String toString() { 
		return "tr.com.arf.toys.db.model.finans.Borcodeme[id=" + this.getRID()  + "]"; 
	} 
	
	@Override 
	public String getUIString() { 
		return getMiktar() + ""; 
	} 
	 
	@Override 
	public void initValues(boolean defaultValues) { 
		if (defaultValues) { 
				// TODO Initialize with default values 
		} else { 
			this.rID = null; 			this.borcRef = null; 			this.tarih = null; 			this.miktar = null; 			this.odemetip = OdemeTip._NULL; 
			this.odemeRef = null;		}  
	}  
	 
	@Override 
	public String getValue() {  
		StringBuilder value = new StringBuilder(""); 
			if(getBorcRef() != null) {
				value.append(KeyUtil.getLabelValue("borcodeme.borcRef") + " : " + getBorcRef().getUIString() + "<br />");
			} 
			if(getTarih() != null) {
				value.append(KeyUtil.getLabelValue("borcodeme.tarih") + " : " + getTarih() + "<br />");
			} 
			if(getMiktar() != null) {
				value.append(KeyUtil.getLabelValue("borcodeme.miktar") + " : " + getMiktar() + "<br />");
			} 
			if(getOdemetip() != null) {
				value.append(KeyUtil.getLabelValue("borcodeme.odemetip") + " : " + getOdemetip() + "");
			}
			if(getOdemeRef() != null) {
				value.append(KeyUtil.getLabelValue("borcodeme.odemeRef") + " : " + getOdemeRef().getUIString() + "<br />");
			} 
		return value.toString(); 
	} 
	 
}	 
