package tr.com.arf.toys.db.model.kisi; 
 
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tr.com.arf.framework.utility.tool.KeyUtil;
import tr.com.arf.toys.db.model.base.ToysBaseEntity;
import tr.com.arf.toys.db.model.system.Personel;
 
 
@Entity 
@Table(name = "KOMISYONDONEMTOPLANTITUTANAK") 
public class KomisyonDonemToplantiTutanak extends ToysBaseEntity { 
 
	private static final long serialVersionUID = 1L; 
 
	@Id 
	@GeneratedValue(strategy = GenerationType.IDENTITY) 
	@Column(updatable = false, unique = true, nullable = false, name = "RID") 
	private Long rID; 
 
	@ManyToOne 
	@JoinColumn(name = "KMSYNDONEMTOPLANTIREF") 
	private KomisyonDonemToplanti komisyonDonemToplantiRef; 
 
	@ManyToOne 
	@JoinColumn(name = "YUKLEYENREF") 
	private Personel yukleyenRef; 
 
	@Column(name = "PATH") 
	private String path; 
	
	@Column(name = "ACIKLAMA") 
	private String aciklama; 
	
	@Column(name = "DOSYAADI") 
	private String dosyaadi; 
	
 
 
	public KomisyonDonemToplantiTutanak() { 
		super(); 
	} 
 
	@Override
	public Long getRID() { 
		return this.rID;  
	}  
	  
	@Override
	public void setRID(Long rID) {  
		this.rID = rID;  
	}   
 
	public KomisyonDonemToplanti getKomisyonDonemToplantiRef() { 
		return komisyonDonemToplantiRef; 
	} 
	 
	public void setKomisyonDonemToplantiRef(KomisyonDonemToplanti komisyonDonemToplantiRef) { 
		this.komisyonDonemToplantiRef = komisyonDonemToplantiRef; 
	} 
 
	public Personel getYukleyenRef() { 
		return yukleyenRef; 
	} 
	 
	public void setYukleyenRef(Personel yukleyenRef) { 
		this.yukleyenRef = yukleyenRef; 
	} 
 
	public String getAciklama() {
		return aciklama;
	}

	public void setAciklama(String aciklama) {
		this.aciklama = aciklama;
	}

	public String getDosyaadi() {
		return dosyaadi;
	}

	public void setDosyaadi(String dosyaadi) {
		this.dosyaadi = dosyaadi;
	}

	@Override
	public String getPath() { 
		return this.path; 
	} 
	 
	@Override
	public void setPath(String path) { 
		this.path = path; 
	} 
 
 
	@Override 
	public String toString() { 
		return "tr.com.arf.toys.db.model.kisi.KomisyonDonemToplantiTutanak[id=" + this.getRID()  + "]"; 
	} 
	@Override 
	public String getUIString() { 
		return getPath() + ""; 
	} 
	 
	@Override 
	public void initValues(boolean defaultValues) { 
			this.rID = null; 			this.komisyonDonemToplantiRef = null; 			this.yukleyenRef = null; 			this.path = ""; 
			this.aciklama="";
			this.dosyaadi="";	}  
	 
	@Override 
	public String getValue() {  
		StringBuilder value = new StringBuilder("Veri : <br />"); 
			if(getKomisyonDonemToplantiRef() != null) {
				value.append(KeyUtil.getLabelValue("komisyonDonemToplantiTutanak.komisyonDonemToplantiRef") + " : " + getKomisyonDonemToplantiRef().getUIString() + "<br />");
			} 
			if(getYukleyenRef() != null) {
				value.append(KeyUtil.getLabelValue("komisyonDonemToplantiTutanak.yukleyenRef") + " : " + getYukleyenRef().getUIString() + "<br />");
			} 
			if(getPath() != null) {
				value.append(KeyUtil.getLabelValue("komisyonDonemToplantiTutanak.path") + " : " + getPath() + "");
			} 
			if(getAciklama() != null) {
				value.append(KeyUtil.getLabelValue("komisyonDonemToplantiTutanak.aciklama") + " : " + getAciklama() + "");
			} 
			if(getDosyaadi() != null) {
				value.append(KeyUtil.getLabelValue("komisyonDonemToplantiTutanak.dosyaadi") + " : " + getDosyaadi() + "");
			} 
		return value.toString(); 
	} 
	 
}	 
