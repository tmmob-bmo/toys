package tr.com.arf.toys.db.model.system; 
 
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tr.com.arf.framework.utility.tool.KeyUtil;
import tr.com.arf.toys.db.model.base.ToysBaseEntity;
 
 
@Entity 
@Table(name = "ILCE") 
public class Ilce extends ToysBaseEntity { 
 
	private static final long serialVersionUID = 1L; 
 
	@Id 
	@GeneratedValue(strategy = GenerationType.IDENTITY) 
	@Column(updatable = false, unique = true, nullable = false, name = "RID") 
	private Long rID; 
 
	@ManyToOne 
	@JoinColumn(name = "SEHIRREF") 
	private Sehir sehirRef; 
 
	@Column(name = "AD") 
	private String ad;  
	
	@Column(name = "ILCEKODU") 
	private Long ilcekodu; 
 
 
	public Ilce() { 
		super(); 
	} 
 
	@Override
	public Long getRID() { 
		return this.rID;  
	}  
	  
	@Override
	public void setRID(Long rID) {  
		this.rID = rID;  
	}   
 
	public Sehir getSehirRef() { 
		return sehirRef; 
	} 
	 
	public void setSehirRef(Sehir sehirRef) { 
		this.sehirRef = sehirRef; 
	} 
 
	public String getAd() { 
		return this.ad; 
	} 
	 
	public void setAd(String ad) { 
		this.ad = ad; 
	} 
 
	public Long getIlcekodu() {
		return ilcekodu;
	}

	public void setIlcekodu(Long ilcekodu) {
		this.ilcekodu = ilcekodu;
	} 
 
	@Override 
	public String toString() { 
		return "tr.com.arf.toys.db.model.system.Ilce[id=" + this.getRID()  + "]"; 
	} 
	@Override 
	public String getUIString() {
		return (sehirRef != null ? sehirRef.getAd() + " / " : "" ) + getAd() + "";
	} 
	 
	@Override 
	public void initValues(boolean defaultValues) { 
		if (defaultValues) { 
				// TODO Initialize with default values 
		} else { 
			this.rID = null; 
			this.sehirRef = null; 
			this.ad = ""; 
			this.ilcekodu=null;
		}  
	}  
	 
	@Override 
	public String getValue() {  
		StringBuilder value = new StringBuilder(""); 
			if(getSehirRef() != null) {
				value.append(KeyUtil.getLabelValue("ilce.sehirRef") + " : " + getSehirRef().getUIString() + "<br />");
			} 
			if(getAd() != null) {
				value.append(KeyUtil.getLabelValue("ilce.ad") + " : " + getAd() + "");
			} 
			if(getIlcekodu() != null) {
				value.append(KeyUtil.getLabelValue("ilce.ilcekodu") + " : " + getIlcekodu() + "");
			} 
		return value.toString(); 
	} 
	 
}	 
