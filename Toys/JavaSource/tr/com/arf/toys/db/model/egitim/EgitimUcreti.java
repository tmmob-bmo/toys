package tr.com.arf.toys.db.model.egitim; 
 
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tr.com.arf.framework.utility.tool.KeyUtil;
import tr.com.arf.toys.db.enumerated.egitim.EgitimUcretTipi;
import tr.com.arf.toys.db.model.base.ToysBaseEntity;
 
@Entity 
@Table(name = "EGITIMUCRETI") 
public class EgitimUcreti extends ToysBaseEntity { 
 
	private static final long serialVersionUID = 1L; 
 
	@Id 
	@GeneratedValue(strategy = GenerationType.IDENTITY) 
	@Column(updatable = false, unique = true, nullable = false, name = "RID") 
	private Long rID; 
 
	@ManyToOne 
	@JoinColumn(name = "EGITIMTANIMREF") 
	private Egitimtanim egitimtanimRef; 
	
	@Column(name = "YIL") 
	private int yil; 
	
	@Column(name = "UCRET") 
	private BigDecimal ucret; 
	
	@Enumerated(EnumType.ORDINAL) 
	@Column(name = "EGITIMUCRETTIPI") 
	private EgitimUcretTipi egitimUcretTipi; 
	
	public EgitimUcreti() { 
		super(); 
	} 
 
	@Override
	public Long getRID() { 
		return this.rID;  
	}  
	  
	@Override
	public void setRID(Long rID) {  
		this.rID = rID;  
	}   
 
	public Egitimtanim getEgitimtanimRef() { 
		return egitimtanimRef; 
	} 
	 
	public void setEgitimtanimRef(Egitimtanim egitimtanimRef) { 
		this.egitimtanimRef = egitimtanimRef; 
	} 

	public int getYil() {
		return yil;
	}

	public void setYil(int yil) {
		this.yil = yil;
	}

	public BigDecimal getUcret() {
		return ucret;
	}

	public void setUcret(BigDecimal ucret) {
		this.ucret = ucret;
	}

	public EgitimUcretTipi getEgitimUcretTipi() {
		return egitimUcretTipi;
	}

	public void setEgitimUcretTipi(EgitimUcretTipi egitimUcretTipi) {
		this.egitimUcretTipi = egitimUcretTipi;
	}

	@Override 
	public String toString() { 
		return "tr.com.arf.toys.db.model.egitim.EgitimUcreti[id=" + this.getRID()  + "]"; 
	} 
	@Override 
	public String getUIString() { 
		if (getEgitimtanimRef()!=null){
			return getEgitimtanimRef() + ""; 
		}else{
			return null;
		}
	} 
	 
	@Override 
	public void initValues(boolean defaultValues) { 
			this.rID = null; 			this.egitimtanimRef = null; 
			this.yil=0;
			this.ucret=null;
			this.egitimUcretTipi=EgitimUcretTipi._NULL;
	}  
	 
	@Override 
	public String getValue() {  
		StringBuilder value = new StringBuilder("Veri : <br />"); 
			if(getEgitimtanimRef() != null) {
				value.append(KeyUtil.getLabelValue("egitimUcreti.egitimtanimRef") + " : " + getEgitimtanimRef().getUIString() + "<br />");
			} 
			if(getYil() != 0) {
				value.append(KeyUtil.getLabelValue("egitimUcreti.yil") + " : " + getYil() + "<br />");
			} 
			if(getUcret() != null) {
				value.append(KeyUtil.getLabelValue("egitimUcreti.ucret") + " : " + getUcret() + "<br />");
			} 
			if(getEgitimUcretTipi() != null) {
				value.append(KeyUtil.getLabelValue("egitimUcreti.egitimUcretTipi") + " : " + getEgitimUcretTipi().getLabel() + "<br />");
			}
			
		return value.toString(); 
	} 
	 
}	 
