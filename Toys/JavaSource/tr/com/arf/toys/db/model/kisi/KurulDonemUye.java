package tr.com.arf.toys.db.model.kisi; 
 
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import tr.com.arf.framework.db.enumerated._common.EvetHayir;
import tr.com.arf.framework.utility.tool.KeyUtil;
import tr.com.arf.toys.db.enumerated.kisi.GorevTuru;
import tr.com.arf.toys.db.enumerated.kisi.KurulUyelikDurumu;
import tr.com.arf.toys.db.enumerated.kisi.UyeTuru;
import tr.com.arf.toys.db.model.base.ToysBaseEntity;
import tr.com.arf.toys.db.model.uye.Uye;
 
@Entity 
@Table(name = "KURULDONEMUYE") 
public class KurulDonemUye extends ToysBaseEntity { 
 
	private static final long serialVersionUID = 1L; 
 
	@Id 
	@GeneratedValue(strategy = GenerationType.IDENTITY) 
	@Column(updatable = false, unique = true, nullable = false, name = "RID") 
	private Long rID; 
 
	@ManyToOne 
	@JoinColumn(name = "KURULDONEMREF") 
	private KurulDonem kurulDonemRef;
	
	@ManyToOne 
	@JoinColumn(name = "UYEREF") 
	private Uye uyeRef;
	
	@Enumerated(EnumType.ORDINAL)
	@Column(name = "GOREVTURU") 
	private GorevTuru gorevTuru; 
	
	@Enumerated(EnumType.ORDINAL)
	@Column(name = "UYETURU") 
	private UyeTuru uyeTuru;
	
	@Enumerated(EnumType.ORDINAL)
	@Column(name = "AKTIF") 
	private EvetHayir aktif; 
	
	@Enumerated(EnumType.ORDINAL)
	@Column(name = "KURULUYELIKDURUMU") 
	private KurulUyelikDurumu kurulUyelikDurumu;
	
	@Temporal(TemporalType.DATE) 
	@Column(name = "DURUMTARIHI") 
	private java.util.Date durumtarihi; 
	
	public KurulDonemUye() { 
		super(); 
	} 
 
	@Override
	public Long getRID() { 
		return this.rID;  
	}  
	  
	@Override
	public void setRID(Long rID) {  
		this.rID = rID;  
	}   
	
	public KurulDonem getKurulDonemRef() {
		return kurulDonemRef;
	}

	public void setKurulDonemRef(KurulDonem kurulDonemRef) {
		this.kurulDonemRef = kurulDonemRef;
	}

	public Uye getUyeRef() {
		return uyeRef;
	}

	public void setUyeRef(Uye uyeRef) {
		this.uyeRef = uyeRef;
	}

	public GorevTuru getGorevTuru() {
		return gorevTuru;
	}

	public void setGorevTuru(GorevTuru gorevTuru) {
		this.gorevTuru = gorevTuru;
	}

	public UyeTuru getUyeTuru() {
		return uyeTuru;
	}

	public void setUyeTuru(UyeTuru uyeTuru) {
		this.uyeTuru = uyeTuru;
	}	

	public EvetHayir getAktif() {
		return aktif;
	}

	public void setAktif(EvetHayir aktif) {
		this.aktif = aktif;
	}

	public KurulUyelikDurumu getKurulUyelikDurumu() {
		return kurulUyelikDurumu;
	}

	public void setKurulUyelikDurumu(KurulUyelikDurumu kurulUyelikDurumu) {
		this.kurulUyelikDurumu = kurulUyelikDurumu;
	}

	public java.util.Date getDurumtarihi() {
		return durumtarihi;
	}

	public void setDurumtarihi(java.util.Date durumtarihi) {
		this.durumtarihi = durumtarihi;
	}

	@Override 
	public String toString() { 
		return "tr.com.arf.toys.db.model.kisi.KurulDonemUye[id=" + this.getRID()  + "]"; 
	} 
	
	@Override 
	public String getUIString() { 
		return getUyeRef().getKisiRef().getAd() +" " + getUyeRef().getKisiRef().getSoyad()  ; 
	} 
	 
	@Override 
	public void initValues(boolean defaultValues) { 
		if (defaultValues) { 
				// TODO Initialize with default values 
		} else { 
			this.rID = null; 			this.kurulDonemRef=null;
			this.uyeRef=null;
			this.gorevTuru=GorevTuru._NULL;
			this.uyeTuru = UyeTuru._NULL;	
			this.aktif = EvetHayir._NULL;
			this.durumtarihi=null;
			this.kurulUyelikDurumu=KurulUyelikDurumu._NULL;		}  
	}  
	 
	@Override 
	public String getValue() {  
		StringBuilder value = new StringBuilder("Veri : <br />"); 
			if(getKurulDonemRef() != null) {
				value.append(KeyUtil.getLabelValue("kurulDonemUye.kurulDonemRef") + " : " + getKurulDonemRef().getUIString() + "<br />");
			} 
			if(getGorevTuru() != null) {
				value.append(KeyUtil.getLabelValue("kurulDonemUye.gorevTuru") + " : " + getGorevTuru().getLabel() + "");
			} 
			if(getAktif() != null) {
				value.append(KeyUtil.getLabelValue("kurulDonemUye.aktif") + " : " + getAktif().getLabel() + "");
			} 
			if(getUyeTuru() != null) {
				value.append(KeyUtil.getLabelValue("kurulDonemUye.uyeTuru") + " : " + getUyeTuru().getLabel() + "");
			} 
			if(getUyeRef() != null) {
				value.append(KeyUtil.getLabelValue("kurulDonemUye.uyeRef") + " : " + getUyeRef().getUIString() + "<br />");
			}
			if(getKurulUyelikDurumu() != null) {
				value.append(KeyUtil.getLabelValue("kurulDonemUye.kurulUyelikDurumu") + " : " + getKurulUyelikDurumu().getLabel() + "");
			} 
			if(getDurumtarihi() != null) {
				value.append(KeyUtil.getLabelValue("kurulDonemUye.durumtarihi") + " : " + getDurumtarihi() + "");
			}
		return value.toString(); 
	} 
	 
}	