package tr.com.arf.toys.db.model.system;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import tr.com.arf.framework.utility.tool.KeyUtil;
import tr.com.arf.toys.db.model.base.ToysBaseEntity;

@Entity
@Table(name = "UZMANLIKTIP")
public class Uzmanliktip extends ToysBaseEntity {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(updatable = false, unique = true, nullable = false, name = "RID")
	private Long rID;

	@Column(name = "KOD")
	private String kod;

	@Column(name = "AD")
	private String ad;

	@Column(name = "ACIKLAMA")
	private String aciklama;

	public Uzmanliktip() {
		super();
	}

	@Override
	public Long getRID() {
		return this.rID;
	}

	@Override
	public void setRID(Long rID) {
		this.rID = rID;
	}

	public String getKod() {
		return this.kod;
	}

	public void setKod(String kod) {
		this.kod = kod;
	}

	public String getAd() {
		return this.ad;
	}

	public void setAd(String ad) {
		this.ad = ad;
	}

	public String getAciklama() {
		return this.aciklama;
	}

	public void setAciklama(String aciklama) {
		this.aciklama = aciklama;
	}

	@Override
	public String toString() {
		return "tr.com.arf.toys.db.model.system.Uzmanliktip[id=" + this.getRID() + "]";
	}

	@Override
	public String getUIString() {
		return getAd() + "";
	}

	@Override
	public void initValues(boolean defaultValues) {
		if (defaultValues) {
			// TODO Initialize with default values
		} else {
			this.rID = null;
			this.kod = "";
			this.ad = "";
			this.aciklama = "";
		}
	}

	@Override
	public String getValue() {
		StringBuilder value = new StringBuilder("");
		if (getKod() != null) {
			value.append(KeyUtil.getLabelValue("uzmanliktip.kod") + " : " + getKod() + "<br />");
		}
		if (getAd() != null) {
			value.append(KeyUtil.getLabelValue("uzmanliktip.ad") + " : " + getAd() + "<br />");
		}
		if (getAciklama() != null) {
			value.append(KeyUtil.getLabelValue("uzmanliktip.aciklama") + " : " + getAciklama() + "");
		}
		return value.toString();
	}
}
