package tr.com.arf.toys.db.model.kisi; 
 
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tr.com.arf.framework.utility.tool.KeyUtil;
import tr.com.arf.toys.db.model.base.ToysBaseEntity;
import tr.com.arf.toys.db.model.system.Birim;
import tr.com.arf.toys.db.model.system.Komisyon;
 
@Entity 
@Table(name = "KOMISYONDONEM") 
public class KomisyonDonem extends ToysBaseEntity { 
 
	private static final long serialVersionUID = 1L; 
 
	@Id 
	@GeneratedValue(strategy = GenerationType.IDENTITY) 
	@Column(updatable = false, unique = true, nullable = false, name = "RID") 
	private Long rID; 
 
	@ManyToOne 
	@JoinColumn(name = "KOMISYONREF") 
	private Komisyon komisyonRef;
	
	@ManyToOne 
	@JoinColumn(name = "DONEMREF") 
	private Donem donemRef;
	
	@Column(name = "ACIKLAMA") 
	private String aciklama; 
	
	@ManyToOne 
	@JoinColumn(name = "BIRIMREF") 
	private Birim birimRef;
	
	public KomisyonDonem() { 
		super(); 
	} 
 
	@Override
	public Long getRID() { 
		return this.rID;  
	}  
	  
	@Override
	public void setRID(Long rID) {  
		this.rID = rID;  
	}   
	
	public Komisyon getKomisyonRef() {
		return komisyonRef;
	}

	public void setKomisyonRef(Komisyon komisyonRef) {
		this.komisyonRef = komisyonRef;
	}

	public Donem getDonemRef() {
		return donemRef;
	}

	public void setDonemRef(Donem donemRef) {
		this.donemRef = donemRef;
	}

	public String getAciklama() {
		return aciklama;
	}

	public void setAciklama(String aciklama) {
		this.aciklama = aciklama;
	}

	public Birim getBirimRef() {
		return birimRef;
	}

	public void setBirimRef(Birim birimRef) {
		this.birimRef = birimRef;
	}

	@Override 
	public String toString() { 
		return "tr.com.arf.toys.db.model.kisi.KomisyonDonem[id=" + this.getRID()  + "]"; 
	} 
	@Override 
	public String getUIString() { 
		return getDonemRef().getTanim() + ", " + getKomisyonRef().getAd(); 
	} 
	 
	@Override 
	public void initValues(boolean defaultValues) { 
		if (defaultValues) { 
				// TODO Initialize with default values 
		} else { 
			this.rID = null; 			this.komisyonRef=null;
			this.donemRef=null;
			this.aciklama=null;
			this.birimRef=null;		}  
	}  
	 
	@Override 
	public String getValue() {  
		StringBuilder value = new StringBuilder(""); 
			if(getKomisyonRef() != null) 
				value.append(KeyUtil.getLabelValue("komisyonDonem.komisyonRef") + " : " + getKomisyonRef().getUIString() + "<br />"); 
			if(getDonemRef() != null) 
				value.append(KeyUtil.getLabelValue("komisyonDonem.donemRef") + " : " + getDonemRef().getUIString() + "<br />"); 
			if(getAciklama() != null) {
				value.append(KeyUtil.getLabelValue("komisyonDonem.aciklama") + " : " + getAciklama()+ "<br />");
			} 
			if(getBirimRef() != null) 
				value.append(KeyUtil.getLabelValue("komisyonDonem.birimRef") + " : " + getBirimRef().getUIString() + "<br />"); 
		return value.toString(); 
	} 
	 
}	 
