package tr.com.arf.toys.db.model.etkinlik; 
 
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tr.com.arf.framework.utility.tool.KeyUtil;
import tr.com.arf.toys.db.enumerated.etkinlik.Uyeturu;
import tr.com.arf.toys.db.model.base.ToysBaseEntity;
import tr.com.arf.toys.db.model.uye.Uye;
 
@Entity 
@Table(name = "ETKINLIKKURULUYE") 
public class Etkinlikkuruluye extends ToysBaseEntity { 
 
	private static final long serialVersionUID = 1L; 
 
	@Id 
	@GeneratedValue(strategy = GenerationType.IDENTITY) 
	@Column(updatable = false, unique = true, nullable = false, name = "RID") 
	private Long rID; 
 
	@ManyToOne 
	@JoinColumn(name = "ETKINLIKKURULREF") 
	private Etkinlikkurul etkinlikkurulRef; 
 
	@Enumerated(EnumType.ORDINAL) 
	@Column(name = "UYETURU") 
	private Uyeturu uyeturu; 
 
	@ManyToOne 
	@JoinColumn(name = "UYEREF") 
	private Uye uyeRef; 
 
 
	public Etkinlikkuruluye() { 
		super(); 
	} 
 
	@Override
	public Long getRID() { 
		return this.rID;  
	}  
	  
	@Override
	public void setRID(Long rID) {  
		this.rID = rID;  
	}   
 
	public Etkinlikkurul getEtkinlikkurulRef() { 
		return etkinlikkurulRef; 
	} 
	 
	public void setEtkinlikkurulRef(Etkinlikkurul etkinlikkurulRef) { 
		this.etkinlikkurulRef = etkinlikkurulRef; 
	} 
 
	public Uyeturu getUyeturu() { 
		return this.uyeturu;  
	}  
  
	public void setUyeturu(Uyeturu uyeturu) {  
		this.uyeturu = uyeturu;  
	}   
 
	public Uye getUyeRef() { 
		return uyeRef; 
	} 
	 
	public void setUyeRef(Uye uyeRef) { 
		this.uyeRef = uyeRef; 
	} 
 
 
	@Override 
	public String toString() { 
		return "tr.com.arf.toys.db.model.etkinlik.Etkinlikkuruluye[id=" + this.getRID()  + "]"; 
	} 
	@Override 
	public String getUIString() { 
		return getEtkinlikkurulRef() + ""; 
	} 
	 
	@Override 
	public void initValues(boolean defaultValues) { 
			this.rID = null; 			this.etkinlikkurulRef = null; 			this.uyeturu = Uyeturu._NULL; 			this.uyeRef = null; 	}  
	 
	@Override 
	public String getValue() {  
		StringBuilder value = new StringBuilder("Veri : <br />"); 
			if(getEtkinlikkurulRef() != null) {
				value.append(KeyUtil.getLabelValue("etkinlikkuruluye.etkinlikkurulRef") + " : " + getEtkinlikkurulRef().getUIString() + "<br />");
			} 
			if(getUyeturu() != null) {
				value.append(KeyUtil.getLabelValue("etkinlikkuruluye.uyeturu") + " : " + getUyeturu().getLabel() + "<br />");
			} 
			if(getUyeRef() != null) {
				value.append(KeyUtil.getLabelValue("etkinlikkuruluye.uyeRef") + " : " + getUyeRef().getUIString() + "");
			} 
		return value.toString(); 
	} 
	 
}	 
