package tr.com.arf.toys.db.model.egitim; 
 
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tr.com.arf.framework.utility.tool.KeyUtil;
import tr.com.arf.toys.db.model.base.ToysBaseEntity;
 
@Entity 
@Table(name = "EGITIMDEGERLENDIRICI") 
public class EgitimDegerlendirici extends ToysBaseEntity { 
 
	private static final long serialVersionUID = 1L; 
 
	@Id 
	@GeneratedValue(strategy = GenerationType.IDENTITY) 
	@Column(updatable = false, unique = true, nullable = false, name = "RID") 
	private Long rID; 
 
	@ManyToOne 
	@JoinColumn(name = "EGITIMTANIMREF") 
	private Egitimtanim egitimtanimRef; 
	
	@ManyToOne 
	@JoinColumn(name = "DEGERLENDIRICIREF") 
	private Degerlendirici degerlendiriciRef; 
 
	public EgitimDegerlendirici() { 
		super(); 
	} 
 
	@Override
	public Long getRID() { 
		return this.rID;  
	}  
	  
	@Override
	public void setRID(Long rID) {  
		this.rID = rID;  
	}   
 
	public Egitimtanim getEgitimtanimRef() { 
		return egitimtanimRef; 
	} 
	 
	public void setEgitimtanimRef(Egitimtanim egitimtanimRef) { 
		this.egitimtanimRef = egitimtanimRef; 
	} 
 
	public Degerlendirici getDegerlendiriciRef() {
		return degerlendiriciRef;
	}

	public void setDegerlendiriciRef(Degerlendirici degerlendiriciRef) {
		this.degerlendiriciRef = degerlendiriciRef;
	}

	@Override 
	public String toString() { 
		return "tr.com.arf.toys.db.model.egitim.EgitimDegerlendirici[id=" + this.getRID()  + "]"; 
	} 
	@Override 
	public String getUIString() { 
		if (getEgitimtanimRef()!=null){
			return getEgitimtanimRef() + ""; 
		}else{
			return null;
		}
	} 
	 
	@Override 
	public void initValues(boolean defaultValues) { 
			this.rID = null; 			this.egitimtanimRef = null; 
			this.degerlendiriciRef=null;	}  
	 
	@Override 
	public String getValue() {  
		StringBuilder value = new StringBuilder("Veri : <br />"); 
			if(getEgitimtanimRef() != null) {
				value.append(KeyUtil.getLabelValue("egitimdegerlendirici.egitimtanimRef") + " : " + getEgitimtanimRef().getUIString() + "<br />");
			} 
			if(getDegerlendiriciRef() != null) {
				value.append(KeyUtil.getLabelValue("egitimdegerlendirici.degerlendiriciRef") + " : " + getDegerlendiriciRef().getUIString() + "<br />");
			} 
			
		return value.toString(); 
	} 
	 
}	 
