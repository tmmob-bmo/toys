package tr.com.arf.toys.db.model.egitim; 
 

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import tr.com.arf.framework.db.enumerated._common.EvetHayir;
import tr.com.arf.framework.utility.tool.KeyUtil;
import tr.com.arf.toys.db.enumerated.egitim.EgitimDurum;
import tr.com.arf.toys.db.model.base.ToysBaseEntity;
import tr.com.arf.toys.db.model.system.Birim;
 
@Entity 
@Table(name = "EGITIM") 
public class Egitim extends ToysBaseEntity { 
 
	private static final long serialVersionUID = 1L; 
 
	@Id 
	@GeneratedValue(strategy = GenerationType.IDENTITY) 
	@Column(updatable = false, unique = true, nullable = false, name = "RID") 
	private Long rID; 
 
	@ManyToOne 
	@JoinColumn(name = "EGITIMTANIMREF") 
	private Egitimtanim egitimtanimRef; 
 
	@Column(name = "AD") 
	private String ad; 
 
	@ManyToOne 
	@JoinColumn(name = "BIRIMREF") 
	private Birim birimRef; 
 
	@Column(name = "YER") 
	private String yer; 
 
	@Column(name = "KONTENJAN") 
	private int kontenjan; 
 
	@Enumerated(EnumType.ORDINAL) 
	@Column(name = "DURUM") 
	private EgitimDurum durum; 
	
	@Temporal(TemporalType.TIMESTAMP) 
	@Column(name = "BASLANGICTARIH") 
	private java.util.Date baslangictarih; 
 
	@Temporal(TemporalType.TIMESTAMP) 
	@Column(name = "BITISTARIH") 
	private java.util.Date bitistarih;
	
	@Enumerated(EnumType.ORDINAL) 
	@Column(name = "YAYINDA") 
	private EvetHayir yayinda; 
	
	@Column(name = "EGITIMKODU") 
	private String egitimkodu; 
 
	public Egitim() { 
		super(); 
	} 
 
	@Override
	public Long getRID() { 
		return this.rID;  
	}  
	  
	@Override
	public void setRID(Long rID) {  
		this.rID = rID;  
	}   
 
	public Egitimtanim getEgitimtanimRef() { 
		return egitimtanimRef; 
	} 
	 
	public void setEgitimtanimRef(Egitimtanim egitimtanimRef) { 
		this.egitimtanimRef = egitimtanimRef; 
	} 
 
	public String getAd() { 
		return this.ad; 
	} 
	 
	public void setAd(String ad) { 
		this.ad = ad; 
	} 
 
	public Birim getBirimRef() { 
		return birimRef; 
	} 
	 
	public void setBirimRef(Birim birimRef) { 
		this.birimRef = birimRef; 
	} 
 
	public String getYer() { 
		return this.yer; 
	} 
	 
	public void setYer(String yer) { 
		this.yer = yer; 
	} 
 
	public int getKontenjan() { 
		return this.kontenjan; 
	} 
	 
	public void setKontenjan(int kontenjan) { 
		this.kontenjan = kontenjan; 
	} 
 
	public EgitimDurum getDurum() { 
		return this.durum;  
	}  
  
	public void setDurum(EgitimDurum durum) {  
		this.durum = durum;  
	}   
	
	public java.util.Date getBaslangictarih() { 
		return baslangictarih; 
	} 
	 
	public void setBaslangictarih(java.util.Date baslangictarih) { 
		this.baslangictarih = baslangictarih; 
	} 
 
	public java.util.Date getBitistarih() { 
		return bitistarih; 
	} 
	 
	public void setBitistarih(java.util.Date bitistarih) { 
		this.bitistarih = bitistarih; 
	} 
 
	public EvetHayir getYayinda() {
		return yayinda;
	}

	public void setYayinda(EvetHayir yayinda) {
		this.yayinda = yayinda;
	}

	public String getEgitimkodu() {
		return egitimkodu;
	}

	public void setEgitimkodu(String egitimkodu) {
		this.egitimkodu = egitimkodu;
	}

	@Override 
	public String toString() { 
		return "tr.com.arf.toys.db.model.egitim.Egitim[id=" + this.getRID()  + "]"; 
	} 
	@Override 
	public String getUIString() { 
		return getAd() + ""; 
	} 
	 
	@Override 
	public void initValues(boolean defaultValues) { 
			this.rID = null; 
			this.egitimtanimRef = null; 
			this.ad = ""; 
			this.birimRef = null; 
			this.yer = ""; 
			this.kontenjan = 0; 
			this.durum = EgitimDurum._NULL; 
			this.baslangictarih = null; 
			this.bitistarih = null;
			this.yayinda=null;
			this.egitimkodu=null;
	}  
	 
	@Override 
	public String getValue() {  
		StringBuilder value = new StringBuilder("Veri : <br />"); 
			if(getEgitimtanimRef() != null) {
				value.append(KeyUtil.getLabelValue("egitim.egitimtanimRef") + " : " + getEgitimtanimRef().getUIString() + "<br />");
			} 
			if(getAd() != null) {
				value.append(KeyUtil.getLabelValue("egitim.ad") + " : " + getAd() + "<br />");
			} 
			if(getBirimRef() != null) {
				value.append(KeyUtil.getLabelValue("egitim.birimRef") + " : " + getBirimRef().getUIString() + "<br />");
			} 
			if(getYer() != null) {
				value.append(KeyUtil.getLabelValue("egitim.yer") + " : " + getYer() + "<br />");
			} 
			if(getKontenjan() != 0) {
				value.append(KeyUtil.getLabelValue("egitim.kontenjan") + " : " + getKontenjan() + "<br />");
			} 
			if(getDurum() != null) {
				value.append(KeyUtil.getLabelValue("egitim.durum") + " : " + getDurum().getLabel() + "<br />");
			} 
			if(getBaslangictarih() != null) {
				value.append(KeyUtil.getLabelValue("egitim.baslangictarih") + " : " + getBaslangictarih() + "<br />");
			} 
			if(getBitistarih() != null) {
				value.append(KeyUtil.getLabelValue("egitim.bitistarih") + " : " + getBitistarih() + "");
			}
			if(getYayinda() != null) {
				value.append(KeyUtil.getLabelValue("egitim.yayinda") + " : " + getYayinda().getLabel() + "<br />");
			} 
			if(getEgitimkodu() != null) {
				value.append(KeyUtil.getLabelValue("egitim.egitimkodu") + " : " + getEgitimkodu() + "<br />");
			}
		return value.toString(); 
	} 
	 
}	 
