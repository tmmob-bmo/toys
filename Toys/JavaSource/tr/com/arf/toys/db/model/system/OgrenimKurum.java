package tr.com.arf.toys.db.model.system; 
 
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import tr.com.arf.framework.utility.tool.KeyUtil;
import tr.com.arf.toys.db.model.base.ToysBaseEntity;
 
 
@Entity 
@Table(name = "OGRENIMKURUM") 
public class OgrenimKurum extends ToysBaseEntity { 
 
	private static final long serialVersionUID = 1L; 
 
	@Id 
	@GeneratedValue(strategy = GenerationType.IDENTITY) 
	@Column(updatable = false, unique = true, nullable = false, name = "RID") 
	private Long rID; 
 
	@Column(name = "KISAAD") 
	private String kisaad; 
 
	@Column(name = "AD") 
	private String ad; 
 
	@Column(name = "ACIKLAMA") 
	private String aciklama; 
 
 
	public OgrenimKurum() { 
		super(); 
	} 
 
	@Override
	public Long getRID() { 
		return this.rID;  
	}  
	  
	@Override
	public void setRID(Long rID) {  
		this.rID = rID;  
	}   
 
	public String getKisaad() { 
		return this.kisaad; 
	} 
	 
	public void setKisaad(String kisaad) { 
		this.kisaad = kisaad; 
	} 
 
	public String getAd() { 
		return this.ad; 
	} 
	 
	public void setAd(String ad) { 
		this.ad = ad; 
	} 
 
	public String getAciklama() { 
		return this.aciklama; 
	} 
	 
	public void setAciklama(String aciklama) { 
		this.aciklama = aciklama; 
	} 
 
 
	@Override 
	public String toString() { 
		return "tr.com.arf.toys.db.model.system.OgrenimKurum[id=" + this.getRID()  + "]"; 
	} 
	@Override 
	public String getUIString() { 
		return getKisaad() + ""; 
	} 
	 
	@Override 
	public void initValues(boolean defaultValues) { 
		if (defaultValues) { 
				// TODO Initialize with default values 
		} else { 
			this.rID = null; 			this.kisaad = ""; 			this.ad = ""; 			this.aciklama = ""; 		}  
	}  
	 
	@Override 
	public String getValue() {  
		StringBuilder value = new StringBuilder(""); 
			if(getKisaad() != null) 
				value.append(KeyUtil.getLabelValue("ogrenimkurum.kisaad") + " : " + getKisaad() + "<br />"); 
			if(getAd() != null) 
				value.append(KeyUtil.getLabelValue("ogrenimkurum.ad") + " : " + getAd() + "<br />"); 
			if(getAciklama() != null) 
				value.append(KeyUtil.getLabelValue("ogrenimkurum.aciklama") + " : " + getAciklama() + ""); 
		return value.toString(); 
	} 
	 
}	 
