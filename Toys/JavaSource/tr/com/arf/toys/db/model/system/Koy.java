package tr.com.arf.toys.db.model.system; 
 
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tr.com.arf.framework.utility.tool.KeyUtil;
import tr.com.arf.toys.db.model.base.ToysBaseEntity;
 
 
@Entity 
@Table(name = "KOY") 
public class Koy extends ToysBaseEntity { 
 
	private static final long serialVersionUID = 1L; 
 
	@Id 
	@GeneratedValue(strategy = GenerationType.IDENTITY) 
	@Column(updatable = false, unique = true, nullable = false, name = "RID") 
	private Long rID; 
 
	@ManyToOne 
	@JoinColumn(name = "ILCEREF") 
	private Ilce ilceRef; 
 
	@Column(name = "TURU") 
	private int turu; 
 
	@Column(name = "AD") 
	private String ad; 
 
 
	public Koy() { 
		super(); 
	} 
 
	@Override
	public Long getRID() { 
		return this.rID;  
	}  
	  
	@Override
	public void setRID(Long rID) {  
		this.rID = rID;  
	}   
 
	public Ilce getIlceRef() { 
		return ilceRef; 
	} 
	 
	public void setIlceRef(Ilce ilceRef) { 
		this.ilceRef = ilceRef; 
	} 
 
	public int getTuru() { 
		return this.turu; 
	} 
	 
	public void setTuru(int turu) { 
		this.turu = turu; 
	} 
 
	public String getAd() { 
		return this.ad; 
	} 
	 
	public void setAd(String ad) { 
		this.ad = ad; 
	} 
 
 
	@Override 
	public String toString() { 
		return "tr.com.arf.toys.db.model.system.Koy[id=" + this.getRID()  + "]"; 
	} 
	@Override 
	public String getUIString() { 
		return getAd() + ""; 
	} 
	 
	@Override 
	public void initValues(boolean defaultValues) { 
		if (defaultValues) { 
				// TODO Initialize with default values 
		} else { 
			this.rID = null; 			this.ilceRef = null; 			this.turu = 0; 			this.ad = ""; 		}  
	}  
	 
	@Override 
	public String getValue() {  
		StringBuilder value = new StringBuilder(""); 
			if(getIlceRef() != null) 
				value.append(KeyUtil.getLabelValue("koy.ilceRef") + " : " + getIlceRef().getUIString() + "<br />"); 
			if(getTuru() != 0) 
				value.append(KeyUtil.getLabelValue("koy.turu") + " : " + getTuru() + "<br />"); 
			if(getAd() != null) 
				value.append(KeyUtil.getLabelValue("koy.ad") + " : " + getAd() + ""); 
		return value.toString(); 
	} 
	 
}	 
