package tr.com.arf.toys.db.model.uye;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import tr.com.arf.framework.utility.tool.KeyUtil;
import tr.com.arf.toys.db.model.base.ToysBaseEntity;
import tr.com.arf.toys.db.model.evrak.Evrak;
import tr.com.arf.toys.db.model.system.Odultip;


@Entity
@Table(name = "UYEODUL")
public class Uyeodul extends ToysBaseEntity {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(updatable = false, unique = true, nullable = false, name = "RID")
	private Long rID;

	@ManyToOne
	@JoinColumn(name = "UYEREF")
	private Uye uyeRef;

	@ManyToOne
	@JoinColumn(name = "ODULTIPREF")
	private Odultip odultipRef;

	@ManyToOne
	@JoinColumn(name = "EVRAKREF")
	private Evrak evrakRef;

	@Temporal(TemporalType.DATE)
	@Column(name = "TARIH")
	private java.util.Date tarih;

	@Column(name = "ACIKLAMA")
	private String aciklama;


	public Uyeodul() {
		super();
	}

	@Override
	public Long getRID() {
		return this.rID;
	}

	@Override
	public void setRID(Long rID) {
		this.rID = rID;
	}

	public Uye getUyeRef() {
		return uyeRef;
	}

	public void setUyeRef(Uye uyeRef) {
		this.uyeRef = uyeRef;
	}

	public Odultip getOdultipRef() {
		return odultipRef;
	}

	public void setOdultipRef(Odultip odultipRef) {
		this.odultipRef = odultipRef;
	}

	public Evrak getEvrakRef() {
		return evrakRef;
	}

	public void setEvrakRef(Evrak evrakRef) {
		this.evrakRef = evrakRef;
	}

	public java.util.Date getTarih() {
		return tarih;
	}

	public void setTarih(java.util.Date tarih) {
		this.tarih = tarih;
	}

	public String getAciklama() {
		return this.aciklama;
	}

	public void setAciklama(String aciklama) {
		this.aciklama = aciklama;
	}


	@Override
	public String toString() {
		return "tr.com.arf.toys.db.model.uye.Uyeodul[id=" + this.getRID()  + "]";
	}
	@Override
	public String getUIString() {
		return KeyUtil.getLabelValue("uyeodul.uyeRef") + " : " + getUyeRef().getUIString() + ", " + KeyUtil.getLabelValue("uyeodul.odultipRef") + " : " +  getOdultipRef().getAd();
	}

	@Override
	public void initValues(boolean defaultValues) {
		if (defaultValues) {
				// TODO Initialize with default values
		} else {
			this.rID = null;			this.uyeRef = null;			this.odultipRef = null;			this.evrakRef = null;			this.tarih = null;			this.aciklama = "";		}
	}

	@Override
	public String getValue() {
		StringBuilder value = new StringBuilder("");
			if(getUyeRef() != null) {
				value.append(KeyUtil.getLabelValue("uyeodul.uyeRef") + " : " + getUyeRef().getUIString() + "<br />");
			}
			if(getOdultipRef() != null) {
				value.append(KeyUtil.getLabelValue("uyeodul.odultipRef") + " : " + getOdultipRef().getUIString() + "<br />");
			}
			if(getEvrakRef() != null) {
				value.append(KeyUtil.getLabelValue("uyeodul.evrakRef") + " : " + getEvrakRef().getUIString() + "<br />");
			}
			if(getTarih() != null) {
				value.append(KeyUtil.getLabelValue("uyeodul.tarih") + " : " + getTarih() + "<br />");
			}
			if(getAciklama() != null) {
				value.append(KeyUtil.getLabelValue("uyeodul.aciklama") + " : " + getAciklama() + "");
			}
		return value.toString();
	}

}
