package tr.com.arf.toys.db.model.system; 
 

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import tr.com.arf.framework.utility.tool.KeyUtil;
import tr.com.arf.toys.db.model.base.ToysBaseEntity;
 
@Entity 
@Table(name = "KOMISYON") 
public class Komisyon extends ToysBaseEntity { 
 
	private static final long serialVersionUID = 1L; 
 
	@Id 
	@GeneratedValue(strategy = GenerationType.IDENTITY) 
	@Column(updatable = false, unique = true, nullable = false, name = "RID") 
	private Long rID; 
 
	@Column(name = "AD") 
	private String ad; 
 
	@Column(name = "KISAAD") 
	private String kisaad; 
 
	@Column(name = "ERISIMKODU") 
	private String erisimKodu; 
 
	
	public Komisyon() { 
		super(); 
	} 
 
	@Override
	public Long getRID() { 
		return this.rID;  
	}  
	  
	@Override
	public void setRID(Long rID) {  
		this.rID = rID;  
	}   
 
	public String getAd() {
		return ad;
	}

	public void setAd(String ad) {
		this.ad = ad;
	}

	public String getKisaad() {
		return kisaad;
	}

	public void setKisaad(String kisaad) {
		this.kisaad = kisaad;
	}

	public String getErisimKodu() {
		return erisimKodu;
	}

	public void setErisimKodu(String erisimKodu) {
		this.erisimKodu = erisimKodu;
	}

	@Override 
	public String toString() { 
		return "tr.com.arf.toys.db.model.system.Komisyon[id=" + this.getRID()  + "]"; 
	} 
	@Override 
	public String getUIString() { 
		String result = getAd() + ""; 
		return result; 
	} 
	 
	@Override 
	public void initValues(boolean defaultValues) { 
		if (defaultValues) { 
				// TODO Initialize with default values 
		} else { 
			this.rID = null; 			this.ad = ""; 			this.kisaad = ""; 			this.erisimKodu = ""; 		}  
	}  
	 
	@Override 
	public String getValue() {  
		StringBuilder value = new StringBuilder(""); 
			if(getAd() != null) {
				value.append(KeyUtil.getLabelValue("komisyon.ad") + " : " + getAd() + "<br />");
			} 
			if(getKisaad() != null) {
				value.append(KeyUtil.getLabelValue("komisyon.kisaad") + " : " + getKisaad() + "<br />");
			} 
			if(getErisimKodu() != null) {
				value.append(KeyUtil.getLabelValue("komisyon.erisimKodu") + " : " + getErisimKodu() + "");
			} 
		return value.toString(); 
	}
	 
}	 
