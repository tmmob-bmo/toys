package tr.com.arf.toys.db.model.uye;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import tr.com.arf.framework.utility.tool.KeyUtil;
import tr.com.arf.toys.db.enumerated.uye.SigortaTip;
import tr.com.arf.toys.db.enumerated.uye.UyeSigortaDurum;
import tr.com.arf.toys.db.model.base.ToysBaseEntity;
import tr.com.arf.toys.db.model.evrak.Evrak;

@Entity
@Table(name = "UYESIGORTA")
public class Uyesigorta extends ToysBaseEntity {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(updatable = false, unique = true, nullable = false, name = "RID")
	private Long rID;

	@ManyToOne
	@JoinColumn(name = "UYEREF")
	private Uye uyeRef;

	@Enumerated(EnumType.ORDINAL)
	@Column(name = "SIGORTATIP")
	private SigortaTip sigortatip;

	@Column(name = "SIGORTASIRKET")
	private String sigortasirket;

	@Column(name = "POLICENO")
	private String policeno;

	@Temporal(TemporalType.DATE)
	@Column(name = "BASLANGICTARIH")
	private java.util.Date baslangictarih;

	@Temporal(TemporalType.DATE)
	@Column(name = "BITISTARIH")
	private java.util.Date bitistarih;

	@Enumerated(EnumType.ORDINAL)
	@Column(name = "UYESIGORTADURUM")
	private UyeSigortaDurum uyesigortadurum;

	@ManyToOne
	@JoinColumn(name = "EVRAKREF")
	private Evrak evrakRef;

	@Column(name = "ACIKLAMA")
	private String aciklama;


	public Uyesigorta() {
		super();
	}

	@Override
	public Long getRID() {
		return this.rID;
	}

	@Override
	public void setRID(Long rID) {
		this.rID = rID;
	}

	public Uye getUyeRef() {
		return uyeRef;
	}

	public void setUyeRef(Uye uyeRef) {
		this.uyeRef = uyeRef;
	}

	public SigortaTip getSigortatip() {
		return this.sigortatip;
	}

	public void setSigortatip(SigortaTip sigortatip) {
		this.sigortatip = sigortatip;
	}

	public String getSigortasirket() {
		return this.sigortasirket;
	}

	public void setSigortasirket(String sigortasirket) {
		this.sigortasirket = sigortasirket;
	}

	public String getPoliceno() {
		return this.policeno;
	}

	public void setPoliceno(String policeno) {
		this.policeno = policeno;
	}

	public java.util.Date getBaslangictarih() {
		return baslangictarih;
	}

	public void setBaslangictarih(java.util.Date baslangictarih) {
		this.baslangictarih = baslangictarih;
	}

	public java.util.Date getBitistarih() {
		return bitistarih;
	}

	public void setBitistarih(java.util.Date bitistarih) {
		this.bitistarih = bitistarih;
	}

	public UyeSigortaDurum getUyesigortadurum() {
		return this.uyesigortadurum;
	}

	public void setUyesigortadurum(UyeSigortaDurum uyesigortadurum) {
		this.uyesigortadurum = uyesigortadurum;
	}

	public Evrak getEvrakRef() {
		return evrakRef;
	}

	public void setEvrakRef(Evrak evrakRef) {
		this.evrakRef = evrakRef;
	}

	public String getAciklama() {
		return this.aciklama;
	}

	public void setAciklama(String aciklama) {
		this.aciklama = aciklama;
	}


	@Override
	public String toString() {
		return "tr.com.arf.toys.db.model.uye.Uyesigorta[id=" + this.getRID()  + "]";
	}
	@Override
	public String getUIString() {
		return getSigortasirket() + "";
	}

	@Override
	public void initValues(boolean defaultValues) {
		if (defaultValues) {
				// TODO Initialize with default values
		} else {
			this.rID = null;
			this.uyeRef = null;
			this.sigortatip = SigortaTip._NULL;
			this.sigortasirket = "";
			this.policeno = "";
			this.baslangictarih = null;
			this.bitistarih = null;
			this.uyesigortadurum = UyeSigortaDurum._NULL;
			this.evrakRef = null;
			this.aciklama = "";
		}
	}

	@Override
	public String getValue() {
		StringBuilder value = new StringBuilder("");
				value.append(KeyUtil.getLabelValue("uyesigorta.uyeRef") + " : " + getUyeRef().getUIString() + "<br />");
				value.append(KeyUtil.getLabelValue("uyesigorta.sigortatip") + " : " + getSigortatip() + "<br />");
				value.append(KeyUtil.getLabelValue("uyesigorta.sigortasirket") + " : " + getSigortasirket() + "<br />");
				value.append(KeyUtil.getLabelValue("uyesigorta.policeno") + " : " + getPoliceno() + "<br />");
				value.append(KeyUtil.getLabelValue("uyesigorta.baslangictarih") + " : " + getBaslangictarih() + "<br />");
				value.append(KeyUtil.getLabelValue("uyesigorta.bitistarih") + " : " + getBitistarih() + "<br />");
				value.append(KeyUtil.getLabelValue("uyesigorta.uyesigortadurum") + " : " + getUyesigortadurum() + "<br />");
				value.append(KeyUtil.getLabelValue("uyesigorta.evrakRef") + " : " + getEvrakRef().getUIString() + "<br />");
				value.append(KeyUtil.getLabelValue("uyesigorta.aciklama") + " : " + getAciklama() + "");
		return value.toString();
	}

}
