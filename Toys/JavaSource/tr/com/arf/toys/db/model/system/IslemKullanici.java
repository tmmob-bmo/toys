package tr.com.arf.toys.db.model.system; 
 
import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tr.com.arf.framework.db.enumerated._common.EvetHayir;
import tr.com.arf.framework.utility.tool.KeyUtil;
import tr.com.arf.toys.db.model.base.ToysBaseEntity;
 
@Entity 
@Table(name = "ISLEMKULLANICI") 
public class IslemKullanici extends ToysBaseEntity { 
 
	private static final long serialVersionUID = 1L; 
 
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(updatable = false, unique = true, nullable = false, name = "RID")
	private Long rID; 
 
	@ManyToOne 
	@JoinColumn(name = "ISLEMREF") 
	private Islem islemRef; 
 
	@ManyToOne 
	@JoinColumn(name = "KULLANICIROLEREF") 
	private KullaniciRole kullaniciRoleRef; 
	
	@Enumerated(EnumType.ORDINAL)
	@Column(name = "LISTPERMISSION") 
	private EvetHayir listPermission; 
	
	@Enumerated(EnumType.ORDINAL)
	@Column(name = "UPDATEPERMISSION") 
	private EvetHayir updatePermission; 
	
	@Enumerated(EnumType.ORDINAL)
	@Column(name = "DELETEPERMISSION") 
	private EvetHayir deletePermission; 
  
	public IslemKullanici() { 
		super(); 
	} 
 
	@Override
	public Long getRID() { 
		return this.rID;  
	}  
	  
	@Override
	public void setRID(Long rID) {  
		this.rID = rID;  
	}   
 
	public Islem getIslemRef() { 
		return islemRef; 
	} 
	 
	public void setIslemRef(Islem islemRef) { 
		this.islemRef = islemRef; 
	} 
 
	public KullaniciRole getKullaniciRoleRef() { 
		return kullaniciRoleRef; 
	} 
	 
	public void setKullaniciRoleRef(KullaniciRole kullaniciRoleRef) { 
		this.kullaniciRoleRef = kullaniciRoleRef; 
	} 
 
	public EvetHayir getListPermission() {
		return listPermission;
	}

	public void setListPermission(EvetHayir listPermission) {
		this.listPermission = listPermission;
	}

	public EvetHayir getUpdatePermission() {
		return updatePermission;
	}

	public void setUpdatePermission(EvetHayir updatePermission) {
		this.updatePermission = updatePermission;
	}

	public EvetHayir getDeletePermission() {
		return deletePermission;
	}

	public void setDeletePermission(EvetHayir deletePermission) {
		this.deletePermission = deletePermission;
	}

	@Override 
	public String toString() { 
		return "tr.com.arf.toys.db.model.system.IslemKullanici[id=" + this.getRID() 
				+ "]"; 
	} 
	@Override 
	public String getUIString() { 
		return ""; 
	} 
	 
	@Override 
	public void initValues(boolean defaultValues) { 
		if (defaultValues) { 
			this.rID = null; 
			this.islemRef = null; 
			this.kullaniciRoleRef = null; 
			this.listPermission = EvetHayir._HAYIR; 
			this.updatePermission = EvetHayir._HAYIR; 
			this.deletePermission = EvetHayir._HAYIR; 
		} else { 
			this.rID = null; 			this.islemRef = null; 			this.kullaniciRoleRef = null; 
			this.listPermission = EvetHayir._NULL; 			this.updatePermission = EvetHayir._NULL; 			this.deletePermission = EvetHayir._NULL; 		}  
	}  
	
	public void initValues(EvetHayir defaultValue) {  
		this.rID = null; 
		this.kullaniciRoleRef = null;
		this.islemRef = null;  
		this.listPermission = defaultValue;
		this.updatePermission = defaultValue; 
		this.deletePermission = defaultValue;   
	}  
	
	@Override 
	public String getValue() {  
		StringBuilder value = new StringBuilder(""); 
		if(getIslemRef() != null) 
			value.append(KeyUtil.getLabelValue("islemkullanici.islemRef") + " : " + getIslemRef().getUIString() + "<br />"); 
		if(getKullaniciRoleRef() != null) 
			value.append(KeyUtil.getLabelValue("islemkullanici.kullaniciRoleRef") + " : " + getKullaniciRoleRef().getUIString() + "<br />"); 
		if(getUpdatePermission() != null) 
			value.append(KeyUtil.getLabelValue("islemkullanici.updatePermission") + " : " + getUpdatePermission() + "<br />"); 
		if(getDeletePermission() != null) 
			value.append(KeyUtil.getLabelValue("islemkullanici.deletePermission") + " : " + getDeletePermission() + "<br />"); 
		if(getListPermission() != null) 
			value.append(KeyUtil.getLabelValue("islemkullanici.listPermission") + " : " + getListPermission() + ""); 
	return value.toString(); 
	} 
	
	public void reverseEnumerated(String islem){
		if(islem.equalsIgnoreCase("listPermission")){
			if(getListPermission() == EvetHayir._EVET)
				setListPermission(EvetHayir._HAYIR);
			else
				setListPermission(EvetHayir._EVET);
		}  else if(islem.equalsIgnoreCase("updatePermission")){
			if(getUpdatePermission() == EvetHayir._EVET)
				setUpdatePermission(EvetHayir._HAYIR);
			else
				setUpdatePermission(EvetHayir._EVET);
		} else if(islem.equalsIgnoreCase("deletePermission")){
			if(getDeletePermission() == EvetHayir._EVET)
				setDeletePermission(EvetHayir._HAYIR);
			else
				setDeletePermission(EvetHayir._EVET);
		} 
	}
	 
	 
}	 
