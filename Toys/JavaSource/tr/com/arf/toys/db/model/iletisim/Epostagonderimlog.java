package tr.com.arf.toys.db.model.iletisim;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import tr.com.arf.framework.utility.tool.DateUtil;
import tr.com.arf.framework.utility.tool.KeyUtil;
import tr.com.arf.toys.db.enumerated.iletisim.GonderimDurum;
import tr.com.arf.toys.db.model.base.ToysBaseEntity;
import tr.com.arf.toys.db.model.kisi.Kisi;
import tr.com.arf.toys.db.model.system.Birim;

@Entity
@Table(name = "EPOSTAGONDERIMLOG")
public class Epostagonderimlog extends ToysBaseEntity {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(updatable = false, unique = true, nullable = false, name = "RID")
	private Long rID;

	@ManyToOne
	@JoinColumn(name = "GONDERIMREF")
	private Gonderim gonderimRef;

	@ManyToOne
	@JoinColumn(name = "KISIREF")
	private Kisi kisiRef;

	@Temporal(TemporalType.DATE)
	@Column(name = "TARIH")
	private java.util.Date tarih;

	@ManyToOne
	@JoinColumn(name = "BIRIMREF")
	private Birim birimRef;

	@Enumerated(EnumType.ORDINAL)
	@Column(name = "DURUM")
	private GonderimDurum durum;

	@ManyToOne
	@JoinColumn(name = "GONDERENREF")
	private Kisi gonderenRef;

	@Column(name = "ACIKLAMA")
	private String aciklama;


	public Epostagonderimlog() {
		super();
	}

	public Epostagonderimlog(Gonderim gonderimRef, Kisi kisiRef, java.util.Date tarih, Birim birimRef, GonderimDurum durum, String aciklama, Kisi gonderen) {
		this.gonderimRef = gonderimRef;
		this.kisiRef = kisiRef;
		this.tarih = new Date();
		this.birimRef = birimRef;
		this.durum = durum;
		this.aciklama = aciklama;
		this.gonderenRef = gonderen;
	}

	@Override
	public Long getRID() {
		return this.rID;
	}

	@Override
	public void setRID(Long rID) {
		this.rID = rID;
	}

	public Gonderim getGonderimRef() {
		return gonderimRef;
	}

	public void setGonderimRef(Gonderim gonderimRef) {
		this.gonderimRef = gonderimRef;
	}

	public Kisi getKisiRef() {
		return kisiRef;
	}

	public void setKisiRef(Kisi kisiRef) {
		this.kisiRef = kisiRef;
	}

	public java.util.Date getTarih() {
		return tarih;
	}

	public void setTarih(java.util.Date tarih) {
		this.tarih = tarih;
	}

	public GonderimDurum getDurum() {
		return this.durum;
	}

	public void setDurum(GonderimDurum durum) {
		this.durum = durum;
	}

	public Kisi getGonderenRef() {
		return gonderenRef;
	}

	public void setGonderenRef(Kisi gonderenRef) {
		this.gonderenRef = gonderenRef;
	}

	public String getAciklama() {
		return this.aciklama;
	}

	public void setAciklama(String aciklama) {
		this.aciklama = aciklama;
	}

	public Birim getBirimRef() {
		return birimRef;
	}

	public void setBirimRef(Birim birimRef) {
		this.birimRef = birimRef;
	}

	@Override
	public String toString() {
		return "tr.com.arf.toys.db.model.iletisim.Epostagonderimlog[id=" + this.getRID()  + "]";
	}
	@Override
	public String getUIString() {
		return getAciklama() + "";
	}

	@Override
	public void initValues(boolean defaultValues) {
			this.rID = null;			this.gonderimRef = null;			this.kisiRef = null;			this.tarih = null;			this.durum = GonderimDurum._NULL;			this.aciklama = "";
			this.birimRef=null;
			this.gonderenRef = null;	}

	@Override
	public String getValue() {
		StringBuilder value = new StringBuilder("Veri : <br />");
			if(getGonderimRef() != null) {
				value.append(KeyUtil.getLabelValue("epostagonderimlog.gonderimRef") + " : " + getGonderimRef().getUIString() + "<br />");
			}
			if(getKisiRef() != null) {
				value.append(KeyUtil.getLabelValue("epostagonderimlog.kisiRef") + " : " + getKisiRef().getUIString() + "<br />");
			}
			if(getTarih() != null) {
				value.append(KeyUtil.getLabelValue("epostagonderimlog.tarih") + " : " + DateUtil.dateToDMYHMS(getTarih()) + "<br />");
			}
			if(getDurum() != null) {
				value.append(KeyUtil.getLabelValue("epostagonderimlog.durum") + " : " + getDurum().getLabel() + "<br />");
			}
			if(getGonderenRef() != null) {
				value.append(KeyUtil.getLabelValue("epostagonderimlog.gonderenRef") + " : " + getGonderenRef().getUIString() + "<br />");
			}
			if(getAciklama() != null) {
				value.append(KeyUtil.getLabelValue("epostagonderimlog.aciklama") + " : " + getAciklama() + "");
			}
		return value.toString();
	}

}
