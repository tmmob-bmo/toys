package tr.com.arf.toys.db.model.system; 
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tr.com.arf.framework.db.enumerated._common.EvetHayir;
import tr.com.arf.framework.utility.tool.KeyUtil;
import tr.com.arf.toys.db.model.base.ToysBaseEntity;
 
 
@Entity 
@Table(name = "BIRIMHESAP") 
public class BirimHesap extends ToysBaseEntity { 
 
	private static final long serialVersionUID = 1L; 
	 
	@Id 
	@GeneratedValue(strategy = GenerationType.IDENTITY) 
	@Column(updatable = false, unique = true, nullable = false, name = "RID") 
	private Long rID; 
 
	@Column(name = "BANKAADI") 
	private String bankaadi; 
	
	@Column(name = "HESAPNO") 
	private Long hesapno; 
	
	@Column(name = "SUBEKODU") 
	private Long subekodu; 
	
	@Column(name = "IBANNUMARASI") 
	private Long ibannumarasi; 
	
	@ManyToOne 
	@JoinColumn(name = "BIRIMREF") 
	private Birim birimRef;
	
	@Enumerated(EnumType.ORDINAL)
	@Column(name = "GECERLI") 
	private EvetHayir gecerli; 
 
	public BirimHesap() { 
		super(); 
	} 
 
	@Override
	public Long getRID() { 
		return this.rID;  
	}  
	  
	@Override
	public void setRID(Long rID) {  
		this.rID = rID;  
	}   
 
	public String getBankaadi() { 
		return this.bankaadi; 
	} 
	 
	public void setBankaadi(String bankaadi) { 
		this.bankaadi = bankaadi; 
	} 

	public Long getHesapno() {
		return hesapno;
	}

	public void setHesapno(Long hesapno) {
		this.hesapno = hesapno;
	}

	public Long getSubekodu() {
		return subekodu;
	}

	public void setSubekodu(Long subekodu) {
		this.subekodu = subekodu;
	}

	public Long getIbannumarasi() {
		return ibannumarasi;
	}

	public void setIbannumarasi(Long ibannumarasi) {
		this.ibannumarasi = ibannumarasi;
	}

	public Birim getBirimRef() {
		return birimRef;
	}

	public void setBirimRef(Birim birimRef) {
		this.birimRef = birimRef;
	}
	
	public EvetHayir getGecerli() {
		return gecerli;
	}

	public void setGecerli(EvetHayir gecerli) {
		this.gecerli = gecerli;
	}

	@Override 
	public String toString() { 
		return "tr.com.arf.toys.db.model.system.BirimHesap[id=" + this.getRID()  + "]"; 
	} 
	@Override 
	public String getUIString() { 
		return getBankaadi() + ""; 
	} 
	 
	@Override 
	public void initValues(boolean defaultValues) { 
		if (defaultValues) { 
				// TODO Initialize with default values 
		} else { 
			this.rID = null; 
			this.bankaadi="";
			this.hesapno=null;
			this.ibannumarasi=null;
			this.subekodu=null;
			this.birimRef=null;
			this.gecerli = EvetHayir._NULL;
		}  
	}  
	 
	@Override 
	public String getValue() {  
		StringBuilder value = new StringBuilder(""); 
			if(getBankaadi() != null) 
				value.append(KeyUtil.getLabelValue("birimhesap.bankaadi") + " : " + getBankaadi() + "<br />"); 
			if(getHesapno() != null) 
				value.append(KeyUtil.getLabelValue("birimhesap.hesapno") + " : " + getHesapno() + "<br />"); 
			if(getIbannumarasi()!= null) 
				value.append(KeyUtil.getLabelValue("birimhesap.ibannumarasi") + " : " + getIbannumarasi() + "<br />"); 
			if(getSubekodu()!= null) 
				value.append(KeyUtil.getLabelValue("birimhesap.subekodu") + " : " + getSubekodu() + "<br />");
			if(getBirimRef() != null) 
				value.append(KeyUtil.getLabelValue("birimhesap.birimRef") + " : " + getBirimRef().getUIString() + "<br />"); 
			if(getGecerli() != null) 
				value.append(KeyUtil.getLabelValue("birimhesap.gecerli") + " : " + getGecerli() + ""); 

		return value.toString(); 
	} 
	 

}	 
