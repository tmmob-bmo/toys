package tr.com.arf.toys.db.model.iletisim; 
 
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import tr.com.arf.framework.utility.tool.KeyUtil;
import tr.com.arf.toys.db.enumerated.iletisim.GonderimTuru;
import tr.com.arf.toys.db.enumerated.iletisim.IcerikTuru;
import tr.com.arf.toys.db.model.base.ToysBaseEntity;
 
@Entity 
@Table(name = "ICERIK") 
public class Icerik extends ToysBaseEntity { 
 
	private static final long serialVersionUID = 1L; 
 
	@Id 
	@GeneratedValue(strategy = GenerationType.IDENTITY) 
	@Column(updatable = false, unique = true, nullable = false, name = "RID") 
	private Long rID; 
 
	@Column(name = "ICERIK") 
	private String icerik; 
 
	@Enumerated(EnumType.ORDINAL) 
	@Column(name = "GONDERIMTURU") 
	private GonderimTuru gonderimturu; 
 
	@Enumerated(EnumType.ORDINAL) 
	@Column(name = "ICERIKTURU") 
	private IcerikTuru icerikturu; 
 
 
	public Icerik() { 
		super(); 
	} 
 
	@Override
	public Long getRID() { 
		return this.rID;  
	}  
	  
	@Override
	public void setRID(Long rID) {  
		this.rID = rID;  
	}   
 
	public String getIcerik() { 
		return this.icerik; 
	} 
	 
	public void setIcerik(String icerik) { 
		this.icerik = icerik; 
	} 
 
	public GonderimTuru getGonderimturu() { 
		return this.gonderimturu;  
	}  
  
	public void setGonderimturu(GonderimTuru gonderimturu) {  
		this.gonderimturu = gonderimturu;  
	}   
 
	public IcerikTuru getIcerikturu() { 
		return this.icerikturu;  
	}  
  
	public void setIcerikturu(IcerikTuru icerikturu) {  
		this.icerikturu = icerikturu;  
	}   
 
 
	@Override 
	public String toString() { 
		return "tr.com.arf.toys.db.model.iletisim.Icerik[id=" + this.getRID()  + "]"; 
	} 
	@Override 
	public String getUIString() { 
		if (this.icerik!=null){
			return this.icerik.toString() + ""; 
		}
		return "";
	} 
	 
	@Override 
	public void initValues(boolean defaultValues) { 
			this.rID = null; 			this.icerik = ""; 			this.gonderimturu = GonderimTuru._NULL; 			this.icerikturu = IcerikTuru._NULL; 	}  
	 
	@Override 
	public String getValue() {  
		StringBuilder value = new StringBuilder("Veri : <br />"); 
			if(getIcerik() != null) {
				value.append(KeyUtil.getLabelValue("icerik.icerik") + " : " + getIcerik() + "<br />");
			} 
			if(getGonderimturu() != null) {
				value.append(KeyUtil.getLabelValue("icerik.gonderimturu") + " : " + getGonderimturu().getLabel() + "<br />");
			} 
			if(getIcerikturu() != null) {
				value.append(KeyUtil.getLabelValue("icerik.icerikturu") + " : " + getIcerikturu().getLabel() + "");
			} 
		return value.toString(); 
	} 
	 
}	 
