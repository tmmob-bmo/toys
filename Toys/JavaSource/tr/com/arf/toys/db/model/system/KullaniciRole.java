package tr.com.arf.toys.db.model.system; 
 
import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tr.com.arf.framework.db.enumerated._common.EvetHayir;
import tr.com.arf.framework.utility.tool.KeyUtil;
import tr.com.arf.toys.db.model.base.ToysBaseEntity;
 
@Entity 
@Table(name = "KULLANICIROLE") 
public class KullaniciRole extends ToysBaseEntity { 
 
	private static final long serialVersionUID = 1L; 
 
	@Id  
	@GeneratedValue(strategy = IDENTITY)
	@Column(updatable = false, unique = true, nullable = false, name = "RID")
	private Long rID; 
 
	@ManyToOne 
	@JoinColumn(name = "KULLANICIREF") 
	private Kullanici kullaniciRef; 
 
	@ManyToOne 
	@JoinColumn(name = "ROLEREF") 
	private Role roleRef; 
 
	@Enumerated(EnumType.ORDINAL)
	@Column(name = "ACTIVE") 
	private EvetHayir active; 
 
 
	public KullaniciRole() { 
		super(); 
	} 
 
	@Override
	public Long getRID() { 
		return this.rID;  
	}  
	  
	@Override
	public void setRID(Long rID) {  
		this.rID = rID;  
	}   
 
	public Kullanici getKullaniciRef() { 
		return kullaniciRef; 
	} 
	 
	public void setKullaniciRef(Kullanici kullaniciRef) { 
		this.kullaniciRef = kullaniciRef; 
	} 
 
	public Role getRoleRef() { 
		return roleRef; 
	} 
	 
	public void setRoleRef(Role roleRef) { 
		this.roleRef = roleRef; 
	} 
 
	public EvetHayir getActive() {
		return active;
	}

	public void setActive(EvetHayir active) {
		this.active = active;
	}

	@Override 
	public String toString() { 
		return "tr.com.arf.toys.db.model.system.KullaniciRole[id=" + this.getRID() 
				+ "]"; 
	} 
	@Override 
	public String getUIString() { 
		return getRoleRef().getName(); 
	} 
	 
	@Override 
	public void initValues(boolean defaultValues) { 
		if (defaultValues) { 
				// TODO Initialize with default values 
		} else { 
			this.rID = null; 			this.kullaniciRef = null; 			this.roleRef = null; 			this.active = EvetHayir._NULL;		}  
	}  
	 
	@Override 
	public String getValue() {  
		StringBuilder value = new StringBuilder(""); 
		if(getKullaniciRef() != null) {
			value.append(KeyUtil.getLabelValue("kullanicirole.kullaniciRef") + " : " + getKullaniciRef() + "<br />");
		} 
		if(getRoleRef() != null) {
			value.append(KeyUtil.getLabelValue("kullanicirole.roleRef") + " : " + getRoleRef().getUIString() + "<br />");
		} 
		if(getActive() != null){
			value.append(KeyUtil.getLabelValue("kullanicirole.active") + " : " + getActive() + "");
		}
		return value.toString(); 
	} 
	 
	 
}	 
