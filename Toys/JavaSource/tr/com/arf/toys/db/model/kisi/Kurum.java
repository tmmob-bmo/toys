package tr.com.arf.toys.db.model.kisi; 
 
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import tr.com.arf.framework.db.enumerated._common.EvetHayir;
import tr.com.arf.framework.utility.tool.KeyUtil;
import tr.com.arf.toys.db.enumerated.iletisim.TelefonTuru;
import tr.com.arf.toys.db.model.base.ToysBaseEntity;
import tr.com.arf.toys.db.model.iletisim.Adres;
import tr.com.arf.toys.db.model.iletisim.Internetadres;
import tr.com.arf.toys.db.model.iletisim.Telefon;
import tr.com.arf.toys.db.model.system.Ilce;
import tr.com.arf.toys.db.model.system.KurumTuru;
import tr.com.arf.toys.db.model.system.Sehir;
 
@Entity 
@Table(name = "KURUM") 
public class Kurum extends ToysBaseEntity { 
 
	private static final long serialVersionUID = 1L; 
 
	@Id 
	@GeneratedValue(strategy = GenerationType.IDENTITY) 
	@Column(updatable = false, unique = true, nullable = false, name = "RID") 
	private Long rID; 
 
	@Column(name = "AD") 
	private String ad; 
 
	@Column(name = "VERGIDAIRESI") 
	private String vergidairesi; 
 
	@Column(name = "VERGINO") 
	private String vergino; 
 
	@ManyToOne 
	@JoinColumn(name = "SORUMLUREF") 
	private Kisi sorumluRef; 
	
	@Transient
	private Kisi sorumluRefPrev;
	
	@ManyToOne 
	@JoinColumn(name = "KURUMTURUREF") 
	private KurumTuru kurumTuruRef; 
 
	@Column(name = "ACIKLAMA") 
	private String aciklama; 
	
	@ManyToOne 
	@JoinColumn(name = "SEHIRREF") 
	private Sehir sehirRef;

	@ManyToOne 
	@JoinColumn(name = "ILCEREF") 
	private Ilce ilceRef; 
 
//	private Internetadres internetadres; 
	
	public Kurum() { 
		super(); 
	} 
 
	@Override
	public Long getRID() { 
		return this.rID;  
	}  
	  
	@Override
	public void setRID(Long rID) {  
		this.rID = rID;  
	}   
 
	public String getAd() { 
		return this.ad; 
	} 
	 
	public void setAd(String ad) { 
		this.ad = ad; 
	} 
  	
	public String getVergidairesi() { 
		return this.vergidairesi; 
	} 
	 
	public KurumTuru getKurumTuruRef() {
		return kurumTuruRef;
	}

	public void setKurumTuruRef(KurumTuru kurumTuruRef) {
		this.kurumTuruRef = kurumTuruRef;
	}

	public void setVergidairesi(String vergidairesi) { 
		this.vergidairesi = vergidairesi; 
	} 
 
	public String getVergino() { 
		return this.vergino; 
	} 
	 
	public void setVergino(String vergino) { 
		this.vergino = vergino; 
	} 
 
	public Kisi getSorumluRef() { 
		return sorumluRef; 
	} 
	 
	public void setSorumluRef(Kisi sorumluRef) { 
		this.sorumluRef = sorumluRef; 
	}  
 
	public Kisi getSorumluRefPrev() {
		return sorumluRefPrev;
	}

	public void setSorumluRefPrev(Kisi sorumluRefPrev) {
		this.sorumluRefPrev = sorumluRefPrev;
	}

	public String getAciklama() { 
		return this.aciklama; 
	} 
	 
	public void setAciklama(String aciklama) { 
		this.aciklama = aciklama; 
	}  
	
	public Sehir getSehirRef() {
		return sehirRef;
	}

	public void setSehirRef(Sehir sehirRef) {
		this.sehirRef = sehirRef;
	}

	public Ilce getIlceRef() {
		return ilceRef;
	}

	public void setIlceRef(Ilce ilceRef) {
		this.ilceRef = ilceRef;
	}

//	public Internetadres getInternetadres() {
//		return internetadres;
//	}
//
//	public void setInternetadres(Internetadres internetadres) {
//		this.internetadres = internetadres;
//	}
	
	public String getAcikAdres(){
		if (getKurumAdres()!=null){
			if (getKurumAdres().getAcikAdres()!=null){
				return "\n"+getKurumAdres().getAcikAdres();
			}
		} else {
			return "\n Tanımlı Adres Bulunmamaktadır!";
		}
		return "\n Tanımlı Adres Bulunmamaktadır!";
	}

	@Transient
	public Adres getKurumAdres(){
		if(getRID() == null) {
			return null;
		} else {
			try {
				if(getDBOperator().recordCount(Adres.class.getSimpleName(), "o.kurumRef.rID=" + getRID() + " AND o.varsayilan=" + EvetHayir._EVET.getCode()) > 0){
					Adres adres = (Adres) getDBOperator().load(Adres.class.getSimpleName(), "o.kurumRef.rID="  + getRID() + " AND o.varsayilan=" + EvetHayir._EVET.getCode(), "o.varsayilan ASC").get(0);
					return adres;
				}
			} catch (Exception e) { 
				e.printStackTrace();
				return null;
			}			
		}
		return null;
	}
	
	@Transient
	public Internetadres getKurumInternetadres(){
		if(getRID() == null) {
			return null;
		} else {
			try {
				if(getDBOperator().recordCount(Internetadres.class.getSimpleName(), "o.kurumRef.rID=" + getRID()) > 0){
					Internetadres internetAdres = (Internetadres) getDBOperator().load(Internetadres.class.getSimpleName(), "o.kurumRef.rID=" + getRID() + " AND o.varsayilan=" + EvetHayir._EVET.getCode(), "o.varsayilan ASC").get(0);
					return internetAdres;
				}
			} catch (Exception e) { 
				e.printStackTrace();
				return null;
			}			
		}
		return null;
	}
	
	@Transient
	public Telefon getKurumTelefon(){
		if(getRID() == null) {
			return null;
		} else {
			try {
				if(getDBOperator().recordCount(Telefon.class.getSimpleName(), "o.kurumRef.rID=" + getRID()) > 0){
					Telefon telefon = (Telefon) getDBOperator().load(Telefon.class.getSimpleName(), "o.kurumRef.rID=" + getRID(), "o.varsayilan ASC").get(0);
					return telefon;
				}
			} catch (Exception e) { 
				e.printStackTrace();
				return null;
			}			
		}
		return null;
	}
	
	@Transient
	public Telefon getKurumFaks(){
		if(getRID() == null) {
			return null;
		} else {
			try {
				if(getDBOperator().recordCount(Telefon.class.getSimpleName(), "o.kurumRef.rID=" + getRID() + "AND o.telefonturu=" + TelefonTuru._FAKS.getCode()) > 0){
					Telefon telefon = (Telefon) getDBOperator().load(Telefon.class.getSimpleName(), "o.kurumRef.rID=" + getRID()  , "o.varsayilan ASC").get(0);
					return telefon;
				}
			} catch (Exception e) { 
				e.printStackTrace();
				return null;
			}			
		}
		return null;
	}
 
	@Override 
	public String toString() { 
		return getUIString(); 
	} 
	
	@Override 
	public String getUIString() { 
		if(getRID() != null){
			if(getKurumAdres() == null) {
				return getAd() + "";
			} else {
                String kurumAdresi = getKurumAdres().getAcikAdres();
                kurumAdresi = kurumAdresi.replaceAll("\\n", "");
                kurumAdresi = kurumAdresi.replaceAll("\\r", "");
				return getAd() + " (" + kurumAdresi + ")";
			}
		} else {
			return "";
		}
	} 
	
	public String getUIStringShort(){
		return getAd();
	}
	 
	@Override 
	public void initValues(boolean defaultValues) { 
		if (defaultValues) { 
				// TODO Initialize with default values 
		} else { 
			this.rID = null; 
			this.ad = ""; 
			this.vergidairesi = ""; 
			this.vergino = ""; 
			this.sorumluRef = null; 
			this.aciklama = ""; 
			this.sehirRef=null;
			this.ilceRef=null;
			this.sorumluRefPrev = null;
		}  
	}  
	 
	@Override 
	public String getValue() {  
		StringBuilder value = new StringBuilder(""); 
			if(getAd() != null) {
				value.append(KeyUtil.getLabelValue("kurum.ad") + " : " + getAd() + "<br />");
			} 
			if(getKurumTuruRef() != null) {
				value.append(KeyUtil.getLabelValue("kurum.kurumTuruRef") + " : " + getKurumTuruRef().getUIString() + "<br />");
			}
			if(getVergidairesi() != null) {
				value.append(KeyUtil.getLabelValue("kurum.vergidairesi") + " : " + getVergidairesi() + "<br />");
			} 
			if(getVergino() != null) {
				value.append(KeyUtil.getLabelValue("kurum.vergino") + " : " + getVergino() + "<br />");
			} 
			if(getSorumluRef() != null) {
				value.append(KeyUtil.getLabelValue("kurum.sorumluRef") + " : " + getSorumluRef().getUIString() + "<br />");
			} 
			if(getAciklama() != null) {
				value.append(KeyUtil.getLabelValue("kurum.aciklama") + " : " + getAciklama() + "");
			} 
			if(getSehirRef() != null) {
				value.append(KeyUtil.getLabelValue("kurum.sehirRef") + " : " + getSehirRef().getUIString() + "<br />");
			} 
			if(getSorumluRef() != null) {
				value.append(KeyUtil.getLabelValue("kurum.ilceRef") + " : " + getSorumluRef().getUIString() + "<br />");
			} 
		return value.toString(); 
	} 
	 
}	 
