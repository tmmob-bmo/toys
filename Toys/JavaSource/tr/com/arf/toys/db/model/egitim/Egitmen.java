package tr.com.arf.toys.db.model.egitim; 
 
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tr.com.arf.framework.utility.tool.KeyUtil;
import tr.com.arf.toys.db.enumerated.egitim.EtkinDurum;
import tr.com.arf.toys.db.model.base.ToysBaseEntity;
import tr.com.arf.toys.db.model.kisi.Kisi;
import tr.com.arf.toys.db.model.system.Birim;
 
@Entity 
@Table(name = "EGITMEN") 
public class Egitmen extends ToysBaseEntity { 
 
	private static final long serialVersionUID = 1L; 
 
	@Id 
	@GeneratedValue(strategy = GenerationType.IDENTITY) 
	@Column(updatable = false, unique = true, nullable = false, name = "RID") 
	private Long rID; 
 
	@ManyToOne 
	@JoinColumn(name = "BIRIMREF") 
	private Birim birimRef; 
 
	@ManyToOne 
	@JoinColumn(name = "KISIREF") 
	private Kisi kisiRef; 
	
	@Column(name = "ACIKLAMA") 
	private String aciklama; 
	
	@Enumerated(EnumType.ORDINAL) 
	@Column(name = "DURUM") 
	private EtkinDurum durum; 
 
 
	public Egitmen() { 
		super(); 
	} 
 
	public Long getRID() { 
		return this.rID;  
	}  
	  
	public void setRID(Long rID) {  
		this.rID = rID;  
	}   
 
	public Birim getBirimRef() {
		return birimRef;
	}

	public void setBirimRef(Birim birimRef) {
		this.birimRef = birimRef;
	}

	public String getAciklama() {
		return aciklama;
	}

	public void setAciklama(String aciklama) {
		this.aciklama = aciklama;
	}

	public Kisi getKisiRef() {
		return kisiRef;
	}

	public void setKisiRef(Kisi kisiRef) {
		this.kisiRef = kisiRef;
	}

	public EtkinDurum getDurum() {
		return durum;
	}

	public void setDurum(EtkinDurum durum) {
		this.durum = durum;
	}

	@Override 
	public String toString() { 
		return "tr.com.arf.toys.db.model.egitim.Egitmen[id=" + this.getRID()  + "]"; 
	} 
	@Override 
	public String getUIString() { 
		return getKisiRef().getUIStringShort() + ""; 
	} 
	 
	@Override 
	public void initValues(boolean defaultValues) { 
			this.rID = null; 
			this.birimRef=null;
			this.kisiRef=null;
			this.aciklama=null;
			this.durum=EtkinDurum._NULL;
	}  
	 
	@Override 
	public String getValue() {  
		StringBuilder value = new StringBuilder("Veri : <br />"); 
			if(getBirimRef()!= null) 
				value.append(KeyUtil.getLabelValue("egitmen.birimRef") + " : " + getBirimRef().getUIString() + "<br />"); 
			if(getKisiRef() != null) 
				value.append(KeyUtil.getLabelValue("egitmen.kisiRef") + " : " + getKisiRef().getUIString() + "<br />"); 
			if(getBirimRef()!= null) 
				value.append(KeyUtil.getLabelValue("egitmen.birimRef") + " : " + getBirimRef().getUIString() + "<br />"); 
			if(getAciklama() != null) 
				value.append(KeyUtil.getLabelValue("egitmen.aciklama") + " : " + getAciklama() + "<br />"); 
			if(getDurum()!= null) 
				value.append(KeyUtil.getLabelValue("egitmen.durum") + " : " + getDurum().getLabel() + "<br />"); 
		return value.toString(); 
	} 
	 
}	 
