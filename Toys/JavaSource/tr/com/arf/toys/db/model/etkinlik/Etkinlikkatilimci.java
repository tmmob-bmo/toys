package tr.com.arf.toys.db.model.etkinlik; 
 
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tr.com.arf.framework.utility.tool.KeyUtil;
import tr.com.arf.toys.db.enumerated.etkinlik.KatilimciDurum;
import tr.com.arf.toys.db.model.base.ToysBaseEntity;
import tr.com.arf.toys.db.model.kisi.Kisi;
 
@Entity 
@Table(name = "ETKINLIKKATILIMCI") 
public class Etkinlikkatilimci extends ToysBaseEntity { 
 
	private static final long serialVersionUID = 1L; 
 
	@Id 
	@GeneratedValue(strategy = GenerationType.IDENTITY) 
	@Column(updatable = false, unique = true, nullable = false, name = "RID") 
	private Long rID; 
 
	@ManyToOne 
	@JoinColumn(name = "ETKINLIKREF") 
	private Etkinlik etkinlikRef; 
 
	@ManyToOne 
	@JoinColumn(name = "KISIREF") 
	private Kisi kisiRef; 
 
	@Enumerated(EnumType.ORDINAL) 
	@Column(name = "DURUM") 
	private KatilimciDurum durum; 
 
 
	public Etkinlikkatilimci() { 
		super(); 
	} 
 
	public Long getRID() { 
		return this.rID;  
	}  
	  
	public void setRID(Long rID) {  
		this.rID = rID;  
	}   
 
	public Etkinlik getEtkinlikRef() { 
		return etkinlikRef; 
	} 
	 
	public void setEtkinlikRef(Etkinlik etkinlikRef) { 
		this.etkinlikRef = etkinlikRef; 
	} 
 
	public Kisi getKisiRef() { 
		return kisiRef; 
	} 
	 
	public void setKisiRef(Kisi kisiRef) { 
		this.kisiRef = kisiRef; 
	} 
 
	public KatilimciDurum getDurum() { 
		return this.durum;  
	}  
  
	public void setDurum(KatilimciDurum durum) {  
		this.durum = durum;  
	}   
 
 
	@Override 
	public String toString() { 
		return "tr.com.arf.toys.db.model.etkinlik.Etkinlikkatilimci[id=" + this.getRID()  + "]"; 
	} 
	@Override 
	public String getUIString() { 
		return getEtkinlikRef() + ""; 
	} 
	 
	@Override 
	public void initValues(boolean defaultValues) { 
			this.rID = null; 			this.etkinlikRef = null; 			this.kisiRef = null; 			this.durum = KatilimciDurum._NULL; 	}  
	 
	@Override 
	public String getValue() {  
		StringBuilder value = new StringBuilder("Veri : <br />"); 
			if(getEtkinlikRef() != null) 
				value.append(KeyUtil.getLabelValue("etkinlikkatilimci.etkinlikRef") + " : " + getEtkinlikRef().getUIString() + "<br />"); 
			if(getKisiRef() != null) 
				value.append(KeyUtil.getLabelValue("etkinlikkatilimci.kisiRef") + " : " + getKisiRef().getUIString() + "<br />"); 
			if(getDurum() != null) 
				value.append(KeyUtil.getLabelValue("etkinlikkatilimci.durum") + " : " + getDurum().getLabel() + ""); 
		return value.toString(); 
	} 
	 
}	 
