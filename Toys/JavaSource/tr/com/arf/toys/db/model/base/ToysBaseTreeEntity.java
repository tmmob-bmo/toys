package tr.com.arf.toys.db.model.base;

import tr.com.arf.framework.db._management.DBOperator;
import tr.com.arf.framework.db.model._base.BaseTreeEntity;
import tr.com.arf.toys.service.application.ManagedBeanLocator;

public abstract class ToysBaseTreeEntity extends BaseTreeEntity {
	   
	private static final long serialVersionUID = -9172396875671786107L;

	public DBOperator getDBOperator(){
		return ManagedBeanLocator.locateSessionController().getDBOperator();
	}
	
	public String getUIStringShort(){
		return getUIString();
	}
}
