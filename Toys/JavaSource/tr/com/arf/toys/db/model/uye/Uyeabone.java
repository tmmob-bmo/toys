package tr.com.arf.toys.db.model.uye;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import tr.com.arf.framework.utility.tool.KeyUtil;
import tr.com.arf.toys.db.enumerated.uye.UyeAboneDurum;
import tr.com.arf.toys.db.model.base.ToysBaseEntity;
import tr.com.arf.toys.db.model.system.Yayin;

@Entity
@Table(name = "UYEABONE")
public class Uyeabone extends ToysBaseEntity {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(updatable = false, unique = true, nullable = false, name = "RID")
	private Long rID;

	@ManyToOne
	@JoinColumn(name = "UYEREF")
	private Uye uyeRef;

	@ManyToOne
	@JoinColumn(name = "YAYINREF")
	private Yayin yayinRef;

	@Temporal(TemporalType.DATE)
	@Column(name = "TARIH")
	private java.util.Date tarih;

	@Temporal(TemporalType.DATE)
	@Column(name = "BASLANGICTARIH")
	private java.util.Date baslangictarih;

	@Temporal(TemporalType.DATE)
	@Column(name = "BITISTARIH")
	private java.util.Date bitistarih;

	@Enumerated(EnumType.ORDINAL)
	@Column(name = "UYEABONEDURUM")
	private UyeAboneDurum uyeabonedurum;

	@Column(name = "ACIKLAMA")
	private String aciklama;

	public Uyeabone() {
		super();
	}

	@Override
	public Long getRID() {
		return this.rID;
	}

	@Override
	public void setRID(Long rID) {
		this.rID = rID;
	}

	public Uye getUyeRef() {
		return uyeRef;
	}

	public void setUyeRef(Uye uyeRef) {
		this.uyeRef = uyeRef;
	}

	public Yayin getYayinRef() {
		return yayinRef;
	}

	public void setYayinRef(Yayin yayinRef) {
		this.yayinRef = yayinRef;
	}

	public java.util.Date getTarih() {
		return tarih;
	}

	public void setTarih(java.util.Date tarih) {
		this.tarih = tarih;
	}

	public java.util.Date getBaslangictarih() {
		return baslangictarih;
	}

	public void setBaslangictarih(java.util.Date baslangictarih) {
		this.baslangictarih = baslangictarih;
	}

	public java.util.Date getBitistarih() {
		return bitistarih;
	}

	public void setBitistarih(java.util.Date bitistarih) {
		this.bitistarih = bitistarih;
	}

	public UyeAboneDurum getUyeabonedurum() {
		return this.uyeabonedurum;
	}

	public void setUyeabonedurum(UyeAboneDurum uyeabonedurum) {
		this.uyeabonedurum = uyeabonedurum;
	}

	public String getAciklama() {
		return this.aciklama;
	}

	public void setAciklama(String aciklama) {
		this.aciklama = aciklama;
	}

	@Override
	public String toString() {
		return "tr.com.arf.toys.db.model.uye.Uyeabone[id=" + this.getRID() + "]";
	}

	@Override
	public String getUIString() {
		return KeyUtil.getLabelValue("uyeabone.uyeRef") + " : " + getUyeRef().getUIString() + ", " + KeyUtil.getLabelValue("uyeabone.yayinRef") + " : " + getYayinRef().getAd();
	}

	@Override
	public void initValues(boolean defaultValues) {
		if (defaultValues) {
			// TODO Initialize with default values
		} else {
			this.rID = null;
			this.uyeRef = null;
			this.yayinRef = null;
			this.tarih = null;
			this.baslangictarih = null;
			this.bitistarih = null;
			this.uyeabonedurum = UyeAboneDurum._NULL;
			this.aciklama = "";
		}
	}

	@Override
	public String getValue() {
		StringBuilder value = new StringBuilder("");
		if (getUyeRef() != null) {
			value.append(KeyUtil.getLabelValue("uyeabone.uyeRef") + " : " + getUyeRef().getUIString() + "<br />");
		}
		if (getYayinRef() != null) {
			value.append(KeyUtil.getLabelValue("uyeabone.yayinRef") + " : " + getYayinRef().getUIString() + "<br />");
		}
		if (getTarih() != null) {
			value.append(KeyUtil.getLabelValue("uyeabone.tarih") + " : " + getTarih() + "<br />");
		}
		if (getBaslangictarih() != null) {
			value.append(KeyUtil.getLabelValue("uyeabone.baslangictarih") + " : " + getBaslangictarih() + "<br />");
		}
		if (getBitistarih() != null) {
			value.append(KeyUtil.getLabelValue("uyeabone.bitistarih") + " : " + getBitistarih() + "<br />");
		}
		if (getUyeabonedurum() != null) {
			value.append(KeyUtil.getLabelValue("uyeabone.uyeabonedurum") + " : " + getUyeabonedurum() + "<br />");
		}
		if (getAciklama() != null) {
			value.append(KeyUtil.getLabelValue("uyeabone.aciklama") + " : " + getAciklama() + "");
		}
		return value.toString();
	}

}
