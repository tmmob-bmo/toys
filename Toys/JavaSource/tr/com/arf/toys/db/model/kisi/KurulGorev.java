package tr.com.arf.toys.db.model.kisi; 
 
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import tr.com.arf.framework.utility.tool.KeyUtil;
import tr.com.arf.toys.db.enumerated.kisi.GorevTuru;
import tr.com.arf.toys.db.model.base.ToysBaseEntity;
 
@Entity 
@Table(name = "KURULGOREV") 
public class KurulGorev extends ToysBaseEntity { 
 
	private static final long serialVersionUID = 1L; 
 
	@Id 
	@GeneratedValue(strategy = GenerationType.IDENTITY) 
	@Column(updatable = false, unique = true, nullable = false, name = "RID") 
	private Long rID; 
	
	@Column(name = "TANIM") 
	private String tanim;
	
	@Enumerated(EnumType.ORDINAL)
	@Column(name = "GOREVTURU") 
	private GorevTuru gorevTuru; 
	
	public KurulGorev() { 
		super(); 
	} 
 
	@Override
	public Long getRID() { 
		return this.rID;  
	}  
	  
	@Override
	public void setRID(Long rID) {  
		this.rID = rID;  
	}   
	
	public String getTanim() {
		return tanim;
	}

	public void setTanim(String tanim) {
		this.tanim = tanim;
	}

	public GorevTuru getGorevTuru() {
		return gorevTuru;
	}

	public void setGorevTuru(GorevTuru gorevTuru) {
		this.gorevTuru = gorevTuru;
	}

	@Override 
	public String toString() { 
		return "tr.com.arf.toys.db.model.kisi.KurulGorev[id=" + this.getRID()  + "]"; 
	} 
	@Override 
	public String getUIString() { 
		return getTanim() + ""; 
	} 
	 
	@Override 
	public void initValues(boolean defaultValues) { 
		if (defaultValues) { 
				// TODO Initialize with default values 
		} else { 
			this.rID = null; 			this.tanim=null;
			this.gorevTuru=GorevTuru._NULL;
		}  
	}  
	 
	@Override 
	public String getValue() {  
		StringBuilder value = new StringBuilder("Veri : <br />"); 
			
			if(getTanim() != null) {
				value.append(KeyUtil.getLabelValue("kurulgorev.tanim") + " : " + getTanim() + "<br />"); 
			}
			if(getGorevTuru() != null) 
				value.append(KeyUtil.getLabelValue("kurulgorev.gorevturu") + " : " + getGorevTuru() + ""); 
			
		return value.toString(); 
	} 
	 
}	 
