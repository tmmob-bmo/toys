package tr.com.arf.toys.db.model.system; 
 
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import tr.com.arf.framework.utility.tool.KeyUtil;
import tr.com.arf.toys.db.model.base.ToysBaseEntity;
 
 
@Entity 
@Table(name = "SEHIR") 
public class Sehir extends ToysBaseEntity { 
 
	private static final long serialVersionUID = 1L; 
 
	@Id 
	@GeneratedValue(strategy = GenerationType.IDENTITY) 
	@Column(updatable = false, unique = true, nullable = false, name = "RID") 
	private Long rID; 
 
	@Column(name = "AD") 
	private String ad; 
   
 
	@Column(name = "ILKODU") 
	private String ilKodu;  
 
 
	public Sehir() { 
		super(); 
	} 
 
	@Override
	public Long getRID() { 
		return this.rID;  
	}  
	  
	@Override
	public void setRID(Long rID) {  
		this.rID = rID;  
	}   
 
	public String getAd() { 
		return this.ad; 
	} 
	 
	public void setAd(String ad) { 
		this.ad = ad; 
	}  
 
 
	public String getIlKodu() {
		return ilKodu;
	}

	public void setIlKodu(String ilKodu) {
		this.ilKodu = ilKodu;
	}

	@Override 
	public String toString() { 
		return "tr.com.arf.toys.db.model.system.Sehir[id=" + this.getRID()  + "]"; 
	} 
	@Override 
	public String getUIString() { 
		return getAd() + ""; 
	} 
	 
	@Override 
	public void initValues(boolean defaultValues) { 
		if (defaultValues) { 
				// TODO Initialize with default values 
		} else { 
			this.rID = null; 			this.ad = ""; 			this.ilKodu = null;		}  
	}  
	 
	@Override 
	public String getValue() {  
		StringBuilder value = new StringBuilder(""); 
			if(getAd() != null) {
				value.append(KeyUtil.getLabelValue("sehir.ad") + " : " + getAd() + "<br />");
			} 
			if(getIlKodu() != null) {
				value.append(KeyUtil.getLabelValue("sehir.ilKodu") + " : " + getIlKodu() + "");
			} 
		return value.toString(); 
	} 
	 
}	 
