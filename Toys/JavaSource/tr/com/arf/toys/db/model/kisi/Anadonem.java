package tr.com.arf.toys.db.model.kisi; 
 
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import tr.com.arf.framework.db.enumerated._common.EvetHayir;
import tr.com.arf.framework.utility.tool.KeyUtil;
import tr.com.arf.toys.db.model.base.ToysBaseEntity;
 
 
@Entity 
@Table(name = "ANADONEM") 
public class Anadonem extends ToysBaseEntity { 
 
	private static final long serialVersionUID = 1L; 
 
	@Id 
	@GeneratedValue(strategy = GenerationType.IDENTITY) 
	@Column(updatable = false, unique = true, nullable = false, name = "RID") 
	private Long rID; 
 
	@Column(name = "TANIM") 
	private String tanim; 
	
	@Enumerated(EnumType.ORDINAL)
	@Column(name = "AKTIF") 
	private EvetHayir aktif;
 
 
	public Anadonem() { 
		super(); 
	} 
 
	@Override
	public Long getRID() { 
		return this.rID;  
	}  
	  
	@Override
	public void setRID(Long rID) {  
		this.rID = rID;  
	}   
 
	public String getTanim() { 
		return this.tanim; 
	} 
	 
	public void setTanim(String tanim) { 
		this.tanim = tanim; 
	} 
 
	public EvetHayir getAktif() {
		return aktif;
	}

	public void setAktif(EvetHayir aktif) {
		this.aktif = aktif;
	}

	@Override 
	public String toString() { 
		return "tr.com.arf.toys.db.model.donem.Anadonem[id=" + this.getRID()  + "]"; 
	} 
	@Override 
	public String getUIString() { 
		return getTanim() + ""; 
	} 
	 
	@Override 
	public void initValues(boolean defaultValues) { 
			this.rID = null; 			this.tanim = ""; 
			this.aktif=EvetHayir._NULL;	}  
	 
	@Override 
	public String getValue() {  
		StringBuilder value = new StringBuilder("Veri : <br />"); 
			if(getTanim() != null) {
				value.append(KeyUtil.getLabelValue("anadonem.tanim") + " : " + getTanim() + "");
			} 
			if(getAktif() != null) {
				value.append(KeyUtil.getLabelValue("anadonem.aktif") + " : " + getAktif() + "");
			} 
		return value.toString(); 
	} 
	 
}	 
