package tr.com.arf.toys.db.model.egitim; 
 
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tr.com.arf.framework.utility.tool.KeyUtil;
import tr.com.arf.toys.db.model.base.ToysBaseEntity;
 
@Entity 
@Table(name = "SINAVDEGERLENDIRICI") 
public class SinavDegerlendirici extends ToysBaseEntity { 
 
	private static final long serialVersionUID = 1L; 
 
	@Id 
	@GeneratedValue(strategy = GenerationType.IDENTITY) 
	@Column(updatable = false, unique = true, nullable = false, name = "RID") 
	private Long rID; 
 
	@ManyToOne 
	@JoinColumn(name = "DEGERLENDIRICIREF") 
	private Degerlendirici degerlendiriciRef; 
	
	@ManyToOne 
	@JoinColumn(name = "SINAVREF") 
	private Sinav sinavRef; 
	
	@Column(name = "ACIKLAMA") 
	private String aciklama; 
 
	public SinavDegerlendirici() { 
		super(); 
	} 
 
	@Override
	public Long getRID() { 
		return this.rID;  
	}  
	  
	@Override
	public void setRID(Long rID) {  
		this.rID = rID;  
	}   
 
	public Degerlendirici getDegerlendiriciRef() {
		return degerlendiriciRef;
	}

	public void setDegerlendiriciRef(Degerlendirici degerlendiriciRef) {
		this.degerlendiriciRef = degerlendiriciRef;
	}

	public Sinav getSinavRef() {
		return sinavRef;
	}

	public void setSinavRef(Sinav sinavRef) {
		this.sinavRef = sinavRef;
	}

	public String getAciklama() {
		return aciklama;
	}

	public void setAciklama(String aciklama) {
		this.aciklama = aciklama;
	}

	@Override 
	public String toString() { 
		return "tr.com.arf.toys.db.model.egitim.SinavDegerlendirici[id=" + this.getRID()  + "]"; 
	} 
	@Override 
	public String getUIString() { 
		if (getDegerlendiriciRef()!=null){
			return getDegerlendiriciRef().getUIString() + ""; 
		}else{
			return null;
		}
	} 
	 
	@Override 
	public void initValues(boolean defaultValues) { 
			this.rID = null; 			this.degerlendiriciRef = null; 
			this.sinavRef=null;
			this.aciklama=null;	}  
	 
	@Override 
	public String getValue() {  
		StringBuilder value = new StringBuilder("Veri : <br />"); 
			if(getDegerlendiriciRef() != null) {
				value.append(KeyUtil.getLabelValue("sinavDegerlendirici.degerlendiriciRef") + " : " + getDegerlendiriciRef().getUIString() + "<br />");
			} 
			if(getSinavRef() != null) {
				value.append(KeyUtil.getLabelValue("sinavDegerlendirici.sinavRef") + " : " + getSinavRef().getUIString() + "<br />");
			} 
			if(getAciklama() != null) {
				value.append(KeyUtil.getLabelValue("sinavDegerlendirici.aciklama") + " : " + getAciklama() + "<br />");
			} 
			
		return value.toString(); 
	} 
	 
}	 
