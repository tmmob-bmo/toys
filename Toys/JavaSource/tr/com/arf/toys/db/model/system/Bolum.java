package tr.com.arf.toys.db.model.system; 
 
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import tr.com.arf.framework.utility.tool.KeyUtil;
import tr.com.arf.toys.db.model.base.ToysBaseEntity;
 
 
@Entity 
@Table(name = "BOLUM") 
public class Bolum extends ToysBaseEntity { 
 
	private static final long serialVersionUID = 1L; 
 
	@Id 
	@GeneratedValue(strategy = GenerationType.IDENTITY) 
	@Column(updatable = false, unique = true, nullable = false, name = "RID") 
	private Long rID; 
 
	@Column(name = "AD") 
	private String ad; 
	
	@Column(name="LISANS_KAYDOLUNABILIR")
	private boolean lisansKaydolunabilir=false;
	
	public Bolum() { 
		super(); 
	} 
 
	@Override
	public Long getRID() { 
		return this.rID;  
	}  
	  
	@Override
	public void setRID(Long rID) {  
		this.rID = rID;  
	}   
 
	public String getAd() { 
		return this.ad; 
	} 
	 
	public void setAd(String ad) { 
		this.ad = ad; 
	}
	
	public boolean isLisansKaydolunabilir() {
		return lisansKaydolunabilir;
	}

	public void setLisansKaydolunabilir(boolean kaydolunabilir) {
		this.lisansKaydolunabilir = kaydolunabilir;
	}

	@Override 
	public String toString() { 
		return this.getRID()  + ""; 
	} 
	@Override 
	public String getUIString() { 
		return getAd() + " " ; 
	} 
	 
	@Override 
	public void initValues(boolean defaultValues) { 
		if (defaultValues) { 
				// TODO Initialize with default values 
		} else { 
			this.rID = null; 
			this.ad = ""; 
		}  
	}  
	 
	@Override 
	public String getValue() {
        StringBuilder value = new StringBuilder("");
        if (getAd() != null) {
            value.append(KeyUtil.getLabelValue("bolum.ad")).append(" : ").append(getAd()).append("<br />");
            value.append(KeyUtil.getLabelValue("bolum.lisansKaydolunabilir")).append(" : ")
                    .append(isLisansKaydolunabilir()).append("<br />");
        }
        return value.toString();
    }
	 
}	 
