package tr.com.arf.toys.db.model.egitim; 
 
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tr.com.arf.framework.utility.tool.KeyUtil;
import tr.com.arf.toys.db.model.base.ToysBaseEntity;

 
@Entity 
@Table(name = "EGITIMOGRETMEN") 
public class Egitimogretmen extends ToysBaseEntity { 
 
	private static final long serialVersionUID = 1L; 
 
	@Id 
	@GeneratedValue(strategy = GenerationType.IDENTITY) 
	@Column(updatable = false, unique = true, nullable = false, name = "RID") 
	private Long rID; 
 
	@ManyToOne 
	@JoinColumn(name = "EGITIMREF") 
	private Egitim egitimRef; 
 
	@ManyToOne 
	@JoinColumn(name = "EGITMENREF") 
	private Egitmen egitmenRef; 
 
	@Column(name = "ACIKLAMA") 
	private String aciklama; 
 
 
	public Egitimogretmen() { 
		super(); 
	} 
 
	@Override
	public Long getRID() { 
		return this.rID;  
	}  
	  
	@Override
	public void setRID(Long rID) {  
		this.rID = rID;  
	}   
 
	public Egitim getEgitimRef() { 
		return egitimRef; 
	} 
	 
	public void setEgitimRef(Egitim egitimRef) { 
		this.egitimRef = egitimRef; 
	} 
 
	public Egitmen getEgitmenRef() {
		return egitmenRef;
	}

	public void setEgitmenRef(Egitmen egitmenRef) {
		this.egitmenRef = egitmenRef;
	}

	public String getAciklama() { 
		return this.aciklama; 
	} 
	 
	public void setAciklama(String aciklama) { 
		this.aciklama = aciklama; 
	} 
 
 
	@Override 
	public String toString() { 
		return "tr.com.arf.toys.db.model.egitim.Egitimogretmen[id=" + this.getRID()  + "]"; 
	} 
	@Override 
	public String getUIString() { 
		return "(" + getEgitimRef().getAd()+ ")  "+ getEgitmenRef().getKisiRef().getAd() + " "  + getEgitmenRef().getKisiRef().getSoyad(); 
	} 
	
	public String getUIStringShort() { 
		return  getEgitmenRef().getKisiRef().getAd() + " "  + getEgitmenRef().getKisiRef().getSoyad(); 
	} 
	 
	@Override 
	public void initValues(boolean defaultValues) { 
			this.rID = null; 
			this.egitimRef = null; 
			this.egitmenRef = null; 
			this.aciklama = ""; 
	}  
	 
	@Override 
	public String getValue() {  
		StringBuilder value = new StringBuilder("Veri : <br />"); 
			if(getEgitimRef() != null) {
				value.append(KeyUtil.getLabelValue("egitimogretmen.egitimRef") + " : " + getEgitimRef().getUIString() + "<br />");
			} 
			if(getEgitmenRef() != null) {
				value.append(KeyUtil.getLabelValue("egitimogretmen.egitmenRef") + " : " + getEgitmenRef().getUIString() + "<br />");
			} 
			if(getAciklama() != null) {
				value.append(KeyUtil.getLabelValue("egitimogretmen.aciklama") + " : " + getAciklama() + "");
			} 
		return value.toString(); 
	} 
	 
}	 
