package tr.com.arf.toys.db.model.evrak; 
 
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tr.com.arf.framework.db.model._base.BaseTreeEntity;
import tr.com.arf.framework.utility.tool.KeyUtil;
import tr.com.arf.toys.db.model.base.ToysBaseTreeEntity;
 
 
@Entity 
@Table(name = "DOSYAKODU") 
public class Dosyakodu extends ToysBaseTreeEntity { 
 
	private static final long serialVersionUID = 1L; 
 
	@Id 
	@GeneratedValue(strategy = GenerationType.IDENTITY) 
	@Column(updatable = false, unique = true, nullable = false, name = "RID") 
	private Long rID; 
 
	@ManyToOne 
	@JoinColumn(name = "USTREF") 
	private Dosyakodu ustRef; 
 
	@Column(name = "KOD") 
	private String kod; 
 
	@Column(name = "AD") 
	private String ad; 
 
	@Column(name = "ERISIMKODU") 
	private String erisimKodu; 
   
	public Dosyakodu() { 
		super(); 
	} 
 
	@Override
	public Long getRID() { 
		return this.rID;  
	}  
	  
	@Override
	public void setRID(Long rID) {  
		this.rID = rID;  
	}   
 
	@Override
	public Dosyakodu getUstRef() { 
		return ustRef; 
	} 
	 
	@Override
	public void setUstRef(BaseTreeEntity ustRef) { 
		this.ustRef = (Dosyakodu) ustRef; 
	} 
	 
	public void setUstRef(Dosyakodu ustRef) { 
		this.ustRef = ustRef; 
	}  
	
	public String getKod() { 
		return this.kod; 
	} 
	 
	public void setKod(String kod) { 
		this.kod = kod; 
	} 
 
	@Override
	public String getAd() { 
		return this.ad; 
	} 
	 
	@Override
	public void setAd(String ad) { 
		this.ad = ad; 
	} 
 
	@Override
	public String getErisimKodu() { 
		return this.erisimKodu; 
	} 
	 
	@Override
	public void setErisimKodu(String erisimKodu) { 
		this.erisimKodu = erisimKodu; 
	} 
  
	@Override 
	public String toString() { 
		return getAd(); 
	} 
	
	@Override 
	public String getUIString() { 
		return getAd(); 
	} 
	 
	@Override 
	public void initValues(boolean defaultValues) { 
		if (defaultValues) { 
				// TODO Initialize with default values 
		} else { 
			this.rID = null; 			this.ustRef = null; 			this.kod = ""; 			this.ad = ""; 			this.erisimKodu = "";  		}  
	}  
	 
	@Override 
	public String getValue() {  
		StringBuilder value = new StringBuilder("Veri : <br />"); 
			if(getUstRef() != null) {
				value.append(KeyUtil.getLabelValue("dosyakodu.ustRef") + " : " + getUstRef().getUIString() + "<br />");
			} 
			if(getKod() != null) {
				value.append(KeyUtil.getLabelValue("dosyakodu.kod") + " : " + getKod() + "<br />");
			} 
			if(getAd() != null) {
				value.append(KeyUtil.getLabelValue("dosyakodu.ad") + " : " + getAd() + "<br />");
			} 
			if(getErisimKodu() != null) {
				value.append(KeyUtil.getLabelValue("dosyakodu.erisimKodu") + " : " + getErisimKodu() + "");
			} 
		return value.toString(); 
	} 
	 
}	 
