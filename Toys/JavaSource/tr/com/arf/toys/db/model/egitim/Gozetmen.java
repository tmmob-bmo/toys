package tr.com.arf.toys.db.model.egitim; 
 

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tr.com.arf.framework.utility.tool.KeyUtil;
import tr.com.arf.toys.db.model.base.ToysBaseEntity;
import tr.com.arf.toys.db.model.system.Birim;
import tr.com.arf.toys.db.model.uye.Uye;
 
@Entity 
@Table(name = "GOZETMEN") 
public class Gozetmen extends ToysBaseEntity { 
 
	private static final long serialVersionUID = 1L; 
 
	@Id 
	@GeneratedValue(strategy = GenerationType.IDENTITY) 
	@Column(updatable = false, unique = true, nullable = false, name = "RID") 
	private Long rID; 
 
	@ManyToOne 
	@JoinColumn(name = "UYEREF") 
	private Uye uyeRef; 
	
	@ManyToOne 
	@JoinColumn(name = "BIRIMREF") 
	private Birim birimRef; 
	
	@Column(name = "ACIKLAMA") 
	private String aciklama; 
	
	public Gozetmen() { 
		super(); 
	} 
 
	@Override
	public Long getRID() { 
		return this.rID;  
	}  
	  
	@Override
	public void setRID(Long rID) {  
		this.rID = rID;  
	}   

	public Uye getUyeRef() {
		return uyeRef;
	}

	public void setUyeRef(Uye uyeRef) {
		this.uyeRef = uyeRef;
	}

	public Birim getBirimRef() {
		return birimRef;
	}

	public void setBirimRef(Birim birimRef) {
		this.birimRef = birimRef;
	}

	public String getAciklama() {
		return aciklama;
	}

	public void setAciklama(String aciklama) {
		this.aciklama = aciklama;
	}

	@Override 
	public String toString() { 
		return "tr.com.arf.toys.db.model.egitim.Gozetmen[id=" + this.getRID()  + "]"; 
	} 
	@Override 
	public String getUIString() { 
		if (getUyeRef()!=null){
			return getUyeRef().getUIString();
		}
		return null;
		
	} 
	 
	@Override 
	public void initValues(boolean defaultValues) { 
			this.rID = null; 			this.aciklama=null;
			this.birimRef=null;
			this.uyeRef=null;	}  
	 
	@Override 
	public String getValue() {  
		StringBuilder value = new StringBuilder("Veri : <br />"); 
			if(getBirimRef() != null) {
				value.append(KeyUtil.getLabelValue("gozetmen.birimRef") + " : " + getBirimRef().getUIString() + "<br />");
			} 
			if(getUyeRef() != null) {
				value.append(KeyUtil.getLabelValue("gozetmen.uyeRef") + " : " + getUyeRef().getUIString() + "<br />");
			} 
			if(getAciklama() != null) {
				value.append(KeyUtil.getLabelValue("gozetmen.aciklama") + " : " + getAciklama() + "<br />");
			} 
		return value.toString(); 
	} 
	 
}	 
