package tr.com.arf.toys.db.model.uye; 
 
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import tr.com.arf.framework.utility.tool.DateUtil;
import tr.com.arf.framework.utility.tool.KeyUtil;
import tr.com.arf.toys.db.enumerated.uye.UyeBelgeDurum;
import tr.com.arf.toys.db.model.base.ToysBaseEntity;
import tr.com.arf.toys.db.model.evrak.Evrak;
import tr.com.arf.toys.db.model.kisi.Kurum;
import tr.com.arf.toys.db.model.system.Belgetip;
 
@Entity 
@Table(name = "UYEBELGE") 
public class Uyebelge extends ToysBaseEntity { 
 
	private static final long serialVersionUID = 1L; 
 
	@Id 
	@GeneratedValue(strategy = GenerationType.IDENTITY) 
	@Column(updatable = false, unique = true, nullable = false, name = "RID") 
	private Long rID; 
 
	@ManyToOne 
	@JoinColumn(name = "UYEREF") 
	private Uye uyeRef; 
 
	@ManyToOne 
	@JoinColumn(name = "KURUMREF") 
	private Kurum kurumRef; 
 
	@ManyToOne 
	@JoinColumn(name = "BELGETIPREF") 
	private Belgetip belgetipRef; 
 
	@Temporal(TemporalType.DATE) 
	@Column(name = "TARIH") 
	private java.util.Date tarih; 
 
	@Temporal(TemporalType.DATE) 
	@Column(name = "BASVURUTARIH") 
	private java.util.Date basvurutarih; 
 
	@Temporal(TemporalType.DATE) 
	@Column(name = "HAZIRLANMATARIH") 
	private java.util.Date hazirlanmatarih; 
 
	@ManyToOne 
	@JoinColumn(name = "EVRAKREF") 
	private Evrak evrakRef; 
 
	@Temporal(TemporalType.DATE) 
	@Column(name = "TESLIMTARIH") 
	private java.util.Date teslimtarih; 
 
	@Enumerated(EnumType.ORDINAL) 
	@Column(name = "UYEBELGEDURUM") 
	private UyeBelgeDurum uyebelgedurum;
	
	@Column(name = "PATH") 
	private String path; 
  
	@Column(name = "ACIKLAMA") 
	private String aciklama; 
 
 
	public Uyebelge() { 
		super(); 
	} 
 
	@Override
	public Long getRID() { 
		return this.rID;  
	}  
	  
	@Override
	public void setRID(Long rID) {  
		this.rID = rID;  
	}   
 
	public Uye getUyeRef() { 
		return uyeRef; 
	} 
	 
	public void setUyeRef(Uye uyeRef) { 
		this.uyeRef = uyeRef; 
	} 
 
	public Kurum getKurumRef() { 
		return kurumRef; 
	} 
	 
	public void setKurumRef(Kurum kurumRef) { 
		this.kurumRef = kurumRef; 
	} 
 
	public Belgetip getBelgetipRef() { 
		return belgetipRef; 
	} 
	 
	public void setBelgetipRef(Belgetip belgetipRef) { 
		this.belgetipRef = belgetipRef; 
	} 
 
	public java.util.Date getTarih() { 
		return tarih; 
	} 
	 
	public void setTarih(java.util.Date tarih) { 
		this.tarih = tarih; 
	} 
 
	public java.util.Date getBasvurutarih() { 
		return basvurutarih; 
	} 
	 
	public void setBasvurutarih(java.util.Date basvurutarih) { 
		this.basvurutarih = basvurutarih; 
	} 
 
	public java.util.Date getHazirlanmatarih() { 
		return hazirlanmatarih; 
	} 
	 
	public void setHazirlanmatarih(java.util.Date hazirlanmatarih) { 
		this.hazirlanmatarih = hazirlanmatarih; 
	} 
 
	public Evrak getEvrakRef() { 
		return evrakRef; 
	} 
	 
	public void setEvrakRef(Evrak evrakRef) { 
		this.evrakRef = evrakRef; 
	} 
 
	public java.util.Date getTeslimtarih() { 
		return teslimtarih; 
	} 
	 
	public void setTeslimtarih(java.util.Date teslimtarih) { 
		this.teslimtarih = teslimtarih; 
	} 
 
	public UyeBelgeDurum getUyebelgedurum() { 
		return this.uyebelgedurum;  
	}  
  
	public void setUyebelgedurum(UyeBelgeDurum uyebelgedurum) {  
		this.uyebelgedurum = uyebelgedurum;  
	}   
 
	public String getAciklama() { 
		return this.aciklama; 
	} 
	 
	public void setAciklama(String aciklama) { 
		this.aciklama = aciklama; 
	} 
  
	@Override
	public String getPath() {
		return path;
	}

	@Override
	public void setPath(String path) {
		this.path = path;
	}

	@Override 
	public String toString() { 
		return "tr.com.arf.toys.db.model.uye.Uyebelge[id=" + this.getRID()  + "]"; 
	} 
	@Override 
	public String getUIString() { 
		return getUyeRef().getKisiRef().getUIStringShort() + ", " + KeyUtil.getLabelValue("uyebelge.belgetipRef") + ":" + getBelgetipRef().getAd(); 
	} 
	 
	@Override 
	public void initValues(boolean defaultValues) { 
		if (defaultValues) { 
				// TODO Initialize with default values 
		} else { 
			this.rID = null; 			this.uyeRef = null; 			this.kurumRef = null; 			this.belgetipRef = null; 			this.tarih = null; 			this.basvurutarih = null; 			this.hazirlanmatarih = null; 			this.evrakRef = null; 			this.teslimtarih = null; 			this.uyebelgedurum = UyeBelgeDurum._NULL; 			this.aciklama = ""; 
			this.path = null;		}  
	}  
	 
	@Override 
	public String getValue() {  
		StringBuilder value = new StringBuilder(""); 
			if(getUyeRef() != null) {
				value.append(KeyUtil.getLabelValue("uyebelge.uyeRef") + " : " + getUyeRef().getUIString() + "<br />");
			} 
			if(getKurumRef() != null) {
				value.append(KeyUtil.getLabelValue("uyebelge.kurumRef") + " : " + getKurumRef().getUIString() + "<br />");
			} 
			if(getBelgetipRef() != null) {
				value.append(KeyUtil.getLabelValue("uyebelge.belgetipRef") + " : " + getBelgetipRef().getUIString() + "<br />");
			} 
			if(getTarih() != null) {
				value.append(KeyUtil.getLabelValue("uyebelge.tarih") + " : " + DateUtil.dateToDMY(getTarih()) + "<br />");
			} 
			if(getBasvurutarih() != null) {
				value.append(KeyUtil.getLabelValue("uyebelge.basvurutarih") + " : " + DateUtil.dateToDMY(getBasvurutarih()) + "<br />");
			} 
			if(getHazirlanmatarih() != null) {
				value.append(KeyUtil.getLabelValue("uyebelge.hazirlanmatarih") + " : " + DateUtil.dateToDMY(getHazirlanmatarih()) + "<br />");
			} 
			if(getEvrakRef() != null) {
				value.append(KeyUtil.getLabelValue("uyebelge.evrakRef") + " : " + getEvrakRef().getUIString() + "<br />");
			} 
			if(getTeslimtarih() != null) {
				value.append(KeyUtil.getLabelValue("uyebelge.teslimtarih") + " : " + DateUtil.dateToDMY(getTeslimtarih()) + "<br />");
			} 
			if(getUyebelgedurum() != null) {
				value.append(KeyUtil.getLabelValue("uyebelge.uyebelgedurum") + " : " + getUyebelgedurum() + "<br />");
			} 
			if(getAciklama() != null) {
				value.append(KeyUtil.getLabelValue("uyebelge.aciklama") + " : " + getAciklama() + "");
			} 
			if(getPath() != null) {
				value.append(KeyUtil.getLabelValue("uyebelge.path") + " : " + getPath() + "");
			}
		return value.toString(); 
	} 
	 
}	 
