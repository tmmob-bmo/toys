package tr.com.arf.toys.db.model.egitim; 
 
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import tr.com.arf.framework.utility.tool.DateUtil;
import tr.com.arf.framework.utility.tool.KeyUtil;
import tr.com.arf.toys.db.model.base.ToysBaseEntity;
 
@Entity 
@Table(name = "EGITIMGUNLERI") 
public class EgitimGunleri extends ToysBaseEntity { 
 
	private static final long serialVersionUID = 1L; 
 
	@Id 
	@GeneratedValue(strategy = GenerationType.IDENTITY) 
	@Column(updatable = false, unique = true, nullable = false, name = "RID") 
	private Long rID; 
 
	@ManyToOne 
	@JoinColumn(name = "EGITIMREF") 
	private Egitim egitimRef; 
	
	@Temporal(TemporalType.TIMESTAMP) 
	@Column(name = "TARIH") 
	private java.util.Date tarih; 
	
 
	public EgitimGunleri() { 
		super(); 
	} 
 
	@Override
	public Long getRID() { 
		return this.rID;  
	}  
	  
	@Override
	public void setRID(Long rID) {  
		this.rID = rID;  
	}   
 
	public Egitim getEgitimRef() {
		return egitimRef;
	}

	public void setEgitimRef(Egitim egitimRef) {
		this.egitimRef = egitimRef;
	}

	public java.util.Date getTarih() {
		return tarih;
	}

	public void setTarih(java.util.Date tarih) {
		this.tarih = tarih;
	}

	@Override 
	public String toString() { 
		return "tr.com.arf.toys.db.model.egitim.EgitimGunleri[id=" + this.getRID()  + "]"; 
	} 
	@Override 
	public String getUIString() { 
		if (getEgitimRef()!=null){
			return getEgitimRef() + ""; 
		}else{
			return null;
		}
	} 
	 
	@Override 
	public void initValues(boolean defaultValues) { 
			this.rID = null; 			this.egitimRef = null; 
			this.tarih=null;	}  
	 
	@Override 
	public String getValue() {  
		StringBuilder value = new StringBuilder("Veri : <br />"); 
			if(getEgitimRef()!= null) {
				value.append(KeyUtil.getLabelValue("egitimGunleri.egitimRef") + " : " + getEgitimRef().getUIString() + "<br />");
			} 
			if(getTarih() != null) {
				value.append(KeyUtil.getLabelValue("egitimGunleri.tarih") + " : " + DateUtil.dateToDMYHMS(getTarih()) + "<br />");
			} 
			
		return value.toString(); 
	} 
	 
}	 
