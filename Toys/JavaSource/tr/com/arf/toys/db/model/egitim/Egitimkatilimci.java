package tr.com.arf.toys.db.model.egitim; 
 
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tr.com.arf.framework.utility.tool.KeyUtil;
import tr.com.arf.toys.db.enumerated.egitim.EgitimkatilimciDurum;
import tr.com.arf.toys.db.enumerated.egitim.KatilimTuru;
import tr.com.arf.toys.db.enumerated.egitim.Katilimbelgesi;
import tr.com.arf.toys.db.enumerated.egitim.Sertifika;
import tr.com.arf.toys.db.enumerated.egitim.SinavbasariDurum;
import tr.com.arf.toys.db.model.base.ToysBaseEntity;
import tr.com.arf.toys.db.model.kisi.Kisi;
 
@Entity 
@Table(name = "EGITIMKATILIMCI") 
public class Egitimkatilimci extends ToysBaseEntity { 
 
	private static final long serialVersionUID = 1L; 
 
	@Id 
	@GeneratedValue(strategy = GenerationType.IDENTITY) 
	@Column(updatable = false, unique = true, nullable = false, name = "RID") 
	private Long rID; 
 
	@ManyToOne 
	@JoinColumn(name = "EGITIMREF") 
	private Egitim egitimRef; 
 
	@ManyToOne 
	@JoinColumn(name = "KISIREF") 
	private Kisi kisiRef; 
 
	@Enumerated(EnumType.ORDINAL) 
	@Column(name = "DURUM") 
	private EgitimkatilimciDurum durum; 
 
	@Enumerated(EnumType.ORDINAL) 
	@Column(name = "KATILIMBELGESI") 
	private Katilimbelgesi katilimbelgesi; 
 
	@Enumerated(EnumType.ORDINAL) 
	@Column(name = "SERTIFIKA") 
	private Sertifika sertifika; 
 
	@Column(name = "SINAVNOTU") 
	private String sinavnotu; 
 
	@Enumerated(EnumType.ORDINAL) 
	@Column(name = "SINAVBASARIDURUM") 
	private SinavbasariDurum sinavbasariDurum; 
 
	@Column(name = "ACIKLAMA") 
	private String aciklama; 
	
	@Enumerated(EnumType.ORDINAL) 
	@Column(name = "KATILIMTURU") 
	private KatilimTuru katilimturu; 
 
 
	public Egitimkatilimci() { 
		super(); 
	} 
 
	@Override
	public Long getRID() { 
		return this.rID;  
	}  

	@Override
	public void setRID(Long rID) {  
		this.rID = rID;  
	}   
 
	public Egitim getEgitimRef() { 
		return egitimRef; 
	} 
	 
	public void setEgitimRef(Egitim egitimRef) { 
		this.egitimRef = egitimRef; 
	} 
 
	public Kisi getKisiRef() { 
		return kisiRef; 
	} 
	 
	public void setKisiRef(Kisi kisiRef) { 
		this.kisiRef = kisiRef; 
	} 
 
	public EgitimkatilimciDurum getDurum() { 
		return this.durum;  
	}  
  
	public void setDurum(EgitimkatilimciDurum durum) {  
		this.durum = durum;  
	}   
 
	public Katilimbelgesi getKatilimbelgesi() { 
		return this.katilimbelgesi;  
	}  
  
	public void setKatilimbelgesi(Katilimbelgesi katilimbelgesi) {  
		this.katilimbelgesi = katilimbelgesi;  
	}   
 
	public Sertifika getSertifika() { 
		return this.sertifika;  
	}  
  
	public void setSertifika(Sertifika sertifika) {  
		this.sertifika = sertifika;  
	}   
 
	public String getSinavnotu() { 
		return this.sinavnotu; 
	} 
	 
	public void setSinavnotu(String sinavnotu) { 
		this.sinavnotu = sinavnotu; 
	} 
 
	public SinavbasariDurum getSinavbasariDurum() { 
		return this.sinavbasariDurum;  
	}  
  
	public void setSinavbasariDurum(SinavbasariDurum sinavbasariDurum) {  
		this.sinavbasariDurum = sinavbasariDurum;  
	}   
 
	public String getAciklama() { 
		return this.aciklama; 
	} 
	 
	public void setAciklama(String aciklama) { 
		this.aciklama = aciklama; 
	} 
 
	public KatilimTuru getKatilimturu() {
		return katilimturu;
	}

	public void setKatilimturu(KatilimTuru katilimturu) {
		this.katilimturu = katilimturu;
	}

	@Override 
	public String toString() { 
		return "tr.com.arf.toys.db.model.egitim.Egitimkatilimci[id=" + this.getRID()  + "]"; 
	} 
	@Override 
	public String getUIString() { 
		return getKisiRef().getAd() + " " + getKisiRef().getSoyad(); 
	} 
	 
	@Override 
	public void initValues(boolean defaultValues) { 
			this.rID = null; 
			this.egitimRef = null; 
			this.kisiRef = null; 
			this.durum = EgitimkatilimciDurum._NULL; 
			this.katilimbelgesi = Katilimbelgesi._NULL; 
			this.sertifika = Sertifika._NULL; 
			this.sinavnotu = ""; 
			this.sinavbasariDurum = SinavbasariDurum._NULL; 
			this.aciklama = ""; 
			this.katilimturu=KatilimTuru._NULL;
	}  
	 
	@Override 
	public String getValue() {  
		StringBuilder value = new StringBuilder("Veri : <br />"); 
			if(getEgitimRef() != null) {
				value.append(KeyUtil.getLabelValue("egitimkatilimci.egitimRef") + " : " + getEgitimRef().getUIString() + "<br />");
			} 
			if(getKisiRef() != null) {
				value.append(KeyUtil.getLabelValue("egitimkatilimci.kisiRef") + " : " + getKisiRef().getUIString() + "<br />");
			} 
			if(getDurum() != null) {
				value.append(KeyUtil.getLabelValue("egitimkatilimci.durum") + " : " + getDurum().getLabel() + "<br />");
			} 
			if(getKatilimbelgesi() != null) {
				value.append(KeyUtil.getLabelValue("egitimkatilimci.katilimbelgesi") + " : " + getKatilimbelgesi().getLabel() + "<br />");
			} 
			if(getSertifika() != null) {
				value.append(KeyUtil.getLabelValue("egitimkatilimci.sertifika") + " : " + getSertifika().getLabel() + "<br />");
			} 
			if(getSinavnotu() != null) {
				value.append(KeyUtil.getLabelValue("egitimkatilimci.sinavnotu") + " : " + getSinavnotu() + "<br />");
			} 
			if(getSinavbasariDurum() != null) {
				value.append(KeyUtil.getLabelValue("egitimkatilimci.sinavbasariDurum") + " : " + getSinavbasariDurum().getLabel() + "<br />");
			} 
			if(getAciklama() != null) {
				value.append(KeyUtil.getLabelValue("egitimkatilimci.aciklama") + " : " + getAciklama() + "");
			} 
			if(getKatilimturu() != null) {
				value.append(KeyUtil.getLabelValue("egitimkatilimci.katilimturu") + " : " + getKatilimturu().getLabel() + "<br />");
			} 
		return value.toString(); 
	} 
	 
}	 
