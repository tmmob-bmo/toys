package tr.com.arf.toys.db.model.system; 
 
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import tr.com.arf.framework.db.enumerated._common.EvetHayir;
import tr.com.arf.framework.utility.exception.data.DBException;
import tr.com.arf.framework.utility.tool.DateUtil;
import tr.com.arf.framework.utility.tool.KeyUtil;
import tr.com.arf.toys.db.enumerated.uye.UyekurumDurum;
import tr.com.arf.toys.db.model.base.ToysBaseEntity;
import tr.com.arf.toys.db.model.iletisim.Adres;
import tr.com.arf.toys.db.model.kisi.Kisi;
import tr.com.arf.toys.db.model.kisi.Kurum;
 
 
@Entity 
@Table(name = "KURUMKISI") 
public class KurumKisi extends ToysBaseEntity { 
 
	private static final long serialVersionUID = 1L; 
 
	@Id 
	@GeneratedValue(strategy = GenerationType.IDENTITY) 
	@Column(updatable = false, unique = true, nullable = false, name = "RID") 
	private Long rID; 
	
	@ManyToOne 
	@JoinColumn(name = "KURUMREF") 
	private Kurum kurumRef; 
	
	@ManyToOne 
	@JoinColumn(name = "KISIREF")
	private Kisi kisiRef;
	
	@Enumerated(EnumType.ORDINAL)
	@Column(name = "GECERLI") 
	private EvetHayir gecerli; 
	
	@Temporal(TemporalType.DATE) 
	@Column(name = "BASLANGICTARIH") 
	private java.util.Date baslangictarih; 
 
	@Temporal(TemporalType.DATE) 
	@Column(name = "BITISTARIH") 
	private java.util.Date bitistarih; 
 
	@Enumerated(EnumType.ORDINAL) 
	@Column(name = "UYEKURUMDURUM") 
	private UyekurumDurum uyekurumDurum; 
	
	@Enumerated(EnumType.ORDINAL)
	@Column(name = "VARSAYILAN") 
	private EvetHayir varsayilan;
	
	@Column(name = "GOREV") 
	private String gorev; 
 
	public KurumKisi() { 
		super(); 
	} 
 
	@Override
	public Long getRID() { 
		return this.rID;  
	}  
	  
	@Override
	public void setRID(Long rID) {  
		this.rID = rID;  
	}   
 
	public Kisi getKisiRef() {
		return kisiRef;
	}

	public void setKisiRef(Kisi kisiRef) {
		this.kisiRef = kisiRef;
	}

	public Kurum getKurumRef() {
		return kurumRef;
	}

	public void setKurumRef(Kurum kurumRef) {
		this.kurumRef = kurumRef;
	}
	
	public EvetHayir getGecerli() {
		return gecerli;
	}

	public void setGecerli(EvetHayir gecerli) {
		this.gecerli = gecerli;
	}

	public java.util.Date getBaslangictarih() {
		return baslangictarih;
	}

	public void setBaslangictarih(java.util.Date baslangictarih) {
		this.baslangictarih = baslangictarih;
	}

	public java.util.Date getBitistarih() {
		return bitistarih;
	}

	public void setBitistarih(java.util.Date bitistarih) {
		this.bitistarih = bitistarih;
	}

	public UyekurumDurum getUyekurumDurum() {
		return uyekurumDurum;
	}

	public void setUyekurumDurum(UyekurumDurum uyekurumDurum) {
		this.uyekurumDurum = uyekurumDurum;
	}

	public EvetHayir getVarsayilan() {
		return varsayilan;
	}

	public void setVarsayilan(EvetHayir varsayilan) {
		this.varsayilan = varsayilan;
	}

	public String getGorev() {
		return gorev;
	}

	public void setGorev(String gorev) {
		this.gorev = gorev;
	}

	@Override 
	public String toString() { 
		return "tr.com.arf.toys.db.model.system.KurumKisi[id=" + this.getRID()  + "]"; 
	} 
	@Override 
	public String getUIString() { 
		return getKisiRef().getUIStringShort() + " -" + getKurumRef().getAd(); 
	} 
	 
	@Override 
	public void initValues(boolean defaultValues) { 
		if (defaultValues) { 
				// TODO Initialize with default values 
		} else { 
			this.rID = null; 			this.kisiRef=null;
			this.kurumRef=null;
			this.gecerli = EvetHayir._NULL;
			this.gorev = ""; 
			this.baslangictarih = null; 
			this.bitistarih = null; 
			this.uyekurumDurum = UyekurumDurum._NULL; 
			this.varsayilan=EvetHayir._NULL;
			this.gorev=null;
		}  
	}  
	 
	@Override 
	public String getValue() {  
		StringBuilder value = new StringBuilder(""); 
			if(getKisiRef() != null) {
				value.append(KeyUtil.getLabelValue("kurumkisi.kisiRef") + " : " + getKisiRef().getUIString() + "<br />");
			} 
			if(getKurumRef() != null) {
				value.append(KeyUtil.getLabelValue("kurumkisi.kurumRef") + " : " + getKurumRef().getUIString() + "<br />");
			} 
			if(getGecerli() != null) {
				value.append(KeyUtil.getLabelValue("kurumkisi.gecerli") + " : " + getGecerli() + "");
			} 
			if(getGorev() != null) {
				value.append(KeyUtil.getLabelValue("kurumkisi.gorev") + " : " + getGorev() + "<br />");
			} 
			if(getBaslangictarih() != null) {
				value.append(KeyUtil.getLabelValue("kurumkisi.baslangictarih") + " : " + DateUtil.dateToDMYHMS(getBaslangictarih()) + "<br />");
			} 
			if(getBitistarih() != null) {
				value.append(KeyUtil.getLabelValue("kurumkisi.bitistarih") + " : " + DateUtil.dateToDMYHMS(getBitistarih()) + "<br />");
			} 
			if(getUyekurumDurum() != null) {
				value.append(KeyUtil.getLabelValue("kurumkisi.uyekurumDurum") + " : " + getUyekurumDurum() + "");
			} 
			if(getVarsayilan() != null) {
				value.append(KeyUtil.getLabelValue("kurumkisi.varsayilan") + " : " + getVarsayilan() + "");
			} 
		return value.toString(); 
	} 
	
	@Transient
	public Adres getAdresRef() {
		if (getKurumRef()!=null){
			try {
				if (getDBOperator().recordCount(Adres.class.getSimpleName(),"o.kurumRef.rID="+ getKurumRef().getRID()+
						" AND o.varsayilan=" + EvetHayir._EVET.getCode())>0){
					Adres adres=(Adres) getDBOperator().load(Adres.class.getSimpleName(),"o.kurumRef.rID="+ getKurumRef().getRID()+
							" AND o.varsayilan=" + EvetHayir._EVET.getCode(),"").get(0);
					return adres;
				}
			} catch (DBException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return null;
			
		
		
	}
	
	 
}	 
