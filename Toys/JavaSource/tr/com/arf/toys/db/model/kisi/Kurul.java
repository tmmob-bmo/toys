package tr.com.arf.toys.db.model.kisi; 
 
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import tr.com.arf.framework.utility.tool.KeyUtil;
import tr.com.arf.toys.db.enumerated.kisi.KurulTuru;
import tr.com.arf.toys.db.model.base.ToysBaseEntity;
 
@Entity 
@Table(name = "KURUL") 
public class Kurul extends ToysBaseEntity { 
 
	private static final long serialVersionUID = 1L; 
 
	@Id 
	@GeneratedValue(strategy = GenerationType.IDENTITY) 
	@Column(updatable = false, unique = true, nullable = false, name = "RID") 
	private Long rID; 
 
	@Column(name = "AD") 
	private String ad; 
 
	@Enumerated(EnumType.ORDINAL)
	@Column(name = "KURULTURU") 
	private KurulTuru kurulturu; 
 
	public Kurul() { 
		super(); 
	} 
 
	@Override
	public Long getRID() { 
		return this.rID;  
	}  
	  
	@Override
	public void setRID(Long rID) {  
		this.rID = rID;  
	}   
	
	public String getAd() {
		return ad;
	}

	public void setAd(String ad) {
		this.ad = ad;
	}

	public KurulTuru getKurulturu() {
		return kurulturu;
	}

	public void setKurulturu(KurulTuru kurulturu) {
		this.kurulturu = kurulturu;
	}

	@Override 
	public String toString() { 
		return "tr.com.arf.toys.db.model.kisi.Kurul[id=" + this.getRID()  + "]"; 
	} 
	@Override 
	public String getUIString() { 
		return getAd(); 
	} 
	 
	@Override 
	public void initValues(boolean defaultValues) { 
		if (defaultValues) { 
				// TODO Initialize with default values 
		} else { 
			this.rID = null; 			this.ad="";
			this.kurulturu=KurulTuru._NULL;		}  
	}  
	 
	@Override 
	public String getValue() {  
		StringBuilder value = new StringBuilder(""); 
			if(getAd() != null) {
				value.append(KeyUtil.getLabelValue("kurul.ad") + " : " + getAd() + "<br />");
			} 
			if(getKurulturu() != null) {
				value.append(KeyUtil.getLabelValue("kurul.kurulturu") + " : " + getKurulturu() + "");
			} 
		return value.toString(); 
	} 
	 
}	 
