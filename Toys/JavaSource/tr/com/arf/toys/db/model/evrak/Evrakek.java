package tr.com.arf.toys.db.model.evrak; 
 
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tr.com.arf.framework.utility.tool.KeyUtil;
import tr.com.arf.toys.db.model.base.ToysBaseEntity;
 
 
@Entity 
@Table(name = "EVRAKEK") 
public class Evrakek extends ToysBaseEntity { 
 
	private static final long serialVersionUID = 1L; 
 
	@Id 
	@GeneratedValue(strategy = GenerationType.IDENTITY) 
	@Column(updatable = false, unique = true, nullable = false, name = "RID") 
	private Long rID; 
 
	@ManyToOne 
	@JoinColumn(name = "EVRAKREF") 
	private Evrak evrakRef; 
 
	@Column(name = "PATH") 
	private String path; 
 
	@Column(name = "TANIM") 
	private String tanim; 
 
 
	public Evrakek() { 
		super(); 
	} 
 
	@Override
	public Long getRID() { 
		return this.rID;  
	}  
	  
	@Override
	public void setRID(Long rID) {  
		this.rID = rID;  
	}   
 
	public Evrak getEvrakRef() { 
		return evrakRef; 
	} 
	 
	public void setEvrakRef(Evrak evrakRef) { 
		this.evrakRef = evrakRef; 
	} 
 
	@Override
	public String getPath() { 
		return this.path; 
	} 
	 
	@Override
	public void setPath(String path) { 
		this.path = path; 
	} 
 
	public String getTanim() { 
		return this.tanim; 
	} 
	 
	public void setTanim(String tanim) { 
		this.tanim = tanim; 
	} 
 
 
	@Override 
	public String toString() { 
		return "tr.com.arf.toys.db.model.evrak.Evrakek[id=" + this.getRID()  + "]"; 
	} 
	@Override 
	public String getUIString() { 
		return getPath() + ""; 
	} 
	 
	@Override 
	public void initValues(boolean defaultValues) { 
		if (defaultValues) { 
				// TODO Initialize with default values 
		} else { 
			this.rID = null; 			this.evrakRef = null; 			this.path = ""; 			this.tanim = ""; 		}  
	}  
	 
	@Override 
	public String getValue() {  
		StringBuilder value = new StringBuilder("Veri : <br />"); 
			if(getEvrakRef() != null) 
				value.append(KeyUtil.getLabelValue("evrakek.evrakRef") + " : " + getEvrakRef().getUIString() + "<br />"); 
			if(getPath() != null) 
				value.append(KeyUtil.getLabelValue("evrakek.path") + " : " + getPath() + "<br />"); 
			if(getTanim() != null) 
				value.append(KeyUtil.getLabelValue("evrakek.tanim") + " : " + getTanim() + ""); 
		return value.toString(); 
	} 
	 
}	 
