package tr.com.arf.toys.db.model.system; 
 
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tr.com.arf.framework.db.model._base.BaseTreeEntity;
import tr.com.arf.framework.utility.tool.KeyUtil;
import tr.com.arf.toys.db.enumerated.system.BirimDurum;
import tr.com.arf.toys.db.enumerated.system.BirimTip;
import tr.com.arf.toys.db.enumerated.system.BirimYetkiTuru;
import tr.com.arf.toys.db.model.base.ToysBaseTreeEntity;
import tr.com.arf.toys.db.model.kisi.Kurum;
 
@Entity 
@Table(name = "BIRIM") 
public class Birim extends ToysBaseTreeEntity { 
 
	private static final long serialVersionUID = 1L; 
 
	@Id 
	@GeneratedValue(strategy = GenerationType.IDENTITY) 
	@Column(updatable = false, unique = true, nullable = false, name = "RID") 
	private Long rID; 
 
	@ManyToOne 
	@JoinColumn(name = "USTREF") 
	private Birim ustRef; 
 
	@Column(name = "AD") 
	private String ad; 
 
	@Column(name = "KISAAD") 
	private String kisaad; 
 
	@Enumerated(EnumType.ORDINAL) 
	@Column(name = "DURUM") 
	private BirimDurum durum; 
 
	@Column(name = "ERISIMKODU") 
	private String erisimKodu; 
 
	@ManyToOne 
	@JoinColumn(name = "KURUMREF") 
	private Kurum kurumRef; 
	
	@ManyToOne 
	@JoinColumn(name = "SEHIRREF") 
	private Sehir sehirRef; 
 
	@ManyToOne 
	@JoinColumn(name = "ILCEREF") 
	private Ilce ilceRef; 
 
	@Enumerated(EnumType.ORDINAL) 
	@Column(name = "BIRIMTIP") 
	private BirimTip birimtip; 
   
	public Birim() { 
		super(); 
	} 
 
	@Override
	public Long getRID() { 
		return this.rID;  
	}  
	  
	@Override
	public void setRID(Long rID) {  
		this.rID = rID;  
	}   
 
	@Override
	public Birim getUstRef() { 
		return ustRef; 
	}  

	@Override
	public void setUstRef(BaseTreeEntity ustRef) {
		this.ustRef = (Birim) ustRef;
	}  
	
	public void setUstRef(Birim ustRef) {
		this.ustRef = ustRef;
	}  
 
	@Override
	public String getAd() { 
		return this.ad; 
	} 
	 
	@Override
	public void setAd(String ad) { 
		this.ad = ad; 
	} 		
 
	public String getKisaad() { 
		return this.kisaad; 
	} 
	 
	public void setKisaad(String kisaad) { 
		this.kisaad = kisaad; 
	} 
 
	public BirimDurum getDurum() { 
		return this.durum;  
	}  
  
	public void setDurum(BirimDurum durum) {  
		this.durum = durum;  
	}   
 
	@Override
	public String getErisimKodu() { 
		return this.erisimKodu; 
	} 
	 
	@Override
	public void setErisimKodu(String erisimKodu) { 
		this.erisimKodu = erisimKodu; 
	} 
 
	public Kurum getKurumRef() {
		return kurumRef;
	}

	public void setKurumRef(Kurum kurumRef) {
		this.kurumRef = kurumRef;
	}

	public Sehir getSehirRef() { 
		return sehirRef; 
	} 
	 
	public void setSehirRef(Sehir sehirRef) { 
		this.sehirRef = sehirRef; 
	} 
 
	public Ilce getIlceRef() { 
		return ilceRef; 
	} 
	 
	public void setIlceRef(Ilce ilceRef) { 
		this.ilceRef = ilceRef; 
	} 
 
	public BirimTip getBirimtip() { 
		return this.birimtip;  
	}  
  
	public void setBirimtip(BirimTip birimtip) {  
		this.birimtip = birimtip;  
	} 
	 
	public BirimYetkiTuru getBirimYetkiTuru(){
		if(getBirimtip() == BirimTip._MERKEZ){
			return BirimYetkiTuru._TUMBIRIMLER;
		} else if(getBirimtip() == BirimTip._ODA){
			return BirimYetkiTuru._KENDIBIRIMIVEALTBIRIMLERI;
		} else {
			return BirimYetkiTuru._SADECEKENDIBIRIMLERI;
		}
	}
 
	@Override 
	public String toString() { 
		if(getAd() != null){
			return getAd();
		}
		else {
			return "";
		// return "tr.com.arf.toys.db.model.system.Birim[id=" + this.getRID()  + "]"; 
		}
	} 
	
	@Override 
	public String getUIString() { 
		return getAd();
	}
	
	public String getEvrakPdfText(){ 
		if(getDBOperator().recordCount(SistemParametre.class.getSimpleName()) > 0){
			SistemParametre param = (SistemParametre) getDBOperator().load(SistemParametre.class.getSimpleName()).get(0);
			String result = param.getOdadetayi() +"\n";
			result += param.getOdawebadresi() + " \n";			
			return result;
		} else {
			return "";
		} 
	} 
	 
	@Override 
	public void initValues(boolean defaultValues) { 
		if (defaultValues) { 
				// TODO Initialize with default values 
		} else { 
			this.rID = null; 			this.ustRef = null; 			this.ad = ""; 			this.kisaad = ""; 			this.durum = BirimDurum._NULL; 			this.erisimKodu = ""; 			this.sehirRef = null; 			this.ilceRef = null; 			this.birimtip = BirimTip._NULL;
			this.kurumRef = null; 		}  
	}  
	 
	@Override 
	public String getValue() {  
		StringBuilder value = new StringBuilder(""); 
			if(getUstRef() != null) {
				value.append(KeyUtil.getLabelValue("birim.ustRef") + " : " + getUstRef().getUIString() + "<br />");
			} 
			if(getAd() != null) {
				value.append(KeyUtil.getLabelValue("birim.ad") + " : " + getAd() + "<br />");
			} 
			if(getKisaad() != null) {
				value.append(KeyUtil.getLabelValue("birim.kisaad") + " : " + getKisaad() + "<br />");
			} 
			if(getDurum() != null) {
				value.append(KeyUtil.getLabelValue("birim.durum") + " : " + getDurum() + "<br />");
			} 
			if(getErisimKodu() != null) {
				value.append(KeyUtil.getLabelValue("birim.erisimKodu") + " : " + getErisimKodu() + "<br />");
			} 
			if(getKurumRef() != null) {
				value.append(KeyUtil.getLabelValue("birim.kurumRef") + " : " + getKurumRef().getUIString() + "<br />");
			} 
			if(getSehirRef() != null) {
				value.append(KeyUtil.getLabelValue("birim.sehirRef") + " : " + getSehirRef().getUIString() + "<br />");
			} 
			if(getIlceRef() != null) {
				value.append(KeyUtil.getLabelValue("birim.ilceRef") + " : " + getIlceRef().getUIString() + "<br />");
			} 
			if(getBirimtip() != null) {
				value.append(KeyUtil.getLabelValue("birim.birimtip") + " : " + getBirimtip() + "");
			} 
		return value.toString(); 
	}
	 
}	 
