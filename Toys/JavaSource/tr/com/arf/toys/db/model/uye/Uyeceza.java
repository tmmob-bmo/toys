package tr.com.arf.toys.db.model.uye; 
 
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import tr.com.arf.framework.utility.tool.KeyUtil;
import tr.com.arf.toys.db.model.base.ToysBaseEntity;
import tr.com.arf.toys.db.model.evrak.Evrak;
import tr.com.arf.toys.db.model.system.Cezatip;
 
 
@Entity 
@Table(name = "UYECEZA") 
public class Uyeceza extends ToysBaseEntity { 
 
	private static final long serialVersionUID = 1L; 
 
	@Id 
	@GeneratedValue(strategy = GenerationType.IDENTITY) 
	@Column(updatable = false, unique = true, nullable = false, name = "RID") 
	private Long rID; 
 
	@ManyToOne 
	@JoinColumn(name = "UYEREF") 
	private Uye uyeRef; 
 
	@ManyToOne 
	@JoinColumn(name = "CEZATIPREF") 
	private Cezatip cezatipRef; 
 
	@ManyToOne 
	@JoinColumn(name = "EVRAKREF") 
	private Evrak evrakRef; 
 
	@Temporal(TemporalType.DATE) 
	@Column(name = "BASLANGICTARIH") 
	private java.util.Date baslangictarih; 
 
	@Temporal(TemporalType.DATE) 
	@Column(name = "BITISTARIH") 
	private java.util.Date bitistarih; 
 
	@Column(name = "ACIKLAMA") 
	private String aciklama; 
 
 
	public Uyeceza() { 
		super(); 
	} 
 
	@Override
	public Long getRID() { 
		return this.rID;  
	}  
	  
	@Override
	public void setRID(Long rID) {  
		this.rID = rID;  
	}   
 
	public Uye getUyeRef() { 
		return uyeRef; 
	} 
	 
	public void setUyeRef(Uye uyeRef) { 
		this.uyeRef = uyeRef; 
	} 
 
	public Cezatip getCezatipRef() { 
		return cezatipRef; 
	} 
	 
	public void setCezatipRef(Cezatip cezatipRef) { 
		this.cezatipRef = cezatipRef; 
	} 
 
	public Evrak getEvrakRef() { 
		return evrakRef; 
	} 
	 
	public void setEvrakRef(Evrak evrakRef) { 
		this.evrakRef = evrakRef; 
	} 
 
	public java.util.Date getBaslangictarih() { 
		return baslangictarih; 
	} 
	 
	public void setBaslangictarih(java.util.Date baslangictarih) { 
		this.baslangictarih = baslangictarih; 
	} 
 
	public java.util.Date getBitistarih() { 
		return bitistarih; 
	} 
	 
	public void setBitistarih(java.util.Date bitistarih) { 
		this.bitistarih = bitistarih; 
	} 
 
	public String getAciklama() { 
		return this.aciklama; 
	} 
	 
	public void setAciklama(String aciklama) { 
		this.aciklama = aciklama; 
	} 
 
 
	@Override 
	public String toString() { 
		return "tr.com.arf.toys.db.model.uye.Uyeceza[id=" + this.getRID()  + "]"; 
	} 
	@Override 
	public String getUIString() { 
		return KeyUtil.getLabelValue("uyeceza.uyeRef") + " : " + getUyeRef().getUIString() + ", " + KeyUtil.getLabelValue("uyeceza.cezatipRef") + " : " +  getCezatipRef().getAd(); 
	} 
	 
	@Override 
	public void initValues(boolean defaultValues) { 
		if (defaultValues) { 
				// TODO Initialize with default values 
		} else { 
			this.rID = null; 			this.uyeRef = null; 			this.cezatipRef = null; 			this.evrakRef = null; 			this.baslangictarih = null; 			this.bitistarih = null; 			this.aciklama = ""; 		}  
	}  
	 
	@Override 
	public String getValue() {  
		StringBuilder value = new StringBuilder(""); 
			if(getUyeRef() != null) 
				value.append(KeyUtil.getLabelValue("uyeceza.uyeRef") + " : " + getUyeRef().getUIString() + "<br />"); 
			if(getCezatipRef() != null) 
				value.append(KeyUtil.getLabelValue("uyeceza.cezatipRef") + " : " + getCezatipRef().getUIString() + "<br />"); 
			if(getEvrakRef() != null) 
				value.append(KeyUtil.getLabelValue("uyeceza.evrakRef") + " : " + getEvrakRef().getUIString() + "<br />"); 
			if(getBaslangictarih() != null) 
				value.append(KeyUtil.getLabelValue("uyeceza.baslangictarih") + " : " + getBaslangictarih() + "<br />"); 
			if(getBitistarih() != null) 
				value.append(KeyUtil.getLabelValue("uyeceza.bitistarih") + " : " + getBitistarih() + "<br />"); 
			if(getAciklama() != null) 
				value.append(KeyUtil.getLabelValue("uyeceza.aciklama") + " : " + getAciklama() + ""); 
		return value.toString(); 
	} 
	 
}	 
