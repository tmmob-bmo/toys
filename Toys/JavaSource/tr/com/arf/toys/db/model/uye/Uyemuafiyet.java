package tr.com.arf.toys.db.model.uye; 
 
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import tr.com.arf.framework.utility.tool.KeyUtil;
import tr.com.arf.toys.db.enumerated.uye.UyeMuafiyetTip;
import tr.com.arf.toys.db.model.base.ToysBaseEntity;
import tr.com.arf.toys.db.model.evrak.Evrak;
 
@Entity 
@Table(name = "UYEMUAFIYET") 
public class Uyemuafiyet extends ToysBaseEntity { 
 
	private static final long serialVersionUID = 1L; 
 
	@Id 
	@GeneratedValue(strategy = GenerationType.IDENTITY) 
	@Column(updatable = false, unique = true, nullable = false, name = "RID") 
	private Long rID; 
 
	@ManyToOne 
	@JoinColumn(name = "UYEREF") 
	private Uye uyeRef; 
 
 
	@Enumerated(EnumType.ORDINAL) 
	@Column(name = "MUAFIYETTIP") 
	private UyeMuafiyetTip muafiyettip; 
  
	@ManyToOne 
	@JoinColumn(name = "EVRAKREF") 
	private Evrak evrakRef; 
 
	@Column(name = "ACIKLAMA") 
	private String aciklama; 
 
	@Temporal(TemporalType.DATE) 
	@Column(name = "BASLANGICTARIH") 
	private java.util.Date baslangictarih; 
 
	@Temporal(TemporalType.DATE) 
	@Column(name = "BITISTARIH") 
	private java.util.Date bitistarih; 
 
	public Uyemuafiyet() { 
		super(); 
	} 
 
	@Override
	public Long getRID() { 
		return this.rID;  
	}  
	  
	@Override
	public void setRID(Long rID) {  
		this.rID = rID;  
	}   
 
	public Uye getUyeRef() { 
		return uyeRef; 
	} 
	 
	public void setUyeRef(Uye uyeRef) { 
		this.uyeRef = uyeRef; 
	}  
 
	public UyeMuafiyetTip getMuafiyettip() { 
		return this.muafiyettip;  
	}  
  
	public void setMuafiyettip(UyeMuafiyetTip muafiyettip) {  
		this.muafiyettip = muafiyettip;  
	}   
 
	public Evrak getEvrakRef() { 
		return this.evrakRef; 
	} 
	 
	public void setEvrakRef(Evrak evrakRef) { 
		this.evrakRef = evrakRef; 
	} 
 
	public String getAciklama() { 
		return this.aciklama; 
	} 
	 
	public void setAciklama(String aciklama) { 
		this.aciklama = aciklama; 
	}  
 
	public java.util.Date getBaslangictarih() { 
		return baslangictarih; 
	} 
	 
	public void setBaslangictarih(java.util.Date baslangictarih) { 
		this.baslangictarih = baslangictarih; 
	} 
 
	public java.util.Date getBitistarih() { 
		return bitistarih; 
	} 
	 
	public void setBitistarih(java.util.Date bitistarih) { 
		this.bitistarih = bitistarih; 
	} 
	
	@Override 
	public String toString() { 
		return "tr.com.arf.toys.db.model.uye.Uyemuafiyet[id=" + this.getRID()  + "]"; 
	} 
	@Override 
	public String getUIString() { 
		return getMuafiyettip().getLabel() + ""; 
	} 
	 
	@Override 
	public void initValues(boolean defaultValues) { 
		if (defaultValues) { 
				// TODO Initialize with default values 
		} else { 
			this.rID = null; 			this.uyeRef = null;  			this.muafiyettip = UyeMuafiyetTip._NULL; 			this.evrakRef = null; 			this.aciklama = ""; 
			this.baslangictarih = null; 
			this.bitistarih = null; 		}  
	}  
	 
	@Override 
	public String getValue() {  
		StringBuilder value = new StringBuilder(""); 
			if(getUyeRef() != null) {
				value.append(KeyUtil.getLabelValue("uyemuafiyet.uyeRef") + " : " + getUyeRef().getUIString() + "<br />");
			}  
			if(getMuafiyettip() != null) {
				value.append(KeyUtil.getLabelValue("uyemuafiyet.muafiyettip") + " : " + getMuafiyettip() + "<br />");
			} 
			if(getEvrakRef() != null) {
				value.append(KeyUtil.getLabelValue("uyemuafiyet.evrakRef") + " : " + getEvrakRef().getUIString() + "<br />");
			} 
			if(getBaslangictarih() != null) {
				value.append(KeyUtil.getLabelValue("uyemuafiyet.baslangictarih") + " : " + getBaslangictarih() + "<br />");
			} 
			if(getBitistarih() != null) {
				value.append(KeyUtil.getLabelValue("uyemuafiyet.bitistarih") + " : " + getBitistarih() + "<br />");
			} 
			if(getAciklama() != null) {
				value.append(KeyUtil.getLabelValue("uyemuafiyet.aciklama") + " : " + getAciklama() + "");
			} 
		return value.toString(); 
	} 
	 
}	 
