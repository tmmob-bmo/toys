package tr.com.arf.toys.db.model.kisi; 
 
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tr.com.arf.framework.utility.tool.KeyUtil;
import tr.com.arf.toys.db.model.base.ToysBaseEntity;
import tr.com.arf.toys.db.model.uye.Uye;
 
 
@Entity 
@Table(name = "KURULDONEMDELEGE") 
public class KurulDonemDelege extends ToysBaseEntity { 
 
	private static final long serialVersionUID = 1L; 
 
	@Id 
	@GeneratedValue(strategy = GenerationType.IDENTITY) 
	@Column(updatable = false, unique = true, nullable = false, name = "RID") 
	private Long rID; 
 
	@ManyToOne 
	@JoinColumn(name = "KURULDONEMREF") 
	private KurulDonem kurulDonemRef; 
 
	@ManyToOne 
	@JoinColumn(name = "UYEREF") 
	private Uye uyeRef; 
 
 
	public KurulDonemDelege() { 
		super(); 
	} 
 
	@Override
	public Long getRID() { 
		return this.rID;  
	}  
	  
	@Override
	public void setRID(Long rID) {  
		this.rID = rID;  
	}   
 
	public KurulDonem getKurulDonemRef() { 
		return kurulDonemRef; 
	} 
	 
	public void setKurulDonemRef(KurulDonem kurulDonemRef) { 
		this.kurulDonemRef = kurulDonemRef; 
	} 
 
	public Uye getUyeRef() { 
		return uyeRef; 
	} 
	 
	public void setUyeRef(Uye uyeRef) { 
		this.uyeRef = uyeRef; 
	} 
 
 
	@Override 
	public String toString() { 
		return "tr.com.arf.toys.db.model.kisi.KurulDonemDelege[id=" + this.getRID()  + "]"; 
	} 
	@Override 
	public String getUIString() { 
		return getKurulDonemRef() + ""; 
	} 
	 
	@Override 
	public void initValues(boolean defaultValues) { 
			this.rID = null; 			this.kurulDonemRef = null; 			this.uyeRef = null; 	}  
	 
	@Override 
	public String getValue() {  
		StringBuilder value = new StringBuilder("Veri : <br />"); 
			if(getKurulDonemRef() != null) {
				value.append(KeyUtil.getLabelValue("kurulDonemDelege.kurulDonemRef") + " : " + getKurulDonemRef().getUIString() + "<br />");
			} 
			if(getUyeRef() != null) {
				value.append(KeyUtil.getLabelValue("kurulDonemDelege.uyeRef") + " : " + getUyeRef().getUIString() + "");
			} 
		return value.toString(); 
	} 
	 
}	 
