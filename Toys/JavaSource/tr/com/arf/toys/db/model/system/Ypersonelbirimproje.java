package tr.com.arf.toys.db.model.system;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import tr.com.arf.framework.utility.tool.DateUtil;
import tr.com.arf.framework.utility.tool.KeyUtil;
import tr.com.arf.toys.db.enumerated.system.YpersonelProjeDurum;
import tr.com.arf.toys.db.model.base.ToysBaseEntity;

@Entity
@Table(name = "YPERSONELBIRIMPROJE")
public class Ypersonelbirimproje extends ToysBaseEntity{
	/**
	 * 
	 */
	private static final long serialVersionUID = -1948780941656871374L;

	@Id 
	@GeneratedValue(strategy = GenerationType.IDENTITY) 
	@Column(updatable = false, unique = true, nullable = false, name = "RID") 
	private Long rID; 
 
	@ManyToOne 
	@JoinColumn(name = "PERSONELREF") 
	private Ypersonel personelRef; 
 
	@ManyToOne 
	@JoinColumn(name = "PROJEREF") 
	private Yproje projeRef; 
 
	@ManyToOne 
	@JoinColumn(name = "BIRIMREF") 
	private Ybirim birimRef; 
 
	@Temporal(TemporalType.DATE) 
	@Column(name = "BASLANGICTARIH") 
	private java.util.Date baslangictarih; 
 
	@Temporal(TemporalType.DATE) 
	@Column(name = "BITISTARIH") 
	private java.util.Date bitistarih; 
 
	@Column(name = "ACIKLAMA") 
	private String aciklama; 
 
	@Enumerated(EnumType.ORDINAL) 
	@Column(name = "DURUM") 
	private YpersonelProjeDurum personelProjeDurum; 
 
 
	
	@Override
	public Long getRID() { 
		return this.rID;  
	}  
	  
	@Override
	public void setRID(Long rID) {  
		this.rID = rID;  
	}   
 
	public Ypersonel getPersonelRef() { 
		return personelRef; 
	} 
	 
	public void setPersonelRef(Ypersonel personelRef) { 
		this.personelRef = personelRef; 
	} 
 
	public Yproje getprojeRef() { 
		return projeRef; 
	} 
	 
	public void setprojeRef(Yproje projeRef) { 
		this.projeRef = projeRef; 
	} 
 
	public Ybirim getBirimRef() { 
		return birimRef; 
	} 
	 
	public void setBirimRef(Ybirim birimRef) { 
		this.birimRef = birimRef; 
	} 
 
	public java.util.Date getBaslangictarih() { 
		return baslangictarih; 
	} 
	 
	public void setBaslangictarih(java.util.Date baslangictarih) { 
		this.baslangictarih = baslangictarih; 
	} 
 
	public java.util.Date getBitistarih() { 
		return bitistarih; 
	} 
	 
	public void setBitistarih(java.util.Date bitistarih) { 
		this.bitistarih = bitistarih; 
	} 
 
	public String getAciklama() { 
		return this.aciklama; 
	} 
	 
	public void setAciklama(String aciklama) { 
		this.aciklama = aciklama; 
	} 
 
	public YpersonelProjeDurum getpersonelProjeDurum() { 
		return this.personelProjeDurum;  
	}  
  
	public void setDurum(YpersonelProjeDurum personelProjeDurum) {  
		this.personelProjeDurum = personelProjeDurum;  
	}   
 
 
	@Override 
	public String toString() { 
		return "tr.com.arf.toys.db.model.system.Ypersonelbirimproje[id=" + this.getRID()  + "]"; 
	} 
	@Override 
	public String getUIString() { 
		return getAciklama() + ""; 
	} 
	 
	@Override 
	public void initValues(boolean defaultValues) { 
		if (defaultValues) { 
				// TODO Initialize with default values 
		} else { 
			this.rID = null; 
			this.personelRef = null; 
			this.projeRef = null; 
			this.birimRef = null; 
			this.baslangictarih = null; 
			this.bitistarih = null; 
			this.aciklama = ""; 
			this.personelProjeDurum = YpersonelProjeDurum._NULL; 
		}  
	}  
	 
	@Override 
	public String getValue() {  
		StringBuilder value = new StringBuilder(""); 
			if(getPersonelRef() != null) {
				value.append(KeyUtil.getLabelValue("ypersonelbirimproje.personelRef") + " : " + getPersonelRef().getUIString() + "<br />");
			} 
			if(getprojeRef() != null) {
				value.append(KeyUtil.getLabelValue("ypersonelbirimproje.projeRef") + " : " + getprojeRef().getUIString() + "<br />");
			} 
			if(getBirimRef() != null) {
				value.append(KeyUtil.getLabelValue("ypersonelbirimproje.birimRef") + " : " + getBirimRef().getUIString() + "<br />");
			} 
			if(getBaslangictarih() != null) {
				value.append(KeyUtil.getLabelValue("ypersonelbirimproje.baslangictarih") + " : " + DateUtil.dateToDMYHMS(getBaslangictarih()) + "<br />");
			} 
			if(getBitistarih() != null) {
				value.append(KeyUtil.getLabelValue("ypersonelbirimproje.bitistarih") + " : " + DateUtil.dateToDMYHMS(getBitistarih()) + "<br />");
			} 
			if(getAciklama() != null) {
				value.append(KeyUtil.getLabelValue("ypersonelbirimproje.aciklama") + " : " + getAciklama() + "<br />");
			} 
			if(getpersonelProjeDurum() != null) {
				value.append(KeyUtil.getLabelValue("ypersonelbirimproje.durum") + " : " + getpersonelProjeDurum() + "");
			} 
		return value.toString(); 
	} 
	 
	
}
