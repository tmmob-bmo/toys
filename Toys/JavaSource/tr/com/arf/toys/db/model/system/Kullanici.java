package tr.com.arf.toys.db.model.system;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import tr.com.arf.framework.utility.tool.KeyUtil;
import tr.com.arf.toys.db.model.base.ToysBaseEntity;
import tr.com.arf.toys.db.model.kisi.Kisi;

@Entity
@Table(name = "KULLANICI")
public class Kullanici extends ToysBaseEntity {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(updatable = false, unique = true, nullable = false, name = "RID")
	private Long rID;

	@ManyToOne
	@JoinColumn(name = "KISIREF")
	private Kisi kisiRef;

	@Column(name = "NAME")
	private String name;

	@Column(name = "PASSWORD")
	private String password;

	@Column(name = "THEME")
	private String theme;

    @Column(name = "LOGIN_ATTEMPT_COUNT")
    private int loginAttemptCount;

	@Transient
	private String prevPassword;

	@Transient
	private String newPassword;

	@Transient
	private String newPasswordCheck;

	public Kullanici() {
		super();
	}

	@Override
	public Long getRID() {
		return this.rID;
	}

	@Override
	public void setRID(Long rID) {
		this.rID = rID;
	}

	public Kisi getKisiRef() {
		return kisiRef;
	}

	public void setKisiRef(Kisi kisiRef) {
		this.kisiRef = kisiRef;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return password;
	}
//
//	@Transient
//	public String getPasswordText() {
//		StringBuffer result = new StringBuffer();
//		result.append(password.substring(0, 1));
//		for (int x = 0; x < password.length(); x++) {
//			result.append("*");
//		}
//		result.append(password.substring(password.length() - 1));
//		return result.toString();
//	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getTheme() {
		return theme;
	}

	public void setTheme(String theme) {
		this.theme = theme;
	}

	public String getPrevPassword() {
		return prevPassword;
	}

	public void setPrevPassword(String prevPassword) {
		this.prevPassword = prevPassword;
	}

	public String getNewPassword() {
		return newPassword;
	}

	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}

	public String getNewPasswordCheck() {
		return newPasswordCheck;
	}

	public void setNewPasswordCheck(String newPasswordCheck) {
		this.newPasswordCheck = newPasswordCheck;
	}

    public int getLoginAttemptCount() {
        return loginAttemptCount;
    }

    public void setLoginAttemptCount(int loginAttemptCount) {
        this.loginAttemptCount = loginAttemptCount;
    }

	@Override
	public String toString() {
		return getUIString();
	}

	@Override
	public String getUIString() {
		return getName() + " (" + getKisiRef().getUIStringShort() + ")";
	}

	public String getUIString2() {
		String temp = getName() + " (" + getKisiRef().getUIStringShort() + ")";
		if (temp.length() < 40) {
			return temp;
		} else {
			return temp.substring(0, 37) + "...";
		}
	}

	@Override
	public void initValues(boolean defaultValues) {
		if (defaultValues) {
			// TODO Initialize with default values
		} else {
			this.rID = null;
			this.kisiRef = null;
			this.name = null;
			this.password = null;
            this.loginAttemptCount = 0;
			this.theme = "bootstrap";
		}
	}

	@Override
	public String getValue() {
		StringBuilder value = new StringBuilder("");
		if (getKisiRef() != null) {
			value.append(KeyUtil.getLabelValue("kullanici.kisiRef") + " : " + getKisiRef().getUIString() + "");
		}
		if (getName() != null) {
			value.append(KeyUtil.getLabelValue("kullanici.name") + " : " + getName() + "<br />");
		}
		if (getPassword() != null) {
			value.append(KeyUtil.getLabelValue("kullanici.password") + " : " + getPassword() + "");
		}
		return value.toString();
	}

}