package tr.com.arf.toys.db.model.iletisim; 
 
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import tr.com.arf.framework.db.model._base.BaseTreeEntity;
import tr.com.arf.framework.utility.tool.KeyUtil;
import tr.com.arf.toys.db.enumerated.iletisim.ListeGizlilikTuru;
import tr.com.arf.toys.db.enumerated.iletisim.ReferansTipi;
import tr.com.arf.toys.db.model.base.ToysBaseTreeEntity;
import tr.com.arf.toys.db.model.system.Birim;
import tr.com.arf.toys.service.application.ApplicationDescriptor;
 
@Entity 
@Table(name = "ILETISIMLISTE") 
public class Iletisimliste extends ToysBaseTreeEntity { 
 
	private static final long serialVersionUID = 1L; 
 
	@Id 
	@GeneratedValue(strategy = GenerationType.IDENTITY) 
	@Column(updatable = false, unique = true, nullable = false, name = "RID") 
	private Long rID; 
 
	@Column(name = "AD") 
	private String ad; 
 
	@ManyToOne 
	@JoinColumn(name = "USTREF") 
	private Iletisimliste ustRef; 
 
	@Column(name = "ERISIMKODU") 
	private String erisimKodu; 
 
	@ManyToOne 
	@JoinColumn(name = "BIRIMREF") 
	private Birim birimRef; 
 
	@Enumerated(EnumType.ORDINAL) 
	@Column(name = "LISTEGIZLILIKTURU") 
	private ListeGizlilikTuru listegizlilikturu; 
 
	@Column(name = "ACIKLAMA") 
	private String aciklama; 
 
 
	public Iletisimliste() { 
		super(); 
	} 
 
	@Override
	public Long getRID() { 
		return this.rID;  
	}  
	  
	@Override
	public void setRID(Long rID) {  
		this.rID = rID;  
	}   
 
	@Override
	public String getAd() { 
		return this.ad; 
	} 
	 
	@Override
	public void setAd(String ad) { 
		this.ad = ad; 
	} 
 
	@Override 
	public Iletisimliste getUstRef() { 
		return ustRef; 
	} 
	 
	@Override 
	public void setUstRef(BaseTreeEntity ustRef) {  
		if(getUstRef() == null) {
			this.ustRef = (Iletisimliste) ustRef;
		} 
	} 
	 
	public void setUstRef(Iletisimliste ustRef) { 
		this.ustRef = ustRef; 
	} 
 
	@Override
	public String getErisimKodu() { 
		return this.erisimKodu; 
	} 
	 
	@Override
	public void setErisimKodu(String erisimKodu) { 
		this.erisimKodu = erisimKodu; 
	} 
 
	public Birim getBirimRef() { 
		return birimRef; 
	} 
	 
	public void setBirimRef(Birim birimRef) { 
		this.birimRef = birimRef; 
	} 
 
	public ListeGizlilikTuru getListegizlilikturu() { 
		return this.listegizlilikturu;  
	}  
  
	public void setListegizlilikturu(ListeGizlilikTuru listegizlilikturu) {  
		this.listegizlilikturu = listegizlilikturu;  
	}   
 
	public String getAciklama() { 
		return this.aciklama; 
	} 
	 
	public void setAciklama(String aciklama) { 
		this.aciklama = aciklama; 
	} 
	
	@SuppressWarnings({ "unchecked" })
	@Transient
	public int getIletisimKisiSayisi(){
		int kisisayisi=0;
		if(getRID() == null) {
			return 0;
		} else {
			try {
				if(getDBOperator().recordCount(Iletisimlistedetay.class.getSimpleName(), "o.iletisimlisteRef.rID=" + getRID()) > 0){
					List <Iletisimlistedetay> iletisimList =getDBOperator().load(Iletisimlistedetay.class.getSimpleName(), "o.iletisimlisteRef.rID=" + getRID(),"");
					for (Iletisimlistedetay entity : iletisimList){
						if (entity.getReferansTipi()==ReferansTipi._KISI){
							kisisayisi++;
						}
						if (entity.getReferansTipi() == ReferansTipi._UYE){
							kisisayisi++;
						}
						if (entity.getReferansTipi()== ReferansTipi._KURUM){
							kisisayisi++;
						}
						if (entity.getReferansTipi()== ReferansTipi._EGITMEN){
							kisisayisi++;
						}
						if (entity.getReferansTipi()== ReferansTipi._ISYERITEMSILCISI){
							kisisayisi++;
						}
						if (entity.getReferansTipi()== ReferansTipi._BIRIM){
							kisisayisi+=getDBOperator().recordCount(ApplicationDescriptor._UYE_MODEL,"o.birimRef.rID="+ entity.getReferans());
						}
						if(entity.getReferansTipi()== ReferansTipi._KURUL){
							kisisayisi+=getDBOperator().recordCount(ApplicationDescriptor._KURULDONEMUYE_MODEL,"o.kurulDonemRef.rID=" + entity.getReferans());
						}
						if(entity.getReferansTipi()== ReferansTipi._KOMISYON){
							kisisayisi+=getDBOperator().recordCount(ApplicationDescriptor._KOMISYONDONEMUYE_MODEL,"o.komisyonDonemRef.rID=" + entity.getReferans());
						}
						if(entity.getReferansTipi()== ReferansTipi._ETKINLIKKATILIMCI){
							kisisayisi+=getDBOperator().recordCount(ApplicationDescriptor._ETKINLIKKATILIMCI_MODEL,"o.etkinlikRef.rID=" + entity.getReferans());
						}
						if(entity.getReferansTipi()== ReferansTipi._EGITIMKATILIMCI){
							kisisayisi+=getDBOperator().recordCount(ApplicationDescriptor._EGITIMKATILIMCI_MODEL,"o.egitimRef.rID=" + entity.getReferans());
						}
					}
					return kisisayisi;
				}
			} catch (Exception e) { 
				e.printStackTrace();
				return kisisayisi;
			}			
		}
		return kisisayisi;
	}
 
 
	@Override 
	public String toString() { 
		return this.getAd();
	} 
	@Override 
	public String getUIString() { 
		String result = getAd() + ""; 
	/*	if (getUstRef() != null) { 
			result += " / " + getUstRef().getAd() + "" ; 
		} */
		return result; 
	} 
	 
	@Override 
	public void initValues(boolean defaultValues) { 
			this.rID = null; 			this.ad = ""; 			this.ustRef = null; 			this.erisimKodu = ""; 			this.birimRef = null; 			this.listegizlilikturu = ListeGizlilikTuru._NULL; 			this.aciklama = ""; 	}  
	 
	@Override 
	public String getValue() {  
		StringBuilder value = new StringBuilder("Veri : <br />"); 
			if(getAd() != null) {
				value.append(KeyUtil.getLabelValue("iletisimliste.ad") + " : " + getAd() + "<br />");
			} 
			if(getUstRef() != null) {
				value.append(KeyUtil.getLabelValue("iletisimliste.ustRef") + " : " + getUstRef().getUIString() + "<br />");
			} 
			if(getErisimKodu() != null) {
				value.append(KeyUtil.getLabelValue("iletisimliste.erisimKodu") + " : " + getErisimKodu() + "<br />");
			} 
			if(getBirimRef() != null) {
				value.append(KeyUtil.getLabelValue("iletisimliste.birimRef") + " : " + getBirimRef().getUIString() + "<br />");
			} 
			if(getListegizlilikturu() != null) {
				value.append(KeyUtil.getLabelValue("iletisimliste.listegizlilikturu") + " : " + getListegizlilikturu().getLabel() + "<br />");
			} 
			if(getAciklama() != null) {
				value.append(KeyUtil.getLabelValue("iletisimliste.aciklama") + " : " + getAciklama() + "");
			} 
		return value.toString(); 
	} 
	 
}	 
