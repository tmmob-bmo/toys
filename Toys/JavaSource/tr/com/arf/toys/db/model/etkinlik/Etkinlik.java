package tr.com.arf.toys.db.model.etkinlik; 
 
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import tr.com.arf.framework.utility.tool.DateUtil;
import tr.com.arf.framework.utility.tool.KeyUtil;
import tr.com.arf.toys.db.enumerated.etkinlik.EtkinlikDurum;
import tr.com.arf.toys.db.model.base.ToysBaseEntity;
import tr.com.arf.toys.db.model.kisi.Donem;
import tr.com.arf.toys.db.model.kisi.Kisi;
import tr.com.arf.toys.db.model.system.Birim;
import tr.com.arf.toys.db.model.system.Ilce;
import tr.com.arf.toys.db.model.system.Sehir;
 
@Entity 
@Table(name = "ETKINLIK") 
public class Etkinlik extends ToysBaseEntity { 
 
	private static final long serialVersionUID = 1L; 
 
	@Id 
	@GeneratedValue(strategy = GenerationType.IDENTITY) 
	@Column(updatable = false, unique = true, nullable = false, name = "RID") 
	private Long rID; 
 
	@ManyToOne 
	@JoinColumn(name = "ETKINLIKTANIMREF") 
	private Etkinliktanim etkinliktanimRef; 
 
	@ManyToOne 
	@JoinColumn(name = "BIRIMREF") 
	private Birim birimRef; 
	
	@ManyToOne 
	@JoinColumn(name = "DONEMREF") 
	private Donem donemRef;
	
	@Column(name = "AD") 
	private String ad; 
 
	@Temporal(TemporalType.DATE) 
	@Column(name = "BASLAMATARIH") 
	private java.util.Date baslamatarih; 
 
	@Temporal(TemporalType.DATE) 
	@Column(name = "BITISTARIH") 
	private java.util.Date bitistarih;
	
	@ManyToOne 
	@JoinColumn(name = "SEHIRREF") 
	private Sehir sehirRef; 
 
	@ManyToOne 
	@JoinColumn(name = "ILCEREF") 
	private Ilce ilceRef; 
 
	@Column(name = "YER") 
	private String yer; 
 
	@Column(name = "PROGRAM") 
	private String program; 

	@Column(name = "WEBADRESI") 
	private String webadresi; 
 
	@Column(name = "EPOSTAADRESI") 
	private String epostaadresi; 
 
	@Column(name = "TELEFONNO") 
	private String telefonno; 
 
	@ManyToOne 
	@JoinColumn(name = "SORUMLUKISIREF") 
	private Kisi sorumlukisiRef; 
 
	@Transient
	private Kisi sorumlukisiRefPrev;
	
	@Enumerated(EnumType.ORDINAL) 
	@Column(name = "DURUM") 
	private EtkinlikDurum durum; 
 
 
	public Etkinlik() { 
		super(); 
	} 
 
	@Override
	public Long getRID() { 
		return this.rID;  
	}  
	  
	@Override
	public void setRID(Long rID) {  
		this.rID = rID;  
	}   
 
	public Etkinliktanim getEtkinliktanimRef() { 
		return etkinliktanimRef; 
	} 
	 
	public void setEtkinliktanimRef(Etkinliktanim etkinliktanimRef) { 
		this.etkinliktanimRef = etkinliktanimRef; 
	} 
 
	public String getAd() { 
		return this.ad; 
	} 
	 
	public void setAd(String ad) { 
		this.ad = ad; 
	} 
 
	public java.util.Date getBaslamatarih() { 
		return baslamatarih; 
	} 
	 
	public void setBaslamatarih(java.util.Date baslamatarih) { 
		this.baslamatarih = baslamatarih; 
	}
	
	public java.util.Date getBitistarih() { 
		return bitistarih; 
	} 
	
	public void setBitistarih(java.util.Date bitistarih) { 
		this.bitistarih = bitistarih; 
	} 
 
	public Sehir getSehirRef() { 
		return sehirRef; 
	} 
	 
	public void setSehirRef(Sehir sehirRef) { 
		this.sehirRef = sehirRef; 
	} 
 
	public Birim getBirimRef() { 
		return birimRef; 
	} 
	
	public void setBirimRef(Birim birimRef) { 
		this.birimRef = birimRef; 
	}
	
	public Donem getDonemRef() { 
		return donemRef; 
	} 
	
	public void setDonemRef(Donem donemRef) { 
		this.donemRef = donemRef; 
	}
	
	public Ilce getIlceRef() { 
		return ilceRef; 
	} 
	 
	public void setIlceRef(Ilce ilceRef) { 
		this.ilceRef = ilceRef; 
	} 
 
	public String getYer() { 
		return this.yer; 
	} 
	 
	public void setYer(String yer) { 
		this.yer = yer; 
	} 
 
	public String getProgram() { 
		return this.program; 
	} 
	
	public void setProgram(String program) { 
		this.program = program; 
	}
	
	public String getWebadresi() { 
		return this.webadresi; 
	} 
	 
	public void setWebadresi(String webadresi) { 
		this.webadresi = webadresi; 
	} 
 
	public String getEpostaadresi() { 
		return this.epostaadresi; 
	} 
	 
	public void setEpostaadresi(String epostaadresi) { 
		this.epostaadresi = epostaadresi; 
	} 
 
	public String getTelefonno() { 
		return this.telefonno; 
	} 
	 
	public void setTelefonno(String telefonno) { 
		this.telefonno = telefonno; 
	} 
 
	public Kisi getSorumlukisiRef() { 
		return sorumlukisiRef; 
	} 
	 
	public void setSorumlukisiRef(Kisi sorumlukisiRef) { 
		this.sorumlukisiRef = sorumlukisiRef; 
	} 	
 
	public Kisi getSorumlukisiRefPrev() {
		return sorumlukisiRefPrev;
	}

	public void setSorumlukisiRefPrev(Kisi sorumlukisiRefPrev) {
		this.sorumlukisiRefPrev = sorumlukisiRefPrev;
	}

	public EtkinlikDurum getDurum() { 
		return this.durum;  
	}  
  
	public void setDurum(EtkinlikDurum durum) {  
		this.durum = durum;  
	}    
 
	@Override 
	public String toString() { 
		return etkinliktanimRef.getAd(); 
	} 
	@Override 
	public String getUIString() { 
		return getAd() + ""; 
	} 
	
	public boolean getDurumKontrol(){
		if(getDurum() == EtkinlikDurum._PLANLANMIS) {
			return false;
		}
		return true;
	}
	 
	@Override 
	public void initValues(boolean defaultValues) { 
			this.rID = null; 			this.etkinliktanimRef = null; 			this.ad = ""; 			this.baslamatarih = null; 			this.bitistarih = null; 			this.sehirRef = null; 			this.ilceRef = null; 			this.yer = ""; 			this.webadresi = ""; 			this.epostaadresi = ""; 			this.telefonno = ""; 			this.sorumlukisiRef = null; 			this.durum = EtkinlikDurum._NULL; 
			this.birimRef = null;			this.donemRef = null;
			this.program = null;	}  
	 
	@Override 
	public String getValue() {  
		StringBuilder value = new StringBuilder("Veri : <br />"); 
			if(getEtkinliktanimRef() != null) {
				value.append(KeyUtil.getLabelValue("etkinlik.etkinliktanimRef") + " : " + getEtkinliktanimRef().getUIString() + "<br />");
			} 
			if(getAd() != null) {
				value.append(KeyUtil.getLabelValue("etkinlik.ad") + " : " + getAd() + "<br />");
			} 
			if(getBaslamatarih() != null) {
				value.append(KeyUtil.getLabelValue("etkinlik.baslamatarih") + " : " + DateUtil.dateToDMYHMS(getBaslamatarih()) + "<br />");
			} 
			if(getBitistarih() != null) {
				value.append(KeyUtil.getLabelValue("etkinlik.bitistarih") + " : " + DateUtil.dateToDMYHMS(getBitistarih()) + "<br />");
			} 
			if(getSehirRef() != null) {
				value.append(KeyUtil.getLabelValue("etkinlik.sehirRef") + " : " + getSehirRef().getUIString() + "<br />");
			} 
			if(getIlceRef() != null) {
				value.append(KeyUtil.getLabelValue("etkinlik.ilceRef") + " : " + getIlceRef().getUIString() + "<br />");
			} 
			if(getYer() != null) {
				value.append(KeyUtil.getLabelValue("etkinlik.yer") + " : " + getYer() + "<br />");
			} 
			if(getProgram() != null) {
				value.append(KeyUtil.getLabelValue("etkinlik.program") + " : " + getProgram() + "<br />");
			} 
			if(getWebadresi() != null) {
				value.append(KeyUtil.getLabelValue("etkinlik.webadresi") + " : " + getWebadresi() + "<br />");
			} 
			if(getEpostaadresi() != null) {
				value.append(KeyUtil.getLabelValue("etkinlik.epostaadresi") + " : " + getEpostaadresi() + "<br />");
			} 
			if(getTelefonno() != null) {
				value.append(KeyUtil.getLabelValue("etkinlik.telefonno") + " : " + getTelefonno() + "<br />");
			} 
			if(getSorumlukisiRef() != null) {
				value.append(KeyUtil.getLabelValue("etkinlik.sorumlukisiRef") + " : " + getSorumlukisiRef().getUIString() + "<br />");
			} 
			if(getDurum() != null) {
				value.append(KeyUtil.getLabelValue("etkinlik.durum") + " : " + getDurum().getLabel() + "");
			} 
			if(getBirimRef() != null) {
				value.append(KeyUtil.getLabelValue("etkinlik.birimRef") + " : " + getBirimRef() + "");
			} 
			if(getDonemRef() != null) {
				value.append(KeyUtil.getLabelValue("etkinlik.donemRef") + " : " + getDonemRef() + "");
			} 
		return value.toString(); 
	} 
	 
}	 
