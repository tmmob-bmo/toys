package tr.com.arf.toys.db.model.system; 
 
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tr.com.arf.framework.utility.tool.KeyUtil;
import tr.com.arf.toys.db.enumerated.system.UniversiteDurum;
import tr.com.arf.toys.db.model.base.ToysBaseEntity;
 
 
@Entity 
@Table(name = "UNIVERSITE") 
public class Universite extends ToysBaseEntity { 
 
	private static final long serialVersionUID = 1L; 
 
	@Id 
	@GeneratedValue(strategy = GenerationType.IDENTITY) 
	@Column(updatable = false, unique = true, nullable = false, name = "RID") 
	private Long rID; 
 
	@Column(name = "AD") 
	private String ad; 
 
	@Column(name = "WEBADRESI") 
	private String webadresi; 
 
	@Enumerated(EnumType.ORDINAL)
	@Column(name = "DURUM") 
	private UniversiteDurum durum; 
	
	@ManyToOne 
	@JoinColumn(name = "ULKEREF") 
	private Ulke ulkeRef; 
	
	@ManyToOne 
	@JoinColumn(name = "ILREF") 
	private Sehir ilRef; 
	
	
	public Universite() { 
		super(); 
	} 
 
	@Override
	public Long getRID() { 
		return this.rID;  
	}  
	  
	@Override
	public void setRID(Long rID) {  
		this.rID = rID;  
	}   
 
	public String getAd() { 
		return this.ad; 
	} 
	 
	public void setAd(String ad) { 
		this.ad = ad; 
	} 
 
	public String getWebadresi() {
		return webadresi;
	}

	public void setWebadresi(String webadresi) {
		this.webadresi = webadresi;
	}

	public UniversiteDurum getDurum() {
		return durum;
	}

	public void setDurum(UniversiteDurum durum) {
		this.durum = durum;
	}

	public Ulke getUlkeRef() {
		return ulkeRef;
	}

	public void setUlkeRef(Ulke ulkeRef) {
		this.ulkeRef = ulkeRef;
	}

	public Sehir getIlRef() {
		return ilRef;
	}

	public void setIlRef(Sehir ilRef) {
		this.ilRef = ilRef;
	}

	@Override 
	public String toString() { 
		return this.getRID() + ""; 
	} 
	@Override 
	public String getUIString() { 
		return getAd() + ""; 
	} 
	 
	@Override 
	public void initValues(boolean defaultValues) { 
		if (defaultValues) { 
				// TODO Initialize with default values 
		} else { 
			this.rID = null; 			this.ad = ""; 
			this.webadresi="";
			this.durum=UniversiteDurum._NULL;
			this.ulkeRef=null;
			this.ilRef=null;		}  
	}  
	 
	@Override 
	public String getValue() {  
		StringBuilder value = new StringBuilder(""); 
			if(getAd() != null) 
				value.append(KeyUtil.getLabelValue("universite.ad") + " : " + getAd() + "<br />"); 
			if(getWebadresi() != null) 
				value.append(KeyUtil.getLabelValue("universite.webadresi") + " : " + getWebadresi() + "<br />"); 
			if(getDurum() != null) 
				value.append(KeyUtil.getLabelValue("universite.durum") + " : " + getDurum() + ""); 
			if(getUlkeRef() != null) 
				value.append(KeyUtil.getLabelValue("universite.ulkeRef") + " : " + getUlkeRef().getUIString() + "<br />"); 
			if(getIlRef() != null) 
				value.append(KeyUtil.getLabelValue("universite.ilRef") + " : " + getIlRef().getUIString() + "<br />"); 
		return value.toString(); 
	} 
	 
}	 
