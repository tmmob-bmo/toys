package tr.com.arf.toys.db.model.uye;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import tr.com.arf.framework.db.enumerated._common.EvetHayir;
import tr.com.arf.framework.utility.tool.BigDecimalUtil;
import tr.com.arf.framework.utility.tool.DateUtil;
import tr.com.arf.framework.utility.tool.KeyUtil;
import tr.com.arf.toys.db.enumerated.uye.UyeDurum;
import tr.com.arf.toys.db.enumerated.uye.UyeKayitDurum;
import tr.com.arf.toys.db.enumerated.uye.UyeTip;
import tr.com.arf.toys.db.model.base.ToysBaseEntity;
import tr.com.arf.toys.db.model.kisi.Kisi;
import tr.com.arf.toys.db.model.system.Birim;
import tr.com.arf.toys.db.model.system.Personel;

@Entity
@Table(name = "UYEBASVURU")
public class UyeBasvuru extends ToysBaseEntity {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(updatable = false, unique = true, nullable = false, name = "RID")
	private Long rID;

	@ManyToOne
	@JoinColumn(name = "KISIREF")
	private Kisi kisiRef;

	@Column(name = "SICILNO")
	private String sicilno;

	@Enumerated(EnumType.ORDINAL)
	@Column(name = "UYETIP")
	private UyeTip uyetip;

	@ManyToOne
	@JoinColumn(name = "BIRIMREF")
	private Birim birimRef;

	@Column(name = "ILETISIMSMS")
	private EvetHayir iletisimSms;

	@Column(name = "ILETISIMEMAIL")
	private EvetHayir iletisimEmail;

	@Column(name = "ILETISIMPOSTA")
	private EvetHayir iletisimPosta;

	@Temporal(TemporalType.DATE)
	@Column(name = "UYELIKTARIH")
	private java.util.Date uyeliktarih;

	@Temporal(TemporalType.DATE)
	@Column(name = "AYRILMATARIH")
	private java.util.Date ayrilmatarih;

	@Enumerated(EnumType.ORDINAL)
	@Column(name = "UYEDURUM")
	private UyeDurum uyedurum;

	@Enumerated(EnumType.ORDINAL)
	@Column(name = "KAYITDURUM")
	private UyeKayitDurum kayitdurum;

	@ManyToOne
	@JoinColumn(name = "ONAYLAYANREF")
	private Personel onaylayanRef;

	@Temporal(TemporalType.DATE)
	@Column(name = "ONAYTARIH")
	private java.util.Date onaytarih;

	@ManyToOne
	@JoinColumn(name = "OGRENCIUYEREF")
	private Uye ogrenciUyeRef;

	@Column(name = "OGRENCINO")
	private String ogrenciNo;

	@Column(name = "SINIF")
	private Integer sinif;

	@Transient
	private boolean epostaIletisimi;

	@Transient
	private boolean telefonIletisimi;

	@Transient
	private boolean postaIletisimi;

	public boolean isEpostaIletisimi() {
		if (iletisimEmail == EvetHayir._EVET) {
			epostaIletisimi = true;
		} else {
			epostaIletisimi = false;
		}
		return epostaIletisimi;
	}

	public void setEpostaIletisimi(boolean epostaIletisimi) {
		if (epostaIletisimi) {
			setIletisimEmail(EvetHayir._EVET);
		} else {
			setIletisimEmail(EvetHayir._HAYIR);
		}
		this.epostaIletisimi = epostaIletisimi;
	}

	public boolean isTelefonIletisimi() {
		if (iletisimSms == EvetHayir._EVET) {
			telefonIletisimi = true;
		} else {
			telefonIletisimi = false;
		}
		return telefonIletisimi;
	}

	public void setTelefonIletisimi(boolean telefonIletisimi) {
		if (telefonIletisimi) {
			setIletisimSms(EvetHayir._EVET);
		} else {
			setIletisimSms(EvetHayir._HAYIR);
		}
		this.telefonIletisimi = telefonIletisimi;
	}

	public boolean isPostaIletisimi() {
		if (iletisimPosta == EvetHayir._EVET) {
			postaIletisimi = true;
		} else {
			postaIletisimi = false;
		}
		return postaIletisimi;
	}

	public void setPostaIletisimi(boolean postaIletisimi) {
		if (postaIletisimi) {
			setIletisimPosta(EvetHayir._EVET);
		} else {
			setIletisimPosta(EvetHayir._HAYIR);
		}
		this.postaIletisimi = postaIletisimi;
	}

	public UyeBasvuru() {
		super();
	}

	@Override
	public Long getRID() {
		return this.rID;
	}

	@Override
	public void setRID(Long rID) {
		this.rID = rID;
	}

	public Kisi getKisiRef() {
		return kisiRef;
	}

	public void setKisiRef(Kisi kisiRef) {
		this.kisiRef = kisiRef;
	}

	public String getSicilno() {
		return this.sicilno;
	}

	public void setSicilno(String sicilno) {
		this.sicilno = sicilno;
	}

	public UyeTip getUyetip() {
		return this.uyetip;
	}

	public void setUyetip(UyeTip uyetip) {
		this.uyetip = uyetip;
	}

	public Birim getBirimRef() {
		return birimRef;
	}

	public void setBirimRef(Birim birimRef) {
		this.birimRef = birimRef;
	}

	public EvetHayir getIletisimSms() {
		return iletisimSms;
	}

	public void setIletisimSms(EvetHayir iletisimSms) {
		this.iletisimSms = iletisimSms;
	}

	public EvetHayir getIletisimEmail() {
		return iletisimEmail;
	}

	public void setIletisimEmail(EvetHayir iletisimEmail) {
		this.iletisimEmail = iletisimEmail;
	}

	public EvetHayir getIletisimPosta() {
		return iletisimPosta;
	}

	public void setIletisimPosta(EvetHayir iletisimPosta) {
		this.iletisimPosta = iletisimPosta;
	}

	public java.util.Date getUyeliktarih() {
		return uyeliktarih;
	}

	public void setUyeliktarih(java.util.Date uyeliktarih) {
		this.uyeliktarih = uyeliktarih;
	}

	public java.util.Date getAyrilmatarih() {
		return ayrilmatarih;
	}

	public void setAyrilmatarih(java.util.Date ayrilmatarih) {
		this.ayrilmatarih = ayrilmatarih;
	}

	public UyeDurum getUyedurum() {
		return this.uyedurum;
	}

	public void setUyedurum(UyeDurum uyedurum) {
		this.uyedurum = uyedurum;
	}

	public UyeKayitDurum getKayitdurum() {
		return this.kayitdurum;
	}

	public void setKayitdurum(UyeKayitDurum kayitdurum) {
		this.kayitdurum = kayitdurum;
	}

	public Personel getOnaylayanRef() {
		return onaylayanRef;
	}

	public void setOnaylayanRef(Personel onaylayanRef) {
		this.onaylayanRef = onaylayanRef;
	}

	public java.util.Date getOnaytarih() {
		return onaytarih;
	}

	public void setOnaytarih(java.util.Date onaytarih) {
		this.onaytarih = onaytarih;
	}

	public Uye getOgrenciUyeRef() {
		return ogrenciUyeRef;
	}

	public void setOgrenciUyeRef(Uye ogrenciUyeRef) {
		this.ogrenciUyeRef = ogrenciUyeRef;
	}

	public String getOgrenciNo() {
		return ogrenciNo;
	}

	public void setOgrenciNo(String ogrenciNo) {
		this.ogrenciNo = ogrenciNo;
	}

	public Integer getSinif() {
		return sinif;
	}

	public void setSinif(Integer sinif) {
		this.sinif = sinif;
	}

	@Transient
	public BigDecimal getUyeAidatBorc() {
		try {
			BigDecimal borc = (BigDecimal) getDBOperator().sum("miktar-odenen", Uyeaidat.class.getSimpleName(), "o.uyeRef.rID=" + getRID() + " AND o.uyemuafiyetRef is null");
			return BigDecimalUtil.changeToCurrency(borc);
		} catch (Exception e) {
			return BigDecimalUtil.changeToCurrency(new BigDecimal(0L));
		}
	}

	@Override
	public String toString() {
		return getUIString();
	}

	public String getIletisimTercih() {
		StringBuffer result = new StringBuffer();
		result.append("<b>").append(KeyUtil.getLabelValue("uye.iletisimEmail")).append(" : </b>").append(getIletisimEmail().getLabel()).append("<br/>");
		result.append("<b>").append(KeyUtil.getLabelValue("uye.iletisimSms")).append(" : </b>").append(getIletisimSms().getLabel()).append("<br/>");
		result.append("<b>").append(KeyUtil.getLabelValue("uye.iletisimPosta")).append(" : </b>").append(getIletisimPosta().getLabel());
		return result.toString();
	}

	@Override
	public String getUIString() {
		return getKisiRef().getUIStringShort() + ", " + KeyUtil.getLabelValue("uye.sicilno") + ": " + getSicilno();
	}

	@Override
	public void initValues(boolean defaultValues) {
		if (defaultValues) {
			// TODO Initialize with default values
		} else {
			this.rID = null;
			this.kisiRef = null;
			this.sicilno = "";
			this.uyetip = UyeTip._NULL;
			this.birimRef = null;
			this.iletisimEmail = EvetHayir._NULL;
			this.iletisimPosta = EvetHayir._NULL;
			this.iletisimSms = EvetHayir._NULL;
			this.uyeliktarih = null;
			this.ayrilmatarih = null;
			this.uyedurum = UyeDurum._NULL;
			this.kayitdurum = UyeKayitDurum._NULL;
			this.onaylayanRef = null;
			this.onaytarih = null;
			this.ogrenciUyeRef = null;
			this.sinif = null;
			this.ogrenciNo = "";
		}
	}

	@Override
	public String getValue() {
		StringBuilder value = new StringBuilder("");
		if (getKisiRef() != null) {
			value.append(KeyUtil.getLabelValue("uyebasvuru.kisiRef") + " : " + getKisiRef().getUIString() + "<br />");
		}
		if (getSicilno() != null) {
			value.append(KeyUtil.getLabelValue("uyebasvuru.sicilno") + " : " + getSicilno() + "<br />");
		}
		if (getUyetip() != null) {
			value.append(KeyUtil.getLabelValue("uyebasvuru.uyetip") + " : " + getUyetip() + "<br />");
		}
		if (getBirimRef() != null) {
			value.append(KeyUtil.getLabelValue("uyebasvuru.birimRef") + " : " + getBirimRef().getUIString() + "<br />");
		}
		if (getUyeliktarih() != null) {
			value.append(KeyUtil.getLabelValue("uyebasvuru.uyeliktarih") + " : " + DateUtil.dateToDMY(getUyeliktarih()) + "<br />");
		}
		if (getAyrilmatarih() != null) {
			value.append(KeyUtil.getLabelValue("uyebasvuru.ayrilmatarih") + " : " + DateUtil.dateToDMY(getAyrilmatarih()) + "<br />");
		}
		if (getUyedurum() != null) {
			value.append(KeyUtil.getLabelValue("uyebasvuru.uyedurum") + " : " + getUyedurum() + "<br />");
		}
		if (getKayitdurum() != null) {
			value.append(KeyUtil.getLabelValue("uyebasvuru.kayitdurum") + " : " + getKayitdurum() + "<br />");
		}
		if (getOnaylayanRef() != null) {
			value.append(KeyUtil.getLabelValue("uyebasvuru.onaylayanRef") + " : " + getOnaylayanRef().getUIString() + "<br />");
		}
		if (getOnaytarih() != null) {
			value.append(KeyUtil.getLabelValue("uyebasvuru.onaytarih") + " : " + DateUtil.dateToDMY(getOnaytarih()) + "");
		}
		if (getSinif() != null) {
			value.append(KeyUtil.getLabelValue("uyebasvuru.sinif") + " : " + getSinif() + "");
		}
		if (getOgrenciNo() != null) {
			value.append(KeyUtil.getLabelValue("uyebasvuru.ogrenciNo") + " : " + getOgrenciNo() + "");
		}
		if (getOgrenciUyeRef() != null) {
			value.append(KeyUtil.getLabelValue("uyebasvuru.ogrenciUyeRef") + " : " + getOgrenciUyeRef().getUIString() + "");
		}
		if (getIletisimEmail() != null) {
			value.append(KeyUtil.getLabelValue("uyebasvuru.iletisimEmail") + " : " + getIletisimEmail().getLabel() + "");
		}
		if (getIletisimPosta() != null) {
			value.append(KeyUtil.getLabelValue("uyebasvuru.iletisimPosta") + " : " + getIletisimPosta().getLabel() + "");
		}
		if (getIletisimSms() != null) {
			value.append(KeyUtil.getLabelValue("uyebasvuru.iletisimSms") + " : " + getIletisimSms().getLabel() + "");
		}
		return value.toString();
	}

}
