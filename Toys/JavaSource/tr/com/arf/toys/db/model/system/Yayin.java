package tr.com.arf.toys.db.model.system; 
 
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import tr.com.arf.framework.utility.tool.KeyUtil;
import tr.com.arf.toys.db.enumerated.system.YayinTip;
import tr.com.arf.toys.db.model.base.ToysBaseEntity;
 
@Entity 
@Table(name = "YAYIN") 
public class Yayin extends ToysBaseEntity { 
 
	private static final long serialVersionUID = 1L; 
 
	@Id 
	@GeneratedValue(strategy = GenerationType.IDENTITY) 
	@Column(updatable = false, unique = true, nullable = false, name = "RID") 
	private Long rID; 
 
	@Column(name = "AD") 
	private String ad; 
 
	@Enumerated(EnumType.ORDINAL) 
	@Column(name = "YAYINTIP") 
	private YayinTip yayintip; 
 
	@Column(name = "ACIKLAMA") 
	private String aciklama; 
 
 
	public Yayin() { 
		super(); 
	} 
 
	@Override
	public Long getRID() { 
		return this.rID;  
	}  
	  
	@Override
	public void setRID(Long rID) {  
		this.rID = rID;  
	}   
 
	public String getAd() { 
		return this.ad; 
	} 
	 
	public void setAd(String ad) { 
		this.ad = ad; 
	} 
 
	public YayinTip getYayintip() { 
		return this.yayintip;  
	}  
  
	public void setYayintip(YayinTip yayintip) {  
		this.yayintip = yayintip;  
	}   
 
	public String getAciklama() { 
		return this.aciklama; 
	} 
	 
	public void setAciklama(String aciklama) { 
		this.aciklama = aciklama; 
	} 
 
 
	@Override 
	public String toString() { 
		return "tr.com.arf.toys.db.model.system.Yayin[id=" + this.getRID()  + "]"; 
	} 
	@Override 
	public String getUIString() { 
		return getAd() + ""; 
	} 
	 
	@Override 
	public void initValues(boolean defaultValues) { 
		if (defaultValues) { 
				// TODO Initialize with default values 
		} else { 
			this.rID = null; 			this.ad = ""; 			this.yayintip = YayinTip._NULL; 			this.aciklama = ""; 		}  
	}  
	 
	@Override 
	public String getValue() {  
		StringBuilder value = new StringBuilder(""); 
			if(getAd() != null) 
				value.append(KeyUtil.getLabelValue("yayin.ad") + " : " + getAd() + "<br />"); 
			if(getYayintip() != null) 
				value.append(KeyUtil.getLabelValue("yayin.yayintip") + " : " + getYayintip() + "<br />"); 
			if(getAciklama() != null) 
				value.append(KeyUtil.getLabelValue("yayin.aciklama") + " : " + getAciklama() + ""); 
		return value.toString(); 
	} 
	 
}	 
