package tr.com.arf.toys.db.model.system;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tr.com.arf.framework.utility.tool.KeyUtil;
import tr.com.arf.toys.db.enumerated.system.YprojeDurum;
import tr.com.arf.toys.db.model.base.ToysBaseEntity;
import tr.com.arf.toys.db.model.kisi.Kurum;

@Entity 
@Table(name = "YPROJE") 
public class Yproje extends ToysBaseEntity {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -239915138581564984L;

	@Id 
	@GeneratedValue(strategy = GenerationType.IDENTITY) 
	@Column(updatable = false, unique = true, nullable = false, name = "RID") 
	private Long rID;
	
	@Column(name = "AD") 
	private String ad; 
 
	@Column(name = "ACIKLAMA") 
	private String aciklama; 
 
	@ManyToOne
	@JoinColumn(name = "KURUM") 
	private Kurum kurumRef; 
	
	@Enumerated(EnumType.ORDINAL) 
	@Column(name = "YPROJEDURUM") 
	private YprojeDurum yprojeDurum; 
 
	 
	
	
	@Override
	public Long getRID() { 
		return this.rID;  
	}  
	  
	@Override
	public void setRID(Long rID) {  
		this.rID = rID;  
	} 
	
	public String getAd() { 
		return this.ad; 
	} 
	 
	public void setAd(String ad) { 
		this.ad = ad; 
	}  
 
	public String getAciklama() { 
		return this.aciklama; 
	} 
	 
	public void setAciklama(String aciklama) { 
		this.aciklama = aciklama; 
	} 	
	
	public Kurum getKurumRef() { 
		return this.kurumRef; 
	} 
	 
	public void setKurumRef(Kurum kurumRef) { 
		this.kurumRef = kurumRef; 
	} 	
	
	public YprojeDurum getyprojeDurum() { 
		return this.yprojeDurum; 
	} 
	 
	public void setyprojeDurum(YprojeDurum yprojeDurum) { 
		this.yprojeDurum = yprojeDurum; 
	} 	
 
	
	
	
 
	@Override 
	public String toString() { 
		return getAd();
	} 
	
	@Override 
	public String getUIString() { 
		return getAd() + ""; 
		
	} 
	 
	@Override 
	public void initValues(boolean defaultValues) { 
		if (defaultValues) { 
				// TODO Initialize with default values 
		} else { 
			this.rID = null; 
			this.kurumRef = null; 
			this.ad = ""; 
			this.aciklama = ""; 
			this.yprojeDurum = null; 
		}  
	}  
	 
	@Override 
	public String getValue() {  
		StringBuilder value = new StringBuilder(""); 
			
			if(getAd() != null) {
				value.append(KeyUtil.getLabelValue("yproje.ad") + " : " + getAd() + "<br />");
			} 
			if(getAciklama() != null) {
				value.append(KeyUtil.getLabelValue("yproje.aciklama") + " : " + getAciklama() + "");
			} 
		return value.toString(); 
	} 
	 
 

}
