package tr.com.arf.toys.db.model.system;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import tr.com.arf.framework.utility.tool.DateUtil;
import tr.com.arf.framework.utility.tool.KeyUtil;
import tr.com.arf.toys.db.enumerated.system.KpsTuru;
import tr.com.arf.toys.db.model.base.ToysBaseEntity;
import tr.com.arf.toys.service.application.ManagedBeanLocator;

@Entity
@Table(name = "KPSLOG")
public class Kpslog extends ToysBaseEntity {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(updatable = false, unique = true, nullable = false, name = "RID")
	private Long rID;

	@ManyToOne
	@JoinColumn(name = "KULLANICIREF")
	private Kullanici kullaniciRef;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "TARIH")
	private java.util.Date tarih;

	@Enumerated(EnumType.ORDINAL)
	@Column(name = "KPSTURU")
	private KpsTuru kpsturu;

	@Column(name = "KIMLIKNO")
	private Long kimlikno;

	public Kpslog() {
		super();
	}

	public Kpslog(KpsTuru kpsTuru, Kullanici kullaniciRef, Long kimlikno) {
		super();
		this.kpsturu = KpsTuru._NULL;
		this.kullaniciRef = kullaniciRef;
		this.tarih = new Date();
		this.kimlikno = kimlikno;
	}
	
	public Kpslog(KpsTuru kpsTuru, Long kimlikno) {
		super();
		this.kpsturu = KpsTuru._NULL;
		this.kullaniciRef = ManagedBeanLocator.locateSessionUser().getKullanici();
		this.tarih = new Date();
		this.kimlikno = kimlikno;
	}

	@Override
	public Long getRID() {
		return this.rID;
	}

	@Override
	public void setRID(Long rID) {
		this.rID = rID;
	}

	public Kullanici getKullaniciRef() {
		return kullaniciRef;
	}

	public void setKullaniciRef(Kullanici kullaniciRef) {
		this.kullaniciRef = kullaniciRef;
	}

	public java.util.Date getTarih() {
		return tarih;
	}

	public void setTarih(java.util.Date tarih) {
		this.tarih = tarih;
	}

	public KpsTuru getKpsturu() {
		return kpsturu;
	}

	public void setKpsturu(KpsTuru kpsturu) {
		this.kpsturu = kpsturu;
	}

	public Long getKimlikno() {
		return kimlikno;
	}

	public void setKimlikno(Long kimlikno) {
		this.kimlikno = kimlikno;
	}

	@Override
	public String toString() {
		return "tr.com.arf.toys.db.model.system.Kpslog[id=" + this.getRID() + "]";
	}

	@Override
	public String getUIString() {
		return getKullaniciRef() + "";
	}

	@Override
	public void initValues(boolean defaultValues) {
		this.rID = null;
		this.kullaniciRef = null;
		this.tarih = null;
		this.kpsturu = KpsTuru._NULL;
		this.kimlikno = null;
	}

	@Override
	public String getValue() {
		StringBuilder value = new StringBuilder("Veri : <br />");
		if (getKullaniciRef() != null) {
			value.append(KeyUtil.getLabelValue("kpslog.kullaniciRef") + " : " + getKullaniciRef().getUIString() + "<br />");
		}
		if (getTarih() != null) {
			value.append(KeyUtil.getLabelValue("kpslog.tarih") + " : " + DateUtil.dateToDMYHMS(getTarih()) + "<br />");
		}
		if (getKpsturu() != null) {
			value.append(KeyUtil.getLabelValue("kpslog.kpsturu") + " : " + getKpsturu().getLabel() + "<br />");
		}
		if (getKimlikno() != null) {
			value.append(KeyUtil.getLabelValue("kpslog.kimlikno") + " : " + getKimlikno() + "<br />");
		}
		return value.toString();
	}

}
