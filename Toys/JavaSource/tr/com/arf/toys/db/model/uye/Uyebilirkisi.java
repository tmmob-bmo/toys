package tr.com.arf.toys.db.model.uye; 
 
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import tr.com.arf.framework.utility.tool.KeyUtil;
import tr.com.arf.toys.db.enumerated.uye.UyeBilirkisiDurum;
import tr.com.arf.toys.db.model.base.ToysBaseEntity;
import tr.com.arf.toys.db.model.evrak.Evrak;
 
@Entity 
@Table(name = "UYEBILIRKISI") 
public class Uyebilirkisi extends ToysBaseEntity { 
 
	private static final long serialVersionUID = 1L; 
 
	@Id 
	@GeneratedValue(strategy = GenerationType.IDENTITY) 
	@Column(updatable = false, unique = true, nullable = false, name = "RID") 
	private Long rID; 
 
	@ManyToOne 
	@JoinColumn(name = "UYEREF") 
	private Uye uyeRef; 
 
	@ManyToOne 
	@JoinColumn(name = "UYEUZMANLIKREF") 
	private Uyeuzmanlik uyeuzmanlikRef; 
 
	@ManyToOne 
	@JoinColumn(name = "EVRAKREF") 
	private Evrak evrakRef; 
 
	@Temporal(TemporalType.DATE) 
	@Column(name = "TARIH") 
	private java.util.Date tarih; 
 
	@Temporal(TemporalType.DATE) 
	@Column(name = "BITISTARIH") 
	private java.util.Date bitistarih; 
 
	@Enumerated(EnumType.ORDINAL) 
	@Column(name = "UYEBILIRKISIDURUM") 
	private UyeBilirkisiDurum uyebilirkisidurum; 
 
	@Column(name = "ACIKLAMA") 
	private String aciklama; 
 
 
	public Uyebilirkisi() { 
		super(); 
	} 
 
	@Override
	public Long getRID() { 
		return this.rID;  
	}  
	  
	@Override
	public void setRID(Long rID) {  
		this.rID = rID;  
	}   
 
	public Uye getUyeRef() { 
		return uyeRef; 
	} 
	 
	public void setUyeRef(Uye uyeRef) { 
		this.uyeRef = uyeRef; 
	} 
 
	public Uyeuzmanlik getUyeuzmanlikRef() { 
		return uyeuzmanlikRef; 
	} 
	 
	public void setUyeuzmanlikRef(Uyeuzmanlik uyeuzmanlikRef) { 
		this.uyeuzmanlikRef = uyeuzmanlikRef; 
	} 
 
	public Evrak getEvrakRef() { 
		return evrakRef; 
	} 
	 
	public void setEvrakRef(Evrak evrakRef) { 
		this.evrakRef = evrakRef; 
	} 
 
	public java.util.Date getTarih() { 
		return tarih; 
	} 
	 
	public void setTarih(java.util.Date tarih) { 
		this.tarih = tarih; 
	} 
 
	public java.util.Date getBitistarih() { 
		return bitistarih; 
	} 
	 
	public void setBitistarih(java.util.Date bitistarih) { 
		this.bitistarih = bitistarih; 
	} 
 
	public UyeBilirkisiDurum getUyebilirkisidurum() { 
		return this.uyebilirkisidurum;  
	}  
  
	public void setUyebilirkisidurum(UyeBilirkisiDurum uyebilirkisidurum) {  
		this.uyebilirkisidurum = uyebilirkisidurum;  
	}   
 
	public String getAciklama() { 
		return this.aciklama; 
	} 
	 
	public void setAciklama(String aciklama) { 
		this.aciklama = aciklama; 
	} 
 
 
	@Override 
	public String toString() { 
		return "tr.com.arf.toys.db.model.uye.Uyebilirkisi[id=" + this.getRID()  + "]"; 
	} 
	@Override 
	public String getUIString() {  
		return getUyeuzmanlikRef().getUzmanliktipRef().getAd();
	} 
	 
	@Override 
	public void initValues(boolean defaultValues) { 
		if (defaultValues) { 
				// TODO Initialize with default values 
		} else { 
			this.rID = null; 			this.uyeRef = null; 			this.uyeuzmanlikRef = null; 			this.evrakRef = null; 			this.tarih = null; 			this.bitistarih = null; 			this.uyebilirkisidurum = UyeBilirkisiDurum._NULL; 			this.aciklama = ""; 		}  
	}  
	 
	@Override 
	public String getValue() {  
		StringBuilder value = new StringBuilder(""); 
			if(getUyeRef() != null) 
				value.append(KeyUtil.getLabelValue("uyebilirkisi.uyeRef") + " : " + getUyeRef().getUIString() + "<br />"); 
			if(getUyeuzmanlikRef() != null) 
				value.append(KeyUtil.getLabelValue("uyebilirkisi.uyeuzmanlikRef") + " : " + getUyeuzmanlikRef().getUIString() + "<br />"); 
			if(getEvrakRef() != null) 
				value.append(KeyUtil.getLabelValue("uyebilirkisi.evrakRef") + " : " + getEvrakRef().getUIString() + "<br />"); 
			if(getTarih() != null) 
				value.append(KeyUtil.getLabelValue("uyebilirkisi.tarih") + " : " + getTarih() + "<br />"); 
			if(getBitistarih() != null) 
				value.append(KeyUtil.getLabelValue("uyebilirkisi.bitistarih") + " : " + getBitistarih() + "<br />"); 
			if(getUyebilirkisidurum() != null) 
				value.append(KeyUtil.getLabelValue("uyebilirkisi.uyebilirkisidurum") + " : " + getUyebilirkisidurum() + "<br />"); 
			if(getAciklama() != null) 
				value.append(KeyUtil.getLabelValue("uyebilirkisi.aciklama") + " : " + getAciklama() + ""); 
		return value.toString(); 
	} 
	 
}	 
