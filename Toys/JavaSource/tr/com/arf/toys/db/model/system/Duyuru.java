package tr.com.arf.toys.db.model.system; 
 

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import tr.com.arf.framework.utility.tool.KeyUtil;
import tr.com.arf.toys.db.model.base.ToysBaseEntity;
 
@Entity 
@Table(name = "DUYURU") 
public class Duyuru extends ToysBaseEntity { 
 
	private static final long serialVersionUID = 1L; 
 
	@Id 
	@GeneratedValue(strategy = GenerationType.IDENTITY) 
	@Column(updatable = false, unique = true, nullable = false, name = "RID") 
	private Long rID; 
 
	@Column(name = "DUYURUBASLIGI") 
	private String duyurubasligi; 
	
	@Column(name = "DUYURUMETNI") 
	private String duyurumetni; 
	
	@Temporal(TemporalType.DATE) 
	@Column(name = "BASLANGICTARIH") 
	private java.util.Date baslangictarih; 
	
	@Temporal(TemporalType.DATE) 
	@Column(name = "BITISTARIH") 
	private java.util.Date bitistarih; 
 
 
	public Duyuru() { 
		super(); 
	} 
 
	@Override
	public Long getRID() { 
		return this.rID;  
	}  
	  
	@Override
	public void setRID(Long rID) {  
		this.rID = rID;  
	}   
 
	public String getDuyurubasligi() {
		return duyurubasligi;
	}

	public void setDuyurubasligi(String duyurubasligi) {
		this.duyurubasligi = duyurubasligi;
	}

	public String getDuyurumetni() {
		return duyurumetni;
	}

	public void setDuyurumetni(String duyurumetni) {
		this.duyurumetni = duyurumetni;
	}

	public java.util.Date getBaslangictarih() {
		return baslangictarih;
	}

	public void setBaslangictarih(java.util.Date baslangictarih) {
		this.baslangictarih = baslangictarih;
	}

	public java.util.Date getBitistarih() {
		return bitistarih;
	}

	public void setBitistarih(java.util.Date bitistarih) {
		this.bitistarih = bitistarih;
	}

	@Override 
	public String toString() { 
		return "tr.com.arf.toys.db.model.system.Duyuru[id=" + this.getRID()  + "]"; 
	} 
	@Override 
	public String getUIString() { 
		return getDuyurubasligi() + ""; 
	} 
	 
	@Override 
	public void initValues(boolean defaultValues) { 
		if (defaultValues) { 
				// TODO Initialize with default values 
		} else { 
			this.rID = null; 			this.duyurubasligi = ""; 			this.duyurumetni="";
			this.baslangictarih=null;
			this.bitistarih=null;		}  
	}  
	 
	@Override 
	public String getValue() {  
		StringBuilder value = new StringBuilder(""); 
			
		if(getDuyurubasligi() != null) {
			value.append(KeyUtil.getLabelValue("duyuru.duyurubasligi") + " : " + getDuyurubasligi() + "<br />");
		}
		if(getDuyurumetni() != null) {
			value.append(KeyUtil.getLabelValue("duyuru.duyurumetni") + " : " + getDuyurumetni() + "<br />");
		}		
		if(getBaslangictarih() != null) 
			value.append(KeyUtil.getLabelValue("duyuru.baslangictarih") + " : " + getBaslangictarih() + "<br />"); 
		if(getBitistarih() != null) 
			value.append(KeyUtil.getLabelValue("duyuru.bitistarih") + " : " + getBitistarih() + "<br />"); 
		return value.toString(); 
	} 
	 
}	 
