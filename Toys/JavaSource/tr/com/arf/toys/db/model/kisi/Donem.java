package tr.com.arf.toys.db.model.kisi; 
 
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import tr.com.arf.framework.utility.tool.KeyUtil;
import tr.com.arf.toys.db.model.base.ToysBaseEntity;
import tr.com.arf.toys.db.model.system.Birim;
 
@Entity 
@Table(name = "DONEM") 
public class Donem extends ToysBaseEntity { 
 
	private static final long serialVersionUID = 1L; 
 
	@Id 
	@GeneratedValue(strategy = GenerationType.IDENTITY) 
	@Column(updatable = false, unique = true, nullable = false, name = "RID") 
	private Long rID; 
 
	@ManyToOne 
	@JoinColumn(name = "ANADONEMREF") 
	private Anadonem anadonemRef; 
	
	@ManyToOne 
	@JoinColumn(name = "BIRIMREF") 
	private Birim birimRef; 
	
	@Temporal(TemporalType.DATE) 
	@Column(name = "BASLANGICTARIH") 
	private java.util.Date baslangictarih; 
 
	@Temporal(TemporalType.DATE) 
	@Column(name = "BITISTARIH") 
	private java.util.Date bitistarih; 
	
	@Column(name = "TANIM") 
	private String tanim;
	
	public Donem() { 
		super(); 
	} 
 
	@Override
	public Long getRID() { 
		return this.rID;  
	}  
	  
	@Override
	public void setRID(Long rID) {  
		this.rID = rID;  
	}   
		
	public Anadonem getAnadonemRef() {
		return anadonemRef;
	}

	public void setAnadonemRef(Anadonem anadonemRef) {
		this.anadonemRef = anadonemRef;
	}
	
	public Birim getBirimRef() {
		return birimRef;
	}

	public void setBirimRef(Birim birimRef) {
		this.birimRef = birimRef;
	}

	public java.util.Date getBaslangictarih() {
		return baslangictarih;
	}

	public void setBaslangictarih(java.util.Date baslangictarih) {
		this.baslangictarih = baslangictarih;
	}

	public java.util.Date getBitistarih() {
		return bitistarih;
	}

	public void setBitistarih(java.util.Date bitistarih) {
		this.bitistarih = bitistarih;
	}

	public String getTanim() {
		return tanim;
	}

	public void setTanim(String tanim) {
		this.tanim = tanim;
	}

	@Override 
	public String toString() { 
		return "tr.com.arf.toys.db.model.kisi.Donem[id=" + this.getRID()  + "]"; 
	} 
	
	@Override 
	public String getUIString() { 
		return getTanim() + ""; 
	} 
	 
	@Override 
	public void initValues(boolean defaultValues) { 
		if (defaultValues) { 
				// TODO Initialize with default values 
		} else { 
			this.rID = null; 
			this.anadonemRef = null;
			this.birimRef = null;			this.baslangictarih=null;
			this.bitistarih=null;
			this.tanim=null;
		}  
	}  
	 
	@Override 
	public String getValue() {  
		StringBuilder value = new StringBuilder(""); 
			if(getAnadonemRef() != null) {
				value.append(KeyUtil.getLabelValue("donem.anadonemRef") + " : " + getAnadonemRef().getUIString() + "<br />"); 
			}
			if(getBirimRef() != null){
				value.append(KeyUtil.getLabelValue("donem.birimRef") + " : " + getBirimRef().getUIString() + "<br />"); 
			}
			if(getTanim() != null) {
				value.append(KeyUtil.getLabelValue("donem.tanim") + " : " + getTanim() + "<br />"); 
			}
			if(getBaslangictarih() != null) {
				value.append(KeyUtil.getLabelValue("donem.baslangictarih") + " : " + getBaslangictarih() + "<br />");
			} 
			if(getBitistarih() != null) {
				value.append(KeyUtil.getLabelValue("donem.bitistarih") + " : " + getBitistarih());
			}			
		return value.toString(); 
	} 
	 
}	 
