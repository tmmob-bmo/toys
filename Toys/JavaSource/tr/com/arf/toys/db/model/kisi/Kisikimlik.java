package tr.com.arf.toys.db.model.kisi; 
 
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import tr.com.arf.framework.utility.tool.KeyUtil;
import tr.com.arf.toys.db.enumerated.kisi.Cinsiyet;
import tr.com.arf.toys.db.enumerated.kisi.KanGrubu;
import tr.com.arf.toys.db.enumerated.kisi.KisiDurum;
import tr.com.arf.toys.db.enumerated.kisi.MedeniHal;
import tr.com.arf.toys.db.model.base.ToysBaseEntity;
import tr.com.arf.toys.db.model.system.Ilce;
import tr.com.arf.toys.db.model.system.Sehir;
 
@Entity 
@Table(name = "KISIKIMLIK") 
public class Kisikimlik extends ToysBaseEntity { 
 
	private static final long serialVersionUID = 1L; 
 
	@Id 
	@GeneratedValue(strategy = GenerationType.IDENTITY) 
	@Column(updatable = false, unique = true, nullable = false, name = "RID") 
	private Long rID; 
 
	@OneToOne 
	@JoinColumn(name = "KISIREF") 
	private Kisi kisiRef; 
	
	@Enumerated(EnumType.ORDINAL) 
	@Column(name = "CINSIYET") 
	private Cinsiyet cinsiyet;  	
 
	@Column(name = "KIMLIKNO") 
	private Long kimlikno; 
	 
	@Column(name = "BABAAD") 
	private String babaad; 
 
	@Column(name = "ANAAD") 
	private String anaad; 
	
	@Enumerated(EnumType.ORDINAL)
	@Column(name = "MEDENIHAL") 
	private MedeniHal medenihal; 
	
	@Enumerated(EnumType.ORDINAL)
	@Column(name = "KANGRUP") 
	private KanGrubu kangrup;
	
	@Column(name = "DOGUMYER") 
	private String dogumyer; 
	
	@Temporal(TemporalType.DATE) 
	@Column(name = "DOGUMTARIH") 
	private java.util.Date dogumtarih; 
	
	@Temporal(TemporalType.DATE) 
	@Column(name = "OLUMTARIH") 
	private java.util.Date olumtarih; 
	
	@Enumerated(EnumType.ORDINAL)
	@Column(name = "KISIDURUM") 
	private KisiDurum kisidurum;
	
 	@ManyToOne 
	@JoinColumn(name = "DOGUMILCEREF") 
	private Ilce dogumsehirRef;

	@ManyToOne 
	@JoinColumn(name = "SEHIRREF") 
	private Sehir sehirRef; 
 
	@ManyToOne 
	@JoinColumn(name = "ILCEREF") 
	private Ilce ilceRef;  
 
	@Column(name = "CILTNO") 
	private String ciltno; 
	
	@Column(name = "AILENO") 
	private String aileno; 
 
	@Column(name = "SIRANO") 
	private String sirano; 
	
	@Column(name = "MAHALLE") 
	private String mahalle; 
	
	@Temporal(TemporalType.DATE) 
	@Column(name = "KPSDOGRULAMATARIH") 
	private java.util.Date kpsdogrulamatarih; 
 	
	public Kisikimlik() { 
		super(); 
	} 
 
	@Override
	public Long getRID() { 
		return this.rID;  
	}  
	  
	@Override
	public void setRID(Long rID) {  
		this.rID = rID;  
	}   
 
	public Kisi getKisiRef() { 
		return kisiRef; 
	} 
	 
	public void setKisiRef(Kisi kisiRef) { 
		this.kisiRef = kisiRef; 
	} 
 
	public Long getKimlikno() {
		return kimlikno;
	}

	public void setKimlikno(Long kimlikno) {
		this.kimlikno = kimlikno;
	}

	public String getBabaad() { 
		return this.babaad; 
	} 
	 
	public void setBabaad(String babaad) { 
		this.babaad = babaad; 
	} 
 
	public String getAnaad() { 
		return this.anaad; 
	} 
	 
	public void setAnaad(String anaad) { 
		this.anaad = anaad; 
	} 
 
	public Ilce getDogumsehirRef() { 
		return dogumsehirRef; 
	} 
	 
	public void setDogumsehirRef(Ilce dogumsehirRef) { 
		this.dogumsehirRef = dogumsehirRef; 
	} 
 
	public String getDogumyer() { 
		return this.dogumyer; 
	} 
	 
	public void setDogumyer(String dogumyer) { 
		this.dogumyer = dogumyer; 
	} 
 
	public java.util.Date getDogumtarih() { 
		return dogumtarih; 
	} 
	 
	public void setDogumtarih(java.util.Date dogumtarih) { 
		this.dogumtarih = dogumtarih; 
	} 
 
	public Cinsiyet getCinsiyet() { 
		return this.cinsiyet;  
	}  
  
	public void setCinsiyet(Cinsiyet cinsiyet) {  
		this.cinsiyet = cinsiyet;  
	}   
 
	public KanGrubu getKangrup() { 
		return this.kangrup;  
	}  
  
	public void setKangrup(KanGrubu kangrup) {  
		this.kangrup = kangrup;  
	}   
 
	public MedeniHal getMedenihal() { 
		return this.medenihal; 
	} 
	 
	public void setMedenihal(MedeniHal medenihal) { 
		this.medenihal = medenihal; 
	} 
 
	public Sehir getSehirRef() { 
		return sehirRef; 
	} 
	 
	public void setSehirRef(Sehir sehirRef) { 
		this.sehirRef = sehirRef; 
	} 
 
	public Ilce getIlceRef() {
		return ilceRef;
	}

	public void setIlceRef(Ilce ilceRef) {
		this.ilceRef = ilceRef;
	}
 
	public String getCiltno() { 
		return this.ciltno; 
	} 
	 
	public void setCiltno(String ciltno) { 
		this.ciltno = ciltno; 
	} 

	public String getAileno() { 
		return this.aileno; 
	} 
	 
	public void setAileno(String aileno) { 
		this.aileno = aileno; 
	} 
 
	public String getSirano() { 
		return this.sirano; 
	} 
	 
	public void setSirano(String sirano) { 
		this.sirano = sirano; 
	} 

	public java.util.Date getOlumtarih() {
		return olumtarih;
	}

	public void setOlumtarih(java.util.Date olumtarih) {
		this.olumtarih= olumtarih;
	}
	
	public KisiDurum getKisidurum() {
		return kisidurum;
	}

	public void setKisidurum(KisiDurum kisidurum) {
		this.kisidurum = kisidurum;
	}
	
	public String getMahalle() {
		return mahalle;
	}

	public void setMahalle(String mahalle) {
		this.mahalle = mahalle;
	}

	public java.util.Date getKpsdogrulamatarih() {
		return kpsdogrulamatarih;
	}

	public void setKpsdogrulamatarih(java.util.Date kpsdogrulamatarih) {
		this.kpsdogrulamatarih = kpsdogrulamatarih;
	}

	@Override 
	public String toString() { 
		return "tr.com.arf.toys.db.model.kisi.Kisikimlik[id=" + this.getRID()  + "]"; 
	} 
	@Override 
	public String getUIString() { 
		return getKimlikno() + ""; 
	} 
	 
	@Override 
	public void initValues(boolean defaultValues) { 
		if (defaultValues) { 
				// TODO Initialize with default values 
		} else { 
			this.rID = null; 			this.kisiRef = null; 			this.kimlikno = null; 			this.babaad = ""; 			this.anaad = ""; 			this.dogumsehirRef = null; 			this.dogumyer = ""; 			this.dogumtarih = null; 			this.cinsiyet = Cinsiyet._NULL; 			this.kangrup = KanGrubu._NULL; 			this.medenihal = MedeniHal._BILINMIYOR; 			this.sehirRef = null; 			this.sehirRef = null; 			this.ciltno = ""; 			this.aileno = ""; 			this.sirano = ""; 
			this.olumtarih=null;
			this.kisidurum=KisiDurum._NULL;
			this.mahalle=null;
			this.kpsdogrulamatarih=null;		}  
	}  
	 
	@Override 
	public String getValue() {  
		StringBuilder value = new StringBuilder(""); 
			if(getKisiRef() != null) {
				value.append(KeyUtil.getLabelValue("kisikimlik.kisiRef") + " : " + getKisiRef().getUIString() + "<br />");
			} 
			if(getKimlikno() != null) {
				value.append(KeyUtil.getLabelValue("kisikimlik.kimlikno") + " : " + getKimlikno() + "<br />");
			} 
			if(getBabaad() != null) {
				value.append(KeyUtil.getLabelValue("kisikimlik.babaad") + " : " + getBabaad() + "<br />");
			} 
			if(getAnaad() != null) {
				value.append(KeyUtil.getLabelValue("kisikimlik.anaad") + " : " + getAnaad() + "<br />");
			} 
			if(getDogumsehirRef() != null) {
				value.append(KeyUtil.getLabelValue("kisikimlik.dogumsehirRef") + " : " + getDogumsehirRef().getUIString() + "<br />");
			} 
			if(getDogumyer() != null) {
				value.append(KeyUtil.getLabelValue("kisikimlik.dogumyer") + " : " + getDogumyer() + "<br />");
			} 
			if(getDogumtarih() != null) {
				value.append(KeyUtil.getLabelValue("kisikimlik.dogumtarih") + " : " + getDogumtarih() + "<br />");
			} 
			if(getCinsiyet() != null) {
				value.append(KeyUtil.getLabelValue("kisikimlik.cinsiyet") + " : " + getCinsiyet() + "<br />");
			} 
			if(getKangrup() != null) {
				value.append(KeyUtil.getLabelValue("kisikimlik.kangrup") + " : " + getKangrup() + "<br />");
			} 
			if(getMedenihal() != null) {
				value.append(KeyUtil.getLabelValue("kisikimlik.medenihal") + " : " + getMedenihal().getLabel() + "<br />");
			} 
			if(getSehirRef() != null) {
				value.append(KeyUtil.getLabelValue("kisikimlik.sehirRef") + " : " + getSehirRef().getUIString() + "<br />");
			} 
			if(getIlceRef() != null) {
				value.append(KeyUtil.getLabelValue("kisikimlik.ilceRef") + " : " + getIlceRef().getUIString() + "<br />");
			} 			
			if(getCiltno() != null) {
				value.append(KeyUtil.getLabelValue("kisikimlik.ciltno") + " : " + getCiltno() + "<br />");
			} 
			if(getAileno() != null) {
				value.append(KeyUtil.getLabelValue("kisikimlik.aileno") + " : " + getAileno() + "<br />");
			} 
			if(getSirano() != null) {
				value.append(KeyUtil.getLabelValue("kisikimlik.sirano") + " : " + getSirano() + "<br />");
			} 
			if(getOlumtarih() != null) {
				value.append(KeyUtil.getLabelValue("kisikimlik.olumtarih") + " : " + getOlumtarih() + "<br />");
			} 
			if(getKisidurum() != null) {
				value.append(KeyUtil.getLabelValue("kisikimlik.kisidurum") + " : " + getKisidurum().getLabel() + "");
			} 
			if(getMahalle() != null) {
				value.append(KeyUtil.getLabelValue("kisikimlik.mahalle") + " : " + getMahalle() + "");
			} 
			if(getKpsdogrulamatarih() != null) {
				value.append(KeyUtil.getLabelValue("kisikimlik.kpsdogrulamatarih") + " : " + getKpsdogrulamatarih() + "");
			} 
		return value.toString(); 
	} 
	 
}	 
