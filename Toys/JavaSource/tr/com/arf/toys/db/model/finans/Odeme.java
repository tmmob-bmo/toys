package tr.com.arf.toys.db.model.finans;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import tr.com.arf.framework.utility.tool.BigDecimalUtil;
import tr.com.arf.framework.utility.tool.DateUtil;
import tr.com.arf.framework.utility.tool.KeyUtil;
import tr.com.arf.framework.utility.tool.StringUtil;
import tr.com.arf.toys.db.enumerated.finans.OdemeTuru;
import tr.com.arf.toys.db.enumerated.uye.OdemeSekli;
import tr.com.arf.toys.db.model.base.ToysBaseEntity;
import tr.com.arf.toys.db.model.kisi.Kisi;
import tr.com.arf.toys.db.model.system.Birim;

@Entity
@Table(name = "ODEME")
public class Odeme extends ToysBaseEntity {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(updatable = false, unique = true, nullable = false, name = "RID")
	private Long rID;

	@ManyToOne
	@JoinColumn(name = "KISIREF")
	private Kisi kisiRef;

	@Column(name = "MIKTAR")
	private BigDecimal miktar;

	@Temporal(TemporalType.DATE)
	@Column(name = "TARIH")
	private java.util.Date tarih;

	@Enumerated(EnumType.ORDINAL)
	@Column(name = "ODEMETURU")
	private OdemeTuru odemeTuru;

	@Temporal(TemporalType.DATE)
	@Column(name = "MAKBUZBASILMATARIHI")
	private java.util.Date makbuzbasilmatarihi;

	@ManyToOne
	@JoinColumn(name = "BIRIMREF")
	private Birim birimRef;

	@Column(name = "MAKBUZNO")
	private BigDecimal makbuzno;

	@Enumerated(EnumType.ORDINAL)
	@Column(name = "odemeSekli")
	private OdemeSekli odemeSekli;

	@Column(name = "dekontNo")
	private String dekontNo;

	@Transient
	private String path;

	@Transient
	private int adet = 1;

	public Odeme() {
		super();
	}

	@Override
	public Long getRID() {
		return this.rID;
	}

	@Override
	public void setRID(Long rID) {
		this.rID = rID;
	}

	public Kisi getKisiRef() {
		return kisiRef;
	}

	public void setKisiRef(Kisi kisiRef) {
		this.kisiRef = kisiRef;
	}

	public BigDecimal getMiktar() {
		return BigDecimalUtil.changeToCurrency(this.miktar);
	}

	public void setMiktar(BigDecimal miktar) {
		this.miktar = BigDecimalUtil.changeToCurrency(miktar);
	}

	public java.util.Date getTarih() {
		return tarih;
	}

	public void setTarih(java.util.Date tarih) {
		this.tarih = tarih;
	}

	public OdemeTuru getOdemeTuru() {
		return odemeTuru;
	}

	public void setOdemeTuru(OdemeTuru odemeTuru) {
		this.odemeTuru = odemeTuru;
	}

	public java.util.Date getMakbuzbasilmatarihi() {
		return makbuzbasilmatarihi;
	}

	public void setMakbuzbasilmatarihi(java.util.Date makbuzbasilmatarihi) {
		this.makbuzbasilmatarihi = makbuzbasilmatarihi;
	}

	public Birim getBirimRef() {
		return birimRef;
	}

	public void setBirimRef(Birim birimRef) {
		this.birimRef = birimRef;
	}

	public BigDecimal getMakbuzno() {
		return makbuzno;
	}

	public void setMakbuzno(BigDecimal makbuzno) {
		this.makbuzno = makbuzno;
	}

	public OdemeSekli getOdemeSekli() {
		return odemeSekli;
	}

	public void setOdemeSekli(OdemeSekli odemeSekli) {
		this.odemeSekli = odemeSekli;
	}

	public String getDekontNo() {
		return dekontNo;
	}

	public void setDekontNo(String dekontNo) {
		this.dekontNo = dekontNo;
	}

	@Override
	public String getPath() {
		return path;
	}

	@Override
	public void setPath(String path) {
		this.path = path;
	}

	public int getAdet() {
		return adet;
	}

	public void setAdet(int adet) {
		this.adet = adet;
	}

	@Override
	public String getUIString() {
		return getKisiRef().getUIStringShort() + " - " + DateUtil.dateToDMY(getTarih()) + " : " + StringUtil.convertToMoney(getMiktar());
	}

	@Override
	public void initValues(boolean defaultValues) {
		if (defaultValues) {
			// TODO Initialize with default values
		} else {
			this.rID = null;
			this.kisiRef = null;
			this.miktar = null;
			this.tarih = null;
			this.odemeTuru = null;
			this.makbuzbasilmatarihi = null;
			this.birimRef = null;
			this.makbuzno = null;
			this.odemeSekli = OdemeSekli._BANKADAN;
			this.dekontNo = null;
			this.path = null;
			this.adet = 1;
		}
	}

	@Override
	public String getValue() {
		StringBuilder value = new StringBuilder("");
		if (getKisiRef() != null) {
			value.append(KeyUtil.getLabelValue("odeme.kisiRef") + " : " + getKisiRef().getUIString() + "<br />");
		}
		if (getMiktar() != null) {
			value.append(KeyUtil.getLabelValue("odeme.miktar") + " : " + getMiktar() + "<br />");
		}
		if (getTarih() != null) {
			value.append(KeyUtil.getLabelValue("odeme.tarih") + " : " + DateUtil.dateToDMYHMS(getTarih()) + "");
		}
		if (getMakbuzbasilmatarihi() != null) {
			value.append(KeyUtil.getLabelValue("odeme.makbuzbasilmatarihi") + " : " + DateUtil.dateToDMYHMS(getMakbuzbasilmatarihi()) + "");
		}
		if (getBirimRef() != null) {
			value.append(KeyUtil.getLabelValue("odeme.birimRef") + " : " + getBirimRef().getUIString() + "<br />");
		}
		if (getMakbuzno() != null) {
			value.append(KeyUtil.getLabelValue("uyeaidat.makbuzno") + " : " + getMakbuzno() + "<br />");
		}
		if (getOdemeSekli() != null) {
			value.append(KeyUtil.getLabelValue("odeme.odemeSekli") + " : " + getOdemeSekli().getLabel() + "<br />");
		}
		if (getDekontNo() != null) {
			value.append(KeyUtil.getLabelValue("odeme.dekontNo") + " : " + getDekontNo() + "<br />");
		}
		return value.toString();
	}

}
