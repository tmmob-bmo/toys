package tr.com.arf.toys.db.enumerated.system; 
 
import tr.com.arf.framework.db.enumerated._base.BaseEnumerated;
import tr.com.arf.framework.utility.tool.KeyUtil;

public enum BirimYetkiTuru implements BaseEnumerated { 

		_NULL(0, ""),
		_TUMBIRIMLER(1, "birimYetkiTuru.tumbirimler"), 
		_KENDIBIRIMIVEALTBIRIMLERI(2, "birimYetkiTuru.kendibirimivealtbirimleri"), 
		_SADECEKENDIBIRIMLERI(3, "birimYetkiTuru.sadecekendibirimleri"); 

		private int code;
		private String label;

		private BirimYetkiTuru(int code, String label) {
		    this.code = code;
		    this.label = label;
		}

		@Override
		public int getCode() {
		    return code;
		}

		@Override
		public String getLabel() {
		    return KeyUtil.getMessageValue(label);
		}

		@Override
		public String toString() {
		    return KeyUtil.getMessageValue(label);
		}

		public static BirimYetkiTuru getWithCode(int code){
			BirimYetkiTuru[] enumArray = BirimYetkiTuru.values();
			for(BirimYetkiTuru enumObj : enumArray){ 
				if(enumObj.getCode() == code) {
					return enumObj;
				}
			}
			return BirimYetkiTuru._NULL;
		} 
}  