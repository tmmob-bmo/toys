package tr.com.arf.toys.db.enumerated.kisi; 
 
import tr.com.arf.framework.db.enumerated._base.BaseEnumerated;
import tr.com.arf.framework.utility.tool.KeyUtil;
  
public enum KurulUyelikDurumu implements BaseEnumerated {  
    
		_NULL(0, ""),
		_NULLDEGERI(1, ""),
		_UYELIKTENISTIFA(2, "kurulUyelikDurumu.uyeliktenistifa"), 
		_UYELIGINDUSMESI(3, "kurulUyelikDurumu.uyeligindusmesi");
		
		private int code;
		private String label;

		private KurulUyelikDurumu(int code, String label) {
		    this.code = code;
		    this.label = label;
		}

		@Override
		public int getCode() {
		    return code;
		}

		@Override
		public String getLabel() {
		    return KeyUtil.getMessageValue(label);
		}

		@Override
		public String toString() {
		    return KeyUtil.getMessageValue(label);
		}

		public static KurulUyelikDurumu getWithCode(int code){
			KurulUyelikDurumu[] enumArray = KurulUyelikDurumu.values();
			for(KurulUyelikDurumu enumObj : enumArray){ 
				if(enumObj.getCode() == code) {
					return enumObj;
				}
			}
			return KurulUyelikDurumu._NULL;
		} 
		
}   
