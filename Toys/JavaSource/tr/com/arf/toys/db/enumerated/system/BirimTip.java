package tr.com.arf.toys.db.enumerated.system; 
 
import tr.com.arf.framework.db.enumerated._base.BaseEnumerated;
import tr.com.arf.framework.utility.tool.KeyUtil;
  
public enum BirimTip implements BaseEnumerated {  
    
		_NULL(0, ""), 
		_MERKEZ(1, "birimTipi.buro"),
		_ODA(2, "birimTipi.oda"), 
		_SUBE(3, "birimTipi.sube"),
		_TEMSILCILIK(4, "birimTipi.temsilcilik");
  
		
		private int code;
		private String label;

		private BirimTip(int code, String label) {
		    this.code = code;
		    this.label = label;
		}

		@Override
		public int getCode() {
		    return code;
		}

		@Override
		public String getLabel() {
		    return KeyUtil.getMessageValue(label);
		}

		@Override
		public String toString() {
		    return KeyUtil.getMessageValue(label);
		}

		public static BirimTip getWithCode(int code){
			BirimTip[] enumArray = BirimTip.values();
			for(BirimTip enumObj : enumArray){ 
				if(enumObj.getCode() == code) {
					return enumObj;
				}
			}
			return BirimTip._NULL;
		} 
}   
