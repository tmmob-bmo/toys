package tr.com.arf.toys.db.enumerated.system; 
 
import tr.com.arf.framework.db.enumerated._base.BaseEnumerated;
import tr.com.arf.framework.utility.tool.KeyUtil;
  
public enum UniversiteDurum implements BaseEnumerated {  
    
		_NULL(0, ""), 
		_DEVLET(1, "universitedurum.devlet"),
		_OZEL(2, "universitedurum.ozel"),
		_YABANCI(3, "universitedurum.yabanci"),
		_DIGER(4, "universitedurum.diger");
		
 
		private int code;
		private String label;

		private UniversiteDurum(int code, String label) {
		    this.code = code;
		    this.label = label;
		}

		@Override
		public int getCode() {
		    return code;
		}

		@Override
		public String getLabel() {
		    return KeyUtil.getMessageValue(label);
		}

		@Override
		public String toString() {
		    return KeyUtil.getMessageValue(label);
		}

		public static UniversiteDurum getWithCode(int code){
			UniversiteDurum[] enumArray = UniversiteDurum.values();
			for(UniversiteDurum enumObj : enumArray){ 
				if(enumObj.getCode() == code)
					return enumObj;
			}
			return UniversiteDurum._NULL;
		} 
		
		
}   
