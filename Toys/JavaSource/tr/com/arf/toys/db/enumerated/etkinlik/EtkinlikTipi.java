package tr.com.arf.toys.db.enumerated.etkinlik;

import tr.com.arf.framework.db.enumerated._base.BaseEnumerated;
import tr.com.arf.framework.utility.tool.KeyUtil;

public enum EtkinlikTipi implements BaseEnumerated {  
    
	_NULL(0, ""), 
	_SEMPOZYUM(1, "etkinliktipi.sempozyum"),
	_KONGRE(2, "etkinliktipi.kongre"),
	_CALISTAY(3, "etkinliktipi.calistay"), 
	_FORUM(3, "etkinliktipi.forum"); 

	private int code;
	private String label;

	private EtkinlikTipi(int code, String label) {
	    this.code = code;
	    this.label = label;
	}

	@Override
	public int getCode() {
	    return code;
	}

	@Override
	public String getLabel() {
	    return KeyUtil.getMessageValue(label);
	}

	@Override
	public String toString() {
	    return KeyUtil.getMessageValue(label);
	}

	public static EtkinlikTipi getWithCode(int code){
		EtkinlikTipi[] enumArray = EtkinlikTipi.values();
		for(EtkinlikTipi enumObj : enumArray){ 
			if(enumObj.getCode() == code) {
				return enumObj;
			}
		}
		return EtkinlikTipi._NULL;
	} 
}  
