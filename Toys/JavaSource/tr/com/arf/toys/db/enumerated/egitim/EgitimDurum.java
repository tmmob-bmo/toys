package tr.com.arf.toys.db.enumerated.egitim; 
 
import tr.com.arf.framework.db.enumerated._base.BaseEnumerated;
import tr.com.arf.framework.utility.tool.KeyUtil;
  
public enum EgitimDurum implements BaseEnumerated {  
    
		_NULL(0, ""), 
		_Planlanan(1, "egitimDurum.planlanan"),
		_Gerceklesen(2, "egitimDurum.gerceklesen"),
		_Iptal(3, "egitimDurum.iptal"); 
 
		private int code;
		private String label;

		private EgitimDurum(int code, String label) {
		    this.code = code;
		    this.label = label;
		}

		@Override
		public int getCode() {
		    return code;
		}

		@Override
		public String getLabel() {
		    return KeyUtil.getMessageValue(label);
		}

		@Override
		public String toString() {
		    return KeyUtil.getMessageValue(label);
		}

		public static EgitimDurum getWithCode(int code){
			EgitimDurum[] enumArray = EgitimDurum.values();
			for(EgitimDurum enumObj : enumArray){ 
				if(enumObj.getCode() == code) {
					return enumObj;
				}
			}
			return EgitimDurum._NULL;
		} 
}   
