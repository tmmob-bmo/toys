package tr.com.arf.toys.db.enumerated.evrak; 
 
import tr.com.arf.framework.db.enumerated._base.BaseEnumerated;
import tr.com.arf.framework.utility.tool.KeyUtil;
  
public enum GizlilikDerecesi implements BaseEnumerated {  
    
		_NULL(0, ""), 
		_OZEL(1, "gizlilikDerecesi.ozel"),
		_HIZMETEOZEL(2, "gizlilikDerecesi.hizmeteOzel"),
		_GIZLI(3, "gizlilikDerecesi.gizli"),
		_COKGIZLI(4, "gizlilikDerecesi.cokGizli"),
		_TASNIFDISI(5, "gizlilikDerecesi.tasnifDisi");  
				
		private int code;
		private String label;

		private GizlilikDerecesi(int code, String label) {
		    this.code = code;
		    this.label = label;
		}

		@Override
		public int getCode() {
		    return code;
		}

		@Override
		public String getLabel() {
		    return KeyUtil.getMessageValue(label);
		}

		@Override
		public String toString() {
		    return KeyUtil.getMessageValue(label);
		}

		public static GizlilikDerecesi getWithCode(int code){
			GizlilikDerecesi[] enumArray = GizlilikDerecesi.values();
			for(GizlilikDerecesi enumObj : enumArray){ 
				if(enumObj.getCode() == code) {
					return enumObj;
				}
			}
			return GizlilikDerecesi._NULL;
		} 
}   
