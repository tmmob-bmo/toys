package tr.com.arf.toys.db.enumerated.iletisim; 
 
import tr.com.arf.framework.db.enumerated._base.BaseEnumerated;
import tr.com.arf.framework.utility.tool.KeyUtil;
  
public enum GonderimDurum implements BaseEnumerated {  
    
		_NULL(0, ""), 
		_ILETILDI(1, "gonderimDurum.TIP1"),
		_BEKLEMEDE(2, "gonderimDurum.TIP2"),
		_ILETILMEDI(3, "gonderimDurum.TIP3"); 
 
		private int code;
		private String label;

		private GonderimDurum(int code, String label) {
		    this.code = code;
		    this.label = label;
		}

		@Override
		public int getCode() {
		    return code;
		}

		@Override
		public String getLabel() {
		    return KeyUtil.getMessageValue(label);
		}

		@Override
		public String toString() {
		    return KeyUtil.getMessageValue(label);
		}

		public static GonderimDurum getWithCode(int code){
			GonderimDurum[] enumArray = GonderimDurum.values();
			for(GonderimDurum enumObj : enumArray){ 
				if(enumObj.getCode() == code) {
					return enumObj;
				}
			}
			return GonderimDurum._NULL;
		} 
}   
