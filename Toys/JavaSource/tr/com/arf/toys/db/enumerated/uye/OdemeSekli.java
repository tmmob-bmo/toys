package tr.com.arf.toys.db.enumerated.uye;

import tr.com.arf.framework.db.enumerated._base.BaseEnumerated;
import tr.com.arf.framework.utility.tool.KeyUtil;

public enum OdemeSekli implements BaseEnumerated {

	_NULL(0, ""), _ELDEN(1, "odemeSekli.elden"), _BANKADAN(2, "odemeSekli.bankadan"), _ONLINE(3, "odemeSekli.online"), _KREDIKARTI(4, "odemeSekli.krediKarti");

	private int code;
	private String label;

	private OdemeSekli(int code, String label) {
		this.code = code;
		this.label = label;
	}

	@Override
	public int getCode() {
		return code;
	}

	@Override
	public String getLabel() {
		return KeyUtil.getMessageValue(label);
	}

	@Override
	public String toString() {
		return KeyUtil.getMessageValue(label);
	}

	public static OdemeSekli getWithCode(int code) {
		OdemeSekli[] enumArray = OdemeSekli.values();
		for (OdemeSekli enumObj : enumArray) {
			if (enumObj.getCode() == code) {
				return enumObj;
			}
		}
		return OdemeSekli._NULL;
	}

	public static OdemeSekli getWithLabel(String labelValue) {
		OdemeSekli[] enumArray = OdemeSekli.values();
		for (OdemeSekli enumObj : enumArray) {
			if (enumObj.getLabel().equalsIgnoreCase(labelValue)) {
				return enumObj;
			}
		}
		return OdemeSekli._NULL;
	}
}
