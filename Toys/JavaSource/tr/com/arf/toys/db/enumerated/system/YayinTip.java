package tr.com.arf.toys.db.enumerated.system; 
 
import tr.com.arf.framework.db.enumerated._base.BaseEnumerated;
import tr.com.arf.framework.utility.tool.KeyUtil;
  
public enum YayinTip implements BaseEnumerated {  
    
		_NULL(0, ""), 
		_DERGI(1, "yayinTip.dergi"),
		_BROSUR(2, "yayinTip.brosur");
 
		private int code;
		private String label;

		private YayinTip(int code, String label) {
		    this.code = code;
		    this.label = label;
		}

		@Override
		public int getCode() {
		    return code;
		}

		@Override
		public String getLabel() {
		    return KeyUtil.getMessageValue(label);
		}

		@Override
		public String toString() {
		    return KeyUtil.getMessageValue(label);
		}

		public static YayinTip getWithCode(int code){
			YayinTip[] enumArray = YayinTip.values();
			for(YayinTip enumObj : enumArray){ 
				if(enumObj.getCode() == code)
					return enumObj;
			}
			return YayinTip._NULL;
		} 
}   
