package tr.com.arf.toys.db.enumerated.uye; 
 
import tr.com.arf.framework.db.enumerated._base.BaseEnumerated;
import tr.com.arf.framework.utility.tool.KeyUtil;
  
public enum UyeDurum implements BaseEnumerated {  
    
		_NULL(0, ""), 		 
		_PASIFUYE(1, "uyeDurum.pasifUye"),
		_AKTIFUYE(2, "uyeDurum.uye"), 
		_ISSIZ(3, "uyeDurum.issiz"),
		_ASKER(4, "uyeDurum.asker"),
		_YURTDISI(5, "uyeDurum.yurtDisi"),
		_EMEKLI(6, "uyeDurum.emekli"),
		_VEFAT(7, "uyeDurum.vefat"),
		_DOGUMIZNI(8, "uyeDurum.dogumIzni"),
		_KAPALI(9, "uyeDurum.kapali"),
		_ISTIFA(10, "uyeDurum.istifa");
		
		private int code;
		private String label;

		private UyeDurum(int code, String label) {
		    this.code = code;
		    this.label = label;
		}

		@Override
		public int getCode() {
		    return code;
		}

		@Override
		public String getLabel() {
		    return KeyUtil.getMessageValue(label);
		}

		@Override
		public String toString() {
		    return KeyUtil.getMessageValue(label);
		}

		public static UyeDurum getWithCode(int code){
			UyeDurum[] enumArray = UyeDurum.values();
			for(UyeDurum enumObj : enumArray){ 
				if(enumObj.getCode() == code) {
					return enumObj;
				}
			}
			return UyeDurum._NULL;
		} 
}   
