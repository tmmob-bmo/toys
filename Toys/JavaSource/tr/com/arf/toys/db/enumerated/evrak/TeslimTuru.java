package tr.com.arf.toys.db.enumerated.evrak; 
 
import tr.com.arf.framework.db.enumerated._base.BaseEnumerated;
import tr.com.arf.framework.utility.tool.KeyUtil;
  
public enum TeslimTuru implements BaseEnumerated {  
    
		_NULL(0, ""), 
		_POSTA(1, "teslimTuru.posta"),
		_ELDENTESLIM(2, "teslimTuru.eldenTeslim"),
		_KARGO(3, "teslimTuru.kargo"),
		_FAKS(4, "teslimTuru.faks"),
		_EPOSTA(5, "teslimTuru.eposta"),
		_EPOSTAVEFAKS(6, "teslimTuru.epostaVeKargo"); 
  		
		private int code;
		private String label;

		private TeslimTuru(int code, String label) {
		    this.code = code;
		    this.label = label;
		}

		@Override
		public int getCode() {
		    return code;
		}

		@Override
		public String getLabel() {
		    return KeyUtil.getMessageValue(label);
		}

		@Override
		public String toString() {
		    return KeyUtil.getMessageValue(label);
		}

		public static TeslimTuru getWithCode(int code){
			TeslimTuru[] enumArray = TeslimTuru.values();
			for(TeslimTuru enumObj : enumArray){ 
				if(enumObj.getCode() == code) {
					return enumObj;
				}
			}
			return TeslimTuru._NULL;
		} 
}   
