package tr.com.arf.toys.db.enumerated.system; 
 
import tr.com.arf.framework.db.enumerated._base.BaseEnumerated;
import tr.com.arf.framework.utility.tool.KeyUtil;
  
public enum DemirbasTuru implements BaseEnumerated {  
    
		_NULL(0, ""), 
		_HIBE(1, "demirbasturu.hibe"),
		_SATINALMA(2, "demirbasturu.satinalma");
		
		
		private int code;
		private String label;

		private DemirbasTuru(int code, String label) {
		    this.code = code;
		    this.label = label;
		}

		@Override
		public int getCode() {
		    return code;
		}

		@Override
		public String getLabel() {
		    return KeyUtil.getMessageValue(label);
		}

		@Override
		public String toString() {
		    return KeyUtil.getMessageValue(label);
		}

		public static DemirbasTuru getWithCode(int code){
			DemirbasTuru[] enumArray = DemirbasTuru.values();
			for(DemirbasTuru enumObj : enumArray){ 
				if(enumObj.getCode() == code) {
					return enumObj;
				}
			}
			return DemirbasTuru._NULL;
		} 
}   
