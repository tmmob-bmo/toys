package tr.com.arf.toys.db.enumerated.uye; 
 
import tr.com.arf.framework.db.enumerated._base.BaseEnumerated;
import tr.com.arf.framework.utility.tool.KeyUtil;
  
public enum SiparisDurum implements BaseEnumerated {  
    
		_NULL(0, ""), 
		_YANITBEKLIYOR(1, "siparisDurum.yanitBekliyor"),
		_ONAYLANDI(2, "siparisDurum.onaylandi"),
		_ONAYLANMADI(3, "siparisDurum.onaylanmadi"); 
 
		private int code;
		private String label;

		private SiparisDurum(int code, String label) {
		    this.code = code;
		    this.label = label;
		}

		@Override
		public int getCode() {
		    return code;
		}

		@Override
		public String getLabel() {
		    return KeyUtil.getMessageValue(label);
		}

		@Override
		public String toString() {
		    return KeyUtil.getMessageValue(label);
		}

		public static SiparisDurum getWithCode(int code){
			SiparisDurum[] enumArray = SiparisDurum.values();
			for(SiparisDurum enumObj : enumArray){ 
				if(enumObj.getCode() == code) {
					return enumObj;
				}
			}
			return SiparisDurum._NULL;
		} 
}   
