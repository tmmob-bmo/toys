package tr.com.arf.toys.db.enumerated.system; 
 
import tr.com.arf.framework.db.enumerated._base.BaseEnumerated;
import tr.com.arf.framework.utility.tool.KeyUtil;
  
public enum UyariDurum implements BaseEnumerated {  
    
		_NULL(0, ""), 
		_AKTIF(1, "uyariDurum.aktif"),
		_IPTALEDILMIS(2, "uyariDurum.iptaledilmis"),
		_TAMAMLANMIS(3, "uyariDurum.tamamlanmis");
 
		private int code;
		private String label;

		private UyariDurum(int code, String label) {
		    this.code = code;
		    this.label = label;
		}

		@Override
		public int getCode() {
		    return code;
		}

		@Override
		public String getLabel() {
		    return KeyUtil.getMessageValue(label);
		}

		@Override
		public String toString() {
		    return KeyUtil.getMessageValue(label);
		}

		public static UyariDurum getWithCode(int code){
			UyariDurum[] enumArray = UyariDurum.values();
			for(UyariDurum enumObj : enumArray){ 
				if(enumObj.getCode() == code)
					return enumObj;
			}
			return UyariDurum._NULL;
		} 
}   
