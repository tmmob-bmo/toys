package tr.com.arf.toys.db.enumerated.iletisim; 
 
import tr.com.arf.framework.db.enumerated._base.BaseEnumerated;
import tr.com.arf.framework.utility.tool.KeyUtil;
  
public enum NetAdresTuru implements BaseEnumerated {  
    
		_NULL(0, ""), 
		_EPOSTA(1, "netAdresTuru.eposta"),
		_WEBSAYFASI(2, "netAdresTuru.websayfasi"),
		_LINKEDIN(3, "netAdresTuru.linkedin"),
		_FACEBOOK(4, "netAdresTuru.facebook"),
		_TWEETER(5, "netAdresTuru.tweeter"),
		_DIGER(6, "netAdresTuru.diger");
 
		private int code;
		private String label;

		private NetAdresTuru(int code, String label) {
		    this.code = code;
		    this.label = label;
		}

		@Override
		public int getCode() {
		    return code;
		}

		@Override
		public String getLabel() {
		    return KeyUtil.getMessageValue(label);
		}

		@Override
		public String toString() {
		    return KeyUtil.getMessageValue(label);
		}

		public static NetAdresTuru getWithCode(int code){
			NetAdresTuru[] enumArray = NetAdresTuru.values();
			for(NetAdresTuru enumObj : enumArray){ 
				if(enumObj.getCode() == code)
					return enumObj;
			}
			return NetAdresTuru._NULL;
		} 
}   
