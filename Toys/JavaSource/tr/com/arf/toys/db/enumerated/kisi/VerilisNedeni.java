package tr.com.arf.toys.db.enumerated.kisi; 
 
import tr.com.arf.framework.db.enumerated._base.BaseEnumerated;
import tr.com.arf.framework.utility.tool.KeyUtil;
  
public enum VerilisNedeni implements BaseEnumerated {  
    
		_NULL(0, ""), 
		_DOGUM(1, "verilisNedeni.dogum"),
		_YENILEME(2, "verilisNedeni.yenileme"),
		_KAYIP(3, "verilisNedeni.kayip");

	 
		private int code;
		private String label;

		private VerilisNedeni(int code, String label) {
		    this.code = code;
		    this.label = label;
		}

		@Override
		public int getCode() {
		    return code;
		}

		@Override
		public String getLabel() {
		    return KeyUtil.getMessageValue(label);
		}

		@Override
		public String toString() {
		    return KeyUtil.getMessageValue(label);
		}

		public static VerilisNedeni getWithCode(int code){
			VerilisNedeni[] enumArray = VerilisNedeni.values();
			for(VerilisNedeni enumObj : enumArray){ 
				if(enumObj.getCode() == code)
					return enumObj;
			}
			return VerilisNedeni._NULL;
		} 
}   
