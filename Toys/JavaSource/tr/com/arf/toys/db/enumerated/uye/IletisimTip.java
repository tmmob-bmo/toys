package tr.com.arf.toys.db.enumerated.uye; 
 
import tr.com.arf.framework.db.enumerated._base.BaseEnumerated;
import tr.com.arf.framework.utility.tool.KeyUtil;
  
public enum IletisimTip implements BaseEnumerated {  
    
		_NULL(0, ""), 
		_POSTA(1, "iletisimTip.posta"),
		_SMS(2, "iletisimTip.sms"),
		_EPOSTA(3, "iletisimTip.eposta");
 
		private int code;
		private String label;

		private IletisimTip(int code, String label) {
		    this.code = code;
		    this.label = label;
		}

		@Override
		public int getCode() {
		    return code;
		}

		@Override
		public String getLabel() {
		    return KeyUtil.getMessageValue(label);
		}

		@Override
		public String toString() {
		    return KeyUtil.getMessageValue(label);
		}

		public static IletisimTip getWithCode(int code){
			IletisimTip[] enumArray = IletisimTip.values();
			for(IletisimTip enumObj : enumArray){ 
				if(enumObj.getCode() == code)
					return enumObj;
			}
			return IletisimTip._NULL;
		} 
}   
