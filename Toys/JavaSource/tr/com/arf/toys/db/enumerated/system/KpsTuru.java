package tr.com.arf.toys.db.enumerated.system; 
 
import tr.com.arf.framework.db.enumerated._base.BaseEnumerated;
import tr.com.arf.framework.utility.tool.KeyUtil;
  
public enum KpsTuru implements BaseEnumerated {  
    
		_NULL(0, ""), 
		_KPSKIMLIK(1, "kpsturu.kpskimlik"),
		_KPSADRES(2, "kpsturu.kpsadres");
		
		
		private int code;
		private String label;

		private KpsTuru(int code, String label) {
		    this.code = code;
		    this.label = label;
		}

		@Override
		public int getCode() {
		    return code;
		}

		@Override
		public String getLabel() {
		    return KeyUtil.getMessageValue(label);
		}

		@Override
		public String toString() {
		    return KeyUtil.getMessageValue(label);
		}

		public static KpsTuru getWithCode(int code){
			KpsTuru[] enumArray = KpsTuru.values();
			for(KpsTuru enumObj : enumArray){ 
				if(enumObj.getCode() == code) {
					return enumObj;
				}
			}
			return KpsTuru._NULL;
		} 
}   
