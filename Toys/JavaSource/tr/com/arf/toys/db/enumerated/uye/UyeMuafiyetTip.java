package tr.com.arf.toys.db.enumerated.uye; 
 
import tr.com.arf.framework.db.enumerated._base.BaseEnumerated;
import tr.com.arf.framework.utility.tool.KeyUtil;
  
public enum UyeMuafiyetTip implements BaseEnumerated {  
    
		_NULL(0, ""), 
		_ASKERLIK(1, "uyeMuafiyetTip.askerlik"),
		_DOGUMIZNI(2, "uyeMuafiyetTip.dogumizni"),
		_OGRENCI(3, "uyeMuafiyetTip.ogrenci"),
		_ISSIZ(4, "uyeMuafiyetTip.issiz"),
		_YURTDISI(5, "uyeMuafiyetTip.yurtdisi"),
		_EMEKLI(6, "uyeMuafiyetTip.emekli"),
		_VEFAT(7, "uyeMuafiyetTip.vefat"),
		_UYELIKIPTAL(8, "uyeMuafiyetTip.uyelikIptal"),
		_ISTIFA(9, "uyeMuafiyetTip.istifa");

 
		private int code;
		private String label;

		private UyeMuafiyetTip(int code, String label) {
		    this.code = code;
		    this.label = label;
		}

		@Override
		public int getCode() {
		    return code;
		}

		@Override
		public String getLabel() {
		    return KeyUtil.getMessageValue(label);
		}

		@Override
		public String toString() {
		    return KeyUtil.getMessageValue(label);
		}

		public static UyeMuafiyetTip getWithCode(int code){
			UyeMuafiyetTip[] enumArray = UyeMuafiyetTip.values();
			for(UyeMuafiyetTip enumObj : enumArray){ 
				if(enumObj.getCode() == code) {
					return enumObj;
				}
			}
			return UyeMuafiyetTip._NULL;
		} 
}   
