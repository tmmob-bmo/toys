package tr.com.arf.toys.db.enumerated.kisi; 
 
import tr.com.arf.framework.db.enumerated._base.BaseEnumerated;
import tr.com.arf.framework.utility.tool.KeyUtil;
  
public enum KurulTuru implements BaseEnumerated {  
    
		_NULL(0, ""),
		_GENELKURUL(1, "kurulTuru.genelkurul"), 
		_YONETIMKURULU(2, "kurulTuru.yonetimkurulu"), 
		_ONURKURULU(3, "kurulTuru.onurkurulu"), 
		_DENETLEMEKURULU(4, "kurulTuru.denetlemekurulu"), 
		_DANISMAKURULU(5, "kurulTuru.danismakurulu"), 
		_KOORDINASYONKURULU(6, "kurulTuru.koordinasyonkurulu"), 
		_ILILCETEMSILCILIKKURULU(7, "kurulTuru.ililcetemsilcilikkurulu"), 
		_ISYERITEMSILCILIKKURULU(8, "kurulTuru.isyeritemsilcilikkurulu");  
 		
		private int code;
		private String label;

		private KurulTuru(int code, String label) {
		    this.code = code;
		    this.label = label;
		}

		@Override
		public int getCode() {
		    return code;
		}

		@Override
		public String getLabel() {
		    return KeyUtil.getMessageValue(label);
		}

		@Override
		public String toString() {
		    return KeyUtil.getMessageValue(label);
		}

		public static KurulTuru getWithCode(int code){
			KurulTuru[] enumArray = KurulTuru.values();
			for(KurulTuru enumObj : enumArray){ 
				if(enumObj.getCode() == code) {
					return enumObj;
				}
			}
			return KurulTuru._NULL;
		} 
		
}   
