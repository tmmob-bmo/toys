package tr.com.arf.toys.db.enumerated.egitim; 
 
import tr.com.arf.framework.db.enumerated._base.BaseEnumerated;
import tr.com.arf.framework.utility.tool.KeyUtil;
  
public enum Cevap implements BaseEnumerated {  
    
		_NULL(0, ""), 
		_TIP1(1, "cevap.TIP1"),
		_TIP2(2, "cevap.TIP2"); 
 
		private int code;
		private String label;

		private Cevap(int code, String label) {
		    this.code = code;
		    this.label = label;
		}

		@Override
		public int getCode() {
		    return code;
		}

		@Override
		public String getLabel() {
		    return KeyUtil.getMessageValue(label);
		}

		@Override
		public String toString() {
		    return KeyUtil.getMessageValue(label);
		}

		public static Cevap getWithCode(int code){
			Cevap[] enumArray = Cevap.values();
			for(Cevap enumObj : enumArray){ 
				if(enumObj.getCode() == code) {
					return enumObj;
				}
			}
			return Cevap._NULL;
		} 
}   
