package tr.com.arf.toys.db.enumerated.uye; 
 
import tr.com.arf.framework.db.enumerated._base.BaseEnumerated;
import tr.com.arf.framework.utility.tool.KeyUtil;
  
public enum UyeAboneDurum implements BaseEnumerated {  
    
		_NULL(0, ""), 
		_TASLAK(1, "uyeAboneDurum.taslak"),
		_AKTIF(2, "uyeAboneDurum.aktif"),
		_IPTALEDILMIS(3, "uyeAboneDurum.iptaledilmis");
 
		private int code;
		private String label;

		private UyeAboneDurum(int code, String label) {
		    this.code = code;
		    this.label = label;
		}

		@Override
		public int getCode() {
		    return code;
		}

		@Override
		public String getLabel() {
		    return KeyUtil.getMessageValue(label);
		}

		@Override
		public String toString() {
		    return KeyUtil.getMessageValue(label);
		}

		public static UyeAboneDurum getWithCode(int code){
			UyeAboneDurum[] enumArray = UyeAboneDurum.values();
			for(UyeAboneDurum enumObj : enumArray){ 
				if(enumObj.getCode() == code)
					return enumObj;
			}
			return UyeAboneDurum._NULL;
		} 
}   
