package tr.com.arf.toys.db.enumerated.uye;

import tr.com.arf.framework.db.enumerated._base.BaseEnumerated;
import tr.com.arf.framework.utility.tool.KeyUtil;

public enum Formturu implements BaseEnumerated {

	_NULL(0, ""), _UYEIMZALIFORM(1, KeyUtil.getMessageValue("formturu.uyeimzaliform")), _DIPLOMAFOTOKOPISI(2, KeyUtil.getMessageValue("formturu.diplomafotokopisi")), _NUFUSCUZDANIFOTOKOPISI(
			3, KeyUtil.getMessageValue("formturu.nufuscuzdanifotokopisi"));

	private int code;
	private String label;

	private Formturu(int code, String label) {
		this.code = code;
		this.label = label;
	}

	@Override
	public int getCode() {
		return code;
	}

	@Override
	public String getLabel() {
		return label;
	}

	@Override
	public String toString() {
		return label;
	}

	public static Formturu getWithCode(int code) {
		Formturu[] enumArray = Formturu.values();
		for (Formturu enumObj : enumArray) {
			if (enumObj.getCode() == code) {
				return enumObj;
			}
		}
		return Formturu._NULL;
	}
}
