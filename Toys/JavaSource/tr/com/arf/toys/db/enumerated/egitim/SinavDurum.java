package tr.com.arf.toys.db.enumerated.egitim; 
 
import tr.com.arf.framework.db.enumerated._base.BaseEnumerated;
import tr.com.arf.framework.utility.tool.KeyUtil;
  
public enum SinavDurum implements BaseEnumerated {  
    
		_NULL(0, ""), 
		_AKTIF(1, "sinavDurum.aktif"),
		_IPTALEDILMIS(2, "sinavDurum.iptaledilmis");
 
		private int code;
		private String label;

		private SinavDurum(int code, String label) {
		    this.code = code;
		    this.label = label;
		}

		@Override
		public int getCode() {
		    return code;
		}

		@Override
		public String getLabel() {
		    return KeyUtil.getMessageValue(label);
		}

		@Override
		public String toString() {
		    return KeyUtil.getMessageValue(label);
		}

		public static SinavDurum getWithCode(int code){
			SinavDurum[] enumArray = SinavDurum.values();
			for(SinavDurum enumObj : enumArray){ 
				if(enumObj.getCode() == code) {
					return enumObj;
				}
			}
			return SinavDurum._NULL;
		} 
}   
