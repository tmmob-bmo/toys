package tr.com.arf.toys.db.enumerated.system; 
 
import tr.com.arf.framework.db.enumerated._base.BaseEnumerated;
import tr.com.arf.framework.utility.tool.KeyUtil;
  
public enum UyariTipi implements BaseEnumerated {  
    
		_NULL(0, ""), 
		_HATIRLATMA(1, "uyariTipi.hatirlatma"),
		_NOT(2, "uyariTipi.not");
 
		private int code;
		private String label;

		private UyariTipi(int code, String label) {
		    this.code = code;
		    this.label = label;
		}

		@Override
		public int getCode() {
		    return code;
		}

		@Override
		public String getLabel() {
		    return KeyUtil.getMessageValue(label);
		}

		@Override
		public String toString() {
		    return KeyUtil.getMessageValue(label);
		}

		public static UyariTipi getWithCode(int code){
			UyariTipi[] enumArray = UyariTipi.values();
			for(UyariTipi enumObj : enumArray){ 
				if(enumObj.getCode() == code)
					return enumObj;
			}
			return UyariTipi._NULL;
		} 
}   
