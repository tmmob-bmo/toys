package tr.com.arf.toys.db.enumerated.uye; 
 
import tr.com.arf.framework.db.enumerated._base.BaseEnumerated;
import tr.com.arf.framework.utility.tool.KeyUtil;
  
public enum UyekurumDurum implements BaseEnumerated {  
    
		_NULL(0, ""), 
		_CALISIYOR(1, "uyekurumDurum.calisiyor"),
		_AYRILMIS(2, "uyekurumDurum.ayrilmis");
 
		private int code;
		private String label;

		private UyekurumDurum(int code, String label) {
		    this.code = code;
		    this.label = label;
		}

		@Override
		public int getCode() {
		    return code;
		}

		@Override
		public String getLabel() {
		    return KeyUtil.getMessageValue(label);
		}

		@Override
		public String toString() {
		    return KeyUtil.getMessageValue(label);
		}

		public static UyekurumDurum getWithCode(int code){
			UyekurumDurum[] enumArray = UyekurumDurum.values();
			for(UyekurumDurum enumObj : enumArray){ 
				if(enumObj.getCode() == code) {
					return enumObj;
				}
			}
			return UyekurumDurum._NULL;
		} 
}   
