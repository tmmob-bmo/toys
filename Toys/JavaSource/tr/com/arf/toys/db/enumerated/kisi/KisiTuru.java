package tr.com.arf.toys.db.enumerated.kisi; 
 
import tr.com.arf.framework.db.enumerated._base.BaseEnumerated;
import tr.com.arf.framework.utility.tool.KeyUtil;
  
public enum KisiTuru implements BaseEnumerated {  
    
		_NULL(0, ""), 
		_UYE(1, "kisiTuru.uye"),
		_SMM(2, "kisiTuru.smm"),
		_DISSAHIS(3, "kisiTuru.dissahis"),
		_PERSONEL(4, "kisiTuru.personel");
	
		private int code;
		private String label;

		private KisiTuru(int code, String label) {
		    this.code = code;
		    this.label = label;
		}

		@Override
		public int getCode() {
		    return code;
		}

		@Override
		public String getLabel() {
		    return KeyUtil.getMessageValue(label);
		}

		@Override
		public String toString() {
		    return KeyUtil.getMessageValue(label);
		}

		public static KisiTuru getWithCode(int code){
			KisiTuru[] enumArray = KisiTuru.values();
			for(KisiTuru enumObj : enumArray){ 
				if(enumObj.getCode() == code)
					return enumObj;
			}
			return KisiTuru._NULL;
		} 
		
}   
