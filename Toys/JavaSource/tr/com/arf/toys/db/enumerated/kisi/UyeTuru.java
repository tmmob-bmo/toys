package tr.com.arf.toys.db.enumerated.kisi; 
 
import tr.com.arf.framework.db.enumerated._base.BaseEnumerated;
import tr.com.arf.framework.utility.tool.KeyUtil;

public enum UyeTuru implements BaseEnumerated { 

	_NULL(0, ""),
	_ASIL(1, "uyeTuru.asil"), 
	_YEDEK(2, "uyeTuru.yedek"); 

		private int code;
		private String label;

		private UyeTuru(int code, String label) {
		    this.code = code;
		    this.label = label;
		}

		@Override
		public int getCode() {
		    return code;
		}

		@Override
		public String getLabel() {
		    return KeyUtil.getMessageValue(label);
		}

		@Override
		public String toString() {
		    return KeyUtil.getMessageValue(label);
		}

		public static UyeTuru getWithCode(int code){
			UyeTuru[] enumArray = UyeTuru.values();
			for(UyeTuru enumObj : enumArray){ 
				if(enumObj.getCode() == code) {
					return enumObj;
				}
			}
			return UyeTuru._NULL;
		} 
}   