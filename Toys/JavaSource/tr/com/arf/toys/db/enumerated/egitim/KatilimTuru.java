package tr.com.arf.toys.db.enumerated.egitim; 
 
import tr.com.arf.framework.db.enumerated._base.BaseEnumerated;
import tr.com.arf.framework.utility.tool.KeyUtil;
  
public enum KatilimTuru implements BaseEnumerated {  
    
		_NULL(0, ""), 
		_KATILIMCI(1, "katilimTuru.katilimci"),
		_DINLEYICI(2, "katilimTuru.dinleyici"); 
 
		private int code;
		private String label;

		private KatilimTuru(int code, String label) {
		    this.code = code;
		    this.label = label;
		}

		@Override
		public int getCode() {
		    return code;
		}

		@Override
		public String getLabel() {
		    return KeyUtil.getMessageValue(label);
		}

		@Override
		public String toString() {
		    return KeyUtil.getMessageValue(label);
		}

		public static KatilimTuru getWithCode(int code){
			KatilimTuru[] enumArray = KatilimTuru.values();
			for(KatilimTuru enumObj : enumArray){ 
				if(enumObj.getCode() == code) {
					return enumObj;
				}
			}
			return KatilimTuru._NULL;
		} 
}   
