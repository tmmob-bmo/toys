package tr.com.arf.toys.db.enumerated.iletisim; 
 
import tr.com.arf.framework.db.enumerated._base.BaseEnumerated;
import tr.com.arf.framework.utility.tool.KeyUtil;
  
public enum AdresTuru implements BaseEnumerated {  
    
		_NULL(0, ""),
		_EVADRESI(1, "adresTuru.evadresi"),
		_ISADRESI(2, "adresTuru.isadresi"),
		_KPSADRESI(3, "adresTuru.kpsAdresi"),
		_KPSDIGERADRESI(4, "adresTuru.kpsDigerAdresi");
 
		private int code;
		private String label;

		private AdresTuru(int code, String label) {
		    this.code = code;
		    this.label = label;
		}

		@Override
		public int getCode() {
		    return code;
		}

		@Override
		public String getLabel() {
		    return KeyUtil.getMessageValue(label);
		}

		@Override
		public String toString() {
		    return KeyUtil.getMessageValue(label);
		}

		public static AdresTuru getWithCode(int code){
			AdresTuru[] enumArray = AdresTuru.values();
			for(AdresTuru enumObj : enumArray){ 
				if(enumObj.getCode() == code) {
					return enumObj;
				}
			}
			return AdresTuru._NULL;
		} 
}   
