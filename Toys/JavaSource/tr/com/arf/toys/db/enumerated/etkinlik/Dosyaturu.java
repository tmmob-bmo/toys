package tr.com.arf.toys.db.enumerated.etkinlik; 
 
import tr.com.arf.framework.db.enumerated._base.BaseEnumerated;
import tr.com.arf.framework.utility.tool.KeyUtil;
  
public enum Dosyaturu implements BaseEnumerated {  
    
		_NULL(0, ""), 
		_BILDIRIOZETI(1, "dosyaturu.bildiriozeti"),
		_BILDIRI(2, "dosyaturu.bildiri"),
		_SUNUM(3, "dosyaturu.sunum"); 
 
		private int code;
		private String label;

		private Dosyaturu(int code, String label) {
		    this.code = code;
		    this.label = label;
		}

		@Override
		public int getCode() {
		    return code;
		}

		@Override
		public String getLabel() {
		    return KeyUtil.getMessageValue(label);
		}

		@Override
		public String toString() {
		    return KeyUtil.getMessageValue(label);
		}

		public static Dosyaturu getWithCode(int code){
			Dosyaturu[] enumArray = Dosyaturu.values();
			for(Dosyaturu enumObj : enumArray){ 
				if(enumObj.getCode() == code) {
					return enumObj;
				}
			}
			return Dosyaturu._NULL;
		} 
}   
