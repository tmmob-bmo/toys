package tr.com.arf.toys.db.enumerated.kisi; 
 
import tr.com.arf.framework.db.enumerated._base.BaseEnumerated;
import tr.com.arf.framework.utility.tool.KeyUtil;
  
public enum KisiUyrukTip implements BaseEnumerated {  
    
		_NULL(0, ""), 
		_TCVATANDASI(1, "kisiUyrukTip.t.c.vatandasi"),
		_YABANCI(2, "kisiUyrukTip.yabanci"),
		_TURKKOKENLI(3, "kisiUyrukTip.turkkokenli"),
		_CIFTUYRUKLU(4, "kisiUyrukTip.ciftuyruklu");
 
		private int code;
		private String label;

		private KisiUyrukTip(int code, String label) {
		    this.code = code;
		    this.label = label;
		}

		@Override
		public int getCode() {
		    return code;
		}

		@Override
		public String getLabel() {
		    return KeyUtil.getMessageValue(label);
		}

		@Override
		public String toString() {
		    return KeyUtil.getMessageValue(label);
		}

		public static KisiUyrukTip getWithCode(int code){
			KisiUyrukTip[] enumArray = KisiUyrukTip.values();
			for(KisiUyrukTip enumObj : enumArray){ 
				if(enumObj.getCode() == code)
					return enumObj;
			}
			return KisiUyrukTip._NULL;
		} 
}   
