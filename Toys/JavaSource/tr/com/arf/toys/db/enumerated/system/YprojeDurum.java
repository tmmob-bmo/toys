package tr.com.arf.toys.db.enumerated.system;

import tr.com.arf.framework.db.enumerated._base.BaseEnumerated;
import tr.com.arf.framework.utility.tool.KeyUtil;

public enum YprojeDurum implements BaseEnumerated {

	_NULL(0, ""), 
	_TASLAK(1, "birimDurum.taslak"),
	_ACIK(2, "birimDurum.acik"),
	_KAPALI(3, "birimDurum.kapali");
 
	private int code;
	private String label;

	private YprojeDurum(int code, String label) {
		this.code = code;
		this.label = label;
	}

	@Override
	public int getCode() {
		return code;
	}

	@Override
	public String getLabel() {
		return KeyUtil.getMessageValue(label);
	}

	@Override
	public String toString() {
		return KeyUtil.getMessageValue(label);
	}
}
