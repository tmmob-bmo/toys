package tr.com.arf.toys.db.enumerated.kisi; 
 
import tr.com.arf.framework.db.enumerated._base.BaseEnumerated;
import tr.com.arf.framework.utility.tool.KeyUtil;
  
public enum KanGrubu implements BaseEnumerated {  
    
		_NULL(0, ""), 
		_0RH_POZITIF(1, "kanGrubu.0rh_Pozitif"),
		_0RH_NEGATIF(2, "kanGrubu.0rh_Negatif"),
		_ARH_POZITIF(3, "kanGrubu.arh_Pozitif"),
		_ARH_NEGATIF(4, "kanGrubu.arh_Negatif"),
		_BRH_POZITIF(5, "kanGrubu.brh_Pozitif"),
		_BRH_NEGATIF(6, "kanGrubu.brh_Negatif"),
		_ABRH_POZITIF(7, "kanGrubu.abrh_Pozitif"),
		_ABRH_NEGATIF(8, "kanGrubu.abrh_Negatif");
	 
		private int code;
		private String label;

		private KanGrubu(int code, String label) {
		    this.code = code;
		    this.label = label;
		}

		@Override
		public int getCode() {
		    return code;
		}

		@Override
		public String getLabel() {
		    return KeyUtil.getMessageValue(label);
		}

		@Override
		public String toString() {
		    return KeyUtil.getMessageValue(label);
		}

		public static KanGrubu getWithCode(int code){
			KanGrubu[] enumArray = KanGrubu.values();
			for(KanGrubu enumObj : enumArray){ 
				if(enumObj.getCode() == code)
					return enumObj;
			}
			return KanGrubu._NULL;
		} 
}   
