package tr.com.arf.toys.db.enumerated.egitim; 
 
import tr.com.arf.framework.db.enumerated._base.BaseEnumerated;
import tr.com.arf.framework.utility.tool.KeyUtil;
  
public enum EtkinDurum implements BaseEnumerated {  
    
		_NULL(0, ""), 
		_ETKIN(1, "etkinDurum.etkin"),
		_ETKINDEGIL(2, "etkinDurum.etkindegil");
 
		private int code;
		private String label;

		private EtkinDurum(int code, String label) {
		    this.code = code;
		    this.label = label;
		}

		@Override
		public int getCode() {
		    return code;
		}

		@Override
		public String getLabel() {
		    return KeyUtil.getMessageValue(label);
		}

		@Override
		public String toString() {
		    return KeyUtil.getMessageValue(label);
		}

		public static EtkinDurum getWithCode(int code){
			EtkinDurum[] enumArray = EtkinDurum.values();
			for(EtkinDurum enumObj : enumArray){ 
				if(enumObj.getCode() == code) {
					return enumObj;
				}
			}
			return EtkinDurum._NULL;
		} 
}   
