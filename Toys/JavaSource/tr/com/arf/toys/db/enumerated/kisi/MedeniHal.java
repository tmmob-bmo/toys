package tr.com.arf.toys.db.enumerated.kisi; 
 
import tr.com.arf.framework.db.enumerated._base.BaseEnumerated;
import tr.com.arf.framework.utility.tool.KeyUtil;
  
public enum MedeniHal implements BaseEnumerated {  
    
		_BILINMIYOR(0, "medeniHal.bilinmiyor"), 
		_BEKAR(1, "medeniHal.bekar"),
		_EVLI(2, "medeniHal.evli"),
		_BOSANMIS(3, "medeniHal.bosanmis"),
		_DUL(4, "medeniHal.dul"),
		_EVLILIGINFESHI(5, "medeniHal.evliliginfeshi"),
		_EVLILIGINIPTALI(6, "medeniHal.evliliginiptali");
		 
	 
		private int code;
		private String label;

		private MedeniHal(int code, String label) {
		    this.code = code;
		    this.label = label;
		}

		@Override
		public int getCode() {
		    return code;
		}

		@Override
		public String getLabel() {
		    return KeyUtil.getMessageValue(label);
		}

		@Override
		public String toString() {
		    return KeyUtil.getMessageValue(label);
		}

		public static MedeniHal getWithCode(int code){
			MedeniHal[] enumArray = MedeniHal.values();
			for(MedeniHal enumObj : enumArray){ 
				if(enumObj.getCode() == code) {
					return enumObj;
				}
			}
			return MedeniHal._BILINMIYOR;
		} 
}   
