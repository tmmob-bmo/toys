package tr.com.arf.toys.db.enumerated.uye; 
 
import tr.com.arf.framework.db.enumerated._base.BaseEnumerated;
import tr.com.arf.framework.utility.tool.KeyUtil;
  
public enum UyeAidatDurum implements BaseEnumerated {  
    
		_NULL(0, ""), 
		_BORCLU(1, "uyeAidatDurum.borclu"),
		_ODENMIS(2, "uyeAidatDurum.odenmis"),
		_MUAF(3, "uyeAidatDurum.muaf"),
		_ALACAKLI(4, "uyeAidatDurum.alacakli");

 
		private int code;
		private String label;

		private UyeAidatDurum(int code, String label) {
		    this.code = code;
		    this.label = label;
		}

		@Override
		public int getCode() {
		    return code;
		}

		@Override
		public String getLabel() {
		    return KeyUtil.getMessageValue(label);
		}

		@Override
		public String toString() {
		    return KeyUtil.getMessageValue(label);
		}

		public static UyeAidatDurum getWithCode(int code){
			UyeAidatDurum[] enumArray = UyeAidatDurum.values();
			for(UyeAidatDurum enumObj : enumArray){ 
				if(enumObj.getCode() == code) {
					return enumObj;
				}
			}
			return UyeAidatDurum._NULL;
		} 
}   
