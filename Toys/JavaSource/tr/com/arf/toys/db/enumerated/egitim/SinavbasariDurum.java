package tr.com.arf.toys.db.enumerated.egitim; 
 
import tr.com.arf.framework.db.enumerated._base.BaseEnumerated;
import tr.com.arf.framework.utility.tool.KeyUtil;
  
public enum SinavbasariDurum implements BaseEnumerated {  
    
		_NULL(0, ""), 
		_BASARILI(1, "sinavbasariDurum.basarili"),
		_BASARISIZ(2, "sinavbasariDurum.basarisiz"); 
 
		private int code;
		private String label;

		private SinavbasariDurum(int code, String label) {
		    this.code = code;
		    this.label = label;
		}

		@Override
		public int getCode() {
		    return code;
		}

		@Override
		public String getLabel() {
		    return KeyUtil.getMessageValue(label);
		}

		@Override
		public String toString() {
		    return KeyUtil.getMessageValue(label);
		}

		public static SinavbasariDurum getWithCode(int code){
			SinavbasariDurum[] enumArray = SinavbasariDurum.values();
			for(SinavbasariDurum enumObj : enumArray){ 
				if(enumObj.getCode() == code) {
					return enumObj;
				}
			}
			return SinavbasariDurum._NULL;
		} 
}   
