package tr.com.arf.toys.db.enumerated.uye; 
 
import tr.com.arf.framework.db.enumerated._base.BaseEnumerated;
import tr.com.arf.framework.utility.tool.KeyUtil;
  
public enum UyeRaporlamaAboneDurum implements BaseEnumerated {  
    
		_NULL(0, ""),
		_ABONELIGIYOK(1, "uyeRaporlamaAboneDurum.aboneligiyok"), 
		_ABONELIKLERVAR(2, "uyeRaporlamaAboneDurum.abonelikvar"), 
		_IPTALEDILMISABONELIGIVAR(3, "uyeRaporlamaAboneDurum.i̇ptaledilmisaboneligivar"); 
 
		private int code;
		private String label;

		private UyeRaporlamaAboneDurum(int code, String label) {
		    this.code = code;
		    this.label = label;
		}

		@Override
		public int getCode() {
		    return code;
		}

		@Override
		public String getLabel() {
			return KeyUtil.getMessageValue(label);
		}

		@Override
		public String toString() {
		    return KeyUtil.getMessageValue(label);
		}

		public static UyeRaporlamaAboneDurum getWithCode(int code){
			UyeRaporlamaAboneDurum[] enumArray = UyeRaporlamaAboneDurum.values();
			for(UyeRaporlamaAboneDurum enumObj : enumArray){ 
				if(enumObj.getCode() == code) {
					return enumObj;
				}
			}
			return UyeRaporlamaAboneDurum._NULL;
		} 
}   
