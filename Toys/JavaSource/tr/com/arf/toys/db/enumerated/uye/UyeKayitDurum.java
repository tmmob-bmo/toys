package tr.com.arf.toys.db.enumerated.uye; 
 
import tr.com.arf.framework.db.enumerated._base.BaseEnumerated;
import tr.com.arf.framework.utility.tool.KeyUtil;
  
public enum UyeKayitDurum implements BaseEnumerated {  
    
		_NULL(0, ""), 
		_TASLAK(1, "uyeKayitDurum.taslak"),
		_INCELEMEDE(2, "uyeKayitDurum.incelemede"),
		_ONAYLANMIS(3, "uyeKayitDurum.onaylanmis");  
 
		private int code;
		private String label;

		private UyeKayitDurum(int code, String label) {
		    this.code = code;
		    this.label = label;
		}

		@Override
		public int getCode() {
		    return code;
		}

		@Override
		public String getLabel() {
		    return KeyUtil.getMessageValue(label);
		}

		@Override
		public String toString() {
		    return KeyUtil.getMessageValue(label);
		}

		public static UyeKayitDurum getWithCode(int code){
			UyeKayitDurum[] enumArray = UyeKayitDurum.values();
			for(UyeKayitDurum enumObj : enumArray){ 
				if(enumObj.getCode() == code) {
					return enumObj;
				}
			}
			return UyeKayitDurum._NULL;
		} 
}   
