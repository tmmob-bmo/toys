package tr.com.arf.toys.db.enumerated.egitim; 
 
import tr.com.arf.framework.db.enumerated._base.BaseEnumerated;
import tr.com.arf.framework.utility.tool.KeyUtil;
  
public enum Egitimturu implements BaseEnumerated {  
    
		_NULL(0, ""), 
		_Egitim(1, "egitimturu.Egitim"),
		_Kurs(2, "egitimturu.Kurs"),
		_Seminer(3, "egitimturu.Seminer"); 
 
		private int code;
		private String label;

		private Egitimturu(int code, String label) {
		    this.code = code;
		    this.label = label;
		}

		@Override
		public int getCode() {
		    return code;
		}

		@Override
		public String getLabel() {
		    return KeyUtil.getMessageValue(label);
		}

		@Override
		public String toString() {
		    return KeyUtil.getMessageValue(label);
		}

		public static Egitimturu getWithCode(int code){
			Egitimturu[] enumArray = Egitimturu.values();
			for(Egitimturu enumObj : enumArray){ 
				if(enumObj.getCode() == code) {
					return enumObj;
				}
			}
			return Egitimturu._NULL;
		} 
}   
