package tr.com.arf.toys.db.enumerated.etkinlik; 
 
import tr.com.arf.framework.db.enumerated._base.BaseEnumerated;
import tr.com.arf.framework.utility.tool.KeyUtil;
  
public enum Uyeturu implements BaseEnumerated {  
    
		_NULL(0, ""), 
		_BASKAN(1, "uyeturu.baskan"),
		_UYE(2, "uyeturu.uye");
 
		private int code;
		private String label;

		private Uyeturu(int code, String label) {
		    this.code = code;
		    this.label = label;
		}

		@Override
		public int getCode() {
		    return code;
		}

		@Override
		public String getLabel() {
		    return KeyUtil.getMessageValue(label);
		}

		@Override
		public String toString() {
		    return KeyUtil.getMessageValue(label);
		}

		public static Uyeturu getWithCode(int code){
			Uyeturu[] enumArray = Uyeturu.values();
			for(Uyeturu enumObj : enumArray){ 
				if(enumObj.getCode() == code) {
					return enumObj;
				}
			}
			return Uyeturu._NULL;
		} 
}   
