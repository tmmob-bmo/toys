package tr.com.arf.toys.db.enumerated.uye; 
 
import tr.com.arf.framework.db.enumerated._base.BaseEnumerated;
import tr.com.arf.framework.utility.tool.KeyUtil;
  
public enum UyeBelgeDurum implements BaseEnumerated {  
    
		_NULL(0, ""), 
		_TASLAK(1, "uyeBelgeDurum.taslak"),
		_TALEPEDILMIS(2, "uyeBelgeDurum.talepedilmis"),
		_HAZIRLANMIS(3, "uyeBelgeDurum.hazirlanmis"),
		_TESLIMEDILMIS(4, "uyeBelgeDurum.teslimedilmis");
 
		private int code;
		private String label;

		private UyeBelgeDurum(int code, String label) {
		    this.code = code;
		    this.label = label;
		}

		@Override
		public int getCode() {
		    return code;
		}

		@Override
		public String getLabel() {
		    return KeyUtil.getMessageValue(label);
		}

		@Override
		public String toString() {
		    return KeyUtil.getMessageValue(label);
		}

		public static UyeBelgeDurum getWithCode(int code){
			UyeBelgeDurum[] enumArray = UyeBelgeDurum.values();
			for(UyeBelgeDurum enumObj : enumArray){ 
				if(enumObj.getCode() == code)
					return enumObj;
			}
			return UyeBelgeDurum._NULL;
		} 
}   
