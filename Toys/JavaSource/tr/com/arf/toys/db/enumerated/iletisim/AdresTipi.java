package tr.com.arf.toys.db.enumerated.iletisim; 
 
import tr.com.arf.framework.db.enumerated._base.BaseEnumerated;
import tr.com.arf.framework.utility.tool.KeyUtil;
  
public enum AdresTipi implements BaseEnumerated {  
    
		_NULL(0, ""), 
		_ILILCEMERKEZI(1, "adresTipi.ililcemerkezi"),
		_BELEDIYEADRESI(2, "adresTipi.belediyeadresi"),
		_KOYADRESI(3, "adresTipi.koyadresi"),
		_YURTDISIADRESI(4, "adresTipi.yurtdisiadresi");
 
		private int code;
		private String label;

		private AdresTipi(int code, String label) {
		    this.code = code;
		    this.label = label;
		}

		@Override
		public int getCode() {
		    return code;
		}

		@Override
		public String getLabel() {
		    return KeyUtil.getMessageValue(label);
		}

		@Override
		public String toString() {
		    return KeyUtil.getMessageValue(label);
		}

		public static AdresTipi getWithCode(int code){
			AdresTipi[] enumArray = AdresTipi.values();
			for(AdresTipi enumObj : enumArray){ 
				if(enumObj.getCode() == code)
					return enumObj;
			}
			return AdresTipi._NULL;
		} 
		
}   
