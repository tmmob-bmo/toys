package tr.com.arf.toys.db.enumerated.uye; 
 
import tr.com.arf.framework.db.enumerated._base.BaseEnumerated;
import tr.com.arf.framework.utility.tool.KeyUtil;
  
public enum SigortaTip implements BaseEnumerated {  
    
		_NULL(0, ""), 
		_HAYATSIGORTASI(1, "sigortaTip.hayatsigortasi");
 
		private int code;
		private String label;

		private SigortaTip(int code, String label) {
		    this.code = code;
		    this.label = label;
		}

		@Override
		public int getCode() {
		    return code;
		}

		@Override
		public String getLabel() {
		    return KeyUtil.getMessageValue(label);
		}

		@Override
		public String toString() {
		    return KeyUtil.getMessageValue(label);
		}

		public static SigortaTip getWithCode(int code){
			SigortaTip[] enumArray = SigortaTip.values();
			for(SigortaTip enumObj : enumArray){ 
				if(enumObj.getCode() == code)
					return enumObj;
			}
			return SigortaTip._NULL;
		} 
}   
