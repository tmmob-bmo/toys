package tr.com.arf.toys.db.enumerated.etkinlik; 
 
import tr.com.arf.framework.db.enumerated._base.BaseEnumerated;
import tr.com.arf.framework.utility.tool.KeyUtil;
  
public enum EtkinlikDurum implements BaseEnumerated {  
    
		_NULL(0, ""), 
		_PLANLANMIS(1, "etkinlikDurum.planlanmis"),
		_GERCEKLESTIRILMIS(2, "etkinlikDurum.gerceklestirilmis"),
		_IPTALEDILMIS(3, "etkinlikDurum.iptaledilmis"); 
 
		private int code;
		private String label;

		private EtkinlikDurum(int code, String label) {
		    this.code = code;
		    this.label = label;
		}

		@Override
		public int getCode() {
		    return code;
		}

		@Override
		public String getLabel() {
		    return KeyUtil.getMessageValue(label);
		}

		@Override
		public String toString() {
		    return KeyUtil.getMessageValue(label);
		}

		public static EtkinlikDurum getWithCode(int code){
			EtkinlikDurum[] enumArray = EtkinlikDurum.values();
			for(EtkinlikDurum enumObj : enumArray){ 
				if(enumObj.getCode() == code) {
					return enumObj;
				}
			}
			return EtkinlikDurum._NULL;
		} 
}   
