package tr.com.arf.toys.db.enumerated.system; 
 
import tr.com.arf.framework.db.enumerated._base.BaseEnumerated;
import tr.com.arf.framework.utility.tool.KeyUtil;
  
public enum KomisyonDurum implements BaseEnumerated {  
    
		_NULL(0, ""), 
		_TASLAK(1, "komisyonDurum.taslak"),
		_ACIK(2, "komisyonDurum.acik"),
		_KAPALI(3, "komisyonDurum.kapali");
 
		private int code;
		private String label;

		private KomisyonDurum(int code, String label) {
		    this.code = code;
		    this.label = label;
		}

		@Override
		public int getCode() {
		    return code;
		}

		@Override
		public String getLabel() {
		    return KeyUtil.getMessageValue(label);
		}

		@Override
		public String toString() {
		    return KeyUtil.getMessageValue(label);
		}

		public static KomisyonDurum getWithCode(int code){
			KomisyonDurum[] enumArray = KomisyonDurum.values();
			for(KomisyonDurum enumObj : enumArray){ 
				if(enumObj.getCode() == code) {
					return enumObj;
				}
			}
			return KomisyonDurum._NULL;
		} 
}   
