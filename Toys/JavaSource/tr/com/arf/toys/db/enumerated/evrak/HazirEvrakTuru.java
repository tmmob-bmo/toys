package tr.com.arf.toys.db.enumerated.evrak;
 
import tr.com.arf.framework.db.enumerated._base.BaseEnumerated;
import tr.com.arf.framework.utility.tool.KeyUtil;

public enum HazirEvrakTuru implements BaseEnumerated {  

		_NULL(0, ""),
		_GENELEVRAK(1, "hazirEvrakTuru.genelevrak"), 
		_UYELIKAYRILMATALEPCEVABI(2, "hazirEvrakTuru.uyelikayrilmatalepcevabi"),
		_BILIRKISILIKCEVABI(3, "hazirEvrakTuru.bilirkisilikcevabi"),
		_BILIRKISILIKGOREVLENDIRME(4, "hazirEvrakTuru.bilirkisilikGorevlendirme"),
		_EMEKLILIKTALEBI(5, "hazirEvrakTuru.emeklilikTalep"),
		_YABANCIUYEBILDIRIMYENILEME(6, "hazirEvrakTuru.yabanciUyeBildirimYenileme"),
		_BILIRKISILIKBELGESI(7, "hazirEvrakTuru.bilirkisilikBelgesi"),
		_IHALEBELGESI(8, "hazirEvrakTuru.ihaleBelgesi"),
		_ODAUYELIKBELGESI(9, "hazirEvrakTuru.uyelikBelgesi"),
		_ODAUYELIKBELGESIADALETKOMISYONU(10, "hazirEvrakTuru.uyelikBelgesiAdaletKomisyonu"),
		_SANTIYESEFLIGIBELGESI(11, "hazirEvrakTuru.santiyeSefligiBelgesi");
		

		private int code;
		private String label;

		private HazirEvrakTuru(int code, String label) {
		    this.code = code;
		    this.label = label;
		}

		@Override
		public int getCode() {
		    return code;
		}

		@Override
		public String getLabel() {
		    return KeyUtil.getMessageValue(label);
		}

		@Override
		public String toString() {
		    return KeyUtil.getMessageValue(label);
		}

		public static HazirEvrakTuru getWithCode(int code){
			HazirEvrakTuru[] enumArray = HazirEvrakTuru.values();
			for(HazirEvrakTuru enumObj : enumArray){ 
				if(enumObj.getCode() == code) {
					return enumObj;
				}
			}
			return HazirEvrakTuru._NULL;
		} 
}  
