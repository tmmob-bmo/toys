package tr.com.arf.toys.db.enumerated.iletisim;

import tr.com.arf.framework.db.enumerated._base.BaseEnumerated;
import tr.com.arf.framework.utility.tool.KeyUtil;

public enum IcerikTuru implements BaseEnumerated {

	_NULL(0, ""), _HAZIRICERIK(1, "icerikTuru.hazirIcerik"), _MANUEL(2, "icerikTuru.manuel");

	private int code;
	private String label;

	private IcerikTuru(int code, String label) {
		this.code = code;
		this.label = label;
	}

	@Override
	public int getCode() {
		return code;
	}

	@Override
	public String getLabel() {
		return KeyUtil.getMessageValue(label);
	}

	@Override
	public String toString() {
		return KeyUtil.getMessageValue(label);
	}

	public static IcerikTuru getWithCode(int code) {
		IcerikTuru[] enumArray = IcerikTuru.values();
		for (IcerikTuru enumObj : enumArray) {
			if (enumObj.getCode() == code) {
				return enumObj;
			}
		}
		return IcerikTuru._NULL;
	}
}
