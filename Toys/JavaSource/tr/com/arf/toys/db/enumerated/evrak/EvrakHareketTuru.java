package tr.com.arf.toys.db.enumerated.evrak; 
 
import tr.com.arf.framework.db.enumerated._base.BaseEnumerated;
import tr.com.arf.framework.utility.tool.KeyUtil;

public enum EvrakHareketTuru implements BaseEnumerated { 

	_NULL(0, ""),
	_GELEN(1, "evrakHareketTuru.gelen"), 
	_GIDEN(2, "evrakHareketTuru.giden"), 
	_ICYAZISMA(3, "evrakHareketTuru.icyazisma");
/*	_GENELGE(4, "evrakHareketTuru.genelge"), 
	_BILGINOTU(5, "evrakHareketTuru.bilginotu") */

	private int code;
	private String label;

	private EvrakHareketTuru(int code, String label) {
		this.code = code;
		this.label = label;
	}

	@Override
	public int getCode() {
		return code;
	}

	@Override
	public String getLabel() {
		return KeyUtil.getMessageValue(label);
	}
	
	@Override
	public String toString() {
		return KeyUtil.getMessageValue(label);
	}

	public static EvrakHareketTuru getWithCode(int code) {
		EvrakHareketTuru[] enumArray = EvrakHareketTuru.values();
		for (EvrakHareketTuru enumObj : enumArray) {
			if (enumObj.getCode() == code) {
				return enumObj;
			}
		}
		return EvrakHareketTuru._NULL;
	}
}  