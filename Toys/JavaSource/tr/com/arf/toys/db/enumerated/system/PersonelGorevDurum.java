package tr.com.arf.toys.db.enumerated.system; 
 
import tr.com.arf.framework.db.enumerated._base.BaseEnumerated;
import tr.com.arf.framework.utility.tool.KeyUtil;
  
public enum PersonelGorevDurum implements BaseEnumerated {  
    
		_NULL(0, ""), 
		_AKTIF(1, "personelGorevDurum.aktif"),
		_PASIF(2, "personelGorevDurum.pasif");
 
		private int code;
		private String label;

		private PersonelGorevDurum(int code, String label) {
		    this.code = code;
		    this.label = label;
		}

		@Override
		public int getCode() {
		    return code;
		}

		@Override
		public String getLabel() {
		    return KeyUtil.getMessageValue(label);
		}

		@Override
		public String toString() {
		    return KeyUtil.getMessageValue(label);
		}

		public static PersonelGorevDurum getWithCode(int code){
			PersonelGorevDurum[] enumArray = PersonelGorevDurum.values();
			for(PersonelGorevDurum enumObj : enumArray){ 
				if(enumObj.getCode() == code)
					return enumObj;
			}
			return PersonelGorevDurum._NULL;
		} 
		
}   
