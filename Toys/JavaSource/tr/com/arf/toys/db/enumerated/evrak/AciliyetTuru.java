package tr.com.arf.toys.db.enumerated.evrak; 
 
import tr.com.arf.framework.db.enumerated._base.BaseEnumerated;
import tr.com.arf.framework.utility.tool.KeyUtil;
  
public enum AciliyetTuru implements BaseEnumerated {  
    
		_NULL(0, ""), 
		_NORMAL(1, "aciliyetTuru.normal"),
		_IVEDI(2, "aciliyetTuru.ivedi"),
		_GUNLUDUR(3, "aciliyetTuru.gunludur"),
		_IVEDIVEGUNLUDUR(4, "aciliyetTuru.ivediVeGunludur");  
 
		private int code;
		private String label;

		private AciliyetTuru(int code, String label) {
		    this.code = code;
		    this.label = label;
		}

		@Override
		public int getCode() {
		    return code;
		}

		@Override
		public String getLabel() {
		    return KeyUtil.getMessageValue(label);
		}

		@Override
		public String toString() {
		    return KeyUtil.getMessageValue(label);
		}

		public static AciliyetTuru getWithCode(int code){
			AciliyetTuru[] enumArray = AciliyetTuru.values();
			for(AciliyetTuru enumObj : enumArray){ 
				if(enumObj.getCode() == code) {
					return enumObj;
				}
			}
			return AciliyetTuru._NULL;
		} 
}   
