package tr.com.arf.toys.db.enumerated.etkinlik; 
 
import tr.com.arf.framework.db.enumerated._base.BaseEnumerated;
import tr.com.arf.framework.utility.tool.KeyUtil;
  
public enum Kurulturu implements BaseEnumerated {  
    
		_NULL(0, ""), 
		_YURUTME(1, "kurulturu.yurutme"),
		_DUZENLEME(2, "kurulturu.duzenleme"),
		_DANISMA(3, "kurulturu.danisma"); 
 
		private int code;
		private String label;

		private Kurulturu(int code, String label) {
		    this.code = code;
		    this.label = label;
		}

		@Override
		public int getCode() {
		    return code;
		}

		@Override
		public String getLabel() {
		    return KeyUtil.getMessageValue(label);
		}

		@Override
		public String toString() {
		    return KeyUtil.getMessageValue(label);
		}

		public static Kurulturu getWithCode(int code){
			Kurulturu[] enumArray = Kurulturu.values();
			for(Kurulturu enumObj : enumArray){ 
				if(enumObj.getCode() == code) {
					return enumObj;
				}
			}
			return Kurulturu._NULL;
		} 
}   
