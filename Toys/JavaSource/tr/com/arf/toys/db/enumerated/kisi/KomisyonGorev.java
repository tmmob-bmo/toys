package tr.com.arf.toys.db.enumerated.kisi;

import tr.com.arf.framework.db.enumerated._base.BaseEnumerated;
import tr.com.arf.framework.utility.tool.KeyUtil;

public enum KomisyonGorev implements BaseEnumerated {

	_NULL(0, ""), 
	_BASKAN(1, "komisyonGorev.baskan"), 
	_BASKANYARDIMCISI(2, "komisyonGorev.baskanyardimcisi"), 
	_RAPORTOR(3, "komisyonGorev.raportor"), 
	_UYE(4, "komisyonGorev.uye");

	private int code;
	private String label;

	private KomisyonGorev(int code, String label) {
		this.code = code;
		this.label = label;
	}

	@Override
	public int getCode() {
		return code;
	}

	@Override
	public String getLabel() {
		return KeyUtil.getMessageValue(label);
	}

	@Override
	public String toString() {
		return KeyUtil.getMessageValue(label);
	}

	public static KomisyonGorev getWithCode(int code) {
		KomisyonGorev[] enumArray = KomisyonGorev.values();
		for (KomisyonGorev enumObj : enumArray) {
			if (enumObj.getCode() == code) {
				return enumObj;
			}
		}
		return KomisyonGorev._NULL;
	}
}