package tr.com.arf.toys.db.enumerated.system; 
 
import tr.com.arf.framework.db.enumerated._base.BaseEnumerated;
import tr.com.arf.framework.utility.tool.KeyUtil;
  
public enum PersonelDurum implements BaseEnumerated {  
    
	_NULL(0, ""), 
	_CALISIYOR(1, "personelDurum.calisiyor"),
	_GECICIGOREVDE(2, "personelDurum.gecicigorevde"),
	_IZINLI(3, "personelDurum.izinli"),
	_AYRILMIS(4, "personelDurum.ayrilmis");
 
	private int code;
	private String label;

	private PersonelDurum(int code, String label) {
		this.code = code;
		this.label = label;
	}

	@Override
	public int getCode() {
		return code;
	}

	@Override
	public String getLabel() {
		return KeyUtil.getMessageValue(label);
	}

	@Override
	public String toString() {
		return KeyUtil.getMessageValue(label);
	}
}   
