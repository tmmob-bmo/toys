package tr.com.arf.toys.db.enumerated.system;

import tr.com.arf.framework.db.enumerated._base.BaseEnumerated;
import tr.com.arf.framework.utility.tool.KeyUtil;

public enum IdariYapiTip implements BaseEnumerated {
	
	_NULL(0, ""),
	_MAHALLE(1, "idariyapitipi.mahalle"),
	_KOY(2, "idariyapitipi.koy"),
	_DIGER(2, "idariyapitipi.diger");
	
	private int code;
	private String label;

    private IdariYapiTip(int code, String label) {
        this.code = code;
        this.label = label;
    }
    
    @Override
    public int getCode() {
        return code;
    }

    @Override
    public String getLabel() {
        return KeyUtil.getMessageValue(label);
    }
    
    @Override
    public String toString() { 
        return KeyUtil.getMessageValue(label);
    }
    
    public static IdariYapiTip getWithCode(int code){
		IdariYapiTip[] idariYapiTipArray = IdariYapiTip.values();
		for(IdariYapiTip idariYapi : idariYapiTipArray){ 
			if(idariYapi.getCode() == code) {
				return idariYapi;
			}
		}
		return IdariYapiTip._NULL;
	} 

}