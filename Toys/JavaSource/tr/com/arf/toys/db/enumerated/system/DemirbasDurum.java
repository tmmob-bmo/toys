package tr.com.arf.toys.db.enumerated.system; 
 
import tr.com.arf.framework.db.enumerated._base.BaseEnumerated;
import tr.com.arf.framework.utility.tool.KeyUtil;
  
public enum DemirbasDurum implements BaseEnumerated {  
    
		_NULL(0, ""), 
		_KULLANILIYOR(1, "demirbasdurum.kullaniliyor"),
		_HIBE(2, "demirbasdurum.hibeedildi"),
		_IMHAEDILDI(3, "demirbasdurum.imhaedildi");
		
		
		private int code;
		private String label;

		private DemirbasDurum(int code, String label) {
		    this.code = code;
		    this.label = label;
		}

		@Override
		public int getCode() {
		    return code;
		}

		@Override
		public String getLabel() {
		    return KeyUtil.getMessageValue(label);
		}

		@Override
		public String toString() {
		    return KeyUtil.getMessageValue(label);
		}

		public static DemirbasDurum getWithCode(int code){
			DemirbasDurum[] enumArray = DemirbasDurum.values();
			for(DemirbasDurum enumObj : enumArray){ 
				if(enumObj.getCode() == code) {
					return enumObj;
				}
			}
			return DemirbasDurum._NULL;
		} 
}   
