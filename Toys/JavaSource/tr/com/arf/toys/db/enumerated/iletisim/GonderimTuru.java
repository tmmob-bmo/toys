package tr.com.arf.toys.db.enumerated.iletisim; 
 
import tr.com.arf.framework.db.enumerated._base.BaseEnumerated;
import tr.com.arf.framework.utility.tool.KeyUtil;
  
public enum GonderimTuru implements BaseEnumerated {  
    
		_NULL(0, ""), 
		_EPOSTA(1, "gonderimTuru.EPOSTA"),
		_SMS(2, "gonderimTuru.SMS"); 
 
		private int code;
		private String label;

		private GonderimTuru(int code, String label) {
		    this.code = code;
		    this.label = label;
		}

		@Override
		public int getCode() {
		    return code;
		}

		@Override
		public String getLabel() {
		    return KeyUtil.getMessageValue(label);
		}

		@Override
		public String toString() {
		    return KeyUtil.getMessageValue(label);
		}

		public static GonderimTuru getWithCode(int code){
			GonderimTuru[] enumArray = GonderimTuru.values();
			for(GonderimTuru enumObj : enumArray){ 
				if(enumObj.getCode() == code) {
					return enumObj;
				}
			}
			return GonderimTuru._NULL;
		} 
}   
