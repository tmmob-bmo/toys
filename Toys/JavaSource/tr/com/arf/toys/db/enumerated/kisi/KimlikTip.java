package tr.com.arf.toys.db.enumerated.kisi; 
 
import tr.com.arf.framework.db.enumerated._base.BaseEnumerated;
import tr.com.arf.framework.utility.tool.KeyUtil;
  
public enum KimlikTip implements BaseEnumerated {  
    
		_NULL(0, ""), 
		_TCKIMLIKNO(1, "kimlikNoTip.tckimlikno"),
		_PASAPORT(2, "kimlikNoTip.pasaport");
		
		private int code;
		private String label;

		private KimlikTip(int code, String label) {
		    this.code = code;
		    this.label = label;
		}

		@Override
		public int getCode() {
		    return code;
		}

		@Override
		public String getLabel() {
		    return KeyUtil.getMessageValue(label);
		}

		@Override
		public String toString() {
		    return KeyUtil.getMessageValue(label);
		}

		public static KimlikTip getWithCode(int code){
			KimlikTip[] enumArray = KimlikTip.values();
			for(KimlikTip enumObj : enumArray){ 
				if(enumObj.getCode() == code) {
					return enumObj;
				}
			}
			return KimlikTip._NULL;
		} 
}   
