package tr.com.arf.toys.db.enumerated.system;

import tr.com.arf.framework.db.enumerated._base.BaseEnumerated;
import tr.com.arf.framework.utility.tool.KeyUtil;

public enum YbirimTip implements BaseEnumerated{
	
	_NULL(0,""),
	_ARGE(1,"birimtip.arge"),
	_PROJE(2,"birimtip.proje");
	
	int code;
	String label;
	
	private YbirimTip(int code, String label){
		this.code = code;
		this.label = label;
	}
	
	@Override
	public int getCode() {
		return this.code;
	}

	@Override
	public String getLabel() {
		return KeyUtil.getMessageValue(label);
	}

	public static YbirimTip getWithCode(int code){
		YbirimTip[] enumArray = YbirimTip.values();
		for(YbirimTip enumObj : enumArray){ 
			if(enumObj.getCode() == code)
				return enumObj;
		}
		return YbirimTip._NULL;
	} 
	
	

}
