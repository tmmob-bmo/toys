package tr.com.arf.toys.db.enumerated.kisi; 
 
import tr.com.arf.framework.db.enumerated._base.BaseEnumerated;
import tr.com.arf.framework.utility.tool.KeyUtil;
  
public enum KomisyonUyelikDurumu implements BaseEnumerated {  
    
		_NULL(0, ""),
		_NULLDEGERI(1, ""),
		_UYELIKTENISTIFA(2, "komisyonUyelikDurumu.uyeliktenistifa"), 
		_UYELIGINDUSMESI(3, "komisyonUyelikDurumu.uyeligindusmesi");
		
		private int code;
		private String label;

		private KomisyonUyelikDurumu(int code, String label) {
		    this.code = code;
		    this.label = label;
		}

		@Override
		public int getCode() {
		    return code;
		}

		@Override
		public String getLabel() {
		    return KeyUtil.getMessageValue(label);
		}

		@Override
		public String toString() {
		    return KeyUtil.getMessageValue(label);
		}

		public static KomisyonUyelikDurumu getWithCode(int code){
			KomisyonUyelikDurumu[] enumArray = KomisyonUyelikDurumu.values();
			for(KomisyonUyelikDurumu enumObj : enumArray){ 
				if(enumObj.getCode() == code) {
					return enumObj;
				}
			}
			return KomisyonUyelikDurumu._NULL;
		} 
		
}   
