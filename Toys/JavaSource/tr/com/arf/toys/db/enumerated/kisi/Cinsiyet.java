package tr.com.arf.toys.db.enumerated.kisi; 
 
import tr.com.arf.framework.db.enumerated._base.BaseEnumerated;
import tr.com.arf.framework.utility.tool.KeyUtil;
  
public enum Cinsiyet implements BaseEnumerated {  
      
		_NULL(0, "cinsiyet.bilinmiyor"),
		_ERKEK(1, "cinsiyet.erkek"),
		_KADIN(2, "cinsiyet.kadin");
		
 
		private int code;
		private String label;

		private Cinsiyet(int code, String label) {
		    this.code = code;
		    this.label = label;
		}

		@Override
		public int getCode() {
		    return code;
		}

		@Override
		public String getLabel() {
		    return KeyUtil.getMessageValue(label);
		}

		@Override
		public String toString() {
		    return KeyUtil.getMessageValue(label);
		}

		public static Cinsiyet getWithCode(int code){
			Cinsiyet[] enumArray = Cinsiyet.values();
			for(Cinsiyet enumObj : enumArray){ 
				if(enumObj.getCode() == code) {
					return enumObj;
				}
			}
			return Cinsiyet._NULL;
		} 
}   
