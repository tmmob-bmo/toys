package tr.com.arf.toys.db.enumerated.egitim; 
 
import tr.com.arf.framework.db.enumerated._base.BaseEnumerated;
import tr.com.arf.framework.utility.tool.KeyUtil;
  
public enum SonucTuru implements BaseEnumerated {  
    
		_NULL(0, ""), 
		_GECTI(1, "sonucTuru.gecti"),
		_KALDI(2, "sonucTuru.kaldi"),
		_SINAVAGIREMEDI(3, "sonucTuru.sinavagirmedi");
 
		private int code;
		private String label;

		private SonucTuru(int code, String label) {
		    this.code = code;
		    this.label = label;
		}

		@Override
		public int getCode() {
		    return code;
		}

		@Override
		public String getLabel() {
		    return KeyUtil.getMessageValue(label);
		}

		@Override
		public String toString() {
		    return KeyUtil.getMessageValue(label);
		}

		public static SonucTuru getWithCode(int code){
			SonucTuru[] enumArray = SonucTuru.values();
			for(SonucTuru enumObj : enumArray){ 
				if(enumObj.getCode() == code) {
					return enumObj;
				}
			}
			return SonucTuru._NULL;
		} 
}   
