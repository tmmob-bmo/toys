package tr.com.arf.toys.db.enumerated.egitim; 
 
import tr.com.arf.framework.db.enumerated._base.BaseEnumerated;
import tr.com.arf.framework.utility.tool.KeyUtil;
  
public enum Katilimbelgesi implements BaseEnumerated {  
    
		_NULL(0, ""), 
		_Verildi(1, "katilimbelgesi.verildi"),
		_Verilmedi(2, "katilimbelgesi.verilmedi"),;
 
		private int code;
		private String label;

		private Katilimbelgesi(int code, String label) {
		    this.code = code;
		    this.label = label;
		}

		@Override
		public int getCode() {
		    return code;
		}

		@Override
		public String getLabel() {
		    return KeyUtil.getMessageValue(label);
		}

		@Override
		public String toString() {
		    return KeyUtil.getMessageValue(label);
		}

		public static Katilimbelgesi getWithCode(int code){
			Katilimbelgesi[] enumArray = Katilimbelgesi.values();
			for(Katilimbelgesi enumObj : enumArray){ 
				if(enumObj.getCode() == code) {
					return enumObj;
				}
			}
			return Katilimbelgesi._NULL;
		} 
}   
