package tr.com.arf.toys.db.enumerated.uye; 
 
import tr.com.arf.framework.db.enumerated._base.BaseEnumerated;
import tr.com.arf.framework.utility.tool.KeyUtil;
  
public enum UyeSigortaDurum implements BaseEnumerated {  
    
		_NULL(0, ""), 
		_TASLAK(1, "uyeSigortaDurum.taslak"),
		_GECERLI(2, "uyeSigortaDurum.gecerli"),
		_TAMAMLANMIS(3, "uyeSigortaDurum.tamamlanmis");
 
		private int code;
		private String label;

		private UyeSigortaDurum(int code, String label) {
		    this.code = code;
		    this.label = label;
		}

		@Override
		public int getCode() {
		    return code;
		}

		@Override
		public String getLabel() {
		    return KeyUtil.getMessageValue(label);
		}

		@Override
		public String toString() {
		    return KeyUtil.getMessageValue(label);
		}

		public static UyeSigortaDurum getWithCode(int code){
			UyeSigortaDurum[] enumArray = UyeSigortaDurum.values();
			for(UyeSigortaDurum enumObj : enumArray){ 
				if(enumObj.getCode() == code)
					return enumObj;
			}
			return UyeSigortaDurum._NULL;
		} 
}   
