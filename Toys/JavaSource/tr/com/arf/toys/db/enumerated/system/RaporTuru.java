package tr.com.arf.toys.db.enumerated.system; 
 
import tr.com.arf.framework.db.enumerated._base.BaseEnumerated;
import tr.com.arf.framework.utility.tool.KeyUtil;

public enum RaporTuru implements BaseEnumerated { 

		_NULL(0, ""),
		_ODAUYELIKBELGESI(1, "raporTuru.odauyelikbelgesi"), 
		_IHALEBELGESI(2, "raporTuru.i̇halebelgesi"), 
		_SANTIYESEFLIGIBELGESI(3, "raporTuru.santiyesefligibelgesi"), 
		_ODAUYELIKBELGESIADALETKOMISYONU(4, "raporTuru.odauyelikbelgesiadaletkomisyonu"), 
		_UYEBILGIFORMU(5, "raporTuru.uyebilgiformu"); 

		private int code;
		private String label;

		private RaporTuru(int code, String label) {
		    this.code = code;
		    this.label = label;
		}

		@Override
		public int getCode() {
		    return code;
		}

		@Override
		public String getLabel() {
		    return KeyUtil.getMessageValue(label);
		}

		@Override
		public String toString() {
		    return KeyUtil.getMessageValue(label);
		}

		public static RaporTuru getWithCode(int code){
			RaporTuru[] enumArray = RaporTuru.values();
			for(RaporTuru enumObj : enumArray){ 
				if(enumObj.getCode() == code) {
					return enumObj;
				}
			}
			return RaporTuru._NULL;
		} 
}  