package tr.com.arf.toys.db.enumerated.iletisim; 
 
import tr.com.arf.framework.db.enumerated._base.BaseEnumerated;
import tr.com.arf.framework.utility.tool.KeyUtil;
  
public enum TelefonTuru implements BaseEnumerated {  
    
		_NULL(0, ""), 
		_GSM(1, "telefonTuru.gsm"),
		_ISTELEFONU(2, "telefonTuru.istelefonu"),
		_EVTELEFONU(3, "telefonTuru.evtelefonu"),
		_FAKS(4, "telefonTuru.faks");
 
		private int code;
		private String label;

		private TelefonTuru(int code, String label) {
		    this.code = code;
		    this.label = label;
		}

		@Override
		public int getCode() {
		    return code;
		}

		@Override
		public String getLabel() {
		    return KeyUtil.getMessageValue(label);
		}

		@Override
		public String toString() {
		    return KeyUtil.getMessageValue(label);
		}

		public static TelefonTuru getWithCode(int code){
			TelefonTuru[] enumArray = TelefonTuru.values();
			for(TelefonTuru enumObj : enumArray){ 
				if(enumObj.getCode() == code) {
					return enumObj;
				}
			}
			return TelefonTuru._NULL;
		} 
}   
