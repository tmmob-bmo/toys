package tr.com.arf.toys.db.enumerated.uye; 
 
import tr.com.arf.framework.db.enumerated._base.BaseEnumerated;
import tr.com.arf.framework.utility.tool.KeyUtil;
  
public enum UyeBilirkisiDurum implements BaseEnumerated {  
    
		_NULL(0, ""), 
		_TASLAK(1, "uyeBilirkisiDurum.taslak"),
		_AKTIF(2, "uyeBilirkisiDurum.aktif"),
		_TAMAMLANMIS(3, "uyeBilirkisiDurum.tamamlanmis");
 
		private int code;
		private String label;

		private UyeBilirkisiDurum(int code, String label) {
		    this.code = code;
		    this.label = label;
		}

		@Override
		public int getCode() {
		    return code;
		}

		@Override
		public String getLabel() {
		    return KeyUtil.getMessageValue(label);
		}

		@Override
		public String toString() {
		    return KeyUtil.getMessageValue(label);
		}

		public static UyeBilirkisiDurum getWithCode(int code){
			UyeBilirkisiDurum[] enumArray = UyeBilirkisiDurum.values();
			for(UyeBilirkisiDurum enumObj : enumArray){ 
				if(enumObj.getCode() == code)
					return enumObj;
			}
			return UyeBilirkisiDurum._NULL;
		} 
}   
