package tr.com.arf.toys.db.enumerated.egitim; 
 
import tr.com.arf.framework.db.enumerated._base.BaseEnumerated;
import tr.com.arf.framework.utility.tool.KeyUtil;
  
public enum Sertifika implements BaseEnumerated {  
    
		_NULL(0, ""), 
		_Verildi(1, "sertifika.verildi"),
		_Verilmedi(2, "sertifika.verilmedi"),;
 
		private int code;
		private String label;

		private Sertifika(int code, String label) {
		    this.code = code;
		    this.label = label;
		}

		@Override
		public int getCode() {
		    return code;
		}

		@Override
		public String getLabel() {
		    return KeyUtil.getMessageValue(label);
		}

		@Override
		public String toString() {
		    return KeyUtil.getMessageValue(label);
		}

		public static Sertifika getWithCode(int code){
			Sertifika[] enumArray = Sertifika.values();
			for(Sertifika enumObj : enumArray){ 
				if(enumObj.getCode() == code) {
					return enumObj;
				}
			}
			return Sertifika._NULL;
		} 
}   
