package tr.com.arf.toys.db.enumerated.kisi; 
 
import tr.com.arf.framework.db.enumerated._base.BaseEnumerated;
import tr.com.arf.framework.utility.tool.KeyUtil;
  
public enum KisiDurum implements BaseEnumerated {  
    
		_NULL(0, ""), 
		_ACIK(1, "kisiDurum.acik"),
		_KAPALI(2, "kisiDurum.kapali"),
		_OLUM(3, "kisiDurum.olum"),
		_GAIP(4, "kisiDurum.gaip"),
		_VATANDASLIKTANCIKMA(5, "kisiDurum.vatandasliktancikma"),
		_KAYITSILME(6, "kisiDurum.kayitsilme"),
		_MUKERRERKAYITSILME(7, "kisiDurum.mukerrerkayitsilme"),
		_DIGER(8, "kisiDurum.diger"),
		_CILTBOLMEBIRLESTIRME(9, "kisiDurum.ciltbolmebirlestirme"),
		_IDARECIGERIALMA(10, "kisiDurum.idarecigerialma"),
		
		_OLUMARASTIRMASI(13, "kisiDurum.olumarastirmasi"),
		_DURDURULMA(14, "kisiDurum.durdurulma"),
		_GIZLITUTULMA(15, "kisiDurum.gizlitutulma"),
		
		_CILTBOLMEACIK(51, "kisiDurum.ciltbolmeacik"),
		_CILTBOLMEKAPALI(52, "kisiDurum.ciltbolmekapali"),
		_CILTBOLMEOLUM(53, "kisiDurum.ciltbolmeolum"),
		_CILTBOLMEGAIP(54, "kisiDurum.ciltbolmegaip"),
		_CILTBOLMEVATANDASLIKTANCIKMA(55, "kisiDurum.ciltbolmevatandasliktancikma"),
		_CILTBOLMEKAYITSILME(56, "kisiDurum.ciltbolmekayitsilme"),
		_CILTBOLMEMUKERREKAYITSILME(57, "kisiDurum.ciltbolmemukerrekayitsilme"),
		_CILTBOLMEDIGER(58, "kisiDurum.ciltbolmediger"),
		_CILTBOLMEBIRLES(59, "kisiDurum.ciltbolmebirles"),
		_CILTBOLMEIDARECEGERIALMA(60, "kisiDurum.ciltbolmeidarecegerialma"),
		
		_CILTBOLMEOLUMARASTIRMASI(63, "kisiDurum.ciltbolmeolumarastirmasi"),
		_CILTBOLMEDURDURULMA(64, "kisiDurum.ciltbolmedurdurulma"),
		_CILTBOLMEGIZLITUTULMA(65, "kisiDurum.ciltbolmegizlitutulma");
 
		private int code;
		private String label;

		private KisiDurum(int code, String label) {
		    this.code = code;
		    this.label = label;
		}

		@Override
		public int getCode() {
		    return code;
		}

		@Override
		public String getLabel() {
		    return KeyUtil.getMessageValue(label);
		}

		@Override
		public String toString() {
		    return KeyUtil.getMessageValue(label);
		}

		public static KisiDurum getWithCode(int code){
			KisiDurum[] enumArray = KisiDurum.values();
			for(KisiDurum enumObj : enumArray){ 
				if(enumObj.getCode() == code)
					return enumObj;
			}
			return KisiDurum._NULL;
		} 
		
}   
