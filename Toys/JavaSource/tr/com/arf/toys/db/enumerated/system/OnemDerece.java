package tr.com.arf.toys.db.enumerated.system;

import tr.com.arf.framework.db.enumerated._base.BaseEnumerated;
import tr.com.arf.framework.utility.tool.KeyUtil;

public enum OnemDerece implements BaseEnumerated {
	
	_NULL(0, ""),
	_DUSUK(1, "onemderece.dusuk"),
	_ORTA(2, "onemderece.orta"),
	_YUKSEK(3, "onemderece.yuksek");
	
	private int code;
	private String label;

    private OnemDerece(int code, String label) {
        this.code = code;
        this.label = label;
    }
    
    @Override
    public int getCode() {
        return code;
    }

    @Override
    public String getLabel() { 
        return KeyUtil.getMessageValue(label);
    }
    
    @Override
	public String toString() {
	    return KeyUtil.getMessageValue(label);
	}
    
}