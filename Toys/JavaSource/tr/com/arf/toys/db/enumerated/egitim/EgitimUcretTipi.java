package tr.com.arf.toys.db.enumerated.egitim; 
 
import tr.com.arf.framework.db.enumerated._base.BaseEnumerated;
import tr.com.arf.framework.utility.tool.KeyUtil;
  
public enum EgitimUcretTipi implements BaseEnumerated {  
    
		_NULL(0, ""), 
		_SEMINERUCRETI(1, "egitimUcretTipi.seminerucreti"),
		_BELGEUCRETI(2, "egitimUcretTipi.belgeucreti");
		 
 
		private int code;
		private String label;

		private EgitimUcretTipi(int code, String label) {
		    this.code = code;
		    this.label = label;
		}

		@Override
		public int getCode() {
		    return code;
		}

		@Override
		public String getLabel() {
		    return KeyUtil.getMessageValue(label);
		}

		@Override
		public String toString() {
		    return KeyUtil.getMessageValue(label);
		}

		public static EgitimUcretTipi getWithCode(int code){
			EgitimUcretTipi[] enumArray = EgitimUcretTipi.values();
			for(EgitimUcretTipi enumObj : enumArray){ 
				if(enumObj.getCode() == code) {
					return enumObj;
				}
			}
			return EgitimUcretTipi._NULL;
		} 
}   
