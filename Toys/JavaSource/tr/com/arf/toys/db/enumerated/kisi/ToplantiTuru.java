package tr.com.arf.toys.db.enumerated.kisi; 
 
import tr.com.arf.framework.db.enumerated._base.BaseEnumerated;
import tr.com.arf.framework.utility.tool.KeyUtil;
  
public enum ToplantiTuru implements BaseEnumerated {  
    
		_NULL(0, ""), 
		_PLANLI(1, "toplantiTuru.planli"), 
		_PLANSIZ(3, "toplantiTuru.plansiz"); 
 
		private int code;
		private String label;

		private ToplantiTuru(int code, String label) {
		    this.code = code;
		    this.label = label;
		}

		@Override
		public int getCode() {
		    return code;
		}

		@Override
		public String getLabel() {
		    return KeyUtil.getMessageValue(label);
		}

		@Override
		public String toString() {
		    return KeyUtil.getMessageValue(label);
		}

		public static ToplantiTuru getWithCode(int code){
			ToplantiTuru[] enumArray = ToplantiTuru.values();
			for(ToplantiTuru enumObj : enumArray){ 
				if(enumObj.getCode() == code) {
					return enumObj;
				}
			}
			return ToplantiTuru._NULL;
		} 
}   
