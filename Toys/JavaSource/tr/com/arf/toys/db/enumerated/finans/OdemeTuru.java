package tr.com.arf.toys.db.enumerated.finans;

import tr.com.arf.framework.db.enumerated._base.BaseEnumerated;
import tr.com.arf.framework.utility.tool.KeyUtil;

public enum OdemeTuru implements BaseEnumerated {

	_NULL(0, ""),
	_AIDAT(1, "odemeTuru.aidat"),
	_BORC(2, "odemeTuru.borc");

	private int code;
	private String label;

	private OdemeTuru(int code, String label) {
		this.code = code;
		this.label = label;
	}

	@Override
	public int getCode() {
		return code;
	}

	@Override
	public String getLabel() {
		return KeyUtil.getMessageValue(label);
	}

	@Override
	public String toString() {
		return KeyUtil.getMessageValue(label);
	}

	public static OdemeTuru getWithCode(int code) {
		OdemeTuru[] enumArray = OdemeTuru.values();
		for (OdemeTuru enumObj : enumArray) {
			if (enumObj.getCode() == code) {
				return enumObj;
			}
		}
		return OdemeTuru._NULL;
	}

	public static OdemeTuru getWithLabel(String labelValue) {
		OdemeTuru[] enumArray = OdemeTuru.values();
		for (OdemeTuru enumObj : enumArray) {
			if (enumObj.getLabel().equalsIgnoreCase(labelValue)) {
				return enumObj;
			}
		}
		return OdemeTuru._NULL;
	}
}