package tr.com.arf.toys.db.enumerated.evrak; 
 
import tr.com.arf.framework.db.enumerated._base.BaseEnumerated;
import tr.com.arf.framework.utility.tool.KeyUtil;
  
public enum EvrakTuru implements BaseEnumerated {  
    
		_NULL(0, ""), 
		_RESMI(1, "evrakTuru.resmi"),
		_EPOSTA(2, "evrakTuru.ePosta"),
		_FAKS(3, "evrakTuru.faks"); 
		
		private int code;
		private String label;

		private EvrakTuru(int code, String label) {
		    this.code = code;
		    this.label = label;
		}

		@Override
		public int getCode() {
		    return code;
		}

		@Override
		public String getLabel() {
		    return KeyUtil.getMessageValue(label);
		}

		@Override
		public String toString() {
		    return KeyUtil.getMessageValue(label);
		}

		public static EvrakTuru getWithCode(int code){
			EvrakTuru[] enumArray = EvrakTuru.values();
			for(EvrakTuru enumObj : enumArray){ 
				if(enumObj.getCode() == code) {
					return enumObj;
				}
			}
			return EvrakTuru._NULL;
		} 
}   
