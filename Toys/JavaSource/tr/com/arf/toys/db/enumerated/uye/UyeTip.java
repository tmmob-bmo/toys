package tr.com.arf.toys.db.enumerated.uye; 
 
import tr.com.arf.framework.db.enumerated._base.BaseEnumerated;
import tr.com.arf.framework.utility.tool.KeyUtil;
  
public enum UyeTip implements BaseEnumerated {  
    
		_NULL(0, ""), 
		_CALISAN(1, "uyeTip.calisan"),	
		_YABANCILAR(2, "uyeTip.yabanci"),
		_TURKUYRUKLU(3, "uyeTip.turkUyruklu"),		
		_OGRENCI(4, "uyeTip.ogrenci");		
		 
		private int code;
		private String label;

		private UyeTip(int code, String label) {
		    this.code = code;
		    this.label = label;
		}

		@Override
		public int getCode() {
		    return code;
		}

		@Override
		public String getLabel() {
		    return KeyUtil.getMessageValue(label);
		}

		@Override
		public String toString() {
		    return KeyUtil.getMessageValue(label);
		}

		public static UyeTip getWithCode(int code){
			UyeTip[] enumArray = UyeTip.values();
			for(UyeTip enumObj : enumArray){ 
				if(enumObj.getCode() == code) {
					return enumObj;
				}
			}
			return UyeTip._NULL;
		} 
		
		
}   
