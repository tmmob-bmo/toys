package tr.com.arf.toys.db.enumerated.kisi; 
 
import tr.com.arf.framework.db.enumerated._base.BaseEnumerated;
import tr.com.arf.framework.utility.tool.KeyUtil;
  
public enum GorevTuru implements BaseEnumerated {  
    
		_NULL(0, ""), 
		_BASKAN(1, "gorevturu.baskan"),
		_BASKANYRD(2, "gorevturu.baskanYrd"),
		_YAZMAN(3, "gorevturu.yazman"),
		_SAYMAN(4, "gorevturu.sayman"),
		_UYE(5, "gorevturu.uye");
		
 
		private int code;
		private String label;

		private GorevTuru(int code, String label) {
		    this.code = code;
		    this.label = label;
		}

		@Override
		public int getCode() {
		    return code;
		}

		@Override
		public String getLabel() {
		    return KeyUtil.getMessageValue(label);
		}

		@Override
		public String toString() {
		    return KeyUtil.getMessageValue(label);
		}

		public static GorevTuru getWithCode(int code){
			GorevTuru[] enumArray = GorevTuru.values();
			for(GorevTuru enumObj : enumArray){ 
				if(enumObj.getCode() == code) {
					return enumObj;
				}
			}
			return GorevTuru._NULL;
		} 
		
		
}   
