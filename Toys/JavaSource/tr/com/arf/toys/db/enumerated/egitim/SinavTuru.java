package tr.com.arf.toys.db.enumerated.egitim; 
 
import tr.com.arf.framework.db.enumerated._base.BaseEnumerated;
import tr.com.arf.framework.utility.tool.KeyUtil;
  
public enum SinavTuru implements BaseEnumerated {  
    
		_NULL(0, ""), 
		_Miem(1, "sinavTuru.miem"),
		_Pbk(2, "sinavTuru.pbk");
 
		private int code;
		private String label;

		private SinavTuru(int code, String label) {
		    this.code = code;
		    this.label = label;
		}

		@Override
		public int getCode() {
		    return code;
		}

		@Override
		public String getLabel() {
		    return KeyUtil.getMessageValue(label);
		}

		@Override
		public String toString() {
		    return KeyUtil.getMessageValue(label);
		}

		public static SinavTuru getWithCode(int code){
			SinavTuru[] enumArray = SinavTuru.values();
			for(SinavTuru enumObj : enumArray){ 
				if(enumObj.getCode() == code) {
					return enumObj;
				}
			}
			return SinavTuru._NULL;
		} 
}   
