package tr.com.arf.toys.db.enumerated.uye; 
 
import tr.com.arf.framework.db.enumerated._base.BaseEnumerated;
import tr.com.arf.framework.utility.tool.KeyUtil;
  
public enum OdemeTip implements BaseEnumerated {  
    
		_NULL(0, ""), 
		_TIP1(1, "odemeTip.sanalpos"),
		_TIP2(2, "odemeTip.vezne"),
		_TIP3(3, "odemeTip.bankavirman");
 
		private int code;
		private String label;

		private OdemeTip(int code, String label) {
		    this.code = code;
		    this.label = label;
		}

		@Override
		public int getCode() {
		    return code;
		}

		@Override
		public String getLabel() {
		    return KeyUtil.getMessageValue(label);
		}

		@Override
		public String toString() {
		    return KeyUtil.getMessageValue(label);
		}

		public static OdemeTip getWithCode(int code){
			OdemeTip[] enumArray = OdemeTip.values();
			for(OdemeTip enumObj : enumArray){ 
				if(enumObj.getCode() == code)
					return enumObj;
			}
			return OdemeTip._NULL;
		} 
}   
