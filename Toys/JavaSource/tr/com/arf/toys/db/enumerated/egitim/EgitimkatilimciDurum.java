package tr.com.arf.toys.db.enumerated.egitim; 
 
import tr.com.arf.framework.db.enumerated._base.BaseEnumerated;
import tr.com.arf.framework.utility.tool.KeyUtil;
  
public enum EgitimkatilimciDurum implements BaseEnumerated {  
    
		_NULL(0, ""), 
		_Katildi(1, "egitimkatilimciDurum.katildi"),
		_Katilmadi(2, "egitimkatilimciDurum.katilmadi"),; 
 
		private int code;
		private String label;

		private EgitimkatilimciDurum(int code, String label) {
		    this.code = code;
		    this.label = label;
		}

		@Override
		public int getCode() {
		    return code;
		}

		@Override
		public String getLabel() {
		    return KeyUtil.getMessageValue(label);
		}

		@Override
		public String toString() {
		    return KeyUtil.getMessageValue(label);
		}

		public static EgitimkatilimciDurum getWithCode(int code){
			EgitimkatilimciDurum[] enumArray = EgitimkatilimciDurum.values();
			for(EgitimkatilimciDurum enumObj : enumArray){ 
				if(enumObj.getCode() == code) {
					return enumObj;
				}
			}
			return EgitimkatilimciDurum._NULL;
		} 
}   
