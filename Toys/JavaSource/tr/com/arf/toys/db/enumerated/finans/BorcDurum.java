package tr.com.arf.toys.db.enumerated.finans; 
 
import tr.com.arf.framework.db.enumerated._base.BaseEnumerated;
import tr.com.arf.framework.utility.tool.KeyUtil;
  
public enum BorcDurum implements BaseEnumerated {  
    
		_NULL(0, ""), 
		_BORCLU(1, "borcDurum.borclu"),
		_ODENMIS(2, "borcDurum.odenmis"),
		_ALACAKLI(3, "borcDurum.alacakli");
 
		private int code;
		private String label;

		private BorcDurum(int code, String label) {
		    this.code = code;
		    this.label = label;
		}

		@Override
		public int getCode() {
		    return code;
		}

		@Override
		public String getLabel() {
		    return KeyUtil.getMessageValue(label);
		}

		@Override
		public String toString() {
		    return KeyUtil.getMessageValue(label);
		}

		public static BorcDurum getWithCode(int code){
			BorcDurum[] enumArray = BorcDurum.values();
			for(BorcDurum enumObj : enumArray){ 
				if(enumObj.getCode() == code)
					return enumObj;
			}
			return BorcDurum._NULL;
		} 
}   
