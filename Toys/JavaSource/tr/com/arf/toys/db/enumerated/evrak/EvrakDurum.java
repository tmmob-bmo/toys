package tr.com.arf.toys.db.enumerated.evrak; 
 
import tr.com.arf.framework.db.enumerated._base.BaseEnumerated;
import tr.com.arf.framework.utility.tool.KeyUtil;
  
public enum EvrakDurum implements BaseEnumerated {  
    
		_NULL(0, ""), 
		_TASLAK(1, "evrakDurum.taslak"),
		_ONAYLANMIS(2, "evrakDurum.onaylanmis"),
		_TAMAMLANMIS(3, "evrakDurum.tamamlanmis"),
		_REDDEDILMIS(4, "evrakDurum.reddedilmis"); 
 				
				
		private int code;
		private String label;

		private EvrakDurum(int code, String label) {
		    this.code = code;
		    this.label = label;
		}

		@Override
		public int getCode() {
		    return code;
		}

		@Override
		public String getLabel() {
		    return KeyUtil.getMessageValue(label);
		}
		
		public String getLabelForEvrakList() {
			switch(getCode()){
				case 1 : return "<span style=\"color:darkgray; font-weight:bold\">" + KeyUtil.getMessageValue(label) + "</span>";
				case 2 : return "<span style=\"color:green; font-weight:bold\">" + KeyUtil.getMessageValue(label) + "</span>";
				case 3 : return "<span style=\"color:orange; font-weight:bold\">" + KeyUtil.getMessageValue(label) + "</span>";
				case 4 : return "<span style=\"color:red; font-weight:bold\">" + KeyUtil.getMessageValue(label) + "</span>";
				default : return KeyUtil.getMessageValue(label);
			} 
		}
		

		@Override
		public String toString() {
		    return KeyUtil.getMessageValue(label);
		}

		public static EvrakDurum getWithCode(int code){
			EvrakDurum[] enumArray = EvrakDurum.values();
			for(EvrakDurum enumObj : enumArray){ 
				if(enumObj.getCode() == code) {
					return enumObj;
				}
			}
			return EvrakDurum._NULL;
		} 
}   
