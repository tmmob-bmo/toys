package tr.com.arf.toys.db.enumerated.egitim; 
 
import tr.com.arf.framework.db.enumerated._base.BaseEnumerated;
import tr.com.arf.framework.utility.tool.KeyUtil;
  
public enum KatilimciTuru implements BaseEnumerated {  
    
		_NULL(0, ""), 
		_SMM(1, "katilimciTuru.smm"),
		_UYE(2, "katilimciTuru.uye"),
		_DISSAHIS(3, "katilimciTuru.dissahis"),
		_HEPSI(4, "katilimciTuru.hepsi");
 
		private int code;
		private String label;

		private KatilimciTuru(int code, String label) {
		    this.code = code;
		    this.label = label;
		}

		@Override
		public int getCode() {
		    return code;
		}

		@Override
		public String getLabel() {
		    return KeyUtil.getMessageValue(label);
		}

		@Override
		public String toString() {
		    return KeyUtil.getMessageValue(label);
		}

		public static KatilimciTuru getWithCode(int code){
			KatilimciTuru[] enumArray = KatilimciTuru.values();
			for(KatilimciTuru enumObj : enumArray){ 
				if(enumObj.getCode() == code) {
					return enumObj;
				}
			}
			return KatilimciTuru._NULL;
		} 
}   
