package tr.com.arf.toys.db.enumerated.system;

import tr.com.arf.framework.db.enumerated._base.BaseEnumerated;
import tr.com.arf.framework.utility.tool.KeyUtil;


public enum YpersonelProjeDurum implements BaseEnumerated{

	_NULL(0, ""), 
	_AKTIF(1, "YpersonelProjeDurum.aktif"),
	_PASIF(2, "YpersonelProjeDurum.pasif");

	private int code;
	private String label;

	private YpersonelProjeDurum(int code, String label) {
	    this.code = code;
	    this.label = label;
	}

	@Override
	public int getCode() {
	    return code;
	}

	@Override
	public String getLabel() {
	    return KeyUtil.getMessageValue(label);
	}

	@Override
	public String toString() {
	    return KeyUtil.getMessageValue(label);
	}

	public static YpersonelProjeDurum getWithCode(int code){
		YpersonelProjeDurum[] enumArray = YpersonelProjeDurum.values();
		for(YpersonelProjeDurum enumObj : enumArray){ 
			if(enumObj.getCode() == code)
				return enumObj;
		}
		return YpersonelProjeDurum._NULL;
	} 

}
