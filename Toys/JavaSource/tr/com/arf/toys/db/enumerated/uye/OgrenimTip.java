package tr.com.arf.toys.db.enumerated.uye; 
 
import tr.com.arf.framework.db.enumerated._base.BaseEnumerated;
import tr.com.arf.framework.utility.tool.KeyUtil;
  
public enum OgrenimTip implements BaseEnumerated {  
    
		_NULL(0, ""),  
		_LISANS(1, "ogrenimTip.lisans"),
		_YUKSEKLISANS(2, "ogrenimTip.yukseklisans"),
		_DOKTORA(3, "ogrenimTip.doktora");
 
		private int code;
		private String label;

		private OgrenimTip(int code, String label) {
		    this.code = code;
		    this.label = label;
		}

		@Override
		public int getCode() {
		    return code;
		}

		@Override
		public String getLabel() {
		    return KeyUtil.getMessageValue(label);
		}

		@Override
		public String toString() {
		    return KeyUtil.getMessageValue(label);
		}

		public static OgrenimTip getWithCode(int code){
			OgrenimTip[] enumArray = OgrenimTip.values();
			for(OgrenimTip enumObj : enumArray){ 
				if(enumObj.getCode() == code) {
					return enumObj;
				}
			}
			return OgrenimTip._NULL;
		} 
}   
