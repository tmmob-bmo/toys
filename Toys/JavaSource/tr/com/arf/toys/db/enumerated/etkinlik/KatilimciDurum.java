package tr.com.arf.toys.db.enumerated.etkinlik; 
 
import tr.com.arf.framework.db.enumerated._base.BaseEnumerated;
import tr.com.arf.framework.utility.tool.KeyUtil;
  
public enum KatilimciDurum implements BaseEnumerated {  
    
		_NULL(0, ""), 
		_KATILDI(1, "katilimciDurum.katildi"),
		_KATILMADI(2, "katilimciDurum.katilmadi");
 
		private int code;
		private String label;

		private KatilimciDurum(int code, String label) {
		    this.code = code;
		    this.label = label;
		}

		@Override
		public int getCode() {
		    return code;
		}

		@Override
		public String getLabel() {
		    return KeyUtil.getMessageValue(label);
		}

		@Override
		public String toString() {
		    return KeyUtil.getMessageValue(label);
		}

		public static KatilimciDurum getWithCode(int code){
			KatilimciDurum[] enumArray = KatilimciDurum.values();
			for(KatilimciDurum enumObj : enumArray){ 
				if(enumObj.getCode() == code) {
					return enumObj;
				}
			}
			return KatilimciDurum._NULL;
		} 
}   
