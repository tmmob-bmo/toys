package tr.com.arf.toys.db.enumerated.iletisim; 
 
import tr.com.arf.framework.db.enumerated._base.BaseEnumerated;
import tr.com.arf.framework.utility.tool.KeyUtil;
  
public enum ReferansTipi implements BaseEnumerated {  
    
		_NULL(0, ""), 
		_KISI(1, "referansTipi.KISI"),
		_UYE(2, "referansTipi.UYE"),
		_BIRIM(3, "referansTipi.BIRIM"),
		_KURUM(4, "referansTipi.KURUM"),
		_KURUL(5, "referansTipi.KURUL"),
		_KOMISYON(6, "referansTipi.KOMISYON"),
		_EGITMEN(7, "referansTipi.EGITMEN"),
		_EGITIMKATILIMCI(8, "referansTipi.EGITIMKATILIMCI"),
		_ETKINLIKKATILIMCI(9, "referansTipi.ETKINLIKKATILIMCI"),
		_ISYERITEMSILCISI(10, "referansTipi.ISYERITEMSILCISI"); 
		
 
		private int code;
		private String label;

		private ReferansTipi(int code, String label) {
		    this.code = code;
		    this.label = label;
		}

		@Override
		public int getCode() {
		    return code;
		}

		@Override
		public String getLabel() {
		    return KeyUtil.getMessageValue(label);
		}

		@Override
		public String toString() {
		    return KeyUtil.getMessageValue(label);
		}

		public static ReferansTipi getWithCode(int code){
			ReferansTipi[] enumArray = ReferansTipi.values();
			for(ReferansTipi enumObj : enumArray){ 
				if(enumObj.getCode() == code) {
					return enumObj;
				}
			}
			return ReferansTipi._NULL;
		} 
}   
