package tr.com.arf.toys.db.enumerated.egitim; 
 
import tr.com.arf.framework.db.enumerated._base.BaseEnumerated;
import tr.com.arf.framework.utility.tool.KeyUtil;
  
public enum Soru implements BaseEnumerated {  
    
		_NULL(0, ""), 
		_TIP1(1, "soru.TIP1"),
		_TIP2(2, "soru.TIP2"),
		_TIP3(3, "soru.TIP3"),
		_TIP4(4, "soru.TIP4"),
		_TIP5(5, "soru.TIP5"),
		_TIP6(6, "soru.TIP6"),
		_TIP7(7, "soru.TIP7"),
		_TIP8(8, "soru.TIP8"),
		_TIP9(9, "soru.TIP9"),
		_TIP10(10, "soru.TIP10"),
		_TIP11(11, "soru.TIP11"),
		_TIP12(12, "soru.TIP12"),
		_TIP13(13, "soru.TIP13"),
		_TIP14(14, "soru.TIP14"),
		_TIP15(15, "soru.TIP15"),
		_TIP16(16, "soru.TIP16"),
		_TIP17(17, "soru.TIP17"),
		_TIP18(18, "soru.TIP18"),
		_TIP19(19, "soru.TIP19"),
		_TIP20(20, "soru.TIP20"); 
 
		private int code;
		private String label;

		private Soru(int code, String label) {
		    this.code = code;
		    this.label = label;
		}

		@Override
		public int getCode() {
		    return code;
		}

		@Override
		public String getLabel() {
		    return KeyUtil.getMessageValue(label);
		}

		@Override
		public String toString() {
		    return KeyUtil.getMessageValue(label);
		}

		public static Soru getWithCode(int code){
			Soru[] enumArray = Soru.values();
			for(Soru enumObj : enumArray){ 
				if(enumObj.getCode() == code) {
					return enumObj;
				}
			}
			return Soru._NULL;
		} 
}   
