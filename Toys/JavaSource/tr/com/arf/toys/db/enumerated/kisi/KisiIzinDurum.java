package tr.com.arf.toys.db.enumerated.kisi; 
 
import tr.com.arf.framework.db.enumerated._base.BaseEnumerated;
import tr.com.arf.framework.utility.tool.KeyUtil;
  
public enum KisiIzinDurum implements BaseEnumerated {  
    
		_NULL(0, ""), 
		_TASLAK(1, "kisiIzinDurum.taslak"),
		_AKTIF(2, "kisiIzinDurum.aktif"),
		_IPTALEDILMIS(3, "kisiIzinDurum.iptaledilmis"),
		_TAMAMLANMIS(4, "kisiIzinDurum.tamamlanmis");
 
		private int code;
		private String label;

		private KisiIzinDurum(int code, String label) {
		    this.code = code;
		    this.label = label;
		}

		@Override
		public int getCode() {
		    return code;
		}

		@Override
		public String getLabel() {
		    return KeyUtil.getMessageValue(label);
		}

		@Override
		public String toString() {
		    return KeyUtil.getMessageValue(label);
		}

		public static KisiIzinDurum getWithCode(int code){
			KisiIzinDurum[] enumArray = KisiIzinDurum.values();
			for(KisiIzinDurum enumObj : enumArray){ 
				if(enumObj.getCode() == code)
					return enumObj;
			}
			return KisiIzinDurum._NULL;
		} 
}   
