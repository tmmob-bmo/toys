package tr.com.arf.toys.db.enumerated.iletisim; 
 
import tr.com.arf.framework.db.enumerated._base.BaseEnumerated;
import tr.com.arf.framework.utility.tool.KeyUtil;
  
public enum ListeGizlilikTuru implements BaseEnumerated {  
    
		_NULL(0, ""), 
		_TIP1(1, "listeGizlilikTuru.TIP1"),
		_TIP2(2, "listeGizlilikTuru.TIP2"),
		_TIP3(3, "listeGizlilikTuru.TIP3"); 
 
		private int code;
		private String label;

		private ListeGizlilikTuru(int code, String label) {
		    this.code = code;
		    this.label = label;
		}

		@Override
		public int getCode() {
		    return code;
		}

		@Override
		public String getLabel() {
		    return KeyUtil.getMessageValue(label);
		}

		@Override
		public String toString() {
		    return KeyUtil.getMessageValue(label);
		}

		public static ListeGizlilikTuru getWithCode(int code){
			ListeGizlilikTuru[] enumArray = ListeGizlilikTuru.values();
			for(ListeGizlilikTuru enumObj : enumArray){ 
				if(enumObj.getCode() == code)
					return enumObj;
			}
			return ListeGizlilikTuru._NULL;
		} 
}   
