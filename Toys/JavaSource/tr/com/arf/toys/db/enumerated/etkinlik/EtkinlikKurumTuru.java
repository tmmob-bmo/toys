package tr.com.arf.toys.db.enumerated.etkinlik; 
 
import tr.com.arf.framework.db.enumerated._base.BaseEnumerated;
import tr.com.arf.framework.utility.tool.KeyUtil;
  
public enum EtkinlikKurumTuru implements BaseEnumerated {  
    
		_NULL(0, ""), 
		_DUZENLEYEN(1, "etkinlikKurumTuru.duzenleyen"),
		_DESTEKLEYEN(2, "etkinlikKurumTuru.destekleyen"),
		_SPONSOROLAN(3, "etkinlikKurumTuru.sponsorolan"); 
 
		private int code;
		private String label;

		private EtkinlikKurumTuru(int code, String label) {
		    this.code = code;
		    this.label = label;
		}

		@Override
		public int getCode() {
		    return code;
		}

		@Override
		public String getLabel() {
		    return KeyUtil.getMessageValue(label);
		}

		@Override
		public String toString() {
		    return KeyUtil.getMessageValue(label);
		}

		public static EtkinlikKurumTuru getWithCode(int code){
			EtkinlikKurumTuru[] enumArray = EtkinlikKurumTuru.values();
			for(EtkinlikKurumTuru enumObj : enumArray){ 
				if(enumObj.getCode() == code) {
					return enumObj;
				}
			}
			return EtkinlikKurumTuru._NULL;
		} 
}   
