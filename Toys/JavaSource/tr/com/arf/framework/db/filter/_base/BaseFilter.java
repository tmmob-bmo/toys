package tr.com.arf.framework.db.filter._base;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import tr.com.arf.framework.db.enumerated._base.BaseEnumerated;
import tr.com.arf.framework.db.model._base.BaseEntity;
import tr.com.arf.framework.db.model.query.QueryObject;
import tr.com.arf.framework.utility.tool.QueryUtil;

public abstract class BaseFilter implements Serializable {

	private static final long serialVersionUID = -27622396175562859L;

	private String masterDefinition;
	private String masterLink;

	public BaseFilter() {
	}

	public abstract void init();

	public abstract List<ArrayList<QueryObject>> createQueryCriterias();

	public String getMasterDefinition() {
		return masterDefinition;
	}

	public void setMasterDefinition(String masterDefinition) {
		this.masterDefinition = masterDefinition;
	}

	public String getMasterLink() {
		return masterLink;
	}

	public void setMasterLink(String masterLink) {
		this.masterLink = masterLink;
	}

	protected String letterCase(String value) {
		return value;
	}

	@SuppressWarnings("rawtypes")
	protected static boolean isEmpty(Collection col) {
		return col == null || col.size() == 0;
	}

	protected boolean isEmpty(String str) {
		return str == null || str.trim().length() == 0;
	}

	protected boolean validateQueryObject(Object object, Integer minValue, boolean enumerated) {
		if (object == null) {
			return false;
		}
		if (object instanceof BaseEntity) {
			if (((BaseEntity) object).getRID() == null) {
				return false;
			} else if (((BaseEntity) object).getRID().longValue() <= 0) {
				return false;
			} else {
				return true;
			}
		}
		if (object instanceof BaseEnumerated) {
			if (((BaseEnumerated) object).getCode() == 0) {
				return false;
			}
		} else if (object instanceof String) {
			if (((String) object).trim().length() == 0) {
				return false;
			}
		} else if (object instanceof Integer) {
			if (minValue == null) {
				minValue = 0;
			}
			if (!QueryUtil.isNumber(object + "")) {
				return false;
			} else if (((Integer) object).intValue() <= minValue) {
				return false;
			}
		} else if (object instanceof Byte) {
			if (minValue == null) {
				minValue = 0;
			}
			if (!QueryUtil.isNumber(object + "")) {
				return false;
			} else if (((Byte) object).intValue() <= minValue) {
				return false;
			}
		} else if (object instanceof Long) {
			if (minValue == null) {
				minValue = 0;
			}
			if (!QueryUtil.isNumber(object + "")) {
				return false;
			} else if (((Long) object).longValue() <= minValue) {
				return false;
			}
		} else if (object instanceof Double) {
			if (minValue == null) {
				minValue = 0;
			}
			if (!QueryUtil.isDouble(object + "")) {
				return false;
			} else {
				if (((Double) object).intValue() <= minValue) {
					return false;
				}
			}
		} else if (object instanceof BigDecimal) {
			if (!QueryUtil.isBigDecimal(object + "")) {
				return false;
			} else if (((BigDecimal) object).intValue() <= minValue) {
				return false;
			}
		}
		return true;
	}

	public void clearQueryCriterias() {
		if (this.getClass().getFields().length == 0) {
			return;
		}
		clearQueryCriterias2(0);
	}

	public void clearQueryCriterias2(int startIndex) {
		int i = startIndex;
		try {
			Field[] fields = this.getClass().getFields();
			for (i = startIndex; i < fields.length; i++) {
				fields[i].set(this, null);
			}
		} catch (Exception e) {
			i++;
			clearQueryCriterias2(i);
		}
	}
}
