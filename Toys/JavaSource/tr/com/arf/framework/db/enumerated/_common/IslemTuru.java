package tr.com.arf.framework.db.enumerated._common;

import tr.com.arf.framework.db.enumerated._base.BaseEnumerated;
import tr.com.arf.framework.utility.tool.KeyUtil;

public enum IslemTuru implements BaseEnumerated {

	_NULL(0, ""), _EKLEME(1, "islemTuru.ekleme"), _GUNCELLEME(2, "islemTuru.guncelleme"), _SILME(3, "islemTuru.silme");

	private int code;
	private String label;

	private IslemTuru(int code, String label) {
		this.code = code;
		this.label = label;
	}

	@Override
	public int getCode() {
		return code;
	}

	@Override
	public String getLabel() {
		return KeyUtil.getMessageValue(label);
	}

	@Override
	public String toString() {
		return KeyUtil.getMessageValue(label);
	}

	public static IslemTuru getWithCode(int code) {
		IslemTuru[] enumArray = IslemTuru.values();
		for (IslemTuru enumObj : enumArray) {
			if (enumObj.getCode() == code) {
				return enumObj;
			}
		}
		return IslemTuru._NULL;
	}
}
