package tr.com.arf.framework.db.enumerated._base;

public interface BaseEnumerated {

	public int getCode();

	public String getLabel();

	@Override
	public String toString();

}
