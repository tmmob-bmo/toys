package tr.com.arf.framework.db.enumerated._common;

import tr.com.arf.framework.db.enumerated._base.BaseEnumerated;
import tr.com.arf.framework.utility.tool.KeyUtil;

public enum VarYokTumu implements BaseEnumerated {

	_NULL(0, ""), _VAR(1, "varYokTumu.var"), _YOK(2, "varYokTumu.yok"), _TUMU(3, "varYokTumu.tumu");

	private int code;
	private String label;

	private VarYokTumu(int code, String label) {
		this.code = code;
		this.label = label;
	}

	@Override
	public int getCode() {
		return code;
	}

	@Override
	public String getLabel() {
		return KeyUtil.getMessageValue(label);
	}

	@Override
	public String toString() {
		return KeyUtil.getMessageValue(label);
	}

	public static VarYokTumu getWithCode(int code) {
		VarYokTumu[] enumArray = VarYokTumu.values();
		for (VarYokTumu enumObj : enumArray) {
			if (enumObj.getCode() == code) {
				return enumObj;
			}
		}
		return VarYokTumu._NULL;
	}

}