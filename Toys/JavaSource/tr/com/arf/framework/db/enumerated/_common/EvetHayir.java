package tr.com.arf.framework.db.enumerated._common;

import tr.com.arf.framework.db.enumerated._base.BaseEnumerated;
import tr.com.arf.framework.utility.tool.KeyUtil;

public enum EvetHayir implements BaseEnumerated {

	_NULL(0, ""), _EVET(1, "evetHayir.evet"), _HAYIR(2, "evetHayir.hayir");

	private int code;
	private String label;

	private EvetHayir(int code, String label) {
		this.code = code;
		this.label = label;
	}

	@Override
	public int getCode() {
		return code;
	}

	@Override
	public String getLabel() {
		return KeyUtil.getMessageValue(label);
	}

	@Override
	public String toString() {
		return KeyUtil.getMessageValue(label);
	}

	public static EvetHayir getWithCode(int code) {
		EvetHayir[] enumArray = EvetHayir.values();
		for (EvetHayir enumObj : enumArray) {
			if (enumObj.getCode() == code) {
				return enumObj;
			}
		}
		return EvetHayir._NULL;
	}
}