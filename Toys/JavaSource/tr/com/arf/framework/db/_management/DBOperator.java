package tr.com.arf.framework.db._management;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.ejb.Singleton;
import javax.faces.model.SelectItem;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;

import org.apache.log4j.Logger;

import tr.com.arf.framework.db.enumerated._common.IslemTuru;
import tr.com.arf.framework.db.model._base.BaseEntity;
import tr.com.arf.framework.db.model.query.QueryObject;
import tr.com.arf.framework.utility.application.ApplicationConstantBase;
import tr.com.arf.framework.utility.exception.data.DBException;
import tr.com.arf.framework.utility.tool.QueryUtil;
import tr.com.arf.toys.db.enumerated.iletisim.AdresTuru;
import tr.com.arf.toys.db.model.iletisim.Adres;
import tr.com.arf.toys.db.model.kisi.Kisi;
import tr.com.arf.toys.service.application.ManagedBeanLocator;

@SuppressWarnings("rawtypes")
@Singleton
public final class DBOperator implements Serializable {

	private static final long serialVersionUID = 6584843695829074802L;
	protected static Logger logger = Logger.getLogger(DBOperator.class);

	private static final DBOperator singleton = new DBOperator();

	private EntityManagerFactory entityManagerFactory = null;

	public static DBOperator getInstance() {
		return singleton;
	}

	public EntityManagerFactory getEntityManagerFactory() {
		if (entityManagerFactory == null) {
			entityManagerFactory = PersistenceManager.getInstance().getEntityManagerFactory();
		}
		return entityManagerFactory;
	}

	/* Data Load Methods */
	public List load(String entity) {
		return load(entity, "", "", null);
	}

	public List load(String entity, String orderAttribute) {
		return load(entity, "", orderAttribute, null);
	}

	public List load(String entity, String orderAttribute, Integer[] recordIntervals) {
		return load(entity, "", orderAttribute, recordIntervals);
	}

	public List load(String entity, String whereCondition, String orderAttribute, Integer[] recordIntervals) {
		List dataList = new ArrayList();
		EntityManager entityManager = getEntityManagerFactory().createEntityManager();
		try {
			String query = "SELECT OBJECT(o) FROM " + entity + " AS o";
			if (!isEmpty(whereCondition)) {
				query += " WHERE " + whereCondition;
			}

			if (!isEmpty(orderAttribute)) {
				query += " ORDER BY " + orderAttribute;
			}
			Query entityQuery = entityManager.createQuery(query);
			if (!isEmpty(recordIntervals) && recordIntervals.length == 2) {
				entityQuery.setFirstResult(recordIntervals[0]);
				entityQuery.setMaxResults(recordIntervals[1]);
			}
			dataList = entityQuery.getResultList();
		} catch (Exception exc) {
			logger.error(exc.toString(), exc);
		} finally {
			entityManager.close();
		}

		return dataList;
	}

	public List load(String entity, String queryCriteria, String orderAttribute) {
		return load(entity, queryCriteria, orderAttribute, null);
	}

	public List load(String entity, ArrayList queryCriteriaDualList, String orderAttribute) {
		return load(entity, queryCriteriaDualList, orderAttribute, null);
	}

	public List load(String entity, ArrayList queryCriteriaDualList, String orderAttribute, int[] recordIntervals) {
		List dataList = new ArrayList();
		EntityManager entityManager = getEntityManagerFactory().createEntityManager();
		if (queryCriteriaDualList.size() > 1) {
			try {
				StringBuffer queryString = new StringBuffer("SELECT o FROM " + entity + " o WHERE ");
				int y = 1;
				for (int x = 0; x < queryCriteriaDualList.size(); x++) {
					queryString.append(queryCriteriaDualList.get(x).toString() + " ?" + y + " ");
					y++;
					if (y > 1 && x < queryCriteriaDualList.size() - 2) {
						queryString.append(" AND ");
					}
					x++;
				}
				queryString.append(" ORDER BY " + orderAttribute);
				Query query = entityManager.createQuery(queryString.toString());
				if (recordIntervals != null && recordIntervals.length == 2) {
					query.setFirstResult(recordIntervals[0]);
					query.setMaxResults(recordIntervals[1]);
				}
				for (int x = 0; x < queryCriteriaDualList.size(); x++) {
					query.setParameter(x / 2 + 1, queryCriteriaDualList.get(x + 1));
					x++;
				}
				dataList = query.getResultList();
			} catch (Exception exc) {
				logger.error(exc.toString(), exc);
			} finally {
				entityManager.close();
			}

		}
		return dataList;
	}

	/* Find Methods */
	public List find(String entity, String attribute, String value) {
		EntityManager entityManager = getEntityManagerFactory().createEntityManager();
		String query = "SELECT OBJECT(o) FROM " + entity + " AS o WHERE o." + attribute + " = " + QueryUtil.loginStringCheck(value);
		try {
			Query entityQuery = entityManager.createQuery(query);
			// System.out.println("Query :" + query);
			List dataList = entityQuery.getResultList();
			return dataList;
		} catch (Exception exc) {
			logger.error(exc.toString(), exc);
			return null;
		} finally {
			entityManager.close();
		}
	}

	public BaseEntity find(String entity, String queryCriteria) {
		EntityManager entityManager = getEntityManagerFactory().createEntityManager();
		String query = "SELECT OBJECT(o) FROM " + entity + " AS o";
		BaseEntity selectedEntity = null;
		try {
			if (!isEmpty(queryCriteria)) {
				query += " WHERE " + queryCriteria;
			}
			try {
				Query entityQuery = entityManager.createQuery(query);
				if (!isEmpty(entityQuery.getResultList())) {
					selectedEntity = (BaseEntity) entityQuery.getSingleResult();
				}
			} catch (Exception e) {
				System.out.println("ERROR @find :" + e.getMessage());
				System.out.println("Query @find :" + query);
				return null;
			}
		} catch (Exception exc) {
			logger.error(exc.toString(), exc);
		} finally {
			entityManager.close();
		}
		return selectedEntity;
	}

	/* Inserts, Updates, Deletes lists of entities in same transaction */
	public void recordInOneTransaction(BaseEntity[] entitiesToBeInserted, BaseEntity[] entitiesToBeUpdated, BaseEntity[] entitiesToBeDeleted) throws DBException {
		List<BaseEntity> entitiesToBeInsertedList = null;
		List<BaseEntity> entitiesToBeUpdatedList = null;
		List<BaseEntity> entitiesToBeDeletedList = null;
		if (!isEmpty(entitiesToBeInserted)) {
			entitiesToBeInsertedList = Arrays.asList(entitiesToBeInserted);
		}
		if (!isEmpty(entitiesToBeUpdated)) {
			entitiesToBeUpdatedList = Arrays.asList(entitiesToBeUpdated);
		}
		if (!isEmpty(entitiesToBeDeleted)) {
			entitiesToBeDeletedList = Arrays.asList(entitiesToBeDeleted);
		}
		recordInOneTransaction(entitiesToBeInsertedList, entitiesToBeUpdatedList, entitiesToBeDeletedList);
	}

	/* Inserts, Updates, Deletes lists of entities in same transaction */
	public void recordInOneTransaction(List<BaseEntity> entitiesToBeInserted, List<BaseEntity> entitiesToBeUpdated, List<BaseEntity> entitiesToBeDeleted) throws DBException {
		EntityManager entityManager = getEntityManagerFactory().createEntityManager();
		try {
			entityManager.getTransaction().begin();
			if (!isEmpty(entitiesToBeInserted)) {
				for (BaseEntity entity : entitiesToBeInserted) {
					entityManager.persist(entity);
				}
			}
			if (!isEmpty(entitiesToBeUpdated)) {
				for (BaseEntity entity : entitiesToBeUpdated) {
					entityManager.merge(entity);
				}
			}
			if (!isEmpty(entitiesToBeDeleted)) {
				for (BaseEntity entity : entitiesToBeDeleted) {
					entityManager.remove(entityManager.merge(entity));
				}
			}
			entityManager.getTransaction().commit();
		} catch (Exception exc) {
			if (entityManager.getTransaction().isActive()) {
				entityManager.getTransaction().rollback();
			}
			logger.error(exc.toString(), exc);
			throw new DBException(exc, this.getClass().getName(), "Ekleme", " Many recs...");
		} finally {
			entityManager.close();
		}
	}

	/* Insert, Update, Delete one of each entities in same transaction */
	public void recordInOneTransaction(BaseEntity entityToBeInserted, BaseEntity entityToBeUpdated, BaseEntity entityToBeDeleted) throws DBException {
		EntityManager entityManager = getEntityManagerFactory().createEntityManager();
		try {
			entityManager.getTransaction().begin();
			if (entityToBeInserted != null) {
				entityManager.persist(entityToBeInserted);
			}
			if (entityToBeUpdated != null) {
				entityManager.merge(entityToBeUpdated);
			}
			if (entityToBeDeleted != null) {
				entityManager.remove(entityManager.merge(entityToBeDeleted));
			}
			entityManager.getTransaction().commit();
		} catch (Exception exc) {
			if (entityManager.getTransaction().isActive()) {
				entityManager.getTransaction().rollback();
			}
			logger.error(exc.toString(), exc);
			throw new DBException(exc, this.getClass().getName(), "Ekleme", " Many recs...");
		} finally {
			entityManager.close();
		}
	}

	/* Insert Methods */
	public void insert(BaseEntity entity) throws DBException {
		EntityManager entityManager = getEntityManagerFactory().createEntityManager();
		try {
			entityManager.getTransaction().begin();
			entityManager.persist(entity);
			entityManager.getTransaction().commit();
		} catch (Exception exc) {
			if (entityManager.getTransaction().isActive()) {
				entityManager.getTransaction().rollback();
			}
			logger.error(exc.toString(), exc);
			throw new DBException(exc, this.getClass().getName(), "Ekleme", "Kayıt: " + entity.getUIString());
		} finally {
			entityManager.close();
		}
	}

	/* Update Methods */
	public void update(BaseEntity entity) throws DBException {
		EntityManager entityManager = getEntityManagerFactory().createEntityManager();
		try {
			entityManager.getTransaction().begin();
			entityManager.merge(entity);
			entityManager.getTransaction().commit();
		} catch (Exception exc) {
			if (entityManager.getTransaction().isActive()) {
				entityManager.getTransaction().rollback();
			}
			logger.error(exc.toString(), exc);
			throw new DBException(exc, this.getClass().getName(), "Gunleme", "Kayıt: " + entity.getUIString());
		} finally {
			entityManager.close();
		}
	}

	/* Delete Methods */
	public boolean delete(BaseEntity entity) throws DBException {
		EntityManager entityManager = getEntityManagerFactory().createEntityManager();
		try {
			entityManager.getTransaction().begin();
			entityManager.remove(entityManager.merge(entity));
			entityManager.getTransaction().commit();
			return true;
		} catch (Exception exc) {
			if (entityManager.getTransaction().isActive()) {
				entityManager.getTransaction().rollback();
			}
			logger.error(exc.toString(), exc);
			throw new DBException(exc, this.getClass().getName(), "Delete", "Kayıt: " + entity.getUIString());
		} finally {
			entityManager.close();
		}
	}

	/* Record Count Methods */
	public int recordCount(String entity) {
		try {
			return recordCount(entity, "");
		} catch (Exception exc) {
			logger.error(exc.toString(), exc);
		}
		return 0;
	}

	public int recordCount(String entity, String queryCriteria) throws DBException {
		EntityManager entityManager = getEntityManagerFactory().createEntityManager();
		String query = "SELECT COUNT(o) FROM " + entity + " AS o";
		if (!isEmpty(queryCriteria)) {
			query += " WHERE " + queryCriteria;
		}
		int count = 0;
		try {
			Query entityQuery = entityManager.createQuery(query);
			count = ((Long) entityQuery.getSingleResult()).intValue();
		} catch (Exception exc) {
			logger.error(exc.toString(), exc);
		} finally {
			entityManager.close();
		}
		return count;
	}

	public int recordCount(String entity, ArrayList<String> queryCriteriaDualList) {
		EntityManager entityManager = getEntityManagerFactory().createEntityManager();

		StringBuffer queryString = new StringBuffer("SELECT COUNT(o) FROM " + entity + " o WHERE ");
		int y = 1;
		for (int x = 0; x < queryCriteriaDualList.size(); x++) {
			queryString.append(queryCriteriaDualList.get(x).toString() + " ?" + y + " ");
			y++;
			if (y > 1 && x < queryCriteriaDualList.size() - 2) {
				queryString.append(" AND ");
			}
			x++;
		}
		int count = 0;
		try {
			Query query = entityManager.createQuery(queryString.toString());
			for (int x = 0; x < queryCriteriaDualList.size(); x++) {
				query.setParameter(x / 2 + 1, queryCriteriaDualList.get(x + 1));
				x++;
			}
			count = ((Long) query.getSingleResult()).intValue();
		} catch (Exception exc) {
			logger.error(exc.toString(), exc);
		} finally {
			entityManager.close();
		}
		return count;
	}

	public Object recordCountByNativeQuery(String query) throws Exception {
		EntityManager entityManager = getEntityManagerFactory().createEntityManager();
		int count = 0;
		try {
			Query entityQuery = entityManager.createNativeQuery(query);
			return entityQuery.getSingleResult();
		} catch (Exception exc) {
			logger.error(exc.toString(), exc);
		} finally {
			entityManager.close();
		}

		return count;
	}

	public List loadWithConditions(String modelName, String[] queryColumns, String[] queryValues, int maxResults, String staticWhereCondition) {
		EntityManager entityManager = getEntityManagerFactory().createEntityManager();
		StringBuffer queryString = new StringBuffer("SELECT o FROM " + modelName + " o WHERE 1=1 ");
		try {
			Integer inx = 1;
			if (staticWhereCondition != null && staticWhereCondition.trim().length() > 0) {
				queryString.append(" AND ").append(staticWhereCondition);
			}

			int values = 1;
			for (String column : queryColumns) {
				if (values == 1) {
					queryString.append(" AND (UPPER(o." + column + ") LIKE UPPER(?) ");
				} else {
					queryString.append(" OR UPPER(o." + column + ") LIKE UPPER(?) ");
				}
				values++;
			}
			queryString.append(")");
			Query query = entityManager.createQuery(queryString.toString());
			for (String value : queryValues) {
				query.setParameter(inx, "%" + value + "%");
				inx++;
			}
			query.setMaxResults(maxResults);
			return query.getResultList();
		} catch (Exception exc) {
			logger.error(exc.toString(), exc);
			return null;
		}
	}

	private String createConditions(ArrayList<QueryObject> kriterler, String conditionType) {
		StringBuilder result = new StringBuilder();
		int x = 0;
		String condType = conditionType;
		for (QueryObject qObj : kriterler) {
			String columnName = qObj.getColumnName();
			if (!qObj.isCaseSensitive() && qObj.getSearchType() != ApplicationConstantBase._userDefined) {
				columnName = "upper(o." + columnName + ")";
			} else if (qObj.getSearchType() != ApplicationConstantBase._userDefined) {
				columnName = "o." + columnName;
			}
			if (x == 0) {
				condType = "";
			} else {
				condType = conditionType;
			}
			if (!isEmpty(qObj.getConstantQueryCriteria())) {
				result.append(" ").append(condType).append(" ").append(qObj.getConstantQueryCriteria()).append(" ");
			} else if (qObj.getSearchType() == ApplicationConstantBase._equals) {
				result.append(" ").append(condType).append(" ").append(columnName).append(" = ? ");
			} else if (qObj.getSearchType() == ApplicationConstantBase._userDefined) {
				result.append(" ").append(condType).append(" ").append(columnName).append(" ");
			} else if (qObj.getSearchType() == ApplicationConstantBase._beginsWith || qObj.getSearchType() == ApplicationConstantBase._inside) {
				if (qObj.isCaseSensitive()) {
					result.append(" ").append(condType).append(" ").append(columnName).append(" LIKE ? ");
				} else {
					result.append(" ").append(condType).append(" (").append(columnName).append(" LIKE UPPER(?) ");
					result.append(" OR ").append(" ").append(columnName.replace("upper", "lower")).append(" LIKE LOWER(?)) ");
				}
			} else if (qObj.getSearchType() == ApplicationConstantBase._smallerThan) {
				result.append(" ").append(condType).append(" ").append(columnName).append(" < ? ");
			} else if (qObj.getSearchType() == ApplicationConstantBase._largerThan) {
				result.append(" ").append(condType).append(" ").append(columnName).append(" > ? ");
			} else if (qObj.getSearchType() == ApplicationConstantBase._equalsAndSmallerThan) {
				result.append(" ").append(condType).append(" ").append(columnName).append(" <= ? ");
			} else if (qObj.getSearchType() == ApplicationConstantBase._equalsAndLargerThan) {
				result.append(" ").append(condType).append(" ").append(columnName).append(" >= ? ");
			} else if (qObj.getSearchType() == ApplicationConstantBase._notEquals) {
				result.append(" ").append(condType).append(" ").append(columnName).append(" <> ? ");
			}
			x++;
		}
		return result.toString();
	}

	private Query setQueryParams(Query query, ArrayList<QueryObject> kriterler, ArrayList<QueryObject> opsiyonelKriterler) {
		Integer inx = 1;
		for (QueryObject qObj : kriterler) {
			if (qObj.getSearchType() != ApplicationConstantBase._userDefined && isEmpty(qObj.getConstantQueryCriteria())) {
				if (qObj.getSearchType() == ApplicationConstantBase._equals || qObj.getSearchType() == ApplicationConstantBase._smallerThan || qObj.getSearchType() == ApplicationConstantBase._largerThan
						|| qObj.getSearchType() == ApplicationConstantBase._equalsAndSmallerThan || qObj.getSearchType() == ApplicationConstantBase._equalsAndLargerThan || qObj.getSearchType() == ApplicationConstantBase._notEquals) {
					query.setParameter(inx, qObj.getSearchValue());
				} else {
					if (qObj.isCaseSensitive()) {
						if (qObj.getSearchType() == ApplicationConstantBase._beginsWith) {
							query.setParameter(inx, qObj.getSearchValue() + "%");
						} else {
							query.setParameter(inx, "%" + qObj.getSearchValue() + "%");
						}
					} else {
						if (qObj.getSearchType() == ApplicationConstantBase._beginsWith) {
							query.setParameter(inx, qObj.getSearchValue().toString().replace("i", "İ") + "%");
							++inx;
							query.setParameter(inx, qObj.getSearchValue() + "%");
						} else {
							query.setParameter(inx, "%" + qObj.getSearchValue() + "%");
							++inx;
							query.setParameter(inx, "%" + qObj.getSearchValue() + "%");
						}
					}
				}
				++inx;
			}
		}
		if (!isEmpty(opsiyonelKriterler)) {
			for (QueryObject qObj : opsiyonelKriterler) {
				if (qObj.getSearchType() != ApplicationConstantBase._userDefined && isEmpty(qObj.getConstantQueryCriteria())) {
					if (qObj.getSearchType() == ApplicationConstantBase._equals || qObj.getSearchType() == ApplicationConstantBase._smallerThan || qObj.getSearchType() == ApplicationConstantBase._largerThan
							|| qObj.getSearchType() == ApplicationConstantBase._equalsAndSmallerThan || qObj.getSearchType() == ApplicationConstantBase._equalsAndLargerThan || qObj.getSearchType() == ApplicationConstantBase._notEquals) {
						query.setParameter(inx, qObj.getSearchValue());
					} else {
						// System.out.println("SETQUERYPARAMS ELSE :" +
						// qObj.getSearchValue());
						// if(!qObj.isCaseSensitive() && qObj.getSearchValue()
						// instanceof String){
						// qObj.setSearchValue(qObj.getSearchValue().toString().replace("ı",
						// "i"));
						// }
						if (qObj.isCaseSensitive()) {
							if (qObj.getSearchType() == ApplicationConstantBase._beginsWith) {
								query.setParameter(inx, qObj.getSearchValue() + "%");
							} else {
								query.setParameter(inx, "%" + qObj.getSearchValue() + "%");
							}
						} else {
							if (qObj.getSearchType() == ApplicationConstantBase._beginsWith) {
								query.setParameter(inx, qObj.getSearchValue() + "%");
								++inx;
								query.setParameter(inx, qObj.getSearchValue() + "%");
							} else {
								query.setParameter(inx, "%" + qObj.getSearchValue() + "%");
								++inx;
								query.setParameter(inx, "%" + qObj.getSearchValue() + "%");
							}
						}
					}
					++inx;
				}
			}
		}
		return query;
	}

	public List loadFiltered(String tableName, ArrayList<QueryObject> kriterler, ArrayList<QueryObject> opsiyonelKriterler, int[] recordIntervals, String orderField) {
		return loadFiltered(tableName, "", kriterler, opsiyonelKriterler, recordIntervals, orderField);
	}

	public List loadFiltered(String tableName, String queryCriteria, ArrayList<QueryObject> kriterler, ArrayList<QueryObject> opsiyonelKriterler, int[] recordIntervals, String orderField) {
		EntityManager entityManager = getEntityManagerFactory().createEntityManager();
		StringBuilder hql = new StringBuilder(" FROM " + tableName + " o WHERE ");
		if (!isEmpty(queryCriteria)) {
			hql.append(queryCriteria).append(" AND ");
		}
		try {
			hql.append(createConditions(kriterler, "AND"));
			if (!isEmpty(opsiyonelKriterler)) {
				hql.append(" AND ( ");
				hql.append(createConditions(opsiyonelKriterler, "OR"));
				hql.append(")");
			}
			if (!isEmpty(orderField)) {
				hql.append(" ORDER BY " + orderField);
			}
			// System.out.println("loadFiltered (0) query :" + hql.toString());
			Query query = entityManager.createQuery(hql.toString());
			query = setQueryParams(query, kriterler, opsiyonelKriterler);
			if (recordIntervals != null && recordIntervals.length == 2) {
				query.setFirstResult(recordIntervals[0]);
				query.setMaxResults(recordIntervals[1]);
			}
			return query.getResultList();
		} catch (Exception e) {
			System.out.println("METHOD : loadFiltered (0)");
			System.out.println("ERROR : " + e.getMessage());
			System.out.println("QUERY : " + hql.toString());
			return null;
		} finally {
			entityManager.close();
		}
	}

	public Long recordCountFiltered(String tableName, ArrayList<QueryObject> kriterler, ArrayList<QueryObject> opsiyonelKriterler) {
		EntityManager entityManager = getEntityManagerFactory().createEntityManager();
		StringBuilder hql = new StringBuilder("SELECT COUNT(*) FROM " + tableName + " o WHERE ");
		try {
			hql.append(createConditions(kriterler, "AND"));
			if (!isEmpty(opsiyonelKriterler)) {
				hql.append(" AND ( ");
				hql.append(createConditions(opsiyonelKriterler, "OR"));
				hql.append(")");
			}
			// System.out.println("Hql @recordCountFiltered :" + hql.toString());
			Query query = entityManager.createQuery(hql.toString());
			query = setQueryParams(query, kriterler, opsiyonelKriterler);
			// System.out.println("Record count :" + query.getSingleResult());
			return (Long) query.getSingleResult();
		} catch (Exception e) {
			System.out.println("METHOD : recordCountFiltered (0)");
			System.out.println("ERROR : " + e.getMessage());
			System.out.println("QUERY : " + hql.toString());
			return null;
		} finally {
			entityManager.close();
		}
	}

	public Long recordCountAll(String tableName) {
		EntityManager entityManager = getEntityManagerFactory().createEntityManager();
		StringBuilder hql = new StringBuilder("SELECT COUNT(rID) FROM " + tableName);
		try {
			Query query = entityManager.createQuery(hql.toString());
			return (Long) query.getSingleResult();
		} catch (Exception e) {
			System.out.println("METHOD : recordCountAll (0)");
			System.out.println("ERROR : " + e.getMessage());
			System.out.println("QUERY : " + hql.toString());
			return null;
		} finally {
			entityManager.close();
		}
	}

	public List loadAll(String tableName, int[] recordIntervals, String orderField) {
		EntityManager entityManager = getEntityManagerFactory().createEntityManager();
		StringBuilder hql = new StringBuilder(" FROM " + tableName);
		if (!isEmpty(orderField)) {
			hql.append(" ORDER BY " + orderField.replace("o.", ""));
		}
		try {
			Query query = entityManager.createQuery(hql.toString());
			if (recordIntervals != null && recordIntervals.length == 2) {
				query.setFirstResult(recordIntervals[0]);
				query.setMaxResults(recordIntervals[1]);
			}
			return query.getResultList();
		} catch (Exception e) {
			System.out.println("METHOD : loadAll (0)");
			System.out.println("ERROR : " + e.getMessage());
			System.out.println("QUERY : " + hql.toString());
			return null;
		} finally {
			entityManager.close();
		}
	}

	public int executeQuery(String query) throws DBException {
		EntityManager entityManager = getEntityManagerFactory().createEntityManager();
		int result = 0;
		try {
			// System.out.println("Execute this query : " + query);
			entityManager.getTransaction().begin();
			Query entityQuery = entityManager.createQuery(query);
			result = entityQuery.executeUpdate();
			entityManager.getTransaction().commit();
		} catch (Exception e) {
			if (entityManager.getTransaction().isActive()) {
				entityManager.getTransaction().rollback();
			}
			System.out.println("METHOD : executeQuery (0)");
			System.out.println("ERROR : " + e.getMessage());
			System.out.println("QUERY : " + query);
			return 0;
		} finally {
			entityManager.close();
		}
		return result;
	}

	public Object sum(String columnToBeSummed, String tableName, String whereCondition) throws DBException {
		EntityManager entityManager = getEntityManagerFactory().createEntityManager();
		String queryText = "SELECT SUM(o." + columnToBeSummed + ") FROM " + tableName + " o";
		Object result = null;
		try {
			if (!isEmpty(whereCondition)) {
				queryText += " WHERE " + whereCondition;
			}
			Query query = entityManager.createQuery(queryText);
			result = query.getResultList().get(0);
			return result;
		} catch (Exception e) {
			System.out.println("METHOD : sum (0)");
			System.out.println("ERROR : " + e.getMessage());
			System.out.println("QUERY : " + queryText);
			return 0;
		} finally {
			entityManager.close();
		}
	}

	public List loadByQuery(String query) {
		try {
			EntityManager entityManager = getEntityManagerFactory().createEntityManager();
			Query entityQuery = entityManager.createQuery(query);
			List dataList = entityQuery.getResultList();
			entityManager.close();
			return dataList;
		} catch (Exception e) {
			System.out.println("ERROR : " + e.getMessage());
			System.out.println("QUERY : " + query);
			return null;
		}
	}

	public List loadByNativeQuery(String nativeQuery) {
		try {
			EntityManager entityManager = getEntityManagerFactory().createEntityManager();
			Query entityQuery = entityManager.createNativeQuery(nativeQuery);
			List dataList = entityQuery.getResultList();
			entityManager.close();
			return dataList;
		} catch (Exception e) {
			System.out.println("ERROR @loadByNativeQuery: " + e.getMessage());
			System.out.println("QUERY : " + nativeQuery);
			return null;
		}
	}

	private static boolean isEmpty(Collection col) {
		return col == null || col.size() == 0;
	}

	private static boolean isEmpty(Object[] col) {
		return col == null || col.length == 0;
	}

	private boolean isEmpty(String str) {
		return str == null || str.trim().length() == 0;
	}

	/* Tum entity'ler icin log kaydi yapan metod. */
	public void logKaydet(BaseEntity entity, IslemTuru islemTuru, Long kullaniciRID, String kullaniciText) {
		EntityManager entityManager = getEntityManagerFactory().createEntityManager();
		try {
			String query = "INSERT INTO " + entity.getClass().getSimpleName() + "LOG(islemturu, islemtarihi, referansrid, yenideger, kullanicirid, kullanicitext)"
					+ " VALUES (:islemTuru, :islemTarihi, :referansRID, :yeniDeger, :kullaniciRID, :kullaniciText) ";
			Query entityQuery = entityManager.createNativeQuery(query);
			entityQuery.setParameter("islemTuru", islemTuru.getCode());
			entityQuery.setParameter("islemTarihi", new Date());
			entityQuery.setParameter("islemTuru", islemTuru.getCode());
			entityQuery.setParameter("referansRID", entity.getRID());
			entityQuery.setParameter("yeniDeger", entity.getValue());
			entityQuery.setParameter("kullaniciRID", kullaniciRID);
			entityQuery.setParameter("kullaniciText", kullaniciText);
			entityQuery.executeUpdate();
		} catch (Exception e) {
			System.out.println("ERROR @logKaydet :" + e.getMessage());
		} finally {
			entityManager.close();
		}
	}

	@SuppressWarnings("unchecked")
	public SelectItem[] getAdresTuruItems(Kisi kisi) {
		try {
			List<Adres> adresList = load(Adres.class.getSimpleName(), "o.kisiRef.rID = " + kisi.getRID(), "o.adresturu ASC", null);			
			ArrayList<SelectItem> preList = new ArrayList<SelectItem>(Arrays.asList(ManagedBeanLocator.locateEnumeratedController().getAdresTuruItems()));
			ArrayList<SelectItem> resultList = new ArrayList<SelectItem>(preList);
			if (adresList != null && adresList.size() > 0) {
				for (Adres ad : adresList) {
					for (int x = 0; x < preList.size(); x++) {
						SelectItem item = preList.get(x); 
						if (ad.getAdresturu().getCode() == ((AdresTuru) item.getValue()).getCode()) {
							resultList.remove(item);
						}
					}
				}
			}
			SelectItem[] resultArray = new SelectItem[resultList.size()];
			resultArray = resultList.toArray(resultArray);
			return resultArray;
		} catch (Exception e) {
			System.out.println("ERROR @getAdresTuruItems :" + e.getMessage());
		}
		return new SelectItem[0];
	}
}
