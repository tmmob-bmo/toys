package tr.com.arf.framework.db._management;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.hibernate.Query;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import tr.com.arf.framework.db.model._base.BaseEntity;
import tr.com.arf.framework.db.model.query.QueryObject;
import tr.com.arf.framework.utility.application.ApplicationConstantBase;
import tr.com.arf.framework.utility.exception.data.DBException;
import tr.com.arf.framework.utility.tool.QueryUtil;

@SuppressWarnings("rawtypes")
public final class HibernateOperator implements Serializable {

	private static final long serialVersionUID = 6584843695829074802L;

	public HibernateOperator() {
	}

	public Session getSession() throws DBException {
		SessionFactory sf = HibernateUtil.getSessionFactory();
		if (sf.getCurrentSession() != null) {
			return sf.getCurrentSession();
		} else {
			Session session = sf.openSession();
			try {
				session.beginTransaction();
			} catch (Exception e) {
				if (session.getTransaction().isActive()) {
					session.getTransaction().rollback();
				}
				e.printStackTrace();
				throw new DBException(e, this.getClass().getName(), "Begin Transaction");
			}
			return session;
		}
	}

	/* Data Load Methods */
	public List load(String entity) {
		String query = "";
		Session hibernateSession = null;
		try {
			hibernateSession = getSession();
			query = "SELECT OBJECT(o) FROM " + entity + " AS o";
			org.hibernate.Query entityQuery = hibernateSession.createQuery(query);
			List dataList = entityQuery.list();
			hibernateSession.close();
			return dataList;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public List load(String entity, String orderAttribute) {
		String query = "";
		try {
			Session hibernateSession = getSession();
			query = "SELECT OBJECT(o) FROM " + entity + " AS o";
			if (!orderAttribute.trim().equalsIgnoreCase("")) {
				query += " ORDER BY " + orderAttribute;
			}
			org.hibernate.Query entityQuery = hibernateSession.createQuery(query);
			List dataList = entityQuery.list();
			hibernateSession.close();
			return dataList;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public List load(String entity, String orderAttribute, int[] recordIntervals) {
		List dataList = new ArrayList();
		Session hibernateSession = null;
		try {
			hibernateSession = getSession();
			String query = "SELECT OBJECT(o) FROM " + entity + " AS o";
			if (!orderAttribute.trim().equalsIgnoreCase("")) {
				query += " ORDER BY " + orderAttribute;
			}
			org.hibernate.Query entityQuery = hibernateSession.createQuery(query);
			entityQuery.setFirstResult(recordIntervals[0]);
			entityQuery.setMaxResults(recordIntervals[1]);
			dataList = entityQuery.list();
		} catch (Exception e) {
			e.printStackTrace();
			try {
				throw new DBException(e, this.getClass().getName(), "Load", "Kayıt: ");
			} catch (DBException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		} finally {
			if (hibernateSession.isOpen()) {
				hibernateSession.close();
			}
		}

		return dataList;
	}

	public List load(String entity, String queryCriteria, String orderAttribute) {
		String query = "";
		try {
			Session hibernateSession = getSession();
			query = "SELECT OBJECT(o) FROM " + entity + " AS o";
			if (!queryCriteria.trim().equalsIgnoreCase("")) {
				query += " WHERE " + queryCriteria;
			}
			if (!orderAttribute.trim().equalsIgnoreCase("")) {
				query += " ORDER BY " + orderAttribute;
			}
			org.hibernate.Query entityQuery = hibernateSession.createQuery(query);
			List dataList = entityQuery.list();
			hibernateSession.close();
			return dataList;
		} catch (Exception e) {
			System.out.println("ERROR  @load , Query : " + query);
			e.printStackTrace();
			return null;
		}
	}

	public List load(String entity, ArrayList queryCriteriaDualList, String orderAttribute) {
		List dataList = new ArrayList();
		Session hibernateSession = null;
		if (queryCriteriaDualList.size() > 1) {
			try {
				hibernateSession = getSession();
				StringBuffer queryString = new StringBuffer("SELECT o FROM " + entity + " o WHERE ");
				int y = 1;
				for (int x = 0; x < queryCriteriaDualList.size(); x++) {
					queryString.append(queryCriteriaDualList.get(x).toString() + " ?" + y + " ");
					y++;
					if (y > 1 && x < queryCriteriaDualList.size() - 2) {
						queryString.append(" AND ");
					}
					x++;
				}
				queryString.append(" ORDER BY " + orderAttribute);
				Query query = hibernateSession.createQuery(queryString.toString());

				for (int x = 0; x < queryCriteriaDualList.size(); x++) {
					System.out.println("Parameters :" + queryCriteriaDualList.get(x + 1).toString());
					query.setParameter(x / 2 + 1, queryCriteriaDualList.get(x + 1));
					x++;
				}
				dataList = query.list();
			} catch (Exception e) {
				try {
					System.out.println("HATA Load 1 :" + e.getMessage());
					throw new DBException(e, this.getClass().getName(), "Load", "Kayıt: ");
				} catch (DBException e1) {
					// TODO Auto-generated catch block
					System.out.println("HATA Load 1 :" + e.getMessage());
				}
			} finally {
				if (hibernateSession.isOpen()) {
					hibernateSession.close();
				}
			}

		}
		return dataList;
	}

	public List load(String entity, ArrayList queryCriteriaDualList, String orderAttribute, int[] recordIntervals) {
		List dataList = new ArrayList();
		Session hibernateSession = null;
		if (queryCriteriaDualList.size() > 1) {
			try {
				hibernateSession = getSession();
				StringBuffer queryString = new StringBuffer("SELECT o FROM " + entity + " o WHERE ");
				int y = 1;
				for (int x = 0; x < queryCriteriaDualList.size(); x++) {
					queryString.append(queryCriteriaDualList.get(x).toString() + " ?" + y + " ");
					y++;
					if (y > 1 && x < queryCriteriaDualList.size() - 2) {
						queryString.append(" AND ");
					}
					x++;
				}
				queryString.append(" ORDER BY " + orderAttribute);
				Query query = hibernateSession.createQuery(queryString.toString());
				query.setFirstResult(recordIntervals[0]);
				query.setMaxResults(recordIntervals[1]);
				for (int x = 0; x < queryCriteriaDualList.size(); x++) {
					query.setParameter(x / 2 + 1, queryCriteriaDualList.get(x + 1));
					x++;
				}
				dataList = query.list();
			} catch (Exception e) {
				try {
					System.out.println("HATA Load 1 :" + e.getMessage());
					throw new DBException(e, this.getClass().getName(), "Load", "Kayıt: ");
				} catch (DBException e1) {
					// TODO Auto-generated catch block
					System.out.println("HATA Load 1 :" + e.getMessage());
				}
			} finally {
				if (hibernateSession.isOpen()) {
					hibernateSession.close();
				}
			}

		}
		return dataList;
	}

	/* Find Methods */
	public List find(String entity, String attribute, String value) {
		Session hibernateSession = null;
		String query = "SELECT OBJECT(o) FROM " + entity + " AS o WHERE o." + attribute + " = " + QueryUtil.loginStringCheck(value);
		try {
			hibernateSession = getSession();
			org.hibernate.Query entityQuery = hibernateSession.createQuery(query);
			// System.out.println("Query :" + query);
			List dataList = entityQuery.list();
			return dataList;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
			if (hibernateSession.isOpen()) {
				hibernateSession.close();
			}
		}
	}

	public BaseEntity find(String entity, String queryCriteria) {
		Session hibernateSession = null;
		String query = "SELECT OBJECT(o) FROM " + entity + " AS o";
		BaseEntity selectedEntity = null;
		try {
			hibernateSession = getSession();
			if (!queryCriteria.trim().equalsIgnoreCase("")) {
				query += " WHERE " + queryCriteria;
			}
			try {
				org.hibernate.Query entityQuery = hibernateSession.createQuery(query);
				if (!isEmpty(entityQuery.list())) {
					selectedEntity = (BaseEntity) entityQuery.uniqueResult();
				}
			} catch (Exception e) {
				System.out.println("ERROR @find :" + e.getMessage());
				System.out.println("Query @find :" + query);
				return null;
			}
		} catch (Exception e) {
			e.printStackTrace();
			try {
				throw new DBException(e, this.getClass().getName(), "Insert", "Kayıt: " + entity.toString());
			} catch (DBException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		} finally {
			if (hibernateSession.isOpen()) {
				hibernateSession.close();
			}
		}
		return selectedEntity;
	}

	/* Inserts, Updates, Deletes lists of entities in same transaction */
	public void recordInOneTransaction(BaseEntity[] entitiesToBeInserted, BaseEntity[] entitiesToBeUpdated, BaseEntity[] entitiesToBeDeleted) throws DBException {
		Session hibernateSession = getSession();
		try {
			hibernateSession.getTransaction().begin();
			if (!isEmpty(entitiesToBeInserted)) {
				for (BaseEntity entity : entitiesToBeInserted) {
					hibernateSession.persist(entity);
				}
			}
			if (!isEmpty(entitiesToBeUpdated)) {
				for (BaseEntity entity : entitiesToBeUpdated) {
					hibernateSession.merge(entity);
				}
			}
			if (!isEmpty(entitiesToBeDeleted)) {
				for (BaseEntity entity : entitiesToBeDeleted) {
					hibernateSession.delete(hibernateSession.merge(entity));
				}
			}
			hibernateSession.getTransaction().commit();
		} catch (Exception e) {
			if (hibernateSession.getTransaction().isActive()) {
				hibernateSession.getTransaction().rollback();
			}
			e.printStackTrace();
			throw new DBException(e, this.getClass().getName(), "Ekleme", " Many recs...");
		} finally {
			if (hibernateSession.isOpen()) {
				hibernateSession.close();
			}
		}
	}

	/* Insert, Update, Delete one of each entities in same transaction */
	public void recordInOneTransaction(BaseEntity entityToBeInserted, BaseEntity entityToBeUpdated, BaseEntity entityToBeDeleted) throws DBException {
		Session hibernateSession = getSession();
		try {
			hibernateSession.getTransaction().begin();
			if (entityToBeInserted != null) {
				hibernateSession.persist(entityToBeInserted);
			}
			if (entityToBeUpdated != null) {
				hibernateSession.merge(entityToBeUpdated);
			}
			if (entityToBeDeleted != null) {
				hibernateSession.delete(hibernateSession.merge(entityToBeDeleted));
			}
			hibernateSession.getTransaction().commit();
		} catch (Exception e) {
			if (hibernateSession.getTransaction().isActive()) {
				hibernateSession.getTransaction().rollback();
			}
			e.printStackTrace();
			throw new DBException(e, this.getClass().getName(), "Ekleme", " Many recs...");
		} finally {
			if (hibernateSession.isOpen()) {
				hibernateSession.close();
			}
		}
	}

	/* Insert Methods */
	public void insert(BaseEntity entity) throws DBException {
		Session hibernateSession = getSession();
		try {
			hibernateSession.getTransaction().begin();
			hibernateSession.persist(entity);
			hibernateSession.getTransaction().commit();
		} catch (Exception e) {
			if (hibernateSession.getTransaction().isActive()) {
				hibernateSession.getTransaction().rollback();
			}
			e.printStackTrace();
			throw new DBException(e, this.getClass().getName(), "Ekleme", "Kayıt: " + entity.getUIString());
		} finally {
			if (hibernateSession.isOpen()) {
				hibernateSession.close();
			}
		}
	}

	/* Update Methods */
	public void update(BaseEntity entity) throws DBException {
		Session hibernateSession = getSession();
		try {
			hibernateSession.getTransaction().begin();
			hibernateSession.merge(entity);
			hibernateSession.getTransaction().commit();
		} catch (Exception e) {
			if (hibernateSession.getTransaction().isActive()) {
				hibernateSession.getTransaction().rollback();
			}
			e.printStackTrace();
			throw new DBException(e, this.getClass().getName(), "Gunleme", "Kayıt: " + entity.getUIString());
		} finally {
			if (hibernateSession.isOpen()) {
				hibernateSession.close();
			}
		}
	}

	/* Delete Methods */
	public boolean delete(BaseEntity entity) throws DBException {
		Session hibernateSession = getSession();
		try {
			hibernateSession.getTransaction().begin();
			hibernateSession.delete(hibernateSession.merge(entity));
			hibernateSession.getTransaction().commit();
			return true;
		} catch (Exception e) {
			if (hibernateSession.getTransaction().isActive()) {
				hibernateSession.getTransaction().rollback();
			}
			e.printStackTrace();
			throw new DBException(e, this.getClass().getName(), "Delete", "Kayıt: " + entity.getUIString());
		} finally {
			if (hibernateSession.isOpen()) {
				hibernateSession.close();
			}
		}
	}

	/* Record Count Methods */
	public int recordCount(String entity) {
		Session hibernateSession = null;
		String query = "SELECT COUNT(o) FROM " + entity + " AS o";
		int count = 0;
		try {
			hibernateSession = getSession();
			org.hibernate.Query entityQuery = hibernateSession.createQuery(query);
			count = ((Long) entityQuery.uniqueResult()).intValue();
		} catch (Exception e) {
			e.printStackTrace();
			try {
				throw new DBException(e, this.getClass().getSimpleName(), "recordCount(String entity)", "Kayıt: ");
			} catch (DBException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		} finally {
			if (hibernateSession.isOpen()) {
				hibernateSession.close();
			}
		}
		return count;
	}

	public int recordCount(String entity, String queryCriteria) throws DBException {
		Session hibernateSession = getSession();
		String query = "SELECT COUNT(o) FROM " + entity + " AS o";
		if (queryCriteria != null && !queryCriteria.trim().equalsIgnoreCase("")) {
			query += " WHERE " + queryCriteria;
		}
		int count = 0;
		try {
			// System.out.println("RecordCount1 sorgu :" + query);
			org.hibernate.Query entityQuery = hibernateSession.createQuery(query);
			count = ((Long) entityQuery.uniqueResult()).intValue();
		} catch (Exception e) {
			System.out.println("ERROR @recordCount :" + e.getMessage());
			e.printStackTrace();
			try {
				throw new DBException(e, this.getClass().getName(), "Update", "Kayıt: " + entity.toString());
			} catch (DBException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		} finally {
			if (hibernateSession.isOpen()) {
				hibernateSession.close();
			}
		}
		return count;
	}

	public int recordCount(String entity, ArrayList<String> queryCriteriaDualList) {
		Session hibernateSession = null;
		StringBuffer queryString = new StringBuffer("SELECT COUNT(o) FROM " + entity + " o WHERE ");
		int y = 1;
		for (int x = 0; x < queryCriteriaDualList.size(); x++) {
			queryString.append(queryCriteriaDualList.get(x).toString() + " ?" + y + " ");
			y++;
			if (y > 1 && x < queryCriteriaDualList.size() - 2) {
				queryString.append(" AND ");
			}
			x++;
		}
		// System.out.println("Record count query :" + queryString.toString());
		int count = 0;
		try {
			hibernateSession = getSession();
			Query query = hibernateSession.createQuery(queryString.toString());
			for (int x = 0; x < queryCriteriaDualList.size(); x++) {
				// System.out.println("Record count param :" +
				// queryCriteriaDualList.get(x+1).toString());
				query.setParameter(x / 2 + 1, queryCriteriaDualList.get(x + 1));
				x++;
			}
			count = ((Long) query.uniqueResult()).intValue();
		} catch (Exception e) {
			e.printStackTrace();
			try {
				throw new DBException(e, this.getClass().getName(), "Load", "Kayıt: ");
			} catch (DBException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		} finally {
			if (hibernateSession.isOpen()) {
				hibernateSession.close();
			}
		}
		// System.out.println("Donen count :" + count);
		return count;
	}

	public Object recordCountByNativeQuery(String query) throws Exception {
		Session hibernateSession = getSession();
		// System.out.println("RecordCountByNativeQuery sorgu :" + query);
		int count = 0;
		try {
			org.hibernate.Query entityQuery = hibernateSession.createSQLQuery(query);
			return entityQuery.uniqueResult();
		} catch (Exception e) {
			e.printStackTrace();
			try {
				throw new DBException(e, this.getClass().getName(), "Select", "Sorgu: " + query);
			} catch (DBException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		} finally {
			if (hibernateSession.isOpen()) {
				hibernateSession.close();
			}
		}

		return count;
	}

	public List loadWithConditions(String modelName, String[] queryColumns, String[] queryValues, int maxResults, String staticWhereCondition) {
		Session hibernateSession = null;
		StringBuffer queryString = new StringBuffer("SELECT o FROM " + modelName + " o WHERE 1=1 ");
		try {
			hibernateSession = getSession();
			Integer inx = 1;
			if (staticWhereCondition != null && staticWhereCondition.trim().length() > 0) {
				queryString.append(" AND ").append(staticWhereCondition);
			}

			int values = 1;
			for (String column : queryColumns) {
				if (values == 1) {
					queryString.append(" AND LOWER(o." + column + ") LIKE ?");
				} else {
					queryString.append(" OR LOWER(o." + column + ") LIKE ?");
				}
				values++;
			}
			queryString.append(")");
			Query query = hibernateSession.createQuery(queryString.toString());
			// System.out.println("Query :" + queryString.toString());
			for (String value : queryValues) {
				query.setParameter(inx, "%" + value.toLowerCase() + "%");
				inx++;
			}
			query.setMaxResults(maxResults);
			return query.list();
		} catch (Exception e) {
			System.out.println("METHOD : loadWithConditions (1)");
			System.out.println("ERROR : " + e.getMessage());
			System.out.println("QUERY : " + queryString.toString());
			return null;
		} finally {
			if (hibernateSession.isOpen()) {
				hibernateSession.close();
			}
		}
	}

	private String createConditions(ArrayList<QueryObject> kriterler, String conditionType) {
		StringBuilder result = new StringBuilder();
		int x = 0;
		String condType = conditionType;
		for (QueryObject qObj : kriterler) {
			String columnName = qObj.getColumnName();
			if (!qObj.isCaseSensitive() && qObj.getSearchType() != ApplicationConstantBase._userDefined) {
				columnName = "lower(o." + columnName + ")";
			} else if (qObj.getSearchType() != ApplicationConstantBase._userDefined) {
				columnName = "o." + columnName;
			}
			if (x == 0) {
				condType = "";
			} else {
				condType = conditionType;
			}
			if (!isEmpty(qObj.getConstantQueryCriteria())) {
				result.append(" ").append(condType).append(" ").append(qObj.getConstantQueryCriteria()).append(" ");
			} else if (qObj.getSearchType() == ApplicationConstantBase._equals) {
				result.append(" ").append(condType).append(" ").append(columnName).append(" = ? ");
			} else if (qObj.getSearchType() == ApplicationConstantBase._userDefined) {
				result.append(" ").append(condType).append(" ").append(columnName).append(" ");
			} else if (qObj.getSearchType() == ApplicationConstantBase._beginsWith || qObj.getSearchType() == ApplicationConstantBase._inside) {
				if (qObj.isCaseSensitive()) {
					result.append(" ").append(condType).append(" ").append(columnName).append(" LIKE ? ");
				} else {
					result.append(" ").append(condType).append(" ").append(columnName).append(" LIKE ? ");
				}
			} else if (qObj.getSearchType() == ApplicationConstantBase._smallerThan) {
				result.append(" ").append(condType).append(" ").append(columnName).append(" < ? ");
			} else if (qObj.getSearchType() == ApplicationConstantBase._largerThan) {
				result.append(" ").append(condType).append(" ").append(columnName).append(" > ? ");
			} else if (qObj.getSearchType() == ApplicationConstantBase._equalsAndSmallerThan) {
				result.append(" ").append(condType).append(" ").append(columnName).append(" <= ? ");
			} else if (qObj.getSearchType() == ApplicationConstantBase._equalsAndLargerThan) {
				result.append(" ").append(condType).append(" ").append(columnName).append(" >= ? ");
			} else if (qObj.getSearchType() == ApplicationConstantBase._notEquals) {
				result.append(" ").append(condType).append(" ").append(columnName).append(" <> ? ");
			}
			x++;
		}
		// System.out.println("Condition :" + result.toString());
		return result.toString();
	}

	private Query setQueryParams(Query query, ArrayList<QueryObject> kriterler, ArrayList<QueryObject> opsiyonelKriterler) {
		Integer inx = 1;
		for (QueryObject qObj : kriterler) {
			// System.out.println("qObj.getSearchValue() :" +
			// qObj.getSearchValue());
			if (qObj.getSearchType() != ApplicationConstantBase._userDefined && isEmpty(qObj.getConstantQueryCriteria())) {
				if (qObj.getSearchType() == ApplicationConstantBase._equals || qObj.getSearchType() == ApplicationConstantBase._smallerThan
						|| qObj.getSearchType() == ApplicationConstantBase._largerThan || qObj.getSearchType() == ApplicationConstantBase._equalsAndSmallerThan
						|| qObj.getSearchType() == ApplicationConstantBase._equalsAndLargerThan || qObj.getSearchType() == ApplicationConstantBase._notEquals) {
					query.setParameter(inx, qObj.getSearchValue());
				} else {
					if (qObj.getSearchType() == ApplicationConstantBase._beginsWith) {
						query.setParameter(inx, qObj.getSearchValue() + "%");
					} else {
						query.setParameter(inx, "%" + qObj.getSearchValue() + "%");
					}
				}
				++inx;
			}
		}
		if (!isEmpty(opsiyonelKriterler)) {
			for (QueryObject qObj : opsiyonelKriterler) {
				// System.out.println("qObj.getSearchValue() optional :" +
				// qObj.getSearchValue());
				if (qObj.getSearchType() != ApplicationConstantBase._userDefined && isEmpty(qObj.getConstantQueryCriteria())) {
					if (qObj.getSearchType() == ApplicationConstantBase._equals || qObj.getSearchType() == ApplicationConstantBase._smallerThan
							|| qObj.getSearchType() == ApplicationConstantBase._largerThan || qObj.getSearchType() == ApplicationConstantBase._equalsAndSmallerThan
							|| qObj.getSearchType() == ApplicationConstantBase._equalsAndLargerThan || qObj.getSearchType() == ApplicationConstantBase._notEquals) {
						query.setParameter(inx, qObj.getSearchValue());
					} else {
						if (qObj.getSearchType() == ApplicationConstantBase._beginsWith) {
							query.setParameter(inx, qObj.getSearchValue() + "%");
						} else {
							query.setParameter(inx, "%" + qObj.getSearchValue() + "%");
						}
					}
					++inx;
				}
			}
		}
		return query;
	}

	public List loadFiltered(String tableName, ArrayList<QueryObject> kriterler, ArrayList<QueryObject> opsiyonelKriterler, int[] recordIntervals, String orderField) {
		Session hibernateSession = null;
		StringBuilder hql = new StringBuilder(" FROM " + tableName + " o WHERE ");
		try {
			hibernateSession = getSession();
			hql.append(createConditions(kriterler, "AND"));
			if (!isEmpty(opsiyonelKriterler)) {
				hql.append(" AND ( ");
				hql.append(createConditions(opsiyonelKriterler, "OR"));
				hql.append(")");
			}
			if (!isEmpty(orderField)) {
				hql.append(" ORDER BY " + orderField);
			}
			// System.out.println("loadFiltered (0) query :" + hql.toString());
			Query query = hibernateSession.createQuery(hql.toString());
			query = setQueryParams(query, kriterler, opsiyonelKriterler);
			if (recordIntervals != null && recordIntervals.length == 2) {
				query.setFirstResult(recordIntervals[0]);
				query.setMaxResults(recordIntervals[1]);
			}
			return query.list();
		} catch (Exception e) {
			System.out.println("METHOD : loadFiltered (0)");
			System.out.println("ERROR : " + e.getMessage());
			System.out.println("QUERY : " + hql.toString());
			return null;
		} finally {
			if (hibernateSession.isOpen()) {
				hibernateSession.close();
			}
		}
	}

	public List loadFiltered(String tableName, String queryCriteria, ArrayList<QueryObject> kriterler, ArrayList<QueryObject> opsiyonelKriterler, int[] recordIntervals,
			String orderField) {
		Session hibernateSession = null;
		StringBuilder hql = new StringBuilder(" FROM " + tableName + " o WHERE ");
		hql.append(queryCriteria).append(" AND ");
		try {
			hibernateSession = getSession();
			hql.append(createConditions(kriterler, "AND"));
			if (!isEmpty(opsiyonelKriterler)) {
				hql.append(" AND ( ");
				hql.append(createConditions(opsiyonelKriterler, "OR"));
				hql.append(")");
			}
			if (!isEmpty(orderField)) {
				hql.append(" ORDER BY " + orderField);
			}
			// System.out.println("loadFiltered (0) query :" + hql.toString());
			Query query = hibernateSession.createQuery(hql.toString());
			query = setQueryParams(query, kriterler, opsiyonelKriterler);
			if (recordIntervals != null && recordIntervals.length == 2) {
				query.setFirstResult(recordIntervals[0]);
				query.setMaxResults(recordIntervals[1]);
			}
			return query.list();
		} catch (Exception e) {
			System.out.println("METHOD : loadFiltered (0)");
			System.out.println("ERROR : " + e.getMessage());
			System.out.println("QUERY : " + hql.toString());
			return null;
		} finally {
			if (hibernateSession.isOpen()) {
				hibernateSession.close();
			}
		}
	}

	public Long recordCountFiltered(String tableName, ArrayList<QueryObject> kriterler, ArrayList<QueryObject> opsiyonelKriterler) {
		Session hibernateSession = null;
		StringBuilder hql = new StringBuilder("SELECT COUNT(*) FROM " + tableName + " o WHERE ");
		try {
			hibernateSession = getSession();
			hql.append(createConditions(kriterler, "AND"));
			if (!isEmpty(opsiyonelKriterler)) {
				hql.append(" AND ( ");
				hql.append(createConditions(opsiyonelKriterler, "OR"));
				hql.append(")");
			}
			// System.out.println("Hql @recordCountFiltered :" +
			// hql.toString());
			Query query = hibernateSession.createQuery(hql.toString());
			query = setQueryParams(query, kriterler, opsiyonelKriterler);
			// System.out.println("Record count :" + query.uniqueResult());
			return (Long) query.uniqueResult();
		} catch (Exception e) {
			System.out.println("METHOD : recordCountFiltered (0)");
			System.out.println("ERROR : " + e.getMessage());
			System.out.println("QUERY : " + hql.toString());
			return null;
		} finally {
			if (hibernateSession.isOpen()) {
				hibernateSession.close();
			}
		}
	}

	public Long recordCountAll(String tableName) {
		Session hibernateSession = null;
		StringBuilder hql = new StringBuilder("SELECT COUNT(rID) FROM " + tableName);
		try {
			hibernateSession = getSession();
			Query query = hibernateSession.createQuery(hql.toString());
			return (Long) query.uniqueResult();
		} catch (Exception e) {
			System.out.println("METHOD : recordCountAll (0)");
			System.out.println("ERROR : " + e.getMessage());
			System.out.println("QUERY : " + hql.toString());
			return null;
		} finally {
			if (hibernateSession.isOpen()) {
				hibernateSession.close();
			}
		}
	}

	public List loadAll(String tableName, int[] recordIntervals, String orderField) {
		Session hibernateSession = null;
		StringBuilder hql = new StringBuilder(" FROM " + tableName);
		if (!isEmpty(orderField)) {
			hql.append(" ORDER BY " + orderField);
		}
		try {
			hibernateSession = getSession();
			Query query = hibernateSession.createQuery(hql.toString());
			if (recordIntervals != null && recordIntervals.length == 2) {
				query.setFirstResult(recordIntervals[0]);
				query.setMaxResults(recordIntervals[1]);
			}
			return query.list();
		} catch (Exception e) {
			System.out.println("METHOD : loadAll (0)");
			System.out.println("ERROR : " + e.getMessage());
			System.out.println("QUERY : " + hql.toString());
			return null;
		} finally {
			if (hibernateSession.isOpen()) {
				hibernateSession.close();
			}
		}
	}

	public int executeQuery(String query) throws DBException {
		Session hibernateSession = getSession();
		int result = 0;
		try {
			// System.out.println("Execute this query : " + query);
			hibernateSession.getTransaction().begin();
			org.hibernate.Query entityQuery = hibernateSession.createQuery(query);
			result = entityQuery.executeUpdate();
			hibernateSession.getTransaction().commit();
		} catch (Exception e) {
			if (hibernateSession.getTransaction().isActive()) {
				hibernateSession.getTransaction().rollback();
			}
			System.out.println("METHOD : executeQuery (0)");
			System.out.println("ERROR : " + e.getMessage());
			System.out.println("QUERY : " + query);
			return 0;
		} finally {
			if (hibernateSession.isOpen()) {
				hibernateSession.close();
			}
		}
		return result;
	}

	public Object sum(String columnToBeSummed, String tableName, String whereCondition) throws DBException {
		Session hibernateSession = getSession();
		String queryText = "SELECT SUM(o." + columnToBeSummed + ") FROM " + tableName + " o";
		Object result = null;
		try {
			hibernateSession.getTransaction().begin();

			if (!isEmpty(whereCondition)) {
				queryText += " WHERE " + whereCondition;
			}
			Query query = hibernateSession.createQuery(queryText);
			result = query.list().get(0);
			return result;
		} catch (Exception e) {
			System.out.println("METHOD : sum (0)");
			System.out.println("ERROR : " + e.getMessage());
			System.out.println("QUERY : " + queryText);
			return 0;
		} finally {
			if (hibernateSession.isOpen()) {
				hibernateSession.close();
			}
		}
	}

	public List loadByQuery(String query) {
		Session hibernateSession = null;
		try {
			hibernateSession = getSession();
			org.hibernate.Query entityQuery = hibernateSession.createQuery(query);
			List dataList = entityQuery.list();
			hibernateSession.close();
			return dataList;
		} catch (Exception e) {
			System.out.println("ERROR : " + e.getMessage());
			System.out.println("QUERY : " + query);
			return null;
		} finally {
			if (hibernateSession.isOpen()) {
				hibernateSession.close();
			}
		}
	}

	private static boolean isEmpty(Collection col) {
		return col == null || col.size() == 0;
	}

	private static boolean isEmpty(Object[] col) {
		return col == null || col.length == 0;
	}

	private boolean isEmpty(String str) {
		return str == null || str.trim().length() == 0;
	}

}
