package tr.com.arf.framework.db._management;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import tr.com.arf.framework.utility.application.ApplicationVariables;

public final class PersistenceManager {
	public static final boolean DEBUG = true;

	private static final PersistenceManager singleton = new PersistenceManager();

	protected EntityManagerFactory entityManagerFactory;

	public static PersistenceManager getInstance() {
		return singleton;
	}

	private PersistenceManager() {
	}

	public EntityManagerFactory getEntityManagerFactory() {
		if (entityManagerFactory == null) {
			createEntityManagerFactory();
		}
		return entityManagerFactory;
	}

	public void closeEntityManagerFactory() {
		if (entityManagerFactory != null) {
			entityManagerFactory.close();
			entityManagerFactory = null;
			if (DEBUG) {
				System.out.println("DEBUG : Persistence finished at " + new java.util.Date());
			}
		}
	}

	protected void createEntityManagerFactory() {
		ApplicationVariables ap = new ApplicationVariables();
		this.entityManagerFactory = Persistence.createEntityManagerFactory(ap.getEntityManagerFactoryName());
		if (DEBUG) {
			System.out.println("DEBUG : Persistence started at " + new java.util.Date());
		}
	}

}
