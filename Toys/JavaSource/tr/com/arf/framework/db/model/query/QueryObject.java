package tr.com.arf.framework.db.model.query;

import org.springframework.util.StringUtils;

public class QueryObject {

	private String columnName;
	private int searchType;
	private Object searchValue;
	private boolean caseSensitive;
	private String constantQueryCriteria;

	public QueryObject() {
	}

	public QueryObject(String columnName, int searchType, Object searchValue) {
		this.columnName = columnName;
		this.searchType = searchType;
		if (searchValue instanceof String) {
			this.searchValue = StringUtils.replace((String) searchValue, "*", "%");
			this.caseSensitive = false;
		} else {
			this.searchValue = searchValue;
			this.caseSensitive = true;
		}
	}

	public QueryObject(String columnName, int searchType, Object searchValue, boolean caseSensitive) {
		this.columnName = columnName;
		this.searchType = searchType;
		if (!caseSensitive) {
			if (searchValue instanceof String) {
				this.searchValue = StringUtils.replace((String) searchValue, "*", "%");
			}
		} else {
			this.searchValue = StringUtils.replace((String) searchValue, "*", "%");
		}
		this.caseSensitive = caseSensitive;
	}

	public QueryObject(String constantQueryCriteria) {
		this.constantQueryCriteria = constantQueryCriteria;
	}

	@Override
	public String toString() {
		StringBuffer result = new StringBuffer("Query Object : ");
		if (getConstantQueryCriteria() == null) {
			result.append(" 1.ColumnName: " + getColumnName() + ", 2.SearchType: " + getSearchType() + ", 3.SearchValue:" + getSearchValue() + ", 4.CaseSensitive:"
					+ isCaseSensitive());
		} else {
			result.append(" 1.Constant Query Criteria : " + getConstantQueryCriteria());
		}
		return result.toString();
	}

	public String getColumnName() {
		return columnName;
	}

	public void setColumnName(String columnName) {
		this.columnName = columnName;
	}

	public int getSearchType() {
		return searchType;
	}

	public void setSearchType(int searchType) {
		this.searchType = searchType;
	}

	public Object getSearchValue() {
		return searchValue;
	}

	public void setSearchValue(Object searchValue) {
		this.searchValue = searchValue;
	}

	public boolean isCaseSensitive() {
		return caseSensitive;
	}

	public void setCaseSensitive(boolean caseSensitive) {
		this.caseSensitive = caseSensitive;
	}

	public String getConstantQueryCriteria() {
		return constantQueryCriteria;
	}

	public void setConstantQueryCriteria(String constantQueryCriteria) {
		this.constantQueryCriteria = constantQueryCriteria;
	}
}
