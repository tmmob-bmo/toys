package tr.com.arf.framework.db.model._base;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;

import javax.persistence.Transient;

import org.springframework.util.StringUtils;

public abstract class BaseTreeEntity extends BaseEntity implements Serializable {

	private static final long serialVersionUID = 3375845546155372418L;

	/* Abstract Methods */
	public abstract BaseTreeEntity getUstRef();

	public abstract void setUstRef(BaseTreeEntity ustRef);

	public abstract String getAd();

	public abstract void setAd(String ad);

	public abstract String getErisimKodu();

	public abstract void setErisimKodu(String erisimKodu);

	@Transient
	private String icon;

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public String getUIStringTreeFormat() {
		if (getUstRef() == null) {
			return "<b>" + getAd() + "</b>";
		} else {
			StringBuffer result = new StringBuffer("");
			ArrayList<BaseTreeEntity> hieararchialEntityList = new ArrayList<BaseTreeEntity>();
			BaseTreeEntity cus = getUstRef();
			hieararchialEntityList.add(cus);
			while (true) {
				if (cus.getUstRef() == null) {
					break;
				} else {
					try {
						hieararchialEntityList.add(cus.getUstRef());
						cus = cus.getUstRef();
					} catch (Exception e) {
						break;
					}
				}
			}
			Collections.reverse(hieararchialEntityList);
			String s = "&nbsp;&nbsp;";
			for (BaseTreeEntity object : hieararchialEntityList) {
				result.append("<font color=\"lightgray\"><i>" + s + " " + object.getAd() + "</i><br /></font>");
				if (s.equalsIgnoreCase("&nbsp;&nbsp;")) {
					s = "&nbsp;&nbsp;└";
				}
				s = "&nbsp;&nbsp;" + s;
			}
			result.append("<b>" + s + " " + getAd() + "</b>");
			return result.toString();
		}
	}

	public String getEmptyBlockSize() {
		return (StringUtils.countOccurrencesOf(getErisimKodu(), ".") + 1) * 10 + "px";
	}

}
