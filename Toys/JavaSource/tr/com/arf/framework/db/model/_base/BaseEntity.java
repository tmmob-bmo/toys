package tr.com.arf.framework.db.model._base;

import java.io.Serializable;

import com.thoughtworks.xstream.XStream;

public abstract class BaseEntity implements Serializable {

	private static final long serialVersionUID = 3375845546155372418L;

	/* Abstract Methods */
	/* Entity Primary Key RID Getter */
	public abstract Long getRID();

	/* Entity Primary Key RID Setter */
	public abstract void setRID(Long rID);

	/* Entity User Interface Presentation */
	public abstract String getUIString();

	@Override
	public String toString(){
		if (getRID() != null) {
			return getUIString();
		} else {
			return "";
		}
	}

	/* New Entity Default Values or Initialization */
	public abstract void initValues(boolean defaultValues);

	// Path attribute for generic file upload and download components
	public String getPath() {
		return "";
	}

	public void setPath(String path) {
		return;
	}

	/* Methods */

	/* Entity String Fields Letter Case Setter */
	protected final String letterCase(String value) {
		return value;
	}

	/* Clone Entity Easily */
	private static final XStream XSTREAM = new XStream();

	public Object cloneObject() {
		Object target = XSTREAM.fromXML(XSTREAM.toXML(this));
		return target;
	}

	/* Equality Checking for JSF Framework */
	@Override
	public final boolean equals(Object object) {
		if (object == null) {
			return false;
		}
		if (object.getClass() != this.getClass()) {
			return false;
		}

		BaseEntity other = (BaseEntity) object;
		if (this.getRID().longValue() != other.getRID().longValue()) {
			return false;
		}

		return true;
	}

	public String getValue() {
		return null;
	}
}
