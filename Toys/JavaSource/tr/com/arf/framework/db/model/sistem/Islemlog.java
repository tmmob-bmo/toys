package tr.com.arf.framework.db.model.sistem;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import tr.com.arf.framework.db.enumerated._common.IslemTuru;
import tr.com.arf.framework.db.model._base.BaseEntity;
import tr.com.arf.framework.utility.tool.DateUtil;
import tr.com.arf.framework.utility.tool.KeyUtil;

@Entity
@Table(name = "ISLEMLOG")
public class Islemlog extends BaseEntity {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(updatable = false, unique = true, nullable = false, name = "RID")
	private Long rID;

	@Enumerated(EnumType.ORDINAL)
	@Column(name = "ISLEMTURU")
	private IslemTuru islemturu;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "ISLEMTARIHI")
	private java.util.Date islemtarihi;

	@Column(name = "TABLOADI")
	private String tabloadi;

	@Column(name = "REFERANSRID")
	private Long referansRID;

	@Column(name = "ESKIDEGER")
	private String eskideger;

	@Column(name = "YENIDEGER")
	private String yenideger;

	@Column(name = "KULLANICIRID")
	private Long kullaniciRid;

	@Column(name = "KULLANICITEXT")
	private String kullaniciText;

	public Islemlog() {
		super();
	}

	@Override
	public Long getRID() {
		return this.rID;
	}

	@Override
	public void setRID(Long rID) {
		this.rID = rID;
	}

	public IslemTuru getIslemturu() {
		return this.islemturu;
	}

	public void setIslemturu(IslemTuru islemturu) {
		this.islemturu = islemturu;
	}

	public java.util.Date getIslemtarihi() {
		return islemtarihi;
	}

	public void setIslemtarihi(java.util.Date islemtarihi) {
		this.islemtarihi = islemtarihi;
	}

	public String getTabloadi() {
		return this.tabloadi;
	}

	public void setTabloadi(String tabloadi) {
		this.tabloadi = tabloadi;
	}

	public Long getReferansRID() {
		return this.referansRID;
	}

	public void setReferansRID(Long referansRID) {
		this.referansRID = referansRID;
	}

	public String getEskideger() {
		return this.eskideger;
	}

	public void setEskideger(String eskideger) {
		this.eskideger = eskideger;
	}

	public String getYenideger() {
		return this.yenideger;
	}

	public void setYenideger(String yenideger) {
		this.yenideger = yenideger;
	}

	public Long getKullaniciRid() {
		return kullaniciRid;
	}

	public void setKullaniciRid(Long kullaniciRid) {
		this.kullaniciRid = kullaniciRid;
	}

	public String getKullaniciText() {
		return kullaniciText;
	}

	public void setKullaniciText(String kullaniciText) {
		this.kullaniciText = kullaniciText;
	}

	@Override
	public String toString() {
		return "tr.com.arf.dokss.db.model.system.Islemlog[id=" + this.getRID() + "]";
	}

	@Override
	public String getUIString() {
		return getTabloadi() + "";
	}

	@Override
	public void initValues(boolean defaultValues) {
		if (defaultValues) {
			// TODO Initialize with default values
		} else {
			this.rID = null;
			this.islemturu = IslemTuru._NULL;
			this.islemtarihi = null;
			this.tabloadi = "";
			this.referansRID = null;
			this.eskideger = "";
			this.yenideger = "";
			this.kullaniciRid = null;
			this.kullaniciText = null;
		}
	}

	@Override
	public String getValue() {
		StringBuilder value = new StringBuilder("");
		if (getIslemturu() != null) {
			value.append(KeyUtil.getFrameworkLabel("islemlog.islemturu") + " : " + getIslemturu() + "<br />");
		}
		if (getIslemtarihi() != null) {
			value.append(KeyUtil.getFrameworkLabel("islemlog.islemtarihi") + " : " + DateUtil.dateToDMYHMS(getIslemtarihi()) + "<br />");
		}
		if (getTabloadi() != null) {
			value.append(KeyUtil.getFrameworkLabel("islemlog.tabloadi") + " : " + getTabloadi() + "<br />");
		}
		if (getReferansRID() != 0) {
			value.append(KeyUtil.getFrameworkLabel("islemlog.referansRID") + " : " + getReferansRID() + "<br />");
		}
		if (getEskideger() != null) {
			value.append(KeyUtil.getFrameworkLabel("islemlog.eskideger") + " : " + getEskideger() + "<br />");
		}
		if (getYenideger() != null) {
			value.append(KeyUtil.getFrameworkLabel("islemlog.yenideger") + " : " + getYenideger() + "");
		}
		if (getKullaniciRid() != null) {
			value.append(" Kull. Rid : " + getKullaniciRid() + "<br />");
		}
		if (getKullaniciText() != null) {
			value.append(" Kullanıcı : " + getKullaniciText() + "<br />");
		}
		return value.toString();
	}

}
