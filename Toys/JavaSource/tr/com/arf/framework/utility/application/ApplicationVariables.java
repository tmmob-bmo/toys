package tr.com.arf.framework.utility.application;

public class ApplicationVariables {

	private String entityManagerFactoryName;

	public ApplicationVariables() {

	}

	public ApplicationVariables(String efName) {
		this.entityManagerFactoryName = efName;
	}

	public String getEntityManagerFactoryName() {
		return entityManagerFactoryName;
	}

	public void setEntityManagerFactoryName(String entityManagerFactoryName) {
		this.entityManagerFactoryName = entityManagerFactoryName;
	}

}
