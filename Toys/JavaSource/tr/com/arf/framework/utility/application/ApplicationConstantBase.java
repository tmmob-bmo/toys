package tr.com.arf.framework.utility.application;

import java.io.Serializable;

public abstract class ApplicationConstantBase implements Serializable {

	private static final long serialVersionUID = -2416457484221240236L;

	/* WEB SERVIS */
	public static final int _MAX_RECORD_COUNT = 200;
	public static final int _MAX_NOFILTER_RECORD_COUNT = 200;
	public static final int _LIST_SIZE = 10;

	public static final String _EDIT_SUFFIX = "_Edit";

	public static final String _insideTextSearch = "inside";
	public static final String _beginsWithTextSearch = "beginsWith";
	public static final String _equalsTextSearch = "equals";

	public static final String _successMessage = "success";

	public static final int _userDefined = 0;
	public static final int _equals = 1;
	public static final int _beginsWith = 2;
	public static final int _inside = 3;
	public static final int _smallerThan = 4;
	public static final int _largerThan = 5;
	public static final int _equalsAndLargerThan = 6;
	public static final int _equalsAndSmallerThan = 7;
	public static final int _notEquals = 8;

	public static final String _detailItemNamePrefix = "detailItemFor";
	public static final String _compIdParamName = "javax.faces.source";

	public static final String _linuxNoImagePath = "/Nobody.jpg";
	public static final String _windowsNoImagePath = "/Nobody.jpg";
}
