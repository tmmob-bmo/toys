package tr.com.arf.framework.utility.application;

public class ApplicationDescriptorBase {

	/* Common */
	public static final String _LOGIN_OUTCOME = "login.do";
	public static final String _USERROLE_OUTCOME = "userRole.do";
	public static final String _REGISTER_OUTCOME = "register.do";
	public static final String _REGISTER_SUMMARY_OUTCOME = "register_Summary.do";
	public static final String _DASHBOARD_OUTCOME = "dashboard.do";
	public static final String _ACCESSDENIED_OUTCOME = "accessDenied.do";
	public static final String _NOFUNCTIONALITY_OUTCOME = "noFunctionality.do";
	public static final String _EXCEPTION_OUTCOME = "exception.do";
	public static final String _REPORT_OUTCOME = "report.do";
	public static final String _PASSWORDCHANGE_OUTCOME = "passwordChange.do";
	public static final String _PASSWORDREQUEST_OUTCOME = "passwordRequest.do";
	public static final String _PAROLE_EXPIRED_OUTCOME = "parole_Expired.do";
	public static final String _ACCOUNT_LOCKED_OUTCOME = "account_Locked.do";

}