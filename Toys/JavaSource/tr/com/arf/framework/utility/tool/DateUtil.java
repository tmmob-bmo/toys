package tr.com.arf.framework.utility.tool;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class DateUtil {

	private static String dayPattern = "dd/MM/yyyy - EEEE";
	private static String datePattern = "dd/MM/yyyy";
	private static String datePatternWithShortYear = "dd/MM/yy";
	private static String datePatternWithDots = "dd.MM.yyyy";
	private static String dateTimePattern = "dd/MM/yyyy HH:mm";
	private static String dateTimeSecondPattern = "dd/MM/yyyy HH:mm:ss";

	private static String datePatternForQuery = "yyyy-MM-dd";
	private static String hourMinutePattern = "HH:mm";

	/*
	 * private static SimpleDateFormat simpleDayFormat = new SimpleDateFormat(
	 * getDayPattern());
	 */
	private static SimpleDateFormat simpleDateFormat = new SimpleDateFormat(getDatePattern());
	private static SimpleDateFormat simpleDateWithDotsFormat = new SimpleDateFormat(getDatePatternWithDots());
	private static SimpleDateFormat simpleDateTimeFormat = new SimpleDateFormat(getDateTimePattern());
	private static SimpleDateFormat simpleDateTimeSecondFormat = new SimpleDateFormat(getDateTimeSecondPattern());
	private static SimpleDateFormat simpleYMDDateFormat = new SimpleDateFormat(getDatePatternForQuery());
	private static SimpleDateFormat simpleHourMinuteFormat = new SimpleDateFormat(getHourMinutePattern());
	private static SimpleDateFormat simpleDateFormatShortYear = new SimpleDateFormat(getDatePatternWithShortYear());

	public static String dateToYMD(Date date) {
		return simpleYMDDateFormat.format(date);
	}

	public static String dateToDMY(Date date) {
		if(date == null) {
			return "";
		}
		return simpleDateFormat.format(date);
	}

	public static String dateToDMYWithDay(Date date, String seperator) {
		return simpleDateFormat.format(date) + seperator + new SimpleDateFormat("EEEE").format(date);
	}

	public static String dateToHM(Date date) {
		return simpleHourMinuteFormat.format(date);
	}

	public static String dateToDMYNullCheck(Date date) {
		if (date != null) {
			return simpleDateFormat.format(date);
		} else {
			return "";
		}
	}

	public static String dateToDMYHM(Date date) {
		return simpleDateTimeFormat.format(date);
	}

	public static String dateToDMYHMS(Date date) {
		return simpleDateTimeSecondFormat.format(date);
	}

	public static String getDayPattern() {
		return dayPattern;
	}

	public static String getDatePattern() {
		return datePattern;
	}

	public static String getDatePatternWithDots() {
		return datePatternWithDots;
	}

	public static String getDateTimePattern() {
		return dateTimePattern;
	}

	public static String getDateTimeSecondPattern() {
		return dateTimeSecondPattern;
	}

	public static String getHourMinutePattern() {
		return hourMinutePattern;
	}

	public static void setHourMinutePattern(String hourMinutePattern) {
		DateUtil.hourMinutePattern = hourMinutePattern;
	}

	public static int getDay(Date date) {
		String dmy = simpleDateFormat.format(date);
		return Integer.parseInt(dmy.substring(0, 2));
	}

	public static int getMonth(Date date) {
		String dmy = simpleDateFormat.format(date);
		return Integer.parseInt(dmy.substring(3, 5));
	}

	public static int getYear(Date date) {
		String dmy = simpleDateFormat.format(date);
		return Integer.parseInt(dmy.substring(6, 10));
	}

	public static String getDatePatternForQuery() {
		return datePatternForQuery;
	}

	public static void setDatePatternForQuery(String datePatternForQuery) {
		DateUtil.datePatternForQuery = datePatternForQuery;
	}

	public static String getDatePatternWithShortYear() {
		return datePatternWithShortYear;
	}

	public static void setDatePatternWithShortYear(String datePatternWithShortYear) {
		DateUtil.datePatternWithShortYear = datePatternWithShortYear;
	}

	public static int getMonthOfADate(Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		return cal.get(Calendar.MONTH) + 1;
	}

	public static int getYearOfADate(Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		return cal.get(Calendar.YEAR);
	}

	public static Date getFirstDayOfAMonth(Date date) {
		Calendar c = new GregorianCalendar();
		c.set(Calendar.DAY_OF_MONTH, 1);
		return c.getTime();
	}

	public static long getFirstSecondOfADay(Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.set(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DATE), 0, 0, 0);
		return cal.getTimeInMillis();
	}

	public static long getLastSecondOfADay(Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.set(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DATE), 23, 59, 59);
		return cal.getTimeInMillis();
	}

	public static Date createDateObject(String dateStr) throws ParseException {
		if (dateStr != null && dateStr.trim().length() >= 8) {
			if (dateStr.contains("/")) {
				return simpleDateFormat.parse(dateStr);
			} else {
				return simpleDateWithDotsFormat.parse(dateStr);
			}
		} else {
			return null;
		}
	}

	public static int calculateDateDifference(Date date1, Date date2) {
		Calendar c1 = Calendar.getInstance();
		Calendar c2 = Calendar.getInstance();
		c1.setTime(date1);
		c2.setTime(date2);
		int diffDay = 0;
		if (c1.before(c2)) {
			diffDay = countDiffDay(c1, c2);
		} else {
			diffDay = countDiffDay(c2, c1);
		}

		return diffDay;
	}

	public static int countDiffDay(Calendar c1, Calendar c2) {
		int returnInt = 0;
		while (!c1.after(c2)) {
			c1.add(Calendar.DAY_OF_MONTH, 1);
			returnInt++;
		}

		if (returnInt > 0) {
			returnInt = returnInt - 1;
		}

		return returnInt;
	}

	public static Date addDaysToDate(Date tarih, int daysToBeAdded) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(tarih);
		cal.add(Calendar.DAY_OF_MONTH, daysToBeAdded);
		return cal.getTime();
	}

	public static Date addHoursToDate(Date tarih, int hoursToBeAdded) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(tarih);
		cal.add(Calendar.HOUR_OF_DAY, hoursToBeAdded);
		return cal.getTime();
	}

	public static Date copyTimeFromDateToDate(Date fromDate, Date toDate) {
		Calendar cal1 = Calendar.getInstance();
		Calendar cal2 = Calendar.getInstance();
		cal1.setTime(fromDate);
		cal2.setTime(toDate);
		cal2.set(Calendar.HOUR_OF_DAY, cal1.get(Calendar.HOUR_OF_DAY));
		cal2.set(Calendar.MINUTE, cal1.get(Calendar.MINUTE));
		return cal2.getTime();
	}

	public static Date copyDayMonthYearFromDateToDate(Date fromDate, Date toDate) {
		Calendar cal1 = Calendar.getInstance();
		Calendar cal2 = Calendar.getInstance();
		cal1.setTime(fromDate);
		cal2.setTime(toDate);
		cal2.set(Calendar.YEAR, cal1.get(Calendar.YEAR));
		cal2.set(Calendar.MONTH, cal1.get(Calendar.MONTH));
		cal2.set(Calendar.DAY_OF_MONTH, cal1.get(Calendar.DAY_OF_MONTH));
		return cal2.getTime();
	}

	public static ArrayList<Date> getCertainDaysBetweenTwoDates(Date fromDate, Date toDate, int gun, int periyod) {
		ArrayList<Date> tarihler = new ArrayList<Date>();
		Calendar startCal = Calendar.getInstance();
		startCal.setTime(fromDate);
		startCal.setFirstDayOfWeek(Calendar.MONDAY);
		startCal.set(Calendar.DAY_OF_WEEK, gun);
		if (startCal.after(fromDate)) {
			tarihler.add(startCal.getTime());
		}
		startCal.add(Calendar.DAY_OF_YEAR, periyod);
		Calendar endCal = Calendar.getInstance();
		endCal.setTime(toDate);
		endCal.setFirstDayOfWeek(Calendar.MONDAY);
		while (startCal.getTimeInMillis() <= endCal.getTimeInMillis()) {
			tarihler.add(startCal.getTime());
			startCal.add(Calendar.DAY_OF_YEAR, periyod);
		}
		return tarihler;

	}

	public static String dateToDMYWithShortYear(Date date) {
		if(date == null) {
			return "";
		}
		return simpleDateFormatShortYear.format(date);
	}
}
