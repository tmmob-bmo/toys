package tr.com.arf.framework.utility.tool;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import tr.com.arf.framework.view.controller._common.SessionUserBase;

public class DynamicImageServlet extends HttpServlet {
    private static final Logger LOGGER = Logger.getLogger(DynamicImageServlet.class);

	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String file = request.getParameter("file");
		String path = request.getParameter("path");
		String fullPath = "";
		SessionUserBase sessionUserBase = (SessionUserBase) request.getSession().getAttribute("xcdbxuamsh372nnrensdw24ew.ed3454dd.ssdxc.23dsqqm45");
		String filePath = sessionUserBase.getSystemPath();
		// System.out.println(" **************** Get this FILE : " + (filePath + path).toLowerCase() + "/" + file);
		try {
			// Get image file.
			if (file != null && file.trim().length() > 0 && path != null && path.trim().length() > 0) {
				// SessionUserBase sessionUser = (SessionUserBase)
				// request.getSession().getAttribute("sessionUser");
				if (request.getSession().getAttribute("xcdbxuamsh372nnrensdw24ew.ed3454dd.ssdxc.23dsqqm45") != null
						&& request.getSession().getAttribute("xcdbxuamsh372nnrensdw24ew.ed3454dd.ssdxc.23dsqqm45").getClass().getSimpleName().equalsIgnoreCase("SessionUser")) {
					fullPath = filePath + "/" + file;
					if (!path.trim().equals("") && !path.trim().equals("/")) {
						fullPath = filePath + path + "/" + file;
					}
					BufferedInputStream in = new BufferedInputStream(new FileInputStream(fullPath));
					// System.out.println(" **************** Get this FILE : " +
					// fullPath);
					// Get image contents.
					byte[] bytes = new byte[in.available()];
					in.read(bytes);
					in.close();
					// Write image contents to response.
					response.getOutputStream().write(bytes);
				}
			}
		} catch (Exception e) {
			try {
				if (filePath.endsWith("/")) {
					fullPath = filePath + "nobody.jpg";
				} else {
					fullPath = filePath + "/" + "nobody.jpg";
				}
				BufferedInputStream in = new BufferedInputStream(new FileInputStream(fullPath));
				// System.out.println(" **************** FILE NOT FOUND! Get this NOBODY IMAGE : "
				// + (filePath + path).toLowerCase() + "/" + file);
				// Get image contents.
				byte[] bytes = new byte[in.available()];
				in.read(bytes);
				in.close();
				// Write image contents to response.
				response.getOutputStream().write(bytes);
			} catch (Exception e2) {
                LOGGER.error(e2.toString(), e2);
			}
		}
	}
}
