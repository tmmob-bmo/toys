package tr.com.arf.framework.utility.tool;

import java.text.MessageFormat;
import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

import javax.faces.context.FacesContext;

public class KeyUtil {

	private static ResourceBundle bundle;

	public static ResourceBundle getBundle() {
		if (bundle == null) {
			FacesContext context = FacesContext.getCurrentInstance();
			bundle = context.getApplication().getResourceBundle(context, "label");
		}
		return bundle;
	}

	public static String getLabelValue(String key) {
		String result = null;
		try {
			result = getBundle().getString(key);
		} catch (MissingResourceException e) {
			result = "???" + key + "??? not found";
		}
		return result;
	}

	public static String getLabelValueWithParams(String key, Object[] paramValue) {
		String result = null;
		try {
			result = getBundle().getString(key);
			return MessageFormat.format(getBundle().getString(key), paramValue);
		} catch (MissingResourceException e) {
			result = key + "??";
		}
		return result;
	}

	public static String getFrameworkLabel(String key) {
		if(key == null || key.trim().length() == 0) {
			return "";
		}
		String text = "";
		try {
			FacesContext context = FacesContext.getCurrentInstance();
			Locale locale = null;
			if (context.getViewRoot().getLocale() != null) {
				if (!context.getViewRoot().getLocale().toString().trim().equals("")) {
					locale = context.getViewRoot().getLocale();
				} else {
					locale = new Locale("tr", "TR");
				}
			} else {
				locale = new Locale("tr", "TR");
			}
			String localeText = locale.toString();
			if (locale.toString().equalsIgnoreCase("en_GB") || locale.toString().equalsIgnoreCase("en_US")) {
				localeText = "en";
			}
			if (localeText.trim().equalsIgnoreCase("") || locale.toString().equalsIgnoreCase("tr_TR")) {
				localeText = "tr";
			}
			ResourceBundle r = ResourceBundle.getBundle("tr.com.arf.framework.view.resource.label_" + localeText.toLowerCase());
			text = r.getString(key);
		} catch (Exception e) {
			System.out.println("Key :" + key);
			System.out.println("Error reading label :" + e.getMessage());
			text = key;
		}
		return text;
	}

	public static String getMessageValue(String key) {
		if(key == null || key.trim().length() == 0) {
			return "";
		}
		String text = "";
		try {
			FacesContext context = FacesContext.getCurrentInstance();
			Locale locale = null;
			if (context.getViewRoot().getLocale() != null) {
				if (!context.getViewRoot().getLocale().toString().trim().equals("")) {
					locale = context.getViewRoot().getLocale();
				} else {
					locale = new Locale("tr", "TR");
				}
			} else {
				locale = new Locale("tr", "TR");
			}
			// System.out.println("Locale :" + locale.toString());
			String localeText = locale.toString();
			if (locale.toString().equalsIgnoreCase("en_GB") || locale.toString().equalsIgnoreCase("en_US")) {
				localeText = "en";
			}
			if (localeText.trim().equalsIgnoreCase("") || locale.toString().equalsIgnoreCase("tr_TR")) {
				localeText = "tr";
			}
			ResourceBundle r = ResourceBundle.getBundle(context.getApplication().getMessageBundle() + "_" + localeText.toLowerCase());
			// System.out.println("Bundle name : " + r.toString());
			text = r.getString(key);
			// System.out.println("okunan text :"+text);
		} catch (Exception e) {
			System.out.println("Key :" + key);
			System.out.println("Error reading label :" + e.getMessage());
			text = key;
		}
		return text;
	}

	public static String getMessageValueWithParams(String key, Object[] paramValue) {
		if(key == null || key.trim().length() == 0) {
			return "";
		}
		try {
			FacesContext context = FacesContext.getCurrentInstance();
			Locale locale = null;
			if (context.getViewRoot().getLocale() != null) {
				if (!context.getViewRoot().getLocale().toString().trim().equals("")) {
					locale = context.getViewRoot().getLocale();
				} else {
					locale = new Locale("tr", "TR");
				}
			} else {
				locale = new Locale("tr", "TR");
			}
			// System.out.println("Locale :" + locale.toString());
			String localeText = locale.toString();
			if (locale.toString().equalsIgnoreCase("en_GB") || locale.toString().equalsIgnoreCase("en_US")) {
				localeText = "en";
			}
			if (localeText.trim().equalsIgnoreCase("") || locale.toString().equalsIgnoreCase("tr_TR")) {
				localeText = "tr";
			}
			ResourceBundle r = ResourceBundle.getBundle(context.getApplication().getMessageBundle() + "_" + localeText.toLowerCase());
			return MessageFormat.format(r.getString(key), paramValue);
			// System.out.println("okunan text :"+text);
		} catch (Exception e) {
			System.out.println("Key :" + key);
			System.out.println("Error reading label :" + e.getMessage());
			return key;
		}
	}

}
