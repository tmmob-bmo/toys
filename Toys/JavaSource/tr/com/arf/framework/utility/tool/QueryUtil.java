package tr.com.arf.framework.utility.tool;

import java.math.BigDecimal;

import tr.com.arf.framework.utility.application.ApplicationConstantBase;

public class QueryUtil {

	public static boolean isNumber(String str) {
		try {
			new Long(str);
			return true;

		} catch (Exception e) {
			return false;
		}
	}

	public static boolean isDouble(String str) {
		try {
			new Double(str);
			return true;

		} catch (Exception e) {
			return false;
		}
	}

	public static boolean isBigDecimal(String str) {
		try {
			new BigDecimal(str);
			return true;

		} catch (Exception e) {
			return false;
		}
	}

	private static String escapeChr(String value) {
		String newValue = value.replace("\\", "\\\\");
		newValue = "'" + newValue.replace("'", "''") + "'";
		return newValue;
	}

	public static String loginStringCheck(String value) {
		String newValue = "";
		if (value == null || value.trim().equalsIgnoreCase("")) {
			newValue = "null";
		} else {
			newValue = escapeChr(value);
		}
		return newValue;
	}

	public static String nullStringCheck(String value) {
		String newValue = "";
		if (value == null || value.trim().equalsIgnoreCase("")) {
			newValue = "null";
		} else {
			newValue = escapeChr(value);
		}
		newValue = newValue.replace(".", ",");
		return newValue;
	}

	public static String stringCheck(String value) {
		String newValue;
		if (value == null || value.trim().equalsIgnoreCase("") || value.trim().equalsIgnoreCase("null")) {
			newValue = "''";
		} else {
			newValue = escapeChr(value);
		}
		newValue = newValue.replace(".", ",");
		return newValue;
	}

	public static String stringCheck(String value, String searchMetod) {
		String newValue;
		if (value == null || value.trim().equalsIgnoreCase("") || value.trim().equalsIgnoreCase("null")) {
			newValue = "''";
		} else {
			if (searchMetod.equals(ApplicationConstantBase._insideTextSearch)) {
				newValue = escapeChrInsideSearch(value);
			} else if (searchMetod.equals(ApplicationConstantBase._beginsWithTextSearch)) {
				newValue = escapeChrBeginsSearch(value);
			} else {
				newValue = escapeChrEqualsSearch(value);
			}
		}
		return newValue;
	}

	private static String escapeChrInsideSearch(String value) {
		String newValue = value.replace("\\", "\\\\");
		newValue = "%" + newValue.replace("'", "''") + "%";
		return newValue;
	}

	private static String escapeChrBeginsSearch(String value) {
		String newValue = value.replace("\\", "\\\\");
		newValue = newValue.replace("'", "''") + "%";
		return newValue;
	}

	private static String escapeChrEqualsSearch(String value) {
		String newValue = value.replace("\\", "\\\\");
		newValue = newValue.replace("'", "''");
		return newValue;
	}

}
