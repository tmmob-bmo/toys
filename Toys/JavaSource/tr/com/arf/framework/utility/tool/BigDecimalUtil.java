package tr.com.arf.framework.utility.tool;

import java.math.BigDecimal;

public class BigDecimalUtil {

	public static BigDecimal changeToCurrency(BigDecimal bd) {
		if (bd == null) {
			return new BigDecimal(0L).setScale(2, BigDecimal.ROUND_DOWN);
		}
		return bd.setScale(2, BigDecimal.ROUND_DOWN);
	}

}
