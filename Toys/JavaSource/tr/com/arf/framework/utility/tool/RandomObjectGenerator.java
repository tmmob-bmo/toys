package tr.com.arf.framework.utility.tool;

import java.lang.reflect.Field;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;
import java.util.Random;

import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.DirectFieldAccessor;

import tr.com.arf.framework.db._management.DBOperator;
import tr.com.arf.framework.db.model._base.BaseEntity;

/**
 * Rastgele object üreten sınıf.
 *
 */
public class RandomObjectGenerator {

	/**
	 * @param args
	 * @throws ClassNotFoundException
	 * @throws SecurityException
	 * @throws NoSuchFieldException
	 * @throws IllegalAccessException
	 * @throws IllegalArgumentException
	 */
	public RandomObjectGenerator() {
		super();
	}

	/**
	 * Rastgele object üreten method.
	 *
	 * @param obj
	 * @return obj
	 */
	public static BaseEntity createObject(BaseEntity obj) throws ClassNotFoundException, NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
		DirectFieldAccessor myObjectAccessor = new DirectFieldAccessor(obj);
		for (Field field : obj.getClass().getDeclaredFields()) {
			if (!field.getName().equalsIgnoreCase("RID")) {
				Class<?> fieldType = field.getType();
				if (fieldType.equals(String.class)) {
					myObjectAccessor.setPropertyValue(field.getName(), RandomStringUtils.randomAlphabetic(10).toUpperCase());
				} else if (Date.class.isAssignableFrom(fieldType)) {
					myObjectAccessor.setPropertyValue(field.getName(), generateRandomDate());
				} else if (Number.class.isAssignableFrom(fieldType)) {
					myObjectAccessor.setPropertyValue(field.getName(), randBetween(1));
				} else if (fieldType.equals(Integer.TYPE)) {
					myObjectAccessor.setPropertyValue(field.getName(), randBetween(1, 5000));
				} else if (fieldType.equals(Long.TYPE) && !field.getName().contains("serialVersion")) {
					myObjectAccessor.setPropertyValue(field.getName(), randBetween(5));
				} else if (fieldType.equals(Double.TYPE)) {
					myObjectAccessor.setPropertyValue(field.getName(), randBetween(2));
				} else if (Enum.class.isAssignableFrom(fieldType)) {
					Object[] enumValues = fieldType.getEnumConstants();
					myObjectAccessor.setPropertyValue(field.getName(), enumValues[randBetween(0, enumValues.length)]);
				} else if (!field.getName().contains("serialVersion")) {
					try {
						DBOperator dbOperator = new DBOperator();
						String entityClass = field.getType().toString();
						entityClass = makeFirstCharUpperCase(entityClass.substring(entityClass.lastIndexOf(".") + 1));
						// System.out.println("REF Kolon :" + entityClass);
						@SuppressWarnings("unchecked")
						List<BaseEntity> objectList = dbOperator.load(entityClass);
						int length = objectList.size();
						if (length == 1) {
							myObjectAccessor.setPropertyValue(field.getName(), objectList.get(0));
						} else {
							myObjectAccessor.setPropertyValue(field.getName(), objectList.get(randBetween(0, length - 1)));
						}
					} catch (Exception e) {
						System.out.println("HATA:" + e.getMessage());
						System.out.println(field.getName() + " alani icin deger set edilemedi...");
					}
				}
			}
		}
		return obj;
	}

	/**
	 * Rastgele tarih objesi üreten method.
	 *
	 * @return gc.getTime()
	 */
	@SuppressWarnings("static-access")
	public static Date generateRandomDate() {
		GregorianCalendar gc = new GregorianCalendar();
		int year = randBetween(1900, 2010);
		gc.set(gc.YEAR, year);
		int dayOfYear = randBetween(1, gc.getActualMaximum(gc.DAY_OF_YEAR));
		gc.set(gc.DAY_OF_YEAR, dayOfYear);
		return gc.getTime();

	}

	/**
	 * Rastgele String objesi üreten method.
	 *
	 * @param lentgh
	 * @param upperCase
	 * @return RandomStringUtils.randomAlphabetic(lentgh).toUpperCase() ||
	 *         RandomStringUtils.randomAlphabetic(lentgh)
	 */
	public static String generateRandomString(int lentgh, boolean upperCase) {
		if (upperCase) {
			return RandomStringUtils.randomAlphabetic(lentgh).toUpperCase(Locale.ENGLISH);
		} else {
			return RandomStringUtils.randomAlphabetic(lentgh);
		}
	}

	/**
	 * Rastgele Long objesi üreten method.
	 *
	 * @param digits
	 * @return result
	 */
	public static Long randBetween(int digits) {
		Long result = 0L;
		while (result.toString().length() != digits) {
			result = Long.parseLong(RandomStringUtils.randomNumeric(digits));
		}
		return result;
	}

	/**
	 * Girilen iki değer arasında rastgele bir sayı(int) üreten method.
	 *
	 * @param min
	 * @param max
	 * @return min + 1 || randomNumber
	 */
	public static int randBetween(int min, int max) {
		Random foo = new Random();
		int randomNumber = foo.nextInt(max - min) + min;
		if (randomNumber == min) {
			return min + 1;
		} else {
			return randomNumber;
		}
	}

	/**
	 * Girilen bir String'in ilk harfini büyük harf yapan method.
	 *
	 * @param someString
	 * @return string1
	 */
	public static String makeFirstCharUpperCase(String someString) {
		String string1 = makeUpperCase(someString.substring(0, 1)) + someString.substring(1);
		return string1;
	}

	/**
	 * Girilen bir String'in tüm harflerini büyük harf yapan method.
	 *
	 * @param someString
	 * @return string1
	 */
	public static String makeUpperCase(String someString) {
		String string1 = null;
		try {
			string1 = someString.toUpperCase();
			string1 = string1.replace("İ", "I");
			string1 = string1.replace("Ö", "O");
			string1 = string1.replace("Ü", "U");
			string1 = string1.replace("Ğ", "G");
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("MakeUpperCase Hata :" + someString);
		}
		return string1;
	}

	private final static String alphabet = "0123456789abcdefghijklmnoprstuvyz";
	private final static int N = alphabet.length();

	public static String generateRandomPassword(int length) {
		Random r = new Random();
		String result = "";
		for (int i = 0; i < length; i++) {
			result += alphabet.charAt(r.nextInt(N));
		}
		return result;
	}
	/*************************** ORNEK ************************************/
	/*
	 * public void createTestData() { for(int x = 0; x < 5; x++) { try {
	 * Etkinliktanim randEtkinlikTanim = (Etkinliktanim)
	 * RandomObjectGenerator.createObject(new Etkinliktanim());
	 * getDBOperator().insert(randEtkinlikTanim); } catch (Exception e) {
	 * e.printStackTrace(); } }
	 *
	 * for(int x = 0; x < 25; x++) { try { Etkinlik randEtkinlik = (Etkinlik)
	 * RandomObjectGenerator.createObject(new Etkinlik());
	 * randEtkinlik.setBitistarih
	 * (DateUtil.addDaysToDate(randEtkinlik.getBitistarih() ,
	 * RandomObjectGenerator.randBetween(1, 15)));
	 * getDBOperator().insert(randEtkinlik); } catch (Exception e) {
	 * e.printStackTrace(); } } }
	 */
}
