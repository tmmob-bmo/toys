package tr.com.arf.framework.utility.tool;

import java.math.BigDecimal;
import java.text.DecimalFormat;

public class CodeGeneratorStringUtil {

	public static String clearTurkishChars(String str) {
		String ret = str;
		char[] turkishChars = new char[] { 0x131, 0x130, 0xFC, 0xDC, 0xF6, 0xD6, 0x15F, 0x15E, 0xE7, 0xC7, 0x11F, 0x11E };
		char[] englishChars = new char[] { 'i', 'I', 'u', 'U', 'o', 'O', 's', 'S', 'c', 'C', 'g', 'G' };
		for (int i = 0; i < turkishChars.length; i++) {
			ret = ret.replaceAll(new String(new char[] { turkishChars[i] }), new String(new char[] { englishChars[i] }));
		}
		return ret;
	}

	public static String cleanAlphaNumericCharacters(String value) {
		value = value.replaceAll("[^0-9]", "");
		return value;
	}

	public static boolean endsWithIgnoreCase(String str, String suffix) {
		return endsWith(str, suffix, true);
	}

	private static boolean endsWith(String str, String suffix, boolean ignoreCase) {
		if (str == null || suffix == null) {
			return str == null && suffix == null;
		}
		if (suffix.length() > str.length()) {
			return false;
		}
		int strOffset = str.length() - suffix.length();
		return str.regionMatches(ignoreCase, strOffset, suffix, 0, suffix.length());
	}

	public static String convertToMoney(BigDecimal value) {
		DecimalFormat moneyFormat = new DecimalFormat("0.00TL");
		return moneyFormat.format(value);
	}

	public static int count(final String s, final char c) {
		final char[] chars = s.toCharArray();
		int count = 0;
		for (int i = 0; i < chars.length; i++) {
			if (chars[i] == c) {
				count++;
			}
		}
		return count;
	}

	public static String fillStringLeft(String string, String fillString, int length) {
		String zeroString = string;
		int stringLength = string.length();

		for (byte i = 0; i < length - stringLength; i++) {
			zeroString = fillString + zeroString;
		}

		return zeroString;
	}

	public static String makeUpperCase(String someString) {
		String string1 = null;
		try {
			string1 = someString.toUpperCase();
			string1 = string1.replace("İ", "I");
			string1 = string1.replace("Ö", "O");
			string1 = string1.replace("Ü", "U");
			string1 = string1.replace("Ğ", "G");
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("MakeUpperCase Hata :" + someString);
		}
		return string1;
	}

	public static String makeLowerCase(String someString) {
		String string1 = null;
		try {
			string1 = someString.toLowerCase();
			string1 = string1.replace("ı", "i");
			string1 = string1.replace("ö", "o");
			string1 = string1.replace("ü", "ü");
			string1 = string1.replace("ğ", "g");
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("MakeLowerCase Hata :" + someString);
		}
		return string1;
	}

	public static String makeFirstCharUpperRestLowerCase(String someString) {
		String string1 = makeUpperCase(someString.substring(0, 1)) + someString.substring(1).toLowerCase();
		return string1;
	}

	public static String makeFirstCharLowerCase(String someString) {
		String string1 = makeLowerCase(someString.substring(0, 1)) + someString.substring(1);
		return string1;
	}

	public static String makeFirstCharUpperCase(String someString) {
		String string1 = makeUpperCase(someString.substring(0, 1)) + someString.substring(1);
		return string1;
	}

	public String findDifferencesBetweenTwoStrings(String oldValue, String newValue) {
		String[] oldVal = oldValue.split("<br />");
		String[] newVal = newValue.split("<br />");
		StringBuilder result = new StringBuilder();
		for (int x = 0; x < oldVal.length; x++) {
			if (!oldVal[x].equalsIgnoreCase(newVal[x])) {
				result.append(newVal[x] + "<br />");
			}
		}
		// System.out.println("Result :" + result.toString());
		return result.toString();
	}

	public static boolean isNumeric(String str) {
		try {
			@SuppressWarnings("unused")
			double d = Double.parseDouble(str);
		} catch (NumberFormatException nfe) {
			return false;
		}
		return true;
	}
	
	public static String cutString(String value, int length){
		if(value.length() <= length){
			return value;
		} else {
			return value.substring(0, length - 1) + ".";
		} 		
	}
}
