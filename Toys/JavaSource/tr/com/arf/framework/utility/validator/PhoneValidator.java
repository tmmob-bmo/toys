package tr.com.arf.framework.utility.validator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.ValidatorException;

import tr.com.arf.framework.utility.tool.KeyUtil;

@FacesValidator("phoneValidator")
public class PhoneValidator extends BaseValidator {

	//private static final String PHONE_PATTERN = "0\\d{3}\\d{7}";
    private static final String PHONE_PATTERN = "0\\s[\\(]\\d{3}[\\)]\\s\\d{3}\\s\\d{4}";
	private final Pattern pattern;

	public PhoneValidator() {
		pattern = Pattern.compile(PHONE_PATTERN);
	}

	@Override
	public void validate(FacesContext facesContext, UIComponent uiComponent, Object value) throws ValidatorException {

		if (value != null && value.toString().trim().length() > 0) {

            Matcher matcher = pattern.matcher(value.toString());
            if (!matcher.matches()) {
                FacesMessage message = new FacesMessage();
                message.setDetail(KeyUtil.getFrameworkLabel("gecersiz.telefon"));
                message.setSummary(KeyUtil.getFrameworkLabel("gecersiz.telefon"));
                message.setSeverity(FacesMessage.SEVERITY_ERROR);
                facesContext.addMessage(uiComponent.getClientId(facesContext), message);
                throw new ValidatorException(message);
            }

//			try {
//				Long.parseLong(value.toString().trim());
//			} catch (Exception e) {
//				FacesMessage message = new FacesMessage();
//				message.setDetail(KeyUtil.getFrameworkLabel("gecersiz.telefon"));
//				message.setSummary(KeyUtil.getFrameworkLabel("gecersiz.telefon"));
//				message.setSeverity(FacesMessage.SEVERITY_ERROR);
//				facesContext.addMessage(uiComponent.getClientId(facesContext), message);
//				throw new ValidatorException(message);
//			}


		}

	}
}
