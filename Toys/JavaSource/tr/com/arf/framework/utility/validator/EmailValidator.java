package tr.com.arf.framework.utility.validator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.ValidatorException;

import tr.com.arf.framework.utility.tool.KeyUtil;

@FacesValidator("eMailValidator")
public class EmailValidator extends BaseValidator {

	private static final String EMAIL_PATTERN = "^[_A-Za-z0-9-]+(\\." + "[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*" + "(\\.[A-Za-z]{2,})$";

	private final Pattern pattern;
	private Matcher matcher;

	public EmailValidator() {
		pattern = Pattern.compile(EMAIL_PATTERN);
	}

	@Override
	public void validate(FacesContext facesContext, UIComponent uiComponent, Object value) throws ValidatorException {

		matcher = pattern.matcher(value.toString());
		if (!matcher.matches()) {
			FacesMessage message = new FacesMessage();
			message.setDetail(KeyUtil.getFrameworkLabel("gecersiz.eposta"));
			message.setSummary(KeyUtil.getFrameworkLabel("gecersiz.eposta"));
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			facesContext.addMessage(uiComponent.getClientId(facesContext), message);
			throw new ValidatorException(message);

		}
	}
}
