package tr.com.arf.framework.utility.validator;

import java.math.BigDecimal;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.ValidatorException;

import tr.com.arf.framework.utility.tool.KeyUtil;
import tr.com.arf.framework.utility.tool.CodeGeneratorStringUtil;

@FacesValidator("bigDecimalValidator")
public class BigDecimalValidator extends BaseValidator {

	public BigDecimalValidator() {
		super();
	}

	@Override
	public void validate(FacesContext facesContext, UIComponent uiComponent, Object value) throws ValidatorException {
		if (value != null && value.toString().length() > 0) {
			String deger = (String) value;
			if (deger.indexOf(".") > 0) {
				String ondalikDeger = deger.trim().substring(deger.indexOf(".") + 1);
				if (ondalikDeger.length() > 2 || CodeGeneratorStringUtil.count(deger, '.') > 1) {
					((UIInput) uiComponent).setValid(false);
					FacesMessage message = new FacesMessage();
					message.setDetail(KeyUtil.getFrameworkLabel("gecersiz.para"));
					message.setSummary(KeyUtil.getFrameworkLabel("gecersiz.para"));
					message.setSeverity(FacesMessage.SEVERITY_ERROR);
					facesContext.addMessage(uiComponent.getClientId(facesContext), message);
					throw new ValidatorException(message);
				}
			} else {
				try {
					@SuppressWarnings("unused")
					BigDecimal s = new BigDecimal(deger);
				} catch (Exception e) {
					((UIInput) uiComponent).setValid(false);
					FacesMessage message = new FacesMessage();
					message.setDetail(KeyUtil.getFrameworkLabel("gecersiz.para"));
					message.setSummary(KeyUtil.getFrameworkLabel("gecersiz.para"));
					message.setSeverity(FacesMessage.SEVERITY_ERROR);
					facesContext.addMessage(uiComponent.getClientId(facesContext), message);
					throw new ValidatorException(message);
				}
			}
		}
	}
}
