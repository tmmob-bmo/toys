package tr.com.arf.framework.utility.validator;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.ValidatorException;

import tr.com.arf.framework.utility.tool.KeyUtil;

@FacesValidator("tcKimlikNoValidator")
public class TCKimlikNoValidator extends BaseValidator {

	public TCKimlikNoValidator() {
		super();
	}

	@Override
	public void validate(FacesContext facesContext, UIComponent uiComponent, Object value) throws ValidatorException {
		String kimlikNo = "";

		if (value != null) {
			kimlikNo = (String) value;
		}
		kimlikNo = kimlikNo.replaceAll(" ", "");
		if (kimlikNo.trim().length() != 11) {
			((UIInput) uiComponent).setValid(false);
			FacesMessage message = new FacesMessage();
			message.setDetail(KeyUtil.getFrameworkLabel("gecersiz.tcKimlik"));
			message.setSummary(KeyUtil.getFrameworkLabel("gecersiz.tcKimlik"));
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			facesContext.addMessage(uiComponent.getClientId(facesContext), message);
			throw new ValidatorException(message);
		} else if (Integer.parseInt(kimlikNo.charAt(0) + "") == 0) {
			((UIInput) uiComponent).setValid(false);
			FacesMessage message = new FacesMessage();
			message.setDetail(KeyUtil.getFrameworkLabel("gecersiz.tcKimlik"));
			message.setSummary(KeyUtil.getFrameworkLabel("gecersiz.tcKimlik"));
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			facesContext.addMessage(uiComponent.getClientId(facesContext), message);
			throw new ValidatorException(message);
		} else {

			int[] numaralar = new int[11];
			for (int i = 0; i < 11; i++) {
				numaralar[i] = Integer.parseInt(kimlikNo.charAt(i) + "");
			}

			int ciftlerToplami = 0;
			for (int i = 1; i < 8; i += 2) {
				ciftlerToplami += numaralar[i];
			}

			int teklerToplami = 0;
			for (int i = 0; i <= 8; i += 2) {

				teklerToplami += numaralar[i];

			}

			int t1 = teklerToplami * 3 + ciftlerToplami;
			int c1 = (10 - t1 % 10) % 10;
			int t2 = c1 + ciftlerToplami;
			int t3 = t2 * 3 + teklerToplami;
			int c2 = (10 - t3 % 10) % 10;

			if (c1 != numaralar[9] || c2 != numaralar[10]) {

				((UIInput) uiComponent).setValid(false);
				FacesMessage message = new FacesMessage();
				message.setDetail(KeyUtil.getFrameworkLabel("gecersiz.tcKimlik"));
				message.setSummary(KeyUtil.getFrameworkLabel("gecersiz.tcKimlik"));
				message.setSeverity(FacesMessage.SEVERITY_ERROR);
				facesContext.addMessage(uiComponent.getClientId(facesContext), message);

				throw new ValidatorException(message);

			}

		}

	}
}
