package tr.com.arf.framework.utility.validator;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

public abstract class BaseValidator implements Validator {

	public BaseValidator() {
	}

	@Override
	public abstract void validate(FacesContext facesContext, UIComponent uiComponent, Object value) throws ValidatorException;

}
