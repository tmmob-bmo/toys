package tr.com.arf.framework.utility.validator;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;

public class CommonValidators {

	/**
	 * Validate email with Java mail
	 */
	public static boolean validateEmail(String email) {
		boolean result = true;
		try {
			InternetAddress emailAddr = new InternetAddress(email);
			emailAddr.validate();
		} catch (AddressException ex) {
			result = false;
		}
		return result;
	}

}
