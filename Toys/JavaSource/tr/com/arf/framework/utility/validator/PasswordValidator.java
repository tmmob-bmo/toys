package tr.com.arf.framework.utility.validator;

import tr.com.arf.framework.utility.tool.KeyUtil;
import tr.com.arf.toys.service.application.ChangePasswordValidator;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.ValidatorException;

@FacesValidator("passwordValidator")
public class PasswordValidator extends BaseValidator {
    private static final ChangePasswordValidator validator = ChangePasswordValidator.buildValidator();


	@Override
	public void validate(FacesContext facesContext, UIComponent uiComponent, Object value) throws ValidatorException {
		if (value == null || value.toString().length() == 0) {
			return;
		}

		if (!validator.validatePassword(value.toString())) {
			FacesMessage message = new FacesMessage();
			message.setDetail(KeyUtil.getFrameworkLabel("gecersiz.sifre"));
			message.setSummary(KeyUtil.getFrameworkLabel("gecersiz.sifre"));
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			facesContext.addMessage(uiComponent.getClientId(facesContext), message);
			throw new ValidatorException(message);

		}

	}
}
