package tr.com.arf.framework.utility.validator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ValidatorUtils {

	private static final String PHONE_PATTERN = "0\\d{3}\\d{7}";
	private static Pattern pattern;

	public ValidatorUtils() {
		pattern = Pattern.compile(PHONE_PATTERN);
	}

	public static boolean validate(Object value) throws Exception {
		if (value == null || value.toString().trim().length() == 0) {
			return false;
		} else {
			try {
				Long.parseLong(value.toString().trim());
			} catch (Exception e) {
				return false;
			}

			Matcher matcher = pattern.matcher(value.toString());
			if (!matcher.matches()) {
				return false;
			}
			return true;
		}

	}
}
