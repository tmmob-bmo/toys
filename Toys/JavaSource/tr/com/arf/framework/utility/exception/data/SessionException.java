package tr.com.arf.framework.utility.exception.data;

import tr.com.arf.framework.utility.exception._base.BaseException;

public class SessionException extends BaseException {

	private static final long serialVersionUID = 962455466904642965L;

	private static String nameInstance = "Oturum Hatası!";

	public SessionException(Exception exception) {
		super(exception, 0, nameInstance);
	}

	public SessionException(Exception exception, String className, String methodName, String specialIdentifier) {
		super(exception, 0, nameInstance, className, methodName, specialIdentifier);
	}

	public SessionException(Exception exception, String className, String methodName) {
		super(exception, 0, nameInstance, className, methodName);
	}

	protected String generateUserMessage(Exception exception) {
		String generatedMessage = "Tarım Dışı: ";
		generatedMessage += "Geçerli ağ oturumunda birden fazla bağlantı yapamazsınız. ";
		generatedMessage += "Yeni bağlantı kurmak için tarayıcınızın tüm kopyalarını ";
		generatedMessage += "kapatmanız gerekmektedir.";
		return generatedMessage;
	}

}