package tr.com.arf.framework.utility.exception.data;

import tr.com.arf.framework.utility.exception._base.BaseException;
import tr.com.arf.framework.utility.tool.KeyUtil;

public class DBException extends BaseException {

	private static final long serialVersionUID = 3706555742105371590L;

	private static String nameInstance = "Veri Hatası";

	public DBException(Exception exception) {
		super(exception, 0, nameInstance);
	}

	public DBException(Exception exception, String className, String methodName) {
		super(exception, 0, nameInstance, className, methodName);
	}

	public DBException(Exception exception, String className, String methodName, String specialIdentifier) {
		super(exception, 0, nameInstance, className, methodName, specialIdentifier);
	}

	@Override
	protected String generateUserMessage(Exception exception) {
		String generatedMessage = "";
		String originalMessage = exception.getMessage();
		try {
			System.out.println("***************************** VT'ndan gelen exception Message :" + exception.getMessage());
			System.out.println("***************************** VT'ndan gelen exception Cause Level 1 :" + exception.getCause().toString());
			System.out.println("***************************** VT'ndan gelen exception Cause Level 2 :" + exception.getCause().getCause().toString());
			System.out.println("***************************** VT'ndan gelen exception Cause Level 3 :" + exception.getCause().getCause().getCause().toString());
		} catch (Exception e) {
			System.out.println("..................................");
		}

		if ((getMethodName().equalsIgnoreCase("Ekleme") || getMethodName().equalsIgnoreCase("Gunleme"))
				&& exception.getCause() instanceof org.hibernate.exception.ConstraintViolationException) {
			if (exception.getCause().getCause().toString().contains("tekil")) {
				generatedMessage = KeyUtil.getFrameworkLabel("kayit.mukerrer");
			} else if (exception.getCause().getCause().toString().contains("not-null") || exception.getCause().getCause().toString().contains("null de")) {
				generatedMessage = KeyUtil.getFrameworkLabel("kayit.notNull");
			} else {
				generatedMessage = KeyUtil.getFrameworkLabel("kayit.primaryKeyIhlal");
			}
		} else if (getMethodName().equalsIgnoreCase("Delete") && exception.getCause().getCause() instanceof org.hibernate.exception.ConstraintViolationException) {
			generatedMessage = KeyUtil.getFrameworkLabel("hibernate.deleteConstraintViolation");
		} else if (originalMessage.contains("mevcut de")) {
			generatedMessage = "Tabloda eksik kolon var! / Tablo yapısı hatalı! ";
			generatedMessage += "\n Bu hatayı sistem yöneticisine bildiriniz! ";

			try {
				String missingColumn = "";
				missingColumn = originalMessage.replace("\"", "");
				missingColumn = missingColumn.replace("sütunu mevcut değil", "");
				missingColumn = missingColumn.replace(" ", "");
				// System.out.println("Missing column : " + missingColumn );
				generatedMessage += "\n Detay kaydın yer aldığı tablo :" + missingColumn;

			} catch (Exception e) {
				// do nothing
			}
		} else {
			generatedMessage += originalMessage;
		}
		// System.out.println("Generated message :" + generatedMessage);
		return generatedMessage;
	}
}
