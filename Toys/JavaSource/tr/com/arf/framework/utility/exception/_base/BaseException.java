package tr.com.arf.framework.utility.exception._base;

public abstract class BaseException extends Exception {

	private static final long serialVersionUID = 47051542565678638L;

	private String name;
	private String className;
	private String methodName;
	private String specialIdentifier;
	private String userMessage;

	protected int type;

	/* Constructors */
	protected BaseException(Exception exception, int type, String name) {
		super(exception);
		this.type = type;
		setName(name);
		// ManagedBeanLocator.locateActiveException().setException(this);
	}

	protected BaseException(Exception exception, int type, String name, String className, String methodName) {
		this(exception, type, name);
		setClassName(className);
		setMethodName(methodName);
		setUserMessage(generateUserMessage(exception));
	}

	protected BaseException(Exception exception, int type, String name, String className, String methodName, String specialIdentifier) {
		this(exception, type, name);
		setClassName(className);
		setMethodName(methodName);
		setSpecialIdentifier(specialIdentifier);
		setUserMessage(generateUserMessage(exception));
	}

	/* Methods */
	protected abstract String generateUserMessage(Exception exception);

	/* Getters & Setters */
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getClassName() {
		return className;
	}

	private void setClassName(String className) {
		this.className = className;
	}

	public String getMethodName() {
		return methodName;
	}

	private void setMethodName(String methodName) {
		this.methodName = methodName;
	}

	public String getSpecialIdentifier() {
		return specialIdentifier;
	}

	private void setSpecialIdentifier(String specialIdentifier) {
		this.specialIdentifier = specialIdentifier;
	}

	public String getUserMessage() {
		return userMessage;
	}

	private void setUserMessage(String userMessage) {
		this.userMessage = userMessage;
	}

}
