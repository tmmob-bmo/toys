package tr.com.arf.framework.utility.exception._base;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

@ManagedBean
@SessionScoped
public class ActiveException {

	private BaseException exception;
	private String lastOutcome;

	public ActiveException() {
	}

	public BaseException getException() {
		return exception;
	}

	public void setException(BaseException exception) {
		this.exception = exception;
	}

	public String getLastOutcome() {
		return lastOutcome;
	}

	public void setLastOutcome(String lastOutcome) {
		this.lastOutcome = lastOutcome;
	}

}
