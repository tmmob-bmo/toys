package tr.com.arf.framework.utility.listener;

import java.util.Locale;

import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

public final class LocaleListener implements HttpSessionListener {

	@Override
	public void sessionCreated(HttpSessionEvent event) {
		Locale.setDefault(new Locale("tr", "TR"));
	}

	@Override
	public void sessionDestroyed(HttpSessionEvent event) {
	}

}