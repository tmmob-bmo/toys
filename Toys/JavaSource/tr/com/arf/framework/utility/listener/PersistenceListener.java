package tr.com.arf.framework.utility.listener;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import tr.com.arf.framework.db._management.PersistenceManager;

public class PersistenceListener implements ServletContextListener {

	public void contextInitialized(ServletContextEvent servletContextEvent) {
	}

	public void contextDestroyed(ServletContextEvent servletContextEvent) {
		PersistenceManager.getInstance().closeEntityManagerFactory();
	}

}