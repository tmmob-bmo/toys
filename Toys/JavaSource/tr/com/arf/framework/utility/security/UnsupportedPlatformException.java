// Copyright (C) 2011 - Will Glozer.  All rights reserved.

package tr.com.arf.framework.utility.security;

/**
 * Exception thrown when the current platform cannot be detected.
 *
 * @author Will Glozer
 */
public class UnsupportedPlatformException extends RuntimeException {
    /**
	 * 
	 */
	private static final long serialVersionUID = -4534855004277463231L;

	public UnsupportedPlatformException(String s) {
        super(s);
    }
}
