package tr.com.arf.framework.utility.security;

public class ScryptPasswordHashing {

	public static String encrypt(String originalPassword) {
		return SCryptUtil.scrypt(originalPassword, 16, 16, 16);
	}

	public static boolean check(String originalPassword, String hashedPassword) {
		return SCryptUtil.check(originalPassword, hashedPassword);
	}
}