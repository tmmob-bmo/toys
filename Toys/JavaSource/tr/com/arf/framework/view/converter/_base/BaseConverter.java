package tr.com.arf.framework.view.converter._base;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;

import tr.com.arf.framework.db._management.DBOperator;

public abstract class BaseConverter implements javax.faces.convert.Converter {

	/* Class Attributes */
	private DBOperator dBOperator;

	/* Constructor */
	public BaseConverter() {
	}

	/* Abstract Methods */
	@Override
	public abstract Object getAsObject(FacesContext facesContext, UIComponent uiComponent, String value);

	@Override
	public abstract String getAsString(FacesContext facesContext, UIComponent uiComponent, Object value);

	/* Methods */
	protected final DBOperator getDBOperator() {
		if (dBOperator == null) {
			dBOperator = DBOperator.getInstance();
		}
		return dBOperator;
	}

}
