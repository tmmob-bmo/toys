package tr.com.arf.framework.view.controller.form._base;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.faces.model.SelectItem;

import tr.com.arf.framework.db._management.DBOperator;
import tr.com.arf.framework.db.filter._base.BaseFilter;
import tr.com.arf.framework.db.model._base.BaseEntity;
import tr.com.arf.framework.db.model.query.QueryObject;
import tr.com.arf.framework.utility.tool.KeyUtil;
import tr.com.arf.framework.utility.tool.CodeGeneratorStringUtil;

@SuppressWarnings("serial")
public abstract class BaseFilteredController extends BaseController {

	Calendar today = Calendar.getInstance();

	private String masterObjectName;
	private String masterOutcome;

	protected BaseFilteredController(@SuppressWarnings("rawtypes") Class c) {
		super(c);
	}

	public String backToMaster() {
		if (getMasterOutcome() != null) {
			return CodeGeneratorStringUtil.makeFirstCharLowerCase(getMasterOutcome()) + ".do";
		} else {
			return "";
		}
	}

	public abstract BaseFilter getFilter();

	public void resetFilter() {
		resetFilterOnly();
		queryAction();
	}

	public void resetFilterOnly() {
		tableRowCancelSelection();
		getFilter().clearQueryCriterias();
		getFilter().init();
		getFilter().createQueryCriterias();
		setTable(null);
	}

	@SuppressWarnings("unchecked")
	@Override
	public void queryAction() {
		DBOperator dbOperator = getDBOperator();
		List<BaseEntity> itemList = new ArrayList<BaseEntity>();
		tableRowCancelSelection();
		ArrayList<ArrayList<QueryObject>> allCriterias = (ArrayList<ArrayList<QueryObject>>) getFilter().createQueryCriterias();
		if (!isEmpty(allCriterias)) {
			if (!isEmpty(allCriterias.get(0))) {
				ArrayList<QueryObject> opsiyonelKriterler = new ArrayList<QueryObject>();
				if (!isEmpty(allCriterias.get(1))) {
					opsiyonelKriterler = allCriterias.get(1);
				}
				setRecordCount(dbOperator.recordCountFiltered(getModelName(), allCriterias.get(0), opsiyonelKriterler).intValue());
				itemList = dbOperator.loadFiltered(getModelName(), allCriterias.get(0), opsiyonelKriterler, getRecordInterval(), getOrderField());
				setListMessage(KeyUtil.getFrameworkLabel("list.toplam") + " " + getRecordCount() + " " + KeyUtil.getFrameworkLabel("kayit.bulundu"));
				setList(itemList);
				return;
			}
		}
		setRecordCount(dbOperator.recordCountAll(getModelName()).intValue());
		itemList = dbOperator.loadAll(getModelName(), getRecordInterval(), getOrderField());
		setListMessage(KeyUtil.getFrameworkLabel("list.toplam") + " " + getRecordCount() + " " + KeyUtil.getFrameworkLabel("kayit.bulundu"));
		setList(itemList);
	}

	@SuppressWarnings("unchecked")
	public SelectItem[] getFilteredSelectItemList() {
		ArrayList<QueryObject> kriterler = getFilter().createQueryCriterias().get(0);
		List<BaseEntity> entityList;
		entityList = getDBOperator().loadFiltered(getModelName(), kriterler, getFilter().createQueryCriterias().get(1), null, getOrderField());
		SelectItem[] selectItemList = new SelectItem[entityList.size() + 1];
		selectItemList[0] = new SelectItem(null, "");
		int i = 1;
		for (BaseEntity entity : entityList) {
			selectItemList[i++] = new SelectItem(entity, entity.getUIString());
		}
		return selectItemList;
	}

	public void newFilter() {
		super.changePage("first");
		setSelection(null);
		setTable(null);
		ArrayList<QueryObject> kriterler = getFilter().createQueryCriterias().get(0);
		if (!isEmpty(kriterler)) {
			getFilter().createQueryCriterias();
			queryAction();
		}
	}

	@Override
	public void browse() {
		queryAction();
		super.cancel();
	}

	public String getMasterOutcome() {
		return masterOutcome;
	}

	public void setMasterOutcome(String masterOutcome) {
		this.masterOutcome = masterOutcome;
	}

	public String getMasterObjectName() {
		return masterObjectName;
	}

	public void setMasterObjectName(String masterObjectName) {
		this.masterObjectName = masterObjectName;
	}
}
