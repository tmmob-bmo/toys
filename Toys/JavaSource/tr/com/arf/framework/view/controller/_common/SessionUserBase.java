package tr.com.arf.framework.view.controller._common;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import tr.com.arf.framework.utility.tool.DateUtil;

public abstract class SessionUserBase implements Serializable {

	private static final long serialVersionUID = 1L;

	private HttpSession session;
	private String message = "";

	private boolean superUser = false;
	private HashMap<String, Object> sessionFilters = new HashMap<String, Object>();
	private int legalAccess;

	private Locale locale = FacesContext.getCurrentInstance().getViewRoot().getLocale();
	private String theme = "bootstrap";

	private Long kullaniciRid;
	private String kullaniciText;

	public HttpSession getSession() {
		return session;
	}

	public void setSession(HttpSession session) {
		this.session = session;
	}

	public String getMessage() {
		String oldMessage = message;
		setMessage("");
		return oldMessage;
	}

	public String getMessageWithoutClearing() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public boolean isSuperUser() {
		return superUser;
	}

	public void setSuperUser(boolean superUser) {
		this.superUser = superUser;
	}

	public HashMap<String, Object> getSessionFilters() {
		return sessionFilters;
	}

	public void setSessionFilters(HashMap<String, Object> sessionFilters) {
		this.sessionFilters = sessionFilters;
	}

	public Object getSessionFilterObject(String objectName) {
		return getSessionFilters().get(objectName);
	}

	public void removeSessionFilterObject(String objectName) {
		try {
			getSessionFilters().remove(objectName);
		} catch (Exception e) {
			System.out.println("Nesne sessionda yok!");
		}
	}

	public void addToSessionFilters(String objectName, Object object) {
		// HashMap varolan entry'nin direk uzerine yazar, o yuzden remove
		// gerekmez!
		getSessionFilters().put(objectName, object);
	}

	public void clearSessionFilters() {
		// System.out.println("Cleaning session filters.");
		getSessionFilters().clear();
	}

	public int getSessionFilterItemSize() {
		return getSessionFilters().size();
	}

	public int getLegalAccess() {
		return legalAccess;
	}

	public void setLegalAccess(int legalAccess) {
		this.legalAccess = legalAccess;
	}

	public Locale getLocale() {
		return locale;
	}

	public void setLocale(Locale locale) {
		this.locale = locale;
	}

	public String getTheme() {
		return theme;
	}

	public void setTheme(String theme) {
		this.theme = theme;
	}

	public void createGenericMessage(String messageValue, javax.faces.application.FacesMessage.Severity messageSeverity) {
		FacesMessage message = new FacesMessage();
		FacesContext context = FacesContext.getCurrentInstance();
		message.setSeverity(messageSeverity);
		message.setDetail(messageValue);
		message.setSummary(messageValue);
		context.addMessage(null, message);
	}

	public void createCustomMessage(String messageValue, javax.faces.application.FacesMessage.Severity messageSeverity, String componentId) {
		FacesMessage message = new FacesMessage();
		FacesContext context = FacesContext.getCurrentInstance();
		message.setSeverity(messageSeverity);
		message.setDetail(messageValue);
		message.setSummary(messageValue);
		context.addMessage(componentId, message);
	}

	public String getTarih() {
		return DateUtil.dateToDMYHMS(new Date());
	}

	public Long getKullaniciRid() {
		return kullaniciRid;
	}

	public void setKullaniciRid(Long kullaniciRid) {
		this.kullaniciRid = kullaniciRid;
	}

	public String getKullaniciText() {
		return kullaniciText;
	}

	public void setKullaniciText(String kullaniciText) {
		this.kullaniciText = kullaniciText;
	}

	public abstract String getSystemPath();


	public abstract String getGlobalMessagePanelId();

	public abstract String getIpAddress();
}
