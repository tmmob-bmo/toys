package tr.com.arf.framework.view.controller.form._base;

import java.util.ArrayList;
import java.util.List;

import tr.com.arf.framework.db._management.DBOperator;
import tr.com.arf.framework.db.model._base.BaseEntity;
import tr.com.arf.framework.db.model.query.QueryObject;
import tr.com.arf.framework.utility.tool.KeyUtil;

public abstract class BaseMustFilteredControllerNoPaging extends BaseFilteredController {

	private static final long serialVersionUID = 6393308819569413390L;

	protected BaseMustFilteredControllerNoPaging(@SuppressWarnings("rawtypes") Class c) {
		super(c);
	}

	@SuppressWarnings("unchecked")
	@Override
	public void queryAction() {
		DBOperator dbOperator = getDBOperator();
		List<BaseEntity> itemList = new ArrayList<BaseEntity>();
		ArrayList<ArrayList<QueryObject>> allCriterias = (ArrayList<ArrayList<QueryObject>>) getFilter().createQueryCriterias();
		if (!isEmpty(allCriterias)) {
			if (!isEmpty(allCriterias.get(0))) {
				ArrayList<QueryObject> opsiyonelKriterler = new ArrayList<QueryObject>();
				if (!isEmpty(allCriterias.get(1))) {
					opsiyonelKriterler = allCriterias.get(1);
				}
				setRecordCount(dbOperator.recordCountFiltered(getModelName(), allCriterias.get(0), opsiyonelKriterler).intValue());
				itemList = dbOperator.loadFiltered(getModelName(), allCriterias.get(0), opsiyonelKriterler, null, getOrderField());
				setListMessage(KeyUtil.getFrameworkLabel("list.toplam") + " " + getRecordCount() + " " + KeyUtil.getFrameworkLabel("kayit.bulundu"));
				setList(itemList);
				return;
			}
		}
		setRecordCount(0);
		setListMessage(KeyUtil.getFrameworkLabel("list.aramaZorunluMesaj"));
		setList(new ArrayList<BaseEntity>());

	}

}
