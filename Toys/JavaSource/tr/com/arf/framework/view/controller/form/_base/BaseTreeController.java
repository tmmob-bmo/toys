package tr.com.arf.framework.view.controller.form._base;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import tr.com.arf.framework.db._management.DBOperator;
import tr.com.arf.framework.db.model._base.BaseEntity;
import tr.com.arf.framework.db.model._base.BaseTreeEntity;
import tr.com.arf.framework.db.model.query.QueryObject;
import tr.com.arf.framework.utility.exception.data.DBException;
import tr.com.arf.framework.utility.tool.KeyUtil;

public abstract class BaseTreeController extends BaseFilteredController {

	@SuppressWarnings("rawtypes")
	protected BaseTreeController(Class c) {
		super(c);
	}

	private String masterEntityStaticWhereCondition = null;

	public String treePage() {
		return getModelName().toLowerCase(Locale.ENGLISH) + "_Tree.do";
	}

	private static final long serialVersionUID = 1L;

	/************************************** TREE VIEW ************************************/

	@SuppressWarnings("unchecked")
	@Override
	public void queryAction() {
		List<BaseEntity> itemList = new ArrayList<BaseEntity>();
		ArrayList<ArrayList<QueryObject>> allCriterias = (ArrayList<ArrayList<QueryObject>>) getFilter().createQueryCriterias();
		if (!isEmpty(allCriterias)) {
			if (!isEmpty(allCriterias.get(0))) {
				ArrayList<QueryObject> opsiyonelKriterler = new ArrayList<QueryObject>();
				if (!isEmpty(allCriterias.get(1))) {
					opsiyonelKriterler = allCriterias.get(1);
				}
				setRecordCount(getDBOperator().recordCountFiltered(getModelName(), allCriterias.get(0), opsiyonelKriterler).intValue());
				itemList = getDBOperator().loadFiltered(getModelName(), allCriterias.get(0), opsiyonelKriterler, getRecordInterval(), getOrderField());
				createTreeList();
				setListMessage(KeyUtil.getFrameworkLabel("list.toplam") + " " + getRecordCount() + " " + KeyUtil.getFrameworkLabel("kayit.bulundu"));
				setList(itemList);
				return;
			}
		}
		setRecordCount(getDBOperator().recordCountAll(getModelName()).intValue());
		itemList = getDBOperator().loadAll(getModelName(), getRecordInterval(), getOrderField());
		createTreeList();
		setListMessage(KeyUtil.getFrameworkLabel("list.toplam") + " " + getRecordCount() + " " + KeyUtil.getFrameworkLabel("kayit.bulundu"));
		setList(itemList);
	}

	public void expandTree() {
		FacesContext context = FacesContext.getCurrentInstance();
		String rid = context.getExternalContext().getRequestParameterMap().get("rid").toString();
		getExpandedItemsOid().add(Long.parseLong(rid));
		createTreeList();
	}

	public void expandThis(Long rid) {
		getExpandedItemsOid().add(rid);
		createTreeList();
	}

	@SuppressWarnings("unchecked")
	public void expandAll() {
		List<BaseTreeEntity> expendableList = getDBOperator().load(getModelName(), " o.rID IN (SELECT m.ustRef.rID FROM " + getModelName() + " m WHERE m.ustRef IS NOT NULL )",
				"o.erisimKodu");
		for (BaseTreeEntity object : expendableList) {
			getExpandedItemsOid().add(object.getRID());
		}
		createTreeList();
	}

	public void collapseAll() {
		getExpandedItemsOid().clear();
		createTreeList();
	}

	@SuppressWarnings("unchecked")
	public void collapseTree() {
		FacesContext context = FacesContext.getCurrentInstance();
		String rid = context.getExternalContext().getRequestParameterMap().get("rid").toString();
		BaseTreeEntity entity = (BaseTreeEntity) getDBOperator().find(getModelName(), "rID", rid).get(0);
		if (entity.getUstRef() == null) {
			setExpandedItemsOid(new ArrayList<Long>());
		} else {
			List<BaseTreeEntity> entitySubList = getDBOperator().load(getModelName(), " o.ustRef.rID =" + entity.getRID(), "o.erisimKodu");
			if (entitySubList != null) {
				for (BaseTreeEntity subEntity : entitySubList) {
					getExpandedItemsOid().remove(subEntity.getRID());
				}
			}
			getExpandedItemsOid().remove(entity.getRID());
		}
		createTreeList();
	}

	private List<BaseTreeEntity> treeList;
	private ArrayList<Long> expandedItemsOid = new ArrayList<Long>();

	@SuppressWarnings("unchecked")
	protected void createTreeList() {
		treeList = new ArrayList<BaseTreeEntity>();
		try {
			String queryCondition = getQueryCriteria();
			if (!isEmpty(getMasterEntityStaticWhereCondition())) {
				queryCondition += " AND " + getMasterEntityStaticWhereCondition();
			}

			List<BaseTreeEntity> preList = getDBOperator().load(getModelName(), queryCondition, "o.erisimKodu ASC");
			for (BaseTreeEntity entity : preList) {
				String queryText = "o.ustRef.rID ='" + entity.getRID() + "'";
				if (!isEmpty(getStaticWhereCondition())) {
					queryText += " AND " + getStaticWhereCondition();
				}
				if (!isEmpty(getMasterEntityStaticWhereCondition())) {
					queryText += " AND " + getMasterEntityStaticWhereCondition();
				}
				if (getDBOperator().recordCount(getModelName(), queryText) > 0) {
					if (!isEmpty(getExpandedItemsOid())) {
						boolean found = getExpandedItemsOid().contains(entity.getRID());
						// System.out.println(entity.getAd() + " Found ? :" +
						// found);
						if (found) {
							entity.setIcon("minus");
						} else {
							entity.setIcon("plus");
						}
					} else {
						entity.setIcon("plus");
					}
				} else {
					entity.setIcon("bosluk");
				}
				treeList.add(entity);
			}
		} catch (Exception e) {
			System.out.println("TreeList Error :" + e.getMessage());
		}
		setTreeList(treeList);
	}

	public List<BaseTreeEntity> getTreeList() {
		return treeList;
	}

	public void setTreeList(List<BaseTreeEntity> treeList) {
		this.treeList = treeList;
	}

	public ArrayList<Long> getExpandedItemsOid() {
		return expandedItemsOid;
	}

	public void setExpandedItemsOid(ArrayList<Long> expandedItemsOid) {
		this.expandedItemsOid = expandedItemsOid;
	}

	public String getQueryCriteria() {
		if (getExpandedItemsOid().size() > 0) {
			StringBuffer queryCriteria = new StringBuffer(" (");
			for (int x = 0; x < getExpandedItemsOid().size(); x++) {
				if (x == 0) {
					queryCriteria.append("(o.rID = '" + getExpandedItemsOid().get(x) + "' OR  o.ustRef.rID = '" + getExpandedItemsOid().get(x) + "') ");
				} else {
					queryCriteria.append(" OR (o.rID = '" + getExpandedItemsOid().get(x) + "' OR  o.ustRef.rID = '" + getExpandedItemsOid().get(x) + "') ");
				}
			}
			queryCriteria.append(")");
			// System.out.println("Query criteria :" +
			// queryCriteria.toString());
			return queryCriteria.toString();
		} else {
			return " o.ustRef is null ";
		}
	}

	public void addToTree(String selectedRID) {
		if (!isEmpty(selectedRID)) {
			setSelectedRID(selectedRID);
			setTable(null);
			BaseTreeEntity ustKayit = (BaseTreeEntity) getEntity();
			BaseTreeEntity cloneableRec = (BaseTreeEntity) ustKayit.cloneObject();
			cloneableRec.setRID(null);
			super.insert();
			cloneableRec.setUstRef(ustKayit);
			setPreviousUstRef(cloneableRec.getUstRef());
			setEntity(cloneableRec);
			if (!getExpandedItemsOid().contains(ustKayit.getRID())) {
				getExpandedItemsOid().add(ustKayit.getRID());
			}
			return;
		}
	}

	public void addSubRecord() {
		if (setSelected()) {
			BaseTreeEntity ustKayit = (BaseTreeEntity) getEntity();
			BaseTreeEntity cloneableRec = (BaseTreeEntity) ustKayit.cloneObject();
			cloneableRec.setRID(null);
			super.insert();
			cloneableRec.setUstRef(ustKayit);
			setEntity(cloneableRec);
			return;
		}
	}

	public void editFromTree(String selectedRID) {
		if (!isEmpty(selectedRID)) {
			setSelectedRID(selectedRID);
			setTable(null);
			setOldValue(getEntity().getValue());
			setPreviousUstRef(((BaseTreeEntity) getEntity()).getUstRef());
			setEditPanelRendered(true);
			return;
		}
	}

	public void deleteFromTree(String selectedRID) {
		if (!isEmpty(selectedRID)) {
			setSelectedRID(selectedRID);
			setTable(null);
			if (selectedRID.equalsIgnoreCase("1")) {
				createGenericMessage(KeyUtil.getFrameworkLabel("tree.baseRecordDelete"), FacesMessage.SEVERITY_ERROR);
				return;
			}
			try {
				getDBOperator().delete(super.getEntity());
				queryAction();
				setSelection(null);
				setTable(null);
				createGenericMessage(KeyUtil.getFrameworkLabel("kayit.silindi"), FacesMessage.SEVERITY_INFO);
				return;
			} catch (DBException e) {
				createGenericMessage(e.getUserMessage(), FacesMessage.SEVERITY_ERROR);
				return;
			}
		}
	}

	/************************************** TREE VIEW ************************************/
	public int getExistingCount() {
		return getDBOperator().recordCount(getModelName());
	}

	@Override
	public synchronized void save() {
		DBOperator dbOperator = getDBOperator();
		BaseTreeEntity treeEntity = (BaseTreeEntity) getEntity();

		if (treeEntity.getUstRef() == null && treeEntity.getRID() == null) {
			if (getExistingCount() > 0) {
				createGenericMessage(KeyUtil.getMessageValue("ustkayit.zorunlu"), FacesMessage.SEVERITY_ERROR);
				return;
			}
		} else if (treeEntity.getUstRef() == null && getPreviousUstRef() != null) {
			treeEntity.setUstRef(getPreviousUstRef());
		}

		if (treeEntity.getRID() == null) {
			try {
				treeEntity.setErisimKodu("-");
				dbOperator.insert(treeEntity);
				if (treeEntity.getUstRef() != null) {
					treeEntity.setErisimKodu(treeEntity.getUstRef().getErisimKodu() + "." + treeEntity.getRID());
				} else {
					treeEntity.setErisimKodu(treeEntity.getRID() + "");
				}
				dbOperator.update(treeEntity);
				createGenericMessage(KeyUtil.getFrameworkLabel("kayit.eklendi"), FacesMessage.SEVERITY_INFO);
			} catch (DBException e) {
				treeEntity.setRID(null);
				createGenericMessage(e.getUserMessage(), FacesMessage.SEVERITY_ERROR);
				return;
			}
		} else {
			try {
				String oncekiErisimKodu = "";
				if (treeEntity.getUstRef() != null && getPreviousUstRef() != null) {
					if (getPreviousUstRef().getRID().longValue() != treeEntity.getUstRef().getRID().longValue()) {
						oncekiErisimKodu = treeEntity.getErisimKodu();
					}
				}

				if (treeEntity.getUstRef() != null) {
					treeEntity.setErisimKodu(treeEntity.getUstRef().getErisimKodu() + "." + treeEntity.getRID());
				} else {
					treeEntity.setErisimKodu(treeEntity.getRID() + "");
				}
				if (treeEntity.getValue().hashCode() == getOldValue().hashCode()) {
					createGenericMessage(KeyUtil.getFrameworkLabel("islem.degisiklikYok"), FacesMessage.SEVERITY_INFO);
					return;
				}
				dbOperator.update(treeEntity);
				if (!isEmpty(oncekiErisimKodu)) {
					getDBOperator().executeQuery(
							"UPDATE " + getModelName() + " o SET o.erisimKodu = replace(o.erisimKodu, '" + oncekiErisimKodu + "', '" + treeEntity.getErisimKodu() + "')  "
									+ " WHERE o.erisimKodu LIKE '" + oncekiErisimKodu + "%' ");
				}

				createGenericMessage(KeyUtil.getFrameworkLabel("kayit.guncellendi"), FacesMessage.SEVERITY_INFO);
			} catch (DBException e) {
				System.out.println("ERROR @Save Method @BaseTreeController : " + e.getMessage());
				createGenericMessage(e.getUserMessage(), FacesMessage.SEVERITY_ERROR);
				return;
			}
		}
		insert();
		cancel();
		queryAction();
		return;
	}

	@Override
	public void setSelectedRID(String selectedRID) {
		this.selectedRID = selectedRID;
		setEntity((BaseTreeEntity) getDBOperator().find(getModelName(), "rID", selectedRID).get(0));
	}

	private BaseTreeEntity previousUstRef;

	public BaseTreeEntity getPreviousUstRef() {
		return previousUstRef;
	}

	public void setPreviousUstRef(BaseTreeEntity previousUstRef) {
		this.previousUstRef = previousUstRef;
	}

	@Override
	public void update() {
		super.update();
		setPreviousUstRef(((BaseTreeEntity) getEntity()).getUstRef());
	}

	public void setMasterEntityStaticWhereCondition(String masterEntityStaticWhereCondition) {
		this.masterEntityStaticWhereCondition = masterEntityStaticWhereCondition;
	}

	public String getMasterEntityStaticWhereCondition() {
		return masterEntityStaticWhereCondition;
	}

}
