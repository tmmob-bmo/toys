package tr.com.arf.framework.view.controller.form._base;

import javax.faces.application.FacesMessage;

import tr.com.arf.framework.utility.exception.data.DBException;
import tr.com.arf.framework.utility.tool.KeyUtil;
import tr.com.arf.framework.view.controller._common.SessionUserBase;

public abstract class BaseFilteredSecuredController extends BaseFilteredController {

	@SuppressWarnings("rawtypes")
	protected BaseFilteredSecuredController(Class c) {
		super(c);
	}

	private static final long serialVersionUID = 1L;
	private SessionUserBase sessionUser;

	@Override
	public boolean isListRendered() {
		if (sessionUser.isSuperUser()) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public boolean isUpdateRecordRendered() {
		if (sessionUser.isSuperUser()) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public boolean isDeleteRendered() {
		if (sessionUser.isSuperUser()) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public void delete() {
		if (!setSelected()) {
			return;
		}

		if (getEntity().getRID().longValue() == 1) {
			createGenericMessage(KeyUtil.getFrameworkLabel("kayit.silinemez"), FacesMessage.SEVERITY_INFO);
			return;
		}
		try {
			getDBOperator().delete(super.getEntity());
			queryAction();
			setSelection(null);
			setTable(null);
			createGenericMessage(KeyUtil.getFrameworkLabel("kayit.silindi"), FacesMessage.SEVERITY_INFO);
			return;
		} catch (DBException e) {
			createGenericMessage(e.getUserMessage(), FacesMessage.SEVERITY_ERROR);
			return;
		}
	}
}
