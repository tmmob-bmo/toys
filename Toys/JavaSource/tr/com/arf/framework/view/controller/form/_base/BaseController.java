package tr.com.arf.framework.view.controller.form._base;

import java.awt.event.ActionEvent;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.math.MathContext;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.model.SelectItem;

import org.apache.log4j.Logger;
import org.primefaces.component.datatable.DataTable;
import org.primefaces.component.selectonemenu.SelectOneMenu;
import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.ToggleEvent;
import org.primefaces.model.DualListModel;
import org.primefaces.model.UploadedFile;
import org.primefaces.model.Visibility;

import tr.com.arf.framework.db._management.DBOperator;
import tr.com.arf.framework.db.enumerated._common.EvetHayir;
import tr.com.arf.framework.db.enumerated._common.IslemTuru;
import tr.com.arf.framework.db.model._base.BaseEntity;
import tr.com.arf.framework.db.model.sistem.Islemlog;
import tr.com.arf.framework.utility.application.ApplicationConstantBase;
import tr.com.arf.framework.utility.application.ApplicationDescriptorBase;
import tr.com.arf.framework.utility.exception.data.DBException;
import tr.com.arf.framework.utility.tool.DateUtil;
import tr.com.arf.framework.utility.tool.KeyUtil;
import tr.com.arf.framework.utility.tool.RandomObjectGenerator;
import tr.com.arf.framework.view.controller._common.SessionUserBase;

public abstract class BaseController implements Serializable {

	private static final long serialVersionUID = 3483304400136924177L;

	private BaseEntity[] selection;

	private boolean addScreen;
	private BaseEntity entity;
	private String orderField;
	private DataTable table;
	private DataTable logTable;
	private javax.faces.component.html.HtmlDataTable treeTable;

	private boolean defaultValues;

	private String oldValue;

	protected String selectedRID = null;

	private List<BaseEntity> filteredList;
	private boolean editPanelRendered;

	protected String[] autoCompleteSearchColumns;
	private String modelName;
	private String className;

	private int currentPage;
	private int listSize = 10;
	private int recordCount;

	private String selectedTabName;
	private String listMessage = "";

	private String staticWhereCondition = "";

	/* Data Operator */
	private DBOperator dBOperator;
	private List<BaseEntity> list;

	/* LOG */
	protected boolean loggable;

	/* Math Context For BigDecimals */
	protected MathContext mc = MathContext.DECIMAL32;

	/* Log4j Logger */
	protected static Logger logger = Logger.getLogger("commonController");

	/*
	 * File Upload listener kullanilmasi gibi durumlarda edit (ekleme/guncelleme) 2 adimda yapilir. Edit step bu amacla kullanilir.
	 */
	private int editStep = 1;

	protected BaseController(@SuppressWarnings("rawtypes") Class c) {
		setOrderField("");
		setOldValue("");
		setModelName(c.getSimpleName());
		setClassName(c.getName());
		setDefaultValues(false);
		setEditPanelRendered(false);
		setCurrentPage(1);
		setRecordCount(0);
		cancel();
		setLoggable(false);
		setLogTable(null);
		setListMessage("");
		setListSize(ApplicationConstantBase._LIST_SIZE);
		this.collapsed = false;
	}

	@PostConstruct
	public void init() {
		if (getObjectFromSessionFilter(getModelName()) != null) {
			setEntity((BaseEntity) getObjectFromSessionFilter(getModelName()));
		}
	}

	public SessionUserBase getSessionUser() {
		Map<String, Object> sessionMap = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
		return (SessionUserBase) sessionMap.get("sessionUser");
	}

	public List<BaseEntity> getList() {
		return list;
	}

	public void setList(List<BaseEntity> list) {
		this.list = list;
	}

	public int getEditStep() {
		return editStep;
	}

	public void setEditStep(int editStep) {
		this.editStep = editStep;
	}

	public String getModelName() {
		return modelName;
	}

	public void setModelName(String modelName) {
		this.modelName = modelName;
	}

	public boolean isListRendered() {
		return true;
	}

	public boolean listRenderedFor(String modelName) {
		return true;
	}

	public boolean isUpdateRecordRendered() {
		return true;
	}

	public boolean updateRecordRenderedFor(String modelName) {
		return true;
	}

	public boolean isDeleteRendered() {
		return true;
	}

	public boolean deleteRenderedFor(String modelName) {
		return true;
	}

	/* Data Operator */
	protected final DBOperator getDBOperator() {
		if (dBOperator == null) {
			dBOperator = DBOperator.getInstance();
		}
		return dBOperator;
	}

	@SuppressWarnings("unchecked")
	public void queryAction() {
		// List<BaseEntity> itemList = new ArrayList<BaseEntity>();
		setRecordCount(getDBOperator().recordCountAll(getModelName()).intValue());
		setList(getDBOperator().loadAll(getModelName(), getRecordInterval(), getOrderField()));
		setListMessage(KeyUtil.getFrameworkLabel("list.toplam") + " " + getRecordCount() + " " + KeyUtil.getFrameworkLabel("kayit.bulundu"));
		// setList(itemList);
		setEditStep(1);
	}

	public DataTable getTable() {
		return table;
	}

	public void setTable(DataTable table) {
		this.table = table;
	}

	public DataTable getLogTable() {
		return logTable;
	}

	public void setLogTable(DataTable logTable) {
		this.logTable = logTable;
	}

	public javax.faces.component.html.HtmlDataTable getTreeTable() {
		return treeTable;
	}

	public void setTreeTable(javax.faces.component.html.HtmlDataTable treeTable) {
		this.treeTable = treeTable;
	}

	public boolean isAddScreen() {
		return addScreen;
	}

	public void setAddScreen(boolean addScreen) {
		this.addScreen = addScreen;
	}

	public String getOldValue() {
		return oldValue;
	}

	public void setOldValue(String oldValue) {
		this.oldValue = oldValue;
	}

	public BaseEntity[] getSelection() {
		return selection;
	}

	public void setSelection(BaseEntity[] selection) {
		this.selection = selection;
	}

	public BaseEntity getEntity() {
		return entity;
	}

	public void setEntity(BaseEntity entity) {
		this.entity = entity;
	}

	protected String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public String getOrderField() {
		return orderField;
	}

	public void setOrderField(String orderField) {
		this.orderField = orderField;
	}

	protected boolean isDefaultValues() {
		return defaultValues;
	}

	protected void setDefaultValues(boolean defaultValues) {
		this.defaultValues = defaultValues;
	}

	public String getListOutcome() {
		return getModelName().toLowerCase(Locale.ENGLISH) + ".do";
	}

	public String getEditOutcome() {
		String result = getModelName().toLowerCase(Locale.ENGLISH) + ApplicationConstantBase._EDIT_SUFFIX + ".do";
		return result;
	}

	protected String getExceptionOutcome() {
		return ApplicationDescriptorBase._EXCEPTION_OUTCOME;
	}

	@SuppressWarnings("unchecked")
	public SelectItem[] getSelectItemList() {
		List<BaseEntity> entityList = getDBOperator().load(getModelName(), getStaticWhereCondition(), "o.rID");
		if (entityList != null) {
			SelectItem[] selectItemList = new SelectItem[entityList.size() + 1];
			selectItemList[0] = new SelectItem(null, "");
			int i = 1;
			for (BaseEntity entity : entityList) {
				selectItemList[i++] = new SelectItem(entity, entity.getUIString());
			}
			return selectItemList;
		}
		return new SelectItem[0];
	}

	public SelectItem[] selectItemListForWithoutNullOption(String modelNameForSelectItems) {
		String staticWhereCon = getStaticWhereCondition();
		if (!modelNameForSelectItems.equalsIgnoreCase(getModelName())) {
			staticWhereCon = getStaticWhereCondition(modelNameForSelectItems);
		}
		return createSelectItems(modelNameForSelectItems, staticWhereCon, false);
	}

	public SelectItem[] selectItemListFor(String modelNameForSelectItems) {
		String staticWhereCon = getStaticWhereCondition();
		if (!modelNameForSelectItems.equalsIgnoreCase(getModelName())) {
			staticWhereCon = getStaticWhereCondition(modelNameForSelectItems);
		}
		return createSelectItems(modelNameForSelectItems, staticWhereCon, true);
	}

	public SelectItem[] selectOrderedItemListFor(String modelNameForSelectItems, String orderField) {
		String staticWhereCon = getStaticWhereCondition();
		if (!modelNameForSelectItems.equalsIgnoreCase(getModelName())) {
			staticWhereCon = getStaticWhereCondition(modelNameForSelectItems);
		}
		return createSelectItems(modelNameForSelectItems, staticWhereCon, true, orderField);
	}

	public SelectItem[] selectOrderedItemListWithoutNullOptionFor(String modelNameForSelectItems, String orderField) {
		String staticWhereCon = getStaticWhereCondition();
		if (!modelNameForSelectItems.equalsIgnoreCase(getModelName())) {
			staticWhereCon = getStaticWhereCondition(modelNameForSelectItems);
		}
		return createSelectItems(modelNameForSelectItems, staticWhereCon, false, orderField);
	}

	public SelectItem[] selectItemListFor(String modelNameForSelectItems, String whereCondition) {
		return createSelectItems(modelNameForSelectItems, whereCondition, true);
	}

	public SelectItem[] selectItemListForWithoutNullOption(String modelNameForSelectItems, String whereCondition) {
		return createSelectItems(modelNameForSelectItems, whereCondition, false);
	}

	public SelectItem[] createSelectItems(String modelNameForSelectItems, String whereCondition, boolean nullOption) {
		return createSelectItems(modelNameForSelectItems, whereCondition, nullOption, "");
	}

	public SelectItem[] createSelectItems(String modelNameForSelectItems, String whereCondition, boolean nullOption, String orderField) {
		if (isEmpty(orderField)) {
			orderField = "o.rID";
		}
		@SuppressWarnings("unchecked")
		List<BaseEntity> entityList = getDBOperator().load(modelNameForSelectItems, whereCondition, orderField);
		if (entityList != null) {
			int i = 0;
			SelectItem[] selectItemList = null;
			if (nullOption == true) {
				selectItemList = new SelectItem[entityList.size() + 1];
				selectItemList[0] = new SelectItem(null, "");
				i = 1;
			} else {
				selectItemList = new SelectItem[entityList.size()];
			}
			if (entityList.size() > 100) {
				System.out.println("SELECT ITEMS created FOR " + modelNameForSelectItems + ", " + entityList.size() + " RECORDS FOUND!!! ");
			}
			for (BaseEntity entity : entityList) {
				selectItemList[i++] = new SelectItem(entity, entity.getUIString());
			}
			return selectItemList;
		}
		return new SelectItem[0];
	}

	public BaseEntity getFirst() {
		return list.get(0);
	}

	protected boolean setSelected() {
		if (getEntity() != null) {
			return true;
		} else {
			try {
				entity = getFirst();
			} catch (Exception e) {
				entity = null;
				return false;
			}
		}
		if (getTable().getSelection() != null) {
			setEntity((BaseEntity) getTable().getSelection());
			return true;
		} else {
			return false;
		}
	}

	@SuppressWarnings("rawtypes")
	public void insertOnly() {
		Class entityClass;
		try {
			entityClass = Class.forName(getClassName());
			entity = (BaseEntity) entityClass.newInstance();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
		entity.initValues(isDefaultValues());
		setTable(null);
		setEditStep(1);
		setOldValue("");
	}

	public void insert() {
		insertOnly();
		setEditPanelRendered(true);
		setAddScreen(true);
		setActiveTab("edit");
	}

	public void update() {
		if (!setSelected()) {
			return;
		}
		setTable(null);
		setOldValue(entity.getValue());
		setAddScreen(false);
		setEditPanelRendered(true);
		setActiveTab("edit");
		setEditStep(1);
	}

	public void forwardEdit() {
		setEditStep(getEditStep() + 1);
	}

	public void delete() {
		if (!setSelected()) {
			return;
		}
		deleteGivenObject(entity, this.loggable);
	}

	public void delete(BaseEntity objectToBeDeleted, boolean doLog) {
		deleteGivenObject(objectToBeDeleted, doLog);
	}

	public void justDelete() {
		if (!setSelected()) {
			return;
		}
		justDeleteGivenObject(entity, this.loggable);
	}

	public void justDelete(BaseEntity objectToBeDeleted, boolean doLog) {
		justDeleteGivenObject(objectToBeDeleted, doLog);
	}

	public void deleteGivenObject(BaseEntity objectToBeDeleted, boolean doLog) {
		setTable(null);
		setEditPanelRendered(false);
		BaseEntity obj = null;
		Long rid = 0L;
		try {
			if (doLog) {
				obj = (BaseEntity) objectToBeDeleted.cloneObject();
				rid = objectToBeDeleted.getRID();
			}
			if (getDBOperator().delete(objectToBeDeleted)) {
				if (doLog) {
					obj.setRID(rid);
					logKaydet(obj, IslemTuru._SILME, "");
				}
				queryAction();
				createGenericMessage(KeyUtil.getFrameworkLabel("kayit.silindi"), FacesMessage.SEVERITY_INFO);
			}
		} catch (DBException e) {
			createGenericMessage(e.getUserMessage(), FacesMessage.SEVERITY_ERROR);
		}
	}

	public void justDeleteGivenObject(BaseEntity objectToBeDeleted, boolean doLog) {
		setTable(null);
		setEditPanelRendered(false);
		setEditPanelRendered(false);
		BaseEntity obj = null;
		Long rid = 0L;
		try {
			if (doLog) {
				obj = (BaseEntity) objectToBeDeleted.cloneObject();
				rid = objectToBeDeleted.getRID();
			}
			if (getDBOperator().delete(objectToBeDeleted)) {
				if (doLog) {
					obj.setRID(rid);
					logKaydet(obj, IslemTuru._SILME, "");
				}
				createGenericMessage(KeyUtil.getFrameworkLabel("kayit.silindi"), FacesMessage.SEVERITY_INFO);
			}
		} catch (DBException e) {
			createGenericMessage(e.getUserMessage(), FacesMessage.SEVERITY_ERROR);
		}
	}

	public void save() {
		if (entity.getRID() != null) {
			if (entity.getValue().hashCode() == getOldValue().hashCode()) {
				createGenericMessage(KeyUtil.getMessageValue("islem.degisiklikYok"), FacesMessage.SEVERITY_WARN);
				return;
			}
		}
		saveGivenEntity(entity, loggable, getOldValue());
		queryAction();
		setActiveTab("list");
		insertOnly();
	}

	public void save(BaseEntity objectToBeSaved, boolean doLog, String oldVal) {
		if (objectToBeSaved.getRID() != null) {
			if (objectToBeSaved.getValue().hashCode() == oldVal.hashCode()) {
				createGenericMessage(KeyUtil.getMessageValue("islem.degisiklikYok"), FacesMessage.SEVERITY_WARN);
				return;
			}
		}
		saveGivenEntity(objectToBeSaved, doLog, oldVal);
		queryAction();
		setActiveTab("list");
		insertOnly();
	}

	public void justSave() {
		if (entity.getRID() != null) {
			if (entity.getValue().hashCode() == getOldValue().hashCode()) {
				createGenericMessage(KeyUtil.getMessageValue("islem.degisiklikYok"), FacesMessage.SEVERITY_WARN);
				return;
			}
		}
		saveGivenEntity(entity, this.loggable, getOldValue());
	}

	public void justSave(BaseEntity objectToBeSaved, boolean doLog, String oldVal) {
		if (objectToBeSaved.getRID() != null) {
			if (objectToBeSaved.getValue().hashCode() == oldVal.hashCode()) {
				createGenericMessage(KeyUtil.getMessageValue("islem.degisiklikYok"), FacesMessage.SEVERITY_WARN);
				return;
			}
		}
		saveGivenEntity(objectToBeSaved, doLog, oldVal);
	}

	private void saveGivenEntity(BaseEntity objectToBeSaved, boolean doLog, String oldVal) {
		setTable(null);
		setEditPanelRendered(false);
		if (objectToBeSaved.getRID() == null) {
			try {
				getDBOperator().insert(objectToBeSaved);
				if (doLog) {
					logKaydet(objectToBeSaved, IslemTuru._EKLEME, "");
				}
				setEditStep(1);
				createGenericMessage(KeyUtil.getFrameworkLabel("kayit.eklendi"), FacesMessage.SEVERITY_INFO);
			} catch (DBException e) {
				objectToBeSaved.setRID(null);
				createGenericMessage(e.getUserMessage(), FacesMessage.SEVERITY_ERROR);
			}
		} else {
			try {
				if (objectToBeSaved.getValue().hashCode() == oldVal.hashCode()) {
					createGenericMessage(KeyUtil.getFrameworkLabel("islem.degisiklikYok"), FacesMessage.SEVERITY_INFO);
					return;
				}
				getDBOperator().update(objectToBeSaved);
				if (doLog) {
					logKaydet(objectToBeSaved, IslemTuru._GUNCELLEME, getOldValue());
				}
				setEditStep(1);
				createGenericMessage(KeyUtil.getFrameworkLabel("kayit.guncellendi"), FacesMessage.SEVERITY_INFO);
			} catch (DBException e) {
				createGenericMessage(e.getUserMessage(), FacesMessage.SEVERITY_ERROR);
			}
		}
	}

	public String post() {
		save();
		return getListOutcome();
	}

	public void browse() {
		cancel();
	}

	public String listPage() {
		browse();
		// System.out.println("getListOutcome : " + getListOutcome()) ;
		return getListOutcome();
	}

	public void cancel() {
		setTable(null);
		setEditPanelRendered(false);
		setCollapsed(false);
		setEditStep(1);
		setActiveTab("list");
	}

	public void createGenericMessage(String messageValue, FacesMessage.Severity messageSeverity) {
		FacesMessage message = new FacesMessage();
		FacesContext context = FacesContext.getCurrentInstance();
		message.setSeverity(messageSeverity);
		message.setDetail(messageValue);
		message.setSummary(messageValue);
		context.addMessage(null, message);
	}

	public void createCustomMessage(String messageValue, FacesMessage.Severity messageSeverity, String componentId) {
		FacesMessage message = new FacesMessage();
		FacesContext context = FacesContext.getCurrentInstance();
		message.setSeverity(messageSeverity);
		message.setDetail(messageValue);
		message.setSummary(messageValue);
		context.addMessage(componentId, message);
	}

	public void excelExportOld(ActionEvent actionEvent) {
		String url = "/ReportServlet?raporAdi=" + getModelName();
		FacesContext facesContext = FacesContext.getCurrentInstance();
		try {
			facesContext.getExternalContext().dispatch(url);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			facesContext.responseComplete();
		}
	}

	public String getSelectedRID() {
		return selectedRID;
	}

	public void tableRowCancelSelection() {
		setEditPanelRendered(false);
		setEntity(null);
		setSelection(null);
	}

	public List<BaseEntity> getFilteredList() {
		setEditPanelRendered(false);
		return filteredList;
	}

	public void setFilteredList(List<BaseEntity> filteredList) {
		this.filteredList = filteredList;
	}

	public boolean isEditPanelRendered() {
		return editPanelRendered;
	}

	public void setEditPanelRendered(boolean editPanelRendered) {
		this.editPanelRendered = editPanelRendered;
	}

	@SuppressWarnings("rawtypes")
	protected static boolean isEmpty(Collection col) {
		return col == null || col.size() == 0;
	}

	protected static boolean isEmpty(Object[] arr) {
		return arr == null || arr.length == 0 || arr[0] == null;
	}

	protected boolean isEmpty(String str) {
		return str == null || str.trim().length() == 0;
	}

	public String[] getAutoCompleteSearchColumns() {
		return autoCompleteSearchColumns;
	}

	public void setAutoCompleteSearchColumns(String[] autoCompleteSearchColumns) {
		this.autoCompleteSearchColumns = autoCompleteSearchColumns;
	}

	@SuppressWarnings("rawtypes")
	public List completeMethod(String value) {
		return completeMethod(value, "");
	}

	@SuppressWarnings("rawtypes")
	public List completeMethod(String value, String parametricWhereCondition) {
		String modelNameForAutoComplete = (String) UIComponent.getCurrentComponent(FacesContext.getCurrentInstance()).getAttributes().get("modelNameForAutoComplete");
		return completeMethod(modelNameForAutoComplete, value, parametricWhereCondition);
	}

	@SuppressWarnings("rawtypes")
	public List completeMethod(String modelNameForAutoComplete, String value, String parametricWhereCondition) {
		String staticWhereCondition = getStaticWhereCondition(modelNameForAutoComplete);
		String[] values = new String[getAutoCompleteSearchColumns(modelNameForAutoComplete).length];
		for (int x = 0; x < getAutoCompleteSearchColumns(modelNameForAutoComplete).length; x++) {
			values[x] = value;
		}
		// System.out.println("staticWhereCondition :" + staticWhereCondition);
		if (!isEmpty(parametricWhereCondition)) {
			if (!isEmpty(staticWhereCondition)) {
				staticWhereCondition += " AND " + parametricWhereCondition;
			} else {
				staticWhereCondition += parametricWhereCondition;
			}
		}
		List entityList = getDBOperator().loadWithConditions(modelNameForAutoComplete, getAutoCompleteSearchColumns(modelNameForAutoComplete), values, ApplicationConstantBase._MAX_RECORD_COUNT, staticWhereCondition);
		if (entityList == null || entityList.size() == 0) {
			createGenericMessage(KeyUtil.getMessageValue("kayit.bulunamadi"), FacesMessage.SEVERITY_ERROR);
			RequestContext context = RequestContext.getCurrentInstance();

			String globalPanel = getSessionUser().getGlobalMessagePanelId();
			context.update(globalPanel);
		}
		return entityList;
	}

	public int getCurrentPage() {
		return currentPage;
	}

	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}

	public int getListSize() {
		return listSize;
	}

	public void setListSize(int listSize) {
		this.listSize = listSize;
	}

	public int getRecordCount() {
		return recordCount;
	}

	public void setRecordCount(int recordCount) {
		this.recordCount = recordCount;
	}

	public void changePage(String process) {
		if (process.equalsIgnoreCase("first")) {
			setCurrentPage(1);
		} else if (process.equalsIgnoreCase("previous")) {
			if (getCurrentPage() > 1) {
				setCurrentPage(getCurrentPage() - 1);
			}
		} else if (process.equalsIgnoreCase("next")) {
			if (getCurrentPage() < ((Integer) getPagingIntervals()[getPagingIntervals().length - 1].getValue()).intValue()) {
				setCurrentPage(getCurrentPage() + 1);
			}
		} else if (process.equalsIgnoreCase("last")) {
			setCurrentPage(((Integer) getPagingIntervals()[getPagingIntervals().length - 1].getValue()).intValue());
		}
		queryAction();
	}

	public int[] getRecordInterval() {
		int[] recordIntervals = new int[2];
		recordIntervals[0] = (getCurrentPage() - 1) * getListSize();
		recordIntervals[1] = getListSize();
		return recordIntervals;
	}

	public SelectItem[] getPagingIntervals() {
		int remainder = getRecordCount() % getListSize();
		int maxPage = getRecordCount() / getListSize();
		if (remainder > 0) {
			++maxPage;
		}
		if (maxPage == 0) {
			maxPage = 1;
		}
		SelectItem[] selectItemList = new SelectItem[maxPage];
		for (int x = 1; x <= maxPage; x++) {
			selectItemList[x - 1] = new SelectItem(x, x + "");
		}
		return selectItemList;
	}

	public void handlePageChange(AjaxBehaviorEvent event) {
		SelectOneMenu selectOneMenuObject = (SelectOneMenu) event.getSource();
		if (selectOneMenuObject.getValue() != null) {
			setCurrentPage((Integer) selectOneMenuObject.getValue());
		}
		queryAction();
		FacesContext.getCurrentInstance().renderResponse();
	}

	public String getSelectedTabName() {
		return selectedTabName;
	}

	public void setSelectedTabName(String selectedTabName) {
		this.selectedTabName = selectedTabName;
	}

	public String getListMessage() {
		String oldListMessage = listMessage;
		// setListMessage("");
		return oldListMessage;
	}

	public String getListMessageWithoutClearing() {
		return listMessage;
	}

	public void setListMessage(String listMessage) {
		this.listMessage = listMessage;
	}

	@SuppressWarnings("unused")
	public void handleChange(AjaxBehaviorEvent event) {
		try {
			// System.out.println("Common Framework handleChange calisti...");
			HtmlSelectOneMenu menu = (HtmlSelectOneMenu) event.getComponent();

		} catch (Exception e) {
			System.out.println("Error @handleChange :" + e.getMessage());
		}
	}

	/* Bir item secilmisse true, secilmemisse false doner... */
	public boolean isDetailRendered() {
		if (getEntity() == null) {
			return false;
		} else if (getEntity().getRID() == null) {
			return false;
		} else {
			return true;
		}
	}

	// File Upload Listener
	public void fileUploadListener(FileUploadEvent event) throws Exception {
		if (!isEmpty(getEntity().getPath())) {
			// Dosya daha once upload edilmis. Daha once uplaod edilmis olan dosya silinecek.
			try {
				File file = new File(getEntity().getPath());
				if (!file.delete()) {
					logger.error(getEntity().getPath() + " could not be deleted!");
				}
			} catch (Exception e) {
				logger.error("ERROR deleting file @fileUploadListener : " + e.getMessage());
			}
		}
		UploadedFile item = event.getFile();
		File creationFile = createFile(item);
		getEntity().setPath(creationFile.getAbsolutePath());
	}

	/**
	 * Dosya upload yaptıktan sonra save metodunu cagiran metod
	 *
	 * @param event
	 * @throws IOException
	 */
	public void fileUploadAndSaveListener(FileUploadEvent event) throws IOException {
		if (!isEmpty(getEntity().getPath())) {
			// Dosya daha once upload edilmis. Daha once uplaod edilmis olan dosya silinecek.
			try {
				File file = new File(getEntity().getPath());
				if (!file.delete()) {
					logger.error(getEntity().getPath() + " could not be deleted!");
				}
			} catch (Exception e) {
				logger.error("ERROR deleting file @fileUploadAndSaveListener : " + e.getMessage());
			}
		}
		UploadedFile item = event.getFile();
		File creationFile = createFile(item);
		getEntity().setPath(creationFile.getAbsolutePath());
		save();
	}

	protected File createFile(UploadedFile item, String modelName) throws IOException {
		return createFileWithGivenParams(item, modelName);
	}

	protected File createFile(UploadedFile item) throws IOException {
		return createFileWithGivenParams(item, getModelName());
	}

	public void setSelectedRID(String selectedRID) {
		this.selectedRID = selectedRID;
		setEntity((BaseEntity) getDBOperator().find(getModelName(), "rID", selectedRID).get(0));
	}

	protected File createFileWithGivenParams(UploadedFile item, String modelNameForUpload) throws IOException {
		String typePath = modelNameForUpload.toLowerCase(Locale.ENGLISH);
		Date now = new Date();
		String filePrefix = RandomObjectGenerator.generateRandomString(6, true);
		String fileSuffix = new SimpleDateFormat("yyyyMMdd'T'HHmmssSS").format(now);
		String fileName = item.getFileName();
		int index1 = fileName.indexOf(".");
		fileName = filePrefix + "_" + fileSuffix + "." + fileName.substring(index1 + 1);

		// Isletim sistemine gore path kontrol ediliyor ve gerekli klasorler
		// -yoksa- olusturuluyor!
		String filePath = getSessionUser().getSystemPath();
		File file = new File(filePath + typePath);
		if (!file.exists()) {
			new File(filePath + typePath).mkdir();
		}
		File creationFile = new File(filePath + typePath + "/", fileName);
		if (!creationFile.exists()) {
			FileOutputStream fileOutStream;
			fileOutStream = new FileOutputStream(creationFile);
			fileOutStream.write(item.getContents());
			fileOutStream.flush();
			fileOutStream.close();
		}
		return creationFile;
	}

	public String getNoImagePath() {
		String os = System.getProperty("os.name");
		String filePath = ApplicationConstantBase._linuxNoImagePath;
		if (os.matches("(?i).*windows.*")) {
			filePath = ApplicationConstantBase._windowsNoImagePath;
		}
		return filePath;
	}

	public String calculateHeightForList(int size, int max) {
		int totalSize = size * 20 + 80;
		if (max > 0) {
			if (totalSize > max) {
				totalSize = max;
			}
		}
		return totalSize + " px";

	}

	/* Generic File Delete */
	public void deleteRec(Object secilenKayit) throws DBException {
		try {
			getDBOperator().delete((BaseEntity) secilenKayit);
			createGenericMessage(KeyUtil.getFrameworkLabel("kayit.silindi"), FacesMessage.SEVERITY_INFO);
			queryAction();
		} catch (DBException e) {
			System.out.println("Error :" + e.getMessage());
			createGenericMessage(e.getUserMessage(), FacesMessage.SEVERITY_ERROR);
		}
	}

	public void completeSelection() {
		setEditPanelRendered(true);
		setActiveTab("edit");
	}

	public boolean isLoggable() {
		return loggable;
	}

	public void setLoggable(boolean loggable) {
		this.loggable = loggable;
	}

	private boolean collapsed;

	public void onToggle(ToggleEvent event) {
		Visibility visibility = event.getVisibility();
		this.collapsed = visibility == Visibility.HIDDEN;
	}

	public boolean isCollapsed() {
		return collapsed;
	}

	public void setCollapsed(boolean collapsed) {
		this.collapsed = collapsed;
	}

	protected boolean isNumber(String s) {
		if (s == null || s.trim().length() == 0) {
			return false;
		}
		try {
			Integer.parseInt(s);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public static void deleteFile(String fileName) {
		// A File object to represent the filename
		File f = new File(fileName);
		// Make sure the file or directory exists and isn't write protected
		if (!f.exists()) {
			logger.error("Delete: no such file or directory: " + fileName);
		}
		if (!f.canWrite()) {
			logger.error("Delete: write protected: " + fileName);
		}
		// If it is a directory, make sure it is empty
		if (f.isDirectory()) {
			String[] files = f.list();
			if (files.length > 0) {
				logger.error("Delete: directory not empty: " + fileName);
			}
		}
		// Attempt to delete it
		try {
			f.delete();
		} catch (Exception e) {
			logger.error("Delete: deletion failed" + e.getMessage());
		}
	}

	public abstract void putObjectToSessionFilter(String objectName, Object object);

	public abstract Object getObjectFromSessionFilter(String objectName);

	public abstract void removeObjectFromSessionFilter(String objectName);

	protected void logKaydet(BaseEntity entity, IslemTuru islemTuru, String oldVal) throws DBException {
		// StringUtil st = new StringUtil();
		Islemlog islemLog = new Islemlog();
		islemLog.setIslemtarihi(new Date());
		islemLog.setIslemturu(islemTuru);
		islemLog.setReferansRID(entity.getRID());
		islemLog.setTabloadi(entity.getClass().getSimpleName());
		if (islemTuru != IslemTuru._SILME) {
			islemLog.setEskideger(oldVal);
			islemLog.setYenideger(entity.getValue());
			// islemLog.setYenideger(st.findDifferencesBetweenTwoStrings(oldVal,
			// entity.getValue()));
		} else {
			islemLog.setEskideger(entity.getValue());
			islemLog.setYenideger("");
		}
		Map<String, Object> sessionMap = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
		islemLog.setKullaniciRid(((SessionUserBase) sessionMap.get("sessionUser")).getKullaniciRid());
		islemLog.setKullaniciText(((SessionUserBase) sessionMap.get("sessionUser")).getKullaniciText());
		getDBOperator().insert(islemLog);
		islemLog = new Islemlog();
	}

	protected Islemlog logOlustur(BaseEntity entity, IslemTuru islemTuru, String oldVal) throws DBException {
		Islemlog islemLog = new Islemlog();
		islemLog.setIslemtarihi(new Date());
		islemLog.setIslemturu(islemTuru);
		islemLog.setReferansRID(entity.getRID());
		islemLog.setTabloadi(entity.getClass().getSimpleName());
		if (islemTuru != IslemTuru._SILME) {
			islemLog.setEskideger(oldVal);
			islemLog.setYenideger(entity.getValue());
		} else {
			islemLog.setEskideger(entity.getValue());
			islemLog.setYenideger("");
		}
		Map<String, Object> sessionMap = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
		islemLog.setKullaniciRid(((SessionUserBase) sessionMap.get("sessionUser")).getKullaniciRid());
		islemLog.setKullaniciText(((SessionUserBase) sessionMap.get("sessionUser")).getKullaniciText());
		return islemLog;
	}

	public void tableRowSelect(SelectEvent event) {
		setEditPanelRendered(false);
		setEntity((BaseEntity) event.getObject());
		if (loggable) {
			if (getEntity() != null) {
				putObjectToSessionFilter("logEntity", getEntity());
			}
		}
	}

	public String detailPage(Long selectedRID) {
		setSelectedRID(selectedRID + "");
		return getModelName() + "Detay.do";
	}

	protected BaseEntity checkSessionForPreSelected() {
		return null;
	}

	/**
	 * Detay sayfasina giden method
	 */
	public String detaySayfa(String outcome) {
		if (!setSelected()) {
			return "";
		}
		// getSessionUserBase().setLegalAccess(1);
		putObjectToSessionFilter(getModelName(), getEntity());
		return outcome;
	}

	/* PICK LIST */
	private DualListModel<BaseEntity> dualList;

	public DualListModel<BaseEntity> getDualList() {
		return dualList;
	}

	public void setDualList(DualListModel<BaseEntity> dualList) {
		this.dualList = dualList;
	}

	@SuppressWarnings("unchecked")
	public void fillDualListFor(String modelName, String queryCriteria, String orderField) {
		String whereCon = queryCriteria;
		if (!isEmpty(getStaticWhereCondition(modelName))) {
			whereCon += " AND " + getStaticWhereCondition(modelName);
		}
		List<BaseEntity> source = getDBOperator().load(modelName, whereCon, orderField);
		List<BaseEntity> target = new ArrayList<BaseEntity>();
		setDualList(new DualListModel<BaseEntity>(source, target));
	}

	@SuppressWarnings("unchecked")
	public void fillDualListFor(String modelName, String orderField) {
		String whereCon = "";
		if (!isEmpty(getStaticWhereCondition(modelName))) {
			whereCon = getStaticWhereCondition(modelName);
		}
		List<BaseEntity> source = getDBOperator().load(modelName, whereCon, orderField);
		List<BaseEntity> target = new ArrayList<BaseEntity>();
		setDualList(new DualListModel<BaseEntity>(source, target));
	}

	public String getStaticWhereCondition() {
		return staticWhereCondition;
	}

	protected abstract String getStaticWhereCondition(String modelName);

	public void setStaticWhereCondition(String staticWhereCondition) {
		this.staticWhereCondition = staticWhereCondition;
	}

	protected abstract String[] getAutoCompleteSearchColumns(String modelName);

	public String getDetailPanelDivId() {
		if (getEntity() == null || getEntity().getRID() == null) {
			return "divfull";
		} else {
			return "divleft";
		}
	}

	/* TAB EKRANLAR ICIN */
	protected String activeTab = "list";

	public String tabStyleFor(String tabName) {
		if (tabName.equalsIgnoreCase(activeTab)) {
			return "ribbonPage-tab-header sel";
		} else {
			return "ribbonPage-tab-header";
		}
	}

	public String getActiveTab() {
		return activeTab;
	}

	public void setActiveTab(String activeTab) {
		this.activeTab = activeTab;
	}

	/* WIZARD EKRANLARI ICIN */
	private int wizardStep = 1;

	public int getWizardStep() {
		return wizardStep;
	}

	public void setWizardStep(int wizardStep) {
		this.wizardStep = wizardStep;
	}

	public void wizardForward() {
		wizardStep++;
	}

	public void wizardBackward() {
		wizardStep--;
	}

	public void setWizardPage(String page) {
		setWizardStep(Integer.parseInt(page));
	}

	public String getStyleClassForWizard(String forPage) {
		Integer x = Integer.parseInt(forPage);
		if (getWizardStep() == x) {
			return "active-step";
		} else if (x < getWizardStep()) {
			return "completed-step";
		} else {
			return "";
		}
	}

	/**
	 * Varsayılan kaydı düzenleyen method.
	 *
	 * @param whereCon
	 */
	public void varsayilanDuzenle(String whereCon) throws DBException {
		if (getDBOperator().recordCount(getModelName(), whereCon + " AND o.varsayilan=" + EvetHayir._EVET.getCode()) > 0) {
			getDBOperator().executeQuery("UPDATE " + getModelName() + " o SET o.varsayilan=" + EvetHayir._HAYIR.getCode() + " WHERE " + whereCon);
		}
	}

	/**
	 * Varsayılan kaydı kontrol eden method.
	 *
	 * @param whereCon
	 */
	public void varsayilanKontrolFor(String whereCon, String modelName) throws DBException {
		if (getDBOperator().recordCount(modelName, whereCon + " AND o.varsayilan=" + EvetHayir._EVET.getCode()) > 0) {
			getDBOperator().executeQuery("UPDATE " + modelName + " o SET o.varsayilan=" + EvetHayir._HAYIR.getCode() + " WHERE " + whereCon);
		}
	}

	public void redirectPage(String url) {
		ExternalContext context2 = FacesContext.getCurrentInstance().getExternalContext();
		try {
			context2.redirect(context2.getRequestContextPath() + url);
		} catch (IOException e) {
			logger.error("Could not redirect! :" + e.getMessage());
		}
	}

	protected void logYaz(String logMessage) {
		System.out.println(logMessage);
		logger.error(logMessage);
		logger.error("Kullanıcı Adı : " + getSessionUser().getKullaniciText());
		logger.error("Kullanıcı RID : " + getSessionUser().getKullaniciRid());
		logger.error("Kullanıcı IP Adress : " + getSessionUser().getIpAddress());
		logger.error("Class : " + getModelName() + "Controller");
		logger.error("Tarih : " + DateUtil.dateToDMYHMS(new Date()));
	}

	protected void logYaz(String logMessage, Exception e) {
		logYaz(logMessage + " : " + e.getMessage());
		logger.error("", e);
	}

}