package initializers;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import tr.com.arf.framework.db._management.DBOperator;
import tr.com.arf.framework.utility.exception.data.DBException;
import tr.com.arf.toys.db.model.evrak.Dosyakodu;

public class ErisimKoduUpdater {

	public static void main(String[] args) throws DBException {
		DBOperator dbOperator = new DBOperator();
		@SuppressWarnings("unchecked")
		List<Dosyakodu> menuElemaniListesi = dbOperator.load(Dosyakodu.class.getSimpleName());
		for (Dosyakodu menuElemani : menuElemaniListesi) {
			ArrayList<Dosyakodu> ustKayitlar = new ArrayList<Dosyakodu>();
			String erisimKodu = "";
			ustKayitlar.add(menuElemani);

			Dosyakodu cloneItem = (Dosyakodu) menuElemani.cloneObject();
			cloneItem.setRID(null);

			if (cloneItem.getUstRef() != null) {
				while (cloneItem.getUstRef() != null) {
					cloneItem = cloneItem.getUstRef();
					ustKayitlar.add(cloneItem);
				}
			}
			Collections.reverse(ustKayitlar);
			for (int x = 0; x < ustKayitlar.size(); x++) {
				if (x < ustKayitlar.size() - 1) {
					erisimKodu = erisimKodu + ustKayitlar.get(x).getRID()
							+ ".";
				} else {
					erisimKodu = erisimKodu + ustKayitlar.get(x).getRID();
				}
			}
			menuElemani.setErisimKodu(erisimKodu);
			dbOperator.update(menuElemani);
		}
		System.out.println("Finished...");

	}
}
