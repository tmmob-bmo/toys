package initializers;



/**
 *
 * @author useyhan
 */
import java.io.IOException;
import java.util.Locale;
 
 
public class EnumGenerator {

    
    public static void main(String[] args) throws IOException { 
    	 
    	String value = "Elden (Vezne), Bankadan";
    	String enumName = "odemeSekli";
    	String enumNameFirstCharUp = makeFirstCharUpperCase(enumName);
    	
    	String[] result = value.split(",");
    	String[] enumValues = new String[result.length];
    	
    	StringBuilder resultToPrint = new StringBuilder("");
    	
    	int a = 0;
    	for(String s : result){
    		enumValues[a++] = s.trim();
    	}    	    	 
    	for(String s : enumValues){
    		System.out.println(enumName + "." + turkceKarakterTemizle(s.toLowerCase(Locale.ENGLISH)) + "=" + s); 
    	}
    	System.out.println("********************************************************************************************************************************** \n");
    	System.out.println("\n");
    	String as = "_NULL(0, \"\"),\n";
    	for(int x = 1; x <= enumValues.length; x++){
    		String son = ",";
    		if(x == enumValues.length) {
				son = ";";
			}
    		as += "_" + turkceKarakterTemizle(enumValues[x - 1].toUpperCase(Locale.ENGLISH)) + "(" + x + ", \"" + enumName + "." + turkceKarakterTemizle(enumValues[x - 1].toLowerCase(Locale.ENGLISH)) + "\")" + son + " \n";
    	} 
    		
		resultToPrint.append("package tr.com.arf.toys.db.enumerated.kisi; \n");
		resultToPrint.append(" \n");
		resultToPrint.append("import tr.com.arf.framework.db.enumerated._base.BaseEnumerated;\n");
		resultToPrint.append("import tr.com.arf.toys.utility.tool.KeyUtil;\n");
		resultToPrint.append("\n");
		resultToPrint.append("public enum " + makeFirstCharUpperCase(enumName) + " implements BaseEnumerated { \n");
		resultToPrint.append("\n");
		resultToPrint.append(as);
		resultToPrint.append("\n");
		resultToPrint.append("		private int code;\n");
		resultToPrint.append("		private String label;\n");
		resultToPrint.append("\n");
		resultToPrint.append("		private " + enumNameFirstCharUp	+ "(int code, String label) {\n");
		resultToPrint.append("		    this.code = code;\n");
		resultToPrint.append("		    this.label = label;\n");
		resultToPrint.append("		}\n");
		resultToPrint.append("\n");
		resultToPrint.append("		@Override\n");
		resultToPrint.append("		public int getCode() {\n");
		resultToPrint.append("		    return code;\n");
		resultToPrint.append("		}\n");
		resultToPrint.append("\n");
		resultToPrint.append("		@Override\n");
		resultToPrint.append("		public String getLabel() {\n");
		resultToPrint.append("		    return KeyUtil.getMessageValue(label);\n");
		resultToPrint.append("		}\n");
		resultToPrint.append("\n");
		resultToPrint.append("		@Override\n");
		resultToPrint.append("		public String toString() {\n");
		resultToPrint.append("		    return KeyUtil.getMessageValue(label);\n");
		resultToPrint.append("		}\n");
		resultToPrint.append("\n");
		resultToPrint.append("		public static " + enumNameFirstCharUp + " getWithCode(int code){\n");
		resultToPrint.append("			" + enumNameFirstCharUp + "[] enumArray = "	+ enumNameFirstCharUp + ".values();\n");
		resultToPrint.append("			for(" + enumNameFirstCharUp	+ " enumObj : enumArray){ \n");
		resultToPrint.append("				if(enumObj.getCode() == code) {\n");
		resultToPrint.append("					return enumObj;\n");
		resultToPrint.append("				}\n");
		resultToPrint.append("			}\n");
		resultToPrint.append("			return " + enumNameFirstCharUp + "._NULL;\n");
		resultToPrint.append("		} \n");
		resultToPrint.append("}  \n");
		resultToPrint.append("\n");
		resultToPrint.append("********************************************************************************************************************************** \n");
		resultToPrint.append("\n");

		resultToPrint.append("private SelectItem[] " + enumNameFirstCharUp	+ "Items;\n");
		resultToPrint.append("\n");
		resultToPrint.append("public SelectItem[] get" + enumNameFirstCharUp + "Items() {\n");
		resultToPrint.append("	return " + enumNameFirstCharUp + "Items;\n");
		resultToPrint.append("}\n");
		resultToPrint.append("\n");
		resultToPrint.append("public void set" + enumNameFirstCharUp + "Items() {\n");
		resultToPrint.append("	" + enumNameFirstCharUp + "Items = new SelectItem[" + enumNameFirstCharUp + ".values().length - 1];\n");
		resultToPrint.append("	for (int x = 0; x < " + enumNameFirstCharUp + ".values().length - 1; x++) {\n");
		resultToPrint.append("		" + enumNameFirstCharUp + "Items[x] = new SelectItem(\n");
		resultToPrint.append("				" + enumNameFirstCharUp	+ ".values()[x + 1],\n");
		resultToPrint.append("				" + enumNameFirstCharUp	+ ".values()[x + 1].getLabel());\n");
		resultToPrint.append("	}\n");
		resultToPrint.append("}\n");

		System.out.println(resultToPrint.toString());
	}

	public static String makeFirstCharUpperCase(String someString) {
		if (someString != null && someString.length() > 1) {
			String string1 = makeUpperCase(someString.substring(0, 1))
					+ someString.substring(1);
			return string1;
		} else {
			return "nullString";
		}
	}

	public static String makeFirstCharLowerRestUpperCase(String someString) {
		String string1 = someString.substring(0, 1).toLowerCase(Locale.ENGLISH)
				+ makeUpperCase(someString.substring(1));
		return string1;
	}

	public static String makeFirstCharUpperRestLowerCase(String someString) {
		String string1 = makeUpperCase(someString.substring(0, 1))
				+ someString.substring(1).toLowerCase(Locale.ENGLISH);
		return string1;
	}

	public static String makeFirstCharLowerCase(String someString) {
		if (someString.substring(0, 1).equals("I")
				|| someString.substring(0, 1).equals("Ý")) {
			return "i" + someString.substring(1);
		} else {
			return someString.substring(0, 1).toLowerCase(Locale.ENGLISH)
					+ someString.substring(1);
		}
	}

	private static String turkceKarakterTemizle(String ret) {
		char[] turkishChars = new char[] { 0x131, 0x130, 0xFC, 0xDC, 0xF6,
				0xD6, 0x15F, 0x15E, 0xE7, 0xC7, 0x11F, 0x11E };
		char[] englishChars = new char[] { 'i', 'I', 'u', 'U', 'o', 'O', 's',
				'S', 'c', 'C', 'g', 'G' };
		for (int i = 0; i < turkishChars.length; i++) {
			ret = ret.replaceAll(new String(new char[] { turkishChars[i] }),
					new String(new char[] { englishChars[i] }));
		}
		return ret.replace(" ", "");
	}

	public static String makeUpperCase(String someString) {
		String string1 = null;
		try {
			string1 = someString.toUpperCase(Locale.ENGLISH);
			string1 = string1.replace("İ", "I");
			string1 = string1.replace("Ö", "O");
			string1 = string1.replace("Ü", "U");
			string1 = string1.replace("Ğ", "G");
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("MakeUpperCase Hata :" + someString);
		}
		return string1;
	}
      
}