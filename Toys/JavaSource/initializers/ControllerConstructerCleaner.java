package initializers;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Locale;


public class ControllerConstructerCleaner {

	/**
	 * @param args
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException {
		Path path0 = Paths.get("D:/Development/IDE_64/eclipse_juno/Workspace/Toys/JavaSource/tr/com/arf/toys/view/controller/form/uye");
		Collection<Path> all = searchInTree(path0, new ArrayList<Path>());

		for(Path a : all){
			addContentToTextFile(a.getParent() + "\\" + a.getFileName());
		}
	}

	public static void addContentToTextFile(String fileName) {
		File f = new File(fileName);
		FileInputStream fs = null;
		InputStreamReader in = null;
		BufferedReader br = null;
		StringBuffer sb = new StringBuffer();
		String textinLine = "";
		String temp = "";
		try {
			fs = new FileInputStream(f);
			in = new InputStreamReader(fs);
			br = new BufferedReader(in);

			while (true) {
				textinLine = br.readLine();
				if (textinLine == null) {
					break;
				}
				// System.out.println(textinLine); 
				if(textinLine.trim().contains("putObjectToSessionFilter(ApplicationDescriptor.") && textinLine.trim().contains("_to_")){
					temp = textinLine.substring(0, textinLine.indexOf("_to_"));
//					System.out.println(temp); 
					temp = temp.substring(0, temp.indexOf("_")) + temp.substring(temp.indexOf("_")).toUpperCase(Locale.ENGLISH) + "_MODEL, ";
//					System.out.println(temp); 
		 			temp = temp + textinLine.trim().substring(textinLine.indexOf(","));
		 			sb.append(temp + "\n");
				} else if ((textinLine.trim().contains("setMasterObjectName(ApplicationDescriptor.") || textinLine.trim().contains("getObjectFromSessionFilter(ApplicationDescriptor."))
						&& textinLine.trim().contains("_to_")){
					temp = textinLine.substring(0, textinLine.indexOf("_to_"));
					temp = temp.substring(0, temp.indexOf("_")) + temp.substring(temp.indexOf("_")).toUpperCase(Locale.ENGLISH) + "_MODEL";
					if(textinLine.indexOf(");") > 0) {
						temp = temp + textinLine.substring(textinLine.indexOf(");"));
					} else if(textinLine.indexOf(") !=") > 0) {
						temp = temp + textinLine.substring(textinLine.indexOf(") !="));
					}  else if(textinLine.indexOf(")!=") > 0) {
						temp = temp + textinLine.substring(textinLine.indexOf(")!="));
					}
					sb.append(temp + "\n");
				} else {
					sb.append(textinLine + "\n");
				} 
			}// while
			// System.out.println(sb.toString());

			fs.close();
			in.close();
			br.close();
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("ERROR @" + fileName);
			System.out.println("ERROR Temp : " +  temp);
			System.out.println("ERROR textinLine : " +  textinLine);
			System.out.println("ERROR :" + e.getMessage());
		}

		try {
			FileWriter fstream = new FileWriter(f);
			BufferedWriter outobj = new BufferedWriter(fstream);
			outobj.write(sb.toString());
			outobj.close();

		} catch (Exception e) {
			System.err.println("Error: " + e.getMessage());
		}
	} // method

	static Collection<Path> searchInTree(Path directory, Collection<Path> all)
	        throws IOException {
	    try (DirectoryStream<Path> ds = Files.newDirectoryStream(directory)) {
	        for (Path child : ds) {
	            if (Files.isDirectory(child)) {
	            	if(!child.getFileName().toString().contains("_base")){
	            		searchInTree(child, all);
	            	}
	            } else {
	            	all.add(child);
	            }
	        }
	    }
	    return all;
	}
}
