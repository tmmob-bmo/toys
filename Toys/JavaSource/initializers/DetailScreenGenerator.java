package initializers;

/**
 *
 * @author useyhan
 */
import java.text.ParseException;

/**
 * Shows example usages of the metadata-extractor library.
 * 
 * @author Drew Noakes http://drewnoakes.com
 */
public class DetailScreenGenerator {


	
	public static void main(String[] args) throws ParseException { 
		String[] items = new String[]{"uyebelge","uyeuzmanlik","uyebilirkisi","uyeogrenim","uyeceza","uyeodul","uyeabone","uyesigorta","uyeaidat","uyeborc"};
		String modelName = "uye";
		
		System.out.println(writeHeightCalculator(items)); 
		
		System.out.println(writeDialogPanel(items, modelName));
		
		/*System.out.println(writeAttributesForDetails(items, modelName));
		
		System.out.println(writeInsertMethods(items));
		System.out.println(writeUpdateMethods(items));
		System.out.println(writeDeleteMethods(items));		
		
		System.out.println(writeSaveMethods(items, modelName));
		System.out.println(writeDetailLists(items, modelName));*/
	}
	 
	public static String writeAttributesForDetails(String[] items, String modelName){ 
		StringBuilder result = new StringBuilder();
		result.append(" \n");
		for(String str : items){
			result.append("@ManagedProperty(value = \"#{" + str + "Controller}\") \n");
			result.append("private " + makeFirstCharUpper(str) + "Controller " + str + "Controller; \n");  
			result.append(" \n");
		}
		
		for(String str : items){
			result.append("private " + makeFirstCharUpper(str) + " " + str + "; \n"); 
			result.append("private String oldValueFor" +  makeFirstCharUpper(str) + "; \n");
			result.append(" \n");
		}
		
		for(String str : items){
			result.append("private void " + str + "Guncelle(" + makeFirstCharUpper(modelName) + " ref" + makeFirstCharUpper(modelName) + ") {\n");
			result.append("	this." + str + "Controller.setTable(null);\n");
			result.append("	this." + str + "Controller.get" + makeFirstCharUpper(str) + "Filter().set" + makeFirstCharUpper(modelName) + "Ref(ref" + makeFirstCharUpper(modelName) + ");\n");
			result.append("	this." + str + "Controller.get" + makeFirstCharUpper(str) + "Filter().createQueryCriterias();\n");
			result.append("	this." + str + "Controller.queryAction();\n");
			result.append("}\n");
			result.append(" \n");		
		}
		return result.toString();
	}
	
	public static String writeInsertMethods(String[] items){ 
		StringBuilder result = new StringBuilder();
		for(String str : items){
			result.append("else if (detailType.equalsIgnoreCase(\"" + str + "\")) { \n");
			result.append("	this." + str + " = new " +  makeFirstCharUpper(str) + "(); \n");
			result.append("	this.oldValueFor" +  makeFirstCharUpper(str) + " = \"\"; \n");;
			result.append("} ");
		}
		return result.toString();
	}
	
	public static String writeUpdateMethods(String[] items){ 
		StringBuilder result = new StringBuilder();
		for(String str : items){ 			
			result.append("else if (detailType.equalsIgnoreCase(\"" + str + "\")) { \n");
			result.append("	this." + str + " = (" +  makeFirstCharUpper(str) + ") entity; \n");
			result.append("	this.oldValueFor" +  makeFirstCharUpper(str) + " =  this." + str + ".getValue(); \n");;
			result.append("} ");
		}
		return result.toString();
	}
	
	public static String writeDeleteMethods(String[] items){ 
		StringBuilder result = new StringBuilder();
		for(String str : items){ 	 
			result.append("else if (detailType.equalsIgnoreCase(\"" + str + "\")) {\n");
			result.append("	this." + str + " = (" + makeFirstCharUpper(str) + ") entity;\n");
			result.append("	super.justDelete(this." + str + ", this." + str + "Controller.isLoggable());\n");
			result.append(" 	this." + str + "Controller.queryAction();\n");
			result.append("}"); 
		}
		return result.toString();
	}
	
	public static String writeSaveMethods(String[] items, String modelName){ 
		StringBuilder result = new StringBuilder();
		modelName = makeFirstCharUpper(modelName);
		for(String str : items){ 		
			result.append("else if(this.detailEditType.equalsIgnoreCase(\"" + str + "\")){\n");		 
			result.append("	this." + str + ".set" + modelName + "Ref(get" + modelName + "Detay());\n");
			result.append("	super.justSave(this." + str + ", this." + str + "Controller.isLoggable(), this.oldValueFor" +  makeFirstCharUpper(str) + ");\n");
			result.append("	this." + str + " = new " +  makeFirstCharUpper(str) + "(); \n");
			result.append("	this." + str + "Controller.queryAction(); \n");
			result.append("	updateDialogAndForm(context); \n");
			result.append("} \n"); 
		} 
		return result.toString();
	}
	
	public static String writeHeightCalculator(String[] items){
		StringBuilder result = new StringBuilder(); 
		result.append("public String getHeightOfEditDialog(){ \n");
		result.append("	if(this.detailEditType == null){\n");
		result.append("		return \"400\";\n");
		result.append("	}");
		for(String str : items){
			result.append(" else if(this.detailEditType.equalsIgnoreCase(\"" + str + "\")){\n");
			result.append("		return \"400\";\n");
			result.append("	} ");
		}
		result.append(" else {\n");
		result.append("		return \"400\";\n");
		result.append("	}\n");
		result.append("}\n");
		return result.toString(); 			
	}
	
	public static String writeTabButtons(String[] items, String modelName){
		StringBuilder result = new StringBuilder(); 
			result.append("<p:commandButton process=\"@this\" value=\"#{label['" + modelName + ".listeyeDon']}\" icon=\"" + modelName + "\" action=\"#{" + modelName + "Controller.listPage}\" rendered=\"#{" + modelName + "Controller.listRenderedFor('" + makeFirstCharUpper(modelName) + "')}\" \n");
			result.append("		style=\"margin-right:30px\" />\n");
			result.append("<p:commandButton process=\"@this\" update=\"@form\" value=\"#{label['" + modelName + ".summary']}\" icon=\"" + modelName + "\" action=\"#{" + modelName + "Controller." + modelName + "}\"\n");
			result.append("		disabled=\"#{" + modelName + "Controller." + modelName + "Detay.RID == null or " + modelName + "Controller.selectedTab == '" + modelName + "'}\" rendered=\"#{" + modelName + "Controller.listRenderedFor('" + makeFirstCharUpper(modelName) + "')}\" />\n");
		for(String str : items){
			result.append("<p:commandButton id=\"" + str + "Command\" process=\"@this\" update=\"@form\" icon=\"" + str + "\" action=\"#{" + modelName + "Controller.create" + makeFirstCharUpper(modelName) + "Detay('" + str + "')}\"\n");
			result.append("		rendered=\"#{" + modelName + "Controller.listRenderedFor('" + makeFirstCharUpper(str) + "')}\" value=\"#{label['command." + str + "']}\"\n");
			result.append("		disabled=\"#{" + modelName + "Controller." + modelName + "Detay.RID == null or " + modelName + "Controller.selectedTab == '" + str + "'}\" />\n");
		}
		return result.toString();
	}
	
	public static String writeDetailLists(String[] items, String modelName){ 
		StringBuilder result = new StringBuilder(); 
		for(String str : items){ 		
			result.append("<p:fieldset legend=\"#{label['" + str + ".list']}\" styleClass=\"pageFieldSet\" id=\"" + str + "ListPanel\" rendered=\"#{" + modelName + "Controller.selectedTab == '" + str + "'}\">\n");
			result.append("	<p:panel id=\"" + str + "Master\" styleClass=\"innerFieldSet\">\n");
			result.append("		<h:graphicImage value=\"/_image/toMasterPage.png\" styleClass=\"masterLinkIcon\" width=\"16\" height=\"16\" style=\"margin-right:10px; vertical-align:middle !important\" />\n");
			result.append("		<h:outputText value=\"#{label['common.parent']} : #{" + modelName + "Controller." + modelName + "Detay.UIString}\" />\n");
			result.append("	</p:panel>\n");
			result.append("	<p:dataTable style=\"margin-top:10px\" id=\"" + str + "Table\" value=\"#{" + str + "Controller.list}\" var=\"" + str + "Row\" binding=\"#{" + str + "Controller.table}\" rowIndexVar=\"rowIndex\"\n");
			result.append("		rowKey=\"#{" + str + "Row.RID}\" resizableColumns=\"false\">\n");
			result.append("\n");result.append("\n");
			result.append("		<p:column width=\"100\" style=\"text-align:center\">\n");
			result.append("			<f:facet name=\"header\">\n");
			result.append("				<h:outputText value=\"#{label['common.islem']}\" />\n");
			result.append("			</f:facet>\n");
			result.append("			<p:commandButton onsuccess=\"detailEditDialog.show()\" action=\"#{" + modelName + "Controller.newDetail('" + str + "')}\" icon=\"new\" immediate=\"true\" process=\"@this\" update=\"@this\"\n");
			result.append("				styleClass=\"commandButton\" ajax=\"true\" style=\"width:30px\" rendered=\"#{" + modelName + "Controller.updateRecordRenderedFor('" +  makeFirstCharUpper(str) + "')}\" />\n");
			result.append("			<p:commandButton onsuccess=\"detailEditDialog.show()\" action=\"#{" + modelName + "Controller.updateDetail('" + str + "', " + str + "Row)}\" icon=\"edit\" immediate=\"true\" process=\"@this\" update=\"@this\"\n");
			result.append("				styleClass=\"commandButton\" ajax=\"true\" style=\"width:30px\" rendered=\"#{" + modelName + "Controller.updateRecordRenderedFor('" +  makeFirstCharUpper(str) + "')}\" />\n");
			result.append("			<p:commandButton id=\"" + modelName + "DeleteCommand\" action=\"#{" + modelName + "Controller.deleteDetail('" + str + "', " + str + "Row)}\"\n");
			result.append("				onclick=\"if (confirm('#{label['recordDelete']}')) {return true} else {return false}\" rendered=\"#{" + modelName + "Controller.deleteRenderedFor('" +  makeFirstCharUpper(str) + "')}\" icon=\"delete\"\n");
			result.append("				update=\":globalMessages\" styleClass=\"commandButton\" ajax=\"true\" style=\"width:30px\" immediate=\"true\" />\n");
			result.append("		</p:column>	\n");	
			result.append("	</p:dataTable>\n");
			result.append("	<p:commandButton onsuccess=\"detailEditDialog.show()\" value=\"#{label['command.insert']}\" action=\"#{" + modelName + "Controller.newDetail('" + str + "')}\" icon=\"new\" immediate=\"true\"\n");
			result.append("		process=\"@this\" update=\"@this\" styleClass=\"commandButton\" ajax=\"true\" style=\"width:70px; margin-left:48%; margin-bottom:30px\"\n");
			result.append("		rendered=\"#{" + modelName + "Controller.updateRecordRenderedFor('" + str + "') and " + str + "Controller.list.size() == 0}\" />\n");
			result.append("</p:fieldset>\n");
			result.append("\n");
		}
		return result.toString();
	}
	
	public static String writeDialogPanel(String[] items, String modelName){ 
		StringBuilder result = new StringBuilder(); 
		result.append("<p:dialog header=\"#{label['common.detailEdit']}\" widgetVar=\"detailEditDialog\" width=\"1000\" height=\"#{" + modelName + "Controller.heightOfEditDialog}\" \n");
		result.append("		style=\"position:absolute; left:5%; top:5%\" modal=\"true\" id=\"dialogUpdateBox\" visible=\"#{" + modelName + "Controller.detailEditType != null}\"> \n");
		result.append(" \n");
		result.append("		<h:form id=\"detailEditForm\">  \n");
		result.append(" \n");
		for(String str : items){
			result.append("			<p:fieldset id=\"" + str + "EditDialogPanel\" toggleable=\"false\" styleClass=\"pageFieldSet\" legend=\"#{label['" + str + ".edit']}\" rendered=\"#{" + modelName + "Controller.detailEditType == '" + str + "'}\"> \n");
			result.append(" \n"); 
			result.append("			</p:fieldset> \n");
			result.append(" \n"); 
		}
				result.append("		<p align=\"center\" style=\"margin-top: 20px\"> \n");
		result.append("			<p:commandButton id=\"saveDetailsButton\" icon=\"save\" value=\"#{label['command.post']}\" styleClass=\"commandButton\" action=\"#{" + modelName + "Controller.saveDetails}\" \n");
		result.append("				update=\"@form :globalMessages\" process=\"@form\" ajax=\"true\" rendered=\"#{" + modelName + "Controller.detailEditType != null}\" /> \n");
		result.append("			<p:commandButton id=\"cancelDetailsButton\" icon=\"cancel\" value=\"#{label['command.cancel']}\" styleClass=\"commandButton\" onclick=\"detailEditDialog.hide()\" process=\"@this\" /><br /> \n");
		result.append("	\n");
		result.append("		</p> \n");
		result.append("	</h:form> \n");
		result.append("</p:dialog> \n");
		return result.toString();
	}
	
		
	public static String makeFirstCharUpper(String input){
		return input.substring(0, 1).toUpperCase() + input.substring(1);
	} 
	 
}
