/**
 * Tmmob_x0020_KPS_x0020_ServiceV20Soap.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package org.tempuri;

public interface Tmmob_x0020_KPS_x0020_ServiceV20Soap extends java.rmi.Remote {
    public java.lang.String TCKimlikNodanKisiBilgisiGetir(long TCKimlikNo, java.lang.String odaKodu, java.lang.String sifre) throws java.rmi.RemoteException;
    public java.lang.String ilBilgisiGetir(java.lang.String odaKodu, java.lang.String sifre) throws java.rmi.RemoteException;
    public java.lang.String ileBagliIlceBilgisiGetir(int ilKodu, java.lang.String odaKodu, java.lang.String sifre) throws java.rmi.RemoteException;
    public java.lang.String kisiAdresBilgisiGetir(long TCKimlikNo, java.lang.String odaKodu, java.lang.String sifre) throws java.rmi.RemoteException;
    public java.lang.String ulkeBilgisiGetir(java.lang.String odaKodu, java.lang.String sifre) throws java.rmi.RemoteException;
    public java.lang.String kisiBilgisindenKimlikBilgisiGetir(java.lang.String ad, java.lang.String soyad, java.lang.String anaAd, java.lang.String babaAd, int cinsiyet, int dogumYil, int dogumAy, int dogumGun, java.lang.String dogumYer, int sorguTipi, java.lang.String odaKodu, java.lang.String sifre) throws java.rmi.RemoteException;
}
