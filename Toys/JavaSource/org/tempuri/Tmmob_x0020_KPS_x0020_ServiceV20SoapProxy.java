package org.tempuri;

import java.rmi.RemoteException;

import org.apache.log4j.Logger;

import tr.com.arf.framework.db._management.DBOperator;
import tr.com.arf.framework.utility.exception.data.DBException;
import tr.com.arf.toys.db.enumerated.system.KpsTuru;
import tr.com.arf.toys.db.model.system.Kpslog;

public class Tmmob_x0020_KPS_x0020_ServiceV20SoapProxy implements org.tempuri.Tmmob_x0020_KPS_x0020_ServiceV20Soap {
	private String _endpoint = null;
	private org.tempuri.Tmmob_x0020_KPS_x0020_ServiceV20Soap tmmob_x0020_KPS_x0020_ServiceV20Soap = null;
	
	/* Data Operator */
	private DBOperator dBOperator; 

	/* Log4j Logger */
	protected static Logger logger = Logger.getLogger("commonController");

	/* Data Operator */
	protected final DBOperator getDBOperator() {
		if (dBOperator == null) {
			dBOperator = DBOperator.getInstance();
		}
		return dBOperator;
	}
	
	public Tmmob_x0020_KPS_x0020_ServiceV20SoapProxy() {
		_initTmmob_x0020_KPS_x0020_ServiceV20SoapProxy();
	}

	public Tmmob_x0020_KPS_x0020_ServiceV20SoapProxy(String endpoint) {
		_endpoint = endpoint;
		_initTmmob_x0020_KPS_x0020_ServiceV20SoapProxy();
	}

	private void _initTmmob_x0020_KPS_x0020_ServiceV20SoapProxy() {
		try {
			tmmob_x0020_KPS_x0020_ServiceV20Soap = new org.tempuri.Tmmob_x0020_KPS_x0020_ServiceV20Locator().getTmmob_x0020_KPS_x0020_ServiceV20Soap();
			if (tmmob_x0020_KPS_x0020_ServiceV20Soap != null) {
				if (_endpoint != null) {
					((javax.xml.rpc.Stub) tmmob_x0020_KPS_x0020_ServiceV20Soap)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
				} else {
					_endpoint = (String) ((javax.xml.rpc.Stub) tmmob_x0020_KPS_x0020_ServiceV20Soap)._getProperty("javax.xml.rpc.service.endpoint.address");
				}
			}

		} catch (javax.xml.rpc.ServiceException serviceException) {
            logger.error(serviceException.toString(), serviceException);
		}
	}

	public String getEndpoint() {
		return _endpoint;
	}

	public void setEndpoint(String endpoint) {
		_endpoint = endpoint;
		if (tmmob_x0020_KPS_x0020_ServiceV20Soap != null) {
			((javax.xml.rpc.Stub) tmmob_x0020_KPS_x0020_ServiceV20Soap)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
		}

	}

	public org.tempuri.Tmmob_x0020_KPS_x0020_ServiceV20Soap getTmmob_x0020_KPS_x0020_ServiceV20Soap() {
		if (tmmob_x0020_KPS_x0020_ServiceV20Soap == null) {
			_initTmmob_x0020_KPS_x0020_ServiceV20SoapProxy();
		}
		return tmmob_x0020_KPS_x0020_ServiceV20Soap;
	}
	
	private void kpsLogKaydet(Kpslog kpsLog){
		try {
			getDBOperator().insert(kpsLog);
		} catch (DBException exc) {
			logger.error("Error @kpsLogKaydet of " + this.getClass().getSimpleName() + " : " + exc.getMessage(), exc);
		}
	}

	@Override
	public java.lang.String TCKimlikNodanKisiBilgisiGetir(long TCKimlikNo, java.lang.String odaKodu, java.lang.String sifre) throws java.rmi.RemoteException {
		if (tmmob_x0020_KPS_x0020_ServiceV20Soap == null) {
			_initTmmob_x0020_KPS_x0020_ServiceV20SoapProxy();
		}
		String result = tmmob_x0020_KPS_x0020_ServiceV20Soap.TCKimlikNodanKisiBilgisiGetir(TCKimlikNo, odaKodu, sifre);
		kpsLogKaydet(new Kpslog(KpsTuru._KPSKIMLIK, TCKimlikNo));
		return result;
	}

	@Override
	public java.lang.String ilBilgisiGetir(java.lang.String odaKodu, java.lang.String sifre) throws java.rmi.RemoteException {
		if (tmmob_x0020_KPS_x0020_ServiceV20Soap == null) {
			_initTmmob_x0020_KPS_x0020_ServiceV20SoapProxy();
		}
		return tmmob_x0020_KPS_x0020_ServiceV20Soap.ilBilgisiGetir(odaKodu, sifre);
	}

	@Override
	public java.lang.String ileBagliIlceBilgisiGetir(int ilKodu, java.lang.String odaKodu, java.lang.String sifre) throws java.rmi.RemoteException {
		if (tmmob_x0020_KPS_x0020_ServiceV20Soap == null) {
			_initTmmob_x0020_KPS_x0020_ServiceV20SoapProxy();
		}
		return tmmob_x0020_KPS_x0020_ServiceV20Soap.ileBagliIlceBilgisiGetir(ilKodu, odaKodu, sifre);
	}

	@Override
	public java.lang.String kisiAdresBilgisiGetir(long TCKimlikNo, java.lang.String odaKodu, java.lang.String sifre) throws java.rmi.RemoteException {
		if (tmmob_x0020_KPS_x0020_ServiceV20Soap == null) {
			_initTmmob_x0020_KPS_x0020_ServiceV20SoapProxy();
		}
		String result = tmmob_x0020_KPS_x0020_ServiceV20Soap.kisiAdresBilgisiGetir(TCKimlikNo, odaKodu, sifre);
		kpsLogKaydet(new Kpslog(KpsTuru._KPSADRES, TCKimlikNo));
		return result;		 
	}

	@Override
	public java.lang.String kisiBilgisindenKimlikBilgisiGetir(java.lang.String ad, java.lang.String soyad, java.lang.String anaAd, java.lang.String babaAd, int cinsiyet,
			int dogumYil, int dogumAy, int dogumGun, java.lang.String dogumYer, int sorguTipi, java.lang.String odaKodu, java.lang.String sifre) throws java.rmi.RemoteException {
		if (tmmob_x0020_KPS_x0020_ServiceV20Soap == null) {
			_initTmmob_x0020_KPS_x0020_ServiceV20SoapProxy();
		}
		return tmmob_x0020_KPS_x0020_ServiceV20Soap.kisiBilgisindenKimlikBilgisiGetir(ad, soyad, anaAd, babaAd, cinsiyet, dogumYil, dogumAy, dogumGun, dogumYer, sorguTipi,
				odaKodu, sifre);
	}

	@Override
	public String ulkeBilgisiGetir(String odaKodu, String sifre) throws RemoteException {
		if (tmmob_x0020_KPS_x0020_ServiceV20Soap == null) {
			_initTmmob_x0020_KPS_x0020_ServiceV20SoapProxy();
		}
		return tmmob_x0020_KPS_x0020_ServiceV20Soap.ulkeBilgisiGetir(odaKodu, sifre);
	}

}