/**
 * Tmmob_x0020_KPS_x0020_ServiceV20.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package org.tempuri;

public interface Tmmob_x0020_KPS_x0020_ServiceV20 extends javax.xml.rpc.Service {

/**
 * ...
 */
    public java.lang.String getTmmob_x0020_KPS_x0020_ServiceV20SoapAddress();

    public org.tempuri.Tmmob_x0020_KPS_x0020_ServiceV20Soap getTmmob_x0020_KPS_x0020_ServiceV20Soap() throws javax.xml.rpc.ServiceException;

    public org.tempuri.Tmmob_x0020_KPS_x0020_ServiceV20Soap getTmmob_x0020_KPS_x0020_ServiceV20Soap(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
