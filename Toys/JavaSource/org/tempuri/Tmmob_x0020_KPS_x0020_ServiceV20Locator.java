/**
 * Tmmob_x0020_KPS_x0020_ServiceV20Locator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package org.tempuri;

import tr.com.arf.toys.service.application.ManagedBeanLocator;

@SuppressWarnings("serial")
public class Tmmob_x0020_KPS_x0020_ServiceV20Locator extends org.apache.axis.client.Service implements org.tempuri.Tmmob_x0020_KPS_x0020_ServiceV20 {

	/**
	 * ...
	 */

	public Tmmob_x0020_KPS_x0020_ServiceV20Locator() {
	}

	public Tmmob_x0020_KPS_x0020_ServiceV20Locator(org.apache.axis.EngineConfiguration config) {
		super(config);
	}

	public Tmmob_x0020_KPS_x0020_ServiceV20Locator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
		super(wsdlLoc, sName);
	}

	// Use to get a proxy class for Tmmob_x0020_KPS_x0020_ServiceV20Soap
	// private java.lang.String Tmmob_x0020_KPS_x0020_ServiceV20Soap_address =
	// "http://85.111.17.251/Tmmob.Kps.Test.WS/TmmobKpsServiceTest.asmx";
	private java.lang.String Tmmob_x0020_KPS_x0020_ServiceV20Soap_address = ManagedBeanLocator.locateSessionController().getKpsEndPoint();

	@Override
	public java.lang.String getTmmob_x0020_KPS_x0020_ServiceV20SoapAddress() {
		return Tmmob_x0020_KPS_x0020_ServiceV20Soap_address;
	}

	// The WSDD service name defaults to the port name.
	private java.lang.String Tmmob_x0020_KPS_x0020_ServiceV20SoapWSDDServiceName = "Tmmob_x0020_KPS_x0020_ServiceV2.0Soap";

	public java.lang.String getTmmob_x0020_KPS_x0020_ServiceV20SoapWSDDServiceName() {
		return Tmmob_x0020_KPS_x0020_ServiceV20SoapWSDDServiceName;
	}

	public void setTmmob_x0020_KPS_x0020_ServiceV20SoapWSDDServiceName(java.lang.String name) {
		Tmmob_x0020_KPS_x0020_ServiceV20SoapWSDDServiceName = name;
	}

	@Override
	public org.tempuri.Tmmob_x0020_KPS_x0020_ServiceV20Soap getTmmob_x0020_KPS_x0020_ServiceV20Soap() throws javax.xml.rpc.ServiceException {
		java.net.URL endpoint;
		try {
			endpoint = new java.net.URL(Tmmob_x0020_KPS_x0020_ServiceV20Soap_address);
		} catch (java.net.MalformedURLException e) {
			throw new javax.xml.rpc.ServiceException(e);
		}
		return getTmmob_x0020_KPS_x0020_ServiceV20Soap(endpoint);
	}

	@Override
	public org.tempuri.Tmmob_x0020_KPS_x0020_ServiceV20Soap getTmmob_x0020_KPS_x0020_ServiceV20Soap(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
		try {
			org.tempuri.Tmmob_x0020_KPS_x0020_ServiceV20SoapStub _stub = new org.tempuri.Tmmob_x0020_KPS_x0020_ServiceV20SoapStub(portAddress, this);
			_stub.setPortName(getTmmob_x0020_KPS_x0020_ServiceV20SoapWSDDServiceName());
			return _stub;
		} catch (org.apache.axis.AxisFault e) {
			return null;
		}
	}

	public void setTmmob_x0020_KPS_x0020_ServiceV20SoapEndpointAddress(java.lang.String address) {
		Tmmob_x0020_KPS_x0020_ServiceV20Soap_address = address;
	}

	/**
	 * For the given interface, get the stub implementation. If this service has
	 * no port for the given interface, then ServiceException is thrown.
	 */
	@SuppressWarnings("rawtypes")
	@Override
	public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
		try {
			if (org.tempuri.Tmmob_x0020_KPS_x0020_ServiceV20Soap.class.isAssignableFrom(serviceEndpointInterface)) {
				org.tempuri.Tmmob_x0020_KPS_x0020_ServiceV20SoapStub _stub = new org.tempuri.Tmmob_x0020_KPS_x0020_ServiceV20SoapStub(new java.net.URL(
						Tmmob_x0020_KPS_x0020_ServiceV20Soap_address), this);
				_stub.setPortName(getTmmob_x0020_KPS_x0020_ServiceV20SoapWSDDServiceName());
				return _stub;
			}
		} catch (java.lang.Throwable t) {
			throw new javax.xml.rpc.ServiceException(t);
		}
		throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  "
				+ (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
	}

	/**
	 * For the given interface, get the stub implementation. If this service has
	 * no port for the given interface, then ServiceException is thrown.
	 */
	@SuppressWarnings("rawtypes")
	@Override
	public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
		if (portName == null) {
			return getPort(serviceEndpointInterface);
		}
		java.lang.String inputPortName = portName.getLocalPart();
		if ("Tmmob_x0020_KPS_x0020_ServiceV2.0Soap".equals(inputPortName)) {
			return getTmmob_x0020_KPS_x0020_ServiceV20Soap();
		} else {
			java.rmi.Remote _stub = getPort(serviceEndpointInterface);
			((org.apache.axis.client.Stub) _stub).setPortName(portName);
			return _stub;
		}
	}

	@Override
	public javax.xml.namespace.QName getServiceName() {
		return new javax.xml.namespace.QName("http://tempuri.org/", "Tmmob_x0020_KPS_x0020_ServiceV2.0");
	}

	@SuppressWarnings("rawtypes")
	private java.util.HashSet ports = null;

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public java.util.Iterator getPorts() {
		if (ports == null) {
			ports = new java.util.HashSet();
			ports.add(new javax.xml.namespace.QName("http://tempuri.org/", "Tmmob_x0020_KPS_x0020_ServiceV2.0Soap"));
		}
		return ports.iterator();
	}

	/**
	 * Set the endpoint address for the specified port name.
	 */
	public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {

		if ("Tmmob_x0020_KPS_x0020_ServiceV20Soap".equals(portName)) {
			setTmmob_x0020_KPS_x0020_ServiceV20SoapEndpointAddress(address);
		} else { // Unknown Port Name
			throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
		}
	}

	/**
	 * Set the endpoint address for the specified port name.
	 */
	public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
		setEndpointAddress(portName.getLocalPart(), address);
	}

}
