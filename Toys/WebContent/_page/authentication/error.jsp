<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isErrorPage="true"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<style type="text/css">
body {
	margin: 0;
	padding: 0;
	background: #f4f4f4;
	font: 14px;
	font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
}

.link {
	text-align: center;
	clear: both;
	padding: 20px 0;
}

.link a {
	color: #333333;
}

/* Form 1 style */
.form1 {
	width: 60%;
	margin-left: 20%;
	margin-top: 5%;
	float: center;
	background: #fff;
	color: #777;
	-webkit-box-shadow: 0px 0px 8px 2px #d1d1d1;
	-moz-box-shadow: 0px 0px 8px 2px #d1d1d1;
	box-shadow: 0px 0px 8px 2px #d1d1d1;
	-webkit-border-top-left-radius: 0px;
	-webkit-border-top-right-radius: 0px;
	-webkit-border-bottom-right-radius: 6px;
	-webkit-border-bottom-left-radius: 6px;
	-moz-border-radius-topleft: 0px;
	-moz-border-radius-topright: 0px;
	-moz-border-radius-bottomright: 6px;
	-moz-border-radius-bottomleft: 6px;
	border-top-left-radius: 0px;
	border-top-right-radius: 0px;
	border-bottom-right-radius: 6px;
	border-bottom-left-radius: 6px;
	overflow: hidden;
}

.formtitle {
	padding: 10px;
	line-height: 20px;
	font-size: 28px;
	text-align: center;
	text-shadow: -1px -1px #e87c19;
	color: #fff;
	font-weight: bold;
	border-bottom: 1px solid #eb8d19;
	width: 100%;
	background: #ffbd27; /* Old browsers */
	background: -moz-linear-gradient(top, #ffbd27 0%, #ffb119 50%, #ff9d19 51%, #ff9d19
		100%); /* FF3.6+ */
	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%, #ffbd27),
		color-stop(50%, #ffb119), color-stop(51%, #ff9d19),
		color-stop(100%, #ff9d19)); /* Chrome,Safari4+ */
	background: -webkit-linear-gradient(top, #ffbd27 0%, #ffb119 50%, #ff9d19 51%,
		#ff9d19 100%); /* Chrome10+,Safari5.1+ */
	background: -o-linear-gradient(top, #ffbd27 0%, #ffb119 50%, #ff9d19 51%, #ff9d19
		100%); /* Opera11.10+ */
	background: -ms-linear-gradient(top, #ffbd27 0%, #ffb119 50%, #ff9d19 51%, #ff9d19
		100%); /* IE10+ */
	filter: progid:DXImageTransform.Microsoft.gradient(     startColorstr='#ffbd27',
		endColorstr='#ff9d19', GradientType=0); /* IE6-9 */
	background: linear-gradient(top, #ffbd27 0%, #ffb119 50%, #ff9d19 51%, #ff9d19 100%);
	/* W3C */
}

.input {
	width: 100%;
	border-bottom: 1px solid #ddd;
	margin-bottom: 10px;
	margin: 20px;
	overflow: hidden;
	text-align: center;
}

.inputtext {
	float: left;
	line-height: 18px;
	height: 35px;
	font-size: 16px;
	font-weight: bold;
	text-align: center;
	width: 100%;
}

.orangebutton {
	background: #ffc339; /* Old browsers */
	background: -moz-linear-gradient(top, #ffc339 0%, #ff9b19 100%);
	/* FF3.6+ */
	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%, #ffc339),
		color-stop(100%, #ff9b19)); /* Chrome,Safari4+ */
	background: -webkit-linear-gradient(top, #ffc339 0%, #ff9b19 100%);
	/* Chrome10+,Safari5.1+ */
	background: -o-linear-gradient(top, #ffc339 0%, #ff9b19 100%);
	/* Opera11.10+ */
	background: -ms-linear-gradient(top, #ffc339 0%, #ff9b19 100%);
	/* IE10+ */
	filter: progid:DXImageTransform.Microsoft.gradient(     startColorstr='#ffc339',
		endColorstr='#ff9b19', GradientType=0); /* IE6-9 */
	background: linear-gradient(top, #ffc339 0%, #ff9b19 100%); /* W3C */
	border: 1px solid #ff9b19;
	line-height: 24px;
	font-size: 18px;
	padding: 4px 8px;
	color: #fff;
	text-shadow: -1px -1px #ff9b19;
	float: center;
	margin-left: 10px;
	cursor: pointer;
}

.orangebutton:hover {
	background: #ff9b19; /* Old browsers */
	background: -moz-linear-gradient(top, #ff9b19 0%, #ffc339 100%);
	/* FF3.6+ */
	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%, #ff9b19),
		color-stop(100%, #ffc339)); /* Chrome,Safari4+ */
	background: -webkit-linear-gradient(top, #ff9b19 0%, #ffc339 100%);
	/* Chrome10+,Safari5.1+ */
	background: -o-linear-gradient(top, #ff9b19 0%, #ffc339 100%);
	/* Opera11.10+ */
	background: -ms-linear-gradient(top, #ff9b19 0%, #ffc339 100%);
	/* IE10+ */
	filter: progid:DXImageTransform.Microsoft.gradient(     startColorstr='#ff9b19',
		endColorstr='#ffc339', GradientType=0); /* IE6-9 */
	background: linear-gradient(top, #ff9b19 0%, #ffc339 100%); /* W3C */
}
</style>
<title>TMMOB Türk Mühendis ve Mimar Odaları Birliği</title>
</head>

<body style="overflow: hidden">
	<div class="main">
		<form class="form1" action="faces/_page/authentication/uyelogin.xhtml">
			<div class="formtitle">
				<p>TMMOB Türk Mühendis ve Mimar Odaları Birliği</p>
			</div>
			<p>&nbsp;</p>
			<p>&nbsp;</p>
			<div class="input">
				<h1>TOYS - Oda Otomasyon Sistemi</h1>
				<p align="center">
					<h2>Beklenmedik bir hata meydana geldi.<br/>Lütfen sistem yöneticisine hatayı bildiriniz!</h2>
				</p> 
			</div>

			<p align="center">
				<input type="submit" value="Üye Girişi" class="orangebutton" />
				<input type="button" value="Geri" class="orangebutton" onclick="window.history.back()" />
			</p>
			<p>&nbsp;</p>
		</form>
	</div>
</body>
</html>