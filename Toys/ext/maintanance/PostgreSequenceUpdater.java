package initializers;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.postgresql.util.PSQLException;

public class PostgreSequenceUpdater {

	public static final String _schemaName = "oltp";
	public static final String _getTableNames = "SELECT DISTINCT(RELNAME) FROM PG_STAT_USER_TABLES WHERE (SCHEMANAME = LOWER(?) OR SCHEMANAME = UPPER(?)) order by relname";
	private final static String _getMaxRID = "SELECT MAX(RID) FROM ";

	public static void main(String[] args) {

		ArrayList<String> dbTablesListFinal = new ArrayList<String>();
		Connection con = null;
		ResultSet rst = null;
		PreparedStatement psmt = null;
		try {
			con = PostGREDBManager.getConnection();
			psmt = con.prepareStatement(_getTableNames);
			psmt.setString(1, _schemaName);
			psmt.setString(2, _schemaName);
			rst = psmt.executeQuery();
			while (rst.next()) {
				dbTablesListFinal.add(rst.getString(1));
			}
			System.out.println("dbTablesListFinal size :" + dbTablesListFinal.size());
			rst.close();
			psmt.close();
			long maxRID = 0;
			long seqValue = 0;
			for (String tableName : dbTablesListFinal) {
				maxRID = 0;
				try {
					psmt = con.prepareStatement(_getMaxRID + " " + _schemaName + "." + tableName);
					rst = psmt.executeQuery();
					while (rst.next()) {
						maxRID = rst.getLong(1);
					}
					rst.close();
					psmt.close();
					if (tableName.toLowerCase().equalsIgnoreCase("ilce")) {
						tableName = "district";
					} else if (tableName.toLowerCase().equalsIgnoreCase("islem")) {
						tableName = "transaction";
					}
					psmt = con.prepareStatement("SELECT nextval('" + _schemaName + "." + tableName.toLowerCase() +"_rid_seq')");
					rst = psmt.executeQuery();
					while (rst.next()) {
						seqValue = rst.getLong(1);
					}
					rst.close();
					psmt.close();
					if (maxRID > 0) {
						if (maxRID >= seqValue) {
							System.out.println("SEQUENCE ERROR @table: " + tableName + "_SEQ" + " maxRID: " + maxRID + " - seqValue:" + seqValue);
							String sql = "SELECT setval('" + _schemaName + "." + tableName + "_rid_seq', ?, true)";
							psmt = con.prepareStatement(sql);
							psmt.setLong(1, maxRID + 1);
							rst = psmt.executeQuery();
							psmt.execute();
							psmt.close();
							System.out.println("SEQUENCE created : " + tableName + "_rid_seq");
						}
					}
				} catch (Exception e) {
					System.out.println("ERROR @reading sequence, trying to create sequence :" + e.getMessage());
					try {
						String sql = "CREATE SEQUENCE " + _schemaName + "." + tableName + "_rid_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START ? CACHE 1; ";
						psmt = con.prepareStatement(sql);
						psmt.setLong(1, maxRID + 1);
						rst = psmt.executeQuery();
						psmt.execute();
						psmt.close();
						System.out.println(tableName + " table or related sequence does not exist");
					} catch (PSQLException e2) {
						System.out.println(tableName + " table or related sequence already exists");
					}
				}
			}
			System.out.println("Sequences updated...");
		} catch (Exception e) {
			System.out.println("HATA :" + e.getMessage() + "- " + e.getClass().getSimpleName());
		} finally {
			if (rst != null) {
				try {
					rst.close();
				} catch (SQLException e2) {
					e2.printStackTrace();
				}
			}
			if (psmt != null) {
				try {
					psmt.close();
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}
}
