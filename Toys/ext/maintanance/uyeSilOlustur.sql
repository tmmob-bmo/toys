delete from oltp.uyeaidat where uyeref > 35;

delete from oltp.uyeaidat where yil = 2014;

delete from oltp.uye where rid > 35;
 
INSERT INTO oltp.uye (kisiref, sicilno, uyetip, uyeliktarih, ayrilmatarih, uyedurum, kayitdurum, onaylayanref, onaytarih, birimref, sinif, ogrencino, ogrenciuyeref, iletisimsms, iletisimemail, iletisimposta) VALUES (72, 'G0000000001', 1, '2013-07-02', NULL, 1, 0, NULL, NULL, 4, NULL, '', NULL, 2, 2, 2);

INSERT INTO oltp.uyeaidat (uyeref, miktar, odenen, uyeaidatdurum, yil, ay, uyemuafiyetref) VALUES ((SELECT MAX(RID) FROM UYE), 10, 0, 1, 2013, 7, NULL);
INSERT INTO oltp.uyeaidat (uyeref, miktar, odenen, uyeaidatdurum, yil, ay, uyemuafiyetref) VALUES ((SELECT MAX(RID) FROM UYE), 10, 0, 1, 2013, 8, NULL);
INSERT INTO oltp.uyeaidat (uyeref, miktar, odenen, uyeaidatdurum, yil, ay, uyemuafiyetref) VALUES ((SELECT MAX(RID) FROM UYE), 10, 0, 1, 2013, 9, NULL);
INSERT INTO oltp.uyeaidat (uyeref, miktar, odenen, uyeaidatdurum, yil, ay, uyemuafiyetref) VALUES ((SELECT MAX(RID) FROM UYE), 10, 0, 1, 2013, 10, NULL);
INSERT INTO oltp.uyeaidat (uyeref, miktar, odenen, uyeaidatdurum, yil, ay, uyemuafiyetref) VALUES ((SELECT MAX(RID) FROM UYE), 10, 0, 1, 2013, 11, NULL);
INSERT INTO oltp.uyeaidat (uyeref, miktar, odenen, uyeaidatdurum, yil, ay, uyemuafiyetref) VALUES ((SELECT MAX(RID) FROM UYE), 10, 0, 1, 2013, 12, NULL);


drop table UYEDURUMDEGISTIRME;

CREATE TABLE oltp.UYEDURUMDEGISTIRME
(
	RID 					BIGSERIAL 						NOT NULL, 
	UYEREF					BIGINT							NOT NULL,
	TARIH					DATE							NOT NULL,
	KAYITYAPAN				BIGINT							NOT NULL, 
	ONCEKIDURUM				INTEGER							NOT NULL,
	YENIDURUM				INTEGER							NOT NULL,
	
	CONSTRAINT UYEDURUMDEGISTIRME_PK PRIMARY KEY (RID),
	CONSTRAINT UYEDURUMDEGISTIRME_UYE_KEY FOREIGN KEY (UYEREF)
	    REFERENCES UYE (RID) MATCH SIMPLE
	    ON UPDATE RESTRICT ON DELETE RESTRICT,
	CONSTRAINT UYEDURUMDEGISTIRME_KULLANICI_KEY FOREIGN KEY (KAYITYAPAN)
	    REFERENCES KULLANICI (RID) MATCH SIMPLE
	    ON UPDATE RESTRICT ON DELETE RESTRICT    
);