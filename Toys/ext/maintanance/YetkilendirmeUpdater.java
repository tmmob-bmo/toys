package initializers;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;


public class YetkilendirmeUpdater {

	/**
	 * @param args
	 */
	static String yeniIslem = "kullanicirapor";
	
	public static final String _getRoles = "SELECT RID From ROLE";
	public static final String _getIslem = "SELECT RID From ISLEM WHERE LOWER(name) = '" + yeniIslem + "'";
	public static final String _getKullanicis = "SELECT RID From KULLANICI"; 
	
	private final static String _createIslemRoles = "INSERT INTO ISLEMROLE(ISLEMREF, ROLEREF, UPDATEPERMISSION, DELETEPERMISSION, LISTPERMISSION) VALUES (?, ?, 1, 1, 1);";	
			
	public static final String _getKullaniciRoles = "SELECT RID From KULLANICIROLE"; 
	private final static String _createKullaniciIslems = "INSERT INTO ISLEMKULLANICI(ISLEMREF, KULLANICIROLEREF, UPDATEPERMISSION, DELETEPERMISSION, LISTPERMISSION) " +
									"		VALUES (?, ?, 1, 1, 1)";
	
	public static void main(String[] args) {
		
		ArrayList<Integer> roleIds = new ArrayList<Integer>();
		Integer islemId = null;
		ArrayList<Integer> kullaniciIds = new ArrayList<Integer>();
		ArrayList<Integer> kullaniciRoleIds = new ArrayList<Integer>();
		Connection con = null;
		ResultSet rst = null;
		PreparedStatement psmt = null;	 
		try{ 
			con = PostGREDBManager.getConnection();
			psmt = con.prepareStatement(_getRoles); 
			rst = psmt.executeQuery();
			while(rst.next()){
				roleIds.add(rst.getInt(1));
			}
			rst.close();
			psmt.close();
			
			psmt = con.prepareStatement(_getIslem);  
			rst = psmt.executeQuery();
			if(rst.next()){
				islemId = rst.getInt(1);
			}
			rst.close();
			psmt.close();
			
			psmt = con.prepareStatement(_getKullanicis); 
			rst = psmt.executeQuery();
			while(rst.next()){
				kullaniciIds.add(rst.getInt(1));
			}
			rst.close();
			psmt.close();
			 
			for(Integer rolId : roleIds){ 
				try { 
					psmt = con.prepareStatement(_createIslemRoles); 
					psmt.setInt(1, islemId);
					psmt.setInt(2, rolId);
					psmt.executeUpdate(); 
				} catch(Exception e){
					System.out.println( rolId + " role could not be added... : " + e.getMessage());
				}  
			}
			rst.close();
			psmt.close();
			
			psmt = con.prepareStatement(_getKullaniciRoles); 
			rst = psmt.executeQuery();
			while(rst.next()){
				kullaniciRoleIds.add(rst.getInt(1));
			}
			rst.close();
			psmt.close();
			 
			try { 
				 for(Integer kullaniciRoleId : kullaniciRoleIds){
						psmt = con.prepareStatement(_createKullaniciIslems); 
						psmt.setInt(1, islemId);
						psmt.setInt(2, kullaniciRoleId);
						psmt.executeUpdate();
					 }
			} catch(Exception e){
				System.out.println( islemId + " islem could not be added...");
			}  
			System.out.println("Authorization mechanism data initalized...");
		} catch(Exception e){
			System.out.println("HATA :" + e.getMessage());
		} finally {
			if(rst != null) {
				try {
					rst.close();
				} catch (SQLException e2) { 
					e2.printStackTrace();
				}
			}
			if(psmt != null) {
				try {
					psmt.close();
				} catch (SQLException e1) { 
					e1.printStackTrace();
				}
			}
			if(con != null) {
				try {
					con.close();
				} catch (SQLException e) { 
					e.printStackTrace();
				}
			}
		}
	}
}
