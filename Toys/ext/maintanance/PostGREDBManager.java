package initializers;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import tr.com.arf.framework.utility.exception.data.DBException;
import tr.com.arf.toys.utility.application.ApplicationConstant;

public class PostGREDBManager {
	private static javax.sql.DataSource dataSource = null;
	@SuppressWarnings("unused")
	private static String dataSourceName = null;
	
	public static final String _postgreSQLUser = "toys";
	public static final String _postgreSQLPassword = "123.321";
	public static final String _postgreSQLUrl = "jdbc:postgresql://localhost:5432/TOYS";

	public static Connection getConnection() throws InstantiationException, IllegalAccessException, DBException {
		Connection con = null;
		try {

			if (dataSource == null) {
				@SuppressWarnings("unused")
				javax.naming.InitialContext ctx = new javax.naming.InitialContext();
				Class.forName("org.postgresql.Driver").newInstance();
				con = DriverManager.getConnection(ApplicationConstant._postgreSQLUrl, ApplicationConstant._postgreSQLUser, ApplicationConstant._postgreSQLPassword);
			}
			return con;
		} catch (Exception e0) {
			e0.printStackTrace();
			throw new DBException(e0);
		}

	}

	public static void closeConnection(Connection connection, Statement statement) throws DBException {
		try {
			if (statement != null) {
				statement.close();
			}
		} catch (Exception e) {
			throw new DBException(e);
		}
		try {
			if (connection != null && !connection.isClosed()) {
				connection.close();
			}
		} catch (Exception e) {
			throw new DBException(e);
		}
	}

	public static void closeConnection(Connection connection, Statement statement, ResultSet resultSet) throws DBException {
		try {
			if (resultSet != null) {
				resultSet.close();
			}
		} catch (Exception e) {
			throw new DBException(e);
		}
		try {
			if (statement != null) {
				statement.close();
			}
		} catch (Exception e) {
			throw new DBException(e);
		}
		try {
			if (connection != null && !connection.isClosed()) {
				connection.close();
			}
		} catch (Exception e) {
			throw new DBException(e);
		}
	}

	public static PreparedStatement getPreparedStatement(Connection con, String sql) {

		try {
			return con.prepareStatement(sql);

		} catch (SQLException e) {

			e.printStackTrace();
		}

		return null;
	}
}
