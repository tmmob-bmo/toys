package initializers;

import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Locale;


public class YetkilendirmeInitializer {

	/**
	 * @param args
	 */
	public static final String _getRoles = "SELECT RID From ROLE";
	public static final String _getIslems = "SELECT RID From ISLEM";
	public static final String _getKullanicis = "SELECT RID From KULLANICI"; 
	private final static String _createIslemRoles = "INSERT INTO ISLEMROLE(ISLEMREF, ROLEREF, UPDATEPERMISSION, DELETEPERMISSION, LISTPERMISSION) VALUES (?, ?, 1, 1, 1);";
	
	private final static String _createKullaniciRoles = "INSERT INTO KULLANICIROLE(KULLANICIREF, ROLEREF, ACTIVE)  VALUES (?,?,1);";
	
	public static final String _getKullaniciRoles = "SELECT RID From KULLANICIROLE"; 
	private final static String _createKullaniciIslems = "INSERT INTO ISLEMKULLANICI(ISLEMREF, KULLANICIROLEREF, UPDATEPERMISSION, DELETEPERMISSION, LISTPERMISSION) " +
									"		VALUES (?, ?, 1, 1, 1)";
	
	public static void main(String[] args) throws IOException {
		createIslemList();
		ArrayList<Integer> roleIds = new ArrayList<Integer>();
		ArrayList<Integer> islemIds = new ArrayList<Integer>();
		ArrayList<Integer> kullaniciIds = new ArrayList<Integer>();
		ArrayList<Integer> kullaniciRoleIds = new ArrayList<Integer>();
		Connection con = null;
		ResultSet rst = null;
		PreparedStatement psmt = null;	 
		try{ 
			con = PostGREDBManager.getConnection();
			psmt = con.prepareStatement(_getRoles); 
			rst = psmt.executeQuery();
			while(rst.next()){
				roleIds.add(rst.getInt(1));
			}
			rst.close();
			psmt.close();
			
			psmt = con.prepareStatement(_getIslems); 
			rst = psmt.executeQuery();
			while(rst.next()){
				islemIds.add(rst.getInt(1));
			}
			rst.close();
			psmt.close();
			
			psmt = con.prepareStatement(_getKullanicis); 
			rst = psmt.executeQuery();
			while(rst.next()){
				kullaniciIds.add(rst.getInt(1));
			}
			rst.close();
			psmt.close();
			 
			for(Integer rolId : roleIds){ 
				try {
					 for(Integer islemId : islemIds){
						psmt = con.prepareStatement(_createIslemRoles); 
						psmt.setInt(1, islemId);
						psmt.setInt(2, rolId);
						psmt.executeUpdate();
					 }
					 
					 for(Integer kullaniciId : kullaniciIds){
							psmt = con.prepareStatement(_createKullaniciRoles); 
							psmt.setInt(1, kullaniciId);
							psmt.setInt(2, rolId);
							psmt.executeUpdate();
						 }
				} catch(Exception e){
					System.out.println( rolId + " role could not be added... : " + e.getMessage());
				}  
			}
			rst.close();
			psmt.close();
			
			psmt = con.prepareStatement(_getKullaniciRoles); 
			rst = psmt.executeQuery();
			while(rst.next()){
				kullaniciRoleIds.add(rst.getInt(1));
			}
			rst.close();
			psmt.close();
			
			for(Integer islemId : islemIds){
				try { 
					 for(Integer kullaniciRoleId : kullaniciRoleIds){
							psmt = con.prepareStatement(_createKullaniciIslems); 
							psmt.setInt(1, islemId);
							psmt.setInt(2, kullaniciRoleId);
							psmt.executeUpdate();
						 }
				} catch(Exception e){
					System.out.println( islemId + " islem could not be added...");
				}  
			}
			System.out.println("Authorization mechanism data initalized...");
		} catch(Exception e){
			System.out.println("HATA :" + e.getMessage());
		} finally {
			if(rst != null) {
				try {
					rst.close();
				} catch (SQLException e2) { 
					e2.printStackTrace();
				}
			}
			if(psmt != null) {
				try {
					psmt.close();
				} catch (SQLException e1) { 
					e1.printStackTrace();
				}
			}
			if(con != null) {
				try {
					con.close();
				} catch (SQLException e) { 
					e.printStackTrace();
				}
			}
		}
	}
	
	public static void createIslemList() throws IOException {
		Path path0 = Paths.get("D:/Development/IDE_64/eclipse_juno/Workspace/Toys/JavaSource/tr/com/arf/toys/db/model");
		Collection<Path> allEntityFiles = findJavaClassInGivenPAth(path0, new ArrayList<Path>());
		String fileName = "";
		Connection con = null;
		ResultSet rst = null;
		PreparedStatement psmt = null;	 
		int count = 0;				
		try{  
			con = PostGREDBManager.getConnection();
			for(Path a : allEntityFiles){
				count = 0; 
				fileName = a.getFileName().toString().replace(".java", "");
				psmt = con.prepareStatement("SELECT COUNT(*) FROM ISLEM WHERE UPPER(NAME) = UPPER(?)");
				psmt.setString(1, fileName);
				rst = psmt.executeQuery();				
				while(rst.next()){
					count = rst.getInt(1);
				}
				if(count == 0) {
					System.out.println("Yeni islem :" + fileName);
					psmt = con.prepareStatement("INSERT INTO ISLEM(NAME, ACTIVE) VALUES(?, ?)");
					psmt.setString(1, fileName.toLowerCase(Locale.ENGLISH)); 
					psmt.setInt(2, 1);
					psmt.executeUpdate();	
				}
				rst.close();
				psmt.close();
			}
		} catch(Exception e){
			System.out.println("HATA :" + e.getMessage());
		} finally {
			if(rst != null) {
				try {
					rst.close();
				} catch (SQLException e2) { 
					e2.printStackTrace();
				}
			}
			if(psmt != null) {
				try {
					psmt.close();
				} catch (SQLException e1) { 
					e1.printStackTrace();
				}
			}
			if(con != null) {
				try {
					con.close();
				} catch (SQLException e) { 
					e.printStackTrace();
				}
			}
		} 		
	}
	
	static Collection<Path> findJavaClassInGivenPAth(Path directory, Collection<Path> all)
	        throws IOException {
	    try (DirectoryStream<Path> ds = Files.newDirectoryStream(directory)) {
	        for (Path child : ds) {
	            if (Files.isDirectory(child)) {
	            	if(!child.getFileName().toString().contains("base")){
	            		findJavaClassInGivenPAth(child, all);
	            	}
	            } else {
	            	if(child.getFileName().toString().endsWith(".java")){
	            		all.add(child);
	            	}
	            }
	        }
	    }
	    return all;
	}
}
