--
-- PostgreSQL database dump
--

-- Dumped from database version 8.4.5
-- Dumped by pg_dump version 9.1.4
-- Started on 2013-08-15 10:24:00

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = off;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET escape_string_warning = off;

SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 140 (class 1259 OID 37342)
-- Dependencies: 2417 6
-- Name: adres; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE adres (
    rid bigint NOT NULL,
    adresturu integer NOT NULL,
    kisiref bigint,
    varsayilan integer DEFAULT 2 NOT NULL,
    kurumref bigint,
    adrestipi integer,
    kpsguncellemetarihi date,
    acikadres character varying(255),
    adresno character varying(255),
    binablokadi character varying(255),
    binakodu character varying(255),
    binasiteadi character varying(255),
    csbm character varying(255),
    csbmkodu character varying(255),
    diskapino character varying(255),
    ickapino character varying(255),
    mahalle character varying(255),
    mahallekodu character varying(255),
    sehirref bigint,
    ilceref bigint,
    beyantarihi date,
    binaada character varying(255),
    binapafta character varying(255),
    binaparsel character varying(255),
    tasinmatarihi date,
    tesciltarihi date,
    ulkeref bigint
);


ALTER TABLE public.adres OWNER TO postgres;

--
-- TOC entry 141 (class 1259 OID 37349)
-- Dependencies: 140 6
-- Name: adres_rid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE adres_rid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.adres_rid_seq OWNER TO postgres;

--
-- TOC entry 3052 (class 0 OID 0)
-- Dependencies: 141
-- Name: adres_rid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE adres_rid_seq OWNED BY adres.rid;


--
-- TOC entry 3053 (class 0 OID 0)
-- Dependencies: 141
-- Name: adres_rid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('adres_rid_seq', 80, true);


--
-- TOC entry 142 (class 1259 OID 37351)
-- Dependencies: 2419 6
-- Name: aidat; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE aidat (
    rid bigint NOT NULL,
    baslangictarih date NOT NULL,
    bitistarih date,
    uyetip integer NOT NULL,
    miktar numeric(14,10) NOT NULL,
    yil integer NOT NULL,
    borclandirmayapildi integer DEFAULT 2 NOT NULL
);


ALTER TABLE public.aidat OWNER TO postgres;

--
-- TOC entry 143 (class 1259 OID 37355)
-- Dependencies: 6 142
-- Name: aidat_rid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE aidat_rid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.aidat_rid_seq OWNER TO postgres;

--
-- TOC entry 3054 (class 0 OID 0)
-- Dependencies: 143
-- Name: aidat_rid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE aidat_rid_seq OWNED BY aidat.rid;


--
-- TOC entry 3055 (class 0 OID 0)
-- Dependencies: 143
-- Name: aidat_rid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('aidat_rid_seq', 13, true);


--
-- TOC entry 291 (class 1259 OID 40696)
-- Dependencies: 6
-- Name: anadonem; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE anadonem (
    rid bigint NOT NULL,
    tanim character varying(50),
    aktif integer
);


ALTER TABLE public.anadonem OWNER TO postgres;

--
-- TOC entry 290 (class 1259 OID 40694)
-- Dependencies: 291 6
-- Name: anadonem_rid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE anadonem_rid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.anadonem_rid_seq OWNER TO postgres;

--
-- TOC entry 3056 (class 0 OID 0)
-- Dependencies: 290
-- Name: anadonem_rid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE anadonem_rid_seq OWNED BY anadonem.rid;


--
-- TOC entry 3057 (class 0 OID 0)
-- Dependencies: 290
-- Name: anadonem_rid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('anadonem_rid_seq', 6, true);


--
-- TOC entry 144 (class 1259 OID 37357)
-- Dependencies: 6
-- Name: belgetip; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE belgetip (
    rid bigint NOT NULL,
    kod character varying(30) NOT NULL,
    ad character varying(30) NOT NULL,
    aciklama character varying(200)
);


ALTER TABLE public.belgetip OWNER TO postgres;

--
-- TOC entry 145 (class 1259 OID 37360)
-- Dependencies: 144 6
-- Name: belgetip_rid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE belgetip_rid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.belgetip_rid_seq OWNER TO postgres;

--
-- TOC entry 3058 (class 0 OID 0)
-- Dependencies: 145
-- Name: belgetip_rid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE belgetip_rid_seq OWNED BY belgetip.rid;


--
-- TOC entry 3059 (class 0 OID 0)
-- Dependencies: 145
-- Name: belgetip_rid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('belgetip_rid_seq', 5, true);


--
-- TOC entry 285 (class 1259 OID 38690)
-- Dependencies: 6
-- Name: bilgisayar; Type: TABLE; Schema: public; Owner: toys; Tablespace: 
--

CREATE TABLE bilgisayar (
    rid bigint NOT NULL,
    marka character varying(30) NOT NULL,
    uretimyili bigint NOT NULL
);


ALTER TABLE public.bilgisayar OWNER TO toys;

--
-- TOC entry 284 (class 1259 OID 38688)
-- Dependencies: 6 285
-- Name: bilgisayar_rid_seq; Type: SEQUENCE; Schema: public; Owner: toys
--

CREATE SEQUENCE bilgisayar_rid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.bilgisayar_rid_seq OWNER TO toys;

--
-- TOC entry 3060 (class 0 OID 0)
-- Dependencies: 284
-- Name: bilgisayar_rid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: toys
--

ALTER SEQUENCE bilgisayar_rid_seq OWNED BY bilgisayar.rid;


--
-- TOC entry 3061 (class 0 OID 0)
-- Dependencies: 284
-- Name: bilgisayar_rid_seq; Type: SEQUENCE SET; Schema: public; Owner: toys
--

SELECT pg_catalog.setval('bilgisayar_rid_seq', 3, true);


--
-- TOC entry 146 (class 1259 OID 37362)
-- Dependencies: 6
-- Name: birim; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE birim (
    rid bigint NOT NULL,
    ustref bigint,
    ad character varying(100) NOT NULL,
    kisaad character varying(20),
    durum integer NOT NULL,
    erisimkodu character varying(400),
    birimtip integer NOT NULL,
    ilceref bigint,
    sehirref bigint,
    kurumref bigint NOT NULL
);


ALTER TABLE public.birim OWNER TO postgres;

--
-- TOC entry 147 (class 1259 OID 37368)
-- Dependencies: 6
-- Name: birimhesap; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE birimhesap (
    rid bigint NOT NULL,
    hesapno bigint,
    subekodu bigint,
    ibannumarasi bigint,
    gecerli integer,
    birimref bigint,
    bankaadi character varying(255)
);


ALTER TABLE public.birimhesap OWNER TO postgres;

--
-- TOC entry 148 (class 1259 OID 37371)
-- Dependencies: 6 147
-- Name: birimhesap_rid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE birimhesap_rid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.birimhesap_rid_seq OWNER TO postgres;

--
-- TOC entry 3062 (class 0 OID 0)
-- Dependencies: 148
-- Name: birimhesap_rid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE birimhesap_rid_seq OWNED BY birimhesap.rid;


--
-- TOC entry 3063 (class 0 OID 0)
-- Dependencies: 148
-- Name: birimhesap_rid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('birimhesap_rid_seq', 4, true);


--
-- TOC entry 149 (class 1259 OID 37373)
-- Dependencies: 6
-- Name: bolum; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE bolum (
    rid bigint NOT NULL,
    ad character varying(255) NOT NULL,
    fakulteref bigint
);


ALTER TABLE public.bolum OWNER TO postgres;

--
-- TOC entry 150 (class 1259 OID 37376)
-- Dependencies: 149 6
-- Name: bolum_rid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE bolum_rid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.bolum_rid_seq OWNER TO postgres;

--
-- TOC entry 3064 (class 0 OID 0)
-- Dependencies: 150
-- Name: bolum_rid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE bolum_rid_seq OWNED BY bolum.rid;


--
-- TOC entry 3065 (class 0 OID 0)
-- Dependencies: 150
-- Name: bolum_rid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('bolum_rid_seq', 7, true);


--
-- TOC entry 151 (class 1259 OID 37378)
-- Dependencies: 6
-- Name: borc; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE borc (
    rid bigint NOT NULL,
    kisiref bigint NOT NULL,
    odemetipref bigint NOT NULL,
    tarih date NOT NULL,
    miktar numeric(14,10) NOT NULL,
    odenen numeric(14,10) NOT NULL,
    borcdurum integer NOT NULL,
    birimref bigint
);


ALTER TABLE public.borc OWNER TO postgres;

--
-- TOC entry 152 (class 1259 OID 37381)
-- Dependencies: 151 6
-- Name: borc_rid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE borc_rid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.borc_rid_seq OWNER TO postgres;

--
-- TOC entry 3066 (class 0 OID 0)
-- Dependencies: 152
-- Name: borc_rid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE borc_rid_seq OWNED BY borc.rid;


--
-- TOC entry 3067 (class 0 OID 0)
-- Dependencies: 152
-- Name: borc_rid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('borc_rid_seq', 8, true);


--
-- TOC entry 153 (class 1259 OID 37383)
-- Dependencies: 6
-- Name: borcodeme; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE borcodeme (
    rid bigint NOT NULL,
    borcref bigint NOT NULL,
    tarih date NOT NULL,
    miktar numeric(14,10) NOT NULL,
    odemetip integer NOT NULL,
    odemeref bigint
);


ALTER TABLE public.borcodeme OWNER TO postgres;

--
-- TOC entry 154 (class 1259 OID 37386)
-- Dependencies: 6 153
-- Name: borcodeme_rid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE borcodeme_rid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.borcodeme_rid_seq OWNER TO postgres;

--
-- TOC entry 3068 (class 0 OID 0)
-- Dependencies: 154
-- Name: borcodeme_rid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE borcodeme_rid_seq OWNED BY borcodeme.rid;


--
-- TOC entry 3069 (class 0 OID 0)
-- Dependencies: 154
-- Name: borcodeme_rid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('borcodeme_rid_seq', 5, true);


--
-- TOC entry 155 (class 1259 OID 37388)
-- Dependencies: 6
-- Name: cezatip; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE cezatip (
    rid bigint NOT NULL,
    kisaad character varying(15) NOT NULL,
    ad character varying(30) NOT NULL,
    aciklama character varying(200)
);


ALTER TABLE public.cezatip OWNER TO postgres;

--
-- TOC entry 156 (class 1259 OID 37391)
-- Dependencies: 6 155
-- Name: cezatip_rid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE cezatip_rid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cezatip_rid_seq OWNER TO postgres;

--
-- TOC entry 3070 (class 0 OID 0)
-- Dependencies: 156
-- Name: cezatip_rid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE cezatip_rid_seq OWNED BY cezatip.rid;


--
-- TOC entry 3071 (class 0 OID 0)
-- Dependencies: 156
-- Name: cezatip_rid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('cezatip_rid_seq', 5, true);


--
-- TOC entry 157 (class 1259 OID 37393)
-- Dependencies: 6
-- Name: sehir; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE sehir (
    rid bigint NOT NULL,
    ad character varying(100) NOT NULL,
    ulkeref bigint,
    plakakodu character varying(3) NOT NULL
);


ALTER TABLE public.sehir OWNER TO postgres;

--
-- TOC entry 158 (class 1259 OID 37396)
-- Dependencies: 157 6
-- Name: city_rid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE city_rid_seq
    START WITH 19
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.city_rid_seq OWNER TO postgres;

--
-- TOC entry 3072 (class 0 OID 0)
-- Dependencies: 158
-- Name: city_rid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE city_rid_seq OWNED BY sehir.rid;


--
-- TOC entry 3073 (class 0 OID 0)
-- Dependencies: 158
-- Name: city_rid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('city_rid_seq', 81, true);


--
-- TOC entry 159 (class 1259 OID 37398)
-- Dependencies: 6
-- Name: ulke; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE ulke (
    rid bigint NOT NULL,
    ad character varying(50) NOT NULL,
    turkkokenli integer NOT NULL
);


ALTER TABLE public.ulke OWNER TO postgres;

--
-- TOC entry 160 (class 1259 OID 37401)
-- Dependencies: 6 159
-- Name: country_rid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE country_rid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.country_rid_seq OWNER TO postgres;

--
-- TOC entry 3074 (class 0 OID 0)
-- Dependencies: 160
-- Name: country_rid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE country_rid_seq OWNED BY ulke.rid;


--
-- TOC entry 3075 (class 0 OID 0)
-- Dependencies: 160
-- Name: country_rid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('country_rid_seq', 2, true);


--
-- TOC entry 161 (class 1259 OID 37403)
-- Dependencies: 6
-- Name: ilce; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE ilce (
    rid bigint NOT NULL,
    ad character varying(100),
    sehirref bigint,
    ilcekodu bigint
);


ALTER TABLE public.ilce OWNER TO postgres;

--
-- TOC entry 162 (class 1259 OID 37406)
-- Dependencies: 6 161
-- Name: district_rid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE district_rid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.district_rid_seq OWNER TO postgres;

--
-- TOC entry 3076 (class 0 OID 0)
-- Dependencies: 162
-- Name: district_rid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE district_rid_seq OWNED BY ilce.rid;


--
-- TOC entry 3077 (class 0 OID 0)
-- Dependencies: 162
-- Name: district_rid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('district_rid_seq', 1077, true);


--
-- TOC entry 163 (class 1259 OID 37408)
-- Dependencies: 2432 6
-- Name: donem; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE donem (
    rid bigint NOT NULL,
    baslangictarih date NOT NULL,
    bitistarih date,
    tanim character varying(255),
    anadonemref bigint NOT NULL,
    birimref bigint DEFAULT 1 NOT NULL
);


ALTER TABLE public.donem OWNER TO postgres;

--
-- TOC entry 164 (class 1259 OID 37411)
-- Dependencies: 6 163
-- Name: donem_rid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE donem_rid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.donem_rid_seq OWNER TO postgres;

--
-- TOC entry 3078 (class 0 OID 0)
-- Dependencies: 164
-- Name: donem_rid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE donem_rid_seq OWNED BY donem.rid;


--
-- TOC entry 3079 (class 0 OID 0)
-- Dependencies: 164
-- Name: donem_rid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('donem_rid_seq', 10, true);


--
-- TOC entry 279 (class 1259 OID 38595)
-- Dependencies: 6
-- Name: dosyakodu; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE dosyakodu (
    rid bigint NOT NULL,
    ustref bigint,
    kod character varying(400) NOT NULL,
    ad character varying(400) NOT NULL,
    erisimkodu character varying(400)
);


ALTER TABLE public.dosyakodu OWNER TO postgres;

--
-- TOC entry 278 (class 1259 OID 38593)
-- Dependencies: 279 6
-- Name: dosyakodu_rid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE dosyakodu_rid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.dosyakodu_rid_seq OWNER TO postgres;

--
-- TOC entry 3080 (class 0 OID 0)
-- Dependencies: 278
-- Name: dosyakodu_rid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE dosyakodu_rid_seq OWNED BY dosyakodu.rid;


--
-- TOC entry 3081 (class 0 OID 0)
-- Dependencies: 278
-- Name: dosyakodu_rid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('dosyakodu_rid_seq', 1048, true);


--
-- TOC entry 165 (class 1259 OID 37413)
-- Dependencies: 6
-- Name: duyuru; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE duyuru (
    rid bigint NOT NULL,
    duyurubasligi character varying(100) NOT NULL,
    duyurumetni character varying(400),
    bitistarih date,
    baslangictarih date
);


ALTER TABLE public.duyuru OWNER TO postgres;

--
-- TOC entry 166 (class 1259 OID 37419)
-- Dependencies: 165 6
-- Name: duyuru_rid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE duyuru_rid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.duyuru_rid_seq OWNER TO postgres;

--
-- TOC entry 3082 (class 0 OID 0)
-- Dependencies: 166
-- Name: duyuru_rid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE duyuru_rid_seq OWNED BY duyuru.rid;


--
-- TOC entry 3083 (class 0 OID 0)
-- Dependencies: 166
-- Name: duyuru_rid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('duyuru_rid_seq', 5, true);


--
-- TOC entry 335 (class 1259 OID 45265)
-- Dependencies: 6
-- Name: egitim; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE egitim (
    rid bigint NOT NULL,
    egitimtanimref bigint NOT NULL,
    ad character varying(200) NOT NULL,
    tarih date NOT NULL,
    birimref bigint NOT NULL,
    sehirref bigint NOT NULL,
    ilceref bigint NOT NULL,
    yer character varying(2500) NOT NULL,
    kontenjan integer NOT NULL,
    sinavli integer NOT NULL,
    sinavyeri character varying(500) NOT NULL,
    sinavtarihi date NOT NULL,
    durum integer NOT NULL
);


ALTER TABLE public.egitim OWNER TO postgres;

--
-- TOC entry 334 (class 1259 OID 45263)
-- Dependencies: 6 335
-- Name: egitim_rid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE egitim_rid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.egitim_rid_seq OWNER TO postgres;

--
-- TOC entry 3084 (class 0 OID 0)
-- Dependencies: 334
-- Name: egitim_rid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE egitim_rid_seq OWNED BY egitim.rid;


--
-- TOC entry 3085 (class 0 OID 0)
-- Dependencies: 334
-- Name: egitim_rid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('egitim_rid_seq', 1, false);


--
-- TOC entry 345 (class 1259 OID 45375)
-- Dependencies: 6
-- Name: egitimdegerlendirme; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE egitimdegerlendirme (
    rid bigint NOT NULL,
    egitimkatilimciref bigint NOT NULL,
    soru integer NOT NULL,
    cevap integer NOT NULL,
    aciklama character varying(500)
);


ALTER TABLE public.egitimdegerlendirme OWNER TO postgres;

--
-- TOC entry 344 (class 1259 OID 45373)
-- Dependencies: 345 6
-- Name: egitimdegerlendirme_rid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE egitimdegerlendirme_rid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.egitimdegerlendirme_rid_seq OWNER TO postgres;

--
-- TOC entry 3086 (class 0 OID 0)
-- Dependencies: 344
-- Name: egitimdegerlendirme_rid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE egitimdegerlendirme_rid_seq OWNED BY egitimdegerlendirme.rid;


--
-- TOC entry 3087 (class 0 OID 0)
-- Dependencies: 344
-- Name: egitimdegerlendirme_rid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('egitimdegerlendirme_rid_seq', 1, false);


--
-- TOC entry 339 (class 1259 OID 45317)
-- Dependencies: 6
-- Name: egitimders; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE egitimders (
    rid bigint NOT NULL,
    egitimref bigint NOT NULL,
    egitimogretmenref bigint,
    zaman timestamp without time zone NOT NULL,
    aciklama character varying(500)
);


ALTER TABLE public.egitimders OWNER TO postgres;

--
-- TOC entry 338 (class 1259 OID 45315)
-- Dependencies: 339 6
-- Name: egitimders_rid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE egitimders_rid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.egitimders_rid_seq OWNER TO postgres;

--
-- TOC entry 3088 (class 0 OID 0)
-- Dependencies: 338
-- Name: egitimders_rid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE egitimders_rid_seq OWNED BY egitimders.rid;


--
-- TOC entry 3089 (class 0 OID 0)
-- Dependencies: 338
-- Name: egitimders_rid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('egitimders_rid_seq', 1, false);


--
-- TOC entry 341 (class 1259 OID 45338)
-- Dependencies: 6
-- Name: egitimdosya; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE egitimdosya (
    rid bigint NOT NULL,
    egitimref bigint NOT NULL,
    ad character varying(200) NOT NULL,
    path character varying(2500) NOT NULL
);


ALTER TABLE public.egitimdosya OWNER TO postgres;

--
-- TOC entry 340 (class 1259 OID 45336)
-- Dependencies: 6 341
-- Name: egitimdosya_rid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE egitimdosya_rid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.egitimdosya_rid_seq OWNER TO postgres;

--
-- TOC entry 3090 (class 0 OID 0)
-- Dependencies: 340
-- Name: egitimdosya_rid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE egitimdosya_rid_seq OWNED BY egitimdosya.rid;


--
-- TOC entry 3091 (class 0 OID 0)
-- Dependencies: 340
-- Name: egitimdosya_rid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('egitimdosya_rid_seq', 1, false);


--
-- TOC entry 343 (class 1259 OID 45354)
-- Dependencies: 6
-- Name: egitimkatilimci; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE egitimkatilimci (
    rid bigint NOT NULL,
    egitimref bigint NOT NULL,
    kisiref bigint NOT NULL,
    durum integer NOT NULL,
    katilimbelgesi integer NOT NULL,
    sertifika integer NOT NULL,
    sinavnotu character varying(10),
    sinavbasaridurum integer NOT NULL,
    aciklama character varying(500)
);


ALTER TABLE public.egitimkatilimci OWNER TO postgres;

--
-- TOC entry 342 (class 1259 OID 45352)
-- Dependencies: 343 6
-- Name: egitimkatilimci_rid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE egitimkatilimci_rid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.egitimkatilimci_rid_seq OWNER TO postgres;

--
-- TOC entry 3092 (class 0 OID 0)
-- Dependencies: 342
-- Name: egitimkatilimci_rid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE egitimkatilimci_rid_seq OWNED BY egitimkatilimci.rid;


--
-- TOC entry 3093 (class 0 OID 0)
-- Dependencies: 342
-- Name: egitimkatilimci_rid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('egitimkatilimci_rid_seq', 1, false);


--
-- TOC entry 337 (class 1259 OID 45296)
-- Dependencies: 6
-- Name: egitimogretmen; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE egitimogretmen (
    rid bigint NOT NULL,
    egitimref bigint NOT NULL,
    kisiref bigint NOT NULL,
    aciklama character varying(500)
);


ALTER TABLE public.egitimogretmen OWNER TO postgres;

--
-- TOC entry 336 (class 1259 OID 45294)
-- Dependencies: 6 337
-- Name: egitimogretmen_rid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE egitimogretmen_rid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.egitimogretmen_rid_seq OWNER TO postgres;

--
-- TOC entry 3094 (class 0 OID 0)
-- Dependencies: 336
-- Name: egitimogretmen_rid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE egitimogretmen_rid_seq OWNED BY egitimogretmen.rid;


--
-- TOC entry 3095 (class 0 OID 0)
-- Dependencies: 336
-- Name: egitimogretmen_rid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('egitimogretmen_rid_seq', 1, false);


--
-- TOC entry 333 (class 1259 OID 45254)
-- Dependencies: 6
-- Name: egitimtanim; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE egitimtanim (
    rid bigint NOT NULL,
    ad character varying(200) NOT NULL,
    egitimturu integer NOT NULL,
    aciklama character varying(2500)
);


ALTER TABLE public.egitimtanim OWNER TO postgres;

--
-- TOC entry 332 (class 1259 OID 45252)
-- Dependencies: 6 333
-- Name: egitimtanim_rid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE egitimtanim_rid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.egitimtanim_rid_seq OWNER TO postgres;

--
-- TOC entry 3096 (class 0 OID 0)
-- Dependencies: 332
-- Name: egitimtanim_rid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE egitimtanim_rid_seq OWNED BY egitimtanim.rid;


--
-- TOC entry 3097 (class 0 OID 0)
-- Dependencies: 332
-- Name: egitimtanim_rid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('egitimtanim_rid_seq', 1, false);


--
-- TOC entry 315 (class 1259 OID 45076)
-- Dependencies: 6
-- Name: epostaanlikgonderimlog; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE epostaanlikgonderimlog (
    rid bigint NOT NULL,
    kisiref bigint NOT NULL,
    icerikref bigint NOT NULL,
    tarih date NOT NULL,
    gonderenref bigint NOT NULL,
    durum integer NOT NULL,
    aciklama character varying(2500)
);


ALTER TABLE public.epostaanlikgonderimlog OWNER TO postgres;

--
-- TOC entry 314 (class 1259 OID 45074)
-- Dependencies: 315 6
-- Name: epostaanlikgonderimlog_rid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE epostaanlikgonderimlog_rid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.epostaanlikgonderimlog_rid_seq OWNER TO postgres;

--
-- TOC entry 3098 (class 0 OID 0)
-- Dependencies: 314
-- Name: epostaanlikgonderimlog_rid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE epostaanlikgonderimlog_rid_seq OWNED BY epostaanlikgonderimlog.rid;


--
-- TOC entry 3099 (class 0 OID 0)
-- Dependencies: 314
-- Name: epostaanlikgonderimlog_rid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('epostaanlikgonderimlog_rid_seq', 1, false);


--
-- TOC entry 311 (class 1259 OID 45024)
-- Dependencies: 6
-- Name: epostagonderimlog; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE epostagonderimlog (
    rid bigint NOT NULL,
    gonderimref bigint NOT NULL,
    kisiref bigint NOT NULL,
    tarih date NOT NULL,
    gonderenref bigint NOT NULL,
    durum integer NOT NULL,
    aciklama character varying(2500)
);


ALTER TABLE public.epostagonderimlog OWNER TO postgres;

--
-- TOC entry 310 (class 1259 OID 45022)
-- Dependencies: 6 311
-- Name: epostagonderimlog_rid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE epostagonderimlog_rid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.epostagonderimlog_rid_seq OWNER TO postgres;

--
-- TOC entry 3100 (class 0 OID 0)
-- Dependencies: 310
-- Name: epostagonderimlog_rid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE epostagonderimlog_rid_seq OWNED BY epostagonderimlog.rid;


--
-- TOC entry 3101 (class 0 OID 0)
-- Dependencies: 310
-- Name: epostagonderimlog_rid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('epostagonderimlog_rid_seq', 1, false);


--
-- TOC entry 321 (class 1259 OID 45140)
-- Dependencies: 6
-- Name: etkinlik; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE etkinlik (
    rid bigint NOT NULL,
    etkinliktanimref bigint NOT NULL,
    ad character varying(200) NOT NULL,
    tarih date NOT NULL,
    sehirref bigint NOT NULL,
    ilceref bigint NOT NULL,
    yer character varying(2500) NOT NULL,
    webadresi character varying(500),
    epostaadresi character varying(500),
    telefonno character varying(500),
    sorumlukisiref bigint,
    durum integer NOT NULL
);


ALTER TABLE public.etkinlik OWNER TO postgres;

--
-- TOC entry 320 (class 1259 OID 45138)
-- Dependencies: 321 6
-- Name: etkinlik_rid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE etkinlik_rid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.etkinlik_rid_seq OWNER TO postgres;

--
-- TOC entry 3102 (class 0 OID 0)
-- Dependencies: 320
-- Name: etkinlik_rid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE etkinlik_rid_seq OWNED BY etkinlik.rid;


--
-- TOC entry 3103 (class 0 OID 0)
-- Dependencies: 320
-- Name: etkinlik_rid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('etkinlik_rid_seq', 1, false);


--
-- TOC entry 331 (class 1259 OID 45238)
-- Dependencies: 6
-- Name: etkinlikdosya; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE etkinlikdosya (
    rid bigint NOT NULL,
    etkinlikref bigint NOT NULL,
    dosyaturu integer NOT NULL,
    ad character varying(200) NOT NULL,
    path character varying(2500) NOT NULL
);


ALTER TABLE public.etkinlikdosya OWNER TO postgres;

--
-- TOC entry 330 (class 1259 OID 45236)
-- Dependencies: 331 6
-- Name: etkinlikdosya_rid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE etkinlikdosya_rid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.etkinlikdosya_rid_seq OWNER TO postgres;

--
-- TOC entry 3104 (class 0 OID 0)
-- Dependencies: 330
-- Name: etkinlikdosya_rid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE etkinlikdosya_rid_seq OWNED BY etkinlikdosya.rid;


--
-- TOC entry 3105 (class 0 OID 0)
-- Dependencies: 330
-- Name: etkinlikdosya_rid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('etkinlikdosya_rid_seq', 1, false);


--
-- TOC entry 323 (class 1259 OID 45171)
-- Dependencies: 6
-- Name: etkinlikkatilimci; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE etkinlikkatilimci (
    rid bigint NOT NULL,
    etkinlikref bigint NOT NULL,
    kisiref bigint NOT NULL,
    durum integer
);


ALTER TABLE public.etkinlikkatilimci OWNER TO postgres;

--
-- TOC entry 322 (class 1259 OID 45169)
-- Dependencies: 323 6
-- Name: etkinlikkatilimci_rid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE etkinlikkatilimci_rid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.etkinlikkatilimci_rid_seq OWNER TO postgres;

--
-- TOC entry 3106 (class 0 OID 0)
-- Dependencies: 322
-- Name: etkinlikkatilimci_rid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE etkinlikkatilimci_rid_seq OWNED BY etkinlikkatilimci.rid;


--
-- TOC entry 3107 (class 0 OID 0)
-- Dependencies: 322
-- Name: etkinlikkatilimci_rid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('etkinlikkatilimci_rid_seq', 1, false);


--
-- TOC entry 327 (class 1259 OID 45207)
-- Dependencies: 6
-- Name: etkinlikkurul; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE etkinlikkurul (
    rid bigint NOT NULL,
    etkinlikref bigint NOT NULL,
    kurulturu integer NOT NULL
);


ALTER TABLE public.etkinlikkurul OWNER TO postgres;

--
-- TOC entry 326 (class 1259 OID 45205)
-- Dependencies: 327 6
-- Name: etkinlikkurul_rid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE etkinlikkurul_rid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.etkinlikkurul_rid_seq OWNER TO postgres;

--
-- TOC entry 3108 (class 0 OID 0)
-- Dependencies: 326
-- Name: etkinlikkurul_rid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE etkinlikkurul_rid_seq OWNED BY etkinlikkurul.rid;


--
-- TOC entry 3109 (class 0 OID 0)
-- Dependencies: 326
-- Name: etkinlikkurul_rid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('etkinlikkurul_rid_seq', 1, false);


--
-- TOC entry 329 (class 1259 OID 45220)
-- Dependencies: 6
-- Name: etkinlikkuruluye; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE etkinlikkuruluye (
    rid bigint NOT NULL,
    etkinlikkurulref bigint NOT NULL,
    uyeturu integer NOT NULL,
    uyeref bigint NOT NULL
);


ALTER TABLE public.etkinlikkuruluye OWNER TO postgres;

--
-- TOC entry 328 (class 1259 OID 45218)
-- Dependencies: 6 329
-- Name: etkinlikkuruluye_rid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE etkinlikkuruluye_rid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.etkinlikkuruluye_rid_seq OWNER TO postgres;

--
-- TOC entry 3110 (class 0 OID 0)
-- Dependencies: 328
-- Name: etkinlikkuruluye_rid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE etkinlikkuruluye_rid_seq OWNED BY etkinlikkuruluye.rid;


--
-- TOC entry 3111 (class 0 OID 0)
-- Dependencies: 328
-- Name: etkinlikkuruluye_rid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('etkinlikkuruluye_rid_seq', 1, false);


--
-- TOC entry 325 (class 1259 OID 45189)
-- Dependencies: 6
-- Name: etkinlikkurum; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE etkinlikkurum (
    rid bigint NOT NULL,
    etkinlikref bigint NOT NULL,
    kurumref bigint NOT NULL,
    turu integer NOT NULL
);


ALTER TABLE public.etkinlikkurum OWNER TO postgres;

--
-- TOC entry 324 (class 1259 OID 45187)
-- Dependencies: 325 6
-- Name: etkinlikkurum_rid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE etkinlikkurum_rid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.etkinlikkurum_rid_seq OWNER TO postgres;

--
-- TOC entry 3112 (class 0 OID 0)
-- Dependencies: 324
-- Name: etkinlikkurum_rid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE etkinlikkurum_rid_seq OWNED BY etkinlikkurum.rid;


--
-- TOC entry 3113 (class 0 OID 0)
-- Dependencies: 324
-- Name: etkinlikkurum_rid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('etkinlikkurum_rid_seq', 1, false);


--
-- TOC entry 319 (class 1259 OID 45129)
-- Dependencies: 6
-- Name: etkinliktanim; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE etkinliktanim (
    rid bigint NOT NULL,
    ad character varying(200) NOT NULL,
    periyodik integer NOT NULL,
    aciklama character varying(2500)
);


ALTER TABLE public.etkinliktanim OWNER TO postgres;

--
-- TOC entry 318 (class 1259 OID 45127)
-- Dependencies: 6 319
-- Name: etkinliktanim_rid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE etkinliktanim_rid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.etkinliktanim_rid_seq OWNER TO postgres;

--
-- TOC entry 3114 (class 0 OID 0)
-- Dependencies: 318
-- Name: etkinliktanim_rid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE etkinliktanim_rid_seq OWNED BY etkinliktanim.rid;


--
-- TOC entry 3115 (class 0 OID 0)
-- Dependencies: 318
-- Name: etkinliktanim_rid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('etkinliktanim_rid_seq', 1, false);


--
-- TOC entry 281 (class 1259 OID 38613)
-- Dependencies: 6
-- Name: evrak; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE evrak (
    rid bigint NOT NULL,
    yil integer NOT NULL,
    evrakHareketTuru integer NOT NULL,
    kayittarihi timestamp without time zone NOT NULL,
    kayitno character varying(100) NOT NULL,
    gonderenkurumref bigint,
    gonderenkisiref bigint,
    dosyakoduref bigint NOT NULL,
    konusu character varying(2500) NOT NULL,
    evraktarihi date,
    evrakno character varying(100),
    evrakturu integer NOT NULL,
    teslimturu integer NOT NULL,
    gizlilikderecesi integer NOT NULL,
    aciliyetturu integer NOT NULL,
    durum integer NOT NULL,
    hazirlayanpersonelref bigint,
    takdimmakami character varying(2500),
    anahtarkelime character varying(2500),
    path character varying(2500),
    icerik character varying(25000),
    gittigikurumref bigint,
    gittigibirimref bigint
);


ALTER TABLE public.evrak OWNER TO postgres;

--
-- TOC entry 280 (class 1259 OID 38611)
-- Dependencies: 6 281
-- Name: evrak_rid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE evrak_rid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.evrak_rid_seq OWNER TO postgres;

--
-- TOC entry 3116 (class 0 OID 0)
-- Dependencies: 280
-- Name: evrak_rid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE evrak_rid_seq OWNED BY evrak.rid;


--
-- TOC entry 3117 (class 0 OID 0)
-- Dependencies: 280
-- Name: evrak_rid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('evrak_rid_seq', 6, true);


--
-- TOC entry 283 (class 1259 OID 38674)
-- Dependencies: 6
-- Name: evrakek; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE evrakek (
    rid bigint NOT NULL,
    evrakref bigint NOT NULL,
    path character varying(2500) NOT NULL,
    tanim character varying(2500) NOT NULL
);


ALTER TABLE public.evrakek OWNER TO postgres;

--
-- TOC entry 282 (class 1259 OID 38672)
-- Dependencies: 283 6
-- Name: evrakek_rid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE evrakek_rid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.evrakek_rid_seq OWNER TO postgres;

--
-- TOC entry 3118 (class 0 OID 0)
-- Dependencies: 282
-- Name: evrakek_rid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE evrakek_rid_seq OWNED BY evrakek.rid;


--
-- TOC entry 3119 (class 0 OID 0)
-- Dependencies: 282
-- Name: evrakek_rid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('evrakek_rid_seq', 9, true);


--
-- TOC entry 167 (class 1259 OID 37429)
-- Dependencies: 6
-- Name: fakulte; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE fakulte (
    rid bigint NOT NULL,
    ad character varying(255) NOT NULL,
    universiteref bigint
);


ALTER TABLE public.fakulte OWNER TO postgres;

--
-- TOC entry 168 (class 1259 OID 37432)
-- Dependencies: 167 6
-- Name: fakulte_rid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE fakulte_rid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.fakulte_rid_seq OWNER TO postgres;

--
-- TOC entry 3120 (class 0 OID 0)
-- Dependencies: 168
-- Name: fakulte_rid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE fakulte_rid_seq OWNED BY fakulte.rid;


--
-- TOC entry 3121 (class 0 OID 0)
-- Dependencies: 168
-- Name: fakulte_rid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('fakulte_rid_seq', 13, true);


--
-- TOC entry 309 (class 1259 OID 45001)
-- Dependencies: 6
-- Name: gonderim; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE gonderim (
    rid bigint NOT NULL,
    iletisimlisteref bigint NOT NULL,
    icerikref bigint NOT NULL,
    gonderimturu integer NOT NULL,
    gonderimzamani date NOT NULL,
    zamanlamali integer NOT NULL,
    durum integer NOT NULL,
    gonderenref bigint NOT NULL
);


ALTER TABLE public.gonderim OWNER TO postgres;

--
-- TOC entry 308 (class 1259 OID 44999)
-- Dependencies: 6 309
-- Name: gonderim_rid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE gonderim_rid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.gonderim_rid_seq OWNER TO postgres;

--
-- TOC entry 3122 (class 0 OID 0)
-- Dependencies: 308
-- Name: gonderim_rid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE gonderim_rid_seq OWNED BY gonderim.rid;


--
-- TOC entry 3123 (class 0 OID 0)
-- Dependencies: 308
-- Name: gonderim_rid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('gonderim_rid_seq', 1, false);


--
-- TOC entry 169 (class 1259 OID 37434)
-- Dependencies: 6
-- Name: gorev; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE gorev (
    rid bigint NOT NULL,
    ustref bigint,
    ad character varying(50) NOT NULL,
    aciklama character varying(500) NOT NULL,
    erisimkodu character varying(400)
);


ALTER TABLE public.gorev OWNER TO postgres;

--
-- TOC entry 170 (class 1259 OID 37440)
-- Dependencies: 6 169
-- Name: gorev_rid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE gorev_rid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.gorev_rid_seq OWNER TO postgres;

--
-- TOC entry 3124 (class 0 OID 0)
-- Dependencies: 170
-- Name: gorev_rid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE gorev_rid_seq OWNED BY gorev.rid;


--
-- TOC entry 3125 (class 0 OID 0)
-- Dependencies: 170
-- Name: gorev_rid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('gorev_rid_seq', 6, true);


--
-- TOC entry 307 (class 1259 OID 44990)
-- Dependencies: 6
-- Name: icerik; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE icerik (
    rid bigint NOT NULL,
    icerik text NOT NULL,
    gonderimturu integer NOT NULL,
    icerikturu integer NOT NULL
);


ALTER TABLE public.icerik OWNER TO postgres;

--
-- TOC entry 306 (class 1259 OID 44988)
-- Dependencies: 6 307
-- Name: icerik_rid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE icerik_rid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.icerik_rid_seq OWNER TO postgres;

--
-- TOC entry 3126 (class 0 OID 0)
-- Dependencies: 306
-- Name: icerik_rid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE icerik_rid_seq OWNED BY icerik.rid;


--
-- TOC entry 3127 (class 0 OID 0)
-- Dependencies: 306
-- Name: icerik_rid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('icerik_rid_seq', 1, false);


--
-- TOC entry 303 (class 1259 OID 44956)
-- Dependencies: 6
-- Name: iletisimliste; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE iletisimliste (
    rid bigint NOT NULL,
    ad character varying(100) NOT NULL,
    ustref bigint NOT NULL,
    erisimkodu character varying(400),
    birimref bigint NOT NULL,
    listegizlilikturu integer NOT NULL,
    aciklama character varying(2500) NOT NULL
);


ALTER TABLE public.iletisimliste OWNER TO postgres;

--
-- TOC entry 302 (class 1259 OID 44954)
-- Dependencies: 303 6
-- Name: iletisimliste_rid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE iletisimliste_rid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.iletisimliste_rid_seq OWNER TO postgres;

--
-- TOC entry 3128 (class 0 OID 0)
-- Dependencies: 302
-- Name: iletisimliste_rid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE iletisimliste_rid_seq OWNED BY iletisimliste.rid;


--
-- TOC entry 3129 (class 0 OID 0)
-- Dependencies: 302
-- Name: iletisimliste_rid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('iletisimliste_rid_seq', 1, false);


--
-- TOC entry 305 (class 1259 OID 44977)
-- Dependencies: 6
-- Name: iletisimlistedetay; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE iletisimlistedetay (
    rid bigint NOT NULL,
    referanstipi integer NOT NULL,
    referans bigint NOT NULL,
    iletisimlisteref bigint NOT NULL
);


ALTER TABLE public.iletisimlistedetay OWNER TO postgres;

--
-- TOC entry 304 (class 1259 OID 44975)
-- Dependencies: 6 305
-- Name: iletisimlistedetay_rid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE iletisimlistedetay_rid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.iletisimlistedetay_rid_seq OWNER TO postgres;

--
-- TOC entry 3130 (class 0 OID 0)
-- Dependencies: 304
-- Name: iletisimlistedetay_rid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE iletisimlistedetay_rid_seq OWNED BY iletisimlistedetay.rid;


--
-- TOC entry 3131 (class 0 OID 0)
-- Dependencies: 304
-- Name: iletisimlistedetay_rid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('iletisimlistedetay_rid_seq', 1, false);


--
-- TOC entry 171 (class 1259 OID 37442)
-- Dependencies: 2436 6
-- Name: internetadres; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE internetadres (
    rid bigint NOT NULL,
    kisiref bigint,
    netadresmetni character varying(500) NOT NULL,
    netadresturu integer NOT NULL,
    varsayilan integer DEFAULT 2 NOT NULL,
    kurumref bigint
);


ALTER TABLE public.internetadres OWNER TO postgres;

--
-- TOC entry 172 (class 1259 OID 37449)
-- Dependencies: 6 171
-- Name: internetadres_rid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE internetadres_rid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.internetadres_rid_seq OWNER TO postgres;

--
-- TOC entry 3132 (class 0 OID 0)
-- Dependencies: 172
-- Name: internetadres_rid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE internetadres_rid_seq OWNED BY internetadres.rid;


--
-- TOC entry 3133 (class 0 OID 0)
-- Dependencies: 172
-- Name: internetadres_rid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('internetadres_rid_seq', 44, true);


--
-- TOC entry 173 (class 1259 OID 37451)
-- Dependencies: 6
-- Name: islem; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE islem (
    rid bigint NOT NULL,
    name character varying(50) NOT NULL,
    active integer NOT NULL
);


ALTER TABLE public.islem OWNER TO postgres;

--
-- TOC entry 174 (class 1259 OID 37454)
-- Dependencies: 2439 6
-- Name: islemkullanici; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE islemkullanici (
    rid bigint NOT NULL,
    updatepermission integer NOT NULL,
    deletepermission integer NOT NULL,
    listpermission integer DEFAULT 2 NOT NULL,
    islemref bigint,
    kullaniciroleref bigint
);


ALTER TABLE public.islemkullanici OWNER TO postgres;

--
-- TOC entry 175 (class 1259 OID 37458)
-- Dependencies: 2441 2442 6
-- Name: islemlog; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE islemlog (
    rid bigint NOT NULL,
    islemturu integer NOT NULL,
    islemtarihi timestamp without time zone NOT NULL,
    tabloadi character varying(255) NOT NULL,
    referansrid bigint NOT NULL,
    eskideger text,
    yenideger text,
    kullanicirid bigint DEFAULT 1 NOT NULL,
    kullanicitext character varying(500) DEFAULT '-'::character varying NOT NULL
)
WITH (autovacuum_enabled=true);


ALTER TABLE public.islemlog OWNER TO postgres;

--
-- TOC entry 176 (class 1259 OID 37464)
-- Dependencies: 175 6
-- Name: islemlog_rid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE islemlog_rid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.islemlog_rid_seq OWNER TO postgres;

--
-- TOC entry 3134 (class 0 OID 0)
-- Dependencies: 176
-- Name: islemlog_rid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE islemlog_rid_seq OWNED BY islemlog.rid;


--
-- TOC entry 3135 (class 0 OID 0)
-- Dependencies: 176
-- Name: islemlog_rid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('islemlog_rid_seq', 422, true);


--
-- TOC entry 177 (class 1259 OID 37466)
-- Dependencies: 2444 6
-- Name: islemrole; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE islemrole (
    rid bigint NOT NULL,
    roleref bigint NOT NULL,
    updatepermission integer NOT NULL,
    deletepermission integer NOT NULL,
    listpermission integer DEFAULT 2 NOT NULL,
    islemref bigint
);


ALTER TABLE public.islemrole OWNER TO postgres;

--
-- TOC entry 178 (class 1259 OID 37470)
-- Dependencies: 6
-- Name: isyeritemsilcileri; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE isyeritemsilcileri (
    rid bigint NOT NULL,
    uyeref bigint,
    kurumref bigint,
    aktif integer
);


ALTER TABLE public.isyeritemsilcileri OWNER TO postgres;

--
-- TOC entry 179 (class 1259 OID 37473)
-- Dependencies: 178 6
-- Name: isyeritemsilcileri_rid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE isyeritemsilcileri_rid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.isyeritemsilcileri_rid_seq OWNER TO postgres;

--
-- TOC entry 3136 (class 0 OID 0)
-- Dependencies: 179
-- Name: isyeritemsilcileri_rid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE isyeritemsilcileri_rid_seq OWNED BY isyeritemsilcileri.rid;


--
-- TOC entry 3137 (class 0 OID 0)
-- Dependencies: 179
-- Name: isyeritemsilcileri_rid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('isyeritemsilcileri_rid_seq', 7, true);


--
-- TOC entry 180 (class 1259 OID 37475)
-- Dependencies: 2447 6
-- Name: kisi; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE kisi (
    rid bigint NOT NULL,
    kimlikno character varying(20) NOT NULL,
    ad character varying(50) NOT NULL,
    soyad character varying(50) NOT NULL,
    kimliknotip integer NOT NULL,
    aktif integer DEFAULT 1 NOT NULL
);


ALTER TABLE public.kisi OWNER TO postgres;

--
-- TOC entry 181 (class 1259 OID 37479)
-- Dependencies: 6 180
-- Name: kisi_rid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE kisi_rid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.kisi_rid_seq OWNER TO postgres;

--
-- TOC entry 3138 (class 0 OID 0)
-- Dependencies: 181
-- Name: kisi_rid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE kisi_rid_seq OWNED BY kisi.rid;


--
-- TOC entry 3139 (class 0 OID 0)
-- Dependencies: 181
-- Name: kisi_rid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('kisi_rid_seq', 90, true);


--
-- TOC entry 182 (class 1259 OID 37481)
-- Dependencies: 6
-- Name: kisifotograf; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE kisifotograf (
    rid bigint NOT NULL,
    kisiref bigint NOT NULL,
    path character varying(100),
    varsayilan integer
);


ALTER TABLE public.kisifotograf OWNER TO postgres;

--
-- TOC entry 183 (class 1259 OID 37484)
-- Dependencies: 182 6
-- Name: kisifotograf_rid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE kisifotograf_rid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.kisifotograf_rid_seq OWNER TO postgres;

--
-- TOC entry 3140 (class 0 OID 0)
-- Dependencies: 183
-- Name: kisifotograf_rid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE kisifotograf_rid_seq OWNED BY kisifotograf.rid;


--
-- TOC entry 3141 (class 0 OID 0)
-- Dependencies: 183
-- Name: kisifotograf_rid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('kisifotograf_rid_seq', 21, true);


--
-- TOC entry 184 (class 1259 OID 37486)
-- Dependencies: 6
-- Name: kisiizin; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE kisiizin (
    rid bigint NOT NULL,
    kisiref bigint NOT NULL,
    evrakref bigint NOT NULL,
    baslangictarih date NOT NULL,
    bitistarih date NOT NULL,
    kisiizindurum integer NOT NULL,
    aciklama character varying(200)
);


ALTER TABLE public.kisiizin OWNER TO postgres;

--
-- TOC entry 185 (class 1259 OID 37489)
-- Dependencies: 184 6
-- Name: kisiizin_rid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE kisiizin_rid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.kisiizin_rid_seq OWNER TO postgres;

--
-- TOC entry 3142 (class 0 OID 0)
-- Dependencies: 185
-- Name: kisiizin_rid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE kisiizin_rid_seq OWNED BY kisiizin.rid;


--
-- TOC entry 3143 (class 0 OID 0)
-- Dependencies: 185
-- Name: kisiizin_rid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('kisiizin_rid_seq', 5, true);


--
-- TOC entry 186 (class 1259 OID 37491)
-- Dependencies: 6
-- Name: kisikimlik; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE kisikimlik (
    rid bigint NOT NULL,
    kisiref bigint NOT NULL,
    kimlikno character varying(20) NOT NULL,
    babaad character varying(20) NOT NULL,
    anaad character varying(20) NOT NULL,
    dogumilceref bigint,
    dogumyer character varying(20) NOT NULL,
    dogumtarih date NOT NULL,
    cinsiyet integer NOT NULL,
    medenihal integer NOT NULL,
    sehirref bigint NOT NULL,
    ilceref bigint NOT NULL,
    ciltno character varying(15) NOT NULL,
    aileno character varying(5) NOT NULL,
    sirano character varying(5) NOT NULL,
    kisidurum integer,
    olumtarih date,
    estckimlikno character varying(20),
    annetckimlikno character varying(20),
    babatckimlikno character varying(20),
    ciltaciklama character varying(100),
    verildigiyer character varying(15),
    verilistarihi date,
    kangrup integer,
    verilisnedeni integer,
    mahalle character varying(30)
);


ALTER TABLE public.kisikimlik OWNER TO postgres;

--
-- TOC entry 187 (class 1259 OID 37494)
-- Dependencies: 6 186
-- Name: kisikimlik_rid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE kisikimlik_rid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.kisikimlik_rid_seq OWNER TO postgres;

--
-- TOC entry 3144 (class 0 OID 0)
-- Dependencies: 187
-- Name: kisikimlik_rid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE kisikimlik_rid_seq OWNED BY kisikimlik.rid;


--
-- TOC entry 3145 (class 0 OID 0)
-- Dependencies: 187
-- Name: kisikimlik_rid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('kisikimlik_rid_seq', 58, true);


--
-- TOC entry 188 (class 1259 OID 37496)
-- Dependencies: 6
-- Name: kisiuyruk; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE kisiuyruk (
    rid bigint NOT NULL,
    kisiref bigint NOT NULL,
    uyruktip integer NOT NULL,
    ulkeref bigint
);


ALTER TABLE public.kisiuyruk OWNER TO postgres;

--
-- TOC entry 189 (class 1259 OID 37499)
-- Dependencies: 6 188
-- Name: kisiuyruk_rid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE kisiuyruk_rid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.kisiuyruk_rid_seq OWNER TO postgres;

--
-- TOC entry 3146 (class 0 OID 0)
-- Dependencies: 189
-- Name: kisiuyruk_rid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE kisiuyruk_rid_seq OWNED BY kisiuyruk.rid;


--
-- TOC entry 3147 (class 0 OID 0)
-- Dependencies: 189
-- Name: kisiuyruk_rid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('kisiuyruk_rid_seq', 8, true);


--
-- TOC entry 190 (class 1259 OID 37501)
-- Dependencies: 6
-- Name: komisyon; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE komisyon (
    rid bigint NOT NULL,
    ustref bigint,
    ad character varying(100) NOT NULL,
    kisaad character varying(20),
    erisimkodu character varying(400),
    birimref bigint
);


ALTER TABLE public.komisyon OWNER TO postgres;

--
-- TOC entry 191 (class 1259 OID 37507)
-- Dependencies: 6 190
-- Name: komisyon_rid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE komisyon_rid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.komisyon_rid_seq OWNER TO postgres;

--
-- TOC entry 3148 (class 0 OID 0)
-- Dependencies: 191
-- Name: komisyon_rid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE komisyon_rid_seq OWNED BY komisyon.rid;


--
-- TOC entry 3149 (class 0 OID 0)
-- Dependencies: 191
-- Name: komisyon_rid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('komisyon_rid_seq', 6, true);


--
-- TOC entry 192 (class 1259 OID 37509)
-- Dependencies: 6
-- Name: komisyondonem; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE komisyondonem (
    rid bigint NOT NULL,
    komisyonref bigint NOT NULL,
    donemref bigint NOT NULL
);


ALTER TABLE public.komisyondonem OWNER TO postgres;

--
-- TOC entry 193 (class 1259 OID 37512)
-- Dependencies: 6 192
-- Name: komisyondonem_rid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE komisyondonem_rid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.komisyondonem_rid_seq OWNER TO postgres;

--
-- TOC entry 3150 (class 0 OID 0)
-- Dependencies: 193
-- Name: komisyondonem_rid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE komisyondonem_rid_seq OWNED BY komisyondonem.rid;


--
-- TOC entry 3151 (class 0 OID 0)
-- Dependencies: 193
-- Name: komisyondonem_rid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('komisyondonem_rid_seq', 6, true);


--
-- TOC entry 295 (class 1259 OID 40782)
-- Dependencies: 6
-- Name: komisyondonemtoplanti; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE komisyondonemtoplanti (
    rid bigint NOT NULL,
    komisyondonemref bigint NOT NULL,
    tarih date NOT NULL,
    toplantituru integer NOT NULL
);


ALTER TABLE public.komisyondonemtoplanti OWNER TO postgres;

--
-- TOC entry 294 (class 1259 OID 40780)
-- Dependencies: 295 6
-- Name: komisyondonemtoplanti_rid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE komisyondonemtoplanti_rid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.komisyondonemtoplanti_rid_seq OWNER TO postgres;

--
-- TOC entry 3152 (class 0 OID 0)
-- Dependencies: 294
-- Name: komisyondonemtoplanti_rid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE komisyondonemtoplanti_rid_seq OWNED BY komisyondonemtoplanti.rid;


--
-- TOC entry 3153 (class 0 OID 0)
-- Dependencies: 294
-- Name: komisyondonemtoplanti_rid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('komisyondonemtoplanti_rid_seq', 1, true);


--
-- TOC entry 299 (class 1259 OID 40816)
-- Dependencies: 6
-- Name: komisyondonemtoplantitutanak; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE komisyondonemtoplantitutanak (
    rid bigint NOT NULL,
    kmsyndonemtoplantiref bigint NOT NULL,
    tarih date NOT NULL,
    yukleyenref bigint NOT NULL,
    path character varying(2500) NOT NULL
);


ALTER TABLE public.komisyondonemtoplantitutanak OWNER TO postgres;

--
-- TOC entry 298 (class 1259 OID 40814)
-- Dependencies: 6 299
-- Name: komisyondonemtoplantitutanak_rid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE komisyondonemtoplantitutanak_rid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.komisyondonemtoplantitutanak_rid_seq OWNER TO postgres;

--
-- TOC entry 3154 (class 0 OID 0)
-- Dependencies: 298
-- Name: komisyondonemtoplantitutanak_rid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE komisyondonemtoplantitutanak_rid_seq OWNED BY komisyondonemtoplantitutanak.rid;


--
-- TOC entry 3155 (class 0 OID 0)
-- Dependencies: 298
-- Name: komisyondonemtoplantitutanak_rid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('komisyondonemtoplantitutanak_rid_seq', 1, false);


--
-- TOC entry 194 (class 1259 OID 37514)
-- Dependencies: 2455 2456 6
-- Name: komisyondonemuye; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE komisyondonemuye (
    rid bigint NOT NULL,
    komisyondonemref bigint NOT NULL,
    uyeref bigint,
    durum integer NOT NULL,
    komisyongorev integer DEFAULT 0 NOT NULL,
    uyeturu integer DEFAULT 0 NOT NULL,
    kisiref bigint
);


ALTER TABLE public.komisyondonemuye OWNER TO postgres;

--
-- TOC entry 195 (class 1259 OID 37517)
-- Dependencies: 6 194
-- Name: komisyondonemuye_rid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE komisyondonemuye_rid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.komisyondonemuye_rid_seq OWNER TO postgres;

--
-- TOC entry 3156 (class 0 OID 0)
-- Dependencies: 195
-- Name: komisyondonemuye_rid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE komisyondonemuye_rid_seq OWNED BY komisyondonemuye.rid;


--
-- TOC entry 3157 (class 0 OID 0)
-- Dependencies: 195
-- Name: komisyondonemuye_rid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('komisyondonemuye_rid_seq', 13, true);


--
-- TOC entry 196 (class 1259 OID 37519)
-- Dependencies: 6
-- Name: koy; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE koy (
    rid bigint NOT NULL,
    turu integer,
    ad character varying(200),
    ilceref bigint
);


ALTER TABLE public.koy OWNER TO postgres;

--
-- TOC entry 197 (class 1259 OID 37522)
-- Dependencies: 2459 6
-- Name: kullanici; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE kullanici (
    rid bigint NOT NULL,
    name character varying(15) NOT NULL,
    parole character varying(200) NOT NULL,
    password character varying(200) NOT NULL,
    theme character varying(100) DEFAULT 'bootstrap'::character varying NOT NULL,
    kisiref bigint NOT NULL
);


ALTER TABLE public.kullanici OWNER TO postgres;

--
-- TOC entry 198 (class 1259 OID 37529)
-- Dependencies: 6
-- Name: kullanicirole; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE kullanicirole (
    rid bigint NOT NULL,
    roleref bigint NOT NULL,
    active integer NOT NULL,
    kullaniciref bigint
);


ALTER TABLE public.kullanicirole OWNER TO postgres;

--
-- TOC entry 199 (class 1259 OID 37532)
-- Dependencies: 6
-- Name: kurul; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE kurul (
    rid bigint NOT NULL,
    ad character varying(100) NOT NULL,
    birimref bigint,
    kurulturu integer
);


ALTER TABLE public.kurul OWNER TO postgres;

--
-- TOC entry 200 (class 1259 OID 37535)
-- Dependencies: 199 6
-- Name: kurul_rid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE kurul_rid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.kurul_rid_seq OWNER TO postgres;

--
-- TOC entry 3158 (class 0 OID 0)
-- Dependencies: 200
-- Name: kurul_rid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE kurul_rid_seq OWNED BY kurul.rid;


--
-- TOC entry 3159 (class 0 OID 0)
-- Dependencies: 200
-- Name: kurul_rid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('kurul_rid_seq', 16, true);


--
-- TOC entry 201 (class 1259 OID 37537)
-- Dependencies: 6
-- Name: kuruldonem; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE kuruldonem (
    rid bigint NOT NULL,
    kurulref bigint,
    donemref bigint
);


ALTER TABLE public.kuruldonem OWNER TO postgres;

--
-- TOC entry 202 (class 1259 OID 37540)
-- Dependencies: 6 201
-- Name: kuruldonem_rid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE kuruldonem_rid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.kuruldonem_rid_seq OWNER TO postgres;

--
-- TOC entry 3160 (class 0 OID 0)
-- Dependencies: 202
-- Name: kuruldonem_rid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE kuruldonem_rid_seq OWNED BY kuruldonem.rid;


--
-- TOC entry 3161 (class 0 OID 0)
-- Dependencies: 202
-- Name: kuruldonem_rid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('kuruldonem_rid_seq', 22, true);


--
-- TOC entry 301 (class 1259 OID 40844)
-- Dependencies: 6
-- Name: kuruldonemdelege; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE kuruldonemdelege (
    rid bigint NOT NULL,
    kuruldonemref bigint NOT NULL,
    uyeref bigint NOT NULL
);


ALTER TABLE public.kuruldonemdelege OWNER TO postgres;

--
-- TOC entry 300 (class 1259 OID 40842)
-- Dependencies: 6 301
-- Name: kuruldonemdelege_rid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE kuruldonemdelege_rid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.kuruldonemdelege_rid_seq OWNER TO postgres;

--
-- TOC entry 3162 (class 0 OID 0)
-- Dependencies: 300
-- Name: kuruldonemdelege_rid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE kuruldonemdelege_rid_seq OWNED BY kuruldonemdelege.rid;


--
-- TOC entry 3163 (class 0 OID 0)
-- Dependencies: 300
-- Name: kuruldonemdelege_rid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('kuruldonemdelege_rid_seq', 3, true);


--
-- TOC entry 293 (class 1259 OID 40769)
-- Dependencies: 6
-- Name: kuruldonemtoplanti; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE kuruldonemtoplanti (
    rid bigint NOT NULL,
    kuruldonemref bigint NOT NULL,
    tarih date NOT NULL,
    toplantituru integer NOT NULL
);


ALTER TABLE public.kuruldonemtoplanti OWNER TO postgres;

--
-- TOC entry 292 (class 1259 OID 40767)
-- Dependencies: 293 6
-- Name: kuruldonemtoplanti_rid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE kuruldonemtoplanti_rid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.kuruldonemtoplanti_rid_seq OWNER TO postgres;

--
-- TOC entry 3164 (class 0 OID 0)
-- Dependencies: 292
-- Name: kuruldonemtoplanti_rid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE kuruldonemtoplanti_rid_seq OWNED BY kuruldonemtoplanti.rid;


--
-- TOC entry 3165 (class 0 OID 0)
-- Dependencies: 292
-- Name: kuruldonemtoplanti_rid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('kuruldonemtoplanti_rid_seq', 1, true);


--
-- TOC entry 297 (class 1259 OID 40795)
-- Dependencies: 6
-- Name: kuruldonemtoplantitutanak; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE kuruldonemtoplantitutanak (
    rid bigint NOT NULL,
    kuruldonemtoplantiref bigint NOT NULL,
    tarih date NOT NULL,
    yukleyenref bigint NOT NULL,
    path character varying(2500) NOT NULL
);


ALTER TABLE public.kuruldonemtoplantitutanak OWNER TO postgres;

--
-- TOC entry 296 (class 1259 OID 40793)
-- Dependencies: 6 297
-- Name: kuruldonemtoplantitutanak_rid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE kuruldonemtoplantitutanak_rid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.kuruldonemtoplantitutanak_rid_seq OWNER TO postgres;

--
-- TOC entry 3166 (class 0 OID 0)
-- Dependencies: 296
-- Name: kuruldonemtoplantitutanak_rid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE kuruldonemtoplantitutanak_rid_seq OWNED BY kuruldonemtoplantitutanak.rid;


--
-- TOC entry 3167 (class 0 OID 0)
-- Dependencies: 296
-- Name: kuruldonemtoplantitutanak_rid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('kuruldonemtoplantitutanak_rid_seq', 4, true);


--
-- TOC entry 203 (class 1259 OID 37542)
-- Dependencies: 2464 2465 2466 6
-- Name: kuruldonemuye; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE kuruldonemuye (
    rid bigint NOT NULL,
    kuruldonemref bigint NOT NULL,
    gorevturu integer DEFAULT 1 NOT NULL,
    uyeturu integer DEFAULT 1 NOT NULL,
    aktif integer DEFAULT 1 NOT NULL,
    uyeref bigint NOT NULL
);


ALTER TABLE public.kuruldonemuye OWNER TO postgres;

--
-- TOC entry 204 (class 1259 OID 37545)
-- Dependencies: 6 203
-- Name: kuruldonemuye_rid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE kuruldonemuye_rid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.kuruldonemuye_rid_seq OWNER TO postgres;

--
-- TOC entry 3168 (class 0 OID 0)
-- Dependencies: 204
-- Name: kuruldonemuye_rid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE kuruldonemuye_rid_seq OWNED BY kuruldonemuye.rid;


--
-- TOC entry 3169 (class 0 OID 0)
-- Dependencies: 204
-- Name: kuruldonemuye_rid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('kuruldonemuye_rid_seq', 21, true);


--
-- TOC entry 205 (class 1259 OID 37547)
-- Dependencies: 6
-- Name: kurulgorev; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE kurulgorev (
    rid bigint NOT NULL,
    tanim character varying(20),
    gorevturu integer
);


ALTER TABLE public.kurulgorev OWNER TO postgres;

--
-- TOC entry 206 (class 1259 OID 37550)
-- Dependencies: 6 205
-- Name: kurulgorev_rid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE kurulgorev_rid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.kurulgorev_rid_seq OWNER TO postgres;

--
-- TOC entry 3170 (class 0 OID 0)
-- Dependencies: 206
-- Name: kurulgorev_rid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE kurulgorev_rid_seq OWNED BY kurulgorev.rid;


--
-- TOC entry 3171 (class 0 OID 0)
-- Dependencies: 206
-- Name: kurulgorev_rid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('kurulgorev_rid_seq', 6, true);


--
-- TOC entry 207 (class 1259 OID 37552)
-- Dependencies: 6
-- Name: kurum; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE kurum (
    rid bigint NOT NULL,
    ad character varying(20) NOT NULL,
    vergidairesi character varying(30),
    vergino character varying(20),
    sorumluref bigint,
    aciklama character varying(200),
    kurumtururef bigint,
    sehirref bigint,
    ilceref bigint
);


ALTER TABLE public.kurum OWNER TO postgres;

--
-- TOC entry 208 (class 1259 OID 37555)
-- Dependencies: 6 207
-- Name: kurum_rid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE kurum_rid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.kurum_rid_seq OWNER TO postgres;

--
-- TOC entry 3172 (class 0 OID 0)
-- Dependencies: 208
-- Name: kurum_rid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE kurum_rid_seq OWNED BY kurum.rid;


--
-- TOC entry 3173 (class 0 OID 0)
-- Dependencies: 208
-- Name: kurum_rid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('kurum_rid_seq', 15, true);


--
-- TOC entry 209 (class 1259 OID 37557)
-- Dependencies: 6
-- Name: kurumhesap; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE kurumhesap (
    rid bigint NOT NULL,
    hesapno bigint,
    subekodu bigint,
    ibannumarasi bigint,
    gecerli integer,
    kurumref bigint,
    bankaadi character varying(255)
);


ALTER TABLE public.kurumhesap OWNER TO postgres;

--
-- TOC entry 210 (class 1259 OID 37560)
-- Dependencies: 6 209
-- Name: kurumhesap_rid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE kurumhesap_rid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.kurumhesap_rid_seq OWNER TO postgres;

--
-- TOC entry 3174 (class 0 OID 0)
-- Dependencies: 210
-- Name: kurumhesap_rid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE kurumhesap_rid_seq OWNED BY kurumhesap.rid;


--
-- TOC entry 3175 (class 0 OID 0)
-- Dependencies: 210
-- Name: kurumhesap_rid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('kurumhesap_rid_seq', 6, true);


--
-- TOC entry 211 (class 1259 OID 37562)
-- Dependencies: 6
-- Name: kurumkisi; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE kurumkisi (
    rid bigint NOT NULL,
    gecerli integer,
    kurumref bigint,
    kisiref bigint,
    uyelikdurumu integer
);


ALTER TABLE public.kurumkisi OWNER TO postgres;

--
-- TOC entry 212 (class 1259 OID 37565)
-- Dependencies: 211 6
-- Name: kurumkisi_rid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE kurumkisi_rid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.kurumkisi_rid_seq OWNER TO postgres;

--
-- TOC entry 3176 (class 0 OID 0)
-- Dependencies: 212
-- Name: kurumkisi_rid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE kurumkisi_rid_seq OWNED BY kurumkisi.rid;


--
-- TOC entry 3177 (class 0 OID 0)
-- Dependencies: 212
-- Name: kurumkisi_rid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('kurumkisi_rid_seq', 12, true);


--
-- TOC entry 213 (class 1259 OID 37567)
-- Dependencies: 6
-- Name: kurumturu; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE kurumturu (
    rid bigint NOT NULL,
    ad character varying(255) NOT NULL
);


ALTER TABLE public.kurumturu OWNER TO postgres;

--
-- TOC entry 214 (class 1259 OID 37570)
-- Dependencies: 213 6
-- Name: kurumturu_rid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE kurumturu_rid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.kurumturu_rid_seq OWNER TO postgres;

--
-- TOC entry 3178 (class 0 OID 0)
-- Dependencies: 214
-- Name: kurumturu_rid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE kurumturu_rid_seq OWNED BY kurumturu.rid;


--
-- TOC entry 3179 (class 0 OID 0)
-- Dependencies: 214
-- Name: kurumturu_rid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('kurumturu_rid_seq', 9, true);


--
-- TOC entry 215 (class 1259 OID 37572)
-- Dependencies: 6
-- Name: menuitem; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE menuitem (
    rid bigint NOT NULL,
    ustref bigint,
    ad character varying(100) NOT NULL,
    kod character varying(30) NOT NULL,
    url character varying(500) NOT NULL,
    erisimkodu character varying(400)
);


ALTER TABLE public.menuitem OWNER TO postgres;

--
-- TOC entry 216 (class 1259 OID 37578)
-- Dependencies: 215 6
-- Name: menuitem_rid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE menuitem_rid_seq
    START WITH 175
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.menuitem_rid_seq OWNER TO postgres;

--
-- TOC entry 3180 (class 0 OID 0)
-- Dependencies: 216
-- Name: menuitem_rid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE menuitem_rid_seq OWNED BY menuitem.rid;


--
-- TOC entry 3181 (class 0 OID 0)
-- Dependencies: 216
-- Name: menuitem_rid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('menuitem_rid_seq', 188, true);


--
-- TOC entry 217 (class 1259 OID 37580)
-- Dependencies: 6
-- Name: odeme; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE odeme (
    rid bigint NOT NULL,
    kisiref bigint NOT NULL,
    miktar numeric(14,10) NOT NULL,
    tarih date NOT NULL,
    odemeturu integer NOT NULL
);


ALTER TABLE public.odeme OWNER TO postgres;

--
-- TOC entry 218 (class 1259 OID 37583)
-- Dependencies: 6 217
-- Name: odeme_rid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE odeme_rid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.odeme_rid_seq OWNER TO postgres;

--
-- TOC entry 3182 (class 0 OID 0)
-- Dependencies: 218
-- Name: odeme_rid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE odeme_rid_seq OWNED BY odeme.rid;


--
-- TOC entry 3183 (class 0 OID 0)
-- Dependencies: 218
-- Name: odeme_rid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('odeme_rid_seq', 7, true);


--
-- TOC entry 219 (class 1259 OID 37585)
-- Dependencies: 6
-- Name: odemetip; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE odemetip (
    rid bigint NOT NULL,
    kod character varying(20) NOT NULL,
    ad character varying(20) NOT NULL,
    miktar numeric(14,10) NOT NULL,
    baslangictarih date NOT NULL,
    bitistarih date NOT NULL
);


ALTER TABLE public.odemetip OWNER TO postgres;

--
-- TOC entry 220 (class 1259 OID 37588)
-- Dependencies: 6 219
-- Name: odemetip_rid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE odemetip_rid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.odemetip_rid_seq OWNER TO postgres;

--
-- TOC entry 3184 (class 0 OID 0)
-- Dependencies: 220
-- Name: odemetip_rid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE odemetip_rid_seq OWNED BY odemetip.rid;


--
-- TOC entry 3185 (class 0 OID 0)
-- Dependencies: 220
-- Name: odemetip_rid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('odemetip_rid_seq', 5, true);


--
-- TOC entry 221 (class 1259 OID 37590)
-- Dependencies: 6
-- Name: odultip; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE odultip (
    rid bigint NOT NULL,
    kisaad character varying(15) NOT NULL,
    ad character varying(30) NOT NULL,
    aciklama character varying(200)
);


ALTER TABLE public.odultip OWNER TO postgres;

--
-- TOC entry 222 (class 1259 OID 37593)
-- Dependencies: 221 6
-- Name: odultip_rid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE odultip_rid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.odultip_rid_seq OWNER TO postgres;

--
-- TOC entry 3186 (class 0 OID 0)
-- Dependencies: 222
-- Name: odultip_rid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE odultip_rid_seq OWNED BY odultip.rid;


--
-- TOC entry 3187 (class 0 OID 0)
-- Dependencies: 222
-- Name: odultip_rid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('odultip_rid_seq', 5, true);


--
-- TOC entry 223 (class 1259 OID 37595)
-- Dependencies: 6
-- Name: ogrenimkurum; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE ogrenimkurum (
    rid bigint NOT NULL,
    kisaad character varying(15) NOT NULL,
    ad character varying(30) NOT NULL,
    aciklama character varying(200)
);


ALTER TABLE public.ogrenimkurum OWNER TO postgres;

--
-- TOC entry 224 (class 1259 OID 37598)
-- Dependencies: 223 6
-- Name: ogrenimkurum_rid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE ogrenimkurum_rid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ogrenimkurum_rid_seq OWNER TO postgres;

--
-- TOC entry 3188 (class 0 OID 0)
-- Dependencies: 224
-- Name: ogrenimkurum_rid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE ogrenimkurum_rid_seq OWNED BY ogrenimkurum.rid;


--
-- TOC entry 3189 (class 0 OID 0)
-- Dependencies: 224
-- Name: ogrenimkurum_rid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('ogrenimkurum_rid_seq', 5, true);


--
-- TOC entry 225 (class 1259 OID 37600)
-- Dependencies: 6
-- Name: personel; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE personel (
    rid bigint NOT NULL,
    sicilno character varying(15),
    baslamatarih date NOT NULL,
    bitistarih date,
    durum integer NOT NULL,
    kisiref bigint,
    birimref bigint NOT NULL
);


ALTER TABLE public.personel OWNER TO postgres;

--
-- TOC entry 226 (class 1259 OID 37603)
-- Dependencies: 6 225
-- Name: personel_rid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE personel_rid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.personel_rid_seq OWNER TO postgres;

--
-- TOC entry 3190 (class 0 OID 0)
-- Dependencies: 226
-- Name: personel_rid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE personel_rid_seq OWNED BY personel.rid;


--
-- TOC entry 3191 (class 0 OID 0)
-- Dependencies: 226
-- Name: personel_rid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('personel_rid_seq', 75, true);


--
-- TOC entry 227 (class 1259 OID 37605)
-- Dependencies: 2479 6
-- Name: personelbirimgorev; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE personelbirimgorev (
    rid bigint NOT NULL,
    personelref bigint NOT NULL,
    gorevref bigint NOT NULL,
    birimref bigint NOT NULL,
    baslangictarih date NOT NULL,
    bitistarih date,
    aciklama character varying(2500),
    durum integer DEFAULT 1 NOT NULL
);


ALTER TABLE public.personelbirimgorev OWNER TO postgres;

--
-- TOC entry 228 (class 1259 OID 37612)
-- Dependencies: 6 227
-- Name: personelbirimgorev_rid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE personelbirimgorev_rid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.personelbirimgorev_rid_seq OWNER TO postgres;

--
-- TOC entry 3192 (class 0 OID 0)
-- Dependencies: 228
-- Name: personelbirimgorev_rid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE personelbirimgorev_rid_seq OWNED BY personelbirimgorev.rid;


--
-- TOC entry 3193 (class 0 OID 0)
-- Dependencies: 228
-- Name: personelbirimgorev_rid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('personelbirimgorev_rid_seq', 55, true);


--
-- TOC entry 229 (class 1259 OID 37614)
-- Dependencies: 6
-- Name: role; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE role (
    rid bigint NOT NULL,
    name character varying(200) NOT NULL,
    active integer NOT NULL
);


ALTER TABLE public.role OWNER TO postgres;

--
-- TOC entry 230 (class 1259 OID 37617)
-- Dependencies: 229 6
-- Name: role_rid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE role_rid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.role_rid_seq OWNER TO postgres;

--
-- TOC entry 3194 (class 0 OID 0)
-- Dependencies: 230
-- Name: role_rid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE role_rid_seq OWNED BY role.rid;


--
-- TOC entry 3195 (class 0 OID 0)
-- Dependencies: 230
-- Name: role_rid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('role_rid_seq', 6, true);


--
-- TOC entry 231 (class 1259 OID 37619)
-- Dependencies: 2482 6
-- Name: sistemParametre; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE sistemParametre (
    rid bigint NOT NULL,
    listeboyutu bigint DEFAULT 20 NOT NULL
);


ALTER TABLE public.sistemParametre OWNER TO postgres;

--
-- TOC entry 232 (class 1259 OID 37623)
-- Dependencies: 6 231
-- Name: sistemParametre_rid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE sistemParametre_rid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sistemParametre_rid_seq OWNER TO postgres;

--
-- TOC entry 3196 (class 0 OID 0)
-- Dependencies: 232
-- Name: sistemParametre_rid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE sistemParametre_rid_seq OWNED BY sistemParametre.rid;


--
-- TOC entry 3197 (class 0 OID 0)
-- Dependencies: 232
-- Name: sistemParametre_rid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('sistemParametre_rid_seq', 5, true);


--
-- TOC entry 317 (class 1259 OID 45102)
-- Dependencies: 6
-- Name: smsanlikgonderimlog; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE smsanlikgonderimlog (
    rid bigint NOT NULL,
    kisiref bigint NOT NULL,
    icerikref bigint NOT NULL,
    tarih date NOT NULL,
    gonderenref bigint NOT NULL,
    durum integer NOT NULL,
    aciklama character varying(2500)
);


ALTER TABLE public.smsanlikgonderimlog OWNER TO postgres;

--
-- TOC entry 316 (class 1259 OID 45100)
-- Dependencies: 317 6
-- Name: smsanlikgonderimlog_rid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE smsanlikgonderimlog_rid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.smsanlikgonderimlog_rid_seq OWNER TO postgres;

--
-- TOC entry 3198 (class 0 OID 0)
-- Dependencies: 316
-- Name: smsanlikgonderimlog_rid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE smsanlikgonderimlog_rid_seq OWNED BY smsanlikgonderimlog.rid;


--
-- TOC entry 3199 (class 0 OID 0)
-- Dependencies: 316
-- Name: smsanlikgonderimlog_rid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('smsanlikgonderimlog_rid_seq', 1, false);


--
-- TOC entry 313 (class 1259 OID 45050)
-- Dependencies: 6
-- Name: smsgonderimlog; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE smsgonderimlog (
    rid bigint NOT NULL,
    gonderimref bigint NOT NULL,
    kisiref bigint NOT NULL,
    tarih date NOT NULL,
    gonderenref bigint NOT NULL,
    durum integer NOT NULL,
    icerik character varying(2500),
    aciklama character varying(2500)
);


ALTER TABLE public.smsgonderimlog OWNER TO postgres;

--
-- TOC entry 312 (class 1259 OID 45048)
-- Dependencies: 313 6
-- Name: smsgonderimlog_rid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE smsgonderimlog_rid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.smsgonderimlog_rid_seq OWNER TO postgres;

--
-- TOC entry 3200 (class 0 OID 0)
-- Dependencies: 312
-- Name: smsgonderimlog_rid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE smsgonderimlog_rid_seq OWNED BY smsgonderimlog.rid;


--
-- TOC entry 3201 (class 0 OID 0)
-- Dependencies: 312
-- Name: smsgonderimlog_rid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('smsgonderimlog_rid_seq', 1, false);


--
-- TOC entry 233 (class 1259 OID 37625)
-- Dependencies: 6
-- Name: sorun; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE sorun (
    rid bigint NOT NULL,
    kullaniciref bigint NOT NULL,
    tarih date NOT NULL,
    onem integer NOT NULL,
    baslik character varying(50) NOT NULL,
    aciklama character varying(300) NOT NULL
);


ALTER TABLE public.sorun OWNER TO postgres;

--
-- TOC entry 234 (class 1259 OID 37628)
-- Dependencies: 233 6
-- Name: sorun_rid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE sorun_rid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sorun_rid_seq OWNER TO postgres;

--
-- TOC entry 3202 (class 0 OID 0)
-- Dependencies: 234
-- Name: sorun_rid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE sorun_rid_seq OWNED BY sorun.rid;


--
-- TOC entry 3203 (class 0 OID 0)
-- Dependencies: 234
-- Name: sorun_rid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('sorun_rid_seq', 6, true);


--
-- TOC entry 235 (class 1259 OID 37630)
-- Dependencies: 197 6
-- Name: systemuser_rid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE systemuser_rid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.systemuser_rid_seq OWNER TO postgres;

--
-- TOC entry 3204 (class 0 OID 0)
-- Dependencies: 235
-- Name: systemuser_rid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE systemuser_rid_seq OWNED BY kullanici.rid;


--
-- TOC entry 3205 (class 0 OID 0)
-- Dependencies: 235
-- Name: systemuser_rid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('systemuser_rid_seq', 3, true);


--
-- TOC entry 236 (class 1259 OID 37632)
-- Dependencies: 198 6
-- Name: systemuserrole_rid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE systemuserrole_rid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.systemuserrole_rid_seq OWNER TO postgres;

--
-- TOC entry 3206 (class 0 OID 0)
-- Dependencies: 236
-- Name: systemuserrole_rid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE systemuserrole_rid_seq OWNED BY kullanicirole.rid;


--
-- TOC entry 3207 (class 0 OID 0)
-- Dependencies: 236
-- Name: systemuserrole_rid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('systemuserrole_rid_seq', 15, true);


--
-- TOC entry 237 (class 1259 OID 37634)
-- Dependencies: 2485 6
-- Name: telefon; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE telefon (
    rid bigint NOT NULL,
    kisiref bigint,
    telefonno character varying(30) NOT NULL,
    telefonturu integer NOT NULL,
    varsayilan integer DEFAULT 2 NOT NULL,
    kurumref bigint
);


ALTER TABLE public.telefon OWNER TO postgres;

--
-- TOC entry 238 (class 1259 OID 37638)
-- Dependencies: 237 6
-- Name: telefon_rid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE telefon_rid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.telefon_rid_seq OWNER TO postgres;

--
-- TOC entry 3208 (class 0 OID 0)
-- Dependencies: 238
-- Name: telefon_rid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE telefon_rid_seq OWNED BY telefon.rid;


--
-- TOC entry 3209 (class 0 OID 0)
-- Dependencies: 238
-- Name: telefon_rid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('telefon_rid_seq', 81, true);


--
-- TOC entry 239 (class 1259 OID 37640)
-- Dependencies: 6 173
-- Name: transaction_rid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE transaction_rid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.transaction_rid_seq OWNER TO postgres;

--
-- TOC entry 3210 (class 0 OID 0)
-- Dependencies: 239
-- Name: transaction_rid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE transaction_rid_seq OWNED BY islem.rid;


--
-- TOC entry 3211 (class 0 OID 0)
-- Dependencies: 239
-- Name: transaction_rid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('transaction_rid_seq', 85, true);


--
-- TOC entry 240 (class 1259 OID 37642)
-- Dependencies: 6 177
-- Name: transactionrole_rid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE transactionrole_rid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.transactionrole_rid_seq OWNER TO postgres;

--
-- TOC entry 3212 (class 0 OID 0)
-- Dependencies: 240
-- Name: transactionrole_rid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE transactionrole_rid_seq OWNED BY islemrole.rid;


--
-- TOC entry 3213 (class 0 OID 0)
-- Dependencies: 240
-- Name: transactionrole_rid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('transactionrole_rid_seq', 352, true);


--
-- TOC entry 241 (class 1259 OID 37644)
-- Dependencies: 174 6
-- Name: transactionsystemuser_rid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE transactionsystemuser_rid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.transactionsystemuser_rid_seq OWNER TO postgres;

--
-- TOC entry 3214 (class 0 OID 0)
-- Dependencies: 241
-- Name: transactionsystemuser_rid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE transactionsystemuser_rid_seq OWNED BY islemkullanici.rid;


--
-- TOC entry 3215 (class 0 OID 0)
-- Dependencies: 241
-- Name: transactionsystemuser_rid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('transactionsystemuser_rid_seq', 627, true);


--
-- TOC entry 242 (class 1259 OID 37646)
-- Dependencies: 146 6
-- Name: unit_rid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE unit_rid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.unit_rid_seq OWNER TO postgres;

--
-- TOC entry 3216 (class 0 OID 0)
-- Dependencies: 242
-- Name: unit_rid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE unit_rid_seq OWNED BY birim.rid;


--
-- TOC entry 3217 (class 0 OID 0)
-- Dependencies: 242
-- Name: unit_rid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('unit_rid_seq', 12, true);


--
-- TOC entry 243 (class 1259 OID 37648)
-- Dependencies: 6
-- Name: universite; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE universite (
    rid bigint NOT NULL,
    webadresi character varying(255) NOT NULL,
    ad character varying(30) NOT NULL,
    durum integer,
    ulkeref bigint,
    ilref bigint
);


ALTER TABLE public.universite OWNER TO postgres;

--
-- TOC entry 244 (class 1259 OID 37651)
-- Dependencies: 6 243
-- Name: universite_rid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE universite_rid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.universite_rid_seq OWNER TO postgres;

--
-- TOC entry 3218 (class 0 OID 0)
-- Dependencies: 244
-- Name: universite_rid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE universite_rid_seq OWNED BY universite.rid;


--
-- TOC entry 3219 (class 0 OID 0)
-- Dependencies: 244
-- Name: universite_rid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('universite_rid_seq', 4, true);


--
-- TOC entry 245 (class 1259 OID 37653)
-- Dependencies: 6
-- Name: uyarinot; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE uyarinot (
    rid bigint NOT NULL,
    kullaniciref bigint NOT NULL,
    uyaritip integer NOT NULL,
    tarih date NOT NULL,
    gecerliliktarih date NOT NULL,
    uyarimetin character varying(100) NOT NULL,
    uyaridurum integer NOT NULL,
    birimref bigint
);


ALTER TABLE public.uyarinot OWNER TO postgres;

--
-- TOC entry 246 (class 1259 OID 37656)
-- Dependencies: 6 245
-- Name: uyarinot_rid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE uyarinot_rid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.uyarinot_rid_seq OWNER TO postgres;

--
-- TOC entry 3220 (class 0 OID 0)
-- Dependencies: 246
-- Name: uyarinot_rid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE uyarinot_rid_seq OWNED BY uyarinot.rid;


--
-- TOC entry 3221 (class 0 OID 0)
-- Dependencies: 246
-- Name: uyarinot_rid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('uyarinot_rid_seq', 4, true);


--
-- TOC entry 247 (class 1259 OID 37658)
-- Dependencies: 2489 2490 2491 6
-- Name: uye; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE uye (
    rid bigint NOT NULL,
    kisiref bigint NOT NULL,
    sicilno character varying(15),
    uyetip integer NOT NULL,
    uyeliktarih date NOT NULL,
    ayrilmatarih date,
    uyedurum integer NOT NULL,
    kayitdurum integer NOT NULL,
    onaylayanref bigint,
    onaytarih date,
    birimref bigint,
    sinif integer,
    ogrencino character varying(50),
    ogrenciuyeref bigint,
    iletisimsms integer DEFAULT 2 NOT NULL,
    iletisimemail integer DEFAULT 2 NOT NULL,
    iletisimposta integer DEFAULT 2 NOT NULL
);


ALTER TABLE public.uye OWNER TO postgres;

--
-- TOC entry 248 (class 1259 OID 37664)
-- Dependencies: 247 6
-- Name: uye_rid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE uye_rid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.uye_rid_seq OWNER TO postgres;

--
-- TOC entry 3222 (class 0 OID 0)
-- Dependencies: 248
-- Name: uye_rid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE uye_rid_seq OWNED BY uye.rid;


--
-- TOC entry 3223 (class 0 OID 0)
-- Dependencies: 248
-- Name: uye_rid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('uye_rid_seq', 75, true);


--
-- TOC entry 249 (class 1259 OID 37666)
-- Dependencies: 6
-- Name: uyeabone; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE uyeabone (
    rid bigint NOT NULL,
    uyeref bigint NOT NULL,
    yayinref bigint NOT NULL,
    tarih date NOT NULL,
    baslangictarih date NOT NULL,
    bitistarih date NOT NULL,
    uyeabonedurum integer NOT NULL,
    aciklama character varying(200)
);


ALTER TABLE public.uyeabone OWNER TO postgres;

--
-- TOC entry 250 (class 1259 OID 37669)
-- Dependencies: 6 249
-- Name: uyeabone_rid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE uyeabone_rid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.uyeabone_rid_seq OWNER TO postgres;

--
-- TOC entry 3224 (class 0 OID 0)
-- Dependencies: 250
-- Name: uyeabone_rid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE uyeabone_rid_seq OWNED BY uyeabone.rid;


--
-- TOC entry 3225 (class 0 OID 0)
-- Dependencies: 250
-- Name: uyeabone_rid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('uyeabone_rid_seq', 6, true);


--
-- TOC entry 251 (class 1259 OID 37671)
-- Dependencies: 6
-- Name: uyeaidat; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE uyeaidat (
    rid bigint NOT NULL,
    uyeref bigint NOT NULL,
    miktar numeric(14,10) NOT NULL,
    odenen numeric(14,10) NOT NULL,
    uyeaidatdurum integer NOT NULL,
    yil integer NOT NULL,
    ay integer NOT NULL,
    uyemuafiyetref bigint
);


ALTER TABLE public.uyeaidat OWNER TO postgres;

--
-- TOC entry 252 (class 1259 OID 37674)
-- Dependencies: 6 251
-- Name: uyeaidat_rid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE uyeaidat_rid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.uyeaidat_rid_seq OWNER TO postgres;

--
-- TOC entry 3226 (class 0 OID 0)
-- Dependencies: 252
-- Name: uyeaidat_rid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE uyeaidat_rid_seq OWNED BY uyeaidat.rid;


--
-- TOC entry 3227 (class 0 OID 0)
-- Dependencies: 252
-- Name: uyeaidat_rid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('uyeaidat_rid_seq', 395, true);


--
-- TOC entry 289 (class 1259 OID 38744)
-- Dependencies: 6
-- Name: uyebasvuru; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE uyebasvuru (
    rid bigint NOT NULL,
    kimlikno character varying(20),
    ad character varying(40),
    soyad character varying(40),
    babaad character varying(20),
    anaad character varying(20),
    dogumilceref bigint,
    dogumyer character varying(20),
    dogumtarih date,
    cinsiyet integer,
    medenihal integer,
    ulkeref bigint,
    sehirref bigint,
    ilceref bigint,
    ciltno character varying(15),
    aileno character varying(5),
    sirano character varying(5),
    ciltaciklama character varying(100),
    verildigiyer character varying(15),
    verilistarihi date,
    kangrup integer,
    verilisnedeni character varying(30),
    email character varying(20),
    posta character varying(20),
    sms character varying(20),
    uyetip integer,
    baslangictarih date,
    ogrencino character varying(30),
    sinif bigint,
    ogrenimtip integer,
    universiteref bigint,
    fakulteref bigint,
    bolumref bigint,
    lisansunvan character varying(30),
    diplomano bigint,
    denklikdurum integer,
    bitistarih date,
    aciklama character varying(250),
    evtelefonu bigint,
    istelefonu bigint,
    gsm bigint,
    faks bigint,
    adrestipi integer,
    acikadres character varying(200),
    binasiteadi character varying(200),
    csbm character varying(200),
    binablokadi character varying(200),
    ickapino bigint,
    diskapino bigint
);


ALTER TABLE public.uyebasvuru OWNER TO postgres;

--
-- TOC entry 288 (class 1259 OID 38742)
-- Dependencies: 289 6
-- Name: uyebasvuru_rid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE uyebasvuru_rid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.uyebasvuru_rid_seq OWNER TO postgres;

--
-- TOC entry 3228 (class 0 OID 0)
-- Dependencies: 288
-- Name: uyebasvuru_rid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE uyebasvuru_rid_seq OWNED BY uyebasvuru.rid;


--
-- TOC entry 3229 (class 0 OID 0)
-- Dependencies: 288
-- Name: uyebasvuru_rid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('uyebasvuru_rid_seq', 1, false);


--
-- TOC entry 253 (class 1259 OID 37676)
-- Dependencies: 6
-- Name: uyebelge; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE uyebelge (
    rid bigint NOT NULL,
    uyeref bigint NOT NULL,
    kurumref bigint,
    belgetipref bigint NOT NULL,
    tarih date NOT NULL,
    basvurutarih date NOT NULL,
    hazirlanmatarih date,
    evrakref bigint,
    teslimtarih date,
    uyebelgedurum integer NOT NULL,
    aciklama character varying(200),
    path character varying(255)
);


ALTER TABLE public.uyebelge OWNER TO postgres;

--
-- TOC entry 254 (class 1259 OID 37679)
-- Dependencies: 6 253
-- Name: uyebelge_rid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE uyebelge_rid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.uyebelge_rid_seq OWNER TO postgres;

--
-- TOC entry 3230 (class 0 OID 0)
-- Dependencies: 254
-- Name: uyebelge_rid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE uyebelge_rid_seq OWNED BY uyebelge.rid;


--
-- TOC entry 3231 (class 0 OID 0)
-- Dependencies: 254
-- Name: uyebelge_rid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('uyebelge_rid_seq', 8, true);


--
-- TOC entry 255 (class 1259 OID 37681)
-- Dependencies: 6
-- Name: uyebilirkisi; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE uyebilirkisi (
    rid bigint NOT NULL,
    uyeref bigint NOT NULL,
    uyeuzmanlikref bigint,
    evrakref bigint,
    tarih date NOT NULL,
    bitistarih date,
    uyebilirkisidurum integer NOT NULL,
    aciklama character varying(200)
);


ALTER TABLE public.uyebilirkisi OWNER TO postgres;

--
-- TOC entry 256 (class 1259 OID 37684)
-- Dependencies: 255 6
-- Name: uyebilirkisi_rid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE uyebilirkisi_rid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.uyebilirkisi_rid_seq OWNER TO postgres;

--
-- TOC entry 3232 (class 0 OID 0)
-- Dependencies: 256
-- Name: uyebilirkisi_rid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE uyebilirkisi_rid_seq OWNED BY uyebilirkisi.rid;


--
-- TOC entry 3233 (class 0 OID 0)
-- Dependencies: 256
-- Name: uyebilirkisi_rid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('uyebilirkisi_rid_seq', 5, true);


--
-- TOC entry 257 (class 1259 OID 37686)
-- Dependencies: 6
-- Name: uyeceza; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE uyeceza (
    rid bigint NOT NULL,
    uyeref bigint NOT NULL,
    cezatipref bigint NOT NULL,
    evrakref bigint,
    baslangictarih date NOT NULL,
    bitistarih date NOT NULL,
    aciklama character varying(200)
);


ALTER TABLE public.uyeceza OWNER TO postgres;

--
-- TOC entry 258 (class 1259 OID 37689)
-- Dependencies: 257 6
-- Name: uyeceza_rid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE uyeceza_rid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.uyeceza_rid_seq OWNER TO postgres;

--
-- TOC entry 3234 (class 0 OID 0)
-- Dependencies: 258
-- Name: uyeceza_rid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE uyeceza_rid_seq OWNED BY uyeceza.rid;


--
-- TOC entry 3235 (class 0 OID 0)
-- Dependencies: 258
-- Name: uyeceza_rid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('uyeceza_rid_seq', 7, true);


--
-- TOC entry 287 (class 1259 OID 38726)
-- Dependencies: 6
-- Name: uyedurumdegistirme; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE uyedurumdegistirme (
    rid bigint NOT NULL,
    uyeref bigint NOT NULL,
    tarih date NOT NULL,
    kayityapan bigint NOT NULL,
    oncekidurum integer NOT NULL,
    yenidurum integer NOT NULL
);


ALTER TABLE public.uyedurumdegistirme OWNER TO postgres;

--
-- TOC entry 286 (class 1259 OID 38724)
-- Dependencies: 287 6
-- Name: uyedurumdegistirme_rid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE uyedurumdegistirme_rid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.uyedurumdegistirme_rid_seq OWNER TO postgres;

--
-- TOC entry 3236 (class 0 OID 0)
-- Dependencies: 286
-- Name: uyedurumdegistirme_rid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE uyedurumdegistirme_rid_seq OWNED BY uyedurumdegistirme.rid;


--
-- TOC entry 3237 (class 0 OID 0)
-- Dependencies: 286
-- Name: uyedurumdegistirme_rid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('uyedurumdegistirme_rid_seq', 6, true);


--
-- TOC entry 259 (class 1259 OID 37691)
-- Dependencies: 6
-- Name: uyekurum; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE uyekurum (
    rid bigint NOT NULL,
    uyeref bigint NOT NULL,
    kurumref bigint NOT NULL,
    adresref bigint,
    gorev character varying(30),
    baslangictarih date NOT NULL,
    bitistarih date,
    uyekurumdurum integer NOT NULL,
    varsayilan integer
);


ALTER TABLE public.uyekurum OWNER TO postgres;

--
-- TOC entry 260 (class 1259 OID 37694)
-- Dependencies: 259 6
-- Name: uyekurum_rid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE uyekurum_rid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.uyekurum_rid_seq OWNER TO postgres;

--
-- TOC entry 3238 (class 0 OID 0)
-- Dependencies: 260
-- Name: uyekurum_rid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE uyekurum_rid_seq OWNED BY uyekurum.rid;


--
-- TOC entry 3239 (class 0 OID 0)
-- Dependencies: 260
-- Name: uyekurum_rid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('uyekurum_rid_seq', 28, true);


--
-- TOC entry 261 (class 1259 OID 37696)
-- Dependencies: 6
-- Name: uyemuafiyet; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE uyemuafiyet (
    rid bigint NOT NULL,
    uyeref bigint NOT NULL,
    muafiyettip integer NOT NULL,
    evrakref bigint,
    aciklama character varying(200),
    baslangictarih date,
    bitistarih date
);


ALTER TABLE public.uyemuafiyet OWNER TO postgres;

--
-- TOC entry 262 (class 1259 OID 37699)
-- Dependencies: 6 261
-- Name: uyemuafiyet_rid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE uyemuafiyet_rid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.uyemuafiyet_rid_seq OWNER TO postgres;

--
-- TOC entry 3240 (class 0 OID 0)
-- Dependencies: 262
-- Name: uyemuafiyet_rid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE uyemuafiyet_rid_seq OWNED BY uyemuafiyet.rid;


--
-- TOC entry 3241 (class 0 OID 0)
-- Dependencies: 262
-- Name: uyemuafiyet_rid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('uyemuafiyet_rid_seq', 108, true);


--
-- TOC entry 263 (class 1259 OID 37701)
-- Dependencies: 6
-- Name: uyeodeme; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE uyeodeme (
    rid bigint NOT NULL,
    uyeaidatref bigint NOT NULL,
    tarih date NOT NULL,
    miktar numeric(14,10) NOT NULL,
    odemetip integer NOT NULL,
    muafiyetref bigint
);


ALTER TABLE public.uyeodeme OWNER TO postgres;

--
-- TOC entry 264 (class 1259 OID 37704)
-- Dependencies: 6 263
-- Name: uyeodeme_rid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE uyeodeme_rid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.uyeodeme_rid_seq OWNER TO postgres;

--
-- TOC entry 3242 (class 0 OID 0)
-- Dependencies: 264
-- Name: uyeodeme_rid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE uyeodeme_rid_seq OWNED BY uyeodeme.rid;


--
-- TOC entry 3243 (class 0 OID 0)
-- Dependencies: 264
-- Name: uyeodeme_rid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('uyeodeme_rid_seq', 6, true);


--
-- TOC entry 265 (class 1259 OID 37706)
-- Dependencies: 6
-- Name: uyeodul; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE uyeodul (
    rid bigint NOT NULL,
    uyeref bigint NOT NULL,
    odultipref bigint NOT NULL,
    evrakref bigint,
    tarih date NOT NULL,
    aciklama character varying(200)
);


ALTER TABLE public.uyeodul OWNER TO postgres;

--
-- TOC entry 266 (class 1259 OID 37709)
-- Dependencies: 6 265
-- Name: uyeodul_rid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE uyeodul_rid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.uyeodul_rid_seq OWNER TO postgres;

--
-- TOC entry 3244 (class 0 OID 0)
-- Dependencies: 266
-- Name: uyeodul_rid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE uyeodul_rid_seq OWNED BY uyeodul.rid;


--
-- TOC entry 3245 (class 0 OID 0)
-- Dependencies: 266
-- Name: uyeodul_rid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('uyeodul_rid_seq', 7, true);


--
-- TOC entry 267 (class 1259 OID 37711)
-- Dependencies: 6
-- Name: uyeogrenim; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE uyeogrenim (
    rid bigint NOT NULL,
    uyeref bigint NOT NULL,
    ogrenimtip integer NOT NULL,
    aciklama character varying(200),
    lisansunvan character varying(200),
    diplomano character varying(200),
    bitirmetarih date,
    varsayilan integer,
    universiteref bigint NOT NULL,
    fakulteref bigint NOT NULL,
    bolumref bigint NOT NULL,
    denklikdurum integer
);


ALTER TABLE public.uyeogrenim OWNER TO postgres;

--
-- TOC entry 268 (class 1259 OID 37717)
-- Dependencies: 267 6
-- Name: uyeogrenim_rid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE uyeogrenim_rid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.uyeogrenim_rid_seq OWNER TO postgres;

--
-- TOC entry 3246 (class 0 OID 0)
-- Dependencies: 268
-- Name: uyeogrenim_rid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE uyeogrenim_rid_seq OWNED BY uyeogrenim.rid;


--
-- TOC entry 3247 (class 0 OID 0)
-- Dependencies: 268
-- Name: uyeogrenim_rid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('uyeogrenim_rid_seq', 81, true);


--
-- TOC entry 269 (class 1259 OID 37719)
-- Dependencies: 6
-- Name: uyesigorta; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE uyesigorta (
    rid bigint NOT NULL,
    uyeref bigint NOT NULL,
    sigortatip integer NOT NULL,
    sigortasirket character varying(30) NOT NULL,
    policeno character varying(30) NOT NULL,
    baslangictarih date NOT NULL,
    bitistarih date NOT NULL,
    uyesigortadurum integer NOT NULL,
    evrakref bigint,
    aciklama character varying(200)
);


ALTER TABLE public.uyesigorta OWNER TO postgres;

--
-- TOC entry 270 (class 1259 OID 37722)
-- Dependencies: 6 269
-- Name: uyesigorta_rid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE uyesigorta_rid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.uyesigorta_rid_seq OWNER TO postgres;

--
-- TOC entry 3248 (class 0 OID 0)
-- Dependencies: 270
-- Name: uyesigorta_rid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE uyesigorta_rid_seq OWNED BY uyesigorta.rid;


--
-- TOC entry 3249 (class 0 OID 0)
-- Dependencies: 270
-- Name: uyesigorta_rid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('uyesigorta_rid_seq', 5, true);


--
-- TOC entry 271 (class 1259 OID 37724)
-- Dependencies: 6
-- Name: uyeuzmanlik; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE uyeuzmanlik (
    rid bigint NOT NULL,
    uyeref bigint NOT NULL,
    uzmanliktipref bigint NOT NULL,
    tarih date NOT NULL,
    aciklama character varying(200)
);


ALTER TABLE public.uyeuzmanlik OWNER TO postgres;

--
-- TOC entry 272 (class 1259 OID 37727)
-- Dependencies: 271 6
-- Name: uyeuzmanlik_rid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE uyeuzmanlik_rid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.uyeuzmanlik_rid_seq OWNER TO postgres;

--
-- TOC entry 3250 (class 0 OID 0)
-- Dependencies: 272
-- Name: uyeuzmanlik_rid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE uyeuzmanlik_rid_seq OWNED BY uyeuzmanlik.rid;


--
-- TOC entry 3251 (class 0 OID 0)
-- Dependencies: 272
-- Name: uyeuzmanlik_rid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('uyeuzmanlik_rid_seq', 7, true);


--
-- TOC entry 273 (class 1259 OID 37729)
-- Dependencies: 6
-- Name: uzmanliktip; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE uzmanliktip (
    rid bigint NOT NULL,
    kod character varying(30) NOT NULL,
    ad character varying(30) NOT NULL,
    aciklama character varying(200)
);


ALTER TABLE public.uzmanliktip OWNER TO postgres;

--
-- TOC entry 274 (class 1259 OID 37732)
-- Dependencies: 6 273
-- Name: uzmanliktip_rid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE uzmanliktip_rid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.uzmanliktip_rid_seq OWNER TO postgres;

--
-- TOC entry 3252 (class 0 OID 0)
-- Dependencies: 274
-- Name: uzmanliktip_rid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE uzmanliktip_rid_seq OWNED BY uzmanliktip.rid;


--
-- TOC entry 3253 (class 0 OID 0)
-- Dependencies: 274
-- Name: uzmanliktip_rid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('uzmanliktip_rid_seq', 5, true);


--
-- TOC entry 275 (class 1259 OID 37734)
-- Dependencies: 6 196
-- Name: village_rid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE village_rid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.village_rid_seq OWNER TO postgres;

--
-- TOC entry 3254 (class 0 OID 0)
-- Dependencies: 275
-- Name: village_rid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE village_rid_seq OWNED BY koy.rid;


--
-- TOC entry 3255 (class 0 OID 0)
-- Dependencies: 275
-- Name: village_rid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('village_rid_seq', 1, false);


--
-- TOC entry 276 (class 1259 OID 37736)
-- Dependencies: 6
-- Name: yayin; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE yayin (
    rid bigint NOT NULL,
    ad character varying(250) NOT NULL,
    yayintip integer NOT NULL,
    aciklama character varying(2500) NOT NULL
);


ALTER TABLE public.yayin OWNER TO postgres;

--
-- TOC entry 277 (class 1259 OID 37742)
-- Dependencies: 6 276
-- Name: yayin_rid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE yayin_rid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.yayin_rid_seq OWNER TO postgres;

--
-- TOC entry 3256 (class 0 OID 0)
-- Dependencies: 277
-- Name: yayin_rid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE yayin_rid_seq OWNED BY yayin.rid;


--
-- TOC entry 3257 (class 0 OID 0)
-- Dependencies: 277
-- Name: yayin_rid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('yayin_rid_seq', 5, true);


--
-- TOC entry 2418 (class 2604 OID 38521)
-- Dependencies: 141 140
-- Name: rid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY adres ALTER COLUMN rid SET DEFAULT nextval('adres_rid_seq'::regclass);


--
-- TOC entry 2420 (class 2604 OID 38522)
-- Dependencies: 143 142
-- Name: rid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY aidat ALTER COLUMN rid SET DEFAULT nextval('aidat_rid_seq'::regclass);


--
-- TOC entry 2513 (class 2604 OID 40699)
-- Dependencies: 290 291 291
-- Name: rid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY anadonem ALTER COLUMN rid SET DEFAULT nextval('anadonem_rid_seq'::regclass);


--
-- TOC entry 2421 (class 2604 OID 38523)
-- Dependencies: 145 144
-- Name: rid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY belgetip ALTER COLUMN rid SET DEFAULT nextval('belgetip_rid_seq'::regclass);


--
-- TOC entry 2510 (class 2604 OID 38693)
-- Dependencies: 285 284 285
-- Name: rid; Type: DEFAULT; Schema: public; Owner: toys
--

ALTER TABLE ONLY bilgisayar ALTER COLUMN rid SET DEFAULT nextval('bilgisayar_rid_seq'::regclass);


--
-- TOC entry 2422 (class 2604 OID 38524)
-- Dependencies: 242 146
-- Name: rid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY birim ALTER COLUMN rid SET DEFAULT nextval('unit_rid_seq'::regclass);


--
-- TOC entry 2423 (class 2604 OID 38525)
-- Dependencies: 148 147
-- Name: rid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY birimhesap ALTER COLUMN rid SET DEFAULT nextval('birimhesap_rid_seq'::regclass);


--
-- TOC entry 2424 (class 2604 OID 38526)
-- Dependencies: 150 149
-- Name: rid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY bolum ALTER COLUMN rid SET DEFAULT nextval('bolum_rid_seq'::regclass);


--
-- TOC entry 2425 (class 2604 OID 38527)
-- Dependencies: 152 151
-- Name: rid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY borc ALTER COLUMN rid SET DEFAULT nextval('borc_rid_seq'::regclass);


--
-- TOC entry 2426 (class 2604 OID 38528)
-- Dependencies: 154 153
-- Name: rid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY borcodeme ALTER COLUMN rid SET DEFAULT nextval('borcodeme_rid_seq'::regclass);


--
-- TOC entry 2427 (class 2604 OID 38529)
-- Dependencies: 156 155
-- Name: rid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cezatip ALTER COLUMN rid SET DEFAULT nextval('cezatip_rid_seq'::regclass);


--
-- TOC entry 2431 (class 2604 OID 38530)
-- Dependencies: 164 163
-- Name: rid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY donem ALTER COLUMN rid SET DEFAULT nextval('donem_rid_seq'::regclass);


--
-- TOC entry 2507 (class 2604 OID 38598)
-- Dependencies: 279 278 279
-- Name: rid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY dosyakodu ALTER COLUMN rid SET DEFAULT nextval('dosyakodu_rid_seq'::regclass);


--
-- TOC entry 2433 (class 2604 OID 38531)
-- Dependencies: 166 165
-- Name: rid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY duyuru ALTER COLUMN rid SET DEFAULT nextval('duyuru_rid_seq'::regclass);


--
-- TOC entry 2535 (class 2604 OID 45268)
-- Dependencies: 335 334 335
-- Name: rid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY egitim ALTER COLUMN rid SET DEFAULT nextval('egitim_rid_seq'::regclass);


--
-- TOC entry 2540 (class 2604 OID 45378)
-- Dependencies: 344 345 345
-- Name: rid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY egitimdegerlendirme ALTER COLUMN rid SET DEFAULT nextval('egitimdegerlendirme_rid_seq'::regclass);


--
-- TOC entry 2537 (class 2604 OID 45320)
-- Dependencies: 339 338 339
-- Name: rid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY egitimders ALTER COLUMN rid SET DEFAULT nextval('egitimders_rid_seq'::regclass);


--
-- TOC entry 2538 (class 2604 OID 45341)
-- Dependencies: 341 340 341
-- Name: rid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY egitimdosya ALTER COLUMN rid SET DEFAULT nextval('egitimdosya_rid_seq'::regclass);


--
-- TOC entry 2539 (class 2604 OID 45357)
-- Dependencies: 342 343 343
-- Name: rid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY egitimkatilimci ALTER COLUMN rid SET DEFAULT nextval('egitimkatilimci_rid_seq'::regclass);


--
-- TOC entry 2536 (class 2604 OID 45299)
-- Dependencies: 337 336 337
-- Name: rid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY egitimogretmen ALTER COLUMN rid SET DEFAULT nextval('egitimogretmen_rid_seq'::regclass);


--
-- TOC entry 2534 (class 2604 OID 45257)
-- Dependencies: 332 333 333
-- Name: rid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY egitimtanim ALTER COLUMN rid SET DEFAULT nextval('egitimtanim_rid_seq'::regclass);


--
-- TOC entry 2525 (class 2604 OID 45079)
-- Dependencies: 315 314 315
-- Name: rid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY epostaanlikgonderimlog ALTER COLUMN rid SET DEFAULT nextval('epostaanlikgonderimlog_rid_seq'::regclass);


--
-- TOC entry 2523 (class 2604 OID 45027)
-- Dependencies: 310 311 311
-- Name: rid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY epostagonderimlog ALTER COLUMN rid SET DEFAULT nextval('epostagonderimlog_rid_seq'::regclass);


--
-- TOC entry 2528 (class 2604 OID 45143)
-- Dependencies: 320 321 321
-- Name: rid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY etkinlik ALTER COLUMN rid SET DEFAULT nextval('etkinlik_rid_seq'::regclass);


--
-- TOC entry 2533 (class 2604 OID 45241)
-- Dependencies: 330 331 331
-- Name: rid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY etkinlikdosya ALTER COLUMN rid SET DEFAULT nextval('etkinlikdosya_rid_seq'::regclass);


--
-- TOC entry 2529 (class 2604 OID 45174)
-- Dependencies: 323 322 323
-- Name: rid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY etkinlikkatilimci ALTER COLUMN rid SET DEFAULT nextval('etkinlikkatilimci_rid_seq'::regclass);


--
-- TOC entry 2531 (class 2604 OID 45210)
-- Dependencies: 326 327 327
-- Name: rid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY etkinlikkurul ALTER COLUMN rid SET DEFAULT nextval('etkinlikkurul_rid_seq'::regclass);


--
-- TOC entry 2532 (class 2604 OID 45223)
-- Dependencies: 328 329 329
-- Name: rid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY etkinlikkuruluye ALTER COLUMN rid SET DEFAULT nextval('etkinlikkuruluye_rid_seq'::regclass);


--
-- TOC entry 2530 (class 2604 OID 45192)
-- Dependencies: 325 324 325
-- Name: rid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY etkinlikkurum ALTER COLUMN rid SET DEFAULT nextval('etkinlikkurum_rid_seq'::regclass);


--
-- TOC entry 2527 (class 2604 OID 45132)
-- Dependencies: 318 319 319
-- Name: rid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY etkinliktanim ALTER COLUMN rid SET DEFAULT nextval('etkinliktanim_rid_seq'::regclass);


--
-- TOC entry 2508 (class 2604 OID 38616)
-- Dependencies: 280 281 281
-- Name: rid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY evrak ALTER COLUMN rid SET DEFAULT nextval('evrak_rid_seq'::regclass);


--
-- TOC entry 2509 (class 2604 OID 38677)
-- Dependencies: 283 282 283
-- Name: rid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY evrakek ALTER COLUMN rid SET DEFAULT nextval('evrakek_rid_seq'::regclass);


--
-- TOC entry 2434 (class 2604 OID 38533)
-- Dependencies: 168 167
-- Name: rid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY fakulte ALTER COLUMN rid SET DEFAULT nextval('fakulte_rid_seq'::regclass);


--
-- TOC entry 2522 (class 2604 OID 45004)
-- Dependencies: 308 309 309
-- Name: rid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY gonderim ALTER COLUMN rid SET DEFAULT nextval('gonderim_rid_seq'::regclass);


--
-- TOC entry 2435 (class 2604 OID 38534)
-- Dependencies: 170 169
-- Name: rid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY gorev ALTER COLUMN rid SET DEFAULT nextval('gorev_rid_seq'::regclass);


--
-- TOC entry 2521 (class 2604 OID 44993)
-- Dependencies: 307 306 307
-- Name: rid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY icerik ALTER COLUMN rid SET DEFAULT nextval('icerik_rid_seq'::regclass);


--
-- TOC entry 2430 (class 2604 OID 38535)
-- Dependencies: 162 161
-- Name: rid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ilce ALTER COLUMN rid SET DEFAULT nextval('district_rid_seq'::regclass);


--
-- TOC entry 2519 (class 2604 OID 44959)
-- Dependencies: 303 302 303
-- Name: rid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY iletisimliste ALTER COLUMN rid SET DEFAULT nextval('iletisimliste_rid_seq'::regclass);


--
-- TOC entry 2520 (class 2604 OID 44980)
-- Dependencies: 305 304 305
-- Name: rid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY iletisimlistedetay ALTER COLUMN rid SET DEFAULT nextval('iletisimlistedetay_rid_seq'::regclass);


--
-- TOC entry 2437 (class 2604 OID 38536)
-- Dependencies: 172 171
-- Name: rid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY internetadres ALTER COLUMN rid SET DEFAULT nextval('internetadres_rid_seq'::regclass);


--
-- TOC entry 2438 (class 2604 OID 38537)
-- Dependencies: 239 173
-- Name: rid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY islem ALTER COLUMN rid SET DEFAULT nextval('transaction_rid_seq'::regclass);


--
-- TOC entry 2440 (class 2604 OID 38538)
-- Dependencies: 241 174
-- Name: rid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY islemkullanici ALTER COLUMN rid SET DEFAULT nextval('transactionsystemuser_rid_seq'::regclass);


--
-- TOC entry 2443 (class 2604 OID 38539)
-- Dependencies: 176 175
-- Name: rid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY islemlog ALTER COLUMN rid SET DEFAULT nextval('islemlog_rid_seq'::regclass);


--
-- TOC entry 2445 (class 2604 OID 38540)
-- Dependencies: 240 177
-- Name: rid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY islemrole ALTER COLUMN rid SET DEFAULT nextval('transactionrole_rid_seq'::regclass);


--
-- TOC entry 2446 (class 2604 OID 38541)
-- Dependencies: 179 178
-- Name: rid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY isyeritemsilcileri ALTER COLUMN rid SET DEFAULT nextval('isyeritemsilcileri_rid_seq'::regclass);


--
-- TOC entry 2448 (class 2604 OID 38542)
-- Dependencies: 181 180
-- Name: rid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY kisi ALTER COLUMN rid SET DEFAULT nextval('kisi_rid_seq'::regclass);


--
-- TOC entry 2449 (class 2604 OID 38543)
-- Dependencies: 183 182
-- Name: rid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY kisifotograf ALTER COLUMN rid SET DEFAULT nextval('kisifotograf_rid_seq'::regclass);


--
-- TOC entry 2450 (class 2604 OID 38544)
-- Dependencies: 185 184
-- Name: rid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY kisiizin ALTER COLUMN rid SET DEFAULT nextval('kisiizin_rid_seq'::regclass);


--
-- TOC entry 2451 (class 2604 OID 38545)
-- Dependencies: 187 186
-- Name: rid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY kisikimlik ALTER COLUMN rid SET DEFAULT nextval('kisikimlik_rid_seq'::regclass);


--
-- TOC entry 2452 (class 2604 OID 38546)
-- Dependencies: 189 188
-- Name: rid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY kisiuyruk ALTER COLUMN rid SET DEFAULT nextval('kisiuyruk_rid_seq'::regclass);


--
-- TOC entry 2453 (class 2604 OID 38547)
-- Dependencies: 191 190
-- Name: rid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY komisyon ALTER COLUMN rid SET DEFAULT nextval('komisyon_rid_seq'::regclass);


--
-- TOC entry 2454 (class 2604 OID 38548)
-- Dependencies: 193 192
-- Name: rid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY komisyondonem ALTER COLUMN rid SET DEFAULT nextval('komisyondonem_rid_seq'::regclass);


--
-- TOC entry 2515 (class 2604 OID 40785)
-- Dependencies: 294 295 295
-- Name: rid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY komisyondonemtoplanti ALTER COLUMN rid SET DEFAULT nextval('komisyondonemtoplanti_rid_seq'::regclass);


--
-- TOC entry 2517 (class 2604 OID 40819)
-- Dependencies: 298 299 299
-- Name: rid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY komisyondonemtoplantitutanak ALTER COLUMN rid SET DEFAULT nextval('komisyondonemtoplantitutanak_rid_seq'::regclass);


--
-- TOC entry 2457 (class 2604 OID 38549)
-- Dependencies: 195 194
-- Name: rid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY komisyondonemuye ALTER COLUMN rid SET DEFAULT nextval('komisyondonemuye_rid_seq'::regclass);


--
-- TOC entry 2458 (class 2604 OID 38550)
-- Dependencies: 275 196
-- Name: rid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY koy ALTER COLUMN rid SET DEFAULT nextval('village_rid_seq'::regclass);


--
-- TOC entry 2460 (class 2604 OID 38551)
-- Dependencies: 235 197
-- Name: rid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY kullanici ALTER COLUMN rid SET DEFAULT nextval('systemuser_rid_seq'::regclass);


--
-- TOC entry 2461 (class 2604 OID 38552)
-- Dependencies: 236 198
-- Name: rid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY kullanicirole ALTER COLUMN rid SET DEFAULT nextval('systemuserrole_rid_seq'::regclass);


--
-- TOC entry 2462 (class 2604 OID 38553)
-- Dependencies: 200 199
-- Name: rid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY kurul ALTER COLUMN rid SET DEFAULT nextval('kurul_rid_seq'::regclass);


--
-- TOC entry 2463 (class 2604 OID 38554)
-- Dependencies: 202 201
-- Name: rid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY kuruldonem ALTER COLUMN rid SET DEFAULT nextval('kuruldonem_rid_seq'::regclass);


--
-- TOC entry 2518 (class 2604 OID 40847)
-- Dependencies: 301 300 301
-- Name: rid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY kuruldonemdelege ALTER COLUMN rid SET DEFAULT nextval('kuruldonemdelege_rid_seq'::regclass);


--
-- TOC entry 2514 (class 2604 OID 40772)
-- Dependencies: 293 292 293
-- Name: rid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY kuruldonemtoplanti ALTER COLUMN rid SET DEFAULT nextval('kuruldonemtoplanti_rid_seq'::regclass);


--
-- TOC entry 2516 (class 2604 OID 40798)
-- Dependencies: 296 297 297
-- Name: rid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY kuruldonemtoplantitutanak ALTER COLUMN rid SET DEFAULT nextval('kuruldonemtoplantitutanak_rid_seq'::regclass);


--
-- TOC entry 2467 (class 2604 OID 38555)
-- Dependencies: 204 203
-- Name: rid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY kuruldonemuye ALTER COLUMN rid SET DEFAULT nextval('kuruldonemuye_rid_seq'::regclass);


--
-- TOC entry 2468 (class 2604 OID 38556)
-- Dependencies: 206 205
-- Name: rid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY kurulgorev ALTER COLUMN rid SET DEFAULT nextval('kurulgorev_rid_seq'::regclass);


--
-- TOC entry 2469 (class 2604 OID 38557)
-- Dependencies: 208 207
-- Name: rid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY kurum ALTER COLUMN rid SET DEFAULT nextval('kurum_rid_seq'::regclass);


--
-- TOC entry 2470 (class 2604 OID 38558)
-- Dependencies: 210 209
-- Name: rid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY kurumhesap ALTER COLUMN rid SET DEFAULT nextval('kurumhesap_rid_seq'::regclass);


--
-- TOC entry 2471 (class 2604 OID 38559)
-- Dependencies: 212 211
-- Name: rid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY kurumkisi ALTER COLUMN rid SET DEFAULT nextval('kurumkisi_rid_seq'::regclass);


--
-- TOC entry 2472 (class 2604 OID 38560)
-- Dependencies: 214 213
-- Name: rid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY kurumturu ALTER COLUMN rid SET DEFAULT nextval('kurumturu_rid_seq'::regclass);


--
-- TOC entry 2473 (class 2604 OID 38561)
-- Dependencies: 216 215
-- Name: rid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY menuitem ALTER COLUMN rid SET DEFAULT nextval('menuitem_rid_seq'::regclass);


--
-- TOC entry 2474 (class 2604 OID 38562)
-- Dependencies: 218 217
-- Name: rid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY odeme ALTER COLUMN rid SET DEFAULT nextval('odeme_rid_seq'::regclass);


--
-- TOC entry 2475 (class 2604 OID 38563)
-- Dependencies: 220 219
-- Name: rid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY odemetip ALTER COLUMN rid SET DEFAULT nextval('odemetip_rid_seq'::regclass);


--
-- TOC entry 2476 (class 2604 OID 38564)
-- Dependencies: 222 221
-- Name: rid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY odultip ALTER COLUMN rid SET DEFAULT nextval('odultip_rid_seq'::regclass);


--
-- TOC entry 2477 (class 2604 OID 38565)
-- Dependencies: 224 223
-- Name: rid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ogrenimkurum ALTER COLUMN rid SET DEFAULT nextval('ogrenimkurum_rid_seq'::regclass);


--
-- TOC entry 2478 (class 2604 OID 38566)
-- Dependencies: 226 225
-- Name: rid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY personel ALTER COLUMN rid SET DEFAULT nextval('personel_rid_seq'::regclass);


--
-- TOC entry 2480 (class 2604 OID 38567)
-- Dependencies: 228 227
-- Name: rid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY personelbirimgorev ALTER COLUMN rid SET DEFAULT nextval('personelbirimgorev_rid_seq'::regclass);


--
-- TOC entry 2481 (class 2604 OID 38568)
-- Dependencies: 230 229
-- Name: rid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY role ALTER COLUMN rid SET DEFAULT nextval('role_rid_seq'::regclass);


--
-- TOC entry 2428 (class 2604 OID 38569)
-- Dependencies: 158 157
-- Name: rid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY sehir ALTER COLUMN rid SET DEFAULT nextval('city_rid_seq'::regclass);


--
-- TOC entry 2483 (class 2604 OID 38570)
-- Dependencies: 232 231
-- Name: rid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY sistemParametre ALTER COLUMN rid SET DEFAULT nextval('sistemParametre_rid_seq'::regclass);


--
-- TOC entry 2526 (class 2604 OID 45105)
-- Dependencies: 317 316 317
-- Name: rid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY smsanlikgonderimlog ALTER COLUMN rid SET DEFAULT nextval('smsanlikgonderimlog_rid_seq'::regclass);


--
-- TOC entry 2524 (class 2604 OID 45053)
-- Dependencies: 312 313 313
-- Name: rid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY smsgonderimlog ALTER COLUMN rid SET DEFAULT nextval('smsgonderimlog_rid_seq'::regclass);


--
-- TOC entry 2484 (class 2604 OID 38571)
-- Dependencies: 234 233
-- Name: rid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY sorun ALTER COLUMN rid SET DEFAULT nextval('sorun_rid_seq'::regclass);


--
-- TOC entry 2486 (class 2604 OID 38572)
-- Dependencies: 238 237
-- Name: rid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY telefon ALTER COLUMN rid SET DEFAULT nextval('telefon_rid_seq'::regclass);


--
-- TOC entry 2429 (class 2604 OID 38573)
-- Dependencies: 160 159
-- Name: rid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ulke ALTER COLUMN rid SET DEFAULT nextval('country_rid_seq'::regclass);


--
-- TOC entry 2487 (class 2604 OID 38574)
-- Dependencies: 244 243
-- Name: rid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY universite ALTER COLUMN rid SET DEFAULT nextval('universite_rid_seq'::regclass);


--
-- TOC entry 2488 (class 2604 OID 38575)
-- Dependencies: 246 245
-- Name: rid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY uyarinot ALTER COLUMN rid SET DEFAULT nextval('uyarinot_rid_seq'::regclass);


--
-- TOC entry 2492 (class 2604 OID 38576)
-- Dependencies: 248 247
-- Name: rid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY uye ALTER COLUMN rid SET DEFAULT nextval('uye_rid_seq'::regclass);


--
-- TOC entry 2493 (class 2604 OID 38577)
-- Dependencies: 250 249
-- Name: rid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY uyeabone ALTER COLUMN rid SET DEFAULT nextval('uyeabone_rid_seq'::regclass);


--
-- TOC entry 2494 (class 2604 OID 38578)
-- Dependencies: 252 251
-- Name: rid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY uyeaidat ALTER COLUMN rid SET DEFAULT nextval('uyeaidat_rid_seq'::regclass);


--
-- TOC entry 2512 (class 2604 OID 38747)
-- Dependencies: 288 289 289
-- Name: rid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY uyebasvuru ALTER COLUMN rid SET DEFAULT nextval('uyebasvuru_rid_seq'::regclass);


--
-- TOC entry 2495 (class 2604 OID 38579)
-- Dependencies: 254 253
-- Name: rid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY uyebelge ALTER COLUMN rid SET DEFAULT nextval('uyebelge_rid_seq'::regclass);


--
-- TOC entry 2496 (class 2604 OID 38580)
-- Dependencies: 256 255
-- Name: rid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY uyebilirkisi ALTER COLUMN rid SET DEFAULT nextval('uyebilirkisi_rid_seq'::regclass);


--
-- TOC entry 2497 (class 2604 OID 38581)
-- Dependencies: 258 257
-- Name: rid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY uyeceza ALTER COLUMN rid SET DEFAULT nextval('uyeceza_rid_seq'::regclass);


--
-- TOC entry 2511 (class 2604 OID 38729)
-- Dependencies: 286 287 287
-- Name: rid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY uyedurumdegistirme ALTER COLUMN rid SET DEFAULT nextval('uyedurumdegistirme_rid_seq'::regclass);


--
-- TOC entry 2498 (class 2604 OID 38582)
-- Dependencies: 260 259
-- Name: rid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY uyekurum ALTER COLUMN rid SET DEFAULT nextval('uyekurum_rid_seq'::regclass);


--
-- TOC entry 2499 (class 2604 OID 38583)
-- Dependencies: 262 261
-- Name: rid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY uyemuafiyet ALTER COLUMN rid SET DEFAULT nextval('uyemuafiyet_rid_seq'::regclass);


--
-- TOC entry 2500 (class 2604 OID 38584)
-- Dependencies: 264 263
-- Name: rid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY uyeodeme ALTER COLUMN rid SET DEFAULT nextval('uyeodeme_rid_seq'::regclass);


--
-- TOC entry 2501 (class 2604 OID 38585)
-- Dependencies: 266 265
-- Name: rid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY uyeodul ALTER COLUMN rid SET DEFAULT nextval('uyeodul_rid_seq'::regclass);


--
-- TOC entry 2502 (class 2604 OID 38586)
-- Dependencies: 268 267
-- Name: rid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY uyeogrenim ALTER COLUMN rid SET DEFAULT nextval('uyeogrenim_rid_seq'::regclass);


--
-- TOC entry 2503 (class 2604 OID 38587)
-- Dependencies: 270 269
-- Name: rid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY uyesigorta ALTER COLUMN rid SET DEFAULT nextval('uyesigorta_rid_seq'::regclass);


--
-- TOC entry 2504 (class 2604 OID 38588)
-- Dependencies: 272 271
-- Name: rid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY uyeuzmanlik ALTER COLUMN rid SET DEFAULT nextval('uyeuzmanlik_rid_seq'::regclass);


--
-- TOC entry 2505 (class 2604 OID 38589)
-- Dependencies: 274 273
-- Name: rid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY uzmanliktip ALTER COLUMN rid SET DEFAULT nextval('uzmanliktip_rid_seq'::regclass);


--
-- TOC entry 2506 (class 2604 OID 38590)
-- Dependencies: 277 276
-- Name: rid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY yayin ALTER COLUMN rid SET DEFAULT nextval('yayin_rid_seq'::regclass);


--
-- TOC entry 2945 (class 0 OID 37342)
-- Dependencies: 140
-- Data for Name: adres; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY adres (rid, adresturu, kisiref, varsayilan, kurumref, adrestipi, kpsguncellemetarihi, acikadres, adresno, binablokadi, binakodu, binasiteadi, csbm, csbmkodu, diskapino, ickapino, mahalle, mahallekodu, sehirref, ilceref, beyantarihi, binaada, binapafta, binaparsel, tasinmatarihi, tesciltarihi, ulkeref) FROM stdin;
70	1	\N	1	4	1	\N	Deneme adresi	\N		\N			\N			Test Mahallesi	\N	6	185	\N	\N	\N	\N	\N	\N	1
75	3	89	2	\N	1	2013-07-11	ARAPSUYU MAH. 659 SK. PEMBE KÖŞK APT. BLOK  NO: 9  İÇ KAPI NO: 2 KONYAALTI / ANTALYA	1773400427	PEMBE KÖŞK APT.			659 SK.	388331	9	2	ARAPSUYU MAH.	2333	7	216	2013-04-12	3815		6	2013-04-08	2013-04-12	\N
76	1	89	1	\N	1	2013-07-11	ARAPSUYU MAH. 659 SK. PEMBE KÖŞK APT. BLOK  NO: 9  İÇ KAPI NO: 2 KONYAALTI / ANTALYA	1773400427	PEMBE KÖŞK APT.			659 SK.	388331	9	2	ARAPSUYU MAH.	2333	7	216	2013-04-12	3815		6	2013-04-08	2013-04-12	1
77	1	89	1	\N	1	2013-07-12	ARAPSUYU MAH. 659 SK. PEMBE KÖŞK APT. BLOK  NO: 9  İÇ KAPI NO: 2 KONYAALTI / ANTALYA	1773400427	PEMBE KÖŞK APT.			659 SK.	388331	9	2	ARAPSUYU MAH.	2333	7	216	2013-04-12	3815		6	2013-04-08	2013-04-12	1
79	1	62	1	\N	1	\N	açık adresi 2	\N		\N			\N			test	\N	20	112	\N	\N	\N	\N	\N	\N	1
78	1	62	2	\N	1	\N	Açık adresi	\N		\N			\N			Deneme	\N	6	185	\N	\N	\N	\N	\N	\N	1
80	1	89	1	\N	1	2013-07-29	ARAPSUYU MAH. 659 SK. PEMBE KÖŞK APT. BLOK  NO: 9  İÇ KAPI NO: 2 KONYAALTI / ANTALYA	1773400427	PEMBE KÖŞK APT.			659 SK.	388331	9	2	ARAPSUYU MAH.	2333	7	216	2013-04-12	3815		6	2013-04-08	2013-04-12	1
\.


--
-- TOC entry 2946 (class 0 OID 37351)
-- Dependencies: 142
-- Data for Name: aidat; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY aidat (rid, baslangictarih, bitistarih, uyetip, miktar, yil, borclandirmayapildi) FROM stdin;
1	2013-01-01	2013-12-31	1	12.5000000000	2014	1
2	2013-01-01	2013-12-31	1	10.0000000000	2013	1
\.


--
-- TOC entry 3020 (class 0 OID 40696)
-- Dependencies: 291
-- Data for Name: anadonem; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY anadonem (rid, tanim, aktif) FROM stdin;
1	1. Dönem	\N
\.


--
-- TOC entry 2947 (class 0 OID 37357)
-- Dependencies: 144
-- Data for Name: belgetip; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY belgetip (rid, kod, ad, aciklama) FROM stdin;
1	001	Belge Tipi 1	
\.


--
-- TOC entry 3017 (class 0 OID 38690)
-- Dependencies: 285
-- Data for Name: bilgisayar; Type: TABLE DATA; Schema: public; Owner: toys
--

COPY bilgisayar (rid, marka, uretimyili) FROM stdin;
1	HP	2013
2	Dell	2012
3	test	2013
\.


--
-- TOC entry 2948 (class 0 OID 37362)
-- Dependencies: 146
-- Data for Name: birim; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY birim (rid, ustref, ad, kisaad, durum, erisimkodu, birimtip, ilceref, sehirref, kurumref) FROM stdin;
1	\N	TMMOB	TMMOB	2	1	1	185	6	1
3	1	Bilg. Müh. Odası	BMO	2	1.3	1	116	1	1
4	1	Makine Müh. Odası	MMO	2	1.4	1	131	2	1
5	1	Makine Müh. Odası Ankara Şubesi	MMO Ankara	2	1.5	4	116	1	1
8	3	Wizard Birimi	wz	1	1.3.8	1	116	1	1
9	3	test	test	1	1.3.9	2	185	6	9
11	3	test_18_07		2	1.3.11	1	185	6	1
12	3	fdsf		1	1.3.12	1	140	3	9
\.


--
-- TOC entry 2949 (class 0 OID 37368)
-- Dependencies: 147
-- Data for Name: birimhesap; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY birimhesap (rid, hesapno, subekodu, ibannumarasi, gecerli, birimref, bankaadi) FROM stdin;
\.


--
-- TOC entry 2950 (class 0 OID 37373)
-- Dependencies: 149
-- Data for Name: bolum; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY bolum (rid, ad, fakulteref) FROM stdin;
2	Matematik Bölümü	3
3	Bilgisayar Muh Bölümü	1
4	Bilgisayar Muh Bölümü	2
5	Malzeme Mühendisliği Bölümü	1
\.


--
-- TOC entry 2951 (class 0 OID 37378)
-- Dependencies: 151
-- Data for Name: borc; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY borc (rid, kisiref, odemetipref, tarih, miktar, odenen, borcdurum, birimref) FROM stdin;
6	71	1	2013-06-26	0.0000000000	0.0000000000	2	\N
\.


--
-- TOC entry 2952 (class 0 OID 37383)
-- Dependencies: 153
-- Data for Name: borcodeme; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY borcodeme (rid, borcref, tarih, miktar, odemetip, odemeref) FROM stdin;
\.


--
-- TOC entry 2953 (class 0 OID 37388)
-- Dependencies: 155
-- Data for Name: cezatip; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY cezatip (rid, kisaad, ad, aciklama) FROM stdin;
1	KI	Kınama	
\.


--
-- TOC entry 2957 (class 0 OID 37408)
-- Dependencies: 163
-- Data for Name: donem; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY donem (rid, baslangictarih, bitistarih, tanim, anadonemref, birimref) FROM stdin;
3	2013-06-01	2013-12-31	1. Dönem	1	1
10	2013-07-10	2013-07-31	1.Dönem	1	3
\.


--
-- TOC entry 3014 (class 0 OID 38595)
-- Dependencies: 279
-- Data for Name: dosyakodu; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY dosyakodu (rid, ustref, kod, ad, erisimkodu) FROM stdin;
6	2	04	Yönergeler	1.2.6
2	1	010	Mevzuat İşleri	1.2
3	2	01	Kanunlar	1.2.3
4	2	02	Tüzükler	1.2.4
5	2	03	Yönetmelikler	1.2.5
7	2	05	Tebliğler	1.2.7
8	2	06	Genelgeler	1.2.8
9	8	01	İç Genelgeler	1.2.8.9
10	8	02	Dış Genelgeler	1.2.8.10
11	2	07	Talimatlar, Duyurular, Sirkülerler	1.2.11
12	11	01	Talimatlar	1.2.11.12
13	11	02	Duyurular	1.2.11.13
14	11	03	Sirkülerler	1.2.11.14
15	2	08	Rehber, Kılavuz	1.2.15
16	2	09	Standartlar	1.2.16
17	2	99	Diğer	1.2.17
20	19	01	Anlaşmalar	1.19.20
21	19	02	Sözleşmeler	1.19.21
22	19	03	Protokoller	1.19.22
23	19	04	Şartnameler	1.19.23
24	23	01	İdari	1.19.23.24
25	23	02	Teknik	1.19.23.25
29	28	01	Haftalık	1.28.29
30	28	02	Aylık	1.28.30
31	28	03	Üç Aylık	1.28.31
32	28	04	Altı Aylık	1.28.32
33	28	05	Yıllık	1.28.33
34	28	06	Konsolide	1.28.34
36	35	01	Brifingler	1.35.36
37	35	02	Bilgi Notları	1.35.37
41	40	01	Teknik ve Mesleki	1.40.41
42	40	02	Hukuki	1.40.42
45	44	01	Genel Kurullar, Senatolar, Meclisler	1.44.45
46	45	01	Seçim ve Atama İşleri	1.44.45.46
47	45	02	Davetler ve Gündemler	1.44.45.47
48	45	03	Tutanaklar	1.44.45.48
49	45	04	Kararlar	1.44.45.49
50	45	05	Ödemeler	1.44.45.50
51	44	02	Yönetim Kurulları, Belediye Encümeni	1.44.51
52	44	03	Danışma, Disiplin, Koordinasyon Kurulları	1.44.52
53	44	04	İhtisas Kurulları	1.44.53
54	44	05	Konseyler	1.44.54
55	44	06	Komite, Komisyonlar	1.44.55
57	56	01	Kongre	1.56.57
58	56	02	Konferans	1.56.58
59	56	03	Şura	1.56.59
60	56	04	Sempozyum	1.56.60
61	56	05	Seminer	1.56.61
62	56	06	Bienal	1.56.62
63	56	07	Kurultay	1.56.63
64	56	08	Çalıştay	1.56.64
65	56	09	Panel	1.56.65
66	56	10	Kollokyum	1.56.66
68	67	01	Kalite El Kitabı	1.67.68
69	67	02	Temel Prosedürler ve Bağlı İşlemler	1.67.69
72	67	05	Formlar	1.67.72
73	67	04	Talimatlar	1.67.73
74	67	07	İzleme ve Ölçme İşlemleri	1.67.74
75	74	01	Müşteri Memnuniyeti (Veri Analizi)	1.67.74.75
76	74	02	Tetkik İşlemleri	1.67.74.76
77	74	03	İzleme ve Ölçme Sonuçlarının Analizi (Veri Analizi)	1.67.74.77
78	67	08	İyileştirme İşlemleri	1.67.78
79	78	01	Düzeltici Faaliyetler	1.67.78.79
80	78	02	Önleyici Faaliyetler	1.67.78.80
81	67	09	Bireysel Öneriler	1.67.81
82	67	10	Yönetimin Gözden Geçirmesi	1.67.82
83	67	11	Hizmet (Ürün) Gerçekleştirme	1.67.83
84	83	01	Planlama	1.67.83.84
85	19	99	Diğer	1.19.85
86	28	99	Diğer	1.28.86
87	35	99	Diğer	1.35.87
88	40	03	İdari	1.40.88
89	40	99	Diğer	1.40.89
90	44	99	Diğer	1.44.90
91	56	99	Diğer	1.56.91
92	83	02	Hizmete Bağlı Şartların Belirlenmesi ve Gözden Geçirilmesi	1.67.83.92
93	83	03	Tasarım ve Geliştirme	1.67.83.93
94	83	04	Üretim ve Hizmet (Ürün) Sağlanması (Sunulması)	1.67.83.94
95	67	99	Diğer	1.67.95
98	97	01	Kuruluş, Yapılanma	1.97.98
99	97	02	Yetkilendirme, Yetki Değişikliği	1.97.99
100	97	03	Yönetimi Geliştirme	1.97.100
101	97	04	Teşkilat Şemaları ve Kadrolar	1.97.101
102	101	01	Teşkilat Şemaları	1.97.101.102
103	101	02	Norm Kadro Çalışmaları	1.97.101.103
104	103	01	Görev/İş Tanımları	1.97.101.103.104
105	103	02	Norm Kadro Pozisyonları	1.97.101.103.105
106	97	99	Diğer	1.97.106
108	107	01	Kalkınma Planı	1.107.108
109	108	01	Hazırlık Çalışmaları	1.107.108.109
110	108	02	İzleme ve Değerlendirme	1.107.108.110
111	107	02	Orta Vadeli Plan/Program	1.107.111
112	107	03	Yıllık Plan/Program	1.107.112
113	107	04	Stratejik Plan	1.107.113
114	113	01	Veri Toplama	1.107.113.114
115	113	02	Analiz ve Raporlama	1.107.113.115
116	113	03	Revizyon	1.107.113.116
117	107	05	Eylem Planları	1.107.117
118	107	06	Yönetim/İş Planları	1.107.118
119	107	07	Yatırım Programları	1.107.119
120	119	01	Hazırlık Çalışmaları	1.107.119.120
121	119	02	Yatırım Revizyonu	1.107.119.121
122	119	03	Ek Ödenek	1.107.119.122
123	119	04	Ödenek Aktarma	1.107.119.123
124	119	05	Serbest Bırakma	1.107.119.124
125	119	06	Maliyet Revizyonu	1.107.119.125
126	119	07	bonem Gerçekleştirme Raporları	1.107.119.126
127	107	08	Performans Programı	1.107.127
128	127	01	Veri Toplama	1.107.127.128
129	127	02	Maliyetlendirme	1.107.127.129
130	127	03	İzleme ve Değerlendirme	1.107.127.130
131	127	04	Revize İşlemleri	1.107.127.131
132	107	99	Diğer	1.107.132
134	133	01	Araştırma Projeleri	1.133.134
135	134	01	Başvurular ve Proje Önerileri	1.133.134.135
136	134	02	Değerlendirme ve Onay	1.133.134.136
137	134	03	İzleme ve Raporlandırma	1.133.134.137
138	134	04	Değişiklikler ve Tadilatlar	1.133.134.138
139	134	05	Ödemeler ve Mali İncelemeler	1.133.134.139
140	134	06	Tescil/Patent/Fikri Mülkiyet İşlemleri	1.133.134.140
141	133	02	Araştırma/Geliştirme Projeleri	1.133.141
142	133	99	Diğer	1.133.142
144	143	01	Veri Toplama	1.143.144
145	143	02	Raporlama	1.143.145
146	143	99	Diğer	1.143.146
150	149	01	Çevre Yönetimi	1.149.150
151	149	02	ÇED	1.149.151
152	149	03	Denetim	1.149.152
153	149	04	İklim Değişikliği	1.149.153
154	149	99	Diğer	1.149.154
156	155	01	İç Kontrol Sistemi ve Standartlar	1.155.156
157	156	01	Süreçler	1.155.156.157
158	156	02	İzleme ve Değerlendirme	1.155.156.158
159	156	03	İnceleme ve Raporlama	1.155.156.159
160	155	02	Ön Mali Kontrol	1.155.160
161	155	99	Diğer	1.155.161
165	164	01	Basın Toplantıları	1.164.165
166	164	02	Basına Verilen Demeçler	1.164.166
167	164	03	Tekzipler	1.164.167
168	164	04	Basında Yer Alan Haberler	1.164.168
169	164	05	Basın Özetleri	1.164.169
170	164	99	Diğer	1.164.170
172	171	01	Talep ve Şikayetler	1.171.172
173	171	02	Görüş ve Teklifler	1.171.173
174	171	03	Bilgi ve Belge Talepleri	1.171.174
175	171	99	Diğer	1.171.175
179	178	01	Avrupa İnsan Haklan Mahkemesi	1.178.179
180	178	02	Tahkim	1.178.180
181	180	01	Ulusal	1.178.180.181
182	180	02	Uluslararası	1.178.180.182
183	178	03	Adli Davalar	1.178.183
184	183	01	Hukuk	1.178.183.184
185	183	02	Ceza	1.178.183.185
186	183	03	İcra	1.178.183.186
187	178	04	İdari Davalar	1.178.187
188	178	05	Vergi Davaları	1.178.188
189	178	99	Diğer	1.178.189
197	196	01	Yıllık	1.196.197
198	196	02	Dönemsel	1.196.198
199	196	99	Diğer	1.196.199
202	201	01	Cevaplı	1.201.202
203	201	02	Denetim	1.201.203
204	201	03	Genel Durum	1.201.204
205	201	04	Ön İnceleme	1.201.205
206	201	05	İnceleme	1.201.206
207	201	06	Basit	1.201.207
208	201	07	Soruşturma	1.201.208
209	201	08	Araştırma	1.201.209
210	201	09	Bilgi	1.201.210
211	201	99	Diğer	1.201.211
213	212	01	İhbar	1.212.213
214	212	02	Suç Duyuruları	1.212.214
215	212	99	Diğer	1.212.215
217	216	01	Yüksek Denetleme Kurulu Raporları	1.216.217
321	310	11	Ombudsman	1.310.321
218	217	01	İvedi Durum Raporları	1.216.217.218
219	217	02	Özel İnceleme Raporları	1.216.217.219
220	217	03	Dönem Raporları	1.216.217.220
221	217	04	Yıllık Raporlar	1.216.217.221
222	217	05	Kuruluşun Cevabi Raporları	1.216.217.222
223	216	02	KİT Komisyonu Raporları	1.216.223
224	216	99	Diğer	1.216.224
228	227	01	Analiz ve Tasarım Çalışmaları	1.227.228
229	227	02	Kodlama Çalışmaları	1.227.229
230	227	03	Test Çalışmaları	1.227.230
231	227	99	Diğer	1.227.231
233	232	01	Proje Yönetimi	1.232.233
234	232	02	Değişiklik Yönetimi	1.232.234
235	232	03	Güvenlik Yönetimi	1.232.235
236	232	04	Kaynak Yönetimi	1.232.236
237	232	05	Konfigürasyon Yönetimi	1.232.237
238	232	06	Olağanüstü Durum Yönetimi	1.232.238
239	232	07	Problem Yönetimi	1.232.239
240	232	99	Diğer	1.232.240
242	241	01	İnternet	1.241.242
243	241	02	Data Hattı	1.241.243
244	241	03	İntranet	1.241.244
245	241	99	Diğer	1.241.245
247	246	01	Yazılım Talepleri	1.246.247
248	246	02	Donanım Talepleri	1.246.248
249	246	99	Diğer	1.246.249
251	250	01	Kayıt	1.250.251
252	250	02	Düzeltme	1.250.252
253	250	03	Silme	1.250.253
254	250	04	Aktarma	1.250.254
255	250	05	Sorgulama	1.250.255
256	250	99	Diğer	1.250.256
258	257	01	Web Sayfası	1.257.258
259	257	02	Abonelik	1.257.259
260	257	03	e-imza	1.257.260
261	257	04	e-posta İşlemleri	1.257.261
262	257	05	Liste İşlemleri	1.257.262
263	257	99	Diğer	1.257.263
265	264	01	Yetki Verilmesi	1.264.265
266	264	02	Yetki Kaldırılması	1.264.266
267	264	03	Yetki Değişikliği	1.264.267
268	264	99	Diğer	1.264.268
273	272	01	Ülkelerle İkili İlişkiler	1.272.273
274	273	01	Genel Bilgiler	1.272.273.274
275	273	02	Anlaşmalar, Sözleşmeler, Protokoller	1.272.273.275
276	273	03	İşbirliği	1.272.273.276
277	273	04	Sorunlar	1.272.273.277
278	273	05	Yardımlar	1.272.273.278
279	272	02	Çok Taraflı Ülke İlişkileri	1.272.279
280	272	03	Türk ve Müslüman Topluluklarla İlişkiler	1.272.280
281	272	99	Diğer	1.272.281
283	282	01	Politikalar	1.282.283
284	282	02	Mevzuat ve Üyelik İşlemleri	1.282.284
285	284	01	Tüzük	1.282.284.285
286	284	02	Üyelik İşlemleri	1.282.284.286
287	284	03	Katkı Payları	1.282.284.287
288	282	03	Toplantılar	1.282.288
289	288	01	Genel Kurul	1.282.288.289
290	288	02	Yönetim Kurulu	1.282.288.290
291	288	03	Komiteler	1.282.288.291
292	288	04	Alt komiteler	1.282.288.292
293	288	05	Çalışma Grupları	1.282.288.293
294	282	06	Projeler	1.282.294
295	294	01	Uluslararası Projeler	1.282.294.295
296	294	02	Türkiye"ye Yönelik Projeler	1.282.294.296
297	282	07	İşbirliği	1.282.297
298	282	08	Rapor, İnceleme, Anket ve İstatistikler	1.282.298
299	298	01	Raporlar	1.282.298.299
300	298	02	İncelemeler	1.282.298.300
301	298	03	Anketler	1.282.298.301
302	298	04	İstatistikler	1.282.298.302
303	282	09	İrtibat Büroları	1.282.303
304	282	10	Bilgi ve Belge Talepleri	1.282.304
305	282	11	Başvuru ve Şikayetler	1.282.305
306	282	12	Denetim	1.282.306
307	282	13	Yardımlar	1.282.307
308	282	99	Diğer	1.282.308
311	310	01	Bakanlar Konseyi	1.310.311
312	310	02	Avrupa Komisyonu	1.310.312
313	310	03	Avrupa Parlamentosu	1.310.313
314	310	04	Avrupa Konseyi	1.310.314
315	310	05	Adalet Divanı	1.310.315
316	310	06	Sayıştay	1.310.316
317	310	07	Ekonomik ve Sosyal Komite	1.310.317
318	310	08	Bölgeler Komitesi	1.310.318
319	310	09	Avrupa Merkez Bankası	1.310.319
320	310	10	Avrupa Yatırım Bankası	1.310.320
322	310	99	Diğer	1.310.322
324	323	01	Ortaklık Konseyi	1.323.324
325	323	02	Ortaklık Komitesi	1.323.325
326	325	01	Tarım ve Balıkçılık Alt Komitesi	1.323.325.326
327	326	01	Toplantılar	1.323.325.326.327
328	326	02	Çalışma Grupları	1.323.325.326.328
329	325	02	İç Pazar ve Rekabet Alt Komitesi	1.323.325.329
330	325	03	Ticaret, Sanayi ve AKÇT Ürünleri Alt Komitesi	1.323.325.330
331	325	04	Ekonomik ve Parasal Konular, Sermaye Hareketleri ve İstatistik Alt K.	1.323.325.331
332	325	05	Yenilikçilik Alt Komitesi	1.323.325.332
333	325	06	Ulaştırma, Çevre ve Enerji (Trans-Avrupa şebekeleri dahil) Alt Komitesi	1.323.325.333
334	325	07	Bölgesel Gelişme, İstihdam ve Sosyal Politika Alt Komitesi	1.323.325.334
335	325	08	Gümrük, Vergilendirme, Uyuşturucu Trafiği ve Kara Para Aklama Alt K.	1.323.325.335
336	323	03	Karma Parlamento Komisyonu	1.323.336
337	323	04	Gümrük İşbirliği Komitesi	1.323.337
338	323	05	Gümrük Birliği Ortak Komitesi	1.323.338
339	323	06	Karma İstişare Komitesi	1.323.339
340	323	07	AB Teknik Komiteleri ve Çalışma Grupları	1.323.340
341	323	99	Diğer	1.323.341
343	342	01	Katılım Ortaklığı Belgesi	1.342.343
344	342	02	Ulusal Program	1.342.344
345	344	01	Hazırlık Çalışmaları	1.342.344.345
346	344	02	İzlenmesi	1.342.344.346
347	342	03	İlerleme Raporları	1.342.347
348	342	04	Katılım Öncesi Mali İzleme Süreci	1.342.348
349	342	05	Katılım Müzakereleri	1.342.349
350	349	01	Müzakere Süreci	1.342.349.350
351	350	01	Malların Serbest Dolaşımı	1.342.349.350.351
352	350	02	İşçilerin Serbest Dolaşımı	1.342.349.350.352
353	350	03	İş Kurma Hakkı ve Hizmet Sunumu	1.342.349.350.353
354	350	04	Sermayenin Serbest Dolaşımı	1.342.349.350.354
355	350	05	Kamu Alımları	1.342.349.350.355
356	350	06	Şirketler Hukuku	1.342.349.350.356
357	350	07	Fikri Mülkiyet Hukuku	1.342.349.350.357
358	350	08	Rekabet Politikası	1.342.349.350.358
359	350	09	Mali Hizmetler	1.342.349.350.359
360	350	10	Bilgi Toplumu ve Medya	1.342.349.350.360
361	350	11	Tarım ve Kırsal Kalkınma	1.342.349.350.361
362	350	12	Gıda Güvenliği, Hayvan ve Bitki Sağlığı	1.342.349.350.362
363	350	13	Balıkçılık	1.342.349.350.363
364	350	14	Ulaştırma Politikası	1.342.349.350.364
365	350	15	Enerji	1.342.349.350.365
366	350	16	Vergilendirme	1.342.349.350.366
367	350	17	Ekonomik ve Parasal Politika	1.342.349.350.367
368	350	18	İstatistik	1.342.349.350.368
369	350	19	Sosyal Politika ve İstihdam	1.342.349.350.369
370	350	20	İşletme ve Sanayi Politikası	1.342.349.350.370
371	350	21	Trans Avrupa Şebekeleri	1.342.349.350.371
372	350	22	Bölgesel Politika ve Yapısal Araçlar	1.342.349.350.372
373	350	23	Yargı ve Temel Haklar	1.342.349.350.373
374	350	24	Adalet, Özgürlük ve Güvenlik	1.342.349.350.374
375	350	25	Bilim ve Araştırma	1.342.349.350.375
376	350	26	Eğitim ve Kültür	1.342.349.350.376
377	350	27	Çevre	1.342.349.350.377
378	350	28	Tüketici ve Sağlığın Korunması	1.342.349.350.378
379	350	29	Gümrük Birliği	1.342.349.350.379
380	350	30	Dış İlişkiler	1.342.349.350.380
381	350	31	Dış Güvenlik ve Savunma Politikası	1.342.349.350.381
382	350	32	Mali Kontrol	1.342.349.350.382
384	350	34	Kurumlar	1.342.349.350.384
385	349	02	Strateji/Eylem Planı	1.342.349.385
386	349	03	Müzakere Pozisyonu	1.342.349.386
387	349	04	Mevzuat Taslakları	1.342.349.387
388	349	05	Çalışma Grupları	1.342.349.388
389	342	99	Diğer	1.342.389
391	390	01	Programlar	1.390.391
392	391	01	AB-Türkiye Eş Finansmanı, Programlar	1.390.391.392
393	391	02	AB Destekli Programlar	1.390.391.393
394	390	02	Ajanslar	1.390.394
395	390	99	Diğer	1.390.395
397	396	01	IPA (Katılım Öncesi Mali Araç-Instrument for Pre-Accession)	1.396.397
398	397	01	Programlama Süreci	1.396.397.398
399	397	02	Projeler	1.396.397.399
400	399	01	Proje Teklifleri	1.396.397.399.400
401	399	02	Proje Fişleri (Raporları)	1.396.397.399.401
402	399	03	Fiş Değişiklikleri (Revizyon)	1.396.397.399.402
403	399	04	İzleme ve Değerlendirme	1.396.397.399.403
405	397	04	Sınır Ötesi İşbirliği	1.396.397.405
406	397	05	Operasyonel Programlar	1.396.397.406
407	397	06	İzleme Komiteleri	1.396.397.407
408	396	02	Çok Faydalanıcılı	1.396.408
409	396	99	Diğer	1.396.409
411	410	01	AB Teknik Yardımları	1.410.411
412	411	01	TAIEX	1.410.411.412
413	412	01	Uzman Desteği	1.410.411.412.413
414	412	02	İnceleme Gezileri	1.410.411.412.414
415	412	03	Seminer/Çalışma Grupları	1.410.411.412.415
416	411	02	Eşleştirme (Twinning)	1.410.411.416
417	416	01	Standart Eşleştirme	1.410.411.416.417
418	416	02	Kısa Süreli Eşleştirme	1.410.411.416.418
419	410	02	Üye Ülke Teknik Yardımları	1.410.419
420	419	01	MATRA/G2G.NL	1.410.419.420
421	419	02	MATRA-MTEC	1.410.419.421
422	410	99	Diğer	1.410.422
427	426	01	Kamulaştırma	1.426.427
428	427	01	Kamu Yararı Kararı ve Onay İşleri	1.426.427.428
429	427	02	Teknik Çalışmalar	1.426.427.429
430	427	03	Bedel Takdir ve Uzlaşma	1.426.427.430
431	427	04	Sorunlar	1.426.427.431
432	427	05	Zarar ve Ziyan	1.426.427.432
433	427	06	Ödeme İşleri	1.426.427.433
434	426	02	İrtifak Hakkı İşlemleri	1.426.434
435	426	99	Diğer	1.426.435
439	438	01	Etüd-Proje ve Fizibilite işleri	1.438.439
440	438	02	İhale İşleri	1.438.440
441	440	01	İzin ve Onay İşleri	1.438.440.441
442	440	02	Yaklaşık Maliyet Hesabı	1.438.440.442
443	440	03	İhale İlan İşleri	1.438.440.443
444	440	04	Şartnameler	1.438.440.444
445	440	05	Başvuru İşlemleri ve Tutanaklar	1.438.440.445
446	440	06	Komisyon İşlemleri	1.438.440.446
447	440	07	İtirazlar	1.438.440.447
448	440	08	Sözleşme	1.438.440.448
449	440	09	Kesin Teminat	1.438.440.449
450	438	03	Uygulama işleri	1.438.450
451	450	01	İş Programı	1.438.450.451
452	450	02	Ruhsatlandırma	1.438.450.452
453	450	03	Hammadde Temini/Malzeme Seçimi	1.438.450.453
454	450	04	KeşifArtışı	1.438.450.454
455	450	05	Süre Uzatımı ve Ödenek Dilim Değişikliği	1.438.450.455
456	450	06	Birim Fiyat ve Yeni Fiyat	1.438.450.456
457	438	04	İnceleme ve Kontrol İşleri	1.438.457
458	438	05	Hakedişler ve Ödemeler	1.438.458
459	438	06	Geçici ve Kesin Kabuller	1.438.459
460	459	01	Geçici ve Kesin Kabuller	1.438.459.460
461	459	02	Fesih, Tasfiye ve Devir İşlemleri	1.438.459.461
462	438	07	Kesin Hesap İşlemleri	1.438.462
463	462	01	Kesin Hesap	1.438.462.463
464	462	02	SSK İlişkisizlik Belgesi	1.438.462.464
465	462	03	İş Deneyim Belgesi Verilmesi	1.438.462.465
466	462	04	Teminatların İadesi	1.438.462.466
467	438	99	Diğer	1.438.467
469	468	01	Tahsis, Devir ve Takas	1.468.469
470	468	02	Satış ve Kiralama	1.468.470
471	468	03	Envanter	1.468.471
472	468	99	Diğer	1.468.472
474	473	01	Talepler	1.473.474
475	473	02	Yapım ve Güncelleme	1.473.475
476	473	03	Sayısallaştırma	1.473.476
477	473	04	Kayıt ve Envanter	1.473.477
478	473	99	Diğer	1.473.478
483	482	01	Kontenjan ve Duyurular	1.482.483
484	482	02	Başvuru ve Değerlendirme	1.482.484
485	482	03	Ödemeler	1.482.485
486	482	04	Tahsil,Takip, Atama	1.482.486
487	482	99	Diğer	1.482.487
489	488	01	Kontenjan İşlemleri	1.488.489
490	488	02	İzleme ve Değerlendirme	1.488.490
491	488	03	Belgelendirme	1.488.491
492	488	99	Diğer	1.488.492
494	493	01	Mesleki Yeterlilik ve Gelişim	1.493.494
495	494	01	Teklifler	1.493.494.495
496	494	02	Program	1.493.494.496
497	494	03	Duyuru	1.493.494.497
498	494	04	Eğitici Temini	1.493.494.498
499	494	05	Kayıt, Katılım Listeleri	1.493.494.499
500	494	06	Eğitim Materyali	1.493.494.500
501	494	07	Sınav	1.493.494.501
502	494	08	Belgelendirme	1.493.494.502
503	494	09	Yeterlilik (Sağlık Raporu vb.)	1.493.494.503
504	493	02	Dil Eğitimi	1.493.504
505	493	03	Rotasyon	1.493.505
506	493	04	İntibak	1.493.506
507	493	05	Görevde Yükselme	1.493.507
508	493	06	Aday Memur	1.493.508
509	493	07	Kariyer Uzman	1.493.509
510	493	08	Kişisel Gelişim ve Motivasyon	1.493.510
511	493	09	Bilgilendirme-Bilinçlendirme	1.493.511
512	493	99	Diğer	1.493.512
517	516	01	Taşıt	1.516.517
518	517	01	Plaka, Ruhsat ve Muayene İşleri	1.516.517.518
519	517	02	Akaryakıt İşleri	1.516.517.519
520	517	03	Tahsis, Sevk ve Kontrol İşleri	1.516.517.520
521	517	04	Kaza İşleri	1.516.517.521
522	516	02	İş Makineleri	1.516.522
523	516	99	Diğer	1.516.523
527	526	01	Gelen-Giden Evrak	1.526.527
528	526	02	Kontrollü Evrak İşlemleri	1.526.528
529	526	03	Yanlış Evrakın İade İşlemleri	1.526.529
530	526	99	Diğer	1.526.530
532	531	01	Belge Yönetimi	1.531.532
533	532	01	Saklama Süreli Dosya Planı	1.531.532.533
534	532	02	Kodlama İşlemleri	1.531.532.534
535	531	02	Arşiv Yönetimi	1.531.535
536	535	01	Devir-Teslim İşlemleri	1.531.535.536
537	535	02	Ayıklama ve İmha İşlemleri	1.531.535.537
538	537	01	Ayıklama ve İmha	1.531.535.537.538
539	537	02	Uygunluk Görüşü	1.531.535.537.539
540	535	03	Tasnif (Sınıflandırma) İşlemleri	1.531.535.540
541	535	04	İnceleme ve Denetleme	1.531.535.541
542	535	05	Arşivlerden Yararlanma	1.531.535.542
543	531	99	Diğer	1.531.543
545	544	01	Derme Geliştirme ve Derleme İşleri	1.544.545
546	545	01	Kitaplar	1.544.545.546
547	545	02	Süreli Yayınlar	1.544.545.547
548	545	03	Tezler	1.544.545.548
549	545	04	Yazma ve Nadir Eserler	1.544.545.549
550	545	05	Veritabanları, Elektronik Ürün ve Yayınlar	1.544.545.550
551	544	02	Kullanıcı Hizmetleri	1.544.551
552	551	01	Ödünç Verme İşlemleri	1.544.551.552
553	551	02	Araştırma Hizmetleri	1.544.551.553
554	551	03	Danışma Hizmetleri	1.544.551.554
555	551	04	Elektronik Hizmetler	1.544.551.555
556	544	99	Diğer	1.544.556
558	557	01	Bina ve Tesisler	1.557.558
559	557	02	Tesisat	1.557.559
560	557	03	Mekanik	1.557.560
561	557	04	Elektronik ve Teknik Cihaz	1.557.561
562	557	05	Taşıt ve İş Makineleri	1.557.562
563	557	99	Diğer	1.557.563
566	565	01	İstek	1.565.566
567	565	02	Devir, Giriş-Çıkış İşlemleri	1.565.567
568	565	03	Sayım ve Döküm	1.565.568
569	565	04	Kesin Hesap	1.565.569
570	565	05	Taşınır Kayıt Kontrol Yetkilisi	1.565.570
571	565	99	Diğer	1.565.571
573	572	01	Dahili Sigorta	1.572.573
574	573	01	Yangın	1.572.573.574
575	573	02	Nakliyat	1.572.573.575
576	573	03	Kaza	1.572.573.576
577	573	04	Mühendislik	1.572.573.577
578	572	02	Harici Sigorta	1.572.578
579	572	99	Diğer	1.572.579
581	580	01	Telefon	1.580.581
582	581	01	Sabit	1.580.581.582
583	581	02	GSM	1.580.581.583
584	581	03	Mobil	1.580.581.584
585	580	02	Kablolu TV	1.580.585
586	580	03	Telsiz	1.580.586
587	580	04	Faks	1.580.587
588	580	05	İnternet	1.580.588
589	580	99	Diğer	1.580.589
592	591	01	Sosyal Tesisler	1.591.592
593	592	01	Eğitim Tesisi, Misafirhane ve Kamp	1.591.592.593
594	593	01	Müracaat	1.591.592.593.594
595	593	02	Tahsis	1.591.592.593.595
596	593	03	Tahliye	1.591.592.593.596
597	592	02	Lojman	1.591.592.597
598	592	03	Kreş ve Gündüz Bakımevi	1.591.592.598
599	592	04	Spor Tesisi	1.591.592.599
600	592	05	Yemekhane, Kafeterya ve Çay Ocağı	1.591.592.600
601	591	02	Temsili Ağırlamalar (yemek, araç vb.)	1.591.601
602	591	03	Sportif Faaliyetler	1.591.602
603	591	99	Diğer	1.591.603
609	608	01	Fuar	1.608.609
610	608	02	Sergi	1.608.610
611	608	03	Festivaller	1.608.611
612	608	04	Tur ve Gezi	1.608.612
613	608	05	Yarışma	1.608.613
614	608	06	Gösteri ve Konserler	1.608.614
615	608	07	Özel Gün ve Haftalar	1.608.615
622	621	01	Kitap	1.621.622
617	1	823	Reklam ve İlan İşleri	1.617
616	608	99	Diğer	1.608.616
618	617	01	Reklam İşleri	1.617.618
619	617	02	İlan İşleri	1.617.619
620	617	99	Diğer	1.617.620
623	622	01	Teklifler	1.621.622.623
624	622	02	Değerlendirmeler	1.621.622.624
625	622	03	Yayın Hazırlık İşleri	1.621.622.625
626	622	04	Telif İşleri	1.621.622.626
627	622	05	İntihal	1.621.622.627
628	621	02	Dergi	1.621.628
629	621	03	Broşür	1.621.629
630	621	04	Matbu Evrak	1.621.630
631	621	05	Takvim ve Ajanda	1.621.631
632	621	06	Bülten	1.621.632
633	621	07	Resmi Doküman	1.621.633
634	621	08	Ses ve Görüntü Malzemeleri	1.621.634
635	621	99	Diğer	1.621.635
637	636	01	Basım	1.636.637
638	636	02	Depolama	1.636.638
639	636	03	Dağıtım	1.636.639
640	639	01	Yayın Talepleri	1.636.639.640
641	639	02	Yardım ve Bağış	1.636.639.641
642	639	03	Abonelik ve Satış	1.636.639.642
643	636	99	Diğer	1.636.643
648	647	01	Bütçe Hazırlık Çalışmaları	1.647.648
649	648	01	Bütçe	1.647.648.649
650	648	02	Ek Bütçe	1.647.648.650
651	648	03	Geçici Bütçe	1.647.648.651
652	647	02	Bütçe Uygulamaları	1.647.652
653	652	01	Ödenek Talep ve Göndermeleri	1.647.652.653
654	652	02	Serbest Bırakma	1.647.652.654
655	652	03	AHP/AFP Vizeleri	1.647.652.655
656	652	04	AHP/AFP Revizeleri	1.647.652.656
657	652	05	Aktarmalar	1.647.652.657
658	652	06	Ek Ödenekler	1.647.652.658
659	652	07	Ödenek Devri	1.647.652.659
660	652	08	Ödenek İptalleri	1.647.652.660
661	652	09	İlama Bağlı Borçlar	1.647.652.661
662	652	10	Yedek Ödenekler	1.647.652.662
663	652	11	Tenkisler	1.647.652.663
664	652	12	Revize İşlemleri	1.647.652.664
665	652	13	Akreditif ve Taahhüt Artıkları	1.647.652.665
666	652	14	Avanslar, Krediler, Mahsuplar	1.647.652.666
667	652	15	Öz Gelirler	1.647.652.667
668	652	16	Gelir Red ve İadeleri	1.647.652.668
669	652	17	Tahakkuk ve Ödeme İşleri	1.647.652.669
670	647	99	Diğer	1.647.670
672	671	01	Mizan	1.671.672
673	671	02	Bilanço	1.671.673
674	671	03	Kesin Hesap	1.671.674
675	671	04	Bütçe Dönem Gerçekleştirme Raporları	1.671.675
676	671	05	Tablolar	1.671.676
677	676	01	Finansal Tablolar	1.671.676.677
678	676	02	Gelir Tabloları	1.671.676.678
679	676	03	Vaziyet Tabloları	1.671.676.679
680	676	04	Günlük Ödeme Planları	1.671.676.680
681	676	05	Nakit Akım Tabloları	1.671.676.681
682	671	99	Diğer	1.671.682
684	683	01	Sorgu	1.683.684
685	683	02	İlam	1.683.685
686	683	03	Raporlama	1.683.686
687	686	01	Uygunluk Bildirimi	1.683.686.687
688	686	02	Performans Raporları	1.683.686.688
689	683	99	Diğer	1.683.689
691	690	01	Vergi	1.690.691
692	690	02	Kefalet	1.690.692
693	690	03	İcra	1.690.693
694	690	04	Sendika	1.690.694
695	690	99	Diğer	1.690.695
697	696	01	Ödeme Talimatları	1.696.697
698	696	02	Repo ve Faiz İşlemleri	1.696.698
699	696	03	Kambiyo İşlemleri	1.696.699
700	696	04	Tahsilatlar	1.696.700
701	696	05	Mutabakatlar	1.696.701
702	696	06	Ekstre ve Dekontlar	1.696.702
703	696	99	Diğer	1.696.703
706	705	01	Finansman Programları	1.705.706
707	705	02	Gelir Gider	1.705.707
708	705	03	Kar Tevzii	1.705.708
709	705	04	Sermaye	1.705.709
710	705	99	Diğer	1.705.710
712	711	01	Yurtdışı Krediler	1.711.712
713	712	01	Geri Ödemeler	1.711.712.713
714	712	02	Kur Farkları	1.711.712.714
715	712	03	Gecikme Faizi	1.711.712.715
716	711	02	Yurtiçi Krediler	1.711.716
717	711	03	Hazineye Borçlar	1.711.717
718	711	99	Diğer	1.711.718
720	719	01	Kuruluşun Borç ve Alacakları	1.719.720
721	719	02	Gerçek ve Tüzel Kişilerin Borç ve Alacakları	1.719.721
722	719	99	Diğer	1.719.722
731	730	01	Gezi ve Ziyaret Programları	1.730.731
732	730	02	Yabancı Temsilciler	1.730.732
733	730	99	Diğer	1.730.733
736	735	01	Özel Mektuplar	1.735.736
737	735	02	Davetiyeler	1.735.737
738	735	03	Tebrikler	1.735.738
739	735	99	Diğer	1.735.739
748	747	01	Alım Talepleri	1.747.748
749	747	02	Alım İzinleri	1.747.749
750	747	03	Sınavlar	1.747.750
751	747	99	Diğer	1.747.751
753	752	01	İşe Giriş Belgeleri	1.752.753
754	752	02	Atama İşleri	1.752.754
755	754	01	Asaleten	1.752.754.755
756	754	02	Vekaleten	1.752.754.756
757	754	03	Tedviren	1.752.754.757
758	752	03	Terfi ve İntibak İşlemleri	1.752.758
759	758	01	Terfi İşleri	1.752.758.759
760	758	02	İntibak İşleri	1.752.758.760
761	760	01	Hizmet Değerlendirmesi	1.752.758.760.761
762	760	02	Öğrenim Değerlendirmesi	1.752.758.760.762
763	752	04	Hizmet Cetveli ve Hizmet Belgesi	1.752.763
764	752	05	İzin İşleri	1.752.764
765	764	01	Yıllık	1.752.764.765
766	764	02	Sıhhi	1.752.764.766
767	764	03	Mazeret	1.752.764.767
768	764	04	Ücretsiz	1.752.764.768
769	764	05	Yurtdışı	1.752.764.769
770	752	06	Görevden Ayrılma	1.752.770
771	770	01	Emeklilik	1.752.770.771
772	770	02	İstifa	1.752.770.772
773	770	03	Görevden Çekilmiş Sayılma	1.752.770.773
774	770	04	Görevden Çıkarılma	1.752.770.774
775	770	05	Vefat	1.752.770.775
776	752	07	Görevlendirmeler	1.752.776
777	776	01	Kurumiçi	1.752.776.777
778	776	02	Kurumdışı	1.752.776.778
779	776	03	Yurtdışı	1.752.776.779
780	776	04	Ek Görevler	1.752.776.780
781	776	05	Rotasyon	1.752.776.781
782	752	08	Mükafat ve Cezalar	1.752.782
783	782	01	Ödül İşleri	1.752.782.783
784	782	02	Disiplin İşleri	1.752.782.784
785	752	09	Sicil İşleri	1.752.785
786	785	01	Sicil Raporları	1.752.785.786
787	785	02	Mal Beyannamesi	1.752.785.787
788	752	10	Aile Yardımı Bildirimi	1.752.788
789	752	11	Askerlik İşlemleri	1.752.789
790	752	12	Hizmet Borçlanması	1.752.790
791	752	13	Kimlik ve Giriş Kartı İşlemleri	1.752.791
792	752	14	Sağlık Karnesi	1.752.792
793	752	99	Diğer	1.752.793
795	794	01	Teklif	1.794.795
796	794	02	İptal ve İhdas	1.794.796
797	794	03	Tenkis-Tahsis	1.794.797
798	794	04	Vize İşlemleri	1.794.798
799	794	99	Diğer	1.794.799
801	800	01	Diplomatik	1.800.801
802	800	02	Hususi	1.800.802
803	800	03	Hizmet	1.800.803
804	800	04	Emekli	1.800.804
805	800	99	Diğer	1.800.805
808	807	01	İşveren Sendikaları	1.807.808
809	807	02	Memur Sendikaları	1.807.809
810	809	01	Üyelik ve Dayanışma Aidatı	1.807.809.810
811	809	02	Kurum İdari Kurulu ve Yüksek İdari Kurul Toplantılar	1.807.809.811
812	809	03	Yetkili Sendika Tespiti	1.807.809.812
813	809	04	Temsilcilik	1.807.809.813
814	807	03	İşçi Sendikaları	1.807.814
815	814	01	Üyelik ve Dayanışma Aidatı	1.807.814.815
816	814	02	Temsilcilik	1.807.814.816
817	814	03	İzin İşlemleri	1.807.814.817
818	814	04	Toplu Sözleşme ve Uygulama İşleri	1.807.814.818
819	818	01	Müzakereler	1.807.814.818.819
820	818	02	Uyuşmazlık Hali	1.807.814.818.820
821	818	03	Grev - Lokavt	1.807.814.818.821
822	818	04	Teşmil İşlemleri	1.807.814.818.822
823	807	99	Diğer	1.807.823
825	824	01	Sigorta Primleri Bildirgeleri	1.824.825
826	824	02	Tescil İşlemleri (İşe Giriş Bildirgeleri)	1.824.826
827	824	03	İşçi Bildirim Listeleri	1.824.827
828	824	99	Diğer	1.824.828
830	829	01	İş Güvenliği	1.829.830
831	829	02	Çalışan Sağlığı	1.829.831
832	831	01	Meslek Hastalıkları	1.829.831.832
833	831	02	İş Kazaları	1.829.831.833
834	829	99	Diğer	1.829.834
839	838	01	Yurtiçi	1.838.839
840	838	02	Yurtdışı	1.838.840
841	838	03	Yasaklı Firmalar	1.838.841
842	838	99	Diğer	1.838.842
844	843	01	Mal ve Malzeme Alımı	1.843.844
845	844	01	Ön İzin Belgesi	1.843.844.845
846	844	02	İhale Onay Belgesi	1.843.844.846
847	844	03	Yaklaşık Maliyet Hesabı	1.843.844.847
848	844	04	Duyurular	1.843.844.848
849	844	05	KİK Onayı	1.843.844.849
850	844	06	İhale İlan Tutanağı	1.843.844.850
851	844	07	İlan Metni	1.843.844.851
852	844	08	Şartnameler	1.843.844.852
853	844	09	İhale Evrakının Teslim Alındığına Dair Tutanak	1.843.844.853
854	844	10	Zarf Açma ve Belge Kontrol Tutanağı	1.843.844.854
855	844	11	İsteklilerce Teklif Edilen Fiyatlar Tutanağı	1.843.844.855
856	844	12	İhale Komisyonu Ara Kararı	1.843.844.856
857	844	13	İhale Komisyonu Kesin Kararı	1.843.844.857
858	844	14	Yasaklı Olup Olmadığına Dair KİK Teyit Belgesi	1.843.844.858
859	844	15	Firmalara Gönderilen Sonuç Bildirimleri	1.843.844.859
860	844	16	İtirazlar	1.843.844.860
861	844	17	Sözleşme	1.843.844.861
862	844	18	Kesin Teminat	1.843.844.862
863	844	19	Ödeme İşleri	1.843.844.863
864	843	02	Hizmet	1.843.864
865	843	03	Gayrimenkul	1.843.865
866	843	99	Diğer	1.843.866
868	867	01	Bayiler	1.867.868
869	867	02	Acentalar	1.867.869
870	867	99	Diğer	1.867.870
874	873	01	Yurt Dışı Satışlar	1.873.874
875	874	01	Direkt Satışlar	1.873.874.875
876	874	02	İhracat Sevkiyatları	1.873.874.876
877	874	03	İhraç Kayıtlı Satışlar	1.873.874.877
878	874	04	DİİB Kapsamında Satışlar	1.873.874.878
879	873	02	Yurt İçi Satışlar	1.873.879
880	879	01	Kontratlı Satışlar	1.873.879.880
881	879	02	Cari Satışlar	1.873.879.881
882	879	03	İhaleli Satışlar	1.873.879.882
883	879	04	Elektronik Ticaret Şeklinde Satışlar	1.873.879.883
884	873	03	Hurda Ve İhtiyaç Fazlası Malzeme Satışları	1.873.884
885	884	01	İhtiyaç Fazlası Malzeme Satışı	1.873.884.885
886	884	02	Ekonomik Ömrünü Tamamlamış Malzeme Satışı	1.873.884.886
887	873	99	Diğer	1.873.887
891	890	01	İrsaliyeler	1.890.891
892	890	02	Satış Fişleri	1.890.892
893	890	03	Faturalar	1.890.893
894	890	04	Yükleme Talimatları	1.890.894
895	890	99	Diğer	1.890.895
897	896	01	Malzeme Giriş İşlemleri	1.896.897
898	896	02	Malzeme Çıkış İşlemleri	1.896.898
899	896	03	Malzeme Transfer İşlemleri	1.896.899
900	896	04	Malzeme İade İşlemleri	1.896.900
901	896	05	Malzemelerin Kayıttan Düşülmesi İşlemleri	1.896.901
902	896	99	Diğer	1.896.902
906	905	01	Milli Alarm Sistemi İle İlgili İşler	1.905.906
907	906	01	Alarm Tedbirleri İlanında Yapılacak Faaliyetler	1.905.906.907
908	906	02	Nato Alarm Sistemi	1.905.906.908
909	906	03	Alarm Kod Kelimeleri ve Anlamları	1.905.906.909
910	906	04	Alarm Kod İşlem ve Görevlileri	1.905.906.910
911	906	05	Milli Alarm Sistemi Eğitimleri	1.905.906.911
912	906	06	Seferberlik İlanı ve Duyuru İşlemleri	1.905.906.912
913	906	07	24 saat Süreli Çalışma Planları	1.905.906.913
914	905	02	Lojistik Seferberlik İşlemleri	1.905.914
915	914	01	Seferberlik ve Savaş Hali Genel Planı	1.905.914.915
916	914	02	Özel Planlar	1.905.914.916
917	914	03	İl Detay Planları	1.905.914.917
918	914	04	Savaş Görev Planları	1.905.914.918
919	914	05	Savaş Hasarı Onanm Planları	1.905.914.919
920	914	06	Araç Seferberliği ve Erteleme İşleri	1.905.914.920
921	914	07	Trafikte Kaydı Olmayan Özel Sektör İş Makinelerinin Tespiti	1.905.914.921
922	914	08	Mal ve Hizmet Seferberliği İşleri	1.905.914.922
923	914	09	Kaynak Planlama Faaliyetleri	1.905.914.923
924	923	01	Kaynak Katalogları	1.905.914.923.924
925	923	02	Kaynak Planlama Faaliyetleri Bilgi Formları	1.905.914.923.925
926	923	03	Kaynak Saptama Çizelgeleri	1.905.914.923.926
927	914	10	İhtiyaçlar, Tahsisler ve Protokoller	1.905.914.927
928	914	11	İl ve İlçe Kaynak Sayım İşleri	1.905.914.928
929	914	12	Planlama ve Koordinasyon Kurulu İşleri	1.905.914.929
930	914	13	Seferberlik ve Savaş Hali Hazırlıkları Yıllık Faaliyet Raporu	1.905.914.930
931	914	14	Milli Müdafaa Mükellefiyeti	1.905.914.931
932	914	15	Memleket İçi Düşmana Kars, Silahlı Müdafaa Mükellefiyeti (Ava Birlikleri) İşleri	1.905.914.932
933	914	16	Ekonomik/Stratejik Tahrip Hedefleri Planlama İşleri	1.905.914.933
934	914	17	Kriz/Harekat Merkezi İle İlgili İşler	1.905.914.934
935	914	18	Haritalar	1.905.914.935
936	914	19	Denetlemeler	1.905.914.936
937	914	20	Milli Güvenlik Kurulu Genel Sekreterliği Tetkik Gezileri	1.905.914.937
938	914	21	Milli Güvenlik Kurulu Genel Sekreterliği Denetleme Raporları	1.905.914.938
939	914	22	TSK Harekat Kontrolüne Girecek Kamu Kurum ve Kuruluşları	1.905.914.939
940	914	23	TSK Lojistik Seferberlik Dosyası	1.905.914.940
941	905	03	Personel Seferberlik İşlemleri	1.905.941
942	941	01	Sevk Tehir İşleri	1.905.941.942
943	941	02	Personel ve Kadro Erteleme İşleri	1.905.941.943
944	941	03	Yasal Yaş Sınırları	1.905.941.944
945	941	04	Sefer Görev Emirleri	1.905.941.945
946	941	05	İnsan Gücü Planlaması	1.905.941.946
947	941	06	Bedelli Askerlik	1.905.941.947
949	905	99	Diğer	1.905.949
951	950	01	Sabotajlara Karşı Koruma Planları, Koruma Planları	1.950.951
952	950	02	Nöbetçi Memurluğu İşleri	1.950.952
953	950	03	Koruyucu Güvenlik Denetleme Sonuç Raporları	1.950.953
954	950	04	Yabancı Uyruklu Personel İle İlgili İşler	1.950.954
955	950	05	Mülteciler ve Sığınmacılar	1.950.955
956	950	06	Özel Güvenlik Teşkilatı İle İlgili İşler	1.950.956
957	950	07	Özel Güvenlik Bölgeleri İle İlgili İşler	1.950.957
958	950	08	İstihbarat ve Kaçakçılık İle İlgili İşler	1.950.958
959	950	09	Patlayıcı Maddeler ve Ateşli Silahlar İle İlgili İşler	1.950.959
960	950	10	Güvenlik Soruşturmaları ve Arşiv Araştırması	1.950.960
961	950	11	Fiziki Emniyet Tedbirleri	1.950.961
962	950	12	Evrak Güvenliği	1.950.962
963	950	13	Haberleşme Güvenliği	1.950.963
964	950	99	Diğer	1.950.964
966	965	01	Sivil Savunma Planlama İşleri	1.965.966
967	966	01	İl ve İlçe Sivil Savunma Planları	1.965.966.967
969	966	03	Tahliye Planları	1.965.966.969
970	966	04	Kabul Planları	1.965.966.970
971	966	05	Sivil Savunma Arama ve Kurtarma Birlikleri Harekat ve İntikal Planları, Haber Merkezi Talimatı	1.965.966.971
972	966	06	İl ve İlçe Afet Acil Yardım Planları	1.965.966.972
973	972	01	Afetlerde Sivil Savunma İcra Planı	1.965.966.972.973
974	972	02	Afetlerde Yapılacak İşlerle İlgili Çalışma Rehberi	1.965.966.972.974
975	965	02	Sivil Savunma Servis ve Yükümlüleri İşleri	1.965.975
976	965	03	İl Acil Kurtarma ve Yardım Ekipleri İşleri	1.965.976
977	965	04	Sivil Savunma Gönüllüleri İşleri	1.965.977
978	965	05	Sivil Toplum Kuruluşları (STK) İle İlgili İşler	1.965.978
979	965	06	Sivil Savunma Sevk ve Harekatı İşleri	1.965.979
981	965	08	Haber Alma ve Yayma, İkaz ve Alarm İşleri	1.965.981
982	981	01	Haber Alma ve Yayma İşleri	1.965.981.982
983	981	02	İkaz ve Alarm Merkezleri İle İlgili İşler	1.965.981.983
984	981	03	Siren Sistemi İle İlgili İşler	1.965.981.984
985	965	09	Sivil Savunma Eğitim İşleri	1.965.985
986	985	01	Temel ve Hazırlayıcı Eğitim	1.965.985.986
987	985	02	Sivil Savunma Servis Eğitimi	1.965.985.987
988	985	03	Arama ve Kurtarma Eğitimi	1.965.985.988
989	985	04	KBRN (Kimyasal, Biyolojik, Radyolojik ve Nükleer) Eğitimi	1.965.985.989
990	985	05	İtfaiye ve Yangın Eğitimi	1.965.985.990
991	985	06	İlk Yardım Eğitimi	1.965.985.991
992	985	07	Geliştirme Eğitimi	1.965.985.992
993	985	08	Halk Eğitimi	1.965.985.993
994	985	09	Eğitim Sonuç Raporları	1.965.985.994
995	965	10	Sivil Savunma Günü İşleri	1.965.995
996	965	11	Sivil Savunma Araç, Gereç ve Malzemelere Ait İşler	1.965.996
997	965	99	Diğer	1.965.997
999	998	01	Kimyasal Savunma	1.998.999
1000	998	02	Biyolojik Savunma	1.998.1000
1001	998	03	Radyolojik ve Nükleer Savunma	1.998.1001
1002	998	04	Dekontaminasyon İşleri	1.998.1002
1040	1039	01	Türkiye-AB Ortaklık Hukuku	1.1039.1040
1041	1040	01	Ortaklık Organlarının Kararları	1.1039.1040.1041
1042	1040	02	İçtihad	1.1039.1040.1042
18	1	020	Olurlar, Onaylar	1.18
1	\N		SDP	1
19	1	030	Anlaşma, Sözleşme ve Protokoller	1.19
28	1	040	Faaliyet Raporları	1.28
35	1	041	Brifingler ve Bilgi Notları	1.35
38	1	042	İstatistikler	1.38
39	1	044	Anketler	1.39
40	1	045	Görüşler	1.40
44	1	050	Kurullar ve Toplantılar	1.44
56	1	051	Bilimsel ve Kültürel Toplantılar	1.56
1003	998	99	Diğer	1.998.1003
1005	1004	01	Yangın Önleme ve Korunma İşleri	1.1004.1005
1006	1004	02	Yangın Söndürme Araç ve Malzemelerine Ait İşler	1.1004.1006
1007	1004	03	İtfaiye Teşkilleri ile İlgili İşler	1.1004.1007
1008	1004	04	Kıyı ve Deniz Yangınlarına Ait İşler	1.1004.1008
1009	1004	05	Orman Yangınlarına Ait İşler	1.1004.1009
1010	1004	99	Diğer	1.1004.1010
1012	1011	01	Genel Sığınaklar	1.1011.1012
1013	1011	02	Özel Sığınaklar	1.1011.1013
1014	1011	03	Tespit ve Denetim İşleri	1.1011.1014
1015	1011	04	Kayıt ve İstatistik İşleri	1.1011.1015
1016	1011	99	Diğer	1.1011.1016
1018	1017	01	Uluslararası ve Nato Tatbikatları	1.1017.1018
1019	1017	02	Milli Tatbikatlar	1.1017.1019
1020	1019	01	Seferberlik ve Savaş Hali Plan Tatbikatları	1.1017.1019.1020
1021	1019	02	Milli Kriz/Harekat Yönetimi Tatbikatları	1.1017.1019.1021
1022	1021	01	Güven Serisi Tatbikatlar	1.1017.1019.1021.1022
1023	1021	02	Afet Tatbikatları	1.1017.1019.1021.1023
1024	1019	03	Milli Alarm Sistemi Tatbikatı	1.1017.1019.1024
1025	1019	04	Sivil Katılımi, Askeri Tatbikatlar	1.1017.1019.1025
1026	1019	05	Mal ve Hizmet Seferberliği Tatbikatları	1.1017.1019.1026
1027	1019	06	Sivil Savunma Tatbikatları	1.1017.1019.1027
1028	1027	01	Sivil Savunma Plan Tatbikatı	1.1017.1019.1027.1028
1029	1027	02	Sivil Savunma Servis Tatbikatı	1.1017.1019.1027.1029
1030	1027	03	Örnek Sivil Savunma Tatbikatı	1.1017.1019.1027.1030
1043	1039	02	AB Hukuku Alanındaki Gelişmeler	1.1039.1043
1044	1043	01	Birincil Hukuk	1.1039.1043.1044
1045	1043	02	İkincil Hukuk	1.1039.1043.1045
1046	1043	03	İçtihad	1.1039.1043.1046
1047	1039	99	Diğer	1.1039.1047
1039	1	741	AB Hukuku	1.1039
1048	1	000	Genel	1.1048
67	1	060	Kalite Yönetim Sistemi	1.67
96	1	600	Araştırma ve Planlama İşleri (Genel)	1.96
97	1	601	Teşkilatlanma İşleri	1.97
107	1	602	Plan ve Program İşleri	1.107
133	1	604	Proje İşleri	1.133
143	1	605	Araştırma İşleri	1.143
147	1	609	Koordinasyon İşleri	1.147
148	1	610	Soru Önergeleri	1.148
149	1	611	Çevre İşleri	1.149
155	1	612	İç Kontrol	1.155
162	1	619	Araştırma ve Planlama ile ilgili Diğer İşler	1.162
163	1	620	Basın ve Halkla İlişkiler (Genel)	1.163
164	1	621	Basın İşleri	1.164
171	1	622	Talep, Şikayet, Görüşler	1.171
176	1	639	Basın ve Halkla İlişkiler Konusunda Diğer İşler	1.176
177	1	640	Hukuk İşleri (Genel)	1.177
178	1	641	Dava Dosyaları	1.178
190	1	645	Tebligatlar	1.190
191	1	646	Vekaletnameler, Azil nameler	1.191
192	1	650	Bilirkişi ve Ekspertiz	1.192
193	1	651	Uzlaşma İşlemleri	1.193
194	1	659	Hukukla İlgili Diğer İşler	1.194
195	1	660	Teftiş/Denetim İşleri (Genel)	1.195
196	1	661	Plan ve Programları	1.196
200	1	662	Görev Emirleri	1.200
201	1	663	Denetim,İnceleme,Soruşturma ve Araştırma	1.201
212	1	667	İhbar ve Suç Duyuruları	1.212
216	1	668	Yüksek Denetleme ve KİT Komisyonu	1.216
225	1	679	Teftiş/Denetim ile İlgili Diğer İşler	1.225
226	1	700	Bilgi İşlem İşleri (Genel)	1.226
227	1	702	Yazılım Geliştirme ve Kodlama Çalışmaları	1.227
232	1	703	Bilgi İşletim Sistemleri Planlama ve Değerlendirme	1.232
241	1	704	Hatlar	1.241
246	1	705	Sistem ile İlgili Talepler	1.246
250	1	708	Veri Girişi, İşleme Ve Aktarma İşleri	1.250
257	1	710	e-uygulamalar	1.257
264	1	713	Kullanım ve Erişim Yetkileri	1.264
269	1	719	Bilgi İşlemle İlgili Diğer İşler	1.269
270	1	720	Dış İlişkiler ve Avrupa Birliği (Genel)	1.270
271	1	721	Uluslararası Hukuk	1.271
272	1	724	Ülkelerle İlişkiler	1.272
282	1	730	Uluslararası ve Bölgesel Kuruluşlarla İlişkiler	1.282
309	1	740	Avrupa Birliği (AB) ile İlişkiler (Genel)	1.309
310	1	742	AB Karar Alma Mekanizması ile İlişkiler	1.310
1004	1	955	Yangın	1.1004
1011	1	956	Sığınak	1.1011
323	1	743	Gümrük Birliği ve Ortaklık Organları	1.323
342	1	744	AB Katılım Süreci	1.342
390	1	745	Topluluk Programları ve Ajanslar	1.390
396	1	746	AB-Türkiye Mali İşbirliği	1.396
410	1	748	AB ve Üye Ülke Teknik Yardımları	1.410
423	1	749	Dış İlişkiler ve AB ile İlgili Diğer İşler	1.423
424	1	750	Emlak ve Yapım İşleri (Genel)	1.424
425	1	751	Toplulaştırma İşlemleri	1.425
426	1	752	Kamulaştırma ve İrtifak Hakkı	1.426
436	1	753	Kadastro Çalışmaları	1.436
437	1	754	İmar İşleri	1.437
438	1	755	Yapım (İnşaat) İşleri	1.438
468	1	756	Taşınmaz İşlemleri	1.468
473	1	757	Harita ve Fotoğraf İşleri	1.473
479	1	769	Emlak ve Yapımla ilgili Diğer İşler	1.479
480	1	770	Eğitim İşleri (Genel)	1.480
481	1	771	Eğitim Planlan	1.481
482	1	772	Burs İşleri	1.482
488	1	773	Staj İşleri	1.488
493	1	774	Eğitimler, Kurslar	1.493
513	1	779	Rehberlik, Danışmanlık İşleri	1.513
514	1	799	Eğitimle İlgili Diğer İşler	1.514
515	1	800	İdari ve Sosyal İşler (Genel)	1.515
516	1	801	Taşıt ve İş Makineleri	1.516
524	1	802	Ulaştırma ve Servis İşleri	1.524
525	1	803	Resmi Mühür İş ve İşlemleri	1.525
526	1	804	Evrak Kayıt ve Sevk İşlemleri	1.526
531	1	805	Belge Yönetimi ve Arşiv İşlemleri	1.531
544	1	806	Kütüphane ve Dokümantasyon İşleri	1.544
557	1	807	Bakım-Onarım İşleri	1.557
564	1	808	Temizlik İşleri	1.564
565	1	809	Taşınır Mal İşlemleri	1.565
572	1	810	Sigorta İşleri	1.572
580	1	811	İletişim ve Haberleşme İşleri	1.580
590	1	812	Çevre Düzenleme İşleri	1.590
591	1	813	Sosyal İşler	1.591
604	1	814	Kampanyalar	1.604
605	1	815	Sosyal Yardımlar	1.605
965	1	953	Sivil Savunma İşleri	1.965
606	1	819	İdari ve Sosyal İşlerle İlgili Diğer İşler	1.606
607	1	820	Tanıtım ve Yayın İşleri (Genel)	1.607
608	1	821	Kültür, Tanıtım İşleri	1.608
621	1	824	Yayın İşleri	1.621
636	1	825	Basım, Depolama ve Dağıtım İşleri	1.636
644	1	828	Tercüme İşleri	1.644
645	1	839	Tanıtım ve Yayınla İlgili Diğer İşler	1.645
646	1	840	Mali İşler (Genel)	1.646
647	1	841	Bütçe Hazırlama ve Uygulama	1.647
671	1	843	Mizan, Bilanço ve Kesin Hesap İşleri	1.671
683	1	845	Sayıştay İncelemeleri	1.683
690	1	846	Emanet işleri	1.690
696	1	849	Banka İşlemleri	1.696
704	1	850	Kıymetli Evrak İşlemleri	1.704
705	1	851	Finansman ve Fon Yönetimi İşleri	1.705
711	1	853	Kredi İşlemleri	1.711
719	1	855	Borç ve Alacak İşlemleri	1.719
723	1	856	Harcama Yetkilileri ve Gerçekleştirme Görevlileri	1.723
724	1	857	Saymanlık ve Sayman Bildirimleri	1.724
725	1	858	İdari Yaptırım (Para Cezaları)	1.725
726	1	869	Mali Konularda Diğer İşler	1.726
727	1	870	Özel Kalem ve Protokol İşleri (Genel)	1.727
728	1	871	Makamın Konuşma ve Açıklamaları	1.728
729	1	872	Randevu Talepleri	1.729
730	1	873	Temas ve Ziyaretler	1.730
734	1	874	Günlük Programlar	1.734
735	1	876	Davet ve Tebrikler	1.735
740	1	877	Protokol İşleri	1.740
741	1	878	Resepsiyonlar	1.741
742	1	879	Rezervasyonlar	1.742
743	1	880	Törenler	1.743
744	1	899	Özel Kalem ve Protokolle İlgili Diğer İşler	1.744
745	1	900	Personel İşleri (Genel)	1.745
746	1	901	İş İstekleri	1.746
747	1	902	Personel Alımı	1.747
752	1	903	Personel Özlük İşleri	1.752
794	1	907	Kadro Pozisyon İşleri	1.794
800	1	912	Pasaport İşlemleri	1.800
806	1	914	Yabana Uyruklu Personel İstihdamı	1.806
807	1	915	Sendikalarla İlgili İşler	1.807
824	1	917	Sosyal Güvence Kapsamında Yapılan İşler	1.824
829	1	918	Çalışan Sağlığı ve İş Güvenliği	1.829
835	1	920	Performans Değerlendirme	1.835
836	1	929	Personelle İlgili Diğer İşler	1.836
837	1	930	Satınalma ve Satış İşleri (Genel)	1.837
838	1	933	Firma Bilgileri	1.838
843	1	934	Satınalma İşleri	1.843
867	1	939	Müşteri İlişkileri	1.867
871	1	940	Fiyatların Belirlenmesi-Tarifeler	1.871
872	1	941	Satış Talepleri ve Teyitleri	1.872
873	1	942	Satış İşlemleri	1.873
888	1	944	Gümrük İşlemleri	1.888
889	1	945	Akreditif İşlemleri	1.889
890	1	946	Ticari Belgeler	1.890
896	1	947	Stok Kontrol İşlemleri	1.896
903	1	949	Satınalma ve Satışla İlgili Diğer İşler	1.903
904	1	950	Topyekün Savunma Sivil Hizmetleri İşleri (Genel)	1.904
905	1	951	Seferberlik ve Savaş Hali Hazırlıkları	1.905
950	1	952	Koruyucu Güvenlik İşleri	1.950
998	1	954	Kimyasal, Biyolojik, Radyolojik ve Nükleer Savunma İşleri	1.998
1017	1	957	Tatbikat	1.1017
383	350	33	Mali ve Bütçesel Hükümler	1.342.349.350.383
404	397	03	ABGS’nin Faydalaması Olduğu Projeler	1.396.397.404
948	941	07	Erteleme Sonuç Raporları	1.905.941.948
968	966	02	Daire ve Müessese Sivil Savunma Planları	1.965.966.968
980	965	07	Kamu Kurum ve Kuruluşları İle STKlarının (STK) Sivil Savunma Harekatı İle İlgili Koordinasyon İşleri	1.965.980
\.


--
-- TOC entry 2958 (class 0 OID 37413)
-- Dependencies: 165
-- Data for Name: duyuru; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY duyuru (rid, duyurubasligi, duyurumetni, bitistarih, baslangictarih) FROM stdin;
\.


--
-- TOC entry 3042 (class 0 OID 45265)
-- Dependencies: 335
-- Data for Name: egitim; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY egitim (rid, egitimtanimref, ad, tarih, birimref, sehirref, ilceref, yer, kontenjan, sinavli, sinavyeri, sinavtarihi, durum) FROM stdin;
\.


--
-- TOC entry 3047 (class 0 OID 45375)
-- Dependencies: 345
-- Data for Name: egitimdegerlendirme; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY egitimdegerlendirme (rid, egitimkatilimciref, soru, cevap, aciklama) FROM stdin;
\.


--
-- TOC entry 3044 (class 0 OID 45317)
-- Dependencies: 339
-- Data for Name: egitimders; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY egitimders (rid, egitimref, egitimogretmenref, zaman, aciklama) FROM stdin;
\.


--
-- TOC entry 3045 (class 0 OID 45338)
-- Dependencies: 341
-- Data for Name: egitimdosya; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY egitimdosya (rid, egitimref, ad, path) FROM stdin;
\.


--
-- TOC entry 3046 (class 0 OID 45354)
-- Dependencies: 343
-- Data for Name: egitimkatilimci; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY egitimkatilimci (rid, egitimref, kisiref, durum, katilimbelgesi, sertifika, sinavnotu, sinavbasaridurum, aciklama) FROM stdin;
\.


--
-- TOC entry 3043 (class 0 OID 45296)
-- Dependencies: 337
-- Data for Name: egitimogretmen; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY egitimogretmen (rid, egitimref, kisiref, aciklama) FROM stdin;
\.


--
-- TOC entry 3041 (class 0 OID 45254)
-- Dependencies: 333
-- Data for Name: egitimtanim; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY egitimtanim (rid, ad, egitimturu, aciklama) FROM stdin;
\.


--
-- TOC entry 3032 (class 0 OID 45076)
-- Dependencies: 315
-- Data for Name: epostaanlikgonderimlog; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY epostaanlikgonderimlog (rid, kisiref, icerikref, tarih, gonderenref, durum, aciklama) FROM stdin;
\.


--
-- TOC entry 3030 (class 0 OID 45024)
-- Dependencies: 311
-- Data for Name: epostagonderimlog; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY epostagonderimlog (rid, gonderimref, kisiref, tarih, gonderenref, durum, aciklama) FROM stdin;
\.


--
-- TOC entry 3035 (class 0 OID 45140)
-- Dependencies: 321
-- Data for Name: etkinlik; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY etkinlik (rid, etkinliktanimref, ad, tarih, sehirref, ilceref, yer, webadresi, epostaadresi, telefonno, sorumlukisiref, durum) FROM stdin;
\.


--
-- TOC entry 3040 (class 0 OID 45238)
-- Dependencies: 331
-- Data for Name: etkinlikdosya; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY etkinlikdosya (rid, etkinlikref, dosyaturu, ad, path) FROM stdin;
\.


--
-- TOC entry 3036 (class 0 OID 45171)
-- Dependencies: 323
-- Data for Name: etkinlikkatilimci; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY etkinlikkatilimci (rid, etkinlikref, kisiref, durum) FROM stdin;
\.


--
-- TOC entry 3038 (class 0 OID 45207)
-- Dependencies: 327
-- Data for Name: etkinlikkurul; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY etkinlikkurul (rid, etkinlikref, kurulturu) FROM stdin;
\.


--
-- TOC entry 3039 (class 0 OID 45220)
-- Dependencies: 329
-- Data for Name: etkinlikkuruluye; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY etkinlikkuruluye (rid, etkinlikkurulref, uyeturu, uyeref) FROM stdin;
\.


--
-- TOC entry 3037 (class 0 OID 45189)
-- Dependencies: 325
-- Data for Name: etkinlikkurum; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY etkinlikkurum (rid, etkinlikref, kurumref, turu) FROM stdin;
\.


--
-- TOC entry 3034 (class 0 OID 45129)
-- Dependencies: 319
-- Data for Name: etkinliktanim; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY etkinliktanim (rid, ad, periyodik, aciklama) FROM stdin;
\.


--
-- TOC entry 3015 (class 0 OID 38613)
-- Dependencies: 281
-- Data for Name: evrak; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY evrak (rid, yil, evrakHareketTuru, kayittarihi, kayitno, gonderenkurumref, gonderenkisiref, dosyakoduref, konusu, evraktarihi, evrakno, evrakturu, teslimturu, gizlilikderecesi, aciliyetturu, durum, hazirlayanpersonelref, takdimmakami, anahtarkelime, path, icerik, gittigikurumref, gittigibirimref) FROM stdin;
6	2013	1	2013-06-28 03:18:21.294	2013 - .......	6	\N	3	Test	2013-05-15	2013 - GL - 00000003	1	1	1	1	1	\N			e:\\Upload\\Toys\\Evrak\\SuperMicro_Web_1372389505000.pdf	\N	\N	\N
3	2013	1	2013-06-27 06:25:05.361	2013 - GL - 00000004	4	\N	3	Test	2013-05-15	2013-02-221	1	1	1	1	1	\N			\N	\N	\N	\N
5	2013	1	2013-06-28 02:59:41.848	2013 - GL - 00000004	1	\N	3	Test	2013-05-15	2013-02-221	1	1	1	1	1	\N			e:\\Upload\\Toys\\Evrak\\SuperMicro_Web_1372388388000.pdf	\N	\N	\N
\.


--
-- TOC entry 3016 (class 0 OID 38674)
-- Dependencies: 283
-- Data for Name: evrakek; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY evrakek (rid, evrakref, path, tanim) FROM stdin;
1	3	e:\\Upload\\Toys\\Evrak\\SuperMicro_Web_1372400094000.pdf	test
7	5	e:\\Upload\\Toys\\Evrak\\EXPLANATORY NOTE_1372640985000.doc	test
8	5	e:\\Upload\\Toys\\Evrak\\SuperMicro_Web_1372641092000.pdf	test2
9	5	e:\\Upload\\Toys\\Evrak\\SoftwareMarketResearch_1372641099000.docx	test3
\.


--
-- TOC entry 2959 (class 0 OID 37429)
-- Dependencies: 167
-- Data for Name: fakulte; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY fakulte (rid, ad, universiteref) FROM stdin;
1	Mühendislik Fakültesi	1
2	Mühendislik Fakültesi	2
3	Fen- Edebiyat Fakültesi	1
11	Güzel Sanatlar Fakultesi	1
\.


--
-- TOC entry 3029 (class 0 OID 45001)
-- Dependencies: 309
-- Data for Name: gonderim; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY gonderim (rid, iletisimlisteref, icerikref, gonderimturu, gonderimzamani, zamanlamali, durum, gonderenref) FROM stdin;
\.


--
-- TOC entry 2960 (class 0 OID 37434)
-- Dependencies: 169
-- Data for Name: gorev; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY gorev (rid, ustref, ad, aciklama, erisimkodu) FROM stdin;
1	\N	Ana Görev	Test	1
\.


--
-- TOC entry 3028 (class 0 OID 44990)
-- Dependencies: 307
-- Data for Name: icerik; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY icerik (rid, icerik, gonderimturu, icerikturu) FROM stdin;
\.


--
-- TOC entry 2956 (class 0 OID 37403)
-- Dependencies: 161
-- Data for Name: ilce; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY ilce (rid, ad, sehirref, ilcekodu) FROM stdin;
112	SAFRANBOLU	20	1587
67	ETİMESGUT	6	1922
115	LADAĞ	1	1757
116	CEYHAN	1	1219
117	ÇUKUROVA	1	2033
118	FEKE	1	1329
119	İMAMOĞLU	1	1806
120	KARAİSALI	1	1437
121	KARATAŞ	1	1443
122	KOZAN	1	1486
123	POZANTI	1	1580
124	SAİMBEYLİ	1	1588
125	SARIÇAM	1	2032
126	SEYHAN	1	1104
127	TUFANBEYLİ	1	1687
128	YUMURTALIK	1	1734
129	YÜREĞİR	1	1748
130	ESNİ	2	1182
131	ÇELİKHAN	2	1246
132	GERGER	2	1347
133	GÖLBAŞI	2	1354
134	KAHTA	2	1425
135	MERKEZ	2	1105
136	SAMSAT	2	1592
137	SİNCİK	2	1985
138	TUT	2	1989
139	AŞMAKÇI	3	1771
140	BAYAT	3	1773
141	BOLVADİN	3	1200
142	ÇAY	3	1239
143	ÇOBANLAR	3	1906
144	DAZKIRI	3	1267
145	DİNAR	3	1281
146	EMİRDAĞ	3	1306
147	EVCİLER	3	1923
148	HOCALAR	3	1944
149	İHSANİYE	3	1404
150	İSCEHİSAR	3	1809
151	KIZILÖREN	3	1961
152	MERKEZ	3	1108
153	SANDIKLI	3	1594
154	SİNANPAŞA	3	1626
155	SULTANDAĞI	3	1639
156	ŞUHUT	3	1664
157	İYADİN	4	1283
158	DOĞUBAYAZIT	4	1287
159	ELEŞKİRT	4	1301
160	HAMUR	4	1379
161	MERKEZ	4	1111
162	PATNOS	4	1568
163	TAŞLIÇAY	4	1667
164	TUTAK	4	1691
165	ĞAÇÖREN	21	1860
166	ESKİL	21	1921
167	GÜLAĞAÇ	21	1932
168	GÜZELYURT	21	1861
169	MERKEZ	21	1120
170	ORTAKÖY	21	1557
171	SARIYAHŞİ	21	1866
172	ÖYNÜCEK	5	1363
173	GÜMÜŞHACIKÖY	5	1368
174	HAMAMÖZÜ	5	1938
175	MERKEZ	5	1134
176	MERZİFON	5	1524
177	SULUOVA	5	1641
178	TAŞOVA	5	1668
179	KYURT	6	1872
180	ALTINDAĞ	6	1130
181	AYAŞ	6	1157
182	BALA	6	1167
183	BEYPAZARI	6	1187
184	ÇAMLIDERE	6	1227
185	ÇANKAYA	6	1231
186	ÇUBUK	6	1260
187	ELMADAĞ	6	1302
188	EVREN	6	1924
189	GÖLBAŞI	6	1744
190	GÜDÜL	6	1365
191	HAYMANA	6	1387
192	KALECİK	6	1427
193	KAZAN	6	1815
194	KEÇİÖREN	6	1745
195	KIZILCAHAMAM	6	1473
196	MAMAK	6	1746
197	NALLIHAN	6	1539
198	POLATLI	6	1578
199	PURSAKLAR	6	2034
200	SİNCAN	6	1747
201	ŞEREFLİKOÇHİSAR	6	1658
202	YENİMAHALLE	6	1723
203	KSEKİ	7	1121
204	AKSU	7	2035
205	ALANYA	7	1126
206	DEMRE	7	1811
207	DÖŞEMEALTI	7	2036
208	ELMALI	7	1303
209	FİNİKE	7	1333
210	GAZİPAŞA	7	1337
211	GÜNDOĞMUŞ	7	1370
212	İBRADI	7	1946
213	KAŞ	7	1451
214	KEMER	7	1959
215	KEPEZ	7	2037
216	KONYAALTI	7	2038
217	KORKUTELİ	7	1483
218	KUMLUCA	7	1492
219	MANAVGAT	7	1512
220	MERKEZ	7	1138
221	MURATPAŞA	7	2039
222	SERİK	7	1616
223	ILDIR	22	1252
224	DAMAL	22	2008
225	GÖLE	22	1356
226	HANAK	22	1380
227	MERKEZ	22	1144
228	POSOF	22	1579
229	RDANUÇ	8	1145
230	ARHAVİ	8	1147
231	BORÇKA	8	1202
232	HOPA	8	1395
233	MERKEZ	8	1152
234	MURGUL	8	1828
235	ŞAVŞAT	8	1653
236	YUSUFELİ	8	1736
237	OZDOĞAN	9	1206
238	BUHARKENT	9	1781
239	ÇİNE	9	1256
240	DİDİM	9	2000
241	GERMENCİK	9	1348
242	İNCİRLİOVA	9	1807
243	KARACASU	9	1435
244	KARPUZLU	9	1957
245	KOÇARLI	9	1479
246	KÖŞK	9	1968
247	KUŞADASI	9	1497
248	KUYUCAK	9	1498
249	MERKEZ	9	1159
250	NAZİLLİ	9	1542
251	SÖKE	9	1637
252	SULTANHİSAR	9	1640
253	YENİPAZAR	9	1724
254	YVALIK	10	1161
255	BALYA	10	1169
256	BANDIRMA	10	1171
257	BİGADİÇ	10	1191
258	BURHANİYE	10	1216
259	DURSUNBEY	10	1291
260	EDREMİT	10	1294
261	ERDEK	10	1310
262	GÖMEÇ	10	1928
263	GÖNEN	10	1360
264	HAVRAN	10	1384
265	İVRİNDİ	10	1418
266	KEPSUT	10	1462
267	MANYAS	10	1514
268	MARMARA	10	1824
269	MERKEZ	10	1168
270	SAVAŞTEPE	10	1608
271	SINDIRGI	10	1619
272	SUSURLUK	10	1644
273	MASRA	23	1761
274	KURUCAŞİLE	23	1496
275	MERKEZ	23	1172
276	ULUS	23	1701
277	EŞİRİ	24	1184
278	GERCÜŞ	24	1345
279	HASANKEYF	24	1941
280	KOZLUK	24	1487
281	MERKEZ	24	1174
282	SASON	24	1607
283	YDINTEPE	25	1767
284	DEMİRÖZÜ	25	1788
285	MERKEZ	25	1176
286	OZÜYÜK	11	1210
287	GÖLPAZARI	11	1359
288	İNHİSAR	11	1948
289	MERKEZ	11	1192
290	OSMANELİ	11	1559
291	PAZARYERİ	11	1571
292	SÖĞÜT	11	1636
293	YENİPAZAR	11	1857
294	DAKLI	12	1750
295	GENÇ	12	1344
296	KARLIOVA	12	1446
297	KİĞI	12	1475
298	MERKEZ	12	1193
299	SOLHAN	12	1633
300	YAYLADERE	12	1855
301	YEDİSU	12	1996
302	DİLCEVAZ	13	1106
303	AHLAT	13	1112
304	GÜROYMAK	13	1798
305	HİZAN	13	1394
306	MERKEZ	13	1196
307	MUTKİ	13	1537
308	TATVAN	13	1669
309	ÖRTDİVAN	14	1916
310	GEREDE	14	1346
311	GÖYNÜK	14	1364
312	KIBRISCIK	14	1466
313	MENGEN	14	1522
314	MERKEZ	14	1199
315	MUDURNU	14	1531
316	SEBEN	14	1610
317	YENİÇAĞA	14	1997
318	ĞLASUN	15	1109
319	ALTINYAYLA	15	1874
320	BUCAK	15	1211
321	ÇAVDIR	15	1899
322	ÇELTİKÇİ	15	1903
323	GÖLHİSAR	15	1357
324	KARAMANLI	15	1813
325	KEMER	15	1816
326	MERKEZ	15	1215
327	TEFENNİ	15	1672
328	YEŞİLOVA	15	1728
329	ÜYÜKORHAN	16	1783
330	GEMLİK	16	1343
331	GÜRSU	16	1935
332	HARMANCIK	16	1799
333	İNEGÖL	16	1411
334	İZNİK	16	1420
335	KARACABEY	16	1434
336	KELES	16	1457
337	KESTEL	16	1960
338	MUDANYA	16	1530
339	MUSTAFAKEMALPAŞA	16	1535
340	NİLÜFER	16	1829
341	ORHANELİ	16	1553
342	ORHANGAZİ	16	1554
343	OSMANGAZİ	16	1832
344	YENİŞEHİR	16	1725
345	YILDIRIM	16	1859
346	YVACIK	17	1160
347	BAYRAMİÇ	17	1180
348	BİGA	17	1190
349	BOZCAADA	17	1205
350	ÇAN	17	1229
351	ECEABAT	17	1293
352	EZİNE	17	1326
353	GELİBOLU	17	1340
354	GÖKÇEADA	17	1408
355	LAPSEKİ	17	1503
356	MERKEZ	17	1230
357	YENİCE	17	1722
358	TKARACALAR	18	1765
359	BAYRAMÖREN	18	1885
360	ÇERKEŞ	18	1248
361	ELDİVAN	18	1300
362	ILGAZ	18	1399
363	KIZILIRMAK	18	1817
364	KORGUN	18	1963
365	KURŞUNLU	18	1494
366	MERKEZ	18	1232
367	ORTA	18	1555
368	ŞABANÖZÜ	18	1649
369	YAPRAKLI	18	1718
370	LACA	19	1124
371	BAYAT	19	1177
372	BOĞAZKALE	19	1778
373	DODURGA	19	1911
374	İSKİLİP	19	1414
375	KARGI	19	1445
376	LAÇİN	19	1972
377	MECİTÖZÜ	19	1520
378	MERKEZ	19	1259
379	OĞUZLAR	19	1976
380	ORTAKÖY	19	1556
381	OSMANCIK	19	1558
382	SUNGURLU	19	1642
383	UĞURLUDAĞ	19	1850
384	CIPAYAM	26	1102
385	AKKÖY	26	1871
386	BABADAĞ	26	1769
387	BAKLAN	26	1881
388	BEKİLLİ	26	1774
389	BEYAĞAÇ	26	1888
390	BOZKURT	26	1889
391	BULDAN	26	1214
392	ÇAL	26	1224
393	ÇAMELİ	26	1226
394	ÇARDAK	26	1233
395	ÇİVRİL	26	1257
396	GÜNEY	26	1371
397	HONAZ	26	1803
398	KALE	26	1426
399	MERKEZ	26	1271
400	SARAYKÖY	26	1597
401	SERİNHİSAR	26	1840
402	TAVAS	26	1670
403	AĞLAR	27	2040
404	BİSMİL	27	1195
405	ÇERMİK	27	1249
406	ÇINAR	27	1253
407	ÇÜNGÜŞ	27	1263
408	DİCLE	27	1278
409	EĞİL	27	1791
410	ERGANİ	27	1315
411	HANİ	27	1381
412	HAZRO	27	1389
413	KAYAPINAR	27	2041
414	KOCAKÖY	27	1962
415	KULP	27	1490
416	LİCE	27	1504
417	MERKEZ	27	1284
418	SİLVAN	27	1624
419	SUR	27	2042
420	YENİŞEHİR	27	2043
421	KÇAKOCA	28	1116
422	CUMAYERİ	28	1784
423	ÇİLİMLİ	28	1905
424	GÖLYAKA	28	1794
425	GÜMÜŞOVA	28	2017
426	KAYNAŞLI	28	2031
427	MERKEZ	28	1292
428	YIĞILCA	28	1730
429	NEZ	29	1307
430	HAVSA	29	1385
431	İPSALA	29	1412
432	KEŞAN	29	1464
433	LALAPAŞA	29	1502
434	MERİÇ	29	1523
435	MERKEZ	29	1295
436	SÜLOĞLU	29	1988
437	UZUNKÖPRÜ	29	1705
438	ĞIN	30	1110
439	ALACAKAYA	30	1873
440	ARICAK	30	1762
441	BASKİL	30	1173
442	KARAKOÇAN	30	1438
443	KEBAN	30	1455
444	KOVANCILAR	30	1820
445	MADEN	30	1506
446	MERKEZ	30	1298
447	PALU	30	1566
448	SİVRİCE	30	1631
449	AYIRLI	31	1243
450	İLİÇ	31	1406
451	KEMAH	31	1459
452	KEMALİYE	31	1460
453	MERKEZ	31	1318
454	OTLUKBELİ	31	1977
455	REFAHİYE	31	1583
456	TERCAN	31	1675
457	ÜZÜMLÜ	31	1853
458	ŞKALE	32	1153
459	AZİZİYE	32	1945
460	ÇAT	32	1235
461	HINIS	32	1392
462	HORASAN	32	1396
463	İSPİR	32	1416
464	KARAÇOBAN	32	1812
465	KARAYAZI	32	1444
466	KÖPRÜKÖY	32	1967
467	MERKEZ	32	1319
468	NARMAN	32	1540
469	OLTU	32	1550
470	OLUR	32	1551
471	PALANDÖKEN	32	2044
472	PASİNLER	32	1567
473	PAZARYOLU	32	1865
474	ŞENKAYA	32	1657
475	TEKMAN	32	1674
476	TORTUM	32	1683
477	UZUNDERE	32	1851
478	YAKUTİYE	32	2045
479	LPU	33	1759
480	BEYLİKOVA	33	1777
481	ÇİFTELER	33	1255
482	GÜNYÜZÜ	33	1934
483	HAN	33	1939
484	İNÖNÜ	33	1808
485	MAHMUDİYE	33	1508
486	MERKEZ	33	1322
487	MİHALGAZİ	33	1973
488	MİHALICÇIK	33	1527
489	ODUNPAZARI	33	2046
490	SARICAKAYA	33	1599
491	SEYİTGAZİ	33	1618
492	SİVRİHİSAR	33	1632
493	TEPEBAŞI	33	2047
494	RABAN	34	1139
495	İSLAHİYE	34	1415
496	KARKAMIŞ	34	1956
497	NİZİP	34	1546
498	NURDAĞI	34	1974
499	OĞUZELİ	34	1549
500	ŞAHİNBEY	34	1841
501	ŞEHİTKAMİL	34	1844
502	YAVUZELİ	34	1720
503	LUCRA	35	1133
504	BULANCAK	35	1212
505	ÇAMOLUK	35	1893
506	ÇANAKÇI	35	1894
507	DERELİ	35	1272
508	DOĞANKENT	35	1912
509	ESPİYE	35	1320
510	EYNESİL	35	1324
511	GÖRELE	35	1361
512	GÜCE	35	1930
513	KEŞAP	35	1465
514	MERKEZ	35	1352
515	PİRAZİZ	35	1837
516	ŞEBİNKARAHİSAR	35	1654
517	TİREBOLU	35	1678
518	YAĞLIDERE	35	1854
519	ELKİT	36	1458
520	KÖSE	36	1822
521	KÜRTÜN	36	1971
522	MERKEZ	36	1369
523	ŞİRAN	36	1660
524	TORUL	36	1684
525	UKURCA	37	1261
526	MERKEZ	37	1377
527	ŞEMDİNLİ	37	1656
528	YÜKSEKOVA	37	1737
529	LTINÖZÜ	38	1131
530	BELEN	38	1887
531	DÖRTYOL	38	1289
532	ERZİN	38	1792
533	HASSA	38	1382
534	İSKENDERUN	38	1413
535	KIRIKHAN	38	1468
536	KUMLU	38	1970
537	MERKEZ	38	1383
538	REYHANLI	38	1585
539	SAMANDAĞ	38	1591
540	YAYLADAĞI	38	1721
541	RALIK	39	1142
542	KARAKOYUNLU	39	2011
543	MERKEZ	39	1398
544	TUZLUCA	39	1692
545	KSU	40	1755
546	ATABEY	40	1154
547	EĞİRDİR	40	1297
548	GELENDOST	40	1341
549	GÖNEN	40	1929
550	KEÇİBORLU	40	1456
551	MERKEZ	40	1401
552	SENİRKENT	40	1615
553	SÜTÇÜLER	40	1648
554	ŞARKİKARAAĞAÇ	40	1651
555	ULUBORLU	40	1699
556	YALVAÇ	40	1717
557	YENİŞARBADEMLİ	40	2001
558	DALAR	41	1103
559	ARNAVUTKÖY	41	2048
560	ATAŞEHİR	41	2049
561	AVCILAR	41	2003
562	BAĞCILAR	41	2004
563	BAHÇELİEVLER	41	2005
564	BAKIRKÖY	41	1166
565	BAŞAKŞEHİR	41	2050
566	BAYRAMPAŞA	41	1886
567	BEŞİKTAŞ	41	1183
568	BEYKOZ	41	1185
569	BEYLİKDÜZÜ	41	2051
570	BEYOĞLU	41	1186
571	BÜYÜKÇEKMECE	41	1782
572	ÇATALCA	41	1237
573	ÇEKMEKÖY	41	2052
574	ESENLER	41	2016
575	ESENYURT	41	2053
576	EYÜP	41	1325
577	FATİH	41	1327
578	GAZİOSMANPAŞA	41	1336
579	GÜNGÖREN	41	2010
580	KADIKÖY	41	1421
581	KAĞITHANE	41	1810
582	KARTAL	41	1449
583	KÜÇÜKÇEKMECE	41	1823
584	MALTEPE	41	2012
585	PENDİK	41	1835
586	SANCAKTEPE	41	2054
587	SARIYER	41	1604
588	SİLİVRİ	41	1622
589	SULTANBEYLİ	41	2014
590	SULTANGAZİ	41	2055
591	ŞİLE	41	1659
592	ŞİŞLİ	41	1663
593	TUZLA	41	2015
594	ÜMRANİYE	41	1852
595	ÜSKÜDAR	41	1708
596	ZEYTİNBURNU	41	1739
597	LİAĞA	42	1128
598	BALÇOVA	42	2006
599	BAYINDIR	42	1178
600	BAYRAKLI	42	2056
601	BERGAMA	42	1181
602	BEYDAĞ	42	1776
603	BORNOVA	42	1203
604	BUCA	42	1780
605	ÇEŞME	42	1251
606	ÇİĞLİ	42	2007
607	DİKİLİ	42	1280
608	FOÇA	42	1334
609	GAZİEMİR	42	2009
610	GÜZELBAHÇE	42	2018
611	KARABAĞLAR	42	2057
612	KARABURUN	42	1432
613	KARŞIYAKA	42	1448
614	KEMALPAŞA	42	1461
615	KINIK	42	1467
616	KİRAZ	42	1477
617	KONAK	42	1819
618	MENDERES	42	1826
619	MENEMEN	42	1521
620	NARLIDERE	42	2013
621	ÖDEMİŞ	42	1563
622	SEFERİHİSAR	42	1611
623	SELÇUK	42	1612
624	TİRE	42	1677
625	TORBALI	42	1682
626	URLA	42	1703
627	FŞİN	43	1107
628	ANDIRIN	43	1136
629	ÇAĞLIYANCERİT	43	1785
630	EKİNÖZÜ	43	1919
631	ELBİSTAN	43	1299
632	GÖKSUN	43	1353
633	MERKEZ	43	1515
634	NURHAK	43	1975
635	PAZARCIK	43	1570
636	TÜRKOĞLU	43	1694
637	FLANİ	20	1296
638	ESKİPAZAR	20	1321
639	MERKEZ	20	1433
640	OVACIK	20	1561
641	YENİCE	20	1856
642	YRANCI	44	1768
643	BAŞYAYLA	44	1884
644	ERMENEK	44	1316
645	KAZIMKARABEKİR	44	1862
646	MERKEZ	44	1439
647	SARIVELİLER	44	1983
648	KYAKA	45	1756
649	ARPAÇAY	45	1149
650	DİGOR	45	1279
651	KAĞIZMAN	45	1424
652	MERKEZ	45	1447
653	SARIKAMIŞ	45	1601
654	SELİM	45	1614
655	SUSUZ	45	1645
656	BANA	46	1101
657	AĞLI	46	1867
658	ARAÇ	46	1140
659	AZDAVAY	46	1162
660	BOZKURT	46	1208
661	CİDE	46	1221
662	ÇATALZEYTİN	46	1238
663	DADAY	46	1264
664	DEVREKANİ	46	1277
665	DOĞANYURT	46	1915
666	HANÖNÜ	46	1940
667	İHSANGAZİ	46	1805
668	İNEBOLU	46	1410
669	KÜRE	46	1499
670	MERKEZ	46	1450
671	PINARBAŞI	46	1836
672	SEYDİLER	46	1984
673	ŞENPAZAR	46	1845
674	TAŞKÖPRÜ	46	1666
675	TOSYA	46	1685
676	KKIŞLA	47	1752
677	BÜNYAN	47	1218
678	DEVELİ	47	1275
679	FELAHİYE	47	1330
680	HACILAR	47	1936
681	İNCESU	47	1409
682	KOCASİNAN	47	1863
683	MELİKGAZİ	47	1864
684	ÖZVATAN	47	1978
685	PINARBAŞI	47	1576
686	SARIOĞLAN	47	1603
687	SARIZ	47	1605
688	TALAS	47	1846
689	TOMARZA	47	1680
690	YAHYALI	47	1715
691	YEŞİLHİSAR	47	1727
692	AHŞİLİ	48	1880
693	BALIŞEYH	48	1882
694	ÇELEBİ	48	1901
695	DELİCE	48	1268
696	KARAKEÇİLİ	48	1954
697	KESKİN	48	1463
698	MERKEZ	48	1469
699	SULAKYURT	48	1638
700	YAHŞİHAN	48	1992
701	ABAESKİ	49	1163
702	DEMİRKÖY	49	1270
703	KOFÇAZ	49	1480
704	LÜLEBURGAZ	49	1505
705	MERKEZ	49	1471
706	PEHLİVANKÖY	49	1572
707	PINARHİSAR	49	1577
708	VİZE	49	1714
709	KÇAKENT	50	1869
710	AKPINAR	50	1754
711	BOZTEPE	50	1890
712	ÇİÇEKDAĞI	50	1254
713	KAMAN	50	1429
714	MERKEZ	50	1472
715	MUCUR	50	1529
716	LBEYLİ	51	2023
717	MERKEZ	51	1476
718	MUSABEYLİ	51	2024
719	POLATELİ	51	2025
720	AŞİSKELE	52	2058
721	ÇAYIROVA	52	2059
722	DARICA	52	2060
723	DERİNCE	52	2030
724	DİLOVASI	52	2061
725	GEBZE	52	1338
726	GÖLCÜK	52	1355
727	İZMİT	52	2062
728	KANDIRA	52	1430
729	KARAMÜRSEL	52	1440
730	KARTEPE	52	2063
731	KÖRFEZ	52	1821
732	MERKEZ	52	1478
733	HIRLI	53	1868
734	AKÖREN	53	1753
735	AKŞEHİR	53	1122
736	ALTINEKİN	53	1760
737	BEYŞEHİR	53	1188
738	BOZKIR	53	1207
739	CİHANBEYLİ	53	1222
740	ÇELTİK	53	1902
741	ÇUMRA	53	1262
742	DERBENT	53	1907
743	DEREBUCAK	53	1789
744	DOĞANHİSAR	53	1285
745	EMİRGAZİ	53	1920
746	EREĞLİ	53	1312
747	GÜNEYSINIR	53	1933
748	HADİM	53	1375
749	HALKAPINAR	53	1937
750	HÜYÜK	53	1804
751	ILGIN	53	1400
752	KADINHANI	53	1422
753	KARAPINAR	53	1441
754	KARATAY	53	1814
755	KULU	53	1491
756	MERAM	53	1827
757	SARAYÖNÜ	53	1598
758	SELÇUKLU	53	1839
759	SEYDİŞEHİR	53	1617
760	TAŞKENT	53	1848
761	TUZLUKÇU	53	1990
762	YALIHÜYÜK	53	1994
763	YUNAK	53	1735
764	LTINTAŞ	54	1132
765	ASLANAPA	54	1764
766	ÇAVDARHİSAR	54	1898
767	DOMANİÇ	54	1288
768	DUMLUPINAR	54	1790
769	EMET	54	1304
770	GEDİZ	54	1339
771	HİSARCIK	54	1802
772	MERKEZ	54	1500
773	PAZARLAR	54	1979
774	SİMAV	54	1625
775	ŞAPHANE	54	1843
776	TAVŞANLI	54	1671
777	KÇADAĞ	55	1114
778	ARAPGİR	55	1143
779	ARGUVAN	55	1148
780	BATTALGAZİ	55	1772
781	DARENDE	55	1265
782	DOĞANŞEHİR	55	1286
783	DOĞANYOL	55	1914
784	HEKİMHAN	55	1390
785	KALE	55	1953
786	KULUNCAK	55	1969
787	MERKEZ	55	1509
788	PÜTÜRGE	55	1582
789	YAZIHAN	55	1995
790	YEŞİLYURT	55	1729
791	HMETLİ	56	1751
792	AKHİSAR	56	1118
793	ALAŞEHİR	56	1127
794	DEMİRCİ	56	1269
795	GÖLMARMARA	56	1793
796	GÖRDES	56	1362
797	KIRKAĞAÇ	56	1470
798	KÖPRÜBAŞI	56	1965
799	KULA	56	1489
800	MERKEZ	56	1513
801	SALİHLİ	56	1590
802	SARIGÖL	56	1600
803	SARUHANLI	56	1606
804	SELENDİ	56	1613
805	SOMA	56	1634
806	TURGUTLU	56	1689
807	ARGEÇİT	57	1787
808	DERİK	57	1273
809	KIZILTEPE	57	1474
810	MAZIDAĞI	57	1519
811	MERKEZ	57	1516
812	MİDYAT	57	1526
813	NUSAYBİN	57	1547
814	ÖMERLİ	57	1564
815	SAVUR	57	1609
816	YEŞİLLİ	57	2002
817	KDENİZ	58	2064
818	ANAMUR	58	1135
819	AYDINCIK	58	1766
820	BOZYAZI	58	1779
821	ÇAMLIYAYLA	58	1892
822	ERDEMLİ	58	1311
823	GÜLNAR	58	1366
824	MERKEZ	58	1402
825	MEZİTLİ	58	2065
826	MUT	58	1536
827	SİLİFKE	58	1621
828	TARSUS	58	1665
829	TOROSLAR	58	2066
830	YENİŞEHİR	58	2067
831	ODRUM	59	1197
832	DALAMAN	59	1742
833	DATÇA	59	1266
834	FETHİYE	59	1331
835	KAVAKLIDERE	59	1958
836	KÖYCEĞİZ	59	1488
837	MARMARİS	59	1517
838	MERKEZ	59	1532
839	MİLAS	59	1528
840	ORTACA	59	1831
841	ULA	59	1695
842	YATAĞAN	59	1719
843	ULANIK	60	1213
844	HASKÖY	60	1801
845	KORKUT	60	1964
846	MALAZGİRT	60	1510
847	MERKEZ	60	1534
848	VARTO	60	1711
849	CIGÖL	61	1749
850	AVANOS	61	1155
851	DERİNKUYU	61	1274
852	GÜLŞEHİR	61	1367
853	HACIBEKTAŞ	61	1374
854	KOZAKLI	61	1485
855	MERKEZ	61	1543
856	ÜRGÜP	61	1707
857	LTUNHİSAR	62	1876
858	BOR	62	1201
859	ÇAMARDI	62	1225
860	ÇİFTLİK	62	1904
861	MERKEZ	62	1544
862	ULUKIŞLA	62	1700
863	KKUŞ	63	1119
864	AYBASTI	63	1158
865	ÇAMAŞ	63	1891
866	ÇATALPINAR	63	1897
867	ÇAYBAŞI	63	1900
868	FATSA	63	1328
869	GÖLKÖY	63	1358
870	GÜLYALI	63	1795
871	GÜRGENTEPE	63	1797
872	İKİZCE	63	1947
873	KABADÜZ	63	1950
874	KABATAŞ	63	1951
875	KORGAN	63	1482
876	KUMRU	63	1493
877	MERKEZ	63	1552
878	MESUDİYE	63	1525
879	PERŞEMBE	63	1573
880	ULUBEY	63	1696
881	ÜNYE	63	1706
882	AHÇE	64	1165
883	DÜZİÇİ	64	1743
884	HASANBEYLİ	64	2027
885	KADİRLİ	64	1423
886	MERKEZ	64	1560
887	SUMBAS	64	2028
888	TOPRAKKALE	64	2029
889	RDEŞEN	65	1146
890	ÇAMLIHEMŞİN	65	1228
891	ÇAYELİ	65	1241
892	DEREPAZARI	65	1908
893	FINDIKLI	65	1332
894	GÜNEYSU	65	1796
895	HEMŞİN	65	1943
896	İKİZDERE	65	1405
897	İYİDERE	65	1949
898	KALKANDERE	65	1428
899	MERKEZ	65	1586
900	PAZAR	65	1569
901	DAPAZARI	66	2068
902	AKYAZI	66	1123
903	ARİFİYE	66	2069
904	ERENLER	66	2070
905	FERİZLİ	66	1925
906	GEYVE	66	1351
907	HENDEK	66	1391
908	KARAPÜRÇEK	66	1955
909	KARASU	66	1442
910	KAYNARCA	66	1453
911	KOCAALİ	66	1818
912	MERKEZ	66	1589
913	PAMUKOVA	66	1833
914	SAPANCA	66	1595
915	SERDİVAN	66	2071
916	SÖĞÜTLÜ	66	1986
917	TARAKLI	66	1847
918	LAÇAM	67	1125
919	ASARCIK	67	1763
920	ATAKUM	67	2072
921	AYVACIK	67	1879
922	BAFRA	67	1164
923	CANİK	67	2073
924	ÇARŞAMBA	67	1234
925	HAVZA	67	1386
926	İLKADIM	67	2074
927	KAVAK	67	1452
928	LADİK	67	1501
929	MERKEZ	67	1593
930	SALIPAZARI	67	1838
931	TEKKEKÖY	67	1849
932	TERME	67	1676
933	VEZİRKÖPRÜ	67	1712
934	YAKAKENT	67	1993
935	19 MAYIS	67	1830
936	YDINLAR	68	1878
937	BAYKAN	68	1179
938	ERUH	68	1317
939	KURTALAN	68	1495
940	MERKEZ	68	1620
941	PERVARİ	68	1575
942	ŞİRVAN	68	1662
943	YANCIK	69	1156
944	BOYABAT	69	1204
945	DİKMEN	69	1910
946	DURAĞAN	69	1290
947	ERFELEK	69	1314
948	GERZE	69	1349
949	MERKEZ	69	1627
950	SARAYDÜZÜ	69	1981
951	TÜRKELİ	69	1693
952	KINCILAR	70	1870
953	ALTINYAYLA	70	1875
954	DİVRİĞİ	70	1282
955	DOĞANŞAR	70	1913
956	GEMEREK	70	1342
957	GÖLOVA	70	1927
958	GÜRÜN	70	1373
959	HAFİK	70	1376
960	İMRANLI	70	1407
961	KANGAL	70	1431
962	KOYULHİSAR	70	1484
963	MERKEZ	70	1628
964	SUŞEHRİ	70	1646
965	ŞARKIŞLA	70	1650
966	ULAŞ	70	1991
967	YILDIZELİ	70	1731
968	ZARA	70	1738
969	KÇAKALE	71	1115
970	BİRECİK	71	1194
971	BOZOVA	71	1209
972	CEYLANPINAR	71	1220
973	HALFETİ	71	1378
974	HARRAN	71	1800
975	HİLVAN	71	1393
976	MERKEZ	71	1702
977	SİVEREK	71	1630
978	SURUÇ	71	1643
979	VİRANŞEHİR	71	1713
980	EYTÜŞŞEBAP	72	1189
981	CİZRE	72	1223
982	GÜÇLÜKONAK	72	1931
983	İDİL	72	1403
984	MERKEZ	72	1661
985	SİLOPİ	72	1623
986	ULUDERE	72	1698
987	ERKEZKÖY	73	1250
988	ÇORLU	73	1258
989	HAYRABOLU	73	1388
990	MALKARA	73	1511
991	MARMARAEREĞLİSİ	73	1825
992	MERKEZ	73	1673
993	MURATLI	73	1538
994	SARAY	73	1596
995	ŞARKÖY	73	1652
996	LMUS	74	1129
997	ARTOVA	74	1151
998	BAŞÇİFTLİK	74	1883
999	ERBAA	74	1308
1000	MERKEZ	74	1679
1001	NİKSAR	74	1545
1002	PAZAR	74	1834
1003	REŞADİYE	74	1584
1004	SULUSARAY	74	1987
1005	TURHAL	74	1690
1006	YEŞİLYURT	74	1858
1007	ZİLE	74	1740
1008	KÇAABAT	75	1113
1009	ARAKLI	75	1141
1010	ARSİN	75	1150
1011	BEŞİKDÜZÜ	75	1775
1012	ÇARŞIBAŞI	75	1896
1013	ÇAYKARA	75	1244
1014	DERNEKPAZARI	75	1909
1015	DÜZKÖY	75	1917
1016	HAYRAT	75	1942
1017	KÖPRÜBAŞI	75	1966
1018	MAÇKA	75	1507
1019	MERKEZ	75	1686
1020	OF	75	1548
1021	SÜRMENE	75	1647
1022	ŞALPAZARI	75	1842
1023	TONYA	75	1681
1024	VAKFIKEBİR	75	1709
1025	YOMRA	75	1732
1026	EMİŞGEZEK	76	1247
1027	HOZAT	76	1397
1028	MAZGİRT	76	1518
1029	MERKEZ	76	1688
1030	NAZIMİYE	76	1541
1031	OVACIK	76	1562
1032	PERTEK	76	1574
1033	PÜLÜMÜR	76	1581
1034	ANAZ	77	1170
1035	EŞME	77	1323
1036	KARAHALLI	77	1436
1037	MERKEZ	77	1704
1038	SİVASLI	77	1629
1039	ULUBEY	77	1697
1040	AHÇESARAY	78	1770
1041	BAŞKALE	78	1175
1042	ÇALDIRAN	78	1786
1043	ÇATAK	78	1236
1044	EDREMİT	78	1918
1045	ERCİŞ	78	1309
1046	GEVAŞ	78	1350
1047	GÜRPINAR	78	1372
1048	MERKEZ	78	1710
1049	MURADİYE	78	1533
1050	ÖZALP	78	1565
1051	SARAY	78	1980
1052	LTINOVA	79	2019
1053	ARMUTLU	79	2020
1054	ÇINARCIK	79	2021
1055	ÇİFTLİKKÖY	79	2022
1056	MERKEZ	79	1716
1057	TERMAL	79	2026
1058	KDAĞMADENİ	80	1117
1059	AYDINCIK	80	1877
1060	BOĞAZLIYAN	80	1198
1061	ÇANDIR	80	1895
1062	ÇAYIRALAN	80	1242
1063	ÇEKEREK	80	1245
1064	KADIŞEHRİ	80	1952
1065	MERKEZ	80	1733
1066	SARAYKENT	80	1982
1067	SARIKAYA	80	1602
1068	SORGUN	80	1635
1069	ŞEFAATLİ	80	1655
1070	YENİFAKILI	80	1998
1071	YERKÖY	80	1726
1072	LAPLI	81	1758
1073	ÇAYCUMA	81	1240
1074	DEVREK	81	1276
1075	EREĞLİ	81	1313
1076	GÖKÇEBEY	81	1926
1077	MERKEZ	81	1741
\.


--
-- TOC entry 3026 (class 0 OID 44956)
-- Dependencies: 303
-- Data for Name: iletisimliste; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY iletisimliste (rid, ad, ustref, erisimkodu, birimref, listegizlilikturu, aciklama) FROM stdin;
\.


--
-- TOC entry 3027 (class 0 OID 44977)
-- Dependencies: 305
-- Data for Name: iletisimlistedetay; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY iletisimlistedetay (rid, referanstipi, referans, iletisimlisteref) FROM stdin;
\.


--
-- TOC entry 2961 (class 0 OID 37442)
-- Dependencies: 171
-- Data for Name: internetadres; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY internetadres (rid, kisiref, netadresmetni, netadresturu, varsayilan, kurumref) FROM stdin;
21	\N	5234534	1	1	1
27	1	ufuk.seyhan@gmail.com	1	1	\N
37	\N	ufuk.seyhan@gmail.com	1	1	9
42	89	ufuk@deneme.com	1	1	\N
43	89	nimet@uluhan.com	1	1	\N
44	89	nimet@uluhan.com	1	1	\N
\.


--
-- TOC entry 2962 (class 0 OID 37451)
-- Dependencies: 173
-- Data for Name: islem; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY islem (rid, name, active) FROM stdin;
7	Menuitem	1
16	Role	1
18	Evrak	1
19	Kisi	1
20	Kisifotograf	1
21	Kisiizin	1
22	Kisikimlik	1
23	Kisiuyruk	1
24	Kurum	1
25	Adres	1
26	Internetadres	1
27	Telefon	1
15	Sehir	1
28	Ulke	1
29	Uye	1
31	Borc	1
32	Borcodeme	1
33	Odeme	1
34	Gorev	1
35	Donem	1
36	IsYeriTemsilcileri	1
37	KomisyonDonem	1
38	KomisyonUye	1
39	Kurul	1
40	KurulDonem	1
41	KurulDonemUye	1
42	KurulGorev	1
43	Aidat	1
44	Belgetip	1
45	BirimHesap	1
46	CezaTip	1
47	Duyuru	1
48	Islem	1
49	Islemlog	1
50	Komisyon	1
51	KurumHesap	1
52	KurumKisi	1
53	KurumTuru	1
54	Odemetip	1
55	Odultip	1
56	OgrenimKurum	1
58	PersonelBirimGorev	1
60	SistemParametre	1
61	Sorun	1
63	Uyarinot	1
64	Uzmanliktip	1
65	Yayin	1
67	Uyeabone	1
68	Uyeaidat	1
69	UyeBelge	1
70	Uyebilirkisi	1
71	Uyeceza	1
72	Uyemuafiyet	1
73	Uyeodeme	1
74	Uyeodul	1
75	Uyeogrenim	1
76	Uyesigorta	1
77	UyeUzmanlik	1
6	Personel	1
3	Ilce	1
9	Kullanici	1
8	KullaniciRole	1
10	IslemRole	1
1	IslemKullanici	1
4	Koy	1
11	Birim	1
78	Universite	1
79	Fakulte	1
80	Bolum	1
81	Evrakek	1
83	Uyedurumdegistirme	1
84	Anadonem	1
\.


--
-- TOC entry 2963 (class 0 OID 37454)
-- Dependencies: 174
-- Data for Name: islemkullanici; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY islemkullanici (rid, updatepermission, deletepermission, listpermission, islemref, kullaniciroleref) FROM stdin;
352	1	1	1	7	12
353	1	1	1	7	13
354	1	1	1	7	14
355	1	1	1	7	15
356	1	1	1	16	12
357	1	1	1	16	13
358	1	1	1	16	14
359	1	1	1	16	15
360	1	1	1	18	12
361	1	1	1	18	13
362	1	1	1	18	14
363	1	1	1	18	15
364	1	1	1	19	12
365	1	1	1	19	13
366	1	1	1	19	14
367	1	1	1	19	15
368	1	1	1	20	12
369	1	1	1	20	13
370	1	1	1	20	14
371	1	1	1	20	15
372	1	1	1	21	12
373	1	1	1	21	13
374	1	1	1	21	14
375	1	1	1	21	15
376	1	1	1	22	12
377	1	1	1	22	13
378	1	1	1	22	14
379	1	1	1	22	15
380	1	1	1	23	12
381	1	1	1	23	13
382	1	1	1	23	14
383	1	1	1	23	15
384	1	1	1	24	12
385	1	1	1	24	13
386	1	1	1	24	14
387	1	1	1	24	15
388	1	1	1	25	12
389	1	1	1	25	13
390	1	1	1	25	14
391	1	1	1	25	15
392	1	1	1	26	12
393	1	1	1	26	13
394	1	1	1	26	14
395	1	1	1	26	15
396	1	1	1	27	12
397	1	1	1	27	13
398	1	1	1	27	14
399	1	1	1	27	15
400	1	1	1	15	12
401	1	1	1	15	13
402	1	1	1	15	14
403	1	1	1	15	15
404	1	1	1	28	12
405	1	1	1	28	13
406	1	1	1	28	14
407	1	1	1	28	15
408	1	1	1	29	12
409	1	1	1	29	13
410	1	1	1	29	14
411	1	1	1	29	15
412	1	1	1	31	12
413	1	1	1	31	13
414	1	1	1	31	14
415	1	1	1	31	15
417	1	1	1	32	13
418	1	1	1	32	14
419	1	1	1	32	15
420	1	1	1	33	12
421	1	1	1	33	13
422	1	1	1	33	14
423	1	1	1	33	15
424	1	1	1	34	12
425	1	1	1	34	13
426	1	1	1	34	14
427	1	1	1	34	15
428	1	1	1	35	12
429	1	1	1	35	13
430	1	1	1	35	14
431	1	1	1	35	15
432	1	1	1	36	12
433	1	1	1	36	13
434	1	1	1	36	14
435	1	1	1	36	15
436	1	1	1	37	12
437	1	1	1	37	13
438	1	1	1	37	14
439	1	1	1	37	15
440	1	1	1	38	12
441	1	1	1	38	13
442	1	1	1	38	14
443	1	1	1	38	15
444	1	1	1	39	12
445	1	1	1	39	13
446	1	1	1	39	14
447	1	1	1	39	15
448	1	1	1	40	12
449	1	1	1	40	13
450	1	1	1	40	14
451	1	1	1	40	15
452	1	1	1	41	12
453	1	1	1	41	13
454	1	1	1	41	14
455	1	1	1	41	15
456	1	1	1	42	12
457	1	1	1	42	13
458	1	1	1	42	14
459	1	1	1	42	15
460	1	1	1	43	12
461	1	1	1	43	13
462	1	1	1	43	14
463	1	1	1	43	15
464	1	1	1	44	12
465	1	1	1	44	13
466	1	1	1	44	14
467	1	1	1	44	15
468	1	1	1	45	12
469	1	1	1	45	13
470	1	1	1	45	14
471	1	1	1	45	15
472	1	1	1	46	12
473	1	1	1	46	13
474	1	1	1	46	14
475	1	1	1	46	15
476	1	1	1	47	12
477	1	1	1	47	13
478	1	1	1	47	14
479	1	1	1	47	15
480	1	1	1	48	12
481	1	1	1	48	13
482	1	1	1	48	14
483	1	1	1	48	15
484	1	1	1	49	12
485	1	1	1	49	13
486	1	1	1	49	14
487	1	1	1	49	15
488	1	1	1	50	12
489	1	1	1	50	13
490	1	1	1	50	14
491	1	1	1	50	15
492	1	1	1	51	12
493	1	1	1	51	13
494	1	1	1	51	14
495	1	1	1	51	15
496	1	1	1	52	12
497	1	1	1	52	13
498	1	1	1	52	14
499	1	1	1	52	15
500	1	1	1	53	12
501	1	1	1	53	13
502	1	1	1	53	14
503	1	1	1	53	15
504	1	1	1	54	12
505	1	1	1	54	13
506	1	1	1	54	14
507	1	1	1	54	15
508	1	1	1	55	12
509	1	1	1	55	13
510	1	1	1	55	14
511	1	1	1	55	15
512	1	1	1	56	12
513	1	1	1	56	13
514	1	1	1	56	14
515	1	1	1	56	15
516	1	1	1	58	12
517	1	1	1	58	13
518	1	1	1	58	14
519	1	1	1	58	15
520	1	1	1	60	12
521	1	1	1	60	13
522	1	1	1	60	14
523	1	1	1	60	15
524	1	1	1	61	12
525	1	1	1	61	13
526	1	1	1	61	14
527	1	1	1	61	15
528	1	1	1	63	12
529	1	1	1	63	13
530	1	1	1	63	14
531	1	1	1	63	15
532	1	1	1	64	12
533	1	1	1	64	13
534	1	1	1	64	14
535	1	1	1	64	15
536	1	1	1	65	12
537	1	1	1	65	13
538	1	1	1	65	14
539	1	1	1	65	15
540	1	1	1	67	12
541	1	1	1	67	13
542	1	1	1	67	14
543	1	1	1	67	15
544	1	1	1	68	12
545	1	1	1	68	13
546	1	1	1	68	14
547	1	1	1	68	15
548	1	1	1	69	12
549	1	1	1	69	13
550	1	1	1	69	14
551	1	1	1	69	15
552	1	1	1	70	12
553	1	1	1	70	13
554	1	1	1	70	14
555	1	1	1	70	15
556	1	1	1	71	12
557	1	1	1	71	13
558	1	1	1	71	14
559	1	1	1	71	15
560	1	1	1	72	12
561	1	1	1	72	13
562	1	1	1	72	14
563	1	1	1	72	15
564	1	1	1	73	12
565	1	1	1	73	13
566	1	1	1	73	14
567	1	1	1	73	15
568	1	1	1	74	12
569	1	1	1	74	13
570	1	1	1	74	14
571	1	1	1	74	15
572	1	1	1	75	12
573	1	1	1	75	13
574	1	1	1	75	14
575	1	1	1	75	15
576	1	1	1	76	12
577	1	1	1	76	13
578	1	1	1	76	14
579	1	1	1	76	15
580	1	1	1	77	12
581	1	1	1	77	13
582	1	1	1	77	14
583	1	1	1	77	15
584	1	1	1	6	12
585	1	1	1	6	13
586	1	1	1	6	14
587	1	1	1	6	15
588	1	1	1	3	12
589	1	1	1	3	13
590	1	1	1	3	14
591	1	1	1	3	15
592	1	1	1	9	12
593	1	1	1	9	13
594	1	1	1	9	14
595	1	1	1	9	15
596	1	1	1	8	12
597	1	1	1	8	13
598	1	1	1	8	14
599	1	1	1	8	15
600	1	1	1	10	12
601	1	1	1	10	13
602	1	1	1	10	14
603	1	1	1	10	15
604	1	1	1	1	12
605	1	1	1	1	13
606	1	1	1	1	14
607	1	1	1	1	15
608	1	1	1	4	12
609	1	1	1	4	13
610	1	1	1	4	14
611	1	1	1	4	15
612	1	1	1	11	12
613	1	1	1	11	13
614	1	1	1	11	14
615	1	1	1	11	15
616	1	1	1	78	12
617	1	1	1	78	13
618	1	1	1	78	14
619	1	1	1	78	15
620	1	1	1	79	12
621	1	1	1	79	13
622	1	1	1	79	14
623	1	1	1	79	15
624	1	1	1	80	12
625	1	1	1	80	13
626	1	1	1	80	14
627	1	1	1	80	15
416	1	1	1	32	12
\.


--
-- TOC entry 2964 (class 0 OID 37458)
-- Dependencies: 175
-- Data for Name: islemlog; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY islemlog (rid, islemturu, islemtarihi, tabloadi, referansrid, eskideger, yenideger, kullanicirid, kullanicitext) FROM stdin;
421	1	2013-07-31 08:53:44.433	Kisi	90		Kimlik No : 26750630908<br />Ad : UFUK<br />Soyad : SEYHAN<br />???kisi.kimliknotip??? not found : TCKimlikNoAktif : Evet	1	ufuk (Ufuk Seyhan)
398	1	2013-07-11 01:43:45.772	Uye	70		Kişi : Adnan Erdursun (22222222222)<br />Sicil No : <br />Üye Tipi : Üye<br />Birim : Makine Müh. Odası<br />Üyelik Tarihi : 10/07/2013<br />Üye Durumu : <br />Kayıt Durum : İncelemede<br />Öğrenci No : 	1	ufuk (Ufuk Seyhan)
399	2	2013-07-11 01:52:18.814	Uye	70	Kişi : Adnan Erdursun (22222222222)<br />Sicil No : <br />Üye Tipi : Üye<br />Birim : Makine Müh. Odası<br />Üyelik Tarihi : 10/07/2013<br />Üye Durumu : <br />Kayıt Durum : İncelemede<br />Öğrenci No : Email : HayırPosta : EvetSMS : Hayır	Kişi : Adnan Erdursun (22222222222)<br />Sicil No : <br />Üye Tipi : Üye<br />Birim : Makine Müh. Odası<br />Üyelik Tarihi : 10/07/2013<br />Üye Durumu : <br />Kayıt Durum : İncelemede<br />Öğrenci No : Email : EvetPosta : EvetSMS : Hayır	1	ufuk (Ufuk Seyhan)
400	2	2013-07-11 01:52:26.352	Uye	70	Kişi : Adnan Erdursun (22222222222)<br />Sicil No : <br />Üye Tipi : Üye<br />Birim : Makine Müh. Odası<br />Üyelik Tarihi : 10/07/2013<br />Üye Durumu : <br />Kayıt Durum : İncelemede<br />Öğrenci No : Email : EvetPosta : EvetSMS : Hayır	Kişi : Adnan Erdursun (22222222222)<br />Sicil No : <br />Üye Tipi : Üye<br />Birim : Makine Müh. Odası<br />Üyelik Tarihi : 10/07/2013<br />Üye Durumu : <br />Kayıt Durum : İncelemede<br />Öğrenci No : Email : EvetPosta : HayırSMS : Hayır	1	ufuk (Ufuk Seyhan)
401	1	2013-07-11 01:57:22.607	Kisi	89		Kimlik No : 69619111958<br />Ad : NİMET<br />Soyad : ULUHAN<br />???kisi.kimliknotip??? not found : TCKimlikNoAktif : Evet	1	ufuk (Ufuk Seyhan)
402	1	2013-07-11 01:57:22.616	Kisikimlik	57		Kişi : NİMET ULUHAN (69619111958)<br />TC Kimlik No : 69619111958<br />Baba Adı : MUSLU<br />Ana Adı : LUTFİYE<br />Doğum Yeri : ANAMUR<br />Doğum Tarihi : Thu Apr 15 00:00:00 VET 1954<br />Cinsiyet : Kadın<br />Medeni Hali : Evli<br />Şehir : UŞAK<br />Şehir : MERKEZ<br />Cilt No : 4<br />Aile No : 1<br />Sira No : 6<br />Anne TC Kimlik No : <br />Baba TC Kimlik No  : <br />Eş TC Kimlik No  : <br />Kişi Durum : Açık	1	ufuk (Ufuk Seyhan)
403	1	2013-07-11 01:58:49.896	Uye	71		Kişi : NİMET ULUHAN (69619111958)<br />Sicil No : G0000000001<br />Üye Tipi : Üye<br />Birim : Makine Müh. Odası<br />Üyelik Tarihi : 11/07/2013<br />Üye Durumu : Pasif Üye<br />Kayıt Durum : <br />Öğrenci No : Email : HayırPosta : HayırSMS : Hayır	1	ufuk (Ufuk Seyhan)
404	1	2013-07-11 02:22:17.18	Uye	72		Kişi : DERIN SERYHAN (12121212121)<br />Sicil No : C0000000002<br />Üye Tipi : Üye<br />Birim : Makine Müh. Odası<br />Üyelik Tarihi : 10/07/2013<br />Üye Durumu : Üye<br />Kayıt Durum : İncelemede<br />Email : EvetPosta : HayırSMS : Evet	1	ufuk (Ufuk Seyhan)
405	1	2013-07-12 03:34:17.489	Uye	73		Kişi : NİMET ULUHAN (69619111958)<br />Sicil No : G0000000001<br />Üye Tipi : Üye<br />Birim : Makine Müh. Odası / TMMOB<br />Üyelik Tarihi : 12/07/2013<br />Üye Durumu : Pasif Üye<br />Kayıt Durum : <br />Öğrenci No : Email : HayırPosta : HayırSMS : Evet	1	ufuk (Ufuk Seyhan)
406	2	2013-07-12 03:34:27.271	Uye	30	Kişi : Ufuk Seyhan (11111111111)<br />Sicil No : C0000000004<br />Üye Tipi : Üye<br />Birim : Makine Müh. Odası / TMMOB<br />Üyelik Tarihi : 12/06/2013<br />Üye Durumu : İşsiz<br />Kayıt Durum : Onaylanmış<br />Onaylayan : Ufuk Seyhan<br />Onay Tarihi : 14/06/2013Öğrenci No : Email : HayırPosta : HayırSMS : Evet	Kişi : Ufuk Seyhan (11111111111)<br />Sicil No : C0000000004<br />Üye Tipi : Üye<br />Birim : Makine Müh. Odası / TMMOB<br />Üyelik Tarihi : 12/06/2013<br />Üye Durumu : Üye<br />Kayıt Durum : Onaylanmış<br />Onaylayan : Ufuk Seyhan<br />Onay Tarihi : 14/06/2013Öğrenci No : Email : HayırPosta : HayırSMS : Evet	1	ufuk (Ufuk Seyhan)
407	2	2013-07-18 08:04:28.97	Personel	1	Başlama Tarihi : 2013-01-01<br />Durum : Çalışıyor<br />Kişi : Adnan Erdursun (22222222222)Birim : Bilg. Müh. Odası / TMMOB	Başlama Tarihi : Tue Jan 01 00:00:00 VET 2013<br />Durum : İzinli<br />Kişi : Adnan Erdursun (22222222222)Birim : Bilg. Müh. Odası / TMMOB	1	ufuk (Ufuk Seyhan)
408	2	2013-07-18 08:04:52.192	Personel	1	Başlama Tarihi : 2013-01-01<br />Durum : İzinli<br />Kişi : Adnan Erdursun (22222222222)Birim : Bilg. Müh. Odası / TMMOB	Başlama Tarihi : Tue Jan 01 00:00:00 VET 2013<br />Durum : Ayrılmış<br />Kişi : Adnan Erdursun (22222222222)Birim : Bilg. Müh. Odası / TMMOB	1	ufuk (Ufuk Seyhan)
409	1	2013-07-18 09:07:45.235	Personel	74		Başlama Tarihi : Wed Jul 10 00:00:00 VET 2013<br />Durum : Çalışıyor<br />Kişi : Tevfik Yağlıcı (33333333333)Birim : Makine Müh. Odası / TMMOB	1	ufuk (Ufuk Seyhan)
410	1	2013-07-18 09:08:35.983	Personel	75		Başlama Tarihi : Mon Jul 08 00:00:00 VET 2013<br />Durum : Çalışıyor<br />Kişi : Utku Demir (55555555555)Birim : Bilg. Müh. Odası / TMMOB	1	ufuk (Ufuk Seyhan)
411	1	2013-07-19 02:19:45.134	Telefon	80		Kişi : Deniz Damla (10101010101)<br />Telefon No : 05005345345<br />Varsayılan : Evet<br />Telefon Türü  : GSM	1	ufuk (Ufuk Seyhan)
412	2	2013-07-22 01:10:14.475	Uye	73	Kişi : NİMET ULUHAN (69619111958)<br />Sicil No : C0000000002<br />Üye Tipi : Üye<br />Birim : Makine Müh. Odası<br />Üyelik Tarihi : 12/07/2013<br />Üye Durumu : Üye<br />Kayıt Durum : Onaylanmış<br />Onaylayan : Ufuk Seyhan<br />Onay Tarihi : 12/07/2013Öğrenci No : Email : HayırPosta : HayırSMS : Evet	Kişi : NİMET ULUHAN (69619111958)<br />Sicil No : C0000000002<br />Üye Tipi : Üye<br />Birim : Makine Müh. Odası<br />Üyelik Tarihi : 12/07/2013<br />Üye Durumu : Üye<br />Kayıt Durum : Onaylanmış<br />Onaylayan : Ufuk Seyhan<br />Onay Tarihi : 12/07/2013Öğrenci No : Email : HayırPosta : HayırSMS : Hayır	1	ufuk (Ufuk Seyhan)
413	2	2013-07-22 01:11:18.302	Uye	73	Kişi : NİMET ULUHAN (69619111958)<br />Sicil No : C0000000002<br />Üye Tipi : Öğrenci<br />Birim : Makine Müh. Odası<br />Üyelik Tarihi : 12/07/2013<br />Üye Durumu : Üye<br />Kayıt Durum : Onaylanmış<br />Onaylayan : Ufuk Seyhan<br />Onay Tarihi : 12/07/2013Sınıfı : 1Öğrenci No : Email : HayırPosta : HayırSMS : Hayır	Kişi : NİMET ULUHAN (69619111958)<br />Sicil No : C0000000002<br />Üye Tipi : Üye<br />Birim : Makine Müh. Odası<br />Üyelik Tarihi : 12/07/2013<br />Üye Durumu : Üye<br />Kayıt Durum : Onaylanmış<br />Onaylayan : Ufuk Seyhan<br />Onay Tarihi : 12/07/2013Sınıfı : 1Öğrenci No : Email : HayırPosta : HayırSMS : Hayır	1	ufuk (Ufuk Seyhan)
414	2	2013-07-22 01:11:45.979	Uye	73	Kişi : NİMET ULUHAN (69619111958)<br />Sicil No : C0000000002<br />Üye Tipi : Üye<br />Birim : Makine Müh. Odası<br />Üyelik Tarihi : 12/07/2013<br />Üye Durumu : Üye<br />Kayıt Durum : Onaylanmış<br />Onaylayan : Ufuk Seyhan<br />Onay Tarihi : 12/07/2013Sınıfı : 1Öğrenci No : Email : HayırPosta : HayırSMS : Hayır	Kişi : NİMET ULUHAN (69619111958)<br />Sicil No : C0000000002<br />Üye Tipi : Öğrenci<br />Birim : Makine Müh. Odası<br />Üyelik Tarihi : 12/07/2013<br />Üye Durumu : Üye<br />Kayıt Durum : Onaylanmış<br />Onaylayan : Ufuk Seyhan<br />Onay Tarihi : 12/07/2013Sınıfı : 1Öğrenci No : 543543534Email : HayırPosta : HayırSMS : Hayır	1	ufuk (Ufuk Seyhan)
415	1	2013-07-22 01:29:14.091	Uye	74		Kişi : NİMET ULUHAN (69619111958)<br />Sicil No : 53453245342<br />Üye Tipi : Üye<br />Birim : Makine Müh. Odası<br />Üyelik Tarihi : 12/07/2013<br />Üye Durumu : Üye<br />Kayıt Durum : Onaylanmış<br />Onaylayan : Ufuk Seyhan<br />Onay Tarihi : 12/07/2013Öğrencilikteki Üyelik : NİMET ULUHAN, Sicil No: 53453245342Email : HayırPosta : HayırSMS : Hayır	1	ufuk (Ufuk Seyhan)
416	2	2013-07-22 01:42:12.664	Uye	73	Kişi : NİMET ULUHAN (69619111958)<br />Sicil No : C0000000002<br />Üye Tipi : Öğrenci<br />Birim : Makine Müh. Odası<br />Üyelik Tarihi : 12/07/2013<br />Üye Durumu : Üye<br />Kayıt Durum : Onaylanmış<br />Onaylayan : Ufuk Seyhan<br />Onay Tarihi : 12/07/2013Sınıfı : 1Öğrenci No : 543543534Email : HayırPosta : HayırSMS : Hayır	Kişi : NİMET ULUHAN (69619111958)<br />Sicil No : G0000000001<br />Üye Tipi : Üye<br />Birim : Makine Müh. Odası<br />Üyelik Tarihi : 12/07/2013<br />Üye Durumu : Üye<br />Kayıt Durum : Onaylanmış<br />Onaylayan : Ufuk Seyhan<br />Onay Tarihi : 12/07/2013Email : HayırPosta : HayırSMS : Hayır	1	ufuk (Ufuk Seyhan)
417	2	2013-07-22 01:48:10.637	Uye	73	Kişi : NİMET ULUHAN (69619111958)<br />Sicil No : G0000000001<br />Üye Tipi : Öğrenci<br />Birim : Makine Müh. Odası<br />Üyelik Tarihi : 12/07/2013<br />Üye Durumu : Üye<br />Kayıt Durum : Onaylanmış<br />Onaylayan : Ufuk Seyhan<br />Onay Tarihi : 12/07/2013Email : HayırPosta : HayırSMS : Hayır	Kişi : NİMET ULUHAN (69619111958)<br />Sicil No : G0000000000<br />Üye Tipi : Üye<br />Birim : Makine Müh. Odası<br />Üyelik Tarihi : 12/07/2013<br />Üye Durumu : Üye<br />Kayıt Durum : Onaylanmış<br />Onaylayan : Ufuk Seyhan<br />Onay Tarihi : 12/07/2013Email : HayırPosta : HayırSMS : Hayır	1	ufuk (Ufuk Seyhan)
418	1	2013-07-25 02:01:30.904	Adres	78		Kişi : ŞEBNEM FERAH (13131313131)<br />Varsayilan : Evet<br />Adres Türü : Ev AdresiAdres Tipi : İl/İlce MerkeziAçık Adres : Açık adresiBina Blok Adi : Bina Site Adi : Csbm : Dis Kapi No : Ic Kapi No : Il : ANKARAIlce : ÇANKAYAMahalle : Deneme	1	ufuk (Ufuk Seyhan)
419	1	2013-07-25 02:01:52.716	Adres	79		Kişi : ŞEBNEM FERAH (13131313131)<br />Varsayilan : Evet<br />Adres Türü : Ev AdresiAdres Tipi : İl/İlce MerkeziAçık Adres : açık adresi 2Bina Blok Adi : Bina Site Adi : Csbm : Dis Kapi No : Ic Kapi No : Il : KARABÜKIlce : SAFRANBOLUMahalle : test	1	ufuk (Ufuk Seyhan)
420	1	2013-07-29 11:11:11.684	Uye	75		Kişi : NİMET ULUHAN (69619111958)<br />Sicil No : G0000000001<br />Üye Tipi : Üye<br />Birim : Makine Müh. Odası<br />Üyelik Tarihi : 29/07/2013<br />Üye Durumu : Pasif Üye<br />Kayıt Durum : <br />Öğrenci No : Email : HayırPosta : HayırSMS : Hayır	1	ufuk (Ufuk Seyhan)
422	1	2013-07-31 08:53:44.462	Kisikimlik	58		Kişi : UFUK SEYHAN (26750630908)<br />TC Kimlik No : 26750630908<br />Baba Adı : ERKİN<br />Ana Adı : TÜRKAN<br />Doğum Yeri : SAFRANBOLU<br />Doğum Tarihi : Mon Mar 17 00:00:00 EET 1980<br />Cinsiyet : Erkek<br />Medeni Hali : Evli<br />Şehir : KARABÜK<br />Şehir : SAFRANBOLU<br />Cilt No : 53<br />Aile No : 27<br />Sira No : 10<br />Anne TC Kimlik No : <br />Baba TC Kimlik No  : <br />Eş TC Kimlik No  : <br />Kişi Durum : Açık	1	ufuk (Ufuk Seyhan)
\.


--
-- TOC entry 2965 (class 0 OID 37466)
-- Dependencies: 177
-- Data for Name: islemrole; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY islemrole (rid, roleref, updatepermission, deletepermission, listpermission, islemref) FROM stdin;
215	1	1	1	1	7
216	1	1	1	1	16
217	1	1	1	1	18
218	1	1	1	1	19
219	1	1	1	1	20
220	1	1	1	1	21
221	1	1	1	1	22
222	1	1	1	1	23
223	1	1	1	1	24
224	1	1	1	1	25
225	1	1	1	1	26
226	1	1	1	1	27
227	1	1	1	1	15
228	1	1	1	1	28
229	1	1	1	1	29
230	1	1	1	1	31
231	1	1	1	1	32
232	1	1	1	1	33
233	1	1	1	1	34
234	1	1	1	1	35
235	1	1	1	1	36
236	1	1	1	1	37
237	1	1	1	1	38
238	1	1	1	1	39
239	1	1	1	1	40
240	1	1	1	1	41
241	1	1	1	1	42
242	1	1	1	1	43
243	1	1	1	1	44
244	1	1	1	1	45
245	1	1	1	1	46
246	1	1	1	1	47
247	1	1	1	1	48
248	1	1	1	1	49
249	1	1	1	1	50
250	1	1	1	1	51
251	1	1	1	1	52
252	1	1	1	1	53
253	1	1	1	1	54
254	1	1	1	1	55
255	1	1	1	1	56
256	1	1	1	1	58
257	1	1	1	1	60
258	1	1	1	1	61
259	1	1	1	1	63
260	1	1	1	1	64
261	1	1	1	1	65
262	1	1	1	1	67
263	1	1	1	1	68
264	1	1	1	1	69
265	1	1	1	1	70
266	1	1	1	1	71
267	1	1	1	1	72
268	1	1	1	1	73
269	1	1	1	1	74
270	1	1	1	1	75
271	1	1	1	1	76
272	1	1	1	1	77
273	1	1	1	1	6
274	1	1	1	1	3
275	1	1	1	1	9
276	1	1	1	1	8
277	1	1	1	1	10
278	1	1	1	1	1
279	1	1	1	1	4
280	1	1	1	1	11
281	1	1	1	1	78
282	1	1	1	1	79
283	1	1	1	1	80
284	2	1	1	1	7
285	2	1	1	1	16
286	2	1	1	1	18
287	2	1	1	1	19
288	2	1	1	1	20
289	2	1	1	1	21
290	2	1	1	1	22
291	2	1	1	1	23
292	2	1	1	1	24
293	2	1	1	1	25
294	2	1	1	1	26
295	2	1	1	1	27
296	2	1	1	1	15
297	2	1	1	1	28
298	2	1	1	1	29
299	2	1	1	1	31
300	2	1	1	1	32
301	2	1	1	1	33
302	2	1	1	1	34
303	2	1	1	1	35
304	2	1	1	1	36
305	2	1	1	1	37
306	2	1	1	1	38
307	2	1	1	1	39
308	2	1	1	1	40
309	2	1	1	1	41
310	2	1	1	1	42
311	2	1	1	1	43
312	2	1	1	1	44
313	2	1	1	1	45
314	2	1	1	1	46
315	2	1	1	1	47
316	2	1	1	1	48
317	2	1	1	1	49
318	2	1	1	1	50
319	2	1	1	1	51
320	2	1	1	1	52
321	2	1	1	1	53
322	2	1	1	1	54
323	2	1	1	1	55
324	2	1	1	1	56
325	2	1	1	1	58
326	2	1	1	1	60
327	2	1	1	1	61
328	2	1	1	1	63
329	2	1	1	1	64
330	2	1	1	1	65
331	2	1	1	1	67
332	2	1	1	1	68
333	2	1	1	1	69
334	2	1	1	1	70
335	2	1	1	1	71
336	2	1	1	1	72
337	2	1	1	1	73
338	2	1	1	1	74
339	2	1	1	1	75
340	2	1	1	1	76
341	2	1	1	1	77
342	2	1	1	1	6
343	2	1	1	1	3
344	2	1	1	1	9
345	2	1	1	1	8
346	2	1	1	1	10
347	2	1	1	1	1
348	2	1	1	1	4
349	2	1	1	1	11
350	2	1	1	1	78
351	2	1	1	1	79
352	2	1	1	1	80
\.


--
-- TOC entry 2966 (class 0 OID 37470)
-- Dependencies: 178
-- Data for Name: isyeritemsilcileri; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY isyeritemsilcileri (rid, uyeref, kurumref, aktif) FROM stdin;
3	30	9	1
5	30	9	1
\.


--
-- TOC entry 2967 (class 0 OID 37475)
-- Dependencies: 180
-- Data for Name: kisi; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY kisi (rid, kimlikno, ad, soyad, kimliknotip, aktif) FROM stdin;
1	11111111111	Ufuk	Seyhan	2	1
2	22222222222	Adnan	Erdursun	1	1
4	33333333333	Tevfik	Yağlıcı	1	1
7	44444444444	test	test	1	0
50	55555555555	Utku	Demir	1	1
51	66666666666	Emre	Aydın	1	1
52	77777777777	Duygu	Atasoy	1	1
53	88888888888	Dilara	Tara	1	1
54	99999999999	Burak	Köksal	1	1
55	10101010101	Deniz	Damla	1	1
59	12121212121	DERIN	SERYHAN	1	0
62	13131313131	ŞEBNEM	FERAH	1	1
63	14141414141	ali	veli	1	1
64	15151515151	ffsdfd	sfsdfsd	1	1
65	16161616161	deneme	test	1	1
66	17171717171	fdsfsd	fsdfdsfds	1	1
68	19191919191	BURCU	ULUHAN SEYHAN	1	1
69	12345123451	Deneme	Deniz	1	1
70	98745697891	Deneme	Damla	1	1
71	98765432109	Berk	Şentürk	1	1
72	69616112002	BURCU	ULUHAN SEYHAN	1	1
67	18181818181	DENİZ	ŞENTÜRK	1	1
89	69619111958	NİMET	ULUHAN	1	1
90	26750630908	UFUK	SEYHAN	1	1
\.


--
-- TOC entry 2968 (class 0 OID 37481)
-- Dependencies: 182
-- Data for Name: kisifotograf; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY kisifotograf (rid, kisiref, path, varsayilan) FROM stdin;
4	2	stackedclouds-1_1366173805000.jpg	\N
14	51	Image1_1368770623000.jpg	\N
12	89	Image1_1368599793000.jpg	\N
20	55	indir_1374199229000.jpg	1
21	1	dasfds_1374199889000.jpg	1
\.


--
-- TOC entry 2969 (class 0 OID 37486)
-- Dependencies: 184
-- Data for Name: kisiizin; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY kisiizin (rid, kisiref, evrakref, baslangictarih, bitistarih, kisiizindurum, aciklama) FROM stdin;
1	1	1	2013-04-15	2013-04-17	1	
\.


--
-- TOC entry 2970 (class 0 OID 37491)
-- Dependencies: 186
-- Data for Name: kisikimlik; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY kisikimlik (rid, kisiref, kimlikno, babaad, anaad, dogumilceref, dogumyer, dogumtarih, cinsiyet, medenihal, sehirref, ilceref, ciltno, aileno, sirano, kisidurum, olumtarih, estckimlikno, annetckimlikno, babatckimlikno, ciltaciklama, verildigiyer, verilistarihi, kangrup, verilisnedeni, mahalle) FROM stdin;
57	89	69619111958	MUSLU	LUTFİYE	\N	ANAMUR	1954-04-15	2	2	77	1037	4	1	6	1	\N				ELMALIDERE	\N	\N	\N	\N	\N
58	90	26750630908	ERKİN	TÜRKAN	\N	SAFRANBOLU	1980-03-17	1	2	20	112	53	27	10	1	\N				KONARI	\N	\N	\N	\N	\N
\.


--
-- TOC entry 2971 (class 0 OID 37496)
-- Dependencies: 188
-- Data for Name: kisiuyruk; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY kisiuyruk (rid, kisiref, uyruktip, ulkeref) FROM stdin;
1	1	1	1
2	2	1	1
4	51	2	1
\.


--
-- TOC entry 2972 (class 0 OID 37501)
-- Dependencies: 190
-- Data for Name: komisyon; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY komisyon (rid, ustref, ad, kisaad, erisimkodu, birimref) FROM stdin;
1	\N	Ana Komisyon	Kom	1	1
6	\N	BMO Oda Genel Kurulu			3
\.


--
-- TOC entry 2973 (class 0 OID 37509)
-- Dependencies: 192
-- Data for Name: komisyondonem; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY komisyondonem (rid, komisyonref, donemref) FROM stdin;
3	1	3
6	6	3
\.


--
-- TOC entry 3022 (class 0 OID 40782)
-- Dependencies: 295
-- Data for Name: komisyondonemtoplanti; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY komisyondonemtoplanti (rid, komisyondonemref, tarih, toplantituru) FROM stdin;
1	6	2013-07-24	1
\.


--
-- TOC entry 3024 (class 0 OID 40816)
-- Dependencies: 299
-- Data for Name: komisyondonemtoplantitutanak; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY komisyondonemtoplantitutanak (rid, kmsyndonemtoplantiref, tarih, yukleyenref, path) FROM stdin;
\.


--
-- TOC entry 2974 (class 0 OID 37514)
-- Dependencies: 194
-- Data for Name: komisyondonemuye; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY komisyondonemuye (rid, komisyondonemref, uyeref, durum, komisyongorev, uyeturu, kisiref) FROM stdin;
3	6	30	1	2	1	1
9	3	30	1	3	1	\N
\.


--
-- TOC entry 2975 (class 0 OID 37519)
-- Dependencies: 196
-- Data for Name: koy; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY koy (rid, turu, ad, ilceref) FROM stdin;
\.


--
-- TOC entry 2976 (class 0 OID 37522)
-- Dependencies: 197
-- Data for Name: kullanici; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY kullanici (rid, name, parole, password, theme, kisiref) FROM stdin;
3	adnan	4QrcOUm6Wau+VuBX8g+IPg==	4QrcOUm6Wau+VuBX8g+IPg==	bootstrap	2
1	ufuk	4QrcOUm6Wau+VuBX8g+IPg==	4QrcOUm6Wau+VuBX8g+IPg==	bootstrap	1
\.


--
-- TOC entry 2977 (class 0 OID 37529)
-- Dependencies: 198
-- Data for Name: kullanicirole; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY kullanicirole (rid, roleref, active, kullaniciref) FROM stdin;
12	1	1	3
13	1	1	1
14	2	1	3
15	2	1	1
\.


--
-- TOC entry 2978 (class 0 OID 37532)
-- Dependencies: 199
-- Data for Name: kurul; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY kurul (rid, ad, birimref, kurulturu) FROM stdin;
3	Genel Kurul	1	1
7	Onur Kurulu	1	3
8	Denetleme Kurulu	1	4
9	Danışma Kurulu	1	5
10	Koordinasyon Kurulu	1	6
6	Oda Yönetim Kurulu	1	2
\.


--
-- TOC entry 2979 (class 0 OID 37537)
-- Dependencies: 201
-- Data for Name: kuruldonem; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY kuruldonem (rid, kurulref, donemref) FROM stdin;
3	3	3
8	9	3
9	8	3
11	10	3
12	7	3
13	6	3
22	6	10
\.


--
-- TOC entry 3025 (class 0 OID 40844)
-- Dependencies: 301
-- Data for Name: kuruldonemdelege; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY kuruldonemdelege (rid, kuruldonemref, uyeref) FROM stdin;
1	3	30
\.


--
-- TOC entry 3021 (class 0 OID 40769)
-- Dependencies: 293
-- Data for Name: kuruldonemtoplanti; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY kuruldonemtoplanti (rid, kuruldonemref, tarih, toplantituru) FROM stdin;
1	3	2013-07-10	1
\.


--
-- TOC entry 3023 (class 0 OID 40795)
-- Dependencies: 297
-- Data for Name: kuruldonemtoplantitutanak; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY kuruldonemtoplantitutanak (rid, kuruldonemtoplantiref, tarih, yukleyenref, path) FROM stdin;
4	1	2013-07-13	3	e:\\Upload\\Toys\\KurulDonemToplantiTutanak\\Akademik Bilgi Sistemi_1373798004000.xlsx
\.


--
-- TOC entry 2980 (class 0 OID 37542)
-- Dependencies: 203
-- Data for Name: kuruldonemuye; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY kuruldonemuye (rid, kuruldonemref, gorevturu, uyeturu, aktif, uyeref) FROM stdin;
17	22	1	1	1	30
\.


--
-- TOC entry 2981 (class 0 OID 37547)
-- Dependencies: 205
-- Data for Name: kurulgorev; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY kurulgorev (rid, tanim, gorevturu) FROM stdin;
5	Kurul Başkanı	1
6	Yazman	2
\.


--
-- TOC entry 2982 (class 0 OID 37552)
-- Dependencies: 207
-- Data for Name: kurum; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY kurum (rid, ad, vergidairesi, vergino, sorumluref, aciklama, kurumtururef, sehirref, ilceref) FROM stdin;
1	Test Kurumu 1	Yeğenbey	4324235	1	test	\N	\N	\N
4	Oracle	Çankaya	454343645	\N		1	\N	\N
6	Wizard Kurumu			\N		1	\N	\N
9	arf			\N		1	\N	\N
10	IBM			\N		1	\N	\N
11	Innova			\N		1	\N	\N
\.


--
-- TOC entry 2983 (class 0 OID 37557)
-- Dependencies: 209
-- Data for Name: kurumhesap; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY kurumhesap (rid, hesapno, subekodu, ibannumarasi, gecerli, kurumref, bankaadi) FROM stdin;
4	214235	232435243	342234234	1	9	Banka adı
\.


--
-- TOC entry 2984 (class 0 OID 37562)
-- Dependencies: 211
-- Data for Name: kurumkisi; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY kurumkisi (rid, gecerli, kurumref, kisiref, uyelikdurumu) FROM stdin;
4	1	9	2	1
12	1	\N	89	1
\.


--
-- TOC entry 2985 (class 0 OID 37567)
-- Dependencies: 213
-- Data for Name: kurumturu; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY kurumturu (rid, ad) FROM stdin;
1	KAMU
2	ÖZEL
7	SERBEST MÜHENDİSLİK BÜROSU(SMB)
8	ÜNİVERSİTE
9	DEMOKRATİK KİTLE ÖRGÜT(DKÖ)
\.


--
-- TOC entry 2986 (class 0 OID 37572)
-- Dependencies: 215
-- Data for Name: menuitem; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY menuitem (rid, ustref, ad, kod, url, erisimkodu) FROM stdin;
132	3	Sistem Parametreleri	menu_SistemParametre	sistemParametre.do	1.3.132
137	3	Üniversiteler	menu_Universite	universite.do	1.3.137
138	137	Fakülteler	menu_Fakulte	fakulte.do	1.3.137.138
139	138	Bölümler	menu_Bolum	bolum.do	1.3.137.138.139
128	74	Kurum Adresleri	menu_Adres_Kurum	adres.do	1.140.74.128
130	74	Kurum Telefonları	menu_Telefon_Kurum	telefon.do	1.140.74.130
127	3	Kurum Türleri	menu_KurumTuru	kurumTuru.do	1.3.127
125	124	Ödemeler	menu_Odeme	odeme.do	1.124.125
124	1	Finansal İşlemler	menu_FinansBaslik	finansalIslemler.do	1.124
144	1	Kullanıcı İşlemleri	menu_KullaniciBaslik	no.do	1.144
13	144	Roller	menu_Role	role.do	1.144.13
148	3	Kurul Görevleri	menu_KurulGorev	kurulGorev.do	1.3.148
9	3	Menü Elemanları	menu_Menuitem	menuitem.do	1.3.9
1	\N	Anasayfa	homePage	dashboard.do	1
11	144	Kullanıcılar	menu_Kullanici	kullanici.do	1.144.11
15	11	Kullanicinin Rolleri	menu_KullaniciRole	kullanicirole.do	1.144.11.15
16	15	Kullanici Islevleri	menu_IslemKullanici	islemkullanici.do	1.144.11.15.16
94	3	Ödül Türleri	menu_Odultip	odultip.do	1.3.94
12	144	İşlevler	menu_Islem	islem.do	1.144.12
14	13	Rolün İşlevleri	menu_IslemRole	islemrole.do	1.144.13.14
69	3	Ülkeler	menu_Ulke	ulke.do	1.3.69
8	69	Şehirler	menu_Sehir	sehir.do	1.3.69.8
7	8	İlçeler	menu_Ilce	ilce.do	1.3.69.8.7
6	7	Mahalleler	menu_Koy	koy.do	1.3.69.8.7.6
70	3	Görevler	menu_Gorev	gorev.do	1.3.70
91	3	Belge Tipleri	menu_Belgetip	belgetip.do	1.3.91
92	3	Ceza Tipleri	menu_CezaTip	cezatip.do	1.3.92
93	3	Ödeme Tipleri	menu_OdemeTip	odemetip.do	1.3.93
96	3	Uzmanlık Tipleri	menu_Uzmanliktip	uzmanliktip.do	1.3.96
89	76	Üye Abonelikleri	menu_Uyeabone	uyeabone.do	1.157.76.89
73	141	Evrak Listesi 	menu_Evrak	evrak.do	1.141.73
78	76	Üye Aidatları	menu_Uyeaidat	uyeaidat.do	1.157.76.78
81	76	Üye Belgeleri	menu_Uyebelge	uyebelge.do	1.157.76.81
122	76	Üye Borçları	menu_Borc	borc.do	1.157.76.122
87	76	Üye Cezaları	menu_Uyeceza	uyeceza.do	1.157.76.87
134	76	Üye Detayı	menu_UyeDetay	uye_Detay.do	1.157.76.134
79	78	Üye Muafiyet	menu_Uyemuafiyet	uyemuafiyet.do	1.157.76.78.79
83	76	Üye Uzmanlıkları	menu_Uyeuzmanlik	uyeuzmanlik.do	1.157.76.83
80	78	Üye Ödemeleri	menu_Uyeodeme	uyeodeme.do	1.157.76.78.80
88	76	Üye Ödülleri	menu_Uyeodul	uyeodul.do	1.157.76.88
86	76	Üye Öğrenim	menu_Uyeogrenim	uyeogrenim.do	1.157.76.86
115	113	İnternet Adresleri	menu_Internetadres_Komisyon	internetadres.do	1.166.113.115
117	113	Telefon	menu_Telefon_Komisyon	telefon.do	1.166.113.117
116	113	Adres	menu_Adres_Komisyon	adres.do	1.166.113.116
76	157	Üye Listesi 	menu_Uye	uye.do	1.157.76
3	1	Sistem Yönetimi	menu_TanimlamaBaslik	/_page/form/tanimlamalar/tanimlamalarMenu.xhtml	1.3
10	166	Personel	menu_Personel	personel.do	1.166.10
75	166	Aidatlar	menu_Aidat	aidat.do	1.166.75
103	2	İnternet Adresi	menu_Internetadres_Birim	internetadres.do	1.166.2.103
120	74	Kurum Hesap	menu_KurumHesap	kurumHesap.do	1.162.74.120
74	162	Kurum Listesi	menu_Kurum	kurum.do	1.162.74
72	162	Kişi Listesi	menu_Kisi	kisi.do	1.162.72
150	1	Raporlama 	menu_RaporBaslik	no.do	1.150
142	166	Kurullar	menu_Kurul	kurul.do	1.166.142
2	166	Birimler	menu_Birim	birim.do	1.166.2
113	166	Komisyonlar	menu_Komisyon	komisyon.do	1.166.113
112	10	Personel Birim Görevleri	menu_PersonelBirimGorev	personelBirimGorev.do	1.166.10.112
104	2	Adres	menu_Adres_Birim	adres.do	1.166.2.104
133	3	Sorun Bildirimleri	menu_Sorun	sorun.do	1.3.133
131	72	Kişi Detayı	menu_KisiDetay	kisi_Detay.do	1.162.72.131
107	72	Kişi Fotoğrafları	menu_Kisifotograf	kisifotograf.do	1.162.72.107
108	72	Kişi Kimlik Bilgileri	menu_Kisikimlik	kisikimlik.do	1.162.72.108
109	72	Kişi Uyruk Bilgileri	menu_Kisiuyruk	kisiuyruk.do	1.162.72.109
99	72	Adres	menu_Adres	adres.do	1.166.72.99
110	74	Kurum Üyeleri	menu_UyeKurum	uyeKurum.do	1.162.74.110
129	74	Kurum İnternet Adresleri	menu_Internetadres_Kurum	internetadres.do	1.162.74.129
98	72	İnternet Adresleri	menu_Internetadres	internetadres.do	1.162.72.98
97	173	Yayınlar	menu_Yayin	yayin.do	1.173.97
157	1	Üye Yönetimi	menu_UyeYonetimi	no.do	1.157
158	157	Üye Yeni Kayıt Sihirbazı	menu_UyeWizard	uye_Wizard.do	1.157.158
84	76	Üye Bilirkişilikleri	menu_Uyebilirkisi	uyebilirkisi.do	1.157.76.84
141	1	Evrak İşlemleri	menu_EvrakBaslik	no.do	1.141
160	141	Giden Evrak 	menu_GidenEvrak	gidenEvrak.do	1.141.160
161	141	Kararlar	menu_Karar	karar.do	1.141.161
162	1	Kişi / Kurum Yönetimi	menu_KisiKurumYonetimi	no.do	1.162
143	182	Dönemler	menu_Donem	donem.do	1.166.182.143
145	143	Kurul Dönemleri	menu_KurulDonem	kurulDonem.do	1.166.182.143.145
147	145	Kurul Dönem Üyeleri	menu_KurulDonemUye	kurulDonemUye.do	1.166.182.143.145.147
146	143	Komisyon Dönemleri	menu_KomisyonDonem	komisyonDonem.do	1.166.182.143.146
118	2	Birim Hesapları	menu_BirimHesap	birimHesap.do	1.166.2.118
155	74	Kurum Detay	menu_KurumDetay	kurum_Detay.do	1.162.74.155
153	74	Kurum Kişi	menu_KurumKisi	kurumKisi.do	1.162.74.153
156	74	İş Yeri Temsilcileri 	menu_IsYeriTemsilcileri	isYeriTemsilcileri.do	1.162.74.156
163	150	Üye Raporlama	menu_UyeRaporlama	uyeRaporlama.do	1.150.163
164	150	Kurum Raporlama	menu_KurumRaporlama	kurumRaporlama.do	1.150.164
165	150	Hazır Raporlar 	menu_HazırRaporlar	hazirRaporlar.do	1.150.165
166	1	Oda Yönetimi	menu_OdaYonetimi	no.do	1.166
152	10	Personel Detay 	menu_PersonelDetay	personel_Detay.do	1.166.10.152
105	2	Telefon	menu_Telefon_Birim	telefon.do	1.166.2.105
168	1	İletişim	menu_Iletisim	no.do	1.168
169	168	E-posta Gönder	menu_EpostaGonder	epostaGonder.do	1.168.169
170	168	SMS Gönder	menu_SmsGonder	smsGonder.do	1.168.170
171	1	Etkinlik	menu_Etkinlik	no.do	1.171
172	1	Eğitim	menu_Egitim	no.do	1.172
173	1	Yayın ve Kütüphane	menu_YayinVeKutuphane	no.do	1.173
106	72	Kişi İzinleri	menu_Kisiizin	kisiizin.do	1.162.72.106
100	72	Telefon	menu_Telefon	telefon.do	1.166.72.100
159	141	Gelen Evrak	menu_GelenEvrak	evrak.do	1.141.159
175	141	Standart Dosya Kodları	menu_Dosyakodu	dosyakodu.do	1.141.175
177	159	Evrak Detayı	menu_EvrakDetay	evrakDetay.do	1.141.159.177
179	76	Üye Durum Değişikliği	menu_Uyedurumdegistirme	uyedurumdegistirme.do	1.157.76.179
180	157	Üye Başvuru	menu_UyeBasvuru	uyeBasvuru.do	1.157.180
181	180	Üye Başvueru Girişi	menu_UyeBasvuruEdit	uyeBasvuru_Edit.do	1.157.180.181
182	166	Ana Dönemler	menu_Anadonem	anadonem.do	1.166.182
167	146	Komisyon Dönem Üye	menu_KomisyonDonemUye	komisyonDonemUye.do	1.166.182.143.146.167
184	145	Kurul Dönem Toplantıları	menu_KurulDonemToplanti	kurulDonemToplanti.do	1.166.182.143.145.184
185	184	Toplantı Tutanakları	menu_KurulDonemToplantiTutanak	kurulDonemToplantiTutanak.do	1.166.182.143.145.184.185
186	146	Komisyon Dönem Toplantıları	menu_KomisyonDonemToplanti	komisyonDonemToplanti.do	1.166.182.143.146.186
187	186	Komisyon Dönem Toplantı Tutanakları	menu_KmsynDonemToplantiTutanak	komisyonDonemToplantiTutanak.do	1.166.182.143.146.186.187
188	145	Kurul Dönem Delegeleri	menu_KurulDonemDelege	kurulDonemDelege.do	1.166.182.143.145.188
\.


--
-- TOC entry 2987 (class 0 OID 37580)
-- Dependencies: 217
-- Data for Name: odeme; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY odeme (rid, kisiref, miktar, tarih, odemeturu) FROM stdin;
4	72	70.0000000000	2013-06-12	1
7	89	50.0000000000	2013-07-19	1
\.


--
-- TOC entry 2988 (class 0 OID 37585)
-- Dependencies: 219
-- Data for Name: odemetip; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY odemetip (rid, kod, ad, miktar, baslangictarih, bitistarih) FROM stdin;
1	001	Aylık Ödeme	100.5000000000	2013-04-02	2014-02-01
\.


--
-- TOC entry 2989 (class 0 OID 37590)
-- Dependencies: 221
-- Data for Name: odultip; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY odultip (rid, kisaad, ad, aciklama) FROM stdin;
1	PÖ	Para Ödülü	
\.


--
-- TOC entry 2990 (class 0 OID 37595)
-- Dependencies: 223
-- Data for Name: ogrenimkurum; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY ogrenimkurum (rid, kisaad, ad, aciklama) FROM stdin;
1	TED Ankara	Ankara TED Koleji	
\.


--
-- TOC entry 2991 (class 0 OID 37600)
-- Dependencies: 225
-- Data for Name: personel; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY personel (rid, sicilno, baslamatarih, bitistarih, durum, kisiref, birimref) FROM stdin;
3	9999999	2013-01-01	2013-05-02	1	1	4
1	9999990	2013-01-01	\N	4	2	3
74	\N	2013-07-10	\N	1	4	4
75	\N	2013-07-08	\N	1	50	3
\.


--
-- TOC entry 2992 (class 0 OID 37605)
-- Dependencies: 227
-- Data for Name: personelbirimgorev; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY personelbirimgorev (rid, personelref, gorevref, birimref, baslangictarih, bitistarih, aciklama, durum) FROM stdin;
29	3	1	4	2013-06-18	2013-06-18	Açıklamaa	1
31	3	1	3	2013-06-20	2013-06-12	Açıklama	1
\.


--
-- TOC entry 2993 (class 0 OID 37614)
-- Dependencies: 229
-- Data for Name: role; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY role (rid, name, active) FROM stdin;
1	Sistem Kullanıcıları	1
2	Normal Kullanıcılar	1
\.


--
-- TOC entry 2954 (class 0 OID 37393)
-- Dependencies: 157
-- Data for Name: sehir; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY sehir (rid, ad, ulkeref, plakakodu) FROM stdin;
1	ADANA	1	01
2	ADIYAMAN	1	02
3	AFYONKARAHİSAR	1	03
4	AĞRI	1	04
5	AMASYA	1	05
6	ANKARA	1	06
7	ANTALYA	1	07
8	ARTVİN	1	08
9	AYDIN	1	09
20	KARABÜK	1	78
10	BALIKESİR	1	10
11	BİLECİK	1	11
12	BİNGÖL	1	12
13	BİTLİS	1	13
14	BOLU	1	14
15	BURDUR	1	15
16	BURSA	1	16
17	ÇANAKKALE	1	17
18	ÇANKIRI	1	18
19	ÇORUM	1	19
21	AKSARAY	1	68
22	ARDAHAN	1	75
23	BARTIN	1	74
24	BATMAN	1	72
25	BAYBURT	1	69
26	DENİZLİ	1	20
27	DİYARBAKIR	1	21
28	DÜZCE	1	81
29	EDİRNE	1	22
30	ELAZIĞ	1	23
31	ERZİNCAN	1	24
32	ERZURUM	1	25
33	ESKİŞEHİR	1	26
34	GAZİANTEP	1	27
35	GİRESUN	1	28
36	GÜMÜŞHANE	1	29
37	HAKKARİ	1	30
38	HATAY	1	31
39	IĞDIR	1	76
40	ISPARTA	1	32
41	İSTANBUL	1	34
42	İZMİR	1	35
43	KAHRAMANMARAŞ	1	46
44	KARAMAN	1	70
45	KARS	1	36
46	KASTAMONU	1	37
47	KAYSERİ	1	38
48	KIRIKKALE	1	71
49	KIRKLARELİ	1	39
50	KIRŞEHİR	1	40
51	KİLİS	1	79
52	KOCAELİ	1	41
53	KONYA	1	42
54	KÜTAHYA	1	43
55	MALATYA	1	44
56	MANİSA	1	45
57	MARDİN	1	47
58	MERSİN	1	33
59	MUĞLA	1	48
60	MUŞ	1	49
61	NEVŞEHİR	1	50
62	NİĞDE	1	51
63	ORDU	1	52
64	OSMANİYE	1	80
65	RİZE	1	53
66	SAKARYA	1	54
67	SAMSUN	1	55
68	SİİRT	1	56
69	SİNOP	1	57
70	SİVAS	1	58
71	ŞANLIURFA	1	63
72	ŞIRNAK	1	73
73	TEKİRDAĞ	1	59
74	TOKAT	1	60
75	TRABZON	1	61
76	TUNCELİ	1	62
77	UŞAK	1	64
78	VAN	1	65
79	YALOVA	1	77
80	YOZGAT	1	66
81	ZONGULDAK	1	67
\.


--
-- TOC entry 2994 (class 0 OID 37619)
-- Dependencies: 231
-- Data for Name: sistemParametre; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY sistemParametre (rid, listeboyutu) FROM stdin;
1	25
\.


--
-- TOC entry 3033 (class 0 OID 45102)
-- Dependencies: 317
-- Data for Name: smsanlikgonderimlog; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY smsanlikgonderimlog (rid, kisiref, icerikref, tarih, gonderenref, durum, aciklama) FROM stdin;
\.


--
-- TOC entry 3031 (class 0 OID 45050)
-- Dependencies: 313
-- Data for Name: smsgonderimlog; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY smsgonderimlog (rid, gonderimref, kisiref, tarih, gonderenref, durum, icerik, aciklama) FROM stdin;
\.


--
-- TOC entry 2995 (class 0 OID 37625)
-- Dependencies: 233
-- Data for Name: sorun; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY sorun (rid, kullaniciref, tarih, onem, baslik, aciklama) FROM stdin;
1	1	2013-05-15	2	test	test
4	1	2013-06-20	3	Başlık Denemesi	Açıklama
\.


--
-- TOC entry 2996 (class 0 OID 37634)
-- Dependencies: 237
-- Data for Name: telefon; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY telefon (rid, kisiref, telefonno, telefonturu, varsayilan, kurumref) FROM stdin;
5	4	05077056990	1	2	\N
62	\N	03125685698	1	1	9
23	1	05052456064	1	1	\N
22	1	03125685699	1	2	\N
72	89	05005050505	3	2	\N
73	89	05005435340	2	2	\N
74	89	05005050505	1	1	\N
75	89	05005340534	4	2	\N
76	89	05055435544	3	2	\N
77	89	05435345345	2	2	\N
78	89	05505050505	1	1	\N
79	89	05345345345	4	2	\N
80	55	05005345345	1	1	\N
81	89	05005050500	1	1	\N
\.


--
-- TOC entry 2955 (class 0 OID 37398)
-- Dependencies: 159
-- Data for Name: ulke; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY ulke (rid, ad, turkkokenli) FROM stdin;
1	TÜRKİYE	1
\.


--
-- TOC entry 2997 (class 0 OID 37648)
-- Dependencies: 243
-- Data for Name: universite; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY universite (rid, webadresi, ad, durum, ulkeref, ilref) FROM stdin;
1	www.atilim.edu.tr	Atılım Üniversitesi	2	1	6
2	www.odtu.edu.tr	Odtü 	1	1	6
\.


--
-- TOC entry 2998 (class 0 OID 37653)
-- Dependencies: 245
-- Data for Name: uyarinot; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY uyarinot (rid, kullaniciref, uyaritip, tarih, gecerliliktarih, uyarimetin, uyaridurum, birimref) FROM stdin;
\.


--
-- TOC entry 2999 (class 0 OID 37658)
-- Dependencies: 247
-- Data for Name: uye; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY uye (rid, kisiref, sicilno, uyetip, uyeliktarih, ayrilmatarih, uyedurum, kayitdurum, onaylayanref, onaytarih, birimref, sinif, ogrencino, ogrenciuyeref, iletisimsms, iletisimemail, iletisimposta) FROM stdin;
30	1	C0000000004	1	2013-06-12	\N	2	3	3	2013-06-14	3	\N		\N	1	2	2
75	89	G0000000001	1	2013-07-29	\N	1	0	\N	\N	4	\N		\N	2	2	2
\.


--
-- TOC entry 3000 (class 0 OID 37666)
-- Dependencies: 249
-- Data for Name: uyeabone; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY uyeabone (rid, uyeref, yayinref, tarih, baslangictarih, bitistarih, uyeabonedurum, aciklama) FROM stdin;
\.


--
-- TOC entry 3001 (class 0 OID 37671)
-- Dependencies: 251
-- Data for Name: uyeaidat; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY uyeaidat (rid, uyeref, miktar, odenen, uyeaidatdurum, yil, ay, uyemuafiyetref) FROM stdin;
390	75	10.0000000000	0.0000000000	1	2013	7	\N
391	75	10.0000000000	0.0000000000	1	2013	8	\N
392	75	10.0000000000	0.0000000000	1	2013	9	\N
393	75	10.0000000000	0.0000000000	1	2013	10	\N
394	75	10.0000000000	0.0000000000	1	2013	11	\N
395	75	10.0000000000	0.0000000000	1	2013	12	\N
\.


--
-- TOC entry 3019 (class 0 OID 38744)
-- Dependencies: 289
-- Data for Name: uyebasvuru; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY uyebasvuru (rid, kimlikno, ad, soyad, babaad, anaad, dogumilceref, dogumyer, dogumtarih, cinsiyet, medenihal, ulkeref, sehirref, ilceref, ciltno, aileno, sirano, ciltaciklama, verildigiyer, verilistarihi, kangrup, verilisnedeni, email, posta, sms, uyetip, baslangictarih, ogrencino, sinif, ogrenimtip, universiteref, fakulteref, bolumref, lisansunvan, diplomano, denklikdurum, bitistarih, aciklama, evtelefonu, istelefonu, gsm, faks, adrestipi, acikadres, binasiteadi, csbm, binablokadi, ickapino, diskapino) FROM stdin;
\.


--
-- TOC entry 3002 (class 0 OID 37676)
-- Dependencies: 253
-- Data for Name: uyebelge; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY uyebelge (rid, uyeref, kurumref, belgetipref, tarih, basvurutarih, hazirlanmatarih, evrakref, teslimtarih, uyebelgedurum, aciklama, path) FROM stdin;
\.


--
-- TOC entry 3003 (class 0 OID 37681)
-- Dependencies: 255
-- Data for Name: uyebilirkisi; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY uyebilirkisi (rid, uyeref, uyeuzmanlikref, evrakref, tarih, bitistarih, uyebilirkisidurum, aciklama) FROM stdin;
\.


--
-- TOC entry 3004 (class 0 OID 37686)
-- Dependencies: 257
-- Data for Name: uyeceza; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY uyeceza (rid, uyeref, cezatipref, evrakref, baslangictarih, bitistarih, aciklama) FROM stdin;
7	30	1	5	2013-07-03	2013-07-25	
\.


--
-- TOC entry 3018 (class 0 OID 38726)
-- Dependencies: 287
-- Data for Name: uyedurumdegistirme; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY uyedurumdegistirme (rid, uyeref, tarih, kayityapan, oncekidurum, yenidurum) FROM stdin;
2	30	2013-11-15	1	3	2
3	30	2013-06-05	1	2	3
4	30	2013-07-02	1	3	2
5	30	2013-12-28	1	2	3
6	30	2013-07-12	1	3	2
\.


--
-- TOC entry 3005 (class 0 OID 37691)
-- Dependencies: 259
-- Data for Name: uyekurum; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY uyekurum (rid, uyeref, kurumref, adresref, gorev, baslangictarih, bitistarih, uyekurumdurum, varsayilan) FROM stdin;
10	30	4	\N		2013-06-19	\N	1	\N
\.


--
-- TOC entry 3006 (class 0 OID 37696)
-- Dependencies: 261
-- Data for Name: uyemuafiyet; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY uyemuafiyet (rid, uyeref, muafiyettip, evrakref, aciklama, baslangictarih, bitistarih) FROM stdin;
101	30	4	\N	-	2013-07-02	\N
103	30	4	\N	-	2013-07-02	\N
104	30	4	\N	-	2013-07-02	\N
105	30	4	\N	-	2013-07-02	\N
106	30	4	\N	-	2013-07-02	\N
107	30	4	\N	-	2013-07-02	\N
108	30	4	\N	-	2013-07-02	\N
\.


--
-- TOC entry 3007 (class 0 OID 37701)
-- Dependencies: 263
-- Data for Name: uyeodeme; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY uyeodeme (rid, uyeaidatref, tarih, miktar, odemetip, muafiyetref) FROM stdin;
\.


--
-- TOC entry 3008 (class 0 OID 37706)
-- Dependencies: 265
-- Data for Name: uyeodul; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY uyeodul (rid, uyeref, odultipref, evrakref, tarih, aciklama) FROM stdin;
\.


--
-- TOC entry 3009 (class 0 OID 37711)
-- Dependencies: 267
-- Data for Name: uyeogrenim; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY uyeogrenim (rid, uyeref, ogrenimtip, aciklama, lisansunvan, diplomano, bitirmetarih, varsayilan, universiteref, fakulteref, bolumref, denklikdurum) FROM stdin;
47	30	1		Lisans Ünvan	5637	2013-06-13	1	1	3	2	1
81	75	1		ds	432423432	2013-07-03	1	1	3	2	2
\.


--
-- TOC entry 3010 (class 0 OID 37719)
-- Dependencies: 269
-- Data for Name: uyesigorta; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY uyesigorta (rid, uyeref, sigortatip, sigortasirket, policeno, baslangictarih, bitistarih, uyesigortadurum, evrakref, aciklama) FROM stdin;
\.


--
-- TOC entry 3011 (class 0 OID 37724)
-- Dependencies: 271
-- Data for Name: uyeuzmanlik; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY uyeuzmanlik (rid, uyeref, uzmanliktipref, tarih, aciklama) FROM stdin;
\.


--
-- TOC entry 3012 (class 0 OID 37729)
-- Dependencies: 273
-- Data for Name: uzmanliktip; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY uzmanliktip (rid, kod, ad, aciklama) FROM stdin;
1	001	Uzmanlık Tipi 1	
\.


--
-- TOC entry 3013 (class 0 OID 37736)
-- Dependencies: 276
-- Data for Name: yayin; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY yayin (rid, ad, yayintip, aciklama) FROM stdin;
1	Yayın 1	1	test
\.


--
-- TOC entry 2610 (class 2606 OID 40863)
-- Dependencies: 194 194 194
-- Name: KOMISYONDONEMUYE_UK; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY komisyondonemuye
    ADD CONSTRAINT "KOMISYONDONEMUYE_UK" UNIQUE (komisyondonemref, uyeref);


--
-- TOC entry 2612 (class 2606 OID 40865)
-- Dependencies: 194 194 194
-- Name: KOMISYONDONEMUYE_UK_2; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY komisyondonemuye
    ADD CONSTRAINT "KOMISYONDONEMUYE_UK_2" UNIQUE (komisyondonemref, kisiref);


--
-- TOC entry 2631 (class 2606 OID 40841)
-- Dependencies: 203 203 203
-- Name: KURULDONEMUYE_UK; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY kuruldonemuye
    ADD CONSTRAINT "KURULDONEMUYE_UK" UNIQUE (kuruldonemref, uyeref);


--
-- TOC entry 2542 (class 2606 OID 37815)
-- Dependencies: 140 140
-- Name: adres_pk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY adres
    ADD CONSTRAINT adres_pk PRIMARY KEY (rid);


--
-- TOC entry 2544 (class 2606 OID 37817)
-- Dependencies: 142 142
-- Name: aidat_pk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY aidat
    ADD CONSTRAINT aidat_pk PRIMARY KEY (rid);


--
-- TOC entry 2719 (class 2606 OID 40701)
-- Dependencies: 291 291
-- Name: anadonem_pk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY anadonem
    ADD CONSTRAINT anadonem_pk PRIMARY KEY (rid);


--
-- TOC entry 2546 (class 2606 OID 37819)
-- Dependencies: 144 144
-- Name: belgetip_pk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY belgetip
    ADD CONSTRAINT belgetip_pk PRIMARY KEY (rid);


--
-- TOC entry 2713 (class 2606 OID 38695)
-- Dependencies: 285 285
-- Name: bilgisayar_pkey; Type: CONSTRAINT; Schema: public; Owner: toys; Tablespace: 
--

ALTER TABLE ONLY bilgisayar
    ADD CONSTRAINT bilgisayar_pkey PRIMARY KEY (rid);


--
-- TOC entry 2550 (class 2606 OID 37821)
-- Dependencies: 147 147
-- Name: birimhesap_pk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY birimhesap
    ADD CONSTRAINT birimhesap_pk PRIMARY KEY (rid);


--
-- TOC entry 2552 (class 2606 OID 37823)
-- Dependencies: 149 149
-- Name: bolum_pk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY bolum
    ADD CONSTRAINT bolum_pk PRIMARY KEY (rid);


--
-- TOC entry 2554 (class 2606 OID 37825)
-- Dependencies: 151 151
-- Name: borc_pk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY borc
    ADD CONSTRAINT borc_pk PRIMARY KEY (rid);


--
-- TOC entry 2556 (class 2606 OID 37827)
-- Dependencies: 153 153
-- Name: borcodeme_pk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY borcodeme
    ADD CONSTRAINT borcodeme_pk PRIMARY KEY (rid);


--
-- TOC entry 2558 (class 2606 OID 37829)
-- Dependencies: 155 155
-- Name: cezatip_pk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY cezatip
    ADD CONSTRAINT cezatip_pk PRIMARY KEY (rid);


--
-- TOC entry 2563 (class 2606 OID 37831)
-- Dependencies: 159 159
-- Name: country_pk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY ulke
    ADD CONSTRAINT country_pk PRIMARY KEY (rid);


--
-- TOC entry 2567 (class 2606 OID 37833)
-- Dependencies: 163 163
-- Name: donem_pk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY donem
    ADD CONSTRAINT donem_pk PRIMARY KEY (rid);


--
-- TOC entry 2707 (class 2606 OID 38603)
-- Dependencies: 279 279
-- Name: dosyakodu_pk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY dosyakodu
    ADD CONSTRAINT dosyakodu_pk PRIMARY KEY (rid);


--
-- TOC entry 2569 (class 2606 OID 37835)
-- Dependencies: 165 165
-- Name: duyuru_pk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY duyuru
    ADD CONSTRAINT duyuru_pk PRIMARY KEY (rid);


--
-- TOC entry 2763 (class 2606 OID 45273)
-- Dependencies: 335 335
-- Name: egitim_pk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY egitim
    ADD CONSTRAINT egitim_pk PRIMARY KEY (rid);


--
-- TOC entry 2773 (class 2606 OID 45383)
-- Dependencies: 345 345
-- Name: egitimdegerlendirme_pk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY egitimdegerlendirme
    ADD CONSTRAINT egitimdegerlendirme_pk PRIMARY KEY (rid);


--
-- TOC entry 2767 (class 2606 OID 45325)
-- Dependencies: 339 339
-- Name: egitimders_pk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY egitimders
    ADD CONSTRAINT egitimders_pk PRIMARY KEY (rid);


--
-- TOC entry 2769 (class 2606 OID 45346)
-- Dependencies: 341 341
-- Name: egitimdosya_pk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY egitimdosya
    ADD CONSTRAINT egitimdosya_pk PRIMARY KEY (rid);


--
-- TOC entry 2771 (class 2606 OID 45362)
-- Dependencies: 343 343
-- Name: egitimkatilimci_pk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY egitimkatilimci
    ADD CONSTRAINT egitimkatilimci_pk PRIMARY KEY (rid);


--
-- TOC entry 2765 (class 2606 OID 45304)
-- Dependencies: 337 337
-- Name: egitimogretmen_pk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY egitimogretmen
    ADD CONSTRAINT egitimogretmen_pk PRIMARY KEY (rid);


--
-- TOC entry 2761 (class 2606 OID 45262)
-- Dependencies: 333 333
-- Name: egitimtanim_pk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY egitimtanim
    ADD CONSTRAINT egitimtanim_pk PRIMARY KEY (rid);


--
-- TOC entry 2743 (class 2606 OID 45084)
-- Dependencies: 315 315
-- Name: epostaanlikgonderimlog_pk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY epostaanlikgonderimlog
    ADD CONSTRAINT epostaanlikgonderimlog_pk PRIMARY KEY (rid);


--
-- TOC entry 2739 (class 2606 OID 45032)
-- Dependencies: 311 311
-- Name: epostagonderimlog_pk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY epostagonderimlog
    ADD CONSTRAINT epostagonderimlog_pk PRIMARY KEY (rid);


--
-- TOC entry 2749 (class 2606 OID 45148)
-- Dependencies: 321 321
-- Name: etkinlik_pk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY etkinlik
    ADD CONSTRAINT etkinlik_pk PRIMARY KEY (rid);


--
-- TOC entry 2759 (class 2606 OID 45246)
-- Dependencies: 331 331
-- Name: etkinlikdosya_pk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY etkinlikdosya
    ADD CONSTRAINT etkinlikdosya_pk PRIMARY KEY (rid);


--
-- TOC entry 2751 (class 2606 OID 45176)
-- Dependencies: 323 323
-- Name: etkinlikkatilimci_pk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY etkinlikkatilimci
    ADD CONSTRAINT etkinlikkatilimci_pk PRIMARY KEY (rid);


--
-- TOC entry 2755 (class 2606 OID 45212)
-- Dependencies: 327 327
-- Name: etkinlikkurul_pk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY etkinlikkurul
    ADD CONSTRAINT etkinlikkurul_pk PRIMARY KEY (rid);


--
-- TOC entry 2757 (class 2606 OID 45225)
-- Dependencies: 329 329
-- Name: etkinlikkuruluye_pk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY etkinlikkuruluye
    ADD CONSTRAINT etkinlikkuruluye_pk PRIMARY KEY (rid);


--
-- TOC entry 2753 (class 2606 OID 45194)
-- Dependencies: 325 325
-- Name: etkinlikkurum_pk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY etkinlikkurum
    ADD CONSTRAINT etkinlikkurum_pk PRIMARY KEY (rid);


--
-- TOC entry 2747 (class 2606 OID 45137)
-- Dependencies: 319 319
-- Name: etkinliktanim_pk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY etkinliktanim
    ADD CONSTRAINT etkinliktanim_pk PRIMARY KEY (rid);


--
-- TOC entry 2709 (class 2606 OID 38621)
-- Dependencies: 281 281
-- Name: evrak_pk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY evrak
    ADD CONSTRAINT evrak_pk PRIMARY KEY (rid);


--
-- TOC entry 2711 (class 2606 OID 38682)
-- Dependencies: 283 283
-- Name: evrakek_pk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY evrakek
    ADD CONSTRAINT evrakek_pk PRIMARY KEY (rid);


--
-- TOC entry 2571 (class 2606 OID 37839)
-- Dependencies: 167 167
-- Name: fakulte_pk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY fakulte
    ADD CONSTRAINT fakulte_pk PRIMARY KEY (rid);


--
-- TOC entry 2573 (class 2606 OID 37841)
-- Dependencies: 169 169
-- Name: gorev_pk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY gorev
    ADD CONSTRAINT gorev_pk PRIMARY KEY (rid);


--
-- TOC entry 2735 (class 2606 OID 44998)
-- Dependencies: 307 307
-- Name: icerik_pk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY icerik
    ADD CONSTRAINT icerik_pk PRIMARY KEY (rid);


--
-- TOC entry 2731 (class 2606 OID 44964)
-- Dependencies: 303 303
-- Name: iletisimliste_pk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY iletisimliste
    ADD CONSTRAINT iletisimliste_pk PRIMARY KEY (rid);


--
-- TOC entry 2733 (class 2606 OID 44982)
-- Dependencies: 305 305
-- Name: iletisimlistedetay_pk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY iletisimlistedetay
    ADD CONSTRAINT iletisimlistedetay_pk PRIMARY KEY (rid);


--
-- TOC entry 2575 (class 2606 OID 37843)
-- Dependencies: 171 171
-- Name: internetadres_pk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY internetadres
    ADD CONSTRAINT internetadres_pk PRIMARY KEY (rid);


--
-- TOC entry 2581 (class 2606 OID 37845)
-- Dependencies: 174 174 174
-- Name: islemkullanici_uk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY islemkullanici
    ADD CONSTRAINT islemkullanici_uk UNIQUE (islemref, kullaniciroleref);


--
-- TOC entry 2586 (class 2606 OID 37847)
-- Dependencies: 175 175
-- Name: islemlog_pk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY islemlog
    ADD CONSTRAINT islemlog_pk PRIMARY KEY (rid);


--
-- TOC entry 2590 (class 2606 OID 37849)
-- Dependencies: 178 178
-- Name: isyeritemsilcileri_pk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY isyeritemsilcileri
    ADD CONSTRAINT isyeritemsilcileri_pk PRIMARY KEY (rid);


--
-- TOC entry 2593 (class 2606 OID 37851)
-- Dependencies: 180 180
-- Name: kisi_pk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY kisi
    ADD CONSTRAINT kisi_pk PRIMARY KEY (rid);


--
-- TOC entry 2595 (class 2606 OID 37853)
-- Dependencies: 182 182
-- Name: kisifotograf_pk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY kisifotograf
    ADD CONSTRAINT kisifotograf_pk PRIMARY KEY (rid);


--
-- TOC entry 2597 (class 2606 OID 37855)
-- Dependencies: 184 184
-- Name: kisiizin_pk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY kisiizin
    ADD CONSTRAINT kisiizin_pk PRIMARY KEY (rid);


--
-- TOC entry 2599 (class 2606 OID 37857)
-- Dependencies: 186 186
-- Name: kisikimlik_pk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY kisikimlik
    ADD CONSTRAINT kisikimlik_pk PRIMARY KEY (rid);


--
-- TOC entry 2601 (class 2606 OID 37859)
-- Dependencies: 188 188
-- Name: kisiuyruk_pk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY kisiuyruk
    ADD CONSTRAINT kisiuyruk_pk PRIMARY KEY (rid);


--
-- TOC entry 2604 (class 2606 OID 37861)
-- Dependencies: 190 190
-- Name: komisyon_pk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY komisyon
    ADD CONSTRAINT komisyon_pk PRIMARY KEY (rid);


--
-- TOC entry 2606 (class 2606 OID 37863)
-- Dependencies: 192 192
-- Name: komisyondonem_pk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY komisyondonem
    ADD CONSTRAINT komisyondonem_pk PRIMARY KEY (rid);


--
-- TOC entry 2608 (class 2606 OID 40760)
-- Dependencies: 192 192 192
-- Name: komisyondonem_uk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY komisyondonem
    ADD CONSTRAINT komisyondonem_uk UNIQUE (komisyonref, donemref);


--
-- TOC entry 2723 (class 2606 OID 40787)
-- Dependencies: 295 295
-- Name: komisyondonemtoplanti_pk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY komisyondonemtoplanti
    ADD CONSTRAINT komisyondonemtoplanti_pk PRIMARY KEY (rid);


--
-- TOC entry 2727 (class 2606 OID 40824)
-- Dependencies: 299 299
-- Name: komisyondonemtoplantitutanak_pk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY komisyondonemtoplantitutanak
    ADD CONSTRAINT komisyondonemtoplantitutanak_pk PRIMARY KEY (rid);


--
-- TOC entry 2614 (class 2606 OID 37865)
-- Dependencies: 194 194
-- Name: komisyondonemuye_pk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY komisyondonemuye
    ADD CONSTRAINT komisyondonemuye_pk PRIMARY KEY (rid);


--
-- TOC entry 2623 (class 2606 OID 37867)
-- Dependencies: 199 199
-- Name: kurul_pk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY kurul
    ADD CONSTRAINT kurul_pk PRIMARY KEY (rid);


--
-- TOC entry 2625 (class 2606 OID 40762)
-- Dependencies: 199 199 199
-- Name: kurul_uk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY kurul
    ADD CONSTRAINT kurul_uk UNIQUE (birimref, kurulturu);


--
-- TOC entry 2627 (class 2606 OID 37869)
-- Dependencies: 201 201
-- Name: kuruldonem_pk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY kuruldonem
    ADD CONSTRAINT kuruldonem_pk PRIMARY KEY (rid);


--
-- TOC entry 2629 (class 2606 OID 40758)
-- Dependencies: 201 201 201
-- Name: kuruldonem_uk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY kuruldonem
    ADD CONSTRAINT kuruldonem_uk UNIQUE (kurulref, donemref);


--
-- TOC entry 2729 (class 2606 OID 40849)
-- Dependencies: 301 301
-- Name: kuruldonemdelege_pk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY kuruldonemdelege
    ADD CONSTRAINT kuruldonemdelege_pk PRIMARY KEY (rid);


--
-- TOC entry 2721 (class 2606 OID 40774)
-- Dependencies: 293 293
-- Name: kuruldonemtoplanti_pk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY kuruldonemtoplanti
    ADD CONSTRAINT kuruldonemtoplanti_pk PRIMARY KEY (rid);


--
-- TOC entry 2725 (class 2606 OID 40803)
-- Dependencies: 297 297
-- Name: kuruldonemtoplantitutanak_pk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY kuruldonemtoplantitutanak
    ADD CONSTRAINT kuruldonemtoplantitutanak_pk PRIMARY KEY (rid);


--
-- TOC entry 2633 (class 2606 OID 37871)
-- Dependencies: 203 203
-- Name: kuruldonemuye_pk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY kuruldonemuye
    ADD CONSTRAINT kuruldonemuye_pk PRIMARY KEY (rid);


--
-- TOC entry 2635 (class 2606 OID 37873)
-- Dependencies: 205 205
-- Name: kurulgorev_pk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY kurulgorev
    ADD CONSTRAINT kurulgorev_pk PRIMARY KEY (rid);


--
-- TOC entry 2638 (class 2606 OID 37875)
-- Dependencies: 207 207
-- Name: kurum_pk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY kurum
    ADD CONSTRAINT kurum_pk PRIMARY KEY (rid);


--
-- TOC entry 2640 (class 2606 OID 37877)
-- Dependencies: 209 209
-- Name: kurumhesap_pk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY kurumhesap
    ADD CONSTRAINT kurumhesap_pk PRIMARY KEY (rid);


--
-- TOC entry 2642 (class 2606 OID 37879)
-- Dependencies: 211 211
-- Name: kurumkisi_pk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY kurumkisi
    ADD CONSTRAINT kurumkisi_pk PRIMARY KEY (rid);


--
-- TOC entry 2644 (class 2606 OID 37881)
-- Dependencies: 213 213
-- Name: kurumturu_pk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY kurumturu
    ADD CONSTRAINT kurumturu_pk PRIMARY KEY (rid);


--
-- TOC entry 2646 (class 2606 OID 37883)
-- Dependencies: 215 215
-- Name: menuitem_pk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY menuitem
    ADD CONSTRAINT menuitem_pk PRIMARY KEY (rid);


--
-- TOC entry 2648 (class 2606 OID 37885)
-- Dependencies: 215 215
-- Name: menuitem_uk_kod; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY menuitem
    ADD CONSTRAINT menuitem_uk_kod UNIQUE (kod);


--
-- TOC entry 2650 (class 2606 OID 37887)
-- Dependencies: 217 217
-- Name: odeme_pk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY odeme
    ADD CONSTRAINT odeme_pk PRIMARY KEY (rid);


--
-- TOC entry 2652 (class 2606 OID 37889)
-- Dependencies: 219 219
-- Name: odemetip_pk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY odemetip
    ADD CONSTRAINT odemetip_pk PRIMARY KEY (rid);


--
-- TOC entry 2654 (class 2606 OID 37891)
-- Dependencies: 221 221
-- Name: odultip_pk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY odultip
    ADD CONSTRAINT odultip_pk PRIMARY KEY (rid);


--
-- TOC entry 2656 (class 2606 OID 37893)
-- Dependencies: 223 223
-- Name: ogrenimkurum_pk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY ogrenimkurum
    ADD CONSTRAINT ogrenimkurum_pk PRIMARY KEY (rid);


--
-- TOC entry 2658 (class 2606 OID 37895)
-- Dependencies: 225 225
-- Name: personel_pk01; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY personel
    ADD CONSTRAINT personel_pk01 PRIMARY KEY (rid);


--
-- TOC entry 2660 (class 2606 OID 37897)
-- Dependencies: 227 227
-- Name: personelbirimgorev_pk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY personelbirimgorev
    ADD CONSTRAINT personelbirimgorev_pk PRIMARY KEY (rid);


--
-- TOC entry 2560 (class 2606 OID 37899)
-- Dependencies: 157 157
-- Name: pk_city; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY sehir
    ADD CONSTRAINT pk_city PRIMARY KEY (rid);


--
-- TOC entry 2565 (class 2606 OID 37901)
-- Dependencies: 161 161
-- Name: pk_district; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY ilce
    ADD CONSTRAINT pk_district PRIMARY KEY (rid);


--
-- TOC entry 2616 (class 2606 OID 37903)
-- Dependencies: 196 196
-- Name: pk_village; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY koy
    ADD CONSTRAINT pk_village PRIMARY KEY (rid);


--
-- TOC entry 2662 (class 2606 OID 37905)
-- Dependencies: 229 229
-- Name: role_pk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY role
    ADD CONSTRAINT role_pk PRIMARY KEY (rid);


--
-- TOC entry 2664 (class 2606 OID 37907)
-- Dependencies: 229 229
-- Name: role_uk_1; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY role
    ADD CONSTRAINT role_uk_1 UNIQUE (name);


--
-- TOC entry 2666 (class 2606 OID 37909)
-- Dependencies: 231 231
-- Name: sistemParametre_pk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY sistemParametre
    ADD CONSTRAINT sistemParametre_pk PRIMARY KEY (rid);


--
-- TOC entry 2745 (class 2606 OID 45110)
-- Dependencies: 317 317
-- Name: smsanlikgonderimlog_pk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY smsanlikgonderimlog
    ADD CONSTRAINT smsanlikgonderimlog_pk PRIMARY KEY (rid);


--
-- TOC entry 2741 (class 2606 OID 45058)
-- Dependencies: 313 313
-- Name: smsgonderimlog_pk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY smsgonderimlog
    ADD CONSTRAINT smsgonderimlog_pk PRIMARY KEY (rid);


--
-- TOC entry 2668 (class 2606 OID 37911)
-- Dependencies: 233 233
-- Name: sorun_pk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY sorun
    ADD CONSTRAINT sorun_pk PRIMARY KEY (rid);


--
-- TOC entry 2619 (class 2606 OID 37913)
-- Dependencies: 197 197
-- Name: systemuser_rid_pk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY kullanici
    ADD CONSTRAINT systemuser_rid_pk PRIMARY KEY (rid);


--
-- TOC entry 2621 (class 2606 OID 37915)
-- Dependencies: 198 198
-- Name: systemuserrole_pk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY kullanicirole
    ADD CONSTRAINT systemuserrole_pk PRIMARY KEY (rid);


--
-- TOC entry 2670 (class 2606 OID 37917)
-- Dependencies: 237 237
-- Name: telefon_pk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY telefon
    ADD CONSTRAINT telefon_pk PRIMARY KEY (rid);


--
-- TOC entry 2577 (class 2606 OID 37919)
-- Dependencies: 173 173
-- Name: transaction_pk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY islem
    ADD CONSTRAINT transaction_pk PRIMARY KEY (rid);


--
-- TOC entry 2579 (class 2606 OID 37921)
-- Dependencies: 173 173
-- Name: transaction_uk_1; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY islem
    ADD CONSTRAINT transaction_uk_1 UNIQUE (name);


--
-- TOC entry 2588 (class 2606 OID 37923)
-- Dependencies: 177 177
-- Name: transactionrole_pk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY islemrole
    ADD CONSTRAINT transactionrole_pk PRIMARY KEY (rid);


--
-- TOC entry 2583 (class 2606 OID 37925)
-- Dependencies: 174 174
-- Name: transactionsystemuser_pk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY islemkullanici
    ADD CONSTRAINT transactionsystemuser_pk PRIMARY KEY (rid);


--
-- TOC entry 2548 (class 2606 OID 37927)
-- Dependencies: 146 146
-- Name: unit_pk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY birim
    ADD CONSTRAINT unit_pk PRIMARY KEY (rid);


--
-- TOC entry 2672 (class 2606 OID 37929)
-- Dependencies: 243 243
-- Name: universite_pk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY universite
    ADD CONSTRAINT universite_pk PRIMARY KEY (rid);


--
-- TOC entry 2674 (class 2606 OID 37931)
-- Dependencies: 245 245
-- Name: uyarinot_pk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY uyarinot
    ADD CONSTRAINT uyarinot_pk PRIMARY KEY (rid);


--
-- TOC entry 2676 (class 2606 OID 37933)
-- Dependencies: 247 247
-- Name: uye_pk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY uye
    ADD CONSTRAINT uye_pk PRIMARY KEY (rid);


--
-- TOC entry 2679 (class 2606 OID 37935)
-- Dependencies: 249 249
-- Name: uyeabone_pk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY uyeabone
    ADD CONSTRAINT uyeabone_pk PRIMARY KEY (rid);


--
-- TOC entry 2681 (class 2606 OID 37937)
-- Dependencies: 251 251
-- Name: uyeaidat_pk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY uyeaidat
    ADD CONSTRAINT uyeaidat_pk PRIMARY KEY (rid);


--
-- TOC entry 2717 (class 2606 OID 38752)
-- Dependencies: 289 289
-- Name: uyebasvuru_pk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY uyebasvuru
    ADD CONSTRAINT uyebasvuru_pk PRIMARY KEY (rid);


--
-- TOC entry 2683 (class 2606 OID 37939)
-- Dependencies: 253 253
-- Name: uyebelge_pk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY uyebelge
    ADD CONSTRAINT uyebelge_pk PRIMARY KEY (rid);


--
-- TOC entry 2685 (class 2606 OID 37941)
-- Dependencies: 255 255
-- Name: uyebilirkisi_pk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY uyebilirkisi
    ADD CONSTRAINT uyebilirkisi_pk PRIMARY KEY (rid);


--
-- TOC entry 2687 (class 2606 OID 37943)
-- Dependencies: 257 257
-- Name: uyeceza_pk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY uyeceza
    ADD CONSTRAINT uyeceza_pk PRIMARY KEY (rid);


--
-- TOC entry 2715 (class 2606 OID 38731)
-- Dependencies: 287 287
-- Name: uyedurumdegistirme_pk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY uyedurumdegistirme
    ADD CONSTRAINT uyedurumdegistirme_pk PRIMARY KEY (rid);


--
-- TOC entry 2689 (class 2606 OID 37945)
-- Dependencies: 259 259
-- Name: uyekurum_pk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY uyekurum
    ADD CONSTRAINT uyekurum_pk PRIMARY KEY (rid);


--
-- TOC entry 2691 (class 2606 OID 37947)
-- Dependencies: 261 261
-- Name: uyemuafiyet_pk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY uyemuafiyet
    ADD CONSTRAINT uyemuafiyet_pk PRIMARY KEY (rid);


--
-- TOC entry 2693 (class 2606 OID 37949)
-- Dependencies: 263 263
-- Name: uyeodeme_pk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY uyeodeme
    ADD CONSTRAINT uyeodeme_pk PRIMARY KEY (rid);


--
-- TOC entry 2695 (class 2606 OID 37951)
-- Dependencies: 265 265
-- Name: uyeodul_pk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY uyeodul
    ADD CONSTRAINT uyeodul_pk PRIMARY KEY (rid);


--
-- TOC entry 2697 (class 2606 OID 37953)
-- Dependencies: 267 267
-- Name: uyeogrenim_pk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY uyeogrenim
    ADD CONSTRAINT uyeogrenim_pk PRIMARY KEY (rid);


--
-- TOC entry 2699 (class 2606 OID 37955)
-- Dependencies: 269 269
-- Name: uyesigorta_pk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY uyesigorta
    ADD CONSTRAINT uyesigorta_pk PRIMARY KEY (rid);


--
-- TOC entry 2701 (class 2606 OID 37957)
-- Dependencies: 271 271
-- Name: uyeuzmanlik_pk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY uyeuzmanlik
    ADD CONSTRAINT uyeuzmanlik_pk PRIMARY KEY (rid);


--
-- TOC entry 2703 (class 2606 OID 37959)
-- Dependencies: 273 273
-- Name: uzmanliktip_pk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY uzmanliktip
    ADD CONSTRAINT uzmanliktip_pk PRIMARY KEY (rid);


--
-- TOC entry 2705 (class 2606 OID 37961)
-- Dependencies: 276 276
-- Name: yayin_pk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY yayin
    ADD CONSTRAINT yayin_pk PRIMARY KEY (rid);


--
-- TOC entry 2737 (class 2606 OID 45006)
-- Dependencies: 309 309
-- Name: zamanlamaligonderim_pk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY gonderim
    ADD CONSTRAINT zamanlamaligonderim_pk PRIMARY KEY (rid);


--
-- TOC entry 2584 (class 1259 OID 37963)
-- Dependencies: 175
-- Name: ISLEMLOG_TABLOADI_IDX; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX "ISLEMLOG_TABLOADI_IDX" ON islemlog USING btree (tabloadi);


--
-- TOC entry 2602 (class 1259 OID 37964)
-- Dependencies: 190
-- Name: fki_komisyon_birim_fk; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX fki_komisyon_birim_fk ON komisyon USING btree (birimref);


--
-- TOC entry 2636 (class 1259 OID 37965)
-- Dependencies: 207
-- Name: fki_kurum_kurumturu_ref; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX fki_kurum_kurumturu_ref ON kurum USING btree (kurumtururef);


--
-- TOC entry 2591 (class 1259 OID 37966)
-- Dependencies: 180
-- Name: kisi_kimlikno_index; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE UNIQUE INDEX kisi_kimlikno_index ON kisi USING btree (kimlikno);


--
-- TOC entry 2561 (class 1259 OID 37967)
-- Dependencies: 157
-- Name: sehir_plakakodu_uk; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE UNIQUE INDEX sehir_plakakodu_uk ON sehir USING btree (plakakodu);


--
-- TOC entry 2617 (class 1259 OID 37968)
-- Dependencies: 197
-- Name: systemuser_name_unq; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE UNIQUE INDEX systemuser_name_unq ON kullanici USING btree (name);


--
-- TOC entry 2677 (class 1259 OID 37969)
-- Dependencies: 247
-- Name: uye_sicilno_uk; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE UNIQUE INDEX uye_sicilno_uk ON uye USING btree (sicilno);


--
-- TOC entry 2826 (class 2606 OID 40835)
-- Dependencies: 247 203 2675
-- Name: KURULDONEMUYE_UYE_FK; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY kuruldonemuye
    ADD CONSTRAINT "KURULDONEMUYE_UYE_FK" FOREIGN KEY (uyeref) REFERENCES uye(rid) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2774 (class 2606 OID 37970)
-- Dependencies: 2564 161 140
-- Name: adres_ilce_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY adres
    ADD CONSTRAINT adres_ilce_fk FOREIGN KEY (ilceref) REFERENCES ilce(rid) ON DELETE RESTRICT;


--
-- TOC entry 2775 (class 2606 OID 37975)
-- Dependencies: 180 140 2592
-- Name: adres_kisi_ref; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY adres
    ADD CONSTRAINT adres_kisi_ref FOREIGN KEY (kisiref) REFERENCES kisi(rid);


--
-- TOC entry 2776 (class 2606 OID 37980)
-- Dependencies: 2637 140 207
-- Name: adres_kurum_ref; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY adres
    ADD CONSTRAINT adres_kurum_ref FOREIGN KEY (kurumref) REFERENCES kurum(rid);


--
-- TOC entry 2777 (class 2606 OID 37985)
-- Dependencies: 157 2559 140
-- Name: adres_sehir_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY adres
    ADD CONSTRAINT adres_sehir_fk FOREIGN KEY (sehirref) REFERENCES sehir(rid) ON DELETE RESTRICT;


--
-- TOC entry 2778 (class 2606 OID 37990)
-- Dependencies: 140 2562 159
-- Name: adres_ulke_ref; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY adres
    ADD CONSTRAINT adres_ulke_ref FOREIGN KEY (ulkeref) REFERENCES ulke(rid) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- TOC entry 2779 (class 2606 OID 37995)
-- Dependencies: 207 2637 146
-- Name: birim_kurum_ref; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY birim
    ADD CONSTRAINT birim_kurum_ref FOREIGN KEY (kurumref) REFERENCES kurum(rid);


--
-- TOC entry 2783 (class 2606 OID 38000)
-- Dependencies: 147 2547 146
-- Name: birimhesap_birim_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY birimhesap
    ADD CONSTRAINT birimhesap_birim_fk FOREIGN KEY (birimref) REFERENCES birim(rid);


--
-- TOC entry 2784 (class 2606 OID 38005)
-- Dependencies: 149 2570 167
-- Name: bolum_fakulte_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY bolum
    ADD CONSTRAINT bolum_fakulte_fk FOREIGN KEY (fakulteref) REFERENCES fakulte(rid);


--
-- TOC entry 2785 (class 2606 OID 38010)
-- Dependencies: 151 180 2592
-- Name: borc_kisi_key; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY borc
    ADD CONSTRAINT borc_kisi_key FOREIGN KEY (kisiref) REFERENCES kisi(rid) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 2786 (class 2606 OID 38015)
-- Dependencies: 219 2651 151
-- Name: borc_odemetip_key; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY borc
    ADD CONSTRAINT borc_odemetip_key FOREIGN KEY (odemetipref) REFERENCES odemetip(rid) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 2789 (class 2606 OID 38020)
-- Dependencies: 153 2553 151
-- Name: borcodeme_borc_key; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY borcodeme
    ADD CONSTRAINT borcodeme_borc_key FOREIGN KEY (borcref) REFERENCES borc(rid) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 2788 (class 2606 OID 38025)
-- Dependencies: 146 2547 153
-- Name: borcodeme_odeme_ref; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY borcodeme
    ADD CONSTRAINT borcodeme_odeme_ref FOREIGN KEY (odemeref) REFERENCES birim(rid);


--
-- TOC entry 2793 (class 2606 OID 40702)
-- Dependencies: 2718 163 291
-- Name: donem_anadonemref; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY donem
    ADD CONSTRAINT donem_anadonemref FOREIGN KEY (anadonemref) REFERENCES anadonem(rid) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2792 (class 2606 OID 40715)
-- Dependencies: 163 146 2547
-- Name: donem_birimref; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY donem
    ADD CONSTRAINT donem_birimref FOREIGN KEY (birimref) REFERENCES birim(rid) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2878 (class 2606 OID 38666)
-- Dependencies: 2706 279 279
-- Name: dosyakodu_dosyakodu_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY dosyakodu
    ADD CONSTRAINT dosyakodu_dosyakodu_fk FOREIGN KEY (ustref) REFERENCES dosyakodu(rid);


--
-- TOC entry 2934 (class 2606 OID 45279)
-- Dependencies: 146 335 2547
-- Name: egitim_birim_key; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY egitim
    ADD CONSTRAINT egitim_birim_key FOREIGN KEY (birimref) REFERENCES birim(rid) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2933 (class 2606 OID 45274)
-- Dependencies: 2760 333 335
-- Name: egitim_egitimtanim_key; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY egitim
    ADD CONSTRAINT egitim_egitimtanim_key FOREIGN KEY (egitimtanimref) REFERENCES egitimtanim(rid) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2936 (class 2606 OID 45289)
-- Dependencies: 2564 335 161
-- Name: egitim_ilce_key; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY egitim
    ADD CONSTRAINT egitim_ilce_key FOREIGN KEY (ilceref) REFERENCES ilce(rid) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2935 (class 2606 OID 45284)
-- Dependencies: 2559 335 157
-- Name: egitim_sehir_key; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY egitim
    ADD CONSTRAINT egitim_sehir_key FOREIGN KEY (sehirref) REFERENCES sehir(rid) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2944 (class 2606 OID 45384)
-- Dependencies: 2770 343 345
-- Name: egitimdegerlendirme_egitimkatilimci_key; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY egitimdegerlendirme
    ADD CONSTRAINT egitimdegerlendirme_egitimkatilimci_key FOREIGN KEY (egitimkatilimciref) REFERENCES egitimkatilimci(rid) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2940 (class 2606 OID 45326)
-- Dependencies: 2762 335 339
-- Name: egitimders_egitim_key; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY egitimders
    ADD CONSTRAINT egitimders_egitim_key FOREIGN KEY (egitimref) REFERENCES egitim(rid) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2939 (class 2606 OID 45331)
-- Dependencies: 339 2764 337
-- Name: egitimders_egitimogretmen_key; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY egitimders
    ADD CONSTRAINT egitimders_egitimogretmen_key FOREIGN KEY (egitimogretmenref) REFERENCES egitimogretmen(rid) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2941 (class 2606 OID 45347)
-- Dependencies: 341 2762 335
-- Name: egitimdosya_egitim_key; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY egitimdosya
    ADD CONSTRAINT egitimdosya_egitim_key FOREIGN KEY (egitimref) REFERENCES egitim(rid) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2943 (class 2606 OID 45363)
-- Dependencies: 2762 343 335
-- Name: egitimkatilimci_egitim_key; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY egitimkatilimci
    ADD CONSTRAINT egitimkatilimci_egitim_key FOREIGN KEY (egitimref) REFERENCES egitim(rid) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2942 (class 2606 OID 45368)
-- Dependencies: 343 180 2592
-- Name: egitimkatilimci_kisi_key; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY egitimkatilimci
    ADD CONSTRAINT egitimkatilimci_kisi_key FOREIGN KEY (kisiref) REFERENCES kisi(rid) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2938 (class 2606 OID 45305)
-- Dependencies: 337 2762 335
-- Name: egitimogretmen_egitim_key; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY egitimogretmen
    ADD CONSTRAINT egitimogretmen_egitim_key FOREIGN KEY (egitimref) REFERENCES egitim(rid) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2937 (class 2606 OID 45310)
-- Dependencies: 180 337 2592
-- Name: egitimogretmen_kisi_key; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY egitimogretmen
    ADD CONSTRAINT egitimogretmen_kisi_key FOREIGN KEY (kisiref) REFERENCES kisi(rid) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2917 (class 2606 OID 45095)
-- Dependencies: 2592 180 315
-- Name: epostaanlikgonderimlog_gonderenref_key; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY epostaanlikgonderimlog
    ADD CONSTRAINT epostaanlikgonderimlog_gonderenref_key FOREIGN KEY (gonderenref) REFERENCES kisi(rid) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2916 (class 2606 OID 45090)
-- Dependencies: 315 2734 307
-- Name: epostaanlikgonderimlog_icerik_key; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY epostaanlikgonderimlog
    ADD CONSTRAINT epostaanlikgonderimlog_icerik_key FOREIGN KEY (icerikref) REFERENCES icerik(rid) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2915 (class 2606 OID 45085)
-- Dependencies: 315 180 2592
-- Name: epostaanlikgonderimlog_kisi_key; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY epostaanlikgonderimlog
    ADD CONSTRAINT epostaanlikgonderimlog_kisi_key FOREIGN KEY (kisiref) REFERENCES kisi(rid) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2911 (class 2606 OID 45043)
-- Dependencies: 311 2592 180
-- Name: epostagonderimlog_gonderenref_key; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY epostagonderimlog
    ADD CONSTRAINT epostagonderimlog_gonderenref_key FOREIGN KEY (gonderenref) REFERENCES kisi(rid) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2909 (class 2606 OID 45033)
-- Dependencies: 309 2736 311
-- Name: epostagonderimlog_gonderim_key; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY epostagonderimlog
    ADD CONSTRAINT epostagonderimlog_gonderim_key FOREIGN KEY (gonderimref) REFERENCES gonderim(rid) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2910 (class 2606 OID 45038)
-- Dependencies: 2592 311 180
-- Name: epostagonderimlog_kisi_key; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY epostagonderimlog
    ADD CONSTRAINT epostagonderimlog_kisi_key FOREIGN KEY (kisiref) REFERENCES kisi(rid) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2921 (class 2606 OID 45149)
-- Dependencies: 2746 321 319
-- Name: etkinlik_etkinliktanim_key; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY etkinlik
    ADD CONSTRAINT etkinlik_etkinliktanim_key FOREIGN KEY (etkinliktanimref) REFERENCES etkinliktanim(rid) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2923 (class 2606 OID 45159)
-- Dependencies: 2564 161 321
-- Name: etkinlik_ilce_key; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY etkinlik
    ADD CONSTRAINT etkinlik_ilce_key FOREIGN KEY (ilceref) REFERENCES ilce(rid) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2924 (class 2606 OID 45164)
-- Dependencies: 2592 180 321
-- Name: etkinlik_kisi_key; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY etkinlik
    ADD CONSTRAINT etkinlik_kisi_key FOREIGN KEY (sorumlukisiref) REFERENCES kisi(rid) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2922 (class 2606 OID 45154)
-- Dependencies: 321 2559 157
-- Name: etkinlik_sehir_key; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY etkinlik
    ADD CONSTRAINT etkinlik_sehir_key FOREIGN KEY (sehirref) REFERENCES sehir(rid) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2932 (class 2606 OID 45247)
-- Dependencies: 321 2748 331
-- Name: etkinlikdosya_etkinlik_key; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY etkinlikdosya
    ADD CONSTRAINT etkinlikdosya_etkinlik_key FOREIGN KEY (etkinlikref) REFERENCES etkinlik(rid) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2925 (class 2606 OID 45182)
-- Dependencies: 2748 323 321
-- Name: etkinlikkatilimci_etkinlik_key; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY etkinlikkatilimci
    ADD CONSTRAINT etkinlikkatilimci_etkinlik_key FOREIGN KEY (etkinlikref) REFERENCES etkinlik(rid) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2926 (class 2606 OID 45177)
-- Dependencies: 323 180 2592
-- Name: etkinlikkatilimci_kisi_key; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY etkinlikkatilimci
    ADD CONSTRAINT etkinlikkatilimci_kisi_key FOREIGN KEY (kisiref) REFERENCES kisi(rid) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2929 (class 2606 OID 45213)
-- Dependencies: 327 2748 321
-- Name: etkinlikkurul_etkinlik_key; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY etkinlikkurul
    ADD CONSTRAINT etkinlikkurul_etkinlik_key FOREIGN KEY (etkinlikref) REFERENCES etkinlik(rid) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2931 (class 2606 OID 45226)
-- Dependencies: 327 329 2754
-- Name: etkinlikkuruluye_etkinlikkurul_key; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY etkinlikkuruluye
    ADD CONSTRAINT etkinlikkuruluye_etkinlikkurul_key FOREIGN KEY (etkinlikkurulref) REFERENCES etkinlikkurul(rid) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2930 (class 2606 OID 45231)
-- Dependencies: 329 247 2675
-- Name: etkinlikkuruluye_uye_key; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY etkinlikkuruluye
    ADD CONSTRAINT etkinlikkuruluye_uye_key FOREIGN KEY (uyeref) REFERENCES uye(rid) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2927 (class 2606 OID 45200)
-- Dependencies: 325 2748 321
-- Name: etkinlikkurum_etkinlik_key; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY etkinlikkurum
    ADD CONSTRAINT etkinlikkurum_etkinlik_key FOREIGN KEY (etkinlikref) REFERENCES etkinlik(rid) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2928 (class 2606 OID 45195)
-- Dependencies: 207 2637 325
-- Name: etkinlikkurum_kurum_key; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY etkinlikkurum
    ADD CONSTRAINT etkinlikkurum_kurum_key FOREIGN KEY (kurumref) REFERENCES kurum(rid) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2881 (class 2606 OID 38632)
-- Dependencies: 2706 281 279
-- Name: evrak_dosyakodu_key; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY evrak
    ADD CONSTRAINT evrak_dosyakodu_key FOREIGN KEY (dosyakoduref) REFERENCES dosyakodu(rid) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 2884 (class 2606 OID 38701)
-- Dependencies: 281 146 2547
-- Name: evrak_gittigibirim_key; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY evrak
    ADD CONSTRAINT evrak_gittigibirim_key FOREIGN KEY (gittigibirimref) REFERENCES birim(rid) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2883 (class 2606 OID 38696)
-- Dependencies: 207 281 2637
-- Name: evrak_gittigikurum_key; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY evrak
    ADD CONSTRAINT evrak_gittigikurum_key FOREIGN KEY (gittigikurumref) REFERENCES kurum(rid) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2880 (class 2606 OID 38627)
-- Dependencies: 2592 180 281
-- Name: evrak_kisi_key; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY evrak
    ADD CONSTRAINT evrak_kisi_key FOREIGN KEY (gonderenkisiref) REFERENCES kisi(rid) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 2879 (class 2606 OID 38622)
-- Dependencies: 2637 207 281
-- Name: evrak_kurum_key; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY evrak
    ADD CONSTRAINT evrak_kurum_key FOREIGN KEY (gonderenkurumref) REFERENCES kurum(rid) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 2882 (class 2606 OID 38637)
-- Dependencies: 2657 281 225
-- Name: evrak_personel_key; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY evrak
    ADD CONSTRAINT evrak_personel_key FOREIGN KEY (hazirlayanpersonelref) REFERENCES personel(rid) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 2885 (class 2606 OID 38683)
-- Dependencies: 283 281 2708
-- Name: evrakek_evrak_key; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY evrakek
    ADD CONSTRAINT evrakek_evrak_key FOREIGN KEY (evrakref) REFERENCES evrak(rid) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2794 (class 2606 OID 38030)
-- Dependencies: 243 167 2671
-- Name: fakulte_universite_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY fakulte
    ADD CONSTRAINT fakulte_universite_fk FOREIGN KEY (universiteref) REFERENCES universite(rid);


--
-- TOC entry 2818 (class 2606 OID 38035)
-- Dependencies: 2564 196 161
-- Name: fk12375285b6e40; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY koy
    ADD CONSTRAINT fk12375285b6e40 FOREIGN KEY (ilceref) REFERENCES ilce(rid);


--
-- TOC entry 2848 (class 2606 OID 38040)
-- Dependencies: 247 225 2657
-- Name: fk14a2170d5d82; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY uye
    ADD CONSTRAINT fk14a2170d5d82 FOREIGN KEY (onaylayanref) REFERENCES personel(rid);


--
-- TOC entry 2849 (class 2606 OID 38045)
-- Dependencies: 247 146 2547
-- Name: fk14a2186a2a006; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY uye
    ADD CONSTRAINT fk14a2186a2a006 FOREIGN KEY (birimref) REFERENCES birim(rid);


--
-- TOC entry 2787 (class 2606 OID 38050)
-- Dependencies: 2547 146 151
-- Name: fk1f333e86a2a006; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY borc
    ADD CONSTRAINT fk1f333e86a2a006 FOREIGN KEY (birimref) REFERENCES birim(rid);


--
-- TOC entry 2791 (class 2606 OID 38055)
-- Dependencies: 2559 161 157
-- Name: fk2254c588dd1606; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ilce
    ADD CONSTRAINT fk2254c588dd1606 FOREIGN KEY (sehirref) REFERENCES sehir(rid);


--
-- TOC entry 2801 (class 2606 OID 38060)
-- Dependencies: 177 2576 173
-- Name: fk2400b80064bad226; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY islemrole
    ADD CONSTRAINT fk2400b80064bad226 FOREIGN KEY (islemref) REFERENCES islem(rid);


--
-- TOC entry 2821 (class 2606 OID 38065)
-- Dependencies: 197 198 2618
-- Name: fk34156ea8a031e6; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY kullanicirole
    ADD CONSTRAINT fk34156ea8a031e6 FOREIGN KEY (kullaniciref) REFERENCES kullanici(rid);


--
-- TOC entry 2780 (class 2606 OID 38070)
-- Dependencies: 2564 146 161
-- Name: fk3c47b4f285b6e40; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY birim
    ADD CONSTRAINT fk3c47b4f285b6e40 FOREIGN KEY (ilceref) REFERENCES ilce(rid);


--
-- TOC entry 2781 (class 2606 OID 38075)
-- Dependencies: 146 157 2559
-- Name: fk3c47b4f88dd1606; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY birim
    ADD CONSTRAINT fk3c47b4f88dd1606 FOREIGN KEY (sehirref) REFERENCES sehir(rid);


--
-- TOC entry 2790 (class 2606 OID 38080)
-- Dependencies: 159 2562 157
-- Name: fk4b213dfa39c66c0; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY sehir
    ADD CONSTRAINT fk4b213dfa39c66c0 FOREIGN KEY (ulkeref) REFERENCES ulke(rid);


--
-- TOC entry 2847 (class 2606 OID 38085)
-- Dependencies: 146 245 2547
-- Name: fk555f651f86a2a006; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY uyarinot
    ADD CONSTRAINT fk555f651f86a2a006 FOREIGN KEY (birimref) REFERENCES birim(rid);


--
-- TOC entry 2863 (class 2606 OID 38090)
-- Dependencies: 2541 140 259
-- Name: fk5bd5a73f899735b7; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY uyekurum
    ADD CONSTRAINT fk5bd5a73f899735b7 FOREIGN KEY (adresref) REFERENCES adres(rid);


--
-- TOC entry 2811 (class 2606 OID 38100)
-- Dependencies: 2562 159 188
-- Name: fkb750cd90a39c66c0; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY kisiuyruk
    ADD CONSTRAINT fkb750cd90a39c66c0 FOREIGN KEY (ulkeref) REFERENCES ulke(rid);


--
-- TOC entry 2798 (class 2606 OID 38105)
-- Dependencies: 174 198 2620
-- Name: fkc8c4bfae298468c6; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY islemkullanici
    ADD CONSTRAINT fkc8c4bfae298468c6 FOREIGN KEY (kullaniciroleref) REFERENCES kullanicirole(rid);


--
-- TOC entry 2799 (class 2606 OID 38110)
-- Dependencies: 173 174 2576
-- Name: fkc8c4bfae64bad226; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY islemkullanici
    ADD CONSTRAINT fkc8c4bfae64bad226 FOREIGN KEY (islemref) REFERENCES islem(rid);


--
-- TOC entry 2795 (class 2606 OID 38115)
-- Dependencies: 169 169 2572
-- Name: gorev_gorev_key; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY gorev
    ADD CONSTRAINT gorev_gorev_key FOREIGN KEY (ustref) REFERENCES gorev(rid) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 2903 (class 2606 OID 44970)
-- Dependencies: 303 146 2547
-- Name: iletisimliste_birim_key; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY iletisimliste
    ADD CONSTRAINT iletisimliste_birim_key FOREIGN KEY (birimref) REFERENCES birim(rid) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2904 (class 2606 OID 44965)
-- Dependencies: 303 2730 303
-- Name: iletisimliste_iletisimliste_key; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY iletisimliste
    ADD CONSTRAINT iletisimliste_iletisimliste_key FOREIGN KEY (ustref) REFERENCES iletisimliste(rid) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2905 (class 2606 OID 44983)
-- Dependencies: 305 2730 303
-- Name: iletisimlistedetay_iletisimliste_key; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY iletisimlistedetay
    ADD CONSTRAINT iletisimlistedetay_iletisimliste_key FOREIGN KEY (iletisimlisteref) REFERENCES iletisimliste(rid) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2797 (class 2606 OID 38120)
-- Dependencies: 180 171 2592
-- Name: internetadres_kisi_key; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY internetadres
    ADD CONSTRAINT internetadres_kisi_key FOREIGN KEY (kisiref) REFERENCES kisi(rid) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 2796 (class 2606 OID 38125)
-- Dependencies: 171 207 2637
-- Name: internetadres_kurum_key; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY internetadres
    ADD CONSTRAINT internetadres_kurum_key FOREIGN KEY (kurumref) REFERENCES kurum(rid);


--
-- TOC entry 2803 (class 2606 OID 38135)
-- Dependencies: 178 207 2637
-- Name: isyeritemsilcileri_kurum_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY isyeritemsilcileri
    ADD CONSTRAINT isyeritemsilcileri_kurum_fk FOREIGN KEY (kurumref) REFERENCES kurum(rid);


--
-- TOC entry 2802 (class 2606 OID 38140)
-- Dependencies: 247 2675 178
-- Name: isyeritemsilcileri_uye_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY isyeritemsilcileri
    ADD CONSTRAINT isyeritemsilcileri_uye_fk FOREIGN KEY (uyeref) REFERENCES uye(rid);


--
-- TOC entry 2804 (class 2606 OID 38145)
-- Dependencies: 182 2592 180
-- Name: kisifotograf_kisi_key; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY kisifotograf
    ADD CONSTRAINT kisifotograf_kisi_key FOREIGN KEY (kisiref) REFERENCES kisi(rid) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 2805 (class 2606 OID 38160)
-- Dependencies: 2592 184 180
-- Name: kisiizin_kisi_key; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY kisiizin
    ADD CONSTRAINT kisiizin_kisi_key FOREIGN KEY (kisiref) REFERENCES kisi(rid) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 2806 (class 2606 OID 38165)
-- Dependencies: 186 157 2559
-- Name: kisikimlik_city_key; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY kisikimlik
    ADD CONSTRAINT kisikimlik_city_key FOREIGN KEY (sehirref) REFERENCES sehir(rid) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 2807 (class 2606 OID 38170)
-- Dependencies: 161 2564 186
-- Name: kisikimlik_district_key; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY kisikimlik
    ADD CONSTRAINT kisikimlik_district_key FOREIGN KEY (dogumilceref) REFERENCES ilce(rid) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 2808 (class 2606 OID 38175)
-- Dependencies: 186 2564 161
-- Name: kisikimlik_district_key2; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY kisikimlik
    ADD CONSTRAINT kisikimlik_district_key2 FOREIGN KEY (ilceref) REFERENCES ilce(rid) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 2809 (class 2606 OID 38180)
-- Dependencies: 180 186 2592
-- Name: kisikimlik_kisi_key; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY kisikimlik
    ADD CONSTRAINT kisikimlik_kisi_key FOREIGN KEY (kisiref) REFERENCES kisi(rid) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 2810 (class 2606 OID 38185)
-- Dependencies: 180 188 2592
-- Name: kisiuyruk_kisi_key; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY kisiuyruk
    ADD CONSTRAINT kisiuyruk_kisi_key FOREIGN KEY (kisiref) REFERENCES kisi(rid) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 2813 (class 2606 OID 38190)
-- Dependencies: 190 2547 146
-- Name: komisyon_birim_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY komisyon
    ADD CONSTRAINT komisyon_birim_fk FOREIGN KEY (birimref) REFERENCES birim(rid);


--
-- TOC entry 2812 (class 2606 OID 38195)
-- Dependencies: 190 190 2603
-- Name: komisyon_ust_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY komisyon
    ADD CONSTRAINT komisyon_ust_fk FOREIGN KEY (ustref) REFERENCES komisyon(rid);


--
-- TOC entry 2814 (class 2606 OID 38200)
-- Dependencies: 163 2566 192
-- Name: komisyondonem_donem_key; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY komisyondonem
    ADD CONSTRAINT komisyondonem_donem_key FOREIGN KEY (donemref) REFERENCES donem(rid) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 2815 (class 2606 OID 38205)
-- Dependencies: 192 2603 190
-- Name: komisyondonem_komisyon_key; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY komisyondonem
    ADD CONSTRAINT komisyondonem_komisyon_key FOREIGN KEY (komisyonref) REFERENCES komisyon(rid) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 2896 (class 2606 OID 40788)
-- Dependencies: 295 2605 192
-- Name: komisyondonemtoplanti_komisyondonem_key; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY komisyondonemtoplanti
    ADD CONSTRAINT komisyondonemtoplanti_komisyondonem_key FOREIGN KEY (komisyondonemref) REFERENCES komisyondonem(rid) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2900 (class 2606 OID 40825)
-- Dependencies: 2722 295 299
-- Name: komisyondonemtoplantitutanak_komisyondonemtoplanti_key; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY komisyondonemtoplantitutanak
    ADD CONSTRAINT komisyondonemtoplantitutanak_komisyondonemtoplanti_key FOREIGN KEY (kmsyndonemtoplantiref) REFERENCES komisyondonemtoplanti(rid) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2899 (class 2606 OID 40830)
-- Dependencies: 2657 299 225
-- Name: komisyondonemtoplantitutanak_personel_key; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY komisyondonemtoplantitutanak
    ADD CONSTRAINT komisyondonemtoplantitutanak_personel_key FOREIGN KEY (yukleyenref) REFERENCES personel(rid) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2816 (class 2606 OID 38210)
-- Dependencies: 194 192 2605
-- Name: komisyondonemuye_komisyondonem_key; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY komisyondonemuye
    ADD CONSTRAINT komisyondonemuye_komisyondonem_key FOREIGN KEY (komisyondonemref) REFERENCES komisyondonem(rid) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 2817 (class 2606 OID 38215)
-- Dependencies: 194 2675 247
-- Name: komisyondonemuye_uye_key; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY komisyondonemuye
    ADD CONSTRAINT komisyondonemuye_uye_key FOREIGN KEY (uyeref) REFERENCES uye(rid) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 2819 (class 2606 OID 38220)
-- Dependencies: 197 2592 180
-- Name: kullanici_kisi_key; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY kullanici
    ADD CONSTRAINT kullanici_kisi_key FOREIGN KEY (kisiref) REFERENCES kisi(rid);


--
-- TOC entry 2822 (class 2606 OID 38225)
-- Dependencies: 146 199 2547
-- Name: kurul_birim_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY kurul
    ADD CONSTRAINT kurul_birim_fk FOREIGN KEY (birimref) REFERENCES birim(rid);


--
-- TOC entry 2823 (class 2606 OID 38230)
-- Dependencies: 163 2566 201
-- Name: kuruldonem_donem_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY kuruldonem
    ADD CONSTRAINT kuruldonem_donem_fk FOREIGN KEY (donemref) REFERENCES donem(rid);


--
-- TOC entry 2824 (class 2606 OID 38235)
-- Dependencies: 201 2622 199
-- Name: kuruldonem_kurul_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY kuruldonem
    ADD CONSTRAINT kuruldonem_kurul_fk FOREIGN KEY (kurulref) REFERENCES kurul(rid);


--
-- TOC entry 2902 (class 2606 OID 40850)
-- Dependencies: 201 301 2626
-- Name: kuruldonemdelege_kuruldonem_key; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY kuruldonemdelege
    ADD CONSTRAINT kuruldonemdelege_kuruldonem_key FOREIGN KEY (kuruldonemref) REFERENCES kuruldonem(rid) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2901 (class 2606 OID 40855)
-- Dependencies: 247 301 2675
-- Name: kuruldonemdelege_uye_key; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY kuruldonemdelege
    ADD CONSTRAINT kuruldonemdelege_uye_key FOREIGN KEY (uyeref) REFERENCES uye(rid) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2895 (class 2606 OID 40775)
-- Dependencies: 201 293 2626
-- Name: kuruldonemtoplanti_kuruldonem_key; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY kuruldonemtoplanti
    ADD CONSTRAINT kuruldonemtoplanti_kuruldonem_key FOREIGN KEY (kuruldonemref) REFERENCES kuruldonem(rid) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2898 (class 2606 OID 40804)
-- Dependencies: 297 293 2720
-- Name: kuruldonemtoplantitutanak_kuruldonemtoplanti_key; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY kuruldonemtoplantitutanak
    ADD CONSTRAINT kuruldonemtoplantitutanak_kuruldonemtoplanti_key FOREIGN KEY (kuruldonemtoplantiref) REFERENCES kuruldonemtoplanti(rid) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2897 (class 2606 OID 40809)
-- Dependencies: 2657 225 297
-- Name: kuruldonemtoplantitutanak_personel_key; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY kuruldonemtoplantitutanak
    ADD CONSTRAINT kuruldonemtoplantitutanak_personel_key FOREIGN KEY (yukleyenref) REFERENCES personel(rid) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2825 (class 2606 OID 38240)
-- Dependencies: 2626 203 201
-- Name: kuruldonemuye_kuruldonem_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY kuruldonemuye
    ADD CONSTRAINT kuruldonemuye_kuruldonem_fk FOREIGN KEY (kuruldonemref) REFERENCES kuruldonem(rid);


--
-- TOC entry 2830 (class 2606 OID 40725)
-- Dependencies: 2564 161 207
-- Name: kurum_ilceref; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY kurum
    ADD CONSTRAINT kurum_ilceref FOREIGN KEY (ilceref) REFERENCES ilce(rid) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2827 (class 2606 OID 38255)
-- Dependencies: 2592 180 207
-- Name: kurum_kisi_key; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY kurum
    ADD CONSTRAINT kurum_kisi_key FOREIGN KEY (sorumluref) REFERENCES kisi(rid) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 2828 (class 2606 OID 38260)
-- Dependencies: 2643 213 207
-- Name: kurum_kurumturu_ref; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY kurum
    ADD CONSTRAINT kurum_kurumturu_ref FOREIGN KEY (kurumtururef) REFERENCES kurumturu(rid);


--
-- TOC entry 2829 (class 2606 OID 40720)
-- Dependencies: 2559 157 207
-- Name: kurum_sehirref; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY kurum
    ADD CONSTRAINT kurum_sehirref FOREIGN KEY (sehirref) REFERENCES sehir(rid) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2831 (class 2606 OID 38265)
-- Dependencies: 209 2637 207
-- Name: kurumhesap_kurum_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY kurumhesap
    ADD CONSTRAINT kurumhesap_kurum_fk FOREIGN KEY (kurumref) REFERENCES kurum(rid);


--
-- TOC entry 2833 (class 2606 OID 38270)
-- Dependencies: 2592 211 180
-- Name: kurumkisi_kisi_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY kurumkisi
    ADD CONSTRAINT kurumkisi_kisi_fk FOREIGN KEY (kisiref) REFERENCES kisi(rid);


--
-- TOC entry 2832 (class 2606 OID 38275)
-- Dependencies: 2637 207 211
-- Name: kurumkisi_kurum_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY kurumkisi
    ADD CONSTRAINT kurumkisi_kurum_fk FOREIGN KEY (kurumref) REFERENCES kurum(rid);


--
-- TOC entry 2834 (class 2606 OID 38280)
-- Dependencies: 215 215 2645
-- Name: menuitem_ust_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY menuitem
    ADD CONSTRAINT menuitem_ust_fk FOREIGN KEY (ustref) REFERENCES menuitem(rid);


--
-- TOC entry 2835 (class 2606 OID 38285)
-- Dependencies: 2592 217 180
-- Name: odeme_kisi_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY odeme
    ADD CONSTRAINT odeme_kisi_fk FOREIGN KEY (kisiref) REFERENCES kisi(rid);


--
-- TOC entry 2837 (class 2606 OID 38290)
-- Dependencies: 146 225 2547
-- Name: personel_birim_ref; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY personel
    ADD CONSTRAINT personel_birim_ref FOREIGN KEY (birimref) REFERENCES birim(rid);


--
-- TOC entry 2836 (class 2606 OID 38295)
-- Dependencies: 180 225 2592
-- Name: personel_kisi_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY personel
    ADD CONSTRAINT personel_kisi_fk FOREIGN KEY (kisiref) REFERENCES kisi(rid);


--
-- TOC entry 2838 (class 2606 OID 38300)
-- Dependencies: 146 227 2547
-- Name: personelbirimgorev_birim_key; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY personelbirimgorev
    ADD CONSTRAINT personelbirimgorev_birim_key FOREIGN KEY (birimref) REFERENCES birim(rid) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 2839 (class 2606 OID 38305)
-- Dependencies: 227 2572 169
-- Name: personelbirimgorev_gorev_key; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY personelbirimgorev
    ADD CONSTRAINT personelbirimgorev_gorev_key FOREIGN KEY (gorevref) REFERENCES gorev(rid) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 2840 (class 2606 OID 38310)
-- Dependencies: 225 227 2657
-- Name: personelbirimgorev_personel_key; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY personelbirimgorev
    ADD CONSTRAINT personelbirimgorev_personel_key FOREIGN KEY (personelref) REFERENCES personel(rid) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 2920 (class 2606 OID 45121)
-- Dependencies: 317 2592 180
-- Name: smsanlikgonderimlog_gonderenref_key; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY smsanlikgonderimlog
    ADD CONSTRAINT smsanlikgonderimlog_gonderenref_key FOREIGN KEY (gonderenref) REFERENCES kisi(rid) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2919 (class 2606 OID 45116)
-- Dependencies: 317 2734 307
-- Name: smsanlikgonderimlog_icerik_key; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY smsanlikgonderimlog
    ADD CONSTRAINT smsanlikgonderimlog_icerik_key FOREIGN KEY (icerikref) REFERENCES icerik(rid) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2918 (class 2606 OID 45111)
-- Dependencies: 2592 317 180
-- Name: smsanlikgonderimlog_kisi_key; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY smsanlikgonderimlog
    ADD CONSTRAINT smsanlikgonderimlog_kisi_key FOREIGN KEY (kisiref) REFERENCES kisi(rid) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2914 (class 2606 OID 45069)
-- Dependencies: 180 2592 313
-- Name: smsgonderimlog_gonderenref_key; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY smsgonderimlog
    ADD CONSTRAINT smsgonderimlog_gonderenref_key FOREIGN KEY (gonderenref) REFERENCES kisi(rid) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2912 (class 2606 OID 45059)
-- Dependencies: 2736 313 309
-- Name: smsgonderimlog_gonderim_key; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY smsgonderimlog
    ADD CONSTRAINT smsgonderimlog_gonderim_key FOREIGN KEY (gonderimref) REFERENCES gonderim(rid) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2913 (class 2606 OID 45064)
-- Dependencies: 180 2592 313
-- Name: smsgonderimlog_kisi_key; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY smsgonderimlog
    ADD CONSTRAINT smsgonderimlog_kisi_key FOREIGN KEY (kisiref) REFERENCES kisi(rid) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2841 (class 2606 OID 38315)
-- Dependencies: 233 2618 197
-- Name: sorun_kullanici_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY sorun
    ADD CONSTRAINT sorun_kullanici_fk FOREIGN KEY (kullaniciref) REFERENCES kullanici(rid);


--
-- TOC entry 2820 (class 2606 OID 38320)
-- Dependencies: 2661 198 229
-- Name: systemuserrole_role_key; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY kullanicirole
    ADD CONSTRAINT systemuserrole_role_key FOREIGN KEY (roleref) REFERENCES role(rid);


--
-- TOC entry 2843 (class 2606 OID 38325)
-- Dependencies: 2592 237 180
-- Name: telefon_kisi_key; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY telefon
    ADD CONSTRAINT telefon_kisi_key FOREIGN KEY (kisiref) REFERENCES kisi(rid) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 2842 (class 2606 OID 38330)
-- Dependencies: 2637 207 237
-- Name: telefon_kurum_key; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY telefon
    ADD CONSTRAINT telefon_kurum_key FOREIGN KEY (kurumref) REFERENCES kurum(rid);


--
-- TOC entry 2800 (class 2606 OID 38335)
-- Dependencies: 229 2661 177
-- Name: transactionrole_role_key; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY islemrole
    ADD CONSTRAINT transactionrole_role_key FOREIGN KEY (roleref) REFERENCES role(rid);


--
-- TOC entry 2782 (class 2606 OID 38340)
-- Dependencies: 146 146 2547
-- Name: unit_ust_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY birim
    ADD CONSTRAINT unit_ust_fk FOREIGN KEY (ustref) REFERENCES birim(rid);


--
-- TOC entry 2845 (class 2606 OID 38345)
-- Dependencies: 243 157 2559
-- Name: universite_il_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY universite
    ADD CONSTRAINT universite_il_fk FOREIGN KEY (ilref) REFERENCES sehir(rid);


--
-- TOC entry 2844 (class 2606 OID 38350)
-- Dependencies: 2562 159 243
-- Name: universite_ulke_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY universite
    ADD CONSTRAINT universite_ulke_fk FOREIGN KEY (ulkeref) REFERENCES ulke(rid);


--
-- TOC entry 2846 (class 2606 OID 38355)
-- Dependencies: 245 180 2592
-- Name: uyarinot_kisi_key; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY uyarinot
    ADD CONSTRAINT uyarinot_kisi_key FOREIGN KEY (kullaniciref) REFERENCES kisi(rid) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 2850 (class 2606 OID 38360)
-- Dependencies: 180 247 2592
-- Name: uye_kisi_key; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY uye
    ADD CONSTRAINT uye_kisi_key FOREIGN KEY (kisiref) REFERENCES kisi(rid);


--
-- TOC entry 2851 (class 2606 OID 38365)
-- Dependencies: 247 247 2675
-- Name: uye_uye_key; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY uye
    ADD CONSTRAINT uye_uye_key FOREIGN KEY (ogrenciuyeref) REFERENCES uye(rid);


--
-- TOC entry 2853 (class 2606 OID 38370)
-- Dependencies: 2675 249 247
-- Name: uyeabone_uye_key; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY uyeabone
    ADD CONSTRAINT uyeabone_uye_key FOREIGN KEY (uyeref) REFERENCES uye(rid) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 2852 (class 2606 OID 38375)
-- Dependencies: 249 276 2704
-- Name: uyeabone_yayin_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY uyeabone
    ADD CONSTRAINT uyeabone_yayin_fk FOREIGN KEY (yayinref) REFERENCES yayin(rid);


--
-- TOC entry 2855 (class 2606 OID 38380)
-- Dependencies: 251 247 2675
-- Name: uyeaidat_uye_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY uyeaidat
    ADD CONSTRAINT uyeaidat_uye_fk FOREIGN KEY (uyeref) REFERENCES uye(rid);


--
-- TOC entry 2854 (class 2606 OID 38385)
-- Dependencies: 251 2690 261
-- Name: uyeaidat_uyemuafiyet_key; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY uyeaidat
    ADD CONSTRAINT uyeaidat_uyemuafiyet_key FOREIGN KEY (uyemuafiyetref) REFERENCES uyemuafiyet(rid);


--
-- TOC entry 2888 (class 2606 OID 38753)
-- Dependencies: 149 2551 289
-- Name: uyebasvuru_bolumref; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY uyebasvuru
    ADD CONSTRAINT uyebasvuru_bolumref FOREIGN KEY (bolumref) REFERENCES bolum(rid);


--
-- TOC entry 2889 (class 2606 OID 38758)
-- Dependencies: 161 2564 289
-- Name: uyebasvuru_dogumilceref_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY uyebasvuru
    ADD CONSTRAINT uyebasvuru_dogumilceref_fk FOREIGN KEY (dogumilceref) REFERENCES ilce(rid);


--
-- TOC entry 2890 (class 2606 OID 38763)
-- Dependencies: 289 167 2570
-- Name: uyebasvuru_fakulte_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY uyebasvuru
    ADD CONSTRAINT uyebasvuru_fakulte_fk FOREIGN KEY (fakulteref) REFERENCES fakulte(rid);


--
-- TOC entry 2891 (class 2606 OID 38768)
-- Dependencies: 2564 289 161
-- Name: uyebasvuru_ilce_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY uyebasvuru
    ADD CONSTRAINT uyebasvuru_ilce_fk FOREIGN KEY (ilceref) REFERENCES ilce(rid);


--
-- TOC entry 2892 (class 2606 OID 38773)
-- Dependencies: 2559 289 157
-- Name: uyebasvuru_sehir_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY uyebasvuru
    ADD CONSTRAINT uyebasvuru_sehir_fk FOREIGN KEY (sehirref) REFERENCES sehir(rid);


--
-- TOC entry 2893 (class 2606 OID 38778)
-- Dependencies: 159 289 2562
-- Name: uyebasvuru_ulke_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY uyebasvuru
    ADD CONSTRAINT uyebasvuru_ulke_fk FOREIGN KEY (ulkeref) REFERENCES ulke(rid);


--
-- TOC entry 2894 (class 2606 OID 38783)
-- Dependencies: 2671 243 289
-- Name: uyebasvuru_universite_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY uyebasvuru
    ADD CONSTRAINT uyebasvuru_universite_fk FOREIGN KEY (universiteref) REFERENCES universite(rid);


--
-- TOC entry 2856 (class 2606 OID 38390)
-- Dependencies: 253 2545 144
-- Name: uyebelge_belgetip_key; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY uyebelge
    ADD CONSTRAINT uyebelge_belgetip_key FOREIGN KEY (belgetipref) REFERENCES belgetip(rid) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 2857 (class 2606 OID 38400)
-- Dependencies: 207 253 2637
-- Name: uyebelge_kurum_key; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY uyebelge
    ADD CONSTRAINT uyebelge_kurum_key FOREIGN KEY (kurumref) REFERENCES kurum(rid) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 2858 (class 2606 OID 38405)
-- Dependencies: 253 247 2675
-- Name: uyebelge_uye_key; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY uyebelge
    ADD CONSTRAINT uyebelge_uye_key FOREIGN KEY (uyeref) REFERENCES uye(rid) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 2860 (class 2606 OID 38415)
-- Dependencies: 255 2675 247
-- Name: uyebilirkisi_uye_key; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY uyebilirkisi
    ADD CONSTRAINT uyebilirkisi_uye_key FOREIGN KEY (uyeref) REFERENCES uye(rid) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 2859 (class 2606 OID 38420)
-- Dependencies: 2700 255 271
-- Name: uyebilirkisi_uyeuzmanlik_key; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY uyebilirkisi
    ADD CONSTRAINT uyebilirkisi_uyeuzmanlik_key FOREIGN KEY (uyeuzmanlikref) REFERENCES uyeuzmanlik(rid) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 2862 (class 2606 OID 38425)
-- Dependencies: 155 257 2557
-- Name: uyeceza_cezatip_key; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY uyeceza
    ADD CONSTRAINT uyeceza_cezatip_key FOREIGN KEY (cezatipref) REFERENCES cezatip(rid) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 2861 (class 2606 OID 38435)
-- Dependencies: 247 2675 257
-- Name: uyeceza_uye_key; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY uyeceza
    ADD CONSTRAINT uyeceza_uye_key FOREIGN KEY (uyeref) REFERENCES uye(rid) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 2886 (class 2606 OID 38737)
-- Dependencies: 287 2618 197
-- Name: uyedurumdegistirme_kullanici_key; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY uyedurumdegistirme
    ADD CONSTRAINT uyedurumdegistirme_kullanici_key FOREIGN KEY (kayityapan) REFERENCES kullanici(rid) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2887 (class 2606 OID 38732)
-- Dependencies: 247 2675 287
-- Name: uyedurumdegistirme_uye_key; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY uyedurumdegistirme
    ADD CONSTRAINT uyedurumdegistirme_uye_key FOREIGN KEY (uyeref) REFERENCES uye(rid) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2864 (class 2606 OID 38440)
-- Dependencies: 2637 259 207
-- Name: uyekurum_kurum_key; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY uyekurum
    ADD CONSTRAINT uyekurum_kurum_key FOREIGN KEY (kurumref) REFERENCES kurum(rid) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 2865 (class 2606 OID 38445)
-- Dependencies: 2675 247 259
-- Name: uyekurum_uye_key; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY uyekurum
    ADD CONSTRAINT uyekurum_uye_key FOREIGN KEY (uyeref) REFERENCES uye(rid) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 2866 (class 2606 OID 38450)
-- Dependencies: 2675 247 261
-- Name: uyemuafiyet_uye_key; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY uyemuafiyet
    ADD CONSTRAINT uyemuafiyet_uye_key FOREIGN KEY (uyeref) REFERENCES uye(rid) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 2868 (class 2606 OID 38455)
-- Dependencies: 263 251 2680
-- Name: uyeodeme_uyeaidat_key; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY uyeodeme
    ADD CONSTRAINT uyeodeme_uyeaidat_key FOREIGN KEY (uyeaidatref) REFERENCES uyeaidat(rid) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 2867 (class 2606 OID 38460)
-- Dependencies: 263 2690 261
-- Name: uyeodeme_uyemuafiyet_key; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY uyeodeme
    ADD CONSTRAINT uyeodeme_uyemuafiyet_key FOREIGN KEY (muafiyetref) REFERENCES uyemuafiyet(rid) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 2870 (class 2606 OID 38470)
-- Dependencies: 265 2653 221
-- Name: uyeodul_odultip_key; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY uyeodul
    ADD CONSTRAINT uyeodul_odultip_key FOREIGN KEY (odultipref) REFERENCES odultip(rid) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 2869 (class 2606 OID 38475)
-- Dependencies: 2675 265 247
-- Name: uyeodul_uye_key; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY uyeodul
    ADD CONSTRAINT uyeodul_uye_key FOREIGN KEY (uyeref) REFERENCES uye(rid) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 2871 (class 2606 OID 38480)
-- Dependencies: 2551 149 267
-- Name: uyeogrenim_bolum_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY uyeogrenim
    ADD CONSTRAINT uyeogrenim_bolum_fk FOREIGN KEY (bolumref) REFERENCES bolum(rid);


--
-- TOC entry 2872 (class 2606 OID 38485)
-- Dependencies: 267 2570 167
-- Name: uyeogrenim_fakulte_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY uyeogrenim
    ADD CONSTRAINT uyeogrenim_fakulte_fk FOREIGN KEY (fakulteref) REFERENCES fakulte(rid);


--
-- TOC entry 2873 (class 2606 OID 38490)
-- Dependencies: 267 2671 243
-- Name: uyeogrenim_universite_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY uyeogrenim
    ADD CONSTRAINT uyeogrenim_universite_fk FOREIGN KEY (universiteref) REFERENCES universite(rid);


--
-- TOC entry 2874 (class 2606 OID 38495)
-- Dependencies: 247 267 2675
-- Name: uyeogrenim_uye_key; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY uyeogrenim
    ADD CONSTRAINT uyeogrenim_uye_key FOREIGN KEY (uyeref) REFERENCES uye(rid) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 2875 (class 2606 OID 38505)
-- Dependencies: 2675 269 247
-- Name: uyesigorta_uye_key; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY uyesigorta
    ADD CONSTRAINT uyesigorta_uye_key FOREIGN KEY (uyeref) REFERENCES uye(rid) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 2877 (class 2606 OID 38510)
-- Dependencies: 247 2675 271
-- Name: uyeuzmanlik_uye_key; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY uyeuzmanlik
    ADD CONSTRAINT uyeuzmanlik_uye_key FOREIGN KEY (uyeref) REFERENCES uye(rid) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 2876 (class 2606 OID 38515)
-- Dependencies: 2702 271 273
-- Name: uyeuzmanlik_uzmanliktip_key; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY uyeuzmanlik
    ADD CONSTRAINT uyeuzmanlik_uzmanliktip_key FOREIGN KEY (uzmanliktipref) REFERENCES uzmanliktip(rid) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 2907 (class 2606 OID 45012)
-- Dependencies: 180 2592 309
-- Name: zamanlamaligonderim_gonderenref_key; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY gonderim
    ADD CONSTRAINT zamanlamaligonderim_gonderenref_key FOREIGN KEY (gonderenref) REFERENCES kisi(rid) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2908 (class 2606 OID 45017)
-- Dependencies: 307 309 2734
-- Name: zamanlamaligonderim_icerikref_key; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY gonderim
    ADD CONSTRAINT zamanlamaligonderim_icerikref_key FOREIGN KEY (icerikref) REFERENCES icerik(rid) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2906 (class 2606 OID 45007)
-- Dependencies: 309 2730 303
-- Name: zamanlamaligonderim_iletisimliste_key; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY gonderim
    ADD CONSTRAINT zamanlamaligonderim_iletisimliste_key FOREIGN KEY (iletisimlisteref) REFERENCES iletisimliste(rid) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 3051 (class 0 OID 0)
-- Dependencies: 6
-- Name: public; Type: ACL; Schema: -; Owner: toys
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM toys;
GRANT ALL ON SCHEMA public TO toys;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


-- Completed on 2013-08-15 10:24:06

--
-- PostgreSQL database dump complete
--

